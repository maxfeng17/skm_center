﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using LunchKingSite.Mongo.Services;
using System.Text.RegularExpressions;
using LunchKingSite.Mongo.Facade;
using System.Linq.Expressions;
using LunchKingSite.Core.Component;
using System.Diagnostics;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesManagementPresenter : Presenter<ISalesManagementView>
    {
        private IHumanProvider _humanProv;
        private ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private SalesService _salesServ;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetEmployeeData();
            View.GetSalesDepartment(GetSalesDepartment());
            GetSalesLeaderListData();
            GetBusinessOrderData(1);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnDeptSaveClicked += OnDeptSaveClicked;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            View.OnDownload += OnDownload;
            View.OnShow += OnShow;
            return true;
        }

        public SalesManagementPresenter(IHumanProvider hp, ISystemProvider sp, SalesService salesServ)
        {
            _humanProv = hp;
            _salesServ = salesServ;
        }

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetBusinessOrderData(1);
        }
        protected void OnDeptSaveClicked(object sender, DataEventArgs<Dictionary<string, string>> e)
        {
            foreach (KeyValuePair<string, string> item in e.Data)
            {
                SalesMember sales = GetSalesMemberData(item.Key);
                if (sales == null)
                {
                    sales = new SalesMember();
                    sales.SalesId = item.Key.ToLower();
                    // 設定職位
                    int managerCount = 1;
                    List<SalesMember> managers = _salesServ.GetSalesMemberList(Level.Manager);
                    if (managers.Count < managerCount)
                        sales.Level = Level.Manager;
                    else
                        sales.Level = Level.Leader;
                }

                if (!string.IsNullOrEmpty(item.Value))
                {
                    sales.Area = item.Value;
                    _salesServ.SaveSalesMember(sales);
                }
                else
                    _salesServ.DeleteSalesMember(sales); // 沒有審核權限則清除資料
            }

            GetSalesLeaderListData();
        }
        #endregion

        #region method
        protected void GetBusinessOrderData(int pageNumber)
        {
            View.GetBusinessList(_salesServ.BusinessOrderGetListByPage(GetExpressions(), x => x.Desc(y => y.CreateTime), View.PageSize, pageNumber));
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _salesServ.BusinessOrderGetListCount(GetExpressions());
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            GetBusinessOrderData(e.Data);
        }

        protected void OnDownload(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(e.Data.Value);
            BusinessOrderDownloadType type = View.IsDownloadPrivilege || businessOrder.IsTravelType ? BusinessOrderDownloadType.Word : BusinessOrderDownloadType.Pdf;

            if (string.IsNullOrWhiteSpace(e.Data.Key))
            {
                // 下載總店
                Dictionary<string, string> dataList = SalesFacade.GetBusinessOrderDictionary(businessOrder, null, type, View.IsDownloadPrivilege);
                View.DownloadTemplate(type, businessOrder.BusinessType, dataList, businessOrder.Store);
            }
            else
            {
                // 下載指定分店
                SalesStore store = businessOrder.Store.Where(x => x.BranchName == e.Data.Key).FirstOrDefault();
                if (store != null)
                {
                    Dictionary<string, string> dataList = SalesFacade.GetBusinessOrderDictionary(businessOrder, store, type, View.IsDownloadPrivilege);
                    View.DownloadTemplate(type, businessOrder.BusinessType, dataList, null);
                }
            }
        }

        protected void OnShow(object sender, DataEventArgs<string> e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(e.Data);
            Dictionary<string, string> dataList = SalesFacade.GetBusinessOrderDictionary(businessOrder, null, BusinessOrderDownloadType.Pdf, true);
            View.OpenHtmlWindow(businessOrder.BusinessType, dataList, businessOrder.Store);
        }
        #endregion

        #region Private Method
        private List<Department> GetSalesDepartment()
        {
            return _humanProv.DepartmentGetListByEnabled(true).ToList().Union(_humanProv.DepartmentGetListByEnabled(true, EmployeeDept.M000)).ToList();
        }

        private Expression<Func<BusinessOrder, bool>> GetExpressions()
        {
            ParameterExpression parameter = Expression.Parameter(typeof(BusinessOrder));

            // 一般搜尋條件
            Expression<Func<BusinessOrder, bool>> expr = x =>
                x.SellerName.Contains(View.SellerName)
                && x.CompanyName.Contains(View.CompanyName)
                && x.BusinessOrderId.Contains(View.BusinessOrderId)
                && View.Status.Contains((int)x.Status);

            // 上檔日期
            if (View.BusinessStartTimeS != null && View.BusinessStartTimeE != null)
            {
                Expression<Func<BusinessOrder, bool>> expr_add = x =>
                    x.BusinessHourTimeS >= (View.BusinessStartTimeS ?? DateTime.MinValue)
                    && (x.BusinessHourTimeS < (View.BusinessStartTimeE ?? DateTime.MaxValue));

                Combine(ref expr, expr_add, parameter);
            }

            // 建立日期
            if (View.CreateTimeStart != null && View.CreateTimeEnd != null)
            {
                Expression<Func<BusinessOrder, bool>> expr_add = x =>
                    x.CreateTime >= (View.CreateTimeStart ?? DateTime.MinValue)
                    && x.CreateTime < (View.CreateTimeEnd ?? DateTime.MaxValue);

                Combine(ref expr, expr_add, parameter);
            }

            // 檔次類型
            if (View.BusinessType != BusinessType.None)
            {
                expr = expr.And(t => t.BusinessType == View.BusinessType);
            }

            // 旅遊檔次查詢
            if (View.IsTravelType)
            {
                expr = expr.And(x => x.IsTravelType == true);
            }

            // 墨攻檔次查詢
            if (View.IsMohistVerify)
            {
                expr = expr.And(x => x.IsMohistVerify == true);
            }

            // 華泰檔次查詢
            if (View.IsTrustHwatai)
            {
                expr = expr.And(x => x.IsTrustHwatai == true);
            }

            // 合約是否送交查詢
            if (View.IsNotContractSend)
            {
                expr = expr.And(x => x.IsContractSend == false);
            }

            // 免稅/其他 查詢
            if (View.InvoiceType == InvoiceType.DuplicateUniformInvoice)
            {
                expr = expr.And(x =>
                    x.Invoice.Type == InvoiceType.DuplicateUniformInvoice && x.Invoice.InTax == "0");
            }
            else if (View.InvoiceType == InvoiceType.Other)
            {
                expr = expr.And(x => x.Invoice.Type == InvoiceType.Other);
            }

            if ((View.SalesLevel == Level.Leader || View.SalesLevel == Level.Manager) && !View.IsViewer)
            {
                // 業務主管(!Viewer): 搜尋審核區域的工單 || 業務主管自己的工單 && 搜尋業務名稱(查詢業務名稱或email)
                if (!string.IsNullOrWhiteSpace(View.SalesName))
                {
                    expr = expr.And(x => View.SalesDept.Contains(x.AuthorizationGroup)
                        && ((string.IsNullOrWhiteSpace(View.SalesName)) ||
                        x.SalesName.Contains(View.SalesName) ||
                        x.CreateId.Contains(View.SalesName)));
                }
                else
                {
                    expr = expr.And(x => View.SalesDept.Contains(x.AuthorizationGroup) || x.CreateId == View.SalesId || x.SalesName == View.EmpName);
                }
            }
            else if (View.IsSalesAssistant)
            {
                // 業務助理
                if (View.CreateIdSearch.Equals(View.LoginUser, StringComparison.OrdinalIgnoreCase))
                {
                    // 搜尋自己建立的工單
                    expr = expr.And(x => x.CreateId.Contains(View.CreateIdSearch));
                }
                else if (!string.IsNullOrWhiteSpace(View.CreateIdSearch))
                {
                    // 搜尋所屬業務區域成員建立的工單
                    expr = expr.And(x => x.SalesDeptId == View.EmpDeptId && x.CreateId.Contains(View.CreateIdSearch));
                }
                else
                {
                    // 搜尋自己業務區域的工單
                    expr = expr.And(x => x.SalesDeptId == View.EmpDeptId);
                }

                if (!string.IsNullOrWhiteSpace(View.SalesName))
                {
                    // 搜尋負責業務名稱的工單
                    expr = expr.And(x => x.SalesName == View.SalesName);
                }
            }
            else if (View.SalesLevel == Level.Specialist && !string.IsNullOrWhiteSpace(View.SalesId) && !View.IsViewer)
            {
                // 業務(!Viewer): 業務只能搜尋自己的工單
                expr = expr.And(x => x.CreateId == View.SalesId || (!string.IsNullOrWhiteSpace(View.EmpName) && x.SalesName == View.EmpName));
            }
            else
            {
                // 創編/營管/Viewer: 搜尋審核區域的工單(不查詢特定區域會傳入審核區域) && 搜尋業務名稱(查詢業務名稱或email)
                expr = expr.And(x => View.SalesDept.Contains(x.AuthorizationGroup));

                if (!string.IsNullOrWhiteSpace(View.SalesName))
                {
                    expr = expr.And(x => x.SalesName == View.SalesName);
                }
                if (!string.IsNullOrWhiteSpace(View.CreateIdSearch))
                {
                    expr = expr.And(x =>x.CreateId.Contains(View.CreateIdSearch));
                }
            }
            return expr;
        }

        private static void Combine<T, K>(ref Expression<Func<T, K>> left_exp, Expression<Func<T, K>> right_exp, ParameterExpression parameter)
        {
            left_exp = Expression.Lambda<Func<T, K>>(Expression.AndAlso(left_exp.Body, right_exp.Body), parameter);
        }

        private SalesMember GetSalesMemberData(string userId)
        {
            SalesMember salesMember = _salesServ.GetSalesMember(userId);
            if (salesMember != null)
            {
                View.SalesLevel = salesMember.Level;
                View.SalesArea = salesMember.Area;
            }

            return salesMember;
        }

        private void SetEmployeeData()
        {
            ViewEmployee emp = _humanProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.SalesId);
            if (emp.IsLoaded)
            {
                View.EmpName = emp.EmpName;
                View.EmpDeptId = emp.DeptId;
            }
            else if (!string.IsNullOrWhiteSpace(View.SalesId))
            {
                View.ShowMessage("您尚未設定歸屬部門，請洽該區特助人員!");
            }
        }

        private void GetSalesLeaderListData()
        {
            GetSalesMemberData(View.SalesId);
            View.GetSalesLeaderList(GetSalesLevelData(Level.Leader, View.SalesLevel, View.SalesId));
        }

        private Dictionary<SalesMember, List<Department>> GetSalesLevelData(Level level, Level userLevel, string userId)
        {
            List<SalesMember> salesList = new List<SalesMember>();
            switch (userLevel)
            {
                case Level.Leader:
                    // 若為區主管，設定可瀏覽權限
                    salesList = _salesServ.GetSalesMemberList(level, userId);
                    break;
                case Level.Manager:
                    // 若為經理，則帶出區主管權限清單
                    salesList = _salesServ.GetSalesMemberList(level);
                    break;
                case Level.Specialist:
                    // 其餘表示尚未設定權限層級，則帶出UserId，並須設定權限
                    salesList.Add(new SalesMember() { SalesId = View.SalesId });
                    break;
                default:
                    break;
            }

            Dictionary<SalesMember, List<Department>> dataList = new Dictionary<SalesMember, List<Department>>();
            List<Department> deptc = GetSalesDepartment();
            foreach (SalesMember member in salesList)
            {
                dataList.Add(member, deptc);
            }
            return dataList;
        }

        #endregion
    }
}

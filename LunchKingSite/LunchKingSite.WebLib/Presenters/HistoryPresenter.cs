﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Enumeration;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System.IO;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class HistoryPresenter : Presenter<IHistoryView>
    {
        protected IHumanProvider hp= ProviderFactory.Instance().GetProvider<IHumanProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            //View.HistoryList(GetHistoryList(1));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Export += OnExport;
            View.AddClaimed += OnAddClaimed;
            View.HistoryCount += OnHistoryCount;
            View.HistoryPageChanged += OnHistoryPageChanged;

            return true;
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.HistoryList(GetHistoryList(1));
        }
        protected void OnExport(object sender, EventArgs e)
        {
            ViewCustomerServiceListCollection vcsls = sp.GetViewCustomerServiceMessageForNotCaimed(1, 99999, ViewCustomerServiceList.Columns.CreateTime, GetFilter());

            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("客服案件");

            #region 表頭 及 title
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("案件編號");
            cols.CreateCell(1).SetCellValue("案件狀態");
            cols.CreateCell(2).SetCellValue("問題等級");
            cols.CreateCell(3).SetCellValue("案件來源");
            cols.CreateCell(4).SetCellValue("裝置平台");
            cols.CreateCell(5).SetCellValue("姓名");
            cols.CreateCell(6).SetCellValue("帳號");
            cols.CreateCell(7).SetCellValue("訂單編號");
            cols.CreateCell(8).SetCellValue("供應商");
            cols.CreateCell(9).SetCellValue("問題分類");
            cols.CreateCell(10).SetCellValue("主因");
            cols.CreateCell(11).SetCellValue("建立時間");
            cols.CreateCell(12).SetCellValue("最後異動時間");
            cols.CreateCell(13).SetCellValue("處理人員1");
            cols.CreateCell(14).SetCellValue("處理人員2");
            cols.CreateCell(15).SetCellValue("來源平台");
            cols.CreateCell(16).SetCellValue("台新帳號");
            #endregion 表頭 及 title

            #region 內容
            int rowIdx = 1;
            foreach (ViewCustomerServiceList vcsl in vcsls)
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(vcsl.ServiceNo);    //案件編號
                row.CreateCell(1).SetCellValue(Helper.GetEnumDescription((statusConvert)vcsl.CustomerServiceStatus));    //案件狀態
                row.CreateCell(2).SetCellValue(Helper.GetEnumDescription((priorityConvert)vcsl.CasePriority));    //問題等級
                row.CreateCell(3).SetCellValue(Helper.GetEnumDescription((messageConvert)vcsl.MessageType));    //案件來源
                row.CreateCell(4).SetCellValue(Helper.GetEnumDescription((Source)vcsl.Source));    //來源平台
                row.CreateCell(5).SetCellValue(vcsl.Name);  //姓名
                string account = vcsl.Email;
                if (vcsl.UserId != null)
                {
                    Member mem = mp.MemberGetbyUniqueId(vcsl.UserId.Value);
                    if (mem.IsLoaded)
                    {
                        if (mem.UserName.ToLower().Contains("@mobile"))
                        {
                            MobileMember mobilemem = mp.MobileMemberGet(vcsl.UserId.Value);
                            if (mobilemem.IsLoaded)
                            {
                                account = mobilemem.MobileNumber;
                            }
                        }
                        else
                        {
                            account = mem.UserName;
                        }
                    }
                }
                row.CreateCell(6).SetCellValue(account);  //帳號

                string deliveryType = string.Empty;
                string orderId = string.Empty;
                string supplier = string.Empty;
                if (vcsl.OrderGuid != null)
                {
                    if (vcsl.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        deliveryType = "(憑證)";
                    }
                    else
                    {
                        deliveryType = "(宅配)";
                    }

                    Order order = op.OrderGet(vcsl.OrderGuid.Value);
                    if (order.IsLoaded)
                    {
                        orderId = order.OrderId;
                        supplier = order.SellerName;
                    }
                }

                row.CreateCell(7).SetCellValue(orderId);    //訂單編號
                row.CreateCell(8).SetCellValue(supplier); //供應商

                string category = sp.CustomerServiceCategoryGet(vcsl.Category).CategoryName + deliveryType;
                row.CreateCell(9).SetCellValue(category);    //問題分類

                string subject = sp.CustomerServiceCategoryGet(vcsl.SubCategory).CategoryName;
                row.CreateCell(10).SetCellValue(subject); //主因
                row.CreateCell(11).SetCellValue(vcsl.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"));   //建立時間
                row.CreateCell(12).SetCellValue(vcsl.ModifyTime == null ? "" : vcsl.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm:ss")); //最後異動時間

                string workUser = vcsl.ServicePeopleName;
                if (vcsl.ServicePeopleId != null)
                {
                    var mem = mp.MemberGetbyUniqueId(vcsl.ServicePeopleId.Value);
                    workUser = mem.LastName + mem.FirstName;
                }
                row.CreateCell(13).SetCellValue(workUser);   //處理人員1
                string secWorkUser = vcsl.SecServicePeopleName;
                if (vcsl.SecServicePeopleId != null)
                {
                    var mem = mp.MemberGetbyUniqueId(vcsl.SecServicePeopleId.Value);
                    secWorkUser = mem.LastName + mem.FirstName;
                }
                row.CreateCell(14).SetCellValue(secWorkUser);   //處理人員2
                row.CreateCell(15).SetCellValue(Helper.GetEnumDescription((IssueFromType)vcsl.IssueFromType));
                row.CreateCell(16).SetCellValue(vcsl.TsMemberNo);
                rowIdx++;
            }
            #endregion 內容

            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(DateTime.Today.ToString("yyyyMMdd") + @".xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);

        }
        protected void OnAddClaimed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.ChoseBox))
            {
                string[] serviceNoArray = View.ChoseBox.Split(',');
                foreach (var item in serviceNoArray)
                {
                    var csm = sp.CustomerServiceMessageGet(item);
                    if (csm.IsLoaded)
                    {
                        csm.Status = (int)statusConvert.process;
                        csm.ModifyId = View.UserId;
                        csm.ModifyTime = DateTime.Now;
                        csm.WorkUser = View.UserId;
                        sp.CustomerServiceMessageSet(csm);
                    }
                }
            }
            View.HistoryList(GetHistoryList(1));
        }
        protected void OnHistoryCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.GetViewCustomerServiceMessageListCount(GetFilter());
        }

        protected void OnHistoryPageChanged(object sender, DataEventArgs<int> e)
        {
            View.HistoryList(GetHistoryList(e.Data));
        }

        #endregion

        private ViewCustomerServiceListCollection GetHistoryList(int pageNumber)
        {
            return sp.GetViewCustomerServiceMessageForNotCaimed(pageNumber, View.PageSize, ViewCustomerServiceList.Columns.CreateTime, GetFilter());
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            if (View.SearchUserId != 0)
            {
                filter.Add(ViewCustomerServiceList.Columns.ServicePeopleId + " = " + View.SearchUserId);
            }
            if(View.SearchSecUserId != 0)
            {
                filter.Add(ViewCustomerServiceList.Columns.SecServicePeopleId + " = " + View.SearchSecUserId);
            }

            InitialFilter(filter);

            return filter.ToArray();
        }

        private List<string> InitialFilter(List<string> filter)
        {
            if (!string.IsNullOrEmpty(View.SearchServiceNo))
            {
                filter.Add(ViewCustomerServiceList.Columns.ServiceNo + " = " + View.SearchServiceNo);
            }
            if (View.SearchOrderGuid != Guid.Empty)
            {
                filter.Add(ViewCustomerServiceList.Columns.OrderGuid + " = " + View.SearchOrderGuid);
            }
            if (!string.IsNullOrEmpty(View.Mail))
            {
                Member mem = mp.MemberGetByUserName(View.Mail);
                if (mem.IsLoaded)
                {
                    filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mem.UniqueId);
                }
                else
                {
                    MobileMember mobileMem = mp.MobileMemberGet(View.Mail);
                    if (mobileMem.IsLoaded)
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mobileMem.UserId);
                    }
                    else
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = 0 ");
                    }
                }
            }

            if (!string.IsNullOrEmpty(View.TsMemberNo))
            {
                filter.Add(ViewCustomerServiceList.Columns.TsMemberNo + " = " + View.TsMemberNo);
            }
            if (View.SearchPriority > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.CasePriority + " = " + View.SearchPriority);
            }
            if (View.SearchCategory > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.Category + " = " + View.SearchCategory);
            }
            if (View.SubCategory > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.SubCategory + " = " + View.SubCategory);
            }
            if (View.MessageType > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.MessageType + " = " + View.MessageType);
            }
            if (View.CaseSource > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.Source + " = " + View.CaseSource);
            }

            if (View.IssueFromType > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.IssueFromType + " = " + View.IssueFromType);
            }

            if (!string.IsNullOrEmpty(View.UniqueId))
            {
                ViewPponDeal vpd = pp.ViewPponDealGetByUniqueId(Convert.ToInt32(View.UniqueId));
                if (Helper.IsFlagSet(vpd.BusinessHourStatus,BusinessHourStatus.ComboDealMain))
                {
                    filter.Add(ViewCustomerServiceList.Columns.MainBid + " = " + vpd.BusinessHourGuid);
                }
                else
                {
                    filter.Add(ViewCustomerServiceList.Columns.BusinessHourGuid + " = " + vpd.BusinessHourGuid);
                }
            }
            if (View.SearchStatus > -1)
            {
                filter.Add(ViewCustomerServiceList.Columns.CustomerServiceStatus + " = " + View.SearchStatus);
            }
            if (!string.IsNullOrEmpty(View.Supplier))
            {
                filter.Add(ViewCustomerServiceList.Columns.SellerName + " like %" + View.Supplier + "%");
            }
            if (!string.IsNullOrEmpty(View.SDate))
            {
                string date = string.Format("{0} {1}:{2}", View.SDate, View.SHour, View.SMinute);
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " >= " + date);
            }
            if (!string.IsNullOrEmpty(View.EDate))
            {
                string date = string.Format("{0} {1}:{2}", View.EDate, View.EHour, View.EMinute);
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " <= " + date);
            }

            if (View.SearchPponDealType != -1)
            {
                if(View.SearchPponDealType == 0)
                {
                    //憑證
                    filter.Add(ViewCustomerServiceList.Columns.DeliveryType + " = " + (int)DeliveryType.ToShop);
                }
                else if (View.SearchPponDealType == 1)
                {
                    //一般宅配
                    filter.Add(ViewCustomerServiceList.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse);
                }
                else if (View.SearchPponDealType == 2)
                {
                    //超取訂單
                    filter.Add(ViewCustomerServiceList.Columns.ProductDeliveryType + " IN ( " + (int)ProductDeliveryType.FamilyPickup + "," + (int)ProductDeliveryType.SevenPickup + ")");
                }
                else if (View.SearchPponDealType == 3)
                {
                    //24H
                    filter.Add(ViewCustomerServiceList.Columns.ProductDeliveryType + " = " + (int)ProductDeliveryType.Wms);
                }
            }
            return filter;
        }

    }
}

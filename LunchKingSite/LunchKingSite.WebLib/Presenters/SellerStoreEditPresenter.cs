﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using Microsoft.SqlServer.Types;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NewtonsoftJson = Newtonsoft.Json;

namespace LunchKingSite.WebLib.Presenters
{
    public class SellerStoreEditPresenter : Presenter<ISellerStoreEditView>
    {
        private IPponProvider _pponProv;
        private ISellerProvider _sellerProv;
        private IMemberProvider _memProv;

        public SellerStoreEditPresenter(IPponProvider pponProv, ISellerProvider sellerProv)
        {
            _pponProv = pponProv;
            _sellerProv = sellerProv;
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            //新增模式填入預設值
            if (View.WorkType == SellerStoreEditViewWorkType.Add)
            {
                ShowDefaultData();
                //GetAllStores();
            }
            //修改模式需先顯示資料內容
            else if (View.WorkType == SellerStoreEditViewWorkType.Modify)
            {
                ShowStoreData();
            }

            View.SetLocationDropDownList();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveSellerStore += OnSaveSellerStore;
            View.CancelEdit += OnCancelEdit;
            View.ReturnApply += OnReturnApply;

            return true;
        }

        private void OnReturnApply(object sender, EventArgs e)
        {
            if (View.StoreGuid != Guid.Empty)
            {
                var store = _sellerProv.StoreGet(View.StoreGuid);
                store.ModifyId = View.UserName;
                store.ReturnTime = store.ModifyTime = DateTime.Now;
                store.TempStatus = (int)SellerTempStatus.Returned;
                store.Message = View.Message;
                _sellerProv.StoreSet(store);
                View.GoToSellerStoreList();
            }
        }

        public void ShowStoreData()
        {
            var store = _sellerProv.StoreGet(View.StoreGuid);
            if (store.IsLoaded)
            {
                var township = CityManager.TownShipGetById(store.TownshipId == null ? -1 : store.TownshipId.Value);
                SellerOrStoreCloseDownInformation info = new SellerOrStoreCloseDownInformation();
                info.HDmailHeaderUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Header.png";
                info.HDmailFooterUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Footer.png";

                Seller seller = _sellerProv.SellerGet(View.SellerGuid);
                View.SellerName = seller.SellerName;
                View.BranchName = store.StoreName;
                View.StoreTel = store.Phone;
                View.AddressCityId = township == null ? -1 : township.ParentId.Value;
                View.AddressTownshipId = township == null ? -1 : township.Id;
                View.StoreAddress = store.AddressString;
                SqlGeography geography = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                if (geography != null)
                {
                    View.LongitudeLatitude = new KeyValuePair<string, string>(geography.Lat.ToString(), geography.Long.ToString());
                }
                else
                {
                    View.LongitudeLatitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(View.AddressTownshipId.Value) + store.AddressString);
                }
                View.BusinessHour = store.OpenTime;
                View.CloseDateInformation = store.CloseDate;
                View.Remark = store.Remarks;
                View.IsOpenBooking = store.IsOpenBooking;
                View.IsOpenReservationSetting = store.IsOpenReservationSetting;
                View.Status = (StoreStatus)store.Status;
                View.IsCloseDown = store.IsCloseDown;
                View.CloseDownDate = store.CloseDownDate;
                View.TrafficMrt = store.Mrt;
                View.TrafficCar = store.Car;
                View.TrafficBus = store.Bus;

                View.TrafficOther = store.OtherVehicles;
                View.WebUrl = store.WebUrl;
                View.FaceBookUrl = store.FacebookUrl;
                View.PlurkUrl = store.PlurkUrl;
                View.BlogUrl = store.BlogUrl;
                View.OtherUrl = store.OtherUrl;

                View.CompanyAccount = store.CompanyAccount;
                View.CompanyAccountName = store.CompanyAccountName;
                View.AccountantName = store.AccountantName;
                View.AccountantTel = store.AccountantTel;
                View.CompanyName = store.CompanyName;
                View.CompanyBankCode = store.CompanyBankCode;
                View.CompanyBossName = store.CompanyBossName;
                View.CompanyBranchCode = store.CompanyBranchCode;
                View.CompanyNotice = store.CompanyNotice;
                View.CompanyEmail = store.CompanyEmail;
                View.CompanyID = store.CompanyID;
                View.SignCompanyID = store.SignCompanyID;
                View.CreditCardAvailable = store.CreditcardAvailable;
                View.StoreRelationCode = store.StoreRelationCode;
                
                #region store_category

                var viewStoreCategoryList =_sellerProv.ViewStoreCategoryCollectionGetByStore(store.Guid).ToList().Where(x => x.IsFinal != false)//排除掉父節點(ex.淡水信義線)
                                                                                                       .GroupBy(x => x.CategoryCode).Select(x => x.First())//排除掉兩線重疊的站點(ex.台北車站)
                                                                                                       .ToList();

                View.MRTLocationCategory = viewStoreCategoryList.Select(x => x.CategoryCode).ToList();
                View.MRTLocationChecked = viewStoreCategoryList.Select(x => new KeyValuePair<string, int>(x.Name, x.CategoryCode)).ToList();
                
                #endregion store_category

                View.Message = store.Message;
                //抓取最新一筆異動紀錄
                LunchKingSite.DataOrm.ChangeLogCollection changelogs = VourcherFacade.ChangeLogCollectionGetAll(LunchKingSite.DataOrm.Store.Schema.TableName, store.Guid);
                StoreCollection sts = _sellerProv.StoreGetListBySellerGuid(store.SellerGuid);
                View.SetStore(store, changelogs, sts);
                View.AuditBoardView.Presenter.OnViewInitialized();
            }
            else
            {
                View.ShowMessage("查無資料。");
            }
        }

        /// <summary>
        /// 檢查seller，如果已經有任一筆store的資料
        /// 則將下列欄位設為第一筆記錄的內容:
        /// 營業時間
        /// </summary>
        public void ShowDefaultData()
        {
            var seller = _sellerProv.SellerGet(View.SellerGuid);
            if (seller != null && seller.IsLoaded)
            {
                var stores = _sellerProv.StoreGetListBySellerGuid(seller.Guid);
                if (stores.Count > 0)
                {
                    View.BusinessHour = stores[0].OpenTime;
                }
                /*
                var sellerRelations = _sellerProv.SellerTreeGetListBySellerGuid(seller.Guid);                
                View.SellerName = seller.SellerName;
                if (sellerRelations.Count > 0)
                {
                    var store = _sellerProv.SellerGet(sellerRelations[0].SellerGuid);
                    View.BusinessHour = store.OpenTime;
                }*/
            }
        }

        /// <summary>
        /// 檢查必要的欄位是否輸入
        /// </summary>
        /// <returns>通過檢查回傳string.Empty 若有問題回傳錯誤訊息</returns>
        public string CheckInputData()
        {
            if (string.IsNullOrEmpty(View.BranchName))
            {
                return "請輸入分店名稱。";
            }

            return string.Empty;
        }

        private void OnSaveSellerStore(object sender, EventArgs e)
        {
            //檢查輸入的資料是否正確
            string checkDataMessage = CheckInputData();
            if (checkDataMessage != string.Empty)
            {
                View.ShowMessage(checkDataMessage);
                return;
            }

            var seller = _sellerProv.SellerGet(View.SellerGuid);
            //SQL DB無該筆seller資料
            if (seller == null || seller.IsLoaded == false)
            {
                View.ShowMessage("查無商家資料，可能資料庫發生問題，請確認。");
                return;
            }

            #region lksite Store

            var township = CityManager.TownShipGetById(View.AddressTownshipId == null ? -1 : View.AddressTownshipId.Value);
            var lkStore = new LunchKingSite.DataOrm.Store();
            if (View.WorkType == SellerStoreEditViewWorkType.Add)
            {
                #region AddlkStore

                lkStore.StoreName = View.BranchName;
                lkStore.SellerGuid = View.SellerGuid;
                lkStore.Phone = View.StoreTel;
                lkStore.TownshipId = View.AddressTownshipId;
                lkStore.CityId = township == null ? -1 : township.ParentId.Value;
                lkStore.AddressString = View.StoreAddress;
                lkStore.StoreRelationCode = View.StoreRelationCode;

                if (lkStore.TownshipId.HasValue)
                {
                    KeyValuePair<string, string> latitude_longitude = new KeyValuePair<string, string>();
                    double lat, lont;
                    if (!double.TryParse(View.LongitudeLatitude.Key, out lat) || !double.TryParse(View.LongitudeLatitude.Value, out lont)
                        || Math.Abs(lat) > 90 || Math.Abs(lont) > 180)
                    {
                        latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(lkStore.TownshipId.Value) + lkStore.AddressString);
                    }
                    else
                    {
                        latitude_longitude = View.LongitudeLatitude;
                    }
                    lkStore.Coordinate = LocationFacade.GetGeographyWKT(latitude_longitude.Key, latitude_longitude.Value);
                }
                lkStore.OpenTime = View.BusinessHour;
                lkStore.CloseDate = View.CloseDateInformation;
                lkStore.Remarks = View.Remark;
                lkStore.IsOpenBooking = View.IsOpenBooking;
                lkStore.IsOpenReservationSetting = (View.IsOpenBooking) ? View.IsOpenReservationSetting : false;
                lkStore.Status = (int)View.Status;
                lkStore.IsCloseDown = View.IsCloseDown;
              
                if (!View.IsCloseDown)
                {
                    lkStore.CloseDownDate = null;
                }
                else
                {
                    lkStore.Status = 1; //倒店 => 隱藏
                    lkStore.CloseDownDate = View.CloseDownDate;
                    List<int> vourcher_event_id_list = VourcherFacade.VourcherEventIdListGetByStoreGuid(lkStore.Guid);
                    //刪除所有優惠券分店
                    VourcherFacade.VourcherStoreDeleteByStoreGuid(lkStore.Guid);
                    VourcherFacade.ChangeLogAdd(Store.Schema.TableName, lkStore.Guid, "{\"分店倒閉隱藏調整\":\"刪除優惠券分店,影響的優惠券編號" + string.Join(",", vourcher_event_id_list.ToArray()) + "\"})", true);
                }
                lkStore.Mrt = View.TrafficMrt;
                lkStore.Car = View.TrafficCar;
                lkStore.Bus = View.TrafficBus;
                lkStore.OtherVehicles = View.TrafficOther;
                lkStore.WebUrl = View.WebUrl;
                lkStore.FacebookUrl = View.FaceBookUrl;
                lkStore.PlurkUrl = View.PlurkUrl;
                lkStore.BlogUrl = View.BlogUrl;
                lkStore.OtherUrl = View.OtherUrl;
                lkStore.CreateId = View.UserName;

                lkStore.CompanyAccount = View.CompanyAccount;
                lkStore.CompanyAccountName = View.CompanyAccountName;
                //帳務聯絡人
                lkStore.AccountantName = View.AccountantName;
                //帳務聯絡電話
                lkStore.AccountantTel = View.AccountantTel;
                lkStore.CompanyBankCode = View.CompanyBankCode;
                lkStore.CompanyBossName = View.CompanyBossName;
                lkStore.CompanyBranchCode = View.CompanyBranchCode;
                lkStore.CompanyEmail = View.CompanyEmail;
                lkStore.CompanyID = View.CompanyID;
                lkStore.SignCompanyID = View.SignCompanyID;
                lkStore.CompanyName = View.CompanyName;
                lkStore.CompanyNotice = View.CompanyNotice;
                lkStore.CreditcardAvailable = View.CreditCardAvailable;
                lkStore.CreateTime = DateTime.Now;

                #endregion AddlkStore
            }
            else
            {
                #region UpdatelkStore

                lkStore = _sellerProv.StoreGet(View.StoreGuid);

                #region aduit

                string aduitStr = string.Empty;

                if (lkStore.StoreName != View.BranchName)
                {
                    aduitStr += "StoreName : " + lkStore.StoreName + " ->" + View.BranchName + "<br />";
                }

                if (lkStore.SellerGuid != View.SellerGuid)
                {
                    aduitStr += "SellerGuid : " + lkStore.SellerGuid + " ->" + View.SellerGuid + "<br />";
                }

                if (lkStore.Phone != View.StoreTel)
                {
                    aduitStr += "Phone : " + lkStore.Phone + " ->" + View.StoreTel + "<br />";
                }

                if (lkStore.TownshipId != View.AddressTownshipId)
                {
                    aduitStr += "TownshipId : " + lkStore.TownshipId + " ->" + View.AddressTownshipId + "<br />";
                }

                if (lkStore.AddressString != View.StoreAddress)
                {
                    aduitStr += "AddressString : " + lkStore.AddressString + " ->" + View.StoreAddress + "<br />";
                }

                if (lkStore.OpenTime != View.BusinessHour)
                {
                    aduitStr += "OpenTime : " + lkStore.OpenTime + " ->" + View.BusinessHour + "<br />";
                }

                if (lkStore.CloseDate != View.CloseDateInformation)
                {
                    aduitStr += "CloseDate : " + lkStore.CloseDate + " ->" + View.CloseDateInformation + "<br />";
                }

                if (lkStore.IsOpenBooking != View.IsOpenBooking)
                {
                    aduitStr += "IsOpenBooking : " + lkStore.IsOpenBooking + " ->" + View.IsOpenBooking + "<br />";
                }

                if (lkStore.IsOpenReservationSetting != View.IsOpenReservationSetting)
                {
                    aduitStr += "IsOpenReservationSetting : " + lkStore.IsOpenReservationSetting + " ->" + View.IsOpenReservationSetting + "<br />";
                }

                if (lkStore.Status != (int)View.Status)
                {
                    aduitStr += "Status : " + lkStore.Status + " ->" + View.Status + "<br />";
                }

                if (lkStore.IsCloseDown != View.IsCloseDown)
                {
                    aduitStr += "IsCloseDown : " + lkStore.IsCloseDown + " ->" + View.IsCloseDown + "<br />";
                }

                if (lkStore.Remarks != View.Remark)
                {
                    aduitStr += "Remarks : " + lkStore.Remarks + " ->" + View.Remark + "<br />";
                }

                if (lkStore.Mrt != View.TrafficMrt)
                {
                    aduitStr += "Mrt : " + lkStore.Mrt + " ->" + View.TrafficMrt + "<br />";
                }

                if (lkStore.Car != View.TrafficCar)
                {
                    aduitStr += "Car : " + lkStore.Car + " ->" + View.TrafficCar + "<br />";
                }

                if (lkStore.Bus != View.TrafficBus)
                {
                    aduitStr += "Bus : " + lkStore.Bus + " ->" + View.TrafficBus + "<br />";
                }

                if (lkStore.OtherVehicles != View.TrafficOther)
                {
                    aduitStr += "OtherVehicles : " + lkStore.OtherVehicles + " ->" + View.TrafficOther + "<br />";
                }

                if (lkStore.WebUrl != View.WebUrl)
                {
                    aduitStr += "WebUrl : " + lkStore.WebUrl + " ->" + View.WebUrl + "<br />";
                }

                if (lkStore.FacebookUrl != View.FaceBookUrl)
                {
                    aduitStr += "FacebookUrl : " + lkStore.FacebookUrl + " ->" + View.FaceBookUrl + "<br />";
                }

                if (lkStore.PlurkUrl != View.PlurkUrl)
                {
                    aduitStr += "PlurkUrl : " + lkStore.PlurkUrl + " ->" + View.PlurkUrl + "<br />";
                }

                if (lkStore.BlogUrl != View.BlogUrl)
                {
                    aduitStr += "BlogUrl : " + lkStore.BlogUrl + " ->" + View.BlogUrl + "<br />";
                }

                if (lkStore.OtherUrl != View.OtherUrl)
                {
                    aduitStr += "OtherUrl : " + lkStore.OtherUrl + " ->" + View.OtherUrl + "<br />";
                }

                if (lkStore.CompanyAccount != View.CompanyAccount)
                {
                    aduitStr += "CompanyAccount : " + lkStore.CompanyAccount + " ->" + View.CompanyAccount + "<br />";
                }

                if (lkStore.CompanyAccountName != View.CompanyAccountName)
                {
                    aduitStr += "CompanyAccountName : " + lkStore.CompanyAccountName + " ->" + View.CompanyAccountName + "<br />";
                }

                if (lkStore.CompanyBankCode != View.CompanyBankCode)
                {
                    aduitStr += "CompanyBankCode : " + lkStore.CompanyBankCode + " ->" + View.CompanyBankCode + "<br />";
                }

                if (lkStore.CompanyBossName != View.CompanyBossName)
                {
                    aduitStr += "CompanyBossName : " + lkStore.CompanyBossName + " ->" + View.CompanyBossName + "<br />";
                }

                if (lkStore.CompanyBranchCode != View.CompanyBranchCode)
                {
                    aduitStr += "CompanyBranchCode : " + lkStore.CompanyBranchCode + " ->" + View.CompanyBranchCode + "<br />";
                }

                if (lkStore.CompanyEmail != View.CompanyEmail)
                {
                    aduitStr += "CompanyEmail : " + lkStore.CompanyEmail + " ->" + View.CompanyEmail + "<br />";
                }

                if (lkStore.CompanyID != View.CompanyID)
                {
                    aduitStr += "CompanyID : " + lkStore.CompanyID + " ->" + View.CompanyID + "<br />";
                }

                if (lkStore.CompanyName != View.CompanyName)
                {
                    aduitStr += "CompanyName : " + lkStore.CompanyName + " ->" + View.CompanyName + "<br />";
                }

                if (lkStore.CompanyNotice != View.CompanyNotice)
                {
                    aduitStr += "CompanyNotice : " + lkStore.CompanyNotice + " ->" + View.CompanyNotice + "<br />";
                }

                if (aduitStr.Length > 0)
                {
                    CommonFacade.AddAudit(View.StoreGuid, AuditType.Store, aduitStr, View.UserName, true);
                }

                #endregion aduit

                lkStore.StoreName = View.BranchName;
                lkStore.SellerGuid = View.SellerGuid;
                lkStore.Phone = View.StoreTel;
                lkStore.TownshipId = View.AddressTownshipId;
                lkStore.CityId = township == null ? -1 : township.ParentId.Value;
                lkStore.AddressString = View.StoreAddress;
                lkStore.StoreRelationCode = View.StoreRelationCode;

                if (lkStore.TownshipId.HasValue)
                {
                    KeyValuePair<string, string> latitude_longitude = new KeyValuePair<string, string>();
                    double lat, lont;
                    if (!double.TryParse(View.LongitudeLatitude.Key, out lat) || !double.TryParse(View.LongitudeLatitude.Value, out lont)
                        || Math.Abs(lat) > 90 || Math.Abs(lont) > 180)
                    {
                        latitude_longitude = LocationFacade.GetArea(CityManager.CityTownShopStringGet(lkStore.TownshipId.Value) + lkStore.AddressString);
                    }
                    else
                    {
                        latitude_longitude = View.LongitudeLatitude;
                    }
                    lkStore.Coordinate = LocationFacade.GetGeographyWKT(latitude_longitude.Key, latitude_longitude.Value);
                }
                lkStore.OpenTime = View.BusinessHour;
                lkStore.CloseDate = View.CloseDateInformation;
                lkStore.IsOpenBooking = View.IsOpenBooking;
                lkStore.IsOpenReservationSetting = (View.IsOpenBooking) ? View.IsOpenReservationSetting : false;
                lkStore.Status = (int)View.Status;

                if (View.IsCloseDown && !lkStore.IsCloseDown)       //posted form IsCloseDown && database !IsCloseDown
                {
                    SendStoreCloseDownMail(lkStore.SellerGuid, lkStore.StoreName, View.CloseDownDate);
                    EmailFacade.SendStoreCloseDownNoticeToMember(lkStore.SellerGuid, seller.SellerName, lkStore.Guid, lkStore.StoreName, View.CloseDownDate.Value);
                }
              
                if (!View.IsCloseDown)
                {
                    lkStore.CloseDownDate = null;
                }
                else
                {
                    lkStore.Status = 1; //倒店 => 狀態要改為隱藏
                    lkStore.CloseDownDate = View.CloseDownDate;
                    UpdateDealsStatus(lkStore.SellerGuid, View.CloseDownDate);
                    List<int> vourcher_event_id_list = VourcherFacade.VourcherEventIdListGetByStoreGuid(lkStore.Guid);
                    //刪除所有優惠券分店
                    VourcherFacade.VourcherStoreDeleteByStoreGuid(lkStore.Guid);
                    VourcherFacade.ChangeLogAdd(Store.Schema.TableName, lkStore.Guid, "{\"分店倒閉隱藏調整\":\"刪除優惠券分店,影響的優惠券編號" + string.Join(",", vourcher_event_id_list.ToArray()) + "\"})", true);
                }
                lkStore.IsCloseDown = View.IsCloseDown;

                lkStore.Remarks = View.Remark;
                lkStore.Mrt = View.TrafficMrt;
                lkStore.Car = View.TrafficCar;
                lkStore.Bus = View.TrafficBus;
                lkStore.OtherVehicles = View.TrafficOther;
                lkStore.WebUrl = View.WebUrl;
                lkStore.FacebookUrl = View.FaceBookUrl;
                lkStore.PlurkUrl = View.PlurkUrl;
                lkStore.BlogUrl = View.BlogUrl;
                lkStore.OtherUrl = View.OtherUrl;
                lkStore.ModifyId = View.UserName;

                lkStore.CompanyAccount = View.CompanyAccount;
                lkStore.CompanyAccountName = View.CompanyAccountName;
                //帳務聯絡人
                lkStore.AccountantName = View.AccountantName;
                //帳務聯絡電話
                lkStore.AccountantTel = View.AccountantTel;
                lkStore.CompanyBankCode = View.CompanyBankCode;
                lkStore.CompanyBossName = View.CompanyBossName;
                lkStore.CompanyBranchCode = View.CompanyBranchCode;
                lkStore.CompanyEmail = View.CompanyEmail;
                lkStore.CompanyID = View.CompanyID;
                lkStore.SignCompanyID = View.SignCompanyID;
                lkStore.CompanyName = View.CompanyName;
                lkStore.CompanyNotice = View.CompanyNotice;
                lkStore.CreditcardAvailable = View.CreditCardAvailable;
                lkStore.ApproveTime = lkStore.ModifyTime = DateTime.Now;
                lkStore.TempStatus = (int)SellerTempStatus.Completed;
                lkStore.Message = string.Empty;
                lkStore.NewCreated = false;

                if (HttpContext.Current.Session["TempStore"] != null)
                {
                    Store TempStore = (Store)HttpContext.Current.Session["TempStore"];
                    //EmailFacade.SendChangeStoreMail(lkStore, TempStore);
                    HttpContext.Current.Session.Remove("TempStore");
                }

                #endregion UpdatelkStore
            }

            _sellerProv.StoreSet(lkStore);

            #region store_category

            var storeCategoryCol = _sellerProv.StoreCategoryCollectionGetByStore(lkStore.Guid);//old
            var mrtLocationCategoryList = View.MRTLocationCategory;//new

            if (storeCategoryCol.Any())
            {
                #region Update store_category

                var previousCategoryList = storeCategoryCol.ToList();

                var parentList = _sellerProv.ViewCategoryDependencyGetByCategoryCodeList(mrtLocationCategoryList);
                List<int> parentCodeList = new List<int>();
                if (parentList != null)
                {
                    parentCodeList = parentList.Select(x => x.ParentCode).Where(x => x != null).Distinct().Cast<int>().ToList();
                }

                #region 新增新的站點
                foreach (var code in mrtLocationCategoryList)
                {
                    if (previousCategoryList.All(x => x.CategoryCode != code))
                    {
                        var storeCategory = new StoreCategory()
                        {
                            StoreGuid = lkStore.Guid,
                            CategoryCode = code,
                            CreateType = (int)MrtReleaseshipStoreSettingType.System,
                            Valid = 1,
                            ModifyId = View.UserName,
                            ModifyTime = DateTime.Now
                        };
                        _sellerProv.StoreCategorySet(storeCategory);
                    }
                }
                #endregion

                #region 註解刪除 "舊站點"or"舊站點的父節點"

                foreach (var storeCategory in previousCategoryList.Where(x => x.Valid == 1).ToList())
                {
                    if (!(mrtLocationCategoryList.Contains(storeCategory.CategoryCode) || parentCodeList.Contains(storeCategory.CategoryCode)))
                    {
                        storeCategory.Valid = 0;
                        storeCategory.ModifyId = View.UserName;
                        storeCategory.ModifyTime = DateTime.Now;
                        _sellerProv.StoreCategorySet(storeCategory);
                    }
                }

                #endregion

                #region 新增新站點的父節點

                foreach (var code in parentCodeList)
                {
                    if (previousCategoryList.Where(x => x.CategoryCode == code).Count() > 0)
                    {
                        //更新
                        var storeCategory = new StoreCategory()
                        {
                            Valid = 1,
                            ModifyId = View.UserName,
                            ModifyTime = DateTime.Now
                        };
                        _sellerProv.StoreCategorySet(storeCategory);
                    }
                    else
                    {
                        //新增
                        var storeCategory = new StoreCategory()
                        {
                            StoreGuid = lkStore.Guid,
                            CategoryCode = code,
                            CreateType = (int)MrtReleaseshipStoreSettingType.System,
                            Valid = 1,
                            ModifyId = View.UserName,
                            ModifyTime = DateTime.Now
                        };
                        _sellerProv.StoreCategorySet(storeCategory);
                    }
                }

                #endregion

                #endregion
            }
            else
            {
                #region Insert store_category

                #region 新增[分店與站點]的關係(store - category) ex.台北車站

                foreach (var code in mrtLocationCategoryList)
                {
                    var storeCategory = new StoreCategory()
                    {
                        StoreGuid = lkStore.Guid,
                        CategoryCode = code,
                        CreateType = (int)MrtReleaseshipStoreSettingType.System,
                        Valid = 1,
                        ModifyId = View.UserName,
                        ModifyTime = DateTime.Now
                    };
                    _sellerProv.StoreCategorySet(storeCategory);
                }

	            #endregion

                #region 新增[分店與站點父節點]的關係(store - category(parent)) ex.淡水信義線

                ViewCategoryDependencyCollection parentList = _sellerProv.ViewCategoryDependencyGetByCategoryCodeList(mrtLocationCategoryList);
                if (parentList != null)
                {
                    List<int> parentCodeList = parentList.Select(x => x.ParentCode).Where(x => x != null).Distinct().Cast<int>().ToList();
                    foreach (var code in parentCodeList)
                    {
                        var storeCategory = new StoreCategory()
                        {
                            StoreGuid = lkStore.Guid,
                            CategoryCode = code,
                            CreateType = (int)MrtReleaseshipStoreSettingType.System,
                            Valid = 1,
                            ModifyId = View.UserName,
                            ModifyTime = DateTime.Now
                        };
                        _sellerProv.StoreCategorySet(storeCategory);
                    }
                }

                #endregion
                
                #endregion
            }

            #endregion store_category

            VourcherFacade.ChangeLogUpdateEnabled(LunchKingSite.DataOrm.Store.Schema.TableName, lkStore.Guid, false);

            #endregion lksite Store

            View.GoToSellerStoreList();
        }

        private void UpdateDealsStatus(Guid sellerGuid, DateTime? closeDownTime)
        {
            BusinessHourCollection bhs = _sellerProv.BusinessHourGetListBySeller(sellerGuid);
            IEnumerable<BusinessHour> bhFiltered = bhs.Where(bh => bh.BusinessHourDeliverTimeE > closeDownTime);
            foreach (BusinessHour bh in bhFiltered)
            {
                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange);
                _sellerProv.BusinessHourSet(bh);
            }
        }

        private void SendStoreCloseDownMail(Guid sellerGuid, string storeName, DateTime? closeDownTime)
        {
            SellerOrStoreCloseDownInformation info = new SellerOrStoreCloseDownInformation();
            Seller seller = _sellerProv.SellerGet(sellerGuid);
            Member mem = _memProv.MemberGet(View.UserName);

            info.SellerName = seller.SellerName;
            info.StoreName = storeName;
            info.CloseDownDate = closeDownTime.HasValue ? closeDownTime.Value.ToString("yyyy/MM/dd") : "";
            info.HDmailHeaderUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Header.png";
            info.HDmailFooterUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Footer.png";

            HiDealMailFacade.SendSellerOrStoreCloseDownNotification(View.UserName, mem.DisplayName, info);
        }

        private void OnCancelEdit(object sender, EventArgs e)
        {
            View.GoToSellerStoreList();
        }

        protected string TrafficInformationGet()
        {
            var traff = new List<string>();
            if (!string.IsNullOrWhiteSpace(View.TrafficMrt))
            {
                traff.Add(View.TrafficMrt);
            }

            if (!string.IsNullOrWhiteSpace(View.TrafficCar))
            {
                traff.Add(View.TrafficCar);
            }

            if (!string.IsNullOrWhiteSpace(View.TrafficBus))
            {
                traff.Add(View.TrafficBus);
            }

            if (!string.IsNullOrWhiteSpace(View.TrafficOther))
            {
                traff.Add(View.TrafficOther);
            }
            return string.Join("/", traff);
        }
    }
}
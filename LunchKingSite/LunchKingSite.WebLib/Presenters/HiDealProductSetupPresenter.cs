﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Component;
using System.Data;
using log4net;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealProductSetupPresenter : Presenter<IHiDealProductSetupView>
    {
        protected IHiDealProvider _hp;
        public event EventHandler ProductSaved = delegate { };

        public HiDealProductSetupPresenter()
        {
            GetProviders();
        }

        protected virtual void GetProviders()
        {
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public override bool OnViewInitialized()
        {
            LoadViewData();

            return true;
        }

        private void LoadViewData()
        {
            HiDealProductEntity entity = new HiDealProductEntity(View.Pid);
            View.Gpid = entity.Product.Guid;
            View.Did = entity.Product.DealId;
            View.ProductName = entity.Product.Name;
            View.ProductDescription = entity.Product.Description;
            View.IsShow = entity.Product.IsOnline.HasValue && entity.Product.IsOnline.Value;
            View.UseStartDate = entity.Product.UseStartTime.HasValue ? entity.Product.UseStartTime.Value.ToString("yyyy/MM/dd") : "";
            View.UseEndDate = entity.Product.UseEndTime.HasValue ? entity.Product.UseEndTime.Value.ToString("yyyy/MM/dd") : "";
            View.ChangeExpireDate = entity.Product.ChangedExpireTime.HasValue
                                        ? entity.Product.ChangedExpireTime.Value.ToString("yyyy/MM/dd")
                                        : "";
            View.BillingModel = (VendorBillingModel)entity.Product.VendorBillingModel;
            View.PaidType = (RemittanceType)entity.Product.RemittanceType;
            View.ReceiptType = (VendorReceiptType)entity.Product.VendorReceiptType;
            View.AccountingMessage = entity.Product.Message;
            View.OriginalPrice = entity.Product.OriginalPrice.HasValue ? entity.Product.OriginalPrice.Value.ToString("######0") : "";
            View.Price = entity.Product.Price.HasValue ? entity.Product.Price.Value.ToString("######0") : "";
            View.TotalQuantity = entity.Product.Quantity.HasValue ? entity.Product.Quantity.Value.ToString() : "";
            View.PerOrderLimit = entity.Product.OrderLimit.HasValue ? entity.Product.OrderLimit.Value.ToString() : "";
            View.PerBuyerLimit = entity.Product.OrderLimitUser.HasValue ? entity.Product.OrderLimitUser.Value.ToString() : "";
            View.IsNoRefund = entity.Product.IsNoRefund;
            View.IsDaysNoRefund = entity.Product.IsDaysNoRefund;
            View.IsExpireNoRefund = entity.Product.IsExpireNoRefund;
            View.IsTaxFree = entity.Product.IsTaxFree;
            View.IsInputTaxFree = entity.Product.IsInputTaxFree;
            View.IsHomeDelivery = entity.Product.IsHomeDelivery;
            View.HomeDeliveryDescription = entity.Product.HomeDeliveryDesc;
            View.HomeDeliveryQuantity = entity.Product.HomeDeliveryQuantity;
            View.IsInStore = entity.Product.IsInStore;
            View.InStoreDescription = entity.Product.InStoreDesc;
            View.NotDeliveryIslands = !entity.Product.DeliveryIslands;
            View.IsShowDiscount = entity.Product.IsShowPriceDiscount;
            View.IsInvoiceCreate = entity.Product.IsInvoiceCreate; //開立發票註記

            View.PayToCompany = entity.Product.PayToCompany;
            View.IsCombo = entity.Product.IsCombo;

            if (string.IsNullOrWhiteSpace(entity.Product.Sms))
            {
                View.SmsTitle = "【品生活】";
                View.SmsPrefix = "舒壓達人{tel}";
                View.SmsInformation = "，編號{0}-{1}，期限{2}-{3}";
            }
            else
            {
                HiDealCouponSmsFormatter smsFormatter = new HiDealCouponSmsFormatter(entity.Product.Sms);
                View.SmsTitle = smsFormatter.GetSmsTitleFromTemplateContent();
                View.SmsPrefix = smsFormatter.GetSmsPrefixFromTemplateContent();
                View.SmsInformation = smsFormatter.GetSmsInformationFromTemplateContent();
                View.DisableSMS = entity.Product.IsSmsClose;
            }


            View.FreightIncome = GetFreightIncome();
            View.FreightExpense = GetFreightExpense();
            View.ProductCosts = GetProductCost();
            View.StoreSettings = _hp.HiDealProductStoreWithStoreInformation(View.Pid);
            View.BookingSystemType = entity.Product.BookingSystemType;
            View.AdvanceReservationDays = entity.Product.AdvanceReservationDays;
            View.CouponUsers = entity.Product.CouponUsers;
            View.IsReserveLock = entity.Product.IsReserveLock;

            #region 其他選單

            XmlDocument dom = new XmlDocument();
            dom.LoadXml("<Optionals/>");
            XmlElement root = dom.DocumentElement;

            foreach (OptionCatg catg in entity.Options)
            {
                XmlElement catgElem = dom.CreateElement("OptCatg");
                catgElem.SetAttribute("catgName", catg.CatgName);

                foreach (OptionItem item in catg.Items)
                {
                    XmlElement itemElem = dom.CreateElement("OptItem");
                    if (item.Quantity.HasValue)
                    {
                        itemElem.SetAttribute("quantity", item.Quantity.Value.ToString());
                    }
                    XmlText itemName = dom.CreateTextNode(item.ItemName);

                    itemElem.AppendChild(itemName);
                    catgElem.AppendChild(itemElem);
                }

                root.AppendChild(catgElem);
            }

            StringWriter sw = new StringWriter();
            XmlTextWriter tw = new XmlTextWriter(sw);
            dom.WriteTo(tw);
            string s = sw.ToString();
            s = s.Replace('<', '[');
            s = s.Replace('>', ']');
            View.OptionalSettings = s;

            #endregion



        }

        public override bool OnViewLoaded()
        {
            View.SaveProduct += OnSaveDeal;
            View.NewProductPurchaseCost += OnNewProductPurchaseCost;
            View.DeleteProductPurchaseCost += OnDeleteProductPurchaseCost;
            View.NewFreightIncome += OnNewFreightIncome;
            View.DeleteFreightIncome += OnDeleteFreightIncome;
            View.NewFreightExpense += OnNewFreightExpense;
            View.DeleteFreightExpense += OnDeleteFreightExpense;

            return base.OnViewLoaded();
        }

        public void OnSaveDeal(object sender, EventArgs e)
        {
            HiDealProductEntity entity = new HiDealProductEntity(View.Pid);

            #region hi_deal_product

            entity.Product.Name = View.ProductName;
            entity.Product.Description = View.ProductDescription;
            entity.Product.IsOnline = View.IsShow;
            if (!string.IsNullOrEmpty(View.UseStartDate))
                entity.Product.UseStartTime = DateTime.Parse(View.UseStartDate);
            if (!string.IsNullOrEmpty(View.UseEndDate))
                entity.Product.UseEndTime = DateTime.Parse(View.UseEndDate);
            if (!string.IsNullOrEmpty(View.ChangeExpireDate))
                entity.Product.ChangedExpireTime = DateTime.Parse(View.ChangeExpireDate);

            entity.Product.RemittanceType = (int)View.PaidType;
            entity.Product.VendorReceiptType = (int)View.ReceiptType;
            entity.Product.Message = View.AccountingMessage;
            // 墨攻核銷，信託陽信銀行
            entity.Product.VendorBillingModel = (int)View.BillingModel;
            entity.Product.TrustType = (View.BillingModel == VendorBillingModel.MohistSystem) ? (int)TrustProvider.Sunny : (int)TrustProvider.TaiShin;

            int tempInt;
            if (int.TryParse(View.OriginalPrice, out tempInt))
            {
                entity.Product.OriginalPrice = tempInt;
            }
            if (int.TryParse(View.Price, out tempInt))
            {
                entity.Product.Price = tempInt;
            }
            View.TotalQuantity = View.TotalQuantity == "" ? "9999" : View.TotalQuantity;
            if (int.TryParse(View.TotalQuantity, out tempInt))
            {
                entity.Product.Quantity = tempInt;
            }
            View.PerOrderLimit = View.PerOrderLimit == "" ? "20" : View.PerOrderLimit;
            if (int.TryParse(View.PerOrderLimit, out tempInt))
            {
                entity.Product.OrderLimit = tempInt;
            }
            View.PerBuyerLimit = View.PerBuyerLimit == "" ? "20" : View.PerBuyerLimit;
            if (int.TryParse(View.PerBuyerLimit, out tempInt))
            {
                entity.Product.OrderLimitUser = tempInt;
            }
            entity.Product.IsNoRefund = View.IsNoRefund;
            entity.Product.IsDaysNoRefund = View.IsDaysNoRefund;
            entity.Product.IsExpireNoRefund = View.IsExpireNoRefund;
            entity.Product.IsTaxFree = View.IsTaxFree;
            entity.Product.IsInputTaxFree = View.IsInputTaxFree;
            entity.Product.IsHomeDelivery = View.IsHomeDelivery;
            entity.Product.HomeDeliveryDesc = View.HomeDeliveryDescription;
            entity.Product.HomeDeliveryQuantity = View.HomeDeliveryQuantity;
            entity.Product.IsInStore = View.IsInStore;
            entity.Product.InStoreDesc = View.InStoreDescription;
            entity.Product.DeliveryIslands = !View.NotDeliveryIslands;
            HiDealCouponSmsFormatter smsFormatter = new HiDealCouponSmsFormatter();
            smsFormatter.SmsTitle = View.SmsTitle;
            smsFormatter.SmsPrefix = View.SmsPrefix;
            smsFormatter.SmsInformation = View.SmsInformation;
            entity.Product.IsSmsClose = View.DisableSMS;


            entity.Product.Sms = smsFormatter.ToDbFormat();
            entity.Product.IsShowPriceDiscount = View.IsShowDiscount;
            entity.Product.IsInvoiceCreate = View.IsInvoiceCreate; //開立發票註記
            entity.Product.PayToCompany = View.PayToCompany;
            entity.Product.IsCombo = View.IsCombo;

            entity.Product.BookingSystemType = View.BookingSystemType;
            entity.Product.AdvanceReservationDays = View.AdvanceReservationDays;
            entity.Product.CouponUsers = View.CouponUsers;
            entity.Product.IsReserveLock = View.IsReserveLock;

            #endregion

            #region hi_deal_product_store

            List<ProductBranchStoreSetting> store = new List<ProductBranchStoreSetting>();

            if (View.IsCouponBySpecifics)
            {
                entity.Product.HasBranchStoreQuantityLimit = true;
            }
            else
            {
                entity.Product.HasBranchStoreQuantityLimit = false;
            }

            string[] stores = View.OrderedStoreSettings.Split(';');
            foreach (string s in stores)
            {
                if (string.IsNullOrEmpty(s))
                {
                    break;
                }
                List<string> storeValues = new List<string>(s.Split(','));
                ProductBranchStoreSetting setting = new ProductBranchStoreSetting
                                                        {
                                                            StoreGuid = Guid.Parse(storeValues[0]),
                                                            Sequence = int.Parse(storeValues[1])
                                                        };
                if (storeValues.Count > 2)
                {
                    setting.QuantityLimit = int.Parse(storeValues[2]);
                }
                store.Add(setting);
            }
            entity.BranchStoreSettings = store;

            #endregion

            #region hi_deal_freight  (HiDealFreightType.Income)
            foreach (Freight freight in View.FreightIncome)
            {
                if (freight.Action == "delete")
                {
                    entity.DeleteFreight(HiDealFreightType.Income);
                }
                if (freight.Action == "add")
                {
                    entity.AddFreight(freight.ThresholdAmount, freight.FrieghtValue, HiDealFreightType.Income);
                }
            }
            #endregion

            #region hi_deal_freight (HiDealFreightType.Cost)
            foreach (Freight freight in View.FreightExpense)
            {
                if (freight.Action == "delete")
                {
                    entity.DeleteFreight(HiDealFreightType.Cost);
                }
                if (freight.Action == "add")
                {
                    entity.AddFreight(freight.ThresholdAmount, freight.FrieghtValue, HiDealFreightType.Cost);
                }
            }
            #endregion

            #region hi_deal_product_cost

            bool isProductCostChange = false;  //判斷是否有異動ProductCost

            foreach (ProductCost prodCost in View.ProductCosts)
            {
                if (prodCost.Action == "delete")
                {
                    entity.DeleteProductPurchaseCost();
                    isProductCostChange = true;
                }
                if (prodCost.Action == "add")
                {
                    entity.AddProductPurchaseCost(decimal.Parse(prodCost.Cost), prodCost.Quantity);
                    isProductCostChange = true;
                }
            }

            //憑證成本變更更新Hi_Deal_Coupon.Cost
            if (isProductCostChange)
            {
               HiDealProductManager.HiDealProductCostChangeByProductId(View.Pid);
            }

            #endregion

            #region hi_deal_product_option_category & hi_deal_product_option_item

            string xml = View.OptionalSettings;
            xml = xml.Replace('[', '<');
            xml = xml.Replace(']', '>');

            XmlDocument dom = new XmlDocument();
            dom.LoadXml(xml);
            XmlElement root = dom.DocumentElement;  //<Optionals>
            XmlNodeList catgList = root.GetElementsByTagName("OptCatg");

            int originalCatgCount = entity.Options.Count;
            for (int i = originalCatgCount; i > originalCatgCount - (originalCatgCount - catgList.Count); i--)
            {
                entity.Options.Remove(entity.Options[i - 1]);
            }

            for (int m = 0; m < catgList.Count; m++)
            {
                XmlElement catgElem = (XmlElement)catgList[m];
                string catgName = catgElem.GetAttribute("catgName");

                if (m <= entity.Options.Count - 1)
                {
                    //update
                    entity.Options[m] = new OptionCatg(entity.Options[m].CatgId, m + 1, catgName);
                    entity.Options[m].Items.Clear();
                }
                else
                {
                    entity.Options.Add(catgName);
                }

                XmlNodeList itemElemList = ((XmlElement)catgList[m]).GetElementsByTagName("OptItem");
                for (int n = 0; n < itemElemList.Count; n++)
                {
                    XmlElement itemElem = (XmlElement)itemElemList[n];
                    string strQuantity = itemElem.GetAttribute("quantity");
                    string itemName = itemElem.InnerText;
                    int? quantity;
                    if (string.IsNullOrEmpty(strQuantity))
                    {
                        quantity = null;
                    }
                    else
                    {
                        quantity = int.Parse(strQuantity);
                    }
                    entity.Options[m].Items.Add(new OptionItem(0, n + 1, itemName, quantity, null));
                }
            }

            #endregion

            entity.Save();
            ProductSaved(this, null);
        }

        public void OnNewFreightIncome(object sender, DataEventArgs<FreightArgs> e)
        {
            if (View.FreightIncome.Count == 0)
            {
                View.FreightIncome.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue) });
            }
            else
            {
                //階梯式運費: 新消費金額 > max(消費金額)
                bool IsValid = false;
                int compareIndex = -1;

                for (int i = View.FreightIncome.Count - 1; i >= 0; i--)
                {
                    if (View.FreightIncome[i].Action != "delete")
                    {
                        compareIndex = i;
                        break;
                    }
                }

                if (compareIndex == -1)
                {
                    View.FreightIncome.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue) });
                }
                else
                {
                    if (float.Parse(e.Data.ThresholdAmount) > View.FreightIncome[compareIndex].ThresholdAmount)
                    {
                        IsValid = true;
                    }
                    if (IsValid)
                    {
                        View.FreightIncome.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue), ThresholdAmount = float.Parse(e.Data.ThresholdAmount) });
                    }
                }
            }
            View.RefreshFreightIncome();
        }

        public void OnDeleteFreightIncome(object sender, EventArgs e)
        {
            /*
             * if freight information is not in DB (added temporarily) => remove item from memory
             * if freight information is in DB => mark delete
             */
            for (int i = View.FreightIncome.Count - 1; i >= 0; i--)
            {
                if (View.FreightIncome[i].Action == "add")
                {
                    View.FreightIncome.RemoveAt(i);
                    break;
                }
                if (string.IsNullOrEmpty(View.FreightIncome[i].Action))
                {
                    View.FreightIncome[i].Action = "delete";
                    break;
                }
            }
            View.RefreshFreightIncome();
        }

        public void OnNewFreightExpense(object sender, DataEventArgs<FreightArgs> e)
        {
            if (View.FreightExpense.Count == 0)
            {
                View.FreightExpense.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue) });
            }
            else
            {
                //階梯式運費: 新消費金額 > max(消費金額)
                bool IsValid = false;
                int compareIndex = -1;

                for (int i = View.FreightExpense.Count - 1; i >= 0; i--)
                {
                    if (View.FreightExpense[i].Action != "delete")
                    {
                        compareIndex = i;
                        break;
                    }
                }
                if (compareIndex == -1)
                {
                    View.FreightExpense.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue) });
                }
                else
                {
                    if (float.Parse(e.Data.ThresholdAmount) > View.FreightExpense[compareIndex].ThresholdAmount)
                    {
                        IsValid = true;
                    }
                    if (IsValid)
                    {
                        View.FreightExpense.Add(new Freight { Action = "add", Id = 0, FrieghtValue = float.Parse(e.Data.FreightValue), ThresholdAmount = float.Parse(e.Data.ThresholdAmount) });
                    }
                }
            }
            View.RefreshFreightExpense();
        }

        public void OnDeleteFreightExpense(object sender, EventArgs e)
        {
            /*
             * if freight information is not in DB (added temporarily) => remove item from memory
             * if freight information is in DB => mark delete
             */
            for (int i = View.FreightExpense.Count - 1; i >= 0; i--)
            {
                if (View.FreightExpense[i].Action == "add")
                {
                    View.FreightExpense.RemoveAt(i);
                    break;
                }
                if (string.IsNullOrEmpty(View.FreightExpense[i].Action))
                {
                    View.FreightExpense[i].Action = "delete";
                    break;
                }
            }
            View.RefreshFreightExpense();
        }

        public void OnNewProductPurchaseCost(object sender, DataEventArgs<CostArgs> e)
        {
            ProductCost newCost = new ProductCost { Id = 0, Action = "add", Cost = e.Data.Cost, Quantity = int.Parse(e.Data.Quantity) };

            if (View.ProductCosts.Count > 0)
            {
                int compareIndex = -1;
                for (int i = View.ProductCosts.Count - 1; i >= 0; i--)
                {
                    if (View.ProductCosts[i].Action != "delete")
                    {
                        compareIndex = i;
                        break;
                    }
                }
                if (compareIndex != -1)
                {
                    newCost.CumulativeQuantity = View.ProductCosts[compareIndex].CumulativeQuantity + int.Parse(e.Data.Quantity);
                }
                else
                {
                    newCost.CumulativeQuantity = int.Parse(e.Data.Quantity);
                }
            }
            else
            {
                newCost.CumulativeQuantity = int.Parse(e.Data.Quantity);
            }
            View.ProductCosts.Add(newCost);
            View.RefreshProductCost();
        }

        public void OnDeleteProductPurchaseCost(object sender, EventArgs e)
        {
            /*
             * if cost information is not in DB (added temporarily) => remove item from memory
             * if cost information is in DB => mark as delete
             */
            for (int i = View.ProductCosts.Count - 1; i >= 0; i--)
            {
                if (View.ProductCosts[i].Action == "add")
                {
                    View.ProductCosts.RemoveAt(i);
                    break;
                }
                if (string.IsNullOrEmpty(View.ProductCosts[i].Action))
                {
                    View.ProductCosts[i].Action = "delete";
                    break;
                }
            }
            View.RefreshProductCost();
        }


        private void UpdateHiDealCouponCostByProductId(int productId, int quantity)
        {
            //判斷是否已有憑證產出，表示Cost可能為舊資料，需要做更新動作
        }


        private Collection<Freight> GetFreightIncome()
        {
            HiDealFreightCollection dbFreightCollection = _hp.HiDealFreightGetListByProductId(View.Pid, HiDealFreightType.Income);
            Collection<Freight> uiFreightCollection = new Collection<Freight>();
            foreach (HiDealFreight freight in dbFreightCollection)
            {
                uiFreightCollection.Add(new Freight { Id = freight.Id, FrieghtValue = (float)freight.FreightAmount, ThresholdAmount = (float)freight.StartAmount, Action = "" });
            }
            return uiFreightCollection;
        }

        private Collection<Freight> GetFreightExpense()
        {
            HiDealFreightCollection dbFreightCollection = _hp.HiDealFreightGetListByProductId(View.Pid, HiDealFreightType.Cost);
            Collection<Freight> uiFreightCollection = new Collection<Freight>();
            foreach (HiDealFreight freight in dbFreightCollection)
            {
                uiFreightCollection.Add(new Freight { Id = freight.Id, FrieghtValue = (float)freight.FreightAmount, ThresholdAmount = (float)freight.StartAmount, Action = "" });
            }
            return uiFreightCollection;
        }

        private Collection<ProductCost> GetProductCost()
        {
            HiDealProductCostCollection dbProdCost = _hp.HiDealProductCostCollectionGetByProductId(View.Pid);
            Collection<ProductCost> uiProdCost = new Collection<ProductCost>();
            foreach (HiDealProductCost cost in dbProdCost)
            {
                uiProdCost.Add(new ProductCost { Id = cost.Id, Cost = cost.Cost.ToString(), Quantity = cost.Quantity, CumulativeQuantity = cost.CumulativeQuantitiy, Action = "" });
            }
            return uiProdCost;
        }

    }
}

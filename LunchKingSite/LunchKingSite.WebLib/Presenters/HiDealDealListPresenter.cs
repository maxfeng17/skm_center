﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealDealListPresenter :Presenter<IHiDealDealListView>
    {
        protected IHiDealProvider hp;

        public HiDealDealListPresenter(IHiDealProvider hideal)
        {
            hp = hideal;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            return true;
        }

        private void OnSearchClicked(object sender, DataEventArgs<HiDealDealListViewSearchRequest> e)
        {
            HiDealDealListViewSearchRequest data = e.Data;
            var sList = new List<string>(); 
            //判斷搜尋方式的條件
            if(!string.IsNullOrWhiteSpace(data.FilterString))
            {
                var filterWhere = string.Empty;
                switch (data.SearchType)
                {
                    case HiDealDealListViewSearchType.SellerName:
                        filterWhere = ViewHiDealSellerProduct.Columns.SellerName + " like " + "%" + data.FilterString + "%";
                        break;
                    case HiDealDealListViewSearchType.DealName:
                        filterWhere = ViewHiDealSellerProduct.Columns.DealName + " like " + "%" + data.FilterString + "%";
                        break;
                    case HiDealDealListViewSearchType.DealId:
                        filterWhere = ViewHiDealSellerProduct.Columns.DealId + " = " + data.FilterString;
                        break;
                    case HiDealDealListViewSearchType.ProductId:
                        filterWhere = ViewHiDealSellerProduct.Columns.ProductId + " = " + data.FilterString;
                        break;
                }
                sList.Add(filterWhere);
            }
            //判斷起始日
            if((data.StartTimeBegin != null)&&(data.StartTimeEnd!=null))
            {
                sList.Add(ViewHiDealSellerProduct.Columns.DealStartTime + " >= " + data.StartTimeBegin.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                sList.Add(ViewHiDealSellerProduct.Columns.DealStartTime + " < " + data.StartTimeEnd.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            //判斷截止日
            if ((data.EndTimeBegin != null) && (data.EndTimeEnd != null))
            {
                sList.Add(ViewHiDealSellerProduct.Columns.DealEndTime + " >= " + data.EndTimeBegin.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                sList.Add(ViewHiDealSellerProduct.Columns.DealEndTime + " < " + data.EndTimeEnd.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            //判斷篩選條件
            switch (data.DealWorkingType)
            {
                case HiDealDealListViewDealWorkingType.Before:
                    sList.Add(ViewHiDealSellerProduct.Columns.DealStartTime + " >= " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case HiDealDealListViewDealWorkingType.Working:
                    sList.Add(ViewHiDealSellerProduct.Columns.DealStartTime + " <= " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    sList.Add(ViewHiDealSellerProduct.Columns.DealEndTime + " > " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
                case HiDealDealListViewDealWorkingType.End:
                    sList.Add(ViewHiDealSellerProduct.Columns.DealEndTime + " < " + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    break;
            }

            //檢查有無條件成立
            if(sList.Count == 0)
            {
                //沒有搜尋條件，顯示錯誤。
                View.ShowMessage("請至少設定一個條件。");
                return;
            }
            //查詢資料
            var vhddsc = hp.ViewHiDealSellerProductGetList(data.PageNum, data.PageSize, sList.ToArray(),
                                                      ViewHiDealSellerProduct.Columns.DealStartTime + " desc ");
            var dataCount = hp.ViewHiDealSellerProductDialogDataGetCount(sList.ToArray());
            //顯示資料
            View.ShowData(vhddsc, dataCount);
        }
    }
}

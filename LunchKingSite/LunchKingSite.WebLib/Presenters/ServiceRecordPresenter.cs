﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class ServiceRecordPresenter : Presenter<IServiceRecordView>
    {
        #region provider
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        #endregion

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetServiceRecord(1);
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }

        #region event
        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = mp.ServiceMessageGetCountForFilter(GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            GetServiceRecord(e.Data);
        }
        #endregion

        #region method
        protected void GetServiceRecord(int pageNumber)
        {
            ServiceMessageCollection message = mp.ServiceMessageGetListForFilter(pageNumber, View.PageSize, ServiceMessage.Columns.CreateTime + " desc", GetFilter());
            Dictionary<KeyValuePair<ServiceMessage, Guid>, List<ServiceLog>> dataList = new Dictionary<KeyValuePair<ServiceMessage, Guid>, List<ServiceLog>>();
            foreach (ServiceMessage msg in message)
            {
                Order order = new Order();
                if (!string.IsNullOrEmpty(msg.OrderId))
                    order = op.OrderGet(Order.Columns.OrderId, msg.OrderId);


                dataList.Add(
                    new KeyValuePair<ServiceMessage, Guid>(msg, (order.IsLoaded && order.UserId == View.UserId) ? order.Guid : Guid.Empty),
                    mp.ServiceLogGetList(msg.Id.ToString()).Where(x => x.SendType == Phrase.MailSent).ToList());
            }
            View.SetServiceRecord(dataList);
        }
        #endregion

        #region private method
        private string[] GetFilter()
        {
            List<string> list = new List<string>();
            list.Add(ServiceMessage.Columns.UserId + "=" + View.UserId);
            list.Add(ServiceMessage.Columns.CreateTime + " > " + DateTime.Now.AddMonths(View.RecordPeriod).ToString("yyyy/MM/dd 00:00:00"));
            list.Add(ServiceMessage.Columns.MessageType + "=" + "來信");
            if (DateTime.Now <= config.NewPiinlifeDate)
            {
                list.Add(ServiceMessage.Columns.Type + "=" + (int)ServiceMessageType.Ppon);
            }
            return list.ToArray();
        }
        #endregion
    }
}

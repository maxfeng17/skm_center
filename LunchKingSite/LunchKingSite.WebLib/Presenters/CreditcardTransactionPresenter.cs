﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Presenters
{
    public class CreditcardTransactionPresenter : Presenter<ICreditcardTransactionView>
    {
        #region members

        protected IOrderProvider op = null;
        protected IMemberProvider mp = null;

        #endregion members

        public CreditcardTransactionPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnGridUpdated; //觸發查詢事件
            View.SearchPCashClicked += OnSearchPCash; //觸發查詢事件
            View.RefundPCashClicked += OnReFund; //觸發查詢事件
            View.GetTransCount += OnGetTransCount;
            View.PageChanged += OnPageChanged;
            View.CancelPcash += OnCancelPcash;
            View.ChargePcash += OnChargePcash;
            View.ChargePcashBatch += OnChargePcashBatch;
            return true;
        }

        protected void OnSearchPCash(object sender, EventArgs e)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetNullOrderIdPCash();
            View.SetPCashGridView(ptc);
        }

        protected void OnReFund(object sender, EventArgs e)
        {
            PaymentTransactionCollection ptc = op.PaymentTransactionGetNullOrderIdPCash();
            DataTable dt = new DataTable();
            dt.Columns.Add("TransId");
            dt.Columns.Add("Message");
            foreach (PaymentTransaction pt in ptc)
            {
                DataRow dr = dt.NewRow();
                dr["TransId"] = pt.TransId;
                try
                {
                    dr["Message"] = PCashWorker.DeCheckOut(pt.TransId, pt.AuthCode, pt.Amount);
                }
                catch (Exception ex)
                {
                    dr["Message"] = ex.Message;
                }
                dt.Rows.Add(dr);

                PayTransResponseType responseType = PayTransResponseType.OK;
                PayTransPhase transPhase = PayTransPhase.Created;
                if (dr["Message"].ToString() == "0000")
                {
                    transPhase = PayTransPhase.Successful;
                }
                else
                {
                    transPhase = PayTransPhase.Failed;
                    responseType = PayTransResponseType.GenericError;
                }
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), transPhase);

                PaymentFacade.NewTransaction(pt.TransId, Guid.Empty, LunchKingSite.Core.PaymentType.PCash, pt.Amount, pt.AuthCode,
                PayTransType.Refund, DateTime.Now, View.CurrentUser, dr["Message"].ToString(), status, responseType);
            }

            View.SetRefundPcash(dt);
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void OnGetTransCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.ViewPaymentTransactionLeftjoinOrderGetCount(GetFilter());
        }

        protected string[] GetFilter()
        {
            System.Collections.ArrayList filterList = new System.Collections.ArrayList();

            if (!string.IsNullOrEmpty(View.OrderId))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.OrderId + " = " + View.OrderId);
            }

            if (!string.IsNullOrEmpty(View.TransId))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.TransId + " = " + View.TransId);
            }

            if (!string.IsNullOrEmpty(View.CreateId))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.CreateId + " = " + View.CreateId);
            }

            if (!string.IsNullOrEmpty(View.TransTimeBegin) && !string.IsNullOrEmpty(View.TransTimeEnd))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.TransTime + " between " + View.TransTimeBegin + " and " + View.TransTimeEnd);
            }

            if (!string.IsNullOrEmpty(View.Msg))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.Message + " like %" + View.Msg + "%");
            }

            if (!string.IsNullOrEmpty(View.AuthCode))
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.AuthCode + " = " + View.AuthCode);
            }

            if (View.Classification != null)
            {
                filterList.Add(ViewPaymentTransactionLeftjoinOrder.Columns.OrderClassification + " = " + View.Classification);
            }

            if (filterList.Count > 0)
            {
                string[] rtn = new string[filterList.Count];
                for (int i = 0; i < filterList.Count; i++)
                {
                    rtn[i] = (string)filterList[i];
                }
                return rtn;
            }

            return null;
        }

        protected void OnCancelPcash(object sender, DataEventArgs<string[]> e)
        {
            string transId = e.Data[0];
            string authCode = e.Data[1];
            decimal amount = decimal.Parse(e.Data[2]);
            string result = PCashWorker.DeCheckOut(transId, authCode, amount);

            PaymentTransaction pt = op.PaymentTransactionGet(transId, PaymentType.PCash, PayTransType.Authorization);
            if (pt != null)
            {
                PayTransResponseType responseType = PayTransResponseType.OK;
                PayTransPhase transPhase = PayTransPhase.Created;
                if (result == "0000")
                {
                    transPhase = PayTransPhase.Successful;
                }
                else
                {
                    transPhase = PayTransPhase.Failed;
                    responseType = PayTransResponseType.GenericError;
                }
                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);
                int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), transPhase);
                PaymentFacade.NewTransaction(transId, pt.OrderGuid.Value, PaymentType.PCash, amount, authCode,
                                             PayTransType.Refund, DateTime.Now, View.CurrentUser, result + "|退溢收款", status, responseType);
                if (result == "0000")
                {
                    result = "成功";
                }

                View.SetPcashResult(result);
            }
        }

        protected void OnChargePcash(object sender, DataEventArgs<string[]> e)
        {
            string transId = e.Data[0];
            decimal amount = decimal.Parse(e.Data[1]);
            string authCode = string.Empty;

            PaymentTransaction pt = op.PaymentTransactionGet(transId, PaymentType.PCash, PayTransType.Authorization);
            MemberLink ml = mp.MemberLinkGet(MemberFacade.GetUniqueId(pt.CreateId), SingleSignOnSource.PayEasy);

            PayTransResponseType responseType = (PayTransResponseType)pt.Result;
            PayTransPhase transPhase = Helper.GetPaymentTransactionPhase(pt.Status);
            DepartmentTypes department = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);

            string pezResult = PCashWorker.CheckOut(ml.ExternalUserId, transId, amount, department, out authCode);

            if (pezResult == "0000")
            {
                transPhase = PayTransPhase.Successful;
                responseType = PayTransResponseType.OK;
            }
            else
            {
                transPhase = PayTransPhase.Failed;
                responseType = PayTransResponseType.GenericError;
            }
            int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, department), transPhase);

            if (pt.OrderGuid == null)
            {
                PaymentFacade.NewTransaction(transId, Guid.Empty, LunchKingSite.Core.PaymentType.PCash, amount, authCode,
                PayTransType.Charging, DateTime.Now, View.CurrentUser, pezResult, status, responseType);
            }
            else
            {
                PaymentFacade.UpdateTransaction(transId, (Guid)pt.OrderGuid, PaymentType.PCash, amount, authCode, PayTransType.Authorization,
                                                DateTime.Now, pezResult, status, (int)responseType);
            }

            if (pezResult == "0000")
            {
                pezResult = "成功";
            }
            View.SetPcashResultC(pezResult);
        }

        protected void OnChargePcashBatch(object sender, DataEventArgs<string> e)
        {
            string result = string.Empty;
            string authCode = string.Empty;
            IList<string> transIds = e.Data.Split(',');
            foreach (var transId in transIds)
            {
                PaymentTransaction pt = op.PaymentTransactionGet(transId, PaymentType.PCash, PayTransType.Authorization);
                if (pt != null)
                {
                    MemberLink ml = mp.MemberLinkGet(MemberFacade.GetUniqueId(pt.CreateId), SingleSignOnSource.PayEasy);

                    PayTransResponseType responseType = (PayTransResponseType)pt.Result;
                    PayTransPhase transPhase = Helper.GetPaymentTransactionPhase(pt.Status);
                    DepartmentTypes department = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);

                    string pezResult = PCashWorker.CheckOut(ml.ExternalUserId, transId, pt.Amount, department, out authCode);

                    if (pezResult == "0000")
                    {
                        transPhase = PayTransPhase.Successful;
                        responseType = PayTransResponseType.OK;
                    }
                    else
                    {
                        transPhase = PayTransPhase.Failed;
                        responseType = PayTransResponseType.GenericError;
                    }

                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, department), transPhase);
                    PaymentFacade.UpdateTransaction(transId, (Guid)pt.OrderGuid, PaymentType.PCash, pt.Amount, authCode, PayTransType.Authorization,
                                                        DateTime.Now, pezResult, status, (int)responseType);

                    pezResult = transId + " " + pezResult + ";";
                    result += pezResult;
                }
                else
                {
                    result += transId + "查無此交易編號;";
                }
            }
            View.BatchResult = result;
        }

        #region Event

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void LoadData(int pageNumber)
        {
            View.SetGridTrans(op.ViewPaymentTransactionLeftjoinOrderGetList(pageNumber, View.PageSize,
                                  ViewPaymentTransactionLeftjoinOrder.Columns.PaymentTransactionId + " desc", GetFilter()));
        }

        #endregion Event
    }
}
﻿using System.Threading;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class PicSetupPresenter : Presenter<IPicSetupView>
    {
        private IPponProvider pp;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public PicSetupPresenter(IPponProvider pp)
        {
            this.pp = pp;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.BusinessHourGuid != Guid.Empty)
            {
                GetCouponEventContent();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SavePic += OnSavePic;
            View.GetCouponEventContent += OnGetCouponEventContent;
            return true;
        }

        public void OnGetCouponEventContent(object sender, EventArgs e)
        {
            GetCouponEventContent();
        }

        public void OnSavePic(object sender, EventArgs e)
        {
            string image_path = string.Empty;
            string special_image_path = string.Empty;
            string travel_edm_special_image_path = string.Empty;
            string message = string.Empty;
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);
            CouponEventContent coupon_event_content = pp.CouponEventContentGet(CouponEventContent.Columns.BusinessHourGuid, View.BusinessHourGuid);
            if (deal == null || deal.BusinessHourGuid == Guid.Empty)
            {
                message = "查無檔次";
            }
            else
            {
                if (!string.IsNullOrEmpty(View.ImagePath))
                {
                    List<int> sorted =
                        (from x in
                             View.ImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    string[] originalOrder = Helper.GetRawPathsFromRawData(coupon_event_content.ImagePath);
                    List<string> newOrder = new List<string>();
                    int[] musthave = { 0, 1, 2 };
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        if (sorted.Contains(i))
                        {
                            newOrder.Add(originalOrder[i]);
                        }

                        if (musthave.Contains(i) && !sorted.Contains(i))
                        {
                            newOrder.Add(string.Empty);
                        }
                    }

                    //if first pic has been deleted and other big pic exists, move thr first big pic to first pic

                    if (!sorted.Any(x => x == 0))
                    {
                        sorted.Add(0);
                    }

                    if (!sorted.Any(x => x == 1))
                    {
                        sorted.Add(1);
                    }

                    if (!sorted.Any(x => x == 2))
                    {
                        sorted.Add(2);
                    }

                    string bid_file = View.BusinessHourGuid.ToString().Replace("-", "EDM");
                    if (string.IsNullOrEmpty(newOrder.DefaultIfEmpty(string.Empty).First()) &&
                        !string.IsNullOrEmpty(newOrder.Skip(3).DefaultIfEmpty(string.Empty).First()))
                    {
                        var baseFile = newOrder[3].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();

                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], deal.SellerId, (newOrder.Any(x => x.Contains(bid_file)) ? tick : bid_file) + "." + baseFile[1].Split('.')[1]);
                        newOrder[0] = baseFile[0] + "," + (newOrder.Any(x => x.Contains(bid_file)) ? tick : bid_file) + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(newOrder[3]);
                        newOrder.RemoveAt(3);
                        sorted.RemoveAll(x => x == 3 || x == 0);
                        sorted.Insert(0, 0);
                    }

                    var s = sorted.Where(x => x != 1 && x != 2).Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    var n = newOrder.Where((x, index) => index != 1 && index != 2).ToList();

                    for (int i = 0; i < n.Count(); i++)
                    {
                        s[i].Path = n[i];
                    }
                    List<string> test = new List<string>();
                    test.Add(s.OrderBy(x => x.SortIndex).First().Path);
                    if (newOrder.Count > 1)
                    {
                        test.Add(newOrder[1]);
                    }

                    if (newOrder.Count > 2)
                    {
                        test.Add(newOrder[2]);
                    }

                    test.AddRange(s.OrderBy(x => x.SortIndex).Skip(1).Select(x => x.Path));

                    if (test.Any(x => x.Contains(bid_file)) && test.DefaultIfEmpty(string.Empty).First() != bid_file)
                    {
                        int index = test.FindIndex(x => x.Contains(bid_file));
                        string original = test[index];
                        var baseFile = test[index].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();
                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], deal.SellerId, tick + "." + baseFile[1].Split('.')[1]);
                        test[index] = baseFile[0] + "," + tick + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(original);
                    }

                    if (!test.Any(x => x.Contains(bid_file)) && !string.IsNullOrEmpty(test.DefaultIfEmpty(string.Empty).First()))
                    {
                        string original = test[0];
                        var baseFile = test[0].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();
                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], deal.SellerId, bid_file + "." + baseFile[1].Split('.')[1]);
                        test[0] = baseFile[0] + "," + bid_file + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(original);
                    }

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (View.ImagePath.CompareTo(newRawData) != 0)
                    {
                        image_path = newRawData;
                    }

                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(coupon_event_content.ImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(coupon_event_content.ImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    image_path = null;
                }


                #region Special Image 的刪除處理

                //檢查前端回傳的圖片紀錄是否有資料
                if (!string.IsNullOrEmpty(View.SpecialImagePath))
                {
                    //前端回傳的資料
                    List<int> sorted =
                        (from x in
                             View.SpecialImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    //原始資料
                    string[] originalOrder = Helper.GetRawPathsFromRawData(coupon_event_content.SpecialImagePath);
                    List<string> newOrder = new List<string>();
                    //比較原始圖檔是否要保留
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        //使用者回傳有第i筆資料
                        if (sorted.Contains(i))
                        {
                            //將第i筆記錄寫入暫存，之後要保留
                            newOrder.Add(originalOrder[i]);
                        }
                    }

                    //把要的資料排序排好
                    var s = sorted.Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    for (int i = 0; i < newOrder.Count(); i++)
                    {
                        s[i].Path = newOrder[i];
                    }
                    List<string> test = new List<string>();
                    test.AddRange(s.OrderBy(x => x.SortIndex).Select(x => x.Path));

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (View.SpecialImagePath.CompareTo(newRawData) != 0)
                    {
                        special_image_path = newRawData;
                    }
                    //將原始紀錄中不存在於新整理好的圖片資料中的圖檔刪除
                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(coupon_event_content.SpecialImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(coupon_event_content.SpecialImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    special_image_path = null;
                }

                #endregion

                #region TraveleDMSpecialPic 的刪除處理

                //檢查前端回傳的圖片紀錄是否有資料
                if (!string.IsNullOrEmpty(View.TravelEdmSpecialImagePath))
                {
                    //前端回傳的資料
                    List<int> sorted =
                        (from x in
                             View.TravelEdmSpecialImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    //原始資料
                    string[] originalOrder = Helper.GetRawPathsFromRawData(coupon_event_content.TravelEdmSpecialImagePath);
                    List<string> newOrder = new List<string>();
                    //比較原始圖檔是否要保留
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        //使用者回傳有第i筆資料
                        if (sorted.Contains(i))
                        {
                            //將第i筆記錄寫入暫存，之後要保留
                            newOrder.Add(originalOrder[i]);
                        }
                    }

                    //把要的資料排序排好
                    var s = sorted.Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    for (int i = 0; i < newOrder.Count(); i++)
                    {
                        s[i].Path = newOrder[i];
                    }
                    List<string> test = new List<string>();
                    test.AddRange(s.OrderBy(x => x.SortIndex).Select(x => x.Path));

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (View.TravelEdmSpecialImagePath.CompareTo(newRawData) != 0)
                    {
                        travel_edm_special_image_path = newRawData;
                    }
                    //將原始紀錄中不存在於新整理好的圖片資料中的圖檔刪除
                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(coupon_event_content.TravelEdmSpecialImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(coupon_event_content.TravelEdmSpecialImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    travel_edm_special_image_path = null;
                }

                #endregion TraveleDMSpecialPic


                #region 上傳圖片

                if (View.PostedFiles.Count > 0)
                {
                    foreach (HttpPostedFile postedFile in View.PostedFiles)
                    {
                        UploadImages(ref image_path, ref special_image_path, ref travel_edm_special_image_path, deal, postedFile);
                        Thread.Sleep(config.PicSetupUploadImageSleepTime);
                    }
                }

                #endregion 上傳圖片

                pp.CouponEventContentUpdatePic(image_path, special_image_path, travel_edm_special_image_path, View.BusinessHourGuid);
                message = "更新成功";
            }
            coupon_event_content = pp.CouponEventContentGet(CouponEventContent.Columns.BusinessHourGuid, View.BusinessHourGuid);
            View.GetCouponEventContentInfo(coupon_event_content, message);
        }

        private void UploadImages(ref string imagePath, ref string specialImagePath, ref string travelEdmSpecialImagPath, IViewPponDeal deal, HttpPostedFile postedFile)
        {
            // we have uploaded file
            //依據圖片用途進行不同的處理
            switch (View.SelectImageUploadType)
            {
                case PponSetupImageUploadType.ByORder:
                case PponSetupImageUploadType.BigPic:
                    //好康大圖(依順序上傳或直接上傳輪播大圖)
                    #region 上傳圖片並取得新路徑
                    List<string> originalImgOrder = Helper.GetRawPathsFromRawData(imagePath).ToList<string>();
                    bool isUploadByOrder = View.SelectImageUploadType == PponSetupImageUploadType.ByORder;
                    string[] imagePathList = imagePath.Split(',');
                    HttpPostedFileBase hpfb = new HttpPostedFileWrapper(postedFile) as HttpPostedFileBase;
                    imagePath = ImageUtility.UploadPponImageAndGetNewPath(hpfb, deal.SellerId, View.BusinessHourGuid.ToString(), imagePath, isUploadByOrder, View.IsPrintWatermark, false);
                    #endregion

                    bool isFirstPicEmpty = originalImgOrder.Count.Equals(0) || string.IsNullOrEmpty(originalImgOrder[0]);
                    // 當上傳第一張輪播大圖(且side deal小圖(1)沒有圖)時，自動上傳Side Deal小圖
                    if (isFirstPicEmpty && !imagePathList.Contains("1"))
                    {
                        imagePath = ImageUtility.UploadPponImageAndGetNewPath(hpfb, deal.SellerId, View.BusinessHourGuid.ToString(), imagePath, true, View.IsPrintWatermark, true);
                    }
                    break;
                case PponSetupImageUploadType.APP:
                    //APP用的特殊規格圖片
                    specialImagePath = UploadSpecialImageFile(postedFile, deal.SellerId, specialImagePath);
                    break;
                case PponSetupImageUploadType.TravelEdmSpecialBigPic:
                    travelEdmSpecialImagPath = UploadTravelEDMSpecialPicFile(postedFile, deal.SellerId, travelEdmSpecialImagPath);
                    break;
                default:
                    //圖片用途不明，不處理上傳
                    break;
            }
        }

        private void GetCouponEventContent()
        {
            CouponEventContent coupon_event_content = pp.CouponEventContentGet(CouponEventContent.Columns.BusinessHourGuid, View.BusinessHourGuid);
            View.GetCouponEventContentInfo(coupon_event_content, (coupon_event_content == null || coupon_event_content.BusinessHourGuid == Guid.Empty) ? "查無檔次" : string.Empty);
        }

        private void RemovePhotoFromDisk(string rawPath)
        {
            if (string.IsNullOrEmpty(rawPath) || rawPath.IndexOf(',') < 0)
            {
                return;
            }

            string[] filePath = rawPath.Split(',');
            ImageUtility.DeleteFile(UploadFileType.PponEvent, filePath[0], filePath[1]);
        }
        
        private string UploadSpecialImageFile(HttpPostedFile uploadedFile, string seller_id, string special_image_path)
        {
            if (uploadedFile != null)
            {
                List<string> originalOrder =
                    Helper.GetRawPathsFromRawData(special_image_path).ToList<string>();
                string fileName = "SP" + DateTime.Now.Ticks.ToString();
                string fileNameWithExtension = fileName + "." +
                                               Helper.GetExtensionByContentType(uploadedFile.ContentType);
                ImageUtility.UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, seller_id, fileName,
                                        View.IsPrintWatermark);


                special_image_path = Helper.AppendRawDataWithRawPath(special_image_path,
                                                                               ImageFacade.GenerateMediaPath(
                                                                                   seller_id,
                                                                                   fileNameWithExtension));

            }
            return special_image_path;
        }

        private string UploadTravelEDMSpecialPicFile(HttpPostedFile uploadedFile, string seller_id, string travel_edm_special_image_path)
        {
            if (uploadedFile != null)
            {
                string fileName = "TravelEDMSpecial" + View.BusinessHourGuid.ToString().Replace("-", "EDM");

                string fileNameWithExtension = fileName + "." +
                                               Helper.GetExtensionByContentType(uploadedFile.ContentType);

                ImageUtility.UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, seller_id, fileName,
                                        View.IsPrintWatermark);


                travel_edm_special_image_path = Helper.AppendRawDataWithRawPath(string.Empty,
                                                                               ImageFacade.GenerateMediaPath(
                                                                                  seller_id,
                                                                                   fileNameWithExtension));
            }
            return travel_edm_special_image_path;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealDefaultPresenter : Presenter<IHiDealDefaultView>
    {
        IHiDealProvider hp;

        public HiDealDefaultPresenter()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.AddSubscript += OnSetSubscript;
            if (View.PageMode == HiDealDefaultViewPageMode.General)
            {
                GetData(View.CityId, View.CategoryId, View.SpecialId);
            }
            else
            {
                GetPreviewData(View.CityId, View.DealGuid);
            }
            return true;
        }

        private void GetData(int city, string category, int? specialId)
        {
            #region GetCategory
            DataTable dt = hp.ViewHiDealGetTodayCategorys(city);
            View.SetCategory(dt);
            Dictionary<HiDealSpecialDiscountType, int> specials = hp.ViewHiDealGetTodaySpecials(city);
            View.SetSpecials(specials);
            #endregion

            #region GetCitys
            View.SetCitys(SystemCodeManager.GetSystemCodeListByGroup("HiDealRegion"));
            #endregion

            #region GetData
            ViewHiDealCollection Mdeals = hp.ViewHiDealGetTodayDeals(city, category, specialId, true /*isMain*/ , ViewHiDeal.Columns.CitySeq);
            ViewHiDealCollection Ldeals = hp.ViewHiDealGetTodayDeals(city, category, specialId, false, ViewHiDeal.Columns.CitySeq);
            View.SetContent(Mdeals, Ldeals);
            #endregion
        }

        private void GetPreviewData(int city, Guid dealGuid)
        {
            #region GetCategory
            DataTable dt = hp.ViewHiDealGetTodayCategorys(city);
            View.SetCategory(dt);
            Dictionary<HiDealSpecialDiscountType, int> specials = hp.ViewHiDealGetTodaySpecials(city);
            View.SetSpecials(specials);
            #endregion

            #region GetCitys
            View.SetCitys(SystemCodeManager.GetSystemCodeListByGroup("HiDealRegion"));
            #endregion

            #region GetData
            var mdeals = hp.ViewHiDealGetList(1, 1, null, ViewHiDeal.Columns.DealGuid + "=" + dealGuid);
            var ldeals = new ViewHiDealCollection();
            View.SetContent(mdeals, ldeals);
            #endregion
        }

        protected void OnSetSubscript(object sender, DataEventArgs<string[]> e)
        {
            HiDealSubscription subscript = HiDealMailFacade.HiDealSubscribeCheckSet(e.Data[1], Convert.ToInt32(e.Data[0]), 0, true);
            string cpa = View.ReferrerSourceId;
            if (!string.IsNullOrWhiteSpace(cpa))
            {
                CpaUtility.RecordSubscriptionByReferrer(cpa, subscript);
            }

            View.ShowAlert("訂閱成功。");
        }
    }
}

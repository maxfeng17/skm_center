﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Security;

namespace LunchKingSite.WebLib.Presenters
{
    public class MemberRoleManagementPresenter : Presenter<IMemberRoleManagementView>
    {
        private IHumanProvider humanProv;
        private IMemberProvider memProv;
        private ISystemProvider sysProv;

        public MemberRoleManagementPresenter(IHumanProvider hp, IMemberProvider mp, ISystemProvider sysp)
        {
            humanProv = hp;
            memProv = mp;
            sysProv = sysp;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.MemberSearchClicked += OnMemberSearchClicked;
            View.OnGetMemberCount += OnGetMemberCount;
            View.GetUserInRolesData += OnGetUserInRolesData;
            View.MemberLeftSearch += OnMemberLeftSearch;
            View.MemberRightSave += OnMemberRightSave;
            View.GetMemberPrivilege += OnGetMemberPrivilege;
            View.MemberPrivilegeRightSave += OnMemberPrivilegeRightSave;

            View.MemberPageChanged += OnMemberPageChanged;
            View.RoleSearchClicked += OnRoleSearchClicked;
            View.GetMemberRolesData += OnGetMemberRolesData;
            View.DeleteMemberRoles += OnDeleteMemberRoles;
            View.AddMemberRoles += OnAddMemberRoles;
            View.RoleAddClicked += OnRoleAddClicked;
            View.RoleNameEdit += OnRoleNameEdit;
            View.OrgLeftSearch += OnOrgLeftSearch;
            View.OrgRightSave += OnOrgRightSave;
            View.GetOrgPrivilege += OnGetOrgPrivilege;
            View.OrgPrivilegeRightSave += OnOrgPrivilegeRightSave;

            View.FuncSearchClicked += OnFuncSearchClicked;
            View.OnGetFuncCount += OnGetFuncCount;
            View.FuncPageChanged += OnFuncPageChanged;
            View.GetPrivilegeData += OnGetPrivilegeData;
            View.SysOrgPrivilegeLeftSearch += OnSysOrgPrivilegeLeftSearch;
            View.SysOrgPrivilegeRightSave += OnSysOrgPrivilegeRightSave;
            View.SysPrivilegeLeftSearch += OnSysPrivilegeLeftSearch;
            View.SysPrivilegeRightSave += OnSysPrivilegeRightSave;
            return true;
        }

        #region event

        /// <summary>
        /// 會員-查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnMemberSearchClicked(object sender, EventArgs e)
        {
            View.GetMemberList(MemberGetList(1));
        }

        protected void OnGetMemberCount(object sender, DataEventArgs<int> e)
        {
            e.Data = memProv.MemberGetCount(Member.Columns.UserName + " like " + View.MemberEmail.Replace("*", "%"));
        }

        protected void OnMemberPageChanged(object sender, DataEventArgs<int> e)
        {
            View.GetMemberList(MemberGetList(e.Data));
        }

        /// <summary>
        /// 會員-群組列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetUserInRolesData(object sender, DataEventArgs<string> e)
        {
            string[] userRoles = Roles.GetRolesForUser(e.Data);
            List<Organization> orgList = new List<Organization>();
            foreach (string role in userRoles)
            {
                Organization org = humanProv.GetOrganization(role);
                if (org.IsLoaded)
                {
                    orgList.Add(org);
                }
            }
            View.SetUserInRolesData(OrganizationGetList(orgList).ToDictionary(x => x.Key.Name, x => x.Value), e.Data);
        }

        /// <summary>
        /// 會員-新增群組查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnMemberLeftSearch(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            View.SetMemberLeftSearchResult(OrganizationFilterExistGetList(e.Data.Key, e.Data.Value));
        }

        /// <summary>
        /// 會員-新增群組儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnMemberRightSave(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            // 移除該成員所屬的所有群組
            List<string> roleList = new List<string>();
            foreach (string role in Roles.GetRolesForUser(e.Data.Key))
            {
                if (!role.Equals(MemberRoles.RegisteredUser.ToString(), StringComparison.OrdinalIgnoreCase)) // 不刪除註冊會員角色
                {
                    roleList.Add(role);
                }
            }

            if (roleList.Count > 0)
            {
                Roles.RemoveUserFromRoles(e.Data.Key, roleList.ToArray());
            }

            // 根據群組清單逐筆加入
            if (e.Data.Value.Count > 0)
            {
                Roles.AddUserToRoles(e.Data.Key, e.Data.Value.ToArray());
            }
            CommonFacade.AddAudit(e.Data.Key, AuditType.Role, string.Join(",", e.Data.Value.ToArray()), View.LoginUser, true);

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetMemberList(MemberGetList(1));
        }

        /// <summary>
        /// 會員-新增權限查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetMemberPrivilege(object sender, DataEventArgs<string> e)
        {
            List<SystemFunction> privileges = MemberFacade.MemberPrivilegeGetByEmail(e.Data);
            View.SetMemberPrivilegeData(privileges.ToDictionary(x => x.Id, x => x.ParentFunc + "-" + x.DisplayName + "(" + x.TypeDesc + ")").Distinct().OrderBy(x => x.Value), e.Data, SystemFunctionGetListFilterExist(privileges.Select(x => x.Id).Distinct().ToList()));
        }

        /// <summary>
        /// 會員-新增權限儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnMemberPrivilegeRightSave(object sender, DataEventArgs<KeyValuePair<string, Dictionary<int, string>>> e)
        {
            foreach (KeyValuePair<int, string> item in e.Data.Value)
            {
                // 新增成員權限資料
                MemberFacade.InsertPrivilegeForUser(item.Key, View.LoginUser, e.Data.Key);
            }

            // 移除成員權限資料
            PrivilegeCollection userPris = humanProv.GetPrivilegeListByEmail(e.Data.Key);
            foreach (Privilege userPri in userPris)
            {
                // 若不存在欲設定的權限清單，則移除權限
                if (!e.Data.Value.Keys.Contains(userPri.FuncId))
                {
                    // 成員權限移除
                    humanProv.DeletePrivilege(userPri.Id);
                }
            }

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetMemberList(MemberGetList(1));
        }

        /// <summary>
        /// 群組-查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnRoleSearchClicked(object sender, EventArgs e)
        {
            View.GetRoleList(OrganizationGetListByOrgName(View.OrgName));
        }

        /// <summary>
        /// 群組-刪除
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnDeleteMemberRoles(object sender, DataEventArgs<string> e)
        {
            // 檢查是否還有人員
            if (GetUsersInRoleByOrgName(e.Data).Count > 0)
            {
                View.ShowMessage("尚有人員名單，無法刪除。");
            }
            else
            {
                humanProv.DeleteOrgPrivilegeByOrgName(e.Data);// 先移除群組權限
                foreach (Organization childOrg in MemberFacade.OrganizationGetChildList(e.Data))
                {
                    humanProv.DeleteOrganization(childOrg.Name);// 移除子群組
                }
                View.GetRoleList(OrganizationGetListByOrgName(View.OrgName));
            }
        }

        /// <summary>
        /// 群組-新增
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnAddMemberRoles(object sender, DataEventArgs<string> e)
        {
            View.SetParentOrganizationInfo(humanProv.GetOrganization(e.Data));
        }

        /// <summary>
        /// 群組-成員列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetMemberRolesData(object sender, DataEventArgs<string> e)
        {
            GetMemberRolesData(e.Data);
        }

        /// <summary>
        /// 群組-新增群組
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnRoleAddClicked(object sender, DataEventArgs<Organization> e)
        {
            Organization org = humanProv.GetOrganization(e.Data.Name);
            // 判斷群組是否已存在
            if (Roles.RoleExists(e.Data.Name) && org.IsLoaded)
            {
                View.ShowMessage(I18N.Message.RoleExistMsg);
            }
            else
            {
                if (!Roles.RoleExists(e.Data.Name))
                {   // create membership role
                    Roles.CreateRole(e.Data.Name);
                }

                if (!org.IsLoaded)
                {
                    // create lunchking organization
                    org = e.Data;
                    ++org.OrgLevel;
                    org.Flag = (int)OrganizationFlag.General;
                    org.CreateId = View.LoginUser;
                    org.CreateTime = DateTime.Now;
                    humanProv.SetOrganization(org);

                    // 根據父群組具有權限設定其子群組權限
                    InsertOrgPrivilegesByOrgName(e.Data.ParentOrgName, View.LoginUser);
                }
            }
            View.OrgName = string.Empty;
            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetRoleList(OrganizationGetListByOrgName(View.OrgName));
        }

        /// <summary>
        /// 群組-編輯群組名稱
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnRoleNameEdit(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            Organization org = humanProv.GetOrganization(e.Data.Key);
            org.DisplayName = e.Data.Value;
            humanProv.SetOrganization(org);
            View.OrgDisplayName = org.DisplayName;
        }

        /// <summary>
        /// 群組-新增成員查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnOrgLeftSearch(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            MemberCollection m = memProv.MemberGetList(Member.Columns.UserName + " like " + e.Data.Key.Replace("*", "%"));
            View.SetOrgLeftSearchResult(m.Where(x => !e.Data.Value.Any(y => y.Contains(x.UserName))).ToList());
        }

        /// <summary>
        /// 群組-新增成員儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnOrgRightSave(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            List<MembershipUser> userList = MemberFacade.GetMembershipByUserId(Roles.GetUsersInRole(e.Data.Key).ToList());
            if (userList.Count > 0)
            {
                Roles.RemoveUsersFromRole(userList.Select(x => x.UserName).ToArray(), e.Data.Key); // 移除成員
            }

            if (e.Data.Value.Count > 0)
            {
                // 新增成員加入群組
                Roles.AddUsersToRole(e.Data.Value.ToArray(), e.Data.Key);
                // 群組異動紀錄
                List<string> exceptList = userList.Select(x => x.Email).Except(e.Data.Value).Union(e.Data.Value.Except(userList.Select(x => x.Email))).ToList();
                for (int i = 0; i < exceptList.Count(); i++)
                {
                    CommonFacade.AddAudit(exceptList[i], AuditType.Role, string.Join(",", Roles.GetRolesForUser(exceptList[i])), View.LoginUser, true);
                }
            }

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetRoleList(OrganizationGetListByOrgName(View.OrgName));
        }

        /// <summary>
        /// 群組-新增權限查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetOrgPrivilege(object sender, DataEventArgs<string> e)
        {
            Organization org = humanProv.GetOrganization(e.Data);
            List<SystemFunction> orgPrivileges = MemberFacade.OrganizationPrivilegeGetByOrgName(e.Data);
            View.SetOrgPrivilegeData(orgPrivileges.ToDictionary(x => x.Id, x => x.ParentFunc + "-" + x.DisplayName + "(" + x.TypeDesc + ")").Distinct().OrderBy(x => x.Value), org, SystemFunctionGetListFilterExist(orgPrivileges.Select(x => x.Id).Distinct().ToList()));
        }

        /// <summary>
        /// 群組-新增權限儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnOrgPrivilegeRightSave(object sender, DataEventArgs<KeyValuePair<string, Dictionary<int, string>>> e)
        {
            string orgName = e.Data.Key;
            List<int> funcIds = e.Data.Value.Keys.ToList();

            // 子群組若少了父群組擁有的權限，則移除父群組擁有的權限 (但不變動其成員所擁有的權限)
            Organization org = humanProv.GetOrganization(orgName);
            while (org.IsLoaded)
            {
                humanProv.OrgPrivilegeDeleteList(org.Name, funcIds);
                org = humanProv.GetOrganization(org.ParentOrgName);
            }
            MemberFacade.InsertOrgPrivilegeByFuncIds(orgName, funcIds, View.LoginUser);

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetRoleList(OrganizationGetListByOrgName(View.OrgName));
        }

        /// <summary>
        /// 功能-查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnFuncSearchClicked(object sender, EventArgs e)
        {
            View.GetSystemFuncList(SystemFunctionGetList(1));
        }

        protected void OnGetFuncCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sysProv.GetSystemFunctionListCount(SystemFunction.Columns.DisplayName + " like " + "%" + View.FuncNameSearch + "%");
        }

        protected void OnFuncPageChanged(object sender, DataEventArgs<int> e)
        {
            View.GetSystemFuncList(SystemFunctionGetList(e.Data));
        }

        /// <summary>
        /// 功能-群組/成員列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetPrivilegeData(object sender, DataEventArgs<int> e)
        {
            OrgPrivilegeCollection orgPris = humanProv.GetOrgPrivilegeListByFuncId(e.Data);
            List<Organization> orgList = humanProv.GetOrganizationListByOrgNameList(orgPris.Select(x => x.OrgName).ToList()).ToList();
            View.SetSystemFunctionInfo(humanProv.ViewPrivilegeGetListByFuncId(e.Data), OrganizationGetList(orgList), sysProv.GetSystemFunction(e.Data));
        }

        /// <summary>
        /// 功能-新增群組查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSysOrgPrivilegeLeftSearch(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            View.SetSysOrgPrivilegeLeftSearchResult(OrganizationFilterExistGetList(e.Data.Key, e.Data.Value));
        }

        /// <summary>
        /// 功能-新增群組儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSysOrgPrivilegeRightSave(object sender, DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>> e)
        {
            List<string> deleteOrgNameList = new List<string>();
            foreach (OrgPrivilege orgPri in humanProv.GetOrgPrivilegeListByFuncId(e.Data.Key.Key))
            {
                if (!e.Data.Value.Contains(orgPri.OrgName))
                {
                    deleteOrgNameList = deleteOrgNameList.Union(DeleteOrgPrivileges(orgPri.FuncId, orgPri.OrgName)).Distinct().ToList();// 移除群組權限
                }
            }

            foreach (string orgName in e.Data.Value.Where(x => !deleteOrgNameList.Any(y => y.Contains(x))))
            {
                // 新增群組權限
                MemberFacade.InsertOrgPrivilegeByFuncIds(orgName, new List<int>() { e.Data.Key.Key }, View.LoginUser);
            }

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetSystemFuncList(SystemFunctionGetList(1));
        }

        /// <summary>
        /// 功能-新增成員查詢
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSysPrivilegeLeftSearch(object sender, DataEventArgs<KeyValuePair<string, List<string>>> e)
        {
            MemberCollection m = memProv.MemberGetList(Member.Columns.UserName + " like " + e.Data.Key.Replace("*", "%"));
            View.SetSysPrivilegeLeftSearchResult(m.Where(x => !e.Data.Value.Any(y => y.Contains(x.UserName))).ToList());
        }

        /// <summary>
        /// 功能-新增成員儲存
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnSysPrivilegeRightSave(object sender, DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>> e)
        {
            foreach (string email in e.Data.Value)
            {
                // 新增成員權限
                MemberFacade.InsertPrivilegeForUser(e.Data.Key.Key, View.LoginUser, email);
            }

            foreach (ViewPrivilege userPri in humanProv.ViewPrivilegeGetListByFuncId(e.Data.Key.Key))
            {
                if (!e.Data.Value.Contains(userPri.UserName))
                {
                    // 成員權限移除
                    humanProv.DeletePrivilege(userPri.PrivilegeId);
                }
            }

            CommonFacade.CreateSystemFunctionPrivilege();
            View.GetSystemFuncList(SystemFunctionGetList(1));
        }

        #endregion event

        #region method

        #endregion method

        #region private method

        /// <summary>
        /// 取得會員列表
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private MemberCollection MemberGetList(int pageNumber)
        {
            return memProv.MemberGetList(pageNumber, View.PageSize, Member.Columns.UserName, Member.Columns.UserName + " like " + View.MemberEmail.Replace("*", "%"));
        }

        /// <summary>
        /// 取得群組列表(含父群組名稱)
        /// </summary>
        /// <param name="orgName">群組名稱</param>
        /// <returns></returns>
        private IOrderedEnumerable<KeyValuePair<Organization, string>> OrganizationGetListByOrgName(string orgName)
        {
            return OrganizationGetList(humanProv.GetOrganizationList(orgName).ToList());
        }

        private IOrderedEnumerable<KeyValuePair<Organization, string>> OrganizationGetList(List<Organization> orgList)
        {
            Dictionary<Organization, string> dataList = new Dictionary<Organization, string>();
            foreach (Organization org in orgList)
            {
                List<Organization> parentOrgList = OrganizationGetParentList(org.Name).Distinct().OrderBy(x => x.OrgLevel).ToList();
                if (!dataList.ContainsKey(org))
                {
                    dataList.Add(org, string.Join(">", parentOrgList.Select(x => x.DisplayName).ToList()));
                }
            }
            return dataList.Distinct().OrderBy(x => x.Value);
        }

        /// <summary>
        /// 取得群組列表
        /// </summary>
        /// <param name="orgName"></param>
        private void GetMemberRolesData(string orgName)
        {
            Organization mainOrg = humanProv.GetOrganization(orgName);
            View.SetOrganizationData(GetUsersInRoleByOrgName(orgName), mainOrg);
        }

        /// <summary>
        /// 取得功能列表
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <returns></returns>
        private SystemFunctionCollection SystemFunctionGetList(int pageNumber)
        {
            return sysProv.GetSystemFunctionList(pageNumber, View.PageSize, SystemFunction.Columns.FuncName, SystemFunction.Columns.DisplayName + " like " + "%" + View.FuncNameSearch + "%");
        }

        /// <summary>
        /// 取得可加入的功能列表(排除已存在的功能權限)
        /// </summary>
        /// <param name="existFuncs">已存在的功能權限</param>
        /// <returns></returns>
        private IOrderedEnumerable<KeyValuePair<int, string>> SystemFunctionGetListFilterExist(List<int> existFuncs)
        {
            Dictionary<int, string> dataList = new Dictionary<int, string>();
            SystemFunctionCollection funcs = sysProv.GetSystemFunctionList(-1, 9999, SystemFunction.Columns.Id);
            foreach (SystemFunction function in funcs)
            {
                if (!existFuncs.Contains(function.Id))
                {
                    dataList.Add(function.Id, string.Format("{0}-{1}({2})", function.ParentFunc, function.DisplayName, function.TypeDesc));
                }
            }
            return dataList.Distinct().OrderBy(x => x.Value);
        }

        /// <summary>
        /// 取得可加入的群組列表(排除已存在的群組清單)
        /// </summary>
        /// <param name="orgName"></param>
        /// <param name="existList"></param>
        /// <returns></returns>
        private Dictionary<string, string> OrganizationFilterExistGetList(string orgName, List<string> existList)
        {
            Dictionary<string, string> dataList = new Dictionary<string, string>();
            foreach (KeyValuePair<Organization, string> item in OrganizationGetListByOrgName(orgName))
            {
                if (!existList.Contains(item.Key.Name))
                {
                    dataList.Add(item.Key.Name, item.Value);
                }
            }
            return dataList;
        }

        /// <summary>
        /// 根據群組名稱找到所有父群組們(包含本身)
        /// </summary>
        /// <param name="name">群組名稱</param>
        /// <returns></returns>
        private OrganizationCollection OrganizationGetParentList(string name)
        {
            OrganizationCollection dataList = new OrganizationCollection();
            Organization org = humanProv.GetOrganization(name);
            while (org.IsLoaded)
            {
                dataList.Add(org);
                org = humanProv.GetOrganization(org.ParentOrgName);
            }
            return dataList;
        }

        /// <summary>
        /// 根據群組名稱找到所有群組成員(包含子群組們)
        /// </summary>
        /// <param name="orgName">群組名稱</param>
        /// <returns></returns>
        private List<string> GetUsersInRoleByOrgName(string orgName)
        {
            List<string> users = new List<string>();
            List<Organization> childList = MemberFacade.OrganizationGetChildList(orgName);
            foreach (Organization org in childList)
            {
                users = users.Union(Roles.GetUsersInRole(org.Name)).ToList();
            }
            return users;
        }

        /// <summary>
        /// 根據權限及群組名稱[移除]其父群組對應權限
        /// </summary>
        /// <param name="funcId">功能權限id</param>
        /// <param name="orgName">群組名稱</param>
        private List<string> DeleteOrgPrivileges(int funcId, string orgName)
        {
            List<string> deleteOrgNameList = new List<string>();
            List<OrgPrivilege> orgPriList = humanProv.GetOrgPrivilegeListByFuncId(funcId).Where(x => x.OrgName.Equals(orgName)).ToList();
            foreach (OrgPrivilege orgPri in orgPriList)
            {
                if (orgPri.IsLoaded)
                {
                    deleteOrgNameList.Add(orgPri.OrgName);
                    Organization org = humanProv.GetOrganization(orgName);
                    deleteOrgNameList = deleteOrgNameList.Union(DeleteOrgPrivileges(orgPri.FuncId, org.ParentOrgName)).ToList();
                    humanProv.DeleteOrgPrivilege(orgPri.Id);
                }
            }
            return deleteOrgNameList;
        }

        /// <summary>
        /// 根據群組名稱查找所有權限，並[新增]於其子群組及成員
        /// </summary>
        /// <param name="orgName">群組名稱</param>
        /// <param name="createId"></param>
        private void InsertOrgPrivilegesByOrgName(string orgName, string createId)
        {
            OrgPrivilegeCollection orgPriList = humanProv.GetOrgPrivilegeListByOrgName(orgName);
            foreach (OrgPrivilege orgPri in orgPriList)
            {
                MemberFacade.InsertOrgPrivileges(orgPri.FuncId, orgPri.OrgName, createId);
            }
        }

        #endregion private method
    }
}
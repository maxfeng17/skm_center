﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class ContactUsPresenter : Presenter<IContactUsView>
    {
        public override bool OnViewInitialized() 
        {
            base.OnViewInitialized();
            int _city = View.CityId;
            int _categoryId = View.CategoryID;

            List<IViewPponDeal> vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByTopSalesDeal(3);
            View.SetTopSalesDeal(vpd);
            return true;
        }
        
    }
}

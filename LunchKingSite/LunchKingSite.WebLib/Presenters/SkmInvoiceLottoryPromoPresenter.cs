﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.BizLogic.Model;
using System.Net.Mail;
using System.Web;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class SkmInvoiceLottoryPromoPresenter : Presenter<ISkmInvoiceLottoryPromoView>
    {
        private ISysConfProvider _cp;
        private IOrderProvider _op;
        private IEventProvider _ep;
        private IMemberProvider _mp;
        private ILocationProvider _lp;

        public SkmInvoiceLottoryPromoPresenter(ISysConfProvider cp, IOrderProvider op, IEventProvider ep, IMemberProvider mp, ILocationProvider lp)
        {
            _cp = cp;
            _op = op;
            _ep = ep;
            _mp = mp;
            _lp = lp;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            Member mem = _mp.MemberGet(View.UserName);
            if (null != mem && mem.IsLoaded)
            {
                if (mem.BuildingGuid.HasValue && !Guid.Equals(MemberUtilityCore.DefaultBuildingGuid, mem.BuildingGuid.Value))
                {
                    Building bu = _lp.BuildingGet(mem.BuildingGuid.Value);
                    City area = _lp.CityGet(bu.CityId);
                    City city = _lp.CityGet(area.ParentId.Value);
                    string address = mem.CompanyAddress;

                    View.UserInfo = mem.LastName + "|" + mem.FirstName + "|" + mem.Mobile + "|" + city.Id.ToString() +
                                    "|" + area.Id.ToString() + "|" + mem.BuildingGuid.ToString() + "|" + address;
                }
                else
                {
                    View.UserInfo = mem.LastName + "|" + mem.FirstName + "|" + mem.Mobile + "|0|0|" +
                                    Guid.Empty.ToString() + "|";
                }
            }
            //新光商圈~登錄發票活動日期為2013.09.24~'2014.1.10'
            //if ( new DateTime(2013, 9, 24)< DateTime.Now && DateTime.Now <= new DateTime(2014, 1, 10)) 
            if (DateTime.Now <= new DateTime(2014, 1, 10))
            {
                if (View.IsPromoLogin)
                {
                    View.ShowExchange();
                }
                else
                {
                    View.ShowChoseEvent();
                }
            }
            else
            {
                View.ShowNoEvent();
            }
            return true;

        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            View.Exchange += OnExchange;
            return true;
        }

        #region event
        protected void OnExchange(object sender, DataEventArgs<SkmLottoryInvoice> e)
        {
            if (string.IsNullOrWhiteSpace(View.UserName))
            {
                View.ShowExchange();
            }
            else
            {
                bool isCreditCardVerification = false;
                bool isInvoiceVerification = false;

                Regex regCreditCardNumber = new Regex(@"^\d{8}$", RegexOptions.Compiled);
                if (regCreditCardNumber.IsMatch(e.Data.CardNumber))
                {
                    CreditCardPremiumCollection taishinCardRules = CreditCardPremiumManager.GetTaishinCreditCardsNumberRules();

                    var item = taishinCardRules.Where(t => e.Data.CardNumber.Length >= t.CardNum.Length &&
                              t.CardNum == e.Data.CardNumber.Substring(0, t.CardNum.Length));

                    isCreditCardVerification = (item.Any()) ? true : false;
                }

                Regex regInvoice = new Regex(@"^[a-zA-Z][a-zA-Z]\d{8}$", RegexOptions.Compiled);
                if (regInvoice.IsMatch(e.Data.InvoiceNumber))
                {

                    var skmInvoices =
                        PromotionFacade.GetSkmEventPromoInvoiceContentCollection()
                                       .Where(x => x.InvoiceNumber.ToLower() == e.Data.InvoiceNumber.ToLower());
                    isInvoiceVerification = (skmInvoices.Any()) ? false : true;
                }

                if (isCreditCardVerification && isInvoiceVerification)
                {
                    PromotionFacade.SetSkmEventPromoInvoiceContent(e.Data.CardNumber, e.Data.StoreName, e.Data.InvoiceNumber, e.Data.LoginUserName, e.Data.LoginUserAddress, e.Data.LoginUserMobile, View.UserName);
                    View.ShowSuccess();
                }
            }
        }
        #endregion

        #region private method

        #endregion
    }
}

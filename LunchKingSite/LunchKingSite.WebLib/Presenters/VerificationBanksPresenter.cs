﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class VerificationBanksPresenter : Presenter<IVerificationBanks>
    {
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchLog += new EventHandler<DataEventArgs<string>>(this.OnSearchLog);
            DateTime basedate;
            TrustVerificationReport report = mp.TrustVerificationReportGetLatest(View.Provider);
            
            if (report != null && report.Id != 0)
            {
                basedate = report.ReportIntervalEnd;
            }
            else
            {
                basedate = new DateTime(2011, 7, 16);
            }
            switch (View.TrustType)
            {
                case TrustBankType.Trust:
                    View.SetAmountSum(report.TotalTrustAmount - report.OverOneYearTotal, report.TotalCount, basedate);
                    break;
                case TrustBankType.Escrow:
                    //TrustVerificationReport report2 = mp.TrustVerificationReportCollectionGet(View.Provider).Where(x => x.Id != report.Id).FirstOrDefault();
                    //int sumamount = (report2.TotalTrustAmount - report2.OverOneYearTotal) + report.EscrowOrderAmount.Value - report.EscrowVerifyAmount.Value - report.EscrowRefundAmount.Value;
                    //View.SetAmountSum(sumamount, report.EscrowOrderAmount.Value, report.EscrowVerifyAmount.Value, report.EscrowRefundAmount.Value);
                    break;
            }
            return true;
        }

        public void OnSearchLog(object sender, DataEventArgs<string> e)
        {
            TrustVerificationReport report = mp.TrustVerificationReportGetLatest(View.Provider);
            switch (View.LogType)
            {
                case TrustLogType.CouponCashTrust:
                    View.SetCouponTrustLog(mp.CashTrustLogGetByTrustSequenceNumber(e.Data, View.Provider), report);
                    break;

                case TrustLogType.UserCashTrust:
                    int id;
                    if (int.TryParse(e.Data, out id))
                    {
                        UserCashTrustLogCollection uctlc = mp.UserCashTrustLogWithinYearGetList(id);
                        if (uctlc.Count > 0)
                        {
                            if (uctlc.Sum(x => x.Amount) < 0)
                            {
                                uctlc = new UserCashTrustLogCollection();
                                uctlc.Add(new UserCashTrustLog { UserId = id, Amount = 0 });
                            }
                        }
                        View.SetUserTrustLog(uctlc, report);
                    }
                    break;
            }
        }

        public int ReturnCombineStatus(TrustBankStatus A, params TrustBankStatus[] args)
        {
            int result = 0;
            foreach (TrustBankStatus item in args)
            {
                result = result | (int)item;
            }
            return result;
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesProductionBusinessListPresenter : Presenter<ISalesProductionBusinessListView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IPponProvider pp;
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }

        public SalesProductionBusinessListPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IPponProvider pponProv)
        {
            sp = sellerProv;
            hp = humProv;
            pp = pponProv;
        }

        #region evnet

        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }
        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.ViewProposalSellerGetCountForProduction(View.City, View.DateType, View.DateStart, View.DateEnd, View.StatusType, 
                                                                View.Status, View.Emp, View.DealProperty, View.PicType, View.DealType, 
                                                                View.DealSubType, View.MarketingResource, View.ChkProduction);

        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }
        
        #endregion

        #region private method
        private void LoadData(int pageNumber)
        {
            string orderby = "";
            if (string.IsNullOrEmpty(View.OrderField))
            {
                if (View.DateType == 2 || View.DateType == 3)
                {
                    orderby = "maxtime desc";
                }
                else
                {
                    orderby = ViewProposalSeller.Columns.OrderTimeS + " desc";
                }
            }
            else
            {
                orderby = View.OrderField + " " + View.OrderBy;
            }
                            

            string sSQL = "";
            ViewProposalSellerCollection Proposals = sp.ViewProposalSellerGetListForProduction(pageNumber, View.PageSize, orderby , View.City, View.DateType, 
                                                                                               View.DateStart, View.DateEnd, View.StatusType, View.Status, View.Emp, 
                                                                                               View.DealProperty, View.PicType, View.DealType, View.DealSubType, View.MarketingResource, View.ChkProduction, ref sSQL);
            Dictionary<ViewProposalSeller, ProposalAssignLogCollection> dataList = new Dictionary<ViewProposalSeller, ProposalAssignLogCollection>();

            foreach (ViewProposalSeller pro in Proposals)
            {
                ProposalAssignLogCollection logs = sp.ProposalAssignLogGetList(pro.Id);
                dataList.Add(pro, logs);
            }


            ProposalFacade.ProposalPerformanceLogSet("/sal/SalesProductionBusinessList.aspx", sSQL, View.UserName);
            View.SetBusinessList(dataList);
        }      

        #endregion
    }
}

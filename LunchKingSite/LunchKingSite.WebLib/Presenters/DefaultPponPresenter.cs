using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.WebLib.Component;
using log4net;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Presenters
{

    public class DefaultPponPresenter : Presenter<IDefaultPponView>
    {
        private IPponProvider pp;
        private ICmsProvider cp;
        private IMemberProvider mp;
        private IOrderProvider op;
        private static ISysConfProvider conf;
        private ISellerProvider sp;
        private IEventProvider ep;
        private ICmsProvider cmp;
        private ILog logger;
       // private ISysConfProvider config;

        public DefaultPponPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            conf = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            // config = ProviderFactory.Instance().GetConfig();
            logger = LogManager.GetLogger(typeof(DefaultPponPresenter));

        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!CheckIfIsEffectiveCityId(View.SelectedCityId))
            {
                return false;
            }

            View.isMultipleMainDeals = conf.MultipleMainDeal;
            View.BypassSsoVerify = conf.BypassSsoVerify;
            View.EdmPopUpCacheName = conf.EdmPopUpCacheName;

            int hotSaleBigTopSaleCount = 20;
            int cityId = View.SelectedCityId;
            int categoryIdInPiinLife = View.CategoryIdInPiinLife;
            int subRegionCategoryId = View.SubRegionCategoryId;

            ///排掉新光
            if (View.SelectedCityId == 496 || View.SelectedChannelId == 185)
            {
                View.RedirectToDefault();
                return false;
            }

            ///主要的檔次列表
            List<MultipleMainDealPreview> multideals = GetMultipleDeals();
            View.SetMutilpMainDeals(multideals);

            #region ●今日熱銷
            var hotDeals = multideals.OrderByDescending(x => x.PponDeal.ItemPrice * x.PponDeal.OrderedQuantity)
                                     .Take(multideals.Count > hotSaleBigTopSaleCount ? hotSaleBigTopSaleCount : multideals.Count)
                                     .ToList();
            View.SetHotSaleMutilpMainDeals(hotDeals);

            #endregion

            #region 收藏
            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            View.SetMemberCollection(mp.MemberCollectDealGetOnlineDealGuids(
                View.UserId, baseDate.AddDays(-1), baseDate.AddDays(1)));
            #endregion

            #region ●最新上檔
            var todayDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeS < DateTime.Now).OrderBy(x => DateTime.Now - x.PponDeal.BusinessHourOrderTimeS).ToList();
            todayDeals = todayDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetToDayMutilpMainDeals(todayDeals);
            #endregion

            #region ●最後倒數
            var lastdaydDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeE > DateTime.Now).OrderBy(x => x.PponDeal.BusinessHourOrderTimeE - DateTime.Now).ToList();
            lastdaydDeals = lastdaydDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetLastDayMutilpMainDeals(lastdaydDeals);
            #endregion

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckEventMail += OnCheckEventMail;
            return true;
        }

        private bool CheckIfIsEffectiveCityId(int cityId)
        {
            bool isEffective = true;

            if (CategoryManager.GetChannelIdByCityId(cityId) == 0)
            {
                isEffective = false;
                View.RedirectToDefault();
            }

            return isEffective;
        }

        private List<MultipleMainDealPreview> GetMultipleDeals()
        {
            DateTime now = DateTime.Now;

            //區域分類
            int channelId = View.SelectedChannelId;
            int areaId = View.SelectedRegionId;
            int subCategoryId = View.SelectedSubcategoryId;
            int cityId = View.SelectedCityId;

            //新版分類
            int dealCategoryId = View.SelectedDealCategoryId;

            //過濾檔次用List<int>
            List<int> categoryList = new List<int>()
            {
                subCategoryId, dealCategoryId
            };
            categoryList = categoryList.Where(x => x > 0).Distinct().ToList();

            List<MultipleMainDealPreview> multideals = ViewPponDealManager.DefaultManager
                .MultipleMainDealPreviewGetListByCategoryCriteria(channelId, areaId, categoryList);

            var filterCateforyIds = View.FilterCategoryIdList;
            if (filterCateforyIds.Count > 0)
            {
                multideals = multideals.Where(x => filterCateforyIds.Any(y => x.DealCategoryIdList.Contains(y) ||
                    (y == CategoryManager.Default.LastDay.CategoryId &&
                        (new TimeSpan(x.PponDeal.BusinessHourOrderTimeE.Ticks - DateTime.Now.Ticks)).TotalDays < 1)))
                        .ToList();
            }

            if (channelId > 0)
            {
                List<CategoryViewItem> categoryInfos = GetCategoryInfos(channelId, areaId, subCategoryId, dealCategoryId, cityId);

                //2.checkbox分類的篩選 (即買即用、最後一天)
                List<CategoryDealCount> filterNodes = PponDealPreviewManager
                    .GetSpecialDealCountListByChannel(channelId, areaId).Where(x => x.DealCount > 0)
                    .Where(x => x.CategoryId != 138 && x.CategoryId != 139 && x.CategoryId != 318).ToList();

                //設定可以篩選的選項
                View.SetMultipleCategories(categoryInfos, filterNodes, null);



                //var root = HxRootCategory.Create();
                //List<HxNode> nodes = root.GetSpecialNodes(channelId, areaId);

                //List<MultipleMainDealPreview> multideals2 =
                //    root.GetDeals(channelId, areaId, View.FilterCategoryIdList, dealCategoryId, subCategoryId);

                //var subCategories = root.Get(dealCategoryId);
                //string json = ProviderFactory.Instance().GetSerializer().Serialize(channelCategory, true);
                //System.IO.File.WriteAllText("c:\\temp\\root.json", json);

            }
            else
            {
                //對應的PponCity未設定ChannelId，依據現況，表示並無DealCategory資料需要顯示
                View.SetMultipleCategories(new List<CategoryViewItem>(), new List<CategoryDealCount>(), new SystemCodeCollection());
            }

            return multideals;
        }

        public static List<CategoryViewItem> GetCategoryInfos(int channelId, int areaId, int subCategoryId, int dealCategoryId, int cityId)
        {
            List<CategoryViewItem> result = new List<CategoryViewItem>();
         
            List<MultipleMainDealPreview> areamultideals = ViewPponDealManager.DefaultManager
                .MultipleMainDealPreviewGetListByCategoryCriteria(channelId, areaId, new List<int>());
            //1.新版分類的篩選
            CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(channelId, areaId, subCategoryId);
            var dealCount = new CategoryDealCount(0, "全部");
            dealCount.DealCount = node.TotalCount;
            node.DealCountList.Insert(0, dealCount);
            //node.DealCountList.InsertRange()
            List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>> categoryDealCountList =
                PponFacade.SubCategoryDealCountByMainCategoryDealCount(
                    node.DealCountList.Where(x => x.DealCount > 0 || x.CategoryId.Equals(0)).ToList(), areamultideals);

            foreach (KeyValuePair<CategoryDealCount, List<CategoryDealCount>> pair in categoryDealCountList)
            {
                var item = new CategoryViewItem(pair.Key);
                item.Children = pair.Value.ConvertAll(t => new CategoryViewItem(t));
                if (item.CategoryId == dealCategoryId)
                {
                    item.Selected = true;
                }
                CategoryViewItem selSubCategory = item.Children.FirstOrDefault(t => t.CategoryId == dealCategoryId);
                if (selSubCategory != null)
                {
                    item.Selected = true;
                    selSubCategory.Selected = true;

                }
                result.Add(item);
            }

            //整理連結等前端會用到的屬性

            foreach (var item in result)
            {
                item.Url = GetCategoryUrl(channelId, areaId, subCategoryId, item.CategoryId, cityId);
                if (channelId == CategoryManager.Default.Delivery.CategoryId)
                {
                    item.OpenPage = CategoryOpenPageType.Ajax;
                }
                foreach (var child in item.Children)
                {
                    child.Url = GetCategoryUrl(channelId, areaId, subCategoryId, child.CategoryId, cityId);
                    if (channelId == CategoryManager.Default.Delivery.CategoryId)
                    {
                        child.OpenPage = CategoryOpenPageType.Ajax;
                    }
                }
            }
            
            //插入品生活的分類
            SystemCodeCollection piinlifeLinkes = SystemCodeManager.GetSystemCodeListByGroup("17PiinlifeLink" + channelId);
            List<CategoryViewItem> piinlifeCategories = new List<CategoryViewItem>();
            foreach (var link in piinlifeLinkes)
            {
                var newOne = new CategoryViewItem(link);
                newOne.Url = GetPiinlifeCategoryUrl(newOne.CategoryId);
                piinlifeCategories.Add(newOne);
            }
            if (channelId == CategoryManager.Default.PponDeal.CategoryId || 
                channelId == CategoryManager.Default.Beauty.CategoryId)
            {
                result.InsertRange(1, piinlifeCategories);
            }
            else if (channelId == CategoryManager.Default.Travel.CategoryId)
            {
                result.AddRange(piinlifeCategories);
            }

            return result;
        }

        private static string GetCategoryUrl(int channelId, int areaId, int subCategoryId, int dealCategoryId, int cityId)
        {
            const string CPA_CATEGORY = "17_category";
 
            
            string categoryUrl = string.Format("/channel/{4},{1},{2}?cid={0}&cpa={3}", 
                cityId, areaId, dealCategoryId, CPA_CATEGORY, channelId);

            if (subCategoryId > 0 && subCategoryId != areaId)
            {
                if (categoryUrl.IndexOf("?", StringComparison.Ordinal) > 0)
                {
                    categoryUrl += "&subc=" + subCategoryId;
                }
                else
                {
                    categoryUrl += "?subc=" + subCategoryId;
                }
            }
            categoryUrl = Helper.CombineUrl(conf.SiteUrl, categoryUrl);
            return categoryUrl;
        }

        private static string GetPiinlifeCategoryUrl(int dealCategoryId)
        {
            string categoryUrl = string.Format("/piinlife/default.aspx?cat={0}&cpa=17_category", dealCategoryId);
            return categoryUrl;
        }

        protected void OnCheckEventMail(object sender, EventArgs e)
        {
            EventActivity activity = ep.EventActivityEdmGetList(-1)
                .FirstOrDefault(x => x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now);
            if (activity != null)
            {
                CmsRandomCityCollection cities = cmp.CmsRandomCityGetByPid(activity.Id, RandomCmsType.PopUpCities);
                KeyValuePair<EventActivity, CmsRandomCityCollection> pair = new KeyValuePair<EventActivity, CmsRandomCityCollection>(activity, cities);
                View.SetEventMainActivities(pair);
            }
            else
            {
                View.ShowEventEmail = false;
            }
        }

 
        #region public class methods

        public bool AddUserTrack(string sessionId, string displayName, string url, string queryString, string context)
        {
            return cp.UserTrackSet(sessionId, displayName, url, queryString, context);
        }

        #endregion public class methods

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesProposalProgressPresenter : Presenter<ISalesProposalProgressView>
    {
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(0, "", View.Monthly);
            }
            else
            {
                //登入者
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(View.UserName));
                if (emp.IsLoaded)
                {
                    if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.CrossDeptTeam))
                    {
                        LoadData(0, View.CrossDeptTeam, View.Monthly);
                    }
                    else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
                    {
                        LoadData(View.EmpUserId, "", View.Monthly);
                    }
                }
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.MonthChanged += OnMonthChanged;
            return true;
        }

        public SalesProposalProgressPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IMemberProvider memProv)
        {
            sp = sellerProv;
            hp = humProv;
            mp = memProv;
        }
        protected void OnMonthChanged(object sender, DataEventArgs<string> e)
        {
            //有全區權限
            int month = default(int);
            int.TryParse(e.Data, out month);
            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(0, "", month);
            }
            else
            {
                //登入者
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(View.UserName));
                if (emp.IsLoaded)
                {
                    if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.CrossDeptTeam))
                    {
                        LoadData(0, View.CrossDeptTeam, month);
                    }
                    else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
                    {
                        LoadData(View.EmpUserId, "", month);
                    }
                }
            }
        }
        #region Private Method

        private void LoadData(int EmpUserId, string CrossDeptTeam, int monthly)
        {
            IEnumerable<ViewProposalSeller> pro = sp.ViewProposalSellerGetListByPeriod((int)ProposalApplyFlag.Apply, EmpUserId, CrossDeptTeam, monthly);           
            View.SetProposalProgress(pro);
            
        }

        #endregion
    }
}

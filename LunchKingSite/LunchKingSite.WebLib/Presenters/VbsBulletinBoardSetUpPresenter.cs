﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class VbsBulletinBoardSetUpPresenter : Presenter<IVbsBulletinBoardSetUpView>
    {
        private IVerificationProvider vp;
        private IMemberProvider mp;

        public VbsBulletinBoardSetUpPresenter()
        {
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            View.BuildCbPublishSellerType();
            View.BuildDdlSellerType();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            View.AddContentMaxLength();
            View.SetPublishing += OnSetPublishing;
            View.StickPublishing += OnStickPublishing;
            View.DeletePublishing += OnDeletePublishing;
            View.BulletinBoardListGet += OnBulletinBoardListGet;
            View.PageChanged += OnPageChanged;
            View.GetPublishCount += OnGetPublishCount;

            return true;
        }

        private void OnBulletinBoardListGet(object sender, BulletinBoardListGetEventArgs e)
        {
            VbsBulletinBoardCollection boardCols = GetBulletinBoardCollection((VbsBulletinBoardPublishSellerType)View.FilterSellerType, View.CurrentPage, View.PageSize);
            View.SetBulletinBoardList(boardCols);
        }

        private void OnSetPublishing(object sender, PublishingArgs e) 
        {
            if (e.Mode == UpdateMode.Add) 
            { 
                VbsBulletinBoard msg = new VbsBulletinBoard();
                msg.PublishSellerType += View.IsPublishToHouse ? (int)VbsBulletinBoardPublishSellerType.ToHouse : 0;
                msg.PublishSellerType += View.IsPublishToShop ? (int)VbsBulletinBoardPublishSellerType.ToShop : 0;
                msg.PublishContent = View.PublishContent;
                msg.PublishStatus = (int)VbsBulletinBoardPublishStatus.Enabled;
                msg.PublishTime = (DateTime)View.PublishTime;
                msg.CreateId = mp.MemberUniqueIdGet(View.UserName);
                msg.CreateTime = DateTime.Now;
                msg.LinkTitle = View.LinkTitle;
                msg.LinkUrl = View.LinkUrl;

                if (vp.VbsBulletinBoardSet(msg)) 
                {
                    OnBulletinBoardListGet(sender, new BulletinBoardListGetEventArgs { CurrentPage = 1, SellerType = View.FilterSellerType });
                    View.PublishSetDone(true, UpdateMode.Add, string.Empty);
                }
            }
        }

        private void OnStickPublishing(object sender, DataEventArgs<int> e) 
        {
            VbsBulletinBoard msg = vp.VbsBulletinBoardGetById(e.Data);
            //如果原本是 "置頂" , 改回 "非置頂"; 反之亦然
            msg.IsSticky = msg.IsSticky ? false : true;
            if (vp.VbsBulletinBoardSet(msg)) 
            {
                OnBulletinBoardListGet(sender, new BulletinBoardListGetEventArgs { CurrentPage = 1, SellerType = View.FilterSellerType });
                View.PublishSetDone(true, UpdateMode.Stick, msg.IsSticky == false ? "取消" : string.Empty);
            }
        }

        private void OnDeletePublishing(object sender, DataEventArgs<int> e) 
        {
            if (vp.VbsBulletinBoardDelete(e.Data)) 
            {
                OnBulletinBoardListGet(sender, new BulletinBoardListGetEventArgs { CurrentPage = 1, SellerType = View.FilterSellerType });
                View.PublishSetDone(true, UpdateMode.Delete, string.Empty);
            }
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            OnBulletinBoardListGet(sender, new BulletinBoardListGetEventArgs { CurrentPage = e.Data, SellerType = View.FilterSellerType });
        }

        protected void OnGetPublishCount(object sender, DataEventArgs<int> e) 
        {
            VbsBulletinBoardCollection boardCols = GetBulletinBoardCollection((VbsBulletinBoardPublishSellerType)View.FilterSellerType, 0, 0);
            e.Data = boardCols.Count();
        }

        #region Method

        private VbsBulletinBoardCollection GetBulletinBoardCollection(VbsBulletinBoardPublishSellerType type, int currentPage, int pageSize) 
        {
            string sellerFilter = string.Empty;

            switch (type)
            {
                case VbsBulletinBoardPublishSellerType.ToHouse:
                    //returnCols = boardCols.Where(x => x.PublishSellerType == (int)VbsBulletinBoardPublishSellerType.ToHouse || x.PublishSellerType == (int)VbsBulletinBoardPublishSellerType.All);
                    sellerFilter = VbsBulletinBoard.Columns.PublishSellerType + " in (" + (int)VbsBulletinBoardPublishSellerType.ToHouse + "," + (int)VbsBulletinBoardPublishSellerType.All + ")";
                    break;
                case VbsBulletinBoardPublishSellerType.ToShop:
                    sellerFilter = VbsBulletinBoard.Columns.PublishSellerType + " in (" + (int)VbsBulletinBoardPublishSellerType.ToShop + "," + (int)VbsBulletinBoardPublishSellerType.All + ")";
                    break;
                default:
                    break;
            }

            VbsBulletinBoardCollection boardCols = VerificationFacade.BulletinBoardGetListByAnnounce(currentPage, pageSize, false, true, sellerFilter);
            return boardCols;
        }

        #endregion
    }
}

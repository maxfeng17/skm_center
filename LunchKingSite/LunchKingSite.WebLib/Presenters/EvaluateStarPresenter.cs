﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class EvaluateStarPresenter : Presenter<IEvaluateStar>
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ILog logger = LogManager.GetLogger(typeof(EvaluateStarPresenter));
        public EvaluateStarPresenter()
        {
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (View.tId.Equals(new Guid())) { return false; }

            var ctl = mp.CashTrustLogGet(View.tId);
            var evaluateID = mp.EvaluateGetMainID(View.tId);

            if (ctl.UsageVerifiedTime < DateTime.Now.AddDays(-7))
            {
                //已逾期
                View.SayMessage("expired");
                return false;
            }

            int eValidItems = mp.EvaluateDetailGetByMainID(evaluateID, (int)EvaluateDataType.Final).Count();
            int eInValidItems = mp.EvaluateDetailGetByMainID(evaluateID, (int)EvaluateDataType.Temp).Count();
            if (eValidItems == 0 && eInValidItems > 0 && View.flag == "done")
            {
                //若負評登入後回來複製temp資料到final
                CopyTempToFinal(evaluateID);
                View.SayMessage("exitPop");
                return false;
            }
            else if (eValidItems == 0 && eInValidItems == 0 && ctl.Status == (int)TrustStatus.Verified && ctl.UsageVerifiedTime != null)
            {
                View.ItemName = ctl.ItemName.ToString();
                LoadQuestions();
                return false;
            }
            else if (eValidItems == 0 && eInValidItems > 0)
            {
                View.ItemName = ctl.ItemName.ToString();
                LoadQuestions();
                return false;
            }
            else
            {
                //已評價
                View.SayMessage("dobefore");
                return false;
            }
        }

        private void LoadQuestions()
        {
            var q = mp.UserEvaluateItemCollectionGet();

            StringBuilder sb = new StringBuilder();
            sb.Append(string.Format("<input type='hidden' name='qMinId' value='{0}'/>", q.Select(x => x.Id).Min()));
            sb.Append(string.Format("<input type='hidden' name='qMaxId' value='{0}'/>", q.Select(x => x.Id).Max()));
            foreach (var item in q.OrderBy(x => x.Sequence))
            {
                sb.Append(string.Format("<div class='form-unit' id='divq{0}' style='display:{1}'>", item.Id, ((item.Id == 1 || item.Id == 5) ? "block" : "none")));
                sb.Append("<div class='data-whole'>");
                sb.Append(string.Format("<p>{0}{1}</p>", item.IsRequired ? "<span class='nam-value'>＊</span>" : "", item.ItemDesc.ToString()));
                switch (item.EvaluateType)
                {
                    case (int)EvaluateType.Rate:
                        sb.Append(string.Format("<div id='q{0}' class='starbox data'  coltype='{1}' isrequire='{2}' ></div><p id='q{0}_text' class='rateTxt'></p>", item.Id, (int)EvaluateType.Rate, item.IsRequired ? 1 : 0));
                        sb.Append(string.Format("<input type='hidden' name='q{0}_val' value=''>", item.Id));
                        break;
                    case (int)EvaluateType.Comment:
                        sb.Append(@"<br/><p class='cancel-mr'><input type='button' class='btn btn-small rd-payment-large-btn btn_message' value='餐點不好吃'></p>");
                        sb.Append(@"<p class='cancel-mr'><input type='button' class='btn btn-small rd-payment-large-btn btn_message' value='店家差別待遇'></p>");
                        sb.Append(@"<p class='cancel-mr'><input type='button' class='btn btn-small rd-payment-large-btn btn_message' value='與優惠內容不符'></p>");
                        sb.Append(@"<p class='cancel-mr'><input type='button' class='btn btn-small rd-payment-large-btn btn_message' value='環境不佳'></p>");
                        sb.Append(@"<p class='cancel-mr'><input type='button' class='btn btn-small rd-payment-large-btn btn_message' value='服務態度不好'></p>");
                        sb.Append(string.Format(@"<textarea class='data' rows='4' cols='50' id='q{0}' coltype='{1}' onkeyup='CheckLength(this);' placeholder='{2}'></textarea><br/><span id='q{0}_tips' style='float:right'></span>", item.Id, (int)EvaluateType.Comment
                            , @"歡迎您在此留下寶貴的意見，店家不會針對您的建議直接回覆給您，但您的意見會成為店家提升品質的重要依據。(限140字)"));
                        sb.Append(string.Format(@"<input type='hidden' name='q{0}_val' value=''>", item.Id));
                        break;
                }
                //sb.Append("<div style='clear:both'/></div>");
                sb.Append("</div>");
                sb.Append("</div>");
            }

            View.QTable = sb.ToString();
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveFeedback += OnSaveFeedback;
            View.SaveOpenEvaluteMail += OnSaveOpenEvaluteMail;
            return true;
        }

        public void OnSaveFeedback(object sender, EventArgs e)
        {
            XElement x = View.QuestionResult;
            Guid tid = View.tId;

            try
            {
                int check = mp.EvaluateGetMainID(tid);
                //M版好像會重複送
                if (check == 0)
                {
                    mp.UserEvaluateMainSet(new UserEvaluateMain
                    {
                        TrustId = tid,
                        CreateTime = DateTime.Now,
                        SourceType = View.fromType
                    });
                }
            }
            catch (Exception ex)
            {
                throw new Exception("UserEvaluateMain 儲存失敗:" + ex.Message + ex.StackTrace);
            }
            
            int mainID = mp.EvaluateGetMainID(tid);

            if (mainID > 0)
            {
                var EAnsCollection = new UserEvaluateDetailCollection();
                foreach (var elem in x.Elements("row"))
                {
                    UserEvaluateDetail eAns = null;
                    int type, qid, val;
                    if (!int.TryParse(elem.Element("type").Value, out type)) { continue; }
                    if (!int.TryParse(elem.Element("qid").Value, out qid)) { continue; }
                    switch (type)
                    {
                        case (int)EvaluateType.Rate:
                            if (!int.TryParse(elem.Element("val").Value, out val)) { continue; }
                            eAns = new UserEvaluateDetail
                            {
                                MainId = mainID,
                                ItemId = qid,
                                Rating = val,
                                DataType = View.DataType
                            };
                            break;
                        case (int)EvaluateType.Comment:
                            eAns = new UserEvaluateDetail
                            {
                                MainId = mainID,
                                ItemId = qid,
                                Comment = elem.Element("val").Value,
                                DataType = View.DataType
                            };
                            break;
                    }
                    if (eAns != null)
                    {
                        EAnsCollection.Add(eAns);
                    }
                }

                //iOS在M版有些問題
                //加上後台檢查必填項目
                if (!EAnsCollection.Any(g => g.ItemId == 1))
                {
                    EAnsCollection.Add(new UserEvaluateDetail
                    {
                        MainId = mainID,
                        ItemId = 1,
                        Rating = 3,
                        DataType = View.DataType
                    });
                }
                if (!EAnsCollection.Any(g => g.ItemId == 2))
                {
                    EAnsCollection.Add(new UserEvaluateDetail
                    {
                        MainId = mainID,
                        ItemId = 2,
                        Rating = 3,
                        DataType = View.DataType
                    });
                }

                if (EAnsCollection.Any())
                {
                    try
                    {
                        mp.UserEvaluateDetailSetList(EAnsCollection);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("EAnsCollection 儲存失敗:" + ex.Message + ex.StackTrace);
                    }
                }
            }
        }

        public void OnSaveOpenEvaluteMail(object sender, EventArgs e)
        {
            if (View.orderGuid == Guid.Empty) {
                return;
            }

            UserEvaluateLog uelog = mp.UserEvaluateLogGetByOGuid(View.orderGuid);

            if (uelog.Id > 0)
            {
                uelog.IsOpen = true;
                mp.UserEvaluateLogSet(uelog);
            }
            else
            {
                return;
            }
        }

        private void CopyTempToFinal(int evaluateID)
        {
            UserEvaluateDetailCollection tempItem = mp.EvaluateDetailGetByMainID(evaluateID, (int)EvaluateDataType.Temp);

            var EAnsCollection = new UserEvaluateDetailCollection();
            int i = 1;
            foreach (var elem in tempItem)
            {
                UserEvaluateDetail eAns = null;
                eAns = new UserEvaluateDetail
                {
                    MainId = elem.MainId,
                    ItemId = elem.ItemId,
                    Rating = elem.Rating,
                    Comment = elem.Comment,
                    DataType = (int)EvaluateDataType.Final
                };
                EAnsCollection.Add(eAns);
                i++;
                if (i > 5) break; //取temp中最新一組5筆Data複製到final
            }

            if (EAnsCollection.Any())
            {
                try
                {
                    mp.UserEvaluateDetailSetList(EAnsCollection);
                }
                catch (Exception ex)
                {
                    throw new Exception("EAnsCollection 儲存失敗:" + ex.Message + ex.StackTrace);
                }
            }
            
        }
    }
}

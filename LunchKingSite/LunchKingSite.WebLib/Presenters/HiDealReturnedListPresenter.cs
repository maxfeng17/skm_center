﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealReturnedListPresenter : Presenter<IHiDealReturnedListView>
    {
        protected static IHiDealProvider hp;

        public HiDealReturnedListPresenter(IHiDealProvider hiDeal)
        {
            hp = hiDeal;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            View.ExportClicked += OnExportClicked;
            return true;
        }

        private void OnSearchClicked(object sender, DataEventArgs<HiDealReturnedLightListViewSearchRequest> e)
        {
            HiDealReturnedLightListViewSearchRequest data = e.Data;
            var sList = WhereClauseListGet(data);

            //檢查有無條件成立
            if (sList.Count == 0)
            {
                //沒有搜尋條件，顯示錯誤。
                View.ShowMessage("請至少設定一個條件。");
                return;
            }

            //查詢資料
            var cols = hp.ViewHiDealReturnedLightGetList(data.PageNum, data.PageSize, sList.ToArray(),
                                                      ViewHiDealReturnedLight.Columns.ApplicationTime + " desc ");
            var dataCount = hp.ViewHiDealReturnedLightDialogDataGetCount(sList.ToArray());

            //顯示資料
            View.ShowData(cols, dataCount);
        }

        private List<string> WhereClauseListGet(HiDealReturnedLightListViewSearchRequest data) 
        {
            var sList = new List<string>();
            //判斷起始日
            if ((data.ApplicationTimeStart != null) && (data.ApplicationTimeEnd != null))
            {
                sList.Add(ViewHiDealReturnedLight.Columns.ApplicationTime + " >= " + data.ApplicationTimeStart.Value.ToString("yyyy-MM-dd HH:mm:ss"));
                sList.Add(ViewHiDealReturnedLight.Columns.ApplicationTime + " < " + data.ApplicationTimeEnd.Value.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            if (!string.IsNullOrEmpty(View.FilterUser))
            {
                if (View.FilterType.Equals(ViewHiDealReturnedLight.Columns.OrderId))
                    sList.Add(ViewHiDealReturnedLight.Columns.OrderId + " = " + View.FilterUser);
                else if (View.FilterType.Equals(ViewHiDealReturnedLight.Columns.DealName))
                    sList.Add(ViewHiDealReturnedLight.Columns.DealName + " like " + View.FilterUser);
                else if (View.FilterType.Equals(ViewHiDealReturnedLight.Columns.Id))
                    sList.Add(ViewHiDealReturnedLight.Columns.Id + " = " + View.FilterUser);
                else if (View.FilterType.Equals(ViewHiDealReturnedLight.Columns.ProductId))
                {
                    sList.Add(ViewHiDealReturnedLight.Columns.ProductId + " = " + View.FilterUser);
                }
            }

            //退貨單狀態
            if (data.ReturnedStatus != null)
            {
                sList.Add(ViewHiDealReturnedLight.Columns.ReturnedStatus + " = " + (int)data.ReturnedStatus.Value);
            }

            return sList;
        }

        private void OnExportClicked(object sender, DataEventArgs<HiDealReturnedLightListViewSearchRequest> e) 
        {
            HiDealReturnedLightListViewSearchRequest data = e.Data;
            var sList = WhereClauseListGet(data);

            //檢查有無條件成立
            if (sList.Count == 0)
            {
                //沒有搜尋條件，顯示錯誤。
                View.ShowMessage("請至少設定一個條件。");
                return;
            }

            ViewHiDealReturnedLightCollection cols = hp.ViewHiDealReturnedLightGetList(0, 0, sList.ToArray(),
                                                      ViewHiDealReturnedLight.Columns.ApplicationTime + " desc ");

            View.Export(cols);
        }

        protected void SetFilterTypes()
        {
            View.SetFilterTypeDropDown(View.FilterTypes);
        }
    }
}

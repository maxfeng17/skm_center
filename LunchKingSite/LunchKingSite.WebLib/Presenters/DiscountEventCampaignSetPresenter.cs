﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class DiscountEventCampaignSetPresenter : Presenter<IDiscountEventCampaignSetView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            DiscountEvent item = op.DiscountEventGet(View.EventId);
            if (item.StartTime < DateTime.Now)
            {
                View.ShowMessage("活動已開始，不能再綁定折價券資料");
            }
            else
            {
                GetDiscountCampaighList(item);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            return true;
        }

        public DiscountEventCampaignSetPresenter(IOrderProvider odrProv)
        {
            op = odrProv;
        }

        #region event

        protected void OnSave(object sender, DataEventArgs<Dictionary<int, int>> e)
        {
            OrderFacade.SetDiscountEventCampaignList(View.EventId, e.Data);
        }

        #endregion

        #region Private Method

        private void GetDiscountCampaighList(DiscountEvent item)
        {
            View.SetDiscountCampaighList(op.ViewDiscountEventCampaignGetAddList(item.StartTime, item.EndTime));
        }

        #endregion
    }
}

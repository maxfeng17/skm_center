﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserDiscountListPresenter : Presenter<IUserDiscountListView>
    {
        private IMemberProvider mp;
        private IOrderProvider op;

        public UserDiscountListPresenter()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.UserName))
            {
                LoadData(1, View.DiscountStatus);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += OnPageChange;
            View.GetDiscountListCount += OnGetDiscountListCount;
            View.UseStatusChanged += OnUseStatusChanged;
            View.SortTypeChanged += OnSortTypeChanged;
            return true;
        }

        private void OnGetDiscountListCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.ViewDiscountDetailGetListCount(GetFilter(View.DiscountStatus));
        }

        protected void LoadData(int pageNumber, FilterStatus status)
        {
            List<string> list = OrderFacade.SendUncollectedDiscountList(View.UserId, View.UserName);
            Dictionary<ViewDiscountDetail, bool> dataList = new Dictionary<ViewDiscountDetail, bool>();
            ViewDiscountDetailCollection discounts = op.ViewDiscountDetailGetList(pageNumber, View.PageSize, GetOrderByString(View.Sort), GetFilter(status));
            foreach (ViewDiscountDetail d in discounts)
            {
                dataList.Add(d, list.Contains(d.Code));
            }
            View.SetDiscountList(dataList);
        }

        protected void OnPageChange(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data, View.DiscountStatus);
        }

        protected void OnUseStatusChanged(object sender, EventArgs e)
        {
            LoadData(1, View.DiscountStatus);
        }

        protected void OnSortTypeChanged(object sender, EventArgs e)
        {
            LoadData(1, View.DiscountStatus);
        }

        private string GetOrderByString(SortType sort)
        {
            switch (sort)
            {
                case SortType.AmountDesc:
                    return ViewDiscountDetail.Columns.Amount + " desc ";
                case SortType.AmountAsc:
                    return ViewDiscountDetail.Columns.Amount;
                case SortType.ExpireDesc:
                    return ViewDiscountDetail.Columns.EndTime;
                default:
                    return ViewDiscountDetail.Columns.SendDate + " desc ";
            }
        }

        private string[] GetFilter(FilterStatus status)
        {
            List<string> filter = new List<string>();
            
            // 擁有者
            filter.Add(ViewDiscountDetail.Columns.Owner + "=" + View.UserId);
            filter.Add(ViewDiscountDetail.Columns.StartTime + " < " + DateTime.Now);

            switch (status)
            {
                case FilterStatus.Unused:
                    filter.Add(ViewDiscountDetail.Columns.UseTime + " is null ");
                    filter.Add(ViewDiscountDetail.Columns.EndTime + ">" + DateTime.Now);
                    filter.Add(ViewDiscountDetail.Columns.CancelTime + " is null ");
                    break;
                case FilterStatus.Expire:
                    filter.Add(ViewDiscountDetail.Columns.UseTime + " is null ");
                    filter.Add(ViewDiscountDetail.Columns.EndTime + " <= " + DateTime.Now);
                    filter.Add(ViewDiscountDetail.Columns.EndTime + " > " + DateTime.Now.AddMonths(-1));
                    break;
                case FilterStatus.Used:
                    filter.Add(ViewDiscountDetail.Columns.UseTime + " is not null ");
                    filter.Add(ViewDiscountDetail.Columns.CancelTime + " is null ");
                    break;
                default:
                    break;
            }

            return filter.ToArray();
        }
    }
}
﻿using EnterpriseDT.Net.Ftp;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class EntrustSellReceiptListPresenter : Presenter<IEntrustSellReceiptListView>
    {
        private static ILog logger = LogManager.GetLogger(typeof(EntrustSellReceiptListPresenter));

        #region members

        private ISysConfProvider cfg = null;
        private IOrderProvider op = null;

        #endregion members

        public EntrustSellReceiptListPresenter()
        {
            cfg = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            List<ViewEntrustSellReceipt> receipts = LoadData(
                View.FilterColumn, View.FilterValue, View.FilterVerifiedStartTime, View.FilterVerifiedEndTime,
                View.FilterIsPhysical, View.FilterIsExported, View.FilterHasReceiptCode);
            View.SetContent(receipts);
            return true;
        }

        /// <summary>
        /// isPhysical, isExported, hasReceiptCode 這3個checkbox的過瀘條件，
        /// 當畫面沒勾選時，是不要過瀘，而不是過瀘值為false，詳見相關企畫
        /// </summary>
        /// <param name="columnName"></param>
        /// <param name="columnValue"></param>
        /// <param name="verifiedStartTime"></param>
        /// <param name="verifiedEndTime"></param>
        /// <param name="isPhysical"></param>
        /// <param name="isExported"></param>
        /// <param name="hasReceiptCode"></param>
        /// <returns></returns>
        private List<ViewEntrustSellReceipt> LoadData(string columnName, string columnValue, DateTime? verifiedStartTime,
            DateTime? verifiedEndTime, bool? isPhysical, bool? isExported, bool? hasReceiptCode)
        {
            if (verifiedEndTime != null)
            {
                verifiedEndTime = Helper.GetFinalTime(verifiedEndTime.Value);
            }
            return op.ViewEntrustSellReceiptList(columnName, columnValue, verifiedStartTime, verifiedEndTime,
                isPhysical, isExported, hasReceiptCode);
        }

        private List<ViewEntrustSellReceipt> GetExportableData()
        {
            return op.ViewEntrustSellReceiptExportableList();
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ExportClicked += OnExportClicked;
            View.SearchClicked += OnSearchClicked;
            return true;
        }

        protected void OnExportClicked(object sender, EventArgs e)
        {
            try
            {
                List<ViewEntrustSellReceipt> exportableReceipts = GetExportableData();
                if (exportableReceipts.Count > 0)
                {
                    byte[] binData = GenerateExportData(exportableReceipts);
                    SendToCowellFtp(binData);
                    SetDataWasExported(exportableReceipts);
                    View.SetMessage(string.Format("同步完成，同步 {0} 筆", exportableReceipts.Count));
                }
                else
                {
                    View.SetMessage("沒有可同步的資料");
                }
            }
            catch (Exception ex)
            {
                View.SetMessage(ex.Message);
            }
            //重整資料
            List<ViewEntrustSellReceipt> receipts = LoadData(
                            View.FilterColumn, View.FilterValue, View.FilterVerifiedStartTime, View.FilterVerifiedEndTime,
                            View.FilterIsPhysical, View.FilterIsExported, View.FilterHasReceiptCode);
            View.SetContent(receipts);
        }

        private void SendToCowellFtp(byte[] binData)
        {
            string fileName = string.Format("EntrustSellReceipt_{0}.xml", DateTime.Now.ToString("yyyy-MM-dd_HHmmss"));
            FTPClient ftp = new FTPClient();
            ftp.Timeout = 10 * 1000; //10s
            try
            {
                ftp.RemoteHost = cfg.CowellFtpHost;
                ftp.ControlPort = 21;
                ftp.ConnectMode = FTPConnectMode.PASV;
                ftp.Connect();
                ftp.Login(cfg.CowellFtpUserName, cfg.CowellFtpPassword);
                if (ftp.Connected)
                {
                    ftp.ChDir(cfg.CowellFtpRemoteUploadFolder);
                    ftp.TransferType = FTPTransferType.BINARY;
                    ftp.Put(binData, fileName);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("同步到科威系統的FTP失敗, 訊息: " + ex.Message);
            }
            finally
            {
                ftp.Quit();
            }
        }

        private byte[] GenerateExportData(List<ViewEntrustSellReceipt> receipts)
        {
            //取得指定長度的big5字串, 太長截掉
            Func<string, int, string> GetBig5SubStr = (str, limit) =>
            {
                Encoding enc = Encoding.GetEncoding("big5", new EncoderExceptionFallback(), new DecoderReplacementFallback(""));
                byte[] strBuf = enc.GetBytes(str);
                if (limit < strBuf.Length)
                {
                    str = enc.GetString(enc.GetBytes(str), 0, limit);
                }
                return str;
            };

            Func<XDocument, string> GetXDomString = (xdoc) =>
            {
                return string.Format("{0}\r\n{1}", xdoc.Declaration, xdoc);
            };

            XElement root = new XElement("root");
            foreach (ViewEntrustSellReceipt receipt in receipts)
            {
                string exportOrderId = string.Format("{0}-{1}", receipt.OriginalOrderId.Replace("-", ""), receipt.Id);
                //24的限制，"據說"科威系統的長度從14調整到24
                if (exportOrderId.Length >= 24 || receipt.Id > 900000)
                {
                    logger.WarnFormat(
                        "可用收據編號己達{0}，接近臨界值，請檢查 EntrustSellReceiptListPresenter.cs", receipt.Id);
                }
                root.Add(new XElement("order",
                    new XElement("ord_no", exportOrderId),
                    new XElement("ord_dt", receipt.VerifiedTime.Value.ToString("yyyy/MM/dd")), //2(10)
                    new XElement("cust_no", receipt.MemberUid), //3(10)
                    new XElement("pax_clnm", GetBig5SubStr(receipt.MemberFirstName, 4)), //4(4)
                    new XElement("pax_cfnm", GetBig5SubStr(receipt.MemberLastName, 6)), //5(6)
                    new XElement("buyer_uno", receipt.ComId), //6(10)
                    new XElement("buyer_nm", receipt.BuyerName), //7(50)

                    new XElement("addr_va", receipt.BuyerAddress), //8
                    new XElement("ext_dr", ""), //9
                    new XElement("petord_no", receipt.ReceiptId.ToString("00000000")), //10
                    new XElement("prcvd_fg", ""), //11

                    new XElement("prod_ca", "Q"), //12
                    new XElement("prod_cd", receipt.ProductUid), //13(12)
                    //加前置詞的目的，是為了讓操作者在科威系統可以辨識出訂單是從17Life過去的
                    new XElement("prod_nm", "17LIFE" + GetBig5SubStr(receipt.ItemName, 24)), //14(30)
                    new XElement("itn_bdt", ""), //15
                    new XElement("itn_edt", ""), //16
                    new XElement("source_nm", ""), //17
                    new XElement("sale_qt", receipt.ItemQuantity), //18(3)
                    new XElement("form_pay", ""), //19
                    new XElement("period_qt", ""), //20
                    new XElement("sale_am", receipt.AmountReceived.ToString("0")), //21-12
                    new XElement("cost_am", receipt.AmountPaid.ToString("0")), //22
                    new XElement("cnfm_fg", "Y"), //23 是否為確認訂單
                    new XElement("ord_rk", ""), //24
                    new XElement("sply_ca", ""), //25
                    new XElement("sply_uno", ""), //26
                    new XElement("sply_fnm", ""), //27
                    new XElement("sply_anm", ""), //28
                    new XElement("sply_inv_no", ""), //29
                    new XElement("system_cd", "3") //30
                ));
            }
            XDocument doc = new XDocument(
                new XDeclaration("1.0", "big5", null),
                root
            );

            return Encoding.GetEncoding("big5").GetBytes(GetXDomString(doc));
        }

        private void SetDataWasExported(List<ViewEntrustSellReceipt> receipts)
        {
            op.EntrustSellReceiptsSetExported(receipts.Select(t => t.ReceiptId).ToArray());
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            List<ViewEntrustSellReceipt> receipts = LoadData(
                View.FilterColumn, View.FilterValue, View.FilterVerifiedStartTime, View.FilterVerifiedEndTime,
                View.FilterIsPhysical, View.FilterIsExported, View.FilterHasReceiptCode);
            View.SetContent(receipts);
        }
    }
}
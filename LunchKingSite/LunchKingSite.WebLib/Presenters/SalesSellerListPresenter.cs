﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Web;
using LunchKingSite.BizLogic.Model.SellerSales;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesSellerListPresenter : Presenter<ISalesSellerListView>
    {
        private ISellerProvider sp;
        private IHumanProvider hp;
        private int RowCount = 0;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrWhiteSpace(View.SignCompanyId))
            {
                LoadData(1);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            View.Export += OnExport;
            View.Import += OnImport;
            return true;
        }

        public SalesSellerListPresenter(ISellerProvider sellerProv, IHumanProvider humProv)
        {
            sp = sellerProv;
            hp = humProv;
        }

        #region event

        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            //e.Data = sp.SellerGetCount(View.SalesId,View.CrossDeptTeam, GetFilter());
            e.Data = sp.SellerGetCount(GetSearchCondition(), View.UserName, View.IsPrivate, View.IsPublic, View.EmpUserId, View.CrossDeptTeam, View.Dept, View.CityId, View.SellerLevelList, View.SellerPorperty, View.SellerManagLogNumber, View.SellerManagLogDatepart, View.RowCount, View.SalesId);
        }
        protected void OnExport(object sender, EventArgs e)
        {
            List<string> ExportSellerList = View.ExportSellerList.Split(',').ToList();

            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("批次轉換業務");

            sheet.SetColumnWidth(0, 3000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 6000);
            sheet.SetColumnWidth(3, 12000);
            sheet.SetColumnWidth(4, 2000);
            sheet.SetColumnWidth(5, 12000);
            sheet.SetColumnWidth(6, 12000);
            sheet.SetColumnWidth(7, 3000);


            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("編號");
            cols.CreateCell(1).SetCellValue("地區");
            cols.CreateCell(2).SetCellValue("規模");
            cols.CreateCell(3).SetCellValue("品牌名稱");
            cols.CreateCell(4).SetCellValue("組別");
            cols.CreateCell(5).SetCellValue("業務1");
            cols.CreateCell(6).SetCellValue("業務2");
            cols.CreateCell(7).SetCellValue("狀態");

            int row = 0;
            for (int i = 0; i < ExportSellerList.Count(); i++)
            {
                Guid SellerGuid = Guid.Empty;
                Guid.TryParse(ExportSellerList[i], out SellerGuid);

                Seller s = sp.SellerGet(SellerGuid);
                List<MultipleSalesModel> multipleSalesModel = SellerFacade.SellerSaleGetBySellerGuid(SellerGuid);
                int maxSalesGroup = multipleSalesModel.Count() == 0 ? 0 : multipleSalesModel.Max(x => x.SalesGroup);

                if (multipleSalesModel.Count()!=0)
                {
                    for (int j = 0; j < maxSalesGroup; j++)
                    {
                        if (s.IsLoaded && SellerGuid != Guid.Empty)
                        {
                            row++;
                            var model = multipleSalesModel.Where(x => x.SalesGroup == j + 1).FirstOrDefault();
                            cols = sheet.CreateRow(row);
                            cols.CreateCell(0).SetCellValue(s.SellerId);
                            cols.CreateCell(1).SetCellValue(CityManager.CityTownShopStringGet(s.CityId));
                            cols.CreateCell(2).SetCellValue((s.SellerLevel.HasValue ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerLevel)(s.SellerLevel))) : string.Empty));
                            cols.CreateCell(3).SetCellValue(s.SellerName);
                            cols.CreateCell(4).SetCellValue(j + 1);
                            cols.CreateCell(5).SetCellValue(hp.ViewEmployeeGet(ViewEmployee.Columns.Email, model.DevelopeSalesEmail).Email);
                            cols.CreateCell(6).SetCellValue(hp.ViewEmployeeGet(ViewEmployee.Columns.Email, model.OperationSalesEmail).Email);
                            cols.CreateCell(7).SetCellValue(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerTempStatus)(s.TempStatus))));
                            
                        }
                    }
                }
                else
                {
                    if (s.IsLoaded && SellerGuid != Guid.Empty)
                    {
                        row++;
                        cols = sheet.CreateRow(row);
                        cols.CreateCell(0).SetCellValue(s.SellerId);
                        cols.CreateCell(1).SetCellValue(CityManager.CityTownShopStringGet(s.CityId));
                        cols.CreateCell(2).SetCellValue((s.SellerLevel.HasValue ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerLevel)(s.SellerLevel))) : string.Empty));
                        cols.CreateCell(3).SetCellValue(s.SellerName);
                        cols.CreateCell(4).SetCellValue("");
                        cols.CreateCell(5).SetCellValue("");
                        cols.CreateCell(6).SetCellValue("");
                        cols.CreateCell(7).SetCellValue(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((SellerTempStatus)(s.TempStatus))));
                        
                    }
                }

                SellerFacade.SellerLogSet(s.Guid, "商家查詢資料批次匯出", View.UserName);

            }



            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(@"ExportSellerSales.xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);


            //直接產生檔案
            //FileStream file = new FileStream(@"d:\ProposalMultiDeals.xls", FileMode.CreateNew);
            //workbook.Write(file);
            //file.Close();

        }

        protected void OnImport(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.FUExport.PostedFile.FileName))
            {
                HSSFWorkbook workbook = new HSSFWorkbook(View.FUExport.PostedFile.InputStream);
                Sheet currentSheet = workbook.GetSheetAt(0);
                Row row;
                List<string> ImportError = new List<string>();
                int EmportCount = 0;
                List<Guid> resetSalesGroupSeller = new List<Guid>();
                for (int i = 1; i <= currentSheet.LastRowNum; i++)
                {
                    
                    row = currentSheet.GetRow(i);
                    if (row.GetCell(0) == null || string.IsNullOrEmpty(row.GetCell(0).ToString()) && (row.GetCell(1) == null || string.IsNullOrEmpty(row.GetCell(1).ToString())) && 
                        (row.GetCell(2) == null || string.IsNullOrEmpty(row.GetCell(2).ToString())) && (row.GetCell(3) == null || string.IsNullOrEmpty(row.GetCell(3).ToString())) &&
                        (row.GetCell(4) == null || string.IsNullOrEmpty(row.GetCell(4).ToString())) && (row.GetCell(5) == null || string.IsNullOrEmpty(row.GetCell(5).ToString())) &&
                        (row.GetCell(6) == null || string.IsNullOrEmpty(row.GetCell(6).ToString())) && (row.GetCell(7) == null || string.IsNullOrEmpty(row.GetCell(7).ToString())))
                    {
                        //空白行即結束
                        break;
                    }


                    string developeSales = "";
                    string operationSales = "";
                    string sellerId = "";
                    int salesGroup = 0;
                    //業務1,2
                    if (row.GetCell(5) == null)
                        developeSales = "";
                    else
                        developeSales = row.GetCell(5).ToString();

                    if (row.GetCell(6) == null)
                        operationSales = "";
                    else
                        operationSales = row.GetCell(6).ToString();

                    //編號
                    if (row.GetCell(0) == null)
                        sellerId = "";
                    else
                        sellerId = row.GetCell(0).ToString();

                    //組別
                    if (row.GetCell(4) == null)
                        salesGroup = 0;
                    if (string.IsNullOrEmpty(row.GetCell(4).ToString()))
                        salesGroup = 0;
                    else
                        salesGroup = Convert.ToInt32(row.GetCell(4).ToString());

                    //再優化
                    bool checkDe = false;
                    bool checkOP = false;
                    if (!string.IsNullOrEmpty(sellerId))
                    {
                        Seller s = sp.SellerGet(Seller.Columns.SellerId, sellerId);
                        if (string.IsNullOrEmpty(developeSales) && string.IsNullOrEmpty(operationSales))
                        {
                            //刪除
                            sp.SellerSaleDelete(s.Guid, salesGroup);
                            checkDe = true;
                            checkOP = true;
                            if (!resetSalesGroupSeller.Contains(s.Guid))
                                resetSalesGroupSeller.Add(s.Guid);
                            EmportCount++;
                            SellerFacade.SellerLogSet(s.Guid, "移除 開發/經營業務 " + salesGroup.ToString() , View.UserName);
                        }
                        else
                        {
                            string check = SellerFacade.CheckSales(3, developeSales, operationSales);
                            int maxNum = 0;
                            if (string.IsNullOrEmpty(check))
                            {
                                ViewEmployee developeEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, developeSales);
                                ViewEmployee OperationEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, operationSales);

                                List<Department> dept = HumanFacade.GetSalesDepartment();
                                if (developeEmp.IsLoaded)
                                {
                                    if (developeEmp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                                    {
                                        checkDe = true;

                                        //是否要匯入業務2
                                        bool isImportopSalesEmp = false;
                                        if (OperationEmp.IsLoaded)
                                        {
                                            if (OperationEmp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                                            {
                                                isImportopSalesEmp = true;
                                                checkOP = true;
                                            }
                                        }
                                        else if (string.IsNullOrEmpty(operationSales))
                                        {
                                            checkOP = true;
                                        }


                                        if (checkDe && checkOP)
                                        {
                                            //兩個業務正確才匯入
                                            SellerSale deSellerSales = sp.SellerSaleGetBySellerGuid(s.Guid, salesGroup, (int)SellerSalesType.Develope);
                                            if (deSellerSales.IsLoaded)
                                            {
                                                //更新
                                                deSellerSales.SellerSalesId = developeEmp.UserId;
                                                sp.SellerSaleSet(deSellerSales);
                                            }
                                            else
                                            {
                                                //新增
                                                maxNum = sp.SellerSaleGetBySellerGuid(s.Guid).Count() != 0 ? sp.SellerSaleGetBySellerGuid(s.Guid).Select(x => x.SalesGroup).Max() : 0;
                                                SellerSale ss = new SellerSale();
                                                ss.SellerGuid = s.Guid;
                                                ss.SellerSalesId = developeEmp.UserId;
                                                ss.SalesType = (int)SellerSalesType.Develope;
                                                ss.SalesGroup = maxNum + 1;
                                                ss.CreateId = View.UserName;
                                                ss.CreateTime = DateTime.Now;
                                                sp.SellerSaleSet(ss);
                                            }


                                            if (isImportopSalesEmp)
                                            {
                                                SellerSale opSellerSales = sp.SellerSaleGetBySellerGuid(s.Guid, salesGroup, (int)SellerSalesType.Operation);
                                                if (opSellerSales.IsLoaded)
                                                {
                                                    //更新
                                                    opSellerSales.SellerSalesId = OperationEmp.UserId;
                                                    sp.SellerSaleSet(opSellerSales);
                                                }
                                                else
                                                {
                                                    //新增
                                                    SellerSale ss = new SellerSale();
                                                    ss.SellerGuid = s.Guid;
                                                    ss.SellerSalesId = OperationEmp.UserId;
                                                    ss.SalesType = (int)SellerSalesType.Operation;
                                                    if (deSellerSales.IsLoaded)
                                                        ss.SalesGroup = salesGroup;//僅缺業務2，不用完全新增
                                                    else
                                                        ss.SalesGroup = maxNum + 1;
                                                    ss.CreateId = View.UserName;
                                                    ss.CreateTime = DateTime.Now;
                                                    sp.SellerSaleSet(ss);
                                                }
                                            }
                                            else
                                            {
                                                //業務2非必填
                                                sp.SellerSaleDelete(s.Guid, salesGroup, (int)SellerSalesType.Operation);
                                            }

                                            EmportCount++;
                                            SellerFacade.SellerLogSet(s.Guid, "商家批次變更 開發業務" + salesGroup.ToString() + " 為 " + developeEmp.EmpName, View.UserName);
                                            SellerFacade.SellerLogSet(s.Guid, "商家批次變更 經營業務" + salesGroup.ToString() + " 為 " + (OperationEmp.EmpName ?? "null"), View.UserName);
                                        }
                                    }
                                    else
                                    {
                                        ImportError.Add("第" + i.ToString() + "列");
                                    }
                                }
                                else
                                {
                                    ImportError.Add("第" + i.ToString() + "列");
                                }
                            }
                            else
                                ImportError.Add("第" + i.ToString() + "列");
                        }
                    }
                    else
                        ImportError.Add("第" + i.ToString() + "列");
      

                }
                if (ImportError.Count() > 0)
                    View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆\\n失敗筆數：" + ImportError.Count().ToString() + "筆\\n失敗清單：" + string.Join("、", ImportError));
                else
                    View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆");


                //重整組別
                SellerFacade.ResetSalesGroup(resetSalesGroupSeller);
            }
            else
            {
                View.ShowMessage("請選擇匯入檔案");
            }

        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        private Seller GetSearchCondition()
        {
            Seller condition = new Seller()
            {
                TempStatus = -1
            };
            if (!string.IsNullOrWhiteSpace(View.SellerName))
            {
                condition.SellerName = View.SellerName;
            }
            if (!string.IsNullOrWhiteSpace(View.BossName))
            {
                condition.SellerBossName = View.BossName;
            }
            if (!string.IsNullOrWhiteSpace(View.CompanyName))
            {
                condition.CompanyName = View.CompanyName;
            }
            if (!string.IsNullOrWhiteSpace(View.SignCompanyId))
            {
                condition.SignCompanyID = View.SignCompanyId;
            }
            if (View.TempStaus != null)
            {
                condition.TempStatus = (int)View.TempStaus;
            }

            return condition;
        }

        #endregion

        #region Private Method

        private void LoadData(int pageNumber)
        {
            //SellerCollection sellers = sp.SellerGetList(pageNumber, View.PageSize, Seller.Schema.TableName + "." + Seller.Columns.CreateTime + " desc ", View.SalesId, View.CrossDeptTeam, GetFilter());
            //View.SetSellerList(sellers);

            
            List<Seller> sellerList = sp.SellerGetList(pageNumber, View.PageSize, Seller.Columns.CreateTime, GetSearchCondition(),
                View.UserName, View.IsPrivate, View.IsPublic, View.EmpUserId, View.CrossDeptTeam, View.Dept, View.CityId, 
                View.SellerLevelList, View.SellerPorperty, View.SellerManagLogNumber, View.SellerManagLogDatepart,View.SalesId,
                ref RowCount);

            View.RowCount = RowCount;
            sellerList = sellerList.OrderByDescending(x => x.CreateTime).ToList();

            View.SetSellerList(sellerList);
        }

        #endregion
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class LionTravelOrderListPresenter : Presenter<ILionTravelOrderListView>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += this.OnPageChange;
            View.Search += OnSearch;
            return true;
        }

        private void OnSearch(object sender, EventArgs e)
        {
            GetCouponList(1, true);
        }

        private void OnPageChange(object sender, DataEventArgs<int> e)
        {
            GetCouponList(e.Data, false);
        }

        private void GetCouponList(int page, bool recount)
        {
            ViewLiontravelCouponListMainCollection data = op.ViewLionTravelCouponListMainGetList(page, 15, GetFilter());
            Dictionary<Guid, int> is_partialrefund_list = new Dictionary<Guid, int>();
            foreach (var item in data.Select(x => x.Guid).Distinct().ToList())
            {
                CashTrustLogCollection cash_trust_logs = mp.CashTrustLogGetListByOrderGuid(item, OrderClassification.LkSite);
                int is_partial_count = cash_trust_logs.Where(x => (x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded) && !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Count();
                is_partialrefund_list.Add(item, is_partial_count);
            }
            if (recount)
            {
                View.PageCount = op.ViewLionTravelCouponListMainGetCount(GetFilter());
                View.SetUpPageCount();
            }
            View.GetCouponList(data, is_partialrefund_list);
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>() { ViewLiontravelCouponListMain.Columns.Type + "=" + (int)View.OrderClassType };
            if (!string.IsNullOrEmpty(View.FilterData))
            {
                LionTravelOrderFilterType type;
                Enum.TryParse<LionTravelOrderFilterType>(View.FilterType.ToString(), out type);
                switch (type)
                {
                    case LionTravelOrderFilterType.LionTravelOrderId:
                        filter.Add(ViewLiontravelCouponListMain.Columns.RelatedOrderId + "=" + View.FilterData + "");
                        break;
                    case LionTravelOrderFilterType.OrderId:
                        filter.Add(ViewLiontravelCouponListMain.Columns.OrderId + "=" + View.FilterData + "");
                        break;
                    case LionTravelOrderFilterType.Mobile:
                        filter.Add(ViewLiontravelCouponListMain.Columns.Mobile + "=" + View.FilterData + "");
                        break;
                    case LionTravelOrderFilterType.OrderGuid:
                        filter.Add(ViewLiontravelCouponListMain.Columns.Guid + "=" + View.FilterData + "");
                        break;
                }
            }
            if (View.FilterDeliveryType > 0)
            {
                filter.Add(ViewLiontravelCouponListMain.Columns.DeliveryType + "=" + View.FilterDeliveryType);
            }
            if (View.StartDate.HasValue && View.EndDate.HasValue)
            {
                filter.Add(ViewLiontravelCouponListMain.Columns.CreateTime + ">=" + View.StartDate.Value.ToString("yyyy-MM-dd"));
                filter.Add(ViewLiontravelCouponListMain.Columns.CreateTime + "<" + View.EndDate.Value.AddDays(1).ToString("yyyy-MM-dd"));
            }
            return filter.ToArray();
        }
    }
}

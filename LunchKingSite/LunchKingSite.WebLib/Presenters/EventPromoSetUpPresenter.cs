﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventPromoSetUpPresenter : Presenter<IEventPromoSetUp>
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private ICmsProvider cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();

        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetEventPromoList();
            SetupBindingCategories();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveEventPromo += OnSaveEventPromo;
            View.ChangeMode += OnChangeMode;
            View.GetEventPromo += OnGetEventPromo;
            View.UpdateEventPromoStatus += OnUpdateEventPromoStatus;
            View.GetEventPromoItemList += OnGetEventPromoItemList;
            View.UpdateEventPromoItemStatus += OnUpdateEventPromoItemStatus;
            View.GetPponDeal += OnGetPponDeal;
            View.SaveEventPromoItem += OnSaveEventPromoItem;
            View.GetEventPromoItem += OnGetEventPromoItem;
            View.WeChatSendTestMessage += OnWeChatSendTestMessage;
            View.GetEventPromoList += OnGetEventPromoList;
            View.SetEventPromoShowInApp += OnSetEventPromoShowInApp;
            View.SetEventPromoShowInWeb += OnSetEventPromoShowInWeb;
            View.DeleteEventPromoItem += OnDeleteEventPromoItem;
            View.ImportBidList += OnImportBidList;
            View.UpdateEventPromoItemSeq += OnUpdateEventPromoItemSeq;
            View.GetEventPromoItemStatus += OnGetEventPromoItemStatus;
            return true;
        }

        public void OnGetEventPromoList(object sender, EventArgs e)
        {
            GetEventPromoList();
        }

        public void OnWeChatSendTestMessage(object sender, DataEventArgs<string> e)
        {
            string return_message = string.Empty;
            EventPromo promo = pp.GetEventPromo(View.EventId);
            if (promo.Type == (int)EventPromoType.WeChatPromoArticle)
            {
                return_message = PromotionFacade.PostWeChatArticleMessage(e.Data, pp.GetEventPromoItemList(View.EventId).Take(9));
            }
            else if (promo.Type == (int)EventPromoType.WeChatPromoMessage)
            {
                return_message = PromotionFacade.PostWeChatTextMessage(e.Data, promo.Description);
            }
            View.ChangePanel(0, return_message);
        }

        public void OnSetEventPromoShowInApp(object sender, DataEventArgs<bool> e)
        {
            EventPromo eventPromo = pp.GetEventPromo(View.EventId);
            if (eventPromo.IsLoaded)
            {
                eventPromo.ShowInApp = e.Data;
                MarketingFacade.SaveCurationAndResetMemoryCache(eventPromo);
            }
            GetEventPromoList();
            View.ChangePanel(0);
        }
        public void OnSetEventPromoShowInWeb(object sender, DataEventArgs<bool> e)
        {
            EventPromo eventPromo = pp.GetEventPromo(View.EventId);
            if (eventPromo.IsLoaded)
            {
                eventPromo.ShowInWeb = e.Data;
                MarketingFacade.SaveCurationAndResetMemoryCache(eventPromo);
            }
            GetEventPromoList();
            View.ChangePanel(0);
        }

        public void OnDeleteEventPromoItem(object sender, DataEventArgs<int> e)
        {
            DeleteEventPromoItem(e.Data);
            OnGetEventPromoItemList(sender, e);
        }

        public void OnImportBidList(object sender, DataEventArgs<List<Tuple<string, string, string>>> e)
        {
            string alertMsg = string.Empty;
            EventPromoItemCollection itemCollection = ValidateAndGetItemListAndAlertMsg(e.Data, out alertMsg);

            if (!string.IsNullOrWhiteSpace(alertMsg))
            {
                View.AlertImportErrorMsg(alertMsg);
            }
            else
            {
                if (itemCollection.Count > 0)
                {
                    pp.SaveEventPromoItemList(itemCollection);
                }
            }
            
            GetEventPromoItemList();
            View.ChangePanel(3);
        }

        public void OnUpdateEventPromoItemSeq(object sender, DataEventArgs<KeyValuePair<int, Dictionary<int, int>>> e){
            int mainId = e.Data.Key;
            Dictionary<int, int> dictionaryIdAndSeq = e.Data.Value;
            UpdateEventPromoItemSeq(mainId, dictionaryIdAndSeq);
        }

        public string OnGetEventPromoItemStatus(ViewEventPromoItem item)
        {
            DateTime startDate= item.BusinessHourOrderTimeS;
            DateTime endDate = item.BusinessHourOrderTimeE;
            return GetEventPromoItemStatus(item, startDate, endDate);
        }

        public void OnGetEventPromoItem(object sender, EventArgs e)
        {
            EventPromoItem item = pp.GetEventPromoItem(View.ItemId);
            var dateStart = new DateTime();
            var linkItemDateStart = new DateTime();
            switch ((EventPromoItemType)item.ItemType)
            {
                case EventPromoItemType.Ppon:
                case EventPromoItemType.PicVote:
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(item.ItemId);
                    dateStart = deal.BusinessHourOrderTimeS;
                    break;
                case EventPromoItemType.Vourcher:
                    VourcherEvent vourcher = ep.VourcherEventGetById(item.ItemId);
                    if (vourcher.StartDate.HasValue)
                        dateStart = vourcher.StartDate.Value;
                    break;
                default:
                    break;
            }
            EventPromo promo = pp.GetEventPromo(View.EventId);

            //get link item
            var linkItem = (item.LinkVourcherId != 0) ? pp.GetEventPromoItem(item.LinkVourcherId) : new EventPromoItem();
            if (linkItem.IsLoaded)
            {
                VourcherEvent vourcher = ep.VourcherEventGetById(linkItem.ItemId);
                if (vourcher.StartDate.HasValue)
                    linkItemDateStart = vourcher.StartDate.Value;
            }
            View.SetEventPromoItem(item, linkItem, dateStart, linkItemDateStart, (EventPromoTemplateType)promo.TemplateType);
        }

        private EventPromoItem GetEventPromoItemById(int itemId, EventPromoItem updateItem)
        {
            var item = pp.GetEventPromoItem(itemId);
            item.ItemId = updateItem.ItemId;
            item.Title = updateItem.Title;
            item.Description = updateItem.Description;
            item.Seq = updateItem.Seq;
            item.Message += ("modified by " + View.UserName + " " +
                                       DateTime.Now.ToString("yyyy/MM/dd"));
            item.Category = updateItem.Category;
            item.ItemType = updateItem.ItemType;
            item.ItemUrl = updateItem.ItemUrl;
            item.ItemPicUrl = updateItem.ItemPicUrl;
            item.SubCategory = updateItem.SubCategory;
            item.LinkVourcherId = updateItem.LinkVourcherId;
            return item;
        }

        public void OnSaveEventPromoItem(object sender, DataEventArgs<EventPromoItemUpdateMode> e)
        {
            if (e.Data.PromoItems == null || e.Data.PromoItems.Count == 0) return;
            var item = e.Data.PromoItems[0];
            item.MainId = View.EventId;
            item.LinkVourcherId = 0;
            pp.SaveEventPromoItem(item.Id != 0 ? GetEventPromoItemById(View.ItemId, item) : item);

            if (e.Data.VotePic != null)
            {
                item = pp.GetEventPromoItem(item.Id);
                item.ItemPicUrl = UploadPromoItemImageFile(e.Data.VotePic, UploadFileType.EventPromoItemPic, item);
                pp.SaveEventPromoItem(item);
            }

            GetEventPromoItemList();
            View.ChangePanel(3);
        }

        public void OnGetPponDeal(object sender, DataEventArgs<KeyValuePair<int, EventPromoItemType>> e)
        {
            DateTime start = new DateTime();
            switch ((EventPromoItemType)e.Data.Value)
            {
                case EventPromoItemType.Ppon:
                case EventPromoItemType.PicVote:
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(e.Data.Key);
                    if (deal != null && deal.IsLoaded)
                    {
                        View.ItemTitle = deal.ItemName;
                        View.ItemDescription = deal.EventTitle;
                        start = deal.BusinessHourOrderTimeS;
                    }
                    break;
                case EventPromoItemType.Vourcher:
                    VourcherEvent vourcher = ep.VourcherEventGetById(e.Data.Key);
                    if (vourcher.IsLoaded && vourcher.StartDate.HasValue)
                    {
                        Seller seller = sp.SellerGet(vourcher.SellerGuid);
                        View.ItemTitle = seller.SellerName;
                        View.ItemDescription = vourcher.Contents;
                        start = vourcher.StartDate.Value;
                    }
                    break;
                default:
                    break;
            }
            View.SetEventPromoItem(start);
        }

        public void OnUpdateEventPromoItemStatus(object sender, EventArgs e)
        {
            pp.UpdateEventPromoItemStatus(View.ItemId);
            GetEventPromoItemList();
            View.ChangePanel(3);
        }

        public void OnGetEventPromoItemList(object sender, EventArgs e)
        {
            GetEventPromoItemList();
            View.EventUseDiscount = PromotionFacade.DiscountEventIdGetList(View.EventId).Count > 0;
            View.ChangePanel(3);
        }

        public void OnUpdateEventPromoStatus(object sender, EventArgs e)
        {
            pp.UpdateEventPromoStatus(View.EventId);
            GetEventPromoList();
            View.ChangePanel(0);
        }

        public void OnGetEventPromo(object sender, EventArgs e)
        {
            EventPromo promo = pp.GetEventPromo(View.EventId);
            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.EventPromo, promo.Id);
            if (View.IsCurationFunction)
            {
                View.SetEventPromoCities(GetPponCities(), cmp.ViewCmsRandomGetCollectionByEventPromoId(View.EventId));
            }
            View.SetEventPromo(seo,promo, (pp.EventPromoItemGetListCount(promo.Id) == 0) ? true : false);
        }

        public void OnChangeMode(object sender, EventArgs e)
        {
            View.ChangePanel(View.Mode);
            if (View.IsCurationFunction && View.Mode == 1) //add curation mode
            {
                View.SetEventPromoCities(GetPponCities(), new ViewCmsRandomCollection());
            }
        }

        public void OnSaveEventPromo(object sender, DataEventArgs<EventPromoUpdateModel> e)
        {
            EventPromo promo;
            if (e.Data.MainEvent.Id != 0)
            {
                promo = pp.GetEventPromo(e.Data.MainEvent.Id);
                promo.Title = e.Data.MainEvent.Title;
                promo.EventPromoTitle = e.Data.MainEvent.EventPromoTitle;
                promo.Url = e.Data.MainEvent.Url;
                promo.Cpa = e.Data.MainEvent.Cpa;
                promo.StartDate = e.Data.MainEvent.StartDate;
                promo.EndDate = e.Data.MainEvent.EndDate;
                promo.MainPic = e.Data.MainEvent.MainPic;
                promo.MobileMainPic = e.Data.MainEvent.MobileMainPic;
                promo.EventIntroduction = e.Data.MainEvent.EventIntroduction;
                promo.BgPic = e.Data.MainEvent.BgPic;
                promo.BgColor = e.Data.MainEvent.BgColor;
                promo.Description = e.Data.MainEvent.Description;
                promo.BtnFontColor = e.Data.MainEvent.BtnFontColor;
                promo.BtnFontColorHover = e.Data.MainEvent.BtnFontColorHover;
                promo.BtnFontColorActive = e.Data.MainEvent.BtnFontColorActive;
                promo.BtnActive = e.Data.MainEvent.BtnActive;
                promo.BtnHover = e.Data.MainEvent.BtnHover;
                promo.BtnOriginal = e.Data.MainEvent.BtnOriginal;
                promo.Message += ("modified by " + View.UserName + " " + DateTime.Now.ToString("yyyy/MM/dd hh:mm"));
                promo.TemplateType = e.Data.MainEvent.TemplateType;
                promo.BindCategoryList = e.Data.MainEvent.BindCategoryList;
                promo.DealPromoTitle = e.Data.MainEvent.DealPromoTitle;
                promo.DealPromoDescription = e.Data.MainEvent.DealPromoDescription;
                promo.SubCategoryBgColor = e.Data.MainEvent.SubCategoryBgColor;
                promo.SubCategoryShowImage = e.Data.MainEvent.SubCategoryShowImage;
                promo.SubCategoryFontColor = e.Data.MainEvent.SubCategoryFontColor;
                promo.SubCategoryBtnColorOriginal = e.Data.MainEvent.SubCategoryBtnColorOriginal;
                promo.SubCategoryBtnColorActive = e.Data.MainEvent.SubCategoryBtnColorActive;
                promo.SubCategoryBtnColorHover = e.Data.MainEvent.SubCategoryBtnColorHover;
                promo.SubCategoryBtnImageOriginal = e.Data.MainEvent.SubCategoryBtnImageOriginal;
                promo.SubCategoryBtnImageActive = e.Data.MainEvent.SubCategoryBtnImageActive;
                promo.SubCategoryBtnImageHover = e.Data.MainEvent.SubCategoryBtnImageHover;
                promo.SubCategoryBtnFontOriginal = e.Data.MainEvent.SubCategoryBtnFontOriginal;
                promo.SubCategoryBtnFontActive = e.Data.MainEvent.SubCategoryBtnFontActive;
                promo.SubCategoryBtnFontHover = e.Data.MainEvent.SubCategoryBtnFontHover;
                promo.MaxVotes = e.Data.MainEvent.MaxVotes;
                promo.VoteCycleType = e.Data.MainEvent.VoteCycleType;
                promo.CampaignId = e.Data.MainEvent.CampaignId;
                promo.EventType = e.Data.MainEvent.EventType;
                MarketingFacade.SaveCurationAndResetMemoryCache(promo);

                //若為策展，需調整策展連結和標題顯示文字
                if (promo.EventType == (int)EventPromoEventType.Curation)
                {
                    CmsRandomContent content = cmp.CmsRandomContentGetByEventPromoId(promo.Id);
                    if (content.IsLoaded)
                    {
                        string url = string.Format("{0}/Event/" + GetPageName((EventPromoTemplateType)promo.TemplateType) + ".aspx?u={1}&cpa={2}", config.SiteUrl, promo.Url, "17_Curation");
                        content.Title = promo.Title;
                        content.Body = string.Format("<a href='{0}'>{1}</a>", url, promo.Title);
                        cmp.CmsRandomContentSet(content);
                    }
                }
            }
            else
            {
                MarketingFacade.SaveCurationAndResetMemoryCache(e.Data.MainEvent);

                promo = pp.GetEventPromo(e.Data.MainEvent.Id);

                if (View.IsCurationFunction)
                {
                    #region 若為策展，自動建立 Curation Banner

                    CmsRandomContent content = new CmsRandomContent();
                    content.Title = promo.Title;
                    content.ContentTitle = "Curation Banner";
                    content.ContentName = "curation";
                    string url = string.Format("{0}/Event/" + GetPageName((EventPromoTemplateType)promo.TemplateType) + ".aspx?u={1}&cpa={2}", config.SiteUrl, promo.Url, "17_Curation");
                    content.Body = string.Format("<a href='{0}'>{1}</a>", url, promo.Title);
                    content.Locale = "zh_TW";
                    content.Status = true;
                    content.CreatedOn = DateTime.Now;
                    content.CreatedBy = View.UserName;
                    content.ModifiedOn = DateTime.Now;
                    content.ModifiedBy = View.UserName;
                    content.Type = (int)RandomCmsType.PponMasterPage;
                    content.EventPromoId = promo.Id;
                    cmp.CmsRandomContentSet(content);

                    #endregion
                }
            }
            if (e.Data.UpdateCmsRandomCities != null && e.Data.UpdateCmsRandomCities.Update)
            {
                CmsRandomContent content = cmp.CmsRandomContentGetByEventPromoId(promo.Id);
                if (content.IsLoaded)
                {
                    foreach (var vcr in cmp.ViewCmsRandomGetCollectionByEventPromoId(promo.Id))
                    {
                        cmp.CmsRandomCityDelete(vcr.Id);
                    }
                    foreach (var cityId in e.Data.UpdateCmsRandomCities.CityIds)
                    {
                        CmsRandomCity city = new CmsRandomCity();
                        city.Pid = content.Id;
                        city.StartTime = promo.StartDate;
                        city.EndTime = promo.EndDate;
                        city.Status = true;
                        city.CreatedBy = View.UserName;
                        city.CreatedOn = DateTime.Now;
                        city.Ratio = 1;
                        city.CityId = cityId;
                        city.Type = (int)RandomCmsType.PponMasterPage;
                        city.CityName = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId).CityName;
                        cmp.CmsRandomCitySet(city);
                    }
                }
            }
            if (e.Data.AppBannerPostedFile != null || e.Data.AppPromoPostedFile != null || e.Data.DealPromoImagePostedFile != null)
            {
                if (e.Data.AppBannerPostedFile != null)
                {
                    string imagePath = UploadImageFile(e.Data.AppBannerPostedFile, UploadFileType.EventPromoAppBanner, promo);
                    promo.AppBannerImage = imagePath;
                }

                if (e.Data.AppPromoPostedFile!=null)
                {
                    string imagePath = UploadImageFile(e.Data.AppPromoPostedFile, UploadFileType.EventPromoAppMainImage , promo);
                    promo.AppPromoImage = imagePath;
                }

                if (e.Data.DealPromoImagePostedFile != null) 
                {
                    string imagePath = UploadImageFile(e.Data.DealPromoImagePostedFile, UploadFileType.DealPromoImage, promo);
                    promo.DealPromoImage = imagePath;
                }
                
                MarketingFacade.SaveCurationAndResetMemoryCache(promo);
            }

            #region 策展Banner顯示位置
            int iBannerType = 0;
            if (promo.AppPromoImage != null)
            {
                iBannerType += (int)EventBannerType.MainUp;
            }
            if (promo.AppBannerImage != null)
            {
                foreach (int i in e.Data.BannerType)
                {
                    iBannerType += i;
                }
            }

            if (iBannerType > 0)
            {
                promo.BannerType = iBannerType;
                MarketingFacade.SaveCurationAndResetMemoryCache(promo);
            }
            #endregion

            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.EventPromo, promo.Id);
            if (string.IsNullOrEmpty(e.Data.SeoKeyword.Description) && string.IsNullOrEmpty(e.Data.SeoKeyword.Keyword))
            {
                PromotionFacade.DeletePromoSeoKeyword(seo);
            }
            else
            {

                if (seo.Id == 0)
                {
                    seo.CreateId = e.Data.SeoKeyword.CreateId;
                    seo.CreateTime = DateTime.Now;
                }
                seo.PromoType = (int)PromoSEOType.EventPromo;
                seo.ActivityId = promo.Id;
                seo.ModifyId = e.Data.SeoKeyword.ModifyId;
                seo.ModifyTime = DateTime.Now;
                seo.Description = e.Data.SeoKeyword.Description;
                seo.Keyword = e.Data.SeoKeyword.Keyword;

                PromotionFacade.SavePromoSeoKeyword(seo);
            }

            GetEventPromoList();
            View.ChangePanel(0);
        }

        protected void GetEventPromoList()
        {
            EventPromoCollection promos;
            if (View.EventType == EventPromoType.Ppon)
            {
                //若僅有策展權限，則只能看到策展活動；若還有商品主題活動瀏覽權限，則可看到全部活動
                string eventTypeFilter = View.IsCurationFunction ? (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read) ? string.Empty : EventPromo.Columns.EventType + "=" + (int)EventPromoEventType.Curation) : string.Empty;
                if (View.IsLimitInThreeMonth)
                {
                    promos = pp.EventPromoGetList(EventPromo.Columns.Type + "=" + (int)EventPromoType.Ppon, EventPromo.Columns.StartDate + ">=" + DateTime.Now.AddMonths(-3), eventTypeFilter);
                }
                else
                {
                    promos = pp.EventPromoGetList(EventPromo.Columns.Type + "=" + (int)EventPromoType.Ppon, eventTypeFilter);
                }
            }
            else
            {
                if (View.IsLimitInThreeMonth)
                {
                    promos = pp.EventPromoGetList(EventPromo.Columns.Type + " in (" + (int)EventPromoType.WeChatPromoMessage + "," + (int)EventPromoType.WeChatPromoArticle + ")", EventPromo.Columns.StartDate + ">=" + DateTime.Now.AddMonths(-3));
                }
                else
                {
                    promos = pp.EventPromoGetList(EventPromo.Columns.Type + " in (" + (int)EventPromoType.WeChatPromoMessage + "," + (int)EventPromoType.WeChatPromoArticle + ")");
                }
            }
            View.SetEventPromoList(promos);
        }
        protected void GetEventPromoItemList()
        {
            // p好康
            if (View.EventType == EventPromoType.Ppon)
            {
                var items = pp.GetViewEventPromoItemList(View.EventId, EventPromoItemType.Ppon);
                if (config.EnableGrossMarginRestrictions)
                {
                    foreach (var vepctmp in items)
                    {
                        vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.DisplayGrossMargin;
                        if ((op.GetHourStatusBybid(vepctmp.BusinessHourGuid).BusinessHourStatus &
                             (int) BusinessHourStatus.LowGrossMarginAllowedDiscount) > 0)
                        {
                            vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.UseDiscount;
                        }
                        vepctmp.GrossMargin =
                            ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(vepctmp.BusinessHourGuid)
                                .BaseGrossMargin;
                        vepctmp.GrossMargin = Math.Round(vepctmp.GrossMargin * 100, 2, MidpointRounding.AwayFromZero);
                        if (vepctmp.GrossMargin <= Convert.ToDecimal(config.GrossMargin))
                        {
                            vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.RedWord;
                        }

                        IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vepctmp.BusinessHourGuid);
                        if (deal != null && deal.OrderedTotal != null)
                        {
                            vepctmp.OrderedTotal = Convert.ToDecimal(deal.OrderedTotal);
                        }
                    }
                }
                View.SetEventPromoItemList(items);

                // 優惠券
                Dictionary<EventPromoItem, DateTime> dataList = new Dictionary<EventPromoItem, DateTime>();
                EventPromoItemCollection promoItems = pp.GetEventPromoItemList(View.EventId, EventPromoItemType.Vourcher);
                foreach (var item in promoItems)
                {
                    VourcherEvent vourcher = ep.VourcherEventGetById(item.ItemId);
                    if (vourcher.IsLoaded && vourcher.StartDate.HasValue)
                    {
                        dataList.Add(item, vourcher.StartDate.Value);
                    }
                }
                View.SetVourcherEventPromoItemList(dataList);

                //圖片投票
                var voteItems = pp.ViewEventPromoItemPicVoteGet(View.EventId);
                View.SetVotePicEventPromoItemList(voteItems);
            }
            else
            {
                EventPromoItemCollection items = pp.GetEventPromoItemList(View.EventId);
                View.SeTEventPromoItemList(items);
            }
        }
        protected void DeleteEventPromoItem(int id)
        {
            EventPromoItem item = pp.GetEventPromoItem(id);
            if (item != null)
            {
                pp.DeleteEventPromoItem(item);
            }
        }
        protected void UpdateEventPromoItemSeq(int mainId, Dictionary<int, int> dictionaryIdAndSeq) 
        {
            EventPromoItemCollection eventPromoItems = pp.GetEventPromoItemList(mainId);
            
            foreach (EventPromoItem item in eventPromoItems)
	        {
                if (dictionaryIdAndSeq.Keys.Contains(item.Id))
	            {
                    int newSeq;
                    dictionaryIdAndSeq.TryGetValue(item.Id, out newSeq);
                    item.Seq = newSeq;
	            }
	        }

            pp.SaveEventPromoItemList(eventPromoItems);
            OnGetEventPromoItemList(this, new EventArgs());
        }

        private void SetupBindingCategories()
        {
            if (!string.IsNullOrEmpty(cp.EventPromoBindCategoryList)) 
            {
                CategoryCollection categories = sp.CategoryGetList(cp.EventPromoBindCategoryList.Split(',').Select(int.Parse).ToList());
                View.SetBindingCategory(categories);
            }
        }

        private string UploadImageFile(HttpPostedFile uploadedFile, UploadFileType fileType , EventPromo promo)
        {
            if (uploadedFile != null)
            {
                string fileName = string.Format("{0}_{1}_{2}", promo.Id, fileType.ToString(), GetTimeStampHashCode());

                string fileNameWithExtension =  fileName + "." + Helper.GetExtensionByContentType(uploadedFile.ContentType);
                ImageUtility.UploadFile(uploadedFile.ToAdapter(), fileType , "Event", fileName);
                string imageUrl = ImageFacade.GenerateMediaPath("Event", fileNameWithExtension);

                if (fileType == UploadFileType.DealPromoImage) 
                {
                    imageUrl += string.Format("?{0}", System.DateTime.Now.ToString("yyyyMMddHHmmss"));
                }

                return imageUrl;

            }
            return string.Empty;
        }

        private string UploadPromoItemImageFile(HttpPostedFile uploadedFile, UploadFileType fileType, EventPromoItem promoItem)
        {
            if (uploadedFile != null)
            {
                string fileName = string.Format(@"{0}_{1}_{2}", promoItem.MainId, promoItem.Id, fileType);

                string fileNameWithExtension = fileName + "." +
                                               Helper.GetExtensionByContentType(uploadedFile.ContentType);

                ImageUtility.UploadFile(uploadedFile.ToAdapter(), fileType, "Event", fileName);
                string imageUrl = ImageFacade.GenerateMediaPath("Event", fileNameWithExtension);
                return imageUrl;
            }
            return string.Empty;
        }

        private string GetEventPromoItemStatus(ViewEventPromoItem promoItem, DateTime startDate, DateTime endDate)
        {
            string status = "";

            if (DateTime.Now < startDate)
            {
                status = "未上檔";
            }
            else
            {
                IViewPponDeal pponDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(promoItem.BusinessHourGuid);

                if (pponDeal.GroupOrderStatus != null && Helper.IsFlagSet((long)pponDeal.GroupOrderStatus, GroupOrderStatus.Completed))
                {
                    status = "已結檔";
                }
                else if (DateTime.Now >= endDate)
                {
                    status = "已下檔";
                }
                else
                {
                    if (pponDeal != null && (pponDeal.OrderedQuantity >= pponDeal.OrderTotalLimit))
                    {
                        status = "已售完";
                    }
                    else
                    {
                        status = "已上檔";
                    }
                }

            }

            return status;
        }

        private EventPromoItemCollection ValidateAndGetItemListAndAlertMsg(List<Tuple<string, string, string>> dataList, out string alertMsg) {
            EventPromoItemCollection itemCollection = new EventPromoItemCollection();
            alertMsg = string.Empty;

            for (int i = 0; i < dataList.Count; i++)
            {
                Guid bid = new Guid();
                if (Guid.TryParse(dataList[i].Item1, out bid))
                {
                    string cate = dataList[i].Item2;
                    string subcate = dataList[i].Item3;
                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(bid);
                    if (vpd != null && vpd.IsLoaded && vpd.UniqueId.HasValue)
                    {
                        if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {
                            #region 檢查ViewPponDeal的欄位是否有填
                            bool columnsComplete = true;
                            string editLink = string.Format("<a target='_blank' href='{0}/controlroom/ppon/setup.aspx?bid={1}'>{1}</a>", config.SiteUrl, dataList[i].Item1);
                            if (string.IsNullOrWhiteSpace(vpd.EventTitle))
                            {
                                columnsComplete = false;
                                alertMsg += string.Format("第{0}列-行銷標題(橘標)未填：{1}<br />", i + 1, editLink);
                            }
                            if (string.IsNullOrWhiteSpace(vpd.ItemName))
                            {
                                columnsComplete = false;
                                alertMsg += string.Format("第{0}列-訂單短標(品牌名稱.商品)未填：{1}<br />",i + 1, editLink);
                            }
                            #endregion

                            if (columnsComplete)
                            {
                                EventPromoItem item = pp.GetEventPromoItem(View.EventId, vpd.UniqueId.Value);
                                if (item == null)
                                {
                                    item = new EventPromoItem();
                                    item.MainId = View.EventId;
                                    item.Title = vpd.ItemName;
                                    item.Description = vpd.EventTitle;
                                    item.Seq = i;
                                    item.Status = true;
                                    item.Category = cate;
                                    item.SubCategory = subcate;
                                    item.ItemId = vpd.UniqueId.Value;
                                    item.ItemType = (int)EventPromoItemType.Ppon;
                                    item.Creator = View.UserName;
                                    item.Cdt = DateTime.Now;
                                }
                                //else
                                //{
                                //    //已存在的話忽略此筆
                                //    alertMsg += string.Format("第{0}列-此活動商品已存在：{1}<br />", i + 1, dataList[i].Item1);
                                //}
                                itemCollection.Add(item);
                            }
                        }
                        else
                        {
                            alertMsg += string.Format("第{0}列-匯入的bid為子檔的：{1}<br />{3}(必須為母檔的：{2})", i + 1, dataList[i].Item1, vpd.MainBid, string.Join("&nbsp;", new string[18]));//18為實際測出來的寬度，為了對齊
                        }
                    }
                    else
                    {
                        alertMsg += string.Format("第{0}列-查無此bid：{1}<br />", i + 1, dataList[i].Item1);
                    }
                }
                else
                {
                    alertMsg += string.Format("第{0}列-bid格式有誤：{1}<br />", i + 1, dataList[i].Item1);
                }
            }
            return itemCollection;
        }

        private static List<PponCity> GetPponCities()
        {
            List<PponCity> cities = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting();
            string[] cityUnwant = {
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.PEZ.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.Tmall.CityCode };
            return cities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();
        }

        private string GetTimeStampHashCode() {
            string stamp = DateTime.Now.ToString();
            string hashCode = string.Format("{0:X}", stamp.GetHashCode());
            return hashCode;
        }

        private string GetPageName(EventPromoTemplateType templateType) {
            string pageName = string.Empty;
            switch (templateType)
            {
                case EventPromoTemplateType.PponOnly:
                    pageName = "Exhibition";
                    break;
                case EventPromoTemplateType.JoinPiinlife:
                    pageName = "ExhibitionJoinPiinlife";
                    break;
                case EventPromoTemplateType.Commercial:
                    pageName = "ExhibitionCommercial";
                    break;
                case EventPromoTemplateType.Kind:
                    pageName = "ExhibitionKind";
                    break;
                case EventPromoTemplateType.Zero:
                    pageName = "ExhibitionZero";
                    break;
                case EventPromoTemplateType.SkmEvent:
                    pageName = "SkmExhibitionCommercial";
                    break;
                case EventPromoTemplateType.VoteMix:
                    pageName = "ExhibitionVote";
                    break;
                case EventPromoTemplateType.NewJoinPiinlife:
                    pageName = "ExhibitionJoinPiinlife";
                    break;
                case EventPromoTemplateType.OnlyNewPiinlife:
                    pageName = "ExhibitionNewPiinlife";
                    break;
                default:
                    pageName = "Exhibition";
                    break;
            }
            return pageName;
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponZeroActivityPresenter : Presenter<IPponZeroActivityView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog Logger = LogManager.GetLogger("ZeroActivity");
        private static object CouponLock = new object();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            View.DealChannelList = PponFacade.DealChannelCategoryGet(View.BusinessHourGuid);
            
            // APP only
            if (PponDealPreviewManager.GetDealCategoryIdList(View.BusinessHourGuid)
                                      .IndexOf(CategoryManager.Default.AppLimitedEdition.CategoryId) > -1)
            {
                View.RedirectToAppLimitedEdtionPage();
                return true;
            }

            // user is not logged in
            if (string.IsNullOrEmpty(View.UserName))
            {
                View.GoToLoginPage();
                return true;
            }

            MemberUtility.SetUserSsoInfoReady(View.UserId);
            if (string.IsNullOrEmpty(View.TicketId))
            {
                IViewPponDeal vpd = View.TheDeal = pp.ViewPponDealGetByBusinessHourGuid(View.BusinessHourGuid);

                if (!decimal.Equals(0, vpd.ItemPrice))
                {
                    View.GoToPay(View.BusinessHourGuid);
                }
                //檢查是否為天貓檔次 (天貓檔次固定為0元檔)
                if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                {
                    View.GoToPponDefaultPage(vpd.BusinessHourGuid, "此檔好康目前無法購買喔。");
                }

                #region check deal stage

                PponDealStage pponDealStage = vpd.GetDealStage();
                if (pponDealStage == PponDealStage.RunningAndFull)
                {
                    Guid goBackBid = View.BusinessHourGuid;
                    View.GoToPponDefaultPage(goBackBid, "此檔好康已銷售一空喔。");
                    return true;
                }
                else if (!View.IsPreview && pponDealStage != PponDealStage.Running && pponDealStage != PponDealStage.RunningAndOn)
                {
                    return false;
                }
                else if ((vpd.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    return false;
                }

                #endregion check deal stage

                #region 檢查 pez 會員

                if ((vpd.GroupOrderStatus & (int)GroupOrderStatus.PEZeventWithUserId) > 0)
                {
                    string pez_external_userid = MemberFacade.CheckIsPEZUserID(View.UserName);
                    if (string.IsNullOrEmpty(pez_external_userid))
                    {
                        View.GoToPponDefaultPage(View.BusinessHourGuid, "此活動僅提供PayEasy會員登錄，請用PayEasy帳號重新登入");
                        return true;
                    }

                    Peztemp peztemp = OrderFacade.GetPezTemp(View.BusinessHourGuid, pez_external_userid);
                    if (string.IsNullOrEmpty(peztemp.PezCode))
                    {
                        View.GoToPponDefaultPage(View.BusinessHourGuid, "很抱歉，您不符合活動資格");
                        return true;
                    }
                }

                #endregion 檢查 pez 會員

                int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, (bool)vpd.IsDailyRestriction);
                var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(View.BusinessHourGuid, VbsRightFlag.VerifyShop);
                View.SetContent(alreadyboughtcount, (bool)vpd.IsDailyRestriction, stores, (bool)vpd.MultipleBranch);
            }
            else
            {
                // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
                if (int.Equals(0, MemberFacade.GetUniqueId(View.UserName)))
                {
                    View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                }

                if (string.IsNullOrWhiteSpace(View.TransactionId))
                {
                    #region get the deal

                    IViewPponDeal vpd = null;
                    try
                    {
                        vpd = View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(new Guid(View.TicketId.Split('_')[1]));
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("tickedId=" + View.TicketId);
                        throw ex;
                    }

                    #endregion get the deal

                    #region get transaction id

                    string tid = PaymentFacade.GetTransactionId();

                    #endregion get transaction id

                    ViewShoppingCartItemCollection vsci = op.ViewShoppingCartItemGetList(View.TicketId);
                    Guid storeGuid = vsci.Count > 0 ? vsci[0].StoreGuid == null ? Guid.Empty : (Guid)vsci[0].StoreGuid : Guid.Empty;

                    var pponStore = pp.PponStoreGet(vpd.BusinessHourGuid, storeGuid);
                    if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) && 
                        pponStore.OrderedQuantity >= pponStore.TotalQuantity)
                    {
                        View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                    }
                    else
                    {
                        // make order
                        Order o = OrderFacade.MakeZeroOrder(View.UserName, View.TicketId, vpd.OrderGuid, vpd, storeGuid, View.FromType);

                        #region create payment transaction

                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)vpd.Department), PayTransPhase.Created);
                        PaymentTransaction ptCreditcard = PaymentFacade.NewTransaction(tid, o.Guid, PaymentType.Creditcard,
                            0, null, PayTransType.Authorization, DateTime.Now, View.UserName, "0 元檔", status, PayTransResponseType.OK);

                        #endregion create payment transaction

                        View.GoToGet(View.TicketId, tid);
                    }
                }
                else
                {
                    #region get the deal

                    IViewPponDeal vpd = null;
                    try
                    {
                        vpd = View.TheDeal = pp.ViewPponDealGetByBusinessHourGuid(new Guid(View.TicketId.Split('_')[1]));
                    }
                    catch (Exception ex)
                    {
                        Logger.Info("tickedId= " + View.TicketId + " transactionId= " + View.TransactionId);
                        throw ex;
                    }

                    #endregion get the deal

                    #region 預防超賣

                    int preventingId = 0;
                    if (!PponFacade.CheckPreventingOversell(View.TheDeal, 1, View.UserId, out preventingId))
                    {
                        View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                        return true;
                    }

                    #endregion 預防超賣

                    View.DealChannelList = PponFacade.DealChannelCategoryGet(vpd.BusinessHourGuid);

                    int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)vpd.Department), PayTransPhase.Created);
                    PaymentTransaction ptCreditcard = op.PaymentTransactionGet(View.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
                    
                    if (checkTransaction(ptCreditcard, status))
                    {
                        if (vpd.DealIsOrderable())
                        {
                            #region make cash trust log

                            //if weeklypay deal
                            int trust_checkout_type = 0;
                            if (((vpd.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                            {
                                trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                            }

                            OrderDetailCollection odCol = op.OrderDetailGetList(1, 1, string.Empty, ptCreditcard.OrderGuid.Value, OrderDetailTypes.Regular);

                            Member usr = mp.MemberGet(View.UserName);
                            CashTrustInfo cti = new CashTrustInfo
                            {
                                OrderGuid = ptCreditcard.OrderGuid.Value,
                                OrderDetails = odCol,
                                CreditCardAmount = 0,
                                SCashAmount = 0,
                                PCashAmount = 0,
                                BCashAmount = 0,
                                Quantity = 1,
                                DeliveryCharge = 0,
                                DeliveryType = vpd.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)vpd.DeliveryType.Value,
                                DiscountAmount = 0,
                                AtmAmount = 0,
                                BusinessHourGuid = vpd.BusinessHourGuid,
                                CheckoutType = trust_checkout_type,
                                TrustProvider = Helper.GetBusinessHourTrustProvider(vpd.BusinessHourStatus),
                                ItemName = vpd.ItemName,
                                ItemPrice = 0,
                                User = usr
                            };

                            CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                            #endregion make cash trust log

                            Coupon coupon = new Coupon();

                            if (!Enum.Equals(DeliveryType.ToHouse, vpd.DeliveryType))
                            {
                                lock (CouponLock)
                                {
                                    if ((Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal) &&
                                         vpd.CouponCodeType == (int)CouponCodeType.Pincode) ||
                                        (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal) &&
                                         Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent)))
                                    {
                                        Peztemp pez = null;
                                        if (vpd.PinType == (int)PinType.Single)
                                        {
                                            pez = OrderFacade.GetPezTemp(vpd.BusinessHourGuid);
                                        }
                                        else
                                        {
                                            pez = OrderFacade.GetPezTemp(vpd.BusinessHourGuid, View.UserName,
                                                Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventWithUserId));
                                        }

                                        if (null == pez)
                                        {
                                            View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                                            return true;
                                        }
                                        else
                                        {
                                            coupon.OrderDetailId = odCol[0].Guid;
                                            coupon.Code = coupon.SequenceNumber = pez.PezCode;
                                            coupon.Id = Convert.ToInt32(OrderFacade.GetCouponID(coupon));
                                            pez.OrderDetailGuid = coupon.OrderDetailId;
                                            pez.OrderGuid = ptCreditcard.OrderGuid.Value;
                                            pez.Status = (int)PeztempStatus.Using;

                                            try
                                            {
                                                using (TransactionScope ts2 = new TransactionScope())
                                                {
                                                    OrderFacade.SetPezTemp(pez);
                                                    foreach (CashTrustLog ctl in ctCol)
                                                    {
                                                        OrderFacade.SaveTrustCoupon(ctl, coupon.Id, coupon.SequenceNumber);
                                                    }

                                                    ts2.Complete();
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                                //update 超賣資料過期
                                                op.SetPreventingOversellExpired(preventingId);
                                                OrderFacade.DeletePeztempCoupon(pez.PezCode, (Guid)pez.Bid);
                                                Logger.Error("Something wrong while trying to fill coupon_id in cash_trust_log", ex);
                                                View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                                                return true;
                                            }

                                            View.SetDealCoupon(coupon, (GroupOrderStatus)vpd.GroupOrderStatus.Value, config.EnableFami3Barcode);
                                        }
                                    }
                                    else
                                    {
                                        Order theOrder = op.OrderGet(odCol[0].OrderGuid);
                                        List<Coupon> coupons = CouponFacade.GenCouponWithSmsWithGeneratePeztemp(theOrder, vpd, false);
                                        if (coupons.Count == 0)
                                        {
                                            View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                                            return true;
                                        }
                                        coupon = coupons[0];
                                    }
                                }
                            }

                            #region update payment transaction

                            status = Helper.SetPaymentTransactionPhase(
                                Helper.SetPaymentTransactionDepartmentTypes(0, (DepartmentTypes)vpd.Department), PayTransPhase.Successful);

                            PaymentFacade.UpdateTransaction(View.TransactionId, ptCreditcard.OrderGuid, PaymentType.Creditcard,
                                0, null, PayTransType.Authorization, DateTime.Now, "0 元檔", status, 0);

                            #endregion update payment transaction

                            OrderFacade.SetOrderStatus(ptCreditcard.OrderGuid.Value, View.UserName, OrderStatus.Complete, true,
                                new SalesInfoArg
                                {
                                    BusinessHourGuid = vpd.BusinessHourGuid,
                                    Quantity = odCol.OrderedQuantity,
                                    Total = odCol.OrderedTotal
                                });

                            //update 超賣資料過期
                            op.SetPreventingOversellExpired(preventingId);
                            
                            // cpa stuff
                            if (!string.IsNullOrWhiteSpace(View.ReferrerSourceId))
                            {
                                Order o = op.OrderGet(ptCreditcard.OrderGuid.Value);
                                CpaUtility.RecordOrderByReferrer(View.ReferrerSourceId, o, View.TheDeal, pp.DealPropertyGet(View.TheDeal.BusinessHourGuid));
                            }
                            else if (!string.IsNullOrEmpty(View.RsrcSession))  //add for BBH share Money , died 2011.05.19
                            {
                                Order o = op.OrderGet(ptCreditcard.OrderGuid.Value);
                                CpaUtility.RecordOrderByReferrer(View.RsrcSession, o, View.TheDeal, pp.DealPropertyGet(View.TheDeal.BusinessHourGuid));
                            }

                            #region New Cpa

                            if (config.NewCpaEnable)
                            {
                                try
                                {
                                    if (View.NewCpa != null && View.NewCpa.Count > 0)
                                    {
                                        View.NewCpa = PponFacade.CpaSetDetailToDB(View.UserName, View.SessionId, Helper.GetClientIP(), View.NewCpa, ptCreditcard.OrderGuid.Value);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Logger.Error(string.Format("CpaSetDetailToDB error: UserName:{0}, SessionId:{1}, Cpa:{2}, OrderGuid:{3}, Exception:{4}",
                                        View.UserName, View.SessionId, View.NewCpa, ptCreditcard.OrderGuid.Value, ex));
                                }
                            }

                            #endregion New Cpa

                            // set deal close time
                            OrderFacade.PponSetCloseTimeIfJustReachMinimum(vpd, 1);

                            #region 分店銷售數量

                            if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) &&
                                odCol[0].StoreGuid != Guid.Empty)
                            {
                                pp.PponStoreUpdateOrderQuantity(vpd.BusinessHourGuid, (Guid)odCol[0].StoreGuid, 1);
                            }

                            #endregion 分店銷售數量
                            
                            // 寄送完成通知
                            if (View.DealChannelList.Any(x => x.CategoryId == CategoryManager.Default.Skm.CategoryId))
                            {
                                OrderFacade.SendSkmZeroDealSuccessMail(usr.UserEmail, vpd, ptCreditcard.OrderGuid.Value);
                            }
                            else
                            {
                                OrderFacade.SendZeroDealSuccessMail(usr.DisplayName, usr.UserEmail, vpd, pp.ViewPponCouponGet(coupon.Id), (GroupOrderStatus)vpd.GroupOrderStatus.Value, (CouponCodeType)vpd.CouponCodeType.Value);
                            }

                            //索取全家好康
                            if (Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
                            {
                                View.SetDealCoupon(coupon, (GroupOrderStatus)vpd.GroupOrderStatus.Value, config.EnableFami3Barcode);
                                View.ShowResult(PaymentResultPageType.FamiBarcodeSuccess);
                                View.SetFamiDealBarcodeContent(vpd);
                            }
                            else
                            {
                                //索取新光好康
                                if (View.DealChannelList.Any(x => x.CategoryId == CategoryManager.Default.Skm.CategoryId))
                                {
                                    View.ShowSkmResult(PaymentResultPageType.SkmZeroPriceSuccess);
                                    View.SetSkmDealCoupon();
                                }
                                //索取一般好康
                                else
                                {
                                    View.ShowResult(PaymentResultPageType.ZeroPriceSuccess);
                                    if (vpd.IsZeroActivityShowCoupon == true)
                                    {
                                        View.SetDealCoupon(coupon, pp.ViewPponOrderGet(ptCreditcard.OrderGuid.Value));
                                    }
                                }
                            }
                        }
                        else
                        {
                            PaymentFacade.CancelPaymentTransactionByTransId(View.TransactionId, "sys", "Unorderable.");
                            View.ShowResult(PaymentResultPageType.ZeroPriceFailed);
                        }
                    }
                    else
                    {
                        View.GoToPponDefaultPage(new Guid(View.TicketId.Split('_')[1]), string.Empty);
                    }
                }
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckOut += OnCheckOut;
            return true;
        }

        protected void OnCheckOut(object sender, EventArgs e)
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);
            View.TheDeal = theDeal;
            string ticketId = OrderFacade.MakeRegularTicketId(
                View.SessionId, View.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", string.Empty);
            string strResult = CheckData(View.TheDeal, (bool)theDeal.IsDailyRestriction);

            if (string.IsNullOrWhiteSpace(strResult))
            {
                ItemEntry ie = new ItemEntry(View.TheDeal.ItemGuid, View.TheDeal.ItemName, 0, 1, new List<AccessoryEntry>(), string.Empty, View.StoreGuid);
                OrderFacade.PutItemToCart(ticketId, ie);
                View.GoToClaim(ticketId);
            }
            else
            {
                View.AlertMessage(strResult);
                OnResetContent(this, null);
            }
        }

        private string CheckData(IViewPponDeal vpd, bool isDailyRestriction)
        {
            if (1 > (vpd.MaxItemCount ?? int.MaxValue))
            {
                return "每單訂購數量最多" + vpd.MaxItemCount + "份，請重新輸入數量";
            }

            int already = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, isDailyRestriction);
            if (vpd.ItemDefaultDailyAmount != null)
            {
                if (1 + already > vpd.ItemDefaultDailyAmount.Value)
                {
                    return "每人訂購數量最多" + vpd.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量";
                }
            }

            if (PponFacade.CheckExceedOrderedQuantityLimit(vpd, 1))
            {
                return "很抱歉!已超過最大訂購數量";
            }

            if (DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }

            return string.Empty;
        }

        protected void OnResetContent(object sender, object e)
        {
            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);
            View.TheDeal = theDeal;

            int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, (bool)theDeal.IsDailyRestriction);
            var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(View.BusinessHourGuid, VbsRightFlag.VerifyShop);
            View.SetContent(alreadyboughtcount, (bool)theDeal.IsDailyRestriction, stores, (bool)theDeal.MultipleBranch);
        }

        private bool checkTransaction(PaymentTransaction pt, int status)
        {
            if (null != pt && string.Equals(pt.CreateId, View.UserName) && int.Equals(status, pt.Status))
            {
                return true;
            }

            return false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class DiscountUserExchangePresenter : Presenter<IDiscountUserExchangeView>
    {
        private IOrderProvider op;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Send += OnSend;
            return true;
        }

        public DiscountUserExchangePresenter(IOrderProvider odrProv)
        {
            op = odrProv;
        }

        #region event
        protected void OnSend(object sender, EventArgs e)
        {
            DiscountEvent de = op.DiscountEventGet(DiscountEvent.Columns.EventCode, View.EventCode);
            if (de.IsLoaded && de.Code == View.Code)
            {
                DateTime now = DateTime.Now;
                int userId = MemberFacade.GetUniqueId(View.UserName);
                if (de.StartTime > now)
                {
                    View.ShowMessage("活動尚未開始");
                }
                else if (de.EndTime < now || de.Status == (int)DiscountEventStatus.Disabled)
                {
                    View.ShowMessage("活動已結束");
                }
                else
                {
                    // 查找是否已領取過
                    DiscountUserCollection userList = op.DiscountUserGetList(userId, de.Id);
                    if (userList.Any())
                    {
                        View.ShowMessage("您已領取");
                    }
                    else
                    {
                        // 查找對應的折價券是否有設定資料
                        ViewDiscountEventCampaignCollection decs = op.ViewDiscountEventCampaignGetList(de.Id);
                        if (decs.Any(x => x.StartTime < now && x.EndTime > now))
                        {
                            DiscountUser du = new DiscountUser();
                            du.UserId = userId;
                            du.EventId = de.Id;
                            du.Status = (int)DiscountUserStatus.Initial;
                            du.CreateId = "sys";
                            du.CreateTime = DateTime.Now;
                            op.DiscountUserSet(du);

                            View.RedirectDiscountList();
                        }
                        else
                        {
                            View.ShowMessage("活動已領取完畢");
                        }
                    }
                }
            }
            else
            {
                View.ShowMessage("無此活動，請確認您輸入的活動代碼");
            }
        }
        #endregion
    }
}

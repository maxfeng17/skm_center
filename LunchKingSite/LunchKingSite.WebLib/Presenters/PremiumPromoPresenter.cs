﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;


namespace LunchKingSite.WebLib.Presenters
{
    public class PremiumPromoPresenter : Presenter<IPremiumPromo>
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            EventPremiumPromo premiumPromo = ep.EventPremiumPromoGet(View.Url, EventPremiumPromoType.Basic);

            if (premiumPromo == null)
            {
                View.ShowPremiumExpire();
                return true;
            }

            EventPremiumPromoItemCollection premiumPromoItemCol = ep.EventPremiumPromoItemGetListByEventyId(premiumPromo.Id);

            View.AppliedPremiumAmount = (premiumPromoItemCol.Any()) ? premiumPromoItemCol.Count : default(int);

            View.EventPremiumPromoSetting(premiumPromo);


            //主題活動存在，且時間符合展示時間，可顯示於WEB，當有Preview參數時，為預覽模式，不考慮時間與顯示於WEB設定
            if (premiumPromo.IsLoaded && ((premiumPromo.StartDate <= DateTime.Now && DateTime.Now <= premiumPromo.EndDate.AddDays(1) && premiumPromo.Status) || !string.IsNullOrEmpty(View.Preview)))
            {
                if (View.AppliedPremiumAmount >= premiumPromo.PremiumAmount)
                {
                    View.ShowEventFinish();
                    return true;
                }

                // user is not logged in
                if (string.IsNullOrEmpty(View.UserName))
                {
                    View.ShowLogin();
                    return true;
                }

                //檢查是否已經領取過
                if (premiumPromoItemCol.Any(x => x.UserId == View.UserId))
                {
                    View.ShowOverUserAmount();
                    return true;
                }

                ViewMemberBuildingCityParentCity vmbcpc = mp.ViewMemberBuildingCityParentCityGetByUserName(View.UserName);
                View.AwardUserInfoSetting(vmbcpc);
                View.ShowApplyUserInfo();
            }
            else
            {
                View.ShowPremiumExpire();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SendAwardUserInfo += OnSendAwardUserInfo;
            return true;
        }

        protected void OnSendAwardUserInfo(object sender, DataEventArgs<EventPremiumPromoItem> e)
        {
            //最後數量檢查
            EventPremiumPromo premiumPromo = ep.EventPremiumPromoGet(View.Url, EventPremiumPromoType.Basic);
            EventPremiumPromoItemCollection premiumPromoItemCol = ep.EventPremiumPromoItemGetListByEventyId(e.Data.EventPremiumPromoId);

            //檢查是否已經領取過
            if (premiumPromoItemCol.Any(x => x.UserId == View.UserId))
            {
                BindData();
                View.ShowOverUserAmount();
            }
            else if (premiumPromoItemCol.Count + 1 > premiumPromo.PremiumAmount)
            {
                BindData();
                View.ShowEventFinish();
            }
            else
            {
                if (e.Data.EventPremiumPromoId == -1)
                {
                    BindData();
                    View.ShowPremiumExpire();
                }
                else
                {
                    //ep.EventPremiumPromoItemSet(e.Data);
                    var insertnumber = ep.EventPremiumPromoItemSet(e.Data.EventPremiumPromoId ,premiumPromo.PremiumAmount,e.Data.AwardName,e.Data.AwardEmail,e.Data.AwardAddress,e.Data.AwardMobile,e.Data.UserId);

                    if (insertnumber == 1)
                    {
                        EventFacade.GreetingPremiumSuccessMail(premiumPromo, e.Data);
                        BindData();
                        View.ShowSuccess();
                    }
                    else
                    {
                        BindData();
                        View.ShowError();
                    }
                }
            }
        }

        private void BindData()
        {
            EventPremiumPromo premiumPromo = ep.EventPremiumPromoGet(View.Url, EventPremiumPromoType.Basic);

            if (premiumPromo == null)
            {
                View.ShowPremiumExpire();
                return;
            }

            EventPremiumPromoItemCollection premiumPromoItemCol = ep.EventPremiumPromoItemGetListByEventyId(premiumPromo.Id);

            View.AppliedPremiumAmount = (premiumPromoItemCol.Any()) ? premiumPromoItemCol.Count : default(int);

            View.EventPremiumPromoSetting(premiumPromo);
        }
    }
}

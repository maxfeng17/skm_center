﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class BookingSystemReservationServicePresenter : Presenter<IBookingSystemReservationServiceView>
    {
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IBookingSystemProvider bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (string.IsNullOrEmpty(View.UserName))
            {
                View.RedirectToLogin();
                return true;
            }

            View.ApiKey = View.ApiKey;
            BookingSystemServiceName serviceName = BookingSystemFacade.GetBookingSystemServiceName(View.ApiKey, BookingSystemApiServiceType.PponService);

            View.OrderGuid = View.OrderGuid;
            View.UserEmail = View.UserName;
            Member m = mp.MemberGet(View.UserName);
            View.MemberKey = m.UniqueId;

            Dictionary<string, string> sellers = new Dictionary<string, string>();
            bool IsNoRestrictedStore = false;

            if (View.OrderGuid != null)
            {
                CashTrustLog vpct = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid.Value).Where(x=>x.BusinessHourGuid != null).FirstOrDefault();
                if(vpct != null)
                {
                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(vpct.BusinessHourGuid.Value);
                    PponStoreCollection psc = pp.PponStoreGetListByBusinessHourGuid(vpct.BusinessHourGuid.Value);
                    IEnumerable<Seller> ss = SellerFacade.GetSellerTreeSellers(vpd.SellerGuid);
                    IsNoRestrictedStore = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);

                    if (IsNoRestrictedStore)
                    {
                        //通用券
                        var dealProperty = pp.DealPropertyGetByOrderGuid(View.OrderGuid.Value);

                        sellers.Add("", "請選擇");
                        foreach (Seller seller in ss)
                        {
                            if (!sellers.Keys.Contains(seller.SellerId))
                            {
                                if ((BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid(seller.Guid, BookingType.Coupon)
                                    || BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid(seller.Guid, BookingType.Travel) )
                                    && dealProperty.BookingSystemType > 0)
                                {
                                    PponStore ps = psc.Where(x => x.StoreGuid == seller.Guid).FirstOrDefault();
                                    if(ps != null)
                                    {
                                        if(Helper.IsFlagSet((int)VbsRightFlag.Verify, ps.VbsRight))
                                        {
                                            //需多判斷「憑證核實」
                                            sellers.Add(seller.Guid.ToString(), seller.SellerName);
                                        }
                                    }
                                    
                                }                                    
                            }
                        }
                    }
                    else
                    {
                        //一般檔次
                        Seller seller = sp.SellerGet(vpd.SellerGuid);
                        sellers.Add(seller.Guid.ToString(), seller.SellerName);
                    }
                    
                    
                }
            }

            View.SetDefaultValue(m.DisplayName, m.Gender, m.Mobile, sellers, IsNoRestrictedStore);
            View.CouponCanUseDate = string.Empty;
            List<string> CouponCanUseDateList = new List<string>();
            if (serviceName == BookingSystemServiceName._17Life)
            {
                ViewCouponListMainCollection main = GetViewCouponListMain();

                if (main.Any())
                {
                    View.CouponUsage = (string.IsNullOrEmpty(main.ElementAtOrDefault(0).AppTitle)) ? main.ElementAtOrDefault(0).CouponUsage : main.ElementAtOrDefault(0).AppTitle;
                    DealProperty dealProperty = pp.DealPropertyGet(main.ElementAtOrDefault(0).BusinessHourGuid);
                    View.BookingSystemType = dealProperty.BookingSystemType;
                    View.ReservationMinDay = dealProperty.AdvanceReservationDays;
                    View.CouponUsers = dealProperty.CouponUsers;
                    View.CouponLink = string.Format("{0}/{1}", config.SiteUrl, main.ElementAtOrDefault(0).BusinessHourGuid);

                    #region check coupon can use date

                    DateTime indexDate;
                    DateTime indexEndDate;
                    var selector = new ExpirationDateSelector();
                    selector.ExpirationDates = new PponUsageExpiration(main.First().Guid);
                    var expireDate = selector.GetExpirationDate();
                    if (DateTime.TryParse(main.ElementAtOrDefault(0).BusinessHourDeliverTimeS.ToString(), out indexDate) && DateTime.TryParse(expireDate.ToString(), out indexEndDate))
                    {
                        while (indexDate <= indexEndDate)
                        {
                            CouponCanUseDateList.Add(indexDate.ToString("yyyy/MM/dd"));
                            indexDate = indexDate.AddDays(1);
                        }
                        View.CouponCanUseDate = string.Join(",", CouponCanUseDateList);
                    }

                    #endregion check coupon can use date
                }
            }
            else if (serviceName == BookingSystemServiceName.HiDeal)
            {
                ViewHiDealOrderMemberOrderShowCollection vhomCol = GetViewHiDealOrderMemberOrderShow();
                if (vhomCol.Any())
                {
                    View.CouponUsage = vhomCol[0].ProductName;
                    HiDealProduct product = hp.HiDealProductGet((int)vhomCol[0].ProductId);
                    View.BookingSystemType = product.BookingSystemType;
                    View.ReservationMinDay = product.AdvanceReservationDays;
                    View.CouponUsers = product.CouponUsers;
                    View.CouponLink = string.Format("{0}/piinlife/HiDeal.aspx?regid=2&did={1}", config.SiteUrl, vhomCol[0].HiDealId);

                    #region check coupon can use date

                    DateTime indexDate;
                    DateTime indexEndDate;
                    if (DateTime.TryParse(product.UseStartTime.ToString(), out indexDate) && DateTime.TryParse(product.UseEndTime.ToString(), out indexEndDate))
                    {
                        while (indexDate <= indexEndDate)
                        {
                            CouponCanUseDateList.Add(indexDate.ToString("yyyy/MM/dd"));
                            indexDate = indexDate.AddDays(1);
                        }
                        View.CouponCanUseDate = string.Join(",", CouponCanUseDateList);
                    }

                    #endregion check coupon can use date
                }
            }

            View.SetCouponSequenceCheckBox(GetCouponSequenceCol());
            View.FullBookingDate = BookingSystemFacade.GetFullBookingDate(View.ReservationMinDay, View.StoreGuid, (BookingType)View.BookingSystemType);

            //Set 店家資訊
            if (View.StoreGuid.HasValue)
            {
                ViewBookingSystemStore store = bp.ViewBookingSystemStoreGetByStoreGuid((Guid)View.StoreGuid, (BookingType)View.BookingSystemType);
                View.StoreName = store.SellerName + store.StoreName;
                View.StoreAddress = store.AddressString;
                View.StorePhoneNumber = store.Phone;
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            return true;
        }

        #region method

        private CashTrustLogCollection GetCouponSequenceCol()
        {
            if (View.OrderGuid == null)
            {
                return new CashTrustLogCollection();
            }
            else
            {
                CashTrustLogCollection sequenceCol = mp.CashTrustLogGetCouponSequencesWithUnused((Guid)View.OrderGuid);
                for (int i = 0; i < sequenceCol.Count(); i++)
                {
                    if (bp.BookingSystemCouponIsExistBySequenceOrderDetailGuid(sequenceCol[i].CouponSequenceNumber, sequenceCol[i].OrderDetailGuid))
                    {
                        sequenceCol.RemoveAt(i);
                        i--;
                    }
                }
                if (sequenceCol.Any())
                {
                    View.StoreGuid = sequenceCol.FirstOrDefault().StoreGuid;
                }
                return sequenceCol;
            }
        }
        private ViewCouponListMainCollection GetViewCouponListMain()
        {
            if (View.OrderGuid != null)
            {
                return mp.GetCouponListMainListByOrderID(MemberFacade.GetUniqueId(View.UserName), op.OrderGet((Guid)View.OrderGuid).OrderId);
            }
            else
            {
                return new ViewCouponListMainCollection();
            }
        }
        private ViewHiDealOrderMemberOrderShowCollection GetViewHiDealOrderMemberOrderShow()
        {
            if (View.OrderGuid != null)
            {
                return hp.ViewHiDealOrderMemberOrderShowGetByOrderGuid((Guid)View.OrderGuid);
            }
            else
            {
                return new ViewHiDealOrderMemberOrderShowCollection();
            }
        }

        #endregion
    }
}

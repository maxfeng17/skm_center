﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponOptionContnetPresenter : Presenter<IPponOptionContnetView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private IPponProvider pp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            return true;
        }

        public PponOptionContnetPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IMemberProvider memProv, IPponProvider pponProv)
        {
            sp = sellerProv;
            hp = humProv;
            mp = memProv;
            pp = pponProv;
        }
        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
        }
        #endregion

        #region method

        //private void LoadData(string deptId, string empName = "")
        //{
        //    Guid bid = View.BusinessHourGuid;
        //    if (bid != Guid.Empty)
        //    {
        //        PponOptionItems poi = PponFacade.PponOptionItemsGet(bid, Guid.Empty, deptId, empName);
        //        View.SetPponOptionContent(poi);
        //    }
        //}
        #endregion
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class CreditcardBankSetUpPresenter : Presenter<ICreditcardBankSetUpView>
    {
        public IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        public IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        public CreditcardBankSetUpPresenter()
        {

        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Import += OnImport;
            View.BankInsert += OnBankInsert;
            View.CreditcardInsert += OnCreditcardInsert;
            return true;
        }

        protected void OnImport(object sender, EventArgs e)
        {
            if (!System.IO.Path.GetExtension(View.FUImport.PostedFile.FileName).Equals(".xls"))
            {
                View.ShowMessage("請使用xls的Excel檔案");
                return;
            }
            if (!string.IsNullOrEmpty(View.FUImport.PostedFile.FileName))
            {
                List<CreditcardBank> banks = op.CreditcardBankGetList().ToList();
                CreditcardBankInfoCollection info = op.CreditcardBankInfoGetList();

                HSSFWorkbook workbook = new HSSFWorkbook(View.FUImport.PostedFile.InputStream);
                Sheet currentSheet = workbook.GetSheetAt(0);
                Row row;

                Member mem = mp.MemberGet(View.UserName);

                for (int i = 1; i <= currentSheet.LastRowNum; i++)
                {
                    row = currentSheet.GetRow(i);
                    string no = row.GetCell(0).ToString();  //NO.
                    string binNum = row.GetCell(1).ToString();   //BIN_NUM
                    string bankId = row.GetCell(2).ToString();   //BANK_ID
                    string bankName = row.GetCell(3).ToString();  //NAME                    
                    string exCardType = row.GetCell(4).ToString();  //CARD_TYPE
                    int cardType = (int)CreditCardType.None;
                    switch (exCardType.ToUpper())
                    {
                        case "A":
                            cardType = (int)CreditCardType.AmericanExpress;
                            break;
                        case "C":
                            cardType = (int)CreditCardType.UnionPay;
                            break;
                        case "J":
                            cardType = (int)CreditCardType.JCBCard;
                            break;
                        case "M":
                            cardType = (int)CreditCardType.MasterCard;
                            break;
                        case "S":
                            cardType = (int)CreditCardType.SmartPay;
                            break;
                        case "U":
                            cardType = (int)CreditCardType.UCard;
                            break;
                        case "V":
                            cardType = (int)CreditCardType.VisaCard;
                            break;
                        default:
                            cardType = (int)CreditCardType.None;
                            break;
                    }

                    var existInfo = info.Where(x => x.Bin == binNum).FirstOrDefault();
                    if (existInfo == null)
                    {
                        var existBank = banks.Where(x => x.BankId == bankId).FirstOrDefault();
                        if (existBank == null)
                        {
                            CreditcardBank bank = new CreditcardBank()
                            {
                                BankName = bankName,
                                IsInstallment = false,
                                UserId = mem.UniqueId,
                                BankId = bankId,
                                LastUpdateDate = DateTime.Now,
                            };
                            op.CreditcardBankSet(bank);
                            banks.Add(bank);
                            existBank = bank;
                        }
                        CreditcardBankInfo bankInfo = new CreditcardBankInfo()
                        {
                            Bin = binNum,
                            BankName = bankName,
                            Flag = 0,
                            CreateId = View.UserName,
                            CreateTime = DateTime.Now,
                            BankId = existBank.Id,
                            CardType = cardType
                        };
                        op.CreditcardBankInfoSet(bankInfo);
                    }
                }
            }
        }
        protected void OnBankInsert(object sender, EventArgs e)
        {
            List<CreditcardBank> banks = op.CreditcardBankGetList().ToList();
            var existBank = banks.Where(x => x.BankId == View.BankId).FirstOrDefault();
            if (existBank == null)
            {
                Member mem = mp.MemberGet(View.UserName);
                CreditcardBank bank = new CreditcardBank()
                {
                    BankName = View.BankName,
                    IsInstallment = false,
                    UserId = mem.UniqueId,
                    BankId = View.BankId,
                    LastUpdateDate = DateTime.Now,
                };
                op.CreditcardBankSet(bank);
            }
        }

        protected void OnCreditcardInsert(object sender, EventArgs e)
        {
            List<CreditcardBank> banks = op.CreditcardBankGetList().ToList();
            var existBank = banks.Where(x => x.Id == View.InsCreditcardBank).FirstOrDefault();
            if (existBank != null)
            {
                Member mem = mp.MemberGet(View.UserName);
                CreditcardBankInfo bankInfo = new CreditcardBankInfo()
                {
                    Bin = View.CreditCardBin,
                    BankName = existBank.BankName,
                    Flag = 0,
                    CreateId = View.UserName,
                    CreateTime = DateTime.Now,
                    BankId = existBank.Id,
                    CardType = View.CardType
                };
                op.CreditcardBankInfoSet(bankInfo);
            }
        }

        protected void OnSearch(object sender, DataEventArgs<int> e)
        {
            int bankId = e.Data;
            List<CreditcardBankInfo> info = op.CreditcardBankInfoGetList().Where(x => x.BankId == bankId).ToList();
            List<CreditcardBankInfoModel> data = new List<CreditcardBankInfoModel>();
            foreach (CreditcardBankInfo fo in info)
            {
                data.Add(new CreditcardBankInfoModel
                {
                    Id = fo.Id,
                    Bin = fo.Bin,
                    BankName = fo.BankName,
                    CreateId = fo.CreateId,
                    CreateTime = fo.CreateTime,
                    BankId = fo.BankId,
                    CardType = fo.CardType,
                    CardTypeName = ((CreditCardType)fo.CardType).ToString()
                });
            }
            View.SetCreditcardBankInfo(data);
        }

        private void LoadData()
        {
            CreditcardBankCollection banks = op.CreditcardBankGetList();

            View.SetCreditcardBank(banks);
        }
    }
}

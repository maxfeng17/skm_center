﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class EntrustSellDailyReportPresenter : Presenter<IEntrustSellDailyReportView>
    {
        #region members
        private IOrderProvider op;
        #endregion

        public EntrustSellDailyReportPresenter()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetContent(GetData(View.FilterStartTime, View.FilterEndTime));
            return true;
        }


        private List<ViewEntrustSellDailyPayment> GetData(DateTime? startTime, DateTime? endTime)
        {
            if (endTime != null)
            {
                endTime = Helper.GetFinalTime(endTime.Value);
            }
            return op.ViewEntrustSellDailyPaymentList(startTime, endTime);
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ExportToExcel += OnExportToExcel;
            View.SearchClicked += OnSearchClicked;
            return true;
        }

        protected void OnExportToExcel(object sender, EventArgs e)
        {
            View.Export(GetData(View.FilterStartTime, View.FilterEndTime));
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            View.SetContent(GetData(View.FilterStartTime, View.FilterEndTime));
        }

    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesPhotographerCalenderPresenter : Presenter<ISalesPhotographerCalenderView>
    {
        private IPponProvider pp;
        private ISellerProvider sp;
        private IHumanProvider hp;


        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(View.Dept, View.Year, View.Month);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadDept))
            {
                LoadData(View.EmpDept, View.Year, View.Month);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
            {
                LoadData(View.EmpDept, View.Year, View.Month, View.EmpUserId);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.PreviousSearch += OnMonthSearch;
            View.NextSearch += OnMonthSearch;
            return true;
        }

        public SalesPhotographerCalenderPresenter(IPponProvider pponProv, ISellerProvider sellerProv, IHumanProvider humProv)
        {
            pp = pponProv;
            sp = sellerProv;
            hp = humProv;
        }



        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(View.Dept,View.Year,View.Month);
        }

        protected void OnMonthSearch(object sender, EventArgs e)
        {
            if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadAll))
            {
                LoadData(View.Dept, View.Year, View.Month);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.ReadDept))
            {
                LoadData(View.EmpDept, View.Year, View.Month);
            }
            else if (CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Read))
            {
                LoadData(View.EmpDept, View.Year, View.Month, View.EmpUserId);
            }

        }


        #endregion

        #region Private Method
        private void LoadData(string deptId, int Year, int Month, int empUserId = 0)
        {
            DateTime start = new DateTime(Year, Month, 1);
            IEnumerable<ViewPponDealCalendar> vpdc = pp.ViewPponDealCalendarGetListByPeriod(start, start.AddMonths(View.MonthCount), deptId);
            if (empUserId != 0)
            {
                vpdc = vpdc.Where(x => x.DevelopeSalesId == empUserId || x.OperationSalesId == empUserId);
            }
            if (View.DealType1 > 0 && View.DealType2 == 0)
            {
                ProposalDealType type = ProposalDealType.TryParse(View.DealType1.ToString(), out type) ? type : ProposalDealType.None;
                Dictionary<int, string> rtn = new Dictionary<int, string>();
                if (type != ProposalDealType.None)
                {
                    rtn = ProposalFacade.ProposalSubDealTypeGet(type, false);
                    vpdc = vpdc.Where(x => rtn.Keys.Contains((int)(x.NewDealType ?? -1)));
                }
            }
            if (View.DealType2 > 0)
            {
                vpdc = vpdc.Where(x => x.NewDealType == View.DealType2);
            }

            int cnt = vpdc.Count();
            int idx = 0;


            Dictionary<ViewPponDealCalendar, Proposal> dataList = new Dictionary<ViewPponDealCalendar, Proposal>();
            ViewEmployee salesIdEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, View.SalesId);

            while (idx <= cnt - 1)
            {
                int batchLimit = 500;
                var newVpdc = vpdc.Skip(idx).Take(batchLimit);
                var bids = newVpdc.Select(x => x.BusinessHourGuid).Distinct().ToList();
                List<Proposal> pros = sp.ProposalGetByBids(bids).ToList();

                foreach (ViewPponDealCalendar vpd in newVpdc)
                {
                    Proposal pro = pros.Where(x => x.BusinessHourGuid == vpd.BusinessHourGuid).FirstOrDefault();

                    if (pro != null)
                    {
                        string photoString = "";
                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Photographers))
                        {
                            Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                            if (specialText != null && specialText.Count > 0)
                            {
                                //攝影師資訊
                                photoString = specialText[ProposalPhotographer.PhotographerAppointCheck];
                                photoString = photoString.Split('|')[3];
                            }
                        }



                        if ((Helper.IsFlagSet(pro.SpecialFlag, View.PhotoSource) || View.PhotoSource == 0) &&
                            (salesIdEmp.UserId == pro.DevelopeSalesId || salesIdEmp.UserId == pro.OperationSalesId || View.SalesId == null) &&
                            (photoString == View.Photographer || View.Photographer == "請選擇"))
                            dataList.Add(vpd, pro);

                    }
                    else
                    {
                        pros.Remove(pro);
                    }


                }

                idx += batchLimit;


            }

            View.SetPponDealCalendar(dataList);

        }

        #endregion
    }
}

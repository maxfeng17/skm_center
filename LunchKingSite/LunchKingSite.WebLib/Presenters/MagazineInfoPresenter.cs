﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;

namespace LunchKingSite.WebLib.Presenters
{
    public class MagazineInfoPresenter : Presenter<IMagazineInfoView>
    {
        private IHumanProvider _humanProv;
        private ISystemProvider _sysProv;
        private SalesService _salesServ;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetDepartmentData(_humanProv.DepartmentGetListByEnabled(true, EmployeeDept.S000));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnPezStoreSearchClicked += OnPezStoreSearchClicked;
            View.OnExportToExcelClicked += OnExportToExcelClicked;
            View.OnPezStoreExportToExcelClicked += OnPezStoreExportToExcelClicked;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }

        public MagazineInfoPresenter(IHumanProvider hp, ISystemProvider sp, SalesService salesServ)
        {
            _humanProv = hp;
            _sysProv = sp;
            _salesServ = salesServ;
        }

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetBusinessOrderData(1);
        }
        protected void OnPezStoreSearchClicked(object sender, EventArgs e)
        {
            View.GetBusinessPezStoreList(GetBusinessOrderLsitByPageNumber(1));
        }
        protected void OnExportToExcelClicked(object sender, EventArgs e)
        {
            View.SetGridViewListData(GetGridViewListData());
        }
        protected void OnPezStoreExportToExcelClicked(object sender, EventArgs e)
        {
            View.ExportPezStoreToExcel(GetBusinessStoreLsitForPezStore());
        }
        #endregion

        #region method
        protected void GetBusinessOrderData(int pageNumber)
        {
            Dictionary<Preferential, BusinessOrder> dataList = GetGridViewListDataByPageNumber(pageNumber);
            View.GetBusinessList(dataList);
        }

        private Dictionary<Preferential, BusinessOrder> GetGridViewListData()
        {
            return GetGridViewListData(_salesServ.BusinessOrderGetList(GetQuery()));
        }

        private Dictionary<Preferential, BusinessOrder> GetGridViewListDataByPageNumber(int pageNumber)
        {
            return GetGridViewListData(GetBusinessOrderLsitByPageNumber(pageNumber));
        }

        private Dictionary<Preferential, BusinessOrder> GetGridViewListData(IEnumerable<BusinessOrder> businessOrderList)
        {
            Dictionary<Preferential, BusinessOrder> dataList = new Dictionary<Preferential, BusinessOrder>();
            foreach (BusinessOrder businessOrder in businessOrderList)
            {
                foreach (Preferential preferential in businessOrder.Preferential)
                {
                    if (preferential.PreferentialMonths.Count > 0 || preferential.PreferentialDateTime.Count > 0)
                    {
                        preferential.Id = ObjectId.GenerateNewId();
                        dataList.Add(preferential, businessOrder);
                    }
                }
            }
            return dataList;
        }

        private IEnumerable<BusinessOrder> GetBusinessOrderLsitByPageNumber(int pageNumber)
        {
            return _salesServ.BusinessOrderGetListByPage(GetQuery(), SortBy.Ascending("ModifyTime"), View.PageSize, pageNumber);
        }

        private IEnumerable<BusinessOrder> GetBusinessStoreLsitForPezStore()
        {
            return _salesServ.BusinessOrderGetList(GetQuery());
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _salesServ.BusinessOrderGetList(GetQuery()).Count();
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            GetBusinessOrderData(e.Data);
        }
        #endregion

        #region Private Method
        private IMongoQuery GetQuery()
        {
            //List<QueryComplete> queryList = new List<QueryComplete>();
            List<IMongoQuery> queryList = new List<IMongoQuery>();

            // 狀態須為已複查之後始可查詢
            queryList.Add(Query.GTE("Status", new BsonInt32(Convert.ToInt32(BusinessOrderStatus.Review))));

            // 店家名稱
            if (!string.IsNullOrEmpty(View.SellerName))
                queryList.Add(Query.Matches("SellerName", new BsonRegularExpression(new Regex(View.SellerName))));

            // 工單編號
            if (!string.IsNullOrEmpty(View.BusinessOrderId))
                queryList.Add(Query.Matches("BusinessOrderId", new BsonRegularExpression(new Regex(View.BusinessOrderId))));

            // 業務部門
            if (View.DeptIdList.Count() > 0)
                queryList.Add(Query.In("SalesDeptId", new BsonArray(View.DeptIdList)));

            // 最後修改日期
            if (View.ModifyTimeStart != DateTime.MinValue && View.ModifyTimeEnd != DateTime.MinValue)
                queryList.Add(Query.And(Query.GTE("ModifyTime", new BsonDateTime(View.ModifyTimeStart)), Query.LTE("ModifyTime", new BsonDateTime(View.ModifyTimeEnd))));

            if (View.Type.Equals(ExportType.Magazine))
            {
                // 刊登日期
                queryList.Add(Query.Or(Query.ElemMatch("Preferential", Query.EQ("PreferentialDateTime", new BsonDateTime(View.PreferentialDateTime))), Query.And(Query.EQ("MagazineYear", new BsonInt32(View.PreferentialDateTime.Year - 1911)), Query.EQ("MagazineMonth", View.PreferentialDateTime.Month))));
            }
            else
            {
                // 特店折扣合約迄日期
                if (View.CreateTimeStart != DateTime.MinValue && View.CreateTimeEnd != DateTime.MinValue)
                    queryList.Add(Query.And(Query.GTE("PezStoreDateStart", new BsonDateTime(View.CreateTimeStart)), Query.LTE("PezStoreDateStart", new BsonDateTime(View.CreateTimeEnd))));
            }
            return Query.And(queryList.ToArray());
        }
        #endregion
    }
}

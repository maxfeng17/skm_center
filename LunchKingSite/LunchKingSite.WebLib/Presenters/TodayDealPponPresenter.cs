using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Presenters
{
    public class TodayDealPponPresenter : Presenter<ITodayDealPponView>
    {
        private IPponProvider pp;
        private ICmsProvider cp;
        private IMemberProvider mp;
        private IOrderProvider op;
        private ISysConfProvider conf;
        private ISellerProvider sp;
        private IEventProvider ep;
        private ICmsProvider cmp;
        private ILog Logger;

        public TodayDealPponPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            conf = ProviderFactory.Instance().GetConfig();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            Logger = LogManager.GetLogger("PponFacade");

        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            
            View.isMultipleMainDeals = conf.MultipleMainDeal;
            View.BypassSsoVerify = conf.BypassSsoVerify;
            View.EdmPopUpCacheName = conf.EdmPopUpCacheName;
            //View.SetFaceBookPageShow(Guid.Empty);

            int _city = View.DealCityId = View.CityId;
            var workCityId = _city == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                                               : _city;
            #region 所選取區域的檔次
            var multideals = SetMultipleDeals();

            #region 熱銷檔次設定
            int hotSaleBigTopSaleCount = 20;
            var hotDeals = multideals.OrderByDescending(x => x.PponDeal.ItemPrice * x.PponDeal.OrderedQuantity)
                                     .Take(multideals.Count > hotSaleBigTopSaleCount ? hotSaleBigTopSaleCount : multideals.Count)
                                     .ToList();
            View.SetHotSaleMutilpMainDeals(hotDeals);
            #endregion
            View.SetMutilpMainDeals(multideals);
            #endregion


            //判斷[馬上看]按鈕是否另開視窗
            View.IsOpenNewWindow = conf.DefaultPageOpenNewWindow;

            //收藏
            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            View.SetMemberCollection(mp.MemberCollectDealGetOnlineDealGuids(
                View.UserId, baseDate.AddDays(-1), baseDate.AddDays(1)));

            #region check areaCategoryId
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            if (city == null)
            {
                city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            }
            //目前城市沒有對應的頻道，目前應該只有24或72小時會這樣，直接設定為台北
            if (!city.ChannelId.HasValue)
            {
                city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            //需依據不同的狀況設定區域的categoryId，主要是針對旅遊需另外判斷
            int? areaCategoryId = null;
            //判斷是否為旅遊頻道
            if (workCityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                if (View.TravelCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    areaCategoryId = View.TravelCategoryId;
                }
            }
            else if (workCityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                if (View.FemaleCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    areaCategoryId = View.FemaleCategoryId;
                }
            }
            else
            {
                areaCategoryId = city.ChannelAreaId;
            }

            #endregion check areaCategoryId

            return true;
        }

        private List<MultipleMainDealPreview> SetMultipleDeals()
        {
            var dealList = NewPponDealSynopsesGetList();

            return dealList;
        }

        public List<MultipleMainDealPreview> NewPponDealSynopsesGetList()
        {
            List<MultipleMainDealPreview> previewList = new List<MultipleMainDealPreview>();
            DateTime date = PponFacade.GetOrderStartTimeAtPresent();

            List<MultipleMainDealPreview> dealList = new List<MultipleMainDealPreview>();

            List<PponDealPreviewData> matchPreviewDataList = PponDealPreviewManager.GetTodayDealPreviewDataList(date);

            #region 取cache的DealTimeSlot 用來判斷要隱藏的檔次
            List<IDealTimeSlot> timeSlots = ViewPponDealManager.DefaultManager.GetDealTimeSlotInEffectiveTime();

            //美食、宅配、旅遊、玩美.休閒、全家
            var allowedCityID = new[]{
                CategoryManager.pponCityId.Taipei,
                CategoryManager.pponCityId.Taoyuan,
                CategoryManager.pponCityId.Taichung,
                CategoryManager.pponCityId.Tainan,
                CategoryManager.pponCityId.Hsinchu,
                CategoryManager.pponCityId.Kaohsiung,
                PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId,
                PponCityGroup.DefaultPponCityGroup.Family.CityId
            };
            IDictionary<Guid, IDealTimeSlot> dicTimeSlots = timeSlots.Where(x => allowedCityID.Contains(x.CityId)).GroupBy(x => x.BusinessHourGuid).Select(y => y.First()).ToDictionary(x => x.BusinessHourGuid);
            #endregion

            if (matchPreviewDataList.Count != 0)
            {
                foreach (PponDealPreviewData _data in matchPreviewDataList)
                {
                    IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(_data.DealId);//pp.ViewPponDealGet(ViewPponDeal.Columns.UniqueId + "=" + _data.DealId);

                    #region 取得DealTimeSlot.DealTimeSlotStatus
                    DealTimeSlotStatus status = DealTimeSlotStatus.NotShowInPponDefault;
                    IDealTimeSlot dealTimeSlot = null;
                    if (dicTimeSlots.TryGetValue(_data.BusinessHourGuid, out dealTimeSlot))
                    {
                        status = (DealTimeSlotStatus)dealTimeSlot.Status;
                    }
                    #endregion

                    if (status != DealTimeSlotStatus.NotShowInPponDefault)
                    {
                        #region 要顯示的檔次，才做後續處理
                        if (ppdeal.IsHideContent != null && ppdeal.IsHideContent == false)
                        {
                            if (!string.IsNullOrEmpty(ppdeal.ContentName))
                            {
                                ppdeal.EventName = ppdeal.ContentName;
                            }
                        }

                        MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(dealTimeSlot.CityId, 0, ppdeal, null, status, 0);
                        mainDealPreview.PponDeal.DefaultDealImage = ImageFacade.GetMediaPathsFromRawData(mainDealPreview.PponDeal.EventImagePath, MediaType.PponDealPhoto).FirstOrDefault();
                        dealList.Add(mainDealPreview);
                        #endregion
                    }
                }
            }
            
            if (matchPreviewDataList.Count < 20)
            {
                //20170414 檔次小於20預設塞入宅配前七天開賣檔次
                List<PponDealPreviewData> dataList = new List<PponDealPreviewData>();
                int switchDay = 1;
                List<int> categoryCriteria = new List<int>();
                categoryCriteria.Add(CategoryManager.Default.Delivery.CategoryId);
                int _cityid = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;

                //如果沒有當日上檔的資料(假日)，往前撈七天
                do
                {
                    DateTime workDate = date.AddDays(-switchDay);

                    dataList = PponDealPreviewManager.GetTodayDealPreviewDataListWithCategory(workDate, categoryCriteria);

                    foreach (PponDealPreviewData _data in dataList)
                    {
                        IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(_data.DealId);

                        #region 取得DealTimeSlot.DealTimeSlotStatus
                        DealTimeSlotStatus status = DealTimeSlotStatus.NotShowInPponDefault;
                        IDealTimeSlot dealTimeSlot = null;
                        if (dicTimeSlots.TryGetValue(_data.BusinessHourGuid, out dealTimeSlot))
                        {
                            status = (DealTimeSlotStatus)dealTimeSlot.Status;
                        }
                        #endregion

                        if (status != DealTimeSlotStatus.NotShowInPponDefault)
                        {
                            #region 要顯示的檔次，才做後續處理
                            if (ppdeal.IsHideContent != null && ppdeal.IsHideContent == false)
                            {
                                if (!string.IsNullOrEmpty(ppdeal.ContentName))
                                {
                                    ppdeal.EventName = ppdeal.ContentName;
                                }
                            }

                            MultipleMainDealPreview mainDealPreview = new MultipleMainDealPreview(_cityid, 0, ppdeal, null, status, 0);
                            dealList.Add(mainDealPreview);
                            #endregion
                        }
                    }

                    //預備搜尋前一天的資料
                    switchDay++;
                } while (dataList.Count < 20 && switchDay < 8);
            }

            previewList.AddRange(dealList);
            previewList = previewList.OrderBy(x => x.CityID).ToList();
            return previewList;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckEventMail += OnCheckEventMail;
            return true;
        }

      
        protected void OnCheckEventMail(object sender, EventArgs e)
        {
            EventActivity activity = ep.EventActivityEdmGetList(-1)
                .FirstOrDefault(x => x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now);
            if (activity != null)
            {
                CmsRandomCityCollection cities = cmp.CmsRandomCityGetByPid(activity.Id, RandomCmsType.PopUpCities);
                KeyValuePair<EventActivity, CmsRandomCityCollection> pair = new KeyValuePair<EventActivity, CmsRandomCityCollection>(activity, cities);
                View.SetEventMainActivities(pair);
            }
            else
            {
                View.ShowEventEmail = false;
            }
        }

        #region public class methods

        public bool AddUserTrack(string sessionId, string displayName, string url, string queryString, string context)
        {
            return cp.UserTrackSet(sessionId, displayName, url, queryString, context);
        }

        #endregion public class methods
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.CustomException;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealReturnedPresenter : Presenter<IHiDealReturnedView>
    {
        #region properties

        protected static IHiDealProvider hp;
        protected static IMemberProvider mp;
        protected static ISysConfProvider config;
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        #endregion properties

        #region overload base methods

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            //檢查有無登入
            if (string.IsNullOrEmpty(View.UserName))
            {
                View.RedirectToLogin();
                return true;
            }
            //前置作業，讀取會員帳號相關的資料
            MemberUtility.SetUserSsoInfoReady(View.UserId);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            //每次都檢查有無登入
            if (string.IsNullOrWhiteSpace(View.UserName))
            {
                View.RedirectToLogin();
                return true;
            }
            View.OrderIdInput += OnOrderIdInput;
            View.ReasonInput += OnReasonInput;
            View.CouponReturn += OnCouponReturn;
            View.GoodsReturn += OnGoodsReturn;
            View.ToCouponRefund += OnToCouponRefund;
            View.ToGoodsRefund += OnToGoodsRefund;
            View.CouponCashBackConfirm += OnCouponCashBackConfirm;
            View.GoodsCashBackConfirm += OnGoodsCashBackConfirm;
            return true;
        }

        public HiDealReturnedPresenter(IHiDealProvider hiDeal, IMemberProvider memPro, ISysConfProvider configPro)
        {
            hp = hiDeal;
            mp = memPro;
            config = configPro;
        }

        #endregion overload base methods

        #region 委派用function

        /// <summary>
        /// 輸入訂單編號
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnOrderIdInput(object sender, DataEventArgs<string> e)
        {
            try
            {
                #region 檢查輸入是否正確

                string orderId = e.Data;
                if (string.IsNullOrWhiteSpace(orderId))
                {
                    //錯誤-請輸入訂單編號
                    throw new DataCheckException<HiDealReturnedViewErrorType>("請輸入訂單編號。",
                                                                              HiDealReturnedViewErrorType.OrderIdError);
                }

                #endregion 檢查輸入是否正確

                #region 檢查訂單資料

                var order = hp.HiDealOrderGetByOrderId(orderId, View.UserId);
                string message;
                HiDealOrderDetailCollection details;

                if (!HiDealReturnedManager.CheckOrderDataForAllReturn(order, false, out message, out details))
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>(message,
                                                                              HiDealReturnedViewErrorType.OrderIdError);
                }

                #endregion 檢查訂單資料

                View.IsCreateEinvoice = false;
                EinvoiceMain einvoice = op.EinvoiceMainGet(order.Guid, OrderClassification.HiDeal);

                if (einvoice.IsLoaded)
                {
                    if (string.IsNullOrEmpty(einvoice.InvoiceNumber))
                    {
                        //沒發票
                        View.IsCreateEinvoice = false;
                    }
                    else
                    {
                        //有發票
                        View.IsCreateEinvoice = true;
                    }
                }

                //判斷Order有無Credit_card_amt 
                View.IsCreditCardPay = false;
                if (order.CreditCardAmt > 0)
                {
                    View.IsCreditCardPay = true;
                }

                View.IsRefundCashOnly = false;
                CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.HiDeal);
                if (ctCol.Count() > 0 && ctCol.Where(x => x.CreditCard > 0 || x.Atm > 0).Count() > 0)
                {
                    View.IsRefundCashOnly = PaymentFacade.IsRefundCashOnly(ctCol.First());
                }

                var productDetails = details.Where(x => x.ProductType == (int)HiDealProductType.Product).ToList();
                if (productDetails.Count == 0)
                {
                    throw new Exception("HiDealOrder退貨，下面Order查無可退貨的orderDetail:orderId=" + orderId);
                }

                //檢查可退貨商品是否有兩筆以上
                if (productDetails.Count > 1)
                {
                    throw new Exception("HiDealOrder退貨，下面Order查有兩筆以上的商品可供退貨 orderDetail:orderId=" + orderId);
                }
                //目前規則上，一件訂單只會有一件商品，所以先預設已第一件商品來進行後續處理。
                var orderDetail = productDetails.First();
                //檢查檔次狀況
                var deal = hp.HiDealDealGet(orderDetail.HiDealId);
                var data = new HiDealReturnedViewReturnOrderData()
                {
                    OrderId = orderId,
                    OrderPk = order.Pk,
                    OrderDetailId = orderDetail.Id,
                    OrderDetailGuid = orderDetail.Guid,
                    DealEndTime = deal.DealEndTime == null ? DateTime.MinValue : deal.DealEndTime.Value
                };
                //檢查購買商品
                var product = hp.HiDealProductGet(orderDetail.ProductId);
                if (product.IsNoRefund)
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>("此訂單不接受退貨申請。",
                                                                              HiDealReturnedViewErrorType.OrderIdError);
                }

                //P好康，且有設定超過使用期限後不可退貨。
                if ((product.IsExpireNoRefund) && (DateTime.Now.Date.CompareTo((product.UseEndTime)) > 0))
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>("此商品超過使用期限後不可退貨。",
                                                          HiDealReturnedViewErrorType.OrderIdError);
                }

                //商品檔出貨回覆日超過七天鑑賞期不得申請退貨單的判定

                if ((orderDetail.DeliveryType == (int) HiDealDeliveryType.ToHouse))
                {
                    DateTime shippedDate = (product.ShippedDate.HasValue) ? Convert.ToDateTime(product.ShippedDate) : Convert.ToDateTime(product.UseEndTime);
                    if (DateTime.Now.Date.CompareTo(shippedDate.AddDays(config.GoodsAppreciationPeriod).Date) > 0)
                    {
                        throw new DataCheckException<HiDealReturnedViewErrorType>("商品已過鑑賞期。",
                                                              HiDealReturnedViewErrorType.OrderIdError);
                    }               
                }

                //有設定[結檔後七天不能退貨]
                if ((orderDetail.DeliveryType == (int)HiDealDeliveryType.ToShop) && (product.IsDaysNoRefund) && (DateTime.Now.Date.CompareTo(product.UseEndTime.Value.AddDays(7).Date) > 0))
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>("結檔後七天不能退貨。",
                                                          HiDealReturnedViewErrorType.OrderIdError);
                }


                //檢查憑證資料
                if ((orderDetail.DeliveryType == (int)HiDealDeliveryType.ToShop))
                {
                    var coupons = hp.ViewHiDealCouponTrustStatusGetList(0, 0, ViewHiDealCouponTrustStatus.Columns.Sequence,
                                                     ViewHiDealCouponTrustStatus.Columns.OrderPk + "=" + order.Pk
                                                    , ViewHiDealCouponTrustStatus.Columns.ProductId + "=" + orderDetail.ProductId
                                                    );
                    if (coupons.Where(x => x.LogSataus == (int)TrustStatus.Initial || x.LogSataus == (int)TrustStatus.Trusted).Count() == 0)
                    {
                        throw new DataCheckException<HiDealReturnedViewErrorType>("已使用完畢，無法退貨。",
                                                              HiDealReturnedViewErrorType.OrderIdError);
                    }
                    else if (coupons.Any(x => x.IsReservationLock == true)) 
                    {
                        throw new DataCheckException<HiDealReturnedViewErrorType>("訂單已預約，無法申請。",
                                                              HiDealReturnedViewErrorType.OrderIdError);
                    }
                }

                //進入下一階段，填寫退貨原因
                View.ShowReturnReasonPage(data, string.Empty);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                View.ShowOrderIdInputError(ex.Message);
                return;
            }
        }

        /// <summary>
        /// 輸入退貨原因
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnReasonInput(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            var reason = e.Data.Reason;
            var orderDetailId = e.Data.OrderDetailId;
            try
            {
                //檢查有無輸入退貨原因
                if (string.IsNullOrWhiteSpace(reason))
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>("請填寫退貨原因。",
                                                                              HiDealReturnedViewErrorType.ReasonError);
                }

                #region 依據訂單取貨方式，到店或宅配，進行後續處理

                //查詢要退貨的訂單明細資料(OrderDetail)
                var orderDetail = hp.HiDealOrderDetailGet(orderDetailId);
                if (orderDetail.DeliveryType == (int)HiDealDeliveryType.ToShop)
                {
                    //到店
                    View.ShowCouponReturnPage(e.Data);
                }
                else
                {
                    //宅配
                    View.ShowGoodsReturnPage(e.Data);
                }

                #endregion 依據訂單取貨方式，到店或宅配，進行後續處理
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    case HiDealReturnedViewErrorType.ReasonError:
                        //輸入資料錯誤，頁面停留於原因輸入的狀態，並寫是錯誤訊息。
                        View.ShowReturnReasonPage(e.Data, ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        /// <summary>
        /// 憑證退貨確認--退購物金
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCouponReturn(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            try
            {
                var orderPk = e.Data.OrderPk;
                var reason = e.Data.Reason;
                HiDealReturned returned;

                ReturnedCreateAllReturn(orderPk, false, reason, false, out returned);
                //寄出退貨申請書
                SendRefundCouponNotificationFormMail(returned.Id, orderPk, View.UserName);
                //憑證商品確認退貨成功先補可用憑證
                GenerateHiDealCouponByRefundId(returned);
                //顯示憑證退貨完成頁面--退回購物金
                View.ShowCouponPointBackSuccess(e.Data, returned);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        /// <summary>
        /// 商品退貨確認--退購物金
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGoodsReturn(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            try
            {
                var orderPk = e.Data.OrderPk;
                var reason = e.Data.Reason;
                HiDealReturned returned;
                //是否直接退款 未結檔為true，已結檔則為false
                var isRefund = DateTime.Now < e.Data.DealEndTime;
                //成立退貨單，未結檔則一併退款
                ReturnedCreateAllReturn(orderPk, false, reason, false, out returned);
                //todo:Sam 20130621 改寄出(商品)退貨申請通知
                SendRefundGoodsNotificationFormMail(returned.Id, orderPk, View.UserName);
                //顯示憑證退貨申請完成頁面--退回購物金
                View.ShowGoodsPointBackSuccess(e.Data, returned);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        /// <summary>
        /// 依據商品資料，判斷是ATM退款還是刷退，並顯示對應畫面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnToCouponRefund(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            try
            {
                //sam 轉向憑證-刷退頁面 20130620 不再判斷是否結檔
                View.ShowCouponCashBack(e.Data);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        /// <summary>
        /// 依據商品資料，判斷是ATM退款還是刷退，並顯示對應畫面
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnToGoodsRefund(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            View.ShowGoodsCashBack(e.Data);
        }

        /// <summary>
        /// 憑證商品確認進行信用卡刷退申請
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCouponCashBackConfirm(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            try
            {
                var orderPk = e.Data.OrderPk;
                var reason = e.Data.Reason;
                //建立退貨單
                HiDealReturned returned;
                ReturnedCreateAllReturn(orderPk, true, reason, false, out returned);
                //寄出退貨申請書
                //SendRefundFormMail(returned.Id, orderPk, View.UserName); 
                SendRefundCouponNotificationFormMail(returned.Id, orderPk, View.UserName);
                //憑證商品確認退貨成功先補可用憑證
                GenerateHiDealCouponByRefundId(returned);
                //顯示退貨申請完成頁面
                View.ShowCouponCashBackSuccess(e.Data, returned);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        /// <summary>
        /// 宅配商品確認進行信用卡刷退申請
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGoodsCashBackConfirm(object sender, DataEventArgs<HiDealReturnedViewReturnOrderData> e)
        {
            try
            {
                var orderPk = e.Data.OrderPk;
                var reason = e.Data.Reason;
                //建立退貨單
                HiDealReturned returned;
                ReturnedCreateAllReturn(orderPk, true, reason, false, out returned);
                //寄出退貨申請書
                SendRefundGoodsNotificationFormMail(returned.Id, orderPk, View.UserName);
                //顯示退貨申請完成頁面
                View.ShowGoodsCashBackSuccess(e.Data, returned);
            }
            catch (DataCheckException<HiDealReturnedViewErrorType> ex)
            {
                switch (ex.ErrorType)
                {
                    case HiDealReturnedViewErrorType.OrderIdError:
                        //訂單編號檢查錯誤，導回訂單頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;

                    default:
                        //預設退回訂單編號輸入頁
                        View.ShowOrderIdInputError(ex.Message);
                        break;
                }
                return;
            }
        }

        #endregion 委派用function

        /// <summary>
        /// 檢查訂單狀況並產生退貨單-全部退貨(憑證會退回尚未核銷的，商品會退回尚未退貨的 所有商品)
        /// </summary>
        /// <param name="orderPk"></param>
        /// <param name="cashBack"></param>
        /// <param name="reason">退貨原因</param>
        /// <param name="isRefund">直接退款</param>
        /// <param name="returned">建立的退貨單</param>
        /// <returns></returns>
        private bool ReturnedCreateAllReturn(int orderPk, bool cashBack, string reason, bool isRefund, out HiDealReturned returned)
        {
            #region 檢查訂單資料

            var order = hp.HiDealOrderGet(orderPk);

            string message;
            HiDealOrderDetailCollection orderDetails;
            if (!HiDealReturnedManager.CheckOrderDataForAllReturn(order, false, out message, out orderDetails))
            {
                throw new DataCheckException<HiDealReturnedViewErrorType>(message,
                                                                          HiDealReturnedViewErrorType.OrderIdError);
            }

            #endregion 檢查訂單資料

            //建立退貨單
            returned = new HiDealReturned();
            if (
                !HiDealReturnedManager.HiDealReturnedSetByOrderWithAllReturn(order, cashBack, View.UserName, reason,
                                                                             out returned))
            {
                throw new Exception("無法建立退貨單，請去查看前面的LOG HiDealOrder.pk:" + order.Pk);
            }

            if (isRefund)
            {
                if (!HiDealReturnedManager.RefundWork(returned.Id, View.UserName, false, false, out message))
                {
                    //修改OrderShow退款失敗
                    HiDealOrderFacade.HiDealOrderShowUpdate(orderPk, HiDealOrderShowStatus.RefundFail, true);
                    throw new Exception("無法退款，請去查看前面的LOG HiDealOrder.pk:" + order.Pk);
                }
                else
                {
                    //修改OrderShow退貨完成。
                    HiDealOrderFacade.HiDealOrderShowUpdate(orderPk, HiDealOrderShowStatus.RefundSuccess, true);
                }
            }
            else
            {
                //修改OrderShow狀態為退貨中
                HiDealOrderFacade.HiDealOrderShowUpdate(orderPk, HiDealOrderShowStatus.RefundProcessing, true);
            }

            return true;
        }

        /// <summary>
        /// 憑證申請退貨成功，先補產憑證
        /// </summary>
        /// <param name="returned">退貨單Id</param>
        private void GenerateHiDealCouponByRefundId(HiDealReturned returned)
        {
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returned.Id);

            if (returnedDetails.Count == 0)
            {
                throw new DataCheckException<HiDealReturnedViewErrorType>("查無此退貨單明細資料",
                                                                          HiDealReturnedViewErrorType.OrderIdError);
            }

            //查詢訂單資料
            var hidealOrder = hp.HiDealOrderGet(returned.OrderPk);
            //查詢登記要退貨的coupon資料
            var returnedCoupons = hp.HiDealReturnedCouponGetList(returned.Id);
            //先取出此訂單所有的CashTrustLog，減少分次查詢的壓力
            var cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(hidealOrder.Guid, OrderClassification.HiDeal);

            #region 整理所以要處理的CashTrustLog

            //憑證退貨申請成功先行補產憑證動作。
            foreach (var returnedDetail in returnedDetails)
            {
                var returnedOptionIds = HiDealReturnedManager.ReturnedDetailGetReturnedOptionIds(returnedDetail);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product && returnedOptionIds.Count > 0 && returnedDetail.StoreGuid != null)
                {
                    hp.HiDealProductOptionItemUpdateSellQuantity(returnedOptionIds, (-returnedDetail.ItemQuantity));
                }
            }

            //可以進行退貨的所有cashtrustLog
            var cashTrustLogsForReturn = new List<CashTrustLog>();
            foreach (var returnedDetail in returnedDetails)
            {
                //運費另外處理
                if (returnedDetail.ProductType == (int)HiDealProductType.Freight)
                {
                    continue;
                }
                var allowTrustStatus =
                    HiDealReturnedManager.GetTrustStatusWithAllowReturns(
                        (HiDealDeliveryType)returnedDetail.DeliveryType);
                var returnCouponIds = (from returnedCoupon in returnedCoupons
                                       where returnedCoupon.ReturnedDetailId == returnedDetail.Id
                                       select returnedCoupon.CouponId).ToList();
                var trustLogs =
                    cashTrustLogs.Where(
                        x =>
                        x.CouponId != null &&
                        (allowTrustStatus.Contains((TrustStatus)x.Status) &&
                         returnCouponIds.Contains((long)x.CouponId))).ToList();
                cashTrustLogsForReturn.AddRange(trustLogs);
            }

            #endregion 整理所以要處理的CashTrustLog

            if (cashTrustLogsForReturn.Count == 0)
            {
                throw new DataCheckException<HiDealReturnedViewErrorType>("查無可退貨憑證",
                                                                          HiDealReturnedViewErrorType.OrderIdError);
            }

            foreach (var cashTrustLog in cashTrustLogsForReturn)
            {
                HiDealCoupon newCoupon;
                if (cashTrustLog.CouponId != null)
                {
                    var coupon = hp.HiDealCouponGet((long)cashTrustLog.CouponId);
                    //檢查是否為有效憑證
                    if (coupon.IsLoaded && coupon.CouponIsEnabled())
                    {
                        HiDealCouponManager.GenerateHiDealCouponByRefundCouponId(coupon, out newCoupon);
                    }
                }
            }
        }

        /// <summary>
        /// 寄發(憑證)退貨申請通知Mail
        /// </summary>
        /// <param name="returnedId">退貨單ID</param>
        /// <param name="orderPk">憑證訂單號碼</param>
        /// <param name="userName">目前使用者名稱</param>
        /// <returns>寄發是否完成</returns>
        /// 
        private bool SendRefundCouponNotificationFormMail(int returnedId, int orderPk, string userName)
        {
            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                //查無退貨單
                return false;
            }
            //退貨申請書PDF URL
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returnedId, orderPk, userName);
            var refundFormUrl = config.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;

            //折讓單PDF URL
            var invoiceMailbackAllowanceUrl = config.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;

            //查詢退貨明細
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);
            string dealName = string.Empty;
            string productName = string.Empty;

            foreach (var returnedDetail in returnedDetails)
            {
                var deal = hp.HiDealDealGet(returnedDetail.HiDealId);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product)
                {
                    dealName = deal.Name;
                    productName = returnedDetail.ProductName;
                }
            }

            Member member = mp.MemberGet(returned.UserId);

            var mailData = new ApplyRefundCouponNotificationInformation
            {
                BuyerName = member.DisplayName,
                OrderItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundItemName = string.Format("{0} {1}", dealName, productName),
                RefundItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundApplicationUrl = refundFormUrl,
                IsCreateEinvoice = View.IsCreateEinvoice,
                InvoiceMailbackAllowanceUrl = invoiceMailbackAllowanceUrl,
                HDmailHeaderUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                HDmailFooterUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
            };
            HiDealMailFacade.SendRefundCouponNotification(View.UserName, mailData);

            return true;
        }

        /// <summary>
        /// 寄發(商品)退貨申請通知Mail
        /// </summary>
        /// <param name="returnedId">退貨單ID</param>
        /// <param name="orderPk">憑證訂單號碼</param>
        /// <param name="userName">目前使用者名稱</param>
        /// <returns>寄發是否完成</returns>
        private bool SendRefundGoodsNotificationFormMail(int returnedId, int orderPk, string userName)
        {
            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                //查無退貨單
                return false;
            }

            //退貨申請書PDF URL
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returnedId, orderPk, userName);
            var refundFormUrl = config.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;

            //折讓單PDF URL
            var invoiceMailbackAllowanceUrl = config.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;

            //查詢退貨明細
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);
            string dealName = string.Empty;
            string productName = string.Empty;

            foreach (var returnedDetail in returnedDetails)
            {
                var deal = hp.HiDealDealGet(returnedDetail.HiDealId);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product)
                {
                    dealName = deal.Name;
                    productName = returnedDetail.ProductName;
                }
            }

            Member member = mp.MemberGet(returned.UserId);

            var mailData = new ApplyRefundGoodsNotificationInformation
            {
                BuyerName = member.DisplayName,
                OrderItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundItemName = string.Format("{0} {1}", dealName, productName),
                RefundItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundApplicationUrl = refundFormUrl,
                IsCreateEinvoice = View.IsCreateEinvoice,
                InvoiceMailbackAllowanceUrl = invoiceMailbackAllowanceUrl,
                HDmailHeaderUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                HDmailFooterUrl = config.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
            };
            HiDealMailFacade.SendRefundGoodsNotification(View.UserName, mailData);

            return true;
        }
    }
}

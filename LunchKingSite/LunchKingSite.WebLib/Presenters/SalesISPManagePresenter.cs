﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using System.Web;
using LunchKingSite.Core.Component;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesISPManagePresenter : Presenter<ISalesISPManageView>
    {
        private static ISellerProvider _sp;
        private static ISysConfProvider _config;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            View.Export += OnExport;
            View.Import += OnImport;
            return true;
        }

        public SalesISPManagePresenter()
        {
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _sp.ViewVbsInstorePickupCollectionGetCount(GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void OnExport(object sender, EventArgs e)
        {
            ExportData(1);
        }
        protected void OnImport(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.FUExport.PostedFile.FileName))
            {
                if (Path.GetExtension(View.FUExport.PostedFile.FileName) == ".xls")
                {
                    HSSFWorkbook workbook = new HSSFWorkbook(View.FUExport.PostedFile.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);
                    Row row;
                    List<string> ImportError = new List<string>();
                    int EmportCount = 0;
                    for (int i = 1; i <= currentSheet.LastRowNum; i++)
                    {
                        row = currentSheet.GetRow(i);
                        if ((row.GetCell(0) == null || string.IsNullOrEmpty(row.GetCell(0).ToString())) && (row.GetCell(1) == null || string.IsNullOrEmpty(row.GetCell(1).ToString())) &&
                            (row.GetCell(2) == null || string.IsNullOrEmpty(row.GetCell(2).ToString())) && (row.GetCell(3) == null || string.IsNullOrEmpty(row.GetCell(3).ToString())) &&
                            (row.GetCell(4) == null || string.IsNullOrEmpty(row.GetCell(4).ToString())) && (row.GetCell(5) == null || string.IsNullOrEmpty(row.GetCell(5).ToString())) &&
                            (row.GetCell(6) == null || string.IsNullOrEmpty(row.GetCell(6).ToString())) && (row.GetCell(7) == null || string.IsNullOrEmpty(row.GetCell(7).ToString())) &&
                            (row.GetCell(8) == null || string.IsNullOrEmpty(row.GetCell(8).ToString())) && (row.GetCell(9) == null || string.IsNullOrEmpty(row.GetCell(9).ToString())) &&
                            (row.GetCell(10) == null || string.IsNullOrEmpty(row.GetCell(10).ToString())) && (row.GetCell(11) == null || string.IsNullOrEmpty(row.GetCell(11).ToString())) &&
                            (row.GetCell(12) == null || string.IsNullOrEmpty(row.GetCell(12).ToString())) && (row.GetCell(13) == null || string.IsNullOrEmpty(row.GetCell(13).ToString())) &&
                            (row.GetCell(14) == null || string.IsNullOrEmpty(row.GetCell(14).ToString())) && (row.GetCell(15) == null || string.IsNullOrEmpty(row.GetCell(15).ToString())) &&
                            (row.GetCell(16) == null || string.IsNullOrEmpty(row.GetCell(16).ToString())) && (row.GetCell(17) == null || string.IsNullOrEmpty(row.GetCell(17).ToString())) &&
                            (row.GetCell(18) == null || string.IsNullOrEmpty(row.GetCell(18).ToString())))
                        {
                            //空白行即結束
                            break;
                        }



                        if (row.GetCell(0) != null && !string.IsNullOrEmpty(row.GetCell(0).ToString()))
                        {
                            string sellerId = row.GetCell(0).ToString();
                            int parentId = Convert.ToInt32(row.GetCell(3).ToString());
                            ServiceChannel channel = parentId == _config.FamilyParentId ? ServiceChannel.FamilyMart : ServiceChannel.SevenEleven;
                            Seller s = _sp.SellerGet(Seller.Columns.SellerId, sellerId);
                            if (s.IsLoaded)
                            {

                                VbsInstorePickup vip = _sp.VbsInstorePickupGet(s.Guid, (int)channel);
                                if (vip.IsLoaded)
                                {
                                    //匯入的狀態
                                    bool storeCreate = (row.GetCell(16) == null || string.IsNullOrEmpty(row.GetCell(16).ToString()) || Convert.ToInt32(row.GetCell(16).ToString()) == 0) ? false : true;
                                    bool checking = (row.GetCell(17) == null || string.IsNullOrEmpty(row.GetCell(17).ToString()) || Convert.ToInt32(row.GetCell(17).ToString()) == 0) ? false : true;
                                    bool complete = (row.GetCell(18) == null || string.IsNullOrEmpty(row.GetCell(18).ToString()) || Convert.ToInt32(row.GetCell(18).ToString()) == 0) ? false : true;

                                    if ((!storeCreate && (checking || complete)) || (!checking && complete))
                                    {
                                        //狀態不一致
                                        ImportError.Add("第" + i.ToString() + "列");
                                    }
                                    else
                                    {
                                        if (storeCreate && vip.Status == (int)ISPStatus.Verifying)
                                        {
                                            //尚未審核且要審核

                                            string msg = "";

                                            var contacts = new ReturnContracts();
                                            contacts = new JsonSerializer().Deserialize<ReturnContracts>(vip.Contacts);

                                            ISPFacade.ISPApply(s.Guid, (ServiceChannel)vip.ServiceChannel, vip.ReturnCycle ?? 0, vip.ReturnType ?? 0, vip.ReturnOther, View.UserName, vip.SellerAddress, contacts.ContactPersonName, contacts.SellerTel, contacts.SellerMobile, contacts.ContactPersonEmail, out msg, false);
                                            if (msg == "送出申請成功")
                                            {
                                                vip.Status = (int)ISPStatus.StoreCreate;
                                                EmportCount++;
                                                continue;//業務審核分開執行
                                            }
                                            else
                                            {
                                                ImportError.Add("第" + i.ToString() + "列");
                                                continue;
                                            }
                                        }

                                        if (vip.ServiceChannel == (int)ServiceChannel.SevenEleven && checking && vip.Status == (int)ISPStatus.StoreCreate)
                                        {
                                            //尚未建檔且要建檔
                                            string result = ISPFacade.UpdateStoreApplicationStatus(vip.SellerGuid, vip.ServiceChannel, (int)ApplicationStatusCode.Verifying);
                                            if (result == ApiResultCode.Success.ToString())
                                            {
                                                vip.Status = (int)ISPStatus.Checking;//為了後續可繼續測標
                                            }
                                            else
                                            {
                                                ImportError.Add("第" + i.ToString() + "列");
                                                continue;

                                            }
                                        }

                                        if (vip.ServiceChannel == (int)ServiceChannel.SevenEleven && complete && vip.Status == (int)ISPStatus.Checking)
                                        {
                                            //尚未測標且要測標
                                            string result = ISPFacade.UpdateStoreApplicationStatus(vip.SellerGuid, vip.ServiceChannel, (int)ApplicationStatusCode.Pass);
                                            if (result == ApiResultCode.Success.ToString())
                                            {
                                                vip.Status = (int)ISPStatus.Complete;
                                            }
                                            else
                                            {
                                                ImportError.Add("第" + i.ToString() + "列");
                                                continue;

                                            }
                                        }
                                        else if (vip.ServiceChannel == (int)ServiceChannel.SevenEleven && !complete && vip.Status == (int)ISPStatus.Checking && (row.GetCell(18) != null && !string.IsNullOrEmpty(row.GetCell(18).ToString())))
                                        {
                                            //測標失敗
                                            string result = ISPFacade.UpdateStoreApplicationStatus(vip.SellerGuid, vip.ServiceChannel, (int)ApplicationStatusCode.Fail);
                                            if (result == ApiResultCode.Success.ToString())
                                            {
                                                vip.Status = (int)ISPStatus.Failed;
                                            }
                                            else
                                            {
                                                ImportError.Add("第" + i.ToString() + "列");
                                                continue;

                                            }
                                        }

                                        EmportCount++;

                                    }

                                }
                                else
                                {
                                    ImportError.Add("第" + i.ToString() + "列");
                                }
                            }
                            else
                            {
                                ImportError.Add("第" + i.ToString() + "列");
                            }
                        }
                        else
                        {
                            ImportError.Add("第" + i.ToString() + "列");
                        }
                    }


                    //執行job更新回本站
                    var job = new ISPUpdateStoreStatus();
                    job.Execute(null);


                    if (ImportError.Count() > 0)
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆\\n失敗筆數：" + ImportError.Count().ToString() + "筆\\n失敗清單：" + string.Join("、", ImportError));
                    }
                    else
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆");
                    }
                }
                else
                {
                    View.ShowMessage("請選擇.xls檔案");
                }
            }
            else
            {
                View.ShowMessage("請選擇匯入檔案");
            }
        } 
        #endregion


        #region private Method
        private void LoadData(int pageNumber)
        {
            var data = _sp.ViewVbsInstorePickupCollectionGet(pageNumber, View.PageSize, GetFilter());

            View.SetISPList(data);
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();
            if (!string.IsNullOrEmpty(View.SellerName))
            {
                filter.Add(ViewVbsInstorePickup.Columns.SellerName + " like %" + View.SellerName + "%");
            }
            if (!string.IsNullOrEmpty(View.SellerId))
            {
                filter.Add(ViewVbsInstorePickup.Columns.SellerId + " = " + View.SellerId);
            }
            if (!string.IsNullOrEmpty(View.ISPStatus))
            {
                filter.Add(ViewVbsInstorePickup.Columns.Status + " = " + int.Parse(View.ISPStatus));
            }
            if (!string.IsNullOrEmpty(View.ServiceChannel))
            {
                filter.Add(ViewVbsInstorePickup.Columns.ServiceChannel + " = " + int.Parse(View.ServiceChannel));
            }
            if (!string.IsNullOrEmpty(View.StartDate))
            {
                filter.Add(ViewVbsInstorePickup.Columns.CreateTime + " >= " + DateTime.Parse(View.StartDate));
            }
            if (!string.IsNullOrEmpty(View.EndDate))
            {
                filter.Add(ViewVbsInstorePickup.Columns.CreateTime + " < " + DateTime.Parse(View.EndDate).AddDays(1));
            }

            return filter.ToArray();
        }

        
        private void ExportData(int pageNumber)
        {
            string[] filters = GetFilter();

            var data = _sp.ViewVbsInstorePickupCollectionGet(pageNumber, 99999, GetFilter());

            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("商家超取申請檔");

            HSSFCellStyle yellowStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            yellowStyle.BackgroundColor(HSSFColor.YELLOW.index);

            HSSFCellStyle greyStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            greyStyle.BackgroundColor(HSSFColor.GREY_25_PERCENT.index);

            sheet.SetColumnWidth(0, 3000);
            sheet.SetColumnWidth(1, 3000);
            sheet.SetColumnWidth(2, 3000);
            sheet.SetColumnWidth(3, 3000);
            sheet.SetColumnWidth(4, 3000);
            sheet.SetColumnWidth(5, 10000);
            sheet.SetColumnWidth(6, 3000);
            sheet.SetColumnWidth(7, 10000);
            sheet.SetColumnWidth(8, 3500);
            sheet.SetColumnWidth(9, 3500);
            sheet.SetColumnWidth(10, 3500);
            sheet.SetColumnWidth(11, 3000);
            sheet.SetColumnWidth(12, 3000);
            sheet.SetColumnWidth(13, 5000);
            sheet.SetColumnWidth(14, 3000);
            sheet.SetColumnWidth(15, 3000);
            sheet.SetColumnWidth(16, 3000);
            sheet.SetColumnWidth(17, 3000);
            sheet.SetColumnWidth(18, 3000);


            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("商家編號");
            cols.CreateCell(1).SetCellValue("超商類型");
            cols.CreateCell(2).SetCellValue("生效日");
            cols.CreateCell(3).SetCellValue("母代號");
            cols.CreateCell(4).SetCellValue("子代號");
            cols.CreateCell(5).SetCellValue("廠商名稱");
            cols.CreateCell(6).SetCellValue("公司別名");
            cols.CreateCell(7).SetCellValue("公司統一名稱");
            cols.CreateCell(8).SetCellValue("子廠商連絡人");
            cols.CreateCell(9).SetCellValue("廠商聯絡電話");
            cols.CreateCell(10).SetCellValue("廠商電子信箱");
            cols.CreateCell(11).SetCellValue("服務類型");
            cols.CreateCell(12).SetCellValue("退貨週期");
            cols.CreateCell(13).SetCellValue("退貨方式");
            cols.CreateCell(14).SetCellValue("退貨地址");
            cols.CreateCell(15).SetCellValue("退貨代碼");
            cols.CreateCell(16).SetCellValue("審核結果");
            cols.CreateCell(17).SetCellValue("建檔結果");
            cols.CreateCell(18).SetCellValue("測標結果");


            cols.GetCell(0).CellStyle = yellowStyle;
            cols.GetCell(1).CellStyle = yellowStyle;

            cols.GetCell(2).CellStyle = greyStyle;
            cols.GetCell(3).CellStyle = greyStyle;
            cols.GetCell(4).CellStyle = greyStyle;
            cols.GetCell(5).CellStyle = greyStyle;
            cols.GetCell(6).CellStyle = greyStyle;
            cols.GetCell(7).CellStyle = greyStyle;
            cols.GetCell(8).CellStyle = greyStyle;
            cols.GetCell(9).CellStyle = greyStyle;
            cols.GetCell(10).CellStyle = greyStyle;
            cols.GetCell(11).CellStyle = greyStyle;
            cols.GetCell(12).CellStyle = greyStyle;
            cols.GetCell(13).CellStyle = greyStyle;
            cols.GetCell(14).CellStyle = greyStyle;
            cols.GetCell(15).CellStyle = greyStyle;

            cols.GetCell(16).CellStyle = yellowStyle;
            cols.GetCell(17).CellStyle = yellowStyle;
            cols.GetCell(18).CellStyle = yellowStyle;


            int i = 0;
            foreach (ViewVbsInstorePickup store in data)
            {
                if (store != null && store.IsLoaded)
                {
                    var contacts = new ReturnContracts();
                    contacts = new JsonSerializer().Deserialize<ReturnContracts>(store.Contacts);

                    cols = sheet.CreateRow(i + 1);
                    cols.CreateCell(0).SetCellValue(store.SellerId);
                    cols.CreateCell(1).SetCellValue(Helper.GetDescription((ServiceChannel)store.ServiceChannel));
                    cols.CreateCell(2).SetCellValue(store.CreateTime.ToShortDateString());
                    cols.CreateCell(3).SetCellValue(store.ServiceChannel == (int)ServiceChannel.FamilyMart ? _config.FamilyParentId.ToString() : _config.SevenParentId.ToString());
                    cols.CreateCell(4).SetCellValue(store.StoreSubCode);
                    cols.CreateCell(5).SetCellValue("17Life_" + store.SellerName);
                    cols.CreateCell(6).SetCellValue("17Life");
                    if (store.ServiceChannel == (int)ServiceChannel.FamilyMart)
                        cols.CreateCell(7).SetCellValue("康太數位整合股份有限公司");
                    else
                        cols.CreateCell(7).SetCellValue(store.SellerName);

                    cols.CreateCell(8).SetCellValue(contacts.ContactPersonName);
                    cols.CreateCell(9).SetCellValue(contacts.SellerTel);
                    cols.CreateCell(10).SetCellValue(contacts.ContactPersonEmail);
                    cols.CreateCell(11).SetCellValue("二者皆申請");
                    if (store.ServiceChannel == (int)ServiceChannel.FamilyMart)
                        cols.CreateCell(12).SetCellValue(Helper.GetDescription((RrturnCycle)store.ReturnCycle));
                    else
                    {
                        if (store.ReturnCycle == (int)RrturnCycle.Daily)
                            cols.CreateCell(12).SetCellValue("日退");
                        else
                            cols.CreateCell(12).SetCellValue("週退");
                    }
                    cols.CreateCell(13).SetCellValue(Helper.GetDescription((RetrunShip)store.ReturnType) + (store.ReturnType == (int)RetrunShip.Other ? "-" + store.ReturnOther : ""));
                    cols.CreateCell(14).SetCellValue(store.SellerAddress);
                    cols.CreateCell(15).SetCellValue("");
                    cols.CreateCell(16).SetCellValue(store.Status >= (int)ISPStatus.StoreCreate ? "1" : "");
                    cols.CreateCell(17).SetCellValue(store.Status >= (int)ISPStatus.Checking ? "1" : "");
                    cols.CreateCell(18).SetCellValue(store.Status == (int)ISPStatus.Complete ? "1" : store.Status == (int)ISPStatus.Failed ? "0" : "");


                    i++;
                }
            }

            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", DateTime.Now.ToString("yyyyMMdd") + HttpUtility.UrlEncode(@"商家超取申請檔.xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);


        } 
        #endregion

        
    }
}

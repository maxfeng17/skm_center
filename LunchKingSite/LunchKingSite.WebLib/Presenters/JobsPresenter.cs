﻿using System;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core.Component;
using System.Net;

namespace LunchKingSite.WebLib.Presenters
{
    public class JobsPresenter : Presenter<IJobsView>
    {
        ISystemProvider sp;

        public JobsPresenter()
        {
            sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            string default_host_name = sp.SystemCodeGetName(SingleOperator.SingleJob.ToString(), 1);
            View.IsJobLoadBalanceProceed = default_host_name.Equals(Dns.GetHostName(), StringComparison.CurrentCultureIgnoreCase);
            if (!string.IsNullOrEmpty(View.JobName))
            {
                JobContext jc = Jobs.Instance.FindJob(View.JobName);
                if (jc != null)
                {
                    View.SetDetailMode(jc);
                    return true;
                }
            }

            View.SetListMode(Jobs.Instance.JobList);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Fire += OnFire;
            View.PlayPause += OnPlayPause;
            View.PlayPauseAll += OnPlayPauseAll;
            View.Remove += OnRemove;
            View.TimerTick += OnTimerTick;
            return true;
        }

        #region Event Handlers
        private void OnRemove(object sender, DataEventArgs<string> e)
        {
            JobContext jc = Jobs.Instance.FindJob(e.Data);
            if (jc == null)
                return;

            Jobs.Instance.RemoveJob(jc.Setting.Name);
            View.SetListMode(Jobs.Instance.JobList);
        }

        private void OnShowList(object sender, EventArgs e)
        {
            View.SetListMode(Jobs.Instance.JobList);
        }

        private void OnPlayPauseAll(object sender, EventArgs e)
        {
            bool allPaused = true;
            foreach (var js in Jobs.Instance.JobList)
                allPaused &= (js.Status == JobStatus.Paused || js.Status == JobStatus.Disabled || js.Status == JobStatus.Elapsed); // ignore disabled & elapsed job

            foreach (var js in Jobs.Instance.JobList)
                if (js.Status != JobStatus.Disabled && js.Status != JobStatus.Elapsed)
                    Jobs.Instance.ControlJob(js.Setting.Name, !allPaused);
        }

        private void OnPlayPause(object sender, DataEventArgs<string> e)
        {
            JobContext jc = Jobs.Instance.FindJob(e.Data);
            if (jc == null)
                return;

            Jobs.Instance.ControlJob(jc.Setting.Name, jc.Status == JobStatus.Queueing);
            View.SetDetailMode(jc);
        }

        private void OnFire(object sender, DataEventArgs<string> e)
        {
            JobContext jc = Jobs.Instance.FindJob(e.Data);
            if (jc == null)
                return;

            Jobs.Instance.FireJob(jc.Setting.Name);
            View.SetDetailMode(jc);
        }

        private void OnTimerTick(object sender, DataEventArgs<string> e)
        {
            string default_host_name = sp.SystemCodeGetName(SingleOperator.SingleJob.ToString(), 1);
            View.IsJobLoadBalanceProceed = default_host_name.Equals(Dns.GetHostName(), StringComparison.CurrentCultureIgnoreCase);
            if (string.IsNullOrEmpty(e.Data))
                OnShowList(sender, null);
            else
                View.SetDetailMode(Jobs.Instance.FindJob(e.Data));
        }
        #endregion
    }
}

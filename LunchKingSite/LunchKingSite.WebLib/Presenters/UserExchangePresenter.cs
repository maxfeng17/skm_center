﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserExchangePresenter : Presenter<IUserExchange>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.TxtOrderId.Attributes.Add("placeholder", "範例：AA-00-000-000000-000");
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetOrderInfo += OnGetOrderInfo;
            View.RequestExchange += OnRequestExchange;
            return true;
        }

        private void OnRequestExchange(object sender, ExchangeFormArgs e)
        {
            try
            {
                var now = DateTime.Now;
                var rtnl = new OrderReturnList
                {
                    OrderGuid = View.OrderGuid,
                    Type = (int)OrderReturnType.Exchange,
                    Reason = e.ExchangeReason,
                    Status = (int)OrderReturnStatus.SendToSeller,
                    VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing,
                    CreateTime = now,
                    CreateId = View.UserName,
                    ModifyTime = now,
                    ModifyId = View.UserName,
                    ReceiverName = e.ReceiverName,
                    ReceiverAddress = e.ReceiverAddress
                };
                
                //避免重複點選確認按鈕 重複新增換貨單
                if (OrderExchangeUtility.HasProcessingExchangeLog(View.OrderGuid))
                {
                    return;
                }

                if (OrderFacade.SetOrderReturList(rtnl))
                {
                    int exchangeLogId = op.OrderReturnListGetbyType(View.OrderGuid, (int)OrderReturnType.Exchange).Id;
                    string auditMessage = string.Format("換貨單({0})建立. 換貨描述:{1}", exchangeLogId, e.ExchangeReason);
                    CommonFacade.AddAudit(View.OrderGuid, AuditType.Refund, auditMessage, View.UserName, false);

                    OrderFacade.SendCustomerExchangeApplicationMail(exchangeLogId);
                    EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Exchange, exchangeLogId);
                }
            }
            catch
            {
                View.ExchangePanel("ExchangeFail");
                return;
            }

            View.ExchangePanel("ExchangeSuccess");
            return;
        }

        private void OnGetOrderInfo(object sender, DataEventArgs<string> e)
        {
            var orderinfo = mp.GetCouponListMainListByOrderID(MemberFacade.GetUniqueId(View.UserName), e.Data).FirstOrDefault();
            if (orderinfo == null)
            {
                View.SetOrderInfo(null, null);
                return;
            }
            if (orderinfo.DeliveryType == (int)DeliveryType.ToShop)
            {
                View.SetOrderInfo(orderinfo, null);
                return;
            }

            //抓取訂單出貨資訊                
            ViewOrderShipList osInfo;
            int orToExchangeCount = op.OrderReturnListGetCountByType(View.OrderGuid, (int)OrderReturnType.Exchange);
            if (orToExchangeCount > 0)
            {
                osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.Guid, OrderShipType.Exchange)
                    .OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                if (osInfo == null)
                {
                    osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.Guid).OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                }
            }
            else
            {
                osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.Guid).OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
            }
            View.SetOrderInfo(orderinfo, osInfo);
        }
    }
}
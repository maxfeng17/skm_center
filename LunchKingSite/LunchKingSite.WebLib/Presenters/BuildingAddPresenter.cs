﻿using System;
using System.Linq;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class BuildingAddPresenter : Presenter<IBuildingAddView>
    {
        protected ILocationProvider ilp;
        protected ISellerProvider isp;

        public const int DEFAULT_PAGE_SIZE = 15;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetCityList();
            if (View.BuildingGuid != Guid.Empty)
            {
                View.SetBuildingBind(ilp.ViewBuildingCityGet(View.BuildingGuid));
            }
            SetZoneList();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UpdataBuildingInfo += OnUpdataBuildingInfo;
            View.SelectCityChanged += OnSelectCityChanged;
            View.UpdateBuildingDelivery += OnUpdateBuildingDelivery;
            View.DeleteBuildingDelivery += OnDeleteBuildingDelivery;
            View.DeleteBuilding += OnDeleteBuilding;
            return true;
        }

        public BuildingAddPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            ilp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            isp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        protected void OnSelectCityChanged(object sender, EventArgs e)
        {
            SetZoneList();
        }

        protected void OnDeleteBuilding(object sender, EventArgs e)
        {
            ilp.BuildingDelete(View.BuildingGuid);
        }

        protected void SetZoneList()
        {
            CityCollection llczone = ilp.CityGetList(View.SelectCity, City.Columns.Rank);

            Dictionary<int, string> zone = new Dictionary<int, string>();

            foreach (DataOrm.City zl in llczone)
            {
                zone.Add(zl.Id, zl.CityName);
            }

            View.SetZoneDropDown(zone);
        }

        protected void SetCityList()
        {
            View.SetCityDropDown(ilp.CityGetListTopLevel().ToDictionary(x => x.Id, x => x.CityName));
        }

        public void OnUpdataBuildingInfo(object sender, DataEventArgs<UpdatableBuildingInfo> e)
        {
            UpdatableBuildingInfo info = e.Data;
            Building building;

            if (View.BuildingGuid == Guid.Empty)
            {
                building = new Building();
                building.Guid = info.BuildingGuid;
            }
            else
            {
                building = ilp.BuildingGet(View.BuildingGuid);
                building.Guid = building.Guid;
            }

            if (building.BuildingStreetName != info.BuildingStreetName)
                building.BuildingStreetName = info.BuildingStreetName;

            if (building.BuildingAddressNumber != info.BuildingAddressNumber)
                building.BuildingAddressNumber = info.BuildingAddressNumber;

            if (building.BuildingName != info.BuildingName)
                building.BuildingName = info.BuildingName;

            if (building.BuildingRank != info.BuildingRank)
                building.BuildingRank = info.BuildingRank;

            if (building.BuildingId != info.BuildingId)
                building.BuildingId = info.BuildingId;

            if (building.CityId != info.CityId)
                building.CityId = info.CityId;

            if (building.BuildingOnline != info.BuildingOnline)
                building.BuildingOnline = info.BuildingOnline;
            if (!string.IsNullOrEmpty(info.BuildingAtitude))
            {
                building.Coordinate= LocationFacade.GetGeographyWKT(info.BuildingAtitude, info.BuildingLongitude);
            }

            if (building.IsDirty)
                ilp.BuildingSet(building);
        }

        public void OnUpdateBuildingDelivery(object sender, DataEventArgs<BuildingDelivery> e)
        {
            ilp.BuildingDeliverySet(e.Data);
        }

        public void OnDeleteBuildingDelivery(object sender, DataEventArgs<BuildingDelivery> e)
        {
            ilp.BuildingDeliveryDelete(GetBuildingDeleveryID(e.Data.BuildingGuid, e.Data.BusinessHourGuid));
        }

        protected int GetBuildingDeleveryID(Guid BuildingGuid,Guid BusinessGuid)
        {
            int BuildingDeleveryID = 0;
            BuildingDeliveryCollection bdList = new BuildingDeliveryCollection();
            bdList = ilp.BuildingDeliveryGetList(BuildingDelivery.Columns.BuildingGuid, BuildingGuid);

            foreach (BuildingDelivery bd in bdList)
            {
                if (bd.BusinessHourGuid == BusinessGuid)
                {
                    BuildingDeleveryID = bd.Id;
                    break;
                }
            }
            return BuildingDeleveryID;
        }
    }
}

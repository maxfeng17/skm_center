﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web.Security;
using System.Data;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealProductInventoryListPresenter : Presenter<IHiDealProductInventoryListView>
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadShopDeal();
            LoadHouseDeal();
            
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetShopReportDetail += OnGetShopReportDetail;
            View.GetHouseReportDetail += OnGetHouseReportDetail;
            View.SaveShippedDate += OnSaveShippedDate;
            return true;
        }

        private void LoadShopDeal()
        {
            ViewHiDealProductShopInfoCollection shopDetail = hp.ViewHiDealProductShopInfoGetListByProductId(int.Parse(View.Productlid));
            View.showShopData(shopDetail);
        }

        private void LoadHouseDeal()
        {
            ViewHiDealProductHouseInfoCollection HouseDetail = hp.ViewHiDealProductHouseInfoGetListByProductId(int.Parse(View.Productlid));
            View.showHouseData(HouseDetail);

            HiDealProduct product = hp.HiDealProductGet(Convert.ToInt32(View.Productlid));
            View.ShippedDate = (product.ShippedDate!=null) ? product.ShippedDate.Value.ToString("MM/dd/yyyy") : string.Empty;
        }

        public void OnGetShopReportDetail(object sender, DataEventArgs<int> e)
        {
            ViewHiDealProductShopInfoCollection productShopInfo = hp.ViewHiDealProductShopInfoGetListByProductId(e.Data);
            View.ExportProductShopData((ViewHiDealProductShopInfoCollection)productShopInfo);

        }

        public void OnGetHouseReportDetail(object sender, DataEventArgs<int> e)
        {
            ViewHiDealProductHouseInfoCollection productHuoseInfo = hp.ViewHiDealProductHouseInfoGetListByProductId(e.Data);
            View.ExportProductHouseData((ViewHiDealProductHouseInfoCollection)productHuoseInfo);
        }

        public void OnSaveShippedDate(object sender, DataEventArgs<DateTime> e)
        {
            #region 紀錄出貨回覆日
            HiDealProduct product = hp.HiDealProductGet(Convert.ToInt32(View.Productlid));
            product.MarkOld();
            product.ShippedDate = e.Data;
            hp.HiDealProductSet(product);
            View.Message = "出貨回覆日已記錄為" + e.Data.ToString();
            #endregion
        }
    }
}

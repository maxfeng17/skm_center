﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class SystemFunctionAddPresenter : Presenter<ISystemFunctionAddView>
    {
        private ISystemProvider sysProv;
        private IHumanProvider humProv;

        public SystemFunctionAddPresenter(ISystemProvider _sysProv, IHumanProvider _humProv)
        {
            sysProv = _sysProv;
            humProv = _humProv;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            View.GetSystemFunctionData += OnGetSystemFunctionData;
            View.SystemFunctionDelete += OnSystemFunctionDelete;
            View.SystemFunctionSave += OnSystemFunctionSave;
            return true;
        }

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            View.GetSystemFunctionList(SystemFunctionGetList(1));
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sysProv.GetSystemFunctionListCount(GetFilter());
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            View.GetSystemFunctionList(SystemFunctionGetList(e.Data));
        }

        protected void OnGetSystemFunctionData(object sender, DataEventArgs<int> e)
        {
            View.SetSystemFunctioData(sysProv.GetSystemFunction(e.Data));
        }

        protected void OnSystemFunctionDelete(object sender, DataEventArgs<int> e)
        {
            // 判斷功能頁是否有設定權限，若是則不可刪除
            PrivilegeCollection privileges = humProv.GetPrivilegeListByFuncId(e.Data);
            OrgPrivilegeCollection orgPrivileges = humProv.GetOrgPrivilegeListByFuncId(e.Data);
            if (privileges.Count == 0 && orgPrivileges.Count == 0)
            {
                sysProv.DeleteSystemFunction(e.Data);
                CommonFacade.CreateSystemFunctionPrivilege();
                View.GetSystemFunctionList(SystemFunctionGetList(1));
            }
            else
            {
                View.ShowMessage("無法刪除資料!!  人員權限尚有" + privileges.Count + "筆；組織權限尚有" + orgPrivileges.Count + "筆");
            }
        }

        protected void OnSystemFunctionSave(object sender, DataEventArgs<List<SystemFunction>> e)
        {
            int parentSortOrder = 0;
            int sortOrder = 0;

            if (e.Data.Count > 0)
            {
                if (e.Data[0].ParentSortOrder.Equals(0))
                {
                    SystemFunction parentFunc = sysProv.GetSystemFunctionList(1, 9999, string.Empty, SystemFunction.Columns.ParentFunc + "=" + e.Data[0].ParentFunc).FirstOrDefault();
                    if (parentFunc == null)
                    {
                        parentSortOrder = sysProv.GetSystemFunctionList(1, 9999, string.Empty, SystemFunction.Columns.ParentFunc + "<> ''").Select(x => x.ParentSortOrder).Distinct().Count() + 1;
                        sortOrder = 1;
                    }
                    else
                    {
                        parentSortOrder = parentFunc.ParentSortOrder;
                        sortOrder = sysProv.GetSystemFunctionList(1, 9999, string.Empty, SystemFunction.Columns.ParentFunc + "=" + e.Data[0].ParentFunc).Select(x => x.SortOrder).Distinct().Count() + 1;
                    }
                }
                else
                {
                    parentSortOrder = e.Data[0].ParentSortOrder;
                    sortOrder = e.Data[0].SortOrder;
                }
                string existsList = string.Empty;
                foreach (SystemFunction item in e.Data)
                {
                    SystemFunction func = sysProv.GetSystemFunction(item.Id);
                    SystemFunctionCollection exists = sysProv.GetSystemFunctionList(1, 9999, string.Empty, SystemFunction.Columns.Link + "=" + item.Link, SystemFunction.Columns.FuncType + "=" + item.FuncType);
                    if (func.IsLoaded || exists.Count == 0)
                    {
                        if (!func.IsLoaded)
                        {
                            func.CreateId = View.LoginUser;
                            func.CreateTime = DateTime.Now;
                        }
                        func.FuncName = item.Link.Substring(item.Link.LastIndexOf('/') + 1, (item.Link.IndexOf('.') > 0 ? item.Link.IndexOf('.') : item.Link.Length) - item.Link.LastIndexOf('/') - 1);
                        func.Link = item.Link;
                        func.DisplayName = item.DisplayName;
                        func.Visible = item.Visible;
                        func.FuncType = item.FuncType;
                        func.TypeDesc = item.TypeDesc;
                        func.ParentFunc = item.ParentFunc;
                        func.SortOrder = sortOrder;
                        func.ParentSortOrder = parentSortOrder;
                        func.ModifyId = View.LoginUser;
                        func.ModifyTime = DateTime.Now;
                        func.EipUsed = item.EipUsed;
                        func.MenuIcon = item.MenuIcon;
                        sysProv.SetSystemFunction(func);
                    }
                    else
                    {
                        existsList += string.Format("連結:{0} 功能:{1}\\n", item.Link, item.FuncType);
                    }
                }
                if (!string.IsNullOrWhiteSpace(existsList))
                {
                    View.ShowMessage("下列功能權限已存在，無法重複新增:\\n\\n" + existsList);
                }
                CommonFacade.CreateSystemFunctionPrivilege();
                View.GetSystemFunctionList(SystemFunctionGetList(1));
            }
        }
        #endregion

        #region private method
        private SystemFunctionCollection SystemFunctionGetList(int pageNumber)
        {
            return sysProv.GetSystemFunctionList(pageNumber, View.PageSize, SystemFunction.Columns.ParentSortOrder + "," + SystemFunction.Columns.SortOrder, GetFilter());
        }
        private string[] GetFilter()
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(View.DisplayNameSearch))
                list.Add(SystemFunction.Columns.DisplayName + " like " + "%" + View.DisplayNameSearch + "%");
            if (!string.IsNullOrWhiteSpace(View.FuncNameSearch))
                list.Add(SystemFunction.Columns.FuncName + " like " + "%" + View.FuncNameSearch + "%");
            if (!string.IsNullOrWhiteSpace(View.ParentFuncSearch))
                list.Add(SystemFunction.Columns.ParentFunc + " like " + "%" + View.ParentFuncSearch + "%");
            return list.ToArray();
        }
        #endregion
    }
}

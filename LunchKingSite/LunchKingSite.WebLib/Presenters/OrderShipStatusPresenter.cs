﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Presenters
{
    public class OrderShipStatusPresenter : Presenter<IOrderShipStatusView>
    {
        private IPponProvider pp;
        private IOrderProvider op;
        public OrderShipStatusPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchPponOrderList += OnSearchPponOrderList;
            View.PageChange += OnPageChange;
            View.UpdateShipOrderStatus += OnUpdateShipOrderStatus;
            return true;
        }

        void OnUpdateShipOrderStatus(object sender, DataEventArgs<KeyValuePair<OrderStatusLog, int>> e)
        {
            pp.ViewPponOrderUpdateShipStatus(e.Data.Key, e.Data.Value);
            SetDefault(View.CurrentPage);
        }

        public void OnSearchPponOrderList(object sender, DataEventArgs<Guid> e)
        {
            View.Bid = e.Data;
            SetDefault(1);
        }

        public void OnPageChange(object sender, DataEventArgs<int> e)
        {
            SetDefault(e.Data);
            View.CurrentPage = e.Data;
        }

        public void SetDefault(int pageindex)
        {
            ViewPponOrderCollection vpoc = pp.ViewPponOrderGetList(pageindex, 10, ViewPponOrder.Columns.CreateTime + "," + ViewPponOrder.Columns.OrderStatus + " desc", ViewPponOrder.Columns.BusinessHourGuid + "=" + View.Bid.Value);
            View.PageCount = pp.ViewPponOrderGetCount(ViewPponOrder.Columns.BusinessHourGuid + "=" + View.Bid.Value);
            ViewPponDeal vpd = pp.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + "=" + View.Bid.Value);
            List<ClassForViewPponOrder_OrderStatusLog> lvpoc = new List<ClassForViewPponOrder_OrderStatusLog>();
            foreach (ViewPponOrder item in vpoc)
            {
                lvpoc.Add(new ClassForViewPponOrder_OrderStatusLog(item, op.OrderStatusLogGetLatest(item.Guid)));
            }
            View.SetOrderList(lvpoc, vpd);
        }
    }
}

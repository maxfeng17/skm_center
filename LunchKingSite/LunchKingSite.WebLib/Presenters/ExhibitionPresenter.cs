﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class ExhibitionPresenter : Presenter<IExhibition>
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.EnableMobileEventPromo = cp.EnableMobileEventPromoPage;
            EventPromo promo = pp.GetEventPromo(View.Url, EventPromoType.Ppon);
            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.EventPromo, promo.Id);
            //主題活動存在，且時間符合展示時間，可顯示於WEB，當有Preview參數時，為預覽模式，不考慮時間與顯示於WEB設定
            if (promo.IsLoaded)
            {
                View.IsCuration = promo.EventType == (int)EventPromoEventType.Curation;
                if (((promo.StartDate <= DateTime.Now && DateTime.Now <= promo.EndDate && promo.ShowInWeb) || !string.IsNullOrEmpty(View.Preview)))
                {
                    View.SeoDescription = seo.Description;
                    View.SeoKeyword = seo.Keyword;
                    View.EventId = promo.Id;
                    View.Rsrc = promo.Cpa;
                    View.EventTitle = promo.Title;
                    if (GetEventPromoItems(promo))
                    {
                        View.OgTitle = promo.Title;
                        View.OgDescription = promo.DealPromoTitle;
                        View.LinkImageSrc = View.OgImage = ImageFacade.GetMediaPath(promo.DealPromoImage, MediaType.DealPromoImage);
                        View.LinkCanonicalUrl = View.OgUrl = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace("&p=show_me_the_preview", "");

                        switch ((EventPromoTemplateType)promo.TemplateType)
                        {

                            case EventPromoTemplateType.Commercial:
                            case EventPromoTemplateType.SkmEvent:
                                SetCategorys();
                                GetVourcherEventPromoItems();
                                break;
                            case EventPromoTemplateType.PponOnly:
                            case EventPromoTemplateType.Kind:
                            case EventPromoTemplateType.Zero:
                            default:
                                break;
                        }
                    }
                    else
                    {
                        View.ShowEventExpire();
                    }
                }
                else
                {
                    View.ShowEventExpire();
                }
            }
            else
            {
                View.ShowEventExpire();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
        #region method
        public void SetCategorys()
        {
            View.SetCategory(pp.GetEventPromoItemList(View.EventId));
        }
        public bool GetEventPromoItems(EventPromo eventPromo)
        {
            
            List<ViewEventPromo> vepc = new List<ViewEventPromo>();
            List<ViewEventPromo> vepc2 = new List<ViewEventPromo>();
            if (string.IsNullOrEmpty(eventPromo.BindCategoryList))
            {
                int piinlifeCityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
                var vepctmp = pp.GetViewEventPromoList(View.Url, true).OrderBy(x => x.Category).OrderBy(x => x.SubCategory).ToList();

                if (config.EnableGrossMarginRestrictions) 
                { 
                    if (op.DiscountEventIdGetList(eventPromo.Id).Count > 0 )
                    {  
                        foreach (var vepctmpval in vepctmp)
                        {
                            vepctmpval.GrossMargin =
                                ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(vepctmpval.BusinessHourGuid).BaseGrossMargin * 100;
                        }
                        if (vepctmp.Any(x => x.GrossMargin!=0))
                        {
                            vepctmp = vepctmp.Where(x => !op.DiscountLimitEnabledGetByBid(x.BusinessHourGuid) &&
                                                         (x.GrossMargin > Convert.ToDecimal(config.GrossMargin) || (x.BusinessHourStatus & (int)BusinessHourStatus.LowGrossMarginAllowedDiscount) > 0)
                                                         ).ToList();
                        }
                    }
                }

                View.SetAllCategory(eventPromo, vepctmp.ToList());
                vepc = vepctmp.Where(x => !x.CityList.Contains(piinlifeCityId.ToString())).OrderBy(x=>x.Seq).ToList();
                vepc2 = vepctmp.Where(x => x.CityList.Contains(piinlifeCityId.ToString())).OrderBy(x => x.Seq).ToList();
                
            }
            else 
            {
                List<int> categoryList = eventPromo.BindCategoryList.Split(',').Select(int.Parse).ToList();
                ViewEventPromoCategoryCollection vepCategoryCol = pp.ViewEventPromoCategoryGetList(categoryList);

                foreach (ViewEventPromoCategory vepCategory in vepCategoryCol) 
                {
                    ViewEventPromo vep = new ViewEventPromo();
                    vep.EventId = eventPromo.Id;
                    vep.EventTitle = eventPromo.Title;
                    vep.Url = eventPromo.Url;
                    vep.Cpa = eventPromo.Cpa;
                    vep.StartDate = eventPromo.StartDate;
                    vep.EndDate = eventPromo.EndDate;
                    vep.MainPic = eventPromo.MainPic;
                    vep.BgPic = eventPromo.BgPic;
                    vep.BgColor = eventPromo.BgColor;
                    vep.EventDescription = eventPromo.Description;
                    vep.EventStatus = eventPromo.Status;
                    vep.BtnOriginal = eventPromo.BtnOriginal;
                    vep.BtnHover = eventPromo.BtnHover;
                    vep.BtnActive = eventPromo.BtnActive;
                    vep.BtnFontColor = eventPromo.BtnFontColor;
                    vep.MobileMainPic = eventPromo.MobileMainPic;
                    vep.BusinessHourGuid = vepCategory.BusinessHourGuid;
                    vep.Title = vepCategory.Title;
                    vep.Description = vepCategory.Description;
                    vep.Seq = (int)vepCategory.Seq;
                    vep.Status = true;
                    vep.ItemId = vepCategory.ItemId;
                    vep.BusinessHourOrderTimeS = vepCategory.BusinessHourOrderTimeS;
                    vep.BusinessHourOrderTimeE = vepCategory.BusinessHourOrderTimeE;
                    vep.ItemOrigPrice = vepCategory.ItemOrigPrice;
                    vep.ItemPrice = vepCategory.ItemPrice;
                    vep.Discount = vepCategory.Discount;
                    vep.ImagePath = vepCategory.ImagePath;
                    vepc.Add(vep);
                }
            
            }
            
            View.ShowEventPromo(eventPromo , vepc);

            if (vepc.Count == 0 && eventPromo.TemplateType != (int)EventPromoTemplateType.OnlyNewPiinlife)
            {
                View.ShowPiinlifeEventPromo(eventPromo , vepc2);
            }
            else
            {
                if (vepc2.Count != 0)
                {
                    View.ShowPiinlifeEventPromo(vepc2);
                }
            }
            
            return vepc.Count > 0 || vepc2.Count > 0;
        }
        public void GetVourcherEventPromoItems()
        {
            // 館內限定
            Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList = new Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>>();
            // 一般優惠券
            Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList = new Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>>();
            // 抽獎活動
            Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList = new Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>>();

            EventPromoItemCollection items = pp.GetEventPromoItemList(View.EventId, EventPromoItemType.Vourcher);
            if (items.Count > 0)
            {
                // 若為商圈活動，商圈名稱規則為[店名+商圈](ex: 新光南西商圈)
                string storeName = View.EventTitle.Substring(0, View.EventTitle.IndexOf("商圈") > 0 ? View.EventTitle.IndexOf("商圈") : View.EventTitle.Length);

                // 會員收藏清單
                VourcherCollectCollection collects = VourcherFacade.GetVourcherCollect(View.UserId);

                foreach (EventPromoItem item in items)
                {
                    VourcherEvent vourcher = ep.VourcherEventGetById(item.ItemId);
                    if (item.Status && vourcher.IsLoaded)
                    {
                        // 逐筆找出優惠券配合之分店
                        ViewVourcherStoreCollection vstore = ep.ViewVourcherStoreCollectionGetByEventId(vourcher.Id);

                        KeyValuePair<VourcherEvent, bool> vourcherEvent = new KeyValuePair<VourcherEvent, bool>(vourcher, collects.Any(x => x.EventId == vourcher.Id));
                        // 若優惠券分店包含該商圈名稱，則此優惠券為館內限定
                        if (vstore.Any(x => x.StoreName.IndexOf(storeName) > 0))
                        {
                            storeList.Add(item, vourcherEvent);
                        }
                        else
                        {
                            if (vourcher.Type == (int)VourcherEventType.Lottery)
                            {
                                lotteryList.Add(item, vourcherEvent);
                            }
                            else
                            {
                                dataList.Add(item, vourcherEvent);
                            }
                        }
                    }
                }
            }
            View.ShowVourcherEventPromo(dataList, lotteryList, storeList);
        }
        #endregion
    }
}

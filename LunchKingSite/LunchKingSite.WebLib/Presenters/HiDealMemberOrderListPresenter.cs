﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealMemberOrderListPresenter : Presenter<IHiDealMemberOrderListView>
    {
        #region provider
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        #endregion

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.ApiKey = BookingSystemFacade.GetApiKey(BookingSystemServiceName.HiDeal);
            GetOrderList(1, true, true);
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += this.OnPageChange;
            View.FilterChange += this.OnFilterChange;
            return true;
        }

        #region event
        protected void OnPageChange(object sender, DataEventArgs<int> e)
        {
            GetOrderList(e.Data, false, false);
        }
        protected void OnFilterChange(object sender, EventArgs e)
        {
            GetOrderList(1, false, true);
            View.SetUpPageCount();
        }
        #endregion

        #region method
        protected void GetOrderList(int i, bool IsInit, bool ReCountPage)
        {
            HiDealOrderShowCollection data = hp.GetHiDealOrderShowListByUser(i, View.PageSize, View.UserId, string.Empty, GetFilterString(ReCountPage));
            View.GetOrderList(data);
            if (IsInit)
            {
                int count = hp.GetHiDealOrderShowListCount(View.UserId, string.Empty);
                Member m = mp.MemberGet(View.UserName);
                if (m.OrderCount != count)
                {
                    m.OrderCount = count;
                    m.OrderTotal = (int)mp.GetCouponTotalSavings(View.UserName);
                    mp.MemberSet(m);
                }
            }
        }
        protected string GetFilterString(bool ReCountPage)
        {
            string filter = string.Empty;
            if (View.IsLatest)
                filter += " and " + HiDealOrderShow.Columns.OrderCreateTime + ">=dateadd(month,-4,getdate()) ";
            switch (View.FilterType)
            {
                case CouponListFilterType.Expired://使用期限已過，有憑證，已結檔，非取消訂單
                    filter += " and " + HiDealOrderShow.Columns.UseEndTime + "< getdate() " +
                              " and " + HiDealOrderShow.Columns.DeliveryType + " = " + (int)HiDealDeliveryType.ToShop +
                              " and " + HiDealOrderShow.Columns.OrderShowStatus + " <> " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
                case CouponListFilterType.NotExpired://使用期限未過，有憑證，已結檔，非取消訂單
                    filter += " and " + HiDealOrderShow.Columns.UseEndTime + ">= getdate() " +
                              " and " + HiDealOrderShow.Columns.DeliveryType + " = " + (int)HiDealDeliveryType.ToShop +
                              " and " + HiDealOrderShow.Columns.OrderShowStatus + " <> " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
                case CouponListFilterType.NotUsed://尚有憑證，有憑證，已結檔，非取消訂單
                    filter += " and " + HiDealOrderShow.Columns.RemainCount + " > 0 " +
                              " and " + HiDealOrderShow.Columns.DeliveryType + " = " + (int)HiDealDeliveryType.ToShop +
                              " and " + HiDealOrderShow.Columns.OrderShowStatus + " <> " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
                case CouponListFilterType.Used://憑證用完，有憑證，已結檔，非取消訂單
                    filter += " and " + HiDealOrderShow.Columns.RemainCount + " = 0 " +
                              " and " + HiDealOrderShow.Columns.DeliveryType + " = " + (int)HiDealDeliveryType.ToShop +
                              " and " + HiDealOrderShow.Columns.OrderShowStatus + " <> " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
                case CouponListFilterType.Cancel://取消訂單
                    filter += " and " + HiDealOrderShow.Columns.OrderShowStatus + " = " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
                case CouponListFilterType.Product://無憑證，已結檔，非取消單
                    filter += " and " + HiDealOrderShow.Columns.OrderEndTime + "<= getdate() " +
                              " and " + HiDealOrderShow.Columns.DeliveryType + " = " + (int)HiDealDeliveryType.ToHouse +
                              " and " + HiDealOrderShow.Columns.OrderShowStatus + " <> " + (int)HiDealOrderShowStatus.RefundSuccess;
                    break;
            }
            if (ReCountPage)
                View.PageCount = hp.GetHiDealOrderShowListCount(View.UserId, filter);
            return filter;
        }
        #endregion
    }
}


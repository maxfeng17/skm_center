﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class BlogListPresenter : Presenter<IBlogListView>
    {
        protected IBlogProvider _blogProv;

        public BlogListPresenter()
        {
            _blogProv = ProviderFactory.Instance().GetProvider<IBlogProvider>();
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.CurrentPage);
            return true;
        }
        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }
        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _blogProv.BlogPostGetCount(GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }
        private void LoadData(int pageNumber)
        {
            BlogPostCollection blogs = _blogProv.BlogPostGetList(pageNumber, View.PageSize, BlogPost.Columns.BeginDate + " desc", GetFilter());
            View.SetBlogListContent(blogs.ToList());
        }
        private string[] GetFilter()
        {
            List<string> filter = new List<string>();
            filter.Add(BlogPost.Columns.Status + " <> -1");
            if (!string.IsNullOrEmpty(View.BlogTitle))
            {
                filter.Add(BlogPost.Columns.Title + " like %" + View.BlogTitle + "%");
            }
            if (View.BeginDate != DateTime.MinValue.Date)
            {
                filter.Add(BlogPost.Columns.BeginDate + " >= " + View.BeginDate);
            }
            if (View.EndDate != DateTime.MinValue.Date)
            {
                filter.Add(BlogPost.Columns.BeginDate + " <= " + View.EndDate);
            }
            return filter.ToArray();
        }
    }
}

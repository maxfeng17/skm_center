using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Collections;

namespace LunchKingSite.WebLib.Presenters
{
    public class VerificationInventoryPresenter : Presenter<IVerificationInventoryView>
    {
        #region members
        
        private IPponProvider pp;
        private IVerificationProvider vp;
        private ILog log;
        private ISellerProvider sp;
        private IMemberProvider memProv;

        #endregion members

        public VerificationInventoryPresenter()
        {
            SetupProviders();
        }

        private void SetupProviders()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            log = LogManager.GetLogger(typeof(VerificationInventoryPresenter));
        }

        public override bool OnViewInitialized()
        {
            LoadData();
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChanged += OnPageChanged;
            View.OkButtonClick += OkButtonClick;
            View.InventoryLostClick += InventoryLostClick;
            View.GetInventoryCount += OnGetInventoryCount;
            View.SelectAllClick += SelectAllClick;
            return true;
        }

        #region Load Data

        protected void LoadData()
        {
            LoadViewPponDealData();
            LoadInventoryData(View.CurrentPage);
            LoadVerificationData();
            LoadDealAccountingData();
        }

        /// <summary>
        /// 店家基本資料
        /// </summary>
        protected void LoadViewPponDealData()
        {
            View.SetViewPponDealData(pp.ViewPponDealGetByBusinessHourGuid(View.BusinessHourGuid));
        }

        /// <summary>
        /// 線上核銷結果
        /// </summary>
        private void LoadVerificationData()
        {
            View.SalesNumber = GetInventoryCount();
            
            if (Helper.IsFlagSet(View.DealAccountingFlag, AccountingFlag.VerificationLocked))
            {
                View.SetVerificationRecordData(pp.VerificationStatisticsLogGetRecordByGuid(View.BusinessHourGuid, VerificationStatisticsLogStatus.Locked));
            }
            else if (Helper.IsFlagSet(View.DealAccountingFlag, AccountingFlag.VerificationLost))
            {
                View.SetVerificationRecordData(pp.VerificationStatisticsLogGetRecordByGuid(View.BusinessHourGuid, VerificationStatisticsLogStatus.Lost));
            }
            else
            {
                PponDealSalesInfo salesInfo = null;
                ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(View.BusinessHourGuid);
                VendorDealSalesCount ds = vp.GetPponVerificationSummary(new List<Guid> { View.BusinessHourGuid }).FirstOrDefault();

                if (ds != null)
                {
                    salesInfo = new PponDealSalesInfo
                    {
                        IsDealClose = (string.IsNullOrEmpty(deal.Slug)) ? false : (decimal.Parse(deal.Slug) >= 0),
                        UnverifiedCount = ds.UnverifiedCount,
                        VerifiedCount = ds.VerifiedCount,
                        ReturnedCount = ds.ReturnedCount,
                        BeforeDealEndReturnedCount = ds.BeforeDealEndReturnedCount,
                        ForceReturnCount = ds.ForceReturnCount,
                        ForceVerifyCount = ds.ForceVerifyCount
                    };
                }

                View.SetVerificationData(salesInfo);
            }
        }

        /// <summary>
        /// 店家清冊清點
        /// </summary>
        private void LoadDealAccountingData()
        {
            View.SetDealAccountingData(pp.DealAccountingGet(View.BusinessHourGuid));
        }

        /// <summary>
        /// 分店結束營業日期
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns>Dictionary key: 分店名稱 value: 結束營業日期</returns>
        public Dictionary<string, DateTime> GetStoreCloseDownDates(Guid sellerGuid)
        {
            StoreCollection stores = sp.StoreGetListBySellerGuid(sellerGuid);

            if (stores == null)
            {
                return new Dictionary<string, DateTime>();
            }

            return stores.Where(store => store.CloseDownDate.HasValue).ToDictionary(store => store.StoreName, store => store.CloseDownDate.Value);
        }

        /// <summary>
        /// 分店調整使用截止日期
        /// </summary>
        /// <param name="bid"></param>
        /// <returns>Dictionary key: 分店名稱 value: 調整後截止日期</returns>
        public Dictionary<string, DateTime> GetStoreExpireDates(Guid bid)
        {
            ViewPponStoreCollection pstores = pp.ViewPponStoreGetListByBidWithNoLock(bid);

            if (pstores == null)
            {
                return new Dictionary<string, DateTime>();
            }

            return pstores.Where(store => store.ChangedExpireDate.HasValue).ToDictionary(store => store.StoreName, store => store.ChangedExpireDate.Value);
        }

        #endregion Load Data

        #region GridView Event

        /// <summary>
        /// 憑證清冊
        /// </summary>
        /// <param name="pageNumber"></param>
        protected void LoadInventoryData(int pageNumber)
        {
            View.SetInventoryList(pp.ViewPponCouponGetList(pageNumber, View.PageSize, View.BusinessHourGuid, View.SortExpression));
        }

        /// <summary>
        /// Page Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadInventoryData(e.Data);
        }

        /// <summary>
        /// Get GridView Data Count
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnGetInventoryCount(object sender, DataEventArgs<int> e)
        {
            e.Data = GetInventoryCount();
        }

        #endregion GridView Event

        #region Button Event

        /// <summary>
        /// 核銷
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OkButtonClick(object sender, EventArgs e)
        {
            Verification(View.VerifyCouponList, false, View.LoginUserId);
            LoadData();
        }

        
        /// <summary>
        /// 清冊遺失
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InventoryLostClick(object sender, EventArgs e)
        {
            OrderFacade.DealVerificationLostSet(View.BusinessHourGuid, View.SellerGuid, View.LoginUserId, View.LoginUserId);
            OrderFacade.VerificationSatisticsLogSetRightNow(View.BusinessHourGuid, View.LoginUserId, VerificationStatisticsLogStatus.Lost);

            LoadData();
        }

        protected void SelectAllClick(object sender, EventArgs e)
        {
            var p = pp.ViewPponCouponGetList(0, 0, View.BusinessHourGuid, View.SortExpression);
            Dictionary<int, string> list = new Dictionary<int, string>();
            CashTrustLog cash_trust_log = new CashTrustLog();
            
            foreach (ViewPponCoupon i in p)
            {
                cash_trust_log = memProv.CashTrustLogGetByCouponId((int)i.CouponId.Value);
                if (cash_trust_log.Status <  (int)TrustStatus.Verified)
                {
                    if (!list.Keys.Contains(i.CouponId.Value))
                    {
                        list.Add(i.CouponId.Value, i.CouponCode);
                    }
                }
            }
            View.SelectedItems = list;
        }

        #endregion Button Event

        #region Private Method

        /// <summary>
        ///
        /// </summary>
        /// <param name="couponList"></param>
        /// <param name="isForced"></param>
        private void Verification(Dictionary<int, string> couponList, bool isForced, string createId)
        {
            int logCouponid = 0;
            try
            {
                foreach (KeyValuePair<int, string> coupon in couponList)
                {
                    logCouponid = coupon.Key;
                    OrderFacade.VerifyCoupon(coupon.Key, coupon.Value, View.SellerGuid, View.LoginUserId, View.ClientIp, isForced, createId, OrderClassification.LkSite);
                    if (isForced) OrderFacade.VerificationSatisticsLogSetRightNow(View.BusinessHourGuid, View.LoginUserId, VerificationStatisticsLogStatus.ForcedVerified);
                }
            }
            catch (Exception ex)
            {
                log.Warn("核銷憑證錯誤:" + ex + ",CouponId: " + logCouponid.ToString());
            }
        }
        
        private int GetInventoryCount()
        {
            return pp.ViewPponCouponGetList(1, 0, View.BusinessHourGuid, View.SortExpression).Count();
        }

        #endregion Private Method
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Web;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesProposalListPresenter : Presenter<ISalesProposalListView>
    {
        private ISellerProvider sp;
        private IPponProvider pp;
        private IHumanProvider hp;
        private ISystemProvider sysp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.Sid != Guid.Empty)
            {
                LoadData(1);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Export += OnExport;
            View.Import += OnImport;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }

        public SalesProposalListPresenter(ISellerProvider sellerProv, IPponProvider pponProv, IHumanProvider humProv, ISystemProvider sysProv)
        {
            sp = sellerProv;
            pp = pponProv;
            hp = humProv;
            sysp = sysProv;
        }

        #region event

        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }
        protected void OnExport(object sender, EventArgs e)
        {
            ExportData(1);
        }
        protected void OnImport(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.FUExport.PostedFile.FileName))
            {
                if (Path.GetExtension(View.FUExport.PostedFile.FileName) == ".xls")
                {
                    HSSFWorkbook workbook = new HSSFWorkbook(View.FUExport.PostedFile.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);
                    Row row;
                    List<string> ImportError = new List<string>();
                    int EmportCount = 0;
                    for (int i = 1; i <= currentSheet.LastRowNum; i++)
                    {
                        row = currentSheet.GetRow(i);

                        if (row.GetCell(0) == null || string.IsNullOrEmpty(row.GetCell(0).ToString()) && (row.GetCell(1) == null || string.IsNullOrEmpty(row.GetCell(1).ToString())) &&
                            (row.GetCell(2) == null || string.IsNullOrEmpty(row.GetCell(2).ToString())) && (row.GetCell(3) == null || string.IsNullOrEmpty(row.GetCell(3).ToString())) &&
                            (row.GetCell(4) == null || string.IsNullOrEmpty(row.GetCell(4).ToString())) && (row.GetCell(5) == null || string.IsNullOrEmpty(row.GetCell(5).ToString())) &&
                            (row.GetCell(6) == null || string.IsNullOrEmpty(row.GetCell(6).ToString())) && (row.GetCell(7) == null || string.IsNullOrEmpty(row.GetCell(7).ToString())))
                        {

                            //空白行即結束
                            break;
                        }


                        string ProposalId = row.GetCell(0).ToString();
                        string BrandName = row.GetCell(1).ToString();
                        string DeSalesEmail = "";
                        string OpSalesEmail = "";

                        //取mail
                        if (row.GetCell(5) == null)
                            DeSalesEmail = "";
                        else
                            DeSalesEmail = row.GetCell(5).ToString();

                        if (row.GetCell(7) == null)
                            OpSalesEmail = "";
                        else
                            OpSalesEmail = row.GetCell(7).ToString();


                        bool checkDe = false;
                        bool checkOP = false;


                        int pid = 0;
                        int.TryParse(ProposalId, out pid);


                        if (!string.IsNullOrEmpty(ProposalId))
                        {

                            string check = SellerFacade.CheckSales(1, DeSalesEmail, OpSalesEmail);

                            if (string.IsNullOrEmpty(check))
                            {
                                Proposal pro = sp.ProposalGet(pid);
                                ViewEmployee DeSalesEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, DeSalesEmail);
                                if (DeSalesEmp.IsLoaded)
                                {
                                    pro.DevelopeSalesId = DeSalesEmp.UserId;
                                    checkDe = true;
                                }


                                ViewEmployee OpSalesEmp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, OpSalesEmail);
                                if (OpSalesEmp.IsLoaded)
                                {
                                    pro.OperationSalesId = OpSalesEmp.UserId;
                                    checkOP = true;

                                }
                                else if (string.IsNullOrEmpty(OpSalesEmail))
                                {
                                    //業務2非必填
                                    pro.OperationSalesId = null;
                                    checkOP = true;
                                }



                                if (checkDe && checkOP)
                                {
                                    //兩個業務正確才匯入
                                    sp.ProposalSet(pro);
                                    ProposalFacade.ProposalLog(pid, "批次變更負責業務1 為 " + DeSalesEmp.EmpName, View.UserName);
                                    ProposalFacade.ProposalLog(pid, "批次變更負責業務2 為 " + (OpSalesEmp.EmpName ?? "null"), View.UserName);
                                    EmportCount++;
                                }
                            }
                            else
                            {
                                ImportError.Add("第" + i.ToString() + "列");
                            }
                        }
                        else
                        {
                            ImportError.Add("第" + i.ToString() + "列");
                        }
                    }
                    if (ImportError.Count() > 0)
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆\\n失敗筆數：" + ImportError.Count().ToString() + "筆\\n失敗清單：" + string.Join("、", ImportError));
                    }
                    else
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆");
                    }
                }
                else
                {
                    View.ShowMessage("請使用匯出時的檔案格式做匯入");
                }                 
            }
            else
            {
                View.ShowMessage("請選擇匯入檔案");
            }
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            string sSQL = "";
            List<int> pids = new List<int>();
            if (!string.IsNullOrEmpty(View.DealContent))
            {
                pids = sp.ProposalMultiDealGetByItemName(View.DealContent).Select(x => x.Pid).Distinct().ToList();
            }
            if (!string.IsNullOrWhiteSpace(View.BrandName))
            {
                pids.AddRange(sp.ProposalMultiDealGetByBrandName(View.BrandName).Distinct().ToList());
                pids.AddRange(pp.ProposalCouponEventContentGetByAppTitle(View.BrandName).Distinct().ToList());
            }
            if (!string.IsNullOrEmpty(View.DealContent) || !string.IsNullOrWhiteSpace(View.BrandName))
            {
                if (!pids.Any())
                    pids.AddRange(new List<int> { 0 });
            }

            e.Data = sp.ViewProposalSellerGetCount(View.Status, View.CheckFlag, View.SpecialFlag, View.ShowChecked, View.SalesId, View.StatusFlag, View.Dept , View.CrossDeptTeam, View.Photographer , View.PhotoType, View.EmpUserId, View.DealStatus, pids, ref sSQL, GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }
        #endregion

        #region Private Method

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();
            if (View.ProposalId != 0)
            {
                filter.Add(ViewProposalSeller.Columns.Id + "=" + View.ProposalId);
            }
            if (View.Bid != Guid.Empty)
            {
                filter.Add(ViewProposalSeller.Columns.BusinessHourGuid + "=" + View.Bid);
            }
            if (View.Sid != Guid.Empty)
            {
                filter.Add(ViewProposalSeller.Columns.SellerGuid + "=" + View.Sid);
            }
            if (!string.IsNullOrWhiteSpace(View.SellerName))
            {
                filter.Add(ViewProposalSeller.Columns.SellerName + " like %" + View.SellerName.ToLower().Replace("in", "%").Replace("between", "%").Replace("like", "%").Replace("null", "%") + "%");
            }
            if (View.OrderTimeS != DateTime.MinValue)
            {
                filter.Add(ViewProposalSeller.Columns.OrderTimeS + ">=" + View.OrderTimeS);
            }
            if (View.OrderTimeE != DateTime.MinValue)
            {
                filter.Add(ViewProposalSeller.Columns.OrderTimeS + "<" + View.OrderTimeE);
            }
            if (View.IsOrderTimeSet)
            {
                filter.Add(ViewProposalSeller.Columns.OrderTimeS + "<" + DateTime.MaxValue.Date);
            }
            if (!string.IsNullOrWhiteSpace(View.MarketingResource))
            {
                filter.Add(ViewProposalSeller.Columns.MarketingResource + " like %" + View.MarketingResource + "%");
            }
            //if (!string.IsNullOrWhiteSpace(View.DealContent))
            //{
            //    filter.Add(ViewProposalSeller.Columns.MultiDeals + " like %" + View.DealContent + "%");
            //}
            //if (!string.IsNullOrWhiteSpace(View.Dept))
            //{
            //    filter.Add(ViewProposalSeller.Columns.DeptId + "=" + View.Dept);
            //}
            //if (View.Type != null)
            //{
            //    filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)View.Type);
            //}
            if (View.DeliveryNewType != null)
            {
                if(View.DeliveryNewType == (int)DeliveryType.ToShop)
                {
                    filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)DeliveryType.ToShop);
                }
                else
                {
                    if (View.DeliveryNewType == (int)DeliveryType.ToHouse)
                    {
                        //所有宅配
                        filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse);
                    }
                    else if (View.DeliveryNewType == 10)
                    {
                        //舊宅配
                        filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse);
                        filter.Add(ViewProposalSeller.Columns.ProposalSourceType + "=" + (int)ProposalSourceType.Original);
                    }
                    else if (View.DeliveryNewType == 11)
                    {
                        //新宅配
                        filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse);
                        filter.Add(ViewProposalSeller.Columns.ProposalSourceType + "=" + (int)ProposalSourceType.House);
                    }
                }
            }
            if (View.NewHouseStatus != null)
            {
                //新宅配
                filter.Add(ViewProposalSeller.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse);
                filter.Add(ViewProposalSeller.Columns.ProposalSourceType + "=" + (int)ProposalSourceType.House);
                if (View.ShowHouseChecked)
                {
                    //已完成
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.VbsApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ApplyFlag + "=" + (int)ProposalApplyFlag.Apply);
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.SaleApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ApproveFlag + "=" + (int)ProposalApproveFlag.QCcheck);
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ManagerApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.BusinessFlag + "=" + (int)ProposalBusinessFlag.ProposalAudit);
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ProductionApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ListingFlag + ">=" + (int)ProposalListingFlag.PageCheck);
                        filter.Add(ViewProposalSeller.Columns.IsProduction + "=" + true);
                    }
                }
                else
                {
                    //未完成
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.VbsApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ApplyFlag + "=" + (int)ProposalApplyFlag.Initial);
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.SaleApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ApplyFlag + "=" + (int)ProposalApplyFlag.Apply);
                        filter.Add(ViewProposalSeller.Columns.ApproveFlag + "=" + (int)ProposalApproveFlag.Initial);
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ManagerApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.ApproveFlag + "=" + (int)ProposalApproveFlag.QCcheck);
                        filter.Add(ViewProposalSeller.Columns.BusinessFlag + "=" + (int)ProposalBusinessFlag.Initial);
                        filter.Add(ViewProposalSeller.Columns.FlowCompleteTime + " is " + "null");
                    }
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ProductionApprove)
                    {
                        filter.Add(ViewProposalSeller.Columns.BusinessFlag + "=" + (int)ProposalBusinessFlag.ProposalAudit);
                        filter.Add(ViewProposalSeller.Columns.ListingFlag + "<>" + (int)ProposalListingFlag.PageCheck);
                        filter.Add(ViewProposalSeller.Columns.ListingFlag + "<>" + 3);
                        filter.Add(ViewProposalSeller.Columns.IsProduction + "= true");
                    }
                }
            }
            if (View.CopyType != null)
            {
                filter.Add(ViewProposalSeller.Columns.CopyType + "=" + (int)View.CopyType);
            }
            if (View.DealType != 0)
            {
                filter.Add(ViewProposalSeller.Columns.DealType + "=" + (int)View.DealType);
            }
            if (View.DealType1 != 0)
            {
                filter.Add(ViewProposalSeller.Columns.DealType1 + "=" + (int)View.DealType1);
            }
            if (View.DealSubType != 0)
            {
                filter.Add(ViewProposalSeller.Columns.DealSubType + "=" + (int)View.DealSubType);
            }
            if (View.ShowAppley)
            {
                filter.Add(ViewProposalSeller.Columns.ApplyFlag + ">" + (int)ProposalApplyFlag.Initial);
            }
            if (View.Status == ProposalStatus.Created)
            {
                if (View.NoOrderTime)
                {
                    filter.Add(ViewProposalSeller.Columns.OrderTimeS + "  is null ");
                }
            }
            if (View.ProposalCreatedType != 0)
            {
                filter.Add(ViewProposalSeller.Columns.ProposalCreatedType + "=" + (int)View.ProposalCreatedType);
            }
            if (View.UniqueId != 0)
            {
                ViewPponDeal vpd = pp.ViewPponDealGetByUniqueId(View.UniqueId);
                filter.Add(ViewProposalSeller.Columns.BusinessHourGuid + "=" + vpd.BusinessHourGuid);
            }


            return filter.ToArray();
        }

        private void LoadData(int pageNumber)
        {
            string[]  filters = GetFilter();
            List<int> pids = new List<int>();
            if (View.NewHouseStatus != null)
            {
                //新宅配
                if (View.ShowHouseChecked)
                {
                    //已完成
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ProductionApprove)
                    {
                        ProposalCollection proList = sp.ProposalGetListByProduction();
                        pids.AddRange(
                            proList.Where(t => Helper.IsFlagSet(t.ListingFlag, ProposalListingFlag.PageCheck))
                            .Select(x => x.Id).ToList()
                            );
                    }
                }
                else
                {
                    //未完成
                    if (View.NewHouseStatus == (int)ProposalHouseStatus.ProductionApprove)
                    {
                        ProposalCollection proList = sp.ProposalGetListByProduction();
                        pids.AddRange(
                            proList.Where(t => !Helper.IsFlagSet(t.ListingFlag, ProposalListingFlag.PageCheck))
                            .Select(x => x.Id).ToList()
                            );
                    }
                }
            }

            if (!string.IsNullOrEmpty(View.DealContent))
            {
                pids.AddRange(sp.ProposalMultiDealGetByItemName(View.DealContent).Select(x => x.Pid).Distinct().ToList());
            }
            if (!string.IsNullOrWhiteSpace(View.BrandName))
            {
                pids.AddRange(sp.ProposalMultiDealGetByBrandName(View.BrandName).Distinct().ToList());
                pids.AddRange(pp.ProposalCouponEventContentGetByAppTitle(View.BrandName).Distinct().ToList());
            }
            if (!string.IsNullOrEmpty(View.DealContent) || !string.IsNullOrWhiteSpace(View.BrandName))
            {
                if (!pids.Any())
                    pids.AddRange(new List<int> { 0 });
            }


            StringBuilder sSQL = new StringBuilder();
            ViewProposalSellerCollection Proposals = sp.ViewProposalSellerGetList(pageNumber, View.PageSize, ViewProposalSeller.Columns.ModifyTime + " desc ", View.Status, View.CheckFlag, View.SpecialFlag, View.ShowChecked, View.SalesId, View.StatusFlag, View.Dept, View.CrossDeptTeam, View.Photographer , View.PhotoType , View.EmpUserId, View.DealStatus, pids.Distinct().ToList(), sSQL, filters);

            View.SetProposalList(Proposals);
        }

        private void ExportData(int pageNumber)
        {
            string[] filters = GetFilter();

            List<int> pids = new List<int>();
            if (!string.IsNullOrEmpty(View.DealContent))
            {
                pids = sp.ProposalMultiDealGetByItemName(View.DealContent).Select(x => x.Pid).Distinct().ToList();
            }
            if (!string.IsNullOrWhiteSpace(View.BrandName))
            {
                pids.AddRange(sp.ProposalMultiDealGetByBrandName(View.BrandName).Distinct().ToList());
                pids.AddRange(pp.ProposalCouponEventContentGetByAppTitle(View.BrandName).Distinct().ToList());
            }
            if (!string.IsNullOrEmpty(View.DealContent) || !string.IsNullOrWhiteSpace(View.BrandName))
            {
                if (!pids.Any())
                    pids.AddRange(new List<int> { 0 });
            }
            StringBuilder sSQL = new StringBuilder();
            ViewProposalSellerCollection Proposals = sp.ViewProposalSellerGetList(1, 9999, ViewProposalSeller.Columns.ModifyTime + " desc ", View.Status, View.CheckFlag, View.SpecialFlag, View.ShowChecked, View.SalesId, View.StatusFlag, View.Dept, View.CrossDeptTeam, View.Photographer, View.PhotoType, View.EmpUserId, View.DealStatus, pids, sSQL, filters);


            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("批次轉換提案單");

            sheet.SetColumnWidth(0, 3000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 5000);
            sheet.SetColumnWidth(3, 5000);
            sheet.SetColumnWidth(4, 3000);
            sheet.SetColumnWidth(5, 12000);
            sheet.SetColumnWidth(6, 3000);
            sheet.SetColumnWidth(7, 12000);
            sheet.SetColumnWidth(8, 5000);
            sheet.SetColumnWidth(9, 5000);
            sheet.SetColumnWidth(10, 5000);


            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("單號");
            cols.CreateCell(1).SetCellValue("品牌名稱");
            cols.CreateCell(2).SetCellValue("商家名稱");
            cols.CreateCell(3).SetCellValue("母檔Bid");
            cols.CreateCell(4).SetCellValue("負責業務1");
            cols.CreateCell(5).SetCellValue("負責業務1Email");
            cols.CreateCell(6).SetCellValue("負責業務2");
            cols.CreateCell(7).SetCellValue("負責業務2Email");
            cols.CreateCell(8).SetCellValue("上檔起日");
            cols.CreateCell(9).SetCellValue("上檔迄日");
            cols.CreateCell(10).SetCellValue("檔次類型");

            int i = 0;
            foreach(ViewProposalSeller pro in Proposals)
            {
                if (pro != null && pro.IsLoaded)
                {
                    ViewEmployee deSalesEmp = hp.ViewEmployeeGet(Employee.Columns.UserId, pro.DevelopeSalesId);
                    ViewEmployee opSalesEmp = hp.ViewEmployeeGet(Employee.Columns.UserId, pro.OperationSalesId);
                    ViewComboDealCollection comboDeals = pp.GetViewComboDealByBid(pro.BusinessHourGuid == null ? Guid.Empty : (Guid)pro.BusinessHourGuid, false);
                    cols = sheet.CreateRow(i + 1);
                    cols.CreateCell(0).SetCellValue(pro.Id);
                    cols.CreateCell(1).SetCellValue(pro.BrandName);
                    cols.CreateCell(2).SetCellValue(pro.SellerName);
                    cols.CreateCell(3).SetCellValue((comboDeals.Count() > 0 ? Convert.ToString(comboDeals.FirstOrDefault().MainBusinessHourGuid) : string.Empty));
                    cols.CreateCell(4).SetCellValue(deSalesEmp.EmpName);
                    cols.CreateCell(5).SetCellValue(deSalesEmp.Email);
                    cols.CreateCell(6).SetCellValue(opSalesEmp.EmpName);
                    cols.CreateCell(7).SetCellValue(opSalesEmp.Email);
                    cols.CreateCell(8).SetCellValue(pro.OrderTimeS == null ? string.Empty : pro.OrderTimeS.Value.ToString("yyyy/MM/dd"));
                    cols.CreateCell(9).SetCellValue(pro.OrderTimeE == null ? string.Empty : pro.OrderTimeE.Value.ToString("yyyy/MM/dd"));
                    cols.CreateCell(10).SetCellValue(pro.DealType1 == null ? string.Empty : sysp.SystemCodeGetByCodeGroupId("DealType", Convert.ToInt32(pro.DealType1)).CodeName);

                    

                    ProposalFacade.ProposalLog(pro.Id, "提案單資料批次匯出", View.UserName);
                    i++;
                }
            }

            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(@"ExportProposal.xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);


        }

        #endregion
    }
}

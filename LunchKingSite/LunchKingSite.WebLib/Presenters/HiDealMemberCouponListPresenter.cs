﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealMemberCouponListPresenter : Presenter<IHiDealMemberCouponListView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetOrderDetail();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SendSMS += this.OnSendSMS;
            View.RequestEinvoice += this.OnRequestEinvoice;
            View.GetSMS += OnGetSMS;
            return true;
        }

        #region event

        private void OnRequestEinvoice(object sender, DataEventArgs<MultipleEinvoiceRequestQuery> e)
        {
            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderGuid(e.Data.OrderGuid, OrderClassification.HiDeal);
            List<int> nonReturnBackCouponIds = ctlc
                    .Where(x => x.Status != (int)TrustStatus.Refunded && x.Status != (int)TrustStatus.Returned)
                    .Select(x => x.CouponId ?? 0).ToList();     // 退貨/刷退 以外的 coupon id
            ViewEinvoiceMailCollection mails = op.ViewEinvoiceMailGetListByOrderGuid(e.Data.OrderGuid);

            //可顯示在畫面上的發票 (有發票號碼 & 發票沒作廢 & 憑證型 & 沒退貨)
            List<ViewEinvoiceMail> viewableInvoice = mails.Where(t =>
                        string.IsNullOrEmpty(t.InvoiceNumber) == false &&
                        t.InvoiceStatus != (int)EinvoiceType.C0501 &&
                        t.CouponId.HasValue &&
                        nonReturnBackCouponIds.Contains(t.CouponId.Value)
                    ).ToList();

            //可申請紙本的發票
            List<string> printableCouponNumbers = viewableInvoice
                .Where(t => t.InvoiceRequestTime.HasValue == false && t.InvoiceWinning == false)
                .Select(t => t.InvoiceNumber).ToList();

            foreach (string einvoiceNumber in printableCouponNumbers)
            {
                op.EinvoiceRequestPaper(View.UserName + "申請紙本", einvoiceNumber, e.Data.Receiver, e.Data.Address);
            }
            View.FinishEinvoiceRequestRequest();
            GetOrderDetail();
        }

        private void OnGetSMS(object sender, DataEventArgs<HiDealSMSQuery> e)
        {
            int smsCount = pp.SMSLogGetCountByPpon(View.UserName, e.Data.CouponId, SmsType.HiDeal);
            View.GetAlreadySmsCount(smsCount, smsCount >= 3 ? 1 : 0);
        }

        private void OnSendSMS(object sender, DataEventArgs<HiDealSMSQuery> e)
        {
            int smsCount = pp.SMSLogGetCountByPpon(View.UserName, e.Data.CouponId, SmsType.HiDeal);
            if (smsCount >= 3)
            {
                View.GetAlreadySmsCount(3, 1);
            }
            else
            {
                int couponid;
                if (int.TryParse(e.Data.CouponId, out couponid))
                {
                    ViewHiDealCoupon vhdc = hp.ViewHiDealCouponGet(couponid);
                    SMS sms = new SMS();

                    HiDealCouponSmsFormatter smsFormatter = new HiDealCouponSmsFormatter(vhdc.Sms.Replace("-", "－"));
                    smsFormatter.CouponPrefix = vhdc.Prefix;
                    //因為Sony手機的簡訊, 在碰到 - 8 這樣的字元組合時, 會替換成音符..故把半行的 dash 改換為全形 dash
                    smsFormatter.CouponSequence = vhdc.Sequence.Replace("-", "－");
                    smsFormatter.CouponCode = vhdc.Code;
                    smsFormatter.BeginDate = vhdc.UseStartTime;
                    smsFormatter.ExpireDate = vhdc.UseEndTime;
                    smsFormatter.TelephoneInfo = OrderFacade.HiDealSmsGetPhone(vhdc);

                    sms.SendMessage("", smsFormatter.ToSms(), "", e.Data.Mobile, View.UserName, Core.SmsType.HiDeal, Guid.Empty, e.Data.CouponId);
                    View.GetAlreadySmsCount(smsCount + 1, 2);
                }
            }
        }

        private void GetOrderDetail()
        {
            HiDealOrder hdo;
            if (View.OrderPkCondition != null)
            {
                hdo = hp.HiDealOrderGet(View.OrderPkCondition.Value);
            }
            else if (View.OrderGuid != Guid.Empty)
            {
                hdo = hp.HiDealOrderGet(View.OrderGuid);
            }
            else
            {
                View.IlleagalUser();
                return;
            }

            if (hdo != null)
            {
                View.OrderId = hdo.OrderId;
                View.OrderPk = hdo.Pk;

                if (string.IsNullOrEmpty(View.UserName) ||
                    string.Equals(MemberFacade.GetUserName(hdo.UserId), View.UserName, StringComparison.OrdinalIgnoreCase) == false)
                {
                    View.IlleagalUser();
                    return;
                }
            }
            else
            {
                View.IlleagalUser();
                return;
            }
            Guid orderGuid = hdo.Guid;
            HiDealDeal hdd = hp.HiDealDealGet(hdo.HiDealId);
            if (hdd != null)
            {
                View.OrderEndDate = hdd.DealEndTime.Value;
            }

            HiDealOrderShow hiDealOrderShow = hp.HiDealOrderShowGetByOrderPk(hdo.Pk);
            if (hiDealOrderShow != null)
            {
                View.OrderShowStatus = hiDealOrderShow.OrderShowStatus;
            }

            Member m = mp.MemberGet(View.UserName);
            if (m != null)
            {
                View.Mobile = m.Mobile;
            }

            ViewHiDealCouponListSequenceCollection vhcsc = hp.GetHiDealCouponListSequenceListByOid(orderGuid);
            if (vhcsc.Count > 0)
            {
                View.ProductName = vhcsc[0].ProductName;
            }
            View.HidealSmsVisible = vhcsc[0].IsSmsClose.Value;
            HiDealOrderDetailCollection hdodc = hp.HiDealOrderDetailGetListByOrderGuid(orderGuid);

            IList<HiDealOrderDetail> list =
                hdodc.Where(
                    x =>
                    (HiDealProductType)x.ProductType != (HiDealProductType.Freight) &&
                    (HiDealDeliveryType)x.DeliveryType == HiDealDeliveryType.ToShop).ToList();
            View.CouponListVisible = list.Count > 0;
            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.HiDeal);
            DiscountCode dc = op.DiscountCodeGetByOrderGuid(orderGuid);
            HiDealReturnedCollection hdrc = hp.HiDealReturnedGetListByOrderPk(hdo.Pk);
            //抓取預計配送結束日期
            HiDealProduct hdp = hp.HiDealProductGet(hdodc.First().ProductId);
            DateTime? deliveryEndDate = (hdp.ShippedDate.HasValue) ? hdp.ShippedDate : hdp.UseEndTime;
            //抓取訂單出貨資訊
            ViewOrderShipList osInfo = op.ViewOrderShipListGetListByProductGuid(hdp.Guid, OrderClassification.HiDeal).FirstOrDefault(x => x.OrderGuid == orderGuid);

            View.SetCouponList(vhcsc);
            View.SetShipInfo(osInfo, deliveryEndDate);
            View.SetOrderDetail(hdodc);
            View.SetOrderCategory(hdodc);
            View.SetOrderPaymentDetail(ctlc, dc);

            #region 發票

            if (hdo.CreateTime >= GetNewInvoiceDate() && list.Count > 0)
            {
                //新發票流程: 7/1 以後的憑證檔
                ViewEinvoiceMailCollection mails = op.ViewEinvoiceMailGetListByOrderGuid(orderGuid);
                View.SetNewEInvoiceDetail(mails, ctlc);
            }
            else
            {
                List<ViewEinvoiceMail> viewEinvoices = op.ViewEinvoiceMailGetListByOrderGuid(orderGuid)
                    .Where(x => x.UserId.Equals(View.UserId) && x.OrderGuid.Equals(View.OrderGuid)).ToList();

                if (viewEinvoices.Count > 0)
                {
                    var veac = op.EinvoiceAllowanceGetList(viewEinvoices[0].Id);
                    View.SetEnvoiceDetail(viewEinvoices, veac);
                }
            }

            #endregion 發票

            View.SetOrderReturned(hdrc, ctlc);
        }

        private DateTime GetNewInvoiceDate()
        {
            return conf.NewInvoiceDate;
        }

        #endregion event
    }
}
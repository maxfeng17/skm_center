﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using log4net;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class SkmPresenter: Presenter<ISkmView>
    {
        private ISysConfProvider _conf;
        protected static ILog Logger = LogManager.GetLogger("SkmPresenter");

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            _conf = ProviderFactory.Instance().GetConfig();
            View.IsOpenNewWindow = _conf.DefaultPageOpenNewWindow;
            View.SiteUrl = _conf.SiteUrl;
            SetMultipleDeals();
            return true;
        }

        private void SetMultipleDeals()
        {
            var channelId = PponCityGroup.DefaultPponCityGroup.Skm.CategoryId;
            var areaId = View.AreaId;
            var subCategoryId = View.CategoryId;
            var subCategoryIdList = new List<int>();
            if (subCategoryId > 0)
            {
                subCategoryIdList.Add(subCategoryId);
            }

            var multidealsSkm = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelId, View.AreaId, subCategoryIdList);
            //multidealsSkm = multidealsSkm.Where(x => x.PponDeal.IsExperience.HasValue && !x.PponDeal.IsExperience.Value).ToList();

            #region 設定有哪些可點選的新版分類、checkbox類的filter

            if (channelId > 0)
            {
                //1.新版分類的篩選
                CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(channelId, areaId, subCategoryId);
                var dealCount = new CategoryDealCount(0, "全部");
                dealCount.DealCount = node.TotalCount;
                node.DealCountList.Insert(0, dealCount);
                var categoryDealCountList = PponFacade.SubCategoryDealCountByMainCategoryDealCount(node.DealCountList.Where(x => x.DealCount > 0 || x.CategoryId.Equals(0)).ToList(), multidealsSkm);

                //2.checkbox分類的篩選 (即買即用、最後一天)
                List<CategoryDealCount> filterNodes = PponDealPreviewManager.GetSpecialDealCountListByChannel(channelId, areaId).Where(x => x.DealCount > 0).Where(x => x.CategoryId != 138).ToList();

                //3.品生活的篩選
                SystemCodeCollection piinlifeLinkes = SystemCodeManager.GetSystemCodeListByGroup("17PiinlifeLink" + channelId);

                //設定可以篩選的選項
                View.SetMultipleCategories(categoryDealCountList, filterNodes, piinlifeLinkes);


                //if (dealCategoryId > 0)
                //{
                //    //用新版分類篩選清單資料
                //    multideals = multideals.Where(x => x.DealCategoryIdList.Contains(dealCategoryId)).ToList();
                //}
            }
            else
            {
                //對應的PponCity未設定ChannelId，依據現況，表示並無DealCategory資料需要顯示
                View.SetMultipleCategories(new List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>>(), new List<CategoryDealCount>(), new SystemCodeCollection());
            }
            #endregion




           
            View.SetSkmDeals(multidealsSkm);
        }

    }
}

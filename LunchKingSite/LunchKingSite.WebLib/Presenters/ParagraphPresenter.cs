using System;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class ParagraphPresenter : Presenter<IParagraphView>
    {
        private ICmsProvider cp;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private string ViewName
        {
            get { return string.IsNullOrEmpty(View.UniqueName) ? View.ContentName : View.UniqueName; }
        }

        public ParagraphPresenter()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        }

        public override bool OnViewInitialized()
        {
            
            base.OnViewInitialized();
            
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetDataForCache += OnGetDataForCache;            
            return true;
        }

        protected void OnGetDataForCache(object sender, EventArgs e)
        {
            string checkContentName;
            if (string.Equals("paragraph1", View.ContentControlId.ToLower()))
            {
                checkContentName = "/ppon/default.aspx_paragraph1";
            }
            else if (string.Equals("headerscript", View.ContentControlId.ToLower()))
            {
                checkContentName = "/ppon/default.aspx_headerscript";
            }
            else
            {
                checkContentName = ViewName;
            }

            string content = View.GetDataFromCache(checkContentName);
            if (content == null)
            {
                CmsContent article = cp.CmsContentGet(checkContentName);

                if (!article.IsLoaded)
                {
                    if (View.UserRoles != null && !string.IsNullOrEmpty(Array.Find(View.UserRoles, (a) => a == MemberRoles.Editor.ToString("g"))))
                    {
                        article = new CmsContent();
                        article.ContentName = checkContentName;
                        article.CreatedBy = View.UserName;
                        article.CreatedOn = DateTime.Now;
                        article.Locale = "zh_TW";
                        cp.CmsContentSet(article);
                    }
                }
                else
                {
                    content = article.Body;
                }
                if (content == null)
                {
                    content = string.Empty;
                }
                if (config.SwitchableHttpsEnabled)
                {
                    content = content.Replace("http://", "https://");
                }

                View.SetCache(checkContentName, content);
            }
            View.SetData(content);
        }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.ControlRoom.Order;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class LionTravelRefundListPresenter : Presenter<ILionTravelRefundListView>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            //GetRefundForms(1, true);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchRefundForms += OnSearchRefundForms;
            View.PageChange += OnPageChange;
            return true;
        }

        void OnPageChange(object sender, DataEventArgs<int> e)
        {
            GetRefundForms(e.Data, false);
        }

        void OnSearchRefundForms(object sender, EventArgs e)
        {
            GetRefundForms(1, true);
        }

        public void GetRefundForms(int page, bool recount)
        {
            List<int> progressStatusLists = new List<int>();
            switch (View.ProdressStatus)
            {
                case ProgressState.ReturnProcessing:
                    progressStatusLists.Add((int)ProgressStatus.Processing);
                    progressStatusLists.Add((int)ProgressStatus.AtmQueueing);
                    progressStatusLists.Add((int)ProgressStatus.AtmQueueSucceeded);
                    progressStatusLists.Add((int)ProgressStatus.AtmFailed);
                    break;

                case ProgressState.ReturnProcessing_AtmFailed:
                    progressStatusLists.Add((int)ProgressStatus.AtmFailed);
                    break;

                case ProgressState.ReturnCompleted:
                    progressStatusLists.Add((int)ProgressStatus.Completed);
                    progressStatusLists.Add((int)ProgressStatus.CompletedWithCreditCardQueued);
                    break;

                case ProgressState.ReturnCancel:
                    progressStatusLists.Add((int)ProgressStatus.Canceled);
                    break;

                case ProgressState.ReturnFail:
                    progressStatusLists.Add((int)ProgressStatus.Unreturnable);
                    break;

                case ProgressState.All:
                default:
                    break;
            }
            List<string> filter = new List<string>() { ViewLiontravelOrderReturnFormList.Columns.Type + "=" + (int)View.OrderClassType };
            if (!string.IsNullOrEmpty(View.FilterData))
            {
                switch (View.FilterType)
                {
                    case (int)LionTravelOrderFilterType.LionTravelOrderId:
                        filter.Add(ViewLiontravelOrderReturnFormList.Columns.RelatedOrderId + "=" + View.FilterData);
                        break;
                    case (int)LionTravelOrderFilterType.OrderId:
                        filter.Add(ViewLiontravelOrderReturnFormList.Columns.OrderId + "=" + View.FilterData);
                        break;
                    case (int)LionTravelOrderFilterType.OrderGuid:
                        filter.Add(ViewLiontravelOrderReturnFormList.Columns.OrderGuid + "=" + View.FilterData);
                        break;
                    case (int)LionTravelOrderFilterType.Mobile:
                        filter.Add(ViewLiontravelOrderReturnFormList.Columns.Mobile + "=" + View.FilterData);
                        break;
                }
            }
            if (progressStatusLists.Count > 0)
            {
                filter.Add(ViewLiontravelOrderReturnFormList.Columns.ProgressStatus + " in (" + string.Join(",", progressStatusLists) + ")");
            }
            if (View.FilterDeliveryType > 0)
            {
                filter.Add(ViewLiontravelCouponListMain.Columns.DeliveryType + "=" + View.FilterDeliveryType);
            }
            if (View.StartDate.HasValue && View.EndDate.HasValue)
            {
                filter.Add(ViewLiontravelOrderReturnFormList.Columns.ReturnApplicationTime + ">=" + View.StartDate.Value.ToString("yyyy/MM/dd"));
                filter.Add(ViewLiontravelOrderReturnFormList.Columns.ReturnApplicationTime + "<" + View.EndDate.Value.AddDays(1).ToString("yyyy/MM/dd"));
            }
            ViewLiontravelOrderReturnFormListCollection data = op.ViewLiontravelOrderReturnFormListGetList(page, 15, filter.ToArray());
            if (recount)
            {
                View.PageCount = op.ViewLiontravelOrderReturnFormListGetCount(filter.ToArray());
                View.SetUpPageCount();
            }
            List<int> form_ids = data.Select(x => x.ReturnFormId).ToList();
            Dictionary<int, int> applicationProductCountInfos = op.ReturnFormProductGetList(form_ids)
                                                                   .GroupBy(x => x.ReturnFormId)
                                                                   .Select(x => new { ReturnFormId = x.Key, Count = x.Count() })
                                                                   .ToDictionary(x => x.ReturnFormId, x => x.Count);
            Dictionary<int, int> refundedProductCountInfos = op.ReturnFormRefundGetList(form_ids)
                                                                .Where(x => x.IsRefunded == true)
                                                                .GroupBy(x => x.ReturnFormId)
                                                                .Select(x => new { ReturnFormId = x.Key, Count = x.Count() })
                                                                .ToDictionary(x => x.ReturnFormId, x => x.Count);




            View.SetRefundForms(data, applicationProductCountInfos, refundedProductCountInfos);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventPremiumPromoSetUpPresenter : Presenter<IEventPremiumPromoSetUp>
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetEventPremiumPromoList();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ChangeMode += OnChangeMode;
            View.SaveEventPremiumPromo += OnSaveEventPremiumPromo;
            View.GetEventPremiumPromo += OnGetEventPremiumPromo;
            View.UpdateEventPremiumPromoStatus += OnUpdateEventPremiumPromoStatus;
            return true;
        }

        public void OnChangeMode(object sender, EventArgs e)
        {
            View.ChangePanel(View.Mode);
        }

        public void OnSaveEventPremiumPromo(object sender, DataEventArgs<EventPremiumPromo> e)
        {
            EventPremiumPromo premiumPromo;
            if (e.Data.Id != 0)
            {
                premiumPromo = ep.EventPremiumPromoGet(e.Data.Id);
                premiumPromo.ModifyTime = DateTime.Now;
                premiumPromo.ChangeLog += " Modify by " + View.UserName + " " + premiumPromo.ModifyTime + " ";
            }
            else
            {
                premiumPromo = new EventPremiumPromo();
                premiumPromo.Status = false;
                premiumPromo.CreateUniqueId = e.Data.CreateUniqueId;
                premiumPromo.CreateTime = DateTime.Now;
                premiumPromo.ChangeLog += " Create by " + View.UserName + " " + premiumPromo.CreateTime + " ";
            }
            premiumPromo.Title = e.Data.Title;
            premiumPromo.Url = e.Data.Url;
            premiumPromo.Cpa = e.Data.Cpa;
            premiumPromo.PremiumName = e.Data.PremiumName;
            premiumPromo.StartDate = e.Data.StartDate;
            premiumPromo.EndDate = e.Data.EndDate;
            premiumPromo.TemplateType = e.Data.TemplateType;
            premiumPromo.PremiumAmount = e.Data.PremiumAmount;
            premiumPromo.MainBanner = e.Data.MainBanner;

            premiumPromo.BackgroundPicture = e.Data.BackgroundPicture;
            premiumPromo.BackgroundColor = e.Data.BackgroundColor;
            premiumPromo.Description = e.Data.Description;

            premiumPromo.BannerFontColor = e.Data.BannerFontColor;
            premiumPromo.ButtonBackgroundColor = e.Data.ButtonBackgroundColor;
            premiumPromo.ButtonFontColor = e.Data.ButtonFontColor;
            premiumPromo.ButtonBackgroundHoverColor = e.Data.ButtonBackgroundHoverColor;

            premiumPromo.ApplySuccessContent = e.Data.ApplySuccessContent;


            ep.EventPremiumPromoSet(premiumPromo);

            GetEventPremiumPromoList();
            View.ChangePanel(0);
        }

        public void OnGetEventPremiumPromo(object sender, EventArgs e)
        {
            EventPremiumPromo eventPremiumPromo = ep.EventPremiumPromoGet(View.EventPremiumId);

            View.SetEventPremiumPromo(eventPremiumPromo);
        }


        protected void GetEventPremiumPromoList()
        {
            EventPremiumPromoCollection premiumPromos;
                if (View.IsLimitInThreeMonth)
                {
                    premiumPromos = ep.EventPremiumPromoGetListPaging(0,-1,EventPremiumPromo.Columns.StartDate + ">=" + DateTime.Now.AddMonths(-3));
                }
                else
                {
                    premiumPromos = ep.EventPremiumPromoGetListPaging(0,-1);
                }
            View.SetEventPremiumPromoList(premiumPromos);
        }

        public void OnUpdateEventPremiumPromoStatus(object sender, EventArgs e)
        {
            EventPremiumPromo eventPremium = ep.EventPremiumPromoGet(View.EventPremiumId);
            eventPremium.Status = !eventPremium.Status;
            ep.EventPremiumPromoSet(eventPremium);
            GetEventPremiumPromoList();
            View.ChangePanel(0);
        }
    }
}

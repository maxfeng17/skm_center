﻿using LunchKingSite.BizLogic.Vbs;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Presenters
{
    public class PaymentReceiptManagePresenter : Presenter<IPaymentReceiptManageView>
    {
        protected static ISysConfProvider config;
        protected static IPponProvider pp;
        protected static IAccountingProvider ap;
        protected static IHiDealProvider hp;
        protected static IOrderProvider op;
        protected static IMemberProvider mp;
        protected static ISellerProvider sp;
        protected static IHumanProvider hump;

        public PaymentReceiptManagePresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            SetFilterRemittanceType();
            SetFilterReceiptType();
            SetFilterBuyerType();
            SetFilterQueryTypes();
            SetFilterDepartment();
            SetFilterDeliveryType();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            View.QueryClicked += OnQueryClicked;
            View.GetConfirmed += OnGetConfirmed;
            View.GetSheet += OnGetSheet;
            View.DeleteBill += OnDeleteBill;
            View.UpdateBillSentDate += OnUpdateBillSentDate;
            View.UpdateFinanceGetDate += OnUpdateFinanceGetDate;
            View.QueryUnReceiptReceivedInfoClicked += OnQueryUnReceiptReceivedInfoClicked;
            View.QueryRemainderBillClicked += OnQueryRemainderBillClicked;
            View.PageChanged += OnPageChanged;
            View.ExportBsBillInfoClicked += OnExportBsBillInfoClicked;
            return true;
        }

        protected void SetupProviders()
        {
            config = ProviderFactory.Instance().GetConfig();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            hump = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        }

        #region even
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            //未輸入查詢條件進行查詢；回傳查無資料
            if (!View.FilterInternal.Any())
            {
                View.SetStoreList(null);
            }
            else
            {
                var bsLists = ap.ViewBalanceSheetBillListGetBalancingList(View.FilterInternal)
                                    .GroupBy(x => new { SellerGuid = x.SellerGuid, SellerName = x.SellerName, StoreGuid = x.StoreGuid, StoreName = x.StoreName })
                                    .Select(x => new { SellerGuid = x.Key.SellerGuid, SellerName = x.Key.SellerName, StoreGuid = x.Key.StoreGuid, StoreName = x.Key.StoreName })
                                    .ToDataTable();
                View.SetStoreList(bsLists);
            }
        }

        public void OnQueryClicked(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnQueryUnReceiptReceivedInfoClicked(object sender, EventArgs e)
        {
            if (View.IsFilterUnReceiptReceivedInfoFilled == false)
            {
                View.SetUnConfirmBalanceSheetInfo(null);
                return;
            }
            View.SetUnConfirmBalanceSheetInfo(
                GetUnConfirmBalanceSheetInfo(ap.ViewBalanceSheetBillListGetList(View.FilterUnReceiptReceivedInfo)));

        }

        protected void OnQueryRemainderBillClicked(object sender, EventArgs e)
        {
            View.SetRemainderBill(ap.ViewRemainderBillListGetList(View.FilterRemainderBillInfo));
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void LoadData(int pageNumber)
        {
            var bsbillCol = ap.ViewBalanceSheetBillListGetListPaging(pageNumber, View.PageSize, GetFilterQuery());
            var vstInfo = GetBillListInfo(bsbillCol);

            var bswbillCol = ap.ViewBalanceSheetWmsBillListGetListPaging(pageNumber, View.PageSize, GetFilterQuery());
            var vstwInfo = GetBillWmsListInfo(bswbillCol);
            

            View.PageDataCount = ap.ViewBalanceSheetBillListGetList(GetFilterQuery()).Count + ap.ViewBalanceSheetWmsBillListGetList(GetFilterQuery()).Count;
            View.SetInvoiceQuery(vstInfo, vstwInfo);
        }

        protected void OnExportBsBillInfoClicked(object sender, EventArgs e)
        {
            var bsbillCol = ap.ViewBalanceSheetBillListGetList(GetFilterQuery());
            var vstInfo = GetBillListInfo(bsbillCol);

            var bswbillCol = ap.ViewBalanceSheetWmsBillListGetList(GetFilterQuery());
            var vstwInfo = GetBillWmsListInfo(bswbillCol);

            View.SetInvoiceQuery(vstInfo, vstwInfo);
        }

        protected void OnGetSheet(object sender, DataEventArgs<string[]> e)
        {
            View.SetSheetList(GetBalanceSheetInfo(ap.ViewBalanceSheetBillListGetBalancingList(e.Data)), GetBalanceSheetWmsInfo(ap.ViewBalanceSheetWmsBillListGetBalancingList(e.Data)));
        }

        protected void OnDeleteBill(object sender, EventArgs e)
        {
            var sArray = ((RepeaterCommandEventArgs)e).CommandArgument.ToString().Split("/");
            var balanceSheetId = int.Parse(sArray[0]);
            var billId = int.Parse(sArray[1]);
            BalanceSheetUseType useType = (BalanceSheetUseType)(int.Parse(sArray[2]));

            if (useType == BalanceSheetUseType.Deal)
            {
                //刪除單據對帳單關聯表
                ap.BalanceSheetBillRelationshipDelete(balanceSheetId, billId);

                //若該筆單據已無對應關聯對帳單內容，則刪除該筆單據
                if (!ap.BalanceSheetBillRelationshipGetListByBillId(billId).Any() && !ap.BalanceSheetWmsBillRelationshipGetListByBillId(billId).Any())
                {
                    ap.BillDelete(billId);
                }

                //先刪除單據後 再更新對帳單狀態
                var bsModel = new BalanceSheetModel(balanceSheetId);
                //取消對帳單確認狀態
                bsModel.CancelBsConfirmedAndReceiptReceived();
            }
            else
            {//刪除單據對帳單關聯表
                ap.BalanceSheetWmsBillRelationshipDelete(balanceSheetId, billId);

                //若該筆單據已無對應關聯對帳單內容，則刪除該筆單據
                if (!ap.BalanceSheetBillRelationshipGetListByBillId(billId).Any() && !ap.BalanceSheetWmsBillRelationshipGetListByBillId(billId).Any())
                {
                    ap.BillDelete(billId);
                }

                //先刪除單據後 再更新對帳單狀態
                var bsModel = new BalanceSheetWmsModel(balanceSheetId);
                //取消對帳單確認狀態
                bsModel.CancelBsConfirmedAndReceiptReceived();

            }
            

            OnQueryClicked(this, e);
        }

        protected void OnGetConfirmed(object sender, EventArgs e)
        {
            var sArray = ((RepeaterCommandEventArgs)e).CommandArgument.ToString().Split("/");
            var sheetId = sArray[0];
            var uniqueId = sArray[1];

            var bsModel = new BalanceSheetModel(int.Parse(sheetId));

            var confirmResult = bsModel.ConfirmBillingStatement(HttpContext.Current.User.Identity.Name);
            var bs = bsModel.GetDbBalanceSheet();
            string resultMsg;

            switch (confirmResult)
            {
                case BillingConfirmationState.Confirmed:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",對帳單已確認";
                    break;
                case BillingConfirmationState.PreviousMonthUnconfirmed:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",上個月對帳單尚未確認；無法確認本月對帳單";
                    break;
                case BillingConfirmationState.AmountInBillsTooDifferent:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",對帳單與單據金額比對不符；無法確認本月對帳單";
                    break;
                default:
                    resultMsg = "確認對帳單過程發生無法預期錯誤；請洽系統管理員";
                    break;
            }

            if (!string.IsNullOrEmpty(resultMsg))
            {
                View.ShowMessage(resultMsg);
            }

            View.SetSheetList(GetBalanceSheetInfo(ap.ViewBalanceSheetBillListGetBalancingList(View.FilterSellerStore)), GetBalanceSheetWmsInfo(ap.ViewBalanceSheetWmsBillListGetBalancingList(View.FilterSellerStore)));

        }


        public void GetDataConfirmed(string sheetId, string uniqueId)
        {
            var bsModel = new BalanceSheetModel(int.Parse(sheetId));

            var confirmResult = bsModel.ConfirmBillingStatement(HttpContext.Current.User.Identity.Name);
            var bs = bsModel.GetDbBalanceSheet();
            string resultMsg;

            switch (confirmResult)
            {
                case BillingConfirmationState.Confirmed:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",對帳單已確認";
                    break;
                case BillingConfirmationState.PreviousMonthUnconfirmed:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",上個月對帳單尚未確認；無法確認本月對帳單";
                    break;
                case BillingConfirmationState.AmountInBillsTooDifferent:
                    resultMsg = bs.Year + "/" + bs.Month + " 檔號" + uniqueId + ",對帳單與單據金額比對不符；無法確認本月對帳單";
                    break;
                default:
                    resultMsg = "確認對帳單過程發生無法預期錯誤；請洽系統管理員";
                    break;
            }

            if (!string.IsNullOrEmpty(resultMsg))
            {
                View.ShowMessage(resultMsg);
            }

            View.SetSheetList(GetBalanceSheetInfo(ap.ViewBalanceSheetBillListGetBalancingList(View.FilterSellerStore)), GetBalanceSheetWmsInfo(ap.ViewBalanceSheetWmsBillListGetBalancingList(View.FilterSellerStore)));

        }

        protected void OnUpdateBillSentDate(object sender, DataEventArgs<Dictionary<int, DateTime>> e)
        {
            var bills = ap.BillGetList(e.Data.Keys.ToList());

            foreach (var bill in bills)
            {
                bill.BillSentDate = e.Data[bill.Id];
                bill.ModifyId = View.UserName;
                bill.ModifyTime = DateTime.Now;
                ap.BillSet(bill);
                BillRepository.SetBillBalanceSheetChangeLog(bill, null,null);
            }

            OnQueryClicked(this, e);
        }

        protected void OnUpdateFinanceGetDate(object sender, DataEventArgs<Dictionary<int, DateTime>> e)
        {
            var bills = ap.BillGetList(e.Data.Keys.ToList());

            foreach (var bill in bills)
            {
                bill.FinanceGetDate = e.Data[bill.Id];
                bill.ModifyId = View.UserName;
                bill.ModifyTime = DateTime.Now;
                ap.BillSet(bill);
                BillRepository.SetBillBalanceSheetChangeLog(bill, null,null);
            }

            OnQueryClicked(this, e);
        }

        #endregion



        #region method

        protected void SetFilterTypes()
        {
            View.FilterUser = View.FilterUser;
            View.SetFilterTypeDropDown(View.FilterTypes);
        }

        protected void SetFilterRemittanceType()
        {
            View.SetFilterRemittanceType(View.FilterRemittanceType);
        }

        protected void SetFilterReceiptType()
        {
            View.SetFilterReceiptType(View.FilterReceiptType);
        }

        protected void SetFilterBuyerType()
        {
            View.SetFilterBuyerType(View.FilterBuyerType);
        }

        protected void SetFilterQueryTypes()
        {
            View.SetFilterQueryTypes(View.FilterQueryTypes);
        }

        public void SetFilterDepartment()
        {
            View.SetFilterDepartment(GetDept());
        }

        protected void SetFilterDeliveryType()
        {
            View.SetFilterDeliveryType(View.FilterDeliveryType);
        }

        protected string[] GetFilterQuery()
        {
            var filter = new string[View.FilterInfo.Length + 1];
            if (!string.IsNullOrEmpty(View.FilterQueryUser))
            {
                filter[0] = View.FilterQueryUser;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = View.FilterQueryType + " like " + filter[0];
            }
            if (View.FilterInfo != null && View.FilterInfo.Length > 0)
            {
                Array.Copy(View.FilterInfo, 0, filter, 1, View.FilterInfo.Length);
            }
            return filter;
        }

        /// <summary>
        /// P好康
        /// 以商家或分店的角度去看對帳單
        /// 對帳單會有不同檔次、不同月份等
        /// </summary>
        protected List<BalanceSheetBillListInfo> GetBalanceSheetInfo(ViewBalanceSheetBillListCollection bsInfos)
        {
            var vstInfo = new List<BalanceSheetBillListInfo>();

            if (bsInfos != null && bsInfos.Count > 0)
            {
                foreach (var vbs in bsInfos.GroupBy(x => x.Id).Select(x => new
                {
                    sellerName = x.First().SellerName,
                    year = x.First().Year,
                    month = x.First().Month,
                    uniqueId = x.First().UniqueId,
                    name = x.First().Name,
                    id = x.First().Id,
                    productType = x.First().ProductType,
                    productGuid = x.First().ProductGuid,
                    isConfirmedReadyToPay = x.First().IsConfirmedReadyToPay,
                    sellerGuid = x.First().SellerGuid,
                    storeGuid = x.First().StoreGuid,
                    storeName = x.First().StoreName,
                    vendorReceiptType = x.First().VendorReceiptType,
                    remittanceType = x.First().RemittanceType,
                    message = x.First().Message,
                    companyId = x.First().CompanyID,
                    generationFrequency = x.First().GenerationFrequency,
                    intervalEnd = x.First().IntervalEnd,
                    deliveryType = x.First().DeliveryType,
                    DeptId = x.First().DeptId,
                    IspFamilyAmount = x.First().IspFamilyAmount,
                    IspSevenAmount = x.First().IspSevenAmount,
                    WmsOrderAmount = x.First().WmsOrderAmount
                }).OrderBy(x => x.uniqueId).ThenBy(x => x.storeName).ThenBy(x => x.year).ThenBy(x => x.month))

                {
                    var info = new BalanceSheetBillListInfo();
                    bool isTax;
                    var balanceSheet = new BalanceSheetModel(vbs.id);

                    if (vbs.productType.Equals((int)BusinessModel.Ppon))
                    {
                        isTax = pp.DealAccountingGet(vbs.productGuid).IsInputTaxRequired;
                    }
                    else
                    {
                        isTax = !hp.HiDealProductGet(vbs.productGuid).IsInputTaxFree;
                    }
                    info.Tax = isTax ? config.BusinessTax : 0;

                    //週結月對帳單顯示實付金額 其餘顯示應付金額(發票金額)
                    if (vbs.generationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet &&
                        (vbs.remittanceType == (int)RemittanceType.AchWeekly ||
                            vbs.remittanceType == (int)RemittanceType.ManualWeekly))
                    {
                        info.InvoiceTotal = balanceSheet.TransferInfo.AccountsPaid.GetValueOrDefault(0);
                        info.FreightAmounts = 0;
                        info.AdjustmentAmount = 0;
                        info.PositivePaymentOverdueAmount = 0;
                        info.NegativePaymentOverdueAmount = 0;
                    }
                    else
                    {
                        //應付總額
                        info.InvoiceTotal = (int)(balanceSheet.GetAccountsPayable().ReceiptAmount);
                        info.FreightAmounts = (int)(balanceSheet.GetAccountsPayable().FreightAmounts);
                        info.AdjustmentAmount = (int)(balanceSheet.GetAccountsPayable().AdjustmentAmount);
                        info.PositivePaymentOverdueAmount = balanceSheet.GetVendorPaymentOverdueAmount(true);
                        info.NegativePaymentOverdueAmount = balanceSheet.GetVendorPaymentOverdueAmount(false);
                    }

                    //超取物流費用
                    info.IspFamilyAmount = 0 - vbs.IspFamilyAmount;
                    info.IspSevenAmount = 0 - vbs.IspSevenAmount;

                    //PCHOME物流費用
                    info.WmsOrderAmount = 0 - vbs.WmsOrderAmount;

                    //未稅金額
                    info.SubTotal = int.Parse(Math.Round(info.InvoiceTotal / (1 + info.Tax), 0).ToString());
                    //稅額
                    info.GstTotal = info.InvoiceTotal - info.SubTotal;

                    //對帳單所有單據的金額
                    var amount = balanceSheet.GetDistributedAmount();
                    info.ResidualInvoiceTotal = info.InvoiceTotal - amount;
                    info.ResidualSubTotal = info.SubTotal - int.Parse(Math.Round(amount / (1 + info.Tax), 0).ToString());
                    info.ResidualGstTotal = info.ResidualInvoiceTotal - info.ResidualSubTotal;
                    info.Id = vbs.id;
                    info.ProductGuid = vbs.productGuid;
                    info.IsConfirmedReadyToPay = vbs.isConfirmedReadyToPay;
                    info.UniqueId = vbs.uniqueId;
                    info.Year = vbs.year.Value;
                    info.Month = vbs.month.Value;
                    info.Name = vbs.name;
                    info.StoreGuid = vbs.storeGuid;
                    info.SellerGuid = vbs.sellerGuid;
                    info.StoreName = vbs.storeName;
                    info.SellerName = vbs.sellerName;

                    //單據開立方式
                    info.VendorReceiptType = vbs.vendorReceiptType;

                    //單據開立方式為收據,沒有稅率的考量
                    //單據開立方式為其他 輸入規則同收據
                    if (info.VendorReceiptType == (int)VendorReceiptType.Receipt ||
                        info.VendorReceiptType == (int)VendorReceiptType.Other)
                    {
                        info.GstTotal = info.ResidualGstTotal = 0;
                        info.ResidualSubTotal = info.ResidualInvoiceTotal;
                        info.SubTotal = info.InvoiceTotal;
                    }
                    info.RemittanceType = vbs.remittanceType;
                    info.Message = vbs.message;
                    info.CompanyID = vbs.companyId;
                    info.GenerationFrequency = vbs.generationFrequency;
                    info.IntervalEnd = vbs.intervalEnd;
                    info.DeliveryType = vbs.deliveryType.Value;
                    info.DeptId = vbs.DeptId;
                    info.IsTaxRequire = vbs.vendorReceiptType == (int)VendorReceiptType.Invoice && isTax;
                    vstInfo.Add(info);

                    
                }
            }
            return vstInfo;
        }

        protected List<BalanceSheetBillListInfo> GetBalanceSheetWmsInfo(ViewBalanceSheetWmsBillListCollection bsInfos)
        {
            var vstInfo = new List<BalanceSheetBillListInfo>();

            if (bsInfos != null && bsInfos.Count > 0)
            {
                foreach (var vbs in bsInfos.GroupBy(x => x.Id).Select(x => new
                {
                    sellerName = x.First().SellerName,
                    year = x.First().Year,
                    month = x.First().Month,
                    name = x.First().Name,
                    id = x.First().Id,
                    isConfirmedReadyToPay = x.First().IsConfirmedReadyToPay,
                    sellerGuid = x.First().SellerGuid,
                    remittanceType = x.First().RemittanceType,
                    generationFrequency = x.First().GenerationFrequency,
                    vendorReceiptType = x.First().VendorReceiptType,
                    intervalEnd = x.First().IntervalEnd,
                    deliveryType = x.First().DeliveryType,
                    Wmsamount = x.First().WmsAmount,
                }).OrderBy(x => x.year).ThenBy(x => x.month))

                {
                    var info = new BalanceSheetBillListInfo();

                    info.Id = vbs.id;
                    info.Name = vbs.name;
                    info.Year = vbs.year;
                    info.Month = vbs.month;
                    info.DeliveryType = vbs.deliveryType;
                    info.SellerName = vbs.sellerName;
                    info.SellerGuid = vbs.sellerGuid;
                    info.RemittanceType = (int)vbs.remittanceType;
                    info.GenerationFrequency = vbs.generationFrequency;
                    info.VendorReceiptType = (int)vbs.vendorReceiptType;
                    info.IntervalEnd = vbs.intervalEnd;
                    info.WmsAmount = vbs.Wmsamount;
                    vstInfo.Add(info);
                    

                }
            }
            return vstInfo;
        }

        protected List<BalanceSheetBillListInfo> GetBillListInfo(ViewBalanceSheetBillListCollection billLists)
        {
            var vstInfo = new List<BalanceSheetBillListInfo>();
            var balanceSheetTotal = new Dictionary<int, int>();
            var balanceSheetFreightTotal = new Dictionary<int, int>();
            var balanceSheetAdjustmentTotal = new Dictionary<int, int>();
            var balanceSheetPositivePaymentOverdueTotal = new Dictionary<int, int>();
            var balanceSheetNegativePaymentOverdueTotal = new Dictionary<int, int>();
            var balanceSheetFamiAmount = new Dictionary<int, int>();
            var balanceSheetSevenAmount = new Dictionary<int, int>();
            var balanceSheetWmsOrderAmount = new Dictionary<int, int>();
            foreach (var bill in billLists)
            {
                var info = new BalanceSheetBillListInfo();
                //將計算出的對帳單金額寫入Dictionary 在算對帳單金額之前先到Dictionary去尋找是否已有對帳單金額資料
                //Dictionary中找不到的話 再去進行計算
                //若多張單據對應一張對帳單 也無須重複計算
                if (!balanceSheetTotal.ContainsKey(bill.Id))
                {
                    var balanceSheet = new BalanceSheetModel(bill.Id);
                    if (bill.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.MonthBalanceSheet) &&
                        (bill.RemittanceType == (int)RemittanceType.AchWeekly ||
                         bill.RemittanceType == (int)RemittanceType.ManualWeekly))
                    {
                        var invoiceTotal = balanceSheet.TransferInfo.AccountsPaid.GetValueOrDefault(0);
                        balanceSheetTotal.Add(bill.Id, invoiceTotal);
                        balanceSheetFreightTotal.Add(bill.Id, 0);
                        balanceSheetAdjustmentTotal.Add(bill.Id, 0);
                        balanceSheetPositivePaymentOverdueTotal.Add(bill.Id, 0);
                        balanceSheetNegativePaymentOverdueTotal.Add(bill.Id, 0);
                        balanceSheetFamiAmount.Add(bill.Id, 0);
                        balanceSheetSevenAmount.Add(bill.Id, 0);
                        balanceSheetWmsOrderAmount.Add(bill.Id, 0);
                    }
                    else
                    {
                        balanceSheetTotal.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().TotalAmount));
                        balanceSheetFreightTotal.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().FreightAmounts));
                        balanceSheetAdjustmentTotal.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().AdjustmentAmount));
                        balanceSheetPositivePaymentOverdueTotal.Add(bill.Id, (int)(balanceSheet.GetVendorPaymentOverdueAmount(true)));
                        balanceSheetNegativePaymentOverdueTotal.Add(bill.Id, (int)(balanceSheet.GetVendorPaymentOverdueAmount(false)));
                        balanceSheetFamiAmount.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().ISPFamilyAmount));
                        balanceSheetSevenAmount.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().ISPSevenAmount));
                        balanceSheetWmsOrderAmount.Add(bill.Id, (int)(balanceSheet.GetAccountsPayable().WmsOrderAmount));
                    }
                }
                info.EstAmount = balanceSheetTotal[bill.Id];
                info.FreightAmounts = balanceSheetFreightTotal[bill.Id];
                info.AdjustmentAmount = balanceSheetAdjustmentTotal[bill.Id];
                info.PositivePaymentOverdueAmount = balanceSheetPositivePaymentOverdueTotal[bill.Id];
                info.NegativePaymentOverdueAmount = balanceSheetNegativePaymentOverdueTotal[bill.Id];
                balanceSheetPositivePaymentOverdueTotal[bill.Id] = 0;
                balanceSheetNegativePaymentOverdueTotal[bill.Id] = 0;

                info.IspFamilyAmount = 0 - balanceSheetFamiAmount[bill.Id];
                info.IspSevenAmount = 0 - balanceSheetSevenAmount[bill.Id];
                info.WmsOrderAmount = 0 - balanceSheetWmsOrderAmount[bill.Id];
                info.VendorInvoiceNumber = bill.VendorInvoiceNumber;
                info.VendorInvoiceNumberTime = bill.VendorInvoiceNumberTime;
                balanceSheetFamiAmount[bill.Id] = 0;
                balanceSheetSevenAmount[bill.Id] = 0;
                balanceSheetWmsOrderAmount[bill.Id] = 0;

                info.Id = bill.Id;
                info.ProductGuid = bill.ProductGuid;
                info.UniqueId = bill.UniqueId;
                info.Year = bill.Year.Value;
                info.Month = bill.Month.Value;
                info.Name = bill.Name;
                info.VendorReceiptType = bill.VendorReceiptType;
                info.BalanceSheetUseType = (int)BalanceSheetUseType.Deal;
                info.Message = bill.Message;
                info.BillNumber = bill.BillNumber;
                info.InvoiceDate = bill.InvoiceDate.Value;
                info.BillMoney = bill.BillMoney.Value;
                info.BillMoneyNotaxed = bill.BillMoneyNotaxed.Value;
                info.BillTax = bill.BillTax.Value;
                info.Remark = bill.Remark;
                info.BlRemark = bill.BlRemark;
                info.BuyerType = bill.BuyerType.Value;
                info.BillId = bill.BillId.Value;
                info.BillSentDate = bill.BillSentDate;
                info.FinanceGetDate = bill.FinanceGetDate;
                info.IsConfirmedReadyToPay = bill.IsConfirmedReadyToPay;
                info.StoreGuid = bill.StoreGuid;
                info.StoreName = bill.StoreName;
                info.RemittanceType = bill.RemittanceType;
                info.DealEndTime = bill.DealEndTime.Value;
                info.CreateTime = bill.CreateTime.Value;
                info.CompanyAccount = bill.CompanyAccount;
                info.CompanyAccountName = bill.CompanyAccountName;
                info.CompanyBankCode = bill.CompanyBankCode;
                info.CompanyBranchCode = bill.CompanyBranchCode;
                info.CompanyID = bill.CompanyID;
                info.CompanyName = bill.CompanyName;
                info.EmpName = bill.DeEmpName + (!string.IsNullOrEmpty(bill.OpEmpName) ? "/" + bill.OpEmpName : "");
                info.InvoiceComId = bill.InvoiceComId;
                info.GenerationFrequency = bill.GenerationFrequency;
                info.IntervalEnd = bill.IntervalEnd;
                info.CreateId = bill.CreateId;
                info.DeliveryType = bill.DeliveryType.Value;
                info.DefaultPaymentTime = bill.DefaultPaymentTime;
                vstInfo.Add(info);
            }
            return vstInfo;
        }

        protected List<BalanceSheetBillListInfo> GetBillWmsListInfo(ViewBalanceSheetWmsBillListCollection billLists)
        {
            var vstInfo = new List<BalanceSheetBillListInfo>();
            var balanceSheetTotal = new Dictionary<int, int>();
            
            var balanceSheetWmsAmount = new Dictionary<int, int>();
            foreach (var bill in billLists)
            {
                var info = new BalanceSheetBillListInfo();
                //將計算出的對帳單金額寫入Dictionary 在算對帳單金額之前先到Dictionary去尋找是否已有對帳單金額資料
                //Dictionary中找不到的話 再去進行計算
                //若多張單據對應一張對帳單 也無須重複計算
                if (!balanceSheetTotal.ContainsKey(bill.Id))
                {
                    var balanceSheet = new BalanceSheetWmsModel(bill.Id);
                    balanceSheetTotal.Add(bill.Id, (int)(balanceSheet.GetWmsAmount()));
                    balanceSheetWmsAmount.Add(bill.Id, (int)(balanceSheet.GetWmsAmount()));
                }

                info.EstAmount = balanceSheetTotal[bill.Id];
                info.WmsAmount = balanceSheetWmsAmount[bill.Id];
                info.VendorInvoiceNumber = bill.VendorInvoiceNumber;//?
                info.VendorInvoiceNumberTime = bill.VendorInvoiceNumberTime;//?
                info.Id = bill.Id;
                info.Year = bill.Year;
                info.Month = bill.Month;
                info.Name = bill.Name;
                info.VendorReceiptType = (int)bill.VendorReceiptType;
                info.BalanceSheetUseType = (int)BalanceSheetUseType.Wms;
                info.Message = bill.Message;
                info.BillNumber = bill.BillNumber;
                info.InvoiceDate = bill.InvoiceDate.Value;
                info.BillMoney = bill.BillMoney.Value;
                info.BillMoneyNotaxed = bill.BillMoneyNotaxed.Value;
                info.BillTax = bill.BillTax.Value;
                info.Remark = bill.Remark;
                info.BlRemark = bill.BlRemark;
                info.BuyerType = bill.BuyerType.Value;
                info.BillId = bill.BillId.Value;
                info.BillSentDate = bill.BillSentDate;
                info.FinanceGetDate = bill.FinanceGetDate;
                info.IsConfirmedReadyToPay = bill.IsConfirmedReadyToPay;
                info.RemittanceType = (int)bill.RemittanceType;
                info.CreateTime = bill.CreateTime.Value;
                info.CompanyAccount = bill.CompanyAccount;
                info.CompanyAccountName = bill.CompanyAccountName;
                info.CompanyBankCode = bill.CompanyBankCode;
                info.CompanyBranchCode = bill.CompanyBranchCode;
                info.CompanyID = bill.CompanyID;
                info.CompanyName = bill.CompanyName;
                info.EmpName = bill.DeEmpName + (!string.IsNullOrEmpty(bill.OpEmpName) ? "/" + bill.OpEmpName : "");
                info.InvoiceComId = bill.InvoiceComId;
                info.GenerationFrequency = bill.GenerationFrequency;
                info.IntervalEnd = bill.IntervalEnd;
                info.CreateId = bill.CreateId;
                info.DeliveryType = bill.DeliveryType;
                info.DefaultPaymentTime = bill.DefaultPaymentTime;
                vstInfo.Add(info);
            }
            return vstInfo;
        }

        protected List<UnConfirmBalanceSheet> GetUnConfirmBalanceSheetInfo(ViewBalanceSheetBillListCollection bsInfos)
        {
            var vstInfo = new List<UnConfirmBalanceSheet>();
            if (bsInfos != null && bsInfos.Count > 0)
            {
                foreach (var vbs in bsInfos.GroupBy(x => x.Id).Select(x => new
                {
                    ProductGuid = x.First().ProductGuid,
                    ProductType = x.First().ProductType,
                    BalanceSheetId = x.First().Id,
                    BillId = x.First().BillId,
                    Year = x.First().Year,
                    Month = x.First().Month,
                    IntervalEnd = x.First().IntervalEnd,
                    UniqueId = x.First().UniqueId,
                    Name = x.First().Name,
                    StoreGuid = x.First().StoreGuid,
                    StoreName = x.First().StoreName,
                    RemittanceType = x.First().RemittanceType,
                    VendorReceiptType = x.First().VendorReceiptType,
                    Message = x.First().Message,
                    GenerationFrequency = x.First().GenerationFrequency,
                    AccountantName = x.First().AccountantName,
                    AccountantTel = x.First().AccountantTel,
                    AccountantEmail = x.First().AccountantEmail,
                    EmpName = x.First().DeEmpName
                }).OrderBy(x => x.UniqueId).ThenBy(x => x.Year).ThenBy(x => x.Month))
                {
                    var balanceSheet = new BalanceSheetModel(vbs.BalanceSheetId);
                    //對帳單所有單據的金額
                    var amount = balanceSheet.GetDistributedAmount();
                    //對帳單實付金額
                    int balalnceSheetTotal;
                    if (vbs.ProductType.Equals((int)BusinessModel.Ppon) &&
                        vbs.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.MonthBalanceSheet) &&
                        (vbs.RemittanceType == (int)RemittanceType.AchWeekly))
                    {
                        balalnceSheetTotal = (int)(balanceSheet.TransferInfo.AccountsPaid.HasValue ? balanceSheet.TransferInfo.AccountsPaid.Value : balanceSheet.TransferInfo.AccountsPayable);
                    }
                    else
                    {
                        balalnceSheetTotal = (int)balanceSheet.GetAccountsPayable().ReceiptAmount;
                    }
                    //對帳單剩餘金額 = 對帳單實付金額 - 已開立單據金額
                    var residualBalalnceSheetTotal = balalnceSheetTotal - amount;

                    //對帳單剩餘金額>0才需顯示
                    if (residualBalalnceSheetTotal <= 0)
                    {
                        continue;
                    }

                    var info = new UnConfirmBalanceSheet
                    {
                        ProductGuid = vbs.ProductGuid,
                        ProductType = vbs.ProductType,
                        BalanceSheetId = vbs.BalanceSheetId,
                        BillId = vbs.BillId,
                        Year = vbs.Year,
                        Month = vbs.Month,
                        IntervalEnd = vbs.IntervalEnd,
                        UniqueId = vbs.UniqueId,
                        Name = vbs.Name,
                        StoreGuid = vbs.StoreGuid,
                        StoreName = vbs.StoreName,
                        VendorReceiptType = vbs.VendorReceiptType,
                        Message = vbs.Message,
                        RemittanceType = vbs.RemittanceType,
                        GenerationFrequency = vbs.GenerationFrequency,
                        AccountantName = vbs.AccountantName,
                        AccountantTel = vbs.AccountantTel,
                        AccountantEmail = vbs.AccountantEmail,
                        BalanceSheetTotal = balalnceSheetTotal,
                        ResidualBalanceSheetTotal = residualBalalnceSheetTotal,
                        EmpName = vbs.EmpName
                    };

                    vstInfo.Add(info);
                }
            }

            return vstInfo;
        }

        public Dictionary<string, string> GetDept()
        {
            var deptList = new Dictionary<string, string>();
            deptList.Add("", "業務所屬篩選");
            deptList.Add("All", "全部");
            deptList = deptList.Concat(hump.DepartmentGetListByEnabled(true, EmployeeDept.S000).Select(x => new { DeptId = x.DeptId, DeptName = x.DeptName.Replace("業務部(", "").Replace(")", "") }).ToDictionary(x => x.DeptId, x => x.DeptName)).ToDictionary(x => x.Key, x => x.Value);

            return deptList;
        }

        /// <summary>
        /// 對帳單備註
        /// </summary>
        /// <param name="balanceSheetId"></param>
        /// <param name="billId"></param>
        /// <param name="bsRemark"></param>
        /// <param name="type"></param>
        public void UpdateBsRemark(int balanceSheetId, int billId, string bsRemark, BalanceSheetUseType type)
        {
            var bill = ap.BillGet(billId);
            if (type == BalanceSheetUseType.Deal)
            {
                var rel = ap.BalanceSheetBillRelationshipGet(balanceSheetId, billId);
                if (rel.IsLoaded)
                {
                    rel.Remark = bsRemark;
                    rel.ModifyId = View.UserName;
                    rel.ModifyTime = DateTime.Now;
                    ap.BalanceSheetBillRelationshipSet(rel);
                }

                BillRepository.SetBillBalanceSheetChangeLog(bill, new List<BalanceSheetBillRelationship> { rel }, null);
            }
            else
            {
                var relw = ap.BalanceSheetWmsBillRelationshipGet(balanceSheetId, billId);
                if (relw.IsLoaded)
                {
                    relw.Remark = bsRemark;
                    relw.ModifyId = View.UserName;
                    relw.ModifyTime = DateTime.Now;
                    ap.BalanceSheetWmsBillRelationshipSet(relw);
                }
                BillRepository.SetBillBalanceSheetChangeLog(bill, null, new List<BalanceSheetWmsBillRelationship> { relw });
            }
            
        }

        /// <summary>
        /// 單據備註
        /// </summary>
        /// <param name="billId"></param>
        /// <param name="billRemark"></param>
        public void UpdateBillRemark(int billId, string billRemark)
        {
            var bill = ap.BillGet(billId);
            bill.Remark = billRemark;
            bill.ModifyId = View.UserName;
            bill.ModifyTime = DateTime.Now;
            ap.BillSet(bill);
            BillRepository.SetBillBalanceSheetChangeLog(bill, null, null);
        }

        #endregion
    }
}

﻿using System;
using System.Linq;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System.Text.RegularExpressions;
using System.Web;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class BrandEventPresenter : Presenter<IBrandEvent>
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.EnableMobileEventPromo = cp.EnableMobileEventPromoPage;
            Brand brand = pp.GetBrand(View.Url);

            #region 收藏
            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            View.SetMemberCollection(mp.MemberCollectDealGetOnlineDealGuids(
                View.UserId, baseDate.AddDays(-1), baseDate.AddDays(1)));
            #endregion

            //主題活動存在，且時間符合展示時間，可顯示於WEB，當有Preview參數時，為預覽模式，不考慮時間與顯示於WEB設定
            if (brand.IsLoaded)
            {
                if (((brand.StartTime <= DateTime.Now && DateTime.Now <= brand.EndTime && brand.ShowInWeb) || !string.IsNullOrEmpty(View.Preview)))
                {
                    View.BrandId = brand.Id;
                    View.Rsrc = brand.Cpa;
                    View.BrandTitle = brand.BrandName;
                    View.jsContentIds = PponFacade.GetBrandDealBid(brand, (int)FacebookPixelCode.JsCode);
                    View.nsContentIds = PponFacade.GetBrandDealBid(brand, (int)FacebookPixelCode.NsCode);
                    View.MetaDescription = brand.MetaDescription;
                    
                    if (GetBrandItems(brand))
                    {
                        var shareTitle = "";
                        if (!string.IsNullOrEmpty(brand.DiscountList))
                        {
                            shareTitle = string.Format("{0}（可領取折價折價券）", brand.BrandName);
                            View.DiscountPageShow = true;
                        }
                        else
                        {
                            shareTitle = string.Format("{0}（商品破盤價）", brand.BrandName);
                            View.DiscountPageShow = false;
                        }
                        View.OgTitle = shareTitle;

                        string tempMainPic = ImageFacade.GetHtmlFirstSrc(brand.MainPic);
                        if (tempMainPic == string.Empty) tempMainPic = brand.MainPic;
                        View.OgImage = (!string.IsNullOrEmpty(brand.FbShareImage)) ? ImageFacade.GetMediaPathsFromRawData(brand.FbShareImage, MediaType.DealPromoImage).First() : ImageFacade.GetMediaPath(tempMainPic, MediaType.DealPromoImage);
                        View.OgImageWidth = 1200;
                        View.OgImageHeight = 630;

                        var link = PromotionFacade.GetCurationTwoLink(brand);
                        View.Share_FB_Url = string.Format("https://www.facebook.com/dialog/share?app_id={0}&display=popup&href={1}&redirect_uri={2}",
                            config.FacebookShareAppId,
                            HttpUtility.UrlEncode(link),
                            HttpUtility.UrlEncode(link)
                        );

                        View.LinkCanonicalUrl = View.OgUrl = HttpContext.Current.Request.Url.AbsoluteUri.Replace("&p=show_me_the_preview", "");

                        if (View.LatestUrl != null && View.LatestUrl.ToString().ToLower().Contains("themecurationchannel"))
                        {
                            View.BackMainUrl = "<a href=\"/event/themecurationchannel\">回主題活動，搶得更多優惠</a>";
                        }
                        else if (View.LatestUrl != null && View.LatestUrl.ToString().ToLower().Contains("exhibitionlist"))
                        {
                            var themeId = HttpUtility.ParseQueryString(View.LatestUrl.Query)["id"];
                            if (themeId != null)
                            {
                                ThemeCurationMain themeMain = pp.GetThemeCurationMainById(int.Parse(themeId));
                                View.BackMainUrl = string.Format("<a href=\"/event/exhibitionlist.aspx?id={0}\">回{1}，搶得更多優惠</a>", themeId, themeMain.Title);
                            }
                            else
                            {
                                View.BackMainUrl = "<a href=\"/Default.aspx\">回首頁</a>";
                            }
                        }
                        else
                        {
                            var themeList = pp.GetViewThemeCurationByCurationId(brand.Id).Where(x => x.MainStart < DateTime.Now && x.MainEnd > DateTime.Now).ToList();
                            if (themeList.Count > 0)
                            {
                                var nowEvent = themeList.OrderByDescending(x => x.GroupStart).First();
                                if (nowEvent.MainBackgroundImage == null)
                                {
                                    View.BackMainUrl = "<a href=\"/event/themecurationchannel\">回主題活動，搶得更多優惠</a>";
                                }
                                else
                                {
                                    View.BackMainUrl = string.Format("<a href=\"/event/exhibitionlist.aspx?id={0}\">回{1}，搶得更多優惠</a>", nowEvent.MainId, nowEvent.MainTitle);
                                }
                            }
                            else
                            {
                                View.BackMainUrl = "<a href=\"/default.aspx\">回首頁</a>";
                            }
                        }
                    }
                    else
                    {
                        View.ShowEventExpire();
                    }
                }
                else
                {
                    View.ShowEventExpire();
                }
            }
            else
            {
                View.ShowEventExpire();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
        #region method
        public bool GetBrandItems(Brand brand)
        {
            List<ViewBrandCategory> vepctmp = pp.GetViewBrandCategoryByBrandId(brand.Id)
                .Where(x =>x.ItemStatus).OrderBy(x => x.Seq).OrderBy(x => x.BrandItemCategoryId).ToList();
            List<ViewBrandCategory> items = new List<ViewBrandCategory>();
            foreach (var tmp in vepctmp)
            {
                IViewPponDeal ppdeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(tmp.BusinessHourGuid, true);
                //停賣
                if (Helper.IsFlagSet(ppdeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                {
                    continue;
                }
                //開檔時間還沒到
                if (DateTime.Now < ppdeal.BusinessHourOrderTimeS)
                {
                    continue;
                }
                //已過結檔時間
                if (DateTime.Now > ppdeal.BusinessHourOrderTimeE)
                {
                    continue;
                }
                //已註記結檔
                if (Helper.IsFlagSet((long)ppdeal.GroupOrderStatus, GroupOrderStatus.Completed))
                {
                    continue;
                }
                //售完的放後面
                if (ppdeal.IsSoldOut)
                {
                    tmp.Seq = 9999 + tmp.Seq;
                }
                items.Add(tmp);
            }
            View.ShowBrandEvent(brand, items);
            return vepctmp.Count > 0;
        }
        #endregion
    }
}

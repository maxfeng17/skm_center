﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.UI;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using LunchKingSite.WebLib.Controllers;
using System.Transactions;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class AtmAchPresenter : Presenter<IAtmAchView>
    {
        private IOrderProvider op;
        private IPponProvider pp;
        private IAccountingProvider ap;

        public AtmAchPresenter()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        }
        
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.BatchImportClicked += OnBatchImportClicked;
            View.BatchImportManualClicked += OnBatchImportManualClicked;
            return true;
        }

        protected void OnBatchImportClicked(object sender, DataEventArgs<StreamReader> e)
        {
            StreamReader sReader = (StreamReader)e.Data;                  //要匯入的FileStream

            AchTextFile file = new AchTextFile();
            file.Reader = sReader;
            AchBatchTransactionResult batchResult = file.Load();
            string result = ImportCtAchRequest(batchResult);
            View.ImportResult = result;

            BalanceSheetService.CompleteWeekBalanceSheetMonthInfo();
        }


        protected void OnBatchImportManualClicked(object sender, DataEventArgs<StreamReader> e)
        {
            StreamReader sReader = (StreamReader)e.Data;                  //要匯入的FileStream

            ManualTextFile file = new ManualTextFile();
            file.Reader = sReader;
            ManualBatchTransactionResult batchResult = file.Load();
            string result = ImportCtManualRequest(batchResult);
            View.ImportResult = result;

            BalanceSheetService.CompleteWeekBalanceSheetMonthInfo();
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>匯入</returns>
        private string ImportCtAchRequest(AchBatchTransactionResult batchResult)
        {
            if(batchResult.IsCorrupted)
            {
                return "匯入失敗! 檔案格式不正確!";
            }

            int amountMismatchCount = 0;
            List<int> amountMismatchDealId = new List<int>();
            int accountTitleMismatchCount = 0;
            List<int> accountTitleMismatchDealId = new List<int>();

            WeeklyPayReportCollection updates = new WeeklyPayReportCollection();
            List<AchTransactionResult> validResults = batchResult.TransactionResults.Where(x => !x.IsCorrupted).ToList();
            foreach(var achTrxResult in validResults)
            {
                WeeklyPayReport wpr = op.GetWeeklyPayReportById(achTrxResult.WeeklyPayReportId);
                if (wpr == null || !wpr.IsLoaded)
                {
                    continue;
                }

                #region 匯款金額 = 實付/應付金額

                if (int.Equals((int)(wpr.TransferAmount ?? wpr.TotalSum), achTrxResult.Amount))
                {
                    //金額相同的意思: 當作匯款給分店 or 沒有設定分店的檔次
                    //註: 若是多分店匯給賣家, 但其他分店金額為 0, 因為不會匯出到 ACH txt 檔, 所以可當一家分店來處理
                    wpr.ResponseTime = batchResult.BatchProcessedTime;
                    wpr.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                    if(achTrxResult.Result.IsSuccess)
                    {
                        //付款失敗時, 不填實付金額
                        wpr.TransferAmount = achTrxResult.Amount;
                        wpr.IsTransferComplete = true;
                    }
                    wpr.ReceiverBankNo = achTrxResult.BankNumber;
                    wpr.ReceiverBranchNo = achTrxResult.BranchNumber;
                    wpr.ReceiverAccountNo = achTrxResult.AccountNumber;
                    wpr.ReceiverCompanyId = achTrxResult.CompanyId;

                    string receiverTitle;
                    if (TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber, achTrxResult.CompanyId, out receiverTitle))
                    {
                        wpr.ReceiverTitle = receiverTitle;
                    }
                    else
                    {
                        if (achTrxResult.Result.IsSuccess)   //轉帳失敗就不需填寫戶名的提醒
                        {
                            accountTitleMismatchCount++;
                            accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                        }
                    }

                    updates.Add(wpr);
                    continue;
                }

                #endregion

                //程式執行到這裡代表實付金額與 wpr 的應付金額對不起來
                //多撈點資訊檢查是否匯給賣家
                ViewWeeklyPayReport vrSeller = op.GetViewWeeklyPayReportById(achTrxResult.WeeklyPayReportId).FirstOrDefault() ?? new ViewWeeklyPayReport();
                if (vrSeller.IsLoaded
                    && int.Equals(0, vrSeller.Paytocompany))
                {
                    #region 匯給分店
                    //匯給分店, 金額對不起來就算了, 把金額以外的結果寫入
                    wpr.ResponseTime = batchResult.BatchProcessedTime;
                    wpr.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                    if(achTrxResult.Result.IsSuccess)
                    {
                        //wpr.TransferAmount 實付金額不可以填入或蓋過
                        //財會希望能突出 "金額對不起來但是有成功付款的"
                        wpr.Result = string.Format("-{0}", achTrxResult.Result.Code);
                        wpr.IsTransferComplete = false;     //由財會用匯款管理功能去確認資料
                    }
                    wpr.ReceiverBankNo = achTrxResult.BankNumber;
                    wpr.ReceiverBranchNo = achTrxResult.BranchNumber;
                    wpr.ReceiverAccountNo = achTrxResult.AccountNumber;
                    wpr.ReceiverCompanyId = achTrxResult.CompanyId;

                    string receiverTitle;
                    if (TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber,achTrxResult.CompanyId, out receiverTitle))
                    {
                        wpr.ReceiverTitle = receiverTitle;
                    }
                    else
                    {
                        if (achTrxResult.Result.IsSuccess)   //轉帳失敗就不需填寫戶名的提醒
                        {
                            accountTitleMismatchCount++;
                            accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                        }
                    }

                    updates.Add(wpr);

                    #endregion
                }
                else
                {
                    #region 匯給賣家

                    var reports = op.WeeklyPayReportGetList(0, 0, null, WeeklyPayReport.Columns.ReportGuid + " = " + wpr.ReportGuid,
                                                                                                                 WeeklyPayReport.Columns.BusinessHourGuid + " = " + wpr.BusinessHourGuid,
                                                                                                                 WeeklyPayReport.Columns.FundTransferType + "=" + ((int)FundTransferType.CtAch).ToString());
                    bool isExpectedAmountEqualActualAmount = int.Equals((int)reports.Sum(x => x.TransferAmount ?? x.TotalSum), achTrxResult.Amount);

                    string receiverTitle;
                    bool receiverTitleValid = TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber, achTrxResult.CompanyId, out receiverTitle);

                    foreach (var report in reports)
                    {
                        report.ResponseTime = batchResult.BatchProcessedTime;
                        report.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                        report.ReceiverBankNo = achTrxResult.BankNumber;
                        report.ReceiverBranchNo = achTrxResult.BranchNumber;
                        report.ReceiverAccountNo = achTrxResult.AccountNumber;
                        report.ReceiverCompanyId = achTrxResult.CompanyId;

                        if(achTrxResult.Result.IsSuccess)
                        {
                            if (isExpectedAmountEqualActualAmount)
                            {
                                report.IsTransferComplete = true;
                                if (report.TransferAmount.HasValue && report.TransferAmount.Value != report.TotalSum)
                                {
                                    report.Result = string.Format("-{0}", achTrxResult.Result.Code);
                                }
                                else
                                {
                                    report.TransferAmount = report.TotalSum;
                                }
                            }
                            else
                            {
                                //無法判斷實際匯款金額, 需提醒使用者哪些檔次有問題
                                //財會希望能突出 "金額對不起來但是有成功付款的"
                                report.Result = string.Format("-{0}", achTrxResult.Result.Code);
                                report.IsTransferComplete = false;
                                amountMismatchCount++;
                                amountMismatchDealId.Add(achTrxResult.DealUniqueId);
                            }
                        }
                        
                        
                        if (receiverTitleValid)
                        {
                            report.ReceiverTitle = receiverTitle;
                        }
                        else 
                        {
                            if (achTrxResult.Result.IsSuccess)   //轉帳失敗的就不需提醒填戶名
                            {
                                accountTitleMismatchCount++;
                                accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                            }
                        }

                        updates.Add(report);
                    }

                    #endregion
                }
            }

            Dictionary<int, VendorBillingModel> billingSystemLookup = GetBillingModel(amountMismatchDealId,
                                                                                         accountTitleMismatchDealId);
          
            int affectedRows = op.WeeklyPayReportCollectionSet(updates);

            string importMsg = string.Format(@"匯入成功! \r\n文字檔總筆數 = {0};\r\n文字檔有效資料筆數 = {1}; \r\n匯入資料量(多分店) = {2}; \r\n檔案實付金額與系統應付金額無法對應數 = {3};{4}\r\n缺受款戶名資料數 = {5};{6}"
                , batchResult.TransactionResults.Count
                , validResults.Count
                , affectedRows
                , amountMismatchCount
                , amountMismatchCount > 0 ? @"\r\n金額不匹配的檔號: " + ConvertToDealIdsWithHint(amountMismatchDealId, billingSystemLookup) + ";" : string.Empty
                , accountTitleMismatchCount
                , accountTitleMismatchCount > 0 ? @"\r\n缺受款戶名的檔號:" + ConvertToDealIdsWithHint(accountTitleMismatchDealId, billingSystemLookup) + ";" : string.Empty
                );
            return importMsg;
        }


        private string ImportCtManualRequest(ManualBatchTransactionResult batchResult)
        {
            if (batchResult.IsCorrupted)
            {
                return "匯入失敗! 檔案格式不正確!";
            }

            int amountMismatchCount = 0;
            List<int> amountMismatchDealId = new List<int>();
            int accountTitleMismatchCount = 0;
            List<int> accountTitleMismatchDealId = new List<int>();

            WeeklyPayReportCollection updates = new WeeklyPayReportCollection();
            List<ManualTransactionResult> validResults = batchResult.TransactionResults.Where(x => !x.IsCorrupted).ToList();
            int existRows = 0;

            foreach (var achTrxResult in validResults)
            {
                WeeklyPayReport wpr = op.GetWeeklyPayReportById(achTrxResult.WeeklyPayReportId);
                if (wpr == null || !wpr.IsLoaded)
                {
                    continue;
                }
                if (wpr.Result != null)
                {
                    existRows++;
                    continue;
                }

                if (wpr.Result != null)
                {
                    existRows++ ;
                    continue;
                }

                #region 匯款金額 = 實付/應付金額

                if (int.Equals((int)(wpr.TransferAmount ?? wpr.TotalSum), achTrxResult.Amount))
                {
                    //金額相同的意思: 當作匯款給分店 or 沒有設定分店的檔次
                    //註: 若是多分店匯給賣家, 但其他分店金額為 0, 因為不會匯出到 ACH txt 檔, 所以可當一家分店來處理
                    wpr.ResponseTime = batchResult.BatchProcessedTime;
                    wpr.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                    if (achTrxResult.Result.IsSuccess)
                    {
                        //付款失敗時, 不填實付金額
                        wpr.TransferAmount = achTrxResult.Amount;
                        wpr.IsTransferComplete = true;
                    }
                    wpr.ReceiverBankNo = achTrxResult.BankNumber;
                    wpr.ReceiverBranchNo = achTrxResult.BranchNumber;
                    wpr.ReceiverAccountNo = achTrxResult.AccountNumber;
                    wpr.ReceiverCompanyId = achTrxResult.CompanyId;
                    wpr.FundTransferType = (int)FundTransferType.FinDept;

                    string receiverTitle;
                    if (TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber, achTrxResult.CompanyId, out receiverTitle))
                    {
                        wpr.ReceiverTitle = receiverTitle;
                    }
                    else
                    {
                        if (achTrxResult.Result.IsSuccess)   //轉帳失敗就不需填寫戶名的提醒
                        {
                            accountTitleMismatchCount++;
                            accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                        }
                    }

                    updates.Add(wpr);
                    continue;
                }

                #endregion

                //程式執行到這裡代表實付金額與 wpr 的應付金額對不起來
                //多撈點資訊檢查是否匯給賣家
                ViewWeeklyPayReport vrSeller = op.GetViewWeeklyPayReportById(achTrxResult.WeeklyPayReportId).FirstOrDefault() ?? new ViewWeeklyPayReport();
                if (vrSeller.IsLoaded
                    && int.Equals(0, vrSeller.Paytocompany))
                {
                    #region 匯給分店
                    //匯給分店, 金額對不起來就算了, 把金額以外的結果寫入
                    wpr.ResponseTime = batchResult.BatchProcessedTime;
                    wpr.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                    if (achTrxResult.Result.IsSuccess)
                    {
                        //wpr.TransferAmount 實付金額不可以填入或蓋過
                        //財會希望能突出 "金額對不起來但是有成功付款的"
                        wpr.Result = string.Format("-{0}", achTrxResult.Result.Code);
                        wpr.IsTransferComplete = false;     //由財會用匯款管理功能去確認資料
                    }
                    wpr.ReceiverBankNo = achTrxResult.BankNumber;
                    wpr.ReceiverBranchNo = achTrxResult.BranchNumber;
                    wpr.ReceiverAccountNo = achTrxResult.AccountNumber;
                    wpr.ReceiverCompanyId = achTrxResult.CompanyId;
                    wpr.FundTransferType = (int)FundTransferType.FinDept;

                    string receiverTitle;
                    if (TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber, achTrxResult.CompanyId, out receiverTitle))
                    {
                        wpr.ReceiverTitle = receiverTitle;
                    }
                    else
                    {
                        if (achTrxResult.Result.IsSuccess)   //轉帳失敗就不需填寫戶名的提醒
                        {
                            accountTitleMismatchCount++;
                            accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                        }
                    }

                    updates.Add(wpr);

                    #endregion
                }
                else
                {
                    #region 匯給賣家

                    var reports = op.WeeklyPayReportGetList(0, 0, null, WeeklyPayReport.Columns.ReportGuid + " = " + wpr.ReportGuid,
                                                                                                                 WeeklyPayReport.Columns.BusinessHourGuid + " = " + wpr.BusinessHourGuid,
                                                                                                                 WeeklyPayReport.Columns.FundTransferType + "=" + ((int)FundTransferType.CtAch).ToString());
                    bool isExpectedAmountEqualActualAmount = int.Equals((int)reports.Sum(x => x.TransferAmount ?? x.TotalSum), achTrxResult.Amount);

                    string receiverTitle;
                    bool receiverTitleValid = TryGetReceiverTitle(achTrxResult.WeeklyPayReportId, achTrxResult.BankNumber, achTrxResult.BranchNumber, achTrxResult.AccountNumber, achTrxResult.CompanyId, out receiverTitle);

                    foreach (var report in reports)
                    {
                        report.ResponseTime = batchResult.BatchProcessedTime;
                        report.Result = achTrxResult.Result.IsNull ? null : achTrxResult.Result.Code;
                        report.ReceiverBankNo = achTrxResult.BankNumber;
                        report.ReceiverBranchNo = achTrxResult.BranchNumber;
                        report.ReceiverAccountNo = achTrxResult.AccountNumber;
                        report.ReceiverCompanyId = achTrxResult.CompanyId;
                        report.FundTransferType = (int)FundTransferType.FinDept;

                        if (achTrxResult.Result.IsSuccess)
                        {
                            if (isExpectedAmountEqualActualAmount)
                            {
                                report.IsTransferComplete = true;
                                if (report.TransferAmount.HasValue && report.TransferAmount.Value != report.TotalSum)
                                {
                                    report.Result = string.Format("-{0}", achTrxResult.Result.Code);
                                }
                                else
                                {
                                    report.TransferAmount = report.TotalSum;
                                }
                            }
                            else
                            {
                                //無法判斷實際匯款金額, 需提醒使用者哪些檔次有問題
                                //財會希望能突出 "金額對不起來但是有成功付款的"
                                report.Result = string.Format("-{0}", achTrxResult.Result.Code);
                                report.IsTransferComplete = false;
                                amountMismatchCount++;
                                amountMismatchDealId.Add(achTrxResult.DealUniqueId);
                            }
                        }


                        if (receiverTitleValid)
                        {
                            report.ReceiverTitle = receiverTitle;
                        }
                        else
                        {
                            if (achTrxResult.Result.IsSuccess)   //轉帳失敗的就不需提醒填戶名
                            {
                                accountTitleMismatchCount++;
                                accountTitleMismatchDealId.Add(achTrxResult.DealUniqueId);
                            }
                        }

                        updates.Add(report);
                    }

                    #endregion
                }
            }

            Dictionary<int, VendorBillingModel> billingSystemLookup = GetBillingModel(amountMismatchDealId,
                                                                                         accountTitleMismatchDealId);

            int affectedRows = op.WeeklyPayReportCollectionSet(updates);

            string importMsg = string.Format(@"匯入成功! \r\n文字檔總筆數 = {0};\r\n文字檔有效資料筆數 = {1}; \r\n匯入資料量(多分店) = {2}; \r\n檔案實付金額與系統應付金額無法對應數 = {3};{4}\r\n缺受款戶名資料數 = {5};{6}\r\n重複匯入資料數 = {7}"
                , batchResult.TransactionResults.Count
                , validResults.Count
                , affectedRows
                , amountMismatchCount
                , amountMismatchCount > 0 ? @"\r\n金額不匹配的檔號: " + ConvertToDealIdsWithHint(amountMismatchDealId, billingSystemLookup) + ";" : string.Empty
                , accountTitleMismatchCount
                , accountTitleMismatchCount > 0 ? @"\r\n缺受款戶名的檔號:" + ConvertToDealIdsWithHint(accountTitleMismatchDealId, billingSystemLookup) + ";" : string.Empty
                , existRows
                );
            return importMsg;
        }



        

        private string ConvertToDealIdsWithHint(List<int> dealIds, Dictionary<int, VendorBillingModel> billingSystemLookup)
        {
            List<string> hintedDealIds = new List<string>();

            foreach(int dealId in dealIds)
            {
                if(billingSystemLookup.ContainsKey(dealId))
                {
                    string billingSystem = billingSystemLookup[dealId] == VendorBillingModel.BalanceSheetSystem
                                               ? "新"
                                               : "舊";
                    hintedDealIds.Add(string.Format("{0}({1})", dealId, billingSystem));
                }
                else
                {
                    hintedDealIds.Add(dealId.ToString());
                }
            }

            return string.Join(",", hintedDealIds);
        }

        private Dictionary<int, VendorBillingModel> GetBillingModel(List<int> amountMismatchDealId, List<int> accountTitleMismatchDealId)
        {
            Dictionary<int, VendorBillingModel> result = new Dictionary<int, VendorBillingModel>();

            List<int> dealIds = new List<int>();
            dealIds.AddRange(amountMismatchDealId);
            dealIds.AddRange(accountTitleMismatchDealId);
            
            List<int> queryIds = dealIds.Distinct().ToList();
            var dps = new DealPropertyCollection();

            foreach(int uid in queryIds)
            {
                var dp = pp.DealPropertyGet(uid);
                if(dp != null && dp.IsLoaded)
                {
                    dps.Add(dp);
                }
            }

            foreach(var dp in dps)
            {
                var da = pp.DealAccountingGet(dp.BusinessHourGuid);
                if(da != null && da.IsLoaded)
                {
                    result.Add(dp.UniqueId, (VendorBillingModel)(da.VendorBillingModel));
                }
            }

            return result;
        }

        //private bool TryGetReceiverTitle(AchTransactionResult achTrxResult, out string receiverTitle)
        private bool TryGetReceiverTitle(
            int WeeklyPayReportId, 
            string BankNumber,
            string BranchNumber,
            string AccountNumber,
            string CompanyId,
            out string receiverTitle)
        {
            string bankNumber;
            string branchNumber;
            string accountNumber;
            string companyId;
            string title;

            ViewWeeklyPayReport vr = op.GetViewWeeklyPayReportById(WeeklyPayReportId).FirstOrDefault() ?? new ViewWeeklyPayReport();

            if(!vr.IsLoaded)
            {
                receiverTitle = string.Empty;
                return false;
            }

            if (int.Equals(1, vr.Paytocompany.GetValueOrDefault(1)))
            {
                bankNumber = vr.BankNo;
                branchNumber = vr.BranchNo;
                accountNumber = vr.AccountNo;
                companyId = vr.AccountId;
                title = vr.AccountName;
            }
            else
            {
                bankNumber = string.IsNullOrWhiteSpace(vr.CompanyBankCode) ? vr.BankNo : vr.CompanyBankCode;
                branchNumber = string.IsNullOrWhiteSpace(vr.CompanyBranchCode) ? vr.BranchNo : vr.CompanyBranchCode;
                accountNumber = string.IsNullOrWhiteSpace(vr.CompanyAccount) ? vr.AccountNo : vr.CompanyAccount;
                companyId = string.IsNullOrWhiteSpace(vr.CompanyID) ? vr.AccountId : vr.CompanyID;
                title = string.IsNullOrWhiteSpace(vr.CompanyAccountName) ? vr.AccountName : vr.CompanyAccountName;
            }

            if(companyId != null)
            {
                companyId = companyId.Trim();
            }
            
            if(string.Equals(bankNumber, BankNumber)
                && string.Equals(branchNumber, BranchNumber)
                && string.Equals(accountNumber, AccountNumber)
                && string.Equals(companyId, CompanyId.Trim(), StringComparison.OrdinalIgnoreCase)
                )
            {
                receiverTitle = title;
                return true;
            }
            
            receiverTitle = string.Empty;
            return false;
        }
     
        

       
    

    }
   


}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Transactions;
using LunchKingSite.BizLogic.Component.MemberActions;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponKindPayPresenter : Presenter<IPponKindPayView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        protected static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        protected static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger("PponKindPayPresenter");
        private static object CouponLock = new object();
     
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            // user is not logged in
            if (string.IsNullOrEmpty(View.UserName))
            {
                View.RedirectToLogin();
                return true;
            }

            MemberUtility.SetUserSsoInfoReady(View.UserId);
            if (string.IsNullOrEmpty(View.TicketId))
            {
                View.TransactionId = null;
                View.ATMAccount = null;
                var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);
                View.TheDeal = theDeal;

                // deal check

                #region 判斷檔次是否使用購物車

                if (View.TheDeal.ShoppingCart.HasValue && View.TheDeal.ShoppingCart.Value)
                {
                    //公益檔不適用Shoppingcart檔次，為檔次人為設定錯誤，調整導回首頁
                    //View.GoToShoppingCartPay();
                    View.GoToDefaulPage();
                }

                #endregion 判斷檔次是否使用購物車

                #region check deal stage

                PponDealStage pponDealStage = View.TheDeal.GetDealStage();
                if (pponDealStage == PponDealStage.RunningAndFull)
                {
                    Guid goBackBid = View.BusinessHourGuid;
                    View.GoToPponDefaultPage(goBackBid, "此檔好康已銷售一空喔。");
                    return true;
                }
                else if (!View.IsPreview && pponDealStage != PponDealStage.Running && pponDealStage != PponDealStage.RunningAndOn)
                {
                    return false;
                }
                else if ((View.TheDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    return false;
                }

                #endregion check deal stage

                #region 檢查是否無法配送離島

                View.NotDeliveryIslands = Helper.IsFlagSet(View.TheDeal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);

                #endregion 檢查是否無法配送離島

                // user check

                #region 檢查 pez 會員

                if ((View.TheDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZeventWithUserId) > 0)
                {
                    string pez_external_userid = MemberFacade.CheckIsPEZUserID(View.UserName);
                    if (string.IsNullOrEmpty(pez_external_userid))
                    {
                        View.RedirectToLoginOnlyPEZ("此活動僅提供PayEasy會員登錄，請用PayEasy帳號重新登入", View.BusinessHourGuid);
                        return true;
                    }
                    Peztemp peztemp = OrderFacade.GetPezTemp(View.BusinessHourGuid, pez_external_userid);
                    if (string.IsNullOrEmpty(peztemp.PezCode))
                    {
                        View.RedirectToLoginOnlyPEZ("很抱歉，您不符合活動資格", View.BusinessHourGuid);
                        return true;
                    }
                }

                #endregion 檢查 pez 會員

                View.CheckRefUrl();

                AccessoryGroupCollection viagc = new AccessoryGroupCollection();
                ViewPponStoreCollection stores = new ViewPponStoreCollection();

                if (View.TheDeal.ItemPrice > 0)
                {
                    #region 選項

                    viagc = ip.AccessoryGroupGetListByItem(View.TheDeal.ItemGuid);
                    foreach (AccessoryGroup ag in viagc)
                    {
                        ag.members = new List<ViewItemAccessoryGroup>();
                        ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(View.TheDeal.ItemGuid, ag.Guid);
                        foreach (ViewItemAccessoryGroup vig in vigc)
                        {
                            ag.members.Add(vig);
                        }
                    }

                    #endregion 選項

                    #region 免運費資訊

                    // 階梯式運費
                    View.TheCouponFreight = pp.CouponFreightGetList(View.TheDeal.BusinessHourGuid, CouponFreightType.Income);

                    #endregion 免運費資訊

                    #region ATM

                    if (config.TurnATMOn)
                    {
                        if (PaymentFacade.CtAtmCheck(View.TheDeal))
                        {
                            View.IsATMAvailable = true;
                        }
                        else
                        {
                            View.IsATMAvailable = false;
                        }

                        if (PaymentFacade.CtAtmCheckZero(View.BusinessHourGuid))
                        {
                            View.IsATMLimitZero = true;
                        }
                        else
                        {
                            View.IsATMLimitZero = false;
                        }
                    }
                    else
                    {
                        View.IsATMAvailable = false;
                        View.IsATMLimitZero = true;
                    }

                    #endregion ATM

                    #region user address info

                    if (string.IsNullOrWhiteSpace(View.AddressInfo))
                    {
                        Member mem = mp.MemberGet(View.UserName);
                        if (mem.IsLoaded)
                        {
                            int cityId = 0;
                            int areaId = 0;
                            string address = string.Empty;
                            Guid buildingGuid = Guid.Empty;
                            if (mem.BuildingGuid.HasValue && !Guid.Equals(MemberUtilityCore.DefaultBuildingGuid, mem.BuildingGuid.Value))
                            {
                                Building bu = lp.BuildingGet(mem.BuildingGuid.Value);
                                City area = lp.CityGet(bu.CityId);
                                City city = lp.CityGet(area.ParentId.Value);                                
                                if (!string.IsNullOrEmpty(mem.CompanyAddress))
                                {
                                    address = mem.CompanyAddress.Replace(city.CityName, string.Empty).Replace(area.CityName, string.Empty);
                                }
                                cityId = city.Id;
                                areaId = area.Id;
                                buildingGuid = bu.Guid;
                            }
                            View.UserInfo = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}",
                                mem.LastName,
                                mem.FirstName,
                                mem.Mobile,
                                cityId,
                                areaId,
                                buildingGuid,
                                address,
                                mem.DisplayName,
                                mem.UserEmail);
                        }
                    }

                    #endregion user address info

                    #region Branch

                    stores = pp.ViewPponStoreGetListByBidWithNoLock(View.BusinessHourGuid, VbsRightFlag.VerifyShop);

                    #endregion Branch
                }
                else
                {
                    View.GoToZeroActivity();
                }

                int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, 
                    (bool)theDeal.IsDailyRestriction);
                View.MultipleBranch = (bool)theDeal.MultipleBranch;
                View.SetContent(View.TheDeal, viagc, alreadyboughtcount, stores, 
                    (EntrustSellType)theDeal.EntrustSell.GetValueOrDefault(), (bool)theDeal.IsDailyRestriction);
            }
            else
            {
                TempSessionCollection tsc = pp.TempSessionGetList(View.TicketId);
                IViewPponDeal vpd = null;
                if (tsc.Count <= 0)
                {
                    View.ShowCreditcardChargeInfo();
                }

                try
                {
                    vpd = View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(new Guid(View.TicketId.Split('_')[1]));
                }
                catch (Exception ex)
                {
                    logger.Info("tickedId=" + View.TicketId);
                    throw ex;
                }

                JsonSerializer serializer = new JsonSerializer();
                PponDeliveryInfo deliveryInfo = serializer.Deserialize<PponDeliveryInfo>(tsc[0].ValueX);

                if (string.IsNullOrWhiteSpace(View.TransactionId))
                {
                    #region get transaction id

                    string tid = View.TransactionId = PaymentFacade.GetTransactionId();

                    #endregion get transaction id

                    DeliveryInfo di = new DeliveryInfo();
                    string usermemo = string.Empty;
                    int orderTotalPay = 0;

                    #region total amout

                    //前端Deal以憑證宅配撈取，所以結帳時不以館別判斷
                    ShoppingCartCollection carts = new ShoppingCartCollection();
                    carts.Add(op.ShoppingCartGet(ShoppingCart.Columns.TicketId, View.TicketId));
                    View.Amount += (int)Math.Round(carts[0].ItemUnitPrice * carts[0].ItemQuantity);
                    SetDeliveryCharge(View.Amount, vpd);
                    View.Amount += View.DeliveryCharge;

                    //訂單總金額
                    orderTotalPay = (int)Math.Round(carts[0].ItemUnitPrice * carts[0].ItemQuantity) + View.DeliveryCharge;

                    #endregion total amout

                    #region delivery info

                    di.DeliveryCharge = View.DeliveryCharge;
                    usermemo = deliveryInfo.DeliveryDateString + deliveryInfo.DeliveryTimeString + deliveryInfo.Notes;

                    if (vpd.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, vpd.DeliveryType.Value)) //好康到店到府消費方式判斷；PponItem=ToHouse
                    {
                        di.CustomerName = deliveryInfo.WriteName;
                        di.CustomerMobile = deliveryInfo.Phone;
                        di.DeliveryAddress = deliveryInfo.WriteAddress;
                    }
                    else
                    {
                        di.DeliveryAddress = PponBuyFacade.GetOrderDesc(deliveryInfo);
                    }

                    #endregion delivery info

                    #region New Version Order Start

                    // make order
                    Guid oid = View.OrderGuid = OrderFacade.MakeOrder(View.UserName, View.TicketId, DateTime.Now,
                        usermemo, di, vpd, View.FromType);

                    Order o = op.OrderGet(oid);

                    o.OrderMemo = ((int)deliveryInfo.ReceiptsType.GetValueOrDefault()).ToString() + "|" + deliveryInfo.InvoiceType + "|" + deliveryInfo.UnifiedSerialNumber + "|" + deliveryInfo.CompanyTitle;
                    op.OrderSet(o);

                    #endregion New Version Order Start

                    PaymentType pType = deliveryInfo.PayType;

                    #region 17購物金折抵

                    if (deliveryInfo.SCash > 0)
                    {
                        OrderFacade.MakeTransaction(View.TransactionId, PaymentType.SCash, int.Parse(deliveryInfo.SCash.ToString("F0")), (DepartmentTypes)View.TheDeal.Department, View.UserName, o.Guid);
                    }

                    #endregion 17購物金折抵

                    //有金額
                    if ((View.Amount > 0))
                    {
                        #region 付款

                        if ((config.BypassPaymentProcess))
                        {
                            OrderFacade.MakeTransaction(tid, PaymentType.ByPass, View.Amount, (DepartmentTypes)vpd.Department, View.UserName, oid);
                            View.ReEnterPaymentGate(pType, tid);
                        }
                        else
                        {
                            PaymentTransaction ptCredit = OrderFacade.MakeTransaction(tid, View.IsPaidByATM ? PaymentType.ATM : PaymentType.Creditcard, View.Amount, (DepartmentTypes)vpd.Department, View.UserName, oid);
                            if (View.IsPaidByATM)
                            {
                                if (!string.IsNullOrWhiteSpace(View.ATMAccount))
                                {
                                    CtAtm ca = op.CtAtmGetByVirtualAccount(View.ATMAccount);
                                    ca.Status = (int)AtmStatus.Expired;
                                    op.CtAtmSet(ca);
                                }
                                View.ATMAccount = PaymentFacade.GetAtmAccount(ptCredit.TransId, View.UserId, View.Amount, vpd.BusinessHourGuid, o.OrderId, oid);
                                View.ShowATMTransactionPanel();
                            }
                            else
                            {
                                View.ShowTransactionPanel();
                            }
                        }

                        #endregion 付款
                    }
                }
                else
                {
                    // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
                    if (int.Equals(0, View.UserId))
                    {
                        View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                    }

                    PaymentTransactionCollection ptc = op.PaymentTransactionGetListByTransIdAndTransType(View.TransactionId, PayTransType.Authorization);

                    #region check response type

                    PayTransResponseType theResponseType = PayTransResponseType.OK;
                    PaymentType errorPaymentType = PaymentType.Creditcard;
                    List<PaymentType> paymentTypes = new List<PaymentType>();

                    foreach (var pt in ptc)
                    {
                        paymentTypes.Add((PaymentType)pt.PaymentType);

                        if (pt.Result != (int)PayTransResponseType.OK)
                        {
                            theResponseType = PayTransResponseType.GenericError;
                            errorPaymentType = (PaymentType)pt.PaymentType;
                        }
                    }

                    #endregion check response type

                    if (theResponseType == PayTransResponseType.OK)
                    {
                        if (vpd.DealIsOrderable())
                        {
                            #region get credit card payment

                            IEnumerable<PaymentTransaction> ptCol = from i in ptc where (i.PaymentType == (int)PaymentType.Creditcard) select i;
                            PaymentTransaction ptCreditcard = (ptCol.Count() > 0) ? ptCol.First() : null;

                            #endregion get credit card payment

                            PaymentTransaction ptATM = null;
                            string authCode = string.Empty;
                            bool orderOK = false;
                            Order theOrder = op.OrderGet(ptc.First().OrderGuid.Value);
                            OrderDetailCollection odCol = op.OrderDetailGetList(1, 1, string.Empty, theOrder.Guid, OrderDetailTypes.Regular);
                            int atmAmount = ptATM == null ? 0 : (int)ptATM.Amount;
                            int trustCreditCardAmount = ptCreditcard == null ? 0 : (int)ptCreditcard.Amount;
                            int quantity = 1;

                            quantity = odCol.Sum(x => x.ItemQuantity);

                            #region get all payment besides credit card

                            // atm
                            ptCol = from i in ptc where (i.PaymentType == (int)PaymentType.ATM) select i;
                            ptATM = (ptCol.Count() > 0) ? ptCol.First() : null;

                            #endregion get all payment besides credit card

                            #region 檢查付款金額與份數是否相符

                            int quantityTotal = (int)odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice) + View.DeliveryCharge;
                            int paymentTotal = trustCreditCardAmount + atmAmount;

                            if (!int.Equals(quantityTotal, paymentTotal))
                            {
                                logger.ErrorFormat("付款金額與份數不符, qty={0}, price={1}, dc={2}, credit={3}, atm={4}",
                                    string.Join(",", odCol.Select(t=>t.ItemQuantity.ToString()).ToArray()),
                                    string.Join(",", odCol.Select(t => t.ItemUnitPrice.ToString()).ToArray()), 
                                    View.DeliveryCharge,
                                    trustCreditCardAmount,
                                    atmAmount);
                                View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                                return true;
                            }

                            #endregion 檢查付款金額與份數是否相符

                            #region Transaction Scope

                            using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                            {
                                #region 扣除選項

                                tsc = pp.TempSessionGetList("Accessory" + View.TicketId);
                                if (tsc.Count > 0)
                                {
                                    //多重選項
                                    foreach (TempSession ts in tsc)
                                    {
                                        if (ProposalFacade.IsVbsProposalNewVersion() && vpd.IsHouseDealNewVersion())
                                        {
                                            Guid option_guid = Guid.Empty;
                                            Guid.TryParse(ts.Name, out option_guid);
                                            PponOption opt1 = pp.PponOptionGetByGuid(option_guid);
                                            int forOut;
                                            int.TryParse(ts.ValueX, out forOut);
                                            if (opt1 != null && opt1.IsLoaded)
                                            {
                                                if (opt1.ItemGuid != null)
                                                {
                                                    ProductItem pdi = pp.ProductItemGet(opt1.ItemGuid.Value);
                                                    if (pdi != null && pdi.IsLoaded)
                                                    {
                                                        int stock = pdi.Stock - forOut;
                                                        if (stock >= 0)
                                                        {
                                                            pdi.Stock = pdi.Stock - forOut;
                                                            pdi.Sales = pdi.Sales + forOut;
                                                            pp.ProductItemSet(pdi);

                                                            PponFacade.UpdateDealMaxItemCount(vpd, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                        }
                                                        else
                                                        {                                                            
                                                            string errMsg = string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, vpd.BusinessHourGuid, vpd.ItemName);
                                                            logger.Error(errMsg);

                                                            PponFacade.UpdateDealMaxItemCount(vpd, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                            View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                                                            return true;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            int forOut = 0;
                                            AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                                            if (!agm.IsLoaded)
                                                continue;

                                            if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                                            {
                                                agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                                int? optId = PponOption.FindId(agm.Guid, vpd.BusinessHourGuid);
                                                if (optId.HasValue)
                                                {
                                                    PponOption opt = pp.PponOptionGet(optId.Value);
                                                    if (opt != null && opt.IsLoaded)
                                                    {
                                                        opt.Quantity = agm.Quantity;
                                                        pp.PponOptionSet(opt);
                                                    }
                                                }
                                            }

                                            ip.AccessoryGroupMemberSet(agm);
                                        }
                                    }
                                }
                                else
                                {
                                    if (ProposalFacade.IsVbsProposalNewVersion() && vpd.IsHouseDealNewVersion())
                                    {
                                        ProposalMultiDeal pmd = sp.ProposalMultiDealGetByBid(vpd.BusinessHourGuid);
                                        if (pmd.IsLoaded)
                                        {
                                            ProposalMultiDealsSpec spec = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options).FirstOrDefault();
                                            if (spec != null)
                                            {
                                                ProductItem pdi = pp.ProductItemGet(spec.Items.FirstOrDefault().item_guid);
                                                if (pdi != null && pdi.IsLoaded)
                                                {
                                                    int stock = pdi.Stock - (quantity * vpd.QuantityMultiplier.GetValueOrDefault(1));
                                                    if (stock >= 0)
                                                    {
                                                        pdi.Stock = pdi.Stock - (quantity * vpd.QuantityMultiplier.GetValueOrDefault(1));
                                                        pdi.Sales = pdi.Sales + (quantity * vpd.QuantityMultiplier.GetValueOrDefault(1));
                                                        pp.ProductItemSet(pdi);

                                                        PponFacade.UpdateDealMaxItemCount(vpd, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                    }
                                                    else
                                                    {                                                        
                                                        string errMsg = string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, vpd.BusinessHourGuid, vpd.ItemName);
                                                        logger.Error(errMsg);

                                                        PponFacade.UpdateDealMaxItemCount(vpd, spec.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                        View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                                                        return true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                
                                pp.TempSessionDelete("Accessory" + View.TicketId);

                                #endregion 扣除選項

                                #region 分店銷售數量

                                if (deliveryInfo.StoreGuid != Guid.Empty)
                                {
                                    pp.PponStoreUpdateOrderQuantity(vpd.BusinessHourGuid, deliveryInfo.StoreGuid, deliveryInfo.Quantity);
                                }

                                #endregion 分店銷售數量

                                #region 信用卡轉購物金

                                if (ptCreditcard != null)
                                {
                                    Guid sOrderGuid = OrderFacade.MakeSCachOrder(ptCreditcard.Amount, ptCreditcard.Amount, "17Life購物金購買(系統)",
                                        CashPointStatus.Approve, theOrder.UserId, vpd.ItemName, CashPointListType.Income, View.UserName, theOrder.Guid);

                                    bool invoiced = (DateTime.Now <(config.NewInvoiceDate));    //Scash will not be invoiced initially after the NewInvoiceDate.
                                    int depositId = OrderFacade.NewScashDeposit(sOrderGuid, ptCreditcard.Amount, View.UserId, theOrder.Guid,
                                                                "購物金購買(系統):" + vpd.SellerName, View.UserName, invoiced, OrderClassification.CashPointOrder);
                                    OrderFacade.NewScashWithdrawal(depositId, ptCreditcard.Amount, theOrder.Guid, "購物金折抵(系統):" + vpd.SellerName, View.UserName, OrderClassification.CashPointOrder);

                                    ptCreditcard.OrderGuid = sOrderGuid;
                                    ptCreditcard.Message += "|" + theOrder.Guid.ToString();
                                    op.PaymentTransactionSet(ptCreditcard);

                                    OrderFacade.MakeTransaction(View.TransactionId, PaymentType.SCash, int.Parse(ptCreditcard.Amount.ToString("F0")), (DepartmentTypes)vpd.Department, View.UserName, theOrder.Guid);
                                }

                                #endregion 信用卡轉購物金

                                #region cpa

                                if (!string.IsNullOrWhiteSpace(View.ReferrerSourceId))
                                {
                                    CpaUtility.RecordOrderByReferrer(View.ReferrerSourceId, theOrder, vpd, pp.DealPropertyGet(vpd.BusinessHourGuid));
                                }
                                else if (!string.IsNullOrEmpty(View.RsrcSession))  //add for BBH share Money , died 2011.05.19
                                {
                                    CpaUtility.RecordOrderByReferrer(View.RsrcSession, theOrder, vpd, pp.DealPropertyGet(vpd.BusinessHourGuid));
                                }

                                #endregion cpa

                                pp.TempSessionDelete(View.TicketId);

                                transScope.Complete();
                                orderOK = true;
                            }

                            #endregion Transaction Scope

                            if (!orderOK)
                            {
                                CreditCardUtility.AuthenticationReverse(View.TransactionId, ptCreditcard.AuthCode);
                                View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                                return true;
                            }
                            else
                            {
                                //if weeklypay deal
                                int trust_checkout_type = 0;
                                if (((vpd.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                                {
                                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;
                                }

                                #region make cash trust log

                                #region payments
                                Dictionary<Core.PaymentType, int> payments = new Dictionary<Core.PaymentType, int>();
                                if ((int)trustCreditCardAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.Creditcard, (int)trustCreditCardAmount);
                                }
                                if ((int)atmAmount > 0)
                                {
                                    payments.Add(Core.PaymentType.ATM, (int)atmAmount);
                                }
                                #endregion payments

                                CashTrustInfo cti = new CashTrustInfo
                                {
                                    OrderGuid = theOrder.Guid,
                                    OrderDetails = odCol,
                                    CreditCardAmount = trustCreditCardAmount,
                                    SCashAmount = 0,
                                    PCashAmount = 0,
                                    BCashAmount = 0,
                                    Quantity = quantity,
                                    DeliveryCharge = View.DeliveryCharge,
                                    DeliveryType = vpd.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)vpd.DeliveryType.Value,
                                    DiscountAmount = 0,
                                    AtmAmount = atmAmount,
                                    BusinessHourGuid = vpd.BusinessHourGuid,
                                    CheckoutType = trust_checkout_type,
                                    TrustProvider = Helper.GetBusinessHourTrustProvider(vpd.BusinessHourStatus),
                                    ItemName = vpd.ItemName,
                                    ItemPrice = (int)vpd.ItemPrice,
                                    User = mp.MemberGet(View.UserName),
                                    Payments = payments
                                };

                                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                                #endregion make cash trust log

                                PaymentResultPageType paymentResult;

                                #region 代收轉付

                                OrderFacade.EntrustSellReceiptSet(deliveryInfo, theOrder, odCol, ctCol, vpd.ItemName, View.UserName);

                                #endregion 代收轉付

                                #region 加註訂單為ATM付款

                                if (ptATM == null)
                                {
                                    OrderFacade.SetOrderStatus(theOrder.Guid, View.UserName, OrderStatus.Complete, true,
                                        new SalesInfoArg
                                        {
                                            BusinessHourGuid = vpd.BusinessHourGuid,
                                            Quantity = odCol.OrderedQuantity,
                                            Total = odCol.OrderedTotal
                                        });
                                }
                                else
                                {
                                    OrderFacade.SetOrderStatus(theOrder.Guid, View.UserName, OrderStatus.ATMOrder, true, new SalesInfoArg
                                    {
                                        BusinessHourGuid = vpd.BusinessHourGuid,
                                        Quantity = odCol.OrderedQuantity,
                                        Total = odCol.OrderedTotal
                                    });
                                }

                                #endregion 加註訂單為ATM付款

                                #region send deal-on mail  //付款成功

                                if (View.IsPaidByATM)
                                {
                                    Member m = mp.MemberGet(View.UserName);
                                    OrderFacade.SendATMOrderNotifyMail(m.DisplayName, m.UserEmail, vpd.ItemName, View.ATMAccount,
                                        atmAmount.ToString(), vpd, theOrder, quantity);
                                }
                                else
                                {
                                    OrderFacade.SendPponMail(MailContentType.Authorized, theOrder.Guid);
                                }

                                #endregion send deal-on mail  //付款成功

                                paymentResult = (paymentTypes.Contains(PaymentType.Creditcard) || paymentTypes.Contains(PaymentType.ATM)) ? PaymentResultPageType.CreditcardSuccess : PaymentResultPageType.PcashSuccess;

                                #region New Cpa

                                if (config.NewCpaEnable)
                                {
                                    try
                                    {
                                        if (View.NewCpa != null && View.NewCpa.Count > 0)
                                        {
                                            View.NewCpa = PponFacade.CpaSetDetailToDB(View.UserName, View.SessionId, Helper.GetClientIP(), View.NewCpa, theOrder.Guid);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        logger.Error(string.Format("CpaSetDetailToDB error: UserName:{0}, SessionId:{1}, Cpa:{2}, OrderGuid:{3}, Exception:{4}", View.UserName, View.SessionId, View.NewCpa, theOrder.Guid, ex));
                                    }
                                }

                                #endregion New Cpa

                                // set deal close time
                                OrderFacade.PponSetCloseTimeIfJustReachMinimum(vpd, quantity);

                                CouponFacade.GenCouponWithSmsWithGeneratePeztemp(theOrder, vpd, false);

                                View.ShowResult(paymentResult, vpd, atmAmount);
                            }
                        }
                        else
                        {
                            if (ptc.Count > 0)
                            {
                                PaymentFacade.CancelPaymentTransactionByTransId(ptc[0].TransId, "sys", "Unorderable. ");
                            }

                            View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                        }
                    }
                    else
                    {
                        OrderFacade.DeleteItemInCart(View.TicketId);
                        pp.TempSessionDelete(View.TicketId);
                        View.TransactionId = null;
                        View.ShowResult(PaymentResultPageType.CreditcardFailed, vpd, 0);
                    }
                }
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckOut += OnCheckOut;
            View.ResetContent += OnResetContent;
            View.ShowPageResult += OnShowPageResult;
            return true;
        }

        protected void OnCheckOut(object sender, CheckDataEventArgs e)
        {
            View.IsTest = config.BypassPaymentProcess;

            var theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);
            View.TheDeal = theDeal;
            string ticketId = OrderFacade.MakeRegularTicketId(View.SessionId, View.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", string.Empty);


            // 為了讓DeliveryInfo會有值
            View.EntrustSell = (EntrustSellType)theDeal.EntrustSell.GetValueOrDefault();

            int qty = e.Quantity;
            string strResult = CheckData(View.TheDeal, qty, View.EntrustSell, (bool)theDeal.IsDailyRestriction);
            if (string.IsNullOrWhiteSpace(strResult))
            {
                ItemEntry ie = new ItemEntry();
                List<AccessoryEntry> acc = new List<AccessoryEntry>();
                if (View.UserMemo != null && View.UserMemo.Length > 1)
                {
                    string kk = View.UserMemo.Split('^')[0];
                    string[] accs = kk.Substring(1).Split(',');

                    foreach (string a in accs)
                    {
                        string[] names = a.Split('|');
                        acc.Add(new AccessoryEntry(names[0], new Guid(names[1])));
                        TempSession ts = new TempSession();
                        ts.SessionId = "Accessory" + ticketId;
                        ts.Name = names[1];
                        ts.ValueX = qty.ToString();
                        pp.TempSessionSet(ts);
                    }
                }
                if (View.SelectedStoreGuid != Guid.Empty)
                {
                    var store = ViewPponStoreManager.DefaultManager.ViewPponStoreGet(View.BusinessHourGuid,
                                                                                     View.SelectedStoreGuid);

                    if (View.TheDeal.DeliveryType == (int)DeliveryType.ToShop && 0 != View.TheDeal.ItemPrice && null != store && store.IsLoaded)
                    {
                        var stores = pp.ViewPponStoreGetListByBidWithNoLock(View.BusinessHourGuid, VbsRightFlag.VerifyShop);
                        string storeName = stores.Count > 1 ? store.StoreName : string.Empty;
                        acc.Add(new AccessoryEntry(string.Format("{0}預約專線{1}", storeName, store.Phone), Guid.Empty));
                    }
                }
                PponDeliveryInfo info = View.DeliveryInfo;
                JsonSerializer serializer = new JsonSerializer();
                string infoStr = serializer.Serialize(info);

                ie = new ItemEntry(View.TheDeal.ItemGuid, View.TheDeal.ItemName, View.TheDeal.ItemPrice, qty, acc, infoStr, View.SelectedStoreGuid);

                try
                {
                    OrderFacade.PutItemToCart(ticketId, ie);

                    #region 把資料放到暫存Session中....找時間改寫成Class

                    TempSession ts = new TempSession();
                    ts.SessionId = ticketId;
                    ts.Name = "pponContent";
                    ts.ValueX = infoStr;
                    pp.TempSessionSetForUpdate(ts);

                    #endregion 把資料放到暫存Session中....找時間改寫成Class
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", ticketId,
                            View.TheDeal.BusinessHourGuid), ex);
                }

                // ATM
                if (e.PaidByAtm)
                {
                    View.GotoATMPay(ticketId);
                }
                else
                {
                    View.GotoCreditCardPay(ticketId);
                }
            }
            else
            {
                View.AlertMessage(strResult);
                OnResetContent(this, null);
            }
        }

        private string CheckData(IViewPponDeal vpd, int qty, EntrustSellType entrustSell, bool isDailyRestriction)
        {
            if (qty > (vpd.MaxItemCount ?? int.MaxValue))
                return "每單訂購數量最多" + vpd.MaxItemCount + "份，請重新輸入數量";
            int already = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, isDailyRestriction);
            if (vpd.ItemDefaultDailyAmount != null)
            {
                if (qty + already > vpd.ItemDefaultDailyAmount.Value)
                    return "每人訂購數量最多" + vpd.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量";
            }
            if (PponFacade.CheckExceedOrderedQuantityLimit(vpd, qty))
            {
                return "很抱歉!已超過最大訂購數量";
            }
            if (DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }

            return string.Empty;
        }

        protected bool IsReadyToBuy(DepartmentTypes departmentTypes)
        {
            MemberBasicCheckType checkType = MemberBasicCheckType.Ppon;
            if (departmentTypes == DepartmentTypes.PponItem)
            {
                checkType = MemberBasicCheckType.PponItem;
            }
            return MemberFacade.IsReadyToBuy(View.UserName, checkType);
        }

        protected void OnResetContent(object sender, object e)
        {
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);

            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(theDeal.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                ag.members = new List<ViewItemAccessoryGroup>();
                ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(theDeal.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    ag.members.Add(vig);
                }
            }

            int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, 
                (bool)theDeal.IsDailyRestriction);
            var stores = pp.ViewPponStoreGetListByBidWithNoLock(View.BusinessHourGuid, VbsRightFlag.VerifyShop);

            View.SetContent(theDeal, viagc, alreadyboughtcount, stores,
                (EntrustSellType)theDeal.EntrustSell.GetValueOrDefault(), (bool)theDeal.IsDailyRestriction);
        }

        private void OnShowPageResult(object sender, ShowResultEventArgs e)
        {
            try
            {
                View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(new Guid(View.TicketId.Split('_')[1]));
            }
            catch (Exception ex)
            {
                logger.Info("tickedId=" + View.TicketId);
                throw ex;
            }
            View.ShowResult(e.PageType, View.TheDeal, 0);
        }

        //寫入資料至UserTrack紀錄使用者特殊操作
        private bool AddUserTrack(string sessionId, string displayName, string url, string queryString, string context)
        {
            return cp.UserTrackSet(sessionId, displayName, url, queryString, context);
        }

        private void SetDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            string couponFreight = string.Empty;
            CouponFreightCollection cfc = pp.CouponFreightGetList(deal.BusinessHourGuid, CouponFreightType.Income);

            if (null != cfc && cfc.Count > 0)
            {
                foreach (CouponFreight cf in cfc)
                {
                    if (orderedAmount >= cf.StartAmount && cf.EndAmount > orderedAmount)
                    {
                        View.DeliveryCharge = (int)(cf.FreightAmount);
                        break;
                    }
                }
            }
            else
            {
                View.DeliveryCharge = (int)CheckDeliveryCharge(orderedAmount, deal);
            }
        }

        private decimal CheckDeliveryCharge(decimal orderedAmount, IViewPponDeal deal)
        {
            return deal.BusinessHourDeliveryCharge;
        }               
    }
}
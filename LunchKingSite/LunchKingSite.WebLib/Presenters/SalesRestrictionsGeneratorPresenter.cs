﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesRestrictionsGeneratorPresenter : Presenter<ISalesRestrictionsGeneratorView>
    {
        private IPponProvider pp;
        private ISellerProvider sp;
        private IHumanProvider hp;
        private ProvisionService ss;
        private ISysConfProvider config;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            SetProvisionData();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            View.ProvisionSelectedIndexChanged += OnProvisionSelectedIndexChanged;
            return true;
        }

        public SalesRestrictionsGeneratorPresenter(IPponProvider pponProv, ISellerProvider sellerProv, IHumanProvider humProv, ProvisionService provServ)
        {
            pp = pponProv;
            sp = sellerProv;
            hp = humProv;
            ss = provServ;
            config = ProviderFactory.Instance().GetConfig();
        }

        #region event

        protected void OnSave(object sender, DataEventArgs<string> e)
        {
            if (View.Bid != Guid.Empty)
            {
                CouponEventContent cec = pp.CouponEventContentGetByBid(View.Bid);
                if (cec.IsLoaded)
                {
                    cec.Restrictions = e.Data.Replace("\n", "<br />");
                    cec.ModifyTime = DateTime.Now;
                    pp.CouponEventContentSet(cec);

                    BusinessHourLog(View.Bid, "[系統] 變更權益說明");
                }
            }
            else
            {
                if (View.Pid != 0)
                {
                    ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(View.Pid);
                    ProposalCouponEventContent oriPcec = pcec.Clone();
                    if (pcec.IsLoaded)
                    {
                        pcec.Restrictions = e.Data.Replace("\n", "<br />");
                        pcec.ModifyTime = DateTime.Now;
                        pp.ProposalCouponEventContentSet(pcec);
                    }
                    else
                    {
                        ProposalCouponEventContent npcec = new ProposalCouponEventContent();
                        npcec.ProposalId = View.Pid;
                        npcec.Restrictions = e.Data.Replace("\n", "<br />");
                        npcec.ModifyTime = DateTime.Now;
                        pp.ProposalCouponEventContentSet(npcec);

                    }
                    ProposalFacade.CompareProposalCouponEventContent(pcec, oriPcec, View.UserName);
                }
            }
            
            View.ShowMessage(string.Empty, BusinessContentMode.Reload, "MainSet");
        }

        protected void OnProvisionSelectedIndexChanged(object sender, EventArgs e)
        {
            SetProvisionData();
        }

        #endregion

        #region Private Method

        private void LoadData()
        {
            Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, View.Bid);
            if (pro.IsLoaded)
            {
                if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                {
                    View.ShowMessage("檔次已通過設定確認，請由後台編輯。", BusinessContentMode.GoToSetup, View.Bid.ToString());
                    return;
                }

                CouponEventContent cec = pp.CouponEventContentGetByBid(View.Bid);
                BusinessHour bh = pp.BusinessHourGet(View.Bid);
                DealAccounting da = pp.DealAccountingGet(View.Bid);
                if (config.IsProvisionSwitchToSql)
                {
                    View.SetRestrictionsModel(GetProvisionDepartmentsModel(), da, bh, pro, cec.Restrictions);
                }
                else
                {
                    View.SetRestrictions(GetProvisionDepartments(), da, bh, pro, cec.Restrictions);
                }
            }
            else
            {
                if (Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created))
                {
                    View.ShowMessage("檔次已通過建檔，請由建檔平台編輯。", BusinessContentMode.Reload);
                    return;
                }

                pro = sp.ProposalGet(View.Pid);
                ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(View.Pid);
                if (config.IsProvisionSwitchToSql)
                {
                    View.SetRestrictionsModel(GetProvisionDepartmentsModel(), null, null, pro, pcec.Restrictions);
                }
                else
                {
                    View.SetRestrictions(GetProvisionDepartments(), null, null, pro, pcec.Restrictions);
                }
                
                //View.ShowMessage("無提案單資訊，無法經由此平台設定權益說明。", BusinessContentMode.BackToList);
            }
        }

        private void BusinessHourLog(Guid bid, string changeLog)
        {
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
            BusinessChangeLog log = new BusinessChangeLog();
            log.BusinessHourGuid = bid;
            log.ChangeLog = changeLog;
            log.Dept = emp.DeptName;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            sp.BusinessChangeLogSet(log);
        }

        private void SetProvisionData()
        {
            if (config.IsProvisionSwitchToSql)
            {
                View.SetProvisionDropDownListModelData(GetProvisionDepartmentsModel());
            }
            else
            {
                View.SetProvisionDropDownListData(GetProvisionDepartments());
            }            
        }

        private IEnumerable<ProvisionDepartment> GetProvisionDepartments()
        {
            return ss.GetProvisionDepartments();
        }
        private IEnumerable<ProvisionDepartmentModel> GetProvisionDepartmentsModel()
        {
            return PponFacade.GetAllProvisionDepartmentModel();
        }
        #endregion
    }
}

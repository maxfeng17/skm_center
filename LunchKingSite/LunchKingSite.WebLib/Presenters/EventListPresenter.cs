﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventListPresenter : Presenter<IEventListView>
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetEventList(LoadData(0, View.PageSize));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += OnPageChange;
            return true;
        }

        public void OnPageChange(object sender, DataEventArgs<int> e)
        {
            View.SetEventList(LoadData(e.Data, View.PageSize));
        }

        public EventContentCollection LoadData(int page,int size)
        {
            return ep.EventContentGetList(page,size);
        }
    }
}

﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealSubscribePresenter : Presenter<IHiDealSubscribeView>
    {
        IHiDealProvider hp;

        public HiDealSubscribePresenter()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.AddSubscript += OnSetSubscript;
            return true;
        }

        protected void OnSetSubscript(object sender, EventArgs e)
        {
            HiDealSubscription subscription = HiDealMailFacade.HiDealSubscribeCheckSet(View.UserName, View.RegionId, 0, true);
            CpaUtility.RecordSubscriptionByReferrer(View.Cpa, subscription);
            View.ShowAlert();
        }
    }
}

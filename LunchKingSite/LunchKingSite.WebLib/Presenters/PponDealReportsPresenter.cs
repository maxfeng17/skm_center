﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponDealReportsPresenter : Presenter<IPponDealReportsView>
    {
        #region members
        protected IOrderProvider op;
        #endregion

        public PponDealReportsPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchByDepartment += OnSearchByDepartment;
            return true;
        }

        protected void OnSearchByDepartment(object sender, DataEventArgs<DateTime[]> e)
        {
            DataTable dt = op.ViewOrderMemberBuildingSellerGetListByDepartment(e.Data);
            View.SetGvDepartment(dt);
        }
    }
}

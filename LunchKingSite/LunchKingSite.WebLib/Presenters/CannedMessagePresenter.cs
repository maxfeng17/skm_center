﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Diagnostics;

namespace LunchKingSite.WebLib.Presenters
{
    public class CannedMessagePresenter : Presenter<ICannedMessageView>
    {
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected LunchKingSite.Core.IAccountingProvider ap = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IAccountingProvider>();

        public override bool OnViewInitialized()
        {
            
            base.OnViewInitialized();
            View.CannedMessageList(GetSampleList(1));
            View.VendorFineCategoryGetList(GetVendorFineCategory());
            return true;

        }

        public override bool OnViewLoaded()
        {

            base.OnViewLoaded();
            View.Search += OnSearch;
            View.AddService +=OnAddService;
            View.AddFine += OnAddFine;
            View.EditCannedMessage += OnEditCannedMessage;
            View.DeleteCannedMessage += OnDeleteCannedMessage;
            View.GetCannedMessageCount += OnGetCannedMessageCount;
            View.CannedMessagePageChanged += OnCannedMessagePageChanged;

            return true;
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.CannedMessageList(GetSampleList(1));
        }
        protected void OnAddService(object sender, EventArgs e)
        {
           
            if (View.Mode == "Edit")
            {
                CustomerServiceCategorySample entity = sp.CustomerServiceCategorySampleGet(View.SampleId);
                if (entity.IsLoaded && View.SampleId!=0)
                {
                    entity.CategoryId = View.InCategoryId;
                    entity.Subject = View.Subject;
                    entity.Content = View.Content;
                    entity.ModifyId = View.UserId;
                    entity.ModifyTime = DateTime.Now;
                }
                else
                {
                    entity.CategoryId = View.InCategoryId;
                    entity.Subject = View.Subject;
                    entity.Content = View.Content;
                    entity.CreateId = View.UserId;
                    entity.CreateTime = DateTime.Now;
                }
                sp.CustomerServiceCategorySampleSet(entity);
            }
            else
            {
                CustomerServiceCategorySample entity = new CustomerServiceCategorySample();

                entity.CategoryId = View.InCategoryId;
                entity.Subject = View.Subject;
                entity.Content = View.Content;
                entity.CreateId = View.UserId;
                entity.CreateTime = DateTime.Now;

                sp.CustomerServiceCategorySampleSet(entity);
            }

            
            View.CannedMessageList(GetSampleList(1));
        }
        protected void OnGetCannedMessageCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.GetSampleListCount(GetFilter());
        }

        protected void OnCannedMessagePageChanged(object sender, DataEventArgs<int> e)
        {
            View.CannedMessageList(GetSampleList(e.Data));
        }

        protected void OnEditCannedMessage(object sender,DataEventArgs<int> e)
        {
            LoadCustomerServiceCategorySample(e.Data);
        }

        protected void OnDeleteCannedMessage(object sender, DataEventArgs<int> e)
        {
            sp.CustomerServiceCategorySampleDeleteByCategoryId(e.Data);
            View.CannedMessageList(GetSampleList(1));
        }

        protected void OnAddFine(object sender, EventArgs e)
        {

            if (View.FineMode == "Edit")
            {
                VendorFineCategory fineCategory = ap.VendorFineCategoryGetList(View.FineId);
                if (fineCategory.IsLoaded && View.FineId != 0)
                {
                    fineCategory.Content = View.FineContent;
                    fineCategory.ModifyId = View.UserName;
                    fineCategory.ModifyTime = DateTime.Now;
                }
                ap.VendorFineCategorySet(fineCategory);
            }
            else
            {
                VendorFineCategory fineCategory = new VendorFineCategory();

                fineCategory.Content = View.FineContent;
                fineCategory.Type = View.FineType;
                fineCategory.CreateId = View.UserName;
                fineCategory.CreateTime = DateTime.Now;

                ap.VendorFineCategorySet(fineCategory);
            }


            View.VendorFineCategoryGetList(GetVendorFineCategory());
        }
        #endregion

        private CustomerServiceCategorySampleCollection GetSampleList(int pageNumber)
        {
            return sp.GetSampleListByPage(pageNumber, View.PageSize, CustomerServiceCategorySample.Columns.Id, GetFilter());
        }


        public VendorFineCategoryCollection GetVendorFineCategory()
        {
            return ap.VendorFineCategoryGetList();
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            if (View.CategoryId != 0)
            {
                filter.Add(CustomerServiceCategorySample.Columns.CategoryId + " = " + View.CategoryId);
            }
            return filter.ToArray();
        }

        private void LoadCustomerServiceCategorySample(int id)
        {
            View.CustomerServiceCategorySampleEdit(sp.CustomerServiceCategorySampleGet(id));
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;
using LunchKingSite.BizLogic.Model;
using System.Net.Mail;
using System.Web;
using LunchKingSite.Core.Models;
using log4net;
using LunchKingSite.BizLogic.Model.SellerSales;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesProposalContentPresenter : Presenter<ISalesProposalContentView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISystemProvider sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private IPponProvider pp;
        private IOrderProvider op;
        private ProvisionService ss;
        private static ILog logger = LogManager.GetLogger(typeof(SalesProposalContentPresenter));

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.ProposalId);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            View.OrderTimeSave += OnOrderTimeSave;
            View.ChangeSeller += OnChangeSeller;
            View.Urgent += OnUrgent;
            View.Delete += OnDelete;
            View.CreateBusinessHour += OnCreateBusinessHour;
            View.Referral += OnReferral;
            View.ProposalSend += OnProposalSend;
            View.ProposalReturned += OnProposalReturned;
            View.Assign += OnAssign;
            View.ChangeLog += OnChangeLog;
            View.CheckFlag += OnCheckFlag;
            View.ReCreateProposal += OnReCreateProposal;
            View.PponStoreSave += OnPponStoreSave;
            View.VbsSave += OnVbsSave;
            View.SellerProposalReturned += OnSellerProposalReturned;
            View.CloneProposal += OnCloneProposal;
            View.SellerProposalSaleEditor += OnSellerProposalSaleEditor;
            View.SellerProposalCheckWaitting += OnSellerProposalCheckWaitting;
            View.Download += OnDownload;
            View.UpdateSellerGuid += OnUpdateSellerGuid;
            return true;
        }

        public SalesProposalContentPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IMemberProvider memProv, IPponProvider pponProv, IOrderProvider odrProv, ProvisionService provServ)
        {
            sp = sellerProv;
            hp = humProv;
            mp = memProv;
            pp = pponProv;
            ss = provServ;
            op = odrProv;
        }

        #region event


        protected void OnSave(object sender, EventArgs e)
        {
            if (View.ProposalId != 0)
            {
                Proposal pro = sp.ProposalGet(View.ProposalId);
                Proposal ori = pro.Clone();
                pro = View.GetProposalData(pro);

                if (pro.CopyType == (int)ProposalCopyType.Similar)
                {
                    pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                }

                if (Helper.IsFlagSet(ori.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers) ||
                    Helper.IsFlagSet(ori.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto) ||
                    Helper.IsFlagSet(ori.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto) ||
                    ori.SystemPic)
                {
                    if (!Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers) &&
                    !Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto) &&
                    !Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto) &&
                    !pro.SystemPic)
                    {
                        View.ShowMessage("請選擇照片來源。", ProposalContentMode.DataError);
                        return;
                    }
                }

                #region 展演檔次檢核
                bool isPromotionDeal = View.IsPromotionDeal;
                bool isExhibitionDeal = View.IsExhibitionDeal;
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    if (isPromotionDeal || isExhibitionDeal)
                    {
                        View.ShowMessage("宅配檔次不可選擇【展演類型檔次】及【展覽類型檔次】。", ProposalContentMode.DataError);
                        return;
                    }
                }
                else
                {
                    if (isPromotionDeal && isExhibitionDeal)
                    {
                        View.ShowMessage("【展演類型檔次】及【展覽類型檔次】只能擇一勾選。", ProposalContentMode.DataError);
                        return;
                    }
                }
                if (pro.DealType == (int)ProposalDealType.Product)
                {
                    if (isPromotionDeal || isExhibitionDeal)
                    {
                        View.ShowMessage("提案類型為宅配，不可選擇【展演類型檔次】及【展覽類型檔次】。", ProposalContentMode.DataError);
                        return;
                    }
                }
                #endregion 展演檔次檢核

                #region 接續檔次
                if (!string.IsNullOrEmpty(View.AncestorBid))
                {
                    Guid _AncestorBusinessHourGuid = Guid.Empty;
                    Guid.TryParse(View.AncestorBid.Trim(), out _AncestorBusinessHourGuid);
                    if (_AncestorBusinessHourGuid == Guid.Empty)
                    {
                        View.ShowMessage("【續接數量檔次】輸入異常，請檢查 Bid 是否正確!。", ProposalContentMode.DataError);
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(View.AncestorSeqBid))
                {
                    Guid _AncestorSequenceBusinessHourGuid = Guid.Empty;
                    Guid.TryParse(View.AncestorSeqBid.Trim(), out _AncestorSequenceBusinessHourGuid);
                    if (_AncestorSequenceBusinessHourGuid == Guid.Empty)
                    {
                        View.ShowMessage("【續接憑證檔次】輸入異常，請檢查 Bid 是否正確!。", ProposalContentMode.DataError);
                        return;
                    }
                }
                #endregion

                #region 權益說明
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    /*
                 * 宅配
                 * 如果是一般出貨/快速出貨，要先塞值到業助畫面
                 * */

                    try
                    {
                        IEnumerable<ProvisionDepartment> departments = ss.GetProvisionDepartments();
                        string restricts = "";
                        if (pro.ShipType == (int)DealShipType.Normal)
                        {
                            //一般出貨
                            if (pro.ShippingdateType == (int)DealShippingDateType.Special)
                            {
                                //最早出貨日
                                restricts = departments.Where(x => x.Name == "P商品").FirstOrDefault()
                                                .Categorys.Where(x => x.Name == "配送事項").FirstOrDefault()
                                                .Items.Where(x => x.Name == "一般出貨").FirstOrDefault()
                                                .Contents.Where(x => x.Name == "依日期").FirstOrDefault()
                                                .ProvisionDescs.FirstOrDefault()
                                                .Description;
                            }
                            else if (pro.ShippingdateType == (int)DealShippingDateType.Normal)
                            {
                                //訂單成立後
                                restricts = departments.Where(x => x.Name == "P商品").FirstOrDefault()
                                                .Categorys.Where(x => x.Name == "配送事項").FirstOrDefault()
                                                .Items.Where(x => x.Name == "一般出貨").FirstOrDefault()
                                                .Contents.Where(x => x.Name == "依訂單").FirstOrDefault()
                                                .ProvisionDescs.FirstOrDefault()
                                                .Description;
                            }
                        }
                        else if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //快速出貨
                            restricts = departments.Where(x => x.Name == "P商品").FirstOrDefault()
                                                .Categorys.Where(x => x.Name == "配送事項").FirstOrDefault()
                                                .Items.Where(x => x.Name == "快速出貨").FirstOrDefault()
                                                .Contents.Where(x => x.Name == "24小時出貨").FirstOrDefault()
                                                .ProvisionDescs.FirstOrDefault()
                                                .Description;
                        }
                        else if (pro.ShipType == (int)DealShipType.Other)
                        {
                            //其他
                            restricts = pro.ShipOther;
                        }
                        //鑑賞期
                        restricts += "<br />" + departments.Where(x => x.Name == "P商品").FirstOrDefault()
                                            .Categorys.Where(x => x.Name == "鑑賞期").FirstOrDefault()
                                            .Items.Where(x => x.Name == "鑑賞期").FirstOrDefault()
                                            .Contents.Where(x => x.Name == (pro.TrialPeriod ?? false ? "不適用" : "適用")).FirstOrDefault()
                                            .ProvisionDescs.FirstOrDefault()
                                            .Description.Replace("更多詳細說明", "<a href='https://www.17life.com/ppon/newbieguide.aspx?s=3&q=20469#anc20469'><font color='blue'>更多詳細說明</font></a>");

                        if (!string.IsNullOrEmpty(restricts))
                        {
                            ProposalCouponEventContent pcec = ProposalFacade.RestrictionsGet(pro.Id);
                            if (pcec.IsLoaded)
                            {
                                //不自動帶
                                //pcec.Restrictions = pcec.Restrictions + "<br />" + restricts.Trim("\n".ToCharArray());
                                //pcec.ModifyTime = DateTime.Now;
                                //pp.ProposalCouponEventContentSet(pcec);
                            }
                            else
                            {
                                ProposalCouponEventContent npcec = new ProposalCouponEventContent();
                                npcec.ProposalId = pro.Id;
                                npcec.Restrictions = restricts.Trim("\n".ToCharArray());
                                npcec.ModifyTime = DateTime.Now;
                                pp.ProposalCouponEventContentSet(npcec);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("連線錯誤，使用者:" + View.UserName, ex);
                    }


                }

                #endregion

                #region 代銷通路
                List<int> agentChannels = View.AgentChannels;
                pro.AgentChannels = string.Join(",", agentChannels);
                #endregion 代銷通路

                pro.IsPromotionDeal = isPromotionDeal;  //展演類型檔次
                pro.IsExhibitionDeal = isExhibitionDeal;    //展覽類型檔次
                pro.IsGame = View.IsGame;    //遊戲檔次
                pro.IsChannelGift = View.IsChannelGift;    //代銷贈品檔次

                pro.ModifyId = View.UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                if (pro.ReferrerBusinessHourGuid != null || Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    //提案單異動紀錄
                    ProposalFacade.CompareProposal(pro, ori, View.UserName);
                }
                else
                {
                    ProposalFacade.CompareProposal(pro, ori, View.UserName);
                    //宅配記錄商家log
                    if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        CompareSellerPorposal(pro, ori);
                    }
                }

                //上檔頻道
                ProposalFacade.ProposalCategoryDealSave(pro, View.GetCategoryDeals(), View.UserName);




                //檢查匯款對象商家之財務資料(宅配因自動勾匯款,因此多在提案單檢核)
                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                    ProposalFacade.CheckProposalFinanceCheck(1, View.UserName, View.ProposalId, Guid.Empty);

                View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, Convert.ToString(View.ProposalId));
            }
            else if (View.BusinessHourGuid != Guid.Empty)
            {
                Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, View.BusinessHourGuid);
                if (pro.IsLoaded)
                {
                    LoadData(pro.Id);
                }
                else
                {
                    View.ShowMessage("查無此提案單。", ProposalContentMode.BackToList);
                }
            }
            else if (View.SellerGuid != Guid.Empty)
            {
                ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
                if (emp.IsLoaded)
                {
                    Proposal pro = new Proposal();
                    pro.SellerGuid = View.SellerGuid;
                    pro.DevelopeSalesId = emp.UserId;
                    pro.CreateId = View.UserName;
                    pro.CreateTime = DateTime.Now;
                    sp.ProposalSet(pro);

                    ProposalFacade.ProposalLog(pro.Id, "[系統] 建立提案單", View.UserName);
                    LoadData(View.ProposalId);
                }
            }
            else
            {
                View.RedirectSellerList();
            }
        }

        protected void OnPponStoreSave(object sender, EventArgs e)
        {
            //現況資料
            ProposalStoreCollection proposalStore = View.GetPponStores();
            ProposalStoreCollection tmpproposalStore = proposalStore.Clone();
            //原始資料
            ProposalStoreCollection oriStore = pp.ProposalStoreGetListByProposalId(View.ProposalId);
            ProposalStoreCollection tmporiStore = oriStore.Clone();
            //儲存資料
            ProposalStoreCollection savePponStore = new ProposalStoreCollection();
            //刪除被勾掉的分店
            ProposalStoreCollection delePponStore = new ProposalStoreCollection();


            foreach (ProposalStore store in oriStore)
            {
                ProposalStore s = proposalStore.Where(x => x.StoreGuid == store.StoreGuid).FirstOrDefault();
                if (s != null)
                {
                    //location還在
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag(true, store.VbsRight, VbsRightFlag.Location));
                }
                else
                {
                    //location取消
                    if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.VerifyShop) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Verify) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Accouting) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.ViewBalanceSheet))
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Location));
                    else
                        delePponStore.Add(store);//刪掉
                }
                savePponStore.Add(store);
            }
            pp.ProposalStoreDeleteList(delePponStore);

            // 新增被勾選的分店
            foreach (ProposalStore store in proposalStore)
            {
                if (!oriStore.Any(x => x.StoreGuid == store.StoreGuid))
                {
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag(true, store.VbsRight, VbsRightFlag.Location));
                    savePponStore.Add(store);
                }
            }
            pp.ProposalStoreSetList(savePponStore);

            CompareLocationSeller(tmpproposalStore, tmporiStore);

            View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, View.ProposalId.ToString());
        }

        protected void OnVbsSave(object sender, DataEventArgs<int> e)
        {
            //現況資料
            ProposalStoreCollection proposalStore = View.GetVbs();
            ProposalStoreCollection tmpproposalStore = proposalStore.Clone();
            //原始資料
            ProposalStoreCollection oriStore = pp.ProposalStoreGetListByProposalId(View.ProposalId);
            ProposalStoreCollection tmporistore = oriStore.Clone();
            //儲存資料
            ProposalStoreCollection savePponStore = new ProposalStoreCollection();
            //刪除被勾掉的分店
            ProposalStoreCollection delePponStore = new ProposalStoreCollection();

            Proposal pro = sp.ProposalGet(View.ProposalId);
            Proposal oriPro = pro.Clone();
            List<Guid> verifyshopGuids = new List<Guid>();
            List<Guid> verifyGuids = new List<Guid>();
            List<Guid> accountingGuids = new List<Guid>();
            List<Guid> balanceSheetGuids = new List<Guid>();
            List<Guid> BalanceSheetHideFromDealSellerGuids = new List<Guid>();
            List<Guid> HideFromVerifyShopGuids = new List<Guid>();

            foreach (ProposalStore store in oriStore)
            {
                ProposalStore s = proposalStore.Where(x => x.StoreGuid == store.StoreGuid).FirstOrDefault();
                if (s != null)
                {
                    store.TotalQuantity = s.TotalQuantity;
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.VerifyShop) > 0 ? true : false, store.VbsRight, VbsRightFlag.VerifyShop));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.Verify) > 0 ? true : false, store.VbsRight, VbsRightFlag.Verify));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.Accouting) > 0 ? true : false, store.VbsRight, VbsRightFlag.Accouting));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0 ? true : false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0 ? true : false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));

                    if ((s.VbsRight & (int)VbsRightFlag.VerifyShop) > 0)
                        verifyshopGuids.Add(s.StoreGuid);
                    if ((s.VbsRight & (int)VbsRightFlag.Verify) > 0)
                        verifyGuids.Add(s.StoreGuid);
                    if ((s.VbsRight & (int)VbsRightFlag.Accouting) > 0)
                        accountingGuids.Add(s.StoreGuid);
                    if ((s.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0)
                        balanceSheetGuids.Add(s.StoreGuid);
                    if ((s.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0)
                        BalanceSheetHideFromDealSellerGuids.Add(s.StoreGuid);
                }
                else
                {
                    if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Location))
                    {
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.VerifyShop));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Verify));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Accouting));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));
                    }
                    else
                        delePponStore.Add(store);//刪掉
                }
                savePponStore.Add(store);
            }

            // 新增被勾選的分店
            foreach (ProposalStore store in proposalStore)
            {
                if (!oriStore.Any(x => x.StoreGuid == store.StoreGuid))
                {
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.VerifyShop) > 0 ? true : false, store.VbsRight, VbsRightFlag.VerifyShop));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.Verify) > 0 ? true : false, store.VbsRight, VbsRightFlag.Verify));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.Accouting) > 0 ? true : false, store.VbsRight, VbsRightFlag.Accouting));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0 ? true : false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                    store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0 ? true : false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));

                    if ((store.VbsRight & (int)VbsRightFlag.VerifyShop) > 0)
                        verifyshopGuids.Add(store.StoreGuid);
                    if ((store.VbsRight & (int)VbsRightFlag.Verify) > 0)
                        verifyGuids.Add(store.StoreGuid);
                    if ((store.VbsRight & (int)VbsRightFlag.Accouting) > 0)
                        accountingGuids.Add(store.StoreGuid);
                    if ((store.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0)
                        balanceSheetGuids.Add(store.StoreGuid);
                    if ((store.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0)
                        BalanceSheetHideFromDealSellerGuids.Add(store.StoreGuid);

                    savePponStore.Add(store);
                }
            }

            var vbsRightInfo = new Dictionary<VbsRightFlag, IEnumerable<Guid>>();
            vbsRightInfo.Add(VbsRightFlag.VerifyShop, verifyshopGuids);
            vbsRightInfo.Add(VbsRightFlag.Verify, verifyGuids);
            vbsRightInfo.Add(VbsRightFlag.Accouting, accountingGuids);
            vbsRightInfo.Add(VbsRightFlag.ViewBalanceSheet, balanceSheetGuids);
            vbsRightInfo.Add(VbsRightFlag.BalanceSheetHideFromDealSeller, BalanceSheetHideFromDealSellerGuids);
            vbsRightInfo.Add(VbsRightFlag.HideFromVerifyShop, HideFromVerifyShopGuids);

            string checkErrorMsg = "";
            bool vbs = SellerFacade.ValiadateSellerTreeSetUp(pro.SellerGuid, vbsRightInfo, out checkErrorMsg);
            if (!vbs && e.Data == 0)
                View.ShowMessage(checkErrorMsg, ProposalContentMode.DataError);
            else
            {
                pp.ProposalStoreDeleteList(delePponStore);
                pp.ProposalStoreSetList(savePponStore);
                CompareVbsSeller(tmpproposalStore, tmporistore);
                //設定匯款方式
                pro.PayType = (int)SellerFacade.GetPayToCompany(pro.SellerGuid, oriStore);
                sp.ProposalSet(pro);

                //提案單異動紀錄
                ProposalFacade.CompareProposal(pro, oriPro, View.UserName);

                //檢查匯款對象商家之財務資料
                ProposalFacade.CheckProposalFinanceCheck(1, View.UserName, View.ProposalId, Guid.Empty);

                View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, View.ProposalId.ToString());
            }

        }

        protected void OnUpdateSellerGuid(object sender, EventArgs e)
        {
            Proposal pro = sp.ProposalGet(View.ProposalId);
            Seller s = sp.SellerGet(View.CheckSellerGuid);

            bool isFirstDeal = false;
            ViewPponDealCollection vpdc = pp.ViewPponDealGetBySellerGuid(View.CheckSellerGuid);
            if (vpdc.Count == 0)
            {
                isFirstDeal = true;
            }

            pro.SellerGuid = View.CheckSellerGuid;
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(isFirstDeal, pro.SpecialFlag, ProposalSpecialFlag.FirstDeal));
            if (s.VendorReceiptType != null)
            {
                pro.VendorReceiptType = s.VendorReceiptType;
                pro.Othermessage = s.Othermessage;
            }


            sp.ProposalSet(pro);

            #region  ProposalStore
            //宅配該賣家預設打勾
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
            if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
            {
                ProposalStoreCollection psc = new ProposalStoreCollection();
                psc.Add(new ProposalStore()
                {
                    ProposalId = pro.Id,
                    StoreGuid = pro.SellerGuid,
                    TotalQuantity = null,
                    VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                    ResourceGuid = Guid.NewGuid(),
                    CreateId = emp.Email,
                    CreateTime = DateTime.Now
                });

                pp.ProposalStoreSetList(psc);
            }
            #endregion

            ViewProposalSeller vps = sp.ViewProposalSellerGet(Proposal.Columns.Id, View.ProposalId);
            ProposalFacade.ProposalLog(View.ProposalId, "商家資訊確認:" + vps.SellerId, View.UserName);
        }

        protected void OnDownload(object sender, DataEventArgs<Guid> e)
        {
            SalesProposalModel model;
            SellerCollection stores;
            ProposalStoreCollection psc;//所有的proposalstore
            Proposal pro = sp.ProposalGet(View.ProposalId);
            ProposalFacade.GetPponDealData(pro, out model, out stores, out psc);

            Seller st = null;
            List<Seller> storeList = new List<Seller>();//proposalstore轉成seller
            List<Seller> LocationstoreList = new List<Seller>();//營業據點
            List<Seller> AccountingstoreList = new List<Seller>();//匯款對象

            if (pro.DeliveryType == (int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && (pro.DealType == (int)ProposalDealType.Piinlife || pro.DealType == (int)ProposalDealType.Travel)))
            {
                if (psc.Count > 0)
                {
                    Guid[] stid = psc.Select(x => x.StoreGuid).ToArray();
                    storeList = stores.Where(x => x.Guid.EqualsAny(stid) && !string.IsNullOrEmpty(x.SellerName)).OrderBy(x => x.CreateTime).ToList();


                    Guid[] Locationstid = psc.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Location)).Select(x => x.StoreGuid).ToArray();
                    LocationstoreList = stores.Where(x => x.Guid.EqualsAny(Locationstid) && !string.IsNullOrEmpty(x.SellerName)).OrderBy(x => x.CreateTime).ToList()
                                              .Join(psc, xs => xs.Guid, xpsc => xpsc.StoreGuid, (xs, xpsc) => new { xs, xpsc })
                                              .OrderBy(x => x.xpsc.SortOrder).ThenBy(x => x.xs.CreateTime).Select(x => x.xs).ToList();

                    Guid[] Accoutingstid = psc.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).ToArray();
                    AccountingstoreList = stores.Where(x => x.Guid.EqualsAny(Accoutingstid) && !string.IsNullOrEmpty(x.SellerName)).OrderBy(x => x.CreateTime).ToList();

                    if (e.Data != Guid.Empty)
                    {
                        //有選到分店
                        st = storeList.Where(x => x.Guid == e.Data).FirstOrDefault();
                    }

                    if (e.Data == Guid.Empty)
                    {
                        //當作是整份的
                        st = storeList.FirstOrDefault();
                    }

                    if (st == null)
                    {
                        View.ShowMessage("查無分店資料。", ProposalContentMode.DataError);
                        return;
                    }
                }
                else
                {
                    if (pro.SellerGuid != Guid.Empty)
                    {
                        View.ShowMessage("檔次尚未勾選配合活動分店。", ProposalContentMode.DataError);
                        return;
                    }
                }
            }



            if (string.IsNullOrEmpty(model.ContentProperty.Restrictions))
            {
                View.ShowMessage("執行列印合約功能前，請先於下方【權益說明】區塊進行編輯，謝謝。", ProposalContentMode.DataError);
                return;
            }
            string fileName = "";
            if (pro.SellerGuid == Guid.Empty)
                fileName = pro.BrandName;
            else
                fileName = model.SellerProperty.SellerName + "_" + pro.BrandName;
            View.DownloadWord(pro, (DeliveryType)pro.DeliveryType, ProposalFacade.GetPponDealDictionary(model, pro, View.IsListCheck), fileName, model.SellerProperty.Contacts, LocationstoreList, AccountingstoreList);
        }


        protected void OnSellerProposalSaleEditor(object sender, DataEventArgs<int> e)
        {
            int pid = View.ProposalId;
            if (pid != 0)
            {
                Proposal pro = sp.ProposalGet(pid);
                if (pro != null && pro.IsLoaded)
                {
                    if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        View.ShowMessage("已送件QC，無法再編輯。", ProposalContentMode.DataError);
                        return;
                    }
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));   //需要是草稿，商家才能查到
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));  //等待商家覆核           
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));   //商家編輯中
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));          //提案覆核
                    sp.ProposalSet(pro);

                    string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleEditor);
                    ProposalFacade.ProposalLog(pid, "[" + title + "] ", View.UserName, ProposalLogType.Seller);
                }

                View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, pid.ToString());
            }
            else
            {
                View.ShowMessage("提案單發生錯誤。", ProposalContentMode.DataError);
            }
        }
        protected void OnSellerProposalCheckWaitting(object sender, DataEventArgs<int> e)
        {
            int pid = View.ProposalId;
            if (pid != 0)
            {
                Proposal pro = sp.ProposalGet(pid);
                ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
                if (pro != null && pro.IsLoaded)
                {
                    if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        View.ShowMessage("已送件QC，無法再編輯。", ProposalContentMode.DataError);
                        return;
                    }
                    ResourceAcl acl = mp.ResourceAclGetListByResourceGuid(pro.SellerGuid);
                    if (acl == null || !acl.IsLoaded)
                    {
                        View.ShowMessage("尚未建立商家帳號", ProposalContentMode.DataError);
                        return;
                    }

                    //if (config.IsConsignment)
                    //{
                    //    if (pro.Consignment)
                    //    {
                    //        //這邊要再抓新的合約類型
                    //        SellerContractFileCollection pcf = ProposalFacade.SellerContractFileGetByType(pro.SellerGuid, (int)SellerContractType.Curation);
                    //        if (pcf.Count() == 0)
                    //        {
                    //            View.ShowMessage("商家尚未上傳【策展專案商品合約書】，請先確認合約是否已上傳後，再進行送商家覆核或送件QC。", ProposalContentMode.DataError);
                    //            return;
                    //        }

                    //    }
                    //}

                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));   //需要是草稿，商家才能查到
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleSend));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));

                    sp.ProposalSet(pro);

                    string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.CheckWaitting);
                    ProposalFacade.ProposalLog(pid, "[" + title + "] ", View.UserName, ProposalLogType.Seller);


                    #region mail(寄給 頁確聯絡人)
                    List<string> MailUsers = new List<string>();

                    //頁確聯絡人(賣家一般聯絡人)
                    Seller seller = sp.SellerGet(pro.SellerGuid);
                    List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                    if (contacts != null)
                    {
                        //一般聯絡人
                        foreach (Seller.MultiContracts c in contacts)
                        {
                            if (c.Type == ((int)ProposalContact.Normal).ToString())
                            {
                                MailUsers.Add(c.ContactPersonEmail);
                            }
                        }
                    }

                    ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                    string SaleName = "";
                    string SaleMail = "";
                    if (emp.IsLoaded && emp != null)
                    {
                        SaleName = emp.EmpName;
                        SaleMail = emp.Email;

                        string Memo = @"• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔";
                        ProposalFacade.SendEmail(MailUsers, "【請求覆核通知】 NO." + pro.Id + "  " + (!string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName),
                           ProposalFacade.SendSellerProposalMailContent(pro, SaleName, config.SiteUrl + "/vbs/ProposalContent?pid=",
                           Memo));
                    }

                    #endregion
                }

                View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, pid.ToString());
            }
            else
            {
                View.ShowMessage("提案單「請求商家覆核」發生錯誤。", ProposalContentMode.DataError);
            }
        }

        protected void OnSellerProposalReturned(object sender, DataEventArgs<string> e)
        {
            Proposal pro = sp.ProposalGet(View.ProposalId);
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(View.ProposalId);
            if (pro != null && pro.IsLoaded)
            {
                //變回草稿
                pro.SellerProposalFlag = (int)SellerProposalFlag.Apply;
                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Returned);

                ProposalFacade.ProposalLog(View.ProposalId, "[" + title + "] " + e.Data, View.UserName, ProposalLogType.Seller);
                #region mail(寄給 頁確聯絡人)
                List<string> MailUsers = new List<string>();

                //頁確聯絡人(賣家一般聯絡人)
                Seller seller = sp.SellerGet(pro.SellerGuid);
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                if (contacts != null)
                {
                    //一般聯絡人
                    foreach (Seller.MultiContracts c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString())
                        {
                            MailUsers.Add(c.ContactPersonEmail);
                        }
                    }
                }


                ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                string SaleName = "";
                string SaleMail = "";
                if (emp.IsLoaded && emp != null)
                {
                    SaleName = emp.EmpName;
                    SaleMail = emp.Email;

                    string Memo = string.Format(@"退件原因：{0}<p />
                                    • 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔", e.Data);
                    ProposalFacade.SendEmail(MailUsers, "【提案覆核通知】 NO." + pro.Id + "  " + (!string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName),
                       ProposalFacade.SendSellerProposalMailContent(pro, SaleName, config.SiteUrl + "/vbs/ProposalContent?pid=",
                       Memo));
                }

                #endregion

                View.ShowMessage("已退件!", ProposalContentMode.BackToList, pro.Id.ToString());
            }
        }

        /// <summary>
        /// 複製提案單(尚未有檔次)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnCloneProposal(object sender, DataEventArgs<KeyValuePair<int, ProposalCopyType>> e)
        {
            int pid = 0;
            Proposal ori = sp.ProposalGet(e.Data.Key);
            //旅遊宅配走原有的複製檔,其他宅配走新的複製方式
            if (ProposalFacade.IsVbsProposalNewVersion() && ori.DeliveryType == (int)DeliveryType.ToHouse 
                && ori.DealType != (int)ProposalDealType.Travel
                && ori.DealType != (int)ProposalDealType.Piinlife)
            {
                pid = PponFacade.CreateProposalByCloneSellerPro(ori.Id, e.Data.Value, View.UserName, false, false);
            }
            else
            {
                pid = PponFacade.CreateProposalByCloneNewDeal(e.Data.Key, e.Data.Value, View.UserName);
            }

            if (pid != 0)
            {
                Proposal pro = sp.ProposalGet(pid);

                if (ProposalFacade.IsVbsProposalNewVersion() && pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
                {
                    View.ShowMessage(string.Empty, ProposalContentMode.GotoHouseProposal, pid.ToString());
                }
                else
                {
                    if (pro.IsEarlierPageCheck)
                    {
                        View.ShowMessage("原提案單有申請頁確，複製提案單後請重新確認是否仍要執行頁確。", ProposalContentMode.AlertGoToProposal, pid.ToString());
                    }
                    else
                    {
                        View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, pid.ToString());
                    }
                }
            }
            else
            {
                View.ShowMessage("提案產生錯誤。", ProposalContentMode.DataError);
            }
        }

        /// <summary>
        /// 複製提案單(有檔次)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void OnReCreateProposal(object sender, DataEventArgs<KeyValuePair<Guid, ProposalCopyType>> e)
        {
            int pid = 0;
            List<Guid> bids = new List<Guid>();
            bids.Add(e.Data.Key);
            Proposal ori = sp.ProposalGetByBids(bids).FirstOrDefault();
            //旅遊宅配走原有的複製檔,其他宅配走新的複製方式
            if (ProposalFacade.IsVbsProposalNewVersion() && ori.DeliveryType == (int)DeliveryType.ToHouse && ori.DealType != (int)ProposalDealType.Travel && ori.DealType != (int)ProposalDealType.Piinlife)
            {
                pid = PponFacade.CreateProposalByCloneSellerPro(ori.Id, e.Data.Value, View.UserName, false, false);
            }
            else
            {
                pid = PponFacade.CreateProposalByCloneDeal(e.Data.Key, e.Data.Value, View.UserName);
            }

            if (pid != 0)
            {
                Proposal pro = sp.ProposalGet(pid);

                if (ProposalFacade.IsVbsProposalNewVersion() && ori.DeliveryType == (int)DeliveryType.ToHouse && ori.DealType != (int)ProposalDealType.Travel)
                {
                    View.ShowMessage(string.Empty, ProposalContentMode.GotoHouseProposal, pid.ToString());
                }
                else
                {
                    if (pro.IsEarlierPageCheck)
                    {
                        View.ShowMessage("原提案單有申請頁確，複製提案單後請重新確認是否仍要執行頁確。", ProposalContentMode.AlertGoToProposal, pid.ToString());
                    }
                    else
                    {
                        View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, pid.ToString());
                    }
                }
            }
            else
            {
                View.ShowMessage("提案產生錯誤。", ProposalContentMode.DataError);
            }
        }

        protected void OnOrderTimeSave(object sender, DataEventArgs<DateTime> e)
        {
            if (View.ProposalId != 0)
            {
                bool isChangeOrderTime = false;
                Proposal pro = sp.ProposalGet(View.ProposalId);
                string strPhotoGrapher = string.Empty;
                string strPhotographerAppointDate = string.Empty;
                if (pro.IsLoaded && pro.BusinessHourGuid.HasValue)
                {
                    string message = "";
                    if (View.IsOrderTimeSave)
                    {
                        //排檔再確認，取消排檔不用確認
                        //合約已回沒勾至少也要勾先上檔
                        if (!Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck) && !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractNotCheck))
                        {
                            View.ShowMessage("「同意合約未回先上檔」尚未確認", ProposalContentMode.DataError);
                            return;
                        }

                        //若勾了合約已回就要勾合約檢核
                        if (Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck) && !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractInspect))
                        {
                            View.ShowMessage("「附約已回」已確認，但「合約檢核」尚未確認", ProposalContentMode.DataError);
                            return;
                        }

                        if (pro.OrderTimeS != null && pro.OrderTimeS != e.Data)
                            isChangeOrderTime = true;

                        pro.OrderTimeS = e.Data;


                        #region 攝影日期確認
                        if (pro.SpecialAppointFlagText != null)
                        {
                            string photoString = "";
                            Dictionary<ProposalPhotographer, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalPhotographer, string>>(pro.SpecialAppointFlagText);
                            if (specialText != null && specialText.Count > 0)
                            {
                                photoString = specialText[(ProposalPhotographer)ProposalPhotographer.PhotographerAppointCheck];
                                strPhotographerAppointDate = photoString.Split('|')[0];
                                strPhotoGrapher = photoString.Split('|')[3];
                            }

                            DateTime PhotographerAppointDate;
                            if (DateTime.TryParse(strPhotographerAppointDate, out PhotographerAppointDate))
                            {
                                int Diff = e.Data.Subtract(PhotographerAppointDate).Days; //日期相減
                                if (Diff <= 4)
                                    message = "【攝影日期、上檔日期，二個日期差距不到4天，請注意！】";
                            }

                        }
                        #endregion

                    }
                    else
                    {
                        pro.OrderTimeS = null;
                        isChangeOrderTime = true;
                    }

                    pro.EditFlag = Convert.ToInt32(Helper.SetFlag(false, pro.EditFlag, ProposalEditorFlag.Setting));
                    pro.EditPassFlag = Convert.ToInt32(Helper.SetFlag(false, pro.EditPassFlag, ProposalEditorFlag.Setting));
                    pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(View.IsOrderTimeSave, pro.BusinessFlag, ProposalBusinessFlag.ScheduleCheck));
                    pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                    pro.ModifyId = View.UserName;
                    pro.ModifyTime = DateTime.Now;

                    ProposalFacade.ProposalLog(View.ProposalId, string.Format("[系統] 排檔於 {0} " + (View.IsOrderTimeSave ? "" : "取消") + "上檔", e.Data.ToString("yyyy/MM/dd")), View.UserName);


                    #region 排檔時，同步更新後台的上檔時間及DealTimeSlot
                    Guid bid = pro.BusinessHourGuid ?? Guid.Empty;

                    PponDeal entity; //for dealTimeSlot
                    int[] cities; //for dealTimeSlot

                    if (bid != Guid.Empty)
                    {
                        //母子檔要同步更新
                        ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(bid);
                        VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));

                        if (pro.OrderTimeS.HasValue)
                        {
                            if (vcdc.Count > 0)
                            {
                                //多檔次
                                foreach (ViewComboDeal vcd in vcdc)
                                {
                                    BusinessHour bh = pp.BusinessHourGet(vcd.BusinessHourGuid);
                                    SetProposalInfo(pro, recentHolidays, bh);
                                    pp.BusinessHourSet(bh);

                                    //取得原本設定的Dts
                                    DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
                                    entity = pp.PponDealGet(bid);
                                    entity.TimeSlotCollection.CopyTo(origDtsCol);
                                    //處理DealTimeSlot 應該共同抓後台的!!!
                                    cities = GetDealPropertyCityList(vcd.BusinessHourGuid);
                                    DealTimeSlotInsert(entity, cities, false);
                                    // first we check to see if number of records match


                                    //應該共同抓後台的
                                    bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
                                    if (!syncLoc)
                                    {
                                        origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                                        entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                                        for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                                            syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                                        entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                                        entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                                        entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                                        entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                                        entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                                        entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
                                    }
                                    //更新DealTimeSlot
                                    pp.PponDealSet(entity, syncLoc, origDtsCol);
                                }
                            }
                            else
                            {
                                //單檔次
                                BusinessHour bh = pp.BusinessHourGet(bid);
                                SetProposalInfo(pro, recentHolidays, bh);
                                pp.BusinessHourSet(bh);

                                //取得原本設定的Dts
                                DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
                                entity = pp.PponDealGet(bid);
                                entity.TimeSlotCollection.CopyTo(origDtsCol);
                                //處理DealTimeSlot
                                cities = GetDealPropertyCityList(bid);
                                DealTimeSlotInsert(entity, cities, false);
                                // first we check to see if number of records match
                                bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
                                if (!syncLoc)
                                {
                                    origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                                    entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                                    for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                                        syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                                    entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                                    entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                                    entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                                    entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                                    entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                                    entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
                                }
                                //更新DealTimeSlot
                                pp.PponDealSet(entity, syncLoc, origDtsCol);
                            }
                        }
                        else
                        {
                            if (vcdc.Count > 0)
                            {
                                foreach (ViewComboDeal vcd in vcdc)
                                {
                                    BusinessHour bh = pp.BusinessHourGet(vcd.BusinessHourGuid);
                                    //取消排檔
                                    bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                                    bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                                    bh.BusinessHourDeliverTimeS = DateTime.MaxValue.Date;
                                    bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                                    pp.BusinessHourSet(bh);
                                    //處理DealTimeSlot
                                    pp.DealTimeSlotDeleteList(vcd.BusinessHourGuid);
                                }
                            }
                            else
                            {
                                //單檔次
                                BusinessHour bh = pp.BusinessHourGet(bid);
                                //取消排檔
                                bh.BusinessHourOrderTimeS = DateTime.MaxValue.Date;
                                bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                                bh.BusinessHourDeliverTimeS = DateTime.MaxValue.Date;
                                bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                                pp.BusinessHourSet(bh);
                                //處理DealTimeSlot
                                pp.DealTimeSlotDeleteList(bid);
                            }
                        }
                    }
                    #endregion

                    #region 若為(取消)排檔，寄送通知信予 排檔收件人權限人員、COD(好康或宅配)及攝影人員

                    List<string> mailToUser = new List<string>();
                    ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                    ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);
                    if (deSalesEmp.IsLoaded)
                    {
                        Seller s = sp.SellerGet(pro.SellerGuid);
                        if (isChangeOrderTime)
                        {
                            //排檔收件人
                            List<string> schedule = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.ScheduleCheckMail);
                            mailToUser.AddRange(schedule);


                            string SalesType = "";

                            //業務,業助群組,業務主管
                            if (deSalesEmp.DeptId == EmployeeChildDept.S001.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S002.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S003.ToString() ||
                                deSalesEmp.DeptId == EmployeeChildDept.S004.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S005.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S006.ToString() ||
                                deSalesEmp.DeptId == EmployeeChildDept.S007.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S008.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S012.ToString() ||
                                deSalesEmp.DeptId == EmployeeChildDept.S013.ToString() || deSalesEmp.DeptId == EmployeeChildDept.S010.ToString())
                            {

                                //業務本人
                                if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                                    mailToUser.Add(deSalesEmp.Email);
                                if (opSalesEmp.IsLoaded)
                                    mailToUser.Add(opSalesEmp.Email);

                                //業務主管
                                var emps = hp.ViewEmployeeCollectionGetAll().Where(x => x.DeptManager != null && x.DeptManager != "" && x.CrossDeptTeam != null);
                                if (emps != null)
                                {
                                    if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                                    {
                                        IEnumerable<ViewEmployee> deManager = emps.Where(x => x.CrossDeptTeam.Contains(deSalesEmp.DeptId));
                                        foreach (ViewEmployee m in deManager)
                                        {
                                            if (RegExRules.CheckEmail(m.Email))
                                            {
                                                mailToUser.Add(m.Email);//開發業務的主管email        
                                            }
                                        }
                                    }

                                    if (opSalesEmp.IsLoaded)
                                    {
                                        IEnumerable<ViewEmployee> opManager = emps.Where(x => x.CrossDeptTeam.Contains(opSalesEmp.DeptId));
                                        foreach (ViewEmployee m in opManager)
                                        {
                                            if (RegExRules.CheckEmail(m.Email))
                                            {
                                                mailToUser.Add(m.Email);//經營業務的主管email        
                                            }
                                        }
                                    }

                                }


                                if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString() && hp.DepartmentGet(deSalesEmp.DeptId).ParentDeptId != EmployeeDept.M000.ToString())
                                {
                                    SalesType = "在地";
                                    //在地各區業助群組
                                    mailToUser.Add(config.CodLocalEmail);
                                    mailToUser.Add(ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId));
                                    if (opSalesEmp.IsLoaded)
                                        mailToUser.Add(ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId));
                                }
                                else
                                {
                                    SalesType = "宅配";
                                    //全國宅配特助群組
                                    mailToUser.Add(config.OadMpShipEmail);
                                }
                            }

                            string subject = string.Format("【{0}】 單號 NO.{1} 【{2}業務：{3}】", "檔期異動通知", pro.Id, SalesType, deSalesEmp.EmpName);
                            string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                            ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));
                        }

                        #region 若為免稅標記，寄送通知信予 免稅通知人員

                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FreeTax))
                        {
                            List<string> mailToUserTax = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.TaxFreeNotice);

                            string subjectTax = string.Format("【免稅】 單號 NO.{0}", pro.Id);
                            string txtUrl = string.Format("{0}/controlroom/ppon/setup.aspx?bid={1}", config.SiteUrl, pro.BusinessHourGuid.ToString());
                            ProposalFacade.SendEmail(mailToUserTax, subjectTax, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, txtUrl));

                        }

                        #endregion
                    }



                    #endregion

                    if (isChangeOrderTime)
                    {
                        pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(false, pro.BusinessFlag, ProposalBusinessFlag.PhotographerCheck));
                    }

                    sp.ProposalSet(pro);

                    if (string.IsNullOrEmpty(message))
                        View.RedirectProposal(pro.Id);
                    else
                        View.ShowMessage(message, ProposalContentMode.AlertGoToProposal, pro.Id.ToString());
                }
            }
        }

        protected void OnChangeSeller(object sender, DataEventArgs<string> e)
        {

            string message = ProposalFacade.ChangeSeller(ProposalSourceType.Original, View.ProposalId, e.Data, View.UserName);

            if (string.IsNullOrEmpty(message))
            {
                View.ShowMessage(string.Empty, ProposalContentMode.GoToProposal, View.ProposalId.ToString());
            }
            else
            {
                View.ShowMessage(message, ProposalContentMode.DataError, View.ProposalId.ToString());
            }
        }

        /// <summary>
        /// 設定開檔及使用起訖時間
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="recentHolidays"></param>
        /// <param name="bh"></param>
        private void SetProposalInfo(Proposal pro, VacationCollection recentHolidays, BusinessHour bh)
        {
            if (pro.OrderTimeS.HasValue)
            {
                #region 時程資訊

                DateTime orderStart = pro.OrderTimeS.Value; // 上檔日
                DateTime OrderTimeS = orderStart.AddMinutes(config.BusinessOrderTimeSMinute);
                if (pro.DeliveryType == (int)DeliveryType.ToShop)
                {
                    #region 憑證

                    if (pro.OrderTimeS != null)
                    {
                        DateTime orderEnd; // 結檔日
                        DateTime useStart; // 開始使用
                        DateTime useEnd; // 結束使用

                        int startUseValue = 0;
                        int.TryParse(pro.StartUseText, out startUseValue);
                        DateTime startUseDate;
                        DateTime.TryParse(pro.StartUseText, out startUseDate);
                        ProposalStartUseUnitType unit = (ProposalStartUseUnitType)pro.StartUseUnit;

                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.OnlineUse))
                        {
                            #region 即買即用

                            // 開始使用 = 上檔日
                            useStart = orderStart;

                            // 結束使用 = 開始使用 + {指定天數} or 指定日期
                            useEnd = ProposalFacade.GetUseTimeEnd(useStart, startUseValue, startUseDate, unit);

                            // 兌換前六天
                            orderEnd = useEnd.AddDays(-6);

                            #endregion
                        }
                        else
                        {
                            if (pro.DealType == (int)ProposalDealType.Travel)
                            {
                                #region 旅遊

                                // 開始使用 = 上檔日
                                useStart = orderStart;

                                // 結束使用 = 開始使用 + {指定天數} or 指定日期
                                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, useStart, ProposalFacade.GetUseTimeEnd(useStart, startUseValue, startUseDate, unit));

                                // 結檔日 = 上檔日+45天
                                orderEnd = orderStart.AddDays(45);

                                #endregion
                            }
                            else
                            {
                                #region 好康

                                // 開始使用 = 上檔日+1天
                                useStart = orderStart.AddDays(1);

                                // 結束使用 = 開始使用 + {指定天數} or 指定日期
                                useEnd = ProposalFacade.GetUseTimeEnd(useStart, startUseValue, startUseDate, unit);

                                // 結檔日 = 結束使用前一週
                                orderEnd = useEnd.AddDays(-6);

                                #endregion
                            }
                        }

                        bh.BusinessHourDeliverTimeE = useEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                        bh.BusinessHourDeliverTimeS = useStart.AddMinutes(config.BusinessOrderTimeSMinute);

                        if (pro.DealType == (int)ProposalDealType.Piinlife)
                        {
                            bh.BusinessHourOrderTimeE = orderEnd.AddMinutes(720);
                            bh.BusinessHourOrderTimeS = (pro.OrderTimeS ?? DateTime.Now).AddMinutes(720);
                        }
                        else
                        {
                            bh.BusinessHourOrderTimeE = orderEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                            bh.BusinessHourOrderTimeS = (pro.OrderTimeS ?? DateTime.Now).AddMinutes(config.BusinessOrderTimeSMinute);
                        }

                    }

                    #endregion
                }
                else if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    #region 宅配

                    if (pro.OrderTimeS != null)
                    {
                        DateTime orderEnd; // 結檔日
                        DateTime useStart; // 開始使用
                        DateTime useEnd; // 結束使用


                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.YearContract))
                        {
                            #region 長期上架

                            // 結檔日 = 上檔日+一年
                            orderEnd = orderStart.AddYears(1);

                            // 開始使用 = 上檔日
                            useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart);

                            if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                            {
                                //快速出貨
                                //結束使用 = 結檔日+1
                                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(1));
                            }
                            else
                            {
                                if (pro.ShippingdateType == (int)ShippingDateType.Normal)
                                {
                                    #region 訂單成立後X個工作日出貨完畢
                                    int useDateEndSet = int.TryParse(pro.ShipText3, out useDateEndSet) ? useDateEndSet : 0;
                                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                                    #endregion
                                }
                                else
                                {
                                    #region 最早出貨日 ：上檔後X個工作日 ，最晚出貨日：統一結檔後x個工作日
                                    int useDateEndSet = int.TryParse(pro.ShipText2, out useDateEndSet) ? useDateEndSet : 0;
                                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                                    #endregion
                                }
                            }




                            #endregion
                        }
                        else if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            #region 快速出貨

                            // 結檔日 = 上檔日+一年
                            orderEnd = orderStart.AddYears(1);

                            // 開始使用 = 上檔日
                            useStart = orderStart;

                            // 結束使用 = 結檔日+1
                            useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(1));

                            #endregion
                        }
                        else
                        {
                            #region 一般出貨

                            if (pro.DealType == (int)ProposalDealType.Travel)
                            {
                                #region 宅配旅遊

                                // 結檔日=上檔日+45
                                orderEnd = orderStart.AddDays(45);

                                // 開始使用=上檔日
                                useStart = orderStart;

                                // 結束使用=開始使用+4
                                useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, useStart, useStart.AddDays(4));

                                #endregion
                            }
                            else
                            {
                                #region 一般宅配

                                // 結檔日=上檔日+一年
                                orderEnd = orderStart.AddYears(1);

                                // 開始使用=上檔日
                                useStart = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderStart, orderStart);

                                // 結束使用=結檔日+指定天數
                                if (pro.ShippingdateType == (int)ShippingDateType.Normal)
                                {
                                    #region 訂單成立後X個工作日出貨完畢
                                    int useDateEndSet = int.TryParse(pro.ShipText3, out useDateEndSet) ? useDateEndSet : 0;
                                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                                    #endregion
                                }
                                else
                                {
                                    #region 最早出貨日 ：上檔後X個工作日 ，最晚出貨日：統一結檔後x個工作日
                                    int useDateEndSet = int.TryParse(pro.ShipText2, out useDateEndSet) ? useDateEndSet : 0;
                                    useEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, orderEnd, orderEnd.AddDays(useDateEndSet));
                                    #endregion
                                }
                                #endregion
                            }

                            #endregion
                        }
                        bh.BusinessHourDeliverTimeE = useEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                        bh.BusinessHourDeliverTimeS = useStart.AddMinutes(config.BusinessOrderTimeSMinute);

                        if (pro.DealType == (int)ProposalDealType.Piinlife)
                        {
                            bh.BusinessHourOrderTimeE = orderEnd.AddMinutes(720);
                            bh.BusinessHourOrderTimeS = (pro.OrderTimeS ?? DateTime.Now).AddMinutes(720);
                        }
                        else
                        {
                            bh.BusinessHourOrderTimeE = orderEnd.AddMinutes(config.BusinessOrderTimeEMinute);
                            bh.BusinessHourOrderTimeS = (pro.OrderTimeS ?? DateTime.Now).AddMinutes(config.BusinessOrderTimeSMinute);
                        }

                    }

                    #endregion
                }

                #endregion
            }
        }

        #region DealTimeSlot 相關
        /// <summary>
        /// DealTimeSlot重構的方法
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="cities"></param>
        /// <param name="orderStartDateChange"></param>
        private void DealTimeSlotInsert(PponDeal entity, int[] cities, bool orderStartDateChange)
        {
            //**********寫入DealTimeSlot的邏輯 Begin**********
            #region 產生DealTimeSlot排程資料, it has a bug on removing items

            DateTime dateStartDay = entity.Deal.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.Deal.BusinessHourOrderTimeE;
            DealTimeSlotCollection dealTimeSlotColForInert = new DealTimeSlotCollection();

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId);
                }

                if (dealTimeSlotColForInert.Count > 0)
                {
                    entity.TimeSlotCollection = dealTimeSlotColForInert;
                }
                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                #region 將不足的資料補上
                //將不足的資料補上
                //一開始排程時間設為DEAL起始日
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    //只要排程時間小於DEAL結束日就需增加一筆排程記錄
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId, true);
                }
                #endregion 將不足的資料補上

                entity.TimeSlotCollection = dealTimeSlotColForInert;
            }

            if ((entity.TimeSlotCollection != null) && (orderStartDateChange))
            {
                // 預設前24小時輪播  add by Max 2011/4/21
                foreach (DealTimeSlot dts in entity.TimeSlotCollection.Where(x => DateTime.Compare(entity.Deal.BusinessHourOrderTimeS.AddDays(1), x.EffectiveStart) > 0))
                {
                    dts.IsInTurn = true;
                }
            }
            //天貓檔次設定不顯示
            if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                foreach (var item in entity.TimeSlotCollection)
                {
                    item.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                }
            }

            //=====================================================
            #endregion 產生DealTimeSlot排程資料, it has a bug on removing items
            //**********寫入DealTimeSlot的邏輯 End**********
        }

        /// <summary>
        /// 迴圈新增DealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert">要寫入的dealTimeSlot</param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="cityId"></param>
        /// <param name="calNewSeq"></param>
        private void AddDealTimeSlotLoop(PponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, DateTime dateStartDay, DateTime dateEndDate, int cityId, bool calNewSeq = false)
        {
            Guid dealGuid = entity.Deal.Guid;
            //檢查第一筆
            bool isFirst = true;


            DealTimeSlotStatus timeSlotStatus = entity.Payment.IsBankDeal ? DealTimeSlotStatus.NotShowInPponDefault : DealTimeSlotStatus.Default;
            if (timeSlotStatus == DealTimeSlotStatus.Default && entity.TimeSlotCollection.Any())
            {
                //直接抓最後一筆的狀態
                var last = entity.TimeSlotCollection.Where(x => x.CityId == cityId).OrderBy(y => y.EffectiveEnd).LastOrDefault();
                if (last != null && last.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                {
                    timeSlotStatus = DealTimeSlotStatus.NotShowInPponDefault;
                }
            }

            //EffectiveStart是Key值
            for (var date = dateStartDay; date < dateEndDate; date = date.AddDays(1))
            {
                //檢查資料是否已經存在
                bool anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);

                //取得該筆舊資料
                DealTimeSlot dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();
                DealTimeSlot insDeal = null;
                bool isFirstInserted = false;

                #region 第一筆

                if (isFirst) //針對第一筆的時間做調整
                {
                    //一開始排程時間設為DEAL起始日
                    Tuple<DateTime, DealTimeSlot> tuple = AdjustFirstEffectiveStartDay(date, dateEndDate, dealGuid, cityId, timeSlotStatus);
                    if (tuple.Item1 != DateTime.MinValue) //調整DEAL起始時間
                    {
                        if (!anyExist)
                        {
                            dealTimeSlotColForInert.Add(tuple.Item2);
                        }
                        else
                        {
                            //檔次已存在，維持舊的排序
                            dealTimeSlotColForInert.Add(GetNewDealTimeSlot(dts, dateEndDate));
                        }

                        //用調整後的時間做新的起始點
                        date = tuple.Item1;
                        isFirstInserted = date == dateEndDate;
                        //檢查調整後的時間資料是否已經存在
                        anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);//★這兩行是多餘的?
                        dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();//★這兩行是多餘的?
                    }

                    //一定要把isFirst關掉
                    isFirst = false;
                }

                if (isFirstInserted)
                {
                    continue;
                }

                #endregion

                #region 最後一筆

                if (date.AddDays(1) > dateEndDate)
                {
                    int lastSeq = 999999;

                    if (!anyExist)
                    {
                    }
                    else
                    {
                        //檔次已存在，維持舊的排序
                        lastSeq = dts.Sequence;
                    }
                    DealTimeSlot lastDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                      lastSeq,
                                                      date, dateEndDate,
                                                      timeSlotStatus);

                    dealTimeSlotColForInert.Add(lastDeal);
                    continue;
                }

                #endregion

                #region  其他筆

                if (!anyExist)
                {
                    //檔次不存在
                    int newSeq = 999999;

                    //新增 此城市這個時間一筆排程記錄
                    insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                        newSeq,
                                                        date, date.AddDays(1),
                                                        timeSlotStatus);
                }
                else
                {
                    //檔次已存在，維持舊的排序
                    insDeal = GetNewDealTimeSlot(dts, dateEndDate);
                }

                #endregion

                if (!dealTimeSlotColForInert.Any(t => t.EffectiveStart == date && t.CityId == cityId))
                {
                    dealTimeSlotColForInert.Add(insDeal);
                }
            }
        }

        /// <summary>
        /// 處理第一筆，如果起始時間不是12點，先寫一筆將其調整到12點
        /// </summary>
        /// <param name="effectiveStartDay"></param>
        /// <param name="effectiveEndDay"></param>
        /// <param name="dealGuid"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        private Tuple<DateTime, DealTimeSlot> AdjustFirstEffectiveStartDay(DateTime effectiveStartDay, DateTime effectiveEndDay, Guid dealGuid, int cityId, DealTimeSlotStatus status = DealTimeSlotStatus.Default)
        {
            DateTime tempEndDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;

            if (effectiveStartDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveStartDay.Hour < 12) //未過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon();

                }
                else //已過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon().AddDays(1);
                }
            }

            //首筆DealTimeSlot截止時間若已大於檔次截止時間則直接修改為截止時間
            if (tempEndDay > effectiveEndDay)
            {
                tempEndDay = effectiveEndDay;
            }

            //int newSeq = PponFacade.GetDealTimeSlotNonLockSeq(cityId, effectiveStartDay, vpdts);
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                         999999, //★
                                          effectiveStartDay, tempEndDay,
                                          status);

            return Tuple.Create(tempEndDay, insDeal);
        }

        /// <summary>
        /// 傳回新建的DealTimeSlot物件，DataTable欄位的值與傳入的DealTimeSlot相同
        /// </summary>
        /// <param name="fromData"></param>
        /// <returns></returns>
        private DealTimeSlot GetNewDealTimeSlot(DealTimeSlot fromData, DateTime dateEndDate)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = fromData.BusinessHourGuid;
            newDeal.CityId = fromData.CityId;
            newDeal.Sequence = fromData.Sequence;
            newDeal.EffectiveStart = fromData.EffectiveStart;

            //fix 手動結檔再開檔，fromData.EffectiveEnd.Noon()同日問題
            if (newDeal.EffectiveStart.Date.Equals(fromData.EffectiveEnd.Date) && newDeal.EffectiveStart >= fromData.EffectiveEnd.Noon())
            {
                //原最後一筆又起訖同一天,繼續拉長
                newDeal.EffectiveEnd = fromData.EffectiveEnd.AddDays(1).Noon();
            }
            else if (fromData.EffectiveEnd < fromData.EffectiveEnd.Noon() && fromData.EffectiveEnd < dateEndDate)
            {
                //0600 繼續拉長(非最後一筆)
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            else if (fromData.EffectiveEnd <= fromData.EffectiveEnd.Noon() && fromData.EffectiveEnd > dateEndDate)
            {
                //1200 只剩第一筆又要縮短
                newDeal.EffectiveEnd = dateEndDate;
            }
            else
            {
                //一般的1200
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            //日期銜接為12點，所以直接設定12點

            newDeal.Status = fromData.Status;
            newDeal.IsInTurn = fromData.IsInTurn;
            newDeal.IsLockSeq = fromData.IsLockSeq; //★本次新增

            return newDeal;
        }

        private DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }

        private int[] GetDealPropertyCityList(Guid bid)
        {
            List<int> selectedCity = new List<int>();
            CategoryDealCollection cds = pp.CategoryDealsGetList(bid);

            foreach (CategoryDeal cd in cds)
            {
                PponCity city = PponCityGroup.GetPponCityByCategoryId(cd.Cid);
                if (city != null)
                {
                    selectedCity.Add(city.CityId);
                }
            }

            int[] cites = selectedCity.ToArray();
            return cites;
        }

        #endregion DealTimeSlot 相關


        protected void OnUrgent(object sender, EventArgs e)
        {
            if (View.ProposalId != 0)
            {
                Proposal pro = sp.ProposalGet(View.ProposalId);
                if (pro.IsLoaded)
                {
                    if (pro.OrderTimeS != null)
                    {
                        ProposalSpecialFlag flag = ProposalSpecialFlag.Urgent;
                        pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(View.IsUrgent, pro.SpecialFlag, flag));
                        sp.ProposalSet(pro);
                        ProposalFacade.ProposalLog(View.ProposalId, (View.IsUrgent ? "" : "取消") + Helper.GetLocalizedEnum(flag), View.UserName);

                        #region 若為快速上檔標註，則寄送通知信予 創編組長

                        List<string> mailToUser = new List<string>() { config.SupportingLeaderEmail };
                        ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        Seller s = sp.SellerGet(pro.SellerGuid);
                        string subject = string.Format("【排檔編輯】 <{0}> 單號 NO.{1}", (View.IsUrgent ? "" : "取消") + Helper.GetLocalizedEnum(flag), pro.Id);
                        string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                        ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, emp.EmpName, url));


                        #endregion

                        View.RedirectProposal(pro.Id);
                    }
                    else
                    {
                        View.ShowMessage("檔次尚未排檔。", ProposalContentMode.DataError);
                    }
                }
            }
        }

        /// <summary>
        /// 商家提案新舊資料比較
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="ori"></param>
        private void CompareSellerPorposal(Proposal pro, Proposal ori)
        {
            List<string> changeList = new List<string>();
            List<string> cols = new List<string>();
            cols.Add("SalesId");
            cols.Add("BrandName");
            cols.Add("DealType");
            cols.Add("DealSubType");
            cols.Add("DeliveryMethod");
            cols.Add("ContractMemo");
            cols.Add("MultiDeals");
            cols.Add("Othermessage");
            cols.Add("ProductSpec");
            cols.Add("SpecialFlag");
            cols.Add("SpecialFlagText");
            cols.Add("ShipType");
            cols.Add("ShipText1");
            cols.Add("ShipText2");
            cols.Add("ShipText3");
            cols.Add("ShippingdateType");


            List<ProposalSpecialFlag> specialFlagList = new List<ProposalSpecialFlag>();
            specialFlagList.Add(ProposalSpecialFlag.ParallelImportation);   //平行輸入
            specialFlagList.Add(ProposalSpecialFlag.Photographers);          //圖片來源(需拍攝)
            specialFlagList.Add(ProposalSpecialFlag.SellerPhoto);           //圖片來源(店家提供照片)
            specialFlagList.Add(ProposalSpecialFlag.FTPPhoto);          //圖片來源(舊照片)
            specialFlagList.Add(ProposalSpecialFlag.Beauty); //醫療 / 妝廣字號
            specialFlagList.Add(ProposalSpecialFlag.ExpiringProduct); //即期品
            specialFlagList.Add(ProposalSpecialFlag.InspectRule); //檢驗規定
            specialFlagList.Add(ProposalSpecialFlag.DealStar); //本檔主打星


            foreach (var item in PponFacade.ColumnValueComparison(ori, pro, cols, specialFlagList))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                ProposalFacade.ProposalLog(pro.Id, string.Join("|", changeList), View.UserName, ProposalLogType.DenySales);
            }
        }




        /// <summary>
        /// 營業據點新舊資料比較
        /// </summary>
        /// <param name="proposalStore"></param>
        /// <param name="oristore"></param>
        private void CompareLocationSeller(ProposalStoreCollection proposalStore, ProposalStoreCollection oristore)
        {
            List<Guid> proposalstoreguid = new List<Guid>();
            List<Guid> oristoreguid = new List<Guid>();
            List<string> changeList = new List<string>();
            List<string> savepponstorename = new List<string>();
            proposalstoreguid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Location)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            oristoreguid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Location)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();


            if (string.Join("、", proposalstoreguid) != string.Join("、", oristoreguid))
            {
                foreach (var s in proposalstoreguid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                changeList.Add(string.Format("變更 {0} 為 {1}", "營業據點", string.Join("、", savepponstorename)));
            }

            if (changeList.Count > 0)
            {
                ProposalFacade.ProposalLog(proposalStore[0].ProposalId, string.Join("、", changeList), View.UserName);
            }
        }

        /// <summary>
        /// 核銷相關新舊資料比較
        /// </summary>
        /// <param name="proposalStore"></param>
        /// <param name="oristore"></param>
        private void CompareVbsSeller(ProposalStoreCollection proposalStore, ProposalStoreCollection oristore)
        {
            List<Guid> VerifyGuid = new List<Guid>();
            List<Guid> AccoutingGuid = new List<Guid>();
            List<Guid> ViewBalanceSheetGuid = new List<Guid>();
            List<Guid> BalanceSheetHideFromDealSellerGuid = new List<Guid>();
            List<Guid> VerifyShopGuid = new List<Guid>();

            List<Guid> OriVerifyGuid = new List<Guid>();
            List<Guid> OriAccoutingGuid = new List<Guid>();
            List<Guid> OriViewBalanceSheetGuid = new List<Guid>();
            List<Guid> OriBalanceSheetHideFromDealSellerGuid = new List<Guid>();
            List<Guid> OriVerifyShopGuid = new List<Guid>();


            List<string> changeList = new List<string>();
            List<string> savepponstorename = new List<string>();


            VerifyGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Verify)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriVerifyGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Verify)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            AccoutingGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriAccoutingGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            ViewBalanceSheetGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.ViewBalanceSheet)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriViewBalanceSheetGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.ViewBalanceSheet)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            BalanceSheetHideFromDealSellerGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriBalanceSheetHideFromDealSellerGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            VerifyShopGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.VerifyShop)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriVerifyShopGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.VerifyShop)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();


            if (string.Join("、", VerifyGuid) != string.Join("、", OriVerifyGuid))
            {
                foreach (var s in VerifyGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                changeList.Add(string.Format("變更 {0} 為 {1}", "憑證核實", string.Join("、", savepponstorename)));

                savepponstorename.Clear();
            }

            if (string.Join("、", AccoutingGuid) != string.Join("、", OriAccoutingGuid))
            {
                foreach (var s in AccoutingGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                changeList.Add(string.Format("變更 {0} 為 {1}", "匯款對象", string.Join("、", savepponstorename)));

                savepponstorename.Clear();
            }

            if (string.Join("、", ViewBalanceSheetGuid) != string.Join("、", OriViewBalanceSheetGuid))
            {
                foreach (var s in ViewBalanceSheetGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "檢視對帳單", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "檢視對帳單", "\"移除檢視對帳單\""));

                savepponstorename.Clear();
            }

            if (string.Join("、", BalanceSheetHideFromDealSellerGuid) != string.Join("、", OriBalanceSheetHideFromDealSellerGuid))
            {
                foreach (var s in BalanceSheetHideFromDealSellerGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "鎖住檢視對帳單", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "鎖住檢視對帳單", "\"移除鎖住檢視對帳單\""));

                savepponstorename.Clear();
            }

            if (string.Join("、", VerifyShopGuid) != string.Join("、", OriVerifyShopGuid))
            {
                foreach (var s in VerifyShopGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "銷售店舖", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "銷售店舖", "\"移銷售店舖\""));

                savepponstorename.Clear();
            }

            if (changeList.Count > 0)
            {
                ProposalFacade.ProposalLog(proposalStore[0].ProposalId, string.Join("|", changeList), View.UserName);
            }
        }



        protected void OnDelete(object sender, EventArgs e)
        {
            if (View.ProposalId != 0)
            {
                ProposalFacade.DeleteProposal(View.ProposalId, null, View.UserName);
                View.RedirectSellerList();
            }
        }


        protected void OnCreateBusinessHour(object sender, EventArgs e)
        {
            //取得提案單及商家資料
            Proposal pro = sp.ProposalGet(View.ProposalId);
            ProposalStoreCollection prostore = pp.ProposalStoreGetListByProposalId(View.ProposalId);
            Seller s = sp.SellerGet(pro.SellerGuid);

            if (!Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Photographers)
                && !Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FTPPhoto)
                && !Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.SellerPhoto)
                && !pro.SystemPic)
            {
                View.ShowMessage("至少選擇一項照片來源", ProposalContentMode.DataError, View.ProposalId.ToString());
                return;
            }


            if (pro.VendorReceiptType == null || s.VendorReceiptType == null)
            {
                View.ShowMessage("商家或提案單之「店家單據開立方式」未填寫", ProposalContentMode.DataError, View.ProposalId.ToString());
                return;
            }

            //宅配檢查
            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (pro.DeliveryMethod == null)
                {
                    View.ShowMessage("「配送方式」未填寫", ProposalContentMode.DataError, View.ProposalId.ToString());
                    return;
                }
            }

            //店家判斷
            if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
            {
                //一般宅配
                if (prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Verify)).Count() == 0 || prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Count() == 0)
                {
                    View.ShowMessage("請勾選核銷店家", ProposalContentMode.DataError, View.ProposalId.ToString());
                    return;
                }
            }
            else
            {
                if (prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Location)).Count() == 0 || prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Verify)).Count() == 0 || prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Count() == 0)
                {
                    View.ShowMessage("請勾選營業及核銷店家", ProposalContentMode.DataError, View.ProposalId.ToString());
                    return;
                }
            }

            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));


            //所有有關的商家
            List<Seller> TempStatus = new List<Seller>();
            List<Seller> Accouting = new List<Seller>();
            ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);


            //該檔次是否同意新合約
            bool IsAgreeNewContractDeal = false;
            if (pro.DeliveryType == (int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
                IsAgreeNewContractDeal = SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToShop);
            else
                IsAgreeNewContractDeal = SellerFacade.IsAgreeNewContractSeller(s.Guid, (int)DeliveryType.ToHouse);

            if (config.IsRemittanceFortnightly && IsAgreeNewContractDeal)
            {
                if (pro.DeliveryType ==(int)DeliveryType.ToShop || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
                {
                    foreach (var ps in psc)
                    {
                        Seller AddSeller = sp.SellerGet(ps.StoreGuid);
                        TempStatus.Add(AddSeller);

                        if (Helper.IsFlagSet(ps.VbsRight, VbsRightFlag.Accouting))
                        {
                            if (string.IsNullOrEmpty(AddSeller.CompanyName) || string.IsNullOrEmpty(AddSeller.CompanyBossName) || AddSeller.CompanyBankCode == "-1" ||
                                string.IsNullOrEmpty(AddSeller.CompanyBranchCode) || string.IsNullOrEmpty(AddSeller.CompanyID) || string.IsNullOrEmpty(AddSeller.CompanyAccount) ||
                                string.IsNullOrEmpty(AddSeller.CompanyAccountName) || string.IsNullOrEmpty(AddSeller.AccountantName) || string.IsNullOrEmpty(AddSeller.AccountantTel) ||
                                string.IsNullOrEmpty(AddSeller.CompanyEmail) || AddSeller.RemittanceType == null || AddSeller.VendorReceiptType == null)
                                Accouting.Add(AddSeller);
                        }

                    }
                }
                else
                {
                    //宅配不用檢查出帳方式
                    foreach (var ps in psc)
                    {
                        Seller AddSeller = sp.SellerGet(ps.StoreGuid);
                        TempStatus.Add(AddSeller);

                        if (Helper.IsFlagSet(ps.VbsRight, VbsRightFlag.Accouting))
                        {
                            if (string.IsNullOrEmpty(AddSeller.CompanyName) || string.IsNullOrEmpty(AddSeller.CompanyBossName) || AddSeller.CompanyBankCode == "-1" ||
                                string.IsNullOrEmpty(AddSeller.CompanyBranchCode) || string.IsNullOrEmpty(AddSeller.CompanyID) || string.IsNullOrEmpty(AddSeller.CompanyAccount) ||
                                string.IsNullOrEmpty(AddSeller.CompanyAccountName) || string.IsNullOrEmpty(AddSeller.AccountantName) || string.IsNullOrEmpty(AddSeller.AccountantTel) ||
                                string.IsNullOrEmpty(AddSeller.CompanyEmail) || AddSeller.VendorReceiptType == null)
                                Accouting.Add(AddSeller);
                        }

                    }
                }
            }
            else
            {
                foreach (var ps in psc)
                {
                    Seller AddSeller = sp.SellerGet(ps.StoreGuid);
                    TempStatus.Add(AddSeller);

                    if (Helper.IsFlagSet(ps.VbsRight, VbsRightFlag.Accouting))
                    {
                        if (string.IsNullOrEmpty(AddSeller.CompanyName) || string.IsNullOrEmpty(AddSeller.CompanyBossName) || AddSeller.CompanyBankCode == "-1" ||
                            string.IsNullOrEmpty(AddSeller.CompanyBranchCode) || string.IsNullOrEmpty(AddSeller.CompanyID) || string.IsNullOrEmpty(AddSeller.CompanyAccount) ||
                            string.IsNullOrEmpty(AddSeller.CompanyAccountName) || string.IsNullOrEmpty(AddSeller.AccountantName) || string.IsNullOrEmpty(AddSeller.AccountantTel) ||
                            string.IsNullOrEmpty(AddSeller.CompanyEmail) || AddSeller.RemittanceType == null || AddSeller.VendorReceiptType == null)
                            Accouting.Add(AddSeller);
                    }

                }
            }


            if (!TempStatus.Contains(s))
                TempStatus.Add(s);

            //尚未完成審核的商家
            TempStatus = TempStatus.Where(x => x.TempStatus == (int)SellerTempStatus.Applied || x.TempStatus == (int)SellerTempStatus.Returned).ToList();


            if (TempStatus.Count() != 0)
            {
                View.ShowMessage("商家尚未審核。尚未審核清單：" + string.Join("、", TempStatus.Select(x => x.SellerId)), ProposalContentMode.DataError);
                return;
            }

            if (Accouting.Count() != 0)
            {
                View.ShowMessage("財務資料未填寫完整，請完成填寫，並確實完成【申請核准】。未填寫完整清單：" + string.Join("、", Accouting.Select(x => x.SellerId)), ProposalContentMode.DataError);
                return;
            }


            int NormalSeller = 0;
            int ReturnPersonSeller = 0;
            List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
            NormalSeller = contacts.Where(x => x.Type == ((int)ProposalContact.Normal).ToString()).Count();
            ReturnPersonSeller = contacts.Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).Count();

            if (pro.DeliveryType == (int)DeliveryType.ToShop && NormalSeller == 0)
            {
                View.ShowMessage("憑證提案單須至少一個一般(頁確)聯絡人。請完成填寫，並確實完成【申請核准】", ProposalContentMode.DataError);
                return;
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToHouse && (NormalSeller == 0 || ReturnPersonSeller == 0))
            {
                View.ShowMessage("宅配提案單須至少一個一般(頁確)聯絡人，一個退換貨聯絡人。請完成填寫，並確實完成【申請核准】", ProposalContentMode.DataError);
                return;
            }

            ProposalSalesModel proposalSalesModel = ProposalFacade.ProposalSaleGetByPid(pro.Id);
            if (!string.IsNullOrEmpty(SellerFacade.CheckSales(1, proposalSalesModel.DevelopeSalesEmail, proposalSalesModel.OperationSalesEmail)))
            {
                View.ShowMessage("負責業務1、2 欄位，沒有完整填入資料，無法建檔，請重新輸入。", ProposalContentMode.DataError);
                return;
            }





            if (pro.Status >= (int)ProposalStatus.Approve && pro.BusinessHourGuid == null)
            {
                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                if (multiDeals != null && multiDeals.Count > 0)
                {

                    var dealItemName = multiDeals.Where(x => string.IsNullOrEmpty(x.ItemName)).FirstOrDefault();
                    if (dealItemName != null)
                    {
                        View.ShowMessage("專案內容不可為空白，旅遊檔次請輸入第二個欄位。", ProposalContentMode.DataError, View.ProposalId.ToString());
                        return;
                    }

                    Guid bid = Guid.Empty;
                    ProposalMultiDeal model = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();

                    #region 建立新檔
                    bid = PponFacade.PponDealCreateByProposal(View.UserName, pro, model);

                    //多重選項
                    if (!string.IsNullOrEmpty(model.Options))
                    {
                        List<dynamic> uiOptions;
                        if (!PponFacade.TryParseOptions(model.Options, out uiOptions))
                        {
                            View.ShowMessage("多重選項格式有誤，請重新輸入。", ProposalContentMode.DataError);
                            return;
                        }

                        foreach (var option in uiOptions)
                        {
                            if (option.OptItem.Length > 50)
                            {
                                View.ShowMessage("多重選項格式有誤或項目字數大於50，請重新輸入。", ProposalContentMode.DataError);
                                return;
                            }

                            if (option.OptItemNo != null && option.OptItemNo.Length > 100)
                            {
                                View.ShowMessage("多重選項格式有誤或貨號大於100，請重新輸入。", ProposalContentMode.DataError);
                                return;
                            }
                        }

                        PponFacade.ProcessOptions(model.Options, bid, pp.DealPropertyGet(bid).UniqueId, View.UserName);
                    }
                    #endregion

                    if (bid != Guid.Empty)
                    {
                        #region 多檔次建立

                        if (multiDeals.Count > 1)
                        {
                            var sortbyMultiDeals = multiDeals.OrderBy(x => x.Sort);
                            foreach (ProposalMultiDeal sub in sortbyMultiDeals)
                            {
                                string msg;
                                Guid newGuid;
                                PponFacade.CopyDeal(bid, true, false, out msg, out newGuid, View.UserName);
                                if (newGuid != Guid.Empty)
                                {
                                    PponFacade.PponDealUpdateByProposal(newGuid, View.UserName, pro, sub);
                                }

                                //建檔時須補回bid
                                ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(sub.Id);
                                pmd.Bid = newGuid;
                                sp.ProposalMultiDealsSet(pmd);
                            }
                        }
                        else
                        {
                            //單檔也須補bid
                            ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(model.Id);
                            pmd.Bid = bid;
                            sp.ProposalMultiDealsSet(pmd);
                        }

                        #endregion

                        #region 更新提案單串接資訊

                        pro.BusinessHourGuid = bid;
                        pro.BusinessCreateFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created));
                        pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                        pro.ModifyId = View.UserName;
                        pro.ModifyTime = DateTime.Now;
                        sp.ProposalSet(pro);
                        ProposalFacade.ProposalLog(View.ProposalId, "[系統] 建檔", View.UserName);

                        #endregion

                        #region 不可使用折價券黑名單
                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount))
                        {
                            PromotionFacade.DiscountLimitSet(bid, View.UserName, DiscountLimitType.Enabled);
                        }
                        #endregion

                        #region 策展
                        if (!string.IsNullOrEmpty(pro.MarketingResource))
                        {
                            if (pro.BusinessHourGuid != null)
                            {
                                ProposalFacade.SaveBrandItem(pro.MarketingResource, bid, View.UserName);
                            }
                        }
                        #endregion 策展


                        View.RedirectBusinessHour(bid);
                    }
                    else
                    {
                        View.ShowMessage("建檔失敗，請洽IT人員。", ProposalContentMode.DataError);
                    }
                }
                else
                {
                    View.ShowMessage("多檔次資料不正確，請至少設定一個檔次。", ProposalContentMode.DataError);
                }
            }
        }

        protected void OnReferral(object sender, DataEventArgs<KeyValuePair<string, SellerSalesType>> e)
        {
            string message = ProposalFacade.ReferralSales(View.ProposalId, e.Data.Value, e.Data.Key, View.UserName);

            if (string.IsNullOrEmpty(message))
            {
                LoadData(View.ProposalId);
            }
            else
            {
                View.ShowMessage(message, ProposalContentMode.DataError);
            }
        }

        protected void OnProposalSend(object sender, EventArgs e)
        {
            Proposal pro = sp.ProposalGet(View.ProposalId);

            if (pro.DealType == (int)ProposalDealType.Product)
            {
                if (!string.IsNullOrEmpty(pro.SaleMarketAnalysis))
                {
                    var ma = new JsonSerializer().Deserialize<ProposalMarketAnalysis>(pro.SaleMarketAnalysis);
                    if (ma != null)
                    {
                        if (string.IsNullOrEmpty(ma.GomajiPrice) ||
                            string.IsNullOrEmpty(ma.GomajiLink) ||
                            string.IsNullOrEmpty(ma.KuobrothersPrice) ||
                            string.IsNullOrEmpty(ma.KuobrothersLink) ||
                            string.IsNullOrEmpty(ma.CrazymikePrice) ||
                            string.IsNullOrEmpty(ma.CrazymikeLink) ||
                            string.IsNullOrEmpty(ma.EzPricePrice1) ||
                            string.IsNullOrEmpty(ma.EzPriceLink1) ||
                        string.IsNullOrEmpty(ma.PinglePrice1) ||
                            string.IsNullOrEmpty(ma.PingleLink1)
                            )
                        {
                            View.ShowMessage("請輸入巿場分析比價價格及連結。", ProposalContentMode.DataError);
                            return;
                        }

                        //宅配建檔判斷
                        if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                        {
                            if (pro.DeliveryMethod == null)
                            {
                                View.ShowMessage("「配送方式」未填寫", ProposalContentMode.DataError, View.ProposalId.ToString());
                                return;
                            }
                        }

                        if (ma.OtherShop1 != "-1")
                        {
                            if (string.IsNullOrEmpty(ma.OtherPrice1) || string.IsNullOrEmpty(ma.OtherLink1))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪1已選擇，請輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ma.OtherPrice1) || !string.IsNullOrEmpty(ma.OtherLink1))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪1未選擇，請確認輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }

                        if (ma.OtherShop2 != "-1")
                        {
                            if (string.IsNullOrEmpty(ma.OtherPrice2) || string.IsNullOrEmpty(ma.OtherLink2))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪2已選擇，請輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ma.OtherPrice2) || !string.IsNullOrEmpty(ma.OtherLink2))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪2未選擇，請確認輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }

                        if (ma.OtherShop3 != "-1")
                        {
                            if (string.IsNullOrEmpty(ma.OtherPrice3) || string.IsNullOrEmpty(ma.OtherLink3))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪3已選擇，請輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(ma.OtherPrice3) || !string.IsNullOrEmpty(ma.OtherLink3))
                            {
                                View.ShowMessage("「巿場分析比價」其他店鋪3未選擇，請確認輸入價格與連結。", ProposalContentMode.DataError);
                                return;
                            }
                        }
                    }
                }
            }



            bool isPhotographers = Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers);
            bool isFTPPhoto = Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto);
            bool isSellerPhoto = Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto);
            bool isSystemPic = pro.SystemPic;

            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                //宅配
                if (!isPhotographers && !isFTPPhoto && !isSellerPhoto && !isSystemPic)
                {
                    View.ShowMessage("未填寫照片來源。", ProposalContentMode.DataError);
                    return;
                }

                //if (config.IsConsignment)
                //{
                //    if (pro.Consignment)
                //    {
                //        //這邊要再抓新的合約類型
                //        SellerContractFileCollection pcf = ProposalFacade.SellerContractFileGetByType(pro.SellerGuid, (int)SellerContractType.Curation);
                //        if (pcf.Count() == 0)
                //        {
                //            View.ShowMessage("商家尚未上傳【策展專案商品合約書】，請先確認合約是否已上傳後，再進行送商家覆核或送件QC。", ProposalContentMode.DataError);
                //            return;
                //        }

                //    }
                //}
            }

            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
            {
                if ((pro.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.None)
                {
                    View.ShowMessage("未選擇成套票券類型。", ProposalContentMode.DataError);
                    return;
                }

                if (!config.EnabledGroupCouponTypeB && (pro.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.CostAssign)
                {
                    View.ShowMessage("尚未開放B型，請重新選擇。", ProposalContentMode.DataError);
                    return;
                }
            }

            if ((int)pro.Status == (int)ProposalStatus.Initial && pro.SellerGuid != Guid.Empty && pro.VendorReceiptType == null)
            {
                View.ShowMessage("未填寫店家單據開立方式。", ProposalContentMode.DataError);
                return;
            }

            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleSend));

            ProposalMultiDealCollection multi = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
            if (multi != null && multi.Count > 0)
            {
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.GroupCoupon))
                {
                    foreach (var item in multi)
                    {
                        if (pro.GroupCouponDealType == (int)GroupCouponDealType.AvgAssign && item.ItemPrice % item.GroupCouponBuy > 0)
                        {
                            View.ShowMessage(string.Format("成套票券A型售價({0})必須能整除買數({1})", item.ItemPrice, item.GroupCouponBuy), ProposalContentMode.DataError);
                            return;
                        }

                        if (pro.GroupCouponDealType == (int)GroupCouponDealType.CostAssign && item.OrigPrice % (item.GroupCouponBuy + item.GroupCouponPresent) > 0)
                        {
                            View.ShowMessage(string.Format("成套票券B型原價({0})必須能整除組數({1})", item.OrigPrice, item.GroupCouponBuy + item.GroupCouponPresent), ProposalContentMode.DataError);
                            return;
                        }

                        if (pro.GroupCouponDealType == (int)GroupCouponDealType.CostAssign && (item.ItemPrice / item.GroupCouponBuy > item.OrigPrice / (item.GroupCouponBuy + item.GroupCouponPresent)))
                        {
                            View.ShowMessage("成套票券B型單張售價不可高於原價", ProposalContentMode.DataError);
                            return;
                        }
                    }
                }

                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    if (pro.PassSeller == 1)
                    {
                        //若走商家覆核流程的提案單，則在業務觸發【送件QC】後，以下程序全部同步自動打勾
                        //pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));     //提案稽核
                        pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));   //附約已回
                        pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractSend));    //合約寄出
                        pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractInspect));    //合約檢核

                        ProposalFacade.ProposalLog(View.ProposalId, "提案申請-系統勾選「附約已回」、「合約寄出」、「合約檢核」", View.UserName);
                    }
                }


                pro.ApplyFlag = (int)ProposalApplyFlag.Apply;
                pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                pro.ModifyId = View.UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                Seller s = sp.SellerGet(pro.SellerGuid);
                string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                if (emp.IsLoaded)
                {
                    #region 若為送件，則寄送通知信予 商審部或主管

                    List<string> mailToUserQC = new List<string>();
                    if (emp.DeptId == EmployeeChildDept.S010.ToString())
                    {
                        //宅配寄給商審
                        EmployeeCollection emps = HumanFacade.EmployeeGetListByDept(EmployeeDept.Q000);
                        if (emps != null)
                        {
                            foreach (Employee em in emps)
                            {
                                var _member = hp.ViewEmployeeGet("emp_id", em.EmpId);
                                if (RegExRules.CheckEmail(_member.Email))
                                {
                                    mailToUserQC.Add(_member.Email);//商審人員email
                                }
                            }
                        }
                    }
                    else
                    {
                        //搜尋上檔頻道
                        ProposalCategoryDealCollection pcd = sp.ProposalCategoryDealsGetList(pro.Id);
                        var piinlife = pcd.Where(x => x.Cid == 148);
                        var travel = pcd.Where(x => x.Cid == 90);

                        //品生活寄給品生活主管，旅遊寄給旅遊主管，其餘寄給該區域主管
                        string DeptId = "";
                        if (piinlife.Count() > 0)
                            DeptId = EmployeeChildDept.S012.ToString();
                        else if (travel.Count() > 0)
                            DeptId = EmployeeChildDept.S008.ToString();
                        else
                            DeptId = emp.DeptId;


                        var emps = hp.ViewEmployeeCollectionGetAll().Where(x => x.DeptManager != null && x.DeptManager != "" && x.CrossDeptTeam != null);
                        if (emps != null)
                        {
                            IEnumerable<ViewEmployee> manager = emps.Where(x => x.CrossDeptTeam.Contains(DeptId));
                            foreach (ViewEmployee m in manager)
                            {
                                if (RegExRules.CheckEmail(m.Email))
                                {
                                    mailToUserQC.Add(m.Email);//業務的主管email        
                                }
                            }
                        }
                    }

                    string subjectQC = "";
                    if (pro.SellerGuid == Guid.Empty)
                        subjectQC = string.Format("【{0}】 單號 NO.{1}", "提案諮詢申請", pro.Id);
                    else
                        subjectQC = string.Format("【{0}】 單號 NO.{1}", Helper.GetLocalizedEnum(ProposalApplyFlag.Apply), pro.Id);
                    ProposalFacade.SendEmail(mailToUserQC, subjectQC, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, emp.EmpName, url));

                    #endregion
                }

                ProposalFacade.ProposalLog(View.ProposalId, "提案申請", View.UserName);
                LoadData(View.ProposalId);
            }
            else
            {
                View.ShowMessage("至少需填寫一個檔次資訊。", ProposalContentMode.DataError);
            }
        }

        protected void OnProposalReturned(object sender, DataEventArgs<string> e)
        {
            Proposal pro = sp.ProposalGet(View.ProposalId);
            ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
            ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);

            #region 通知人員

            List<string> mailToUser = new List<string>();
            if (pro.EditFlag > 0)
            {
                // 檢核已到編輯檢核狀態時，寄送通知信給創編組長
                mailToUser.Add(config.SupportingLeaderEmail);
            }
            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Photographers))
            {
                // 若提案有標註為須拍攝，寄送通知信給攝影組
                List<string> photoCheck = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.PhotographerCheck);
                mailToUser.AddRange(photoCheck);
            }

            // 業務
            if (opSalesEmp.IsLoaded)
                mailToUser.Add(opSalesEmp.Email);
            if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                mailToUser.Add(deSalesEmp.Email);

            //業務主管
            var emps = hp.ViewEmployeeCollectionGetAll().Where(x => x.DeptManager != null && x.DeptManager != "" && x.CrossDeptTeam != null);
            if (emps != null)
            {
                if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                {
                    IEnumerable<ViewEmployee> deManager = emps.Where(x => x.CrossDeptTeam.Contains(deSalesEmp.DeptId));
                    foreach (ViewEmployee m in deManager)
                    {
                        if (RegExRules.CheckEmail(m.Email))
                        {
                            mailToUser.Add(m.Email);//業務的主管email        
                        }
                    }
                }

                if (opSalesEmp.IsLoaded)
                {
                    IEnumerable<ViewEmployee> opManager = emps.Where(x => x.CrossDeptTeam.Contains(opSalesEmp.DeptId));
                    foreach (ViewEmployee m in opManager)
                    {
                        if (RegExRules.CheckEmail(m.Email))
                        {
                            mailToUser.Add(m.Email);//業務的主管email        
                        }
                    }
                }

            }


            //創編設定組負責人(取指派紀錄)
            ProposalLogCollection assign = sp.ProposalLogGetList(pro.Id, ProposalLogType.Assign);
            var EditorAssign = assign.GroupBy(x => new { ChangeLog = x.ChangeLog.Substring(0, 8), x.CreateId }).Select(x => new { ChangeLog = x.Key.ChangeLog.Substring(0, 8), x.Key.CreateId, CreateTime = x.Max(b => b.CreateTime) }).ToList();
            foreach (var ea in EditorAssign)
                mailToUser.Add(ea.CreateId);

            ProposalAssignLogCollection palc = sp.ProposalAssignLogGetList(View.ProposalId);
            foreach (var pal in palc)
                mailToUser.Add(hp.ViewEmployeeGet(ViewEmployee.Columns.EmpName, pal.AssignEmail).Email);


            // 排檔人員
            List<string> schedule = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.ScheduleCheck);
            mailToUser.AddRange(schedule);

            //創意主管群組
            List<string> pm = HumanFacade.GetUsesEmailInRole(MemberRoles.ProductionManager.ToString());
            mailToUser.AddRange(pm);

            if (deSalesEmp.DeptId == EmployeeChildDept.S010.ToString() || hp.DepartmentGet(deSalesEmp.DeptId).ParentDeptId == EmployeeDept.M000.ToString())
            {
                //營運分析部-宅配企服群組
                mailToUser.Add(config.CodShipEmail);
            }


            //開發業務
            string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
            if (!mailToUser.Contains(sales))
                mailToUser.Add(sales);

            //經營業務
            sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
            if (!mailToUser.Contains(sales))
                mailToUser.Add(sales);



            #endregion

            #region 清除flag (僅保留建檔確認、財務確認及編輯檢核非檔次設定)

            pro.ApplyFlag = pro.ApproveFlag = pro.BusinessFlag = pro.BusinessCreateFlag = 0;
            pro.EditFlag = Convert.ToInt32(Helper.SetFlag(false, pro.EditFlag, ProposalEditorFlag.Setting));
            pro.EditPassFlag = Convert.ToInt32(Helper.SetFlag(false, pro.EditPassFlag, ProposalEditorFlag.Setting));

            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleSend));
            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));

            if (!Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.FinanceCheck) && !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck))
                pro.ListingFlag = (int)ProposalListingFlag.Initial;
            else if (Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.FinanceCheck) && !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck))
                pro.ListingFlag = (int)ProposalListingFlag.FinanceCheck;
            else if (!Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.FinanceCheck) && Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck))
                pro.ListingFlag = (int)ProposalListingFlag.PaperContractCheck;
            else if (Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.FinanceCheck) && Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck))
                pro.ListingFlag = (int)ProposalListingFlag.FinanceCheck + (int)ProposalListingFlag.PaperContractCheck;



            if (pro.BusinessHourGuid != null)
            {
                pro.BusinessCreateFlag = (int)ProposalBusinessCreateFlag.Created;
                pro.OrderTimeS = null;
            }

            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (pro.PassSeller == 1)
                {
                    //若走商家覆核流程的提案單，則在業務觸發【送件QC】後，以下程序全部同步自動打勾
                    //pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(false, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));     //提案稽核
                    pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));   //附約已回
                    pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractSend));    //合約寄出
                    pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractInspect));    //合約檢核

                    ProposalFacade.ProposalLog(View.ProposalId, "提案申請-系統取消勾選「附約已回」、「合約寄出」、「合約檢核」", View.UserName);
                }
            }

            #endregion

            #region 清除原預計上檔日

            if (pro.BusinessHourGuid.HasValue)
            {
                //母子檔要同步更新
                ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(pro.BusinessHourGuid.Value);

                if (vcdc.Count > 0)
                {
                    foreach (ViewComboDeal vcd in vcdc)
                    {
                        BusinessHour bh = sp.BusinessHourGet(vcd.BusinessHourGuid);
                        bh.BusinessHourOrderTimeS = bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                        bh.BusinessHourDeliverTimeS = bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                        sp.BusinessHourSet(bh);

                        pp.DealTimeSlotDeleteList(bh.Guid);

                    }
                }
                else
                {
                    BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                    bh.BusinessHourOrderTimeS = bh.BusinessHourOrderTimeE = DateTime.MaxValue.Date;
                    bh.BusinessHourDeliverTimeS = bh.BusinessHourDeliverTimeE = DateTime.MaxValue.Date;
                    sp.BusinessHourSet(bh);

                    pp.DealTimeSlotDeleteList(bh.Guid);
                }

            }
            #endregion

            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                {
                    pro.Status = (int)ProposalStatus.SellerCreate;
                }
                else
                {
                    pro.Status = (int)ProposalStatus.Initial;
                }
            }
            else
            {
                pro.Status = (int)ProposalStatus.Initial;
            }

            pro.ModifyId = View.UserName;
            pro.ModifyTime = DateTime.Now;
            sp.ProposalSet(pro);

            string subject = "";
            if (pro.SellerGuid == Guid.Empty)
                subject = string.Format("【提案諮詢退件】 單號 NO.{0}", View.ProposalId);
            else
                subject = string.Format("【退件】 單號 NO.{0}", View.ProposalId);


            Seller s = sp.SellerGet(pro.SellerGuid);
            string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
            ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(View.ProposalId, s.SellerName, deSalesEmp.EmpName, url, e.Data));



            string changeLog = "【退件】 " + e.Data + "|退件類型：" + string.Join(" / ", View.ReturnReason.Values);

            #region 退件因素細項
            List<ProposalReturnDetail> detailList = new List<ProposalReturnDetail>();

            if (View.ReturnReason.ContainsKey((int)ProposalReturnPpon.DataModify))
            {
                if (!string.IsNullOrWhiteSpace(View.ReturnDetailDataModifyValue) && View.ReturnDetailDataModifyValue != "[]")
                {
                    changeLog += "|" + View.ReturnDetailDataModifyTxt;
                    detailList.Add(new ProposalReturnDetail()
                    {
                        ReturnReasonBinary = (int)ProposalReturnPpon.DataModify,
                        SystemCodeGroup = SystemCodeGroup.ProposalReturnDetailDataModifyOfPpon.ToString(),
                        Detail = View.ReturnDetailDataModifyValue
                    });
                }
            }
            if (View.ReturnReason.ContainsKey((int)ProposalReturnHouse.DataModify))
            {
                if (!string.IsNullOrWhiteSpace(View.ReturnDetailDataModifyValue) && View.ReturnDetailDataModifyValue != "[]")
                {
                    changeLog += "|" + View.ReturnDetailDataModifyTxt;
                    detailList.Add(new ProposalReturnDetail()
                    {
                        ReturnReasonBinary = (int)ProposalReturnHouse.DataModify,
                        SystemCodeGroup = SystemCodeGroup.ProposaReturnDetailDataModifyOfDelivery.ToString(),
                        Detail = View.ReturnDetailDataModifyValue
                    });
                }
            }
            //←由此擴充
            #endregion

            ProposalLog log = ProposalLogSaveThenSelect(View.ProposalId, changeLog);
            foreach (var detail in detailList)
            {
                detail.ProposalId = View.ProposalId;
                detail.ProposalLogId = log.Id;
                SaveProposalReturnDetail(detail);
            }

            LoadData(View.ProposalId);
        }

        protected void OnAssign(object sender, DataEventArgs<KeyValuePair<int, List<string>>> e)
        {
            List<string> AssignFlagList = e.Data.Value;
            ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(e.Data.Key);
            Proposal pro = sp.ProposalGet(View.ProposalId);

            if (!string.IsNullOrEmpty(AssignFlagList[0]))
            {
                for (int i = 0; i < AssignFlagList.Count(); i++)
                {
                    #region 若為指派，則寄送通知信予 被指派人員
                    string flag = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ProposalEditorFlag)Convert.ToInt32(AssignFlagList[i]));
                    string log = string.Format("指派 {0} 為 {1}", flag, emp.EmpName);
                    ProposalFacade.ProposalLog(View.ProposalId, log, View.UserName, ProposalLogType.Assign);
                    string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, View.ProposalId);
                    string subject = string.Format("【指派】 單號 NO.{0} <{1}>", View.ProposalId, flag);
                    List<string> mailToUser = new List<string>() { emp.Email };

                    ProposalAssignLog palc = sp.ProposalAssignLogGetList(View.ProposalId).Where(x => x.AssignFlag == Convert.ToInt32(AssignFlagList[i])).FirstOrDefault();
                    if (palc != null)
                    {
                        //Update
                        palc.AssignEmail = emp.EmpName;
                        palc.CreateId = View.UserName;
                        palc.CreateTime = DateTime.Now;
                        sp.ProposalAssignLogSet(palc);
                    }
                    else
                    {
                        //Create
                        ProposalAssignLog paf = new ProposalAssignLog()
                        {
                            Pid = View.ProposalId,
                            AssignFlag = Convert.ToInt32(AssignFlagList[i]),
                            AssignEmail = emp.EmpName,
                            CreateId = View.UserName,
                            CreateTime = DateTime.Now
                        };
                        sp.ProposalAssignLogSet(paf);
                    }

                    Seller s = sp.SellerGet(pro.SellerGuid);
                    ViewEmployee sale = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                    ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, emp.EmpName, url));


                    #endregion
                }

                View.RedirectProposal(pro.Id, "#assign");
            }
            else
                View.ShowMessage("至少需勾選一個項目。", ProposalContentMode.DataError);

        }

        protected void OnChangeLog(object sender, DataEventArgs<string> e)
        {
            ProposalFacade.ProposalLog(View.ProposalId, e.Data, View.UserName);
            LoadData(View.ProposalId);
        }

        protected void OnCheckFlag(object sender, DataEventArgs<string> e)
        {
            int value = int.TryParse(e.Data.Split('|')[0], out value) ? value : 0;
            string col = e.Data.Split('|')[1];
            string text = e.Data.Split('|')[2];
            bool hasFlag = bool.TryParse(e.Data.Split('|')[3], out hasFlag) ? hasFlag : false;
            Proposal pro = sp.ProposalGet(View.ProposalId);
            if (pro.IsLoaded && value != 0)
            {
                #region 尚未通過建檔檢核，無法進行提案稽核

                if (text == Helper.GetLocalizedEnum((ProposalBusinessFlag.ProposalAudit)) && !Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit)
                    && pro.Status < (int)ProposalStatus.Created)
                {
                    View.ShowMessage("提案尚未通過建檔檢核，無法進行提案稽核。", ProposalContentMode.DataError);
                    return;
                }
                if (text == Helper.GetLocalizedEnum((ProposalApproveFlag.QCcheck)) && !Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                {
                    View.ShowMessage("很抱歉！提案已被業務人員抽回，無法進行QC作業。", ProposalContentMode.DataError);
                    return;
                }

                #endregion

                if (hasFlag)
                {
                    pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) ^ value);
                }
                else
                {
                    pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) | value);
                }
                pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                pro.ModifyId = View.UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);
                Seller s = sp.SellerGet(pro.SellerGuid);

                #region 若為通過QC確認，則寄送通知信予 業務、業助群組及宅配事業處-商品開發部

                if (text == Helper.GetLocalizedEnum((ProposalApproveFlag.QCcheck)) && Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    List<string> mailToUser = new List<string>();

                    // 經營業務
                    if (opSalesEmp.IsLoaded)
                        mailToUser.Add(opSalesEmp.Email);

                    if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                        // 開發業務
                        mailToUser.Add(deSalesEmp.Email);


                    //開發業務
                    string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                    if (!mailToUser.Contains(sales))
                        mailToUser.Add(sales);

                    //經營業務
                    sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                    if (!mailToUser.Contains(sales))
                        mailToUser.Add(sales);


                    string subject = string.Format("【{0}】 單號 NO.{1}", text, pro.Id);
                    string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                    ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));


                }

                #endregion

                #region 若為通過攝影確認，則寄送通知信予 攝影確認權限人員

                if (text == Helper.GetLocalizedEnum((ProposalBusinessFlag.PhotographerCheck)) && Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.PhotographerCheck))
                {
                    List<string> mailToUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.Assign);

                    string subject = string.Format("【{0}】 單號 NO.{1}", text, pro.Id);
                    string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                    ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));

                }

                #endregion

                if (pro.BusinessHourGuid.HasValue)
                {
                    #region 若註記為上架確認，則寄送通知信予 業助群組

                    if (text == Helper.GetLocalizedEnum((ProposalEditorFlag.Listing)) && Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.Listing))
                    {
                        List<string> mailToUser = new List<string>();
                        string DeliverType = "";
                        if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString() && hp.DepartmentGet(deSalesEmp.DeptId).ParentDeptId != EmployeeDept.M000.ToString())
                        {
                            DeliverType = "好康";
                        }
                        else
                        {
                            DeliverType = "宅配";
                        }

                        //開發業務
                        string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                        if (!mailToUser.Contains(sales))
                            mailToUser.Add(sales);

                        //經營業務
                        sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                        if (!mailToUser.Contains(sales))
                            mailToUser.Add(sales);


                        if (mailToUser.Count > 0)
                        {
                            string subject = string.Format("【{0}】 單號 NO.{1}【{2}】<{3}>", text, pro.Id, DeliverType, deSalesEmp.EmpName);
                            string url = config.SiteUrl + @"/ppon/default.aspx?bid=" + pro.BusinessHourGuid;
                            ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));


                        }

                        #region 發送新版信件
                        if (pro.IsEarlierPageCheck && pro.OrderTimeS != null)
                        {

                            bool success = false;
                            string newSubject = string.Empty;
                            string newSender = config.SystemEmail;
                            string mailContent = string.Empty;
                            string itemName = string.Empty;
                            string RemittanceType = string.Empty;
                            bool isToShop = false;
                            string preWorkDay = string.Empty;

                            //抓取資料檔
                            ViewProposalSeller vps = sp.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, pro.BusinessHourGuid.Value);     

                            #region 檢查preWorkDay是否為假日，是的話再往前延一天                
                            int minDay = -1;
                            int preDay = (int)vps.OrderTimeS.Value.AddDays(minDay).DayOfWeek;
                            DateTime preWork = new DateTime();
                            if (preDay == 0)
                            {
                                minDay = minDay - 2;
                            }
                            else if (preDay == 6)
                            {
                                minDay = minDay - 1;
                            }
                            preWork = vps.OrderTimeS.Value.AddDays(minDay);

                            //檢查preWorkDay是否有在放假條件內
                            while (pp.GetVacationByDate(preWork).IsLoaded)
                            {
                                preWork = preWork.AddDays(-1);
                            }

                            preWorkDay = string.Format("{0:yyyy\\/MM\\/dd}", preWork);
                            #endregion

                            //多檔次取主檔資料
                            Item item = pp.ItemGetBySubDealBid(pro.BusinessHourGuid.Value);
                            if (item.IsLoaded)
                            {
                                itemName = item.ItemName;
                            }
                            else
                            {
                                Item mainItem = pp.ItemGetByBid(pro.BusinessHourGuid.Value);
                                if (mainItem.IsLoaded)
                                {
                                    itemName = mainItem.ItemName;
                                }
                            }
                            //出帳方式
                            DealAccounting dAccounting = pp.DealAccountingGet(pro.BusinessHourGuid.Value);
                            if (dAccounting.IsLoaded)
                            {
                                RemittanceType = WayOut((RemittanceType)dAccounting.RemittanceType);
                            }


                            if (pro.DeliveryType == (int)DeliveryType.ToShop)
                            {
                                List<LunchKingSite.BizLogic.Facade.SellerAccountModel> sellerModel = PponFacade.GetSellerAccount(pro.BusinessHourGuid.Value);

                                isToShop = true;
                                newSubject = string.Format("【17Life上檔通知】{0}上檔【{1}】頁面確認、核銷帳密信", string.Format("{0:yyyy\\/MM\\/dd}", vps.OrderTimeS.Value), itemName);
                                mailContent = "您好：<br />17Life很榮幸且珍惜能與您有此次的合作機會，創意專員已針對商品特性及賣點，製作完成專屬於您的銷售頁面。<br />";
                                mailContent = mailContent + "獨家頁面即將於<font color='red'><b>" + string.Format("{0:yyyy\\/MM\\/dd HH:mm}", vps.OrderTimeS.Value) + "</b></font>上檔，我們希望於<font color='red'><b>" + preWorkDay + " 14:00</b></font>前向您完成確認工作，已呈現最完美的頁面。<br />";
                                mailContent = mailContent + "敬請確認以下四個部分，<font color='red'><b>標題、價格、好康權益說明、店家資訊</b></font>，是否與合約內容相符。<br />";
                                mailContent = mailContent + "<font color='red'><b>若資訊有任何問題，請來信聯繫業務/業助窗口，未反應則視為確認完成，感謝您的配合，預祝 銷售長紅！</b></font><br />";
                                mailContent = mailContent + "前台頁面：<a href='" + config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "' target='_blank'>" + config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "</a><br />";
                                mailContent = mailContent + "隨信附上核銷的網址、帳號、系統使用說明手冊及核銷紀錄表格，<br />";
                                mailContent = mailContent + "若臨時出現無法核銷、系統當機...等無法立即解決的問題，<br />";
                                mailContent = mailContent + "請務必紀錄客人的憑證編號、確認碼、姓名、聯絡電話等，以利後續查詢。<br /><br />";
                                mailContent = mailContent + "商家系統網址：<a href='" + config.SSLSiteUrl + "/vbs' target='_blank'>" + config.SSLSiteUrl + "/vbs</a><br /><br />";
                                mailContent = mailContent + "登入帳號：依原帳號密碼<br />";
                                mailContent = mailContent + "初始密碼：123456(限首次合作店家，若已有變更密碼者請延用舊密碼)<br /><br />";
                                mailContent = mailContent + "<table style='border-collapse:collapse;'><tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>商家名稱</td><td style='width:150px;border:1px solid black;'>商家預設帳號</td></tr>";
                                foreach (var seller in sellerModel)
                                {
                                    mailContent = mailContent + "<tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>" + seller.sellerName + "</td><td style='width:150px;border:1px solid black;'>" + seller.accountId + "</td></tr>";
                                }
                                mailContent = mailContent + "</table>";
                                mailContent = mailContent + "提醒您！請務必即時核銷，兌換逾期/以退貨恕無法核銷，若造成損失由店家承擔！<br />";
                                mailContent = mailContent + "出帳方式：" + RemittanceType + "，並於對帳單產出後將發票開立寄回以免影響撥款。";
                            }
                            //宅配
                            else
                            {
                                newSubject = string.Format("【17Life合作上檔通知(頁面)】{0}上檔【{1}】", string.Format("{0:MM/dd}", vps.OrderTimeS.Value), itemName);
                                mailContent = "您好：<br />17Life很榮幸且珍惜能與您有此次的合作機會，創意專員已針對商品特性及賣點，製作完成專屬於您的銷售頁面。<br />";
                                mailContent = mailContent + "獨家頁面將於<font color='red'><b>" + string.Format("{0:yyyy\\/MM\\/dd HH:mm}", vps.OrderTimeS.Value) + "</b></font>上檔，我們希望<font color='red'><b>" + preWorkDay + " 14:00</b></font>前向您完成確認工作，以呈現最完美的頁面。<br />";
                                mailContent = mailContent + "前台頁面：<a href='" + config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "' target='_blank'>" + config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "</a><br />";
                                mailContent = mailContent + "若有任何問題請於<font color='red'><b>" + preWorkDay + " 14:00</b></font>前來信聯繫業務/業助窗口，為反應則視為確認完成。<br />";
                                mailContent = mailContent + "感謝您的配合，預祝 銷售長紅！<br />";
                                mailContent = mailContent + "提醒您！檔次上檔販售後請留意每日訂單相關資訊，並協助『準時出貨』。<br />";
                                mailContent = mailContent + "商家系統網址：<a href='" + config.SSLSiteUrl + "/vbs' target='_blank'>" + config.SSLSiteUrl + "/vbs</a><br /><br />";
                            }

                            success = EmailFacade.SendSetupPageMail(newSender, newSubject, mailContent, vps, isToShop, deSalesEmp, opSalesEmp);
                        }
                        #endregion

                    }

                    #endregion
                }

                ProposalFacade.ProposalLog(View.ProposalId, (hasFlag ? "取消-" : string.Empty) + text, View.UserName);
                LoadData(View.ProposalId);
            }


        }



        #endregion

        #region Private Method

        private string WayOut(RemittanceType type)
        {
            string result = string.Empty;
            switch (type)
            {
                case RemittanceType.Others:
                    result = "其他付款方式";
                    break;
                case RemittanceType.AchWeekly:
                    result = "ACH每週出帳";
                    break;
                case RemittanceType.AchMonthly:
                    result = "ACH每月出帳";
                    break;
                case RemittanceType.ManualPartially:
                    result = "商品(暫付70%)";
                    break;
                case RemittanceType.ManualWeekly:
                    result = "人工每週出帳";
                    break;
                case RemittanceType.ManualMonthly:
                    result = "人工每月出帳";
                    break;
                case RemittanceType.Flexible:
                    result = "彈性選擇出帳";
                    break;
                case RemittanceType.Weekly:
                    result = "新每週出帳";
                    break;
                case RemittanceType.Monthly:
                    result = "新每月出帳";
                    break;
                default:
                    result = "其他付款方式";
                    break;
            }

            return result;
        }
        private void LoadData(int id)
        {
            ViewProposalSeller pro = sp.ViewProposalSellerGet(ViewProposalSeller.Columns.Id, id);
            if (pro.IsLoaded)
            {
                List<int> logTypes = new List<int>()
                {
                    (int)ProposalLogType.Initial,
                    (int)ProposalLogType.Seller
                };

                ProposalLogCollection logs = sp.ProposalLogGetList(pro.Id, logTypes);
                ProposalLogCollection assignLogs = sp.ProposalLogGetList(pro.Id, ProposalLogType.Assign);
                Seller seller = sp.SellerGet(pro.SellerGuid);
                ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(id);
                SalesBusinessModel model = new SalesBusinessModel();
                if (pro.BusinessHourGuid == null)
                {
                    model = null;
                }
                else
                {
                    BusinessHour b = pp.BusinessHourGet(pro.BusinessHourGuid ?? Guid.Empty);
                    model.Business = b;
                    DealAccounting d = pp.DealAccountingGet(pro.BusinessHourGuid ?? Guid.Empty);
                    model.Accounting = d;
                    GroupOrder g = op.GroupOrderGetByBid(pro.BusinessHourGuid ?? Guid.Empty);
                    model.Group = g;
                }

                ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(id);

                //合約上傳
                ProposalContractFileCollection pcfc = sp.ProposalContractFileGetListBypid(id);

                ProposalCategoryDealCollection pcdc = sp.ProposalCategoryDealsGetList(id);

                //整棵tree
                SellerCollection tcsc = new SellerCollection();
                Guid root = SellerFacade.GetRootSellerGuid(pro.SellerGuid);
                var AllChrildren = SellerFacade.GetChildrenSellerGuid(root);
                tcsc.Add(sp.SellerGet(root));
                foreach (var children in AllChrildren)
                    tcsc.Add(sp.SellerGet(children));

                //檔案上傳
                ProposalSellerFileCollection sellerFiles = sp.ProposalSellerFileListGet(pro.Id);


                List<ShoppingCartFreight> scfs = sp.ShoppingCartFreightsGetBySellerGuid(
                                                            new List<Guid>() { pro.SellerGuid })
                                                            .Where(x => x.FreightsStatus == (int)ShoppingCartFreightStatus.Approve).ToList();

                View.SetProposalContent(model, pro, logs, assignLogs, tcsc, psc, pcec, pcfc, pcdc, seller, sellerFiles, scfs);
            }
            else
            {
                View.ShowMessage("查無提案單資料。", ProposalContentMode.ProposalNotFound);
            }

        }

        private ProposalLog ProposalLogSaveThenSelect(int id, string changeLog, ProposalLogType type = ProposalLogType.Initial)
        {
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
            ProposalLog log = new ProposalLog();
            log.ProposalId = id;
            log.ChangeLog = changeLog;
            log.Type = (int)type;
            log.Dept = emp.DeptName;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            return sp.ProposalLogSaveThenSelect(log);
        }
        private void SaveProposalReturnDetail(ProposalReturnDetail returnDetail)
        {
            sp.ProposalReturnDetailSave(returnDetail);
        }


        #endregion
    }
}

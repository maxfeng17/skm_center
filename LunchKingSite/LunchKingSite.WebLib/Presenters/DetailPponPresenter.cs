﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.UI;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.WebLib.Component;
using log4net;
using LunchKingSite.BizLogic.Models;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{

    public class DetailPponPresenter : Presenter<IDetailPponView>
    {
        private ICmsProvider cp;
        private IMemberProvider mp;
        private IOrderProvider op;
        private IEventProvider ep;
        private ICmsProvider cmp;
        private ISellerProvider sp;
        private ILog logger;
        private ISysConfProvider config;

        private int _city;
        private int _categoryId;

        public DetailPponPresenter()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            logger = LogManager.GetLogger(typeof(DetailPponPresenter).Name);
            config = ProviderFactory.Instance().GetConfig();
        }


        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (config.StarRatingEnabled)
            {
                PponDealEvaluateStar EvaAvg = ViewPponDealManager.DefaultManager.GetPponDealEvaluateStarList(View.BusinessHourId);
                View.EvaluateAverage = EvaAvg;
            }
 
            IViewPponDeal mainDeal;
            if (View.BusinessHourId == Guid.Empty)
            {
                mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(View.BusinessHourUniqueId);
                View.BusinessHourId = mainDeal.BusinessHourGuid;
                View.BusinessHourUniqueId = mainDeal.UniqueId ?? -1;
            }
            else
            {
                if (View.ForceReload)
                {
                    mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourId, true, true);
                }
                else
                {
                    mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourId, true, false);
                }
                View.BusinessHourUniqueId = mainDeal.UniqueId ?? -1;
                View.BusinessHourId = mainDeal.BusinessHourGuid;
            }

            if ((mainDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                View.RedirectToComboDealMain(mainDeal.MainBid, mainDeal.BusinessHourGuid);
                return false;
            }

            //避免輸入錯誤的bid進入錯誤頁面，錯誤的bid則導回首頁
            if ((mainDeal.BusinessHourGuid == Guid.Empty && View.BusinessHourUniqueId == -1) || (mainDeal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                View.BusinessHourId = Guid.Empty;
                View.RedirectToDefault();
                return false;
            }

            if (Helper.IsFlagSet(mainDeal.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
            {
                View.BusinessHourId = Guid.Empty;
                View.RedirectToDefault();
                return false;
            }

            if (GameFacade.HasGameDealViewPermission(View.UserId, mainDeal.BusinessHourGuid))
            {
                View.RedirectToGameDetail(mainDeal.BusinessHourGuid);
                return false;
            }

            if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
            {
                if (HttpContext.Current.Request["force"] != "open" + DateTime.Today.ToString("yyyyMMdd"))
                {
                    IViewPponDeal similarDeal = PponFacade.GetSimilarDeal(mainDeal);
                    string expireRedirectUrl = string.Empty;
                    bool useExpireRedirectUrlAsCanonical = (similarDeal != null);
                    expireRedirectUrl = PponFacade.ExpireRedirectUrl(similarDeal, mainDeal.PicAlt);

                    View.BusinessHourId = Guid.Empty;
                    View.RedirectNoShowToDefault(expireRedirectUrl);
                    //View.RedirectToDefault();
                    return false;
                }
            }

            View.isMultipleMainDeals = false;
            View.BypassSsoVerify = config.BypassSsoVerify;
            View.EdmPopUpCacheName = config.EdmPopUpCacheName;

            MemberLinkCollection mlc = new MemberLinkCollection();
            _city = View.DealCityId = View.CityId;
            _categoryId = View.CategoryID;
            var workCityId = _city == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                                               : _city;

            //依照city, category的Id撈取對應的檔次列表
            List<MultipleMainDealPreview> totalMultiDeals = SetMultipleDeals();
            var multideals = totalMultiDeals;
            if (_categoryId != 0)
            {
                multideals = totalMultiDeals.Where(x => x.DealCategoryIdList.Contains(_categoryId)).ToList();
            }

            #region ●今日熱銷
            int hotSaleBigTopSaleCount = 20;
            var hotDeals = multideals.OrderByDescending(x => x.PponDeal.ItemPrice * x.PponDeal.OrderedQuantity)
                                     .Take(multideals.Count > hotSaleBigTopSaleCount ? hotSaleBigTopSaleCount : multideals.Count)
                                     .ToList();
            View.SetHotSaleMutilpMainDeals(hotDeals);
            #endregion

            #region ●最新上檔
            var todayDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeS < DateTime.Now).OrderBy(x => DateTime.Now - x.PponDeal.BusinessHourOrderTimeS).ToList();
            todayDeals = todayDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetToDayMutilpMainDeals(todayDeals);
            #endregion

            #region ●最後倒數
            var lastdaydDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeE > DateTime.Now).OrderBy(x => x.PponDeal.BusinessHourOrderTimeE - DateTime.Now).ToList();
            lastdaydDeals = lastdaydDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetLastDayMutilpMainDeals(lastdaydDeals);
            #endregion

            RecentlyViewedDealsmMnager.Add(mainDeal);

            #region Retargeting

            try
            {
                View.DealArgs = RetargetingUtility.GetDealArgs(mainDeal, View.UserId);
            }
            catch (Exception ex)
            {
                logger.InfoFormat("Retargeting Exception -> {0}. {1}", ex.Message, ex.StackTrace);
            }

            #endregion Retargeting

            // 若為品生活檔次，轉址為品生活頁面
            var dealCoategoryIdList = ViewPponDealManager.DefaultManager.GetCategoryIds(mainDeal.BusinessHourGuid);
            if (dealCoategoryIdList.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
            {
                View.RedirectToPiinlife();
            }

            View.DealType = mainDeal.DealType.GetValueOrDefault();

            if ((mainDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                var combodeals =
                    ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(mainDeal.BusinessHourGuid);
                View.SetProductEntries(combodeals, mainDeal);
            }
            else
            {
                //暫存資料正確性與JOB執行周期有關,自行重查銷售數量
                mainDeal.OrderedQuantity = op.OrderDetailGetOrderedQuantityByBid(mainDeal.BusinessHourGuid,
                    mainDeal.BusinessHourOrderTimeS >= config.StopRefundQuantityCalculateDate);
                View.SetProductEntries(null, mainDeal);
            }

            SetMain(mainDeal, workCityId, mlc);

            var ttmodel = new TrackTagInputModel
            {
                ViewPponDealData = mainDeal,
                Config = config,
                IsPiinLifeDeal = false,
                GtagPage = GtagPageType.Product,
                YahooEa = YahooEaType.ViewProduct,
                ScupioEventCategory = ScupioEventCategory.PageView,
                ScupioPageType = ScupioPageType.ProductPage,
            };

            View.MicroDataJson = TagManager.CreateFatory(ttmodel, TagProvider.MicroData).GetJson();
            View.ScupioDataJson = TagManager.CreateFatory(ttmodel, TagProvider.Scupio).GetJson();
            View.GtagDataJson = TagManager.CreateFatory(ttmodel, TagProvider.GoogleGtag).GetJson();
            View.YahooDataJson = TagManager.CreateFatory(ttmodel, TagProvider.Yahoo).GetJson();


            var ttCartPageModel = new TrackTagInputModel
            {
                ViewPponDealData = mainDeal,
                Config = config,
                IsPiinLifeDeal = false,
                GtagPage = GtagPageType.Product,
                YahooEa = YahooEaType.ViewProduct,
                ScupioEventCategory = ScupioEventCategory.PageView,
                ScupioPageType = ScupioPageType.AddToCart,
            };
            View.ScupioCartPageDataJson = TagManager.CreateFatory(ttCartPageModel, TagProvider.Scupio).GetJson();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckEventMail += OnCheckEventMail;
            return true;
        }

        #region Event
        protected void OnCheckEventMail(object sender, EventArgs e)
        {
            EventActivity activity = ep.EventActivityEdmGetList(-1)
                .FirstOrDefault(x => x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now);
            if (activity != null)
            {
                CmsRandomCityCollection cities = cmp.CmsRandomCityGetByPid(activity.Id, RandomCmsType.PopUpCities);
                KeyValuePair<EventActivity, CmsRandomCityCollection> pair = new KeyValuePair<EventActivity, CmsRandomCityCollection>(activity, cities);
                View.SetEventMainActivities(pair);
            }
            else
            {
                View.ShowEventEmail = false;
            }
        }
        #endregion

        private bool CheckIfNotShowDealDetailOnWeb(Guid bid)
        {            
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (deal == null || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
            {
                return true;
            }
            return false;
        }

        private List<MultipleMainDealPreview> SetMultipleDeals()
        {
            List<MultipleMainDealPreview> multideals = new List<MultipleMainDealPreview>();
            //區域分類
            int cityId = View.SelectedCityId;
            int channelId = View.SelectedChannelId;
            int areaId = View.SelectedRegionId;

            multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(channelId, areaId, new List<int>());

            return multideals;
        }

        public void SetMain(IViewPponDeal mainDeal, int city, MemberLinkCollection mlc)
        {
            List<IDealTimeSlot> dtsc = ViewPponDealManager.DefaultManager.DealTimeSlotGetByBidAndTime(mainDeal.BusinessHourGuid, DateTime.Today.AddHours(12));

            if (dtsc.OrderBy(x => x.CityId).Any(x => x.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                || mainDeal.BusinessHourOrderTimeS > DateTime.Now)
            {
                View.NoRobots();
            }

            View.SeoSetting(mainDeal);

            ViewPponStoreCollection pponStores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(mainDeal.BusinessHourGuid, VbsRightFlag.VerifyShop);
            View.ExpireNotice.ControlMode = ExpireNoticeMode.WebPpon;
            View.ExpireNotice.SellerName = mainDeal.SellerName;
            View.ExpireNotice.SellerCloseDownDate = mainDeal.CloseDownDate;
            View.ExpireNotice.StoreCloseDownDates = pponStores.Where(store => store.CloseDownDate.HasValue).
                ToDictionary(store => store.StoreName, store => store.CloseDownDate.Value);
            View.ExpireNotice.StoreChangedExpireDates = pponStores.Where(store => store.ChangedExpireDate.HasValue).
                ToDictionary(store => store.StoreName, store => store.ChangedExpireDate.Value);
            View.ExpireNotice.DealOriginalExpireDate = mainDeal.BusinessHourDeliverTimeE.HasValue ? mainDeal.BusinessHourDeliverTimeE.Value : DateTime.MaxValue;
            View.ExpireNotice.DealChangedExpireDate = mainDeal.ChangedExpireDate;

            if (View.UserId != 0)
            {
                mlc = mp.MemberLinkGetList(View.UserId);
            }

            SetupDeals(mainDeal, mlc);

            // ATM icon
            if (config.TurnATMOn && mainDeal.BusinessHourAtmMaximum > 0 && (DateTime.Now.AddHours(24) < mainDeal.BusinessHourOrderTimeE))
            {
                View.ShowATMIcon();
            }

            if (View.HamiTicketId != null)
            {
                View.SetHami();
            }

            // 設定標籤區域
            List<string> cityList = new List<string>();
            if (mainDeal.CityList != null)
            {
                cityList = new JsonSerializer().Deserialize<List<string>>(mainDeal.CityList);
            }

            PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityForPponTag(cityList, View.CityId);
            View.DealCityId = PponCityGroup.DefaultPponCityGroup.GetPponCityForDealChannel(cityList, View.CityId).CityId;

            View.Model.CityName = ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(
                pponcity, mainDeal);            

            //優惠內容
            //if ((mainDeal.IsCloseMenu ?? true) == false)
            //{
            //    View.MenuVisible = true;
            //    View.MenuImage = config.MediaBaseUrl + mainDeal.SellerId + "/" + View.BusinessHourId + "_Menu.png";
            //}

            View.IsDeliveryDeal = (mainDeal.DeliveryType == (int)DeliveryType.ToHouse);
            //結檔後，轉檔網址
            View.ExpireRedirectDisplay = Helper.GetEnumDescription((ExpireRedirectDisplay)(mainDeal.ExpireRedirectDisplay?? 1));
            string expireRedirectUrl = mainDeal.ExpireRedirectUrl;

            expireRedirectUrl = PponFacade.GetClosedDealExpireRedirectUrl(mainDeal, expireRedirectUrl);

            View.ExpireRedirectUrl = expireRedirectUrl;
        }

        protected void SetupDeals(IViewPponDeal mainDeal, MemberLinkCollection memberLinks)
        {
            Proposal pro = sp.ProposalGet(mainDeal.BusinessHourGuid);
            Dictionary<Guid, string> relatedDeal = new Dictionary<Guid, string>();
            if (pro.IsLoaded)
            {
                List<ProposalRelatedDealModel> list = ProposalFacade.GetRelatedDeal(pro.Id);
                foreach(ProposalRelatedDealModel m in list)
                {
                    if (m.IsShow)
                    {
                        relatedDeal.Add(m.BusinessHourGuid, ProposalFacade.GetProposalRelatedItemName(m));
                    }
                }
            }

            View.TwentyFourHours = config.TwentyFourHours;
            View.PopulateDealInfo(mainDeal, memberLinks, relatedDeal);
        }

        #region public class methods

        public bool AddUserTrack(string sessionId, string displayName, string url, string queryString, string context)
        {
            return cp.UserTrackSet(sessionId, displayName, url, queryString, context);
        }

        #endregion public class methods
    }
}

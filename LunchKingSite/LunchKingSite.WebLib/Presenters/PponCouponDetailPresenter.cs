﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using log4net.Util;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Models;
using PaymentType = LunchKingSite.Core.PaymentType;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponCouponDetailPresenter : Presenter<IPponCouponDetailView>
    {
        #region property
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        protected IFamiportProvider fp = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        protected IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private CashTrustLogCollection ctlogs = null;
        private CashTrustLogCollection returnFormCtlogs = null;
        private Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();
        
        #endregion

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (View.OrderGuid != Guid.Empty)
            {
                ViewCouponListMain main = MemberFacade.ViewCouponListMainGetByOrderGuid(MemberFacade.GetUniqueId(View.UserName), View.OrderGuid).FirstOrDefault();

                if (main != null && View.ShowDetail(View.OrderGuid, main))
                {
                    OnPanelChange(null, new DataEventArgs<bool>(false)); // false is show detail
                    OnGetOrderDetail(main);
                }
                else
                {
                    View.ShowAlert("這不是您的訂單唷，也許是另外一個帳號的。");
                }
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PanelChange += this.OnPanelChange;
            View.SendSMS += this.OnSendSMS;
            View.RequestEinvoice += this.OnRequestEinvoice;
            View.GetSMS += this.OnGetSMS;
            View.GetViewPponDealGetByBid += this.OnGetViewPponDealGetByBid;
            View.RequestInvoicePaper += OnRequestInvoicePaper;
            return true;
        }

        #region event

        private void OnGetViewPponDealGetByBid(object sender, Guid businessHourGuid)
        {
            View.CurrentQuantity = pp.ViewPponDealGetByBusinessHourGuid(businessHourGuid).OrderedQuantity ?? 0; 
        }

        private void OnRequestInvoicePaper(object sender, string oid)
        {
            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGetByOrderId(oid);
            if (emc.Count > 0 && !string.IsNullOrEmpty(GetUserAddress()))
            {
                DateTime now = DateTime.Now;
                if(emc.Any(x => x.InvoiceStatus == (int)EinvoiceType.C0401 && x.InvoiceRequestTime == null))
                {
                    Member m = mp.MemberGet(View.UserId);
                    string buyerName = m.LastName + m.FirstName;
                    string buyerAddress = m.CompanyAddress;
                    emc.Where(x => x.InvoiceStatus == (int)EinvoiceType.C0401 && x.InvoiceRequestTime == null).ForEach(x =>
                    {
                        if(!x.IsIntertemporal && x.InvoiceRequestTime == null && x.CarrierType != (int)CarrierType.None)
                        {
                            x.InvoiceStatus = (int)EinvoiceType.C0701;
                            x.InvoiceVoidTime = now;
                            x.InvoiceVoidMsg = "變更載具，註銷重開";
                            CommonFacade.AddAudit(View.OrderGuid, AuditType.Order, string.Format("註銷發票:{0}，原因:變更載具{1}", x.InvoiceNumber, string.Format("{0}->{1}", ((CarrierType)x.CarrierType).ToString(), CarrierType.None.ToString())), View.UserName, true);
                            x.CarrierType = (int)CarrierType.None;
                        }
                        x.InvoiceRequestTime = now;
                        x.InvoiceBuyerName = string.IsNullOrEmpty(x.InvoiceBuyerName) ? buyerName : x.InvoiceBuyerName;
                        x.InvoiceBuyerAddress = string.IsNullOrEmpty(x.InvoiceBuyerAddress) ? buyerAddress : x.InvoiceBuyerAddress;
                    });
                    emc.Where(x => x.InvoiceStatus == (int)EinvoiceType.C0401 && x.InvoiceRequestTime == null).ForEach(x => x.InvoiceRequestTime = now);
                    EinvoiceFacade.SetEinvoiceMainCollectionWithLog(emc, View.UserName);
                }
            }
        }

        private void OnRequestEinvoice(object sender, DataEventArgs<MultipleEinvoiceRequestQuery> e)
        {
            CashTrustLogCollection ctlc = mp.CashTrustLogGetListByOrderGuid(e.Data.OrderGuid, OrderClassification.LkSite);
            // 退貨/刷退 以外的 coupon id
            List<int> nonReturnBackCouponIds = ctlc
                .Where(t => t.Status != (int)TrustStatus.Refunded && t.Status != (int)TrustStatus.Returned)
                .Select(x => x.CouponId ?? 0).ToList();

            EinvoiceMainCollection einvoices = op.EinvoiceMainGetListByOrderGuid(e.Data.OrderGuid);

            //可顯示在畫面上的發票 (有發票號碼 & 發票沒作廢 & 憑證型 & 沒退貨)
            IEnumerable<EinvoiceMain> viewableInvoice = einvoices.Where(t =>
                        string.IsNullOrEmpty(t.InvoiceNumber) == false &&
                        t.InvoiceStatus != (int)EinvoiceType.C0501 &&
                        t.CouponId.HasValue &&
                        nonReturnBackCouponIds.Contains(t.CouponId.Value)
                    ).ToList();

            List<string> printableCouponNumbers = viewableInvoice
                .Where(t => t.InvoiceRequestTime.HasValue == false && t.InvoiceWinning == false)
                .Select(t => t.InvoiceNumber).ToList();

            foreach (string einvoiceNumber in printableCouponNumbers)
            {
                op.EinvoiceRequestPaper(View.UserName + "申請紙本", einvoiceNumber, e.Data.Receiver, e.Data.Address);
            }
            View.FinishEinvoiceRequestRequest();
        }

        private void OnGetSMS(object sender, DataEventArgs<SMSQuery> e)
        {
            int smsCount = pp.SMSLogGetCountByPpon(View.UserName, e.Data.CouponId);
            View.GetAlreadySmsCount(smsCount, smsCount >= 3 ? 1 : 0);
        }

        private void OnSendSMS(object sender, DataEventArgs<SMSQuery> e)
        {
            int smsCount = pp.SMSLogGetCountByPpon(View.UserName, e.Data.CouponId);
            if (smsCount >= 3)
            {
                View.GetAlreadySmsCount(3, 1);
            }
            else
            {
                int couponid;
                if (int.TryParse(e.Data.CouponId, out couponid))
                {
                    Guid bid = new Guid(e.Data.Bid);
                    ViewPponCoupon c = pp.ViewPponCouponGet(couponid);
                    SMS sms = new SMS();
                    SmsContent sc = pp.SMSContentGet(bid);
                    string[] msgs = sc.Content.Split('|');
                    string branch = OrderFacade.SmsGetPhone(pp.CouponGet(couponid).OrderDetailId);    //分店資訊 & 多重選項

                    DateTime finalExpireDate = GetFinalExpireDate(e.Data.OrderGuid);
                    
                    string msg = CouponFacade.SmsMessage(bid, c.GroupOrderStatus, c.CouponCode, branch, msgs, c.SequenceNumber, c.BusinessHourDeliverTimeS, finalExpireDate, c.CouponStoreSequence);
                    
                    sms.SendMessage(string.Empty, msg, string.Empty, e.Data.Mobile, View.UserName, Core.SmsType.Ppon, bid, e.Data.CouponId);
                }
            }
        }

        private void OnPanelChange(object sender, DataEventArgs<bool> e)
        {
            View.PanelType = e.Data;
        }

        #endregion event

        #region method
        private void OnGetOrderDetail(ViewCouponListMain main)
        {
            if (View.OrderGuid != Guid.Empty)
            {
                PaymentTransactionCollection pt_atm = new PaymentTransactionCollection();
                ViewCouponListSequenceCollection data = mp.GetCouponListSequenceListByOid(View.OrderGuid, OrderClassification.LkSite);
                EinvoiceMainCollection einvoice = getEinvoiceMainGetListByOrderGuid();
                ViewPponOrderDetailCollection odCol = op.ViewPponOrderDetailGetListByOrderGuid(View.OrderGuid);
                PaymentTransactionCollection pt = op.PaymentTransactionGetList(View.OrderGuid);
                ViewPponOrder o = pp.ViewPponOrderGet(View.OrderGuid);
                ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(o.BusinessHourGuid);
                View.OrderId = o.OrderId;

                if (pt.Count > 0)
                {
                    pt_atm = op.PaymentTransactionGetListByTransIdAndTransType(pt.First().TransId, (int)PayTransType.Authorization);
                }
                CashTrustLogCollection os = getCashTrustLogGetListByOrderGuid();
                var orToExchange = op.OrderReturnListGetListByType(View.OrderGuid, (int)OrderReturnType.Exchange).ToList();
                var allowances = einvoice.Count > 0
                    ? op.EinvoiceAllowanceGetList(einvoice.Select(x => x.Id).ToList())
                    : new List<EinvoiceAllowanceInfo>();
                CtAtmRefund ctatmrefund = op.CtAtmRefundGetLatest(View.OrderGuid);
                EntrustSellReceipt receipt = getEntrustSellReceiptGetByOrder();
                View.ThirdPartyPaymentSystem = os.Any() ? (ThirdPartyPayment)os.First().ThirdPartyPayment : ThirdPartyPayment.None;
                DealProperty dp = pp.DealPropertyGetByOrderGuid(View.OrderGuid);
                var returnForms = ReturnFormRepository.FindAllByOrder(View.OrderGuid);

                //判斷為全家成套票券(咖啡寄杯)
                if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 &&
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                    (deal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                {
                    View.IsFamiportGroupCoupon = true;
                }

                List<ViewMgmGift> mgmGifts = mgm.GiftGetByOid(View.OrderGuid).Where(x => x.IsActive == true).ToList();

                View.SetCouponListSequence(data, einvoice, odCol, pt, pt_atm, os, orToExchange, allowances, ctatmrefund,
                    (receipt.IsLoaded && receipt.ReceiptCode != null), returnForms, op.OrderGet(View.OrderGuid), dp, mgmGifts);


                EntrustSellType entrustSell = (EntrustSellType)dp.EntrustSell;
                switch (entrustSell)
                {
                    case EntrustSellType.PayEasyTravel:
                        throw new NotSupportedException("PayEasyTravel not support.");

                    case EntrustSellType.KindReceipt:
                        View.SetReceiptDealInfo(entrustSell, false, null, null, receipt.IsPhysical, false);
                        break;

                    case EntrustSellType.No:
                    default:
                        View.SetReceiptDealInfo(entrustSell, false, null, null, false, false);
                        break;
                }

                if (dp.DealAccBusinessGroupId != null)
                {
                    View.AccBusinessGroupId = dp.DealAccBusinessGroupId.Value;
                }

                //抓取預計配送結束日期
                //DateTime? deliveryEndDate = pp.BusinessHourGet(dp.BusinessHourGuid).BusinessHourDeliverTimeE;
                DateTime? deliveryEndDate = GetOrderShipEnd(dp.ShipType, dp.ShippingdateType, o.CreateTime, deal.BusinessHourOrderTimeE,
                    dp.Shippingdate, dp.ProductUseDateEndSet, deal.BusinessHourDeliverTimeE); //modify by deal_property調整

                //抓取訂單出貨資訊                
                ViewOrderShipList osInfo;

                if (orToExchange.Count() > 0)
                {
                    osInfo = op.ViewOrderShipListGetListByOrderGuid(View.OrderGuid, OrderShipType.Exchange)
                        .OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                    if (osInfo == null)
                    {
                        osInfo = op.ViewOrderShipListGetListByOrderGuid(View.OrderGuid).OrderByDescending(p => p.OrderShipModifyTime)
                            .FirstOrDefault();
                    }
                }
                else
                {
                    osInfo = op.ViewOrderShipListGetListByOrderGuid(View.OrderGuid).OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                }
                
                if (main.IsWms)
                {
                    List<ShipInfo> shipInfos = OrderFacade.GetOrderShipInfoList(main);
                    string shipText = string.Join("<br/>", shipInfos.Select(x => PponOrderManager.GetShipInfo(x, "：", main)));
                    View.SetShipInfo(osInfo, deliveryEndDate, shipInfos.FirstOrDefault(), shipText);
                }
                else
                {
                    //顯示配送資訊
                    ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                    //不久的將來會使用，先保留
                    //string orderDesc = PponDealApiManager.GetOrderTypeDesc(main);
                    string shipText = PponOrderManager.GetShipInfo(shipInfo, "：", main); //訂單detail的廠商出貨資訊

                    View.SetShipInfo(osInfo, deliveryEndDate, shipInfo, shipText);
                }
               


                #region 全家遙遙茶例外處理

                if ((conf.FamiteaDealId.Length > 0) && (conf.FamiteaDealId.Where(x => x == dp.UniqueId).Any()))
                {
                    //此檔次為全家遙遙茶檔次
                    View.ShowFamiTea("全家活動專屬條碼請點此", true);

                }
                else
                {
                    View.ShowFamiTea(string.Empty, false);
                }

                #endregion
            }
        }

        public EntrustSellReceipt getEntrustSellReceiptGetByOrder()
        {
            return op.EntrustSellReceiptGetByOrder(View.OrderGuid);
        }

        public EinvoiceMainCollection getEinvoiceMainGetListByOrderGuid()
        {
            return op.EinvoiceMainGetListByOrderGuid(View.OrderGuid);
        }

        public CashTrustLogCollection getCashTrustLogGetListByOrderGuid()
        {
            return mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
        }

        public string GetUserMobileNumber()
        {
            Member m = mp.MemberGet(View.UserName);
            return (m != null) ? m.Mobile : string.Empty;
        }

        public string GetUserAddress()
        {
            Member m = mp.MemberGet(View.UserName);
            var pattern = @"(?<city>\D+[縣市])(?<district>\D+?(市區|鎮區|鎮市|[鄉鎮市區]))(?<others>.+)";
            return (m != null) && !string.IsNullOrEmpty(m.CompanyAddress) && Regex.Match(m.CompanyAddress, pattern).Success ? m.CompanyAddress : string.Empty;
        }

        public DateTime GetNewInvoiceDate()
        {
            return conf.NewInvoiceDate;
        }
        public string GetCouponUsageVerifiedTime(int couponId)
        {
            if (ctlogs == null)
            {
                ctlogs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            }
            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            return ctlog.UsageVerifiedTime == null ? string.Empty : ctlog.UsageVerifiedTime.Value.ToString("yyyy/MM/dd HH:mm:ss");
        }
        public string GetCouponStatus(int couponId)
        {
            if (ctlogs == null)
            {
                ctlogs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            }
            if (returnFormCtlogs == null)
            {
                returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderGuid);
            }

            CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (returningCtlog != null)
            {
                if (returningCtlog.Status == (int)TrustStatus.Initial ||
                    returningCtlog.Status == (int)TrustStatus.Trusted ||
                    (returningCtlog.Status == (int)TrustStatus.Verified && returningCtlog.CouponId == null))
                {
                    return "退貨中";
                }
            }

            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        ExpirationDateSelector selector = GetExpirationComponent(View.OrderGuid);
                        if (GetExpirationComponent(View.OrderGuid) != null && selector.IsPastExpirationDate(DateTime.Now))
                        {
                            return "憑證過期";
                        }
                        else
                        {
                            return "未使用";
                        }

                    case TrustStatus.Verified:
                        if (OrderFacade.IsExpiredVerifiedDeal(ctlog))
                        {
                            //展演檔次由系統強制核銷，顯示憑證已過期
                            return "憑證過期";
                        }
                        return "已使用";

                    case TrustStatus.Returned:
                    case TrustStatus.Refunded:
                        return "已退貨";

                    default:
                        return string.Empty;
                }
            }
            return string.Empty;
        }
                
        /// <summary>
        /// 取得宅配檔次之最晚出貨日
        /// </summary>
        /// <param name="ShipType">宅配出貨方式</param>
        /// <param name="ShippingdateType">宅配出貨次方式</param>
        /// <param name="CreateTime">訂單成立日</param>
        /// <param name="OrderEndTime">好康結檔日</param>
        /// <param name="Shippingdate">訂單成立後天數</param>
        /// <param name="ProductUseDateEndSet">結檔後最晚出貨天數</param>
        /// <param name="BusinessDeliver">開始配送日期</param>
        /// <returns></returns>
        public DateTime? GetOrderShipEnd(int? ShipType, int? ShippingdateType, DateTime CreateTime, DateTime OrderEndTime, int? Shippingdate, int? ProductUseDateEndSet, DateTime? BusinessDeliver)
        {
            DateTime? OrderShipEnd = null;  // 宅配最晚出貨日

            try
            {
                VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));
                if (ShipType == (int)DealShipType.Normal)
                {
                    if (ShippingdateType == (int)DealShippingDateType.Normal)
                    {
                        OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime, CreateTime.AddDays((double)Shippingdate));
                    }
                    else if (ShippingdateType == (int)DealShippingDateType.Special)
                    {
                        OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, OrderEndTime, OrderEndTime.AddDays((double)ProductUseDateEndSet));
                    }
                }
                else if (ShipType == (int)DealShipType.Ship72Hrs)
                {
                    CreateTime = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime.AddDays(-1), CreateTime); //週休假日或例假日成立之訂單，均視為次一工作天之訂單
                    OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime, CreateTime.AddDays(1));
                }
                else if (ShipType == (int)DealShipType.LastShipment || ShipType == (int)DealShipType.LastDelivery)
                {
                    OrderShipEnd = BusinessDeliver;
                }
                return OrderShipEnd;
            }
            catch
            {
                return null;
            }
        }

        public Guid GetTrustId(int couponId)
        {
            var tid = mp.CashTrustLogGetByCouponId(couponId);
            if (tid.UsageVerifiedTime.Value.AddDays(7) < DateTime.Now)
            {
                return new Guid();
            }
            if (tid != null)
            {
                var trustId = mp.EvaluateGetMainID(tid.TrustId);
                if (trustId == 0) //代表未填寫過
                {
                    return tid.TrustId;
                }
                else
                {
                    int eValidItems = mp.EvaluateDetailGetByMainID(trustId, (int)EvaluateDataType.Final).Count();
                    if (eValidItems > 0) //代表已填寫過
                    {
                        return new Guid();
                    }
                    else //代表僅填寫過暫存資料
                    {
                        return tid.TrustId;
                    }
                }
            }

            return new Guid();
        }

        private DateTime GetFinalExpireDate(string p)
        {
            Guid oid;
            if (Guid.TryParse(p, out oid))
            {
                ExpirationDateSelector selector = GetExpirationComponent(oid);

                if (selector != null)
                {
                    return selector.GetExpirationDate();
                }
            }
            return new DateTime(1900, 1, 1);
        }

        public ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }

        #endregion method

        public void couponDetailPopRtnDetail()
        {
            string message = string.Empty;
            bool isleagl = true;
            if ((View.OrderGuid == null || string.IsNullOrEmpty(View.UserName)))
                isleagl = false;
            EinvoiceMain einvoice = op.EinvoiceMainGet(View.OrderGuid);
            List<EinvoiceMain> einvoices = op.EinvoiceMainGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite)
                .Where(x => string.IsNullOrEmpty(x.InvoiceNumber) == false).ToList();
            List<EntrustSellReceipt> receipts = op.EntrustSellReceiptGetListByOrder(View.OrderGuid);
            EntrustSellReceipt receipt = receipts.FirstOrDefault();
            CashTrustLogCollection cs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            OldCashTrustLogCollection ocs = mp.GetOldCashTrustLogCollectionByOid(View.OrderGuid);
            Member m = mp.MemberGet(View.UserName);
            CtAtmRefund atmrefund = op.CtAtmRefundGetLatest(View.OrderGuid);
            Order o = op.OrderGet(View.OrderGuid);
            //取得退貨單狀態
            var returnFormStatus = op.ReturnFormGetListByOrderGuid(View.OrderGuid)
                                    .OrderByDescending(x => x.Id)
                                    .First().ProgressStatus;

            if (einvoices.Count() == 0 && (receipt == null || receipt.ReceiptCode == null))//沒發票 也沒代收轉付收據
            {
                isleagl = false;
                message = "此訂單不需填寫【折讓單】!";
            }
            else if (View.UserId == 0 || (einvoices.Count > 0 && einvoices.First().UserId != View.UserId)
                || o.UserId != View.UserId)
            {
                isleagl = false;
                message = "會員資料有誤，請洽客服中心!";
            }
            else if (cs.Count > 0)
            {
                List<CashTrustLog> couponTypeCs = cs.Where(log => log.CouponId != null).ToList();

                if (cs.First().UserId != m.UniqueId)//同會員
                {
                    isleagl = false;
                    message = "會員資料有誤，請洽客服中心!";
                }
            }
            else if (cs.Count == 0)
            {
                if (ocs.Count == 0 && einvoices.Count() > 0 && !einvoices.First().OrderIsPponitem)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
                else if (ocs.Count > 0 && ocs.Count(x => x.Status < (int)TrustStatus.Verified) == 0)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
            }
            ViewPponCouponCollection vpcc = pp.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderGuid, View.OrderGuid);
            ViewPponCoupon vpc = vpcc.FirstOrDefault();
            DealProperty dp;
            if (vpc != null)
            {
                dp = pp.DealPropertyGet(vpc.BusinessHourGuid);
            }
            else
            {
                dp = new DealProperty();
            }
            if (dp.SaleMultipleBase > 0)
            {
                CashTrustLog newCtl = new CashTrustLog();
                newCtl.CreditCard = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.CreditCard);
                newCtl.Atm = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.Atm);
                newCtl.UninvoicedAmount = cs.Where(x => x.Status != (int)TrustStatus.Verified).Sum(x => x.UninvoicedAmount);
                cs = new CashTrustLogCollection();
                cs.Add(newCtl);
            }
            if (einvoices.Count() > 0)
            {
                if (einvoices.First().OrderTime < Convert.ToDateTime(conf.NewInvoiceDate) || dp.DeliveryType == (int)DeliveryType.ToHouse || dp.SaleMultipleBase > 0)
                    View.SetRefundFormInfo(einvoice, cs, ocs, atmrefund, dp, o, isleagl, message, returnFormStatus);
                else
                    View.SetRefundFormInfoForMultiInvoices(einvoices, cs, atmrefund, dp, o, isleagl, message, returnFormStatus);
            }
            else
            {
                View.SetEntrustSellRefundFormInfo(receipts, o, cs, dp, isleagl, message, returnFormStatus);
            }
        }
    }
}
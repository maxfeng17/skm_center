﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class EntrustSellOrderListPresenter : Presenter<IEntrustSellOrderListView>
    {
        #region members
        protected IMemberProvider mp;
        protected IPponProvider pp;
        protected ISystemProvider sp;
        protected IOrderProvider op;
        #endregion

        public EntrustSellOrderListPresenter()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            ViewEntrustsellOrderCollection orders = LoadOrderData(
                View.FilterColumn, View.FilterValue, View.FilterStartTime, View.FilterEndTime, View.FilterOrderStatus);
            View.SetOrderList(orders);
            return true;
        }

        private ViewEntrustsellOrderCollection LoadOrderData(
            string filterColumn, string filterValue, DateTime? startTime, DateTime? endTime, string filterOrderStatus)
        {
            if (endTime != null)
            {
                endTime = Helper.GetFinalTime(endTime.Value);
            }
            return op.EntruslSellOrderList(filterColumn, filterValue, filterOrderStatus, startTime, endTime);
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ExportToExcel += OnExportToExcel;
            View.SearchClicked += OnSearchClicked;
            return true;
        }

        protected void OnExportToExcel(object sender, EventArgs e)
        {
            ViewEntrustsellOrderCollection orders = LoadOrderData(
                View.FilterColumn, View.FilterValue, View.FilterStartTime, View.FilterEndTime, View.FilterOrderStatus);
            View.Export(orders);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            ViewEntrustsellOrderCollection orders = LoadOrderData(
                View.FilterColumn, View.FilterValue, View.FilterStartTime, View.FilterEndTime, View.FilterOrderStatus);
            View.SetOrderList(orders);
        }

    }
}

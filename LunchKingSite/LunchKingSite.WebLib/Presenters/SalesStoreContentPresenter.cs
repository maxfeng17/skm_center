﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesStoreContentPresenter : Presenter<ISalesStoreContentView>
    {
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.StoreGuid);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            View.ChangeLog += OnChangeLog;
            View.StoreApprove += OnStoreApprove;
            return true;
        }

        public SalesStoreContentPresenter(ISellerProvider StoreProv, IHumanProvider humProv, IMemberProvider memProv)
        {
            sp = StoreProv;
            hp = humProv;
            mp = memProv;
        }

        #region event

        protected void OnStoreApprove(object sender, DataEventArgs<SellerTempStatus> e)
        {
            Store store = sp.StoreGet(View.StoreGuid);
            Store ori = store.Clone();
            store.TempStatus = (int)e.Data;
            store.ApproveTime = store.ModifyTime = DateTime.Now;
            store.NewCreated = false;
            store.Message = string.Empty;
            StoreSave(store, ori);

            if (e.Data == SellerTempStatus.Applied)
            {
                Seller seller = sp.SellerGet(store.SellerGuid);
                Seller sori = seller.Clone();
                seller.TempStatus = (int)e.Data;
                seller.ApproveTime = seller.ModifyTime = DateTime.Now;
                seller.NewCreated = false;
                seller.Message = string.Empty;
                SellerSave(store, seller, sori);
            }

            LoadData(View.StoreGuid);
        }

        protected void OnSave(object sender, EventArgs e)
        {
            if (View.StoreGuid != Guid.Empty)
            {
                Store Store = sp.StoreGet(View.StoreGuid);
                Store ori = Store.Clone();
                Store = View.GetStoreData(Store);
                StoreSave(Store, ori);
            }
            else if (View.SellerGuid != Guid.Empty)
            {
                if (!CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Create))
                {
                    View.ShowMessage("無建立商家權限!!", StoreContentMode.PrivilegeError);
                    return;
                }

                Store store = new Store();
                store = View.GetStoreData(store);
                store.SellerGuid = View.SellerGuid;
                store.CreateId = View.UserName;
                store.CreateTime = DateTime.Now;
                sp.StoreSet(store);

                StoreLog(store.Guid, "[系統] 建立店鋪資訊");
                View.RedirectStore(store.Guid);
            }
            LoadData(View.StoreGuid);
        }

        private void StoreSave(Store store, Store ori)
        {
            store.ModifyId = View.UserName;
            store.ModifyTime = DateTime.Now;
            sp.StoreSet(store);


            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, store))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                StoreLog(View.StoreGuid, string.Join("|", changeList));
            }
        }

        private void SellerSave(Store store, Seller seller, Seller ori)
        {
            seller.ModifyId = View.UserName;
            seller.ModifyTime = DateTime.Now;
            sp.SellerSet(seller);

            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, seller))
            {
                changeList.Add(string.Format("[{0}] 變更 {1} 為 {2}", store.StoreName, item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                SellerLog(seller.Guid, string.Join("|", changeList));
            }
        }

        protected void OnChangeLog(object sender, DataEventArgs<string> e)
        {
            StoreLog(View.StoreGuid, e.Data);
            LoadData(View.StoreGuid);
        }

        #endregion

        #region Private Method

        private void StoreLog(Guid stid, string changeLog)
        {
            StoreChangeLog log = new StoreChangeLog();
            log.StoreGuid = stid;
            log.ChangeLog = changeLog;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            sp.StoreChangeLogSet(log);
        }

        private void SellerLog(Guid sid, string changeLog)
        {
            SellerChangeLog log = new SellerChangeLog();
            log.SellerGuid = sid;
            log.ChangeLog = changeLog;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            sp.SellerChangeLogSet(log);
        }


        private void LoadData(Guid stid)
        {
            Store store = sp.StoreGet(stid);
            ViewEmployee emp = new ViewEmployee();
            Guid sid = View.SellerGuid != Guid.Empty ? View.SellerGuid : store.SellerGuid;
            Seller seller = sp.SellerGet(sid);
            if (seller.SalesId != null)
            {
                emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, seller.SalesId);
            }
            StoreChangeLogCollection changeLogs = sp.StoreChangeLogGetList(stid);

            StoreCollection sts = sp.StoreGetListBySellerGuid(store.SellerGuid);

            View.SetStoreContent(seller, store, emp, changeLogs, sts);
        }

        #endregion
    }
}

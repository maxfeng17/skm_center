
namespace LunchKingSite.WebLib.Presenters
{
    public abstract class Presenter<TView>
    {
        // Fields
        private TView _view;

        // Methods
        protected Presenter()
        {
        }

        public virtual bool OnViewInitialized()
        {
            return true;
        }

        public virtual bool OnViewLoaded()
        {
            return true;
        }

        // Properties
        public TView View
        {
            get
            {
                return this._view;
            }
            set
            {
                this._view = value;
            }
        }
    }
}

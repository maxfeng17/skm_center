﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class YahooBigSetupPresenter : Presenter<IYahooBigSetupView>
    {
        private IPponProvider _pponProv;
        private IHiDealProvider _hidealProv;
        private ISysConfProvider _config;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnSortClicked += OnSortClicked;
            View.GetPponYahooPropertyBySort += OnGetPponYahooPropertyBySort;
            View.CheckPponYahooPropertyAction += OnCheckPponYahooPropertyAction;
            View.CheckSelectedPponYahooPropertyAction += OnCheckSelectedPponYahooPropertyAction;
            View.OnSortSaveClicked += OnSortSaveClicked;
            View.SaveYahooProperty += OnSaveYahooProperty;
            View.GetPponYahooProperty += OnGetPponYahooProperty;
            View.GetPiinlifeYahooProperty += OnGetPiinlifeYahooProperty;
            View.OnPponGetCount += OnPponGetCount;
            View.PponPageChanged += OnPponPageChanged;
            View.ImportBidList += OnImportBidList;
            return true;
        }

        public YahooBigSetupPresenter(IPponProvider pponProv, IHiDealProvider hidealProv, ISysConfProvider config)
        {
            _pponProv = pponProv;
            _hidealProv = hidealProv;
            _config = config;
        }

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetDealData(View.SearchType);
        }

        protected void OnSortClicked(object sender, EventArgs e)
        {
            GetSortData();
        }

        protected void OnGetPponYahooPropertyBySort(object sender, DataEventArgs<int> e)
        {
            YahooProperty yp = _pponProv.YahooPropertyGet(e.Data);
            if (yp.IsLoaded)
            {
                View.SetYahooProperty(yp);
            }
        }

        protected void OnCheckPponYahooPropertyAction(object sender, DataEventArgs<int> e)
        {
            _pponProv.YahooPropertyActionUpdate(e.Data, View.UserId, DateTime.Now);
            GetDealData(View.SearchType);
        }

        protected void OnCheckSelectedPponYahooPropertyAction(object sender, DataEventArgs<List<int>> e)
        {
            
            _pponProv.YahooPropertyActionListUpdate(e.Data, View.UserId, DateTime.Now);

            GetDealData(View.SearchType);
        }


        protected void OnSortSaveClicked(object sender, DataEventArgs<Dictionary<int, int?[]>> e)
        {
            foreach (KeyValuePair<int, int?[]> item in e.Data)
            {
                _pponProv.YahooPropertyUpdate(item.Key, item.Value[0], item.Value[1], item.Value[2]);
            }
            GetSortData();
        }

        protected void OnSaveYahooProperty(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            YahooProperty yp = View.GetYahooPropertyData(_pponProv.YahooPropertyGet(e.Data.Key, e.Data.Value));

            #region img upload
            string imgTitle = @"YahooBig_";
            string extensionFileName = "." + Helper.GetExtensionByContentType(View.ImgUploadFile.PostedFile.ContentType);
            System.Drawing.Size size = new System.Drawing.Size(390, 260);
            string fileName = imgTitle + DateTime.Now.Ticks.ToString();
            if (yp.Type == (int)YahooPropertyType.Ppon && yp.Bid.HasValue)
            {
                #region P好康
                ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(yp.Bid.Value);
                if (vpd.IsLoaded)
                {
                    string discount = View.IsPrintDiscountMark ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : string.Empty;
                    if (View.ImgUploadFile.HasFile)
                    {
                        if (CheckFile(View.ImgUploadFile))
                        {
                            if (!string.IsNullOrWhiteSpace(yp.ImgUrl))
                            {
                                // 刪除原檔
                                ImageUtility.DeleteFile(UploadFileType.YahooBigImage, string.Empty, yp.ImgUrl);
                            }
                            // 根據上傳圖檔設定為新的Yahoo大團購圖檔
                            string savePath = vpd.SellerId + @"\" + fileName + extensionFileName;
                            ImageUtility.UploadFile(View.ImgUploadFile.PostedFile.ToAdapter(), UploadFileType.YahooBigImage, vpd.SellerId, fileName);
                            ImageUtility.GenerateImageWithDiscount(savePath, savePath, View.IsCutCenter, discount, size, View.IsPrintLogoWaterMark);
                            yp.ImgUrl = vpd.SellerId + @"/" + fileName + extensionFileName;
                        }
                    }
                    else if (string.IsNullOrWhiteSpace(yp.ImgUrl) && View.Action == YahooPropertyAction.New)
                    {
                        // 新建資料預設取第一張輪播大圖
                        if (Helper.GetRawPathsFromRawData(vpd.EventImagePath).FirstOrDefault() != null)
                        {
                            string[] firstPic = Helper.GetRawPathsFromRawData(vpd.EventImagePath).FirstOrDefault().Split(',');
                            string defaultImgPath = (firstPic.Length > 0 ? firstPic[0] + @"\" + (firstPic.Length > 1 ? firstPic[1] : string.Empty) : string.Empty);
                            if (!string.IsNullOrWhiteSpace(defaultImgPath))
                            {

                                string savePath = firstPic[0] + @"\" + fileName + "." + firstPic[1].Split('.')[1];
                                if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/media/") + defaultImgPath))
                                {
                                    ImageUtility.GenerateImageWithDiscount(defaultImgPath, savePath, View.IsCutCenter, discount, size, View.IsPrintLogoWaterMark);
                                    yp.ImgUrl = firstPic[0] + @"/" + fileName + "." + firstPic[1].Split('.')[1];
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            else if (yp.Type == (int)YahooPropertyType.Piinlife && yp.Pid.HasValue)
            {
                #region 品生活
                HiDealProduct hdp = _hidealProv.HiDealProductGet(yp.Pid.Value);
                if (hdp.IsLoaded)
                {
                    HiDealDeal hdd = _hidealProv.HiDealDealGet(hdp.DealId);
                    string dirPath = string.Format(@"HiDeal_{0}", hdd.Id.ToString());
                    string discount = View.IsPrintDiscountMark ? ((Math.Round(((hdp.Price ?? 0) / (hdp.OriginalPrice ?? 1)), 2, MidpointRounding.AwayFromZero)) * 10).ToString("0.0") : string.Empty;
                    string defaultImgPath = hdd.PrimarySmallPicture;
                    if (View.ImgUploadFile.HasFile)
                    {
                        if (CheckFile(View.ImgUploadFile))
                        {
                            // 刪除原檔
                            if (!string.IsNullOrWhiteSpace(yp.ImgUrl))
                            {
                                // 因品生活圖檔在檔次(HiDealDeal)架構下，因此在清除舊圖檔時僅清除新上傳之圖檔
                                ImageUtility.DeleteFile(UploadFileType.YahooBigImage, string.Empty, dirPath + @"\" + fileName + extensionFileName);
                            }
                            // 根據上傳圖檔設定為新的Yahoo大團購圖檔
                            string savePath = dirPath + @"\" + fileName + extensionFileName;
                            ImageUtility.UploadFile(View.ImgUploadFile.PostedFile.ToAdapter(), UploadFileType.YahooBigImage, dirPath, fileName, View.IsPrintLogoWaterMark);
                            ImageUtility.GenerateImageWithDiscount(savePath, savePath, View.IsCutCenter, discount, size, View.IsPrintLogoWaterMark);
                            yp.ImgUrl = dirPath + @"/" + fileName + extensionFileName;
                        }
                    }
                    else if (string.IsNullOrWhiteSpace(yp.ImgUrl) && View.Action == YahooPropertyAction.New)
                    {
                        // 新建資料預設取第一張輪播大圖
                        if (!string.IsNullOrWhiteSpace(defaultImgPath))
                        {
                            string defaultFileName = defaultImgPath.Split('/').LastOrDefault();
                            string savePath = dirPath + @"\" + fileName + "." + defaultFileName.Split('.')[1];
                            if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/media/") + defaultImgPath))
                            {
                                ImageUtility.GenerateImageWithDiscount(defaultImgPath, savePath, View.IsCutCenter, discount, size, View.IsPrintLogoWaterMark);
                                yp.ImgUrl = dirPath + @"/" + fileName + "." + defaultFileName.Split('.')[1];
                            }
                        }
                    }
                }
                #endregion
            }
            else
            {
                return;
            }
            #endregion

            if (View.IsMarkDelete)
            {
                if (View.Action == YahooPropertyAction.New)
                {
                    if (!string.IsNullOrWhiteSpace(yp.ImgUrl) && File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/media/") + yp.ImgUrl))
                    {
                        File.Delete(System.Web.HttpContext.Current.Server.MapPath("~/media/") + yp.ImgUrl);
                    }
                    _pponProv.YahooPropertyDelete(yp.Id);

                    GetDataBySearhMode();
                    View.IsShowYahooPropertyEditPanel = false;
                    return;
                }
                else
                {
                    yp.Action = (int)YahooPropertyAction.Delete;
                    yp.ModifyId = View.UserId;
                    yp.ModifyTime = DateTime.Now;
                }
            }
            else if (View.Action == YahooPropertyAction.New)
            {
                yp.Action = (int)YahooPropertyAction.New;
                yp.CreateId = View.UserId;
                yp.CreateTime = DateTime.Now;
            }
            else
            {
                yp.Action = (int)YahooPropertyAction.Update;
                yp.ModifyId = View.UserId;
                yp.ModifyTime = DateTime.Now;
            }
            _pponProv.YahooPropertySet(yp);
            View.SetYahooProperty(yp);
            GetDataBySearhMode();
        }

        protected void OnGetPponYahooProperty(object sender, DataEventArgs<Guid> e)
        {
            YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Bid, e.Data);
            if (yp.IsLoaded)
            {
                View.SetYahooProperty(yp);
            }
            else
            {
                ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(e.Data);
                if (vpd.IsLoaded)
                {
                    // set default value
                    View.Action = YahooPropertyAction.New;
                    yp.Action = (int)YahooPropertyAction.New;
                    yp.Type = (int)YahooPropertyType.Ppon;
                    yp.Bid = vpd.BusinessHourGuid;
                    yp.Title = vpd.ItemName;
                    yp.DealDesc = vpd.EventTitle;
                    yp.Category = GetNewYahooCategoryByDealGuid(vpd.BusinessHourGuid);
                    yp.AreaFlag = GetNewYahooAreaByDealGuidAndYahooCategory(vpd.BusinessHourGuid, yp.Category);
                    View.SetYahooProperty(yp);
                }
                else
                {
                    View.ShowMessage("查無檔次資料");
                }
            }
        }

        protected void OnGetPiinlifeYahooProperty(object sender, DataEventArgs<int> e)
        {
            YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Pid, e.Data);
            if (yp.IsLoaded)
            {
                View.SetYahooProperty(yp);
            }
            else
            {
                HiDealProduct hdp = _hidealProv.HiDealProductGet(e.Data);
                if (hdp.IsLoaded)
                {
                    HiDealDeal hd = _hidealProv.HiDealDealGet(hdp.DealId);
                    if (hd.IsLoaded)
                    {
                        // set default value
                        View.Action = YahooPropertyAction.New;
                        yp.Action = (int)YahooPropertyAction.New;
                        yp.Type = (int)YahooPropertyType.Piinlife;
                        yp.Pid = hdp.Id;
                        yp.Title = hdp.Name;
                        yp.DealDesc = hdp.Description;
                        View.SetYahooProperty(yp);
                    }
                    else
                    {
                        View.ShowMessage("查無檔次資料");
                    }
                }
                else
                {
                    View.ShowMessage("查無產品資料");
                }
            }
        }

        public void OnImportBidList(object sender, DataEventArgs<List<string>> e)
        {
            string alertMsg = string.Empty;
            YahooPropertyCollection itemCollection = ValidateAndGetItemListAndAlertMsg(e.Data, out alertMsg);

            if (!string.IsNullOrWhiteSpace(alertMsg))
            {
                View.AlertImportErrorMsg(alertMsg);
            }
            else
            {
                if (itemCollection.Count > 0)
                {
                    _pponProv.YahooPropertySetList(itemCollection);
                }
            }

            GetDealData(View.SearchType);
        }

        private YahooPropertyCollection ValidateAndGetItemListAndAlertMsg(List<string> dataList, out string alertMsg)
        {
            YahooPropertyCollection itemCollection = new YahooPropertyCollection();
            alertMsg = string.Empty;

            for (int i = 0; i < dataList.Count; i++)
            {
                Guid bid = new Guid();
                if (Guid.TryParse(dataList[i], out bid))
                {
                    ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
                    if (vpd != null && vpd.IsLoaded && vpd.UniqueId.HasValue)
                    {
                        if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {
                            #region 檢查ViewPponDeal的欄位是否有填
                            bool columnsComplete = true;
                            string editLink = string.Format("<a target='_blank' href='{0}/controlroom/ppon/setup.aspx?bid={1}'>{1}</a>", _config.SiteUrl, dataList[i]);
                            if (string.IsNullOrWhiteSpace(vpd.EventTitle))
                            {
                                columnsComplete = false;
                                alertMsg += string.Format("第{0}列-行銷標題(橘標)未填：{1}<br />", i + 1, editLink);
                            }
                            if (string.IsNullOrWhiteSpace(vpd.ItemName))
                            {
                                columnsComplete = false;
                                alertMsg += string.Format("第{0}列-訂單短標(品牌名稱.商品)未填：{1}<br />", i + 1, editLink);
                            }
                            #endregion

                            if (columnsComplete)
                            {
                                GenerateYahooPropertyAndAddIntoCollection(itemCollection, vpd);
                            }
                        }
                        else
                        {
                            alertMsg += string.Format("第{0}列-匯入的bid為子檔的：{1}<br />{3}(必須為母檔的：{2})", i + 1, dataList[i], vpd.MainBid, string.Join("&nbsp;", new string[18]));//18為實際測出來的寬度，為了對齊
                        }
                    }
                    else
                    {
                        alertMsg += string.Format("第{0}列-查無此bid：{1}<br />", i + 1, dataList[i]);
                    }
                }
                else
                {
                    alertMsg += string.Format("第{0}列-bid格式有誤：{1}<br />", i + 1, dataList[i]);
                }
            }

            return itemCollection;
        }

        private void GenerateYahooPropertyAndAddIntoCollection(YahooPropertyCollection collection, ViewPponDeal vpd)
        {
            if (vpd.IsLoaded)
            {
                YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Bid, vpd.BusinessHourGuid);

                if (yp.IsLoaded)
                {
                    yp.Action = (int)YahooPropertyAction.Update;
                    yp.ModifyTime = DateTime.Now;
                    yp.ModifyId = View.UserId;
                }
                else
                {
                    yp.Action = (int)YahooPropertyAction.New;
                    yp.CreateId = View.UserId;
                    yp.CreateTime = DateTime.Now;
                }

                yp.Type = (int)YahooPropertyType.Ppon;
                yp.Bid = vpd.BusinessHourGuid;
                yp.Title = vpd.ItemName;
                yp.DealDesc = vpd.EventTitle;
                yp.Category = GetNewYahooCategoryByDealGuid(vpd.BusinessHourGuid);
                yp.AreaFlag = GetNewYahooAreaByDealGuidAndYahooCategory(vpd.BusinessHourGuid, yp.Category);
                if (yp.AreaFlag.HasValue)
                {
                    OldAreaFlagSetting(yp);
                }
                GenerateYahooImgUrl(yp, vpd);

                collection.Add(yp);
            }
        }

        private void GenerateYahooImgUrl(YahooProperty yp, ViewPponDeal vpd)
        {
            // 資料預設取第一張輪播大圖
            if (Helper.GetRawPathsFromRawData(vpd.EventImagePath).FirstOrDefault() != null)
            {
                string imgTitle = @"YahooBig_";
                System.Drawing.Size size = new System.Drawing.Size(390, 260);
                string fileName = imgTitle + DateTime.Now.Ticks.ToString();
                string discount = View.IsPrintDiscountMark ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), vpd.ItemPrice, vpd.ItemOrigPrice) : string.Empty;

                string[] firstPic = Helper.GetRawPathsFromRawData(vpd.EventImagePath).FirstOrDefault().Split(',');
                string defaultImgPath = (firstPic.Length > 0 ? firstPic[0] + @"\" + (firstPic.Length > 1 ? firstPic[1] : string.Empty) : string.Empty);
                if (!string.IsNullOrWhiteSpace(defaultImgPath))
                {

                    string savePath = firstPic[0] + @"\" + fileName + "." + firstPic[1].Split('.')[1];
                    if (File.Exists(System.Web.HttpContext.Current.Server.MapPath("~/media/") + defaultImgPath))
                    {
                        ImageUtility.GenerateImageWithDiscount(defaultImgPath, savePath, View.IsCutCenter, discount, size, View.IsPrintLogoWaterMark);
                        yp.ImgUrl = firstPic[0] + @"/" + fileName + "." + firstPic[1].Split('.')[1];
                    }
                }
            }
        }

        private static void OldAreaFlagSetting(YahooProperty yp)
        {
            //foreach (var item in Enum.GetValues(typeof(YahooPropertyArea)))
            foreach (var item in Enum.GetValues(typeof(YahooPropertyArea)))
            {
                if (Helper.IsFlagSet(yp.AreaFlag.Value, (int)(YahooPropertyArea)item))
                {
                    switch ((int)(YahooPropertyArea)item)
                    {
                        case 1: yp.AreaN = (int)YahooPropertyAreaNorth.taipei; yp.AreaC = (int)YahooPropertyAreaCentral.taichung; ; yp.AreaS = (int)YahooPropertyAreaSouth.kaohsiung; ; break;
                        case 2: yp.AreaN = (int)YahooPropertyAreaNorth.taipei; break;
                        case 4: yp.AreaN = (int)YahooPropertyAreaNorth.taoyuan; break;
                        case 8: yp.AreaN = (int)YahooPropertyAreaNorth.hsinchu; break;
                        case 16: yp.AreaN = (int)YahooPropertyAreaNorth.keelung; break;

                        case 32: yp.AreaC = (int)YahooPropertyAreaCentral.taichung; break;
                        case 64: yp.AreaC = (int)YahooPropertyAreaCentral.miaoli; break;
                        case 128: yp.AreaC = (int)YahooPropertyAreaCentral.changhua; break;
                        case 256: yp.AreaC = (int)YahooPropertyAreaCentral.nantou; break;
                        case 512: yp.AreaC = (int)YahooPropertyAreaCentral.yunlin; break;

                        case 1024: yp.AreaS = (int)YahooPropertyAreaSouth.kaohsiung; break;
                        case 2048: yp.AreaS = (int)YahooPropertyAreaSouth.tainan; break;
                        case 4096: yp.AreaS = (int)YahooPropertyAreaSouth.chiayi; break;
                        case 8192: yp.AreaS = (int)YahooPropertyAreaSouth.pingtung; break;
                    }
                }
            }
        }

        #endregion

        #region method
        protected void GetPponDealData(int pageNumber)
        {
            Dictionary<ViewPponDeal, KeyValuePair<YahooProperty, string[]>> dataList = new Dictionary<ViewPponDeal, KeyValuePair<YahooProperty, string[]>>();
            Guid bid = Guid.TryParse(View.DealId, out bid) ? bid : Guid.Empty;
            ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListPagingForYahoo(pageNumber, View.PageSize, View.DealStartDate, View.DealEndDate, bid, View.DealName);
            foreach (ViewPponDeal vpd in vpdc)
            {
                // get cityList string
                string cityList = string.Empty;
                if (!string.IsNullOrWhiteSpace(vpd.CityList))
                {
                    List<int> citys = new JsonSerializer().Deserialize<List<int>>(vpd.CityList);
                    foreach (int city in citys)
                    {
                        cityList += PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(city).CityName + ", ";
                    }
                }

                // 是否已有設定Yahoo大團購資料
                YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Bid, vpd.BusinessHourGuid);

                string[] info = {
                                    vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm"),
                                    vpd.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm"),
                                    cityList,
                                };
                dataList.Add(vpd, new KeyValuePair<YahooProperty, string[]>(yp, info));
            }
            View.SetPponDealList(dataList);
        }

        protected void GetPiinlifeDealData()
        {
            Dictionary<HiDealProduct, KeyValuePair<YahooProperty, string[]>> dataList = new Dictionary<HiDealProduct, KeyValuePair<YahooProperty, string[]>>();
            HiDealDealCollection hddc = _hidealProv.HiDealDealGetList(GetPiinlifeDealFilter());
            foreach (HiDealDeal hdd in hddc)
            {
                IEnumerable<HiDealProduct> products = _hidealProv.HiDealProductCollectionGet(hdd.Id).Where(x => x.Type != (int)HiDealProductType.Freight);
                foreach (HiDealProduct product in products)
                {
                    // 是否已有設定Yahoo大團購資料
                    YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Pid, product.Id);
                    string[] info = {
                                        hdd.DealStartTime.Value.ToString("yyyy/MM/dd HH:mm"),
                                        hdd.DealEndTime.Value.ToString("yyyy/MM/dd HH:mm"),
                                        hdd.Name,
                                    };
                    dataList.Add(product, new KeyValuePair<YahooProperty, string[]>(yp, info));
                }
            }
            View.SetPiinlifeDealList(dataList);
        }

        protected void OnPponGetCount(object sender, DataEventArgs<int> e)
        {
            Guid bid = Guid.TryParse(View.DealId, out bid) ? bid : Guid.Empty;
            e.Data = _pponProv.ViewPponDealGetCountForYahoo(View.DealStartDate, View.DealEndDate, bid, View.DealName);
        }

        protected void OnPponPageChanged(object sender, DataEventArgs<int> e)
        {
            GetPponDealData(e.Data);
        }
        #endregion

        #region Private Method
        private string[] GetPiinlifeDealFilter()
        {
            List<string> list = new List<string>();
            if (View.DealStartDate != DateTime.MinValue)
            {
                list.Add(HiDealDeal.Columns.DealStartTime + "<" + View.DealStartDate.ToString("yyyy/MM/dd HH:mm"));
                list.Add(HiDealDeal.Columns.DealEndTime + ">" + View.DealEndDate.ToString("yyyy/MM/dd HH:mm"));
            }
            if (!string.IsNullOrWhiteSpace(View.DealId))
            {
                int did;
                if (int.TryParse(View.DealId, out did))
                {
                    list.Add(HiDealDeal.Columns.Id + "=" + did);
                }
            }
            if (!string.IsNullOrWhiteSpace(View.DealName))
            {
                list.Add(HiDealDeal.Columns.Name + " like %" + View.DealName + "%");
            }
            return list.ToArray();
        }

        /// <summary>
        /// 判斷檔案是否符合限制條件
        /// </summary>
        /// <param name="objFile"></param>
        /// <returns></returns>
        private bool CheckFile(FileUpload objFile)
        {
            bool blnFalg = true;

            string extensionName = System.IO.Path.GetExtension(objFile.FileName); // 取得路徑
            int megaBytes = (objFile.FileBytes.Length / (1024 * 1024));  // 計算檔案大小
            string alertMeesage = string.Empty;

            if (!objFile.HasFile) // HasFile 取得值，判斷控制項是否包含檔案(bool)
            {
                alertMeesage = "請上傳檔案";
                blnFalg = false;
            }
            else if (megaBytes > 1) // 設定上傳檔案大小限制
            {
                alertMeesage = "檔案大小不得大於1MB";
                blnFalg = false;
            }
            else if (objFile.PostedFile.ContentType != "image/jpeg")  // 限定檔案格式
            {
                alertMeesage = "檔案格式不正確，請選擇jpg檔案";
                blnFalg = false;
            }

            if (!blnFalg)
            {
                View.ShowMessage(alertMeesage);
            }

            return blnFalg;

        }

        private void GetDealData(YahooPropertyType type)
        {
            switch (type)
            {
                case YahooPropertyType.Ppon:
                    GetPponDealData(View.CurrentPage);
                    break;
                case YahooPropertyType.Piinlife:
                    GetPiinlifeDealData();
                    break;
                default:
                    break;
            }
        }

        private void GetDataBySearhMode()
        {
            if (View.SearchMode == "Sort")
            {
                GetSortData();
            }
            else
            {
                GetDealData(View.SearchType);
            }
        }

        private void GetSortData()
        {
            List<YahooProperty> ypsN = new List<YahooProperty>();
            List<YahooProperty> ypsC = new List<YahooProperty>();
            List<YahooProperty> ypsS = new List<YahooProperty>();
            
            // 僅限排今日檔次(ex: 9/25 12:00 上檔的檔次，要搜尋9/26 00:00 ~ 9/17 00:00 處於on檔狀態的檔次)
            // 2013/10/31 eva須預先排序，因此再挪後一天查詢(ex: 9/25 查詢資料，要撈取 9/27 整天on檔狀態的檔次)
            DateTime dateStart = DateTime.Now.Date.AddDays(2);
            DateTime dateEnd = dateStart.AddDays(1);

            ViewPponDealCollection vpdc = _pponProv.ViewPponDealGetListPagingForYahoo(0, 0, dateStart, dateEnd, Guid.Empty, string.Empty);
            foreach (ViewPponDeal vpd in vpdc.OrderByDescending(x => x.BusinessHourOrderTimeS))
            {
                YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Bid, vpd.BusinessHourGuid);
                if (yp.IsLoaded && !yp.Action.EqualsAny((int)YahooPropertyAction.Delete, (int)YahooPropertyAction.Remove))
                {
                    if (yp.AreaN != null)
                    {
                        ypsN.Add(yp);
                    }
                    if (yp.AreaC != null)
                    {
                        ypsC.Add(yp);
                    }
                    if (yp.AreaS != null)
                    {
                        ypsS.Add(yp);
                    }
                }
            }
            HiDealDealCollection hddc = _hidealProv.HiDealDealGetList(HiDealDeal.Columns.DealStartTime + "<" + dateStart.ToString("yyyy/MM/dd HH:mm"), HiDealDeal.Columns.DealEndTime + ">" + dateEnd.ToString("yyyy/MM/dd HH:mm"));
            foreach (HiDealDeal hdd in hddc.OrderByDescending(x => x.DealStartTime))
            {
                IEnumerable<HiDealProduct> products = _hidealProv.HiDealProductCollectionGet(hdd.Id).Where(x => x.Type != (int)HiDealProductType.Freight);
                foreach (HiDealProduct product in products)
                {
                    YahooProperty yp = _pponProv.YahooPropertyGet(YahooProperty.Columns.Pid, product.Id);
                    if (yp.IsLoaded && !yp.Action.EqualsAny((int)YahooPropertyAction.Delete, (int)YahooPropertyAction.Remove))
                    {
                        if (yp.AreaN != null)
                        {
                            ypsN.Add(yp);
                        }
                        if (yp.AreaC != null)
                        {
                            ypsC.Add(yp);
                        }
                        if (yp.AreaS != null)
                        {
                            ypsS.Add(yp);
                        }
                    }
                }
            }
            View.SetSortData(ypsN.OrderBy(x => x.SortN), ypsC.OrderBy(x => x.SortC), ypsS.OrderBy(x => x.SortS));
        }


        private int GetNewYahooCategoryByDealGuid(Guid bid)
        {
            //var viewppondeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            //var categoryIdList = PponDealPreviewManager.GetDealCategoryIdList(bid);
            List<int> categoryList = new List<int>();
            categoryList =  _pponProv.CategoryDealsGetList(bid).Select(x=>x.Cid).ToList();
            if (categoryList.Any())
            {
                //餐廳 restaurant
                if (categoryList.Contains(CategoryManager.Default.PponDeal.CategoryId) ||
                    categoryList.Contains(CategoryManager.Default.Piinlife.CategoryId))
                {
                    return (int)YahooPropertyCategory.restaurant;
                }

                //旅遊 travel
                if (categoryList.Contains(CategoryManager.Default.Travel.CategoryId))
                {
                    if (categoryList.Contains(848))
                    {
                        return (int)YahooPropertyCategory.sports;
                    }
                    return (int)YahooPropertyCategory.travel;
                }

                if (categoryList.Contains(CategoryManager.Default.Delivery.CategoryId))
                {
                    if (categoryList.Contains(113) || categoryList.Contains(114))
                    {
                        //宅配頻道>人氣美食、生鮮超市
                        return (int)YahooPropertyCategory.food;
                    }

                    if (categoryList.Contains(115) || categoryList.Contains(121) || categoryList.Contains(222))
                    {
                        //宅配頻道>居家生活、傢俱傢飾、媽咪寶貝
                        return (int)YahooPropertyCategory.life;
                    }
                    else if (categoryList.Contains(124))
                    {
                        //宅配頻道>美妝保養
                        return (int)YahooPropertyCategory.cosmetic;
                    }
                    else if (categoryList.Contains(117))
                    {
                        //宅配頻道>3c家電
                        return (int)YahooPropertyCategory.computer;
                    }
                    else if (categoryList.Contains(122))
                    {
                        //宅配頻道>流行時尚
                        return (int)YahooPropertyCategory.dress;
                    }

                    return (int)YahooPropertyCategory.delivery;
                }

                if (categoryList.Contains(CategoryManager.Default.Beauty.CategoryId))
                {
                    //玩美休閒頻道>親子寵物、汽車美容、展演票券、休閒娛樂
                    if (categoryList.Contains(900) || categoryList.Contains(901) || categoryList.Contains(218) || categoryList.Contains(219))
                    {
                        return (int)YahooPropertyCategory.life;
                    }else if (categoryList.Contains(183))
                    {
                        //玩美休閒頻道>運動健身
                        return (int)YahooPropertyCategory.sports;
                    }
                    else if (!(categoryList.Contains(183) || categoryList.Contains(900) || categoryList.Contains(901) || categoryList.Contains(218) || categoryList.Contains(219)))
                    {
                        //玩美休閒頻道，排除 [運動健身] [親子寵物] [汽車美容][展演票券] [休閒娛樂]
                        return (int)YahooPropertyCategory.cosmetic;
                    }
                }
            }

            return (int) YahooPropertyCategory.all;
        }

        private int? GetNewYahooAreaByDealGuidAndYahooCategory(Guid bid,int? yahooCategory)
        {
            int? areaFlagValue = null;
            if (yahooCategory.HasValue)
            {
                YahooPropertyCategory seletedCategory =(YahooPropertyCategory)Enum.Parse(typeof(YahooPropertyCategory),yahooCategory.Value.ToString());

                switch (seletedCategory)
                {
                    case YahooPropertyCategory.beauty://已不使用
                    case YahooPropertyCategory.fashion://已不使用
                        break;
                    case YahooPropertyCategory.restaurant://餐廳
                        //取得店家地址轉換Flag
                        areaFlagValue = GetNewYahooAreaFalgByDealStore(bid);
                        break;
                    case YahooPropertyCategory.cosmetic://美妝保養、完美休閒
                        //取得店家地址轉換Flag
                        areaFlagValue = GetNewYahooAreaFalgByDealStore(bid);
                        //玩美、休閒 頻道檔次 不加入不分區Flag
                        if (PponDealPreviewManager.GetDealCategoryIdList(bid).Contains(CategoryManager.Default.Beauty.CategoryId))
                        {
                        }
                        else
                        {
                            areaFlagValue = (int)Helper.SetFlag(true, (areaFlagValue.HasValue) ? areaFlagValue.Value : default(int), YahooPropertyArea.all);
                        }
                        break;
                    case YahooPropertyCategory.travel:// 旅遊
                        //取得店家地址轉換Flag
                        areaFlagValue = GetNewYahooAreaFalgByDealStore(bid);
                        areaFlagValue = (int)Helper.SetFlag(true, (areaFlagValue.HasValue) ? areaFlagValue.Value : default(int), YahooPropertyArea.all);
                        break;
                    case YahooPropertyCategory.delivery://宅配
                    case YahooPropertyCategory.food://美食
                    case YahooPropertyCategory.life://居家生活
                    case YahooPropertyCategory.sports://運動
                    case YahooPropertyCategory.computer://3C家電
                    case YahooPropertyCategory.dress://服飾鞋包
                        //宅配分類僅有不分區
                        areaFlagValue = (int)YahooPropertyArea.all;
                        break;
                    case YahooPropertyCategory.all://全部
                        areaFlagValue = (int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.all);
                        break;
                }
            }


            return areaFlagValue;
        }
        /// <summary>
        /// 取得店家地址轉換NewYahooAreaFlag
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private int? GetNewYahooAreaFalgByDealStore(Guid bid)
        {
            int? areaFlagValue = null;
            ViewPponStoreCollection vpsCol = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(bid);

            if (vpsCol.Any())
            {
                foreach (var store in vpsCol)
                {              
                    if ( store.IsLoaded && Helper.IsFlagSet((int)VbsRightFlag.VerifyShop, store.VbsRight))
                    {

                        switch (store.CityShortName)
                        {
                            case "台北": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.taipei);break;
                            case "桃園": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.taoyuan);break;
                            case "新竹": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.hsinchu);break;
                            case "基隆": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.keelung);break;
                            case "台中": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.taichung);break;
                            case "苗栗": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.miaoli);break;
                            case "彰化": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.changhua);break;
                            case "南投": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.nantou);break;
                            case "雲林": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.yunlin);break;
                            case "高雄": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.kaohsiung);break;
                            case "台南": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.tainan);break;
                            case "嘉義": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.chiayi);break;
                            case "屏東": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.pingtung);break;
                            case "新北": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.new_taipei);break;
                            case "宜蘭": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.yilan);break;
                            case "花蓮": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.hualien);break;
                            case "台東": areaFlagValue=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.taitung);break;
                            case "澎湖": 
                            case "金門": 
                            case "連江":
                            case "南海":areaFlagValue+=(int)Helper.SetFlag(true, (areaFlagValue.HasValue)?areaFlagValue.Value:default(int), YahooPropertyArea.other);break;
                        }
                    }
                }
            }

            return areaFlagValue;
        }

        #endregion
    }
}

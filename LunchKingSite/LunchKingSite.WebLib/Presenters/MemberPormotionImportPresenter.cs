﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class MemberPormotionImportPresenter : Presenter<IMemberPormotionImportView>
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        IMemberProvider mp;

        public MemberPormotionImportPresenter(IMemberProvider memPro)
        {
            mp = memPro;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Import += OnImport;
            return true;
        }

        #region event

        protected void OnImport(object sender, DataEventArgs<List<int>> e)
        {
            if (View.Amount > 0)
            {
                int multiple = 10;
                DateTime now = DateTime.Now;
                List<int> errorUser = new List<int>();
                DateTime endTime = new DateTime(View.EndTime.Year, View.EndTime.Month, View.EndTime.Day, View.EndTime.Hour, View.EndTime.Minute, View.EndTime.Minute == 59 ? 59 : 00);
                MemberPromotionDepositCollection medc = new MemberPromotionDepositCollection();
                foreach (int user in e.Data)
                {
                    Member m = mp.MemberGet(user);
                    if (m.IsLoaded)
                    {
                        medc.Add(new MemberPromotionDeposit()
                        {
                            StartTime = View.StartTime,
                            ExpireTime = endTime,
                            PromotionValue = View.Amount * multiple,
                            CreateTime = now,
                            Action = View.ActionName,
                            CreateId = View.UserName,
                            UserId = user,
                            Type = (int)MemberPromotionType.Bouns
                        });

                        string auditMsg = string.Format("{0}補紅利{1}點", View.ActionName, View.Amount * multiple);
                        CommonFacade.AddAudit(m.UserName, AuditType.Member, auditMsg, View.UserName, true);
                    }
                    else
                    {
                        errorUser.Add(user);
                    }
                }
                int count = mp.MemberPromotionDepositSetList(medc);
                string msg = string.Format("已匯入紅利{0}筆資料!! 共計{1}元。", count, count * View.Amount);
                if (errorUser.Count > 0)
                {
                    msg += string.Format("錯誤的帳號:{0}。請重新確認!!", string.Join(",", errorUser.ToArray()));
                }
                SendMail(View.ActionName, View.StartTime, View.EndTime, count, View.Amount, View.UserName);
                View.ShowMessage(msg);
            }
            else
                View.ShowMessage("僅允許匯入正數");

        }

        private void SendMail(string actionName, DateTime startTime, DateTime endTime, int count, double amount, string username)
        {
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.SystemEmail);
            msg.To.Add(username); // 申請人
            msg.To.Add(config.CsManagerEmail); // 客服主管
            msg.Subject = "[紅利匯入通知]" + actionName;
            msg.Body = string.Format("申請人:{0}\n紅利名稱:{1}\n紅利金額:${2}\n紅利使用期限:{3}\n發送名單:{4}名\n\n共計${5}元",
                                        username, 
                                        actionName,
                                        amount.ToString("N0"), 
                                        startTime.ToString("yyyy/MM/dd HH:mm") + " ~ " + endTime.ToString("yyyy/MM/dd HH:mm"),
                                        count,
                                        (count * amount).ToString("N0"));
            msg.IsBodyHtml = false;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }

        #endregion
    }
}

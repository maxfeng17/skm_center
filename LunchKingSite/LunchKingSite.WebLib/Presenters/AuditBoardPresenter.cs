using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class AuditBoardPresenter : Presenter<IAuditBoardView>
    {
        private ICmsProvider cp;

        public AuditBoardPresenter()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.ReferenceId))
            {
                AuditCollection a = cp.AuditGetList(View.ReferenceId, View.ShowAllRecords);
                if (a.Count > 0)
                {
                    View.HideView = false;
                    View.SetRecordGrid(a);
                }

                return true;
            }
            return false;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            this.View.AddClick += this.OnAddClick;
            this.View.EditClick += this.OnEditClick;
            return true;
        }

        protected void OnAddClick(object sender, DataEventArgs<AuditEntry> e)
        {
            string cid = e.Data.CreateId != null ? e.Data.CreateId.Trim() : null;
            string msg = e.Data.Message != null ? e.Data.Message.Trim() : null;

            if (CommonFacade.AddAudit(View.ReferenceId, View.ReferenceType, msg, cid, e.Data.VisibleInViewMode))
            {
                View.SetRecordGrid(cp.AuditGetList(View.ReferenceId, View.ShowAllRecords));
            }
        }

        protected void OnEditClick(object sender, DataEventArgs<KeyValuePair<int, bool>> e)
        {
            Audit record = cp.AuditGet(e.Data.Key);
            if (record.IsLoaded)
            {
                record.CommonVisible = e.Data.Value;
                if (record.IsDirty)
                {
                    cp.AuditSet(record);
                }
            }
        }
    }
}
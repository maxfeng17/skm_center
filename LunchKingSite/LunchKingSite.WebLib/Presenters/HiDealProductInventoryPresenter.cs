﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web.Security;
using System.Data;
using LunchKingSite.BizLogic.Model;

using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealProductInventoryPresenter : Presenter<IHiDealProductInventoryView>
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadBusinessDeal();
            LoadProductDeal();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            //觸發商品ID出清冊
            View.GetProductReportDetail += OnGetProductReportDetail;
            View.GetDealReportDetail += OnGetDealReportDetail;
            return true;
        }
        #region method

        private void LoadBusinessDeal()
        {
            ViewHiDealSellerProductCollection DealDetail = hp.ViewHiDealSellerProductGetDeal(View.Dealid.ToString());
            View.showBusinessData(DealDetail);
         }

        private void LoadProductDeal()
        {
            int dealId = int.Parse(View.Dealid);
            ViewHiDealOrderReturnQuantityCollection OrderReturnQty = hp.ViewHiDealOrderReturnQuantityGetListByDealId(dealId);

            //沒訂單資料的產品也要列出
            var deal = HiDealDealManager.HiDealDealGetById(dealId);
            foreach (var product in HiDealDealManager.HiDealProductGetListByDealId(dealId))
            {
                if (OrderReturnQty.Any(t=>t.ProductId == product.Id))
                {
                    continue;
                }
                OrderReturnQty.Add(new ViewHiDealOrderReturnQuantity
                {
                    HiDealId = deal.Id,
                    Name = deal.Name,
                    ProductId = product.Id,
                    ProductName = product.Name
                });
            }
            View.showProductData(OrderReturnQty);
        }

        public void OnGetProductReportDetail(object sender, DataEventArgs<int> e)
        {
            ViewHiDealProductHouseInfoCollection  productHuoseInfo =hp.ViewHiDealProductHouseInfoGetListByProductId(e.Data) ;
            ViewHiDealProductShopInfoCollection productShopInfo = hp.ViewHiDealProductShopInfoGetListByProductId(e.Data);
            ViewHiDealOrderReturnQuantityCollection dealInfo = hp.ViewHiDealOrderReturnQuantityGetListByProductId(e.Data);

            View.ExportProductData((ViewHiDealProductShopInfoCollection)productShopInfo, (ViewHiDealProductHouseInfoCollection)productHuoseInfo,(ViewHiDealOrderReturnQuantityCollection) dealInfo);
        }

        public void OnGetDealReportDetail(object sender, DataEventArgs<int> e)
        {
            HiDealProductCollection productList = hp.HiDealProductCollectionGet(e.Data);
            if (productList.Count > 0)
            {
                List<HiDealProductInventoryViewDealProductInfo> dealProductInfoList = new List<HiDealProductInventoryViewDealProductInfo>();

                foreach (var item in productList)
                {
                    HiDealProductInventoryViewDealProductInfo productInfo = new HiDealProductInventoryViewDealProductInfo();
                    productInfo.ProductHouseInfos = hp.ViewHiDealProductHouseInfoGetListByProductId((int)item.Id);
                    productInfo.ProductShopInfos = hp.ViewHiDealProductShopInfoGetListByProductId((int)item.Id);

                    productInfo.Product = item;
                    productInfo.Deal = hp.HiDealDealGet(item.DealId);
                    dealProductInfoList.Add(productInfo);
                }

                View.ExportDealData(dealProductInfoList);
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
namespace LunchKingSite.WebLib.Presenters
{
    public class VourcherEventEditPresenter : Presenter<IVourcherEventEditView>
    {
        private IOrderProvider op;

        public VourcherEventEditPresenter(IOrderProvider order_provider)
        {
            op = order_provider;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
           // VourcherEventCollection vourcher_events_apply = VourcherFacade.VourcherEventCollectionGetByStatus(string.Empty, (int)VourcherEventStatus.ApplyEvent);
           // VourcherEventCollection vourcher_events_eventchecked = VourcherFacade.VourcherEventCollectionGetByStatus(string.Empty, (int)VourcherEventStatus.EventChecked);
            //foreach (var item in vourcher_events_eventchecked)
            //{
            //    vourcher_events_apply.Add(item);
            //}

            //View.SetApplyCaseVourcherEvent(vourcher_events_apply);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetDataCount += OnGetDataCount;
            View.PageChanged += OnPageChanged;
            View.GetSellerById += OnGetSellerById;
            View.SaveVourcherEventStores += OnSaveVourcherEventStores;
            View.GetVourcherEvent += OnGetVourcherEvent;
            View.SearchVourcherEvent += OnSearchVourcherEvent;
            View.UpdateVourcherEventStatus += OnUpdateVourcherEventStatus;
            return true;
        }

        void OnUpdateVourcherEventStatus(object sender, DataEventArgs<KeyValuePair<int, string>> e)
        {
            if (View.VourcherEventId != 0)
            {
                bool check_sellerstore_tempstatus = true;
                VourcherEvent original_vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
                VourcherStoreCollection vourcher_stores = VourcherFacade.VourcherStoreCollectionGetByEventId(original_vourcher_event.Id);
                Seller seller = VourcherFacade.SellerGetByGuid(original_vourcher_event.SellerGuid);
                StoreCollection stores = VourcherFacade.StoreCollectionGetBySellerGuid(original_vourcher_event.SellerGuid);
                View.SellerGuid = original_vourcher_event.SellerGuid;
                if (e.Data.Key == (int)VourcherEventStatus.EventChecked)
                {
                    if (seller.TempStatus != (int)SellerTempStatus.Completed)
                        check_sellerstore_tempstatus = false;
                    foreach (var item in vourcher_stores)
                    {
                        Store store;
                        if ((store = stores.First(x => x.Guid == item.SellerGuid)) != null && store.TempStatus != (int)SellerTempStatus.Completed)
                        {
                            check_sellerstore_tempstatus = false;
                        }
                    }
                }
                if (check_sellerstore_tempstatus)
                {
                    original_vourcher_event.Status = e.Data.Key;
                    original_vourcher_event.ModifyId = View.UserName;
                    original_vourcher_event.ModifyTime = DateTime.Now;
                    original_vourcher_event.Message = e.Data.Value;
                    VourcherFacade.VourcherEventSet(original_vourcher_event);
                    View.SetVourcherEventStores(seller, stores, original_vourcher_event, vourcher_stores);
                }
                else
                    View.SetVourcherEventStatus(original_vourcher_event.Status, "賣家、分店尚未通過審核");
            }
        }

        void OnSearchVourcherEvent(object sender, EventArgs e)
        {
            LoadVourcherEvent(1);
        }

        void OnGetVourcherEvent(object sender, DataEventArgs<int> e)
        {
            View.VourcherEventId = e.Data;
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(e.Data);
            View.SellerGuid = vourcher_event.SellerGuid;
            VourcherStoreCollection vourcher_stores = VourcherFacade.VourcherStoreCollectionGetByEventId(e.Data);
            Seller seller = VourcherFacade.SellerGetByGuid(vourcher_event.SellerGuid);
            StoreCollection stores = VourcherFacade.StoreCollectionGetBySellerGuid(vourcher_event.SellerGuid);
            View.SetVourcherEventStores(seller, stores, vourcher_event, vourcher_stores);
        }

        void OnSaveVourcherEventStores(object sender, DataEventArgs<KeyValuePair<VourcherEvent, VourcherStoreCollection>> e)
        {

            //ImageUtility.UploadFile(e.Data.UploadedFile, UploadFileType.PponEvent, entity.Store.SellerId, fileName, View.IsPrintWatermark);
            int status;
            VourcherEvent vourcher_event = e.Data.Key;
            if (View.VourcherEventId != 0)
            {
                VourcherEvent original_vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
                original_vourcher_event.EventName = vourcher_event.EventName;
                original_vourcher_event.SellerGuid = vourcher_event.SellerGuid;
              
                

                original_vourcher_event.Contents = vourcher_event.Contents;
                original_vourcher_event.Instruction = vourcher_event.Instruction;
                original_vourcher_event.PicUrl = vourcher_event.PicUrl;
                original_vourcher_event.Type = vourcher_event.Type;
                original_vourcher_event.StartDate = vourcher_event.StartDate;
                original_vourcher_event.EndDate = vourcher_event.EndDate;
                original_vourcher_event.MaxQuantity = vourcher_event.MaxQuantity;
                original_vourcher_event.ModifyId = View.UserName;
                original_vourcher_event.ModifyTime = DateTime.Now;
                original_vourcher_event.Message = vourcher_event.Message;
                //original_vourcher_event.Status = (int)VourcherEventStatus.ApplyEvent;
                VourcherFacade.VourcherEventSet(original_vourcher_event);
                VourcherFacade.VourcherStoreDeleteByEventId(View.VourcherEventId);
                foreach (var item in e.Data.Value)
                {
                    item.VourcherEventId = original_vourcher_event.Id;
                }
                VourcherFacade.VourcherStoresSet(e.Data.Value);
                View.VourcherEventId = original_vourcher_event.Id;
                status = original_vourcher_event.Status;
            }
            else
            {
                vourcher_event.CreateId = View.UserName;
                vourcher_event.CreateTime = DateTime.Now;
                vourcher_event.Status = (int)VourcherEventStatus.ApplyEvent;
                VourcherFacade.VourcherEventSet(vourcher_event);
                VourcherFacade.VourcherStoreDeleteByEventId(vourcher_event.Id);
                foreach (var item in e.Data.Value)
                {
                    item.VourcherEventId = vourcher_event.Id;
                    item.CreateId = View.UserName;
                    item.CreateTime = DateTime.Now;
                }
                VourcherFacade.VourcherStoresSet(e.Data.Value);
                View.VourcherEventId = vourcher_event.Id;
                status = (int)VourcherEventStatus.EventChecked;
            }
            View.SetVourcherEventStatus(status,string.Empty);
        }

        void OnGetSellerById(object sender, DataEventArgs<string> e)
        {
            Seller seller = VourcherFacade.SellerGetById(e.Data);
            StoreCollection stores = VourcherFacade.StoreCollectionGetBySellerGuid(seller.Guid);
            View.SellerGuid = seller.Guid;
            View.SetSellerStores(seller, stores);
        }
        /// <summary>
        /// 搜尋優惠券轉分頁
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">第幾分頁</param>
        void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadVourcherEvent(e.Data);
        }
        /// <summary>
        /// 依優惠券搜尋條件計算總資料數
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnGetDataCount(object sender, EventArgs e)
        {
            //判斷搜尋條件是否有填值
            View.PageCount = VourcherFacade.VourcherEventCollectionGetCount(View.SearchKeys, View.Status);
        }
        private void LoadVourcherEvent(int page)
        {
            VourcherEventCollection vourcher_events = new VourcherEventCollection();
            //vourcher_events = VourcherFacade.VourcherEventCollectionGetBySearch(page, View.PageSize, View.SearchKeys, string.Empty, View.Status);
            //回傳優惠券資料
            View.SetVourcherEventCollection(vourcher_events);
        }
    }
}

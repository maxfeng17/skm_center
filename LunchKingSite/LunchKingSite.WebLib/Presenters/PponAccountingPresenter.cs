﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponAccountingPresenter : Presenter<IPponAccountingView>
    {
        #region members

        protected IPponProvider pp;

        #endregion members

        public PponAccountingPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetData(View.BusinessHourGuid);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.QueryCost += OnQueryCost;
            View.UpdateAccounting += OnUpdateAccounting;
            View.CreateCost += OnCreateCost;
            View.DeleteCost += OnDeleteCost;

            return true;
        }

        protected void SetupProviders()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        private void SetData(Guid pid)
        {
            DealAccounting dAccounting = pp.DealAccountingGet(pid);
            DealCostCollection costCol = pp.DealCostGetList(pid);

            if (dAccounting.SalesCommission == null) dAccounting.SalesCommission = 0.2;
            if (dAccounting.SalesBonus == null) dAccounting.SalesBonus = 0;
            if (dAccounting.Status == null) dAccounting.Status = (int)AccountsPayableFormula.Default;

            View.SetData(dAccounting, (costCol.Count > 0) ? costCol : null);
        }

        private void OnQueryCost(object sender, EventArgs e)
        {
            DealCostCollection costCol = pp.DealCostGetList(View.BusinessHourGuid);
            View.SetGrid(costCol);
        }

        public void OnUpdateAccounting(object sender, DataEventArgs<UpdatableAccountingInfo> e)
        {
            DealAccounting dAccounting = pp.DealAccountingGet(View.BusinessHourGuid);
            if (dAccounting.IsNew)
            {
                dAccounting.BusinessHourGuid = View.BusinessHourGuid;
            }
            dAccounting.SalesId = e.Data.SalesId;
            dAccounting.SalesCommission = Convert.ToDouble(e.Data.Commission);
            dAccounting.SalesBonus = int.Parse(e.Data.Bonus);
            if (!string.IsNullOrEmpty(e.Data.ChargingDate))
            {
                dAccounting.ChargingDate = DateTime.Parse(e.Data.ChargingDate);
            }

            if (!string.IsNullOrEmpty(e.Data.EnterDate))
            {
                dAccounting.EnterDate = DateTime.Parse(e.Data.EnterDate);
            }

            if (!string.IsNullOrEmpty(e.Data.InvoiceDate))
            {
                dAccounting.InvoiceDate = DateTime.Parse(e.Data.InvoiceDate);
            }
            dAccounting.Status = int.Parse(e.Data.APFormula);
            pp.DealAccountingSet(dAccounting);

            DealCostCollection costCol = pp.DealCostGetList(View.BusinessHourGuid);
            DealCost dCost = new DealCost();
            if (costCol.Count == 0)
            {
                dCost.BusinessHourGuid = View.BusinessHourGuid;
            }
            else
            {
                dCost = costCol[0];
            }

            if (!string.IsNullOrEmpty(e.Data.Cost))
            {
                dCost.Cost = decimal.Parse(e.Data.Cost);

                ViewPponDeal deal = pp.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid);
                if (deal != null)
                {
                    dCost.Quantity = (int)deal.OrderTotalLimit;
                    dCost.CumulativeQuantity = (int)deal.OrderTotalLimit;
                    dCost.LowerCumulativeQuantity = (int)deal.OrderTotalLimit;
                }
            }
            pp.DealCostSet(dCost);
        }

        public void OnCreateCost(object sender, DataEventArgs<DealCost> e)
        {
            DealCostCollection CostCol = pp.DealCostGetList(e.Data.BusinessHourGuid);
            if (CostCol.Count == 0)
            {
                e.Data.CumulativeQuantity = e.Data.Quantity;
                e.Data.LowerCumulativeQuantity = e.Data.Quantity;
            }
            else
            {
                e.Data.CumulativeQuantity = CostCol[CostCol.Count - 1].CumulativeQuantity + e.Data.Quantity;
                e.Data.LowerCumulativeQuantity = CostCol[CostCol.Count - 1].CumulativeQuantity;
            }
            pp.DealCostSet(e.Data);
        }

        protected void OnDeleteCost(object sender, EventArgs e)
        {
            DealCostCollection CostCol = pp.DealCostGetList(View.BusinessHourGuid);
            if (CostCol.Count > 0)
            {
                pp.DealCostDelete(CostCol[CostCol.Count - 1].Id);
            }
        }
    }
}
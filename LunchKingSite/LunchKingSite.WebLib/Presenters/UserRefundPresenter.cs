﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserRefundPresenter : Presenter<IUserRefund>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            BankInfoCollection bks = op.BankInfoGetMainList();
            bks.Insert(0, new BankInfo() { BankName = "請選擇銀行別", BankNo = "-1" });
            View.TxtOrderId.Attributes.Add("placeholder", "範例：AA-00-000-000000-000");
            View.TxtAccountId.Attributes.Add("placeholder", "Ex：00010002000304");
            View.TxtMobile.Attributes.Add("placeholder", "Ex：0912345678");
            OnGetATMBranch(this, new DataEventArgs<string>("-1"));
            View.SetBankInfo(bks);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetOrderInfo += OnGetOrderInfo;
            View.RequestRefund += OnRequestRefund;
            View.GetATMBranch += OnGetATMBranch;
            View.RequestRefundATM += OnRequestRefundATM;
            View.CancelNotCompleteOrder += OnCancelNotCompleteOrder;
            return true;
        }

        private void OnCancelNotCompleteOrder(object sender, DataEventArgs<string> e)
        {
            ViewCouponListMain main = mp.GetCouponListMainByOid(View.OrderGuid);
            Order o = op.OrderGet(View.OrderGuid);

            if (string.IsNullOrEmpty(main.PreShipNo))
            {
                //配編前直接取消
                PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys");

                OrderFacade.SendOrderCancelMail(o.Guid, false, true);
            }
            else
            {
                //配編後為取消中
                o.IsCanceling = true;
                op.OrderSet(o);

                OrderFacade.SendOrderCancelMail(o.Guid, true, false);
            }
            CommonFacade.AddAudit(o.Guid, AuditType.Order, "取消訂單申請(WEB)", View.UserName, false);
            View.ReturnPanel("CancelComplete");
        }

        private void OnRequestRefundATM(object sender, DataEventArgs<KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>>> e)
        {
            //本來這邊的程式碼是專門給ATM 訂單用的, 所以PaymentTransaction的條件也要是ATM. 但因為 "訂購超過一年" 的訂單
            //無法刷退, 所以也規劃走ATM 退款模式. 因此在這裡把ATM 判斷取消
            PaymentTransactionCollection pt = op.PaymentTransactionGetList(View.OrderGuid);
            PaymentTransaction pt_atm = (pt.Count() > 0) ? op.PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                PaymentTransaction.Columns.TransId + " = " + pt.FirstOrDefault().TransId,
                PaymentTransaction.Columns.TransType + " = " + (int)PayTransType.Authorization).FirstOrDefault() : null;
            if (pt_atm != null)
            {
                CtAtmRefund ctatmrefund = e.Data.Key;
                ctatmrefund.PaymentId = pt_atm.TransId;
                ctatmrefund.Amount = 0;
                op.CtAtmRefundSet(ctatmrefund);
                op.CtAtmUpdateStatus((int)AtmStatus.RefundRequest, View.OrderGuid);

                CommonFacade.AddAudit(View.OrderGuid, AuditType.Order, 
                    string.Format("新增修改匯款帳戶：戶名:{0}，ID:{1}，{2}({3})，帳戶:{4}，手機:{5}",
                        ctatmrefund.AccountName,
                        ctatmrefund.Id,
                        ctatmrefund.BankName,
                        ctatmrefund.BranchName,
                        ctatmrefund.AccountNumber,
                        ctatmrefund.Phone), 
                    View.UserName, true);
            }
            OnRequestRefund(sender, new DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>>(e.Data.Value));
        }

        private void OnGetATMBranch(object sender, DataEventArgs<string> e)
        {
            BankInfoCollection bks = new BankInfoCollection();
            if (e.Data == "-1")
            {
                bks.Add(new BankInfo() { BranchName = "請選擇分行別", BranchNo = "-1" });
            }
            else
            {
                bks = op.BankInfoGetBranchList(e.Data);
            }

            View.SetATMBranch(bks);
        }

        private void OnRequestRefund(object sender, DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>> e)
        {
            //消費者訂單頁退貨
            OrderStatusLog orderStatusLog = e.Data.Key;
            RefundType refundType = e.Data.Value;

            EinvoiceMainCollection einvoice = op.EinvoiceMainGetListByOrderGuid(View.OrderGuid);
            //EntrustSellReceipt receipt = op.EntrustSellReceiptGetByOrder(View.OrderGuid); 
            ViewPponDeal deal = pp.ViewPponDealGet(op.OrderGet(orderStatusLog.OrderGuid).ParentOrderId.Value);

            bool isRefundCash = refundType == RefundType.Atm || refundType == RefundType.Cash || refundType == RefundType.Tcash;
            //bool isReceiptGenerated = receipt.IsLoaded && receipt.ReceiptCode != null; // 如果代收轉付收據已開立 **因代收轉付目前未上線使用 故判斷需折讓單暫不管**
            
            CreateReturnFormResult result = CreateReturnFormResult.None;
            int? dummy;

            string failReason = ""; 
            bool isRefundScashToCash = OrderFacade.CanApplyRefund(View.OrderGuid, View.UserName, false, out failReason);
            List<int> returnItemList = new List<int>();


            ViewCouponListMain vclm = mp.GetCouponListMainByOid(View.OrderGuid);
            if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int) BusinessHourStatus.GroupCoupon) &&
                Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int) GroupOrderStatus.FamiDeal))
            {
                //全家寄杯註銷 todo:OnRequestRefundATM 是否要先做註銷?
                if (!OrderFacade.FamilyNetPincodeReturn(View.OrderGuid, deal.BusinessHourGuid, out failReason))
                {
                    View.alertMessage = failReason;
                    return;
                }
            }
            else if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int) BusinessHourStatus.GroupCoupon) &&
                      Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int) GroupOrderStatus.HiLifeDeal))
            {
                //萊爾富寄杯 
                if (conf.EnableHiLifeDealSetup && !OrderFacade.HiLifePincodeReturn(View.OrderGuid, out failReason))
                {
                    View.alertMessage = failReason;
                    return;
                }
            }

            if (einvoice.Count > 0)
            {
                //電子折讓單上線 : 除三聯紙本發票 申請退貨 皆視為走電子折讓
                //2016以前的只能走紙本，主要影響成套，一般憑證不影響、宅配已過鑑賞期
                einvoice.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Duplicate &&
                                    x.AllowanceStatus.GetValueOrDefault((int)AllowanceStatus.PaperAllowance) == (int)AllowanceStatus.PaperAllowance && x.OrderTime > conf.EinvAllowanceStartDate)
                        .ToList().ForEach(i => i.AllowanceStatus = (int)AllowanceStatus.ElecAllowance);
                EinvoiceFacade.SetEinvoiceMainCollectionWithLog(einvoice, View.UserName);
            }

            #region 憑證退貨申請單

            if (View.IsCoupon)
            {
                var ctls = ReturnService.GetRefundableCouponCtlogs(orderStatusLog.OrderGuid, BusinessModel.Ppon);
                IEnumerable<int> refundableCouponIds = ctls.Where(x => x.CouponId != null).Select(x => (int)x.CouponId).ToList();
                if (refundableCouponIds.Any()) 
                {
                    result = ReturnService.CreateCouponReturnForm(
                        orderStatusLog.OrderGuid, refundableCouponIds, !isRefundCash, View.UserName, orderStatusLog.Reason, out dummy, false);
                    returnItemList = refundableCouponIds.ToList();
                }
                else if (isRefundScashToCash) 
                {
                    //取得系統建立的退購物金退貨單 => progressStatus轉成處理中 refundType => scahshToCash or ScashToAtm
                    ReturnFormCollection rfc = ReturnService.RefundScashReturnForm(View.OrderGuid);
                    foreach(ReturnForm rf in rfc)
                    {
                        ReturnFormEntity returnForm = ReturnFormRepository.FindById(rf.Id);

                        if (!returnForm.CanChangeToCashRefund(out failReason))
                        {
                            View.alertMessage = failReason;
                            return;
                        }

                        var msg = string.Format("退貨單({0}) - 處理購物金轉現金", rf.Id);
                        CommonFacade.AddAudit(View.OrderGuid, AuditType.Order, msg, View.UserName, false);
                        ReturnService.RefundScashToCash(rf.Id, View.UserName);
                    }
                }
                else
                {
                    result = CreateReturnFormResult.ProductsUnreturnable;
                }
            }

            #endregion 憑證退貨申請單

            #region 宅配退貨申請單
            
            else
            {
                //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                List<int> orderProductIds = op.OrderProductGetListByOrderGuid(orderStatusLog.OrderGuid)
                        .Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                if (orderProductIds.Any())
                {
                    result = ReturnService.CreateRefundForm(orderStatusLog.OrderGuid, orderProductIds, !isRefundCash, View.UserName,
                        orderStatusLog.Reason, orderStatusLog.ReceiverName, orderStatusLog.ReceiverAddress, orderStatusLog.IsReceive, null, out dummy, false);
                    returnItemList = orderProductIds;
                }
                else if (isRefundScashToCash)
                {
                    //取得系統建立的退購物金退貨單 => progressStatus轉成處理中 refundType => scahshToCash or ScashToAtm
                    ReturnFormCollection rfc = ReturnService.RefundScashReturnForm(View.OrderGuid);
                    foreach (ReturnForm rf in rfc)
                    {
                        ReturnFormEntity returnForm = ReturnFormRepository.FindById(rf.Id);

                        if (!returnForm.CanChangeToCashRefund(out failReason))
                        {
                            View.alertMessage = failReason;
                            return;
                        }

                        CommonFacade.AddAudit(View.OrderGuid, AuditType.Order, string.Format("退貨單({0}) - 處理購物金轉現金", rf.Id), View.UserName, false);
                        ReturnService.RefundScashToCash(rf.Id, View.UserName);
                    }
                }
                else
                {
                    result = CreateReturnFormResult.ProductsUnreturnable;
                }
            }

            #endregion

            if (result == CreateReturnFormResult.Created)
            {
                if (einvoice.Count > 0 && einvoice.Any(x => x.InvoiceRequestTime.HasValue))
                {
                    UserRefundFacade.EinvocieCancelRequestProcess(einvoice, orderStatusLog.OrderGuid, returnItemList, View.IsCoupon ? DeliveryType.ToShop : DeliveryType.ToHouse, View.UserName);
                }

                int returnFormId = op.ReturnFormGetListByOrderGuid(orderStatusLog.OrderGuid)
                      .OrderByDescending(x => x.Id)
                      .First().Id;

                string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, orderStatusLog.Reason);
                CommonFacade.AddAudit(orderStatusLog.OrderGuid, AuditType.Refund, auditMessage, View.UserName, false);

                OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                if (!View.IsCoupon)
                {
                    EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                }
            }
            else
            {
                if (!isRefundScashToCash)
                {
                    //建立退貨單失敗
                    View.ReturnPanel("RefundFail");
                    return;
                }
            }

            if (isRefundCash)
            {
                //刷退或是指定帳戶退款
                if (View.IsCoupon)
                {
                    //憑證
                    if (refundType == RefundType.Atm)
                    {
                        //退款至帳戶
                        View.ReturnPanel("RefundATMSuccess_ToShopOrder");
                        return;
                    }
                    else
                    {
                        //刷退
                        View.ReturnPanel("RefundCashSuccess_ToShopOrder");
                        return;
                    }
                }
                else
                {
                    //宅配
                    if (refundType == RefundType.Atm)
                    {
                        //退款至帳戶
                        View.ReturnPanel("RefundATMSuccess_ToHouseOrder");
                        return;
                    } 
                    else
                    {
                        //刷退
                        View.ReturnPanel("RefundCashSuccess_ToHouseOrder");
                        return;
                    }
                }
            }
            else
            {
                //只退購物金
                if (DateTime.Compare(DateTime.Today, deal.BusinessHourDeliverTimeS.Value.Date) < 0)
                {
                    //兌換開始日前
                    View.ReturnPanel("RefundCashPointSuccess_BeforeUseStartTime");
                    return;
                }
                else
                {
                    //兌換開始日後
                    if (View.IsCoupon)
                    {
                        //憑證
                        View.ReturnPanel("RefundCashPointSuccess_AfterUseStartTime");
                        return;
                    }
                    else
                    {
                        //宅配
                        View.ReturnPanel("RefundCashPointSuccess_AfterUseStartTime_ToHouseOrder");
                        return;
                    }
                }
            }
        }

        private void OnGetOrderInfo(object sender, DataEventArgs<string> e)
        {
            RefundOrderInfo orderinfo = UserRefundFacade.GetOrderInfo(MemberFacade.GetUniqueId(View.UserName), e.Data);
            View.IsRefundCashOnly = false;
            if (orderinfo.Coupons.Count > 0)
            {
                View.OrderGuid = orderinfo.OrderGuid;
                View.IsPaidByCash = orderinfo.IsPaidByCash;
            }
            if (orderinfo.HasPartialCancelOrVerified && orderinfo.IsInstallment)
            {
                View.ReturnPanel("RefundFail");
                return;
            }

            if (orderinfo.IsPpon)
            {
                View.SetOrderInfo(orderinfo, null);
                return;
            }

            //抓取訂單出貨資訊                
            ViewOrderShipList osInfo;
            int orToExchangeCount = op.OrderReturnListGetCountByType(orderinfo.OrderGuid, (int)OrderReturnType.Exchange);
            if (orToExchangeCount > 0)
            {
                osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.OrderGuid, OrderShipType.Exchange)
                    .OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                if (osInfo == null)
                {
                    osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.OrderGuid).OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
                }
            }
            else
            {
                osInfo = op.ViewOrderShipListGetListByOrderGuid(orderinfo.OrderGuid).OrderByDescending(p => p.OrderShipModifyTime).FirstOrDefault();
            }
            View.SetOrderInfo(orderinfo, osInfo);
        }
    }
}
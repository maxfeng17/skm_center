﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NewtonsoftJson = Newtonsoft.Json;

namespace LunchKingSite.WebLib.Presenters
{
    public class VourcherSellerPresenter : Presenter<IVourcherSellerView>
    {
        #region local property

        /// <summary>
        /// 修改賣家相關欄位名稱
        /// </summary>
        private List<string> seller_columns = new List<string>() { Seller.SellerNameColumn.ColumnName, Seller.SellerBossNameColumn.ColumnName, Seller.SellerTelColumn.ColumnName,
                Seller.SellerFaxColumn.ColumnName,Seller.SellerMobileColumn.ColumnName, Seller.CityIdColumn.ColumnName,Seller.SellerAddressColumn.ColumnName,
                Seller.SellerEmailColumn.ColumnName,Seller.CompanyNameColumn.ColumnName,Seller.SignCompanyIDColumn.ColumnName,
                Seller.CompanyBossNameColumn.ColumnName,Seller.SellerContactPersonColumn.ColumnName,Seller.CoordinateColumn.ColumnName,
                Seller.SellerBlogColumn.ColumnName,Seller.SellerDescriptionColumn.ColumnName,Seller.SellerCategoryColumn.ColumnName,Seller.SellerConsumptionAvgColumn.ColumnName
        };

        /// <summary>
        /// 修改分店相關欄位名稱
        /// </summary>
        private List<string> store_columns = new List<string>(){Store.StoreNameColumn.ColumnName,Store.CompanyBossNameColumn.ColumnName,Store.PhoneColumn.ColumnName,
                Store.AddressStringColumn.ColumnName, Store.OpenTimeColumn.ColumnName, Store.CloseDateColumn.ColumnName,Store.RemarksColumn.ColumnName, Store.TownshipIdColumn.ColumnName,
                Store.MrtColumn.ColumnName, Store.CarColumn.ColumnName, Store.BusColumn.ColumnName,Store.OtherVehiclesColumn.ColumnName, Store.WebUrlColumn.ColumnName,Store.SignCompanyIDColumn.ColumnName,
                Store.FacebookUrlColumn.ColumnName, Store.PlurkUrlColumn.ColumnName,Store.BlogUrlColumn.ColumnName,Store.OtherUrlColumn.ColumnName, Store.CompanyNameColumn.ColumnName,
                Store.MessageColumn.ColumnName,Store.Columns.CityId,Store.CoordinateColumn.ColumnName,Store.CreditcardAvailableColumn.ColumnName
        };

        #endregion local property

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SearchReturnCase();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchSellerInfo += OnSearchSellerInfo;
            View.SaveTempSeller += OnSaveTempSeller;
            View.SaveTempStore += OnSaveTempStore;
            View.GetSellerByGuid += OnGetSellerByGuid;
            View.GetStoreByGuid += OnGetStoreByGuid;
            View.PageChanged += OnPageChanged;
            View.GetDataCount += OnGetDataCount;
            View.GetTempStatusSellerStore += OnGetTempStatusSellerStore;
            return true;
        }

        private void OnGetTempStatusSellerStore(object sender, EventArgs e)
        {
            SearchReturnCase();
        }

        #region event

        /// <summary>
        /// 依賣家Guid撈取賣家資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">賣家Guid</param>
        private void OnGetSellerByGuid(object sender, DataEventArgs<Guid> e)
        {
            Seller seller = VourcherFacade.SellerGetByGuid(e.Data);
            View.SellerGuid = e.Data;
            View.SellerId = seller.SellerId;
            //賣家的修改資料紀錄(json)
            StoreCollection stores = VourcherFacade.StoreCollectionGetBySellerGuid(e.Data);
            ChangeLogCollection changelogs = VourcherFacade.ChangeLogCollectionGetAll(Seller.Schema.TableName, View.SellerGuid);
            //回傳賣家和其分店資料及賣家修改紀錄
            View.SetSellerStores(seller, stores, changelogs);
        }

        /// <summary>
        /// 依分店Guid撈取分店資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">分店Guid</param>
        private void OnGetStoreByGuid(object sender, DataEventArgs<Guid> e)
        {
            Store store = VourcherFacade.StoreGetByGuid(e.Data);
            View.StoreGuid = e.Data;
            OnGetSellerByGuid(sender, new DataEventArgs<Guid>(store.SellerGuid));
            //分店的修改資料紀錄(json)
            ChangeLogCollection changelogs = VourcherFacade.ChangeLogCollectionGetAll(Store.Schema.TableName, View.StoreGuid);
            //回傳分店和其修改資料紀錄
            View.SetStore(store, changelogs);
        }

        #region 分頁

        /// <summary>
        /// 依賣家搜尋條件計算總資料數
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnGetDataCount(object sender, EventArgs e)
        {
            //判斷搜尋條件是否有填值
            View.PageCount = VourcherFacade.SellerGetCount(View.SearchKeys, string.Empty);
        }

        /// <summary>
        /// 搜尋賣家轉分頁
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">第幾分頁</param>
        private void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadSellerCollection(e.Data);
        }

        /// <summary>
        /// 搜尋賣家
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSearchSellerInfo(object sender, EventArgs e)
        {
            LoadSellerCollection(1);
        }

        #endregion 分頁

        /// <summary>
        /// 儲存賣家資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">賣家資料</param>
        private void OnSaveTempSeller(object sender, DataEventArgs<KeyValuePair<Seller, List<PhotoInfo>>> e)
        {
            string[] folder = { "original", "small", "large" };
            string changeitems = string.Empty;
            Seller original_seller;
            Seller edited_seller = e.Data.Key;
            List<PhotoInfo> photos = e.Data.Value;
            List<string> logo_imgs;
            //判斷是否為已存在的賣家
            if (View.SellerGuid != Guid.Empty)
            {
                //賣家欄位和修改後的值
                Dictionary<string, string> change_items = new Dictionary<string, string>();
                //原賣家資料
                original_seller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
                if (original_seller.NewCreated)
                {
                    CheckChangeItem(original_seller, edited_seller);
                    original_seller.TempStatus = (int)SellerTempStatus.Applied;
                    original_seller.ApplyId = View.UserName;
                    original_seller.ApplyTime = DateTime.Now;
                    original_seller.ReturnTime = original_seller.ApproveTime = null;
                    VourcherFacade.SellerSave(original_seller);
                }
                else
                {
                    //比對原賣家資料和修改後的資料
                    CheckChangeItem(change_items, original_seller, edited_seller);
                    if (change_items.Count > 0)
                    {
                        //將欄位和值轉為json格式字串
                        string change_json = ChangeItemsToJson(change_items);
                        //儲存json到changelog
                        VourcherFacade.ChangeLogAdd(Seller.Schema.TableName, View.SellerGuid, change_json, true);
                        //改變TempStatus為申請修改Applied
                        original_seller.TempStatus = (int)SellerTempStatus.Applied;
                        original_seller.ApplyId = View.UserName;
                        original_seller.ApplyTime = DateTime.Now;
                        original_seller.ReturnTime = original_seller.ApproveTime = null;
                        logo_imgs = original_seller.SellerLogoimgPath.Split('|').ToList();

                        if (!string.IsNullOrEmpty(photos[0].DestFileName))
                        {
                            photos[0].DestFilePath = original_seller.SellerId;
                            ImageUtility.UploadFile(photos[0].PFile.ToAdapter(), photos[0].Type, photos[0].DestFilePath, photos[0].DestFileName);
                            if (logo_imgs.Count > 0)
                            {
                                if (logo_imgs[0].IndexOf(',') >= 0)
                                {
                                    string[] filePath = logo_imgs[0].Split(',');
                                    foreach (var item in folder)
                                    {
                                        ImageUtility.DeleteFile(photos[0].Type, filePath[0], Path.Combine(item, filePath[1]));
                                    }
                                }
                                logo_imgs[0] = (ImageFacade.GenerateMediaPath(original_seller.SellerId, photos[0].DestFileName + "." + Helper.GetExtensionByContentType(photos[0].PFile.ContentType)));
                            }
                            else
                            {
                                logo_imgs.Add(ImageFacade.GenerateMediaPath(original_seller.SellerId, photos[0].DestFileName + "." + Helper.GetExtensionByContentType(photos[0].PFile.ContentType)));
                            }
                        }

                        if (!string.IsNullOrEmpty(photos[1].DestFileName))
                        {
                            photos[1].DestFilePath = original_seller.SellerId;
                            ImageUtility.UploadFile(photos[1].PFile.ToAdapter(), photos[1].Type, photos[1].DestFilePath, photos[1].DestFileName);
                            if (logo_imgs.Count > 1)
                            {
                                if (logo_imgs[1].IndexOf(',') >= 0)
                                {
                                    string[] filePath = logo_imgs[1].Split(',');
                                    foreach (var item in folder)
                                    {
                                        ImageUtility.DeleteFile(photos[1].Type, filePath[0], Path.Combine(item, filePath[1]));
                                    }
                                }
                                logo_imgs[1] = ImageFacade.GenerateMediaPath(original_seller.SellerId, photos[1].DestFileName + "." + Helper.GetExtensionByContentType(photos[1].PFile.ContentType));
                            }
                            else if (logo_imgs.Count == 0)
                            {
                                logo_imgs.Add(string.Empty);
                                logo_imgs.Add(ImageFacade.GenerateMediaPath(original_seller.SellerId, photos[1].DestFileName + "." + Helper.GetExtensionByContentType(photos[1].PFile.ContentType)));
                            }
                            else if (logo_imgs.Count == 1)
                            {
                                logo_imgs.Add(ImageFacade.GenerateMediaPath(original_seller.SellerId, photos[1].DestFileName + "." + Helper.GetExtensionByContentType(photos[1].PFile.ContentType)));
                            }
                        }

                        if (logo_imgs.Count > 0)
                        {
                            original_seller.SellerLogoimgPath = logo_imgs.Aggregate((current, next) => current + "|" + next);
                        }
                        VourcherFacade.SellerSave(original_seller);
                        changeitems = change_json;
                    }
                    else
                    {
                        //如和原資料相同，將changelog的異動資料enabled欄位設為false
                        VourcherFacade.ChangeLogUpdateEnabled(Seller.Schema.TableName, View.SellerGuid, false);
                    }
                }
                //將賣家資料設定置view的property
                View.SellerGuid = original_seller.Guid;
                View.SellerId = original_seller.SellerId;
                //地理座標必須經資料庫存取，無法直接由前台輸入移轉
                original_seller = VourcherFacade.SellerGetByGuid(original_seller.Guid);
                StoreCollection tempstores = VourcherFacade.StoreCollectionGetBySellerGuid(View.SellerGuid);
                //回傳賣家、分店和賣家異動資料
                ChangeLogCollection changelogs = VourcherFacade.ChangeLogCollectionGetAll(Seller.Schema.TableName, View.SellerGuid);
                View.SetSellerStores(original_seller, tempstores, changelogs);
            }
            else
            {
                edited_seller.Department = (int)DepartmentTypes.Ppon;
                edited_seller.SellerId = SellerFacade.GetNewSellerID();
                edited_seller.ApplyId = edited_seller.CreateId = View.UserName;
                edited_seller.ApplyTime = edited_seller.CreateTime = DateTime.Now;
                edited_seller.NewCreated = true;
                edited_seller.TempStatus = (int)SellerTempStatus.Applied;
                SellerCollection repeat_sellers = VourcherFacade.SearchRepeatSellers(edited_seller.SellerName, edited_seller.SignCompanyID, edited_seller.CompanyBossName);
                if (repeat_sellers.Count > 0)
                {
                    View.ShowRepeaterSeller(repeat_sellers);
                    return;
                }
                else
                {
                    logo_imgs = new List<string>();
                    if (!string.IsNullOrEmpty(photos[0].DestFileName))
                    {
                        photos[0].DestFilePath = edited_seller.SellerId;
                        ImageUtility.UploadFile(photos[0].PFile.ToAdapter(), photos[0].Type, photos[0].DestFilePath, photos[0].DestFileName);
                        logo_imgs.Add(ImageFacade.GenerateMediaPath(edited_seller.SellerId, photos[0].DestFileName + "." + Helper.GetExtensionByContentType(photos[0].PFile.ContentType)));
                    }

                    if (!string.IsNullOrEmpty(photos[1].DestFileName))
                    {
                        photos[1].DestFilePath = edited_seller.SellerId;
                        ImageUtility.UploadFile(photos[1].PFile.ToAdapter(), photos[1].Type, photos[1].DestFilePath, photos[1].DestFileName);
                        if (logo_imgs.Count == 0)
                        {
                            logo_imgs.Add(string.Empty);
                            logo_imgs.Add(ImageFacade.GenerateMediaPath(edited_seller.SellerId, photos[1].DestFileName + "." + Helper.GetExtensionByContentType(photos[1].PFile.ContentType)));
                        }
                        else if (logo_imgs.Count == 1)
                        {
                            logo_imgs.Add(ImageFacade.GenerateMediaPath(edited_seller.SellerId, photos[1].DestFileName + "." + Helper.GetExtensionByContentType(photos[1].PFile.ContentType)));
                        }
                    }

                    if (logo_imgs.Count > 0)
                    {
                        edited_seller.SellerLogoimgPath = logo_imgs.Aggregate((current, next) => current + "|" + next);
                    }

                    VourcherFacade.SellerSave(edited_seller);

                    //將異動資料轉為json，欄位和異動值
                    string change_json = ChangeItemsToJson(new KeyValuePair<string, string>("NewCreated", "前台新增賣家"));
                    //存儲異動資料
                    VourcherFacade.ChangeLogAdd(Seller.Schema.TableName, edited_seller.Guid, change_json, false);
                    original_seller = VourcherFacade.SellerGetByGuid(edited_seller.Guid);
                    View.SellerGuid = edited_seller.Guid;
                    View.SellerId = edited_seller.SellerId;
                    StoreCollection tempstores = VourcherFacade.StoreCollectionGetBySellerGuid(View.SellerGuid);
                    //回傳賣家、分店和賣家異動資料
                    View.SetSellerStores(original_seller, tempstores, new ChangeLogCollection());
                }
            }
            EventArgs ee = new EventArgs();
            OnGetTempStatusSellerStore(sender, ee);
        }

        /// <summary>
        /// 儲存分店資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">分店資料</param>
        private void OnSaveTempStore(object sender, DataEventArgs<Store> e)
        {
            string changeitems = string.Empty;
            Store tempstore = new Store();
            //是否為已存在的分店
            if (View.StoreGuid != Guid.Empty)
            {
                Dictionary<string, string> change_items = new Dictionary<string, string>();
                tempstore = VourcherFacade.StoreGetByGuid(View.StoreGuid);

                if (tempstore.NewCreated)
                {
                    CheckChangeItem(tempstore, e.Data);
                    tempstore.TempStatus = (int)SellerTempStatus.Applied;
                    tempstore.ApplyId = View.UserName;
                    tempstore.ApplyTime = DateTime.Now;
                    tempstore.ReturnTime = tempstore.ApproveTime = null;
                    VourcherFacade.StoreSave(tempstore);
                }
                else
                {
                    //比對原資料和異動資料
                    CheckChangeItem(change_items, tempstore, e.Data);
                    //是否有異動資料
                    if (change_items.Count > 0)
                    {
                        //將異動資料轉為json，欄位和異動值
                        string change_json = ChangeItemsToJson(change_items);
                        //存儲異動資料
                        VourcherFacade.ChangeLogAdd(Store.Schema.TableName, View.StoreGuid, change_json, true);
                        //分店狀態改為Applied
                        tempstore.TempStatus = (int)SellerTempStatus.Applied;
                        tempstore.ApplyId = View.UserName;
                        tempstore.ApplyTime = DateTime.Now;
                        tempstore.ReturnTime = tempstore.ApproveTime = null;
                        VourcherFacade.StoreSave(tempstore);
                        //異動資料紀錄(json)
                        changeitems = change_json;
                    }
                    else
                    {
                        //和原資料相同，將changelog異動資料disabled
                        VourcherFacade.ChangeLogUpdateEnabled(Store.Schema.TableName, View.StoreGuid, false);
                    }
                } //設定分店Guid
                View.StoreGuid = tempstore.Guid;
            }
            else
            {
                e.Data.ApplyId = e.Data.CreateId = View.UserName;
                e.Data.ApplyTime = e.Data.CreateTime = DateTime.Now;
                e.Data.NewCreated = true;
                e.Data.TempStatus = (int)SellerTempStatus.Applied;
                e.Data.SellerGuid = View.SellerGuid;
                VourcherFacade.StoreSave(e.Data);
                //將異動資料轉為json，欄位和異動值
                string change_json = ChangeItemsToJson(new KeyValuePair<string, string>("NewCreated", "前台新增分店"));
                //存儲異動資料
                VourcherFacade.ChangeLogAdd(Store.Schema.TableName, e.Data.Guid, change_json, false);

                View.StoreGuid = e.Data.Guid;
            }
            Seller tempseller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
            StoreCollection tempstores = VourcherFacade.StoreCollectionGetBySellerGuid(View.SellerGuid);
            ChangeLogCollection changelogs = VourcherFacade.ChangeLogCollectionGetAll(Store.Schema.TableName, View.SellerGuid);
            //回傳賣家、分店和異動資料
            View.SetSellerStores(tempseller, tempstores, changelogs);
            EventArgs ee = new EventArgs();
            OnGetTempStatusSellerStore(sender, ee);
        }

        #endregion event

        #region local method

        private void SearchReturnCase()
        {
            SellerCollection sellers_applied = VourcherFacade.SellerCollectionReturnCase(View.UserName, SellerTempStatus.Applied);
            SellerCollection sellers_returned = VourcherFacade.SellerCollectionReturnCase(View.UserName, SellerTempStatus.Returned);
            sellers_applied.AddRange(sellers_returned);
            StoreCollection stores_applied = VourcherFacade.StoreCollectionReturnCase(View.UserName, SellerTempStatus.Applied);
            StoreCollection stores_returned = VourcherFacade.StoreCollectionReturnCase(View.UserName, SellerTempStatus.Returned);
            stores_applied.AddRange(stores_returned);
            View.GetReturnCase(sellers_applied, stores_applied);
        }

        /// <summary>
        /// 搜尋賣家依分頁撈取
        /// </summary>
        /// <param name="page">頁數</param>
        private void LoadSellerCollection(int page)
        {
            SellerCollection sellers = new SellerCollection();
            //搜尋賣家
            sellers = VourcherFacade.SearchSellerInfo(page, View.PageSize, View.SearchKeys, string.Empty, string.Empty);
            //回傳賣家資料
            View.SetSellerCollection(sellers);
        }

        /// <summary>
        /// 異動資料轉為json格式字串
        /// </summary>
        /// <param name="changeitems">欄位名稱和異動值</param>
        /// <returns></returns>
        private string ChangeItemsToJson(Dictionary<string, string> changeitems)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                foreach (var item in changeitems)
                {
                    writer.WritePropertyName(item.Key);
                    writer.WriteValue(item.Value);
                }
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private string ChangeItemsToJson(KeyValuePair<string, string> changeitems)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName(changeitems.Key);
                writer.WriteValue(changeitems.Value);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        /// <summary>
        /// 比對賣家資料，將異動轉為欄位名稱和資料儲存
        /// </summary>
        /// <param name="changeitems">異動欄位名稱和資料</param>
        /// <param name="original_seller">原賣家資料</param>
        /// <param name="edited_seller">編輯後的賣家資料</param>
        private void CheckChangeItem(Dictionary<string, string> changeitems, Seller original_seller, Seller edited_seller)
        {
            foreach (var item in seller_columns)
            {
                string original_string = original_seller.GetColumnValue(item) == null ? string.Empty : original_seller.GetColumnValue(item).ToString();
                string edited_string = edited_seller.GetColumnValue(item) == null ? string.Empty : edited_seller.GetColumnValue(item).ToString();
                if (!original_string.Equals(edited_string))
                {
                    changeitems[item] = edited_string;
                }
            }
        }

        /// <summary>
        /// 比對分店資料，將異動轉為欄位名稱和資料儲存
        /// </summary>
        /// <param name="changeitems">異動欄位名稱和資料</param>
        /// <param name="original_store">原分店資料</param>
        /// <param name="edited_store">編輯後的分店資料</param>
        private void CheckChangeItem(Dictionary<string, string> changeitems, Store original_store, Store edited_store)
        {
            foreach (var item in store_columns)
            {
                string original_string = original_store.GetColumnValue(item) == null ? string.Empty : original_store.GetColumnValue(item).ToString();
                string edited_string = edited_store.GetColumnValue(item) == null ? string.Empty : edited_store.GetColumnValue(item).ToString();
                if (!original_string.Equals(edited_string))
                {
                    changeitems[item] = edited_string;
                }
            }
        }

        private void CheckChangeItem(Seller original_seller, Seller edited_seller)
        {
            foreach (var item in seller_columns)
            {
                original_seller.SetColumnValue(item, edited_seller.GetColumnValue(item));
            }
        }

        private void CheckChangeItem(Store original_store, Store edited_store)
        {
            foreach (var item in store_columns)
            {
                original_store.SetColumnValue(item, edited_store.GetColumnValue(item));
            }
        }
        #endregion local method
    }
}
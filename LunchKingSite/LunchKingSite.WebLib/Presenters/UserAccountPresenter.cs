﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserAccountPresenter : Presenter<IUserAccount>
    {
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetUpInit();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetAddressInfo += OnGetAddressInfo;
            View.CityChange += OnCityChange;
            View.AreaChange += OnAreaChange;
            View.BuildingChange += OnBuildingChange;
            View.UpdateMemberInfo += OnUpdateMemberInfo;
            View.UpdateEinvoiceTriple += OnUpdateEinvoiceTriple;
            View.UpdateWinEinvoice += OnUpdateWinEinvoice;
            View.UpdateSubscription += OnUpdateSubscription;
            View.AddCreditCard += OnAddCreditCard;
            View.DeleteCreditCard += OnDeleteCreditCard;
            View.UpdateMemberDelivery += OnUpdateMemberDelivery;
            View.DeleteDeliveryInfo += OnDeleteDeliveryInfo;
            View.UpdateMemberPassword += OnUpdateMemberPassword;
            View.ChangeMemberEmail += OnChangeMemberEmail;
            View.ChangeMemberEmailCancel += OnChangeMemberEmailCancel;
            View.ChangeMemberEmailResend += OnChangeMemberEmailResend;
            View.UpdateMemberEInvoiceInfo += OnUpdateMemberEInvoiceInfo;
            View.BindingMemberEmailResend += OnBindingMemberEmailResend;
            View.BindingMemberEmailCancel += OnBindingMemberEmailCancel;
            return true;
        }

        private void OnDeleteDeliveryInfo(object sender, EventArgs e)
        {
            mp.MemberDeliveryDelete(View.MID.ToString());
            SetUpInit();
        }

        private void OnUpdateMemberDelivery(object sender, DataEventArgs<MemberDelivery> e)
        {
            e.Data.ModifyId = MemberFacade.GetShortUserName(View.UserName);
            MemberUtility.UpdateMemberDeliveryOnly(e.Data);
            SetUpInit();
        }

        private void OnUpdateEinvoiceTriple(object sender, DataEventArgs<EinvoiceTriple> e)
        {
            mp.SaveEinvoiceTriple(e.Data);
            SetUpInit();
        }
        private void OnUpdateWinEinvoice(object sender, DataEventArgs<EinvoiceWinner> e)
        {
            mp.SaveEinvoiceWinner(e.Data);
            int tmpCount = DateTime.Now.Month % 2 > 0 ? 4 : 5;
            string msg = "您的資料已儲存成功!";
            DateTime tmpDateTime = DateTime.Now;
            tmpDateTime = tmpDateTime.AddMonths(-tmpCount).GetFirstDayOfMonth();
            var winnerData = op.EinvoiceMainByUserId(e.Data.UserId, tmpDateTime, DateTime.Now);
            if (winnerData.Count > 0)
            {
                var paperedList = winnerData.Where(x => x.InvoicePaperedTime != null);
                var noPaperedList = winnerData.Where(x => x.InvoicePaperedTime == null);
                var paperedStr = "";
                var noPaperedStr = "";

                KeyValuePair<DateTime, DateTime> kp = GetDates();
                op.EinvoiceUpdateWinner(e.Data.UserName, e.Data.Mobile, e.Data.Adress, View.UserId, kp.Key, kp.Value);

                if (noPaperedList.Any())
                {
                    foreach (var d in noPaperedList)
                    {
                        noPaperedStr += string.IsNullOrEmpty(noPaperedStr) ? d.InvoiceNumber : "," + d.InvoiceNumber;
                    }
                }

                if (paperedList.Any())
                {
                    foreach (var d in paperedList)
                    {
                        paperedStr += string.IsNullOrEmpty(paperedStr) ? d.InvoiceNumber : "," + d.InvoiceNumber;
                    }
                }

                if ((!string.IsNullOrEmpty(noPaperedStr)) && (!string.IsNullOrEmpty(paperedStr)))
                {
                    msg += @"\n\n"+ "中獎發票號碼："+ noPaperedStr + @"，也將會使用您更新的新地址，寄發給您。\n\n" 
                        + @"請注意，\n"+ "您有"+ paperedList.Count() + @"筆中獎發票已經進入寄發的程序。\n" + "中獎發票號碼:"+ paperedStr
                        +"此中獎發票，將會寄送到原填寫的寄送地址。若有任何疑問或需要洽詢，請聯繫17life客服。";
                }
                else if (!string.IsNullOrEmpty(paperedStr))
                {
                    msg += @"\n\n" + @"請注意，\n" + "您有" + paperedList.Count() + @"筆中獎發票已經進入寄發的程序。\n" + "中獎發票號碼:" + paperedStr
                           + "此中獎發票，將會寄送到原填寫的寄送地址。若有任何疑問或需要洽詢，請聯繫17life客服。";
                }
                else
                {
                    msg += @"\n\n" + "中獎發票號碼：" + noPaperedStr + @"，也將會使用您更新的新地址，寄發給您。\n\n";
                }

            }
            
            SetUpInit();
            View.ShowMessage(msg);
        }

        private void OnUpdateMemberInfo(object sender, DataEventArgs<Member> e)
        {
            Member m = mp.MemberGet(View.UserName);
            m.LastName = e.Data.LastName;
            m.FirstName = e.Data.FirstName;
            m.Gender = e.Data.Gender;
            m.Birthday = e.Data.Birthday;
            m.Mobile = e.Data.Mobile;
            m.BuildingGuid = e.Data.BuildingGuid;
            m.CompanyAddress = e.Data.CompanyAddress;
            m.UserEmail = e.Data.UserEmail;

            m.ModifyId = View.UserName;
            m.ModifyTime = DateTime.Now;
            MemberUtility.UpdateMemberInfo(m);

            SetUpInit();
        }

        /// <summary>
        /// 更新密碼
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">[0]:原密碼, [1]:新密碼</param>
        private void OnUpdateMemberPassword(object sender, DataEventArgs<List<string>> e)
        {
            Member m = mp.MemberGet(View.UserName);
            bool updateMemberPasswordResult = false;
            if (m != null && m.IsLoaded)
            {
                updateMemberPasswordResult = MemberFacade.ResetMemberPassword(m.UserName, e.Data[0], e.Data[1],
                    ResetPasswordReason.MemberChange);

                if (updateMemberPasswordResult)
                {
                    MemberAuthInfo mai = mp.MemberAuthInfoGet(m.UniqueId);
                    if (mai.IsLoaded)
                    {
                        mai.ForgetPasswordCode = null;
                        mai.ForgetPasswordKey = null;
                        mai.PasswordQueryTime = null;
                        mp.MemberAuthInfoSet(mai);
                    }
                }
            }
            View.SetPasswordUpdateResult(updateMemberPasswordResult);
        }

        private void OnChangeMemberEmail(object sender, DataEventArgs<string> e)
        {
            var changeResult = NewMemberUtility.ChangeEmailAuth(View.UserId, e.Data);
            SetUpInit();
            if (changeResult != ReAuthStatus.OK)
            {
                View.SetChangeEmailError(changeResult);
            }
        }

        private void OnChangeMemberEmailCancel(object sender, EventArgs e)
        {
            NewMemberUtility.ChangeEmailCancel(View.UserId);
            SetUpInit();
        }

        private void OnChangeMemberEmailResend(object sender, EventArgs e)
        {
            var changeResult = NewMemberUtility.ChangeEmailResend(View.UserId);
            SetUpInit();
            View.SetChangeEmailAlert(changeResult);
        }

        private void OnBindingMemberEmailResend(object sender, EventArgs e)
        {
            var changeResult = NewMemberUtility.BindingEmailResend(View.UserId);
            SetUpInit();
            View.SetBindEmailAlert(changeResult);
        }

        private void OnBindingMemberEmailCancel(object sender, EventArgs e)
        {
            NewMemberUtility.BindEmailCancel(View.UserId);
            SetUpInit();
        }

        private void OnUpdateMemberEInvoiceInfo(object sender, DataEventArgs<LunchKingSite.BizLogic.Model.PponDeliveryInfo> e)
        {
            MemberFacade.MemberPurchaserSet(View.UserName, "", "", "", "", 0, "", e.Data.CarrierType, e.Data.CarrierId, e.Data.InvoiceBuyerName, e.Data.InvoiceBuyerAddress);
            SetUpInit();
        }

        private void OnUpdateSubscription(object sender, EventArgs e)
        {
            foreach (KeyValuePair<int, bool> item in View.SubscribeRegions)
            {
                MemberFacade.SubscribeCheckSet(View.UserName, CategoryManager.Default.Find(item.Key), item.Value);
            }

            Member m = mp.MemberGet(View.UserName);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveEDM, m.EdmType, MemberEdmType.PponEvent);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveEDM, m.EdmType, MemberEdmType.PiinlifeEvent);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveStartedSell, m.EdmType, MemberEdmType.PponStartedSell);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveStartedSell, m.EdmType, MemberEdmType.PiinlifeStartedSell);
            
            mp.MemberSet(m);

            SetUpInit();
        }

        private void OnAddCreditCard(object sender, DataEventArgs<UserEditCreditCard> e)
        {
            string errorMsg;
            bool result =MemberFacade.AddOrUpdateMemberCreditCard(Guid.Empty,
                View.UserName, e.Data.Number, e.Data.Year, e.Data.Month, e.Data.Name, out errorMsg);
            if(!result)
            {
                View.SetDeleteCreditCardAlert(result, errorMsg);
            }
                        
            SetUpInit();            
        }

        private void OnDeleteCreditCard(object sender, DataEventArgs<Guid> e)
        {
            string errorMessage;
            bool result = MemberFacade.DeleteMemberCreditCard(View.UserName, e.Data, out errorMessage);
            SetUpInit();
            View.SetDeleteCreditCardAlert(result, errorMessage);
        }

        private void OnBuildingChange(object sender, DataEventArgs<Guid> e)
        {
            Building building = new Building();
            if (e.Data != new Guid("00000000-0000-0000-0000-000000000000"))
            {
                building = lp.BuildingGet(e.Data);
            }
            View.GetBuildingChange(building);
        }

        private void OnAreaChange(object sender, DataEventArgs<int> e)
        {
            ListItemCollection buildings = new ListItemCollection();
            if (e.Data == -1)
            {
                buildings.Add(new ListItem("請選擇", "00000000-0000-0000-0000-000000000000"));
            }
            else
            {
                buildings = lp.BuildingGetListInZoneAndPrefix(e.Data, "");
            }

            View.GetAreaChange(buildings);
        }

        private void OnCityChange(object sender, DataEventArgs<int> e)
        {
            CityCollection cities = new CityCollection();
            if (e.Data == -1)
            {
                cities.Add(new City() { CityName = "請選擇", Id = -1 });
            }
            else
            {
                cities = lp.CityGetList(e.Data);
            }

            View.GetCityChange(cities);
        }

        private void OnGetAddressInfo(object sender, DataEventArgs<KeyValuePair<Guid, string>> e)
        {
            Building bld = lp.BuildingGet(e.Data.Key);
            City city = lp.CityGet(bld.CityId);
            View.SetAddressInfo(e.Data.Value, bld, city);
        }

        private void SetUpInit()
        {
            View.Mode = true;
            var tmpData =
                CityManager.Citys.Where(x => !x.Code.Equals("sys", StringComparison.OrdinalIgnoreCase)).ToList();
            View.SetDropDownList(tmpData);
            Dictionary<int, string> cityList = new Dictionary<int, string>();
            foreach (var d in tmpData)
            {
                if (d.Id > 0)
                {
                    cityList.Add(d.Id, d.CityName);
                }
            }
            View.CityListStr = JsonConvert.SerializeObject(cityList);
            Member m = mp.MemberGet(View.UserName);
            MemberAuthInfo mai = mp.MemberAuthInfoGet(View.UserId);
            MemberDeliveryCollection mdc = mp.MemberDeliveryGetCollection(View.UserId);
            MemberLinkCollection mlc = mp.MemberLinkGetList(View.UserId);
            EinvoiceTriple einvoiceTripleData = MemberFacade.GetEinvoiceTriple(View.UserId);
            EinvoiceWinner winnerData = MemberFacade.EinvoiceWinnerGetByUserid(View.UserId);
            bool setPassword = mlc.Any(
                t => t.ExternalOrg == (int)SingleSignOnSource.ContactDigitalIntegration ||
                    t.ExternalOrg == (int)SingleSignOnSource.Mobile17Life
                );
            View.GetMemberInfo(m, mdc, setPassword, mlc, mai);
            View.SetPponCityGroupList(CategoryManager.CategoryNodesForSubscription);
            View.SetEinvoiceTriple(einvoiceTripleData);
            View.SetEinvoiceWinner(winnerData);
            MemberCreditCardCollection mccs = mp.MemberCreditCardGetList(View.UserId);
            var isTaishinMall = ChannelFacade.GetOrderClassificationByHeaderToken() == AgentChannel.TaiShinMall;
                List<dynamic> creditCards = new List<dynamic>();
            foreach (MemberCreditCard mcc in mccs)
            {

                if (isTaishinMall && mcc.BankId != config.TaishinCreditCardBankId) continue;

                dynamic dy = new
                {
                    mcc.Guid,
                    mcc.CreditCardTail,
                    mcc.CreditCardYear,
                    mcc.CreditCardMonth,
                    mcc.CreditCardName
                };
                creditCards.Add(dy);
            }


            View.SetCreditCardForm(creditCards);

            Dictionary<int, bool> regions = new Dictionary<int, bool>();
            IList<Subscription> subCol = pp.SubscriptionGetList(View.UserName);
            if (subCol.Count > 0)
            {
                foreach (Subscription subscribe in subCol)
                {
                    if (regions.ContainsKey(subscribe.CategoryId) == false)
                    {
                        if (Helper.IsFlagSet((SubscribeType)subscribe.Status, SubscribeType.Unsubscribe))
                        {
                            regions.Add(subscribe.CategoryId, false);
                        }
                        else
                        {
                            regions.Add(subscribe.CategoryId, true);
                        }
                    }
                }
            }
            View.SubscribeRegions = regions;
        }

        #region method
        protected KeyValuePair<DateTime, DateTime> GetDates()
        {
            DateTime now = DateTime.Now;
            DateTime d_start = new DateTime();
            DateTime d_end = new DateTime();
            if (now.Month % 2 == 1)
            {
                d_start = new DateTime(now.AddMonths(-4).Year, now.AddMonths(-4).Month, 1);
                d_end = d_start.AddMonths(4);
            }
            else
            {
                d_start = new DateTime(now.AddMonths(-5).Year, now.AddMonths(-5).Month, 1);
                d_end = d_start.AddMonths(5);
            }
            return new KeyValuePair<DateTime, DateTime>(d_start, d_end);
        }
        #endregion
    }
}
﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Transactions;

namespace LunchKingSite.WebLib.Presenters
{
    public class TmallPresenter : Presenter<ITmallView>
    {
        protected static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static IVerificationProvider vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        protected static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        protected static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected static ILog Logger = LogManager.GetLogger("TmallPresenter");
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        private static object CouponLock = new object();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.GetFailSmsList(op.ViewOrderCorrespondingSmsLogGetFailList());

            //View.GetFailSmsList(op.ViewOrderCorrespondingSmsLogGetList(1, View.PageSize, ViewOrderCorrespondingSmsLog.Columns.CreatedTime, GetFailSmsFilter()));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchDealByBid += OnSearchDealByBid;
            View.SearchDealByUid += OnSearchDealByUid;
            View.MakeTmallOrder += OnMakeTmallOrder;
            View.SearchList += OnSearchList;
            View.PageChange += OnPageChange;
            View.ReSendCouponSms += OnReSendCouponSms;
            return true;
        }

        #region event
        public void OnSearchList(object sender, EventArgs e)
        {
            View.PageCount = op.ViewOrderCorrespondingSmsLogListGetCount(View.BusinessHourGuid, GetSearchFilter());
            View.SetPageCount();
            View.GetSearchList(op.ViewOrderCorrespondingSmsLogGetList(1, View.PageSize, ViewOrderCorrespondingSmsLog.Columns.CreatedTime, View.BusinessHourGuid, GetSearchFilter()));
        }

        public void OnSearchDealByUid(object sender, DataEventArgs<int> e)
        {
            SetDealDetialContent(pp.ViewPponDealGetByUniqueId(e.Data));
        }

        public void OnSearchDealByBid(object sender, DataEventArgs<Guid> e)
        {
            SetDealDetialContent(pp.ViewPponDealGetByBusinessHourGuid(e.Data));
        }

        public void OnPageChange(object sender, DataEventArgs<int> e)
        {
            View.GetSearchList(op.ViewOrderCorrespondingSmsLogGetList(e.Data, View.PageSize, ViewOrderCorrespondingSmsLog.Columns.CreatedTime + " desc", View.BusinessHourGuid, GetSearchFilter()));
        }

        public void OnReSendCouponSms(object sender, DataEventArgs<int> e)
        {
            ViewPponCoupon view_ppon_coupon = pp.ViewPponCouponGet(e.Data);
            OrderCorresponding order_corresponding = op.OrderCorrespondingListGetByOrderGuid(view_ppon_coupon.OrderGuid);
            SendSmsProcess(view_ppon_coupon, order_corresponding.Mobile, order_corresponding.Memo);
            View.GetFailSmsList(op.ViewOrderCorrespondingSmsLogGetFailList());
            //View.GetFailSmsList(op.ViewOrderCorrespondingSmsLogGetList(1, View.PageSize, ViewOrderCorrespondingSmsLog.Columns.CreatedTime, GetFailSmsFilter()));
            View.ShowErrorMessage("已重新發送");
        }

        public void OnMakeTmallOrder(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);
            Regex r_order = new Regex("[0-9]{15}");
            Regex r_mobile = new Regex("[0-9]{11}");
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) == 0)
            {
                View.ShowErrorMessage("此檔非天貓檔次");
                return;
            }
            else if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                View.ShowErrorMessage("此檔為多檔次母檔，請重新輸入子檔");
                return;
            }
            else if (deal.BusinessHourOrderTimeE < DateTime.Now)
            {
                View.ShowErrorMessage("已過銷售時間");
                return;
            }
            else if (deal.ItemPrice != 0)
            {
                View.ShowErrorMessage("天貓須為0元好康檔");
                return;
            }
            else if (!r_order.IsMatch(e.Data.Key))
            {
                View.ShowErrorMessage("訂單號碼格式錯誤");
                return;
            }
            else if (!r_mobile.IsMatch(e.Data.Value))
            {
                View.ShowErrorMessage("手機號碼格式錯誤");
                return;
            }
            else if (!(e.Data.Value.StartsWith("1") || e.Data.Value.StartsWith("8525")
                || e.Data.Value.StartsWith("8526") || e.Data.Value.StartsWith("8529") || e.Data.Value.StartsWith("853")))
            {
                View.ShowErrorMessage("手機號碼開頭為1、852(5、6、9)或853");
                return;
            }
            //檢查是否有相同的天貓訂單號碼
            OrderCorrespondingCollection occ = op.OrderCorrespondingListGet(new string[] { OrderCorresponding.Columns.RelatedOrderId + "=" + e.Data.Key, OrderCorresponding.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid });
            if (occ.Count > 0)
            {
                View.ShowErrorMessage("此天貓訂單號碼已存在，對應的P好康訂單號碼為" + occ.First().OrderId);
                return;
            }

            View.DefaultUserName = config.TmallDefaultUserName;

            //判斷是否為宅配
            bool isItem = deal.DeliveryType.HasValue && int.Equals((int)DeliveryType.ToHouse, deal.DeliveryType.Value);
            string mobile = e.Data.Value.StartsWith("1") ? ("86" + e.Data.Value) : e.Data.Value;
            string ticketId = OrderFacade.MakeRegularTicketId(View.SessionId, View.BusinessHourGuid) + "_" + Path.GetRandomFileName().Replace(".", string.Empty);
            //天貓預設為0元好康檔
            PponDeliveryInfo info = new PponDeliveryInfo();
            info.EntrustSell = View.DefaultEntrustSell;
            info.StoreGuid = View.SelectedStoreGuid;
            info.DiscountCode = string.Empty;
            info.SellerType = isItem ? DepartmentTypes.PponItem : DepartmentTypes.Ppon;//View.DefaultSellerType;
            info.DeliveryType = isItem ? DeliveryType.ToHouse : DeliveryType.ToShop; // View.DefaultDeliveryType;
            info.IsForFriend = false;
            info.ReceiptsType = DonationReceiptsType.None;
            info.BonusPoints = 0;
            info.DeliveryDateString = string.Empty;
            info.DeliveryTimeString = string.Empty;
            info.Notes = string.Empty;
            info.PayEasyCash = 0;
            info.SCash = 0;
            info.Quantity = View.DefaultQuanty;
            info.WriteAddress = View.DefaultDeliveryAddress;
            info.WriteName = View.DefaultWriteName;
            info.Phone = mobile;

            JsonSerializer serializer = new JsonSerializer();
            string infoStr = serializer.Serialize(info);
            List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(View.UserMemo, ticketId, deal, View.SelectedStoreGuid, infoStr);

            try
            {
                OrderFacade.PutItemToCart(ticketId, ies);

                #region 把資料放到暫存Session中....找時間改寫成Class

                TempSession ts = new TempSession();
                ts.SessionId = ticketId;
                ts.Name = "pponContent";
                ts.ValueX = infoStr;
                pp.TempSessionSetForUpdate(ts);

                #endregion 把資料放到暫存Session中....找時間改寫成Class
            }
            catch (Exception ex)
            {
                Logger.Error(string.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", ticketId,
                    View.BusinessHourGuid), ex);
                throw;
            }

            #region with ticketid
            TempSessionCollection tsc = pp.TempSessionGetList(ticketId);
            if (tsc.Count <= 0)
            {
                View.ShowErrorMessage("發生錯誤");
                return;
            }
            //直接用上面的PponDeliveryInfo 不再反序列化
            //PponDeliveryInfo deliveryInfo = serializer.Deserialize<PponDeliveryInfo>(tsc[0].ValueX);

            string TransactionId = PaymentFacade.GetTransactionId();

            #region New Version Order Start
            DeliveryInfo di = new DeliveryInfo();
            di.DeliveryCharge = View.DefaultDeliveryCharge;
            string usermemo = info.DeliveryDateString + info.DeliveryTimeString + info.Notes;
            //將送貨地址改為天貓
            if (isItem)
            {
                di.CustomerName = info.WriteName;
                di.CustomerMobile = info.Phone;
                di.DeliveryAddress = info.WriteAddress;
            }
            else
            {
                di.DeliveryAddress = info.WriteAddress;
            }
            // make order
            Guid oid = OrderFacade.MakeOrder(View.DefaultUserName, ticketId, DateTime.Now,
                usermemo, di, deal, OrderFromType.ByWeb);

            Order o = op.OrderGet(oid);
            o.OrderMemo = ((int)info.ReceiptsType.GetValueOrDefault()).ToString() + "|" + info.InvoiceType + "|" + info.UnifiedSerialNumber + "|" + info.CompanyTitle;
            op.OrderSet(o);

            #endregion New Version Order Start

            #region 天貓0元檔次 PaymentType用ByPass
            OrderFacade.MakeTransaction(TransactionId, LunchKingSite.Core.PaymentType.ByPass, 0, (DepartmentTypes)deal.Department, View.DefaultUserName, o.Guid);
            #endregion For test purpose

            #region with transid
            PaymentTransactionCollection ptc = op.PaymentTransactionGetListByTransIdAndTransType(TransactionId, PayTransType.Authorization);

            PayTransResponseType theResponseType = PayTransResponseType.OK;
            //LunchKingSite.Core.PaymentType errorPaymentType = LunchKingSite.Core.PaymentType.PCash;
            //List<LunchKingSite.Core.PaymentType> paymentTypes = new List<LunchKingSite.Core.PaymentType>();

            foreach (var pt in ptc)
            {
                //paymentTypes.Add((LunchKingSite.Core.PaymentType)pt.PaymentType);

                if (pt.Result != (int)PayTransResponseType.OK)
                {
                    theResponseType = PayTransResponseType.GenericError;
                    //errorPaymentType = (LunchKingSite.Core.PaymentType)pt.PaymentType;
                }
            }

            if (theResponseType == PayTransResponseType.OK)
            {
                bool orderOK = false;
                o = op.OrderGet(ptc.First().OrderGuid.Value);
                OrderDetailCollection odCol = op.OrderDetailGetList(1, 1, string.Empty, o.Guid, OrderDetailTypes.Regular);

                int quantity = odCol.Sum(x => x.ItemQuantity);

                using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    pp.TempSessionDelete(ticketId);
                    #region 扣除選項
                    tsc = pp.TempSessionGetList("Accessory" + ticketId);
                    foreach (TempSession ts in tsc)
                    {
                        int forOut = 0;
                        AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                        if (!agm.IsLoaded)
                            continue;

                        if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                        {
                            agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                            int? optId = PponOption.FindId(agm.Guid, deal.BusinessHourGuid);
                            if (optId.HasValue)
                            {
                                PponOption opt = pp.PponOptionGet(optId.Value);
                                if (opt != null && opt.IsLoaded)
                                {
                                    opt.Quantity = agm.Quantity;
                                    pp.PponOptionSet(opt);
                                }
                            }
                        }

                        ip.AccessoryGroupMemberSet(agm);
                    }
                    pp.TempSessionDelete("Accessory" + ticketId);

                    #endregion 扣除選項

                    #region 分店銷售數量

                    if (info.StoreGuid != Guid.Empty)
                    {
                        int k = (int)(Math.Ceiling((double)info.Quantity / (deal.ComboPackCount ?? 1)));
                        pp.PponStoreUpdateOrderQuantity(deal.BusinessHourGuid, info.StoreGuid, k);
                    }

                    #endregion 分店銷售數量
                    transScope.Complete();
                    orderOK = true;
                }
                //orderOk 用來判斷付款錯誤 是否要恢復pcash creditcard付款別款項; 因為天貓為0元，就省略判斷
                if (!orderOK)
                {
                    View.ShowErrorMessage("發生錯誤");
                }
            #endregion Transaction Scope
                //if weeklypay deal
                int trust_checkout_type = 0;
                if (((deal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                    trust_checkout_type = (int)TrustCheckOutType.WeeklyPay;

                #region make cash trust log
                Member m = mp.MemberGet(View.DefaultUserName);

                #region payments
                Dictionary<Core.PaymentType, int> payments = new Dictionary<Core.PaymentType, int>();
                if ((int)info.SCash > 0)
                {
                    payments.Add(Core.PaymentType.SCash, (int)info.SCash);
                }
                if ((int)info.PayEasyCash > 0)
                {
                    payments.Add(Core.PaymentType.PCash, (int)info.PayEasyCash);
                }
                if ((int)info.BonusPoints > 0)
                {
                    payments.Add(Core.PaymentType.BonusPoint, (int)info.BonusPoints);
                }
                #endregion payments

                CashTrustInfo cti = new CashTrustInfo
                {
                    OrderGuid = o.Guid,
                    OrderDetails = odCol,
                    CreditCardAmount = 0,
                    SCashAmount = (int)info.SCash,
                    PCashAmount = info.PayEasyCash,
                    BCashAmount = info.BonusPoints,
                    Quantity = quantity,
                    DeliveryCharge = 0,
                    DeliveryType = deal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)deal.DeliveryType.Value,
                    DiscountAmount = 0,
                    AtmAmount = 0,
                    BusinessHourGuid = deal.BusinessHourGuid,
                    CheckoutType = trust_checkout_type,
                    TrustProvider = Helper.GetBusinessHourTrustProvider(deal.BusinessHourStatus),
                    ItemName = deal.ItemName,
                    ItemPrice = (int)deal.ItemPrice,
                    User = m,
                    Payments = payments
                };

                CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);
                #endregion make cash trust log
                // set deal close time
                OrderFacade.PponSetCloseTimeIfJustReachMinimum(deal, quantity);

                // send deal-on mail  //付款成功
                OrderFacade.SetOrderStatus(o.Guid, View.DefaultUserName, OrderStatus.Complete, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = deal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    });
                OrderFacade.SendPponMail(MailContentType.Authorized, o.Guid);
                #region Coupon

                #region 產生天貓和P好康訂單對應表
                //中國手機號碼為1開頭，系統加上86國碼

                OrderCorresponding oc = new OrderCorresponding()
                {
                    CreatedTime = DateTime.Now,
                    UserId = mp.MemberGet(View.UserName).UniqueId,
                    Mobile = mobile,
                    OrderGuid = o.Guid,
                    RelatedOrderId = e.Data.Key.ToString(),
                    OrderId = o.OrderId,
                    BusinessHourGuid = deal.BusinessHourGuid,
                    UniqueId = deal.UniqueId.Value,
                    Memo = View.SmsMemo,
                    Type = (int)OrderClassification.TmallOrder
                };

                op.OrderCorrespondingSet(oc);
                #endregion

                if (!isItem)
                {
                    /*產生coupon並寄信 start*/
                    Coupon c = new Coupon();
                    OrderDetail od = odCol[0];
                    CashTrustLogCollection ctl = OrderFacade.GetTrustByOrderDetail(od.Guid);
                    for (int i = 0; i < od.ItemQuantity; i++)
                    {
                        try
                        {
                            lock (CouponLock)
                            {
                                using (TransactionScope ts = new TransactionScope())
                                {
                                    c.OrderDetailId = (Guid)od.Guid;
                                    //取coupon字數排列與編號
                                    int si = OrderFacade.GetLastCouponSi(deal.BusinessHourGuid.ToString(), deal.BusinessHourStatus);
                                    if (int.Equals(1, si))
                                    {
                                        si += OrderFacade.GetAncestorCouponCount(deal.BusinessHourGuid);
                                    }//產生CouponSequence碼
                                    c.SequenceNumber = OrderFacade.GenerateCouponSequence(o.ParentOrderId.ToString(), View.DefaultUserName, si);
                                    c.Code = OrderFacade.GenerateCouponVerification(c.SequenceNumber, o.ParentOrderId.ToString());

                                    if (od.StoreGuid != null)
                                    {
                                        c.StoreSequence = OrderFacade.GenerateStoreCouponSequence(deal.BusinessHourGuid.ToString(), od.StoreGuid.ToString());
                                    }

                                    c.Id = Convert.ToInt32(OrderFacade.GetCouponID(c));    //get coupon_id for SMS
                                    ts.Complete();
                                }
                            }
                            //更新cash_trust_log裡的coupon_id,sequence_number
                            OrderFacade.SaveTrustCoupon(ctl[i], c.Id, c.SequenceNumber);


                            #region send SMS 不走workflow
                            SendSmsProcess(deal, c, mobile, od.Guid, View.SmsMemo);
                            #endregion

                        }
                        catch (Exception error)
                        {
                            Logger.Error("Something wrong while trying to make Coupon: " + Helper.Object2String(c, "\n"), error);
                        }
                    }
                }
                /*產生coupon並寄信 end*/
                OnSearchDealByBid(sender, new DataEventArgs<Guid>(deal.BusinessHourGuid));
                View.OrderMakeSuccess(o.OrderId);
                #endregion Coupon
            }
            else
            {
                OrderFacade.DeleteItemInCart(ticketId);
                pp.TempSessionDelete(ticketId);
                TransactionId = null;
                View.ShowErrorMessage("發生錯誤");
            }
            
            #endregion
        }
        #endregion

        #region private method
        private void SendSmsProcess(IViewPponDeal deal, Coupon c, string mobile, Guid order_detail_guid, string sms_memo)
        {
            SendSmsProcess(deal.BusinessHourGuid, deal.BusinessHourDeliverTimeS.Value, deal.BusinessHourDeliverTimeE.Value, c.Id, c.SequenceNumber, c.Code,
                c.StoreSequence.HasValue ? "，序號" + c.StoreSequence.Value.ToString("0000") : string.Empty, mobile, order_detail_guid, sms_memo);
        }

        private void SendSmsProcess(ViewPponCoupon coupon, string mobile, string sms_memo)
        {
            SendSmsProcess(coupon.BusinessHourGuid, coupon.BusinessHourDeliverTimeS.Value, coupon.BusinessHourDeliverTimeE.Value, coupon.CouponId.Value, coupon.SequenceNumber,
                coupon.CouponCode, coupon.CouponStoreSequence.HasValue ? "，序號" + coupon.CouponStoreSequence.Value.ToString("0000") : string.Empty, mobile, coupon.OrderDetailGuid, sms_memo);
        }

        private void SendSmsProcess(Guid business_hour_guid, DateTime business_hour_delivertime_s, DateTime business_hour_deliver_e,
            int coupon_id, string sequence_number, string code, string store_sequence, string mobile, Guid order_detail_guid, string sms_memo)
        {
            #region send SMS 不走workflow
            SMS sms = new SMS();
            SmsContent sc = OrderFacade.SMSContentGet(business_hour_guid);
            string[] msgs = sc.Content.Split('|');
            string branch = OrderFacade.SmsGetPhoneFromWhere(order_detail_guid, business_hour_guid);    //分店資訊 & 多重選項
            string msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2] + (string.IsNullOrEmpty(sms_memo) ? string.Empty : ("，" + sms_memo)),
                sequence_number,
                code,
                business_hour_delivertime_s.ToString("yyyyMMdd"),
                business_hour_deliver_e.ToString("yyyyMMdd"),
                store_sequence);
            //c.StoreSequence.HasValue ? "，序號" + c.StoreSequence.Value.ToString("0000") : string.Empty);
            sms.QueueMessage(msg, SmsType.Ppon, "", mobile, business_hour_guid, coupon_id.ToString(),
                CommunicationFacade.GetSmsSystemByRatio());

            #endregion
        }

        private string[] GetFailSmsFilter()
        {
            return new string[] { ViewOrderCorrespondingSmsLog.Columns.SmsStatus + "=" + (int)SmsStatus.Fail, 
            ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + ">=" + DateTime.Now.AddDays(-3).ToString("yyyy-MM-dd")  ,
                ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + "<=" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd")
            };
        }
        /// <summary>
        /// 依搜尋選項組成搜尋條件
        /// </summary>
        /// <returns></returns>
        private string[] GetSearchFilter()
        {
            string filter = string.Empty;
            //依關鍵字搜尋
            if (View.IsSearchByKey)
            {
                switch (View.SearchFilter)
                {
                    case 0:
                        // filter = ViewOrderCorrespondingSmsLog.Columns.OrderId + "=" + View.SearchKey;
                        filter = "o.order_id='" + View.SearchKey + "'";
                        break;
                    case 1:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.RelatedOrderId + "=" + View.SearchKey;
                        filter = "o.related_order_id='" + View.SearchKey + "'";
                        break;
                    case 2:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.Mobile + "=" + (View.SearchKey.StartsWith("1") ? ("86" + View.SearchKey) : View.SearchKey);
                        filter = "o.mobile='" + (View.SearchKey.StartsWith("1") ? ("86" + View.SearchKey) : View.SearchKey) + "'";
                        break;
                    case 3:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.SequenceNumber + "=" + View.SearchKey;
                        filter = "o.sequence_number='" + View.SearchKey + "'";
                        break;
                }
                //return new string[] { filter, ViewOrderCorrespondingSmsLog.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid };
                return new string[] { filter, "o.business_hour_guid='" + View.BusinessHourGuid + "'", "o.type=" + (int)OrderClassification.TmallOrder };
            }
            else //依簡訊狀態搜尋
            {
                switch (View.SmsFilter)
                {
                    case 1:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.SmsStatus + "=" + (int)SmsStatus.queue;
                        filter = "sm.status=" + (int)SmsStatus.Queue;
                        break;
                    case 2:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.SmsStatus + "=" + (int)SmsStatus.fail;
                        filter = "sm.status=" + (int)SmsStatus.Fail;
                        break;
                    case 3:
                        //filter = ViewOrderCorrespondingSmsLog.Columns.SmsStatus + "=" + (int)SmsStatus.success;
                        filter = "sm.status=" + (int)SmsStatus.Success;
                        break;
                }
                if (string.IsNullOrEmpty(filter))
                {
                    //    return new string[] { ViewOrderCorrespondingSmsLog.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid,
                    //ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + ">=" + View.SmsStart.ToString("yyyy-MM-dd")  ,
                    //ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + "<=" + View.SmsEnd.AddDays(1).ToString("yyyy-MM-dd")};
                    return new string[] { "o.business_hour_guid='"  + View.BusinessHourGuid+"'",
                "sm.create_time>='" + View.SmsStart.ToString("yyyy-MM-dd")+"'"  ,
                "sm.create_time<='" + View.SmsEnd.AddDays(1).ToString("yyyy-MM-dd")+"'",
                    "o.type=" + (int)OrderClassification.TmallOrder};
                }
                else
                {
                    //    return new string[] { filter, ViewOrderCorrespondingSmsLog.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid,
                    //ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + ">=" + View.SmsStart  , 
                    //    ViewOrderCorrespondingSmsLog.Columns.SmsCreateTime + "<=" + View.SmsEnd};
                    return new string[] { filter, "o.business_hour_guid='" + View.BusinessHourGuid+"'",
                "sm.create_time>='" + View.SmsStart.ToString("yyyy-MM-dd")+"'"  ,
                "sm.create_time<='" + View.SmsEnd.AddDays(1).ToString("yyyy-MM-dd")+"'",
                    "o.type=" + (int)OrderClassification.TmallOrder};
                }
            }
        }

        /// <summary>
        /// 依檔次資料，搜尋核銷統計、分店和商品選項
        /// </summary>
        /// <param name="deal">檔次</param>
        private void SetDealDetialContent(ViewPponDeal deal)
        {
            View.BusinessHourGuid = deal.BusinessHourGuid;

            int fail_total = op.ViewOrderCorrespondingSmsLogListGetCount(deal.BusinessHourGuid, new string[] { "sm.status=" + (int)SmsStatus.Fail });
            int success_total = op.ViewOrderCorrespondingSmsLogListGetCount(deal.BusinessHourGuid, new string[] { "sm.status=" + (int)SmsStatus.Success });
            int queue_total = op.ViewOrderCorrespondingSmsLogListGetCount(deal.BusinessHourGuid, new string[] { "sm.status=" + (int)SmsStatus.Queue });
            //核銷統計
            List<VendorDealSalesCount> verify_summary_list = vp.GetPponVerificationSummary(new Guid[] { deal.BusinessHourGuid });
            //商品選項
            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(deal.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                ag.members = new List<ViewItemAccessoryGroup>();
                ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(deal.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    ag.members.Add(vig);
                }
            }
            //分店
            var stores = pp.ViewPponStoreGetListByBidWithNoLock(deal.BusinessHourGuid, VbsRightFlag.VerifyShop);
            View.SearchDealById(deal, verify_summary_list.FirstOrDefault(), viagc, stores, fail_total, success_total, queue_total);
        }
        #endregion
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Interface;
using City = LunchKingSite.DataOrm.City;
using Seller = LunchKingSite.DataOrm.Seller;

namespace LunchKingSite.WebLib.Presenters
{
    public class SellerAddPresenter : Presenter<ISellerAddView>
    {
        #region Property

        public string UpdateMessage;
        protected ILocationProvider lp;
        protected ISellerProvider sp;
        protected IItemProvider ip;
        protected IPponProvider pp;
        protected IMemberProvider mp;
        protected ISysConfProvider cp;
        protected ICmsProvider cmp;
        protected IPCPProvider pcp;
        private ISystemProvider _systemProv;
        private SalesService _salServ;
        public const int DEFAULT_PAGE_SIZE = 15;
        private static ILog logger = LogManager.GetLogger(typeof(SellerAddPresenter));

        #endregion

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetCityList();
            //SetSellerImageDetail();
            //商家預設要顯示在商家列表中
            //View.ShowInSellerList = true;
            /////////////////////////
            if (View.SellerGuid != Guid.Empty)
            {
                Seller s = sp.SellerGet(View.SellerGuid);
                SetZoneList(lp.CityGet(s.CityId).ParentId);

                var auditLogs = cmp.AuditGetList(s.Guid.ToString(), true)
                                    .Where(x => x.ReferenceType == (int)AuditType.Seller)
                                    .Select(x => new Audit()
                                    {
                                        Id = x.Id,
                                        ReferenceId = x.ReferenceId,
                                        ReferenceType = x.ReferenceType,
                                        CommonVisible = x.CommonVisible,
                                        Message = x.Message.IndexOf("seller_info") > 0 ? x.Message.Replace(x.Message.Substring(x.Message.IndexOf("seller_info"), x.Message.IndexOf("-> <br />", x.Message.IndexOf("seller_info")) - x.Message.IndexOf("seller_info") + 9),"") : x.Message,
                                        CreateId = x.CreateId,
                                        CreateTime = x.CreateTime,
                                    });
                var childrenSellers = SellerFacade.GetChildrenSellerGuid(View.SellerGuid);
                var storeLists = sp.SellerGetList(childrenSellers)
                                        .Select(x => new KeyValuePair<Guid, string> ( x.Guid, x.SellerName))
                                        .ToList();
                View.SetSellerGet(s, auditLogs, storeLists);

                #region store_category

                var viewStoreCategoryCol = sp.ViewStoreCategoryCollectionGetByStore(View.SellerGuid);

                View.MRTLocationCategory = viewStoreCategoryCol.Select(x => x.CategoryCode).ToList();
                View.MRTLocationChecked = viewStoreCategoryCol.Select(x => new KeyValuePair<string, int>(x.Name, x.CategoryCode)).ToList();
                
                #endregion store_category

                View.SetImageGrid(s.SellerLogoimgPath);
                View.SellerIntroduct = s.SellerInfo;
                
                var sellerTree = sp.SellerTreeGetListBySellerGuid(View.SellerGuid);
                if (sellerTree.Any())
                {
                    var parentSeller = sp.SellerGet(sellerTree.First().ParentSellerGuid);
                    if (parentSeller != null)
                    {
                        View.ParentSellerInfo = new KeyValuePair<string, string>(parentSeller.SellerId, parentSeller.SellerName);
                        View.ParentSellerGuid = parentSeller.Guid;
                    }
                }

                ViewPponDealCollection vpc = pp.ViewPponDealGetListPaging(1, View.PageSizeOfPpon,
                    ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc", ViewPponDeal.Columns.SellerGuid + " = " + View.SellerGuid,
                    ViewPponDeal.Columns.UniqueId + " is not null");
                try
                {
                    BusinessOrder businessOrder = _salServ.GetBusinessOrderBySellerGuid(s.Guid);
                    if (businessOrder != null)
                    {
                        View.BusinessOrderId = businessOrder.BusinessOrderId;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("連線錯誤，使用者:" + View.UserName, ex);
                }

                View.SetPponList(vpc);
            }
            else
            {
                SetZoneList(View.SelectCity);
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ImageListChanged += OnImageListChanged;
            View.UpdatePhoto += OnUpdatePhoto;

            View.SelectCityChanged += OnSelectCityChanged;
            View.UpdateSellerInfo += OnUpdateSellerInfo;
            View.UpdateSellerCategory += OnUpdateSellerCategory;
            View.CreateSellerID += OnCreateSellerID;
            View.ImportBusinessOrderInfo += OnImportBusinessOrderInfo;
            View.DeleteAccessoryGroup += OnDeleteAccessoryGroup;

            View.SellerDetailSave += OnSellerDetailSave;
            View.ImportIntroduct += OnImportIntroduct;
            View.DealPagerChanged += OnDealPagerChanged;
            View.DealPagerGetCount += OnDealPagerGetCount;
            View.CopySeller += OnCopySeller;

            return true;
        }
        
        private void GenerateAuditRecord(LunchKingSite.DataOrm.Store newStore, LunchKingSite.DataOrm.Store oldStore, string createId)
        {
            string aduitStr = string.Empty;
            foreach (SubSonic.TableSchema.TableColumn col in DataOrm.Store.Schema.Columns)
            {
                if (col.IsPrimaryKey)
                {
                    // PrimaryKey無法修改
                    continue;
                }

                if (oldStore.GetColumnValue(col.ColumnName) == null)
                {
                    if (newStore.GetColumnValue(col.ColumnName) != null)
                    {
                        aduitStr += col.ColumnName + " : " + oldStore.GetColumnValue(col.ColumnName) + " -> " + newStore.GetColumnValue(col.ColumnName) + "<br />";
                        oldStore.SetColumnValue(col.ColumnName, newStore.GetColumnValue(col.ColumnName));
                    }
                    continue;
                }

                if (!oldStore.GetColumnValue(col.ColumnName).Equals(newStore.GetColumnValue(col.ColumnName)) && col.ColumnName != "create_id" && col.ColumnName != "create_time")
                {
                    if (col.ColumnName != "modify_time" && col.ColumnName != "coordinate" && col.ColumnName != "modify_id")
                    {
                        aduitStr += col.ColumnName + " : " + oldStore.GetColumnValue(col.ColumnName) + " -> " + newStore.GetColumnValue(col.ColumnName) + "<br />";
                    }

                    oldStore.SetColumnValue(col.ColumnName, newStore.GetColumnValue(col.ColumnName));
                }
            }
            if (aduitStr.Length > 0)
            {
                CommonFacade.AddAudit(oldStore.Guid, AuditType.Store, aduitStr, createId, true);
            }
        }

        protected string TrafficInformationGet(LunchKingSite.DataOrm.Store lkStore)
        {
            var traff = new List<string>();
            if (!string.IsNullOrWhiteSpace(lkStore.Mrt))
            {
                traff.Add(lkStore.Mrt);
            }

            if (!string.IsNullOrWhiteSpace(lkStore.Car))
            {
                traff.Add(lkStore.Car);
            }

            if (!string.IsNullOrWhiteSpace(lkStore.Bus))
            {
                traff.Add(lkStore.Bus);
            }

            if (!string.IsNullOrWhiteSpace(lkStore.OtherVehicles))
            {
                traff.Add(lkStore.OtherVehicles);
            }
            return string.Join("/", traff);
        }

        private void OnImportIntroduct(object sender, EventArgs e)
        {
            LogManager.GetLogger(LineAppender._LINE).Info("OnImportIntroduct");
        }

        public SellerAddPresenter(SalesService salesService)
        {
            SetupProviders();
            _salServ = salesService;
        }

        public SellerAddPresenter(SalesService salesService, ISystemProvider sysP)
        {
            SetupProviders();
            _salServ = salesService;
            _systemProv = sysP;
        }

        protected void SetupProviders()
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            cp = ProviderFactory.Instance().GetConfig();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        }

        protected void OnSelectCityChanged(object sender, EventArgs e)
        {
            SetZoneList(View.SelectCity);
        }
        

        protected void OnCreateSellerID(object sender, EventArgs e)
        {
            View.SetSellerID(GetNewSellerID());
        }

        protected void OnUpdateSellerInfo(object sender, DataEventArgs<Seller> e)
        {
            Seller theSeller;
            string aduitStr = string.Empty;
            bool closeDownNotify = false;

            if (View.SellerGuid == Guid.Empty)
            {
                theSeller = e.Data;

                var repeatSellers = VourcherFacade.SearchRepeatSellers(theSeller.SellerName, theSeller.SignCompanyID, theSeller.CompanyBossName);
                if (repeatSellers.Count > 0)
                {
                    UpdateMessage = "更新失敗!!請檢查是否重複建立賣家";
                }

                theSeller.Guid = Helper.GetNewGuid(theSeller);
                theSeller.CreateId = View.UserName;
                theSeller.CreateTime = DateTime.Now;
                theSeller.MarkNew();
            }
            else
            {
                e.Data.Guid = View.SellerGuid;
                theSeller = sp.SellerGet(View.SellerGuid);
                e.Data.CreateTime = theSeller.CreateTime;

                //若忘了填經緯度則幫忙補上地址取出的經緯度
                double lat, lont;
                if (!double.TryParse(View.LongitudeLatitude.Key, out lat) || !double.TryParse(View.LongitudeLatitude.Value, out lont)
                    || Math.Abs(lat) > 90 || Math.Abs(lont) > 180)
                {
                    KeyValuePair<string, string> coordinate = LocationFacade.GetArea(e.Data.StoreTownshipId.HasValue ? CityManager.CityTownShopStringGet(e.Data.StoreTownshipId.Value) + e.Data.StoreAddress : string.Empty);
                    if (coordinate.Key != null && coordinate.Value != null)
                    {
                        e.Data.Coordinate = LocationFacade.GetGeographyWKT(coordinate.Key, coordinate.Value);
                    }
                }

                //結束營業要發內部通知信給相關人員
                if (e.Data.IsCloseDown && !theSeller.IsCloseDown)  //posted form IsCloseDown && database !IsCloseDown
                {
                    if (e.Data.CloseDownDate.HasValue)
                    {
                        closeDownNotify = true;
                        theSeller.IsCloseDown = e.Data.IsCloseDown;
                        theSeller.CloseDownDate = e.Data.CloseDownDate;
                    }
                }

                //先暫存起來
                string tmpContacts = theSeller.Contacts;
                int? tmpVendorReceiptType = theSeller.VendorReceiptType;
                string tmpAccountingMessage = theSeller.Othermessage;
                DateTime? tmpContractCkeckTimeHouse = theSeller.ContractCkeckTimeHouse;
                string tmpContractVersionHouse = theSeller.ContractVersionHouse;
                DateTime? tmpContractCkeckTimePpon = theSeller.ContractCkeckTimePpon;
                string tmpContractVersionPpon = theSeller.ContractVersionPpon;

                string tmpSellerFax = theSeller.SellerFax;
                string tmpSellerMobilex = theSeller.SellerMobile;
                string tmpSellerEmail = theSeller.SellerEmail;
                string tmpSellerContactPerson = theSeller.SellerContactPerson;
                string tmpReturnedPersonName = theSeller.ReturnedPersonName;
                string tmpReturnedPersonTel = theSeller.ReturnedPersonTel;
                string tmpReturnedPersonEmail = theSeller.ReturnedPersonEmail;


                // the following code depends on SubSonic, which will need to be replaced if we are not using
                // SubSonic as model generator in future.
                // another way, of course, is to use reflection on Seller.Columns struct to get column values
                foreach (SubSonic.TableSchema.TableColumn col in Seller.Schema.Columns)
                {
                    if (theSeller.GetColumnValue(col.ColumnName) == null)
                    {
                        if (e.Data.GetColumnValue(col.ColumnName) != null)
                        {
                            aduitStr += col.ColumnName + " : " + theSeller.GetColumnValue(col.ColumnName) + " -> " + e.Data.GetColumnValue(col.ColumnName) + "<br />";
                            theSeller.SetColumnValue(col.ColumnName, e.Data.GetColumnValue(col.ColumnName));
                        }
                        continue;
                    }

                    if (!theSeller.GetColumnValue(col.ColumnName).Equals(e.Data.GetColumnValue(col.ColumnName)) && col.ColumnName != "create_id")
                    {
                        if (col.ColumnName != "modify_time" && col.ColumnName != "coordinate" && col.ColumnName != "modify_id")
                        {
                            aduitStr += col.ColumnName + " : " + theSeller.GetColumnValue(col.ColumnName) + " -> " + e.Data.GetColumnValue(col.ColumnName) + "<br />";
                        }

                        theSeller.SetColumnValue(col.ColumnName, e.Data.GetColumnValue(col.ColumnName));
                    }
                }
                theSeller.Contacts = tmpContacts;
                if(tmpVendorReceiptType!= null)
                {
                    theSeller.VendorReceiptType = tmpVendorReceiptType.Value;
                }
                theSeller.Othermessage = tmpAccountingMessage;

                theSeller.ContractCkeckTimeHouse = tmpContractCkeckTimeHouse;
                theSeller.ContractVersionHouse = tmpContractVersionHouse;
                theSeller.ContractCkeckTimePpon = tmpContractCkeckTimePpon;
                theSeller.ContractVersionPpon = tmpContractVersionPpon;

                theSeller.SellerFax = tmpSellerFax;
                theSeller.SellerMobile = tmpSellerMobilex;
                theSeller.SellerEmail = tmpSellerEmail;
                theSeller.SellerContactPerson = tmpSellerContactPerson;
                theSeller.ReturnedPersonName = tmpReturnedPersonName;
                theSeller.ReturnedPersonTel = tmpReturnedPersonTel;
                theSeller.ReturnedPersonEmail = tmpReturnedPersonEmail;


                theSeller.TempStatus = (int)SellerTempStatus.Completed;
                theSeller.ModifyId = View.UserName;
                theSeller.ApproveTime = theSeller.ModifyTime = DateTime.Now;
                theSeller.NewCreated = false;
                theSeller.Message = string.Empty;

                theSeller.SellerInfo = View.SellerIntroduct;

                if (View.ParentSellerGuid.HasValue)
                {
                    if (View.ParentSellerGuid != theSeller.Guid)
                    { 
                        var childSellerGuids = SellerFacade.GetChildrenSellerGuid(theSeller.Guid);
                        if (childSellerGuids.Contains(View.ParentSellerGuid.Value))
                        {
                            UpdateMessage = "上層公司不得設定為此賣家所屬之下層公司!!";
                        }
                    }
                    else
                    {
                        UpdateMessage = "上層公司不得設定為賣家本身!!";
                    }
                }

            }

            if (string.IsNullOrEmpty(UpdateMessage))
            {
                if (!sp.SellerSet(theSeller))
                {
                    UpdateMessage = "更新失敗!!請聯繫It_Service確認失敗原因";
                }
                else
                {
                    //結束營業刪除所有優惠券分店、優惠券活動disabled
                    if (theSeller.IsCloseDown)
                    {
                        if (closeDownNotify)
                        {
                            UpdateDealsStatus(View.SellerGuid, theSeller.CloseDownDate.Value);
                            SendSellerCloseDownMail(theSeller.SellerName, theSeller.CloseDownDate.Value);
                            //發送通知信給購買此賣家檔次的消費者
                            EmailFacade.SendSellerCloseDownNoticeToMember(theSeller.Guid, theSeller.SellerName, theSeller.CloseDownDate.Value);
                        }

                        VourcherEventCollection vourcher_events = VourcherFacade.VourcherEventGetBySellerGuid(theSeller.Guid);
                        foreach (VourcherEvent vourcher_event in vourcher_events)
                        {
                            vourcher_event.Enable = false;
                            VourcherFacade.VourcherEventSet(vourcher_event);
                            VourcherFacade.VourcherStoreDeleteByEventId(vourcher_event.Id);
                        }
                    }

                    if (aduitStr.Length > 0)
                    {
                        CommonFacade.AddAudit(View.SellerGuid, AuditType.Seller, aduitStr, View.UserName, true);
                    }

                    VourcherFacade.ChangeLogUpdateEnabled(Seller.Schema.TableName, theSeller.Guid, false);
                    
                    #region store_category

                    var storeCategoryCol = sp.StoreCategoryCollectionGetByStore(theSeller.Guid);
                    var mrtLocationCategoryList = View.MRTLocationCategory;

                    if (storeCategoryCol.Any())
                    {
                        var previoursCategoryList = storeCategoryCol.ToList();

                        foreach (var code in mrtLocationCategoryList)
                        {
                            if (previoursCategoryList.All(x => x.CategoryCode != code))
                            {
                               var storeCategory = new StoreCategory()
                               {
                                   StoreGuid = theSeller.Guid,
                                   CategoryCode = code,
                                   CreateType = (int) MrtReleaseshipStoreSettingType.System,
                                   Valid = 1,
                                   ModifyId = View.UserName,
                                   ModifyTime = DateTime.Now
                               };
                               sp.StoreCategorySet(storeCategory);
                            }
                        }

                        foreach (var storeCategory in previoursCategoryList)
                        {
                            if (!mrtLocationCategoryList.Contains(storeCategory.CategoryCode))
                            {
                                storeCategory.Valid = 0;
                                storeCategory.ModifyId = View.UserName;
                                storeCategory.ModifyTime = DateTime.Now;
                                sp.StoreCategorySet(storeCategory);
                            }
                        }
                    }
                    else
                    {
                        foreach (var code in mrtLocationCategoryList)
                        {
                            var storeCategory = new StoreCategory()
                            {
                                StoreGuid = theSeller.Guid,
                                CategoryCode = code,
                                CreateType = (int) MrtReleaseshipStoreSettingType.System,
                                Valid = 1,
                                ModifyId = View.UserName,
                                ModifyTime = DateTime.Now
                            };
                            sp.StoreCategorySet(storeCategory);
                        }
                    }
                    #endregion store_category

                    // 工單系統
                    try
                    {
                        BusinessOrder businessOrder = _salServ.GetBusinessOrder(View.BusinessOrderId);
                        if (businessOrder != null && !businessOrder.SellerGuid.Equals(theSeller.Guid))
                        {
                            businessOrder.SellerGuid = View.SellerGuid;
                            businessOrder.SellerId = View.SellerID;
                            businessOrder.ModifyId = View.UserName;
                            businessOrder.ModifyTime = DateTime.Now;
                            businessOrder.HistoryList.Add(new History() { Changeset = I18N.Phrase.ImportBusinessOrderSellerInfo + ":" + businessOrder.SellerId, ModifyId = View.UserName, ModifyTime = DateTime.Now });
                            _salServ.SaveBusinessOrder(businessOrder);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error("連線錯誤，使用者:" + View.UserName, ex);
                    }
                    

                    #region seller_tree

                    /*
                     * 1.parent_id異動須一併更新 root_id
                     * 2.原符合root_id條件新增parent_id 需一併更新其他關聯root_id data
                     * */
                    var parentSellerGuids = new List<Guid>();
                    if (View.ParentSellerGuid.HasValue)
                    {
                        parentSellerGuids.Add(View.ParentSellerGuid.Value);
                    }
                    if (string.IsNullOrEmpty(UpdateMessage))
                    { 
                        SellerFacade.SellerTreeSet(View.SellerGuid, parentSellerGuids.Any() ? parentSellerGuids : null, View.UserName, string.Empty);
                    }

                    #endregion seller_tree

                    #region 購物車運費
                    SellerFacade.ShoppingCartFreightsCreateBySellerGuid(theSeller.Guid);
                    #endregion
                }
            }
            View.SetTabPanel(0);
        }


        protected void OnCopySeller(object sender, EventArgs e)
        {
            Seller seller = sp.SellerGet(View.SellerGuid).Clone();
            seller.Guid = Helper.GetNewGuid(seller);
            seller.SellerId = GetNewSellerID();
            seller.CompanyNotice = null;
            seller.Coordinate = null;
            seller.VbsPrefix = null;
            seller.ApplyId = null;
            seller.ApplyTime = null;
            seller.ReturnTime = null;
            seller.TempStatus = (int)SellerTempStatus.Applied;
            seller.StoreStatus = (int)StoreStatus.Available;
            seller.CreateId = View.UserName;
            seller.CreateTime = DateTime.Now;
            seller.ModifyId = View.UserName;
            seller.ModifyTime = DateTime.Now;
            sp.SellerSet(seller);

            SellerFacade.ShoppingCartFreightsCreateBySellerGuid(seller.Guid);

            #region insert SellerTree

            //若複製之賣家有下層賣家 則將此筆複製賣家 也一併複製為下層賣家
            var sellerTree = sp.SellerTreeGetListByParentSellerGuid(View.SellerGuid);
            if (sellerTree.Any())
            {
                SellerFacade.SellerTreeSet(seller.Guid, new List<Guid> { View.SellerGuid }, View.UserName, string.Empty);
            }

            #endregion insert SellerTree

            View.CopySellerLink(seller.Guid);
        }

        private void SellerSave(Seller seller, Seller ori)
        {
            seller.ModifyId = View.UserName;
            seller.ModifyTime = DateTime.Now;
            sp.SellerSet(seller);


            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, seller))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                SellerLog(View.SellerGuid, string.Join("|", changeList));
            }
            View.SetTabPanel(0);
        }

        private void SellerLog(Guid sid, string changeLog)
        {
            SellerFacade.SellerLogSet(sid, changeLog, View.UserName);
        }

        /// <summary>
        /// 賣家倒店時間若影響到配送時間, 要註記在相關 business_hour 的 status.
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="closeTime"></param>
        private void UpdateDealsStatus(Guid sellerGuid, DateTime closeTime)
        {
            BusinessHourCollection bhs = sp.BusinessHourGetListBySeller(sellerGuid);
            IEnumerable<BusinessHour> bhFiltered = bhs.Where(bh => bh.BusinessHourDeliverTimeE > closeTime);
            foreach (BusinessHour bh in bhFiltered)
            {
                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange);
                sp.BusinessHourSet(bh);
            }
        }

        /// <summary>
        /// 發送內部通知信
        /// </summary>
        /// <param name="sellerName"></param>
        /// <param name="closeDownTime"></param>
        private void SendSellerCloseDownMail(string sellerName, DateTime closeDownTime)
        {
            SellerOrStoreCloseDownInformation info = new SellerOrStoreCloseDownInformation();
            info.SellerName = sellerName;
            info.CloseDownDate = closeDownTime.ToString("yyyy/MM/dd");
            info.HDmailHeaderUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Header.png";
            info.HDmailFooterUrl = WebUtility.GetSiteRoot() + "/Themes/HighDeal/images/mail/HDmail_Footer.png";

            Member mem = mp.MemberGet(View.UserName);

            HiDealMailFacade.SendSellerOrStoreCloseDownNotification(mem.UserEmail, mem.DisplayName, info);
        }

        protected void OnUpdateSellerCategory(object sender, DataEventArgs<Dictionary<int, bool>> e)
        {
            SellerCategory SellerCategoryInfo = new SellerCategory();
            SellerCategoryInfo.SellerGuid = View.SellerGuid;
            SellerCategoryCollection SellerCategoryList = sp.SellerCategoryGetList(View.SellerGuid);

            foreach (KeyValuePair<int, bool> SellerCategory in e.Data)
            {
                SellerCategoryInfo.CategoryId = SellerCategory.Key;

                switch (SellerCategory.Value)
                {
                    case true:
                        if (SellerCategoryList.Find("CategoryId", SellerCategoryInfo.CategoryId) == -1)
                        {
                            SellerCategoryInfo.MarkNew();
                            sp.SellerCategorySave(SellerCategoryInfo);
                        }
                        break;

                    case false:
                        if (SellerCategoryList.Find("CategoryId", SellerCategoryInfo.CategoryId) > -1)
                        {
                            SellerCategoryInfo.MarkClean();
                            sp.SellerCategoryDelete(SellerCategoryList[SellerCategoryList.Find("CategoryId", SellerCategory.Key)].Id);
                        }
                        break;
                }
            }
        }

        private void OnImportBusinessOrderInfo(object sender, DataEventArgs<string> e)
        {
            BusinessOrder businessOrder = _salServ.GetBusinessOrder(e.Data);
            if (businessOrder != null)
            {
                #region Import Store

                var seller = sp.SellerGet(View.SellerGuid); 
                //SQL DB無該筆seller資料
                if (seller == null || seller.IsLoaded == false)
                {
                    View.ShowMessage(I18N.Message.MongoNoStoreDataErrorMsg);
                    View.GoToSellerStoreList();
                }

                StoreCollection branches = sp.StoreGetListBySellerGuid(View.SellerGuid);

                #region 新增總店資料

                #region lkstore
                LunchKingSite.DataOrm.Store lkMainStore = branches.Where(x => (!string.IsNullOrWhiteSpace(x.CompanyID) && x.CompanyID.Equals(businessOrder.AccountingId)) ||
                                                                                (!string.IsNullOrWhiteSpace(x.CompanyAccount) && x.CompanyAccount.Equals(businessOrder.Account)) ||
                                                                                (!string.IsNullOrWhiteSpace(x.Phone) && x.Phone.Equals(businessOrder.ReservationTel))).FirstOrDefault();
                if (lkMainStore == null)
                {
                    lkMainStore = new DataOrm.Store();
                    lkMainStore.StoreName = businessOrder.StoreName;
                    lkMainStore.Phone = businessOrder.ReservationTel;
                    City city = CityManager.TownShipGetById(businessOrder.AddressTownshipId);
                    if (city != null)
                    {
                        lkMainStore.CityId = city.ParentId;
                    }
                    lkMainStore.TownshipId = businessOrder.AddressTownshipId;
                    lkMainStore.AddressString = businessOrder.SellerAddress;

                    KeyValuePair<string, string> coordinate = LocationFacade.GetArea(CityManager.CityTownShopStringGet(businessOrder.AddressTownshipId) + businessOrder.SellerAddress);
                    if (coordinate.Key != null && coordinate.Value != null)
                    {
                        lkMainStore.Coordinate = LocationFacade.GetGeographyWKT(coordinate.Key, coordinate.Value);
                    }
                    lkMainStore.OpenTime = businessOrder.OpeningTime;
                    lkMainStore.CloseDate = businessOrder.CloseDate;
                    lkMainStore.Remarks = string.Empty;
                    lkMainStore.Mrt = businessOrder.Mrt;
                    lkMainStore.Car = businessOrder.Car;
                    lkMainStore.Bus = businessOrder.Bus;
                    lkMainStore.OtherVehicles = businessOrder.VehicleInfo;
                    lkMainStore.WebUrl = businessOrder.WebUrl;
                    lkMainStore.FacebookUrl = businessOrder.FBUrl;
                    lkMainStore.PlurkUrl = businessOrder.PlurkUrl;
                    lkMainStore.BlogUrl = businessOrder.BlogUrl;
                    lkMainStore.OtherUrl = businessOrder.OtherUrl;

                    lkMainStore.CompanyAccount = businessOrder.Account.Replace("-", string.Empty).Trim();
                    lkMainStore.CompanyAccountName = businessOrder.Accounting;
                    if (businessOrder.AccountingName.Length > 20)
                    {
                        lkMainStore.AccountantName = businessOrder.AccountingName.Substring(0, 20);
                    }
                    else
                    {
                        lkMainStore.AccountantName = businessOrder.AccountingName;
                    }
                    lkMainStore.AccountantTel = businessOrder.AccountingTel;
                    lkMainStore.CompanyBankCode = businessOrder.AccountingBankId;
                    lkMainStore.CompanyBossName = businessOrder.BossName;
                    lkMainStore.CompanyBranchCode = businessOrder.AccountingBranchId;
                    lkMainStore.CompanyEmail = businessOrder.AccountingEmail;
                    lkMainStore.CompanyID = businessOrder.AccountingId;
                    if (businessOrder.IsPrivateContract)
                    {
                        lkMainStore.SignCompanyID = businessOrder.PersonalId;
                    }
                    else
                    {
                        lkMainStore.SignCompanyID = businessOrder.GuiNumber;
                    }
                    lkMainStore.CompanyName = businessOrder.CompanyName;
                    lkMainStore.CompanyNotice = string.Empty;
                    lkMainStore.SellerGuid = View.SellerGuid;
                    lkMainStore.CreateId = View.UserName;
                    lkMainStore.CreateTime = DateTime.Now;
                    sp.StoreSet(lkMainStore);

                    if (businessOrder.AccountingName.Length > 20)
                    {
                        View.ShowMessage("帳務聯絡人資料過長，請逕行修正資料。");
                    }  
                }

                #endregion lkstore

                #endregion 新增總店資料

                #region 新增分店資料

                foreach (SalesStore salStore in businessOrder.Store)
                {
                    #region lksite store

                    LunchKingSite.DataOrm.Store lkStore = branches.Where(x => (!string.IsNullOrWhiteSpace(x.CompanyID) && x.CompanyID.Equals(salStore.StoreID)) ||
                                                                                (!string.IsNullOrWhiteSpace(x.CompanyAccount) && x.CompanyAccount.Equals(salStore.StoreAccount)) ||
                                                                                (!string.IsNullOrWhiteSpace(x.Phone) && x.Phone.Equals(salStore.StoreTel))).FirstOrDefault();
                    if (lkStore == null)
                    {
                        lkStore = new DataOrm.Store();
                    }
                    else
                    {
                        #region aduit

                        string aduitStr = string.Empty;

                        if (lkStore.StoreName != salStore.BranchName)
                        {
                            aduitStr += "StoreName : " + lkStore.StoreName + " ->" + salStore.BranchName + "<br />";
                        }

                        if (lkStore.Phone != salStore.StoreTel)
                        {
                            aduitStr += "Phone : " + lkStore.Phone + " ->" + salStore.StoreTel + "<br />";
                        }

                        if (lkStore.TownshipId != salStore.AddressTownshipId)
                        {
                            aduitStr += "TownshipId : " + lkStore.TownshipId + " ->" + salStore.AddressTownshipId + "<br />";
                        }

                        if (lkStore.AddressString != salStore.StoreAddress)
                        {
                            aduitStr += "AddressString : " + lkStore.AddressString + " ->" + salStore.StoreAddress + "<br />";
                        }

                        if (lkStore.OpenTime != salStore.OpeningTime)
                        {
                            aduitStr += "OpenTime : " + lkStore.OpenTime + " ->" + salStore.OpeningTime + "<br />";
                        }

                        if (lkStore.CloseDate != salStore.CloseDate)
                        {
                            aduitStr += "CloseDate : " + lkStore.CloseDate + " ->" + salStore.CloseDate + "<br />";
                        }

                        if (lkStore.Remarks != salStore.Remark)
                        {
                            aduitStr += "Remarks : " + lkStore.Remarks + " ->" + salStore.Remark + "<br />";
                        }

                        if (lkStore.Mrt != salStore.Mrt)
                        {
                            aduitStr += "Mrt : " + lkStore.Mrt + " ->" + salStore.Mrt + "<br />";
                        }

                        if (lkStore.Car != salStore.Car)
                        {
                            aduitStr += "Car : " + lkStore.Car + " ->" + salStore.Car + "<br />";
                        }

                        if (lkStore.Bus != salStore.Bus)
                        {
                            aduitStr += "Bus : " + lkStore.Bus + " ->" + salStore.Bus + "<br />";
                        }

                        if (lkStore.OtherVehicles != salStore.OV)
                        {
                            aduitStr += "OtherVehicles : " + lkStore.OtherVehicles + " ->" + salStore.OV + "<br />";
                        }

                        if (lkStore.WebUrl != salStore.WebUrl)
                        {
                            aduitStr += "WebUrl : " + lkStore.WebUrl + " ->" + salStore.WebUrl + "<br />";
                        }

                        if (lkStore.FacebookUrl != salStore.FBUrl)
                        {
                            aduitStr += "FacebookUrl : " + lkStore.FacebookUrl + " ->" + salStore.FBUrl + "<br />";
                        }

                        if (lkStore.PlurkUrl != salStore.PlurkUrl)
                        {
                            aduitStr += "PlurkUrl : " + lkStore.PlurkUrl + " ->" + salStore.PlurkUrl + "<br />";
                        }

                        if (lkStore.BlogUrl != salStore.BlogUrl)
                        {
                            aduitStr += "BlogUrl : " + lkStore.BlogUrl + " ->" + salStore.BlogUrl + "<br />";
                        }

                        if (lkStore.OtherUrl != salStore.OtherUrl)
                        {
                            aduitStr += "OtherUrl : " + lkStore.OtherUrl + " ->" + salStore.OtherUrl + "<br />";
                        }

                        if (lkStore.CompanyAccount != salStore.StoreAccount)
                        {
                            aduitStr += "CompanyAccount : " + lkStore.CompanyAccount + " ->" + salStore.StoreAccount + "<br />";
                        }

                        if (lkStore.CompanyAccountName != salStore.StoreAccountName)
                        {
                            aduitStr += "CompanyAccountName : " + lkStore.CompanyAccountName + " ->" + salStore.StoreAccountName + "<br />";
                        }

                        if (lkStore.CompanyBankCode != salStore.StoreBankCode)
                        {
                            aduitStr += "CompanyBankCode : " + lkStore.CompanyBankCode + " ->" + salStore.StoreBankCode + "<br />";
                        }

                        if (lkStore.CompanyBossName != salStore.StoreBossName)
                        {
                            aduitStr += "CompanyBossName : " + lkStore.CompanyBossName + " ->" + salStore.StoreBossName + "<br />";
                        }

                        if (lkStore.CompanyBranchCode != salStore.StoreBranchCode)
                        {
                            aduitStr += "CompanyBranchCode : " + lkStore.CompanyBranchCode + " ->" + salStore.StoreBranchCode + "<br />";
                        }

                        if (lkStore.CompanyEmail != salStore.StoreEmail)
                        {
                            aduitStr += "CompanyEmail : " + lkStore.CompanyEmail + " ->" + salStore.StoreEmail + "<br />";
                        }

                        if (lkStore.CompanyID != salStore.StoreID)
                        {
                            aduitStr += "CompanyID : " + lkStore.CompanyID + " ->" + salStore.StoreID + "<br />";
                        }

                        if (lkStore.SignCompanyID != salStore.StoreSignCompanyID)
                        {
                            aduitStr += "SignCompanyID : " + lkStore.SignCompanyID + " ->" + salStore.StoreSignCompanyID + "<br />";
                        }

                        if (lkStore.CompanyName != salStore.StoreName)
                        {
                            aduitStr += "CompanyName : " + lkStore.CompanyName + " ->" + salStore.StoreName + "<br />";
                        }

                        if (lkStore.CompanyNotice != salStore.StoreNotice)
                        {
                            aduitStr += "CompanyNotice : " + lkStore.CompanyNotice + " ->" + salStore.StoreNotice + "<br />";
                        }

                        if (aduitStr.Length > 0)
                        {
                            CommonFacade.AddAudit(lkStore.Guid, AuditType.Store, aduitStr, View.UserName, true);
                        }

                        #endregion aduit
                    }
                    lkStore.StoreName = salStore.BranchName;
                    lkStore.Phone = salStore.StoreTel;
                    City city = CityManager.TownShipGetById(salStore.AddressTownshipId);
                    if (city != null)
                    {
                        lkStore.CityId = city.ParentId;
                    }
                    lkStore.TownshipId = salStore.AddressTownshipId;
                    lkStore.AddressString = salStore.StoreAddress;

                    KeyValuePair<string, string> coordinate = LocationFacade.GetArea(CityManager.CityTownShopStringGet(salStore.AddressTownshipId) + salStore.StoreAddress);
                    if (coordinate.Key != null && coordinate.Value != null)
                    {
                        lkStore.Coordinate = LocationFacade.GetGeographyWKT(coordinate.Key, coordinate.Value);
                    }
                    lkStore.OpenTime = salStore.OpeningTime;
                    lkStore.CloseDate = salStore.CloseDate;
                    lkStore.Remarks = salStore.Remark;
                    lkStore.Mrt = salStore.Mrt;
                    lkStore.Car = salStore.Car;
                    lkStore.Bus = salStore.Bus;
                    lkStore.OtherVehicles = salStore.OV;
                    lkStore.WebUrl = salStore.WebUrl;
                    lkStore.FacebookUrl = salStore.FBUrl;
                    lkStore.PlurkUrl = salStore.PlurkUrl;
                    lkStore.BlogUrl = salStore.BlogUrl;
                    lkStore.OtherUrl = salStore.OtherUrl;

                    lkStore.CompanyAccount = salStore.StoreAccount.Replace("-", string.Empty).Trim();
                    lkStore.CompanyAccountName = salStore.StoreAccountName;
                    lkStore.AccountantName = salStore.StoreAccountingName;
                    lkStore.AccountantTel = salStore.StoreAccountingTel;
                    lkStore.CompanyBankCode = salStore.StoreBankCode;
                    lkStore.CompanyBossName = salStore.StoreBossName;
                    lkStore.CompanyBranchCode = salStore.StoreBranchCode;
                    lkStore.CompanyEmail = salStore.StoreEmail;
                    lkStore.CompanyID = salStore.StoreID;
                    lkStore.SignCompanyID = salStore.StoreSignCompanyID;
                    lkStore.CompanyName = salStore.StoreName;
                    lkStore.CompanyNotice = salStore.StoreNotice;
                    if (!lkStore.Guid.Equals(Guid.Empty))
                    {
                        lkStore.ModifyId = View.UserName;
                        lkStore.ModifyTime = DateTime.Now;
                    }
                    else
                    {
                        lkStore.SellerGuid = View.SellerGuid;
                        lkStore.CreateId = View.UserName;
                        lkStore.CreateTime = DateTime.Now;
                    }
                    sp.StoreSet(lkStore);

                    #endregion lksite store
                }

                #endregion 新增分店資料

                #endregion Import Store

                View.SetBusinessOrderInfo(businessOrder);
            }
            else
            {
                View.ShowMessage(I18N.Message.NoBusinessOrderId);
            }
        }

        protected void OnDeleteAccessoryGroup(object sender, DataEventArgs<Guid> e)
        {
            ip.AccessoryGroupDelete(AccessoryGroup.Columns.Guid, e.Data);
        }

        protected void OnSellerDetailSave(object sender, EventArgs e)
        {
            //商家資訊不再儲存於mango，改存入DB  [SellerInfo]欄位
            Seller theSeller;
            if (View.SellerGuid == Guid.Empty)
            {
                View.ShowMessage(I18N.Message.MongoNoStoreDataErrorMsg);
                return;
            }
            else
            {
                theSeller = sp.SellerGet(View.SellerGuid);
                theSeller.SellerInfo = View.SellerIntroduct;

                if (!sp.SellerSet(theSeller))
                {
                    View.ShowMessage(I18N.Message.MongoNoStoreDataErrorMsg);
                    return;
                }
                View.SellerIntroduct = theSeller.SellerInfo;
            }
        }

        protected BusinessHourCollection SetBusinessHourTypeList(BusinessHourCollection BusinessHourList)
        {
            BusinessHourCollection BusinessHourTypeList = new BusinessHourCollection();

            int TypeID = -1;
            if (BusinessHourList.Count > 0)
            {
                TypeID = BusinessHourList[0].BusinessHourTypeId;

                for (int i = 0; i < BusinessHourList.Count; i++)
                {
                    if (BusinessHourTypeList.Count <= 0)
                    {
                        BusinessHourTypeList.Add(BusinessHourList[0]);
                    }
                    else if (BusinessHourList[i].BusinessHourTypeId != TypeID)
                    {
                        BusinessHourTypeList.Add(BusinessHourList[i]);
                        TypeID = BusinessHourList[i].BusinessHourTypeId;
                    }
                }
            }
            return BusinessHourTypeList;
        }

        protected Dictionary<int, BusinessHourCollection> SetBusinessHourTimeList(BusinessHourCollection BusinessHourList)
        {
            Dictionary<int, BusinessHourCollection> BusinessHourTimeList = new Dictionary<int, BusinessHourCollection>();
            BusinessHourCollection BusinessHourTimeListTemp = new BusinessHourCollection();

            int BusinessHourTypeIdTemp = -1;
            if (BusinessHourList.Count > 0)
            {
                BusinessHourTypeIdTemp = BusinessHourList[0].BusinessHourTypeId;

                for (int i = 0; i < BusinessHourList.Count; i++)
                {
                    if (BusinessHourTimeListTemp.Count <= 0)
                    {
                        BusinessHourTimeListTemp.Add(BusinessHourList[0]);
                    }
                    else if (BusinessHourList[i].BusinessHourTypeId == BusinessHourTypeIdTemp)
                    {
                        BusinessHourTimeListTemp.Add(BusinessHourList[i]);
                    }
                    else
                    {
                        BusinessHourTimeList.Add(BusinessHourTimeList.Count, BusinessHourTimeListTemp);
                        BusinessHourTimeListTemp = new BusinessHourCollection();
                        BusinessHourTimeListTemp.Add(BusinessHourList[i]);
                        BusinessHourTypeIdTemp = BusinessHourList[i].BusinessHourTypeId;
                    }

                    if (i >= BusinessHourList.Count - 1)
                    {
                        BusinessHourTimeList.Add(BusinessHourTimeList.Count, BusinessHourTimeListTemp);
                    }
                }
            }
            return BusinessHourTimeList;
        }

        protected void SetCityList()
        {
            View.SetCityDropDown(CityManager.Citys.Where(x => !x.Code.Equals("sys", StringComparison.OrdinalIgnoreCase)).ToList());
        }

        protected void SetZoneList(int? selectCity)
        {
            CityCollection llczone = selectCity != null ? lp.CityGetList(selectCity.Value, City.Columns.Id) : lp.CityGetListTopLevel();
            Dictionary<int, string> zone = llczone.Where(x => !string.IsNullOrEmpty(x.Code)).ToDictionary(x => x.Id, x => x.CityName);
            View.SetZoneDropDown(zone);
        }
        
        protected string GetNewSellerID()
        {
            return SellerFacade.GetNewSellerID();
        }

        protected string[] GetFilter()
        {
            string[] filter = new string[1];
            if (!string.IsNullOrEmpty(View.Filter))
            {
                filter[0] = View.Filter;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = Item.Columns.ItemName + " like %" + filter[0] + "%";
            }
            return filter;
        }

        public void OnUpdatePhoto(object sender, DataEventArgs<PhotoInfo> e)
        {
            int d = sp.SellerGet(View.SellerGuid).Department;
            switch ((DepartmentTypes)d)
            {
                case DepartmentTypes.Delicacies:
                    if (e.Data.Type == UploadFileType.ItemPhoto)
                    {
                        e.Data.Type = UploadFileType.DelicaciesItemPhoto;
                    }
                    else if (e.Data.Type == UploadFileType.SellerPhoto)
                    {
                        e.Data.Type = UploadFileType.DelicaciesSellerPhoto;
                    }
                    break;
            }

            View.SetImageFile(e.Data);
        }

        private void OnImageListChanged(object sender, DataEventArgs<string> e)
        {
            Seller s = sp.SellerGet(View.SellerGuid);
            s.SellerLogoimgPath = e.Data;
            if (s.IsDirty)
            {
                sp.SellerSet(s);
            }
            View.SetImageGrid(s.SellerLogoimgPath);
        }

        private void RemovePhotoFromDisk(string rawPath)
        {
            if (string.IsNullOrEmpty(rawPath) || rawPath.IndexOf(',') < 0)
            {
                return;
            }

            string[] filePath = rawPath.Split(',');
            ImageUtility.DeleteFile(UploadFileType.PponEvent, filePath[0], filePath[1]);
        }
        //17Life好康，檔次分頁
        protected void OnDealPagerGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = pp.ViewPponDealGetCount(ViewPponDeal.Columns.SellerGuid + " = " + View.SellerGuid);
        }

        protected void OnDealPagerChanged(object sender, DataEventArgs<int> e)
        {
            int pageNumber = e.Data;
            ViewPponDealCollection vpc = pp.ViewPponDealGetListPaging(pageNumber, View.PageSizeOfPpon,
                ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc",
                ViewPponDeal.Columns.SellerGuid + " = " + View.SellerGuid);

            View.SetPponList(vpc);
        }
    }
}
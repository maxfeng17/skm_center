﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.BizLogic.Component;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using System.Linq;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealUnsubscriptionPresenter:Presenter<IHiDealUnsubscriptionView>
    {
        IHiDealProvider hp;
        ILocationProvider lp;
        IMemberProvider mp;

        protected void HiDealUnsubscriptionProviders()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        #region overload base methods
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UnSubscribe += OnUnSubscribe;
            return true;
        }
        #endregion

        private void OnUnSubscribe(object sender, DataEventArgs<string[]> e)
        {
            HiDealMailFacade.HiDealSubscribeCheckSet(View.theEmail, (int)HiDealRegionManager.DealOverviewRegion, 0, false);
            //跳出成功頁面
            View.ShowAlert(1);
        }  

         
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using MongoDB.Driver;

namespace LunchKingSite.WebLib.Presenters
{
    public class EinvoiceWinnerPresenter : Presenter<IEinvoiceWinner>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            CheckGetEinvoiceWinner();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UpdateWinnerInfo += OnUpdateWinnerInfo;
            return true;
        }
        #region interface
        protected void CheckGetEinvoiceWinner()
        {
            KeyValuePair<DateTime, DateTime> kp = GetDates();
            EinvoiceMainCollection data = op.EinvoiceMainGetByWinner(View.UserId, kp.Key, kp.Value);
            View.GetEinvoiceMain(data);
        }
        #endregion
        #region event
        protected void OnUpdateWinnerInfo(object sender, DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>> e)
        {
            DateTime tmpDateTime = DateTime.Now;

            if (DateTime.Now.Month % 2 > 0)
            {
                tmpDateTime = tmpDateTime.AddMonths(-4);
            }
            else
            {
                tmpDateTime = tmpDateTime.AddMonths(-5);
            }
            string msg = "您的資料已儲存成功!";
            DateTime.TryParse(tmpDateTime.Year + "-" + tmpDateTime.Month + "-1 00:00", out tmpDateTime);
            var winnerData = op.EinvoiceMainByUserId(View.UserId, tmpDateTime, DateTime.Now);

            EinvoiceWinner einvoiceWinnerData = MemberFacade.EinvoiceWinnerGetByUserid(View.UserId);
            einvoiceWinnerData.UserName = e.Data.Value.Key;
            einvoiceWinnerData.Adress = e.Data.Key;
            einvoiceWinnerData.Mobile = e.Data.Value.Value;
            einvoiceWinnerData.ModificationTime = DateTime.Now;
            einvoiceWinnerData.UserId = View.UserId;
            mp.SaveEinvoiceWinner(einvoiceWinnerData);

            if (winnerData.Count > 0)
            {
                var paperedList = winnerData.Where(x => x.InvoicePaperedTime != null);
                var noPaperedList = winnerData.Where(x => x.InvoicePaperedTime == null);
                var paperedStr = "";
                var noPaperedStr = "";

                KeyValuePair<DateTime, DateTime> kp = GetDates();
                EinvoiceWinner ew = mp.EinvoiceWinnerGetByUserid(View.UserId);
                ew.UserId = View.UserId;
                ew.UserName = e.Data.Value.Key;
                ew.Mobile = e.Data.Value.Value;
                ew.Adress = e.Data.Key;
                ew.ModificationTime = DateTime.Now;
                op.EinvoiceUpdateWinner(e.Data.Value.Key, e.Data.Value.Value, e.Data.Key, View.UserId, kp.Key, kp.Value);

                if (noPaperedList.Any())
                {
                    foreach (var d in noPaperedList)
                    {
                        noPaperedStr += string.IsNullOrEmpty(noPaperedStr) ? d.InvoiceNumber : "," + d.InvoiceNumber;
                    }
                }

                if (paperedList.Any())
                {
                    foreach (var d in paperedList)
                    {
                        paperedStr += string.IsNullOrEmpty(paperedStr) ? d.InvoiceNumber : "," + d.InvoiceNumber;
                    }
                }

                if ((!string.IsNullOrEmpty(noPaperedStr)) && (!string.IsNullOrEmpty(paperedStr)))
                {
                    msg += @"\n\n" + "中獎發票號碼：" + noPaperedStr + @"，也將會使用您更新的新地址，寄發給您。\n\n"
                           + @"請注意，\n" + "您有" + paperedList.Count() + @"筆中獎發票已經進入寄發的程序。\n" + "中獎發票號碼:" + paperedStr
                           + "此中獎發票，將會寄送到原填寫的寄送地址。若有任何疑問或需要洽詢，請聯繫17life客服。";
                }
                else if (!string.IsNullOrEmpty(paperedStr))
                {
                    msg += @"\n\n" + @"請注意，\n" + "您有" + paperedList.Count() + @"筆中獎發票已經進入寄發的程序。\n" + "中獎發票號碼:" + paperedStr
                           + "此中獎發票，將會寄送到原填寫的寄送地址。若有任何疑問或需要洽詢，請聯繫17life客服。";
                }
                else if(!string.IsNullOrEmpty(noPaperedStr))
                {
                    msg += @"\n\n" + "中獎發票號碼：" + noPaperedStr + @"，也將會使用您更新的新地址，寄發給您。\n\n";
                }

            }

            if (msg == "您的資料已儲存成功!")
            {
                View.FinishUpdate();
            }
            else{
                View.ShowMessage(msg);
            }
        }
        #endregion
        #region method
        protected KeyValuePair<DateTime, DateTime> GetDates()
        {
            DateTime now = DateTime.Now;
            DateTime d_start = new DateTime();
            DateTime d_end = new DateTime();
            if (now.Month % 2 == 1)
            {
                d_start = new DateTime(now.AddMonths(-4).Year, now.AddMonths(-4).Month, 1);
                d_end = d_start.AddMonths(4);
            }
            else
            {
                d_start = new DateTime(now.AddMonths(-5).Year, now.AddMonths(-5).Month, 1);
                d_end = d_start.AddMonths(5);
            }
            return new KeyValuePair<DateTime, DateTime>(d_start, d_end);
        }
        #endregion
    }
}

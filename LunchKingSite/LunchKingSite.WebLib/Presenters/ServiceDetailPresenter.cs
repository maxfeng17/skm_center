﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Web;
using LunchKingSite.Core.Enumeration;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class ServiceDetailPresenter : Presenter<IServiceDetailView>
    {
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {

            base.OnViewInitialized();
            View.MessageList(GetMessageList());

            if (!string.IsNullOrEmpty(View.UserData))
            {
                SetMemberInfo();
            }
            else
            {
                SetMemberInfoHide();
            }

            SetServiceDetail(View.ServiceNo);
            return true;

        }

        public override bool OnViewLoaded()
        {

            base.OnViewLoaded();
            View.Save += OnSave;
            View.Search += OnSearch;
            View.Claim += OnClaim;
            View.Join += OnJoin;
            View.Notify += OnNotify;
            NotifyRead();

            return true;
        }

        #region event
        protected void NotifyRead()
        {
            var vcsl = sp.GetViewCustomerServiceMessageByServiceNo(View.ServiceNo);
            bool worker = vcsl.ServicePeopleId == View.UserId || vcsl.SecServicePeopleId == View.UserId;
            if (worker)
            {
                var status = vcsl.ServicePeopleId == View.UserId ? (int)statusConvert.ServiceNotify : (int)statusConvert.secServiceNotify;
                var notRead = sp.CustomerServiceInsideLogGet(View.ServiceNo).Where(x=>x.Status == status && x.IsRead == Convert.ToInt32(false));
                if(notRead.Count() > 0)
                {
                    CustomerServiceInsideLogCollection csic = new CustomerServiceInsideLogCollection();
                    notRead.ForEach(x => { x.IsRead = Convert.ToInt32(true); csic.Add(x); });
                    sp.CustomerServiceInsideLogCollectionSet(csic);
                }
            }
        }

        //public event EventHandler<DataEventArgs<string>> Save;
        protected void OnSearch(object sender, EventArgs e)
        {
            View.MessageList(GetMessageList());
        }

        protected void OnClaim(object sender, EventArgs e)
        {
            var csm = sp.CustomerServiceMessageGet(View.ServiceNo);
            if (csm.IsLoaded)
            {
                csm.Status = (int)statusConvert.process;
                csm.ModifyId = View.UserId;
                csm.ModifyTime = DateTime.Now;
                csm.WorkUser = View.UserId;
                sp.CustomerServiceMessageSet(csm);

                #region 儲存中台
                if (csm.IssueFromType == (int)IssueFromType.TaiShin)
                {
                    LifeEntrepotApi.CustomerServiceStatus(View.ServiceNo, null, (int)statusConvert.process, View.UserId.ToString());
                }
                #endregion
            }
            SetServiceDetail(View.ServiceNo);
        }
        #endregion
        private void SetMemberInfo()
        {
            ViewMemberBuildingCityParentCity vmbcpc = mp.ViewMemberBuildingCityParentCityGetByUserName(View.UserData);
            MobileMember mm = MemberFacade.GetMobileMember(vmbcpc.UniqueId);
            View.SetMemberData(vmbcpc, mm);
            View.SetMemberAddress(MemberFacade.GetMemberAddress(vmbcpc));
            View.SetMemberLinkList(mp.MemberLinkGetList(vmbcpc.UniqueId));
            ViewMemberPromotionTransactionCollection promotionTrans = View.UserData == config.TmallDefaultUserName ? new ViewMemberPromotionTransactionCollection()
                : mp.ViewMemberPromotionTransactionGetList(View.UserData,
                ViewMemberPromotionTransaction.Columns.CreateTime);
            View.SetMemberBonus(promotionTrans);

            decimal scashE7;
            decimal scashPez;
            decimal scash = OrderFacade.GetSCashSum2(vmbcpc.UniqueId, out scashE7, out scashPez);
            View.SetMemberScash(scash, scashE7, scashPez);
            bool isTaishinPayLinked;
            decimal taishinPayCashPoint = MemberFacade.GetTaisihinPayCashPoint(vmbcpc.UniqueId, out isTaishinPayLinked);

        }

        private void SetMemberInfoHide()
        {
            View.SetMemberHide();
        }

        private void SetServiceDetail(string serviceNo)
        {
            ViewCustomerServiceList vcsl = sp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
            ViewCustomerServiceInsideLogCollection vcsi = sp.GetInsideLogByServiceNo(serviceNo);
            ViewCustomerServiceOutsideLogCollection vcso = sp.GetOutsideLogByServiceNo(serviceNo);
            
            View.SetServiceDetail(vcsl, vcsi, vcso);
            
        }

        protected void OnSave(object sender, DataEventArgs<string> e)
        {
            var entity = sp.CustomerServiceMessageGet(View.ServiceNo);
            if (entity.IsLoaded)
            {
                entity.Priority = View.Priority;
                entity.Category = View.MainCategory;
                entity.SubCategory = View.SubCategory;
                entity.Status = View.Status;
                entity.ModifyId = View.UserId;
                entity.ModifyTime = DateTime.Now;
                sp.CustomerServiceMessageSet(entity);

                #region 儲存中台
                if (entity.IssueFromType == (int)IssueFromType.TaiShin)
                {
                    LifeEntrepotApi.CustomerServiceStatus(View.ServiceNo, View.SubCategory, View.Status, View.UserId.ToString());
                }
                #endregion
            }

            #region 儲存備註
            if (View.Image.HasFile || !string.IsNullOrEmpty(View.Content))
            {
                //新增資料
                CustomerServiceInsideLog csi = new CustomerServiceInsideLog();
                csi.Content = View.Content;
                csi.ServiceNo = View.ServiceNo;
                csi.Status = View.Status;
                csi.CreateTime = DateTime.Now;
                csi.CreateId = View.UserId;

                //上傳圖片
                if (View.Image.HasFile)
                {
                    csi.File = View.ServiceNo + "_" + DateTime.Now.ToString("MMddHHmmss") + Path.GetExtension(View.Image.FileName);
                    HttpPostedFileBase hpfb = new HttpPostedFileWrapper(View.Image.PostedFile) as HttpPostedFileBase;
                    ImageUtility.UploadFileWithResizeForFileBase(hpfb, e.ToString(),csi.ServiceNo, csi.File,640);
                }

                int csiId = sp.CustomerServiceInsideLogSet(csi);
                
                if (View.Status == (int)statusConvert.transfer)
                {
                    EmailFacade.SendTransferMailToSeller(csiId);
                }
            }
            #endregion
            SetServiceDetail(View.ServiceNo);

        }

        protected void OnJoin(object sender, DataEventArgs<int> e)
        {
            var csm = sp.CustomerServiceMessageGet(View.ServiceNo);
            csm.CoWorkUser = e.Data;
            sp.CustomerServiceMessageSet(csm);
            OnNotify(null, new DataEventArgs<int>((int)statusConvert.secServiceNotify));
        }

        protected void OnNotify(object sender, DataEventArgs<int> e)
        {
            CustomerServiceInsideLog csi = new CustomerServiceInsideLog();
            csi.Content = "";
            csi.ServiceNo = View.ServiceNo;
            csi.Status = e.Data;
            csi.CreateTime = DateTime.Now;
            csi.CreateId = View.UserId;
            sp.CustomerServiceInsideLogSet(csi);
            EmailFacade.SendNotifyToCs(View.ServiceNo, (statusConvert)e.Data);
            SetServiceDetail(View.ServiceNo);
            View.alertMessage = "信件已寄出";
        }

        private ViewCustomerServiceListCollection GetMessageList()
        {
            return sp.GetViewCustomerServiceMessageByPage(1, 100, ViewCustomerServiceList.Columns.CreateTime, GetFilter());
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            if (!string.IsNullOrEmpty(View.ContactTarget))
            {
                ViewCustomerServiceList csm = sp.GetViewCustomerServiceMessageByServiceNo(View.ServiceNo);
                if (csm.IsLoaded)
                {
                    switch (View.ContactTarget)
                    {
                        case "orderId":
                            filter.Add(ViewCustomerServiceList.Columns.OrderGuid + " = " + csm.OrderGuid);
                            break;
                        case "mem":
                            filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + csm.UserId);
                            break;
                        case "generalContinue":
                            filter.Add(ViewCustomerServiceList.Columns.ParentServiceNo + " = " + csm.ParentServiceNo);
                            filter.Add(ViewCustomerServiceList.Columns.OrderGuid + " is null");
                            break;
                    }                    
                }
            }


            if (!string.IsNullOrEmpty(View.ServiceNo))
            {
                filter.Add(ViewCustomerServiceList.Columns.ServiceNo + " <> " + View.ServiceNo);
            }
            else
            {
                filter.Add(ViewCustomerServiceList.Columns.ServiceNo + " = " + View.ServiceNo);
            }

            return filter.ToArray();
        }
    }
}

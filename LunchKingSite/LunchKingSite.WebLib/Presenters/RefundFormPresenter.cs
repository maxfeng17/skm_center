﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class RefundFormPresenter : Presenter<IRefundForm>
    {
        private IOrderProvider _ordProv;
        private IMemberProvider _memProv;

        public RefundFormPresenter(IPponProvider pponProv, IMemberProvider memProv, IOrderProvider ordProv)
        {
            _memProv = memProv;
            _ordProv = ordProv;
        }

        public override bool OnViewInitialized()
        {
            string message = string.Empty;
            bool isleagl = true;
            if ((View.OrderGuid == null || string.IsNullOrEmpty(View.UserName)))
            {
                isleagl = false;
            }
            EinvoiceMain einvoice = _ordProv.EinvoiceMainGet(View.OrderGuid);
            CashTrustLogCollection cs = _memProv.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            OldCashTrustLogCollection ocs = _memProv.GetOldCashTrustLogCollectionByOid(View.OrderGuid);
            Member m = _memProv.MemberGet(View.UserName);
            CtAtmRefund atmrefund = _ordProv.CtAtmRefundGetLatest(View.OrderGuid);
            if (einvoice.Id == 0)//沒發票
            {
                isleagl = false;
                message = "此訂單不需填寫【退貨申請書】!";
            }
            else if (View.UserId == 0 || einvoice.UserId != View.UserId)
            {
                isleagl = false;
                message = "會員資料有誤，請洽客服中心!";
            }
            else if (cs.Count > 0)
            {
                List<CashTrustLog> couponTypeCs = cs.Where(log => log.CouponId != null).ToList();

                if (couponTypeCs.Count > 0 &&
                    couponTypeCs.Count(clog => (clog.Status != ((int)TrustStatus.Verified) && clog.Status != ((int)TrustStatus.Refunded))) == 0)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
                else if (cs.First().UserId != m.UniqueId)//同會員
                {
                    isleagl = false;
                    message = "會員資料有誤，請洽客服中心!";
                }
            }
            else if (cs.Count == 0)
            {
                if (ocs.Count == 0 && !einvoice.OrderIsPponitem)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
                else if (ocs.Count > 0 && ocs.Count(x => x.Status < (int)TrustStatus.Verified) == 0)
                {
                    isleagl = false;
                    message = "此訂單已全部使用或已申請退貨!";
                }
            }
            View.SetRefundFormInfo(einvoice, cs, ocs, atmrefund, isleagl, message);
            return true;
        }
    }
}
﻿using System.Data;
using System.Linq;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Transactions;
using IsolationLevel = System.Transactions.IsolationLevel;
using LunchKingSite.BizLogic.Models.BounPoints;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserServiceIntergratePresenter : Presenter<IUserServiceIntergrateView>
    {
        protected IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        protected ILog log = LogManager.GetLogger(typeof(UserServiceIntergratePresenter));
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();


        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetStatusInfo();
            if (!string.IsNullOrEmpty(View.UserData))
            {
                MemberCollection mc = mp.MemberGetList(Member.Columns.UserName, View.UserData.Replace("_", "[_]"));
                if (mc.Count == 1)
                {
                    View.UserData = mc[0].UserName;
                    LoadData(mc[0].UserName);
                }
                else
                {
                    View.SetUserName(mc);
                }
            }
            ServiceMessageCategoryCollection serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(null);
            View.BuildServiceCategory(serviceCates);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            View.OnSearchClicked += OnSearchClicked;
            View.OnStatusSelected += OnStatusSelected;
            View.OnIDEASSearchClicked += OnIDEASSearchClicked;
            View.OnTmallSearchClicked += OnTmallSearchClicked;
            View.OnBcashAddClicked += OnBcashAddClicked;
            View.OnRefundScashClicked += OnRefundScashClicked;
            View.OnSetServiceMsgClicked += OnSetServiceMsgClicked;
            View.OnSubscribeUpdateClick += OnSubscribeUpdateClick;
            View.OnGetOrderCount += OnGetOrderCount;
            View.OrderPageChanged += OnOrderPageChanged;
            View.OnGetHidealOrderCount += OnGetHidealOrderCount;
            View.HidealOrderPageChanged += OnHidealOrderPageChanged;
            View.OnGetServiceCount += OnGetServiceCount;
            View.ServicePageChanged += OnServicePageChanged;
            View.OnGetNotClosedCount += OnGetNotClosedCount;
            View.NotClosedPageChanged += OnNotClosedPageChanged;
            View.OnGetClosedCount += OnGetClosedCount;
            View.ClosedPageChanged += OnClosedPageChanged;
            View.OnGetLunchKingCount += OnGetLunchKingCount;
            View.OnGetIDEASOrderCount += OnGetIDEASOrderCount;
            View.OnIDEASOrderPageChanged += OnIDEASOrderPageChanged;
            View.OnGetTmallCount += OnGetTmallCount;
            View.OnTmallPageChanged += OnTmallPageChanged;
            View.GetBonusListInfo += OnBonusListInfo;

            View.GetMembershipCardListInfo += OnMembershipCardListInfo;
            View.OnGetMembershipCardCount += OnGetMembershipCardCount;
            View.MembershipCardPageChanged += OnMembershipCardPageChanged;


            return true;
        }

        #region enum



        #endregion enum

        #region event

        protected void OnBonusListInfo(object sender, DataEventArgs<string> e)
        {
            switch (e.Data)
            {
                case "Bonus":
                    SetBonusListInfo();
                    break;
                case "Subscription":
                    SetSubscriptionData();
                    break;
                case "LunchKing":
                    SetLunchKingOrderData(1);
                    break;
                case "PayeasyScashRecords":
                    List<PezScashTransaction> records = OrderFacade.GetPezScashTransactions(View.UserId);
                    View.SetPayeasyScashRecords(records);
                    break;
            }
        }

        protected void OnMembershipCardListInfo(object sender, DataEventArgs<string> e)
        {
            SetMembershipCardListInfo();
        }


        protected void OnSearchClicked(object sender, EventArgs e)
        {
            string filter = string.Empty;
            string searchText = View.UserDataView;
            FilterType filterType = (FilterType)View.FilterTypeValue;
            switch (filterType)
            {
                case FilterType.UserName:
                    filter = Member.Columns.UserName;
                    searchText = searchText.Replace("_", "[_]");
                    break;

                case FilterType.Mobile:
                    filter = Member.Columns.Mobile;
                    break;

                case FilterType.UserId:
                    filter = Member.Columns.UniqueId;
                    break;

                case FilterType.RegisteredMobile:
                    filter = Member.Columns.UniqueId;
                    MobileMember mm = MemberFacade.GetMobileMember(searchText);
                    if (mm.IsLoaded)
                    {
                        searchText = mm.UserId.ToString();
                    }
                    break;

                default:
                    break;
            }

            //如果只用訂單編號的話
            if (string.IsNullOrEmpty(View.UserDataView) && View.OrderGuid != Guid.Empty)
            {
                Order order = op.OrderGet(View.OrderGuid);
                if (order.IsLoaded)
                {
                    searchText = order.UserId.ToString();
                }
                filter = Member.Columns.UniqueId;
            }

            MemberCollection memCol = mp.MemberGetList(filter, searchText);
            if (memCol.Count == 1)
            {
                View.UserData = memCol[0].UserName;
                LoadData(memCol[0].UserName);
            }
            else
            {
                View.SetUserName(memCol);
            }
        }

        protected void OnStatusSelected(object sender, EventArgs e)
        {
            LoadData();
        }

        protected void OnIDEASSearchClicked(object sender, EventArgs e)
        {
            SetIDEASOrderInfo(1);
        }

        public void OnTmallSearchClicked(object sender, EventArgs e)
        {
            SetTmallInfo(1);
        }

        protected void OnBcashAddClicked(object sender, EventArgs e)
        {
            MemberPromotionDeposit mpd = View.MemberPromotionDeposit;

            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = IsolationLevel.ReadCommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                //新增 vendor_payment_change
                if (View.IsBcashPayByVendor)
                {
                    Guid orderGuid = Guid.Empty;
                    Guid.TryParse(mpd.OrderGuid.ToString(), out orderGuid);

                    if (!View.DealGuid.HasValue)
                    {
                        return;
                    }

                    if (View.Fine == (int)VendorFineType.Change)
                    {
                        var vpc = new VendorPaymentChange
                        {
                            BusinessHourGuid = View.DealGuid.Value,
                            Reason = mpd.Action,
                            Amount = (int)-mpd.PromotionValue / 10,
                            PromotionValue = (int)mpd.PromotionValue / 10,
                            CreateId = mpd.CreateId,
                            CreateTime = mpd.CreateTime,
                            AllowanceAmount = (int)-mpd.PromotionValue / 10,
                            ModifyId = mpd.CreateId,
                            ModifyTime = mpd.CreateTime
                        };
                        var vpcId = ap.VendorPaymentChangeSet(vpc);
                        mpd.VendorPaymentChangeId = vpcId;
                    }
                    else if (View.Fine == (int)VendorFineType.Overdue)
                    {
                        var vpo = new VendorPaymentOverdue
                        {
                            BusinessHourGuid = View.DealGuid.Value,
                            OrderGuid = orderGuid,
                            Reason = mpd.Action,
                            PromotionAmount = (int)mpd.PromotionValue / 10,
                            Amount = (int)-mpd.PromotionValue / 10,
                            BalanceSheetId = null,
                            CreateId = mpd.CreateId,
                            CreateTime = mpd.CreateTime
                        };
                        var vpoId = ap.VendorPaymentOverdueSet(vpo);
                        mpd.VendorPaymentChangeId = vpoId;
                    }


                    //紀錄log
                    string msg = "扣款/異動金額：" + ((int)-mpd.PromotionValue / 10).ToString() + "元 <br /> 扣款 / 異動原因：" + mpd.Action + "<br />  紅利金摘要： " + mpd.Action + " <br />  須給消費者紅利金：" + ((int)mpd.PromotionValue / 10).ToString() + "元";
                    CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, mpd.CreateId, false);

                }

                MemberFacade.MemberPromotionProcess(mpd, View.UserData, true);

                scope.Complete();
            }

            View.SetBonusList(GetBonusAuditList());
        }

        protected void OnRefundScashClicked(object sender, EventArgs e)
        {
            if (View.RefundScashAmount > 0)
            {
                MemberFacade.ExportScash(View.UserId, View.RefundScashAmount, "以ATM退回剩餘購物金", View.CreateId);
            }
        }

        protected void OnSetServiceMsgClicked(object sender, DataEventArgs<ServiceMessage> e)
        {
            mp.ServiceMessageSet(e.Data, View.UserId);

            ServiceLog sl = new ServiceLog();
            sl.SendTo = e.Data.Email;           //對應到ServiceMessage的Email
            sl.RefX = e.Data.Id;     //FK, 對應到ServiceMessage的ID
            sl.RemarkSeller = e.Data.RemarkSeller;          //回報廠商備註
            sl.CreateId = e.Data.CreateId;
            sl.CreateTime = e.Data.CreateTime;
            mp.ServiceLogSet(sl);

            SetServicelData(1);
        }

        protected void OnSubscribeUpdateClick(object sender, EventArgs e)
        {
            Member m = mp.MemberGet(View.UserData);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveEDM, m.EdmType, MemberEdmType.PponEvent);
            m.EdmType = (int)Helper.SetFlag(View.IfReceiveStartedSell, m.EdmType, MemberEdmType.PponStartedSell);
            m.EdmType = (int)Helper.SetFlag(View.IfReceivePiinlifeEDM, m.EdmType, MemberEdmType.PiinlifeEvent);
            m.EdmType = (int)Helper.SetFlag(View.IfReceivePiinlifeStartedSell, m.EdmType, MemberEdmType.PiinlifeStartedSell);
            mp.MemberSet(m);

            foreach (KeyValuePair<Subscription, bool> item in View.UnsubscribeList)
            {
                if (!Helper.IsFlagSet((SubscribeType)item.Key.Status, SubscribeType.Unsubscribe) && item.Value)
                {
                    pp.SubscriptionCancel(item.Key.Id);
                }
                else if (Helper.IsFlagSet((SubscribeType)item.Key.Status, SubscribeType.Unsubscribe) && !item.Value)
                {
                    pp.SubscriptionReSet(item.Key.Id);
                }
            }

            foreach (KeyValuePair<HiDealSubscription, bool> item in View.UnHidealsubscribeList)
            {
                if (item.Key.RegionCodeId != null || item.Key.City != null)
                {
                    HiDealMailFacade.HiDealSubscribeCheckSet(item.Key.Email, Convert.ToInt32(item.Key.RegionCodeId), Convert.ToInt32(item.Key.City), item.Value);
                }
            }

            SetSubscriptionData();
        }

        #endregion event

        #region method

        #region Member

        private void SetMemberInfo()
        {
            ViewMemberBuildingCityParentCity vmbcpc = mp.ViewMemberBuildingCityParentCityGetByUserName(View.UserData);
            MobileMember mm = MemberFacade.GetMobileMember(vmbcpc.UniqueId);
            View.SetMemberData(vmbcpc, mm);
            View.SetMemberAddress(MemberFacade.GetMemberAddress(vmbcpc));
            View.SetMemberLinkList(mp.MemberLinkGetList(View.UserId));
            double promotionValue = MemberFacade.GetAvailableMemberPromotionValue(View.UserData);
            View.SetMemberBonus(promotionValue);
            decimal scashE7;
            decimal scashPez;
            decimal scash = OrderFacade.GetSCashSum2(View.UserId, out scashE7, out scashPez);
            string pezMemNum = null;
            var ml = mp.PcashMemberLinkGet(View.UserId);
            if (ml.IsLoaded)
            {
                pezMemNum = ml.ExternalUserId;
            }
            View.SetMemberScash(scash, scashE7, scashPez, pezMemNum);
            View.IfReceiveEDM = Helper.IsFlagSet((MemberEdmType)vmbcpc.EdmType, MemberEdmType.PponEvent);
            View.IfReceiveStartedSell = Helper.IsFlagSet((MemberEdmType)vmbcpc.EdmType, MemberEdmType.PponStartedSell);
            View.IfReceivePiinlifeEDM = Helper.IsFlagSet((MemberEdmType)vmbcpc.EdmType, MemberEdmType.PiinlifeEvent);
            View.IfReceivePiinlifeStartedSell = Helper.IsFlagSet((MemberEdmType)vmbcpc.EdmType, MemberEdmType.PiinlifeStartedSell);

            var a = MemberFacade.GetAvailableMemberPromotionValue(View.UserData);

        }

        #endregion Member

        #region Order

        private void SetOrderInfo(int pageNumber, bool filterIsFunds)
        {
            var result = op.MemberOrderListGet(pageNumber, View.PageSize, View.UserData, View.SortExpression, filterIsFunds,
                PponCityGroup.DefaultPponCityGroup.Family.CityId, View.GetStatusTypeDropDown);

            foreach (var o in result.AsEnumerable())
            {
                if (!string.IsNullOrEmpty(o.Field<string>("label_icon_list")))
                {
                    string fastget = "";
                    foreach (var s in o.Field<string>("label_icon_list").Split(','))
                    {
                        if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                            s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                        {
                            fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                        }
                        else if (s == ((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString())
                        {
                            fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                        }
                    }
                    o["item_name"] = fastget + o["item_name"];
                }

                if (!string.IsNullOrEmpty(o.Field<string>("city_list")))
                {
                    List<int> cities = new JsonSerializer().Deserialize<List<int>>(o.Field<string>("city_list"));
                    if (cities.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId))
                    {
                        o["item_name"] = "[" + PponCityGroup.DefaultPponCityGroup.Piinlife.CityName + "]" + o["item_name"];
                    }
                }

                OrderCorrespondingCollection occ = op.OrderCorrespondingListGet(new string[] { OrderCorresponding.Columns.OrderGuid + "=" + o.Field<Guid>("Guid") });
                var oc = occ.FirstOrDefault();
                if (oc != null && oc.Id != 0)
                {
                    var ocCol = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Where(t => (int)t == oc.Type);
                    o["item_name"] += ocCol.Any() ? "[" + Helper.GetEnumDescription(ocCol.FirstOrDefault()) + "]" : string.Empty;
                }
            }
            View.SetOrderData(result);
        }

        protected void OnGetOrderCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.MemberOrderListGetCount(View.UserData, View.CBIsFunds, PponCityGroup.DefaultPponCityGroup.Family.CityId, View.GetStatusTypeDropDown);
        }

        protected void OnOrderPageChanged(object sender, DataEventArgs<int> e)
        {
            SetOrderInfo(e.Data, View.CBIsFunds);
        }

        private void SetLunchKingOrderData(int pageNumber)
        {
            View.SetLunchKingOrderData(op.MemberLunchKingOrderListGet(pageNumber, View.PageSize, View.UserData, View.SortExpression));
        }

        protected void OnGetLunchKingCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.MemberLunchKingOrderListGetCount(View.UserData);
        }

        protected void OnOrderLunchKingPageChanged(object sender, DataEventArgs<int> e)
        {
            SetLunchKingOrderData(e.Data);
        }

        #endregion Order

        #region HidealOrder

        private void SetHidealInfo(int pageNumber)
        {
            View.SetHidealOrderList(hp.GetViewHiDealOrderDealOrderReturnedDialogData(pageNumber, View.PageSize, GetFilter(ViewHiDealOrderDealOrderReturned.Columns.UserName), View.HidealSortExpression));
        }

        protected void OnGetHidealOrderCount(object sender, DataEventArgs<int> e)
        {
            e.Data = hp.ViewHiDealOrderDealOrderReturnedDialogDataCount(GetFilter(ViewHiDealOrderDealOrderReturned.Columns.UserName));
        }

        protected void OnHidealOrderPageChanged(object sender, DataEventArgs<int> e)
        {
            SetHidealInfo(e.Data);
        }

        #endregion HidealOrder

        #region Bonus

        protected void SetBonusListInfo()
        {
            //紅利累積及兌換記錄
            Dictionary<ViewMemberPromotionTransaction, string> dataList = new Dictionary<ViewMemberPromotionTransaction, string>();
            ViewMemberPromotionTransactionCollection vmptc = 
                mp.ViewMemberPromotionTransactionGetList(View.UserData, "create_time", MemberPromotionType.Bouns, true, 500);
            foreach (ViewMemberPromotionTransaction item in vmptc)
            {
                dataList.Add(item, item.CreateId);
            }

            //17Life購物金收支紀錄
            List<UserScashTransactionInfo> vstc = MemberFacade.GetUserScashTransactions(op.ViewScashTransactionGetAll(View.UserData));

            //17Life購物金收支紀錄(舊)
            ViewMemberCashpointListCollection cplist = op.CashpointOldGetRecordsWithTransScash(0, -1, View.UserData);

            //Payeasy購物金消費紀錄
            ViewOrderMemberPaymentCollection vompcp = op.ViewOrderMemberPaymentGetListForPcashRecords(View.UserData);

            //刷卡記錄
            ViewOrderMemberPaymentCollection vompcc = op.ViewOrderMemberPaymentGetListForCreditcardRecords(View.UserData);

            //折價券抵用紀錄
            ViewDiscountOrderCollection vddc = op.ViewDiscountOrderGetList(ViewDiscountOrder.Columns.UseId, View.UserId);

            View.SetBonusData(GetBonusAuditList(), dataList, cplist, vstc, vompcp, vompcc, vddc);
        }

        protected void SetMembershipCardListInfo()
        {
            List<ViewMembershipCardLog> membershipCardConvertList = new List<ViewMembershipCardLog>();
            //會員卡使用紀錄
            var membershipCardConvertCol = mp.ViewMembershipCardLogGetList(View.UserId, ViewMembershipCardLog.Columns.CreateId);
            if (membershipCardConvertCol.Count > 0)
            {
                membershipCardConvertList = membershipCardConvertCol.OrderByDescending(x => x.CreateTime).Take((membershipCardConvertCol.Count < 10) ? membershipCardConvertCol.Count : 10).ToList();
            }

            View.SetMembershipCardData(membershipCardConvertList);

        }

        /// <summary>
        /// 紅利金補入紀錄
        /// </summary>
        /// <returns></returns>
        private AuditCollection GetBonusAuditList()
        {
            return cp.AuditGetList(View.UserData, false);
        }

        #endregion Bonus

        #region ServiceMessage

        private void SetServicelData(int pageNumber)
        {
            Dictionary<ServiceMessage, Guid> dataList = new Dictionary<ServiceMessage, Guid>();
            ServiceMessageCollection serviceList = mp.ServiceMessageGetListForFilter(pageNumber, View.PageSize,
                ServiceMessage.Columns.CreateTime + " desc",
                ServiceMessage.Columns.UserId + " = " + View.UserId);
            foreach (ServiceMessage service in serviceList)
            {
                Guid oid = Guid.Empty;
                if (!string.IsNullOrEmpty(service.OrderId))
                {
                    switch ((ServiceMessageType)service.Type)
                    {
                        case ServiceMessageType.Ppon:
                            Order order = op.OrderGet(LunchKingSite.DataOrm.Order.Columns.OrderId, service.OrderId);
                            oid = order != null ? order.Guid : oid;
                            break;

                        case ServiceMessageType.Hideal:
                            HiDealOrder hidealOrder = hp.HiDealOrderGet(HiDealOrder.Columns.OrderId, service.OrderId);
                            oid = hidealOrder != null ? hidealOrder.Guid : oid;
                            break;

                        default:
                            break;
                    }
                }
                dataList.Add(service, oid);
            }
            View.SetServiceMessageList(dataList);
        }

        protected void OnGetServiceCount(object sender, DataEventArgs<int> e)
        {
            e.Data = mp.ServiceMessageGetCountForFilter(ServiceMessage.Columns.UserId + " = " + View.UserId);
        }

        protected void OnServicePageChanged(object sender, DataEventArgs<int> e)
        {
            //SetServicelData(e.Data);
        }


        protected void OnBuildServiceMessageCategory(object sender, DataEventArgs<int?> e)
        {
            ServiceMessageCategoryCollection serviceCates = mp.ServiceMessageCategoryCollectionGetListByParentId(e.Data);
            View.BuildServiceCategory(serviceCates);
        }

        #endregion ServiceMessage

        #region CustomerServiceMessage
        private void SetCustomerServiceData(int pageNumber, int type = 0)
        {
            //放入篩選條件
            List<string> filter = new List<string>();
            filter.Add(CustomerServiceMessage.Columns.UserId + " = " + View.UserId);

            if (type == 0)
            {
                filter.Add(CustomerServiceMessage.Columns.Status + " <> 4");
            }
            else
            {
                filter.Add(CustomerServiceMessage.Columns.Status + " = 4");
            }
            if (View.OrderGuid != Guid.Empty)
            {
                filter.Add(CustomerServiceMessage.Columns.OrderGuid + " = " + View.OrderGuid);
            }

            Dictionary<CustomerServiceMessage, Guid> dataList = new Dictionary<CustomerServiceMessage, Guid>();
            CustomerServiceMessageCollection serviceList = sp.CustomerServiceMessageGetListForFilter(pageNumber, View.PageSize,
                CustomerServiceMessage.Columns.CreateTime + " desc", filter.ToArray()
                );
            foreach (CustomerServiceMessage service in serviceList)
            {
                dataList.Add(service, service.OrderGuid == null ? Guid.Empty : service.OrderGuid.Value);
            }
            View.SetCustomerServiceMessageList(dataList);
        }

        private void SetSetViewMergeOldNewServiceMessageData(int pageNumber)
        {
            Dictionary<ViewMergeOldNewServiceMessage, Guid> dataList = new Dictionary<ViewMergeOldNewServiceMessage, Guid>();
            ViewMergeOldNewServiceMessageCollection serviceList = sp.ViewMergeOldNewServiceMessageCollectionGet(View.UserId, pageNumber, View.PageSize);
            foreach (var service in serviceList)
            {
                dataList.Add(service, service.OrderGuid == null ? Guid.Empty : service.OrderGuid.Value);
            }
            View.SetViewMergeOldNewServiceMessageList(dataList);
        }

        protected void OnGetNotClosedCount(object sender, DataEventArgs<int> e)
        {
            List<string> filter = new List<string>();
            filter.Add(CustomerServiceMessage.Columns.UserId + " = " + View.UserId);
            filter.Add(CustomerServiceMessage.Columns.Status + " <> 4 ");
            if (View.OrderGuid != Guid.Empty)
            {
                filter.Add(CustomerServiceMessage.Columns.OrderGuid + " = " + View.OrderGuid);
            }
            e.Data = sp.CustomerServiceMessageGetCountForFilter(filter.ToArray());
        }

        protected void OnNotClosedPageChanged(object sender, DataEventArgs<int> e)
        {
            //SetCustomerServiceData(e.Data,0);
        }

        protected void OnGetClosedCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.ViewMergeOldNewServiceMessageGetCount(View.UserId);
        }

        protected void OnClosedPageChanged(object sender, DataEventArgs<int> e)
        {
            //SetCustomerServiceData(e.Data);
            SetSetViewMergeOldNewServiceMessageData(e.Data);
        }

        #endregion

        #region Subscription

        private void SetSubscriptionData()
        {
            View.SetSubscriptionList(pp.SubscriptionGetList(View.UserData), hp.HiDealSubscriptionGetListByUser(View.UserData));
        }

        #endregion Subscription

        #region IDEAS Order
        protected void OnGetIDEASOrderCount(object sender, DataEventArgs<int> e)
        {
            int count = 0;
            string[] filter = GetIDEASOrderSearchFilter();
            if (filter != null && filter.Length > 0)
            {
                count = op.ViewCompanyUserOrderListGetCount(filter);
            }
            e.Data = count;
        }
        protected void OnIDEASOrderPageChanged(object sender, DataEventArgs<int> e)
        {
            SetIDEASOrderInfo(e.Data);
        }

        private void SetIDEASOrderInfo(int pageNumber)
        {
            string[] filter = GetIDEASOrderSearchFilter();
            if (filter == null || filter.Length == 0)
            {
                //無條件
                return;
            }
            ViewCompanyUserOrderCollection orderList = op.ViewCompanyUserOrderGetList(pageNumber, 10, ViewCompanyUserOrder.Columns.CreateTime + " desc ", filter);
            View.SetIDEASOrderList(orderList);
        }

        protected void OnGetTmallCount(object sender, DataEventArgs<int> e)
        {
            int count = 0;
            string[] filter = GetTmallSearchFilter();
            if (filter != null && filter.Length > 0)
            {
                count = op.ViewOrderCorrespondingSmsLogListGetCount(filter);
            }
            e.Data = count;
        }
        protected void OnTmallPageChanged(object sender, DataEventArgs<int> e)
        {
            SetTmallInfo(e.Data);
        }

        private void SetTmallInfo(int pageNumber)
        {
            string[] filter = GetTmallSearchFilter();
            if (filter == null || filter.Length == 0)
            {
                //無條件
                return;
            }
            ViewOrderCorrespondingSmsLogCollection logList = op.ViewOrderCorrespondingSmsLogGetList(pageNumber, 10, ViewOrderCorrespondingSmsLog.Columns.CreatedTime + " desc ", filter);
            View.SetTmallList(logList);
        }

        private void SetStatusInfo()
        {
            Dictionary<string, string> list = new Dictionary<string, string>();
            list.Add("All", "全部");
            list.Add("NotUsed", "憑證可使用");
            list.Add("Expired", "憑證逾期未使用");
            list.Add("Verified", "憑證已使用");
            list.Add("Returning", "退貨處理中");
            list.Add("Returned", "退貨已完成");
            View.SetStatusList(list);
        }

        #endregion IDEAS Order

        protected void OnGetMembershipCardCount(object sender, DataEventArgs<int> e)
        {
            ViewMembershipCardLogCollection vmptc = mp.ViewMembershipCardLogGetList(View.UserId, ViewMembershipCardLog.Columns.CreateTime);
            e.Data = vmptc.Count;
        }

        protected void OnMembershipCardPageChanged(object sender, DataEventArgs<int> e)
        {
            List<ViewMembershipCardLog> membershipCardLogConvertList = new List<ViewMembershipCardLog>();
            //SetSuperDiscountTicketInfo(e.Data);
            var viewMembershipCardLogConverCol = mp.ViewMembershipCardLogGetList(View.UserId, ViewMembershipCardLog.Columns.CreateTime); //op.ViewDiscountOrderGetList(ViewDiscountOrder.Columns.Owner, View.UserId, (int)DiscountCampaignType.SuperDiscountTicket);
            if (viewMembershipCardLogConverCol.Count > 0)
            {
                var pageNumber = (int)Math.Ceiling((double)viewMembershipCardLogConverCol.Count / 10);
                if (e.Data < pageNumber)
                {
                    membershipCardLogConvertList = viewMembershipCardLogConverCol.OrderByDescending(x => x.CreateTime).Skip((e.Data - 1) * 10).Take((viewMembershipCardLogConverCol.Count < 10) ? viewMembershipCardLogConverCol.Count : 10).ToList();
                }
                else
                {
                    membershipCardLogConvertList = viewMembershipCardLogConverCol.OrderByDescending(x => x.CreateTime).Skip((e.Data - 1) * 10).Take(viewMembershipCardLogConverCol.Count - ((e.Data - 1) * 10)).ToList();
                }

            }

            View.SetMembershipCardData(membershipCardLogConvertList);
        }


        #endregion method

        #region private method
        private string[] GetIDEASOrderSearchFilter()
        {
            //檢查有無輸入條件
            if (string.IsNullOrWhiteSpace(View.IDEASSearchData))
            {
                return null;
            }

            string filter = string.Empty;
            switch (View.IDEASFilterType)
            {
                case UserServiceIntergrateIDEASFilterType.OrderId:
                    filter = string.Format("{0} = {1}", ViewCompanyUserOrder.Columns.OrderId, View.IDEASSearchData);
                    break;
                case UserServiceIntergrateIDEASFilterType.PurchaserEmail:
                    filter = string.Format("{0} like {1}", ViewCompanyUserOrder.Columns.PurchaserEmail, View.IDEASSearchData);
                    break;
                case UserServiceIntergrateIDEASFilterType.PurchaserMobile:
                    filter = string.Format("{0} = {1}", ViewCompanyUserOrder.Columns.PurchaserPhone, View.IDEASSearchData);
                    break;
                case UserServiceIntergrateIDEASFilterType.PurchaserName:
                    filter = string.Format("{0} like {1}", ViewCompanyUserOrder.Columns.PurchaserName, View.IDEASSearchData);
                    break;
                default:
                    //異常不查詢
                    return null;
                    //break;
            }

            return new string[] { filter };
        }

        private string[] GetTmallSearchFilter()
        {
            //檢查有無輸入條件
            if (string.IsNullOrWhiteSpace(View.TmallSearchData))
            {
                return null;
            }

            string filter = string.Empty;
            switch (View.TmallFilterType)
            {
                case UserServiceIntergrateTmallFilterType.OrderId:
                    filter = string.Format("{0} = {1}", ViewOrderCorrespondingSmsLog.Columns.RelatedOrderId, View.TmallSearchData);
                    break;
                case UserServiceIntergrateTmallFilterType.Mobile:
                    filter = string.Format("{0} = {1}", ViewOrderCorrespondingSmsLog.Columns.Mobile, View.TmallSearchData.StartsWith("1") ? ("86" + View.TmallSearchData) : View.TmallSearchData);
                    break;
                case UserServiceIntergrateTmallFilterType.CouponSequence:
                    filter = string.Format("{0} = {1}", ViewOrderCorrespondingSmsLog.Columns.SequenceNumber, View.TmallSearchData);
                    break;
                default:
                    //異常不查詢
                    return null;
                    //break;
            }

            return new string[] { filter };
        }

        private string[] GetFilter(object obj)
        {
            List<string> list = new List<string>();
            if (View.UserData != "")
            {
                list.Add(obj + "=" + View.UserData);
            }
            return list.ToArray();
        }

        private void LoadData(string userName = null)
        {
            if (string.IsNullOrEmpty(userName) == false)
            {
                View.UserData = userName;
            }
            else
            {
                int userId;
                if (RegExRules.CheckMobile(View.UserDataView) == false && int.TryParse(View.UserDataView, out userId))
                {
                    Member mem = MemberFacade.GetMember(userId);
                    View.UserData = mem.UserName;
                }
                else
                {
                    Member mem = MemberFacade.GetMember(View.UserDataView);
                    View.UserData = mem.UserName;
                }
            }
            //need UserData
            if (string.IsNullOrEmpty(View.UserData))
            {
                return;
            }
            SetMemberInfo();
            SetOrderInfo(1, View.CBIsFunds);
            //SetHidealInfo(1);
            //SetLunchKingOrderData(1);
            //SetBonusListInfo();
            //SetServicelData(1);
            //SetSubscriptionData();
        }

        #endregion private method
    }
}
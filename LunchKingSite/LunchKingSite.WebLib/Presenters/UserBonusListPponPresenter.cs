﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.BounPoints;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserBonusListPponPresenter : Presenter<IUserBounsListPponView>
    {
        private IMemberProvider mp;
        private IPponProvider pp;
        private IOrderProvider op;

        public UserBonusListPponPresenter()
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.UserName))
            {
                View.PageCount[BounsListType.Scash] = op.ViewScashTransactionGetCount(View.UserName);
                View.PageCount[BounsListType.Bouns] = mp.ViewMemberPromotionTransactionGetOrderGroupListCount(View.UserName);
                View.PageCount[BounsListType.Pcash] = op.ViewOrderMemberPaymentGetListForPcashRecordsCount(View.UserName);
                View.PageCount[BounsListType.Discount] = op.DiscountCodeGetCount(DiscountCode.Columns.Owner + "=" + View.UserId);
                ShowDefaultList(1);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GoBackToListClicked += OnGoBackToListClicked;
            View.UnbindTaishinPay += OnUnbindTaishinPay;
            View.PageChange += this.OnPageChange;
            return true;
        }

        void OnUnbindTaishinPay(object sender, EventArgs e)
        {
            string accessToken = MemberFacade.GetTaishinPayAccessToken(View.UserId);
            MemberFacade.UnbindTaishinPay(View.UserId);
            ShowDefaultList(1);
        }

        protected void ShowDefaultList(int pageindex, BounsListType type = BounsListType.Default)
        {
            if (type == BounsListType.Default)
            {
                decimal userTotalBonusPoint = Convert.ToDecimal(MemberFacade.GetMemberPromotionValue(View.UserName));
                decimal scashE7;
                decimal scashPez;
                decimal scash = OrderFacade.GetSCashSum2(View.UserId, out scashE7, out scashPez);
                //decimal userTotalPayeasy = MemberFacade.GetPayeasyCashPoint(View.UserId);
                bool isTaishinPayLinked;
                decimal userTaishinPayCashPoint = MemberFacade.GetTaisihinPayCashPoint(View.UserId, out isTaishinPayLinked);
                View.Populate_SumValue(scash, scashE7, scashPez, userTotalBonusPoint, userTaishinPayCashPoint, isTaishinPayLinked);
                var generalScashRecrods = GetUserScashTransactionsPaging(
                        op.ViewScashTransactionGetAll(View.UserName),
                        OrderFacade.GetPezScashTransactions(View.UserId),
                        pageindex, View.PageSize[BounsListType.Scash]);
                View.PopulateGeneralScashRecords(generalScashRecrods);
                View.Populate_rro2(mp.ViewMemberPromotionTransactionGetOrderGroupList(View.CurrentPage[BounsListType.Bouns], View.PageSize[BounsListType.Bouns], View.UserName, ViewMemberPromotionTransaction.Columns.CreateTime));
            }

            if (type == BounsListType.Scash)
            {
                var generalScashRecords = GetUserScashTransactionsPaging(
                    op.ViewScashTransactionGetAll(View.UserName),
                    OrderFacade.GetPezScashTransactions(View.UserId),
                    pageindex, View.PageSize[BounsListType.Scash]);
                View.PopulateGeneralScashRecords(generalScashRecords);
            }
            else if (type == BounsListType.Bouns)
            {
                View.Populate_rro2(mp.ViewMemberPromotionTransactionGetOrderGroupList(pageindex, View.PageSize[BounsListType.Bouns], View.UserName, ViewMemberPromotionTransaction.Columns.CreateTime));
            }
            else if (type == BounsListType.Pcash)
            {
                
            }
        }

        protected void OnPponOrderDetailClicked(object sender, DataEventArgs<Guid> e)
        {
            var odl = pp.ViewPponOrderGetList(1, -1, ViewPponOrder.Columns.Guid, ViewPponOrder.Columns.Guid + "=" + e.Data);
            if (odl != null && odl.Count > 0)
            {
                View.TransferToPponDealPage(odl[0].BusinessHourGuid.ToString());
            }
        }

        protected void OnGoBackToListClicked(object sender, EventArgs e)
        {
            ShowDefaultList(1);
        }

        protected void OnPageChange(object sender, DataEventArgs<int> e)
        {
            ShowDefaultList(e.Data, sender is BounsListType ? (BounsListType)sender : BounsListType.Scash);
        }

        public List<UserScashTransactionInfo> GetUserScashTransactionsPaging(
            ViewScashTransactionCollection scashTransactions, List<PezScashTransaction> pscashRecords, int pageindex, int pageLength)
        {
            List<UserScashTransactionInfo> vstInfos = MemberFacade.GetUserScashTransactions(scashTransactions, pscashRecords);
            return vstInfos.Skip(pageLength * (pageindex - 1)).Take(pageLength).ToList();
        }
    }
}
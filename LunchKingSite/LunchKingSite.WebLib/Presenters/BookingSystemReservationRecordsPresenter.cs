﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Presenters
{
    public class BookingSystemReservationRecordsPresenter : Presenter<IBookingSystemReservationRecordsView>
    {
        private IBookingSystemProvider bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (string.IsNullOrEmpty(View.UserName))
            {
                View.RedirectToLogin();
                return true;
            }
            View.MemberKey = MemberFacade.GetUniqueId(View.UserName).ToString();
            View.BindReservationRecord(bp.ReservationRecordGetByServiceNameAndMemberKey(View.MemberKey, BookingSystemFacade.GetBookingSystemServiceName(View.ApiKey, BookingSystemApiServiceType.PponService), (View.IsExpiredRecord) ? BookingSystemRecordType.Expired : BookingSystemRecordType.Unexpired));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();

            View.IsExpiredReservationChange += this.OnIsExpiredRecordChange;
            View.CancelReservation += this.OnCancelReservation;
            
            return true;
        }

        #region event

        protected void OnIsExpiredRecordChange(object sender, DataEventArgs<int> e)
        {
            View.BindReservationRecord(bp.ReservationRecordGetByServiceNameAndMemberKey(View.MemberKey, BookingSystemFacade.GetBookingSystemServiceName(View.ApiKey, BookingSystemApiServiceType.PponService), (BookingSystemRecordType)e.Data));
        }

        protected void OnCancelReservation(object sender, DataEventArgs<int> e) 
        {
            ViewBookingSystemReservationList vbsr = bp.ViewBookingSystemReservationListGet(e.Data);

            if (vbsr != null)
            {
                BookingSystemReservation record = bp.BookingSystemReservationGet(e.Data);
                Member m = mp.MemberGet(int.Parse(record.MemberKey));
                string gender =  ( m.Gender.GetValueOrDefault(0)==(int)GenderType.Unknown )  ?"" : ( m.Gender.GetValueOrDefault(0)==(int)GenderType.Male ) ? "先生" : "小姐";
                
                ViewBookingSystemStore store = bp.ViewBookingSystemStoreGetByStoreGuid(bp.BookingSystemStoreGetBookingId(vbsr.BookingSystemStoreBookingId).StoreGuid, BookingType.Coupon);

                //ReservationCancelMail
                var reservationCancelMailInfo = new ReservationCancelMailInfomation
                {
                    MemberName = m.DisplayName  + " " + gender,
                    SellerName = store.SellerName,
                    StoreName = store.StoreName,
                };
                BookingSystemFacade.SendReservationCancelMail(m.UserEmail, reservationCancelMailInfo);

            }
            bp.SetReservationRecordCancel(e.Data, View.UserName);
            View.BindReservationRecord(bp.ReservationRecordGetByServiceNameAndMemberKey(View.MemberKey, BookingSystemFacade.GetBookingSystemServiceName(View.ApiKey, BookingSystemApiServiceType.PponService), (BookingSystemRecordType)((View.IsExpiredRecord) ? 1 : 0)));
        }

        #endregion
    }
}

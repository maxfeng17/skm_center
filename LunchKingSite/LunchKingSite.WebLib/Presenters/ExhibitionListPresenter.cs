﻿using System;
using System.Linq;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class ExhibitionListPresenter : Presenter<IExhibitionList>
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private ISysConfProvider cp = ProviderFactory.Instance().GetConfig();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        #region method

        #endregion method
    }
}

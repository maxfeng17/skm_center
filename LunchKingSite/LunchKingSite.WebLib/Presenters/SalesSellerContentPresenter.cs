﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using System.Net.Mail;
using LunchKingSite.BizLogic.Model.SellerSales;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesSellerContentPresenter : Presenter<ISalesSellerContentView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private IPponProvider pp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.SellerGuid);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SellerApprove += OnSellerApprove;
            View.CreateProposal += OnCreateProposal;
            View.Save += OnSave;
            View.SaveLocation += OnSaveLocation;
            View.UpdatePhoto += OnUpdatePhoto;
            View.ImageListChanged += OnImageListChanged;
            View.ISPApply += OnISPApply;
            View.ISPReject += OnISPReject;

            //View.ChangeLog += OnChangeLog;
            //View.ManageLog += OnManageLog;

            //SetSalesmanNameArray();
            SetVbsISP();

            return true;
        }

        public SalesSellerContentPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IMemberProvider memProv, IPponProvider pponProv)
        {
            sp = sellerProv;
            hp = humProv;
            mp = memProv;
            pp = pponProv;
        }

        #region event

        protected void OnSellerApprove(object sender, DataEventArgs<SellerTempStatus> e)
        {
            Seller seller = sp.SellerGet(View.SellerGuid);
            Seller ori = seller.Clone();
            seller.TempStatus = (int)e.Data;
            seller.ApproveTime = seller.ModifyTime = DateTime.Now;
            seller.NewCreated = false;
            seller.Message = string.Empty;
            SellerSave(seller, ori);

            LoadData(View.SellerGuid);
        }

        protected void OnCreateProposal(object sender, DataEventArgs<Guid> e)
        {
            Guid SellerGuid = e.Data;

            Seller seller = sp.SellerGet(SellerGuid);
            if(seller != null && seller.IsLoaded)
            {
                if (string.IsNullOrEmpty(seller.SignCompanyID))
                {
                    View.ShowMessage("請輸入統一編號/ID", SellerContentMode.SalesError);
                    return;
                }
                if (string.IsNullOrEmpty(seller.SellerAddress))
                {
                    View.ShowMessage("請輸入公司地址", SellerContentMode.SalesError);
                    return;
                }
                if (string.IsNullOrEmpty(seller.SellerBossName))
                {
                    View.ShowMessage("請輸入負責人", SellerContentMode.SalesError);
                    return;
                }
                //在申請提案單時，則檢查轉介人員與%數欄位是否有值，若其一有值，則另一欄一定要輸入儲存後，才能提案
                if (!string.IsNullOrEmpty(seller.ReferralSale))
                {
                    if(seller.ReferralSalePercent == null || seller.ReferralSalePercent == 0)
                    {
                        //View.ShowMessage("請輸入轉介人員%數", SellerContentMode.SalesError);
                        //return;
                    }
                }
                if (seller.ReferralSalePercent != null && seller.ReferralSalePercent != 0)
                {
                    if (string.IsNullOrEmpty(seller.ReferralSale))
                    {
                        //View.ShowMessage("請輸入轉介人員", SellerContentMode.SalesError);
                        //return;
                    }
                }
            }

            int Id = default(int);
            if (ProposalFacade.IsVbsProposalNewVersion() 
                && View.ProposalDeliveryType == (int)DeliveryType.ToHouse 
                && View.IsTravelProposal == false
                && View.IsPiinLifeProposal == false)
            {
                Id = ProposalFacade.CreateProposalByHouse(View.UserName, "", SellerGuid, View.UserName, false, View.IsWms, ProposalCreatedType.Sales);
            }
            else
            {
                Id = ProposalFacade.CreateProposal(View.SelectDevelopeSales, View.SelectOperationSales, SellerGuid, View.UserName, View.ProposalDeliveryType,
                View.IsTravelProposal, View.IsPBeautyProposal, false, View.IsPiinLifeProposal, ProposalCreatedType.Sales);
            }
            View.RedirectProposal(sp.ProposalGet(Id));
        }

        protected void OnSave(object sender, EventArgs e)
        {
            if (View.SellerGuid != Guid.Empty)
            {
                //異動賣家
                Seller sellerNow = new Seller();
                sellerNow = View.GetSellerData(sellerNow);

                #region 轉介人員檢查
                if(View.ReferralPercent != 0)
                {
                    if (string.IsNullOrEmpty(View.ReferralSale))
                    {
                        View.ShowMessage("%數欄位有值時，轉介人員必定有值", SellerContentMode.SalesError);
                        return;
                    }
                }
                
                #endregion

                #region 統一編號/ID 驗證
                if (!View.NoCompanyIDCheck)
                {
                    if (sellerNow.SignCompanyID.Length != 0)
                    {
                        //有填沒有勾才要檢核
                        string msg = string.Empty;
                        if (sellerNow.SignCompanyID.Length == 8)
                        {
                            msg = RegExRules.CompanyNoCheck(sellerNow.SignCompanyID);
                        }
                        else
                        {
                            msg = RegExRules.PersonalIdCheck(sellerNow.SignCompanyID);
                        }
                        if (!string.IsNullOrWhiteSpace(msg))
                        {
                            View.ShowMessage(msg, SellerContentMode.CompanyIdCheck);
                            return;
                        }

                    }
                }
                #endregion

                #region 檢查 統編/ID 是否已存在商家
                


                Seller cs = sp.SellerGetByCompany(sellerNow.SellerName, sellerNow.SignCompanyID);
                if (cs.IsLoaded)
                {
                    if (View.hid == "True1")
                    {
                        return;
                    }
                    else if (View.hid == "False1")
                    {
                        View.ShowMessage("", SellerContentMode.TurnUrl, cs.SignCompanyID);
                        return;
                    }
                }
                else
                {
                    cs = sp.SellerGet(Seller.Columns.SignCompanyID, sellerNow.SignCompanyID);
                    if (cs.IsLoaded)
                    {
                        if (View.hid == "True2")
                        {
                            //繼續儲存
                        }
                        else if (View.hid == "False2")
                        {
                            return;
                        }
                    }
                }
                #endregion

                #region 檢查轉介人員是否為員工
                bool chkReferral = false;
                if (!string.IsNullOrEmpty(View.ReferralSale))
                {
                    var emp = hp.ViewEmployeeGet("email", View.ReferralSale);
                    if (emp.IsLoaded)
                    {
                        //需再判斷是否為業務
                        var dept = hp.DepartmentGet(emp.DeptId);
                        if (dept.ParentDeptId == EmployeeDept.S000.ToString())
                        {
                            chkReferral = true;
                        }
                    }
                }
                if (string.IsNullOrEmpty(View.ReferralSale))
                {
                    chkReferral = true;
                }
                if (!chkReferral)
                {
                    View.ShowMessage("輸人的帳號不存在於系統中，無法進行存檔。請確認該人員資料是否已在系統中建立完成。(人員的系統建置請洽人資部同仁)", SellerContentMode.SalesError);
                    return;
                }
                
                #endregion

                //儲存商家資訊
                Seller seller = sp.SellerGet(View.SellerGuid);
                Seller ori = seller.Clone();
                seller = View.GetSellerData(seller);
                SellerSave(seller, ori);
                //檢查財務資料
                EmailFacade.SendChangeSellerMail(seller, ori);
                //儲存sellertree
                SaveSellerTree(seller);
                //儲存業務資料
                SellerSaleCollection sellerSale = sp.SellerSaleGetBySellerGuid(View.SellerGuid);
                SellerSaleCollection oriSellerSale = sellerSale.Clone();
                SaveSellerSales(oriSellerSale);

                //儲存第二線客服資料
                if (View.SecondService != "")
                {
                    SellerMappingEmployee sme = hp.SellerMappingEmployeeGet(View.SellerGuid);
                    if (sme.IsLoaded)
                    {
                        if (sme.EmpId != View.SecondService)
                        {
                            sme.EmpId = View.SecondService;
                            hp.SellerMappingEmployeeSet(sme);
                            SellerLog(seller.Guid, "[系統] 修改第二客服關聯");
                        }
                    }
                    else
                    {
                        SellerMappingEmployee smed = new SellerMappingEmployee();
                        smed.SellerGuid = seller.Guid;
                        smed.EmpId = View.SecondService;
                        smed.EmpType = 1;
                        smed.CreateId = View.UserName;
                        smed.CreateTime = DateTime.Now;
                        hp.SellerMappingEmployeeSet(smed);
                        SellerLog(seller.Guid, "[系統] 建立第二客服關聯");
                    }

                    
                }


                SellerFacade.ShoppingCartFreightsCreateBySellerGuid(seller.Guid);
            }
            else
            {
                //新增賣家
                if (!CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.Create))
                {
                    View.ShowMessage("無建立商家權限!!", SellerContentMode.PrivilegeError);
                    return;
                }

                ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
                if (emp.IsLoaded)
                {
                    Seller seller = new Seller();
                    seller = View.GetSellerData(seller);

                    #region 統一編號/ID 驗證
                    if (!View.NoCompanyIDCheck)
                    {
                        if (seller.SignCompanyID.Length != 0)
                        {
                            //有填沒有勾才要檢核
                            string msg = string.Empty;
                            if (seller.SignCompanyID.Length == 8)
                            {
                                msg = RegExRules.CompanyNoCheck(seller.SignCompanyID);
                            }
                            else
                            {
                                msg = RegExRules.PersonalIdCheck(seller.SignCompanyID);
                            }
                            if (!string.IsNullOrWhiteSpace(msg))
                            {
                                View.ShowMessage(msg, SellerContentMode.CompanyIdCheck);
                                return;
                            }
                           
                        }
                    }
                    #endregion

                    #region 檢查 統編/ID 是否已存在商家

                    Seller cs = sp.SellerGetByCompany(seller.SellerName, seller.SignCompanyID);
                    if (cs.IsLoaded)
                    {
                        if (View.hid == "True1")
                        {
                            return;
                        }
                        else if (View.hid == "False1")
                        {
                            View.ShowMessage("", SellerContentMode.TurnUrl, cs.SignCompanyID);
                            return;
                        }
                    }
                    else
                    {
                        cs = sp.SellerGet(Seller.Columns.SignCompanyID, seller.SignCompanyID);
                        if (cs.IsLoaded)
                        {                       
                            if (View.hid == "True2")
                            {
                                //繼續儲存
                            }
                            else if (View.hid == "False2")
                            {
                                return;
                            }
                        }
                    }
                    
                    #endregion

                    seller.TempStatus = (int)SellerTempStatus.Applied;
                    seller.SellerId = SellerFacade.GetNewSellerID();
                    seller.SalesId = emp.UserId;
                    seller.SellerSales = emp.EmpName;
                    seller.CreateId = View.UserName;
                    seller.CreateTime = DateTime.Now;
                    seller.Department = (int)DepartmentTypes.Ppon;
                    seller.VendorReceiptType = (int)View.ReceiptType;
                    seller.Othermessage = View.AccountingMessage;
                    seller.IsCloseDown = View.IsCloseDown;


                    if (!View.IsCloseDown)
                    {
                        seller.StoreStatus = (int)View.Status;
                        seller.CloseDownDate = null;
                    }
                    else
                    {
                        seller.StoreStatus = 1; //倒店 => 隱藏
                        seller.CloseDownDate = View.CloseDownDate;

                        List<int> vourcher_event_id_list = VourcherFacade.VourcherEventIdListGetByStoreGuid(seller.Guid);
                        //刪除所有優惠券分店
                        VourcherFacade.VourcherStoreDeleteByStoreGuid(seller.Guid);
                        VourcherFacade.ChangeLogAdd(Store.Schema.TableName, seller.Guid, "{\"分店倒閉隱藏調整\":\"刪除優惠券分店,影響的優惠券編號" + string.Join(",", vourcher_event_id_list.ToArray()) + "\"})", true);
                    }
                    sp.SellerSet(seller);

                    SaveSellerTree(seller);
                    //儲存業務資料
                    InitialSaveSellerSales(seller.Guid, emp.UserId);
                    SellerLog(seller.Guid, "[系統] 建立商家資訊");

                    //儲存二線客服資料
                    if (View.SecondService != "")
                    {

                        SellerMappingEmployee sme = new SellerMappingEmployee();
                        sme.SellerGuid = seller.Guid;
                        sme.EmpId = View.SecondService;
                        sme.EmpType = 1;
                        sme.CreateId = View.UserName;
                        sme.CreateTime = DateTime.Now;

                        hp.SellerMappingEmployeeSet(sme);
                    }

                    SellerFacade.ShoppingCartFreightsCreateBySellerGuid(seller.Guid);


                    View.RedirectSeller(seller.Guid);

                    
                }
            }

            

            LoadData(View.SellerGuid);
        }

        /// <summary>
        /// 儲存SellerTree
        /// </summary>
        /// <param name="seller"></param>
        private void SaveSellerTree(Seller seller)
        {
            SellerTree sellertree = sp.SellerTreeGetListBySellerGuid(seller.Guid).FirstOrDefault();

            SellerTree ori = sellertree == null ? new SellerTree() : sellertree.Clone();

            List<Guid> parentGuid = new List<Guid>();
            if (View.ParentGuid != Guid.Empty)
                parentGuid.Add(View.ParentGuid);


            ResourceAclCollection acls = new ResourceAclCollection();
            List<Guid> childSellerGuidList = new List<Guid>();
            List<string> accountList = new List<string>();
            List<Guid> middleSellerGuidList = new List<Guid>();


            #region 中間層拔權限(在拔上層前先做) 1.有上層->無上層 2. 換上層
            //if ((View.ParentGuid == Guid.Empty && ori.ParentSellerGuid != Guid.Empty) || (View.ParentGuid != Guid.Empty && ori.ParentSellerGuid != View.ParentGuid && ori.ParentSellerGuid != Guid.Empty))
            //{
            //    //拔除上層


                
            //    childSellerGuidList = sp.SellerTreeMiddleGetSellerGuid(new List<Guid> { seller.Guid }, null).ToList();

            //    if (childSellerGuidList.Count() > 0)
            //    {
            //        //自己原為中間層
            //        middleSellerGuidList.Add(seller.Guid);

            //        //往下找所有的下層
            //        List<Guid> child = new List<Guid>();
            //        child = sp.SellerTreeMiddleGetSellerGuid(childSellerGuidList, null).ToList();
            //        while (child.Count > 0)
            //        {
            //            childSellerGuidList.AddRange(child);
            //            child = sp.SellerTreeMiddleGetSellerGuid(child, null).ToList();
            //        }
            //    }
            //    else
            //    {
            //        //找下層的商家
            //        childSellerGuidList = sp.SellerTreeMiddleGetSellerGuid(new List<Guid> { ori.ParentSellerGuid }, seller.Guid).ToList();
            //        if (childSellerGuidList.Count() > 0)
            //        {
            //            //原上層為中間層
            //            middleSellerGuidList.Add(ori.ParentSellerGuid);

            //            //往上找所有的中間層
            //            SellerTreeCollection stc = sp.SellerTreeGetListBySellerGuid(seller.Guid);
            //            while (stc.Count() > 0)
            //            {
            //                if (stc[0].ParentSellerGuid != stc[0].RootSellerGuid)
            //                {
            //                    middleSellerGuidList.Add(stc[0].ParentSellerGuid);
            //                }

            //                stc = sp.SellerTreeGetListBySellerGuid(stc[0].ParentSellerGuid);
            //            }
            //        }
            //    }

            //    //找身上的帳號
            //    accountList = mp.ResourceAclGetListByResourceGuidList(middleSellerGuidList)
            //                .Where(x => ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
            //                        && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
            //                .GroupBy(x => new { x.AccountId}).Select(y => y.Key.AccountId).ToList();


            //    foreach (string accountid in accountList)//多帳號
            //    {
            //        foreach (var childSellerGuid in childSellerGuidList)//多下層商家
            //        {
            //            var childAclSeller = mp.ResourceAclGet(accountid, ResourceAclAccountType.VendorBillingSystem, childSellerGuid, ResourceType.Seller);
            //            acls.Add(childAclSeller);

            //            var childAclStore = mp.ResourceAclGet(accountid, ResourceAclAccountType.VendorBillingSystem, childSellerGuid, ResourceType.Store);
            //            acls.Add(childAclStore);
            //        }
            //    }

            //    SellerFacade.ResourceAclDelete(acls, "身分權限清除", View.UserName);
            //}
            #endregion

            SellerFacade.SellerTreeSet(seller.Guid, parentGuid, View.UserName, string.Empty);

            #region 中間層加權限(在塞上層後再做)

            ////reset
            //acls = new ResourceAclCollection();
            //childSellerGuidList = new List<Guid>();
            //accountList = new List<string>();
            //middleSellerGuidList = new List<Guid>();

            //if (View.ParentGuid != Guid.Empty && ori.ParentSellerGuid != View.ParentGuid)
            //{
            //    //加蓋上層

                

            //    //找下層的商家,且只找自己就好,原本就有的下層不要再塞
            //    childSellerGuidList = sp.SellerTreeMiddleGetSellerGuid(new List<Guid> { View.ParentGuid }, seller.Guid).ToList();
            //    if (childSellerGuidList.Count() > 0)
            //    {
            //        //上層變中間層
            //        middleSellerGuidList.Add(View.ParentGuid);


            //        //往上找所有的中間層
            //        SellerTreeCollection stc = sp.SellerTreeGetListBySellerGuid(seller.Guid);
            //        while (stc.Count() > 0)
            //        {
            //            if (stc[0].ParentSellerGuid != stc[0].RootSellerGuid)
            //            {
            //                middleSellerGuidList.Add(stc[0].ParentSellerGuid);
            //            }

            //            stc = sp.SellerTreeGetListBySellerGuid(stc[0].ParentSellerGuid);
            //        }

            //    }
            //    else
            //    {
            //        //找下層的商家
            //        childSellerGuidList = sp.SellerTreeMiddleGetSellerGuid(new List<Guid> { seller.Guid }, null).ToList();
            //        if (childSellerGuidList.Count() > 0)
            //        {
            //            //自己變中間層
            //            middleSellerGuidList.Add(seller.Guid);

            //            //往下找所有的下層
            //            List<Guid> child = new List<Guid>();
            //            child =  sp.SellerTreeMiddleGetSellerGuid(childSellerGuidList,null).ToList();
            //            while (child.Count > 0)
            //            {
            //                childSellerGuidList.AddRange(child);
            //                child = sp.SellerTreeMiddleGetSellerGuid(child, null).ToList();
            //            }
                        
            //        }
            //    }

                

            //    //找中間層的帳號
            //    accountList = mp.ResourceAclGetListByResourceGuidList(middleSellerGuidList)
            //                    .Where(x => ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
            //                            && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
            //                    .GroupBy(x => new { x.AccountId}).Select(y => y.Key.AccountId).ToList();
                



            //    foreach (string accountid in accountList)//多帳號
            //    {
            //        foreach (var childSellerGuid in childSellerGuidList)//多下層商家
            //        {
            //            var childAclSeller = new ResourceAcl()
            //            {
            //                AccountId = accountid,
            //                AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
            //                ResourceGuid = childSellerGuid,
            //                ResourceType = (int)ResourceType.Seller,
            //                PermissionType = (int)PermissionType.Read,
            //                PermissionSetting = (int)PermissionType.Read
            //            };
            //            acls.Add(childAclSeller);

            //            var childAclStore = new ResourceAcl()
            //            {
            //                AccountId = accountid,
            //                AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
            //                ResourceGuid = childSellerGuid,
            //                ResourceType = (int)ResourceType.Store,
            //                PermissionType = (int)PermissionType.Read,
            //                PermissionSetting = (int)PermissionType.Read
            //            };
            //            acls.Add(childAclStore);
            //        }
            //    }

            //    SellerFacade.ResourceAclSetList(acls, "新增帳號權限", View.UserName);
            //}
            #endregion

            sellertree = sp.SellerTreeGetListBySellerGuid(seller.Guid).FirstOrDefault();
            sellertree = sellertree == null ? new SellerTree() : sellertree;

            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, sellertree))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                SellerLog(View.SellerGuid, string.Join("|", changeList));
            }
        }

        protected void OnSaveLocation(object sender, EventArgs e)
        {
            if (View.SellerGuid != Guid.Empty)
            {
                Seller seller = sp.SellerGet(View.SellerGuid);
                Seller ori = seller.Clone();
                seller = View.GetSellerLocationData(seller);
                SellerSave(seller, ori);
            }
            else
            {
                View.ShowMessage("請先建立商家基本資料資料!!", SellerContentMode.PrivilegeError);
                return;
            }


            LoadData(View.SellerGuid);
        }

        public void OnUpdatePhoto(object sender, DataEventArgs<PhotoInfo> e)
        {
            int d = sp.SellerGet(View.SellerGuid).Department;
            switch ((DepartmentTypes)d)
            {
                case DepartmentTypes.Delicacies:
                    if (e.Data.Type == UploadFileType.ItemPhoto)
                    {
                        e.Data.Type = UploadFileType.DelicaciesItemPhoto;
                    }
                    else if (e.Data.Type == UploadFileType.SellerPhoto)
                    {
                        e.Data.Type = UploadFileType.DelicaciesSellerPhoto;
                    }
                    break;
            }

            View.SetImageFile(e.Data);
        }

        private void OnImageListChanged(object sender, DataEventArgs<string> e)
        {
            Seller s = sp.SellerGet(View.SellerGuid);
            s.SellerLogoimgPath = e.Data;
            if (s.IsDirty)
            {
                sp.SellerSet(s);
            }
            View.SetImageGrid(s.SellerLogoimgPath);
        }

        private void OnISPApply(object sender, DataEventArgs<KeyValuePair<bool,ServiceChannel>> e)
        {
            string msg = "";
            bool isUpdate = e.Data.Key;
            if (e.Data.Value == ServiceChannel.FamilyMart)
            {
                ISPFacade.ISPApply(View.SellerGuid, ServiceChannel.FamilyMart, View.ReturnCycleFamily, View.ReturnTypeFamily, View.ReturnOtherFamily, View.UserName, View.ReturnAddress, View.ContactName, View.ContactTel, View.ContactMobile, View.ContactEmail, out msg, isUpdate);
                if (!string.IsNullOrEmpty(msg))
                {
                    View.ShowMessage(msg, SellerContentMode.SalesError);
                }
            }
            else if (e.Data.Value == ServiceChannel.SevenEleven)
            {
                ISPFacade.ISPApply(View.SellerGuid, ServiceChannel.SevenEleven, View.ReturnCycleSeven, View.ReturnTypeSeven, View.ReturnOtherSeven, View.UserName, View.ReturnAddress, View.ContactName, View.ContactTel, View.ContactMobile, View.ContactEmail, out msg, isUpdate);
                if (!string.IsNullOrEmpty(msg))
                {
                    View.ShowMessage(msg, SellerContentMode.SalesError);
                }
                
            }
           
            
            SetVbsISP();
        }

        private void OnISPReject(object sender, DataEventArgs<KeyValuePair<string, ServiceChannel>> e)
        {
            if (e.Data.Value == ServiceChannel.FamilyMart)
            {
                var ispFamily = sp.VbsInstorePickupGet(View.SellerGuid, (int)ServiceChannel.FamilyMart);
                if (ispFamily.IsLoaded)
                {
                    ispFamily.Status = (int)ISPStatus.Reject;
                    ispFamily.VerifyMemo = e.Data.Key;
                    ispFamily.ModifyId = View.UserName;
                    ispFamily.ModifyTime = DateTime.Now;
                    sp.VbsInstorePickupSet(ispFamily);
                }
            }
            else if (e.Data.Value == ServiceChannel.SevenEleven)
            {
                if (config.EnableSevenIsp)
                {
                    var ispSeven = sp.VbsInstorePickupGet(View.SellerGuid, (int)ServiceChannel.SevenEleven);
                    if (ispSeven.IsLoaded)
                    {
                        ispSeven.Status = (int)ISPStatus.Reject;
                        ispSeven.VerifyMemo = e.Data.Key;
                        ispSeven.ModifyId = View.UserName;
                        ispSeven.ModifyTime = DateTime.Now;
                        sp.VbsInstorePickupSet(ispSeven);
                    }

                }
            }
            
            

            
            SetVbsISP();
        }

        private void SellerSave(Seller seller, Seller ori)
        {
            seller.ModifyId = View.UserName;
            seller.ModifyTime = DateTime.Now;
            sp.SellerSet(seller);


            // 變更紀錄
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(ori, seller))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                SellerLog(View.SellerGuid, string.Join("|", changeList));
            }

            /*
             * 轉介人員 - 若 A 欄資料寫入後，且第一檔己開檔，A 欄則需要變更權限方可變更，變更後需要發送通知給人資部門
             * */
            if (View.IsReferralModifyMail)
            {
                List<string> mailToUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.ReferralHRMail);

                string subject = "【轉介人員中途異動】系統通知信";
                string body = string.Format(@"商家代號：{0}<br/>
                                商家名稱：{1}<br/>
                                負責業務：{2}<br/>
                                轉介人員1：{3}<br/>
                                商家頁連結：{4}<br/>
                                執行異動人員：{5}<br/>", seller.SellerId, seller.SellerName, seller.SalesId, seller.ReferralSale,  config.SiteUrl + "/sal/SellerContent.aspx?sid=" + seller.Guid.ToString(), View.UserName);

                body += "<p>請計算轉介獎金人員協助注意。本信件為系統主動發出，請勿直接回覆。若有異動問題請直接洽詢執行異動人員，謝謝。";
                SendEmail(mailToUser, subject, body);
            }
        }

        //protected void OnChangeLog(object sender, DataEventArgs<string> e)
        //{
        //    SellerLog(View.SellerGuid, e.Data);
        //    LoadData(View.SellerGuid);
        //}
        //protected void OnManageLog(object sender, DataEventArgs<string> e)
        //{
        //    LoadData(View.SellerGuid);
        //}
        #endregion

        #region Private Method

        private void SellerLog(Guid sid, string changeLog)
        {
            SellerFacade.SellerLogSet(sid, changeLog, View.UserName);
            //SellerChangeLog log = new SellerChangeLog();
            //log.SellerGuid = sid;
            //log.ChangeLog = changeLog;
            //log.CreateId = View.UserName;
            //log.CreateTime = DateTime.Now;
            //sp.SellerChangeLogSet(log);
        }

        private void StoreLog(Guid stid, string changeLog)
        {
            StoreChangeLog log = new StoreChangeLog();
            log.StoreGuid = stid;
            log.ChangeLog = changeLog;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            sp.StoreChangeLogSet(log);
        }

        private void LoadData(Guid sid)
        {
            //View.SellerList = JsonConvert.SerializeObject(
            //    sp.SellerGetList().Select(x => new
            //    {
            //        value = x.SellerName,
            //        label = x.SellerName,
            //        id = x.Guid
            //    })
            //);

            Seller seller = sp.SellerGet(sid);
            if (seller.IsLoaded)
            {
                ViewEmployee emp = new ViewEmployee();
                if (seller.SalesId != null)
                {
                    emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, seller.SalesId);
                }
                //變更記錄
                SellerChangeLogCollection changeLogs = sp.SellerChangeLogGetList(sid);
                //經營筆記
                SellerManageLogCollection manageLogs = sp.SellerManageLogGetList(sid);
                //合約上傳
                SellerContractFileCollection scfs = sp.SellerContractFileGetListBySellerGuid(seller.Guid);


                //SellerTree
                Dictionary<Seller, Seller> ds = new Dictionary<Seller, Seller>();
                var ParentTree = sp.SellerTreeGetListBySellerGuid(sid).FirstOrDefault();
                if(ParentTree!=null)
                {
                    SellerTreeCollection stc = sp.SellerTreeGetListByParentSellerGuid(ParentTree.ParentSellerGuid);
                    Seller ParentSeller = sp.SellerGet(ParentTree.ParentSellerGuid);
                    foreach (var st in stc)
                    {
                        ds.Add(sp.SellerGet(st.SellerGuid), ParentSeller);
                    }
                }

                //購物車異動紀錄
                List<Guid> guidList = new List<Guid>();
                guidList.Add(seller.Guid);
                ShoppingCartFreightCollection freis = sp.ShoppingCartFreightsGetBySellerGuid(guidList);
                List<ShoppingCartManageList> freiList = new List<ShoppingCartManageList>();

                foreach (ShoppingCartFreight sf in freis)
                {
                    freiList.Add(new ShoppingCartManageList()
                    {
                        Id = sf.Id,
                        UniqueId = sf.UniqueId,
                        SellerName = SellerNameGet(sf.SellerGuid),
                        FreightsStatusName = GetStatus(sf.FreightsStatus),
                        FreightsName = sf.FreightsName,
                        NoFreightLimit = sf.NoFreightLimit,
                        Freights = sf.Freights,
                        CreateTime = sf.CreateTime
                    });
                }

                ShoppingCartFreightsLogCollection scLogs = sp.ShoppingCartFreightsLogGetByIds(
                                                                freis.Select(x => x.Id).Distinct().ToList());

                SetVbsISP();

                var acls = mp.ResourceAclGetListByResourceGuidList(new List<Guid> { sid })
                            .Where(x => ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                                    && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
                            .GroupBy(x => new { x.AccountId, x.ResourceGuid });
                foreach(var acl in acls)
                {
                    VbsMembership vbsMembership = mp.VbsMembershipGetByAccountId(acl.Key.AccountId);
                    if (vbsMembership.ViewWmsRight && vbsMembership.WmsContract == config.WmsContractVersion)
                    {
                        View.IsWmsEnabled = true;
                        break;
                    }
                }

                View.SetSellerContent(seller, emp, changeLogs, manageLogs, scfs, ds, freiList, scLogs);
            }
            else
            {
                View.ShowMessage("查無商家資料。", SellerContentMode.SellerNotFound);
            }
        }

        private void SetVbsISP()
        {
            View.IsISPVerifyFamily = false;
            View.IsISPEnabledFamily = false;
            View.IsISPVerifySeven = false;
            View.IsISPEnabledSeven = false;


            ViewVbsInstorePickupCollection vip = sp.ViewVbsInstorePickupCollectionGet(View.SellerGuid);
            View.ViewVbsInstorePickupViewData = vip;


            ViewVbsInstorePickup vipFamily = sp.ViewVbsInstorePickupGet(View.SellerGuid, (int)ServiceChannel.FamilyMart);
            View.ViewVbsInstorePickupFamily = vipFamily;
            if (vipFamily.IsLoaded)
            {
                View.IsISPVerifyFamily = vipFamily.Status == (int)ISPStatus.Verifying;
                View.IsISPEnabledFamily = vipFamily.Status == (int)ISPStatus.Complete;
            }


            ViewVbsInstorePickup vipSeven = sp.ViewVbsInstorePickupGet(View.SellerGuid, (int)ServiceChannel.SevenEleven);
            View.ViewVbsInstorePickupSeven = vipSeven;
            if (vipSeven.IsLoaded)
            {
                View.IsISPVerifySeven = vipSeven.Status == (int)ISPStatus.Verifying;
                View.IsISPEnabledSeven = vipSeven.Status == (int)ISPStatus.Complete;
            }
        }

        private string GetStatus(int s)
        {
            return Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (ShoppingCartFreightStatus)s);
        }

        private string SellerNameGet(Guid g)
        {
            string result = "";
            Seller seller = sp.SellerGet(g);
            if (seller.IsLoaded)
            {
                result = string.Format("{0}-{1}", seller.SellerId, seller.SellerName);
            }
            return result;
        }

        private void SendEmail(List<string> mailToUser, string subject ,string body)
        {
            foreach (string user in mailToUser)
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(config.SystemEmail);
                msg.To.Add(user);
                msg.Subject = subject;
                msg.Body = body;
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        /// <summary>
        /// 儲存業務資料
        /// </summary>
        /// <param name="oriSellerSale"></param>
        private void SaveSellerSales(SellerSaleCollection oriSellerSale)
        {
            //F5會重複
            if (View.DevelopeSales != "[]" || View.OperationSales != "[]")
            {
                List<SellerSalesModel> developeSales = new List<SellerSalesModel>();
                List<SellerSalesModel> operationSales = new List<SellerSalesModel>();
                developeSales = new JsonSerializer().Deserialize<List<SellerSalesModel>>(View.DevelopeSales);
                operationSales = new JsonSerializer().Deserialize<List<SellerSalesModel>>(View.OperationSales);

                foreach (var sales in developeSales)
                {
                    ViewEmployee salesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales.SalesMail);

                    SellerSale s = new SellerSale();
                    s.SellerGuid = View.SellerGuid;
                    s.SellerSalesId = salesEmp.UserId;
                    s.SalesGroup = Convert.ToInt32(sales.SalesGroup);
                    s.SalesType = (int)SellerSalesType.Develope;
                    s.CreateId = View.UserName;
                    s.CreateTime = DateTime.Now;
                    sp.SellerSaleSet(s);

                }


                foreach (var sales in operationSales)
                {
                    ViewEmployee salesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales.SalesMail);

                    SellerSale s = new SellerSale();
                    s.SellerGuid = View.SellerGuid;
                    s.SellerSalesId = salesEmp.UserId;
                    s.SalesGroup = Convert.ToInt32(sales.SalesGroup);
                    s.SalesType = (int)SellerSalesType.Operation;
                    s.CreateId = View.UserName;
                    s.CreateTime = DateTime.Now;
                    sp.SellerSaleSet(s);

                }

                //紀錄Log
                SellerSaleCollection sellerSale = sp.SellerSaleGetBySellerGuid(View.SellerGuid);
                if (sellerSale.Count() != oriSellerSale.Count())
                {
                    if (View.DevelopeSales!= "[]")
                        SellerLog(View.SellerGuid, string.Format("新增 {0} 為 {1}", "開發業務", string.Join("、",developeSales.Select(x=>x.SalesMail).ToList())));
                    if (View.OperationSales != "[]")
                        SellerLog(View.SellerGuid, string.Format("新增 {0} 為 {1}", "經營業務", string.Join("、", operationSales.Select(x => x.SalesMail).ToList())));
                }
            }
        }

        /// <summary>
        /// 初始帶入業務資料
        /// </summary>
        /// <param name="userId"></param>
        private void InitialSaveSellerSales(Guid sellerGuid, int userId)
        {
            SellerSale developeSales = new SellerSale();
            developeSales.SellerGuid = sellerGuid;
            developeSales.SellerSalesId = userId;
            developeSales.SalesGroup = 1;
            developeSales.SalesType = (int)SellerSalesType.Develope;
            developeSales.CreateId = View.UserName;
            developeSales.CreateTime = DateTime.Now;
            sp.SellerSaleSet(developeSales);
        }


            #endregion
        }
}

﻿using System;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserDiscountCodeListPresenter : Presenter<IUserDiscountCodeListView>
    {
        private IOrderProvider op;
        public UserDiscountCodeListPresenter()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.UserName))
            {
                View.PageCount = op.DiscountCodeGetCount(DiscountCode.Columns.Owner + "=" + View.UserId);
                ViewDiscountDetailGetListByPage();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += new EventHandler<DataEventArgs<int>>(OnPageChange);
            return true;
        }

        #region method
        public void ViewDiscountDetailGetListByPage()
        {
            View.ViewDiscountDetailGetList(op.ViewDiscountDetailCollectionGetByOwner(
                View.CurrentPage, View.PageSize, string.Empty, View.UserId));
        }
        #endregion

        #region event
        void OnPageChange(object sender, DataEventArgs<int> e)
        {
            View.CurrentPage = e.Data;
            ViewDiscountDetailGetListByPage();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Component.Template;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventEdmPresenter : Presenter<IEventEdmView>
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.Default_SetUp();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.AreaChange += OnAreaChange;
            return true;
        }

        private void OnAreaChange(object sender, EventArgs e)
        {
            View.FillDeal(ep.GetDealFromCity(View.CityID));
        }

    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventPresenter : Presenter<IEventView>
    {
        private IEventProvider ep = ProviderFactory.Instance().GetProvider<IEventProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.DoClear();
            LoadEvent();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.EmailEnter += OnEmailEnter;
            View.Reload += OnReload;
            return true;
        }

        private void LoadEvent()
        {
            EventContent data = new EventContent();
            if (!string.IsNullOrEmpty(View.eid))
            {
                View.FillForm(ep.EventContentGet(new Guid(View.eid)));
            }
            else if (!string.IsNullOrEmpty(View.Url))
            {
                data = ep.EventContentGet(View.Url);
                View.FillForm(data);
            }
            else
            {
                View.ReturnDefault();
            }
            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoManage,data.Id);
            View.SeoDescription = seo.Description;
            View.SeoKeyword = seo.Keyword;
        }

        private void OnEmailEnter(object sender, DataEventArgs<EventEmailList> e)
        {
            View.EmailSuccess = (ep.EventEmailSet(e.Data)) ? true : false;
        }

        private void OnReload(object sender, EventArgs e)
        {
            LoadEvent();
        }
    }
}
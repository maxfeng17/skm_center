﻿using System;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class EmpSetPresenter : Presenter<IEmpSetView>
    {
        protected IHumanProvider hp = null;
        protected IMemberProvider mp = null;

        public EmpSetPresenter()
        {
            hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetParentDepartment(hp.DepartmentGetParentDeptListByEnabled(true));
            View.SetDepartmentSearch(GetDepartment(View.parentDeptIdSearch));
            View.SetEmpLevel();
            View.SetDepartment(GetDepartment(View.parentDeptId));
            View.SetDeptManager(GetDepartment(View.parentDeptId));
            View.SetCrossDeptTeam(GetDepartment(EmployeeDept.S000));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.SaveEmp += OnSaveEmp;
            View.ShowEmp += OnShowEmp;
            View.OnParentDeptSelectedIndexChanged += OnParentDeptSelectedIndexChanged;
            View.OnParentDeptSearchSelectedIndexChanged += OnParentDeptSearchSelectedIndexChanged;
            return true;
        }

        protected void ShowData()
        {
            switch (View.ViewWorkType)
            {
                case EmpSetViewWorkType.List:
                    ShowListData();
                    break;
                case EmpSetViewWorkType.Add:
                case EmpSetViewWorkType.Modify:
                    ShowEmpData();
                    break;
            }
            View.SetPageWorkType(View.ViewWorkType);
        }

        protected void ShowListData()
        {
            ViewEmployeeCollection viewEmpCol = hp.ViewEmployeeCollectionGetByFilter(View.filter[0].Key, View.filter[0].Value, ((bool)View.filter[1].Value ? false : (bool?)null));
            View.KPIVisible = IsKPIVisible(viewEmpCol.GroupBy(g => g.DeptId).Count() != 1 ? string.Empty : viewEmpCol.FirstOrDefault().DeptId);
            View.ShowList(viewEmpCol);
        }

        protected void ShowEmpData()
        {
            Employee emp = hp.EmployeeGet(Employee.Columns.EmpId, View.EmpId);
            Department dept = hp.DepartmentGet(emp.DeptId);
            EmployeeDept parentDeptId = EmployeeDept.None;
            EmployeeDept.TryParse(dept.ParentDeptId, out parentDeptId);
            View.parentDeptId = parentDeptId;
            EmployeeDept empDept = EmployeeDept.None;
            EmployeeDept.TryParse(dept.ParentDeptId, out empDept);
            View.SetDepartment(GetDepartment(empDept));
            View.SetDeptManager(GetDepartment(empDept));
            View.SetCrossDeptTeam(GetDepartment(EmployeeDept.S000));
            View.KPIVisible = IsKPIVisible(emp.DeptId);
            View.IsGrpPerformance = Convert.ToBoolean(emp.IsGrpPerformance);
            View.IsOfficial = Convert.ToBoolean(emp.IsOfficial);
            View.ShowEmpData(emp);
        }

        protected bool IsKPIVisible(string dptId)
        {
            if (hp.DepartmentGet(dptId).ParentDeptId == EmployeeDept.S000.ToString())
            {
                if (dptId == EmployeeChildDept.S010.ToString())
                {
                    return CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.KPISetS010);
                }
                else
                {
                    return CommonFacade.IsInSystemFunctionPrivilege(View.UserName, SystemFunctionType.KPISet);
                }
            }
            return false;
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            View.ViewWorkType = EmpSetViewWorkType.List;
            ShowData();
        }

        protected void OnSaveEmp(object sender, DataEventArgs<Employee> e)
        {
            Employee emp = hp.EmployeeGet(Employee.Columns.UserId, e.Data.UserId);
            if (e.Data.UserId == 0)
            {
                View.ShowMessage("會員資料中沒有此帳號，請確認員工Email是否輸入錯誤!!");
                return;
            }

            ViewEmployee data = hp.ViewEmployeeGet(Employee.Columns.UserId, e.Data.UserId);
            // 新增員工資料, 判斷員工資料是否已存在
            if (View.ViewWorkType == EmpSetViewWorkType.Add)
            {
                if (data.IsLoaded)
                {
                    View.ShowMessage(string.Format("員工資料 {0} 已存在!!", data.Email));
                    return;
                }
                data = hp.ViewEmployeeGet(Employee.Columns.EmpNo, e.Data.EmpNo);
                if (data.IsLoaded)
                {
                    View.ShowMessage(string.Format("員工編號 {0} 已存在!!", data.EmpNo));
                    return;
                }
            }
            else
            {
                data = hp.ViewEmployeeGet(Employee.Columns.EmpNo, e.Data.EmpNo);
                if (data.UserId != e.Data.UserId)
                {
                    View.ShowMessage(string.Format("員工編號 {0} 已存在!!", data.EmpNo));
                    return;
                }
            }

            if (View.ViewWorkType == EmpSetViewWorkType.Add && string.IsNullOrEmpty(e.Data.EmpId))
            {
                //新增模式取得新的EmpId
                emp.EmpId = HumanFacade.GetNewEmpId();
                emp.CreateId = View.UserName;
                emp.CreateTime = DateTime.Now;
                View.SetPageWorkType(EmpSetViewWorkType.List);
            }
            else
            {
                emp.ModifyId = View.UserName;
                emp.ModifyTime = DateTime.Now;
                View.SetPageWorkType(EmpSetViewWorkType.List);
            }
            emp.EmpNo = e.Data.EmpNo;
            emp.EmpName = e.Data.EmpName;
            emp.Mobile = e.Data.Mobile;
            emp.Extension = e.Data.Extension;
            emp.UserId = e.Data.UserId;
            emp.DeptId = e.Data.DeptId;
            emp.EmpLevel = (e.Data.EmpLevel == (int)EmployeeLevel.None) ? null : e.Data.EmpLevel;
            if (e.Data.IsInvisible) // 離職
            {
                //刪除權限
                Member mem = mp.MemberGet(e.Data.UserId);
                hp.PrivilegeDeleteByUser(mem.UniqueId);
                //清掉舊帳號的暫存權限
                FunctionPrivilegeManager.ClearUserCacheData(mem.UserName);
                //清掉角色 只保留 RegisterUser
                Roles.RemoveUserFromRoles(mem.UserName, Roles.GetRolesForUser(mem.UserName));
                Roles.AddUserToRole(mem.UserName, MemberRoles.RegisteredUser.ToString());
                CommonFacade.AddAudit(mem.UserName, AuditType.Role, "[系統] 員工離職 權限清除", View.UserName, true);
            }
            emp.IsInvisible = e.Data.IsInvisible;
            emp.AvailableDate = e.Data.AvailableDate;
            emp.DepartureDate = e.Data.DepartureDate;
            emp.TeamNo = e.Data.TeamNo;
            emp.TeamManager = e.Data.TeamManager;
            emp.DeptManager = e.Data.DeptManager;
            emp.IsGrpPerformance = e.Data.IsGrpPerformance;
            emp.IsOfficial = e.Data.IsOfficial;

            SalesVolumeMonth svm = HumanFacade.GetSalesVolumeMonth(e.Data.UserId, e.Data.YearMon);
            if (e.Data.KPI != null || e.Data.DealTarget != null)
            {
                if ((svm != null && (svm.KpiTarget != e.Data.KPI || svm.DealTarget != e.Data.DealTarget)) || svm == null)
                {
                    SalesVolumeMonth svm_new = new SalesVolumeMonth();
                    svm_new.YearMon = e.Data.YearMon;
                    svm_new.SalesId = e.Data.UserId.ToString();
                    svm_new.KpiTarget = e.Data.KPI != null ? e.Data.KPI : (svm != null ? svm.KpiTarget : null);
                    svm_new.DealTarget = e.Data.DealTarget != null ? e.Data.DealTarget : (svm != null ? svm.DealTarget : null);
                    svm_new.CreateId = View.UserName;
                    svm_new.CreateTime = DateTime.Now;
                    hp.SalesVolumeMonthSet(svm_new);
                }
            }


            if (View.CrossDeptTeam && !string.IsNullOrEmpty(e.Data.CrossDeptTeam) && HumanFacade.GetDepartmentByDeptId(e.Data.DeptId).ParentDeptId == EmployeeDept.S000.ToString())
            {
                emp.CrossDeptTeam = e.Data.CrossDeptTeam;
            }
            else
            {
                emp.CrossDeptTeam = "";
            }


            hp.EmployeeSet(emp);
            ShowData();
        }

        protected void OnShowEmp(object sender, EventArgs e)
        {
            ShowEmpData();
        }

        protected void OnParentDeptSelectedIndexChanged(object sender, EventArgs e)
        {
            View.SetDepartment(GetDepartment(View.parentDeptId));
        }

        protected void OnParentDeptSearchSelectedIndexChanged(object sender, EventArgs e)
        {
            View.SetDepartmentSearch(GetDepartment(View.parentDeptIdSearch));
        }

        private DepartmentCollection GetDepartment(EmployeeDept parentDeptId)
        {
            return hp.DepartmentGetListByEnabled(true, parentDeptId);
        }
    }
}

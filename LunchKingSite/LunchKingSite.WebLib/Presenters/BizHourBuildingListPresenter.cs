﻿using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class BizHourBuildingListPresenter : Presenter<IBizHourBuildingListView>
    {
        protected ILocationProvider lp;
        protected ISellerProvider sp;

        public enum BizHourBuildingListType
        {
            HasGUID,
            JustSeller
        }

        public const int DEFAULT_PAGE_SIZE = 15;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetSellerCityBizHourList(sp.ViewSellerCityBizHourGetListByBuilding(View.CurrentPage, View.PageSize, View.SortExpression, View.BuildingGuid, GetFilter()));
            SetDropDownList();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetSellerCount += OnGetSellerCount;
            View.SortClicked += OnGridUpdated;
            View.SearchClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            return true;
        }

        public BizHourBuildingListPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        protected string[] GetFilter()
        {
            string[] filter = new string[2];
            if (!string.IsNullOrEmpty(View.Filter))
            {
                filter[0] = View.Filter;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = View.FilterType + " like %" + filter[0] + "%";
            }
            return filter;
        }

        protected void OnGetSellerCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.ViewSellerCityBizHourGetCountByBuilding(View.BuildingGuid, GetFilter());
        }

        protected void OnGridUpdated(object sender, CommandEventArgs e)
        {
            
            if (View.QueryType == BizHourBuildingListType.JustSeller)
            {
                LoadData(1, false);
            }
            else
            {
                LoadData(1,true);
            }
                
            
        }

        protected void OnPageChanged(object sender, CommandEventArgs e)
        {
            LoadData((int)e.CommandArgument,true);
        }
        /// <summary>
        /// 載入商家資料(如果要回傳此路段的商家則HasGUID=true)
        /// </summary>
        /// <param name="PageNumber"></param>
        /// <param name="HasGUID"></param>
        protected void LoadData(int PageNumber,bool HasGUID)
        {
            if(HasGUID)
                View.SetSellerCityBizHourList(sp.ViewSellerCityBizHourGetListByBuilding(PageNumber, View.PageSize, View.SortExpression, View.BuildingGuid, GetFilter()));
            else
                View.SetSellerCityBizHourList(sp.ViewSellerCityBizHourGetList(PageNumber, View.PageSize, View.SortExpression, GetFilter()));
        }
        
        protected void SetDropDownList()
        {
            Dictionary<string, string> SearchType = new Dictionary<string, string>();
            SearchType.Add("此時段已有商家", "buid");
            SearchType.Add("尚未加入此時段商家", "default");
            View.SetSearchDropDown(SearchType);
        }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Refund;
using System.Net.Mail;
using LunchKingSite.BizLogic.Model.BookingSystem;

namespace LunchKingSite.WebLib.Presenters
{
    public class LionTravelOrderDetailPresenter : Presenter<ILionTravelOrderDetailView>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IBookingSystemProvider bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
        protected static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetOrderInfo(string.Empty);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OrderUserMemoSet += OnOrderUserMemoSet;
            View.RefundCoupon += OnRefundCoupon;
            View.ReturnProduct += OnReturnProduct;
            View.ReSendSms += OnReSendSms;
            View.DeleteUserMemo += OnDeleteUserMemo;
            return true;
        }

        void OnDeleteUserMemo(object sender, DataEventArgs<int> e)
        {
            op.OrderUserMemoListDelete(e.Data);
            GetOrderInfo(string.Empty);
        }

        void OnReSendSms(object sender, DataEventArgs<int> e)
        {
            string result = string.Empty;
            //int smsCount = pp.SMSLogGetCountByPpon(config.TmallDefaultUserName, e.Data.ToString(), DataProviderType.LoadingBalance);
            //if (smsCount < 3)
            //{
            CashTrustLogCollection cash_trust_logs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            CashTrustLogCollection return_cash_trust_logs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderGuid);
            OrderCorresponding order_corresponding = op.OrderCorrespondingListGetByOrderGuid(View.OrderGuid);
            if (cash_trust_logs.Any(x => x.CouponId == e.Data))
            {
                CashTrustLog cash_trust_log = cash_trust_logs.FirstOrDefault(x => x.CouponId == e.Data);
                switch ((TrustStatus)cash_trust_log.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        if (return_cash_trust_logs.Any(x => x.CouponId == e.Data))
                        {
                            result = "訂單已申請退貨";
                        }
                        else
                        {
                            ExpirationDateSelector selector = new ExpirationDateSelector();
                            selector.ExpirationDates = new PponUsageExpiration(View.OrderGuid);
                            if (selector.IsPastExpirationDate(DateTime.Now))
                            {
                                result = "憑證已過期";
                            }
                            else
                            {
                                try
                                {
                                    ViewPponCoupon view_ppon_coupon = pp.ViewPponCouponGet(e.Data);
                                    SendSmsProcess(view_ppon_coupon.BusinessHourGuid, view_ppon_coupon.BusinessHourDeliverTimeS.Value, view_ppon_coupon.BusinessHourDeliverTimeE.Value, view_ppon_coupon.CouponId.Value, view_ppon_coupon.SequenceNumber,
                                         view_ppon_coupon.CouponCode, view_ppon_coupon.CouponStoreSequence.HasValue ? "，序號" + view_ppon_coupon.CouponStoreSequence.Value.ToString("0000") : string.Empty,
                                         order_corresponding.Mobile, view_ppon_coupon.OrderDetailGuid, string.Empty, config.TmallDefaultUserName, true);

                                }
                                catch
                                {
                                    result = "簡訊發送失敗";
                                }
                            }
                        }
                        break;
                    case TrustStatus.Verified:
                        DateTime modifyDT = cash_trust_log.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.VerificationForced) || Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            result = "已強制核銷";
                        }
                        else
                        {
                            result = "已核銷";
                        }
                        break;
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            result = "已強制退貨";
                        }
                        else
                        {
                            result = "已退貨";
                        }
                        break;
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(cash_trust_log.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            result = "已強制退貨";
                        }
                        else
                        {
                            result = "已退貨";
                        }
                        break;
                    case TrustStatus.ATM:
                    default:
                        result = "憑證狀態錯誤";
                        break;
                }
            }
            //}
            //else
            //{
            //    result = "簡訊發送超過三次";
            //}
            GetOrderInfo(string.IsNullOrEmpty(result) ? "新增簡訊成功，排程將自動發送<br/>發送成功後次數會自動加1，請勿重複執行發送動作" : result);
        }

        private void SendSmsProcess(Guid business_hour_guid, DateTime business_hour_delivertime_s, DateTime business_hour_deliver_e,
        int coupon_id, string sequence_number, string code, string store_sequence, string mobile, Guid order_detail_guid, string sms_memo, string created_id, bool remove_17life_title = false)
        {
            #region send SMS 不走workflow
            SMS sms = new SMS();
            SmsContent sc = OrderFacade.SMSContentGet(business_hour_guid);
            string[] msgs = sc.Content.Split('|');
            string branch = OrderFacade.SmsGetPhoneFromWhere(order_detail_guid, business_hour_guid);     //分店資訊 & 多重選項
            string msg = string.Empty;
            if (remove_17life_title && msgs[0].Contains("17Life", StringComparison.InvariantCultureIgnoreCase) && msgs[2].Contains("下載APP看憑證", StringComparison.InvariantCultureIgnoreCase))
            {
                msg = string.Format(msgs[1] + branch + msgs[2].Replace(@"，下載APP看憑證：http://x.co/4r1JG", string.Empty) + (string.IsNullOrEmpty(sms_memo) ? string.Empty : ("，" + sms_memo)),
                    sequence_number,
                    code,
                    business_hour_delivertime_s.ToString("yyyyMMdd"),
                    business_hour_deliver_e.ToString("yyyyMMdd"),
                    store_sequence);
            }
            else
            {
                msg = string.Format(msgs[0] + msgs[1] + branch + msgs[2] + (string.IsNullOrEmpty(sms_memo) ? string.Empty : ("，" + sms_memo)),
                        sequence_number,
                        code,
                        business_hour_delivertime_s.ToString("yyyyMMdd"),
                        business_hour_deliver_e.ToString("yyyyMMdd"),
                        store_sequence);
            }
            sms.QueueMessage(msg, SmsType.Ppon, created_id, mobile, business_hour_guid, coupon_id.ToString(), SmsSystem.iTe2);

            #endregion
        }

        void OnRefundCoupon(object sender, DataEventArgs<KeyValuePair<string, List<int>>> e)
        {
            bool is_successful_refund = false;
            string refund_result_message = string.Empty;

            List<int> coupons = e.Data.Value;
            string reason = e.Data.Key;
            Order o = op.OrderGet(View.OrderGuid);
            RefundOrderInfo orderinfo = UserRefundFacade.GetOrderInfo(MemberFacade.GetUniqueId(config.TmallDefaultUserName), o.OrderId);
            int slug;
            DateTime now = DateTime.Now;
            ViewCouponListMain main = orderinfo.Coupons.First();
            //if (orderinfo.HasProcessingReturnForm)
            //{
            //    refund_result_message = "已申請退貨，無法再申請";
            //}
            //else
            if (!orderinfo.IsPpon && orderinfo.IsExchangeProcessing)
            {
                refund_result_message = "已申請換貨處理中，無法再申請";
            }
            else if (!orderinfo.IsReturnable)
            {
                refund_result_message = "訂單已全數退貨，無法再申請";
            }
            else if (main.BusinessHourOrderTimeE <= now && int.TryParse(main.Slug, out slug) &&
                     slug < main.BusinessHourOrderMinimum)
            {
                refund_result_message = "未達門檻不需退貨";
            }
            else if (orderinfo.IsInvoice2To3)
            {
                refund_result_message = "發票二聯改三聯處理中，無法申請退貨";
            }
            else
            {
                if (orderinfo.IsPpon)
                {
                    if (main.TotalCount != 0 && main.RemainCount == 0)
                    {
                        refund_result_message = "憑證已使用完畢，無法退貨";
                    }
                }
                else
                {
                    if (PponOrderManager.CheckIsOverTrialPeriod(main.Guid))
                    {
                        refund_result_message = "商品已過鑑賞期，無法退貨";
                    }
                }
            }
            if ((main.OrderStatus & (int)OrderStatus.Complete) == 0)
            {
                if ((main.OrderStatus & (int)OrderStatus.ATMOrder) > 0)
                {
                    refund_result_message = main.CreateTime.Date.Equals(DateTime.Now.Date)
                                                    ? "尚未完成ATM付款，無需退貨。"
                                                    : "ATM 逾期未付款，無法退貨。";
                }
                else
                {
                    refund_result_message = "未完成付款的訂單";
                }
            }
            if ((main.Status & (int)GroupOrderStatus.NoRefund) > 0)
            {
                refund_result_message = "此訂單不接受退貨申請";
            }
            if ((main.Status & (int)GroupOrderStatus.DaysNoRefund) > 0 && main.BusinessHourOrderTimeE.AddDays(config.ProductRefundDays) < DateTime.Now)
            {
                refund_result_message = "此訂單不接受退貨申請";
            }
            if ((main.Status & (int)GroupOrderStatus.ExpireNoRefund) > 0)
            {
                if (main.BusinessHourDeliverTimeE != null && main.BusinessHourDeliverTimeE.Value.AddDays(1) < now)
                {
                    refund_result_message = "此訂單不接受退貨申請";
                }
            }
            if ((main.Status & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 && now.Date >= ((DateTime)main.BusinessHourDeliverTimeS).AddDays(-config.PponNoRefundBeforeDays).Date)
            {
                refund_result_message = "此訂單不接受退貨申請";
            }
            if (main.ItemPrice.Equals(0) || (main.Status & (int)GroupOrderStatus.KindDeal) > 0)
            {
                refund_result_message = "此訂單不接受退貨申請";
            }
            if (main.BusinessHourOrderTimeS < new DateTime(2011, 4, 1))
            {
                refund_result_message = "此訂單不接受退貨申請";
            }
            string booking_lock_datetime = string.Empty;
            List<int> locked_coupons = new List<int>();
            if (orderinfo.IsReservationLock)
            {
                foreach (var coupon_id in coupons)
                {
                    BookingSystemReserveLockStatusLogCollection booking_logs = bp.BookingSystemReserveLockStatusLogGetByCouponId(coupon_id, (int)ReserveCouponStatus.Lock);
                    if (booking_logs.Count() > 0)
                    {
                        booking_lock_datetime += string.Format("，CouponId:{0}，預約時間:{1}", coupon_id, booking_logs.OrderByDescending(x => x.ModifyTime).First().ModifyTime.ToString("yyyy/MM/dd HH:mm"));
                        locked_coupons.Add(coupon_id);
                    }
                }
            }
            if (locked_coupons.Count == coupons.Count)
            {
                refund_result_message = "訂單已預約，無法申請退貨" + booking_lock_datetime;
            }
            else
            {
                coupons = coupons.Except(locked_coupons).ToList();
            }
            if (string.IsNullOrEmpty(refund_result_message))
            {
                bool isRefundCash = false;//(e.Data.Value == RefundType.Atm || e.Data.Value == RefundType.Cash) ? true : false; //是否刷退 或 退款至帳戶
                
                //bool requireCreditNote = false;
                CreateReturnFormResult result;
                int? dummy;
                if (orderinfo.IsPpon)
                {
                    IEnumerable<int> refundableCouponIds = ReturnService.GetRefundableCouponCtlogs(View.OrderGuid, BusinessModel.Ppon).Select(x => (int)x.CouponId).ToList();
                    if (coupons != null && coupons.Count > 0)
                    {
                        IEnumerable<int> intersect_coupons = coupons.Intersect(refundableCouponIds);
                        if (intersect_coupons.Any())
                        {
                            result = ReturnService.CreateCouponReturnForm(View.OrderGuid, intersect_coupons, isRefundCash, config.TmallDefaultUserName, reason, out dummy, false);
                        }
                        else
                        {
                            result = CreateReturnFormResult.ProductsUnreturnable;
                        }
                    }
                    //else if (refundableCouponIds.Any())
                    //{
                    //    result = ReturnService.CreateCouponReturnForm(View.OrderGuid, refundableCouponIds, isRefundCash, config.TmallDefaultUserName, string.Empty, out dummy, true);

                    //    //檢查是否需折讓單:已開立或準備開立發票、有使用現金購買及申請退現金
                    //    //requireCreditNote = einvoice.Any(x => x.CouponId.HasValue &&
                    //    //                                      refundableCouponIds.Contains(x.CouponId.Value) &&
                    //    //                                      x.VerifiedTime.HasValue)
                    //    //                    && orderinfo.IsPaidByCash
                    //    //                    && isRefundCash;
                    //}
                    else
                    {
                        result = CreateReturnFormResult.ProductsUnreturnable;
                    }
                }
                else
                {
                    List<int> orderProductIds = op.OrderProductGetListByOrderGuid(View.OrderGuid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                    if (orderProductIds.Any())
                    {
                        result = ReturnService.CreateRefundForm(View.OrderGuid, orderProductIds, isRefundCash, config.TmallDefaultUserName, reason,
                            null, null, null, null, out dummy, false);
                    }
                    else
                    {
                        result = CreateReturnFormResult.ProductsUnreturnable;
                    }
                }

                if (result == CreateReturnFormResult.Created)
                {
                    int returnFormId = op.ReturnFormGetListByOrderGuid(View.OrderGuid)
                          .OrderByDescending(x => x.Id)
                          .First().Id;

                    string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, reason);
                    CommonFacade.AddAudit(View.OrderGuid, AuditType.Refund, auditMessage, config.TmallDefaultUserName, false);

                    OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                    //憑證不用寄(宅配)退貨通知給廠商

                    is_successful_refund = true;
                }
                else
                {

                    refund_result_message = "退貨過程錯誤";
                }
            }
            GetOrderInfo(is_successful_refund ? ("退貨單建立成功" + (string.IsNullOrEmpty(booking_lock_datetime) ? string.Empty : ("(部分憑證預約鎖定中" + booking_lock_datetime + ")"))) : refund_result_message);
        }

        void OnReturnProduct(object sender, DataEventArgs<KeyValuePair<string, List<int>>> e)
        {
            string refund_result_message = string.Empty;
            List<int> orderProductIds = e.Data.Value;
            string reason = e.Data.Key;
            int? dummy;
            CreateReturnFormResult result = ReturnService.CreateRefundForm(View.OrderGuid, orderProductIds, true, config.TmallDefaultUserName, reason,
                null, null, null, null, out dummy, true);

            if (result != CreateReturnFormResult.Created)
            {
                switch (result)
                {
                    case CreateReturnFormResult.ProductsUnreturnable:
                        refund_result_message = "欲退項目的狀態無法退貨";
                        break;
                    case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                        refund_result_message = "ATM訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                        refund_result_message = "舊訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.InvalidArguments:
                        int comboCount = ReturnService.QueryComboPackCount(View.OrderGuid);
                        if (!Equals(0, orderProductIds.Count() % comboCount))
                        {
                            refund_result_message = string.Format("此訂單為成套售出，退貨需成套 [({0})的倍數] 申請。", comboCount.ToString());
                        }
                        else
                        {
                            refund_result_message = "無法建立退貨單, 請洽技術部";
                        }
                        break;
                    case CreateReturnFormResult.OrderNotCreate:
                        refund_result_message = "訂單未完成付款, 無法建立退貨單";
                        break;
                    case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                        refund_result_message = "分期且部分退, 請先設定ATM退款帳號";
                        break;
                    default:
                        refund_result_message = "不明錯誤, 請洽技術部";
                        break;
                }
            }
            else
            {
                int returnFormId = op.ReturnFormGetListByOrderGuid(View.OrderGuid)
                        .OrderByDescending(x => x.Id)
                        .First().Id;
                string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.ToString(), reason);
                CommonFacade.AddAudit(View.OrderGuid, AuditType.Refund, auditMessage, config.TmallDefaultUserName, false);
                OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
                EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
                
                //View.ReturnFormCreated(DeliveryType.ToHouse);
            }
            //GetOrderInfo(is_successful_refund ? ("退貨單建立成功" + (string.IsNullOrEmpty(booking_lock_datetime) ? string.Empty : ("(部分憑證預約鎖定中" + booking_lock_datetime + ")"))) : refund_result_message);
            GetOrderInfo(refund_result_message != string.Empty ? refund_result_message : "退貨單建立成功");
        }

        void OnOrderUserMemoSet(object sender, DataEventArgs<OrderUserMemoList> e)
        {
            op.OrderUserMemoListSet(e.Data);
            Order o = op.OrderGet(View.OrderGuid);
            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.ServiceEmail, Helper.GetEnumDescription(View.OrderClassType) + "客服平台");
            msg.To.Add(new MailAddress(config.LionTravelServiceEmail));
            msg.Subject = Helper.GetEnumDescription(View.OrderClassType) + "客服平台-新增訂單備註";
            msg.Body = string.Format("{0}客服平台新增了一筆<a href='{1}{2}'>訂單備註</a><br/>訂單編號:{3}<br/>訂單備註:{4}", config.SiteUrl + "/controlroom/Order/order_detail.aspx?oid=", Helper.GetEnumDescription(View.OrderClassType), e.Data.OrderGuid, o.OrderId, e.Data.UserMemo);
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            View.SetOrderUserMemoInfo(op.OrderUserMemoListGetList(View.OrderGuid));
        }

        private void GetOrderInfo(string message)
        {
            OrderCorresponding order_corresponding = op.OrderCorrespondingListGetByOrderGuid(View.OrderGuid);
            if (order_corresponding.IsLoaded && order_corresponding.Type == (int)View.OrderClassType)
            {
                CashTrustLogCollection cash_trust_logs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
                CashTrustLogCollection return_cash_trust_logs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderGuid);
                IList<ReturnFormEntity> return_forms = ReturnFormRepository.FindAllByOrder(View.OrderGuid);
                View.OrderDeliveryType = cash_trust_logs.FirstOrDefault().DeliveryType;
                ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(order_corresponding.BusinessHourGuid);
                OrderUserMemoListCollection user_memos = op.OrderUserMemoListGetList(View.OrderGuid);
                View.SetOrderInfo(order_corresponding, deal, user_memos);
                if (cash_trust_logs.FirstOrDefault().DeliveryType == (int)DeliveryType.ToShop)
                {
                    ViewPponCouponCollection view_ppon_coupons = pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, View.OrderGuid);
                    Dictionary<int, int> sms_count_list = new Dictionary<int, int>();
                    foreach (var item in view_ppon_coupons)
                    {
                        if (!sms_count_list.ContainsKey(item.CouponId ?? 0))
                        {
                            sms_count_list.Add(item.CouponId ?? 0, pp.SMSLogGetCountByPpon(config.TmallDefaultUserName, (item.CouponId ?? 0).ToString()));
                        }
                    }
                    if (!string.IsNullOrEmpty(message))
                    {
                        View.ShowMessage(message);
                    }
                    View.SetCouponInfo(view_ppon_coupons, cash_trust_logs, return_cash_trust_logs, sms_count_list, return_forms);
                }
                else
                {
                    if (!string.IsNullOrEmpty(message))
                    {
                        View.ShowMessage(message);
                    }
                    PponOrder odr = new PponOrder(View.OrderGuid);
                    IEnumerable<SummarizedProductSpec> source = odr.GetToHouseProductSummary();
                    View.SetProductInfo(source, return_forms);
                    View.SetDeliverInfo(op.ViewOrderShipListGetListByOrderGuid(odr.OrderGuid));
                }
            }
            else
            {
                View.ShowNoData();
            }
        }
    }
}

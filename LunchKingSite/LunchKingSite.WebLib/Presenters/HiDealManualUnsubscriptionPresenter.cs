﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.WebLib.Component;
using System.Web.Security;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealManualUnsubscriptionPresenter : Presenter<IHiDealManualUnsubscriptionView>
    {
        IHiDealProvider hp;
        ILocationProvider lp;
        IMemberProvider mp;

        protected void HiDealManualUnsubscriptionProviders()
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            //檢查是否登入
            if (string.IsNullOrEmpty(View.UserName))
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                //判斷路徑是否有帶值
                if (View.theEmail != null)
                {
                    //判斷路徑帶入Email與登入帳號是否為同一帳號
                    if (View.UserName == View.theEmail)
                    {
                        //讀取會員帳號相關的資料
                        MemberUtility.SetUserSsoInfoReady(View.UserId);
                        View.ShowTheUnSubscribe();
                    }
                    else
                    {
                        View.ShowTheError();
                    }
                }
                else
                {
                    View.ShowTheError();
                }
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UnSubscribe += OnUnSubscribe;
            return true;
        }

        void OnUnSubscribe(object sender, DataEventArgs<string> e)
        {
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
           
            //確認UnsubscriptionDB是否有值，沒有的話就新增
            Unsubscription checkMail = hp.UnsubscriptionGet(e.Data);

            if (!checkMail.IsLoaded)
            {
                checkMail.UserName = View.theEmail;
                checkMail.PiinlifeEdm = true;
                checkMail.CreateTime = DateTime.Now;
                checkMail.CreateId = View.UserName;
                hp.UnsubscriptionSet(checkMail);
            }
            else
            {
                checkMail.UserName = View.theEmail;
                checkMail.PiinlifeEdm = true;
                checkMail.ModifyId = View.UserName;
                checkMail.ModifyTime = DateTime.Now;
                hp.UnsubscriptionSet(checkMail);
            }

            //edit subscriptionFlag
            HiDealSubscriptionCollection getSubscriptFlags = hp.HiDealSubscriptionGetList(0, 0, string.Empty, HiDealSubscription.EmailColumn + "=" + e.Data);
            if (getSubscriptFlags.Count > 0)
            {
                foreach (var subscript in getSubscriptFlags)
                {
                    subscript.Flag = Convert.ToInt32(PiinlifeSubscriptionFlag.Cancel);
                    subscript.CancelTime = DateTime.Now;
                }
                hp.HiDealSubscriptionSetList(getSubscriptFlags);
            }
            View.ShowTheSuccess();
        }
    }
}

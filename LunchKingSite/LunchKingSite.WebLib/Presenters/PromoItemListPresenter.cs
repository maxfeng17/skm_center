﻿using System;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Net.Mail;
using LunchKingSite.BizLogic.Facade;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class PromoItemListPresenter : Presenter<IPromoItemListView>
    {
        private IEventProvider ep;
        private IMemberProvider mp;
        private ISysConfProvider config;
        public PromoItemListPresenter()
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.UserName))
            {
                EventActivity activity = ep.EventActivityGet(View.EventId);
                if (activity.IsLoaded)
                {
                    if (activity.Id != 0 && activity.Status)
                    {
                        View.Activity = activity;
                        if (DateTime.Now < View.Activity.StartDate)
                            View.ShowMessage(I18N.Message.EventCouponExchangeNotExist);
                        else
                        {
                            View.PageCount = ep.PromoItemGetListCount(GetFilter());
                            LoadPromoItemListPaging();
                        }
                    }
                    else
                    {
                        View.ShowMessage(I18N.Message.EventCouponExchangeNotExist);
                    }
                }
                else
                {
                    View.ShowMessage(I18N.Message.EventCouponExchangeNotExist);
                }
                
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += new EventHandler<DataEventArgs<int>>(OnPageChange);
            View.Print += new EventHandler<DataEventArgs<int>>(OnPrint);
            View.Send += new EventHandler<DataEventArgs<int>>(OnSent);
            return true;
        }

        #region method
        public void LoadPromoItemListPaging()
        {
            View.PromoItemGetListPaging(ep.PromoItemGetListPaging(View.CurrentPage, View.PageSize, GetFilter()));
        }
        #endregion

        #region event
        void OnPageChange(object sender, DataEventArgs<int> e)
        {
            View.CurrentPage = e.Data;
            LoadPromoItemListPaging();
        }
        void OnPrint(object sender, DataEventArgs<int> e)
        {
            Member m = mp.MemberGet(View.UserId);
            EventActivity activity = ep.EventActivityGet(View.Activity.Id);
            if (activity.IsLoaded)
            {
                PromoItem item = ep.PromoItemGet(e.Data);
                if (item.IsLoaded)
                {
                    View.Download(activity.Eventname, PromotionFacade.GetEventCoupontSendMailTemplate(m.DisplayName, item, activity));
                }
            }
        }
        void OnSent(object sender, DataEventArgs<int> e)
        {
            Member m = mp.MemberGet(View.UserId);
            EventActivity activity = ep.EventActivityGet(View.Activity.Id);
            if (activity.IsLoaded)
            {
                PromoItem item = ep.PromoItemGet(e.Data);
                if (item.IsLoaded)
                {
                    // 發送兌換序號通知信
                    MailMessage msg = new MailMessage();
                    msg.From = new MailAddress(config.PponServiceEmail, config.ServiceName);
                    msg.To.Add(m.UserEmail);
                    msg.Subject = string.Format("【兌換通知】{0}-憑活動序號獨享優惠!!", View.Activity.Eventname);
                    msg.Body = PromotionFacade.GetEventCoupontSendMailTemplate(m.DisplayName, item, activity);
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    View.ShowMessage("序號已重新寄送。");
                }
            }
        }
        #endregion

        #region private method
        private string[] GetFilter()
        {
            List<string> list = new List<string>();
            list.Add(PromoItem.EventActivityIdColumn.ColumnName + "=" + View.EventId);
            list.Add(PromoItem.UserIdColumn.ColumnName + "=" + View.UserId);
            return list.ToArray();
        }
        #endregion
    }
}

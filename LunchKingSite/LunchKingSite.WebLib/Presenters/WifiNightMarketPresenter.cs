using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Presenters
{
    public class WifiNightMarketPresenter : Presenter<IWifiNightMarketView>
    {
        private IPponProvider pp;
        public WifiNightMarketPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            //View.SetFreewifiUseLog += OnSetFreewifiUseLog;
            return true;
        }

        protected void OnSetFreewifiUseLog(object sender,DataEventArgs<KeyValuePair<string, string>> e)
        {
            if (e.Data.Key == "" && e.Data.Value != "")
            {
                FreewifiUseLog wifiUseLog = new FreewifiUseLog();
                wifiUseLog.MacAddress = e.Data.Value;
                wifiUseLog.WifiType = 1;
                wifiUseLog.UseTime = 30;
                wifiUseLog.CreateTime = DateTime.Now;
                pp.FreewifiUseLogSet(wifiUseLog);
            }
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class ShoppingCartManagePresenter : Presenter<IShoppingCartManageView>
    {
        protected static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        protected static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected static IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            return true;
        }

        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            List<Guid> sellers = new List<Guid>();
            sellers.AddRange(sp.SellerGetListBySalesName(SaleNameGet(View.UserName), true).Select(x=>x.Guid));

            List<ShoppingCartFreight> freights = sp.ShoppingCartFreightsGetByStatus(ShoppingCartFreightStatus.Init).ToList();
            freights = freights.Where(x => sellers.Contains(x.SellerGuid)).ToList();


            List<ShoppingCartManageList> items = new List<ShoppingCartManageList>();
            foreach (ShoppingCartFreight sf in freights)
            {
                ShoppingCartManageList item = new ShoppingCartManageList
                {
                    Id = sf.Id,
                    UniqueId = sf.UniqueId,
                    SellerName = SellerFacade.SellerNameGet(sf.SellerGuid),
                    FreightsStatusName = SellerFacade.GetFreightStatus(sf.FreightsStatus),
                    FreightsName = sf.FreightsName,
                    NoFreightLimit = sf.NoFreightLimit,
                    Freights = sf.Freights,
                    CreateTime = sf.CreateTime
                };
                items.Add(item);
            }

            if (!string.IsNullOrEmpty(View.SearchName))
            {
                items = items.Where(x => x.FreightsName.IndexOf(View.SearchName) >= 0 || x.SellerName.IndexOf(View.SearchName) >= 0).ToList();
            }

            View.SetShoppintCartData(items);
        }

        private string SaleNameGet(string UserName)
        {
            string EmpName = "";
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
            if(emp != null && emp.IsLoaded)
            {
                EmpName = emp.EmpName;
            }
            return EmpName;
        }
        
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System.IO;
using System.Drawing;
using NewtonsoftJson = Newtonsoft.Json;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class VourcherEventPresenter : Presenter<IVourcherEventView>
    {
        ISellerProvider _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        IHumanProvider _humProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();

        #region local property
        /// <summary>
        /// 更新優惠券類別會變動到的欄位
        /// </summary>
        private List<string> VourcherEventTypeColumns = new List<string>()
        {
            VourcherEvent.DiscountColumn.ColumnName,
            VourcherEvent.OriginalPriceColumn.ColumnName,
            VourcherEvent.DiscountPriceColumn.ColumnName,
            VourcherEvent.Message1Column.ColumnName,
            VourcherEvent.Message2Column.ColumnName,
            VourcherEvent.Message3Column.ColumnName,
            VourcherEvent.AllTableColumn.ColumnName,
            VourcherEvent.OneMealColumn.ColumnName,
            VourcherEvent.MultipleMealsColumn.ColumnName,
            VourcherEvent.TheFourthAppliedColumn.ColumnName,
            VourcherEvent.GiftAttendingColumn.ColumnName,
            VourcherEvent.GiftConsumptionColumn.ColumnName,
            VourcherEvent.GiftLimitedColumn.ColumnName,
            VourcherEvent.GiftQuantityColumn.ColumnName,
            VourcherEvent.GiftReplacableColumn.ColumnName,
            VourcherEvent.ServiceFeeColumn.ColumnName,
            VourcherEvent.ServiceFeeAdditionalColumn.ColumnName,
            VourcherEvent.ConsumptionAppliedColumn.ColumnName,
            VourcherEvent.MinConsumptionColumn.ColumnName,
            VourcherEvent.MinPersonsColumn.ColumnName,
            VourcherEvent.HolidayAppliedColumn.ColumnName,
            VourcherEvent.WeekendAppliedColumn.ColumnName
            ,VourcherEvent.OtherTimesColumn.ColumnName
        };
        #endregion
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetVourcherEventStatus(1);
            SetupSelectableCommercialCategories();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetDataCount += OnGetDataCount;
            View.GetReturnCaseDataCount += OnGetReturnCaseDataCount;
            View.PageChanged += OnPageChanged;
            View.PageReturnCaseChanged += OnPageReturnCaseChanged;
            View.GetSellerById += OnGetSellerById;
            View.SaveVourcherEventStores += OnSaveVourcherEventStores;
            View.GetVourcherEvent += OnGetVourcherEvent;
            View.SearchVourcherEvent += OnSearchVourcherEvent;
            View.SearchVourcherByStatus += OnSearchVourcherByStatus;
            View.ReturnApply += OnReturnApply;
            View.SellerPass += OnSellerPass;
            View.StorePass += OnStorePass;
            View.ApproveApply += OnApproveApply;
            View.EnableEvent += OnEnableEvent;
            return true;
        }

        public void OnEnableEvent(object sender, DataEventArgs<bool> e)
        {
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
            vourcher_event.Enable = !e.Data;
            VourcherFacade.VourcherEventSet(vourcher_event);
            GetVourcherEventStatus(1);
        }

        /// <summary>
        /// 賣家審核通過
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnSellerPass(object sender, EventArgs e)
        {
            Seller seller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
            seller.TempStatus = (int)SellerTempStatus.Completed;
            _sellerProv.SellerSet(seller);

            StoreCollection stores = new StoreCollection();
            if (!seller.IsCloseDown)
            {
                stores = VourcherFacade.StoreCollectionGetBySellerGuid(View.SellerGuid);
            }
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
            VourcherStoreCollection vourcher_stores = VourcherFacade.VourcherStoreCollectionGetByEventId(View.VourcherEventId);
            View.SetVourcherEventStores(seller, stores, vourcher_event, vourcher_stores);
        }

        /// <summary>
        /// 分店審核通過
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnStorePass(object sender, EventArgs e)
        {
            StoreCollection stores = VourcherFacade.StoreCollectionGetBySellerGuid(View.SellerGuid);
            foreach (Store store in stores)
            {
                store.TempStatus = (int)SellerTempStatus.Completed;
                _sellerProv.StoreSet(store);
            }

            Seller seller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
            VourcherStoreCollection vourcher_stores = VourcherFacade.VourcherStoreCollectionGetByEventId(View.VourcherEventId);
            View.SetVourcherEventStores(seller, stores, vourcher_event, vourcher_stores);
        }

        /// <summary>
        /// 通過審核
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnApproveApply(object sender, EventArgs e)
        {
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
            //未設日期 起始日以今日+1 結束日+3個月
            if (!vourcher_event.StartDate.HasValue)
                vourcher_event.StartDate = DateTime.Today.AddDays(1);
            if (!vourcher_event.EndDate.HasValue)
                vourcher_event.EndDate = vourcher_event.StartDate.Value.AddMonths(6);
            vourcher_event.Status = (int)VourcherEventStatus.EventChecked;
            vourcher_event.ApproveTime = DateTime.Now;
            vourcher_event.Enable = true;
            VourcherFacade.VourcherEventSet(vourcher_event);
            OnGetVourcherEvent(sender, new DataEventArgs<int>(View.VourcherEventId));
            GetVourcherEventStatus(1);
        }
        /// <summary>
        /// 退回申請
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnReturnApply(object sender, DataEventArgs<string> e)
        {
            VourcherFacade.VourcherEventUpdateStatus(View.VourcherEventId, e.Data);
            OnGetVourcherEvent(sender, new DataEventArgs<int>(View.VourcherEventId));
            GetVourcherEventStatus(1);
        }
        /// <summary>
        /// 依狀態搜尋所有優惠券
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnSearchVourcherByStatus(object sender, EventArgs e)
        {
            OnGetReturnCaseDataCount(sender, e);
            GetVourcherEventStatus(1);
        }
        /// <summary>
        /// 搜尋優惠券(分頁)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnSearchVourcherEvent(object sender, EventArgs e)
        {
            LoadVourcherEvent(1);
        }
        /// <summary>
        /// 抓取選取的優惠券資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnGetVourcherEvent(object sender, DataEventArgs<int> e)
        {
            View.VourcherEventId = e.Data;
            VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(e.Data);
            View.SellerGuid = vourcher_event.SellerGuid;
            VourcherStoreCollection vourcher_stores = VourcherFacade.VourcherStoreCollectionGetByEventId(e.Data);
            Seller seller = VourcherFacade.SellerGetByGuid(vourcher_event.SellerGuid);
            StoreCollection stores = new StoreCollection();
            if (!seller.IsCloseDown)
            {
                stores = VourcherFacade.StoreCollectionGetBySellerGuid(vourcher_event.SellerGuid);
            }
            View.SetVourcherEventStores(seller, stores, vourcher_event, vourcher_stores);
            // 生活商圈
            CategoryVourcherCollection cvc = VourcherFacade.CategoryVourchersGetList(e.Data);
            Dictionary<int, int[]> dataList = new Dictionary<int, int[]>();
            CategoryCollection commercialCategory = _sellerProv.CategoryGetList((int)CategoryType.CommercialCircle);
            foreach (Category cate in commercialCategory.Where(x => x.ParentCode == null))
            {
                int[] cids = commercialCategory.Where(x => x.ParentCode != null && x.ParentCode.Equals(cate.Code)).Select(y => y.Id).ToArray();
                dataList.Add(cate.Id, cvc.Where(x => x.Cid.EqualsAny(cids)).Select(x => x.Cid).ToArray());
            }
            List<int> category_list = new List<int>();

            _sellerProv.ViewCategoryDependencyGetByParentType((int)CategoryType.Voucher).ForEach(x =>
            {
                if (cvc.Any(y => y.Cid == x.CategoryId))
                {
                    category_list.Add(x.CategoryId);
                }
            });
            View.SelectedCategory = category_list;
            View.SelectedCommercialCategoryId = dataList;
            SetSalesmanNameArray();
        }
        /// <summary>
        /// 儲存優惠券資料
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnSaveVourcherEventStores(object sender, DataEventArgs<VourcherEventInfo> e)
        {
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/");
            VourcherEvent vourcher_event = e.Data.Vourcher_Event;
            List<PhotoInfo> photos = e.Data.Photo_Infos;
            List<int> delete_imgs = e.Data.DeleteImgs;
            List<string> photo_url = new List<string>();
            Seller seller = VourcherFacade.SellerGetByGuid(vourcher_event.SellerGuid);
            DateTime now = DateTime.Now;
            if (View.VourcherEventId != 0)
            {
                VourcherEvent original_vourcher_event = VourcherFacade.VourcherEventGetById(View.VourcherEventId);
                original_vourcher_event.Contents = vourcher_event.Contents;
                original_vourcher_event.Instruction = vourcher_event.Instruction;
                original_vourcher_event.Restriction = vourcher_event.Restriction;
                //original_vourcher_event.PicUrl = vourcher_event.PicUrl;
                original_vourcher_event.Type = vourcher_event.Type;
                original_vourcher_event.ContractStatus = vourcher_event.ContractStatus;
                original_vourcher_event.NoContractReason = vourcher_event.NoContractReason;
                original_vourcher_event.SalesName = vourcher_event.SalesName;
                if (View.IsAdmin)
                {
                    original_vourcher_event.Ratio = vourcher_event.Ratio;
                    original_vourcher_event.StartDate = vourcher_event.StartDate;
                    original_vourcher_event.EndDate = vourcher_event.EndDate;
                    if (original_vourcher_event.Status != (int)VourcherEventStatus.EventChecked)
                        original_vourcher_event.Status = (int)VourcherEventStatus.ApplyEvent;
                }
                else
                    original_vourcher_event.Status = (int)VourcherEventStatus.ApplyEvent;
                original_vourcher_event.MaxQuantity = vourcher_event.MaxQuantity;
                original_vourcher_event.ModifyId = View.UserName;
                original_vourcher_event.ModifyTime = DateTime.Now;
                original_vourcher_event.Others = vourcher_event.Others;
                original_vourcher_event.Magazine = vourcher_event.Magazine;
                original_vourcher_event.Mode = vourcher_event.Mode;
                SeveCategoryVroucherList(View.VourcherEventId);
                foreach (var item in VourcherEventTypeColumns)
                {
                    original_vourcher_event.SetColumnValue(item, vourcher_event.GetColumnValue(item));
                }
                #region img
                List<string> original_photo_url = new List<string>();
                if (!string.IsNullOrEmpty(original_vourcher_event.PicUrl))
                    original_photo_url = original_vourcher_event.PicUrl.Split(new char[] { '|' }).ToList();

                int i = 0;
                foreach (var photo in photos)
                {
                    bool pic_has_deleted = false;
                    string original_pic = original_photo_url.Skip(i).DefaultIfEmpty(string.Empty).First();
                    if (delete_imgs.Any(x => x == i) && !string.IsNullOrEmpty(original_pic))
                    {
                        if (original_pic.IndexOf(',') >= 0)
                        {
                            string[] filePath = original_pic.Split(',');
                            ImageUtility.DeleteFile(photo.Type, filePath[0], filePath[1]);
                            pic_has_deleted = true;
                        }
                    }
                    if (photo != null && !string.IsNullOrEmpty(photo.DestFileName))
                    {
                        photo.DestFilePath = seller.SellerId;
                        photo.DestFileName = original_vourcher_event.Id.ToString().PadLeft(5, '0') + "-" + photo.DestFileName;
                        ImageUtility.UploadFileWithResize(photo.PFile.ToAdapter(), photo.Type, photo.DestFilePath, photo.DestFileName, 300, 300);
                        photo_url.Add(ImageFacade.GenerateMediaPath(seller.SellerId, photo.DestFileName + "." + Helper.GetExtensionByContentType(photo.PFile.ContentType)));
                        if (!string.IsNullOrEmpty(original_pic) && !pic_has_deleted)
                        {
                            if (original_pic.IndexOf(',') >= 0)
                            {
                                string[] filePath = original_pic.Split(',');
                                ImageUtility.DeleteFile(photo.Type, filePath[0], filePath[1]);
                            }
                        }
                    }
                    else
                    {
                        if (pic_has_deleted)
                            photo_url.Add(string.Empty);
                        else
                            photo_url.Add(original_pic);
                    }
                    i++;
                }
                original_vourcher_event.PicUrl = photo_url.Aggregate((current, next) => current + "|" + next);
                #endregion

                VourcherFacade.VourcherStoreDeleteByEventId(View.VourcherEventId);
                //優惠券所選取的分店，加總城市的bit
                int city_bit = 0;
                foreach (var item in e.Data.Vourcher_Store_Collection)
                {
                    //提醒營管，優惠券所選取的分店
                    item.VourcherEventId = original_vourcher_event.Id;
                    Store store = VourcherFacade.StoreGetByGuid(item.StoreGuid);
                    if (!View.IsAdmin)
                    {

                        store.TempStatus = (int)SellerTempStatus.Applied;
                        store.ApplyId = View.UserName;
                        store.ApplyTime = now;
                        store.ApproveTime = null;
                        VourcherFacade.StoreSave(store);
                    }
                    VourcherFacade.ChangeLogAdd(Store.Schema.TableName, store.Guid, ChangeItemsToJson(new KeyValuePair<string, string>("Modify Vourcher Id " + original_vourcher_event.Id, "異動優惠券分店")), true);
                    City city = VourcherFacade.CityGetById(store.CityId ?? 0);
                    if (city != null && (city_bit & city.BitNumber) == 0)
                        city_bit += city.BitNumber;
                }
                VourcherFacade.VourcherStoresSet(e.Data.Vourcher_Store_Collection);
                original_vourcher_event.CityBit = city_bit;
                VourcherFacade.VourcherEventSet(original_vourcher_event);
                View.VourcherEventId = original_vourcher_event.Id;
                //提醒營管，優惠券所選取的賣家
                if (!View.IsAdmin)
                {
                    seller.TempStatus = (int)SellerTempStatus.Applied;
                    seller.ApplyId = View.UserName;
                    seller.ApplyTime = now;
                    seller.ApplyTime = null;
                    VourcherFacade.SellerSave(seller);
                }
                VourcherFacade.ChangeLogAdd(Seller.Schema.TableName, seller.Guid, ChangeItemsToJson(new KeyValuePair<string, string>("Modify Vourcher Id " + original_vourcher_event.Id, "異動優惠券賣家")), true);

            }
            else
            {
                vourcher_event.CreateId = View.UserName;
                vourcher_event.CreateTime = DateTime.Now;
                //if (View.IsAdmin)
                //{
                //    vourcher_event.Status = (int)VourcherEventStatus.EventChecked;
                //}
                //else
                vourcher_event.Status = (int)VourcherEventStatus.ApplyEvent;
                VourcherFacade.VourcherEventSet(vourcher_event);
                if (photos.Any(x => !string.IsNullOrEmpty(x.DestFileName)))
                {
                    foreach (var photo in photos)
                    {
                        if (photo != null && !string.IsNullOrEmpty(photo.DestFileName))
                        {
                            photo.DestFilePath = seller.SellerId;
                            photo.DestFileName = vourcher_event.Id.ToString().PadLeft(5, '0') + "-" + photo.DestFileName;
                            ImageUtility.UploadFileWithResize(photo.PFile.ToAdapter(), photo.Type, photo.DestFilePath, photo.DestFileName, 300, 300);
                            photo_url.Add(ImageFacade.GenerateMediaPath(seller.SellerId, photo.DestFileName + "." + Helper.GetExtensionByContentType(photo.PFile.ContentType)));
                        }
                        else
                            photo_url.Add(string.Empty);
                    }
                    vourcher_event.PicUrl = photo_url.Aggregate((current, next) => current + "|" + next);

                }
                SeveCategoryVroucherList(vourcher_event.Id);
                //優惠券所選取的分店，加總城市的bit
                int city_bit = 0;
                foreach (var item in e.Data.Vourcher_Store_Collection)
                {
                    item.VourcherEventId = vourcher_event.Id;
                    item.CreateId = View.UserName;
                    item.CreateTime = now;
                    Store store = VourcherFacade.StoreGetByGuid(item.StoreGuid);
                    store.TempStatus = (int)SellerTempStatus.Applied;
                    store.ApplyId = View.UserName;
                    store.ApplyTime = now;
                    store.ApproveTime = null;
                    VourcherFacade.StoreSave(store);
                    VourcherFacade.ChangeLogAdd(Store.Schema.TableName, store.Guid, ChangeItemsToJson(new KeyValuePair<string, string>("Modify Vourcher Id " + vourcher_event.Id, "新增優惠券分店")), true);
                    City city = VourcherFacade.CityGetById(store.CityId ?? 0);
                    if (city != null && (city_bit & city.BitNumber) == 0)
                        city_bit += city.BitNumber;
                }
                VourcherFacade.VourcherStoresSet(e.Data.Vourcher_Store_Collection);
                vourcher_event.CityBit = city_bit;
                VourcherFacade.VourcherEventSet(vourcher_event);
                View.VourcherEventId = vourcher_event.Id;
                seller.TempStatus = (int)SellerTempStatus.Applied;
                seller.ApplyId = View.UserName;
                seller.ApplyTime = now;
                seller.ApplyTime = null;
                VourcherFacade.SellerSave(seller);
                VourcherFacade.ChangeLogAdd(Seller.Schema.TableName, seller.Guid, ChangeItemsToJson(new KeyValuePair<string, string>("Modify Vourcher Id " + vourcher_event.Id, "新增優惠券賣家")), true);
            }

            if (e.Data.AddNext)
            {
                View.VourcherEventId = 0;
                OnGetSellerById(sender, new DataEventArgs<string>(View.SellerId));
            }
            else
                OnGetVourcherEvent(sender, new DataEventArgs<int>(View.VourcherEventId));
            GetVourcherEventStatus(1);
        }

        /// <summary>
        /// 儲存生活商圈資料
        /// </summary>
        /// <param name="vourcherEventId"></param>
        private void SeveCategoryVroucherList(int vourcherEventId)
        {
            CategoryVourcherCollection cvs = new CategoryVourcherCollection();
            foreach (KeyValuePair<int, int[]> categoryList in View.SelectedCommercialCategoryId)
            {
                foreach (int categoryId in categoryList.Value)
                {
                    cvs.Add(new CategoryVourcher() { VourcherId = vourcherEventId, Cid = categoryId });
                }
            }
            foreach (var item in View.SelectedCategory)
            {
                cvs.Add(new CategoryVourcher() { VourcherId = vourcherEventId, Cid = item });
            }
            VourcherFacade.CategoryVourchersSet(cvs, vourcherEventId);
        }
        /// <summary>
        /// 依賣家id抓取資料，並回傳賣家和分店資料做優惠券新增用
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnGetSellerById(object sender, DataEventArgs<string> e)
        {
            GetSellerById(e.Data);
            SetSalesmanNameArray();
        }
        private void GetSellerById(string sellerid)
        {
            Seller seller = VourcherFacade.SellerGetById(sellerid);
            StoreCollection stores = new StoreCollection();
            if (!seller.IsCloseDown)
            {
                stores = VourcherFacade.StoreCollectionGetBySellerGuid(seller.Guid);
            }
            View.SellerGuid = seller.Guid;
            View.SellerId = seller.SellerId;
            View.SetSellerStores(seller, stores);
        }
        /// <summary>
        /// 搜尋優惠券轉分頁
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e">第幾分頁</param>
        void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadVourcherEvent(e.Data);
        }
        public void OnPageReturnCaseChanged(object sender, DataEventArgs<int> e)
        {
            GetVourcherEventStatus(e.Data);
        }
        /// <summary>
        /// 依優惠券搜尋條件計算總資料數
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OnGetDataCount(object sender, EventArgs e)
        {
            //判斷搜尋條件是否有填值
            View.PageCount = VourcherFacade.VourcherEventCollectionGetCount(View.SearchKeys);
        }

        void OnGetReturnCaseDataCount(object sender, EventArgs e)
        {
            if (View.IsAdmin)
            {
                View.PageReturnCaseCount = VourcherFacade.VourcherEventCollectionGetCount(string.Empty, (int)View.EventStatus);
            }
            else
            {
                int apply = VourcherFacade.VourcherEventCollectionGetCount(View.UserName, (int)VourcherEventStatus.ApplyEvent);
                int returncase = VourcherFacade.VourcherEventCollectionGetCount(View.UserName, (int)VourcherEventStatus.ReturnApply);
                View.PageReturnCaseCount = apply + returncase;
            }
        }
        private void LoadVourcherEvent(int page)
        {
            ViewVourcherSellerCollection vourcher_sellers = new ViewVourcherSellerCollection();
            //搜尋優惠券
            vourcher_sellers = VourcherFacade.VourcherEventCollectionGetBySearch(page, View.PageSize, View.SearchKeys, string.Empty);
            //回傳優惠券資料
            View.SetVourcherEventSellerCollection(vourcher_sellers);
        }
        private void GetVourcherEventStatus()
        {
            ViewVourcherSellerCollection vourcher_events = new ViewVourcherSellerCollection();
            if (View.IsAdmin)
                vourcher_events = VourcherFacade.VourcherEventSellerCollectionGetByStatus(string.Empty, (int)View.EventStatus);
            else
            {
                vourcher_events = VourcherFacade.VourcherEventSellerCollectionGetByStatus(View.UserName, (int)VourcherEventStatus.ApplyEvent);
                vourcher_events.AddRange(VourcherFacade.VourcherEventSellerCollectionGetByStatus(View.UserName, (int)VourcherEventStatus.ReturnApply));
            }
            View.SetReturnCaseVourcherEvent(vourcher_events);
        }
        private void SetupSelectableCommercialCategories()
        {
            Dictionary<Category, List<Category>> dataList = new Dictionary<Category, List<Category>>();
            CategoryCollection categories = _sellerProv.CategoryGetList((int)CategoryType.CommercialCircle);
            foreach (Category item in categories.Where(x => x.ParentCode == null))
            {
                dataList.Add(item, categories.Where(x => x.ParentCode.Equals(item.Code)).ToList());
            }
            Dictionary<int, string> category_list = VourcherFacade.GetVoucherCategoryList();
            View.SetSelectableCommercialCategory(dataList, category_list);
        }
        private void GetVourcherEventStatus(int pageindex)
        {
            ViewVourcherSellerCollection vourcher_events = new ViewVourcherSellerCollection();
            if (View.IsAdmin)
                vourcher_events = VourcherFacade.VourcherEventSellerCollectionGetByStatus(pageindex, View.PageReturnCaseSize, string.Empty, string.Empty, (int)View.EventStatus);
            else
            {
                vourcher_events = VourcherFacade.VourcherEventSellerCollectionGetByStatus(View.UserName, (int)VourcherEventStatus.ApplyEvent);
                vourcher_events.AddRange(VourcherFacade.VourcherEventSellerCollectionGetByStatus(View.UserName, (int)VourcherEventStatus.ReturnApply));
            }
            View.SetReturnCaseVourcherEvent(vourcher_events);
        }
        private string ChangeItemsToJson(KeyValuePair<string, string> changeitems)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName(changeitems.Key);
                writer.WriteValue(changeitems.Value);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }
        private void SetSalesmanNameArray()
        {
            ViewEmployee user = _humProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
            IEnumerable<ViewEmployee> viewEmpCol = HumanFacade.ViewEmployeeGetListByDept(EmployeeDept.S000, user.DeptId);
            View.SalesEmailArray = "'" + string.Join("','", viewEmpCol.Select(x => x.EmpName)) + "'";
        }
    }
}

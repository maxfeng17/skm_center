﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using log4net.Util;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponCouponListPresenter : Presenter<IPponCouponListView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();

        private CashTrustLogCollection ctlogs = null;
        private CashTrustLogCollection returnFormCtlogs = null;
        private Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.OrderGuid == Guid.Empty)
            {
                if (conf.OrderFilterVerion == (int)OrderFilterVerion.MemberOrderMainFilterType)
                {
                    GetCouponListMainByMainFilter(1, true, true);
                }
                else
                {
                    GetCouponListMain(1, true, true);
                }
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += this.OnPageChange;
            View.FilterChange += this.OnFilterChange;
            return true;
        }

        #region event

        private void OnPageChange(object sender, DataEventArgs<int> e)
        {
            if (conf.OrderFilterVerion == (int)OrderFilterVerion.MemberOrderMainFilterType)
            {
                GetCouponListMainByMainFilter(e.Data, false, false);
            }
            else
            {
                GetCouponListMain(e.Data, false, false);
            }
        }

        private void OnFilterChange(object sender, EventArgs e)
        {
            if (conf.OrderFilterVerion == (int)OrderFilterVerion.MemberOrderMainFilterType)
            {
                GetCouponListMainByMainFilter(1, false, true);
            }
            else
            {
                GetCouponListMain(1, false, true);
            }
            View.SetUpPageCount();
        }

        #endregion event

        #region method
        public string GetUserMobileNumber()
        {
            Member m = mp.MemberGet(View.UserName);
            return (m != null) ? m.Mobile : string.Empty;
        }

        public DateTime GetNewInvoiceDate()
        {
            return conf.NewInvoiceDate;
        }

        public int GetReturnedQuantity(Guid orderGuid)
        {
            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            return ctCol.Where(x => (x.Status == (int)TrustStatus.Returned || x.Status == (int)TrustStatus.Refunded) && !Helper.IsFlagSet(x.SpecialStatus, TrustSpecialStatus.Freight)).Count();
        }

        public int? GetFirstCouponIdByOrderId(Guid orderGuid)
        {
            ViewCouponListCollection vcl = mp.GetViewCouponListByOrderGuid(orderGuid);
            return vcl.FirstOrDefault().Id;
        }

        /// <summary>
        /// 取得訂單所屬分店有設訂位系統且該檔次有配合訂位服務
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bookingType"></param>
        /// <returns></returns>
        public bool IsEnableBookingSystem(Guid orderGuid, BookingType bookingType)
        {
            ViewPponOrderDetail vpod = op.ViewPponOrderDetailGetListByOrderGuid(orderGuid).FirstOrDefault(x => x.OrderDetailStatus == (int)OrderDetailStatus.None);
            
            bool IsNoRestrictedStore = false;   //通用券
            if (vpod.BusinessHourGuid != null)
            {
                ViewPponDeal vpd = new ViewPponDeal();
                vpd = pp.ViewPponDealGetByBusinessHourGuid(vpod.BusinessHourGuid.Value);
                IsNoRestrictedStore = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
            }
            if (vpod.StoreGuid != null)
            {
                var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
                if (BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid((Guid)vpod.StoreGuid, bookingType) && dealProperty.BookingSystemType > 0)
                {
                    return true;
                }
            }else if (IsNoRestrictedStore)
            {
                //通用券
                var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
                Dictionary<string, string> sellers = new Dictionary<string, string>();
                CashTrustLog vpct = mp.CashTrustLogGetListByOrderGuid(orderGuid).Where(x => x.BusinessHourGuid != null).FirstOrDefault();
                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(vpct.BusinessHourGuid.Value);
                IEnumerable<Seller> ss = SellerFacade.GetSellerTreeSellers(vpd.SellerGuid);
                foreach(Seller seller in ss)
                {
                    if (BookingSystemFacade.GetBookingSystemStoreSettingByStoreGuid(seller.Guid, bookingType) && dealProperty.BookingSystemType > 0)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public int GetAdvanceReservationDays(Guid orderGuid, BookingType bookingType)
        {
            var dealProperty = pp.DealPropertyGetByOrderGuid(orderGuid);
            if (dealProperty != null && dealProperty.IsLoaded)
            {
                return dealProperty.AdvanceReservationDays;
            }
            return 0;
        }

        /// <summary>
        /// 取得訂單資料 [v2.0]
        /// </summary>
        /// <param name="i"></param>
        /// <param name="isInit"></param>
        /// <param name="reCountPage"></param>
        protected void GetCouponListMain(int i, bool isInit, bool reCountPage)
        {
            ViewCouponListMainCollection data = 
                MemberFacade.ViewCouponListMainGetListByUser(i, 10, View.UserId, View.NewFilterType, View.OrderGuid, View.IsLatest,true);
            if (reCountPage)
            {
                View.PageCount =  MemberFacade.ViewCouponListMainGetCount(View.UserId, View.NewFilterType, View.OrderGuid, View.IsLatest);
            }
            View.GetCouponList(data);
            if (isInit)
            {
                int count = mp.GetCouponListMainCount(View.UserId, string.Empty);

                Member m = mp.MemberGet(View.UserName);
                if (m.OrderCount != count)
                {
                    m.OrderCount = count;
                    m.OrderTotal = (int)mp.GetCouponTotalSavings(View.UserName);
                    mp.MemberSet(m);
                }
                View.SetUserMobileNumber();
            }
        }

        /// <summary>
        /// 取得訂單資料 [v3.0]
        /// </summary>
        /// <param name="i"></param>
        /// <param name="isInit"></param>
        /// <param name="reCountPage"></param>
        protected void GetCouponListMainByMainFilter(int i, bool isInit, bool reCountPage)
        {
            ViewCouponListMainCollection data =
                mp.GetCouponListMainColByMainFilter(View.UserId, View.MainFilterType, i, 10);

            if (reCountPage)
            {
                View.PageCount = mp.GetMainFilterOrderCount(View.UserId, View.MainFilterType);
            }

            Member mem = MemberFacade.GetMember(View.UserId);
            if (mem.IsFraudSuspect)
            {
                ViewCouponListMainCollection fakeData = new ViewCouponListMainCollection();
                var items = mp.GetCouponListMainColByMainFilter(View.UserId, View.MainFilterType, i, 20)
                    .Where(t => ((t.DeliveryType == (int)DeliveryType.ToShop && t.TotalCount > 0) ||
                    t.DeliveryType == (int)DeliveryType.ToHouse) && !Helper.IsFlagSet(t.GroupOrderStatus.Value, GroupOrderStatus.SKMDeal)).ToList();
                fakeData.AddRange(items.Skip(10));
                data = fakeData;
            }

            View.GetCouponList(data);
        }

        public string GetCouponStatus(int couponId)
        {
            if (ctlogs == null)
            {
                ctlogs = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.LkSite);
            }
            if (returnFormCtlogs == null)
            {
                returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(View.OrderGuid);
            }

            CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (returningCtlog != null)
            {
                if (returningCtlog.Status == (int)TrustStatus.Initial ||
                    returningCtlog.Status == (int)TrustStatus.Trusted ||
                    (returningCtlog.Status == (int)TrustStatus.Verified && returningCtlog.CouponId==null))
                {
                    return "退貨中";
                }
            }

            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        ExpirationDateSelector selector = GetExpirationComponent(View.OrderGuid);
                        if (GetExpirationComponent(View.OrderGuid) != null && selector.IsPastExpirationDate(DateTime.Now))
                        {
                            return "憑證過期";
                        }
                        else
                        {
                            return "未使用";
                        }

                    case TrustStatus.Verified:
                        return "已使用";

                    case TrustStatus.Returned:
                    case TrustStatus.Refunded:
                        return "已退貨";

                    default:
                        return string.Empty;
                }
            }
            return string.Empty;
        }
                
        /// <summary>
        /// 取得宅配檔次之最晚出貨日
        /// </summary>
        /// <param name="ShipType">宅配出貨方式</param>
        /// <param name="ShippingdateType">宅配出貨次方式</param>
        /// <param name="CreateTime">訂單成立日</param>
        /// <param name="OrderEndTime">好康結檔日</param>
        /// <param name="Shippingdate">訂單成立後天數</param>
        /// <param name="ProductUseDateEndSet">結檔後最晚出貨天數</param>
        /// <param name="BusinessDeliver">開始配送日期</param>
        /// <returns></returns>
        //public DateTime? GetOrderShipEnd(int? ShipType, int? ShippingdateType, DateTime CreateTime,DateTime OrderEndTime, int? Shippingdate,int? ProductUseDateEndSet,DateTime?  BusinessDeliver)
        //{
        //    DateTime? OrderShipEnd = null;  // 宅配最晚出貨日

        //    try
        //    {
        //        VacationCollection recentHolidays = pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));
        //        if (ShipType == (int)DealShipType.Normal)
        //        {
        //            if (ShippingdateType == (int)DealShippingDateType.Normal)
        //            {
        //                OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime, CreateTime.AddDays((double)Shippingdate));
        //            }
        //            else if (ShippingdateType == (int)DealShippingDateType.Special)
        //            {
        //                OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, OrderEndTime, OrderEndTime.AddDays((double)ProductUseDateEndSet));
        //            }
        //        }
        //        else if (ShipType == (int)DealShipType.Ship72Hrs)
        //        {                    
        //            CreateTime = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime.AddDays(-1), CreateTime); //週休假日或例假日成立之訂單，均視為次一工作天之訂單
        //            OrderShipEnd = OrderFacade.ExcludeHolidaysByPeriod(recentHolidays, CreateTime, CreateTime.AddDays(1));
        //        }
        //        else if (ShipType == (int)DealShipType.LastShipment || ShipType == (int)DealShipType.LastDelivery)
        //        {
        //            OrderShipEnd = BusinessDeliver;
        //        }
        //        return OrderShipEnd;
        //    }
        //    catch 
        //    {
        //        return null;
        //    }        
        //}

        //可填評價的憑證數量
        public int GetEvaluatCount(Guid guid)
        {
            return new EvaluateFacade().GetEvaluateCount(guid);
        }

        public int GetGiftCount(Guid guid)
        {
            return MGMFacade.GetGiftCountByOrderId(guid);
        }

        public Guid GetTrustId(int couponId)
        {
            var tid = mp.CashTrustLogGetByCouponId(couponId); 
            if (tid.UsageVerifiedTime < DateTime.Now.AddDays(-7))
            {
                return new Guid();
            }
            if (tid != null)
            {
                var trustId = mp.EvaluateGetMainID(tid.TrustId);
                if (trustId == 0)
                {
                    return tid.TrustId;
                } 
            }
            
            return new Guid();
        }

        public ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }
        #endregion method
    }
}
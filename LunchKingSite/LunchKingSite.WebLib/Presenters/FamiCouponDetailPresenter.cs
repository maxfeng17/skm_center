﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class FamiCouponDetailPresenter : Presenter<IFamiCouponDetailView>
    {
        private ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        protected static IPponProvider pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (View.CouponId == null || View.UserName == null)
            {
                return false;
            }

            ViewPponCoupon coupon = pponProv.ViewPponCouponGet(View.CouponId ?? 0);

            if (!coupon.IsLoaded || string.Equals(coupon.MemberEmail, View.UserName, StringComparison.OrdinalIgnoreCase) == false)
            {
                return false;
            }

            //判斷是否為送禮憑證
            if (mgm.GiftGetByCouponId(View.CouponId ?? 0).IsLoaded)
            {
                return false;
            }

            View.PinCode = coupon.CouponCode;
            View.SiteUrl = _config.SiteUrl;
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(coupon.BusinessHourGuid);

            View.EnableFami3Barcode = vpd.CouponCodeType != (int)CouponCodeType.BarcodeEAN13 && _config.EnableFami3Barcode;

            View.SetFamiCouponContent(coupon, vpd);
            return true;
        }
        public override bool OnViewLoaded()
        {
            return true;
        }

    }
}

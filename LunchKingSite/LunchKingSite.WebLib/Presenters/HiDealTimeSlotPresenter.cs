﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealTimeSlotPresenter : Presenter<IHiDealTimeSlotView>
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            int? defRegion = HiDealRegionManager.DealOverviewRegion;
            RegionsSet(defRegion);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UpdateTimeSlotSequence += OnUpdateTimeSlotSequence;
            View.SelectRegionIdChange += OnSelectRegionIdChange;
            return true;
        }

        private void OnUpdateTimeSlotSequence(object sender, DataEventArgs<HiDealTimeSlotViewUpdateSlotEntity> e)
        {
            foreach (var item in e.Data.NewSlotList)
            {
                hp.HiDealCityCollectionUpdateSequence(item.Key, item.Value);
            }
            GetHiDealTimeSlot(e.Data.SelectRegionId);
        }

        public void OnSelectRegionIdChange(object sender, DataEventArgs<int> e)
        {
            GetHiDealTimeSlot(e.Data);
        }

        #region method

        private void GetHiDealTimeSlot(int regionId)
        {
            var data = hp.ViewHiDealCollectionGetByTime(View.StartDate, View.EndDate, regionId);

            ViewHiDealCollection freshVisaPriorityDeals = hp.ViewHiDealFreshVisaPriority(
                View.StartDate, View.EndDate, regionId);

            ViewHiDealCollection staleVisaPriorityDeals = hp.ViewHiDealStaleVisaPriority(
                View.StartDate, View.EndDate, regionId);

            View.GetHiDealTimeSlot(data.Where(x => x.IsMain).ToList(), data.Where(x => !x.IsMain).ToList(),
               freshVisaPriorityDeals, staleVisaPriorityDeals);
        }

        private void RegionsSet(int? defRegion)
        {
            var regions = HiDealRegionManager.GetRegionOfSet();
            View.SetRegionData(regions, defRegion);
            if (defRegion != null)
            {
                GetHiDealTimeSlot(defRegion.Value);
            }
        }

        #endregion method
    }
}
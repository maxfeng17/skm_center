﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealDealDetailForBusinessPresenter : Presenter<IHiDealDealDetailForBusinessView>
    {
        protected IHiDealProvider hp;

        public HiDealDealDetailForBusinessPresenter(IHiDealProvider hideal)
        {
            hp = hideal;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        private void QueryDealData()
        {
            //檢查有無傳入檔次編號
            if(View.DealId==null)
            {
                //回傳錯誤訊息。導回DealList頁面
                return;
            }

            var deal = hp.HiDealDealGet(View.DealId.Value);
            //檢查有無查到資料
            if(!deal.IsLoaded)
            {
                //查無檔次資料
                return;
            }

            //查詢商品資料
            var products = hp.HiDealProductGetList(0, 0, HiDealProduct.Columns.Id,
                                                   HiDealProduct.Columns.DealId + " = " + View.DealId);
        }
    }
}

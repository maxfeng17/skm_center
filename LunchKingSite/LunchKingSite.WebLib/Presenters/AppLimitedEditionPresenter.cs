﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class AppLimitedEditionPresenter : Presenter<IAppLimitedEdition>
    {
        private readonly ISysConfProvider _config;

        public AppLimitedEditionPresenter()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            IViewPponDeal vpd;
            if (!CheckDeal(out vpd))
            {
                View.RedirectToDefault(_config.SiteUrl, vpd.BusinessHourGuid);
                return true;
            }

            View.RenderViewContent(vpd);

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        private bool CheckDeal(out IViewPponDeal vpd)
        {
            if (View.BusinessHourGuid == Guid.Empty)
            {
                vpd = new ViewPponDeal();
                return false;
            }

            vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);
            if (!vpd.IsLoaded || vpd.BusinessHourGuid == Guid.Empty)
            {
                return false;
            }

            View.IsAppLimitedEditionDeal = PponDealPreviewManager.GetDealCategoryIdList(vpd.BusinessHourGuid).IndexOf(CategoryManager.Default.AppLimitedEdition.CategoryId) > -1;
            return View.IsAppLimitedEditionDeal;
        }
    }
}

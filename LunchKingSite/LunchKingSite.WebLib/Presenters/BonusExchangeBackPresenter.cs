using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class BonusExchangeBackPresenter : Presenter<IBonusExchangeBackView>
    {
        private IOrderProvider op;
        private IItemProvider ip;
        private IMemberProvider mp;

        public BonusExchangeBackPresenter()
        {
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrEmpty(View.RedeemOrderId))
            {
            }
            else
                LoadData(1);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.RedeemOrderDetailClicked += this.OnRedeemOrderDetailClicked;
            View.RedeemOrderUpdated += this.OnRedeemOrderUpdated;
            View.GetOrderCount += this.OnGetOrderCount;
            View.PageChanged += this.OnPageChanged;
            return true;
        }

        private void LoadData(int page)
        {
            View.SetOrderListView(op.RedeemOrderGetList(page, View.PageSize, RedeemOrder.Columns.CreateTime + " DESC"));
        }

        protected void OnRedeemOrderDetailClicked(object sender, DataEventArgs<Guid> e)
        {
            RedeemOrder odr = op.RedeemOrderGet(e.Data);
            RedeemOrderDetailCollection dtl = op.RedeemOrderDetailGetList(e.Data);
            View.SetOrderDetailView(odr, dtl);
        }

        protected void OnRedeemOrderUpdated(object sender, DataEventArgs<UpdatableRedeemOrderInfo> e)
        {
            RedeemOrder r = op.RedeemOrderGet(e.Data.Id);
            if (r.IsLoaded)
            {
                r.MemberName = e.Data.CustomerName;
                r.PhoneNumber = e.Data.CustomerPhone;
                r.MobileNumber = e.Data.CustomerMobile;
                r.DeliveryAddress = e.Data.DeliveryAddress;
                r.DeliveryTime = e.Data.DeliveryTime;
                r.OrderMemo = e.Data.OrderMemo;
                if (r.IsDirty)
                    op.RedeemOrderSet(r);
            }
        }

        protected void OnGetOrderCount(object sender, DataEventArgs<int> e)
        {
            e.Data = op.RedeemOrderGetCount();
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }
    }
}

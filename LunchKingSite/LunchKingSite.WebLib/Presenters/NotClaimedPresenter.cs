﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Diagnostics;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebLib.Presenters
{
    public class NotClaimedPresenter : Presenter<INotClaimedView>
    {
        protected LunchKingSite.Core.IServiceProvider sp = ProviderFactory.Instance().GetProvider<LunchKingSite.Core.IServiceProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {

            base.OnViewInitialized();
            View.MessageList(GetMessageList(1));
            return true;

        }

        public override bool OnViewLoaded()
        {

            base.OnViewLoaded();
            View.AddClaimed += OnAddClaimed;
            View.Search += OnSearch;
            View.ClaimedCount += OnClaimedCount;
            View.ClaimedPageChanged += OnClaimedPageChanged;

            return true;
        }

        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.MessageList(GetMessageList(1));
        }
        protected void OnAddClaimed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.ChoseBox))
            {
                string[] serviceNoArray = View.ChoseBox.Split(',');
                foreach (var item in serviceNoArray)
                {
                    var csm = sp.CustomerServiceMessageGet(item);
                    if (csm.IsLoaded)
                    {
                        csm.Status = (int)statusConvert.process;
                        csm.ModifyId = View.UserId;
                        csm.ModifyTime = DateTime.Now;
                        csm.WorkUser = View.UserId;
                        sp.CustomerServiceMessageSet(csm);
                    }
                }
            }
            View.MessageList(GetMessageList(1));
        }
        protected void OnClaimedCount(object sender, DataEventArgs<int> e)
        {
            e.Data = sp.GetViewCustomerServiceMessageListCount(GetFilter());
        }

        protected void OnClaimedPageChanged(object sender, DataEventArgs<int> e)
        {
            View.MessageList(GetMessageList(e.Data));
        }

        #endregion

        private ViewCustomerServiceListCollection GetMessageList(int pageNumber)
        {

            return sp.GetViewCustomerServiceMessageForNotCaimed(pageNumber, View.PageSize, ViewCustomerServiceList.Columns.CreateTime, GetFilter());

        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();


            if (View.SearchCategory != 0)
            {
                filter.Add(ViewCustomerServiceList.Columns.Category + " = " + View.SearchCategory);
            }
            if (View.SearchOrderGuid != Guid.Empty)
            {
                filter.Add(ViewCustomerServiceList.Columns.OrderGuid + " = " + View.SearchOrderGuid);
            }
            if (!string.IsNullOrEmpty(View.SearchServiceNo))
            {
                filter.Add(ViewCustomerServiceList.Columns.ServiceNo + " = " + View.SearchServiceNo);
            }
            if (!string.IsNullOrEmpty(View.Mail))
            {
                Member mem = mp.MemberGetByUserName(View.Mail);
                if (mem.IsLoaded)
                {
                    filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mem.UniqueId);
                }
                else
                {
                    MobileMember mobileMem = mp.MobileMemberGet(View.Mail);
                    if (mobileMem.IsLoaded)
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = " + mobileMem.UserId);
                    }
                    else
                    {
                        filter.Add(ViewCustomerServiceList.Columns.UserId + " = 0 ");
                    }
                }
            }
            if (!string.IsNullOrEmpty(View.SDate))
            {
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " >= " + View.SDate);
            }
            if (!string.IsNullOrEmpty(View.EDate))
            {
                filter.Add(ViewCustomerServiceList.Columns.CreateTime + " <= " + View.EDate);
            }

            filter.Add(ViewCustomerServiceList.Columns.CustomerServiceStatus + " = 0");

            return filter.ToArray();
        }

    }
}

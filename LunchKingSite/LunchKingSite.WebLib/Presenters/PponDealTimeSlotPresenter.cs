﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponDealTimeSlotPresenter : Presenter<IPponDealTimeSlotView>
    {
        private IPponProvider _pponProv;
        private ISysConfProvider _config;

        public PponDealTimeSlotPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SelectCityIdChange += OnSelectCityIdChange;

            //每次頁面開啟都要先把JQUERY用的欄位框架建立好
            DefaulDataShow();
            return true;
        }

        protected void DefaulDataShow()
        {
            PponDealCalendarPage pageData = new PponDealCalendarPage(7);

            SetCityNameToPage(pageData);

            DateTime firstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
            if (DateTime.Now.Hour < 12)
            {
                firstDate = firstDate.AddDays(-1);
            }
            //目前共產生七天的表格資料
            for (int i = 0; i < pageData.SlotDateLeagth; i++)
            {
                DateTime workDate = firstDate.AddDays(i);
                //設定日期用於顯示
                pageData.PponColumns[i].TitleName = workDate.ToString("MM/dd hh:mm") + " ~ " + workDate.AddDays(1).AddMinutes(-1).ToString("MM/dd hh:mm");
            }

            View.ShowPponDealTimeSlotData(pageData);
        }

        protected void QueryData(PponDealTimeSlotViewSearchRequest request)
        {
            PponDealCalendarPage pageData;
            if (request.CityId != null && request.CityId.Value == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                pageData = new PponDealCalendarPage(_config.DealTimeSlotDays);
            }
            else
            {
                pageData = new PponDealCalendarPage(7);
            }

            SetCityNameToPage(pageData);

            DateTime firstDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 12, 0, 0);
            if (DateTime.Now.Hour < 12)
            {
                firstDate = firstDate.AddDays(-1);
            }

            //取得七天內的新上檔次
            ViewPponBusinessHourGuidCollection newDeals = _pponProv.ViewPponBusinessHourGuidGetList(firstDate, firstDate.AddDays(7));

            //目前共產生七天的表格資料
            for (int i = 0; i < pageData.SlotDateLeagth; i++)
            {
                DateTime workDate = firstDate.AddDays(i);
                //設定日期用於顯示
                pageData.PponColumns[i].TitleName = workDate.ToString("MM/dd hh:mm") + " ~ " + workDate.AddDays(1).AddMinutes(-1).ToString("MM/dd hh:mm");

                //設定每天的新檔次
                foreach (ViewPponBusinessHourGuid vpbhg in newDeals.Where(x => x.BusinessHourOrderTimeS.HasValue && int.Equals(workDate.Day, x.BusinessHourOrderTimeS.Value.Day)))
                {
                    pageData.NewDealData[i].Add(vpbhg.Guid.Value);
                }

                //依據查詢所有程式的資料
                List<string> searchWhere = new List<string>();
                searchWhere.Add(ViewPponDealTimeSlot.Columns.EffectiveStart + " >= " + workDate.ToString("yyyy/MM/dd HH:mm:ss"));
                searchWhere.Add(ViewPponDealTimeSlot.Columns.EffectiveStart + " < " + workDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"));
                if (request.CityId != null)
                {
                    searchWhere.Add(ViewPponDealTimeSlot.Columns.CityId + " = " + request.CityId.Value);
                }

                ViewPponDealTimeSlotCollection dataColl =
                    _pponProv.ViewPponDealTimeSlotGetList(
                        ViewPponDealTimeSlot.Columns.CityId + "," + ViewPponDealTimeSlot.Columns.Sequence,
                        searchWhere.ToArray());

                int iCity = 0;
                ViewPponDealTimeSlotCollection showColl = new ViewPponDealTimeSlotCollection();
                foreach (ViewPponDealTimeSlot item in dataColl.Where(x => !((x.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)))
                {
                    if (iCity != item.CityId)
                    {
                        if (showColl.Count > 0)
                        {
                            PponCity showCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(iCity);

                            if (showCity != null)
                            {
                                ShowDealTimeSlotDataToPage(pageData, showCity, showColl, i);

                            }
                        }
                        showColl = new ViewPponDealTimeSlotCollection();
                        iCity = item.CityId;
                    }
                    showColl.Add(item);
                }
                //處理最後一筆
                if (showColl.Count > 0)
                {
                    PponCity showCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(iCity);
                    if (showCity != null)
                    {
                        ShowDealTimeSlotDataToPage(pageData, showCity, showColl, i);
                    }
                }
            }

            View.ShowPponDealTimeSlotData(pageData);
        }

        protected void SetCityNameToPage(PponDealCalendarPage page)
        {
            page.AllCountryData.CityName = I18N.Phrase.PponCityAllCountry;
            page.TaipeiData.CityName = I18N.Phrase.PponCityTaipeiCity;
            page.NewTaipeiCityData.CityName = I18N.Phrase.PponCityNewTaipeiCity;
            page.TaoyuanData.CityName = I18N.Phrase.PponCityTaoyuan;
            page.HsinchuData.CityName = I18N.Phrase.PponCityHsinchu;
            page.TaichungData.CityName = I18N.Phrase.PponCityTaichung;
            page.TainanData.CityName = I18N.Phrase.PponCityTainan;
            page.KaohsiungData.CityName = I18N.Phrase.PponCityKaohsiung;
            page.TravelData.CityName = I18N.Phrase.PponCityTravel;
            page.PeautyData.CityName = I18N.Phrase.PponCityPeauty;
            page.ComData.CityName = I18N.Phrase.PponCityPEZ;
            page.FamilyData.CityName = I18N.Phrase.PponCityFamily;
            page.Tmall.CityName = I18N.Phrase.PponCityTmall;
            page.Piinlife.CityName = "品生活";
            page.SkmData.CityName = I18N.Phrase.PponCitySkm;
        }

        protected void ShowHotDealSetting(PponDealTimeSlotViewSearchRequest request)
        {
            if (request.CityId != null)
            {
                var hdc = _pponProv.HotDealConfigGet(request.CityId.Value);
                if (hdc.IsLoaded)
                {
                    View.ShowHotDealSetting(hdc);
                }
            }
        }

        //show權重表
        protected void ShowWeights(PponDealTimeSlotViewSearchRequest request)
        {
            if (request.CityId != null)
            {
                var cityWeights = _pponProv.DealTimeSlotSortElementGet().Where(x => x.CityId == request.CityId.Value).ToList();
                if (cityWeights.Count > 0)
                {
                    View.ShowPponDealTimeSlotWeights(cityWeights, _pponProv.DealTimeSlotSortElementRankGet().ToList());
                }
                else if (CitiesGet().Contains(request.CityId.Value))
                {
                    var dtsseList = new DealTimeSlotSortElementCollection();

                    foreach (var elementId in Enum.GetValues(typeof(DealTimeSlotElement)))
                    {
                        dtsseList.Add(new DealTimeSlotSortElement
                        {
                            CityId = request.CityId.Value,
                            ElementId = (int)elementId,
                            ElementWeight = 1
                        });
                    }
                    if (_pponProv.DealTimeSlotSortElementInsert(dtsseList))
                    {
                        View.ShowPponDealTimeSlotWeights(
                            _pponProv.DealTimeSlotSortElementGet().Where(x => x.CityId == request.CityId.Value).ToList(),
                            _pponProv.DealTimeSlotSortElementRankGet().ToList());
                    }
                }
            }
        }

        protected void ShowDealTimeSlotDataToPage(PponDealCalendarPage page, PponCity city, ViewPponDealTimeSlotCollection dataCol, int colIndex)
        {
            if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                page.AllCountryData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
            {
                page.TaipeiData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId)
            {
                page.NewTaipeiCityData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
            {
                page.TaoyuanData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId)
            {
                page.HsinchuData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
            {
                page.TaichungData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Tainan.CityId)
            {
                page.TainanData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
            {
                page.KaohsiungData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                page.TravelData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                page.PeautyData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.PEZ.CityId)
            {
                page.ComData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.DepositCoffee.CityId)
            {
                page.DepositCoffeeData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Family.CityId)
            {
                page.FamilyData.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Tmall.CityId)
            {
                page.Tmall.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
            {
                page.Piinlife.TimeSlots[colIndex] = dataCol;
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Skm.CityId)
            {
                page.SkmData.TimeSlots[colIndex] = dataCol;
            }
        }

        private void OnSelectCityIdChange(object sender, DataEventArgs<PponDealTimeSlotViewSearchRequest> e)
        {
            QueryData(e.Data);
            ShowWeights(e.Data);
            ShowHotDealSetting(e.Data);
        }

        private static List<int> CitiesGet()
        {
            var cities = new List<int>
            {
                PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId,
                PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId,
                PponCityGroup.DefaultPponCityGroup.Hsinchu.CityId,
                PponCityGroup.DefaultPponCityGroup.Taichung.CityId,
                PponCityGroup.DefaultPponCityGroup.Tainan.CityId,
                PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId,
                PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId,
                PponCityGroup.DefaultPponCityGroup.Piinlife.CityId
            };
            return cities;
        }

    }
}

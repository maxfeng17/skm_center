﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponReferralRedirectionPresenter : Presenter<IPponReferralRedirectionView>
    {
        private IPponProvider pp;
        private IMemberProvider mp;

        public PponReferralRedirectionPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            IViewPponDeal mainDeal = null;
           
            //檢查有無QueryString中ReferenceId，若有產生一個cookie記錄於使用者電腦中
            string referenceUserName = string.Empty;
            string changeUrl = View.WebRoot + "/ppon/default.aspx";

            if (View.BusinessHourId != Guid.Empty)
            {
                mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourId, true);
                ResolveEventName(mainDeal);
                View.SetPageMeta(mainDeal);
                changeUrl = View.WebRoot + "/deal/" + View.BusinessHourId;
            }

            if (!string.IsNullOrEmpty(View.ReferenceId))
            {
                if (View.SetCookie(View.ReferenceId, DateTime.Now.AddHours(72), LkSiteCookie.ReferenceId)) //cookie的值為推薦人MemberId, cookie有效期限預設為72小時
                {
                    //顯示提示訊息，告訴使用者於72小時內購物，將會發放紅利給推薦人
                    string[] referenceId = View.ReferenceId.Split('|');
                    string externalId = referenceId[1];
                    int iSingleSign = int.Parse(referenceId[0]);
                    if (Enum.IsDefined(typeof(SingleSignOnSource), iSingleSign))
                    {
                        MemberLink ml = mp.MemberLinkGetByExternalId(externalId, (SingleSignOnSource)iSingleSign);

                        if (ml.IsLoaded)
                        {
                            View.RedirectPage(changeUrl);
                        }
                        else
                        {
                            View.ShowMessage("查無邀請人資料。", changeUrl);
                        }
                    }
                    else
                    {
                        View.ShowMessage("查無邀請人資料。", changeUrl);
                    }
                }
            }
            else
            {
                View.ShowMessage("連結內容有誤，未完成邀請作業，請重新取得連結。", changeUrl);
            }

            return true;
        }

        private void ResolveEventName(IViewPponDeal mainDeal)
        {
            string newEventName = PponFacade.PponNewContentGetByDeal(mainDeal);
            mainDeal.EventName = newEventName;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
    }
}
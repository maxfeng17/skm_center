﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class CtAtmComparedPresenter : Presenter<ICtAtmComparedView>
    {
        private IOrderProvider op;
        public CtAtmComparedPresenter()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized(); 
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ImportClicked += OnImportClicked;
            View.Search += OnSearch;
            return true;
        }

        protected void OnImportClicked(object sender, DataEventArgs<StreamReader> e)
        {
            StreamReader sReader = (StreamReader)e.Data;                  //要匯入的FileStream

            string line;
            while ((line = sReader.ReadLine()) != null)
            {
                string atmTime = line.Substring(13, 7);
                string prefix = line.Substring(115, 5);
                string virtualAccount = line.Substring(20, 9);
                int amount = Convert.ToInt32(line.Substring(33, 14));
                CtAtmCompared ct = new CtAtmCompared() {
                    VirtualAccount = prefix + virtualAccount,
                    Amount = amount,
                    Status = 0,
                    CreateTime = DateTime.Now,
                    AtmTime = atmTime
                };

                op.CtAtmComparedSet(ct);
            }
        }
        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData();
        }
        private void LoadData()
        {
            List<string> accs = new List<string>();
            DateTime dt = DateTime.Now;
            DateTime.TryParse(View.AtmDate, out dt);

            for(int idx = 0; idx < 5; idx++)
            {
                dt = dt.AddDays(0 - idx);
                CtAtmComparedCollection cols = op.CtAtmComparedGetAtmTime((Convert.ToInt32(dt.ToString("yyyyMMdd")) - 19000000).ToString());
                if(cols.Count == 0)
                {
                    accs.Add(dt.ToString("yyyy/MM/dd") + "資料未匯入");
                }
            }

            CtAtmCollection atms = op.CtAtmGetList(DateTime.Now.AddDays(-5));

            foreach(CtAtm atm in atms)
            {
                CtAtmCompared comp = op.CtAtmComparedGetByVirtualAccount(atm.VirtualAccount);
                if(comp != null && comp.IsLoaded)
                {
                    //如果中國信託有找到，但是Atm這邊是空的
                    if(atm.Status != (int)AtmStatus.Paid)
                    {
                        accs.Add(atm.VirtualAccount);
                    }
                }
            }

            View.SetContentList(accs);
        }
    }
}

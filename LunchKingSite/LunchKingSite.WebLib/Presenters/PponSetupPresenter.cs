﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NewtonsoftJson = Newtonsoft.Json;
using log4net;
using System.Text.RegularExpressions;
using Microsoft.VisualBasic;
using System.Drawing;
using System.Net.Mail;
using System.Xml.Schema;
using Microsoft.VisualBasic.ApplicationServices;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using LunchKingSite.BizLogic.Jobs;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponSetupPresenter : Presenter<IPponSetupView>
    {
        private IPponProvider _pponProv;
        private ILocationProvider _locProv;
        private ISellerProvider _sellerProv;
        private IItemProvider _itemProv;
        private ISysConfProvider _config;
        private IOrderProvider _orderProv;
        private IHumanProvider _humanProv;
        private ISystemProvider _systemProv;
        private IMemberProvider _memProv;
        private SalesService _salServ;
        private PponDealClose _dealClose;
        private ISkmProvider _skmProv;
        private ISellerProvider _sp;
        private ILog logger = LogManager.GetLogger("PponSetupPresenter");
        private int ancQuantity;
        private const int FamilyLockCategoryId = 320;
        private const int FamilyOnlyTodayCategoryId = 321;
        private const int FamilyBeaconUnLockCategoryId = 323;
        private static ILog Log = LogManager.GetLogger(typeof(PponSetupPresenter));
        private IChannelProvider _chProv;

        public PponSetupPresenter(IPponProvider pp, ILocationProvider lp, ISellerProvider sp, IItemProvider ip, IOrderProvider op, IHumanProvider hp, ISystemProvider sysP, SalesService sals)
        {
            _pponProv = pp;
            _locProv = lp;
            _sellerProv = sp;
            _itemProv = ip;
            _config = ProviderFactory.Instance().GetConfig();
            _orderProv = op;
            _systemProv = sysP;
            _humanProv = hp;
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _salServ = sals;
            _dealClose = new PponDealClose();
            _skmProv = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _chProv = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.SellerId == Guid.Empty && View.BusinessHourId == Guid.Empty)
            {
                View.JumpTo(PponSetupMode.SellerNotFound, null);
                return false;
            }

            View.DefaultOnTime = TimeSpan.Parse(_config.PponOnTime);
            View.DefaultEndTime = TimeSpan.Parse(_config.PponEndTime);

            View.GrossMarginVal = _config.GrossMargin;

            SetupSelectableChannel();
            SetupSelectableCommercialCategories();
            SetupDealLabels();
            SetupGroupCouponAppStyleCategory();
            SetupDefaultIconSystemCode();
            SetupSelectableDealSpecialCategory();
            SetupUnhiddenDealSpecialCategory();

            var accBusinessGroup = _systemProv.AccBusinessGroupGetList();
            //取得行銷和業務的所有部門, 作為業務主管選擇業務歸屬的介面, 這是因為業績歸屬的規則改變.
            var depts = _humanProv.DepartmentGetListBySalesAndMarketing(true);
            bool isNewDeal = View.BusinessHourId == Guid.Empty;
            View.SetAccField(accBusinessGroup, CityManager.Citys.Where(x => x.Code != "SYS").ToList(), depts);

            //建立銷售分類
            if (isNewDeal)
            {
                View.SetDealType(0, 0, SystemCodeManager.GetDealType().OrderBy(x => x.Seq).ToList());
                View.AgentChannels = string.Join(",", ProposalFacade.GetAgentChannels().Select(x => x.Key).ToList());
            }

            View.SetSellerField(SystemCodeManager.GetSellerVerifyType());
            if (!isNewDeal)
            {
                LoadDeal(View.BusinessHourId);
                try
                {
                    BusinessOrder businessOrder = _salServ.GetBusinessOrderByBusinessHourGuid(View.BusinessHourId);

                    if (businessOrder != null)
                    {
                        View.BusinessOrderId = businessOrder.BusinessOrderId;
                    }
                }
                catch (Exception ex)
                {
                    logger.Error("連線錯誤，使用者:" + View.UserName, ex);
                }

            }
            else
            {
                //View.PayToCompany = DealAccountingPayType.PayToCompany;
                if (View.SellerId != Guid.Empty)
                {
                    var sellerInfo = _sellerProv.SellerGet(View.SellerId);
                    if (sellerInfo != null && sellerInfo.IsLoaded)
                    {
                        View.SellerPaidType = _sellerProv.SellerGet(View.SellerId).RemittanceType;
                        View.SellerName = string.Format("{0} {1}", sellerInfo.SellerId, sellerInfo.SellerName);
                    }
                }
                View.PaidType = View.SellerPaidType.HasValue
                                    ? (RemittanceType)View.SellerPaidType.Value
                                    : RemittanceType.AchWeekly;

                if (View.IsKindDeal)
                {
                    View.BillingModel = VendorBillingModel.None;
                }
                else
                {
                    View.BillingModel = VendorBillingModel.BalanceSheetSystem;
                }
                View.ReceiptType = VendorReceiptType.Invoice;


                /*Roger 這個不知道需不需要
                if (View.PayType == PayTypeModel.TaiShinBank)
                {
                    View.PayType = PayTypeModel.TaiShinBank;
                }
                else
                {
                    View.PayType = PayTypeModel.None;
                }
                */
                View.SetDealStage(PponDealStage.NotExist);
                var seller = _sp.SellerGet(View.SellerId);
                if (seller != null)
                {
                    View.SellerName = string.Format("{0} {1}", seller.SellerId, seller.SellerName);
                }

                SetInitStores(View.SellerId);
                SetupSellerTreeView(View.SellerId, false);
            }

            View.SetExpireRedirectDisplay();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveDeal += OnSaveDeal;
            View.CopyDeal += OnCopyDeal;
            View.CreateOrder += OnCreateOrder;
            View.GenerateCoupon += OnGenerateCoupon;
            View.DeleteDeal += OnDeleteDeal;
            View.CreateDealTimeSlot += OnCreateDealTimeSlot;
            View.SetBBH += OnSetBBH;
            View.ImportBusinessOrderInfo += OnImportBusinessOrderInfo;
            View.ProposalImport += OnProposalImport;
            View.ManualDealClose += OnManualDealClose;
            View.SyncPChome1 += OnSyncPChome1;
            View.SyncPChome2 += OnSyncPChome2;
            View.RreceiveType_TextChanged += OnRreceiveType_TextChanged;
            View.CheckFlag += OnCheckFlag;
            View.DeleteCDNImages += OnDeleteCDNImages;
            View.SetShoppingCart += OnSetShoppingCart;

            #region 每次載入頁面須執行的部分

            SetSalesmanNameArray();
            //SetupSelectableChannel();
            #endregion 每次載入頁面須執行的部分

            return true;
        }
      
        private void SetupComboDeals(PponDeal deal)
        {
            var comboDeals = _pponProv.GetViewComboDealAllByBid(View.BusinessHourId);
            PponDeal comboMain = null;
            Guid comboMainBid = comboDeals.Where(t => t.MainBusinessHourGuid == t.BusinessHourGuid).Select(t => t.BusinessHourGuid).FirstOrDefault();
            if (comboMainBid != Guid.Empty)
            {
                comboMain = _pponProv.PponDealGet(comboMainBid);
            }
            var comboSubDeals = comboDeals.Where(t => t.MainBusinessHourGuid != t.BusinessHourGuid).ToList();

            Guid mainGuid = Guid.Empty;

            if (comboDeals.Count > 0)
            {
                mainGuid = (Guid)comboSubDeals[0].MainBusinessHourGuid;
            }
            List<PponDeal> comboEntityDeals = new List<PponDeal>();
            List<CategoryDealCollection> comboCategoryDeals = new List<CategoryDealCollection>();
            List<DealAccounting> comboAccountingDeals = new List<DealAccounting>();
            List<ViewPponDeal> comboViewPponDeal = new List<ViewPponDeal>();
            //List<GroupOrder> comboGroupOrderDeals = new List<GroupOrder>();
            //取出母檔
            //DealProperty _MainProperty = _pponProv.DealPropertyGet(mainGuid);
            //BusinessHour _MainHour = _pponProv.BusinessHourGet(mainGuid);
            DealAccounting mainAccounting = _pponProv.DealAccountingGet(mainGuid);
            CategoryDealCollection mainCategory = _pponProv.CategoryDealsGetList(mainGuid);
            //PponDeal _entityMain = _pponProv.PponDealGet(mainGuid);
            //GroupOrder orderGo = _orderProv.GroupOrderGetByBid(mainGuid);
            ViewPponDeal pponOrder = _pponProv.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + " = " + mainGuid);
            //取出子檔
            foreach (var combo in comboDeals)
            {
                comboEntityDeals.Add(_pponProv.PponDealGet(combo.BusinessHourGuid));
                comboCategoryDeals.Add(_pponProv.CategoryDealsGetList(combo.BusinessHourGuid));
                comboAccountingDeals.Add(_pponProv.DealAccountingGet(combo.BusinessHourGuid));
                comboViewPponDeal.Add(_pponProv.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + " = " + combo.BusinessHourGuid));
            }


            List<Category> categoryList = CategoryManager.CategoryGetAllList();

            //CategoryGetAllList

            var sellerGuid = _sellerProv.SellerGetByBid(View.BusinessHourId).Guid;
            View.SetInitComboDeals(
                _pponProv.ViewPponDealGetBySellerGuid(sellerGuid, View.BusinessHourId),
                deal,
                comboMain,
                comboSubDeals,
                comboEntityDeals,
                mainCategory,
                comboCategoryDeals,
                mainAccounting,
                comboAccountingDeals,
                comboViewPponDeal,
                pponOrder,
                categoryList
                );
        }

        private void LoadDeal(Guid guid)
        {
            PponDeal deal = _pponProv.PponDealGet(guid);

            View.IsMulti = ((deal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0) ||
                ((deal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0) ? "1" : "0";

            if (!deal.Deal.IsLoaded)
            {
                View.JumpTo(PponSetupMode.DealNotFound, null);
                return;
            }
            var pponStores = _pponProv.PponStoreGetListByBusinessHourGuid(deal.Deal.Guid);
            ViewPponDeal pponOrder = _pponProv.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + " = " + guid);

            View.IsDealClosed = pponOrder.Slug != null;
            View.BusinessHourUniqueId = pponOrder.UniqueId.ToString();
            View.SellerName = string.Format("{0} {1}", pponOrder.SellerId, pponOrder.SellerName);
            View.SellerGuid = deal.Store.Guid;
            View.TheDeliveryType = (DeliveryType)(pponOrder.DeliveryType ?? 0);
            //設定完TheDeliveryType後，須同步異動對應可選取的DealSpecialCategory資料
            SetupUnhiddenDealSpecialCategory();

            DealAccounting dAccounting = _pponProv.DealAccountingGet(View.BusinessHourId);
            DealCostCollection costCol = _pponProv.DealCostGetList(View.BusinessHourId);
            View.SetAccountingData(dAccounting, (costCol.Count > 0) ? costCol : null);

            if (PponFacade.IsSkmDeal(View.BusinessHourId) == false)
            {
                SetInitStores(deal.Store.Guid);
                SetupSellerTreeView(deal.Store.Guid, View.TheDeliveryType == DeliveryType.ToHouse);
            }
            PchomeChannelProd pchomeProd;
            bool isPchomeChannelProd = ChannelFacade.IsPChomeChannelProd(guid, out pchomeProd);

            View.IsAllowedToReviseCPC = pponOrder.OrderedQuantity <= 0;
            View.SetDealView(deal, PponFacade.GetPponOptionSetupText(guid), deal.DealContent, pponStores, isPchomeChannelProd);

            if (pponOrder.ShoppingCart.HasValue)
            {
                View.DShoppingCart = pponOrder.ShoppingCart.Value;
            }

            View.GroupCoupon = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            View.GroupCouponType = pponOrder.GroupCouponDealType ?? 0;
            View.ComboPackCount = pponOrder.ComboPackCount ?? 0;
            View.PresentQuantity = pponOrder.PresentQuantity ?? 0;
            View.SaleMultipleBase = pponOrder.SaleMultipleBase ?? 1;
            View.GroupCouponAppStyle = pponOrder.GroupCouponAppStyle ?? 0;
            View.SetDealStage(pponOrder.GetDealStage());

            View.IsMergeCount = deal.Property.IsMergeCount;

            //if (pponOrder.GetDealStage() != PponDealStage.NotExist && pponOrder.GetDealStage() != PponDealStage.Created)
            //{
            //    View.IsHiddenFromRecentDeals = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.Hidden);
            //}
            View.DisableSMS = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.DisableSMS);
            View.NoRefund = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefund);
            View.DaysNoRefund = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.DaysNoRefund);
            View.ExpireNoRefund = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.ExpireNoRefund);
            View.NoRefundBeforeDays = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoRefundBeforeDays);
            View.NoTax = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax);
            View.NoInvoice = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NoInvoice);
            if (pponOrder.ItemPrice == 0)
            {
                View.PriceZeorShowOriginalPrice = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
            }
            else
            {
                View.PriceZeorShowOriginalPrice = false;
            }
            View.LowGrossMarginAllowedDiscount = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.LowGrossMarginAllowedDiscount);


            Guid comboMainBid = Guid.Empty;
            var comboDeals = _pponProv.GetViewComboDealAllByBid(guid);
            if ((deal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                comboMainBid = guid;
            }
            else if ((deal.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
            {
                comboMainBid = comboDeals.Where(t => t.MainBusinessHourGuid == t.BusinessHourGuid).Select(t => t.BusinessHourGuid).FirstOrDefault();
            }

            if (comboMainBid == Guid.Empty)
            {
                comboMainBid = guid;
            }

            View.NotAllowedDiscount = _orderProv.DiscountLimitEnabledGetByBid(comboMainBid);


            View.BuyoutCoupon = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.BuyoutCoupon);
            View.NoDiscountShown = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NoDiscountShown);
            View.DonotInstallment = deal.Property.DenyInstallment;
            //天貓檔次
            View.TmallDeal = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.TmallDeal);
            // 取得代收轉付信託類型
            View.TrustType = Helper.GetBusinessHourTrustProvider(pponOrder.BusinessHourStatus);
            //View.NoShippingFeeMessage = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NoShippingFeeMessage);
            View.NotDeliveryIslands = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
            //通用券
            View.NoRestrictedStore = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
            //檔次資訊不出現於Web
            View.NotShowDealDetailOnWeb = Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
            //PEZ特殊活動 新增GroupOrderStatus
            View.PEZevent = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.PEZevent);
            View.PEZeventCouponDownload = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventCouponDownload);
            View.PEZeventWithUserId = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.PEZeventWithUserId);
            //全家檔次
            View.FamiDeal = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal);
            //萊爾富檔次
            View.HiLifeDeal = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.HiLifeDeal);
            //公益檔次
            View.IsKindDeal = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
            View.IsSinglePinCode = (pponOrder.PinType == (int)PinType.Single) ? true : false;
            //新光檔次
            View.SkmDeal = Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal);
            if (_config.IsConsignment)
            {
                View.Consignment = deal.Property.Consignment;
            }
            //BabyHome不顯示的設定
            if (Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.NotInBBH))
            {
                View.btnNotInBBH = "BBH:不顯示";
            }
            else
            {
                View.btnNotInBBH = "BBH:顯示";
            }

            #region SMSContent

            SmsContent sms = _pponProv.SMSContentGet(guid);
            View.SetSMSContent(sms);

            #endregion SMSContent

            #region DealProperty

            var dealProperty = _pponProv.DealPropertyGet(View.BusinessHourId);
            int? dealType = dealProperty.DealTypeDetail ?? dealProperty.DealType;
            if (dealType != null)
            {
                dealType = DealTypeFacade.GetClosestNode(dealType.Value);
                SystemCode sc = _systemProv.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), dealType.Value);
                View.SetDealType(sc.CodeId, dealType.Value, SystemCodeManager.GetDealType().ToList());
            }
            else
            {
                View.SetDealType(0, 0, SystemCodeManager.GetDealType().OrderBy(x => x.Seq).ToList());
            }

            View.IsLongContract = dealProperty.IsLongContract;
            View.MultipleBranch = dealProperty.MultipleBranch;
            View.CompleteCopy = dealProperty.CompleteCopy;
            View.EntrustSell = (EntrustSellType)dealProperty.EntrustSell;
            View.TravelPlace = dealProperty.TravelPlace;
            if (dealProperty.SellerVerifyType != null)
            {
                View.SellerVerifyType = dealProperty.SellerVerifyType.Value;
            }

            if (dealProperty.CouponSeparateDigits != null || dealProperty.CouponSeparateDigits != 0)
            {
                View.CouponSeparateDigits = dealProperty.CouponSeparateDigits;
            }

            if (dealProperty.DealAccBusinessGroupId != null)
            {
                View.AccBusinessGroupNo = dealProperty.DealAccBusinessGroupId.Value;
            }

            View.AncestorCouponCount = View.SuccessorCouponCount = View.AncestorOrderedQuantity = View.SuccessorOrderedQuantity = string.Empty;
            if (dealProperty.AncestorBusinessHourGuid.HasValue)
            {
                View.AncestorBusinessHourGuid = dealProperty.AncestorBusinessHourGuid.Value.ToString();

                if (dealProperty.IsContinuedQuantity)
                {
                    View.AncestorOrderedQuantity = "上檔次最後數量：" + dealProperty.ContinuedQuantity;
                    View.SuccessorOrderedQuantity = "本檔次接續數量：" + (dealProperty.ContinuedQuantity + 1);
                }

                View.Is_Continued_Quantity = dealProperty.IsContinuedQuantity;
            }
            if (dealProperty.AncestorSequenceBusinessHourGuid.HasValue)
            {
                View.AncestorSequenceBusinessHourGuid = dealProperty.AncestorSequenceBusinessHourGuid.Value.ToString();
                if (dealProperty.IsContinuedSequence)
                {
                    int oCount = OrderFacade.GetAncestorCouponCount(View.BusinessHourId);
                    View.AncestorCouponCount = "上檔次最後號碼：" + oCount;
                    View.SuccessorCouponCount = "本檔次接續號碼：" + (oCount + 1);
                }
                View.Is_Continued_Sequence = dealProperty.IsContinuedSequence;
            }
            View.IsDailyRstriction = dealProperty.IsDailyRestriction;
            View.ActivityUrl = dealProperty.ActivityUrl;

            //取得 不顯示折後價於前台
            View.IsDealDiscountPriceBlacklist = PponFacade.IsDealDiscountPriceBlacklist(View.BusinessHourId);

            List<int> dealDefaultIconList = new List<int>();
            //設定熱銷 (排除公益檔次)
            if (!View.IsKindDeal)
            {
                if (pponOrder.OrderedQuantity >= 1000)
                {
                    dealDefaultIconList.Add((int)DealLabelSystemCode.SellOverOneThousand);
                }
            }

            if (!string.IsNullOrEmpty(dealProperty.LabelIconList))
            {
                if (dealProperty.LabelIconList.Split(',')
                                .Select(int.Parse)
                                .ToList()
                                .IndexOf((int)DealLabelSystemCode.AppLimitedEdition) != -1)
                {
                    dealDefaultIconList.Add((int)DealLabelSystemCode.AppLimitedEdition);
                }

                if (dealProperty.LabelIconList.Split(',')
                                .Select(int.Parse)
                                .ToList()
                                .IndexOf((int)DealLabelSystemCode.TwentyFourHoursArrival) != -1)
                {
                    dealDefaultIconList.Add((int)DealLabelSystemCode.TwentyFourHoursArrival);
                }
            }

            View.SelectedDefaultIcon = dealDefaultIconList;

            //List<int> temp_DealPropertyLabelIconList = new List<int>();
            List<int> tempDealPropertyLabelTagList = new List<int>();

            //if (!string.IsNullOrEmpty(dealProperty.LabelIconList))
            //{
            //    temp_DealPropertyLabelIconList = new List<int>(string.IsNullOrEmpty(dealLabelIconList) ? dealProperty.LabelIconList.Split(',').Select(int.Parse) : (dealLabelIconList + "," + dealProperty.LabelIconList).Split(',').Select(int.Parse));
            //}
            //else if (!string.IsNullOrEmpty(dealLabelIconList))
            //{
            //    temp_DealPropertyLabelIconList = new List<int>(dealLabelIconList.Split(',').Select(int.Parse));
            //}
            if (!string.IsNullOrEmpty(dealProperty.LabelTagList))
            {
                tempDealPropertyLabelTagList = new List<int>(dealProperty.LabelTagList.Split(',').Select(int.Parse));
            }

            if (View.TheDeliveryType == DeliveryType.ToShop)
            {
                //View.SelectedCouponIcon = temp_DealPropertyLabelIconList.ToArray();
                View.SelectedCouponDealTag = tempDealPropertyLabelTagList.ToArray();
                View.SelectedCouponTravelDealTag = tempDealPropertyLabelTagList.ToArray();
            }
            else if (View.TheDeliveryType == DeliveryType.ToHouse)
            {
                //宅配+旅遊必勾實體票券
                tempDealPropertyLabelTagList.Add((int)DealLabelSystemCode.Tickets);
                //View.SelectedDeliveryIcon = temp_DealPropertyLabelIconList.ToArray();
                View.SelectedDeliveryDealTag = tempDealPropertyLabelTagList.ToArray();
                View.SelectedDeliveryTravelDealTag = tempDealPropertyLabelTagList.ToArray();
            }
            View.CustomTag = dealProperty.CustomTag;

            //咖啡寄杯
            View.IsDepositCoffee = dealProperty.IsDepositCoffee;

            //反向核銷
            View.IsReverseVerify = dealProperty.VerifyActionType == (int)VerifyActionType.Reverse;

            //全家檔次 barcode / pingcode 選項
            View.FamiDealType = dealProperty.CouponCodeType;
            View.ExchangePrice = dealProperty.ExchangePrice;

            //SKM
            View.SkmExperience = dealProperty.IsExperience ?? false;
            View.SkmDiscountType = dealProperty.DiscountType;
            View.SkmDiscount = dealProperty.Discount;
            if (Helper.IsFlagSet(pponOrder.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
            {
                var ved = _pponProv.ViewExternalDealGetByBid(guid);
                if (ved.IsLoaded)
                    View.SkmTags = ved.TagList;
            }

            //訂位系統
            View.BookingSystemType = dealProperty.BookingSystemType;
            View.AdvanceReservationDays = dealProperty.AdvanceReservationDays;
            View.CouponUsers = dealProperty.CouponUsers;
            View.IsReserveLock = dealProperty.IsReserveLock;

            View.TmallRmbExchangeRate = dealProperty.TmallRmbExchangeRate;
            View.TmallRmbPrice = dealProperty.TmallRmbPrice;
            View.ZeroActivityShowCoupon = dealProperty.IsZeroActivityShowCoupon;

            View.IsPromotionDeal = dealProperty.IsPromotionDeal;
            View.IsExhibitionDeal = dealProperty.IsExhibitionDeal;

            View.IsCChannel = dealProperty.Cchannel;
            View.CChannelLink = dealProperty.CchannelLink;
            View.IsGame = dealProperty.IsGame;
            View.IsChannelGift = dealProperty.IsChannelGift;
            View.IsWms = dealProperty.IsWms;
            //代銷通路
            View.AgentChannels = dealProperty.AgentChannels;
            //台新特談商品
            View.IsTaishinChosen = dealProperty.IsTaishinChosen;
            
            #endregion DealProperty

            #region DealPayment
            DealPayment dealPayment = _pponProv.DealPaymentGet(View.BusinessHourId);
            if (dealPayment != null)
            {
                View.LimitCreditCardBankId = dealPayment.BankId.HasValue == true ? (LimitBankIdModel)dealPayment.BankId.Value : 0;
                View.IsBankDeal = dealPayment.IsBankDeal;
                View.AllowGuestBuy = dealPayment.AllowGuestBuy;
            }
            else
            {
                View.LimitCreditCardBankId = 0;
            }

            #endregion

            #region 分類

            CategoryDealCollection cdc = _pponProv.CategoryDealsGetList(View.BusinessHourId);
            // 生活商圈
            Dictionary<int, int[]> dataList = new Dictionary<int, int[]>();
            CategoryCollection commercialCategory = _sellerProv.CategoryGetList((int)CategoryType.CommercialCircle);
            foreach (Category cate in commercialCategory.Where(x => x.ParentCode == null))
            {
                int[] cids = commercialCategory.Where(x => x.ParentCode != null && x.ParentCode.Equals(cate.Code)).Select(y => y.Id).ToArray();
                dataList.Add(cate.Id, cdc.Where(x => x.Cid.EqualsAny(cids)).Select(x => x.Cid).ToArray());
            }
            View.SelectedCommercialCategoryId = dataList;

            #region 新版分類
            Dictionary<int, List<int>> selectedChannelCategories = new Dictionary<int, List<int>>();
            Dictionary<int, List<int>> selectedSpecialRegionCategories = new Dictionary<int, List<int>>();
            List<int> selectedChannelSpecialRegionSubCategories = new List<int>();

            foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
            {
                List<int> areaList = new List<int>();
                if (cdc.Any(x => x.Cid == node.CategoryId))
                {
                    List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (subList != null && subList.Count > 0)
                    {
                        foreach (var categoryNode in subList)
                        {
                            if (cdc.Any(x => x.Cid == categoryNode.CategoryId))
                            {
                                areaList.Add(categoryNode.CategoryId);

                                //商圈檢查
                                List<int> subspecial = new List<int>();
                                List<CategoryNode> specialregionList = categoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                                if (specialregionList.Any())
                                {
                                    var fatherCategoryNode = specialregionList.First();
                                    foreach (var specialregion in fatherCategoryNode.GetSubCategoryTypeNode(CategoryType.PponChannelArea))
                                    {
                                        if (cdc.Any(x => x.Cid == specialregion.CategoryId))
                                        {
                                            subspecial.Add(specialregion.CategoryId);

                                            //商圈子區域檢查
                                            //List<int> subSPR = new List<int>();

                                            //List<CategoryNode> subspecialregionList =
                                            //    specialregion.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                                            //if (subspecialregionList.Any())
                                            //{
                                            //    foreach (var sonCategoryNode in subspecialregionList)
                                            //    {
                                            //        if (cdc.Any(x => x.Cid == sonCategoryNode.CategoryId))
                                            //        {
                                            //            subSPR.Add(sonCategoryNode.CategoryId);
                                            //        }
                                            //    }
                                            //}
                                            //if (!SelectedChannelSpecialRegionSubCategories.ContainsKey(specialregion.CategoryId))
                                            //{
                                            //    SelectedChannelSpecialRegionSubCategories.Add(specialregion.CategoryId, subSPR);
                                            //}

                                            //商圈子區域檢查
                                            List<CategoryNode> subspecialregionList = specialregion.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                                            if (subspecialregionList.Any())
                                            {
                                                foreach (var sonCategoryNode in subspecialregionList)
                                                {
                                                    if (cdc.Any(x => x.Cid == sonCategoryNode.CategoryId))
                                                    {
                                                        selectedChannelSpecialRegionSubCategories.Add(sonCategoryNode.CategoryId);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!selectedSpecialRegionCategories.ContainsKey(categoryNode.CategoryId))
                                {
                                    selectedSpecialRegionCategories.Add(categoryNode.CategoryId, subspecial);
                                }
                            }
                        }
                    }
                    if (!selectedChannelCategories.ContainsKey(node.CategoryId))
                    {
                        selectedChannelCategories.Add(node.CategoryId, areaList);
                    }
                }
            }
            //舊有勾選城市的資料
            List<PponCity> oldPponCityList = deal.TimeSlotCollection.Select(x => x.CityId).Distinct().Select(x => PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(x)).ToList();
            //未使用新版的舊版檔次，顯示處理
            if (selectedChannelCategories.Count == 0)
            {
                foreach (CategoryNode node in CategoryManager.PponChannelCategoryTree.CategoryNodes)
                {
                    //有子項目
                    List<CategoryNode> subList = node.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                    if (subList != null && subList.Count > 0)
                    {
                        //旅遊度假需另外處理
                        if (node.CategoryId == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                        {
                            if (oldPponCityList.Any(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId))
                            {
                                //有勾選城市
                                List<int> areaList = new List<int>();
                                //檢查有無小區域的部分
                                foreach (Category c in ViewPponDealManager.DefaultManager.TravelCategories)
                                {
                                    if (cdc.Any(x => x.Cid == c.Id))
                                    {
                                        Category newTravelCategory = CategoryManager.GetPponChannelAreaByTravel(c.Id);
                                        if (newTravelCategory != null)
                                        {
                                            areaList.Add(newTravelCategory.Id);
                                        }
                                    }
                                }
                                selectedChannelCategories.Add(node.CategoryId, areaList);
                            }
                        }
                        else
                        {
                            //有子項目，判斷城市是否有勾選子項目的區塊，若有新增此區塊於新分類顯示
                            List<int> areaList = new List<int>();
                            foreach (var categoryNode in subList)
                            {
                                if (oldPponCityList.Any(x => x.CategoryId == categoryNode.CategoryId))
                                {
                                    areaList.Add(categoryNode.CategoryId);
                                }
                            }
                            if (areaList.Count > 0)
                            {
                                selectedChannelCategories.Add(node.CategoryId, areaList);
                            }
                        }
                    }
                    else
                    {
                        //無子項目，直接判斷主區塊是否選取
                        if (oldPponCityList.Any(x => x.CategoryId == node.CategoryId))
                        {
                            selectedChannelCategories.Add(node.CategoryId, new List<int>());
                        }
                    }
                }
            }

            View.SelectedChannelCategories = selectedChannelCategories;
            View.SelectedChannelSpecialRegionCategories = selectedSpecialRegionCategories;
            View.SelectedChannelSpecialRegionSubCategories = selectedChannelSpecialRegionSubCategories;

            int[] dealCategoryIds = CategoryManager.CategoryGetListByType(CategoryType.DealCategory).Select(x => x.Id).ToArray();
            View.SelectedDealCategories = cdc.Where(x => x.Cid.EqualsAny(dealCategoryIds)).Select(x => x.Cid).ToList();
            View.IsAppLimitedEdition = cdc.Where(x => x.Cid == CategoryManager.Default.AppLimitedEdition.CategoryId).Any();

            #region 顯示SpecialCategory區塊
            int[] specialCategoryIds = CategoryManager.CategoryGetListByType(CategoryType.DealSpecialCategory).Select(x => x.Id).ToArray();
            List<int> selectedSpecialCategoryIds = cdc.Where(x => x.Cid.EqualsAny(specialCategoryIds)).Select(x => x.Cid).ToList();

            //有紀錄是24小時的cityId,新紀錄卻沒有
            /*if ((oldPponCityList.Any(x => x.CityId == PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityId)) &&
                (selectedSpecialCategoryIds.All(x => x != PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CategoryId)))
            {
                //因為母子檔須同步，所以拿掉此判斷，否則資料已同步，畫面還是不同步
                //selectedSpecialCategoryIds.Add(PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CategoryId);
            }
            //有紀錄是72小時的cityId,新紀錄卻沒有
            if ((oldPponCityList.Any(x => x.CityId == PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId)) &&
                (selectedSpecialCategoryIds.All(x => x != PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CategoryId)))
            {
                //因為母子檔須同步，所以拿掉此判斷，否則資料已同步，畫面還是不同步
                //selectedSpecialCategoryIds.Add(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CategoryId);
            }*/

            if (selectedSpecialCategoryIds.Count != 0)
            {
                //如果有設定的Category資料，以CategoryDeal的紀錄為準
                View.SelectedSpecialCategories = selectedSpecialCategoryIds;
            }
            else if (!string.IsNullOrWhiteSpace(dealProperty.LabelIconList))
            {
                //如果沒有CategoryDeal資料，確認有無dealProperty.LabelIconList的舊紀錄，若有以舊有紀錄方式為準
                List<int> labelIconList = new List<int>(dealProperty.LabelIconList.Split(',').Select(int.Parse));
                List<int> categoryIdList = new List<int>();
                foreach (var i in labelIconList)
                {
                    int? cid = CategoryManager.GetCategoryIdByDealLabelSystemCode((DealLabelSystemCode)i);
                    if (cid.HasValue)
                    {
                        categoryIdList.Add(cid.Value);
                    }
                }
                View.SelectedSpecialCategories = categoryIdList;
            }
            #endregion 顯示SpecialCategory區塊

            #endregion 新版分類

            #endregion 分類

            //全家商品類型
            var familyDealType = (cdc.FirstOrDefault(x => x.Cid == FamilyLockCategoryId || x.Cid == FamilyBeaconUnLockCategoryId) != null) ? (int)Core.FamilyDealType.BeaconEvent : (int)Core.FamilyDealType.General;
            View.FamilyDealType = familyDealType;

            if (familyDealType == (int)Core.FamilyDealType.General)
            {
                View.FamilyIsLock = false;
            }
            else
            {
                View.FamilyIsLock = (cdc.FirstOrDefault(x => x.Cid == FamilyLockCategoryId) != null);
            }

            //運費
            var freightCollection = _pponProv.CouponFreightGetList(View.BusinessHourId);
            var freightIncome = freightCollection.Where(x => x.FreightType == (int)CouponFreightType.Income)
                .OrderBy(x => x.StartAmount)
                .Select(freight => new PponSetupViewFreight() { FreightAmount = freight.FreightAmount, StartAmount = freight.StartAmount }).ToList();
            var freightCost = freightCollection.Where(x => x.FreightType == (int)CouponFreightType.Cost)
                .OrderBy(x => x.StartAmount)
                .Select(freight => new PponSetupViewFreight() { FreightAmount = freight.FreightAmount, StartAmount = freight.StartAmount, PayTo = freight.PayTo.HasValue ? Enum.GetName(typeof(CouponFreightPayTo), freight.PayTo.Value) : string.Empty }).ToList();
            View.FreightIncome = freightIncome;
            View.FreightCost = freightCost;

            //進貨價-新
            var costColNew = _pponProv.DealCostGetList(View.BusinessHourId);
            var multiCost = costColNew.Select(freight =>
            new PponSetupViewMultiCost
            {
                Id = freight.Id,
                Cost = freight.Cost,
                Quantity = freight.Quantity,
                Cumulative_quantity = freight.CumulativeQuantity,
                Lower_cumulative_quantity = freight.LowerCumulativeQuantity
            }).ToList();

            View.MultiCost = multiCost;
            View.QuantityMultiplier = dealProperty.QuantityMultiplier;
            View.IsQuantityMultiplier = dealProperty.IsQuantityMultiplier;
            View.IsAveragePrice = dealProperty.IsAveragePrice;

            // 接續檔次設定
            View.Is_Continued_Quantity_Visible = !Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.ComboDealSub); // 子檔不可續接數量
            View.Is_Continued_Sequence_Visible = !Helper.IsFlagSet(pponOrder.BusinessHourStatus, BusinessHourStatus.ComboDealMain); // 母檔不可續接序號
            View.SetSuccessorBid(_pponProv.DealPropertyGetByAncestor(View.BusinessHourId));

            SetupComboDeals(deal);
            SetupProposal();
        }


        /// <summary>
        /// 設定提案單相關資料
        /// </summary>
        /// <param name="isSub"></param>
        private void SetupProposal()
        {
            if (!View.isSub)
            {
                ViewProposalSeller pro = _sellerProv.ViewProposalSellerGet(Proposal.Columns.BusinessHourGuid, View.BusinessHourId);
                if (pro.IsLoaded)
                {
                    if (pro.ProposalSourceType == (int)ProposalSourceType.Original && !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                    {
                        View.JumpTo(PponSetupMode.ProposalUnCheck, pro.BusinessHourGuid.ToString());
                    }
                    else
                    {
                        View.SetProposalImport(pro);
                        View.SetSendPageConfirmMailButton(pro);
                        View.GetProposalData(pro);
                    }
                }
            }
            else
            {
                //子檔要先撈母檔對應的提案單
                Guid? mainBid = _pponProv.GetComboDeal(View.BusinessHourId).MainBusinessHourGuid;
                if (mainBid != null)
                {
                    ViewProposalSeller pro2 = _sellerProv.ViewProposalSellerGet(Proposal.Columns.BusinessHourGuid, mainBid);
                    if (pro2.IsLoaded)
                    {
                        View.GetProposalData(pro2);
                    }

                }
            }
        }

        private void SetupSelectableChannel()
        {
            List<ViewCategoryDependency> allViewCategoryDependencies = _sellerProv.ViewCategoryDependencyGetAll(orderBy: null).ToList();
            View.SetSelectableChannel(CategoryManager.PponChannelCategoryTree, allViewCategoryDependencies);
        }

        private void SetupSelectableCommercialCategories()
        {
            Dictionary<Category, List<Category>> dataList = new Dictionary<Category, List<Category>>();
            CategoryCollection categories = _sellerProv.CategoryGetList((int)CategoryType.CommercialCircle);
            foreach (Category item in categories.Where(x => x.ParentCode == null))
            {
                dataList.Add(item, categories.Where(x => x.ParentCode.Equals(item.Code)).ToList());
            }
            View.SetSelectableCommercialCategory(dataList);
        }

        private void SetupDealLabels()
        {
            View.SetDealLabel(_sellerProv.GetDealLabelCollection());
        }

        private void SetupGroupCouponAppStyleCategory()
        {
            Dictionary<int, string> list = new Dictionary<int, string>();
            list.Add(CategoryManager.Default.PponDeal.CategoryId, CategoryManager.Default.PponDeal.CategoryName);
            list.Add(CategoryManager.Default.Delivery.CategoryId, CategoryManager.Default.Delivery.CategoryName);
            list.Add(CategoryManager.Default.Travel.CategoryId, CategoryManager.Default.Travel.CategoryName);
            list.Add(CategoryManager.Default.Beauty.CategoryId, CategoryManager.Default.Beauty.CategoryName);
            list.Add(CategoryManager.Default.Family.CategoryId, CategoryManager.Default.Family.CategoryName);

            View.SetGroupCouponAppStyleList(list);
        }

        private void SetupDefaultIconSystemCode()
        {
            //固定顯示破千與破萬
            List<KeyValuePair<int, string>> codeList = new List<KeyValuePair<int, string>>
                {
                    new KeyValuePair<int, string>((int) DealLabelSystemCode.SellOverOneThousand,
                                                  SystemCodeManager.GetDealLabelCodeName(
                                                      (int) DealLabelSystemCode.SellOverOneThousand)),
                    new KeyValuePair<int, string>((int) DealLabelSystemCode.SoldEndAfterOneDay,
                                                  SystemCodeManager.GetDealLabelCodeName(
                                                      (int) DealLabelSystemCode.SoldEndAfterOneDay)),
                    new KeyValuePair<int, string>((int) DealLabelSystemCode.AppLimitedEdition,
                                                  SystemCodeManager.GetDealLabelCodeName(
                                                      (int) DealLabelSystemCode.AppLimitedEdition)),
                    new KeyValuePair<int, string>((int) DealLabelSystemCode.GroupCoupon,
                                                  SystemCodeManager.GetDealLabelCodeName(
                                                      (int) DealLabelSystemCode.GroupCoupon)),
                    new KeyValuePair<int, string>((int) DealLabelSystemCode.TwentyFourHoursArrival,
                                              SystemCodeManager.GetDealLabelCodeName(
                                                  (int) DealLabelSystemCode.TwentyFourHoursArrival))
                };
            View.SetDefaultIconSystemCode(codeList);
        }

        private void SetupSelectableDealSpecialCategory()
        {
            //View.TheDeliveryType = DeliveryType.ToShop;
            View.SetSelectableDealSpecialCategory(CategoryManager.CategoryGetListByType(CategoryType.DealSpecialCategory));
        }

        private void SetupUnhiddenDealSpecialCategory()
        {
            Category mainCategory = CategoryManager.DeliveryCategeryGetByDeliveryType(View.TheDeliveryType);
            List<Category> specialCategoryList = new List<Category>();
            CategoryNode node = CategoryManager.DeliveryTypeCategoryTree.CategoryNodes.FirstOrDefault(
                    x => x.CategoryId == mainCategory.Id);
            if (node != default(CategoryNode))
            {
                List<CategoryNode> iconList = node.GetSubCategoryTypeNode(CategoryType.DealSpecialCategory);
                if (iconList != null && iconList.Count > 0)
                {
                    specialCategoryList = iconList.Select(x => x.GetMainCategory()).ToList();
                }
            }
            View.SetUnhiddenDealSpecialCategory(mainCategory, specialCategoryList);
        }

        private void RemovePhotoFromDisk(string rawPath)
        {
            if (string.IsNullOrEmpty(rawPath) || rawPath.IndexOf(',') < 0)
            {
                return;
            }

            string[] filePath = rawPath.Split(',');
            ImageUtility.DeleteFile(UploadFileType.PponEvent, filePath[0], filePath[1]);
        }

        private void SetInitStores(Guid sellerGuid)
        {
            View.SetInitStores(SellerFacade.GetSellerTreeSellers(sellerGuid));
        }

        private void SetupSellerTreeView(Guid sellerGuid, bool enabledSelf)
        {
            View.SetSellerTreeView(SellerFacade.GetSellerTreeNode(sellerGuid, enabledSelf));
        }

        private void SetupShoppingCartOption()
        {
            if (!_config.ShoppingCartV2Enabled)
            {
                return;
            }

            string defaultFreightsName = "免運";
            string displayFreightsName = "{0} (運費{1})";
            string selectValue = "new";
            List<KeyValuePair<string, string>> listitem = new List<KeyValuePair<string, string>>();

            var freight = _sp.ShoppingCartFreightsGetBySellerGuid(new List<Guid>() { View.SellerGuid }).ToList();

            //是否有系統預設免運的購物車 沒有的話就加入一筆
            if (freight.Any(x => (x.FreightsName == defaultFreightsName && x.NoFreightLimit == 0 && x.Freights == 0)))
            {
                //系統免運購物車是否有審核 沒有就審核
                ShoppingCartFreight scf = freight.FirstOrDefault(x => (x.FreightsName == defaultFreightsName && x.NoFreightLimit == 0 && x.Freights == 0));
                if (scf.FreightsStatus != (int)ShoppingCartFreightStatus.Approve)
                {
                    scf.FreightsStatus = (int)ShoppingCartFreightStatus.Approve;
                    _sp.ShoppingCartFreightsSet(scf);
                    SellerFacade.ShoppingCartFreightsLogSet(View.UserName, scf.Id, string.Format("【購物車審核確認】{0}", scf.Id), ShoppingCartFreightLogType.System);
                }
            }
            else
            {
                listitem.Add(new KeyValuePair<string, string>(string.Format(displayFreightsName, defaultFreightsName, 0), string.Empty));
                selectValue = View.TheDeliveryType.Equals(DeliveryType.ToShop) ? string.Empty : selectValue;
            }

            var scfItem = _sp.ShoppingCartFreightsItemGetByBid(View.BusinessHourId);
            //開始加入購物車項目
            foreach (var row in freight.Where(x => x.FreightsStatus == (int)ShoppingCartFreightStatus.Approve).OrderBy(x => x.Id))
            {
                //憑證
                if (View.TheDeliveryType.Equals(DeliveryType.ToShop) && row.FreightsName == defaultFreightsName && row.NoFreightLimit == 0 && row.Freights == 0)
                {
                    listitem.Add(new KeyValuePair<string, string>(string.Format(displayFreightsName, row.FreightsName, row.Freights), Convert.ToString(row.Id)));
                    selectValue = Convert.ToString(row.Id);
                    break;
                }

                //宅配
                if (View.TheDeliveryType.Equals(DeliveryType.ToHouse))
                {
                    listitem.Add(new KeyValuePair<string, string>(string.Format(displayFreightsName, row.FreightsName, row.Freights), Convert.ToString(row.Id)));
                    selectValue = ((selectValue == "new") && (row.Id == scfItem.FreightsId)) ? Convert.ToString(row.Id) : selectValue;
                }
            }

            View.SetShoppingCartOption(listitem, selectValue);
        }

        private void UpdateShoppingCart(Guid businessHourGuid, int? viewFreightsId)
        {
            int businessHourUniqueId = _pponProv.DealPropertyGet(businessHourGuid).UniqueId;
            ShoppingCartFreightsItem item = _sp.ShoppingCartFreightsItemGetByBid(businessHourGuid);

            #region 刪除商品
            if (item.IsLoaded)
            {
                if (_sp.ShoppingCartFreightsItemDelete(new List<Guid>() { businessHourGuid }))
                {
                    SellerFacade.ShoppingCartFreightsLogSet(View.UserName, item.FreightsId, string.Format("【刪除商品】：{0}", businessHourUniqueId), ShoppingCartFreightLogType.System);
                }
            }
            #endregion

            ShoppingCartFreight scf = new ShoppingCartFreight();

            #region 是否完全無購物車 沒有的話新增一筆免運
            if (viewFreightsId == null)
            {
                string uniqueidSeq = "001";
                Seller seller = _sp.SellerGet(View.SellerGuid);
                string sellerid = seller.SellerId.Replace("-", "");
                List<ShoppingCartFreight> scflist = _sp.ShoppingCartFreightsGetBySellerGuid(new List<Guid>() { View.SellerGuid }).ToList();
                if (scflist.Count() > 0)
                {
                    uniqueidSeq = Convert.ToString(Convert.ToInt32(scflist.Max(x => x.UniqueId).Replace(sellerid, "")) + 1).PadLeft(3, '0');
                }

                scf.UniqueId = sellerid + uniqueidSeq;
                scf.SellerGuid = View.SellerGuid;
                scf.FreightsStatus = (int)ShoppingCartFreightStatus.Approve;
                scf.FreightsName = "免運";
                scf.NoFreightLimit = 0;
                scf.Freights = 0;
                scf.CreateTime = DateTime.Now;
                scf.CreateUser = "sys";

                _sp.ShoppingCartFreightsSet(scf);
                SellerFacade.ShoppingCartFreightsLogSet(View.UserName, scf.Id, string.Format("【新增購物車】{0}", scf.Id), ShoppingCartFreightLogType.System);
            }
            #endregion

            int freightsId = viewFreightsId == null ? scf.Id : (int)viewFreightsId;

            #region 更新套用商品
            item = new ShoppingCartFreightsItem();
            item.FreightsId = freightsId;
            item.ItemStatus = (int)ShoppingCartFreightStatus.Approve;
            item.UniqueId = businessHourUniqueId;
            item.BusinessHourGuid = businessHourGuid;
            item.CreateTime = DateTime.Now;
            item.CreateUser = View.UserName;

            _sp.ShoppingCartFreightsItemSetBulkInsert(new List<ShoppingCartFreightsItem>() { item });
            SellerFacade.ShoppingCartFreightsLogSet(View.UserName, freightsId, string.Format("【套用商品】：{0}", businessHourUniqueId), ShoppingCartFreightLogType.User);
            #endregion

        }

        #region DealTimeSlot 相關

        /// <summary>
        /// 處理第一筆，如果起始時間不是12點，先寫一筆將其調整到12點
        /// </summary>
        /// <param name="effectiveStartDay"></param>
        /// <param name="dealGuid"></param>
        /// <param name="cityId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        private Tuple<DateTime, DealTimeSlot> AdjustFirstEffectiveStartDay(DateTime effectiveStartDay, DateTime effectiveEndDay, Guid dealGuid, int cityId, DealTimeSlotStatus status = DealTimeSlotStatus.Default)
        {
            DateTime tempEndDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;

            if (effectiveStartDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveStartDay.Hour < 12) //未過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon();

                }
                else //已過中午12點
                {
                    tempEndDay = effectiveStartDay.Noon().AddDays(1);
                }
            }

            //首筆DealTimeSlot截止時間若已大於檔次截止時間則直接修改為截止時間
            if (tempEndDay > effectiveEndDay)
            {
                tempEndDay = effectiveEndDay;
            }

            //int newSeq = PponFacade.GetDealTimeSlotNonLockSeq(cityId, effectiveStartDay, vpdts);
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                         999999, //★
                                          effectiveStartDay, tempEndDay,
                                          status);

            return Tuple.Create(tempEndDay, insDeal);
        }

        /// <summary>
        /// 處理最後一筆，如果起始時間不是12點，先寫一筆將其調整到12點（暫時不用）
        /// </summary>
        /// <param name="effectiveEndDay"></param>
        /// <param name="dealGuid"></param>
        /// <param name="cityId"></param>
        /// <param name="seq"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        private Tuple<DateTime, DealTimeSlot> AdjustLastEffectiveEndDay(DateTime effectiveEndDay, Guid dealGuid, int cityId, int seq = 1, DealTimeSlotStatus status = DealTimeSlotStatus.Default)
        {
            DateTime tempStartDay = DateTime.MinValue;
            DealTimeSlot insDeal = null;
            if (effectiveEndDay.ToString("HH:mm") != "12:00")
            {
                if (effectiveEndDay.Hour < 12) //未過中午12點
                {
                    tempStartDay = effectiveEndDay.AddDays(-1).Noon();

                }
                else //已過中午12點
                {
                    tempStartDay = effectiveEndDay.Noon();
                }
            }
            insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                          1,
                                          tempStartDay, effectiveEndDay,
                                          status);

            return Tuple.Create(tempStartDay, insDeal);
        }

        /// <summary>
        /// 迴圈新增DealTimeSlot
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert">要寫入的dealTimeSlot</param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDate"></param>
        /// <param name="cityId"></param>
        /// <param name="calNewSeq"></param>
        private void AddDealTimeSlotLoop(PponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, DateTime dateStartDay, DateTime dateEndDate, int cityId, bool calNewSeq = false)
        {
            Guid dealGuid = entity.Deal.Guid;
            //檢查第一筆
            bool isFirst = true;


            DealTimeSlotStatus timeSlotStatus = View.IsBankDeal ? DealTimeSlotStatus.NotShowInPponDefault : DealTimeSlotStatus.Default;
            if (timeSlotStatus == DealTimeSlotStatus.Default && entity.TimeSlotCollection.Any())
            {
                //直接抓最後一筆的狀態
                var last = entity.TimeSlotCollection.Where(x => x.CityId == cityId).OrderBy(y => y.EffectiveEnd).LastOrDefault();
                if (last != null && last.Status == (int)DealTimeSlotStatus.NotShowInPponDefault)
                {
                    timeSlotStatus = DealTimeSlotStatus.NotShowInPponDefault;
                }
            }

            //EffectiveStart是Key值
            for (var date = dateStartDay; date < dateEndDate; date = date.AddDays(1))
            {
                //檢查資料是否已經存在
                bool anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);

                //取得該筆舊資料
                DealTimeSlot dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();
                DealTimeSlot insDeal = null;
                bool isFirstInserted = false;

                #region 第一筆

                if (isFirst) //針對第一筆的時間做調整
                {
                    //一開始排程時間設為DEAL起始日
                    Tuple<DateTime, DealTimeSlot> tuple = AdjustFirstEffectiveStartDay(date, dateEndDate, dealGuid, cityId, timeSlotStatus);
                    if (tuple.Item1 != DateTime.MinValue) //調整DEAL起始時間
                    {
                        if (!anyExist)
                        {
                            dealTimeSlotColForInert.Add(tuple.Item2);
                        }
                        else
                        {
                            //檔次已存在，維持舊的排序
                            dealTimeSlotColForInert.Add(GetNewDealTimeSlot(dts, dateEndDate));
                        }

                        //用調整後的時間做新的起始點
                        date = tuple.Item1;
                        isFirstInserted = date == dateEndDate;
                        //檢查調整後的時間資料是否已經存在
                        anyExist = entity.TimeSlotCollection == null ? false : entity.TimeSlotCollection.Any(t => t.EffectiveStart == date && t.CityId == cityId);//★這兩行是多餘的?
                        dts = !anyExist ? null : entity.TimeSlotCollection.Where(t => t.EffectiveStart == date && t.CityId == cityId).FirstOrDefault();//★這兩行是多餘的?
                    }

                    //一定要把isFirst關掉
                    isFirst = false;
                }

                if (isFirstInserted)
                {
                    continue;
                }

                #endregion

                #region 最後一筆

                if (date.AddDays(1) > dateEndDate)
                {
                    int lastSeq = 999999;

                    if (!anyExist)
                    {
                    }
                    else
                    {
                        //檔次已存在，維持舊的排序
                        lastSeq = dts.Sequence;
                    }
                    DealTimeSlot lastDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                      lastSeq,
                                                      date, dateEndDate,
                                                      timeSlotStatus);


                    dealTimeSlotColForInert.Add(lastDeal);
                    continue;
                }

                #endregion

                #region  其他筆

                if (!anyExist)
                {
                    //檔次不存在
                    int newSeq = 999999;

                    //新增 此城市這個時間一筆排程記錄
                    insDeal = GetNewDealTimeSlot(dealGuid, cityId,
                                                        newSeq,
                                                        date, date.AddDays(1),
                                                        timeSlotStatus);
                }
                else
                {
                    //檔次已存在，維持舊的排序
                   insDeal = GetNewDealTimeSlot(dts, dateEndDate);
                }

                #endregion

                if (!dealTimeSlotColForInert.Any(t => t.EffectiveStart == date && t.CityId == cityId))
                {
                    dealTimeSlotColForInert.Add(insDeal);
                }


                //      在2013/12/10現改成當新的slot增加時，順序放在最後面時，就不用+1
                //_pponProv.DealTimeSlotUpdateSeqAddOne(cityId, effectiveStartDay);
                //////////////////////////////////////////////
            }
        }

        /// <summary>
        /// 檢查DealTimeSlot是否在時間間隔內
        /// </summary>
        /// <param name="editDeal"></param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDay"></param>
        /// <returns></returns>
        private bool InIntervalDate(DealTimeSlot editDeal, DateTime dateStartDay, DateTime dateEndDay)
        {
            return (editDeal.EffectiveStart > dateStartDay) && (editDeal.EffectiveEnd < dateEndDay);
        }

        /// <summary>
        /// 逐筆檢察看資料是否位於新的deal起始與截止時間內，且紀錄的程式是有勾選的程式
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="dealTimeSlotColForInert"></param>
        /// <param name="cities"></param>
        /// <param name="dateStartDay"></param>
        /// <param name="dateEndDay"></param>
        //private void CheckTimeSlotLoop(PponDeal entity, DealTimeSlotCollection dealTimeSlotColForInert, int[] cities, DateTime dateStartDay, DateTime dateEndDay)
        //{
        //    //逐筆檢察看資料是否位於新的deal起始與截止時間內，且紀錄的程式是有勾選的程式
        //    for (int i = entity.TimeSlotCollection.Count - 1; i >= 0; i--)
        //    {
        //        #region 逐筆檢察看資料是否位於新的deal起始與截止時間內，且紀錄的程式是有勾選的程式

        //        DealTimeSlot editDeal = entity.TimeSlotCollection[i];
        //        //原紀錄城市不包含於新勾選項目內
        //        if (!cities.Any(x => x == editDeal.CityId))
        //        {
        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        //檢查 起始日與截止日都不再範圍內
        //        if ((editDeal.EffectiveEnd < dateStartDay) || (editDeal.EffectiveStart > dateEndDay))
        //        {
        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        //DEAL為完整的一天且起始日與截止日皆在範圍內，不需修正直接離開
        //        TimeSpan span = editDeal.EffectiveEnd.Subtract(editDeal.EffectiveStart);

        //        bool isInIntervalDate = InIntervalDate(editDeal, dateStartDay, dateEndDay);

        //        //if ((span.Hours >= 24) && isInIntervalDate) //完整的一天
        //        if ((span.TotalHours >= 24) && isInIntervalDate) //完整的一天
        //        {
        //            continue;
        //        }

        //        //如果時間區隔小於24小時，且Deal起始日晚於PPon的起始日，且Deal結束日早於PPon結束日。（落在PPon時間間隔中）
        //        //if ((span.Hours < 24) && isInIntervalDate)
        //        if ((span.TotalHours < 24) && isInIntervalDate)
        //        {
        //            DateTime tmpEndDate;//暫存結束日
        //            if (editDeal.EffectiveStart.Hour < 12) //起始日未過中午12點，暫存結束日設為當天中午12點
        //            {
        //                tmpEndDate = editDeal.EffectiveStart.Noon();
        //            }
        //            else //起始日已過中午12點，暫存結束日設為隔天中午12點
        //            {
        //                tmpEndDate = editDeal.EffectiveStart.Noon().AddDays(1);
        //            }
        //            DateTime tmpStartDate;//暫存起始日
        //            if ((editDeal.EffectiveEnd.Hour <= 11) ||
        //                (editDeal.EffectiveEnd.Hour == 12 && editDeal.EffectiveEnd.Minute == 0)) //結束日未過中午12點，暫存起始日設為前天中午12點
        //            {
        //                tmpStartDate = editDeal.EffectiveEnd.Noon().AddDays(-1);
        //            }
        //            else //結束日已過中午12點，暫存起始日設為當天中午12點
        //            {
        //                tmpStartDate = editDeal.EffectiveEnd.Noon();
        //            }

        //            DealTimeSlot insDeal = GetNewDealTimeSlot(editDeal);//製作副本
        //            insDeal.IsInTurn = false;
        //            insDeal.EffectiveStart = tmpStartDate;
        //            insDeal.EffectiveEnd = tmpEndDate;

        //            dealTimeSlotColForInert.Add(insDeal);

        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        //檢查起始日與截止日範圍大於 新的deal起始日與截止日 為什麼要調整起始和結束日？而不是直接移除？editDeal只有一天
        //        if ((editDeal.EffectiveStart < dateStartDay) && (editDeal.EffectiveEnd > dateEndDay))
        //        {
        //            //EffectiveStart為KEY值，不能直接修改，須先新增一筆再刪除舊資料
        //            DealTimeSlot insDeal = GetNewDealTimeSlot(editDeal);
        //            insDeal.EffectiveStart = dateStartDay;
        //            insDeal.EffectiveEnd = dateEndDay;
        //            dealTimeSlotColForInert.Add(insDeal);

        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        //處理第一筆
        //        //起始資料早於設定的起始日
        //        if (editDeal.EffectiveStart <= dateStartDay)
        //        {
        //            DateTime tmpEndDate;
        //            if (editDeal.EffectiveStart.Hour < 12) //未過中午12點
        //            {
        //                tmpEndDate = editDeal.EffectiveStart.Noon();
        //            }
        //            else //已過中午12點
        //            {
        //                tmpEndDate = editDeal.EffectiveStart.Noon().AddDays(1);
        //            }
        //            //EffectiveStart為KEY值，不能直接修改，須先新增一筆再刪除舊資料
        //            DealTimeSlot insDeal = GetNewDealTimeSlot(editDeal);

        //            insDeal.EffectiveStart = dateStartDay;

        //            if (dateEndDay > tmpEndDate)
        //            {
        //                insDeal.EffectiveEnd = tmpEndDate;
        //            }
        //            else
        //            {
        //                insDeal.EffectiveEnd = dateEndDay;
        //            }

        //            if (insDeal.EffectiveStart != insDeal.EffectiveEnd)
        //            {
        //                dealTimeSlotColForInert.Add(insDeal);
        //            }

        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        //處理最後一筆
        //        //截止的資料晚於設定的結束日
        //        if (editDeal.EffectiveEnd >= dateEndDay)
        //        {
        //            DateTime tmpStartDate;
        //            if ((editDeal.EffectiveEnd.Hour <= 11) ||
        //                (editDeal.EffectiveEnd.Hour == 12 && editDeal.EffectiveEnd.Minute == 0)) //未過中午12點
        //            {
        //                tmpStartDate = editDeal.EffectiveEnd.Noon().AddDays(-1);
        //            }
        //            else //已過中午12點
        //            {
        //                tmpStartDate = editDeal.EffectiveEnd.Noon();
        //            }
        //            //EffectiveStart為KEY值，不能直接修改，須先新增一筆再刪除舊資料
        //            DealTimeSlot insDeal = GetNewDealTimeSlot(editDeal);
        //            insDeal.IsInTurn = false;
        //            if (dateStartDay < tmpStartDate)
        //            {
        //                insDeal.EffectiveStart = tmpStartDate;
        //            }
        //            else
        //            {
        //                insDeal.EffectiveStart = dateStartDay;
        //            }

        //            insDeal.EffectiveEnd = dateEndDay;

        //            if (insDeal.EffectiveStart != insDeal.EffectiveEnd)
        //            {
        //                dealTimeSlotColForInert.Add(insDeal);
        //            }
        //            entity.TimeSlotCollection.RemoveAt(i);
        //            continue;
        //        }

        //        #endregion 逐筆檢察看資料是否位於新的deal起始與截止時間內，且紀錄的程式是有勾選的程式
        //    }
        //}


        /// <summary>
        /// DealTimeSlot重構的方法
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="cities"></param>
        /// <param name="orderStartDateChange"></param>
        /// <param name="processBuilder"></param>
        private void DealTimeSlotInsert(PponDeal entity, int[] cities, bool orderStartDateChange, StringBuilder processBuilder)
        {
            //**********寫入DealTimeSlot的邏輯 Begin**********
            #region 產生DealTimeSlot排程資料, it has a bug on removing items

            DateTime dateStartDay = entity.Deal.BusinessHourOrderTimeS;
            DateTime dateEndDay = entity.Deal.BusinessHourOrderTimeE;
            DealTimeSlotCollection dealTimeSlotColForInert = new DealTimeSlotCollection();

            //檢查目前是否已有排程資料，若沒有依據輸入的啟始與截止日期自動產生
            if (entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count == 0)
            {
                #region 依據輸入的啟始與截止日期自動產生
                //依據On檔的時間與地區產生DealTimeSlot的資料(檔期排程資料)
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId);
                }

                if (dealTimeSlotColForInert.Count > 0)
                {
                    entity.TimeSlotCollection = dealTimeSlotColForInert;
                }
                #endregion 依據輸入的啟始與截止日期自動產生
            }
            else //若已有排程資料則需判斷是否需要修改
            {
                #region 若已有排程資料則需判斷是否需要修改

                //todo:改寫這塊邏輯，應該是不需要檢查，只要取得舊有的seq。反正最後都是全部砍掉重練。

                #region 逐筆檢查舊資料（邏輯需要再檢查）
                //CheckTimeSlotLoop(entity, dealTimeSlotColForInert, cities, dateStartDay, dateEndDay);
                #endregion 逐筆檢查舊資料

                //entity.TimeSlotCollection.AddRange(dealTimeSlotColForInert);

                //填入新增的資料

                #region 將不足的資料補上
                //將不足的資料補上
                //一開始排程時間設為DEAL起始日
                //依序新增勾選區域的排程記錄
                foreach (int cityId in cities)
                {
                    //只要排程時間小於DEAL結束日就需增加一筆排程記錄
                    AddDealTimeSlotLoop(entity, dealTimeSlotColForInert, dateStartDay, dateEndDay, cityId, true);
                }
                #endregion 將不足的資料補上

                entity.TimeSlotCollection = dealTimeSlotColForInert;


                #endregion
            }

            if ((entity.TimeSlotCollection != null) && (orderStartDateChange))
            {
                // 預設前24小時輪播  add by Max 2011/4/21
                foreach (DealTimeSlot dts in entity.TimeSlotCollection.Where(x => DateTime.Compare(entity.Deal.BusinessHourOrderTimeS.AddDays(1), x.EffectiveStart) > 0))
                {
                    dts.IsInTurn = true;
                }
            }
            //天貓檔次設定不顯示
            if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                foreach (var item in entity.TimeSlotCollection)
                {
                    item.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                }
                // _pponProv.DealTimeSlotUpdateStatus(entity.Deal.Guid, DealTimeSlotStatus.NotShowInPponDefault);
            }


            //=====================================================

            #endregion 產生DealTimeSlot排程資料, it has a bug on removing items
            //**********寫入DealTimeSlot的邏輯 End**********


        }


        /// <summary>
        /// Copy DealTimeSlot重構
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="selectedCity"></param>
        /// <returns></returns>
        private CategoryDealCollection DealTimeSlotCopy(PponDeal entity, List<int> selectedCity)
        {
            #region 首頁類別 copy PponDeal Category Deal data

            CategoryDealCollection cds = new CategoryDealCollection();

            #region 新版的頻道、區域、分類之處理
            Dictionary<int, List<int>> selectedChannelCategories = View.SelectedChannelCategories;
            //儲存依據新版頻道及區域 對應的 舊版cityId
            //List<int> selectedCity = new List<int>();
            foreach (var selectedChannel in selectedChannelCategories)
            {
                PponCity city = PponCityGroup.GetPponCityByCategoryId(selectedChannel.Key);
                if (city != null)
                {
                    selectedCity.Add(city.CityId);
                }

                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = selectedChannel.Key });
                foreach (var areaId in selectedChannel.Value)
                {
                    city = PponCityGroup.GetPponCityByCategoryId(areaId);
                    if (city != null)
                    {
                        selectedCity.Add(city.CityId);
                    }

                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = areaId });
                }
            }

            List<int> selectedDealCategories = View.SelectedDealCategories;
            foreach (var dealCategoryId in selectedDealCategories)
            {
                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = dealCategoryId });
            }
            //取貨方式區塊
            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = View.SelectedDeliveryCategoryId });
            //檔次的特殊類別(ICON區塊)
            List<int> selectedSpecialCategories = View.SelectedSpecialCategories;
            foreach (var specialCategoryId in selectedSpecialCategories)
            {
                //特殊區塊有24與72小時的須設定，所以也要查詢對應的cityId
                PponCity city = PponCityGroup.GetPponCityByCategoryId(specialCategoryId);
                if (city != null)
                {
                    selectedCity.Add(city.CityId);
                }

                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = specialCategoryId });
            }
            #endregion 新版的頻道、區域、分類之處理

            foreach (KeyValuePair<int, int[]> categoryList in View.SelectedCommercialCategoryId)
            {
                foreach (int categoryId in categoryList.Value)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = categoryId });
                }
            }

            #endregion 首頁類別 copy PponDeal Category Deal data

            return cds;
        }


        private DealTimeSlot GetNewDealTimeSlot(Guid bid, int cityId, int seq, DateTime startDate, DateTime endDate, DealTimeSlotStatus status, bool isInTurn = false)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = bid;
            newDeal.CityId = cityId;
            newDeal.Sequence = seq;
            newDeal.EffectiveStart = startDate;
            newDeal.EffectiveEnd = endDate;
            newDeal.Status = (int)status;
            newDeal.IsInTurn = isInTurn;

            return newDeal;
        }

        /// <summary>
        /// 傳回新建的DealTimeSlot物件，DataTable欄位的值與傳入的DealTimeSlot相同
        /// </summary>
        /// <param name="fromData"></param>
        /// <param name="dateEndDate"></param>
        /// <returns></returns>
        private DealTimeSlot GetNewDealTimeSlot(DealTimeSlot fromData, DateTime dateEndDate)
        {
            //新增 此城市這個時間一筆排程記錄
            DealTimeSlot newDeal = new DealTimeSlot();
            newDeal.BusinessHourGuid = fromData.BusinessHourGuid;
            newDeal.CityId = fromData.CityId;
            newDeal.Sequence = fromData.Sequence;
            newDeal.EffectiveStart = fromData.EffectiveStart;

            //fix 手動結檔再開檔，fromData.EffectiveEnd.Noon()同日問題
            if (newDeal.EffectiveStart.Date.Equals(fromData.EffectiveEnd.Date) && newDeal.EffectiveStart >= fromData.EffectiveEnd.Noon())
            {
                //原最後一筆又起訖同一天,繼續拉長
                newDeal.EffectiveEnd = fromData.EffectiveEnd.AddDays(1).Noon();
            }
            else if (fromData.EffectiveEnd < fromData.EffectiveEnd.Noon() && fromData.EffectiveEnd < dateEndDate)
            {
                //0600 繼續拉長(非最後一筆)
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            else if (fromData.EffectiveEnd <= fromData.EffectiveEnd.Noon() && fromData.EffectiveEnd > dateEndDate)
            {
                //1200 只剩第一筆又要縮短
                newDeal.EffectiveEnd = dateEndDate;
            }
            else
            {
                //一般的1200
                newDeal.EffectiveEnd = fromData.EffectiveEnd.Noon();
            }
            //日期銜接為12點，所以直接設定12點

            newDeal.Status = fromData.Status;
            newDeal.IsInTurn = fromData.IsInTurn;
            newDeal.IsLockSeq = fromData.IsLockSeq; //★本次新增

            return newDeal;
        }

        #endregion

        #region event handlers
        private string DateTimeTicks(DateTime times, out DateTime lastTimes)
        {
            lastTimes = DateTime.Now;
            return (DateTime.Now - times).TotalSeconds.ToString() + "秒<br/>";
        }
        private void OnSaveDeal(object sender, DataEventArgs<PponSetupViewModel> e)
        {
            Guid? mainBid = _pponProv.GetComboDeal(View.BusinessHourId).MainBusinessHourGuid;
            Proposal pro = _sellerProv.ProposalGet(mainBid ?? View.BusinessHourId);

            PponDeal data = e.Data.Deal;
            PponDeal entity;
            bool orderStartDateChange = true;//好康開檔日是否有異動。
            bool isNew = false;
            var insStores = new PponStoreCollection();
            var delStores = new PponStoreCollection();
            DateTime lastTimes = DateTime.Now;
            StringBuilder processBuilder = new StringBuilder();
            processBuilder.AppendLine("OnSaveDeal(Begin) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");
            string sellerName = _sellerProv.SellerGet(View.SellerGuid).SellerName;
            AccessoryGroupCollection newGroups = null;

            PchomeChannelProd pchomeProd = new PchomeChannelProd();
            bool pchomeSyncPrice = false;
            bool pchomeSyncDesc = false;
            bool pchomeSyncEG = false;
            bool pchomeSyncProd = false;
            bool pchomeSyncQty = false;
            bool pchomeSyncPic = false;

            string checkErrorMsg;
            string tempErrMsg = "";
            if (!CheckRequired(out checkErrorMsg))
            {
                tempErrMsg += checkErrorMsg + "\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            string checkInputErrorMsg;
            if (!CheckInput(e.Data, out checkInputErrorMsg))
            {
                tempErrMsg += checkInputErrorMsg + "\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }
            else if (!string.IsNullOrEmpty(checkInputErrorMsg))
            {
                tempErrMsg += checkInputErrorMsg + "\\n";
            }

            if (!ValidateLengthAccessoryGroupText(e.Data.AccessoryString))
            {

                tempErrMsg += _config.IsEnableVbsNewShipFile ? "多重選項格式有誤(貨號+名稱超過150個字)，請檢查!!\\n" : "多重選項格式有誤(名稱超過50個字)，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.IsPromotionDeal && View.IsExhibitionDeal)
            {
                tempErrMsg += "【展演類型檔次】及【展覽類型檔次】只能擇一勾選，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.NoRefundBeforeDays && View.ExpireNoRefund)
            {
                tempErrMsg += "【演出時間前十日內不能退貨】及【過期不能退貨】只能擇一勾選，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.IsPromotionDeal && !View.NoRefundBeforeDays)
            {
                tempErrMsg += "您勾選了【展演類型檔次】，請同時勾選【演出時間前十日內不能退貨】，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.IsExhibitionDeal && !View.ExpireNoRefund)
            {
                tempErrMsg += "您勾選了【展覽類型檔次】請同時勾選【過期不能退貨】，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if ((View.IsExhibitionDeal || View.IsPromotionDeal) && View.AccBusinessGroupNo == (int)AccBusinessGroupNo.Delivery)
            {
                tempErrMsg += "館別為【宅配】時，不得勾選【展演類型檔次】及【展覽類型檔次】，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }
            if ((View.IsExhibitionDeal || View.IsPromotionDeal) && View.TheDeliveryType == DeliveryType.ToHouse)
            {
                tempErrMsg += "宅配檔次不得勾選【展演類型檔次】及【展覽類型檔次】，請檢查!!\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            var comboDeals = _pponProv.GetViewComboDealAllByBid(View.BusinessHourId);

            if (View.BusinessHourId != Guid.Empty) // edit mode
            {
                #region edit mode

                processBuilder.AppendLine("edit mode = " + DateTimeTicks(lastTimes, out lastTimes));

                // 檢查檔次開檔時間是否重疊
                /*DealPropertyCollection dpc = _pponProv.DealPropertyGetByAncestor(View.BusinessHourId);
                PponDeal pd;
                foreach (DealProperty dp in dpc)
                {
                    pd = _pponProv.PponDealGet(dp.BusinessHourGuid);
                    if (DateTime.Compare(pd.Deal.BusinessHourOrderTimeS, data.Deal.BusinessHourOrderTimeE) < 0)
                    {
                        //View.ShowMessage("接續檔次開檔日期早於此檔結檔日期。");
                        //return;
                    }
                }*/

                processBuilder.AppendLine("檢查檔次開檔時間 = " + DateTimeTicks(lastTimes, out lastTimes));

                entity = _pponProv.PponDealGet(View.BusinessHourId);
                if (data.Deal.BusinessHourOrderTimeS == entity.Deal.BusinessHourOrderTimeS)
                {
                    orderStartDateChange = false;
                }
                // update properties

                bool dealChangedExpireDate = !entity.Deal.ChangedExpireDate.HasValue && e.Data.Deal.Deal.ChangedExpireDate.HasValue;

                if (entity.Deal.ChangedExpireDate.HasValue && e.Data.Deal.Deal.ChangedExpireDate.HasValue)
                {
                    if (DateTime.Compare(entity.Deal.ChangedExpireDate.Value, e.Data.Deal.Deal.ChangedExpireDate.Value) != 0)
                    {
                        dealChangedExpireDate = true;
                    }
                }

                //比對PCHOME異動
                if (entity.ItemDetail.ItemPrice != e.Data.Deal.ItemDetail.ItemPrice || entity.ItemDetail.ItemOrigPrice != e.Data.Deal.ItemDetail.ItemOrigPrice)
                {
                    pchomeSyncPrice = true;
                }

                if (entity.Deal.OrderTotalLimit != e.Data.Deal.Deal.OrderTotalLimit)
                {
                    pchomeSyncQty = true;
                }

                if (entity.DealContent.Remark != e.Data.Deal.DealContent.Remark ||
                    entity.Deal.BusinessHourDeliverTimeS != e.Data.Deal.Deal.BusinessHourDeliverTimeS ||
                    entity.Deal.BusinessHourDeliverTimeE != e.Data.Deal.Deal.BusinessHourDeliverTimeE ||
                    entity.Deal.ChangedExpireDate != e.Data.Deal.Deal.ChangedExpireDate ||
                    View.IsSaveComboDeals_DS || View.IsSaveComboDeals_DE || View.IsSaveComboDeals_ChangedExpireDate)
                {
                    pchomeSyncEG = true;
                }

                if (entity.ItemDetail.ItemName != e.Data.Deal.ItemDetail.ItemName)
                {
                    pchomeSyncProd = true;
                }

                if (entity.DealContent.AppDealPic != e.Data.ImageData.AppDealPic)
                {
                    pchomeSyncPic = true;
                }

                if (entity.DealContent.Description != e.Data.Deal.DealContent.Description || entity.DealContent.Restrictions != e.Data.Deal.DealContent.Restrictions)
                {
                    pchomeSyncDesc = true;
                }


                //只要有延長時間資料就要+status
                if (e.Data.Deal.Deal.ChangedExpireDate.HasValue)
                {
                    data.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, data.Deal.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange);
                }

                processBuilder.AppendLine("多檔次設定 = " + DateTimeTicks(lastTimes, out lastTimes));

                //在多檔次設定合併到setup之前，另外判斷多檔次狀態
                if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    data.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, data.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain);
                }
                else if ((entity.Deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0)
                {
                    data.Deal.BusinessHourStatus = (int)Helper.SetFlag(true, data.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub);
                }

                processBuilder.AppendLine("結檔後延長 = " + DateTimeTicks(lastTimes, out lastTimes));

                //如果結檔後延長 "結檔日期" , 自動執行 "復原結檔"
                if (View.IsDealClosed &&
                    e.Data.Deal.Deal.BusinessHourOrderTimeE > entity.Deal.BusinessHourOrderTimeE &&
                    e.Data.Deal.Deal.BusinessHourOrderTimeE > DateTime.Now)
                {
                    if (!RecoverDeal(entity.Deal.Guid))
                    {
                        tempErrMsg += "嘗試自動進行[復原結檔]出現異常，請洽It_Service！\\n";
                        View.ShowMessage(tempErrMsg);
                        return;
                    }
                    else
                    {
                        tempErrMsg += "自動進行[復原結檔]成功。\\n";
                    }
                }

                foreach (var c in data.Deal.DirtyColumns)
                {
                    if (pro.Id != 0 && pro.OrderTimeS == null)
                    {
                        if (c.ColumnName == BusinessHour.Columns.BusinessHourDeliverTimeE || c.ColumnName == BusinessHour.Columns.BusinessHourDeliverTimeS ||
                            c.ColumnName == BusinessHour.Columns.BusinessHourOrderTimeE || c.ColumnName == BusinessHour.Columns.BusinessHourOrderTimeS)
                        {
                            if (c.ColumnName == BusinessHour.Columns.BusinessHourOrderTimeS && Convert.ToDateTime(data.Deal.GetColumnValue(c.ColumnName)).ToString("yyyy/MM/dd") != DateTime.MaxValue.ToString("yyyy/MM/dd"))
                            {
                                tempErrMsg += "活動搶購時間請從提案單設定。\\n";
                            }
                            continue;
                        }
                    }

                    entity.Deal.SetColumnValue(c.ColumnName, data.Deal.GetColumnValue(c.ColumnName));
                }

                processBuilder.AppendLine("SendExpireDateChangedMail = " + DateTimeTicks(lastTimes, out lastTimes));

                if (dealChangedExpireDate)
                {
                    SendExpireDateChangedMail(sellerName, data.DealContent.Name, entity.Property.UniqueId, entity.Deal);
                    EmailFacade.SendDealChangeExpiredDateNoticeToMember(entity.Deal.BusinessHourDeliverTimeE.Value, entity.Deal.ChangedExpireDate.Value, entity.Deal.Guid, View.ItemName);
                }

                processBuilder.AppendLine("//SendExpireDateChangedMail = " + DateTimeTicks(lastTimes, out lastTimes));

                foreach (var c in data.DealContent.DirtyColumns)
                {
                    if (c.ColumnName != CouponEventContent.Columns.ImagePath &&
                        c.ColumnName != CouponEventContent.Columns.SpecialImagePath && 
                        c.ColumnName != CouponEventContent.Columns.TravelEdmSpecialImagePath &&
                        c.ColumnName != CouponEventContent.Columns.Availability) // need special care with images
                    {
                        entity.DealContent.SetColumnValue(c.ColumnName, data.DealContent.GetColumnValue(c.ColumnName));
                    }
                }
                entity.DealContent.ModifyTime = DateTime.Now;

                foreach (var c in data.ItemDetail.DirtyColumns)
                {
                    entity.ItemDetail.SetColumnValue(c.ColumnName, data.ItemDetail.GetColumnValue(c.ColumnName));
                }

                foreach (var c in data.Store.DirtyColumns)
                {
                    entity.Store.SetColumnValue(c.ColumnName, data.Store.GetColumnValue(c.ColumnName));
                }

                // 註記代收轉付信託類型(墨攻核銷信託陽信銀行；其他代收轉付一律信託華泰銀行)

                entity.Deal.BusinessHourStatus = Helper.SetBusinessHourTrustProvider(entity.Deal.BusinessHourStatus, View.TrustType);
                //entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoShippingFeeMessage, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoShippingFeeMessage);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NotDeliveryIslands, entity.Deal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
                if (entity.ItemDetail.ItemPrice == 0)
                {
                    entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.PriceZeorShowOriginalPrice, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
                }
                else
                {
                    entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(false, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
                }

                //低毛利率開放折價券or本檔次不可使用折價券
                if (comboDeals.Count > 0)
                {
                    foreach (var subBid in comboDeals.Select(x => x.BusinessHourGuid))
                    {
                        var subDeal = _pponProv.PponDealGet(subBid);
                        subDeal.Deal.BusinessHourStatus =
                            (int)
                                Helper.SetFlag(View.LowGrossMarginAllowedDiscount, subDeal.Deal.BusinessHourStatus,
                                    BusinessHourStatus.LowGrossMarginAllowedDiscount);
                        if (View.NotAllowedDiscount)
                        {
                            PromotionFacade.DiscountLimitSet(entity.Deal.Guid, View.UserName, DiscountLimitType.Enabled);
                        }
                        _pponProv.PponDealSet(subDeal, false);
                    }
                    if (comboDeals.First().MainBusinessHourGuid != null)
                    {
                        PromotionFacade.DiscountLimitSet(comboDeals.First().MainBusinessHourGuid.Value, View.UserName,
                            View.NotAllowedDiscount ? DiscountLimitType.Enabled : DiscountLimitType.Disabled);
                    }
                }
                else
                {
                    PromotionFacade.DiscountLimitSet(View.BusinessHourId, View.UserName,
                        View.NotAllowedDiscount ? DiscountLimitType.Enabled : DiscountLimitType.Disabled);
                }
                processBuilder.AppendLine("低毛利率開放折價券 = " + DateTimeTicks(lastTimes, out lastTimes));

                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.LowGrossMarginAllowedDiscount, entity.Deal.BusinessHourStatus, BusinessHourStatus.LowGrossMarginAllowedDiscount);

                if (View.NotAllowedDiscount)
                {
                    PromotionFacade.DiscountLimitSet(entity.Deal.Guid, View.UserName, DiscountLimitType.Enabled);
                }

                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoDiscountShown, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown);
                //天貓檔次
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.TmallDeal, entity.Deal.BusinessHourStatus, BusinessHourStatus.TmallDeal);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.BuyoutCoupon, entity.Deal.BusinessHourStatus, BusinessHourStatus.BuyoutCoupon);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.GroupCoupon, entity.Deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoRestrictedStore, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NotShowDealDetailOnWeb, entity.Deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                entity.Deal.ModifyTime = entity.ItemDetail.ModifyTime = DateTime.Now;
                entity.Deal.ModifyId = entity.ItemDetail.ModifyId = View.UserName;
                entity.Deal.BusinessHourAtmMaximum = data.Deal.BusinessHourAtmMaximum;
                //多檔次取得毛利率最低的子檔毛利
                var getRate = _pponProv.DealMinGrossMarginGet(View.BusinessHourId) * 100;
                //檔次類型,不可分期是否打勾,毛利率計算結果
                bool[] getInstallmentReturn = PponFacade.DecideInstallment(View.TheDeliveryType, View.DonotInstallment, entity.ItemDetail.ItemPrice, int.Parse(string.IsNullOrEmpty(View.slottingFeeQuantity) ? "0" : View.slottingFeeQuantity), getRate);
                entity.Property.Installment3months = getInstallmentReturn[0];
                entity.Property.Installment6months = getInstallmentReturn[1];
                entity.Property.Installment12months = getInstallmentReturn[2];
                entity.Property.DenyInstallment = View.DonotInstallment;

                if (View.TheDeliveryType == DeliveryType.ToHouse)
                {
                    entity.Property.ShipType = (int)View.OrderShipType;
                    entity.Property.ShippingdateType = (int)View.DealShippingDateType;
                    if (View.OrderShipType == DealShipType.Normal && View.DealShippingDateType == DealShippingDateType.Normal)
                    {
                        entity.Property.Shippingdate = View.ShippingDate;
                        entity.Property.ProductUseDateStartSet = 0;
                        entity.Property.ProductUseDateEndSet = 0;
                    }
                    else if (View.OrderShipType == DealShipType.Normal && View.DealShippingDateType == DealShippingDateType.Special)
                    {
                        entity.Property.Shippingdate = 0;
                        entity.Property.ProductUseDateStartSet = View.ProductUseDateStartSet;
                        entity.Property.ProductUseDateEndSet = View.ProductUseDateEndSet;
                    }
                    else
                    {
                        entity.Property.Shippingdate = 0;
                        entity.Property.ProductUseDateStartSet = 0;
                        entity.Property.ProductUseDateEndSet = 0;
                    }
                }

                entity.Property.VerifyActionType = (View.IsReverseVerify == true) ?
                    (int)VerifyActionType.Reverse : (int)VerifyActionType.General;

                processBuilder.AppendLine("data.DealContent.ImagePath = " + DateTimeTicks(lastTimes, out lastTimes));

                // now let's take care of images
                if (!string.IsNullOrEmpty(data.DealContent.ImagePath))
                {
                    List<int> sorted =
                        (from x in
                             data.DealContent.ImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    string[] originalOrder = Helper.GetRawPathsFromRawData(entity.DealContent.ImagePath);
                    List<string> newOrder = new List<string>();
                    int[] musthave = { 0, 1, 2 };
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        if (sorted.Contains(i))
                        {
                            newOrder.Add(originalOrder[i]);
                        }

                        if (musthave.Contains(i) && !sorted.Contains(i))
                        {
                            newOrder.Add(string.Empty);
                        }
                    }

                    //if first pic has been deleted and other big pic exists, move thr first big pic to first pic

                    if (!sorted.Any(x => x == 0))
                    {
                        sorted.Add(0);
                    }

                    if (!sorted.Any(x => x == 1))
                    {
                        sorted.Add(1);
                    }

                    if (!sorted.Any(x => x == 2))
                    {
                        sorted.Add(2);
                    }

                    string bidFile = entity.DealContent.BusinessHourGuid.ToString().Replace("-", "EDM");
                    if (string.IsNullOrEmpty(newOrder.DefaultIfEmpty(string.Empty).First()) &&
                        !string.IsNullOrEmpty(newOrder.Skip(3).DefaultIfEmpty(string.Empty).First()))
                    {
                        var baseFile = newOrder[3].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();
                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], entity.Store.SellerId, (newOrder.Any(x => x.Contains(bidFile)) ? tick : bidFile) + "." + baseFile[1].Split('.')[1]);
                        newOrder[0] = baseFile[0] + "," + (newOrder.Any(x => x.Contains(bidFile)) ? tick : bidFile) + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(newOrder[3]);
                        newOrder.RemoveAt(3);
                        sorted.RemoveAll(x => x == 3 || x == 0);
                        sorted.Insert(0, 0);
                    }

                    var s = sorted.Where(x => x != 1 && x != 2).Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    var n = newOrder.Where((x, index) => index != 1 && index != 2).ToList();

                    for (int i = 0; i < n.Count(); i++)
                    {
                        s[i].Path = n[i];
                    }
                    List<string> test = new List<string>();
                    test.Add(s.OrderBy(x => x.SortIndex).First().Path);
                    if (newOrder.Count > 1)
                    {
                        test.Add(newOrder[1]);
                    }

                    if (newOrder.Count > 2)
                    {
                        test.Add(newOrder[2]);
                    }

                    test.AddRange(s.OrderBy(x => x.SortIndex).Skip(1).Select(x => x.Path));

                    if (test.Any(x => x.Contains(bidFile)) && test.DefaultIfEmpty(string.Empty).First() != bidFile)
                    {
                        int index = test.FindIndex(x => x.Contains(bidFile));
                        string original = test[index];
                        var baseFile = test[index].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();
                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], entity.Store.SellerId, tick + "." + baseFile[1].Split('.')[1]);
                        test[index] = baseFile[0] + "," + tick + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(original);
                    }

                    if (!test.Any(x => x.Contains(bidFile)) && !string.IsNullOrEmpty(test.DefaultIfEmpty(string.Empty).First()))
                    {
                        string original = test[0];
                        var baseFile = test[0].Split(',');
                        string tick = DateTime.Now.Ticks.ToString();
                        ImageFacade.CopyFile(UploadFileType.PponEvent, baseFile[0], baseFile[1], entity.Store.SellerId, bidFile + "." + baseFile[1].Split('.')[1]);
                        test[0] = baseFile[0] + "," + bidFile + "." + baseFile[1].Split('.')[1];
                        RemovePhotoFromDisk(original);
                    }

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (entity.DealContent.ImagePath.CompareTo(newRawData) != 0)
                    {
                        entity.DealContent.ImagePath = newRawData;
                    }

                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(entity.DealContent.ImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(entity.DealContent.ImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    entity.DealContent.ImagePath = null;
                }

                #region Special Image 的刪除處理
                processBuilder.AppendLine("Special Image 的刪除處理 = " + DateTimeTicks(lastTimes, out lastTimes).ToString());
                //檢查前端回傳的圖片紀錄是否有資料
                if (!string.IsNullOrEmpty(data.DealContent.SpecialImagePath))
                {
                    //前端回傳的資料
                    List<int> sorted =
                        (from x in
                             data.DealContent.SpecialImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    //原始資料
                    string[] originalOrder = Helper.GetRawPathsFromRawData(entity.DealContent.SpecialImagePath);
                    List<string> newOrder = new List<string>();
                    //比較原始圖檔是否要保留
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        //使用者回傳有第i筆資料
                        if (sorted.Contains(i))
                        {
                            //將第i筆記錄寫入暫存，之後要保留
                            newOrder.Add(originalOrder[i]);
                        }
                    }

                    //把要的資料排序排好
                    var s = sorted.Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    for (int i = 0; i < newOrder.Count(); i++)
                    {
                        s[i].Path = newOrder[i];
                    }
                    List<string> test = new List<string>();
                    test.AddRange(s.OrderBy(x => x.SortIndex).Select(x => x.Path));

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (entity.DealContent.SpecialImagePath.CompareTo(newRawData) != 0)
                    {
                        entity.DealContent.SpecialImagePath = newRawData;
                    }
                    //將原始紀錄中不存在於新整理好的圖片資料中的圖檔刪除
                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(entity.DealContent.SpecialImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(entity.DealContent.SpecialImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    entity.DealContent.SpecialImagePath = null;
                }
                #endregion

                #region TraveleDMSpecialPic 的刪除處理
                processBuilder.AppendLine("TraveleDMSpecialPic 的刪除處理 = " + DateTimeTicks(lastTimes, out lastTimes).ToString());
                //檢查前端回傳的圖片紀錄是否有資料
                if (!string.IsNullOrEmpty(data.DealContent.TravelEdmSpecialImagePath))
                {
                    //前端回傳的資料
                    List<int> sorted =
                        (from x in
                             data.DealContent.TravelEdmSpecialImagePath.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                         select int.Parse(x)).ToList();
                    //原始資料
                    string[] originalOrder = Helper.GetRawPathsFromRawData(entity.DealContent.TravelEdmSpecialImagePath);
                    List<string> newOrder = new List<string>();
                    //比較原始圖檔是否要保留
                    for (int i = 0; i < originalOrder.Length; i++)
                    {
                        //使用者回傳有第i筆資料
                        if (sorted.Contains(i))
                        {
                            //將第i筆記錄寫入暫存，之後要保留
                            newOrder.Add(originalOrder[i]);
                        }
                    }

                    //把要的資料排序排好
                    var s = sorted.Select((x, index) => new ImageOrderClass() { Index = x, SortIndex = index }).OrderBy(x => x.Index).ToList();
                    for (int i = 0; i < newOrder.Count(); i++)
                    {
                        s[i].Path = newOrder[i];
                    }
                    List<string> test = new List<string>();
                    test.AddRange(s.OrderBy(x => x.SortIndex).Select(x => x.Path));

                    string newRawData = Helper.GenerateRawDataFromRawPaths(test.ToArray());
                    if (entity.DealContent.TravelEdmSpecialImagePath.CompareTo(newRawData) != 0)
                    {
                        entity.DealContent.TravelEdmSpecialImagePath = newRawData;
                    }
                    //將原始紀錄中不存在於新整理好的圖片資料中的圖檔刪除
                    foreach (string removed in originalOrder.Except(newOrder))
                    {
                        RemovePhotoFromDisk(removed);
                    }
                }
                else if (!string.IsNullOrEmpty(entity.DealContent.TravelEdmSpecialImagePath)) // all images are to be removed
                {
                    foreach (string p in Helper.GetRawPathsFromRawData(entity.DealContent.TravelEdmSpecialImagePath))
                    {
                        RemovePhotoFromDisk(p);
                    }
                    entity.DealContent.TravelEdmSpecialImagePath = null;
                }
                #endregion TraveleDMSpecialPic

                //更新檔次介紹內容裡，圖檔的參照
                PponFacade.UpdateDealImageReference(entity.DealContent.BusinessHourGuid, entity.DealContent.Description);

                #region SMSContent
                processBuilder.AppendLine("SMSContent = " + DateTimeTicks(lastTimes, out lastTimes).ToString());
                string[] contents = View.SMSContent.Split('|');
                SmsContent sc = _pponProv.SMSContentGet(View.BusinessHourId);
                sc.Content = (!string.IsNullOrWhiteSpace(contents[1])) ? View.SMSContent : String.Format("{0}|{1}|{2}", contents[0], View.ItemName, contents[2]);
                sc.ModifyId = View.UserName;
                sc.ModifyTime = DateTime.Now;

                _pponProv.SMSContentSet(sc);
                #endregion SMSContent

                #region Menu 圖片處理，同步到CDN
                if (!View.IsCloseMenu)
                {
                    string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath(string.Format("~/media/{0}/", entity.Store.SellerId));
                    if (File.Exists(baseDirectoryPath + View.BusinessHourId + "_Menu_tmp.png"))
                    {
                        if (File.Exists(baseDirectoryPath + View.BusinessHourId + "_Menu.png"))
                        {
                            File.Delete(baseDirectoryPath + View.BusinessHourId + "_Menu.png");
                        }
                        ImageUtility.UploadFile(new PhysicalPostedFileAdapter(baseDirectoryPath + View.BusinessHourId + "_Menu_tmp.png"),
                            UploadFileType.PponEvent, entity.Store.SellerId, View.BusinessHourId + "_Menu", View.IsMenuLogo);
                    }
                }
                #endregion

                if (PponFacade.IsSkmDeal(View.BusinessHourId) == false)
                {

                    #region PponStore

                    processBuilder.AppendLine("PponStore = " + DateTimeTicks(lastTimes, out lastTimes).ToString());
                    insStores = _pponProv.PponStoreGetListByBusinessHourGuid(View.BusinessHourId);
                    //移除的部分
                    for (int i = insStores.Count - 1; i >= 0; i--)
                    {
                        var delStore = insStores[i];
                        if (e.Data.PponSetupPponStore.Where(x => x.StoreGuid == delStore.StoreGuid).Count() == 0)
                        {
                            delStores.Add(delStore);
                            insStores.RemoveAt(i);
                        }
                    }

                    //新增與修改
                    foreach (var selectedStore in e.Data.PponSetupPponStore)
                    {
                        var workStore = new PponStore();
                        var oldStores = insStores.Where(x => x.StoreGuid == selectedStore.StoreGuid).ToList();
                        if (oldStores.Count > 0)
                        {
                            workStore = oldStores.First();
                            workStore.TotalQuantity = selectedStore.TotalQuantity;
                            workStore.SortOrder = selectedStore.SortOrder;
                            workStore.UseTime = selectedStore.UseTime;
                            workStore.VbsRight = selectedStore.VbsRight;

                            if (selectedStore.ChangedExpireTime.HasValue)
                            {
                                if (workStore.ChangedExpireDate.HasValue)
                                {
                                    if (DateTime.Compare(selectedStore.ChangedExpireTime.Value, workStore.ChangedExpireDate.Value) != 0)
                                    //原本有截止日期 => 日期變更要送信
                                    {
                                        entity.Deal.BusinessHourStatus =
                                            (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange);
                                        var store = _sellerProv.SellerGet(workStore.StoreGuid);
                                        SendStoreExpireDateChangedMail(sellerName, entity.DealContent.Name, entity.Property.UniqueId, entity.Deal, store.SellerName,
                                            selectedStore.ChangedExpireTime);
                                    }
                                }
                                else //原本沒有截止日期 => 要送信
                                {
                                    entity.Deal.BusinessHourStatus =
                                        (int)Helper.SetFlag(true, entity.Deal.BusinessHourStatus, BusinessHourStatus.FinalExpireTimeChange);
                                    var store = _sellerProv.SellerGet(workStore.StoreGuid);
                                    SendStoreExpireDateChangedMail(sellerName, entity.DealContent.Name, entity.Property.UniqueId, entity.Deal, store.SellerName,
                                        selectedStore.ChangedExpireTime);
                                    EmailFacade.SendStoreChangeExpiredDateNoticeToMember(entity.Deal.BusinessHourDeliverTimeE.Value,
                                        selectedStore.ChangedExpireTime.Value, entity.Deal.Guid, selectedStore.StoreGuid, View.ItemName);
                                }
                            }
                            workStore.ChangedExpireDate = selectedStore.ChangedExpireTime;
                        }
                        else
                        {
                            workStore.BusinessHourGuid = View.BusinessHourId;
                            workStore.StoreGuid = selectedStore.StoreGuid;
                            workStore.TotalQuantity = selectedStore.TotalQuantity;
                            workStore.SortOrder = selectedStore.SortOrder;
                            workStore.ResourceGuid = Guid.NewGuid();
                            workStore.VbsRight = selectedStore.VbsRight;
                            workStore.CreateId = View.UserName;
                            workStore.CreateTime = DateTime.Now;
                            insStores.Add(workStore);
                        }
                    }
                    processBuilder.AppendLine("//PponStore = " + DateTimeTicks(lastTimes, out lastTimes).ToString());

                    #endregion PponStore

                }

                #endregion edit mode
            }
            else // add mode
            {
                #region add mode
                processBuilder.AppendLine("add mode = " + DateTimeTicks(lastTimes, out lastTimes));
                isNew = true;

                entity = data;
                // now let's connect the dots
                entity.Store = _sellerProv.SellerGet(View.SellerId);
                entity.Deal.BusinessHourTypeId = (int)BusinessHourType.Ppon;
                entity.Deal.SellerGuid = View.SellerId;
                entity.Deal.Guid = Helper.GetNewGuid(entity);
                entity.DealContent.BusinessHourGuid = entity.Deal.Guid;
                entity.ItemDetail.BusinessHourGuid = entity.Deal.Guid;
                entity.ItemDetail.MenuGuid = Guid.Empty;
                entity.ItemDetail.Guid = Helper.GetNewGuid(entity);
                entity.Deal.CreateTime = entity.ItemDetail.CreateTime = DateTime.Now;
                entity.Deal.CreateId = entity.ItemDetail.CreateId = View.UserName;
                entity.Deal.BusinessHourStatus = (int)Helper.SetBusinessHourTrustProvider(entity.Deal.BusinessHourStatus, View.TrustType);
                //entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoShippingFeeMessage, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoShippingFeeMessage);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NotDeliveryIslands, entity.Deal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
                if (entity.ItemDetail.ItemPrice == 0)
                {
                    entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.PriceZeorShowOriginalPrice, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
                }
                else
                {
                    entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(false, entity.Deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice);
                }
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.LowGrossMarginAllowedDiscount, entity.Deal.BusinessHourStatus, BusinessHourStatus.LowGrossMarginAllowedDiscount);
                //成套票券檔次一般創編建檔預設不可使用折價券

                if ((View.GroupCoupon && !View.HasUseDiscountSetPrivilege) || View.NotAllowedDiscount)
                {
                    PromotionFacade.DiscountLimitSet(entity.Deal.Guid, View.UserName, DiscountLimitType.Enabled);
                }

                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoDiscountShown, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown);

                if (View.TheDeliveryType == DeliveryType.ToShop)
                {
                    PromotionFacade.DiscountLimitSet(entity.Deal.Guid, View.UserName, DiscountLimitType.Enabled);
                }

                //天貓檔次
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.TmallDeal, entity.Deal.BusinessHourStatus, BusinessHourStatus.TmallDeal);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.GroupCoupon, entity.Deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NoRestrictedStore, entity.Deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
                entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.NotShowDealDetailOnWeb, entity.Deal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                //多檔次取得毛利率最低的子檔毛利
                var getRate = _pponProv.DealMinGrossMarginGet(View.BusinessHourId) * 100;
                //檔次類型,不可分期是否打勾,毛利率計算結果
                bool[] getInstallmentReturn = PponFacade.DecideInstallment(View.TheDeliveryType, View.DonotInstallment, entity.ItemDetail.ItemPrice, int.Parse(string.IsNullOrEmpty(View.slottingFeeQuantity) ? "0" : View.slottingFeeQuantity), getRate);
                entity.Property.Installment3months = getInstallmentReturn[0];
                entity.Property.Installment6months = getInstallmentReturn[1];
                entity.Property.Installment12months = getInstallmentReturn[2];
                entity.Property.DenyInstallment = View.DonotInstallment;

                if (View.TheDeliveryType == DeliveryType.ToHouse)
                {
                    entity.Property.ShipType = (int)View.OrderShipType;
                    entity.Property.ShippingdateType = (int)View.DealShippingDateType;
                    if (View.OrderShipType == DealShipType.Normal && View.DealShippingDateType == DealShippingDateType.Normal)
                    {
                        entity.Property.Shippingdate = View.ShippingDate;
                        entity.Property.ProductUseDateStartSet = 0;
                        entity.Property.ProductUseDateEndSet = 0;
                    }
                    if (View.OrderShipType == DealShipType.Normal && View.DealShippingDateType == DealShippingDateType.Special)
                    {
                        entity.Property.Shippingdate = 0;
                        entity.Property.ProductUseDateStartSet = View.ProductUseDateStartSet;
                        entity.Property.ProductUseDateEndSet = View.ProductUseDateEndSet;
                    }
                }

                entity.Property.VerifyActionType = (View.IsReverseVerify == true) ?
                    (int)VerifyActionType.Reverse : (int)VerifyActionType.General;

                #region SMSContent
                processBuilder.AppendLine("SMSContent = " + DateTimeTicks(lastTimes, out lastTimes));
                SmsContent sc = new SmsContent();
                sc.BusinessHourGuid = entity.Deal.Guid;
                sc.Content = View.SMSContent;
                sc.CreateId = View.UserName;
                sc.CreateTime = DateTime.Now;
                _pponProv.SMSContentSet(sc);

                #endregion SMSContent

                if (PponFacade.IsSkmDeal(View.BusinessHourId) == false)
                {

                    #region PponStore

                    processBuilder.AppendLine("PponStore = " + DateTimeTicks(lastTimes, out lastTimes));
                    foreach (var selectedStore in e.Data.PponSetupPponStore)
                    {
                        var pponStore = new PponStore();
                        pponStore.BusinessHourGuid = entity.Deal.Guid;
                        pponStore.StoreGuid = selectedStore.StoreGuid;
                        pponStore.ResourceGuid = Guid.NewGuid();
                        pponStore.TotalQuantity = selectedStore.TotalQuantity;
                        pponStore.SortOrder = selectedStore.SortOrder;
                        pponStore.VbsRight = selectedStore.VbsRight;
                        pponStore.CreateId = View.UserName;
                        pponStore.CreateTime = DateTime.Now;
                        insStores.Add(pponStore);
                    }

                    #endregion PponStore

                }

                #endregion add mode
            }

            if (PponFacade.IsSkmDeal(View.BusinessHourId) == false
                && !ValidateSellerTree(entity.Deal.SellerGuid, e.Data.PponSetupPponStore, out checkErrorMsg))
            {
                tempErrMsg += checkErrorMsg + "\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (PponFacade.IsSkmDeal(View.BusinessHourId) == false
                && View.TheDeliveryType == DeliveryType.ToShop)
            {
                if (!View.NoRestrictedStore && !insStores.Any(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop)))
                {
                    tempErrMsg += "[銷售店鋪]設定未選擇\\n";
                    View.ShowMessage(tempErrMsg);
                    return;
                }
                if (View.NoRestrictedStore && insStores.Any(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop)))
                {
                    tempErrMsg += "通用券檔次無須設定[銷售店鋪]賣家\\n";
                    View.ShowMessage(tempErrMsg);
                    return;
                }
            }
            else if (View.TheDeliveryType == DeliveryType.ToHouse)
            {
                if (insStores.Any(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop)))
                {
                    tempErrMsg += "宅配檔次無須設定[銷售店鋪]賣家\\n";
                    View.ShowMessage(tempErrMsg);
                    return;
                }
                if (insStores.Any(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting) && x.StoreGuid != entity.Deal.SellerGuid))
                {
                    tempErrMsg += "宅配檔次限制[匯款對象]只能設定為檔次賣家，若有疑慮請確認是否將檔次建錯賣家\\n";
                    View.ShowMessage(tempErrMsg);
                    return;
                }
            }

            if (!(View.ComboPackCount > 0))
            {
                tempErrMsg += "多重選項結帳倍數必須大於零\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if ((!(View.SaleMultipleBase > 0) || !(View.PresentQuantity >= 0)) && View.GroupCoupon && _config.IsGroupCouponOn)
            {
                tempErrMsg += "成套票券組合欄位皆必須大於零\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.SaleMultipleBase > 0 && View.GroupCouponType <= 0 && Helper.IsFlagSet(entity.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain) && _config.IsGroupCouponOn)
            {
                tempErrMsg += "請選擇成套票券類型\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (!_config.EnabledGroupCouponTypeB && View.GroupCouponType == (int)GroupCouponDealType.CostAssign)
            {
                tempErrMsg += "B型尚未開放\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.GroupCoupon && int.Parse(View.purchasePrice) % View.SaleMultipleBase > 0)
            {
                tempErrMsg += string.Format("成套票券檔次進貨價({0})必須能整除組數({1})\\n", View.purchasePrice, View.SaleMultipleBase);
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (View.GroupCoupon)
            {
                if (!CheckGroupCouponDealType(entity.ItemDetail.ItemPrice, entity.ItemDetail.ItemOrigPrice, View.SaleMultipleBase, View.PresentQuantity, View.GroupCouponType, out tempErrMsg))
                {
                    View.ShowMessage(tempErrMsg);
                    return;
                }

                //檢查欲同步成套票券子檔的類型是否符合規則
                if (comboDeals.Count > 0 && comboDeals.Any(x => x.GroupCouponDealType != View.GroupCouponType))
                {
                    foreach (var deal in comboDeals.Where(x => x.MainBusinessHourGuid != x.BusinessHourGuid))
                    {
                        if (!CheckGroupCouponDealType(deal.ItemPrice, deal.ItemOrigPrice, deal.SaleMultipleBase, deal.PresentQuantity, View.GroupCouponType, out tempErrMsg, true))
                        {
                            View.ShowMessage(tempErrMsg);
                            return;
                        }
                    }
                }
            }

            if (View.FamiDeal && View.FamiDealType != (int)CouponCodeType.FamiItemCode && View.ExchangePrice == 0)
            {
                tempErrMsg += "此檔次為全家檔次，請填寫全家兌換價。\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            if (PponFacade.CheckPponIntroductionValid(data) == false)
            {
                tempErrMsg += "「權益說明」「更多權益」有無法解析的Html語法，請重新填寫或洽IT處理。\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            //憑證檔次需檢查勾選顯示分店，是否已填寫經緯度
            if (View.TheDeliveryType == DeliveryType.ToShop)
            {
                string nullCoordinate = "";
                foreach (var ps in e.Data.PponSetupPponStore)
                {
                    if (Helper.IsFlagSet(ps.VbsRight, VbsRightFlag.Location))
                    {
                        Seller s = _sellerProv.SellerGet(ps.StoreGuid);

                        if (string.IsNullOrEmpty(s.Coordinate))
                        {
                            nullCoordinate += string.Format("分店名稱:{0}\\n", s.SellerName);
                        }
                    }
                }

                if (!string.IsNullOrWhiteSpace(nullCoordinate))
                {
                    tempErrMsg += "請提醒COD人員填寫此檔次以下分店經緯度資訊:\\n" + nullCoordinate;
                }
            }

            processBuilder.AppendLine("_pponProv.ViewPponDealGetByBusinessHourGuid = " + DateTimeTicks(lastTimes, out lastTimes));
            ViewPponDeal vpDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);

            #region 1.編輯模式下 2.開檔後改檔名，須將相關的 OrderDetail 裡的 ItemName 一併改過來

            if (View.BusinessHourId != Guid.Empty && View.IsDealOpened && entity.ItemDetail.ItemName != vpDeal.ItemName)
            {
                IEnumerable<OrderDetail> ods = _orderProv.OrderDetailGetListByBid(View.BusinessHourId)
                    .Where(x => x.ItemName != I18N.Phrase.DeliveryCharge).ToList();
                if (ods.Any())
                {
                    OrderDetailCollection orderDetails = new OrderDetailCollection();
                    foreach (OrderDetail od in ods)
                    {
                        od.ItemName = View.ItemName;
                        orderDetails.Add(od);
                    }
                    _orderProv.OrderDetailSetList(orderDetails);
                }
            }

            #endregion

            #region 好康哪裡找
            processBuilder.AppendLine("好康哪裡找 = " + DateTimeTicks(lastTimes, out lastTimes));
            //如果有輸入分店資料，以分店資料產生好康哪裡找的紀錄
            var availables = new List<AvailableInformation>();
            var locationStores = insStores.Where(t => Helper.IsFlagSet(t.VbsRight, VbsRightFlag.Location))
                .OrderBy(x => x.SortOrder).ToList();
            foreach (var st in locationStores)
            {
                var store = _sellerProv.SellerGet(st.StoreGuid);
                var viewstorecategoryCol = _sellerProv.ViewStoreCategoryCollectionGetByStore(st.StoreGuid);
                var mrtInfo = string.Empty;
                if (viewstorecategoryCol.Any())
                {
                    mrtInfo = string.Join(",", viewstorecategoryCol.Select(x => x.Name).ToArray());
                }

                SqlGeography geo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                var tmp = new AvailableInformation()
                {
                    N = store.SellerName,
                    P = store.StoreTel,
                    A = (CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) + store.StoreAddress),
                    Longitude = geo != null ? geo.Long.ToString() : string.Empty,
                    Latitude = geo != null ? geo.Lat.ToString() : string.Empty,
                    OT = store.OpenTime,
                    UT = st.UseTime,
                    U = store.WebUrl,
                    R = store.StoreRemark,
                    CD = store.CloseDate,
                    MR = (string.IsNullOrEmpty(store.Mrt)) ? mrtInfo : store.Mrt,
                    CA = store.Car,
                    BU = store.Bus,
                    OV = store.OtherVehicles,
                    FB = store.FacebookUrl,
                    PL = string.Empty,
                    BL = store.BlogUrl,
                    OL = store.OtherUrl
                };
                availables.Add(tmp);
            }

            string oldAvailability = "";
            if (availables.Count >= 0)
            {
                oldAvailability = entity.DealContent.Availability;
                entity.DealContent.Availability = new JsonSerializer().Serialize(availables);
            }

            //比對PCHOME異動
            if (oldAvailability != entity.DealContent.Availability)
            {
                pchomeSyncDesc = true;
            }

            #endregion 好康哪裡找

            #region DealProperty
            processBuilder.AppendLine("DealProperty = " + DateTimeTicks(lastTimes, out lastTimes));
            DealProperty dealProperty = _pponProv.DealPropertyGet(entity.Deal.Guid);
            if (!dealProperty.IsLoaded)
            {
                dealProperty.BusinessHourGuid = entity.Deal.Guid;
                dealProperty.CreateId = View.UserName;
                dealProperty.CreateTime = DateTime.Now;
            }
            else
            {
                dealProperty.ModifyId = View.UserName;
                dealProperty.ModifyTime = DateTime.Now;
            }

            dealProperty.IsDepositCoffee = View.IsDepositCoffee;
            dealProperty.VerifyActionType = View.IsReverseVerify ? (int)VerifyActionType.Reverse : (int)VerifyActionType.General;

            dealProperty.DealType = DealTypeFacade.GetLvl2ParentValue(View.DealTypeNewValue);
            dealProperty.DealTypeDetail = View.DealTypeNewValue;
            dealProperty.EntrustSell = (int)View.EntrustSell;
            dealProperty.TravelPlace = View.TravelPlace;
            dealProperty.SellerVerifyType = View.SellerVerifyType;
            dealProperty.DealAccBusinessGroupId = View.AccBusinessGroupNo;
            dealProperty.DealEmpName = View.DevelopeSales;
            dealProperty.CouponSeparateDigits = View.CouponSeparateDigits;
            dealProperty.IsPromotionDeal = View.IsPromotionDeal;
            dealProperty.IsExhibitionDeal = View.IsExhibitionDeal;
            dealProperty.Cchannel = View.IsCChannel;
            dealProperty.CchannelLink = View.CChannelLink;
            dealProperty.IsGame = View.IsGame;
            dealProperty.IsChannelGift = View.IsChannelGift;
            dealProperty.AgentChannels = View.AgentChannels;
            dealProperty.IsTaishinChosen = View.IsTaishinChosen;

            //檢查是否設定到己關閉的銷售分類
            if (DealTypeFacade.CheckSelfAndParentsAllEnabled(View.DealTypeNewValue) == false)
            {
                tempErrMsg += "請設定有效的銷售分類，請重整網頁再確認。\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            //用業務名稱抓取對應Employee EmpNo 寫進dealProperty這個資料表 若對應不到 則無需填寫
            var deEmpInfo = _humanProv.EmployeeGet(Employee.Columns.EmpName, View.DevelopeSales);
            if (deEmpInfo.IsLoaded && deEmpInfo != null && (!isNew | (!deEmpInfo.IsInvisible && deEmpInfo.DepartureDate == null)))
            {
                dealProperty.EmpNo = deEmpInfo.EmpNo;
                dealProperty.DevelopeSalesId = deEmpInfo.UserId;
            }
            else
            {
                tempErrMsg += "請填入有效業務1\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            var opEmpInfo = _humanProv.EmployeeGet(Employee.Columns.EmpName, View.OperationSales);
            string checkSales = SellerFacade.CheckSales(2, View.DevelopeSales, View.OperationSales);
            if (string.IsNullOrEmpty(checkSales))
            {
                dealProperty.OperationSalesId = opEmpInfo.UserId;
            }
            else
            {
                tempErrMsg += "請填入有效業務2\\n";
                View.ShowMessage(tempErrMsg);
                return;
            }

            dealProperty.DeliveryType = (int)View.TheDeliveryType;
            dealProperty.ShoppingCart = View.DShoppingCart;
            dealProperty.MultipleBranch = View.MultipleBranch;
            dealProperty.CompleteCopy = View.CompleteCopy;

            dealProperty.ComboPackCount = View.ComboPackCount;
            dealProperty.PresentQuantity = View.PresentQuantity;
            dealProperty.SaleMultipleBase = View.SaleMultipleBase;
            dealProperty.GroupCouponAppStyle = View.GroupCouponAppStyle;
            dealProperty.GroupCouponDealType = View.GroupCouponType;

            #region 處理檔次接續
            processBuilder.AppendLine("處理檔次接續 = " + DateTimeTicks(lastTimes, out lastTimes));
            Guid ancBid;    //接續數量
            Guid.TryParse(View.AncestorBusinessHourGuid, out ancBid);
            Guid ancSeqBid; //接續憑證
            Guid.TryParse(View.AncestorSequenceBusinessHourGuid, out ancSeqBid);
            //數量
            if (!Guid.Equals(Guid.Empty, ancBid) && ancQuantity != -1)
            {
                dealProperty.AncestorBusinessHourGuid = ancBid;
                dealProperty.IsContinuedQuantity = View.Is_Continued_Quantity;
                dealProperty.ContinuedQuantity = ancQuantity;
            }
            else
            {
                dealProperty.AncestorBusinessHourGuid = null;
                dealProperty.IsContinuedQuantity = false;
                dealProperty.ContinuedQuantity = 0;
            }
            //憑證
            if (!Guid.Equals(Guid.Empty, ancSeqBid))
            {
                dealProperty.AncestorSequenceBusinessHourGuid = ancSeqBid;
                dealProperty.IsContinuedSequence = View.Is_Continued_Sequence;
            }
            else
            {
                dealProperty.AncestorSequenceBusinessHourGuid = null;
                dealProperty.IsContinuedSequence = false;
            }

            #endregion 處理檔次接續

            dealProperty.QuantityMultiplier = View.QuantityMultiplier;
            dealProperty.IsQuantityMultiplier = View.IsQuantityMultiplier;
            dealProperty.IsAveragePrice = View.IsAveragePrice;
            dealProperty.IsDailyRestriction = View.IsDailyRstriction;
            dealProperty.ActivityUrl = View.ActivityUrl;

            List<int> dealLabelTags = new List<int>();
            if (View.TheDeliveryType == DeliveryType.ToShop)
            {
                //dealProperty.LabelIconList = ReplaceDealIconSellOverOneOrTenThousand(View.SelectedCouponIcon);
                if (View.SelectedCouponDealTag.Length > 0)
                {
                    dealLabelTags.AddRange(View.SelectedCouponDealTag);
                }
                if (View.AccBusinessGroupNo == (int)LunchKingSite.Core.AccBusinessGroupNo.Travel && View.SelectedCouponTravelDealTag.Length > 0)
                {
                    dealLabelTags.AddRange(View.SelectedCouponTravelDealTag);
                }
            }
            else if (View.TheDeliveryType == DeliveryType.ToHouse)
            {
                //dealProperty.LabelIconList = ReplaceDealIconSellOverOneOrTenThousand(View.SelectedDeliveryIcon);
                if (View.SelectedDeliveryDealTag.Length > 0)
                {
                    dealLabelTags.AddRange(View.SelectedDeliveryDealTag);
                }
                if (View.AccBusinessGroupNo == (int)LunchKingSite.Core.AccBusinessGroupNo.Travel && View.SelectedDeliveryTravelDealTag.Length > 0)
                {
                    dealLabelTags.AddRange(View.SelectedDeliveryTravelDealTag);
                }
            }
            dealProperty.LabelTagList = string.Join(",", dealLabelTags);

            #region 儲存LabelIconList
            processBuilder.AppendLine("儲存LabelIconList = " + DateTimeTicks(lastTimes, out lastTimes));
            List<int> iconIdList = new List<int>();
            if (View.SelectedDefaultIcon.IndexOf((int)DealLabelSystemCode.AppLimitedEdition) != -1)
            {
                iconIdList.Add((int)DealLabelSystemCode.AppLimitedEdition);
            }
            //24小時到貨
            if (View.SelectedDefaultIcon.IndexOf((int)DealLabelSystemCode.TwentyFourHoursArrival) != -1)
            {
                iconIdList.Add((int)DealLabelSystemCode.TwentyFourHoursArrival);
            }
            if (View.GroupCoupon)
            {
                iconIdList.Add((int)DealLabelSystemCode.GroupCoupon);
            }
            foreach (int categoryId in View.SelectedSpecialCategories)
            {
                DealLabelSystemCode? code = CategoryManager.GetDealLabelSystemCodeByCategoryId(categoryId);
                if (code.HasValue)
                {
                    iconIdList.Add((int)code.Value);
                }
            }
            dealProperty.LabelIconList = DealIconlListToString(iconIdList);
            #endregion 儲存LabelIconList

            dealProperty.CustomTag = View.CustomTag;

            //設定BarcodeType
            if (View.FamiDeal)
            {
                dealProperty.CouponCodeType = View.FamiDealType;
            }
            else if (View.HiLifeDeal)
            {
                dealProperty.CouponCodeType = (int)CouponCodeType.HiLifeItemCode;
            }
            else if (View.TheDeliveryType == DeliveryType.ToShop)
            {
                dealProperty.CouponCodeType = (int)CouponCodeType.Barcode128;
            }
            else
            {
                dealProperty.CouponCodeType = (int)CouponCodeType.None;
            }

            dealProperty.PinType = (View.IsSinglePinCode) ? (int)PinType.Single : (int)PinType.Normal;

            dealProperty.ExchangePrice = (View.FamiDeal && View.FamiDealType != (int)CouponCodeType.FamiItemCode) ? View.ExchangePrice : 0;

            //SKM
            dealProperty.IsExperience = View.SkmDeal ? (bool?)View.SkmExperience : null;
            dealProperty.DiscountType = View.SkmDeal ? View.SkmDiscountType : 0;
            dealProperty.Discount = View.SkmDeal ? View.SkmDiscount : 0;

            dealProperty.BookingSystemType = View.BookingSystemType;
            dealProperty.AdvanceReservationDays = View.AdvanceReservationDays;
            dealProperty.CouponUsers = View.CouponUsers;
            dealProperty.IsReserveLock = View.IsReserveLock;

            dealProperty.TmallRmbPrice = View.TmallRmbPrice;
            dealProperty.TmallRmbExchangeRate = View.TmallRmbExchangeRate;

            dealProperty.IsMergeCount = View.IsMergeCount;
            dealProperty.IsZeroActivityShowCoupon = View.ZeroActivityShowCoupon;
            dealProperty.IsLongContract = View.IsLongContract;

            //設定台新分期
            dealProperty.Installment3months = entity.Property.Installment3months;
            dealProperty.Installment6months = entity.Property.Installment6months;
            dealProperty.Installment12months = entity.Property.Installment12months;
            dealProperty.DenyInstallment = entity.Property.DenyInstallment;

            dealProperty.ShipType = entity.Property.ShipType;
            dealProperty.ShippingdateType = entity.Property.ShippingdateType;
            dealProperty.Shippingdate = entity.Property.Shippingdate;
            dealProperty.ProductUseDateStartSet = entity.Property.ProductUseDateStartSet;
            dealProperty.ProductUseDateEndSet = entity.Property.ProductUseDateEndSet;

            if (_config.IsConsignment)
            {
                dealProperty.Consignment = View.Consignment;
            }

            #endregion DealProperty

            #region DealPayment
            var dealPayment = _pponProv.DealPaymentGet(entity.Deal.Guid);

            if (!dealPayment.IsLoaded)
            {
                dealPayment.BusinessHourGuid = entity.Deal.Guid;
            }

            dealPayment.BankId = (int)View.LimitCreditCardBankId;
            dealPayment.AllowGuestBuy = View.AllowGuestBuy;
            dealPayment.IsBankDeal = View.IsBankDeal;

            #endregion

            #region Save DealAccounting

            processBuilder.AppendLine("Save DealAccounting = " + DateTimeTicks(lastTimes, out lastTimes));
            var dAccounting = _pponProv.DealAccountingGet(View.BusinessHourId);
            if (!dAccounting.IsLoaded)
            {
                dAccounting.BusinessHourGuid = entity.Deal.Guid;
            }

            if (dAccounting.SalesCommission == null)
            {
                dAccounting.SalesCommission = 0.2;
            }

            if (dAccounting.SalesBonus == null)
            {
                dAccounting.SalesBonus = 0;
            }

            if (dAccounting.Status == null)
            {
                dAccounting.Status = (int)AccountsPayableFormula.Default;
            }
            dAccounting.SalesId = View.DevelopeSales;
            dAccounting.OperationSalesId = View.OperationSales;

            //業績歸屬
            dAccounting.AccCity = View.AccCityId;
            dAccounting.DeptId = View.DeptId;
            //發票對開的手續費
            dAccounting.Commission = View.Commission;

            var paytoCompany = (int)SellerFacade.GetPayToCompany(entity.Deal.SellerGuid, insStores);
            dAccounting.Paytocompany = paytoCompany;

            if (View.IsKindDeal)
            {
                dAccounting.RemittanceType = (int)RemittanceType.Others;
                dAccounting.VendorBillingModel = (int)VendorBillingModel.BalanceSheetSystem;
            }
            else
            {
                dAccounting.RemittanceType = (int)View.PaidType;
                dAccounting.VendorBillingModel = (int)View.BillingModel;
            }
            dAccounting.VendorReceiptType = (int)View.ReceiptType;
            dAccounting.Message = View.AccountingMessage;
            dAccounting.IsInputTaxRequired = !(View.IsInputTaxNotRequired);

            #endregion Save DealAccounting

            #region Save DealDiscountPriceBlacklist

            processBuilder.AppendLine("Save DealDiscountPriceBlacklist = " + DateTimeTicks(lastTimes, out lastTimes));

            var dealDiscountPriceBlacklists = new DealDiscountPriceBlacklist
            {
                Bid = View.BusinessHourId,
                CreatedTime = DateTime.Now,
                CreateId = View.UserName,
            };

            PponFacade.UpdateDealDiscountPriceBlacklist(View.IsDealDiscountPriceBlacklist, dealDiscountPriceBlacklists);

            #endregion Save DealDiscountPriceBlacklist

            #region WeeklyPayAccount
            processBuilder.AppendLine("WeeklyPayAccount = " + DateTimeTicks(lastTimes, out lastTimes));
            var accounts = _orderProv.WeeklyPayAccountGetList(View.BusinessHourId);
            var now = DateTime.Now;

            //對應ppon_store數量儲存 另抓取store 取得匯款帳號資料
            var stores = _sellerProv.SellerGetList(insStores.Where(s => Helper.IsFlagSet(s.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid));
            foreach (var store in stores)
            {
                var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                            ?? new WeeklyPayAccount
                            {
                                BusinessHourGuid = View.BusinessHourId,
                                CreateDate = now,
                                Creator = View.UserName
                            };
                account.StoreGuid = store.Guid;
                account.EventName = (entity.DealContent.Name.Length > 100)
                                        ? entity.DealContent.Name.Substring(0, 100) + "..."
                                        : entity.DealContent.Name;
                account.CompanyName = store.CompanyName;
                account.SellerName = store.CompanyBossName;
                account.AccountId = store.CompanyID;
                account.AccountName = store.CompanyAccountName;
                account.AccountNo = store.CompanyAccount;
                account.Email = store.CompanyEmail;
                account.BankNo = store.CompanyBankCode;
                account.BranchNo = store.CompanyBranchCode;
                account.Message = store.CompanyNotice;
                account.SignCompanyID = store.SignCompanyID;

                accounts.Add(account);
            }

            #endregion WeeklyPayAccount

            #region Save Category Deal data
            processBuilder.AppendLine("Save Category Deal data = " + DateTimeTicks(lastTimes, out lastTimes));
            CategoryDealCollection cds = new CategoryDealCollection();

            #region 新版的頻道、區域、分類之處理

            Dictionary<int, List<int>> selectedChannelCategories = View.SelectedChannelCategories;
            Dictionary<int, List<int>> selectedSpecialRegionCategories = View.SelectedChannelSpecialRegionCategories;
            List<int> selectedSubSpecialRegionSubCategories = View.SelectedChannelSpecialRegionSubCategories;

            List<int> selectedDealCategories = View.SelectedDealCategories;

            //2016/03/31 決定不做檢查
            ////檢查分類是否正確
            //if (_config.EnableInspectDealCategorises)
            //{
            //    if (entity.ItemDetail.ItemPrice > 0)
            //    {
            //        CategoryManager.InspectDealCategorises(ref selectedChannelCategories, ref selectedDealCategories);
            //    }
            //}

            //儲存依據新版頻道及區域 對應的 舊版cityId
            List<int> selectedCity = new List<int>();
            foreach (var selectedChannel in selectedChannelCategories)
            {
                PponCity city = PponCityGroup.GetPponCityByCategoryId(selectedChannel.Key);
                if (city != null)
                {
                    selectedCity.Add(city.CityId);
                }

                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = selectedChannel.Key });
                foreach (var areaId in selectedChannel.Value)
                {
                    city = PponCityGroup.GetPponCityByCategoryId(areaId);
                    if (city != null)
                    {
                        selectedCity.Add(city.CityId);
                    }

                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = areaId });


                    //商圈
                    foreach (var specialRegion in selectedSpecialRegionCategories.Where(x => x.Key == areaId))
                    {
                        var subcategorydepency = _sellerProv.ViewCategoryDependencyGetByParentId(areaId).ToList();


                        if ((subcategorydepency.Any(x => x.CategoryName.Contains("商圈‧景點"))))
                        {
                            foreach (var sub in subcategorydepency.Where(x => x.CategoryName.Contains("商圈‧景點")))
                            {
                                if (!cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == sub.CategoryId))
                                {
                                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = sub.CategoryId });
                                }
                            }
                        }


                        foreach (var subRegion in specialRegion.Value)
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = subRegion });

                            //商圈子區域
                            foreach (int selectedSPR in selectedSubSpecialRegionSubCategories)
                            {
                                if (!cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == selectedSPR))
                                {
                                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = selectedSPR });
                                }
                            }
                        }
                    }
                }

                //2014/02/10進版只進後台，前台功能更新，為確保顯示正常，當勾選旅遊檔時，需同時處理舊的旅遊分類
                if (selectedChannel.Key == PponCityGroup.DefaultPponCityGroup.Travel.CategoryId)
                {
                    //處理旅遊頻道的每個區域
                    bool haveTravel = false;
                    foreach (var areaId in selectedChannel.Value)
                    {
                        //查詢新的旅遊區域對應的舊旅遊區域category物件
                        Category oldCategory = CategoryManager.GetTravelByPponChannelArea(areaId);
                        if (oldCategory != null)
                        {
                            //如果對應沒有出錯，新增一筆紀錄。
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = oldCategory.Id });
                            haveTravel = true;
                        }
                    }
                    //有任何一個旅遊區域被勾選，填入預設值
                    if ((haveTravel) && !cds.Any(c => c.Cid == ViewPponDealManager.DefaultManager.TravelDefaultCategoryId))
                    {
                        cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = ViewPponDealManager.DefaultManager.TravelDefaultCategoryId });
                    }
                }
            }

            //行政區分店自動檢查category
            List<ViewCategoryDependency> vcdList = _sellerProv.ViewCategoryDependencyGetByParentType((int)CategoryType.PponChannelArea).ToList();

            IEnumerable<Guid> storeGuidList;
            if (View.GroupCoupon)//僅代金卷(成套商品卷)抓所有分店資料
            {
                storeGuidList = SellerFacade.GetSellerTreeSellers(View.SellerGuid).Select(x => x.Guid); //分店隱藏的不抓
            }
            else //其他都抓前台顯示的分店資料
            {
                storeGuidList = insStores.Select(x => x.StoreGuid);
            }

            foreach (var storeGuid in storeGuidList)
            {
                var store = _sellerProv.SellerGet(storeGuid);

                if (store.IsLoaded && store.StoreCityId.HasValue && store.StoreTownshipId.HasValue)
                {
                    var township = CityManager.TownShipGetById(store.StoreTownshipId.Value);
                    var parentcity = CityManager.CityGetById(store.StoreCityId.Value);
                    if (township != null && parentcity != null)
                    {
                        ViewCategoryDependency vcd = vcdList.Where(x => x.ParentName == parentcity.CityName && x.CategoryName == township.CityName).DefaultIfEmpty(null).First();
                        if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.ParentId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                        }
                        if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.CategoryId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.CategoryId });
                        }
                    }

                    #region 捷運站分類檢查

                    var viewStoreCategoryCol = _sellerProv.ViewStoreCategoryCollectionGetByStore(store.Guid);
                    if (viewStoreCategoryCol != null && viewStoreCategoryCol.Any())
                    {
                        foreach (var viewstorecategory in viewStoreCategoryCol)
                        {
                            ViewCategoryDependency vcd = vcdList.Where(x => x.CategoryId == viewstorecategory.CategoryId).DefaultIfEmpty(null).First();
                            if (vcd != null && !cds.Any(x => x.Bid == entity.Deal.Guid && x.Cid == vcd.ParentId))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = vcd.ParentId });
                            }
                            if (viewstorecategory != null && !cds.Any(x => x.Bid == entity.Deal.Guid &&
                                viewstorecategory.CategoryId.HasValue && x.Cid == viewstorecategory.CategoryId.Value))
                            {
                                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = viewstorecategory.CategoryId.Value });
                            }
                        }
                    }

                    #endregion 捷運站分類檢查
                }
            }

            foreach (var dealCategoryId in selectedDealCategories)
            {
                if (dealCategoryId != FamilyLockCategoryId && dealCategoryId != FamilyBeaconUnLockCategoryId)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = dealCategoryId });
                }

            }

            //取貨方式區塊
            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = View.SelectedDeliveryCategoryId });
            //檔次的特殊類別(ICON區塊)
            List<int> selectedSpecialCategories = View.SelectedSpecialCategories;
            foreach (var specialCategoryId in selectedSpecialCategories)
            {
                //特殊區塊有24與72小時的須設定，所以也要查詢對應的cityId
                PponCity city = PponCityGroup.GetPponCityByCategoryId(specialCategoryId);
                if (city != null)
                {
                    selectedCity.Add(city.CityId);
                }
                if (specialCategoryId != FamilyLockCategoryId && specialCategoryId != FamilyBeaconUnLockCategoryId)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = specialCategoryId });
                }
            }

            if (View.IsAppLimitedEdition)
            {
                cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = CategoryManager.Default.AppLimitedEdition.CategoryId });
            }

            #endregion 新版的頻道、區域、分類之處理
            //生活商圈區塊
            foreach (KeyValuePair<int, int[]> categoryList in View.SelectedCommercialCategoryId)
            {
                foreach (int categoryId in categoryList.Value)
                {
                    cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = categoryId });
                }
            }

            //全家Beacon檢查
            if (View.FamiDeal)
            {

                if (View.FamilyDealType == (int)Core.FamilyDealType.General)
                {
                    //General
                    #region General

                    //remove FamilyLockCategoryId
                    //if (cds.Any(x => x.Cid == FamilyLockCategoryId))
                    //{
                    //do
                    //{
                    //    if (cds.FirstOrDefault(x => x.Cid == FamilyLockCategoryId) == null)
                    //    {
                    //        break;
                    //    }
                    //    cds.Remove(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = FamilyLockCategoryId });
                    //}while(cds.Any(x => x.Cid == FamilyLockCategoryId));
                    //}
                    //remove FamilyBeaconUnLockCategoryId
                    //if (cds.Any(x => x.Cid == FamilyBeaconUnLockCategoryId))
                    //{
                    //do
                    //{
                    //    if (cds.FirstOrDefault(x => x.Cid == FamilyBeaconUnLockCategoryId) == null)
                    //    {
                    //        break;
                    //    }
                    //    cds.Remove(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = FamilyBeaconUnLockCategoryId });
                    //} while (cds.Any(x => x.Cid == FamilyBeaconUnLockCategoryId));

                    //}

                    #endregion General
                }
                else
                {
                    //Beacon
                    #region Beacon

                    if (View.FamilyIsLock)
                    {
                        #region FamilyLockCategoryId

                        //Add FamilyLockCategoryId
                        if (!cds.Any(x => x.Cid == FamilyLockCategoryId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = FamilyLockCategoryId });
                        }

                        #endregion FamilyLockCategoryId
                    }
                    else
                    {
                        #region FamilyBeaconUnLockCategoryId

                        //Add FamilyBeaconUnLockCategoryId
                        if (!cds.Any(x => x.Cid == FamilyBeaconUnLockCategoryId))
                        {
                            cds.Add(new CategoryDeal() { Bid = entity.Deal.Guid, Cid = FamilyBeaconUnLockCategoryId });
                        }

                        #endregion FamilyBeaconUnLockCategoryId
                    }

                    #endregion Beacon
                }


            }
            processBuilder.AppendLine("//Save Category Deal data = " + DateTimeTicks(lastTimes, out lastTimes));
            #endregion Save Category Deal data

            #region Property
            if (pro.IsLoaded)
            {
                if ((pro.OrderTimeS != data.Deal.BusinessHourOrderTimeS) || (pro.OrderTimeE != data.Deal.BusinessHourOrderTimeE))
                {
                    pro.OrderTimeS = data.Deal.BusinessHourOrderTimeS;
                    pro.OrderTimeE = data.Deal.BusinessHourOrderTimeE;
                    ProposalFacade.ProposalLog(pro.Id, "[後台異動檔期]" + string.Format("{0}~{1}", pro.OrderTimeS, pro.OrderTimeE), View.UserName);
                    _sellerProv.ProposalSet(pro);
                }                
            }
            #endregion Property


            //將選取的城市紀錄於dealProperty
            dealProperty.CityList = new JsonSerializer().Serialize(selectedCity);
            if (cds.FirstOrDefault(t => t.Cid == CategoryManager.Default.Travel.CategoryId) != null)
            {
                dealProperty.IsTravelDeal = true;
            }
            else
            {
                dealProperty.IsTravelDeal = false;
            }

            //使用者選取的城市， 新版分類上線後，改由 新分類設定的頻道對應產生城市編號
            int[] cities = selectedCity.ToArray();

            //取得原本設定的Dts
            DealTimeSlotCollection origDtsCol = new DealTimeSlotCollection();
            entity.TimeSlotCollection.CopyTo(origDtsCol);

            processBuilder.AppendLine("處理DealTimeSlot = " + DateTimeTicks(lastTimes, out lastTimes));
            if (!Helper.IsFlagSet(entity.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub)) //不是子檔才insert DealTimeSlot
            {
                //處理DealTimeSlot
                DealTimeSlotInsert(entity, cities, orderStartDateChange, processBuilder);
            }
            processBuilder.AppendLine("//處理DealTimeSlot = " + DateTimeTicks(lastTimes, out lastTimes));

            #region skm_deal_time_slot        
            if (View.SkmDeal)
            {
                //新光檔次                
                ViewExternalDeal ved = _pponProv.ViewExternalDealGetByBid(entity.DealContent.BusinessHourGuid);
                if (ved.IsLoaded)
                {
                    if (ved.Status >= (int)SKMDealStatus.WaitingConfirm)
                    {
                        PponFacade.SkmDealTimeSlotSet(entity);
                    }
                }

            }
            #endregion skm_deal_time_slot

            // preset reward to 500 pts for ppon & pponitem only
            entity.Store.SellerStatus = SellerFacade.SetRewardType(entity.Store.SellerStatus, RewardType.FiveHundredPoints);

            //賣服務則運費一律設定運費為0
            if (!dealProperty.DeliveryType.HasValue || int.Equals((int)DeliveryType.ToShop, dealProperty.DeliveryType.Value)) //到府不用運費
            {
                entity.Deal.BusinessHourDeliveryCharge = 0;
            }
            processBuilder.AppendLine("上傳圖片 = " + DateTimeTicks(lastTimes, out lastTimes));
            #region 上傳圖片
            //檢查有無檔案需要上傳
            if (e.Data.UploadedFile != null)
            {
                // we have uploaded file
                //依據圖片用途進行不同的處理
                switch (e.Data.ImageUploadType)
                {
                    case PponSetupImageUploadType.ByORder:
                    case PponSetupImageUploadType.BigPic:
                        //好康大圖(依順序上傳或直接上傳輪播大圖)
                        #region 上傳圖片並取得新路徑
                        List<string> originalImgOrder = Helper.GetRawPathsFromRawData(entity.DealContent.ImagePath).ToList<string>();
                        bool isUploadByOrder = e.Data.ImageUploadType == PponSetupImageUploadType.ByORder;
                        string[] imagePathList = data.DealContent.ImagePath.Split(',');
                        HttpPostedFileBase hpfb = new HttpPostedFileWrapper(e.Data.UploadedFile) as HttpPostedFileBase;
                        entity.DealContent.ImagePath = ImageUtility.UploadPponImageAndGetNewPath(hpfb, entity.Store.SellerId, entity.DealContent.BusinessHourGuid.ToString(), entity.DealContent.ImagePath, isUploadByOrder, View.IsPrintWatermark, false);
                        #endregion

                        bool isFirstPicEmpty = originalImgOrder.Count.Equals(0) || string.IsNullOrEmpty(originalImgOrder[0]);
                        // 當上傳第一張輪播大圖(且side deal小圖(1)沒有圖)時，自動上傳Side Deal小圖
                        if (isFirstPicEmpty && !imagePathList.Contains("1"))
                        {
                            entity.DealContent.ImagePath = ImageUtility.UploadPponImageAndGetNewPath(hpfb, entity.Store.SellerId, entity.DealContent.BusinessHourGuid.ToString(), entity.DealContent.ImagePath, isUploadByOrder, View.IsPrintWatermark, true);
                            string tmpOriImagePath = ImageUtility.UploadPponImageAndGetNewPath(hpfb, entity.Store.SellerId, entity.DealContent.BusinessHourGuid.ToString(), entity.DealContent.ImagePath, false, false, false);
                            string pcOriImagePath = string.Empty;
                            if (!string.IsNullOrEmpty(tmpOriImagePath))
                            {
                                pcOriImagePath = tmpOriImagePath.Substring(tmpOriImagePath.LastIndexOf("||") + 2);
                            }
                            entity.DealContent.OriImagePath = pcOriImagePath;
                        }
                        break;
                    case PponSetupImageUploadType.APP:
                        //APP用的特殊規格圖片
                        UploadSpecialImageFile(e.Data.UploadedFile, ref entity);
                        break;
                    case PponSetupImageUploadType.TravelEdmSpecialBigPic:
                        UploadTravelEDMSpecialPicFile(e.Data.UploadedFile, ref entity);
                        break;
                    default:
                        //圖片用途不明，不處理上傳
                        break;
                }
            }
            //2種情境下要刪圖檔
            //  1.原本有圖，現在沒圖
            //  2.原本有圖，現在換圖
            if (string.IsNullOrEmpty(e.Data.ImageData.AppDealPic) == false)
            {
                if (e.Data.ImageData.AppDealPic != entity.DealContent.AppDealPic)
                {
                    if (entity.DealContent.AppDealPic != null &&
                        entity.DealContent.AppDealPic.EndsWith(e.Data.ImageData.AppDealPic) == false)
                    {
                        try
                        {
                            if (string.IsNullOrEmpty(entity.DealContent.AppDealPic) == false)
                            {
                                ImageUtility.DeleteFile(UploadFileType.PponEvent, entity.Store.SellerId,
                                    entity.DealContent.AppDealPic.Split(',')[1]);
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.WarnFormat("後臺上檔刪圖失敗, bid={0}, pic={1}, ex={2} ", entity.Deal.Guid,
                                entity.DealContent.AppDealPic, ex);
                        }
                    }
                    string appDealPicImagePath = Path.Combine(_config.WebTempPath, WebStorage._IMAGE_TEMP,
                        e.Data.ImageData.AppDealPic);
                    UploadAppDealPic(appDealPicImagePath,
                        new Rectangle
                        {
                            X = e.Data.ImageData.CropX,
                            Y = 0,
                            Size = new Size(CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM,
                                    CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM)
                        },
                        ref entity);
                    File.Delete(appDealPicImagePath);
                }
            }
            else
            {
                try
                {
                    if (string.IsNullOrEmpty(entity.DealContent.AppDealPic) == false)
                    {
                        ImageUtility.DeleteFile(UploadFileType.PponEvent, entity.Store.SellerId,
                            entity.DealContent.AppDealPic.Split(',')[1]);
                    }
                }
                catch (Exception ex)
                {
                    logger.WarnFormat("後臺上檔刪圖失敗, bid={0}, pic={1}, ex={2} ", entity.Deal.Guid,
                        entity.DealContent.AppDealPic, ex);
                }
                entity.DealContent.AppDealPic = "";
            }

            processBuilder.AppendLine("//上傳圖片 = " + DateTimeTicks(lastTimes, out lastTimes));
            #endregion 上傳圖片

            // first we check to see if number of records match
            bool syncLoc = entity.TimeSlotCollection == null || entity.TimeSlotCollection.Count != origDtsCol.Count;
            if (!syncLoc)
            {
                origDtsCol = origDtsCol.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                entity.TimeSlotCollection.OrderByAsc(DealTimeSlot.Columns.CityId).OrderByAsc(DealTimeSlot.Columns.EffectiveStart);
                for (int i = 0; !syncLoc && i < entity.TimeSlotCollection.Count; i++)
                    syncLoc |= (entity.TimeSlotCollection[i].CityId != origDtsCol[i].CityId ||
                                entity.TimeSlotCollection[i].EffectiveStart != origDtsCol[i].EffectiveStart ||
                                entity.TimeSlotCollection[i].EffectiveEnd != origDtsCol[i].EffectiveEnd ||
                                entity.TimeSlotCollection[i].Sequence != origDtsCol[i].Sequence ||
                                entity.TimeSlotCollection[i].Status != origDtsCol[i].Status ||
                                entity.TimeSlotCollection[i].IsInTurn != origDtsCol[i].IsInTurn ||
                                entity.TimeSlotCollection[i].IsLockSeq != origDtsCol[i].IsLockSeq);
            }
            processBuilder.AppendLine("syncLoc = " + DateTimeTicks(lastTimes, out lastTimes));
            var success = _pponProv.PponDealSet(entity, syncLoc, origDtsCol);
            processBuilder.AppendLine("PponDealSet = " + DateTimeTicks(lastTimes, out lastTimes));
            #region accessory conversion

            newGroups = ItemFacade.ProcessAccessoryGroupText(entity.Deal.Guid, entity.Store.Guid, "17P", e.Data.AccessoryString);

            List<dynamic> dummy;
            if (!PponFacade.TryParseOptions(e.Data.AccessoryString, out dummy)
                || !PponFacade.ProcessAccessory(entity.Deal.Guid, entity.Store.Guid, entity.ItemDetail.Guid, "17P", newGroups))
            {
                tempErrMsg += "多重選項有誤，請檢查格式是否正確、內容有無重複、各類別項目不超過50字!!\\n";
            }

            #endregion accessory conversion
            processBuilder.AppendLine("GroupOrderGetList = " + DateTimeTicks(lastTimes, out lastTimes));
            GroupOrderCollection gOrderList = _orderProv.GroupOrderGetList(GroupOrder.Columns.BusinessHourGuid + "=" + entity.Deal.Guid);
            GroupOrder gOrder;
            if (gOrderList.Count > 0)
            {
                gOrder = gOrderList[0];
            }
            else
            {
                Guid grpOrderGuid = OrderFacade.MakeGroupOrder(_config.ServiceEmail, _config.ServiceName,
                    entity.Deal.Guid, DateTime.Now, entity.Deal.BusinessHourOrderTimeE);
                gOrder = _orderProv.GroupOrderGet(grpOrderGuid);
            }
            gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.HighQuantityNotToShow).ToString())
                ? true : (string.IsNullOrEmpty(View.QuantityToShow) || View.QuantityToShow.Equals(((int)GroupOrderStatus.LowQuantityToShow).ToString())
                    ? false : View.IsHiddenFromRecentDeals),
            gOrder.Status ?? 0, GroupOrderStatus.Hidden);

            gOrder.Status = (int)Helper.SetFlag(View.DisableSMS, gOrder.Status ?? 0, GroupOrderStatus.DisableSMS);
            gOrder.Status = (int)Helper.SetFlag(View.NoRefund, gOrder.Status ?? 0, GroupOrderStatus.NoRefund);
            gOrder.Status = (int)Helper.SetFlag(View.DaysNoRefund, gOrder.Status ?? 0, GroupOrderStatus.DaysNoRefund);
            gOrder.Status = (int)Helper.SetFlag(View.ExpireNoRefund, gOrder.Status ?? 0, GroupOrderStatus.ExpireNoRefund);
            gOrder.Status = (int)Helper.SetFlag(View.NoRefundBeforeDays, gOrder.Status ?? 0, GroupOrderStatus.NoRefundBeforeDays);
            gOrder.Status = (int)Helper.SetFlag(View.NoTax, gOrder.Status ?? 0, GroupOrderStatus.NoTax);
            gOrder.Status = (int)Helper.SetFlag(View.NoInvoice, gOrder.Status ?? 0, GroupOrderStatus.NoInvoice);
            //for PEZ 特殊活動
            gOrder.Status = (int)Helper.SetFlag(View.PEZevent, gOrder.Status ?? 0, GroupOrderStatus.PEZevent);
            gOrder.Status = (int)Helper.SetFlag(View.PEZeventCouponDownload, gOrder.Status ?? 0, GroupOrderStatus.PEZeventCouponDownload);
            gOrder.Status = (int)Helper.SetFlag(View.PEZeventWithUserId, gOrder.Status ?? 0, GroupOrderStatus.PEZeventWithUserId);
            //全家檔次
            gOrder.Status = (int)Helper.SetFlag(View.FamiDeal, gOrder.Status ?? 0, GroupOrderStatus.FamiDeal);
            //萊爾富檔次
            gOrder.Status = (int)Helper.SetFlag(View.HiLifeDeal, gOrder.Status ?? 0, GroupOrderStatus.HiLifeDeal);
            gOrder.Status = (int)Helper.SetFlag(View.IsKindDeal, gOrder.Status ?? 0, GroupOrderStatus.KindDeal);
            gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.LowQuantityToShow).ToString()),
                gOrder.Status ?? 0, GroupOrderStatus.LowQuantityToShow);
            gOrder.Status = (int)Helper.SetFlag(View.QuantityToShow.Equals(((int)GroupOrderStatus.HighQuantityNotToShow).ToString()),
                gOrder.Status ?? 0, GroupOrderStatus.HighQuantityNotToShow);
            //新光檔次
            gOrder.Status = (int)Helper.SetFlag(View.SkmDeal, gOrder.Status ?? 0, GroupOrderStatus.SKMDeal);
            _orderProv.GroupOrderSet(gOrder);
            processBuilder.AppendLine("//GroupOrderGetList = " + DateTimeTicks(lastTimes, out lastTimes));

            //每週出帳設定
            _orderProv.UpdateBusinessHourStatusForWeeklyPay(View.WeeklyPay, entity.Deal.Guid);
            processBuilder.AppendLine("每週出帳設定 = " + DateTimeTicks(lastTimes, out lastTimes));

            #region 運費處理
            processBuilder.AppendLine("運費處理 = " + DateTimeTicks(lastTimes, out lastTimes));
            var freightIncome = View.FreightIncome;
            var freightCost = View.FreightCost;
            var freights = new CouponFreightCollection();
            if (!dealProperty.DeliveryType.HasValue ||
                int.Equals((int)DeliveryType.ToHouse, dealProperty.DeliveryType.Value)) //宅配才可能有運費
            {
                CouponFreight proFreight = null;
                foreach (var viewFreight in freightIncome.OrderBy(x => x.StartAmount))
                {
                    var newFreight = new CouponFreight();
                    newFreight.BusinessHourGuid = entity.Deal.Guid;
                    newFreight.StartAmount = viewFreight.StartAmount;
                    newFreight.EndAmount = 9999999;
                    newFreight.FreightAmount = viewFreight.FreightAmount;
                    newFreight.FreightType = (int)CouponFreightType.Income;
                    newFreight.CreateId = View.UserName;
                    newFreight.CreateTime = DateTime.Now;
                    freights.Add(newFreight);
                    if (proFreight != null)
                    {
                        proFreight.EndAmount = newFreight.StartAmount;
                    }
                    proFreight = newFreight;
                }
                proFreight = null;
                foreach (var viewFreight in freightCost.OrderBy(x => x.StartAmount))
                {
                    var newFreight = new CouponFreight();
                    newFreight.BusinessHourGuid = entity.Deal.Guid;
                    newFreight.StartAmount = viewFreight.StartAmount;
                    newFreight.EndAmount = 9999999;
                    newFreight.FreightAmount = viewFreight.FreightAmount;
                    newFreight.FreightType = (int)CouponFreightType.Cost;
                    newFreight.CreateId = View.UserName;
                    newFreight.CreateTime = DateTime.Now;
                    newFreight.PayTo = (int)Enum.Parse(typeof(CouponFreightPayTo), viewFreight.PayTo);
                    freights.Add(newFreight);
                    if (proFreight != null)
                    {
                        proFreight.EndAmount = newFreight.StartAmount;
                    }
                    proFreight = newFreight;
                }
            }
            processBuilder.AppendLine("//運費處理 = " + DateTimeTicks(lastTimes, out lastTimes));
            #endregion 運費處理

            #region 進貨價處理
            processBuilder.AppendLine("進貨價處理 = " + DateTimeTicks(lastTimes, out lastTimes));
            var multiCost = View.MultiCost;
            var costCol1 = new DealCostCollection();
            //刪除進貨價
            DealCostCollection dealCostGetList = _pponProv.DealCostGetList(entity.Deal.Guid);
            foreach (var dealCost in dealCostGetList)
            {
                bool isExist = false;
                foreach (var viewFreight in multiCost)
                {
                    if (_pponProv.DealCostGet(viewFreight.Id).BusinessHourGuid != entity.Deal.Guid)
                    {
                        isExist = true;
                    }

                    if (!dealCost.Id.Equals(viewFreight.Id) && dealCost.BusinessHourGuid == entity.Deal.Guid)
                    {
                        isExist = false;
                    }
                    else
                    {
                        isExist = true;
                        break;
                    }
                }

                if (!isExist)
                {
                    _pponProv.DealCostDelete(dealCost.Id);
                }
            }

            //新增進貨價
            foreach (var viewFreight in multiCost.OrderBy(x => x.Id))
            {
                var newFreight = new DealCost();
                newFreight.BusinessHourGuid = entity.Deal.Guid;
                newFreight.Id = viewFreight.Id;
                newFreight.Cost = viewFreight.Cost;
                newFreight.Quantity = viewFreight.Quantity;
                newFreight.CumulativeQuantity = viewFreight.Cumulative_quantity;
                newFreight.LowerCumulativeQuantity = viewFreight.Lower_cumulative_quantity;
                if (_pponProv.DealCostGet(viewFreight.Id) == null || _pponProv.DealCostGet(viewFreight.Id).BusinessHourGuid != entity.Deal.Guid)
                {
                    costCol1.Add(newFreight);
                }
            }

            #endregion 進貨價處理

            processBuilder.AppendLine("//Mongo 好康異動時須進行的處理  = " + DateTimeTicks(lastTimes, out lastTimes));
            if (success)
            {
                //儲存PponStore
                _pponProv.PponStoreDeleteList(delStores);
                _pponProv.PponStoreSetList(insStores);
                processBuilder.AppendLine("儲存PponStore = " + DateTimeTicks(lastTimes, out lastTimes));

                //儲存業務資料
                dealProperty.CategoryList = NewtonsoftJson.JsonConvert.SerializeObject(cds.Select(x => x.Cid).Distinct().ToList(), NewtonsoftJson.Formatting.None);
                _pponProv.DealAccountingSet(dAccounting);
                _pponProv.DealPropertySet(dealProperty);
                _pponProv.DealPaymentSet(dealPayment);
                processBuilder.AppendLine("儲存業務資料 = " + DateTimeTicks(lastTimes, out lastTimes));

                //運費
                _pponProv.CouponFreightDeleteListByBid(entity.Deal.Guid);
                _pponProv.CouponFreightSetList(freights);
                processBuilder.AppendLine("運費 = " + DateTimeTicks(lastTimes, out lastTimes));

                //寫入CategoryDeal
                PponFacade.SaveCategoryDeals(cds, entity.Deal.Guid);
                processBuilder.AppendLine("寫入CategoryDeal = " + DateTimeTicks(lastTimes, out lastTimes));

                //儲存進貨價
                _pponProv.DealCostSetList(costCol1);
                processBuilder.AppendLine("儲存進貨價 = " + DateTimeTicks(lastTimes, out lastTimes));

                //每周付款
                _orderProv.WeeklyPayAccountSetList(accounts);
                processBuilder.AppendLine("每周付款 = " + DateTimeTicks(lastTimes, out lastTimes));

                // 圖片替代文字內容新增至關鍵字詞庫
                if (!string.IsNullOrWhiteSpace(entity.Deal.PicAlt))
                {
                    List<string> keywords = entity.Deal.PicAlt.Split('/').ToList();
                    foreach (string word in keywords)
                    {
                        if (!string.IsNullOrWhiteSpace(word))
                        {
                            Keyword keyword = _pponProv.KeywordGet(word);
                            if (!keyword.IsLoaded)
                            {
                                keyword.Word = word;
                                keyword.CreateId = View.UserName;
                                keyword.CreateTime = DateTime.Now;
                                _pponProv.KeywordSet(keyword);
                            }
                        }
                    }
                }
                processBuilder.AppendLine("圖片替代文字內容新增至關鍵字詞庫 = " + DateTimeTicks(lastTimes, out lastTimes));

                //新多選項
                DealProperty dp = _pponProv.DealPropertyGet(entity.Deal.Guid);
                if (!dp.IsHouseNewVer)
                {
                    //新版宅配提案單，後台不需要存多重選項，統一從提案單同步回來
                    PponFacade.ProcessOptions(e.Data.AccessoryString, entity.Deal.Guid, _pponProv.DealPropertyGet(entity.Deal.Guid).UniqueId, View.UserName);
                    processBuilder.AppendLine("新多選項 = " + DateTimeTicks(lastTimes, out lastTimes));
                }
                

                //多檔次 宅配 鎖帳務設定
                //均更新與母檔相同

                Guid comboMainBid =
                        comboDeals.Where(t => t.MainBusinessHourGuid == t.BusinessHourGuid)
                                  .Select(t => t.BusinessHourGuid)
                                  .FirstOrDefault();
                if (comboMainBid == View.BusinessHourId && View.TheDeliveryType == DeliveryType.ToHouse)
                {
                    foreach (var subBid in comboDeals.Where(x => x.MainBusinessHourGuid != x.BusinessHourGuid).Select(x => x.BusinessHourGuid))
                    {
                        var subAccounting = _pponProv.DealAccountingGet(subBid);

                        //匯款資料
                        subAccounting.Paytocompany = paytoCompany;
                        //對帳方式
                        //出帳方式
                        subAccounting.RemittanceType = (int)View.PaidType;
                        subAccounting.VendorBillingModel = (int)View.BillingModel;
                        //店家單據開立方式
                        subAccounting.VendorReceiptType = (int)View.ReceiptType;
                        //免稅設定
                        subAccounting.IsInputTaxRequired = !(View.IsInputTaxNotRequired);

                        _pponProv.DealAccountingSet(subAccounting);

                        //NoTax

                        var subGo = _orderProv.GroupOrderGetByBid(subBid);
                        subGo.Status = (int)Helper.SetFlag(View.NoTax, subGo.Status ?? 0, GroupOrderStatus.NoTax);
                        _orderProv.GroupOrderSet(subGo);
                    }
                }
                processBuilder.AppendLine("多檔次 宅配 鎖帳務設定 = " + DateTimeTicks(lastTimes, out lastTimes));

                // 多檔次 ComboData
                if (e.Data.ComboData != null)
                {
                    PponDeal comboMain = null;
                    if (comboMainBid != Guid.Empty)
                    {
                        comboMain = _pponProv.PponDealGet(comboMainBid);
                    }
                    else if (e.Data.ComboData.MainDeal != null && e.Data.ComboData.MainDeal.Bid != Guid.Empty)
                    {
                        comboMain = _pponProv.PponDealGet(e.Data.ComboData.MainDeal.Bid);
                    }

                    // from database (old)
                    var comboSubDeals = comboDeals.Where(t => t.MainBusinessHourGuid != t.BusinessHourGuid)
                                                  .OrderBy(t => t.Sequence)
                                                  .Select(t => new ComboItem { Bid = t.BusinessHourGuid, Title = t.Title })
                                                  .ToList();
                    // from ui (new)
                    var comboSubDeals2 = e.Data.ComboData.SubDeals;

                    if (comboMainBid != Guid.Empty)
                    {
                        // 取出目前檔次
                        foreach (ComboItem i in comboSubDeals2)
                        {
                            if (i.Bid == View.BusinessHourId)
                            {
                                // 統一代檔次短標
                                i.Title = data.DealContent.CouponUsage;
                            }
                        }
                    }

                    if (comboSubDeals2.SequenceEqual(comboSubDeals) == false)
                    {
                        // 簡單做，如果有任何不一致，就刪掉全部重設
                        PponFacade.ResetComboDeal(comboMain, comboSubDeals2, View.UserName);
                    }

                    //同步成套票券類型
                    if (View.GroupCoupon)
                    {
                        UpdateGroupCouponType(comboSubDeals2.Select(x => x.Bid).ToList(), View.GroupCouponType);
                    }
                }
                else
                {
                    if (comboMainBid != Guid.Empty)
                    {
                        PponDeal comboMain = null;
                        comboMain = _pponProv.PponDealGet(comboMainBid);
                        var comboSubDeals = comboDeals.Where(t => t.MainBusinessHourGuid != t.BusinessHourGuid)
                                                  .OrderBy(t => t.Sequence)
                                                  .Select(t => new ComboItem { Bid = t.BusinessHourGuid, Title = t.Title })
                                                  .ToList();

                        foreach (ComboItem i in comboSubDeals)
                        {
                            if (i.Bid == View.BusinessHourId)
                            {
                                // 統一代檔次短標
                                i.Title = data.DealContent.CouponUsage;
                            }
                        }

                        PponFacade.ResetComboDeal(comboMain, comboSubDeals, View.UserName);
                    }
                }
                PponFacade.CheckMainComboDealAveragePrice(entity);
                processBuilder.AppendLine("多檔次 ComboData = " + DateTimeTicks(lastTimes, out lastTimes));

                ViewPponDeal ppon = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);
                string s = ViewPponDealToJson(ppon);
                _pponProv.ChangeLogInsert("ViewPponDeal", View.BusinessHourId.ToString(), s);

                string d = DealAccountingToJson(dAccounting);
                _pponProv.ChangeLogInsert("DealAccounting", View.BusinessHourId.ToString(), d);

                ComboDealCollection cdc = _pponProv.GetComboDealByBid(View.BusinessHourId, false);
                List<Guid> guidlist = new List<Guid>();
                foreach (var cd in cdc)
                    guidlist.Add(cd.BusinessHourGuid);
                string c = cdc.Count() == 0 ? "無子檔" : "子檔為" + string.Join("、", guidlist);
                _pponProv.ChangeLogInsert("ComboDeal", View.BusinessHourId.ToString(), c);

                PponStoreCollection pponStores = _pponProv.PponStoreGetListByBusinessHourGuid(View.BusinessHourId);
                foreach (var pponStore in pponStores)
                {
                    _pponProv.ChangeLogInsert("PponStore", View.BusinessHourId.ToString(), PponStoreToJson(pponStore));
                }

                PponOptionCollection pponOptions = _pponProv.PponOptionGetList(View.BusinessHourId);
                foreach (var pponOption in pponOptions)
                {
                    _pponProv.ChangeLogInsert("PponOption", View.BusinessHourId.ToString(), PponOptionToJson(pponOption));
                }

                _pponProv.ChangeLogInsert("DealCategories", View.BusinessHourId.ToString(), (new JsonSerializer()).Serialize(View.SelectedDealCategories));

                processBuilder.AppendLine("_pponProv.ChangeLogInsert = " + DateTimeTicks(lastTimes, out lastTimes));

                var cloneDealGuids = new List<Guid>();
                #region 母檔存檔時，同步更新子檔的收款帳戶
                if (View.BusinessHourId != Guid.Empty)
                {
                    if (Helper.IsFlagSet(entity.Deal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                    {
                        /* 
                         * 取得所有子檔(不包含母檔，因為只要同步更新子檔)
                         * */
                        cloneDealGuids = _pponProv.GetComboDealByBid(View.BusinessHourId, false).Where(x => x.BusinessHourGuid != x.MainBusinessHourGuid)
                                                .Select(x => x.BusinessHourGuid)
                                                .ToList();

                        UpdateDealAccount(cloneDealGuids, entity);
                        UpdatePromotionDeal(cloneDealGuids, dealProperty.IsPromotionDeal, dealProperty.IsExhibitionDeal);
                        if (_config.IsConsignment)
                        {
                            UpdateConsignment(cloneDealGuids, View.Consignment);
                        }
                        if (View.GroupCoupon)
                        {
                            UpdateGroupCouponType(cloneDealGuids, View.GroupCouponType);
                        }
                    }
                }
                processBuilder.AppendLine("母檔存檔時，同步更新子檔的收款帳戶 = " + DateTimeTicks(lastTimes, out lastTimes));
                #endregion

                #region "母檔及子檔連動功能"
                //if checkbox checked
                if (View.IsSaveComboDeals)
                {
                    if (View.BusinessHourId != Guid.Empty)
                    {
                        SaveComboDeals(View.BusinessHourId, data, cloneDealGuids, processBuilder);
                    }
                }
                processBuilder.AppendLine("母檔及子檔連動功能 = " + DateTimeTicks(lastTimes, out lastTimes));
                #endregion

                #region CDN圖片清空(上檔狀態的檔次圖片才需要清空)

                if (entity.Deal.BusinessHourOrderTimeS < DateTime.Now && entity.Deal.BusinessHourOrderTimeE > DateTime.Now)
                {
                    CDNImageDelete(entity.DealContent.ImagePath, entity.Deal.Guid);
                }
                processBuilder.AppendLine("CDN圖片清空 = " + DateTimeTicks(lastTimes, out lastTimes));
                #endregion

                #region 購物車設定
                if (_config.ShoppingCartV2Enabled)
                {
                    UpdateShoppingCart(dealProperty.BusinessHourGuid, e.Data.FreightsId);
                }
                #endregion


                #region PChome商品異動時一併同步
                string errMessage = "";
                if (mainBid != null)
                {
                    //多檔
                    if (mainBid == View.BusinessHourId)
                    {
                        var newComboDeals = _pponProv.GetViewComboDealAllByBid(View.BusinessHourId);
                        //改的是母檔,只會更新到子檔的圖文,一姬的使用時間
                        foreach (ViewComboDeal deal in newComboDeals)
                        {
                            if (deal.BusinessHourGuid != deal.MainBusinessHourGuid)
                            {
                                if (ChannelFacade.IsPChomeChannelProd(deal.BusinessHourGuid, out pchomeProd))
                                {
                                    if (pchomeSyncDesc)
                                    {
                                        ChannelFacade.SyncDesc(pchomeProd, entity.DealContent.Description, entity.DealContent.Restrictions, entity.DealContent.Availability, out errMessage);
                                        if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                            tempErrMsg += errMessage;

                                    }

                                    if (pchomeSyncEG)
                                    {
                                        ChannelFacade.SyncPChomeEG(pchomeProd, entity.DealContent.Remark, deal.BusinessHourDeliverTimeS.Value,
                                            deal.ChangedExpireDate.HasValue ? deal.ChangedExpireDate.Value : deal.BusinessHourDeliverTimeE.Value, out errMessage);
                                        if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                            tempErrMsg += errMessage;
                                    }

                                    if (pchomeSyncPic)
                                    {
                                        ChannelFacade.SyncPChomePic(pchomeProd, entity.DealContent.AppDealPic, out errMessage);
                                        if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                            tempErrMsg += errMessage;
                                    }
                                }

                            }
                        }
                    }
                    else
                    {
                        if (ChannelFacade.IsPChomeChannelProd(View.BusinessHourId, out pchomeProd))
                        {
                            //子檔只會更新價格、數量、一姬使用時間
                            if (pchomeSyncPrice)
                            {
                                ChannelFacade.SyncPChomePrice(pchomeProd, entity.ItemDetail.ItemPrice, entity.ItemDetail.ItemOrigPrice, out errMessage);
                                if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                    tempErrMsg += errMessage;
                            }

                            if (pchomeSyncQty)
                            {
                                ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);
                                int qty = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(entity.Deal.OrderTotalLimit)) ? 0 : Convert.ToInt32(entity.Deal.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
                                ChannelFacade.SyncPChomeQty(pchomeProd, qty, out errMessage);
                                if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                    tempErrMsg += errMessage;
                            }

                            if (pchomeSyncEG)
                            {
                                CouponEventContent content = _pponProv.CouponEventContentGetByBid((Guid)mainBid);
                                ChannelFacade.SyncPChomeEG(pchomeProd, content.Remark, entity.Deal.BusinessHourDeliverTimeS.Value, (entity.Deal.ChangedExpireDate.HasValue ? entity.Deal.ChangedExpireDate.Value : entity.Deal.BusinessHourDeliverTimeE.Value), out errMessage);
                                if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                    tempErrMsg += errMessage;
                            }

                            if (pchomeSyncProd)
                            {
                                ChannelFacade.SyncPChomeProd(pchomeProd, entity.ItemDetail.ItemName, out errMessage);
                                if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                    tempErrMsg += errMessage;
                            }
                        }
                    }



                }
                else
                {
                    //單檔
                    if (ChannelFacade.IsPChomeChannelProd(View.BusinessHourId, out pchomeProd))
                    {
                        if (pchomeSyncPrice)
                        {
                            ChannelFacade.SyncPChomePrice(pchomeProd, entity.ItemDetail.ItemPrice, entity.ItemDetail.ItemOrigPrice, out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }

                        if (pchomeSyncDesc)
                        {
                            ChannelFacade.SyncDesc(pchomeProd, entity.DealContent.Description, entity.DealContent.Restrictions, entity.DealContent.Availability, out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }

                        if (pchomeSyncEG)
                        {
                            ChannelFacade.SyncPChomeEG(pchomeProd, entity.DealContent.Remark, entity.Deal.BusinessHourDeliverTimeS.Value, (entity.Deal.ChangedExpireDate.HasValue ? entity.Deal.ChangedExpireDate.Value : entity.Deal.BusinessHourDeliverTimeE.Value), out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }

                        if (pchomeSyncProd)
                        {
                            ChannelFacade.SyncPChomeProd(pchomeProd, entity.ItemDetail.ItemName, out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }

                        if (pchomeSyncQty)
                        {
                            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);
                            int qty = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(entity.Deal.OrderTotalLimit)) ? 0 : Convert.ToInt32(entity.Deal.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
                            ChannelFacade.SyncPChomeQty(pchomeProd, qty, out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }

                        if (pchomeSyncPic)
                        {
                            ChannelFacade.SyncPChomePic(pchomeProd, entity.DealContent.AppDealPic, out errMessage);
                            if (!string.IsNullOrEmpty(errMessage) && !tempErrMsg.Contains(errMessage))
                                tempErrMsg += errMessage;
                        }
                    }

                } 
                #endregion

                if (View.BusinessHourId != Guid.Empty)
                {
                    /*try
                    {
                        //紀錄存檔時間
                        //processBuilder.AppendLine("OnSaveDeal(End) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");
                        //Log.Info(processBuilder.ToString());
                    }
                    catch { }*/

                    //最後Sink CkEditor上傳的圖片
                    ImageUtility.SinkImages(true, View.BusinessHourId);
                    SetupDealLabels();
                    LoadDeal(entity.Deal.Guid);

                    //儲存時清除母檔暫存Cache
                    if (comboMainBid != Guid.Empty)
                    {
                        ViewPponDealManager.DefaultManager.ClearViewPponDealCache(comboMainBid);
                    }
                    else
                    {
                        ViewPponDealManager.DefaultManager.ClearViewPponDealCache(View.BusinessHourId);
                    }
                }
                else
                {
                    /*try
                    {
                        //紀錄存檔時間
                        //processBuilder.AppendLine("OnSaveDeal(End) = " + DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss.ffff") + "<br/>");
                        //Log.Info(processBuilder.ToString());
                    }
                    catch { }*/

                    View.JumpTo(PponSetupMode.DealPage, entity.Deal.Guid.ToString());
                }
            }
            else
            {
                View.JumpTo(PponSetupMode.GenericError, null);
            }

            //將提醒文字一起顯示
            if (tempErrMsg != "")
            {
                View.ShowMessage(tempErrMsg);
            }
        }

        private void OnSetShoppingCart(object sender, EventArgs e)
        {
            SetupShoppingCartOption();
        }

        private void CDNImageDelete(string imagePath, Guid bid)
        {
            foreach (string imgPath in Helper.GetRawPathsFromRawData(imagePath))
            {
                if (imgPath.IndexOf(',') > 0)
                {
                    string folder = imgPath.Split(',')[0];
                    string filename = imgPath.Split(',')[1];
                    ImageFacade.DeleteCDNImage(string.Format("/{0}/{1}/{2}", ImageLoadBalanceFolder.Media.ToString().ToLower(), folder, filename));
                }
            }
            LoadDeal(bid);
        }

        private void SaveComboDeals(Guid mainBusinessHourGuid, PponDeal data, List<Guid> cloneDealGuids, StringBuilder processBuilder)
        {
            if (mainBusinessHourGuid != Guid.Empty)
            {

                try
                {
                    processBuilder.AppendLine("母子檔同步(子檔數量) = " + cloneDealGuids.Count.ToString() + "<br/>");
                    if (cloneDealGuids.Count > 0)
                    {
                        //取出母檔
                        DealProperty mainProperty = _pponProv.DealPropertyGet(mainBusinessHourGuid);
                        BusinessHour mainHour = _pponProv.BusinessHourGet(mainBusinessHourGuid);
                        DealAccounting mainAccounting = _pponProv.DealAccountingGet(mainBusinessHourGuid);
                        CategoryDealCollection mainCategory = _pponProv.CategoryDealsGetList(mainBusinessHourGuid);
                        Proposal pro = _sellerProv.ProposalGet(mainBusinessHourGuid);
                        string sellerName = _sellerProv.SellerGetByBid(View.SellerGuid).SellerName;
                        foreach (Guid dealGuid in cloneDealGuids)
                        {
                            processBuilder.AppendLine("母子檔同步(子檔bid) = " + dealGuid.ToString() + "<br/>");

                            CategoryDealCollection cds = new CategoryDealCollection();

                            DealProperty dealCopiedProperty = _pponProv.DealPropertyGet(dealGuid);
                            DealAccounting dealCopiedAccouning = _pponProv.DealAccountingGet(dealGuid);
                            GroupOrder orderGo = _orderProv.GroupOrderGetByBid(dealGuid);
                            DealPayment dealPayment = _pponProv.DealPaymentGet(dealGuid);

                            var entity = _pponProv.PponDealGet(dealGuid);
                            if (View.IsSaveComboDeals_Sales)
                            {
                                //業務
                                /*
                             * Table : deal_property
                             * Column : deal_emp_name 
                             * Table : deal_accounting
                             * Column : sales_id
                             * */
                                dealCopiedProperty.DealEmpName = mainProperty.DealEmpName;
                                dealCopiedProperty.DevelopeSalesId = mainProperty.DevelopeSalesId;
                                dealCopiedProperty.OperationSalesId = mainProperty.OperationSalesId;
                                dealCopiedAccouning.SalesId = mainAccounting.SalesId;
                                dealCopiedAccouning.OperationSalesId = mainAccounting.OperationSalesId;

                            }
                            if (View.IsSaveComboDeals_Channel)
                            {
                                //上檔頻道(依排檔區域勾選) CategoryDeal
                                /*
                             * Table :category_deals
                             * */
                                //cds = DealCategoryCopy(_entity);
                                foreach (CategoryDeal dc in mainCategory)
                                {
                                    cds.Add(new CategoryDeal() { Bid = dealGuid, Cid = dc.Cid });
                                }
                                //Icon專區的在這兒
                                dealCopiedProperty.LabelIconList = mainProperty.LabelIconList;

                            }
                            if (View.IsSaveComboDeals_SEO)
                            {
                                //SEO設定
                                /*
                             * Table :business_hour
                             * Column : page_title/page_desc/pic_alt
                             * */
                                entity.Deal.PageTitle = mainHour.PageTitle;
                                entity.Deal.PageDesc = mainHour.PageDesc;
                                entity.Deal.PicAlt = mainHour.PicAlt;

                            }
                            if (View.IsSaveComboDeals_NotDeliveryIslands)
                            {
                                //無法配送外島     
                                entity.Deal.BusinessHourStatus =
                                    (int)
                                    Helper.SetFlag(
                                        View.NotDeliveryIslands,
                                        entity.Deal.BusinessHourStatus,
                                        BusinessHourStatus.NotDeliveryIslands);

                            }
                            if (View.IsSaveComboDeals_AccBusGroup)
                            {
                                //館別
                                /*
                             * Table : deal_property
                             * Column :deal_acc_business_group_id
                             * */
                                dealCopiedProperty.DealAccBusinessGroupId = View.AccBusinessGroupNo;

                            }
                            if (View.IsSaveComboDeals_SalesBelong)
                            {
                                //業績歸屬
                                /*
                             * Table :deal_accounting
                             * Column :dept_id
                             * */
                                dealCopiedAccouning.DeptId = mainAccounting.DeptId;

                            }
                            if (View.IsSaveComboDeals_DealType)
                            {
                                //分類
                                /*
                             * Table :deal_property
                             * Column :deal_type
                             * */
                                dealCopiedProperty.DealType = mainProperty.DealType;
                                dealCopiedProperty.DealTypeDetail = mainProperty.DealTypeDetail;
                            }
                            if (View.IsSaveComboDeals_EntrustSell)
                            {
                                //收據資料
                                /*
                             * Table :deal_property
                             * Column : entrust_sell
                             * */
                                dealCopiedProperty.EntrustSell = (int)View.EntrustSell;
                            }

                            if (View.IsSaveComboDeals_IsLongContract)
                            {
                                //長期約
                                dealCopiedProperty.IsLongContract = View.IsLongContract;
                            }

                            if (View.IsSaveComboDeals_PaySet)
                            {
                                //核銷與帳戶設定
                                /*
                             * Table :deal_accounting
                             * Column : paytocompany(匯款資料)
                             * Table :deal_accounting
                             * Column : vendor_billing_model(對帳方式)
                             * Table :deal_accounting
                             * Column : remittance_type(出帳方式)
                             * Table :deal_accounting
                             * Column : vendor_receipt_type/message(店家單據開立方式)
                             * Table :deal_accounting
                             * Column : is_input_tax_required(免稅設定)
                             * */

                                dealCopiedAccouning.Paytocompany = mainAccounting.Paytocompany;
                                dealCopiedAccouning.VendorBillingModel = mainAccounting.VendorBillingModel;
                                dealCopiedAccouning.RemittanceType = mainAccounting.RemittanceType;
                                dealCopiedAccouning.VendorReceiptType = mainAccounting.VendorReceiptType;
                                dealCopiedAccouning.Message = mainAccounting.Message;
                                dealCopiedAccouning.IsInputTaxRequired = mainAccounting.IsInputTaxRequired;

                                orderGo.Status =
                                    (int)Helper.SetFlag(View.NoTax, orderGo.Status ?? 0, GroupOrderStatus.NoTax);

                            }
                            if (pro.Id == 0 || pro.OrderTimeS != null)
                            {
                                if (View.IsSaveComboDeals_OS)
                                {
                                    //活動搶購時間
                                    /*
                                 * Table :business_hour
                                 * Column :business_hour_order_time_s
                                 * */
                                    entity.Deal.BusinessHourOrderTimeS = View.OrderTimeS;

                                }
                                if (View.IsSaveComboDeals_OE)
                                {
                                    //活動搶購時間
                                    /*
                                 * Table :business_hour
                                 * Column :business_hour_order_time_e
                                 * */
                                    if (View.OrderTimeE > entity.Deal.BusinessHourOrderTimeE
                                        && View.OrderTimeE > DateTime.Now)
                                    {
                                        if (!dealCopiedProperty.IsExpiredDeal)
                                        {
                                            //非提案單刪除的檔次，才可以復原
                                            if (!RecoverDeal(dealGuid))
                                            {
                                                //View.ShowMessage("嘗試自動進行[復原結檔]出現異常，請洽技術部！");
                                                //return;
                                            }
                                            else
                                            {
                                                //View.ShowMessage("自動進行[復原結檔]成功。");
                                            }
                                        }
                                    }
                                    entity.Deal.BusinessHourOrderTimeE = View.OrderTimeE;

                                }
                                if (View.IsSaveComboDeals_UseDiscount)
                                {
                                    entity.Deal.BusinessHourStatus = (int)Helper.SetFlag(View.LowGrossMarginAllowedDiscount, entity.Deal.BusinessHourStatus, BusinessHourStatus.LowGrossMarginAllowedDiscount);
                                    if (View.NotAllowedDiscount)
                                    {
                                        PromotionFacade.DiscountLimitSet(entity.Deal.Guid, View.UserName, DiscountLimitType.Enabled);
                                    }
                                }
                                if (View.IsSaveComboDeals_DS)
                                {
                                    //活動商品有效期限/開始配送日期
                                    /*
                                 * Table :business_hour
                                 * Column :business_hour_deliver_time_s/business_hour_deliver_time_e
                                 * */
                                    entity.Deal.BusinessHourDeliverTimeS = View.DeliverTimeS;
                                }
                                if (View.IsSaveComboDeals_DE)
                                {
                                    //活動商品有效期限/開始配送日期
                                    /*
                                 * Table :business_hour
                                 * Column :business_hour_deliver_time_s/business_hour_deliver_time_e
                                 * */
                                    entity.Deal.BusinessHourDeliverTimeE = View.DeliverTimeE;
                                }
                            }

                            if (View.IsSaveComboDeals_ChangedExpireDate)
                            {
                                //調整有效期限截止日
                                /*
                             * Table :business_hour
                             * Column :changed_expire_date
                             * */
                                bool dealChangedExpireDate = !entity.Deal.ChangedExpireDate.HasValue
                                                             && data.Deal.ChangedExpireDate.HasValue;

                                if (entity.Deal.ChangedExpireDate.HasValue && data.Deal.ChangedExpireDate.HasValue)
                                {
                                    if (DateTime.Compare(
                                        entity.Deal.ChangedExpireDate.Value,
                                        data.Deal.ChangedExpireDate.Value) != 0)
                                    {
                                        dealChangedExpireDate = true;
                                    }
                                }

                                if (dealChangedExpireDate)
                                {
                                    entity.Deal.BusinessHourStatus =
                                        (int)
                                        Helper.SetFlag(
                                            true,
                                            entity.Deal.BusinessHourStatus,
                                            BusinessHourStatus.FinalExpireTimeChange);
                                }

                                entity.Deal.ChangedExpireDate = View.ChangedExpireDate;
                                entity.Deal.SettlementTime = View.SettlementTime;

                                if (dealChangedExpireDate)
                                {
                                    SendExpireDateChangedMail(
                                        sellerName,
                                        data.DealContent.Name,
                                        entity.Property.UniqueId,
                                        entity.Deal);
                                    EmailFacade.SendDealChangeExpiredDateNoticeToMember(
                                        entity.Deal.BusinessHourDeliverTimeE.Value,
                                        entity.Deal.ChangedExpireDate.Value,
                                        entity.Deal.Guid,
                                        View.ItemName);
                                }


                            }
                            if (View.IsSaveComboDeals_NoRefund)
                            {
                                //不接受退貨(捐款檔次)
                                /*
                             * Table :group_order
                             * Column :status
                             * */
                                orderGo.Status =
                                    (int)Helper.SetFlag(View.NoRefund, orderGo.Status ?? 0, GroupOrderStatus.NoRefund);
                                orderGo.Status =
                                    (int)Helper.SetFlag(View.IsKindDeal, orderGo.Status ?? 0, GroupOrderStatus.KindDeal);
                            }
                            if (View.IsSaveComboDeals_DaysNoRefund)
                            {
                                //結檔後七天不能退貨
                                /*
                             * Table :group_order
                             * Column :status
                             * */
                                orderGo.Status =
                                    (int)
                                    Helper.SetFlag(
                                        View.DaysNoRefund,
                                        orderGo.Status ?? 0,
                                        GroupOrderStatus.DaysNoRefund);
                            }
                            if (View.IsSaveComboDeals_NoRefundBeforeDays)
                            {
                                //演出時間前十日內不能退貨
                                /*
                             * Table :group_order
                             * Column :status
                             * */
                                orderGo.Status =
                                    (int)
                                    Helper.SetFlag(
                                        View.NoRefundBeforeDays,
                                        orderGo.Status ?? 0,
                                        GroupOrderStatus.NoRefundBeforeDays);
                            }
                            if (View.IsSaveComboDeals_ExpireNoRefund)
                            {
                                //過期不能退貨
                                /*
                             * Table :group_order
                             * Column :status
                             * */
                                orderGo.Status =
                                    (int)
                                    Helper.SetFlag(
                                        View.ExpireNoRefund,
                                        orderGo.Status ?? 0,
                                        GroupOrderStatus.ExpireNoRefund);
                            }
                            //廠商提供序號活動
                            if (View.IsSaveComboDealsPeZevent)
                            {
                                //廠商提供序號活動
                                orderGo.Status = (int)Helper.SetFlag(View.PEZevent, orderGo.Status ?? 0, GroupOrderStatus.PEZevent);
                                //序號綁UserId
                                orderGo.Status = (int)Helper.SetFlag(View.PEZeventWithUserId, orderGo.Status ?? 0, GroupOrderStatus.PEZeventWithUserId);
                                //單一序號
                                dealCopiedProperty.PinType = (View.IsSinglePinCode) ? (int)PinType.Single : (int)PinType.Normal;
                                //自訂簡訊每n碼加入-分隔
                                dealCopiedProperty.CouponSeparateDigits = mainProperty.CouponSeparateDigits;
                            }
                            if (View.IsSaveComboDeals_PEZeventCouponDownload)
                            {
                                //0元或廠商提序號檔次，提供下載序號(不含簡訊)
                                /*
                             * Table :group_order
                             * Column :status
                             * */
                                orderGo.Status =
                                    (int)
                                    Helper.SetFlag(
                                        View.PEZeventCouponDownload,
                                        orderGo.Status ?? 0,
                                        GroupOrderStatus.PEZeventCouponDownload);
                            }
                            //咖啡寄杯
                            if (View.IsSaveComboDealsDepositCoffee)
                            {
                                dealCopiedProperty.IsDepositCoffee = mainProperty.IsDepositCoffee;
                            }

                            //反向核銷
                            if (View.IsSaveComboDealsVerifyActionType)
                            {
                                dealCopiedProperty.VerifyActionType = mainProperty.VerifyActionType;
                            }

                            if (View.IsSaveComboDeals_ReserveLock)
                            {
                                //住宿類，商家系統提供「訂房鎖定」功能
                                /*
                             * Table :deal_property
                             * Column : is_reserve_lock
                             * */
                                dealCopiedProperty.IsReserveLock = mainProperty.IsReserveLock;
                            }
                            if (View.IsSaveComboDeals_BuyoutCoupon)
                            {
                                entity.Deal.BusinessHourStatus =
                                    (int)
                                    Helper.SetFlag(
                                        View.BuyoutCoupon,
                                        entity.Deal.BusinessHourStatus,
                                        BusinessHourStatus.BuyoutCoupon);
                            }
                            //分期子檔同步
                            if (View.IsSaveComboDeals_DonotInstallment)
                            {
                                dealCopiedProperty.DenyInstallment = mainProperty.DenyInstallment;
                                dealCopiedProperty.Installment3months = mainProperty.DenyInstallment ? false : dealCopiedProperty.Installment3months;
                                dealCopiedProperty.Installment6months = mainProperty.DenyInstallment ? false : dealCopiedProperty.Installment6months;
                                dealCopiedProperty.Installment12months = mainProperty.DenyInstallment ? false : dealCopiedProperty.Installment12months;

                            }
                            //出貨資訊
                            if (View.IsSaveComboDeals_ShipType)
                            {
                                dealCopiedProperty.ShipType = mainProperty.ShipType;
                                dealCopiedProperty.ShippingdateType = mainProperty.ShippingdateType;
                                dealCopiedProperty.Shippingdate = mainProperty.Shippingdate;
                                dealCopiedProperty.ProductUseDateStartSet = mainProperty.ProductUseDateStartSet;
                                dealCopiedProperty.ProductUseDateEndSet = mainProperty.ProductUseDateEndSet;
                            }

                            //限制付款方式
                            if (!dealPayment.IsLoaded)
                            {
                                dealPayment.BusinessHourGuid = dealGuid;
                            }
                            dealPayment.BankId = (int)View.LimitCreditCardBankId;
                            dealPayment.IsBankDeal = View.IsBankDeal;
                            dealPayment.AllowGuestBuy = View.AllowGuestBuy;

                            //businessCopiedHour

                            bool success = _pponProv.PponDealSet(entity, false);
                            if (success)
                            {
                                //儲存業務資料
                                dealCopiedProperty.CategoryList = NewtonsoftJson.JsonConvert.SerializeObject(cds.Select(x => x.Cid).Distinct().ToList(), NewtonsoftJson.Formatting.None);
                                _pponProv.DealAccountingSet(dealCopiedAccouning);
                                _pponProv.DealPropertySet(dealCopiedProperty);

                                //儲存付款方式
                                _pponProv.DealPaymentSet(dealPayment);

                                //寫入CategoryDeal
                                if (View.IsSaveComboDeals_Channel)
                                {
                                    //有勾才處理
                                    if (cds != null)
                                    {
                                        PponFacade.SaveCategoryDeals(cds, dealGuid);
                                    }
                                }


                                _orderProv.GroupOrderSet(orderGo);

                                //寫入子檔change_log viewppondeal 用以觀察是否同步異常才導致加時未同步問題
                                ViewPponDeal ppon = _pponProv.ViewPponDealGetByBusinessHourGuid(dealGuid);
                                string s = ViewPponDealToJson(ppon);
                                _pponProv.ChangeLogInsert("ViewPponDeal", dealGuid.ToString(), s);

                                processBuilder.AppendLine(
                                    "母子檔同步(success) = " + dealGuid.ToString() + "/" + success.ToString() + "<br/>");
                            }
                            else
                            {
                                processBuilder.AppendLine(
                                    "母子檔同步(success) = " + dealGuid.ToString() + "/" + success.ToString() + "<br/>");
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    var jsonS = new JsonSerializer();
                    log4net.LogManager.GetLogger("LogToDb")
                        .Error(
                            string.Format(
                                "PponSetupPresenter.cs SaveComboDeals,error:Exception:{0},login name:{1}",
                                ex.Message,
                                HttpContext.Current.User.Identity.Name));
                }
            }
        }

        /// <summary>
        /// 修改母檔時，同步修正子檔的付款帳戶
        /// </summary>
        /// <param name="cloneDealGuids"></param>
        /// <param name="mainEntity"></param>
        private void UpdateDealAccount(List<Guid> cloneDealGuids, PponDeal mainEntity)
        {
            if (cloneDealGuids.Count > 0)
            {
                foreach (Guid dealGuid in cloneDealGuids)
                {
                    var accounts = _orderProv.WeeklyPayAccountGetList(dealGuid);
                    var insStores = _pponProv.PponStoreGetListByBusinessHourGuid(dealGuid)
                                        .Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting));

                    //對應ppon_store數量儲存 另抓取store 取得匯款帳號資料
                    var stores = _sellerProv.SellerGetList(insStores.Select(x => x.StoreGuid).ToList());
                    foreach (var store in stores)
                    {
                        var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                                    ?? new WeeklyPayAccount
                                    {
                                        BusinessHourGuid = dealGuid,
                                        CreateDate = System.DateTime.Now,
                                        Creator = View.UserName
                                    };
                        account.StoreGuid = store.Guid;
                        account.EventName = (mainEntity.DealContent.Name.Length > 100)
                                                ? mainEntity.DealContent.Name.Substring(0, 100) + "..."
                                                : mainEntity.DealContent.Name;
                        account.CompanyName = store.CompanyName;
                        account.SellerName = store.CompanyBossName;
                        account.AccountId = store.CompanyID;
                        account.AccountName = store.CompanyAccountName;
                        account.AccountNo = store.CompanyAccount;
                        account.Email = store.CompanyEmail;
                        account.BankNo = store.CompanyBankCode;
                        account.BranchNo = store.CompanyBranchCode;
                        account.Message = store.CompanyNotice;
                        account.SignCompanyID = store.SignCompanyID;

                        accounts.Add(account);
                    }

                    _orderProv.WeeklyPayAccountSetList(accounts);
                }
            }
        }

        /// <summary>
        /// 修改母檔時，同步修正子檔的成套票券類型
        /// </summary>
        /// <param name="cloneDealGuids"></param>
        /// <param name="type"></param>
        private void UpdateGroupCouponType(List<Guid> cloneDealGuids, int type)
        {
            if (cloneDealGuids.Count > 0)
            {
                foreach (Guid dealGuid in cloneDealGuids)
                {
                    DealProperty dp = _pponProv.DealPropertyGet(dealGuid);
                    dp.GroupCouponDealType = type;
                    _pponProv.DealPropertySet(dp);
                }
            }
        }

        /// <summary>
        /// 修改母檔時，同步修正子檔的商品寄倉
        /// </summary>
        /// <param name="cloneDealGuids"></param>
        /// <param name="isconsignment"></param>
        private void UpdateConsignment(List<Guid> cloneDealGuids, bool isconsignment)
        {
            if (cloneDealGuids.Count > 0)
            {
                foreach (Guid dealGuid in cloneDealGuids)
                {
                    DealProperty dp = _pponProv.DealPropertyGet(dealGuid);
                    dp.Consignment = isconsignment;
                    _pponProv.DealPropertySet(dp);
                }
            }
        }

        /// <summary>
        /// 更新展演票券狀態
        /// </summary>
        /// <param name="cloneDealGuids"></param>
        /// <param name="isPromotionDeal">是否為展演票券</param>
        /// <param name="isExhibitionDeal">是否為展覽票券</param>
        private void UpdatePromotionDeal(List<Guid> cloneDealGuids, bool isPromotionDeal, bool isExhibitionDeal)
        {
            if (cloneDealGuids.Count > 0)
            {
                foreach (Guid dealGuid in cloneDealGuids)
                {
                    DealProperty dp = _pponProv.DealPropertyGet(dealGuid);
                    dp.IsPromotionDeal = isPromotionDeal;
                    dp.IsExhibitionDeal = isExhibitionDeal;
                    dp.AgentChannels = View.AgentChannels;
                    _pponProv.DealPropertySet(dp);
                }
            }
        }

        private void OnCopyDeal(object sender, ComboDealEventArgs e)
        {
            bool isCopyAsSubDeal = e.IsCopyAsSubDeal;
            bool isCopyComboDeals = e.IsCopyComboDeals;

            string errorMessages;
            Guid returnNewGuid;
            PponFacade.CopyDeal(View.BusinessHourId, isCopyAsSubDeal, isCopyComboDeals, out errorMessages, out returnNewGuid, View.UserName);

            string changeType = (!isCopyAsSubDeal && !isCopyComboDeals) ? " 複製此檔好康 " : (isCopyAsSubDeal ? " 複製成子檔 " : (isCopyComboDeals ? " 打包複製 " : ""));
            _pponProv.ChangeLogInsert("CopyDeal", View.BusinessHourId.ToString(), ChangeItemsToJson(new KeyValuePair<string, string>("Bid: " + View.BusinessHourId.ToString(), changeType + "至Bid:" + returnNewGuid.ToString())));

            if (!string.IsNullOrWhiteSpace(errorMessages))
            {
                View.ShowMessage(errorMessages);
            }

            if (returnNewGuid != Guid.Empty)
            {
                //回傳完成訊息與產生的BusinessHourGuid資料。
                View.CopyDealLink(returnNewGuid);
            }

            LoadDeal(View.BusinessHourId);
        }

        private bool CheckInput(PponSetupViewModel data, out string checkInputErrorMsg)
        {
            checkInputErrorMsg = string.Empty;
            PponDeal pponDeal = data.Deal;
            //檢查選取的頻道與地區
            Dictionary<int, List<int>> selectedChannelCategories = View.SelectedChannelCategories;
            //儲存依據新版頻道及區域 對應的 舊版cityId
            foreach (var selectedChannel in selectedChannelCategories)
            {
                CategoryNode channel =
                    CategoryManager.PponChannelCategoryTree.CategoryNodes.FirstOrDefault(x => x.CategoryId == selectedChannel.Key);
                //選取的頻道不存在
                if (channel == default(CategoryNode))
                {
                    checkInputErrorMsg = "頻道與區域預設資訊有誤，請重新整理頁面後再進行動作。";
                    return false;
                }
                //選取的頻道是否有區域(PponChannelArea)的分類需要選取
                List<CategoryNode> subNodes = channel.GetSubCategoryTypeNode(CategoryType.PponChannelArea);
                bool haveArea = (subNodes != null && subNodes.Count > 0);
                //有選取子項目
                bool haveSelectedArea = selectedChannel.Value.Count > 0;
                //有區域分類 卻 沒有選取子分類
                if (haveArea && !haveSelectedArea)
                {
                    checkInputErrorMsg = string.Format("請勾選{0}區域", channel.NameInConsole);
                    return false;
                }

                if (channel.CategoryId == CategoryManager.Default.Travel.CategoryId && string.IsNullOrEmpty(pponDeal.DealContent.TravelEdmSpecialImagePath) && data.UploadedFile == null)
                {
                    checkInputErrorMsg = string.Format("您還未上傳旅遊eDM專屬大圖");
                    //return false;
                }

            }

            if (View.IsMutualInvoice && View.Commission <= 0)
            {
                checkInputErrorMsg = "若選擇 發票對開 , 則傭金必須大於零";
                return false;
            }

            if (string.IsNullOrEmpty(View.AncestorBusinessHourGuid))
            {
                if (View.Is_Continued_Quantity)
                {
                    checkInputErrorMsg = "未填入接續數量檔次BID";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(View.AncestorSequenceBusinessHourGuid))
            {
                if (View.Is_Continued_Sequence)
                {
                    checkInputErrorMsg = "未填入接續憑證檔次Bid";
                    return false;
                }
            }

            if (View.Is_Continued_Quantity)
            {
                if (string.IsNullOrEmpty(View.AncestorBusinessHourGuid))
                {
                    checkInputErrorMsg = "未填入接續數量檔次BID";
                    return false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(View.AncestorBusinessHourGuid))
                {
                    checkInputErrorMsg = "請勾選「接續銷售數字(前台顯示)」";
                    return false;
                }
            }
            if (View.Is_Continued_Sequence)
            {
                if (string.IsNullOrEmpty(View.AncestorSequenceBusinessHourGuid))
                {
                    checkInputErrorMsg = "未填入接續憑證檔次Bid";
                    return false;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(View.AncestorSequenceBusinessHourGuid))
                {
                    checkInputErrorMsg = "請勾選「接續憑證號碼」";
                    return false;
                }
            }
            if (_config.IsRemittanceFortnightly && View.IsAgreePponNewContractSeller)
            {
                if (View.TheDeliveryType == DeliveryType.ToShop || (View.TheDeliveryType == DeliveryType.ToHouse && View.DeptId != EmployeeChildDept.S010.ToString()))
                {
                    if (View.SellerRemittanceType != (int)View.PaidType)
                    {
                        checkInputErrorMsg = "請確認，檔次與商家管理內的出帳方式需一致";
                    }
                }
            }




            if (View.Is_Continued_Quantity || View.Is_Continued_Sequence)
            {
                Guid ancBid = Guid.Empty;
                Guid ancSeqBid = Guid.Empty;
                if (View.Is_Continued_Quantity)
                {
                    if (Guid.TryParse(View.AncestorBusinessHourGuid, out ancBid))
                    {
                    }
                    else
                    {
                        checkInputErrorMsg = "接續檔次的 bid 格式錯誤";
                        return false;
                    }
                }

                if (View.Is_Continued_Sequence)
                {
                    if (Guid.TryParse(View.AncestorSequenceBusinessHourGuid, out ancSeqBid))
                    {

                    }
                    else
                    {
                        checkInputErrorMsg = "接續檔次的 bid 格式錯誤";
                        return false;
                    }
                }


                string errormessage = CheckAncestorBusinessHourGuid(ancBid, ancSeqBid, pponDeal, true);
                if (!string.IsNullOrEmpty(errormessage))
                {
                    checkInputErrorMsg = errormessage;
                    return false;
                }
            }



            return true;
        }

        private bool CheckRequired(out string checkErrorMsg)
        {
            if (View.AccBusinessGroupNo == -1)
            {
                checkErrorMsg = "請選擇館別";
                return false;
            }

            if (View.DealTypeNewValue.EqualsAny(0, -1))
            {
                checkErrorMsg = "請選擇分類選項";
                return false;
            }

            if (View.DeptId == "-1")
            {
                checkErrorMsg = "請選擇 銷售分析 > 業績歸屬";
                return false;
            }

            checkErrorMsg = string.Empty;
            return true;
        }

        /// <summary>
        /// 檢查接續檔次
        /// </summary>
        /// <param name="ancBid"></param>
        /// <param name="ancSeqBid"></param>
        /// <param name="data"></param>
        /// <param name="existedCheck"></param>
        /// <returns></returns>
        private string CheckAncestorBusinessHourGuid(Guid ancBid, Guid ancSeqBid, PponDeal data, bool existedCheck)
        {
            string errormessage = string.Empty;
            //檢查目前BusinessHour存不存在
            ViewPponDeal ancDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(ancBid);         //接續數量
            ViewPponDeal ancSeqDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(ancSeqBid);   //接續憑證
            ViewPponDeal selfDeal = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);   //Self

            if (null == ancDeal && !ancDeal.IsLoaded)
            {
                if (View.Is_Continued_Quantity)
                {
                    errormessage = "接續數量檔次的 bid : " + ancBid + " 好康不存在。";
                }
            }
            else if (null == ancSeqDeal && !ancSeqDeal.IsLoaded)
            {
                if (View.Is_Continued_Sequence)
                {
                    errormessage = "接續憑證檔次的 bid : " + ancSeqBid + " 好康不存在。";
                }
            }
            else
            {
                // 檢查檔次開檔時間是否重疊
                // 拿掉日期判斷，因為結檔時會自動更新接續檔的續接數量
                if (View.Is_Continued_Quantity)
                {
                    if (DateTime.Compare(data.Deal.BusinessHourOrderTimeS, ancDeal.BusinessHourOrderTimeE) < 0)
                    {
                        //errormessage += "續接數量檔次結檔日期晚於此檔開檔日期。";
                    }
                }
                if (View.Is_Continued_Sequence)
                {
                    if (DateTime.Compare(data.Deal.BusinessHourOrderTimeS, ancSeqDeal.BusinessHourOrderTimeE) < 0)
                    {
                        //errormessage += "續接憑證檔次結檔日期晚於此檔開檔日期。";
                    }
                }


                if (existedCheck)
                {
                    DealPropertyCollection dpc = _pponProv.DealPropertyGetByAncestor(ancBid);
                    if (dpc.Count > 0)
                    {
                        if (!int.Equals(1, dpc.Count) || Guid.Equals(Guid.Empty, View.BusinessHourId) || !Guid.Equals(dpc[0].BusinessHourGuid, View.BusinessHourId))
                        {
                            //變更：刪除同一檔只能接續一次的限制
                            //errormessage += "續接檔次已被其他檔次接續。";
                        }
                    }
                }

                if (!View.Is_Continued_Quantity && !View.Is_Continued_Sequence)
                {
                    errormessage += "請至少選擇一項接續種類。";
                }

                if (View.QuantityMultiplier.HasValue != ancDeal.QuantityMultiplier.HasValue || ((View.QuantityMultiplier ?? 0) != (ancDeal.QuantityMultiplier ?? 0)))
                {
                    //變更：不限銷售倍數相同
                    //errormessage += "接續檔次銷售倍數不一致。";
                }
                //檢查前一檔
                if (ancBid != Guid.Empty)
                {
                    if (View.BusinessHourId == ancBid)
                    {
                        errormessage += "接續銷售數字(前台顯示)不可重複續接回自己。";
                    }
                }
                if (ancSeqBid != Guid.Empty)
                {
                    if (View.BusinessHourId == ancSeqBid)
                    {
                        errormessage += "接續憑證號碼不可重複續接回自己。";
                    }
                }
                //遞迴檢查
                if (View.Is_Continued_Quantity)
                {
                    if (!PponFacade.CheckPponDealContinuedSelf(View.BusinessHourId, ancBid))
                    {
                        errormessage += "接續檔不可重複續接回自己。";
                    }
                }


                if (View.Is_Continued_Quantity)
                {
                    if (Helper.IsFlagSet(ancDeal.BusinessHourStatus, BusinessHourStatus.ComboDealSub) && View.Is_Continued_Quantity)
                    {
                        // 2013/12/11 接續銷售數字 改為必須接續母檔
                        errormessage += "接續銷售數字(前台顯示) 不可接續子檔。";
                    }
                }
                if (View.Is_Continued_Sequence)
                {
                    if (Helper.IsFlagSet(ancSeqDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain) && View.Is_Continued_Sequence)
                    {
                        errormessage += "接續憑證號碼 不可接續母檔。";
                    }
                    if (Helper.IsFlagSet(selfDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain) && View.Is_Continued_Sequence)
                    {
                        errormessage += "多檔次母檔不可續接憑證號碼。";
                    }
                }

                if (View.Is_Continued_Quantity)
                {
                    ancQuantity = PponFacade.GetPponDealContinuedQuantity(ancBid, Helper.IsFlagSet(ancDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                }
            }
            return errormessage;
        }

        private void UploadSpecialImageFile(HttpPostedFile uploadedFile, ref PponDeal entity)
        {
            if (uploadedFile != null)
            {
                string fileName = "SP" + DateTime.Now.Ticks.ToString();
                string fileNameWithExtension = fileName + "." +
                                               Helper.GetExtensionByContentType(uploadedFile.ContentType);
                ImageUtility.UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, entity.Store.SellerId, fileName,
                                        View.IsPrintWatermark);


                entity.DealContent.SpecialImagePath = Helper.AppendRawDataWithRawPath(entity.DealContent.SpecialImagePath,
                                                                               ImageFacade.GenerateMediaPath(
                                                                                   entity.Store.SellerId,
                                                                                   fileNameWithExtension));

            }
        }
        private void UploadTravelEDMSpecialPicFile(HttpPostedFile uploadedFile, ref PponDeal entity)
        {
            if (uploadedFile != null)
            {
                string fileName = "TravelEDMSpecial" + entity.DealContent.BusinessHourGuid.ToString().Replace("-", "EDM");

                string fileNameWithExtension = fileName + "." +
                                               Helper.GetExtensionByContentType(uploadedFile.ContentType);

                ImageUtility.UploadFile(uploadedFile.ToAdapter(), UploadFileType.PponEvent, entity.Store.SellerId, fileName,
                                        View.IsPrintWatermark);

                //only one pic
                entity.DealContent.TravelEdmSpecialImagePath = Helper.AppendRawDataWithRawPath(string.Empty,
                                                                               ImageFacade.GenerateMediaPath(
                                                                                   entity.Store.SellerId,
                                                                                   fileNameWithExtension));
            }
        }

        /// <summary>
        /// 上傳App正方型圖
        /// </summary>
        /// <param name="tempImageFileName"></param>
        /// <param name="rectangleToCrop"></param>
        /// <param name="entity"></param>
        private void UploadAppDealPic(string tempImageFileName, Rectangle rectangleToCrop, ref PponDeal entity)
        {
            if (string.IsNullOrEmpty(tempImageFileName))
            {
                entity.DealContent.AppDealPic = "";
            }
            if (ImageFacade.GetImageSizeFast(tempImageFileName).X > CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM)
            {
                ImageFacade.CropImage(tempImageFileName, tempImageFileName, rectangleToCrop);
            }
            ImageUtility.UploadFile(new PhysicalPostedFileAdapter(tempImageFileName), UploadFileType.PponEvent, entity.Store.SellerId,
                Path.GetFileNameWithoutExtension(tempImageFileName), false);
            string imagePath = ImageFacade.GenerateMediaPath(entity.Store.SellerId, Path.GetFileName(tempImageFileName));
            entity.DealContent.AppDealPic = string.Format("{0}?ts={1}", imagePath, Helper.GetCurrentTimeStampHashCode());
        }

        private void SendExpireDateChangedMail(string sellerName, string dealName, int UniqueId, BusinessHour deal)
        {
            CouponExpireDateChangedInformation info = new CouponExpireDateChangedInformation();
            info.SellerName = sellerName;
            info.DealName = dealName;
            info.DealUrl = string.Format(_config.SiteUrl + "/{0}", deal.Guid);
            info.UniqueId = UniqueId;

            if (deal.BusinessHourDeliverTimeS.HasValue)
            {
                info.CouponUseStartDate = deal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd");
            }

            if (deal.BusinessHourDeliverTimeE.HasValue)
            {
                info.CouponUseOriginalExpireDate = deal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
            }

            if (deal.ChangedExpireDate.HasValue)
            {
                info.ChangedExpireDate = deal.ChangedExpireDate.Value.ToString("yyyy/MM/dd");
            }

            Member mem = _memProv.MemberGet(View.UserName);

            HiDealMailFacade.SendCouponExpireDateChangedNotification(mem.UserEmail, mem.DisplayName, info);
        }

        private void SendStoreExpireDateChangedMail(string sellerName, string dealName, int UniqueId, BusinessHour deal, string storeName, DateTime? changedExpireDate)
        {
            CouponExpireDateChangedInformation info = new CouponExpireDateChangedInformation();
            info.SellerName = sellerName;
            info.StoreName = storeName;
            info.DealName = dealName;
            info.DealUrl = string.Format(_config.SiteUrl + "/", deal.Guid);
            info.UniqueId = UniqueId;

            if (deal.BusinessHourDeliverTimeS.HasValue)
            {
                info.CouponUseStartDate = deal.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd");
            }

            if (deal.BusinessHourDeliverTimeE.HasValue)
            {
                info.CouponUseOriginalExpireDate = deal.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd");
            }

            if (changedExpireDate.HasValue)
            {
                info.ChangedExpireDate = changedExpireDate.Value.ToString("yyyy/MM/dd");
            }

            Member mem = _memProv.MemberGet(View.UserName);

            HiDealMailFacade.SendCouponExpireDateChangedNotification(mem.UserEmail, mem.DisplayName, info);
        }

        private void OnRreceiveType_TextChanged(object sender, EventArgs e)
        {
            SetupDealLabels();
            SetupUnhiddenDealSpecialCategory();
            SetupShoppingCartOption();
            SetupSellerTreeView(View.SellerId, View.TheDeliveryType == DeliveryType.ToHouse);
        }

        private bool ValidateLengthAccessoryGroupText(string content)
        {
            bool validateLength = true;
            int checkLength = _config.IsEnableVbsNewShipFile ? 150 : 50;

            foreach (var line in content.Split('\n').Select(x => x.Trim()))
            {
                if (line.Length >= checkLength) //validate 長度
                {
                    validateLength = false;
                }
            }

            return validateLength;
        }

        private static bool ValidateSellerTree(Guid sellerGuid, IEnumerable<PponSetupPponStore> stores, out string checkErrorMsg)
        {
            var verifyGuids = new List<Guid>();
            var accountingGuids = new List<Guid>();
            var viewBalalnceSheetGuids = new List<Guid>();
            var hideBalalnceSheetGuids = new List<Guid>();
            var verifyShopGuids = new List<Guid>();
            var hideVerifyShopGuids = new List<Guid>();
            var vbsRightInfo = new Dictionary<VbsRightFlag, IEnumerable<Guid>>();

            foreach (var store in stores)
            {
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Verify))
                {
                    verifyGuids.Add(store.StoreGuid);
                }
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Accouting))
                {
                    accountingGuids.Add(store.StoreGuid);
                }
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.ViewBalanceSheet))
                {
                    viewBalalnceSheetGuids.Add(store.StoreGuid);
                }
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller))
                {
                    hideBalalnceSheetGuids.Add(store.StoreGuid);
                }
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.VerifyShop))
                {
                    verifyShopGuids.Add(store.StoreGuid);
                }
                if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.HideFromVerifyShop))
                {
                    hideVerifyShopGuids.Add(store.StoreGuid);
                }
            }

            vbsRightInfo.Add(VbsRightFlag.Verify, verifyGuids);
            vbsRightInfo.Add(VbsRightFlag.Accouting, accountingGuids);
            vbsRightInfo.Add(VbsRightFlag.ViewBalanceSheet, viewBalalnceSheetGuids);
            vbsRightInfo.Add(VbsRightFlag.BalanceSheetHideFromDealSeller, hideBalalnceSheetGuids);
            vbsRightInfo.Add(VbsRightFlag.VerifyShop, verifyShopGuids);
            vbsRightInfo.Add(VbsRightFlag.HideFromVerifyShop, hideVerifyShopGuids);

            return SellerFacade.ValiadateSellerTreeSetUp(sellerGuid, vbsRightInfo, out checkErrorMsg);
        }

        private void OnGenerateCoupon(object sender, EventArgs e)
        {
            OrderFacade.GenerateCoupon(View.BusinessHourId);
            LoadDeal(View.BusinessHourId);
        }

        private void OnCreateOrder(object sender, EventArgs e)
        {
            //查詢GROUP_ORDER 看看是否此BID已有GROUP_ORDER紀錄
            GroupOrderCollection groupOrderList = _orderProv.GroupOrderGetList(GroupOrder.Columns.BusinessHourGuid + "=" + View.BusinessHourId);
            //若已有GroupOrder則不能新增，重新整理頁面後，直接離開function
            if (groupOrderList.Count > 0)
            {
                LoadDeal(View.BusinessHourId);
                return;
            }
            PponDeal deal = _pponProv.PponDealGet(View.BusinessHourId);// rewrite later
            OrderFacade.MakeGroupOrder(_config.ServiceEmail, _config.ServiceName, View.BusinessHourId, DateTime.Now,
                deal.Deal.BusinessHourOrderTimeE);
            LoadDeal(View.BusinessHourId);
        }

        private void OnDeleteDeal(object sender, EventArgs e)
        {
            _pponProv.PponDealDelete(View.BusinessHourId);
            View.JumpTo(PponSetupMode.DealNotFound, null);
        }

        private void OnCreateDealTimeSlot(object sender, EventArgs e)
        {
            //檢查是否已產生訂單
            //若已有產生訂單檢查是否已有展開的日期記錄，若有則讀取就紀錄顯示，若沒有依據搶購的起始截止日產生相關內容
        }

        protected void OnSetBBH(object sender, EventArgs e)
        {
            ViewPponDeal deal = _pponProv.ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + " = " + View.BusinessHourId);
            GroupOrder go = _orderProv.GroupOrderGet((Guid)deal.GroupOrderGuid);
            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.NotInBBH))
            {
                go.Status = (int)Helper.SetFlag(false, go.Status ?? 0, GroupOrderStatus.NotInBBH);
                _orderProv.GroupOrderSet(go);
            }
            else
            {
                go.Status = (int)Helper.SetFlag(true, go.Status ?? 0, GroupOrderStatus.NotInBBH);
                _orderProv.GroupOrderSet(go);
            }
            LoadDeal(View.BusinessHourId);
        }

        private void OnImportBusinessOrderInfo(object sender, DataEventArgs<KeyValuePair<string, int>> e)
        {
            string businessOrderId;
            int index = 0;
            if (e.Data.Key.Split('-').Length > 2)
            {
                // 串接子檔(ex: S001-000235-2，代表欲串接第二個檔次內容)
                businessOrderId = e.Data.Key.Split('-')[0] + "-" + e.Data.Key.Split('-')[1];
                index = int.TryParse(e.Data.Key.Split('-')[2], out index) ? index - 1 : 0;
            }
            else
            {
                businessOrderId = e.Data.Key;
            }
            BusinessOrder businessOrder = _salServ.GetBusinessOrder(businessOrderId);
            if (businessOrder != null)
            {
                if (businessOrder.Status < BusinessOrderStatus.Await)
                {
                    View.ShowMessage(I18N.Message.BusinessOrderStatusValid);
                }
                else
                {
                    VacationCollection recentHolidays = _pponProv.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));
                    SystemCode sc = _systemProv.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), Convert.ToInt32(businessOrder.DealType));
                    View.SetDealType(sc.CodeId, Convert.ToInt32(businessOrder.DealType), SystemCodeManager.GetDealType().ToList());
                    if (index > 0)
                    {
                        if (businessOrder.Deals.Count > index)
                        {
                            View.SetBusinesssOrderDeal(businessOrder, recentHolidays, index);
                        }
                        else
                        {
                            View.ShowMessage(string.Format("無效的檔次數，此工單僅{0}筆檔次，請填入1~{0}", businessOrder.Deals.Count));
                        }
                    }
                    else
                    {
                        View.SetBusinessOrderInfo(businessOrder, recentHolidays);
                    }
                    SetupUnhiddenDealSpecialCategory();
                }
            }
            else
            {
                View.ShowMessage(I18N.Message.NoBusinessOrderId);
            }
        }

        private void OnProposalImport(object sender, EventArgs e)
        {
            Guid bid = View.BusinessHourId;
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bid);
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub) && vpd.MainBid != null)
            {
                bid = vpd.MainBid.Value;
            }
            Proposal pro = _sellerProv.ProposalGet(Proposal.Columns.BusinessHourGuid, bid);
            if (pro.IsLoaded)
            {
                if (pro.OrderTimeS.HasValue)
                {
                    VacationCollection recentHolidays = _pponProv.GetVacationListByPeriod(DateTime.Now.AddMonths(-1), DateTime.Now.AddMonths(14));
                    LoadDeal(bid);
                    View.SetProposalInfo(pro, recentHolidays);
                }
                else
                {
                    View.ShowMessage("檔次尚未排檔。");
                }
            }
            else
            {
                View.ShowMessage("提案單不存在。");
            }
        }

        protected void OnManualDealClose(object sender, DataEventArgs<Guid> bid)
        {
            var resultMsg = new StringBuilder();
            var dealGuids = new List<Guid> { Guid.Parse(bid.ToString()) };
            var combodeals = _pponProv.GetViewComboDealByBid(Guid.Parse(bid.ToString()), false);
            dealGuids.AddRange(combodeals.Select(cd => cd.BusinessHourGuid));
            var deals = _pponProv.ViewPponDealGetByBusinessHourGuidList(dealGuids).ToList();
            var removeDealGuids = new List<Guid>();

            foreach (var deal in deals.Where(deal => DateTime.Compare(deal.BusinessHourOrderTimeE, DateTime.Now.AddMinutes(-5)) < 0 ||
                                                     DateTime.Compare(deal.BusinessHourOrderTimeE, DateTime.Now) > 0))
            {
                resultMsg.Append(string.Format("檔號[{0}]結檔時間須調整為距離現在時間五分鐘以內\\n", deal.UniqueId));
                removeDealGuids.Add(deal.BusinessHourGuid);
            }
            deals = deals.Where(x => !removeDealGuids.Contains(x.BusinessHourGuid)).ToList();

            if (deals.Any())
            {
                List<DealCloseResult> results = _dealClose.CloseDeal(deals, HttpContext.Current.User.Identity.Name);

                foreach (var item in results)
                {
                    var deal = deals.FirstOrDefault(x => x.BusinessHourGuid == item.BusinessHourGuid);
                    if (deal == null)
                    {
                        continue;
                    }
                    switch (item.Result)
                    {
                        case DealCloseResultStatus.AlreadyClose:
                            resultMsg.Append(string.Format("檔號[{0}]無須結檔\\n", deal.UniqueId));
                            break;

                        case DealCloseResultStatus.Success:
                            resultMsg.Append(string.Format("檔號[{0}]結檔成功\\n", deal.UniqueId));
                            break;

                        case DealCloseResultStatus.Fail:
                            resultMsg.Append(string.Format("檔號[{0}]結檔失敗，請聯絡系統管理員\\n", deal.UniqueId));
                            break;

                        default:
                            resultMsg.Append(string.Format("檔號[{0}]系統錯誤，請聯絡系統管理員\\n", deal.UniqueId));
                            break;
                    }
                }
            }

            if (resultMsg.Length > 0)
            {
                View.ShowMessage(resultMsg.ToString());
            }
            else
            {
                View.ShowMessage("無須結檔");
            }
            LoadDeal(View.BusinessHourId);
        }

        protected void OnSyncPChome1(object sender, DataEventArgs<Guid> bid)
        {
            PchomeChannelProd pchomeProd = _chProv.PChomeChannelProdGetByBid(Guid.Parse(bid.ToString()));
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(Guid.Parse(bid.ToString()));


            Guid mainBid = vpd.MainBid == null ? Guid.Parse(bid.ToString()) : Guid.Parse(vpd.MainBid.ToString());
            CouponEventContent content = _pponProv.CouponEventContentGetByBid(mainBid);

            bool priceResult = false;
            bool qtyResult = false;
            bool descResult = false;
            bool prodResult = false;
            string errMessage = "";

            //同步價格
            priceResult = ChannelFacade.SyncPChomePrice(pchomeProd, vpd.ItemPrice, vpd.ItemOrigPrice, out errMessage);

            //同步庫存
            int qty = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(vpd.OrderTotalLimit)) ? 0 : Convert.ToInt32(vpd.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
            if (qty > 1200)
                qty = 1200;
            qtyResult = ChannelFacade.SyncPChomeQty(pchomeProd, qty, out errMessage);

            //同步一姬(slogan)
            descResult = ChannelFacade.SyncPChomeEG(pchomeProd, content.Remark, vpd.BusinessHourDeliverTimeS.Value, (vpd.ChangedExpireDate.HasValue ? vpd.ChangedExpireDate.Value : vpd.BusinessHourDeliverTimeE.Value), out errMessage);

            //同步銷售口號(nick)
            prodResult = ChannelFacade.SyncPChomeProd(pchomeProd, vpd.ItemName, out errMessage);

            //同步
            if (priceResult && qtyResult && descResult && prodResult)
                View.ShowMessage("同步成功");
            else if (!priceResult)
                View.ShowMessage("價格同步失敗");
            else if (!qtyResult)
                View.ShowMessage("庫存同步失敗");
            else if (!descResult)
                View.ShowMessage("一姬說好康同步失敗");
            else if (!prodResult)
                View.ShowMessage("短標同步失敗");

        }

        protected void OnSyncPChome2(object sender, DataEventArgs<Guid> bid)
        {
            PchomeChannelProd pchomeProd = _chProv.PChomeChannelProdGetByBid(Guid.Parse(bid.ToString()));
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(Guid.Parse(bid.ToString()));

            Guid mainBid = vpd.MainBid == null ? Guid.Parse(bid.ToString()) : Guid.Parse(vpd.MainBid.ToString());
            CouponEventContent content = _pponProv.CouponEventContentGetByBid(mainBid);

            bool introResult = false;
            bool picResult = false;
            string errMessage = "";

            //同步圖文
            introResult = ChannelFacade.SyncDesc(pchomeProd, content.Description, content.Restrictions, content.Availability, out errMessage);

            //同步大圖
            picResult = ChannelFacade.SyncPChomePic(pchomeProd, content.AppDealPic, out errMessage);

            //同步
            if (introResult && picResult)
                View.ShowMessage("同步成功");
            else if (!introResult)
                View.ShowMessage("圖文同步失敗");
            else if (!picResult)
                View.ShowMessage("大圖同步失敗");


        }

        protected void OnCheckFlag(object sender, DataEventArgs<string> e)
        {
            int value = int.TryParse(e.Data.Split('|')[0], out value) ? value : 0;
            string col = e.Data.Split('|')[1];
            string text = e.Data.Split('|')[2];
            bool hasFlag = bool.TryParse(e.Data.Split('|')[3], out hasFlag) ? hasFlag : false;
            Proposal pro = _sellerProv.ProposalGet(View.ProposalId);
            ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
            ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);
            Seller s = _sellerProv.SellerGet(pro.SellerGuid);

            if (pro.IsLoaded && pro.BusinessHourGuid.HasValue && value != 0)
            {
                if (hasFlag)
                {
                    pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) ^ value);
                }
                else
                {
                    pro.SetColumnValue(col, Convert.ToInt32(pro.GetColumnValue(col)) | value);
                }

                pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                pro.ModifyId = View.UserName;
                pro.ModifyTime = DateTime.Now;
                _sellerProv.ProposalSet(pro);

                #region 若註記為上架確認，則寄送通知信予 好康客服或宅配客服權限人員

                if (text == Helper.GetLocalizedEnum((ProposalEditorFlag.Listing)) && Helper.IsFlagSet(pro.EditFlag, ProposalEditorFlag.Listing))
                {
                    List<string> mailToUser = new List<string>();
                    string deliverType;
                    if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString() && _humanProv.DepartmentGet(deSalesEmp.DeptId).ParentDeptId != EmployeeDept.M000.ToString())
                    {
                        deliverType = "好康";
                    }
                    else
                    {
                        deliverType = "宅配";
                    }

                    //開發業務
                    string sales = ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId);
                    if (!mailToUser.Contains(sales))
                        mailToUser.Add(sales);

                    //經營業務
                    sales = ProposalFacade.PponAssistantEmailGet(opSalesEmp.DeptId);
                    if (!mailToUser.Contains(sales))
                        mailToUser.Add(sales);

                    if (mailToUser.Count > 0)
                    {
                        string subject = string.Format("【{0}】 單號 NO.{1}【{2}】<{3}>", text, pro.Id, deliverType, deSalesEmp.EmpName);
                        SendEmail(mailToUser, deSalesEmp.EmpName, pro.Id, View.BusinessHourId, subject, s.SellerName);
                    }

                    #region 發送新版信件
                    if (pro.IsEarlierPageCheck && pro.OrderTimeS != null)
                    {
                        bool success = false;
                        string newSubject = string.Empty;
                        string newSender = _config.SystemEmail;
                        string mailContent = string.Empty;
                        string itemName = string.Empty;
                        string RemittanceType = string.Empty;
                        bool isToShop = false;
                        string preWorkDay = string.Empty;

                        //抓取資料檔
                        ViewProposalSeller vps = _sellerProv.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, pro.BusinessHourGuid.Value);

                        #region 檢查preWorkDay是否為假日，是的話再往前延一天                
                        int minDay = -1;
                        int preDay = (int)vps.OrderTimeS.Value.AddDays(minDay).DayOfWeek;
                        DateTime preWork = new DateTime();
                        if (preDay == 0)
                        {
                            minDay = minDay - 2;
                        }
                        else if (preDay == 6)
                        {
                            minDay = minDay - 1;
                        }
                        preWork = vps.OrderTimeS.Value.AddDays(minDay);

                        //檢查preWorkDay是否有在放假條件內
                        while (_pponProv.GetVacationByDate(preWork).IsLoaded)
                        {
                            preWork = preWork.AddDays(-1);
                        }

                        preWorkDay = string.Format("{0:yyyy\\/MM\\/dd}", preWork);
                        #endregion

                        //多檔次取主檔資料
                        Item item = _pponProv.ItemGetBySubDealBid(pro.BusinessHourGuid.Value);
                        if (item.IsLoaded)
                        {
                            itemName = item.ItemName;
                        }
                        else
                        {
                            Item mainItem = _pponProv.ItemGetByBid(pro.BusinessHourGuid.Value);
                            if (mainItem.IsLoaded)
                            {
                                itemName = mainItem.ItemName;
                            }
                        }

                        //出帳方式
                        DealAccounting dAccounting = _pponProv.DealAccountingGet(pro.BusinessHourGuid.Value);
                        if (dAccounting.IsLoaded)
                        {
                            RemittanceType = WayOut((RemittanceType)dAccounting.RemittanceType);
                        }

                        if (pro.DeliveryType == (int)DeliveryType.ToShop)
                        {
                            List<LunchKingSite.BizLogic.Facade.SellerAccountModel> sellerModel=PponFacade.GetSellerAccount(pro.BusinessHourGuid.Value);

                            isToShop = true;
                            newSubject = string.Format("【17Life上檔通知】{0}上檔【{1}】頁面確認、核銷帳密信", string.Format("{0:yyyy\\/MM\\/dd}", vps.OrderTimeS.Value), itemName);
                            mailContent = "您好：<br />17Life很榮幸且珍惜能與您有此次的合作機會，創意專員已針對商品特性及賣點，製作完成專屬於您的銷售頁面。<br />";
                            mailContent = mailContent + "獨家頁面即將於<font color='red'><b>" + string.Format("{0:yyyy\\/MM\\/dd HH:mm}", vps.OrderTimeS.Value) + "</b></font>上檔，我們希望於<font color='red'><b>" + preWorkDay + " 14:00</b></font>前向您完成確認工作，已呈現最完美的頁面。<br />";
                            mailContent = mailContent + "敬請確認以下四個部分，<font color='red'><b>標題、價格、好康權益說明、店家資訊</b></font>，是否與合約內容相符。<br />";
                            mailContent = mailContent + "<font color='red'><b>若資訊有任何問題，請來信聯繫業務/業助窗口，未反應則視為確認完成，感謝您的配合，預祝 銷售長紅！</b></font><br />";
                            mailContent = mailContent + "前台頁面：<a href='" + _config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "' target='_blank'>" + _config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "</a><br />";
                            mailContent = mailContent + "隨信附上核銷的網址、帳號、系統使用說明手冊及核銷紀錄表格，<br />";
                            mailContent = mailContent + "若臨時出現無法核銷、系統當機...等無法立即解決的問題，<br />";
                            mailContent = mailContent + "請務必紀錄客人的憑證編號、確認碼、姓名、聯絡電話等，以利後續查詢。<br /><br />";
                            mailContent = mailContent + "商家系統網址：<a href='" + _config.SSLSiteUrl + "/vbs' target='_blank'>" + _config.SSLSiteUrl + "/vbs</a><br /><br />";
                            mailContent = mailContent + "登入帳號：依原帳號密碼<br />";
                            mailContent = mailContent + "初始密碼：123456(限首次合作店家，若已有變更密碼者請延用舊密碼)<br /><br />";
                            mailContent = mailContent + "<table style='border-collapse:collapse;'><tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>商家名稱</td><td style='width:150px;border:1px solid black;'>商家預設帳號</td></tr>";
                            foreach (var seller in sellerModel)
                            {
                                mailContent = mailContent + "<tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>" + seller.sellerName + "</td><td style='width:150px;border:1px solid black;'>" + seller.accountId + "</td></tr>";
                            }
                            mailContent = mailContent + "</table>";
                            mailContent = mailContent + "提醒您！請務必即時核銷，兌換逾期/以退貨恕無法核銷，若造成損失由店家承擔！<br />";
                            mailContent = mailContent + "出帳方式：" + RemittanceType + "，並於對帳單產出後將發票開立寄回以免影響撥款。";
                        }
                        //宅配
                        else
                        {
                            newSubject = string.Format("【17Life合作上檔通知(頁面)】{0}上檔【{1}】", string.Format("{0:yyyy\\/MM\\/dd}", vps.OrderTimeS.Value), itemName);
                            mailContent = "您好：<br />17Life很榮幸且珍惜能與您有此次的合作機會，創意專員已針對商品特性及賣點，製作完成專屬於您的銷售頁面。<br />";
                            mailContent = mailContent + "獨家頁面將於<font color='red'><b>" + string.Format("{0:yyyy\\/MM\\/dd HH:mm}", vps.OrderTimeS.Value) + "</b></font>上檔，我們希望<font color='red'><b>" + preWorkDay + " 14:00</b></font>前向您完成確認工作，以呈現最完美的頁面。<br />";
                            mailContent = mailContent + "前台頁面：<a href='" + _config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "' target='_blank'>" + _config.SSLSiteUrl + "/deal/" + pro.BusinessHourGuid.Value + "</a><br />";
                            mailContent = mailContent + "若有任何問題請於" + preWorkDay + " 14:00前來信聯繫業務/業助窗口，為反應則視為確認完成。<br />";
                            mailContent = mailContent + "感謝您的配合，預祝 銷售長紅！<br />";
                            mailContent = mailContent + "提醒您！檔次上檔販售後請留意每日訂單相關資訊，並協助『準時出貨』。<br />";
                            mailContent = mailContent + "商家系統網址：<a href='" + _config.SSLSiteUrl + "/vbs' target='_blank'>" + _config.SSLSiteUrl + "/vbs</a><br /><br />";
                        }

                        success = EmailFacade.SendSetupPageMail(newSender, newSubject, mailContent, vps, isToShop, deSalesEmp, opSalesEmp);
                    }
                    #endregion

                }

                #endregion

                ProposalFacade.ProposalLog(View.ProposalId, (hasFlag ? "取消-" : string.Empty) + text + "(後台)", View.UserName);
                LoadDeal(View.BusinessHourId);
            }
        }

        /// <summary>
        /// 寄通知信
        /// </summary>
        /// <param name="mailToUser"></param>
        /// <param name="salesName"></param>
        /// <param name="pid"></param>
        /// <param name="bid"></param>
        /// <param name="subject"></param>
        /// <param name="sellerName"></param>
        private void SendEmail(List<string> mailToUser, string salesName, int pid, Guid bid, string subject, string sellerName)
        {
            foreach (string user in mailToUser)
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(_config.SystemEmail);
                msg.To.Add(user);
                msg.Subject = subject;
                string url = _config.SiteUrl + @"/ppon/default.aspx?bid=" + bid;
                msg.Body = HttpUtility.HtmlDecode(GetBodyHtml(pid, sellerName, salesName, url));
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        private string WayOut(RemittanceType type)
        {
            string result = string.Empty;
            switch (type)
            {
                case RemittanceType.Others:
                    result = "其他付款方式";
                    break;
                case RemittanceType.AchWeekly:
                    result = "ACH每週出帳";
                    break;
                case RemittanceType.AchMonthly:
                    result = "ACH每月出帳";
                    break;
                case RemittanceType.ManualPartially:
                    result = "商品(暫付70%)";
                    break;
                case RemittanceType.ManualWeekly:
                    result = "人工每週出帳";
                    break;
                case RemittanceType.ManualMonthly:
                    result = "人工每月出帳";
                    break;
                case RemittanceType.Flexible:
                    result = "彈性選擇出帳";
                    break;
                case RemittanceType.Weekly:
                    result = "新每週出帳";
                    break;
                case RemittanceType.Monthly:
                    result = "新每月出帳";
                    break;
                default:
                    result = "其他付款方式";
                    break;
            }

            return result;
        }

        private string GetBodyHtml(int pid, string sellerName, string salesName, string url)
        {
            string html = I18N.Message.ProposalSendEmailHtmlContent;
            string returnHtml = string.Empty;
            return string.Format(html, _config.SiteUrl, pid, sellerName, salesName, url, string.Empty);
        }


        protected void OnDeleteCDNImages(object sender, EventArgs e)
        {
            ViewPponDeal vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(View.BusinessHourId);
            CDNImageDelete(vpd.EventImagePath, vpd.BusinessHourGuid);
        }

        #endregion event handlers

        #region Method

        /// <summary>
        /// 產生一筆新的PponDeal資料
        /// </summary>
        /// <param name="pponDeal">要複製的PponDeal</param>
        /// <returns></returns>
        private static PponDeal GetNewPponDealClone(PponDeal pponDeal)
        {
            var entity = new PponDeal(true);
            entity.Deal = pponDeal.Deal.Clone();
            entity.DealContent = pponDeal.DealContent.Clone();
            entity.ItemDetail = pponDeal.ItemDetail.Clone();
            entity.Property = pponDeal.Property.Clone();
            return entity;
        }

        /// <summary>
        /// 查詢未離職之業務員的名稱，填入View的SalesmanNameArray中
        /// </summary>
        protected void SetSalesmanNameArray()
        {
            var empCollection = _humanProv.EmployeeCollectionGetByDepartment(EmployeeDept.S000);
            View.SalesmanNameArray = empCollection.Where(x => !x.IsInvisible)
                                                  .Select(emp => emp.EmpName)
                                                  .ToArray();
        }

        public void UpdateGroupOrderStatus(Guid goGuid, int orderedQuantity, decimal orderTotalLimit)
        {
            //new GroupOrderStatus for checking if the deal is soldout when closing the deal
            GroupOrder go = _orderProv.GroupOrderGet(goGuid);
            if (!go.IsLoaded)
            {
                throw new Exception("completing a not existing group order?");
            }
            else
            {
                if (orderedQuantity >= orderTotalLimit)
                {
                    go.Status |= (int)GroupOrderStatus.SoldOut;
                    if (orderedQuantity < 500)
                    {
                        go.Status |= (int)GroupOrderStatus.LowQuantityToShow;
                    }
                }
                go.Slug = orderedQuantity.ToString();
                _orderProv.GroupOrderSet(go);
            }
        }

        /// <summary>
        /// 回傳Icon檔次標籤陣列的字串組合 (以,分隔)
        /// </summary>
        /// <param name="selectedIcon"></param>
        /// <returns></returns>
        private string DealIconlListToString(List<int> selectedIcon)
        {
            return string.Join(",", selectedIcon);
        }

        private bool CheckGroupCouponDealType(decimal itemPrice, decimal itemOrigPrice, int saleMultipleBase, int presentQuantity, int type, out string errorMsg, bool isSubDeal = false)
        {
            errorMsg = isSubDeal ? "子檔" : "";

            if (type == (int)GroupCouponDealType.AvgAssign && itemPrice % (saleMultipleBase - presentQuantity) > 0)
            {
                errorMsg += string.Format("成套票券A型售價({0})必須能整除買數({1})\\n", itemPrice, (saleMultipleBase - presentQuantity));
                return false;
            }

            if (type == (int)GroupCouponDealType.CostAssign && itemOrigPrice % saleMultipleBase > 0)
            {
                errorMsg += string.Format("成套票券B型原價({0})必須能整除組數({1})\\n", itemOrigPrice, saleMultipleBase);
                return false;
            }

            if (type == (int)GroupCouponDealType.CostAssign && (itemPrice / (saleMultipleBase - presentQuantity) > itemOrigPrice / saleMultipleBase))
            {
                errorMsg += string.Format("成套票券B型單張售價不可高於原價\\n");
                View.ShowMessage(errorMsg);
                return false;
            }
            errorMsg = "";
            return true;
        }
        #endregion Method

        private string ViewPponDealToJson(ViewPponDeal ppon)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("AppTitle");
                writer.WriteValue(ppon.AppTitle);
                writer.WritePropertyName("AppDealPic");
                writer.WriteValue(ppon.AppDealPic);
                writer.WritePropertyName("Availability");
                writer.WriteValue(ppon.Availability);
                writer.WritePropertyName("BusinessHourAtmMaximum");
                writer.WriteValue(ppon.BusinessHourAtmMaximum);
                writer.WritePropertyName("BusinessHourDeliverTimeE");
                writer.WriteValue(ppon.BusinessHourDeliverTimeE == null ? "n/a" : ppon.BusinessHourDeliverTimeE.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliverTimeS");
                writer.WriteValue(ppon.BusinessHourDeliverTimeS == null ? "n/a" : ppon.BusinessHourDeliverTimeS.Value.ToString("o"));
                writer.WritePropertyName("BusinessHourDeliveryCharge");
                writer.WriteValue(ppon.BusinessHourDeliveryCharge);
                writer.WritePropertyName("BusinessHourGuid");
                writer.WriteValue(ppon.BusinessHourGuid.ToString());
                writer.WritePropertyName("BusinessHourOrderMinimum");
                writer.WriteValue(ppon.BusinessHourOrderMinimum);
                writer.WritePropertyName("BusinessHourOrderTimeE");
                writer.WriteValue(ppon.BusinessHourOrderTimeE.ToString("o"));
                writer.WritePropertyName("BusinessHourOrderTimeS");
                writer.WriteValue(ppon.BusinessHourOrderTimeS.ToString("o"));
                writer.WritePropertyName("BusinessHourStatus");
                writer.WriteValue(ppon.BusinessHourStatus);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(ppon.ChangedExpireDate.HasValue ? ppon.ChangedExpireDate.Value.ToString("yyyy/MM/dd") : "");
                writer.WritePropertyName("ComboPackCount");
                writer.WriteValue(ppon.ComboPackCount);
                writer.WritePropertyName("CompleteCopy");
                writer.WriteValue(ppon.CompleteCopy);
                writer.WritePropertyName("CouponUsage");
                writer.WriteValue(ppon.CouponUsage);
                writer.WritePropertyName("CouponCodeType");
                writer.WriteValue(ppon.CouponCodeType);
                writer.WritePropertyName("CreateTime");
                writer.WriteValue(ppon.CreateTime);
                writer.WritePropertyName("DealAccBusinessGroupId");
                writer.WriteValue(ppon.DealAccBusinessGroupId);
                writer.WritePropertyName("DealType");
                writer.WriteValue(ppon.DealType);
                writer.WritePropertyName("DealTypeDetail");
                writer.WriteValue(ppon.DealTypeDetail);
                writer.WritePropertyName("DeliveryType");
                writer.WriteValue(ppon.DeliveryType);
                writer.WritePropertyName("DenyInstallment");
                writer.WriteValue(ppon.DenyInstallment);
                writer.WritePropertyName("Department");
                writer.WriteValue(ppon.Department);
                writer.WritePropertyName("Description");
                string description = ViewPponDealManager.DefaultManager.GetDealDescription(ppon);
                writer.WriteValue(description);
                writer.WritePropertyName("DiscountType");
                writer.WriteValue(ppon.DiscountType);
                writer.WritePropertyName("DiscountValue");
                writer.WriteValue(ppon.DiscountValue);
                writer.WritePropertyName("EntrustSell");
                writer.WriteValue(ppon.EntrustSell);
                writer.WritePropertyName("EventName");
                writer.WriteValue(ppon.EventName);
                writer.WritePropertyName("EventTitle");
                writer.WriteValue(ppon.EventTitle);
                writer.WritePropertyName("ExchangePrice");
                writer.WriteValue(ppon.ExchangePrice);
                writer.WritePropertyName("GroupOrderGuid");
                writer.WriteValue(ppon.GroupOrderGuid.ToString());
                writer.WritePropertyName("GroupOrderStatus");
                writer.WriteValue(ppon.GroupOrderStatus);
                writer.WritePropertyName("Introduction");
                writer.WriteValue(ppon.Introduction);
                writer.WritePropertyName("IsAveragePrice");
                writer.WriteValue(ppon.IsAveragePrice);
                writer.WritePropertyName("IsDailyRestriction");
                writer.WriteValue(ppon.IsDailyRestriction);
                writer.WritePropertyName("IsExperience");
                writer.WriteValue(ppon.IsExperience);
                writer.WritePropertyName("IsLongContract");
                writer.WriteValue(ppon.IsLongContract);
                writer.WritePropertyName("IsQuantityMultiplier");
                writer.WriteValue(ppon.IsQuantityMultiplier);
                writer.WritePropertyName("ItemDefaultDailyAmount");
                writer.WriteValue(ppon.ItemDefaultDailyAmount);
                writer.WritePropertyName("ItemGuid");
                writer.WriteValue(ppon.ItemGuid.ToString());
                writer.WritePropertyName("ItemName");
                writer.WriteValue(ppon.ItemName);
                writer.WritePropertyName("ItemOrigPrice");
                writer.WriteValue(ppon.ItemOrigPrice);
                writer.WritePropertyName("ItemPrice");
                writer.WriteValue(ppon.ItemPrice);
                writer.WritePropertyName("LabelIconList");
                writer.WriteValue(ppon.LabelIconList);
                writer.WritePropertyName("MaxItemCount");
                writer.WriteValue(ppon.MaxItemCount);
                writer.WritePropertyName("MultipleBranch");
                writer.WriteValue(ppon.MultipleBranch);
                writer.WritePropertyName("OrderedQuantity");
                writer.WriteValue(ppon.OrderedQuantity);
                writer.WritePropertyName("OrderedTotal");
                writer.WriteValue(ppon.OrderedTotal);
                writer.WritePropertyName("OrderGuid");
                writer.WriteValue(ppon.OrderGuid.ToString());
                writer.WritePropertyName("OrderTotalLimit");
                writer.WriteValue(ppon.OrderTotalLimit);
                writer.WritePropertyName("PageDesc");
                writer.WriteValue(ppon.PageDesc);
                writer.WritePropertyName("PageTitle");
                writer.WriteValue(ppon.PageTitle);
                writer.WritePropertyName("PicAlt");
                writer.WriteValue(ppon.PicAlt);
                writer.WritePropertyName("PinType");
                writer.WriteValue(ppon.PinType);
                writer.WritePropertyName("PurchasePrice");
                writer.WriteValue(View.purchasePrice);
                writer.WritePropertyName("QuantityMultiplier");
                writer.WriteValue(ppon.QuantityMultiplier);
                writer.WritePropertyName("Reasons");
                writer.WriteValue(ppon.Reasons);
                writer.WritePropertyName("ReferenceText");
                writer.WriteValue(ppon.ReferenceText);
                writer.WritePropertyName("Remark");
                writer.WriteValue(ppon.Remark);
                writer.WritePropertyName("SellerAddress");
                writer.WriteValue(ppon.SellerAddress);
                writer.WritePropertyName("SellerCityId");
                writer.WriteValue(ppon.SellerCityId);
                writer.WritePropertyName("SellerGuid");
                writer.WriteValue(ppon.SellerGuid.ToString());
                writer.WritePropertyName("SellerId");
                writer.WriteValue(ppon.SellerId);
                writer.WritePropertyName("SellerName");
                writer.WriteValue(ppon.SellerName);
                writer.WritePropertyName("SellerTel");
                writer.WriteValue(ppon.SellerTel);
                writer.WritePropertyName("SettlementTime");
                writer.WriteValue(ppon.SettlementTime);
                writer.WritePropertyName("SlottingFeeQuantity");
                writer.WriteValue(View.slottingFeeQuantity);
                writer.WritePropertyName("Slug");
                writer.WriteValue(ppon.Slug);
                writer.WritePropertyName("ShipType");
                writer.WriteValue(ppon.ShipType);
                writer.WritePropertyName("ShoppingCart");
                writer.WriteValue(ppon.ShoppingCart);
                writer.WritePropertyName("SubjectName");
                writer.WriteValue(ppon.SubjectName);
                writer.WritePropertyName("TravelPlace");
                writer.WriteValue(ppon.TravelPlace);
                writer.WritePropertyName("Consignment");
                writer.WriteValue(ppon.Consignment);
                writer.WritePropertyName("ContentName");
                writer.WriteValue(ppon.ContentName);
                writer.WritePropertyName("EventImagePath");
                writer.WriteValue(ppon.EventImagePath);
                writer.WritePropertyName("IsGame");
                writer.WriteValue(ppon.IsGame);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private string DealAccountingToJson(DealAccounting dealaccounting)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("DeptId");
                writer.WriteValue(dealaccounting.DeptId);
                writer.WritePropertyName("IsInputTaxRequired");
                writer.WriteValue(dealaccounting.IsInputTaxRequired);
                writer.WritePropertyName("Message");
                writer.WriteValue(dealaccounting.Message);
                writer.WritePropertyName("Paytocompany");
                writer.WriteValue(dealaccounting.Paytocompany);
                writer.WritePropertyName("RemittanceType");
                writer.WriteValue(dealaccounting.RemittanceType);
                writer.WritePropertyName("VendorReceiptType");
                writer.WriteValue(dealaccounting.VendorReceiptType);
                writer.WritePropertyName("VendorBillingModel");
                writer.WriteValue(dealaccounting.VendorBillingModel);


                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private string PponStoreToJson(PponStore pponStore)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("StoreGuid");
                writer.WriteValue(pponStore.StoreGuid);
                writer.WritePropertyName("TotalQuantity");
                writer.WriteValue(pponStore.TotalQuantity);
                writer.WritePropertyName("SortOrder");
                writer.WriteValue(pponStore.SortOrder);
                writer.WritePropertyName("ChangedExpireDate");
                writer.WriteValue(pponStore.ChangedExpireDate);
                writer.WritePropertyName("VbsRight");
                writer.WriteValue(pponStore.VbsRight);

                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private string PponOptionToJson(PponOption pponOption)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("Id");
                writer.WriteValue(pponOption.Id);
                writer.WritePropertyName("CatgName");
                writer.WriteValue(pponOption.CatgName);
                writer.WritePropertyName("CatgSeq");
                writer.WriteValue(pponOption.CatgSeq);
                writer.WritePropertyName("OptionName");
                writer.WriteValue(pponOption.OptionName);
                writer.WritePropertyName("OptionSeq");
                writer.WriteValue(pponOption.OptionSeq);
                writer.WritePropertyName("Quantity");
                writer.WriteValue(pponOption.Quantity);
                writer.WritePropertyName("IsDisabled");
                writer.WriteValue(pponOption.IsDisabled);
                writer.WritePropertyName("ModifyId");
                writer.WriteValue(pponOption.ModifyId);
                writer.WritePropertyName("ModifyTime");
                writer.WriteValue(pponOption.ModifyTime);

                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        private string ChangeItemsToJson(KeyValuePair<string, string> changeitems)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName(changeitems.Key);
                writer.WriteValue(changeitems.Value);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }

        protected bool RecoverDeal(Guid bid)
        {
            return _pponProv.PponDealExtendAfterClose(bid);
        }

        //字串中含有規則內的符號則轉為全形
        static string CapText(string txt)
        {
            string pattern = "[_(,\")]";
            Regex rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            string optionName = string.Empty;
            foreach (var t in txt)
            {
                optionName += rgx.Replace(t.ToString(), new MatchEvaluator(x => x.Result(Strings.StrConv(t.ToString(), VbStrConv.Wide, 0))));
            }
            return optionName;
        }

    }
    public static class DateTimeHelper
    {
        public static DateTime Noon(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.Day, 12, 0, 0);
        }

    }

    public class ImageOrderClass
    {
        public int Index { get; set; }

        public int SortIndex { get; set; }

        public string Path { get; set; }
    }

}
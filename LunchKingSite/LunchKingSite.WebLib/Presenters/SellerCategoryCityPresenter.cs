﻿using System.Collections.Generic;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class SellerCategoryCityPresenter : Presenter<ISellerCategoryCityView>
    {
        protected ISellerProvider icp = null;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.CategoryId < 0)
                return false;
            
            ViewSellerCategoryCityCollection data = icp.ViewSellerCategoryCityGetList(ViewSellerCategoryCity.Columns.CategoryId, View.CategoryId, true, ViewSellerCategoryCity.Columns.CityName);
            if (data.Count == 0)
                return false;

            SetupDataAndSetView(data);
            return true;
        }

        private void SetupDataAndSetView(ViewSellerCategoryCityCollection data)
        {
            if (data.Count == 0)
                return;

            string cityName = data[0].CityName;
            List<CitySellers> citySellersList = new List<CitySellers>();
            int cityCode = data[0].CityId;

            ViewSellerCategoryCityCollection sellerList = new ViewSellerCategoryCityCollection();
            CitySellers sellers = new CitySellers(data[0].CityName);
            foreach (ViewSellerCategoryCity s in data)
            {
                if (s.CityId != cityCode)
                {
                    sellers.Sellers = sellerList;
                    citySellersList.Add(sellers);
                    cityCode = s.CityId;
                    sellers = new CitySellers(s.CityName);
                    sellerList = new ViewSellerCategoryCityCollection();
                }
                // we set photo size at presenter for now, in case if we ever have need to show different sizes
                // in different views we can always put this logic into view
                s.SellerLogoimgPath = ImageFacade.GetMediaPath(s.SellerLogoimgPath, MediaType.SellerPhotoSmall);
                sellerList.Add(s);
            }
            sellers.Sellers = sellerList;
            citySellersList.Add(sellers);

            View.SetSellers(citySellersList);
        }

        public SellerCategoryCityPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            icp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }
    }
}

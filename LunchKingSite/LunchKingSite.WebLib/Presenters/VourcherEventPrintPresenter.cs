﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System.IO;
using LunchKingSite.Core.Component;
namespace LunchKingSite.WebLib.Presenters
{
    public class VourcherEventPrintPresenter : Presenter<IVourcherEventPrintView>
    {
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetVourcherEventBySellerGuid += OnGetVourcherEventBySellerGuid;
            View.GetVourcherEventById += OnGetVourcherEventById;
            View.GetPrintVourcherEvent += OnGetPrintVourcherEvent;
            return true;
        }

        void OnGetVourcherEventById(object sender, EventArgs e)
        {
            ViewVourcherSeller vourcherevent = VourcherFacade.ViewVourcherSellerGetById(View.EventId);
            Seller seller = VourcherFacade.SellerGetByGuid(vourcherevent.SellerGuid);
            ViewVourcherSellerCollection vourcherevents = new ViewVourcherSellerCollection();
            vourcherevents.Add(vourcherevent);
            View.SetSellerVourcherEventCollection(seller, vourcherevents);
        }

        void OnGetPrintVourcherEvent(object sender, DataEventArgs<List<int>> e)
        {
            Seller seller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
            List<VourcherEventStoreInfo> info = new List<VourcherEventStoreInfo>();
            foreach (int item in e.Data)
            {
                VourcherEvent vourcher_event = VourcherFacade.VourcherEventGetById(item);
                ViewVourcherStoreCollection vourcher_stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(item);
                info.Add(new VourcherEventStoreInfo(vourcher_event, vourcher_stores));
            }
            IHumanProvider humProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            string salesName = info.Count > 0 ? info[0].Vourcher_Event.SalesName : string.Empty;
            View.SetSellerVourcherEventSotres(seller, info, salesName);
        }

        void OnGetVourcherEventBySellerGuid(object sender, EventArgs e)
        {
            Seller seller = VourcherFacade.SellerGetByGuid(View.SellerGuid);
            ViewVourcherSellerCollection vourcherevents = VourcherFacade.ViewVourcherSellerCollectionGetBySellerGuid(View.SellerGuid);
            View.SetSellerVourcherEventCollection(seller, vourcherevents);
        }
    }
}

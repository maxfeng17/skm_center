﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class GiftSearchPresenter : Presenter<IGiftSearchView>
    {
        private IMGMProvider mgp;


        public override bool OnViewInitialized()
        {            
            base.OnViewInitialized();
            //View.SetMgmGiftList(GetMgmGiftList(1));
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.ViewMgmGiftListGet += OnViewMgmGiftListGet;
            View.ViewMgmGiftListGetCount += OnViewMgmGiftListGetCount;
            View.ViewMgmGiftListPageChanged += OnViewMgmGiftListPageChanged;
            return true;
        }

        
        public GiftSearchPresenter(IMGMProvider mgmProv)
        {
            mgp = mgmProv;
        }
        

        
        #region event
        protected void OnSearch(object sender, EventArgs e)
        {
            View.SetMgmGiftList(GetMgmGiftList(1));
        }

        protected void OnViewMgmGiftListGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = mgp.ViewMgmGiftGetCount(GetFilter());
        }
        protected void OnViewMgmGiftListPageChanged(object sender, DataEventArgs<int> e)
        {
            View.SetMgmGiftList(GetMgmGiftList(e.Data));
        }

        protected void OnViewMgmGiftListGet(object sender, DataEventArgs<string> e)
        {
            string[] commardArgs = e.Data.Split(',');
            LoadViewMgmGiftBacklist(commardArgs[0], commardArgs[1]);
        }
        #endregion

        #region Private Method
        private string[] GetFilter()
        {
            List<string> filter = new List<string>();

            if (View.UserId != 0)
            {
                filter.Add(ViewMgmGiftBacklist.Columns.SenderId + " = " + View.UserId);
            }
            if (!string.IsNullOrEmpty(View.Mobile))
            {
                filter.Add(ViewMgmGiftBacklist.Columns.Mobile + " = " + View.Mobile);
            }
            if (!string.IsNullOrEmpty(View.SequenceNumber))
            {
                filter.Add(ViewMgmGiftBacklist.Columns.SequenceNumber + " = " + View.SequenceNumber);
            }
            if (!string.IsNullOrEmpty(View.ProductName))
            {
                filter.Add(ViewMgmGiftBacklist.Columns.Title + " like %" + View.ProductName +"%"); 
            }
            if (!string.IsNullOrEmpty(View.TbOS))
            {
                DateTime sDate = DateTime.Parse(View.TbOS);
                filter.Add(ViewMgmGiftBacklist.Columns.SendTime + " >=" + sDate);
            }
            if (!string.IsNullOrEmpty(View.TbOE))
            {
                DateTime eDate = DateTime.Parse(View.TbOE).AddDays(1);
                filter.Add(ViewMgmGiftBacklist.Columns.SendTime + " <=" + eDate);
            }


            return filter.ToArray();
        }
        
        private ViewMgmGiftBacklistCollection GetMgmGiftList(int pageNumber)
        {
            return mgp.ViewMgmGiftBacklistGetByPage(pageNumber, View.PageSize, ViewMgmGiftBacklist.Columns.SendTime, GetFilter());
        }

        private void LoadViewMgmGiftBacklist(string sequenceNumber,string Account)
        {
            View.SetViewMgmGiftBacklist(mgp.ViewMgmGiftGetAll(sequenceNumber),sequenceNumber,Account);
        }

        #endregion
    }
}

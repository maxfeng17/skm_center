﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class ContentManagePresenter : Presenter<IContentManageView>
    {
        private ICmsProvider _cp;

        public ContentManagePresenter(ICmsProvider cmsProv)
        {
            _cp = cmsProv;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (View.ContentId == null)
                View.SetContentList(_cp.CmsContentGetList(0, -1, CmsContent.Columns.ContentId + " desc", null));
            else
            {
                CmsContent c = _cp.CmsContentGet(View.ContentId ?? -1);
                if (!c.IsLoaded)
                    return false;
                PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoActivit,c.ContentId);
                View.SetContent(c, seo);
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveContent += OnSaveContent;
            return true;
        }

        private void OnSaveContent(object sender, DataEventArgs<ContentManageSave> e)
        {
            CmsContent cnt = _cp.CmsContentGet(View.ContentId ?? -1);
            if (!cnt.IsLoaded)
                return;

            PromoSeoKeyword seo = PromotionFacade.GetPromoSeoKeyword((int)PromoSEOType.PromoActivit, cnt.ContentId);
            if (string.IsNullOrEmpty(e.Data.SeoDescription) && string.IsNullOrEmpty(e.Data.SeoKeyWords))
            {
                PromotionFacade.DeletePromoSeoKeyword(seo);
            }
            else
            {
                if (seo.Id == 0)
                {
                    seo.CreateId = e.Data.UserID;
                    seo.CreateTime = DateTime.Now;
                }
                seo.PromoType = (int)PromoSEOType.PromoActivit;
                seo.ActivityId = cnt.ContentId;
                seo.ModifyId = e.Data.UserID;
                seo.ModifyTime = DateTime.Now;
                seo.Description = e.Data.SeoDescription;
                seo.Keyword = e.Data.SeoKeyWords;

                PromotionFacade.SavePromoSeoKeyword(seo);
            }

            cnt.Title = e.Data.Title;
            cnt.Body = e.Data.PromoContent;
            cnt.ModifiedOn = DateTime.Now;
            cnt.ModifiedBy = View.UserName;
            _cp.CmsContentSet(cnt);
            View.ClearCacheDependency(cnt.ContentName);
            OnViewInitialized();
        }
    }
}

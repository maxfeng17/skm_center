﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class FamiPortManagerPresenter : Presenter<IFamiPortManagerView>
    {
        private IPponProvider _pponProv;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetFamiPortData(1);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnSaveClicked += OnSaveClicked;
            View.OnDeleteClicked += OnDeleteClicked;
            View.OnGetCount += OnGetCount;
            View.OnPageChanged += OnPageChanged;
            return true;
        }

        public FamiPortManagerPresenter(IPponProvider pponProv)
        {
            _pponProv = pponProv;
        }

        #region event
        protected void OnSaveClicked(object sender, EventArgs e)
        {
            // 判斷bid是否存在
            BusinessHour businessHour = _pponProv.BusinessHourGet(View.BusinessHourGuid);
            if (businessHour.IsLoaded)
            {
                Famiport famiPort = _pponProv.FamiportGet(Famiport.Columns.BusinessHourGuid, View.BusinessHourGuid);
                if (!famiPort.IsLoaded)
                {
                    famiPort.BusinessHourGuid = View.BusinessHourGuid;
                    famiPort.CreateId = View.LoginUser;
                    famiPort.CreateTime = DateTime.Now;
                    famiPort.IsOutOfDate = true;
                }
                famiPort.EventId = View.EventId;
                famiPort.Password = View.Password;
                _pponProv.FamiportSet(famiPort);
            }
            else
                View.ShowMessage(I18N.Message.NoBusinessHourGuid);

            GetFamiPortData(1);
        }

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetFamiPortData(1);
        }

        protected void OnDeleteClicked(object sender, DataEventArgs<string> e)
        {
            int id = int.TryParse(e.Data, out id) ? id : 0;
            if (!id.Equals(0))
                _pponProv.FamiportDelete(id);

            GetFamiPortData(1);
        }
        #endregion

        #region method
        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _pponProv.FamiportGetCount(GetFilter());
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            GetFamiPortData(e.Data);
        }
        #endregion

        #region Private Method
        private void GetFamiPortData(int pageNumber)
        {
            Dictionary<Famiport, KeyValuePair<Guid, string>> dataList = new Dictionary<Famiport, KeyValuePair<Guid, string>>();
            FamiportCollection famiPortList = _pponProv.FamiportGetList(pageNumber, View.PageSize, Famiport.Columns.Id, GetFilter());
            foreach (Famiport item in famiPortList)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.BusinessHourGuid);
                dataList.Add(item, new KeyValuePair<Guid, string>(vpd.BusinessHourGuid, vpd.ItemName));
            }
            View.GetFamiPortList(dataList);
        }

        private string[] GetFilter()
        {
            List<string> list = new List<string>();
            list.Add(Famiport.Columns.IsOutOfDate + "=true");
            if (!View.BusinessHourGuid.Equals(Guid.Empty))
                list.Add(Famiport.Columns.BusinessHourGuid + "=" + View.BusinessHourGuid.ToString());
            if (!string.IsNullOrEmpty(View.EventId))
                list.Add(Famiport.Columns.EventId + "=" + View.EventId);
            return list.ToArray();
        }
        #endregion
    }
}

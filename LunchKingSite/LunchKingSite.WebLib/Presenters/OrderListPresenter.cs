using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class OrderListPresenter : Presenter<IOrderListView>
    {
        #region members

        public const int DEFAULT_PAGE_SIZE = 15;

        protected IOrderProvider op = null;
        protected IWmsProvider wp = null;

        #endregion members

        public OrderListPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetOrderCount += OnGetOrderCount;
            View.SortClicked += OnGridUpdated;
            View.SearchClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            return true;
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        protected void SetFilterTypes()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add(ViewOrderMemberBuildingSeller.Columns.OrderId, "訂單編號");
            data.Add(ViewOrderMemberBuildingSeller.Columns.MemberName, "會員姓名");
            data.Add(ViewOrderMemberBuildingSeller.Columns.UniqueId, "檔號");
            data.Add("DiscountCode", "折價券序號");
            data.Add(EinvoiceMain.Columns.InvoiceNumber, "發票號碼");
            data.Add(ViewOrderMemberBuildingSeller.Columns.ShipNo, "物流單號");
            data.Add("pchome", "PChome訂單編號");
            data.Add("LifeEntrepot", "台新中台訂單編號");
            View.SetFilterTypeDropDown(data);
        }



        protected void OnGetOrderCount(object sender, DataEventArgs<int> e)
        {
            
            string ddlSelection = View.GetFilterTypeDropDown();

            string orderId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.OrderId
                ? View.SearchExpression
                : null;

            int? uniqueId = null;
            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.UniqueId)
            {
                int result;
                if(int.TryParse(View.SearchExpression, out result))
                {
                    uniqueId = result;
                }
            }
            if (ddlSelection == "pchome")
            {
                var wmsOrder = wp.WmsOrderGetByPchomeId(View.SearchExpression);
                if (wmsOrder.IsLoaded == false)
                {
                    return;
                }
                Order o = op.OrderGet(wmsOrder.OrderGuid);
                if (o.IsLoaded == false)
                {
                    return;
                }
                orderId = o.OrderId;
            }

            if (ddlSelection == EinvoiceMain.Columns.InvoiceNumber)
            {
                var em = op.EinvoiceMainGet(View.SearchExpression.Trim());
                orderId = em.IsLoaded ? em.OrderId : "0";
            }

            string memberName = ddlSelection == ViewOrderMemberBuildingSeller.Columns.MemberName
                ? View.SearchExpression
                : null;

            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.ShipNo)
            {
                List<string> orderGuidList = op.OrderShipGetListByShipNo(View.SearchExpression.Trim()).Select(x => x.OrderGuid.ToString()).ToList();

                e.Data = op.ViewOrderMemberBuildingSellerGet(
                 0, 0, string.Empty, ViewOrderMemberBuildingSeller.Columns.OrderGuid + " IN " + string.Join(",", orderGuidList)).Count;
            }
            else
            {
                DateTime? minOrderCreateTime = View.MinOrderCreateTime;
                DateTime? maxOrderCreateTime = View.MaxOrderCreateTime;
               
                e.Data = op.ViewOrderMemberBuildingSellerGetCountForNew(orderId, uniqueId, memberName, minOrderCreateTime,
                                                              maxOrderCreateTime, View.DateType, View.ShipType, View.CsvType);
            }
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void LoadData(int pageNumber)
        {

            string ddlSelection = View.GetFilterTypeDropDown();

            string orderId = ddlSelection == ViewOrderMemberBuildingSeller.Columns.OrderId
                ? View.SearchExpression
                : null;

            int? uniqueId = null;
            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.UniqueId)
            {
                int result;
                if (int.TryParse(View.SearchExpression, out result))
                {
                    uniqueId = result;
                }
            }
            if (ddlSelection == "pchome")
            {
                var wmsOrder = wp.WmsOrderGetByPchomeId(View.SearchExpression);
                if (wmsOrder.IsLoaded == false)
                {
                    View.SetOrderList(null);
                    return;
                }
                Order o = op.OrderGet(wmsOrder.OrderGuid);
                if (o.IsLoaded == false)
                {
                    View.SetOrderList(null);
                    return;
                }
                orderId = o.OrderId;
            }

            string memberName = ddlSelection == ViewOrderMemberBuildingSeller.Columns.MemberName
                ? View.SearchExpression
                : null;

            if (ddlSelection == "DiscountCode")
            {
                var discount = op.ViewDiscountDetailByCode(View.SearchExpression.Trim());
                if (discount.IsLoaded &&
                    discount.OrderGuid != null && discount.OrderGuid != Guid.Empty &&
                    !Helper.IsFlagSet(discount.Flag, DiscountCampaignUsedFlags.SingleSerialKey) &&
                    !Helper.IsFlagSet(discount.Flag, DiscountCampaignUsedFlags.CustomSerialKey))
                {
                    var o = op.OrderGet((Guid)discount.OrderGuid);
                    if (o.IsLoaded)
                    {
                        orderId = o.OrderId;
                    }
                }
            }

            if (ddlSelection == EinvoiceMain.Columns.InvoiceNumber)
            {
                var em = op.EinvoiceMainGet(View.SearchExpression.Trim());
                orderId = em.IsLoaded ? em.OrderId : "0";
            }

            if (ddlSelection == "LifeEntrepot")
            {
                List<string> filter = new List<string>() { ViewLiontravelCouponListMain.Columns.Type + "=" + (int)AgentChannel.LifeEntrepot };
                filter.Add(ViewLiontravelCouponListMain.Columns.RelatedOrderId + "=" + View.SearchExpression + "");

                ViewLiontravelCouponListMainCollection data = op.ViewLionTravelCouponListMainGetList(1, 15, filter.ToArray());
                if (data.Count() == 0)
                {
                    View.SetOrderList(null);
                    return;
                }
                orderId = data.FirstOrDefault().OrderId;
            }

            if (ddlSelection == ViewOrderMemberBuildingSeller.Columns.ShipNo)
            {
                List<string> orderGuidList = op.OrderShipGetListByShipNo(View.SearchExpression.Trim()).Select(x => x.OrderGuid.ToString()).ToList();

                View.SetOrderList(op.ViewOrderMemberBuildingSellerGet(
               pageNumber, View.PageSize, View.SortExpression, ViewOrderMemberBuildingSeller.Columns.OrderGuid + " IN " + string.Join(",", orderGuidList)));
            }
            else
            {
                DateTime? minOrderCreateTime = View.MinOrderCreateTime;
                DateTime? maxOrderCreateTime = View.MaxOrderCreateTime;

                View.SetOrderList(op.ViewOrderMemberBuildingSellerGetListPagingForNew(
                    pageNumber, View.PageSize, View.SortExpression, orderId, uniqueId, memberName, minOrderCreateTime, maxOrderCreateTime, View.DateType, View.ShipType, View.CsvType));
            }

        }

    }
}
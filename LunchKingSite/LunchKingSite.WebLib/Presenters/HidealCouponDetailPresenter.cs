﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HidealCouponDetailPresenter : Presenter<IHiDealCouponDetailView>
    {
        private IHiDealProvider _hdProv;
        private ISellerProvider _selProv;
        private ISysConfProvider _config;

        public HidealCouponDetailPresenter(IHiDealProvider hdProv, ISysConfProvider conf, IOrderProvider ordProv, ISellerProvider selProv)
        {
            _config = conf;
            _hdProv = hdProv;
            _selProv = selProv;
        }

        public override bool OnViewInitialized()
        {
            if (View.CouponId == null || (string.IsNullOrEmpty(View.UserName) && View.ActionMode != "preview"))
            {
                return false;
            }

            View.GoogleApiKey = _config.GoogleMapsAPIKey;

            if (View.ActionMode != "preview")
            {
                #region 一般列印憑證

                ViewHiDealCoupon coupon = _hdProv.ViewHiDealCouponGet(View.CouponId ?? 0);
                HiDealContent content =
                    _hdProv.HiDealContentGetAllByDealId(coupon.DealId).Where(x => x.Seq == 5).FirstOrDefault();
                ViewHiDealStoresInfoCollection storeList = new ViewHiDealStoresInfoCollection();

                if (!coupon.IsLoaded || coupon.UserName != View.UserName)
                {
                    return false;
                }
                ViewHiDeal deal = _hdProv.ViewHiDealGetByDealId(coupon.DealId);
                if (deal == null || !deal.IsLoaded)
                {
                    return false;
                }

                if (!deal.DealId.Equals(0))
                {
                    storeList = _hdProv.ViewHiDealDealStoreInfoGetByHiDealId(deal.DealId);
                }

                View.EncryptedCouponCode = coupon.Prefix + coupon.Sequence + "-" +
                                           (new EasyDigitCipher().Encrypt(coupon.Code));
                View.SetDetail(coupon, content,
                               storeList.Where(x => x.Status.Equals((int) StoreStatus.Available)).ToList());
                return true;

                #endregion 一般列印憑證
            }
            else
            {
                if (View.DealId.HasValue && View.ProductId.HasValue)
                {
                    #region 預覽列印憑證

                    ViewHiDeal deal = _hdProv.ViewHiDealGetByDealId(View.DealId.Value);
                    if (deal == null || !deal.IsLoaded)
                    {
                        return false;
                    }

                    ViewHiDealStoresInfoCollection storeList = new ViewHiDealStoresInfoCollection();
                    if (!deal.DealId.Equals(0))
                    {
                        storeList = _hdProv.ViewHiDealDealStoreInfoGetByHiDealId(deal.DealId);
                    }

                    ViewHiDealCoupon previewCoupon = new ViewHiDealCoupon();

                    HiDealProduct hiDealProduct = _hdProv.HiDealProductGet(View.ProductId.Value);
                    var seller = _selProv.SellerGet(deal.SellerGuid);
                    previewCoupon.SellerName = (seller.IsLoaded)?seller.SellerName:"";
                    previewCoupon.DealName = deal.DealName;
                    previewCoupon.Name = hiDealProduct.Name;
                    previewCoupon.UseStartTime = (hiDealProduct.UseStartTime.HasValue)?hiDealProduct.UseStartTime.Value:DateTime.Now;
                    previewCoupon.UseEndTime = (hiDealProduct.UseEndTime.HasValue) ? hiDealProduct.UseEndTime.Value : DateTime.Now;

                    previewCoupon.Prefix ="A";
                    previewCoupon.Sequence="000000-0000";
                    previewCoupon.Code = "000000";

                    HiDealContent content = _hdProv.HiDealContentGetAllByDealId(View.DealId.Value).Where(x => x.Seq == 5).FirstOrDefault();
                    
                    View.EncryptedCouponCode = previewCoupon.Prefix + previewCoupon.Sequence + "-" +
                                              (new EasyDigitCipher().Encrypt(previewCoupon.Code));
                    
                    View.SetDetail(previewCoupon, content,
                                   storeList.Where(x => x.Status.Equals((int) StoreStatus.Available)).ToList());
                    return true;

                    #endregion 預覽列印憑證
                }
   
                return false;
                
            }
        }
    }
}

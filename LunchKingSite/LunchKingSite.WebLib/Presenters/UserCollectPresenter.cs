﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class UserCollectPresenter : Presenter<IUserCollectView>
    {
        private IMemberProvider mp;

        public UserCollectPresenter() 
        {
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized() 
        {
            base.OnViewInitialized();

            // user is not logged in
            if (!View.IsAuthenticated)
            {
                View.RedirectToLogin();
                return true;
            }

            Member member = mp.MemberGet(View.UserName);
            View.UserId = member.UniqueId;
            View.MemberCollectDealExpireMessage = member.CollectDealExpireMessage;
            View.OutOfDateDealCount = mp.MemberCollectDealGetOutOfDateDealCount(member.UniqueId);
            View.CollectBidList = mp.ViewMemberCollectDealGetDealList(member.UniqueId);
            SetCouponListMain(1, true);

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.PageChange += this.OnPageChange;
            View.CancelOutOfDateCollectDeal += this.OnCancelOutOfDateCollectDeal;
            View.RemoveCollectDeal += this.OnRemoveCollectDeal;
            return true;
        }

        /// <summary>
        /// 移除收藏的檔次
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnRemoveCollectDeal(object sender, DataEventArgs<Guid> e) 
        {
            mp.MemberCollectDealRemove(View.UserId, e.Data);
            View.OutOfDateDealCount = mp.MemberCollectDealGetOutOfDateDealCount(View.UserId);
            SetCouponListMain(View.CurrentPage, true);
            View.ReloadPagerTotalCount();
        }

        /// <summary>
        /// 移除過期的檔次
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnCancelOutOfDateCollectDeal(object sender, EventArgs e)
        {
            mp.MemberCollectDealRemoveOutOfDateDeal(View.UserId);
            View.OutOfDateDealCount = mp.MemberCollectDealGetOutOfDateDealCount(View.UserId);
            SetCouponListMain(1, true);
            View.ReloadPagerTotalCount();
        }

        private void OnPageChange(object sender, DataEventArgs<int> e)
        {
            SetCouponListMain(e.Data, false);
        }

        /// <summary>
        /// 取得列表資料
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="returnRecordCount"></param>
        private void SetCouponListMain(int pageIndex, bool returnRecordCount)
        {
            if (returnRecordCount)
            {
                View.CollectCount = mp.ViewMemberCollectDealGetDealCount(View.UserId);
            }

            //order by string
            string orderBy = string.Format("{0}, {1}", View.FirstOrderByColumn, View.SecondOrderByColumn);
            View.SetDealCollectList(mp.ViewMemberCollectionDealGetListByPager(View.UserId, pageIndex, View.PageSize, orderBy));
        }
    }
}

using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class RandomParagraphPresenter : Presenter<IRandomParagraphView>
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            ViewCmsRandomCollection dataCol = CmsRandomFacade.GetOrAddData(View.ContentName, View.Type);
            if (config.SwitchableHttpsEnabled)
            {
                foreach(var data in dataCol)
                {
                    data.Body = data.Body.Replace("http://", "https://");
                }
            }
            View.SetContent(dataCol);
            return true;
        }
    }
}

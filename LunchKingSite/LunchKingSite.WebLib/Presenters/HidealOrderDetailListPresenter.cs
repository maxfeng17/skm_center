﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.CustomException;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class HidealOrderDetailListPresenter : Presenter<IHidealOrderDetailListView>
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected ISysConfProvider cp = ProviderFactory.Instance().GetConfig();

        private static readonly string MakeReturnedFormReason = I18N.Phrase.HiDealOrderDetailListPresenterMakeReturnedFormReason;
        private static readonly string CancelReturnedFormReason = I18N.Phrase.HiDealOrderDetailListPresenterCancelReturnedFormReason;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetHiDealOrderDetailData();
            GetHiDealOrderShipData();
            SetServiceMessageCategory();
            return true;
        }

        public override bool OnViewLoaded()
        {
            //先將訊息清空
            View.ShowMessage(string.Empty);
            base.OnViewLoaded();
            View.ReturnCash += OnReturnCash;
            View.ReturnAllPassAndCashBack += OnReturnAllPassAndCashBack;
            View.ReturnAllPassAndPointBack += OnReturnAllPassAndPointBack;
            View.MakeReturnedForm += OnMakeReturnedForm;
            View.ReturnedFormCancel += OnReturnedFormCancel;
            View.ReturnComplete += OnReturnComplete;
            View.InvoiceUpdate += OnInvoiceUpdate;
            View.AddOrderShip += OnAddOrderShip;
            View.DeleteOrderShip += OnDeleteOrderShip;
            View.UpdateOrderShip += OnUpdateOrderShip;
            //新增客服紀錄動作
            View.OnSetServiceMsgClicked += OnSetServiceMsgClicked;
            //寄退貨申請書Mail
            View.SendRefundApplicationFormNotification += OnSendRefundApplicationFormNotification;
            //寄折讓單Mail
            View.SendDiscountSingleFormNotification += OnSendDiscountSingleFormNotification;
            //更新發票的折讓單是否寄回及紙本發票是否寄回資訊
            View.UpdateEInvoiceMainInfo += OnUpdateEInvoiceMainInfo;
            //退貨單資料更新
            View.UpdateReturned += OnUpdateReturned;
            return true;
        }

        #region event

        private void OnReturnCash(object sender, DataEventArgs<int> e)
        {
            var returnedId = e.Data;
            //尚未刷退，將狀態改為刷退，並進行刷退作業
            string message;
            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                View.ShowMessage("查無退貨單資料。");
            }
            else
            {
                switch ((HiDealReturnedStatus)returned.Status)
                {
                    case HiDealReturnedStatus.Create:
                        if (!HiDealReturnedManager.NewReturnedOrderChangeToReturnCash(returnedId, View.UserName, out message))
                        {
                            View.ShowMessage(message);
                        }
                        break;

                    case HiDealReturnedStatus.Completed:
                        if (!HiDealReturnedManager.RefundWorkReturnPointToReturnCash(returnedId, View.UserName, out message))
                        {
                            View.ShowMessage(message);
                        }
                        break;

                    case HiDealReturnedStatus.Cancel:
                        View.ShowMessage("退貨單已取消。");
                        break;
                }
            }

            GetHiDealOrderDetailData();
        }

        private void OnReturnComplete(object sender, DataEventArgs<int> e)
        {
            var returnedId = e.Data;
            //尚未刷退，將狀態改為刷退，並進行刷退作業
            CompleteReturned(returnedId);
            GetHiDealOrderDetailData();
        }

        private void OnReturnAllPassAndCashBack(object sender, DataEventArgs<Guid> e)
        {
            ReturnAllPass(e.Data, true);
            GetHiDealOrderDetailData();
        }

        private void OnReturnAllPassAndPointBack(object sender, DataEventArgs<Guid> e)
        {
            ReturnAllPass(e.Data, false);
            GetHiDealOrderDetailData();
        }

        private void OnMakeReturnedForm(object sender, DataEventArgs<HidealOrderDetailListViewMakeReturnedForm> e)
        {
            HiDealOrder order = hp.HiDealOrderGet(e.Data.OrderGuid);
            if (!order.IsLoaded)
            {
                View.ShowMessage("查無訂單紀錄。");
                return;
            }
            bool isCashBack = e.Data.IsCashBack;
            HiDealReturned returned;

            if (!HiDealReturnedManager.HiDealReturnedSetByOrderWithAllReturn(order, isCashBack, View.UserName,
                                                                             MakeReturnedFormReason,
                                                                             out returned))
            {
                View.ShowMessage("無法建立退貨單");
                return;
            }
            //退貨單建檔完成

            #region 憑證檔先行補產憑證動作

            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returned.Id);

            if (returnedDetails.Count == 0)
            {
                View.ShowMessage("查無此退貨單明細資料");
                return;
            }

            //檢查是否為憑證退貨單
            var couponReturnedDetails = returnedDetails.Where(x => x.DeliveryType == (int)HiDealDeliveryType.ToShop && x.ProductType == (int)HiDealProductType.Product);
            if (couponReturnedDetails.Any())
            {
                //查詢登記要退貨的coupon資料
                var returnedCoupons = hp.HiDealReturnedCouponGetList(returned.Id);
                //先取出此訂單所有的CashTrustLog，減少分次查詢的壓力
                var cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(order.Guid, OrderClassification.HiDeal);

                #region 整理所以要處理的CashTrustLog

                //可以進行退貨的所有cashtrustLog
                var cashTrustLogsForReturn = new List<CashTrustLog>();
                foreach (var returnedDetail in returnedDetails)
                {
                    //運費另外處理
                    if (returnedDetail.ProductType == (int)HiDealProductType.Freight)
                    {
                        continue;
                    }
                    var allowTrustStatus =
                        HiDealReturnedManager.GetTrustStatusWithAllowReturns(
                            (HiDealDeliveryType)returnedDetail.DeliveryType);
                    var returnCouponIds = (from returnedCoupon in returnedCoupons
                                           where returnedCoupon.ReturnedDetailId == returnedDetail.Id
                                           select returnedCoupon.CouponId).ToList();
                    var trustLogs =
                        cashTrustLogs.Where(
                            x =>
                            x.CouponId != null &&
                            (allowTrustStatus.Contains((TrustStatus)x.Status) &&
                             returnCouponIds.Contains((long)x.CouponId))).ToList();
                    cashTrustLogsForReturn.AddRange(trustLogs);

                    var returnedOptionIds = HiDealReturnedManager.ReturnedDetailGetReturnedOptionIds(returnedDetail);
                    if (returnedDetail.ProductType == (int)HiDealProductType.Product && returnedOptionIds.Count > 0 && returnedDetail.StoreGuid != null)
                    {
                        hp.HiDealProductOptionItemUpdateSellQuantity(returnedOptionIds, (-returnedDetail.ItemQuantity));
                    }
                }

                #endregion

                if (cashTrustLogsForReturn.Count == 0)
                {
                    View.ShowMessage("查無此退貨單明細資料");
                    return;
                }

                var hiDealCoupons = hp.HiDealCouponGetListByProdcutId(couponReturnedDetails.First().ProductId);

                foreach (var cashTrustLog in cashTrustLogsForReturn)
                {
                    long couponId;
                    if (cashTrustLog.CouponId != null && long.TryParse(cashTrustLog.CouponId.ToString(), out couponId))
                    {
                        var coupon = hiDealCoupons.First(x => x.Id == couponId);
                        //檢查是否為有效憑證
                        if (coupon.IsLoaded && coupon.CouponIsEnabled())
                        {
                            HiDealCoupon newCoupon;
                            HiDealCouponManager.GenerateHiDealCouponByRefundCouponId(coupon, out newCoupon);
                        }
                    }
                }
            }

            #endregion 憑證檔先行補產憑證動作

            //修改OrderShow狀態為退貨中
            HiDealOrderFacade.HiDealOrderShowUpdate(order.Pk, HiDealOrderShowStatus.RefundProcessing, true);

            //完成作業，重新查詢。
            GetHiDealOrderDetailData();
        }

        private void OnReturnedFormCancel(object sender, DataEventArgs<Guid> e)
        {
            HiDealOrder order = hp.HiDealOrderGet(e.Data);
            if (!order.IsLoaded)
            {
                View.ShowMessage("查無訂單。");
                return;
            }
            string message;
            if (!HiDealReturnedManager.ReturnedCancelWork(order, View.UserName, CancelReturnedFormReason, out message))
            {
                View.ShowMessage(message);
            }

            //完成作業，重新查詢。
            GetHiDealOrderDetailData();
        }

        private void OnInvoiceUpdate(object sender, DataEventArgs<InvoiceUpdateData> e)
        {
            EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(EinvoiceMain.Columns.OrderGuid,
                View.OrderGuid.ToString(), true);

            InvoiceUpdateData data = e.Data;
            foreach (EinvoiceMain invoice in einvCol)
            {
                if (invoice.IsDonateMark || string.IsNullOrEmpty(invoice.InvoiceNumber) == false)
                {
                    continue;
                }

                bool invoiceFrom3To2 = invoice.IsEinvoice3 && data.InvoiceType == InvoiceMode2.Duplicate;
                bool invoiceFrom2To3 = invoice.IsEinvoice2 && data.InvoiceType == InvoiceMode2.Triplicate;

                if (data.InvoiceType == InvoiceMode2.Triplicate)
                {
                    invoice.InvoiceMode = 0; //the field will be delete
                }
                else if (data.InvoiceType == InvoiceMode2.Duplicate)
                {
                    invoice.InvoiceMode = 0; //the field will be delete
                }
                invoice.InvoiceComName = data.CompanyName;
                invoice.InvoiceComId = data.CompanyId;
                invoice.InvoiceBuyerName = data.BuyerName;
                invoice.InvoiceBuyerAddress = data.BuyerAddress;
                invoice.InvoiceMailbackPaper = data.InvoiceFormBack;
                invoice.InvoiceMailbackAllowance = data.AllowanceFormBack;

                if (invoice.Version == (int)InvoiceVersion.CarrierEra)
                {
                    invoice.InvoiceMode2 = Convert.ToInt32(data.InvoiceType) == (int)InvoiceMode2.Triplicate
                        ? (int)InvoiceMode2.Triplicate
                        : (int)InvoiceMode2.Duplicate;

                    if (invoiceFrom2To3 || invoiceFrom3To2)
                    {
                        invoice.CarrierType = (int)CarrierType.None;
                        invoice.CarrierId = null;
                    }
                    else
                    {
                        invoice.CarrierType = (int)data.CarrierType;
                        invoice.CarrierId = data.CarrierId;
                    }
                }

                if (invoiceFrom2To3)
                {
                    //二聯發票轉三聯式, 要押VerifiedTime, 以免發票Job 未產生發票號碼; 還要押上InvoiceRequestTime, 才能被抓到並印出
                    if (invoice.VerifiedTime == null)
                    {
                        invoice.VerifiedTime = DateTime.Now;
                    }
                    invoice.InvoiceRequestTime = DateTime.Now;
                }
                else if (invoice.OrderIsPponitem == false && invoiceFrom3To2) //憑證發票、3聯轉2聯
                {
                    invoice.VerifiedTime = OrderFacade.GetCouponVerifiedTime(invoice.CouponId.Value,
                        (OrderClassification)invoice.OrderClassification.Value);
                }

                invoice.Message = data.Message;
                op.EinvoiceSetMain(invoice);

            }
            GetHiDealOrderDetailData();
        }

        protected void OnUpdateReturned(object sender, DataEventArgs<RefundFormBackInfo> e)
        {
            HiDealReturned returned = hp.HiDealReturnedGet(e.Data.ReturnedId);
            returned.RefundFormBack = e.Data.IsRefundFormBack;
            hp.HiDealReturnedSet(returned);
            View.ShowMessage("退貨申請單回收狀態已更新");
        }

        protected void OnAddOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            OrderShip os = new OrderShip();
            os.OrderClassification = (int)OrderClassification.HiDeal;
            os.OrderGuid = View.OrderGuid;
            os.ShipCompanyId = e.Data.ShipCompanyId;
            //檢查出貨日期是否大於等於 "預計出貨日期"
            HiDealProduct prod = hp.HiDealProductGet(View.ProductId);
            if (e.Data.ShipTime >= (DateTime)prod.UseStartTime)
            {
                os.ShipTime = e.Data.ShipTime;
            }
            else
            {
                View.alertMessage = "您所填的出貨日期必須大於等於預計出貨日期!";
                return;
            }
            //檢查運單編號是否重複
            if (hp.IsDuplicateHiDealShipNo(prod.Id, e.Data.ShipNo, null))
            {
                View.alertMessage = "運單編號重複, 請選擇別的單號!";
                return;
            }
            //出貨備註長度限制
            if (e.Data.ShipMemo.Length > View.ShipMemoLimit)
            {
                View.alertMessage = string.Format("出貨備註只限最多 {0} 個字。", View.ShipMemoLimit);
                return;
            }
            os.ShipMemo = e.Data.ShipMemo;
            os.ShipNo = e.Data.ShipNo;
            os.CreateId = View.CurrentUser;
            os.CreateTime = DateTime.Now;
            os.ModifyId = View.CurrentUser;
            os.ModifyTime = DateTime.Now;
            os.Id = op.OrderShipSet(os);
            if (os.Id > 0)
            {
                View.alertMessage = "新增成功!";
                //寫Log
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = os.ShipCompanyId;
                osl.ShipTime = os.ShipTime;
                osl.ShipNo = os.ShipNo;
                osl.ShipMemo = os.ShipMemo;
                osl.CreateId = os.CreateId;
                osl.CreateTime = os.CreateTime;
                op.OrderShipLogSet(osl);
            }
        }

        protected void OnDeleteOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            OrderShip os = op.GetOrderShipById(e.Data.Id);
            if (op.OrderShipDelete(e.Data.Id))
            {
                View.alertMessage = "刪除成功!";
                //寫Log
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = null;
                osl.ShipTime = null;
                osl.ShipNo = null;
                osl.ShipMemo = null;
                osl.CreateId = View.CurrentUser;
                osl.CreateTime = DateTime.Now;
                op.OrderShipLogSet(osl);
            }
        }

        protected void OnUpdateOrderShip(object sender, DataEventArgs<OrderShipArgs> e)
        {
            OrderShip os = op.GetOrderShipById(e.Data.Id);
            OrderShip osForLog = os;
            os.ShipCompanyId = e.Data.ShipCompanyId;
            os.ShipTime = e.Data.ShipTime;
            //檢查出貨日期是否大於等於 "預計出貨日期"
            HiDealProduct prod = hp.HiDealProductGet(View.ProductId);
            if (e.Data.ShipTime >= (DateTime)prod.UseStartTime)
            {
                os.ShipTime = e.Data.ShipTime;
            }
            else
            {
                View.alertMessage = "修改失敗! 您所填的出貨日期必須大於預計出貨日期!";
                return;
            }

            //檢查運單編號是否重複
            if (hp.IsDuplicateHiDealShipNo(prod.Id, e.Data.ShipNo, e.Data.Id))
            {
                View.alertMessage = "修改失敗! 運單編號重複, 請選擇別的單號!";
                return;
            }

            //出貨備註長度限制
            if (e.Data.ShipMemo.Length > View.ShipMemoLimit)
            {
                View.alertMessage = string.Format("出貨備註只限最多 {0} 個字。", View.ShipMemoLimit);
                return;
            }
            os.ShipMemo = e.Data.ShipMemo;
            os.ShipNo = e.Data.ShipNo;
            os.ModifyId = View.CurrentUser;
            os.ModifyTime = DateTime.Now;
            if (op.OrderShipUpdate(os))
            {
                View.alertMessage = "修改成功!";
                OrderShipLog osl = new OrderShipLog();
                osl.OrderShipId = os.Id;
                osl.ShipCompanyId = os.ShipCompanyId;
                osl.ShipTime = os.ShipTime;
                osl.ShipNo = os.ShipNo;
                osl.ShipMemo = os.ShipMemo;
                osl.CreateId = os.ModifyId;
                osl.CreateTime = os.ModifyTime;
                op.OrderShipLogSet(osl);
            }
        }

        #endregion event

        #region method

        private void GetHiDealOrderDetailData()
        {
            var orderDetails = hp.HiDealOrderDetailGetListByOrderGuid(View.OrderGuid);
            var order = hp.HiDealOrderGet(View.OrderGuid);
            var m = mp.MemberGet(order.UserId);
            var couponList = hp.ViewHiDealCouponTrustStatusGetList(0, 0, ViewHiDealCouponTrustStatus.Columns.Sequence,
                                                                   ViewHiDealCouponTrustStatus.Columns.OrderPk + "=" +
                                                                   order.Pk);
            var returneds = hp.HiDealReturnedGetListByOrderPk(order.Pk);
            //查詢發票資訊
            EinvoiceMainCollection invCol = op.EinvoiceMainCollectionGetByOrderId(order.OrderId, OrderClassification.HiDeal);
            //查詢付款狀況
            PaymentTransactionCollection paymentTransColl = op.PaymentTransactionGetList(0, 0,
                                                                                         PaymentTransaction.Columns.PaymentType,
                                                                                         PaymentTransaction.Columns.TransId + " = " + order.OrderId,
                                                                                         PaymentTransaction.Columns.OrderClassification + " = " + (int)OrderClassification.HiDeal);
            //查詢商品資料
            //目前已訂單中第一個商品資料為準
            var orderDetail = orderDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).First();
            var product = hp.HiDealProductGet(orderDetail.ProductId);

            View.IsRefundCashOnly = false;
            if (couponList.Count() > 0)
            {
                CashTrustLog ct = mp.CashTrustLogGet(couponList.First().TrustId.Value);
                if (ct != null)
                {
                    View.IsRefundCashOnly = PaymentFacade.IsRefundCashOnly(ct);
                }
            }

            CashTrustLogCollection ctCol = mp.CashTrustLogGetListByOrderGuid(View.OrderGuid, OrderClassification.HiDeal);
            View.GoodsReturnStatus = OrderFacade.GoodsReturnStatusGet(ctCol);

            //商品檔
            List<HiDealInvoiceGoodsInfo> hiDealInvoiceGoodsInfos = new List<HiDealInvoiceGoodsInfo>();
            if (orderDetail.DeliveryType == (int)HiDealDeliveryType.ToHouse)
            {
                var EinvoiceMainCol = op.EinvoiceMainCollectionGetByOrderId(order.OrderId, OrderClassification.HiDeal);

                ViewHiDealCouponTrustStatus couponTrustStatus = couponList.Where(x => x.OrderDetailGuid == orderDetail.Guid).First();
                HiDealInvoiceGoodsInfo hiDealInvoiceGoodsInfo = new HiDealInvoiceGoodsInfo();
                hiDealInvoiceGoodsInfo.OrderItemName = orderDetail.ProductName;
                hiDealInvoiceGoodsInfo.OrderItemQuantity = orderDetail.ItemQuantity;
                hiDealInvoiceGoodsInfo.CouponStatus = couponTrustStatus.LogSataus;

                if (EinvoiceMainCol.Any())
                {
                    EinvoiceMain einvoice = EinvoiceMainCol.First();
                    hiDealInvoiceGoodsInfo.InvoiceNumber = einvoice.InvoiceNumber;
                    hiDealInvoiceGoodsInfo.InvoiceWinning = einvoice.InvoiceWinning;
                    var returned = (returneds.OrderByDescending(x => x.ApplicationTime).ToList());
                    if (returned.Count > 0)
                    {
                        string pageId = HiDealReturnedManager.GetRefundFormPageId(returned.First().Id,
                                                                                  returned.First().OrderPk,
                                                                                  returned.First().CreateId);
                        hiDealInvoiceGoodsInfo.DiscountSingleFormURL = cp.SiteUrl +
                                                                       "/service/hidealrefundform.ashx?FormType=DSF&id=" +
                                                                       pageId;
                    }
                    else
                    {
                        hiDealInvoiceGoodsInfo.DiscountSingleFormURL = "#";
                    }
                    hiDealInvoiceGoodsInfo.IsInvoiceMailbackAllowance = einvoice.InvoiceMailbackAllowance;
                    hiDealInvoiceGoodsInfo.IsInvoiceMailbackPaper = einvoice.InvoiceMailbackPaper;
                    hiDealInvoiceGoodsInfo.EInvoiceId = einvoice.Id;
                }
                else
                {
                    //查無發票資料情況，紅利購買情況
                    hiDealInvoiceGoodsInfo.InvoiceNumber = "";
                    hiDealInvoiceGoodsInfo.InvoiceWinning = false;
                    hiDealInvoiceGoodsInfo.DiscountSingleFormURL = "#";
                    hiDealInvoiceGoodsInfo.IsInvoiceMailbackAllowance = false;
                    hiDealInvoiceGoodsInfo.IsInvoiceMailbackPaper = false;
                    hiDealInvoiceGoodsInfo.EInvoiceId = 0;
                }
                hiDealInvoiceGoodsInfos.Add(hiDealInvoiceGoodsInfo);
            }
            //憑證檔
            List<HiDealInvoiceCouponInfo> hiDealInvoiceCouponInfos = new List<HiDealInvoiceCouponInfo>();
            if (orderDetail.DeliveryType == (int)HiDealDeliveryType.ToShop)
            {
                var einvoiceMainCol = op.EinvoiceMainCollectionGetByOrderId(order.OrderId, OrderClassification.HiDeal);

                foreach (var coupon in couponList)
                {
                    HiDealInvoiceCouponInfo hiDealInvoiceCouponInfo = new HiDealInvoiceCouponInfo();
                    hiDealInvoiceCouponInfo.CouponSequence = coupon.Prefix + coupon.Sequence;
                    hiDealInvoiceCouponInfo.CouponCode = coupon.Code;
                    hiDealInvoiceCouponInfo.CouponStatus = coupon.LogSataus;
                    hiDealInvoiceCouponInfo.IsReservationLock = coupon.IsReservationLock;

                    if (einvoiceMainCol.Where(x => x.CouponId == coupon.Id).Any())
                    {
                        var einvoice = einvoiceMainCol.Where(x => x.CouponId == coupon.Id).First();
                        hiDealInvoiceCouponInfo.InvoiceNumber = einvoice.InvoiceNumber;
                        hiDealInvoiceCouponInfo.InvoiceWinning = einvoice.InvoiceWinning;
                        var returned = (returneds.OrderByDescending(x => x.ApplicationTime).ToList());
                        if (returned.Count > 0)
                        {
                            string pageId = HiDealReturnedManager.GetRefundFormPageId(returned.First().Id,
                                                                                      returned.First().OrderPk,
                                                                                      returned.First().CreateId);
                            hiDealInvoiceCouponInfo.DiscountSingleFormURL = cp.SiteUrl +
                                                                            "/service/hidealrefundform.ashx?FormType=DSF&cid=" +
                                                                            coupon.Id + "&id=" + pageId;
                        }
                        else
                        {
                            hiDealInvoiceCouponInfo.DiscountSingleFormURL = "#";
                        }
                        hiDealInvoiceCouponInfo.IsInvoiceMailbackAllowance = einvoice.InvoiceMailbackAllowance;
                        hiDealInvoiceCouponInfo.IsInvoiceMailbackPaper = einvoice.InvoiceMailbackPaper;
                        hiDealInvoiceCouponInfo.EInvoiceId = einvoice.Id;
                    }
                    else
                    {
                        //無EinvoiceMain
                        hiDealInvoiceCouponInfo.InvoiceNumber = "";
                        hiDealInvoiceCouponInfo.InvoiceWinning = false;
                        hiDealInvoiceCouponInfo.DiscountSingleFormURL = "#";
                        hiDealInvoiceCouponInfo.IsInvoiceMailbackAllowance = false;
                        hiDealInvoiceCouponInfo.IsInvoiceMailbackPaper = false;
                        hiDealInvoiceCouponInfo.EInvoiceId = 0;
                    }
                    hiDealInvoiceCouponInfos.Add(hiDealInvoiceCouponInfo);
                }
            }

            View.ShowPageData(orderDetails, order, m, couponList, returneds, invCol, paymentTransColl, product, hiDealInvoiceGoodsInfos, hiDealInvoiceCouponInfos);
        }

        private void GetHiDealOrderShipData()
        {
            View.ShipCompanyCol = op.ShipCompanyGetList(null);
            View.SetDeliverData(op.ViewOrderShipListGetListByOrderGuid(View.OrderGuid));
        }

        /// <summary>
        /// 強制退貨
        /// </summary>
        /// <param name="orderGuid">退貨訂單編號</param>
        /// <param name="isCashBack">退回現金</param>
        private void ReturnAllPass(Guid orderGuid, bool isCashBack)
        {
            #region 檢查訂單資料

            var order = hp.HiDealOrderGet(orderGuid);

            string message;
            HiDealOrderDetailCollection orderDetails;
            //檢查是否符合強制退貨規定
            if (!HiDealReturnedManager.CheckOrderDataForAllReturn(order, true, out message, out orderDetails))
            {
                View.ShowMessage(message);
                return;
            }

            #endregion 檢查訂單資料

            //建立退貨單
            //後台退貨，退貨原因預設為客服退貨
            //是否檢查發票與退貨申請書，此作業為強制退貨，一律不檢查。
            const bool isAllPass = true;
            var returned = new HiDealReturned();
            if (
                !HiDealReturnedManager.HiDealReturnedSetByOrderWithAllReturn(order, isCashBack, View.UserName,
                                                                             MakeReturnedFormReason,
                                                                             out returned))
            {
                throw new Exception("無法建立退貨單，請去查看前面的LOG HiDealOrder.pk:" + order.Pk);
            }

            //憑證數量先補回判定
            GenerateHiDealCouponByRefundId(returned);

            if (!HiDealReturnedManager.RefundWork(returned.Id, View.UserName, isAllPass, false, out message))
            {
                View.ShowMessage(message);
                //修改OrderShow退款失敗
                //HiDealOrderFacade.HiDealOrderShowUpdate(order.Pk, HiDealOrderShowStatus.RefundFail, true);
                //throw new Exception("無法退款，請去查看前面的LOG HiDealOrder.pk:" + order.Pk);
            }
            else
            {
                //修改OrderShow退貨完成。
                HiDealOrderFacade.HiDealOrderShowUpdate(order.Pk, HiDealOrderShowStatus.RefundSuccess, true);
            }
        }

        /// <summary>
        /// 將以建立的退貨單完成退貨作業。
        /// </summary>
        /// <param name="returnedId"></param>
        private void CompleteReturned(int returnedId)
        {
            const bool isAllPass = false;
            string message;

            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                View.ShowMessage("查無退貨單。");
                return;
            }
            if (returned.Status == (int)HiDealReturnedStatus.Completed)
            {
                View.ShowMessage("已完成退貨，不可在退貨。");
                return;
            }
            if (returned.Status == (int)HiDealReturnedStatus.Cancel)
            {
                View.ShowMessage("退貨單已作廢，不可退貨。");
                return;
            }


            if (!HiDealReturnedManager.RefundWork(returnedId, View.UserName, isAllPass, false, out message))
            {
                View.ShowMessage(message);
                //修改OrderShow退款失敗
                //HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundFail, true);
            }
            else
            {
                //修改OrderShow退貨完成。
                HiDealOrderFacade.HiDealOrderShowUpdate(returned.OrderPk, HiDealOrderShowStatus.RefundSuccess, true);
            }
        }

        /// <summary>
        /// 憑證申請退貨成功，先補產憑證
        /// </summary>
        /// <param name="returned">退貨單Id</param>
        private void GenerateHiDealCouponByRefundId(HiDealReturned returned)
        {
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returned.Id);

            if (returnedDetails.Count == 0)
            {
                throw new DataCheckException<HiDealReturnedViewErrorType>("查無此退貨單明細資料",
                                                                          HiDealReturnedViewErrorType.OrderIdError);
            }

            //查詢訂單資料
            var hidealOrder = hp.HiDealOrderGet(returned.OrderPk);

            //檢查是否為憑證退貨單
            var couponReturnedDetails = returnedDetails.Where(x => x.DeliveryType == (int)HiDealDeliveryType.ToShop && x.ProductType == (int)HiDealProductType.Product);
            if (couponReturnedDetails.Any())
            {

                //查詢登記要退貨的coupon資料
                var returnedCoupons = hp.HiDealReturnedCouponGetList(returned.Id);
                //先取出此訂單所有的CashTrustLog，減少分次查詢的壓力
                var cashTrustLogs = mp.CashTrustLogGetListByOrderGuid(hidealOrder.Guid, OrderClassification.HiDeal);

                #region 整理所以要處理的CashTrustLog

                //憑證退貨申請成功先行補產憑證動作。
                foreach (var returnedDetail in returnedDetails)
                {
                    var returnedOptionIds = HiDealReturnedManager.ReturnedDetailGetReturnedOptionIds(returnedDetail);
                    if (returnedDetail.ProductType == (int)HiDealProductType.Product && returnedOptionIds.Count > 0 &&
                        returnedDetail.StoreGuid != null)
                    {
                        hp.HiDealProductOptionItemUpdateSellQuantity(returnedOptionIds, (-returnedDetail.ItemQuantity));
                    }
                }

                //可以進行退貨的所有cashtrustLog
                var cashTrustLogsForReturn = new List<CashTrustLog>();
                foreach (var returnedDetail in returnedDetails)
                {
                    //運費另外處理
                    if (returnedDetail.ProductType == (int)HiDealProductType.Freight)
                    {
                        continue;
                    }
                    var allowTrustStatus =
                        HiDealReturnedManager.GetTrustStatusWithAllowReturns(
                            (HiDealDeliveryType)returnedDetail.DeliveryType);
                    var returnCouponIds = (from returnedCoupon in returnedCoupons
                                           where returnedCoupon.ReturnedDetailId == returnedDetail.Id
                                           select returnedCoupon.CouponId).ToList();
                    var trustLogs =
                        cashTrustLogs.Where(
                            x =>
                            x.CouponId != null &&
                            (allowTrustStatus.Contains((TrustStatus)x.Status) &&
                             returnCouponIds.Contains((long)x.CouponId))).ToList();
                    cashTrustLogsForReturn.AddRange(trustLogs);
                }

                #endregion

                if (cashTrustLogsForReturn.Count == 0)
                {
                    throw new DataCheckException<HiDealReturnedViewErrorType>("查無可退貨憑證",
                                                                              HiDealReturnedViewErrorType.OrderIdError);
                }

                foreach (var cashTrustLog in cashTrustLogsForReturn)
                {
                    HiDealCoupon newCoupon;
                    if (cashTrustLog.CouponId != null)
                    {
                        var coupon = hp.HiDealCouponGet((long)cashTrustLog.CouponId);
                        //檢查是否為有效憑證
                        if (coupon.IsLoaded && coupon.CouponIsEnabled())
                        {
                            HiDealCouponManager.GenerateHiDealCouponByRefundCouponId(coupon, out newCoupon);
                        }

                    }
                }
            }
        }
        #endregion

        #region 新增客服紀錄

        protected void OnSetServiceMsgClicked(object sender, DataEventArgs<ServiceMessage> e)
        {
            mp.ServiceMessageSet(e.Data);
        }

        protected void SetServiceMessageCategory() 
        {
            View.BuildServiceCategory(MemberFacade.ServiceMessageCategoryGetListByParentId(null), View.ddlServiceCategory);
        }

        #endregion 新增客服紀錄

        protected void OnSendRefundApplicationFormNotification(object sender, DataEventArgs<RefundFormPageInfo> e)
        {
            int returnedId = e.Data.ReturnedId;
            int orderPk = e.Data.OrderPk;
            string userName = e.Data.UserName;

            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                //查無退貨單
                View.ShowMessage("查無退貨單。");
                return;
            }
            //退貨申請書PDF URL
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returnedId, orderPk, userName);
            var refundFormUrl = cp.SiteUrl + "/service/hidealrefundform.ashx?FormType=RAF&id=" + pageId;

            //查詢退貨明細
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);
            string dealName = string.Empty;
            string productName = string.Empty;

            foreach (var returnedDetail in returnedDetails)
            {
                var deal = hp.HiDealDealGet(returnedDetail.HiDealId);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product)
                {
                    dealName = deal.Name;
                    productName = returnedDetail.ProductName;
                }
            }

            Member member = mp.MemberGet(returned.UserId);

            var mailData = new RefundApplicationFormNotificationInformation
            {
                BuyerName = member.DisplayName,
                OrderItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundItemName = string.Format("{0} {1}", dealName, productName),
                RefundItemsCount = returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundApplicationUrl = refundFormUrl,
                HDmailHeaderUrl = cp.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                HDmailFooterUrl = cp.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
            };
            HiDealMailFacade.SendRefundApplicationFormNotification(member.UserEmail, mailData);
        }

        protected void OnSendDiscountSingleFormNotification(object sender, DataEventArgs<RefundFormPageInfo> e)
        {
            int returnedId = e.Data.ReturnedId;
            int orderPk = e.Data.OrderPk;
            string userName = e.Data.UserName;
            int einvoiceId = e.Data.EinvoiceId;

            var returned = hp.HiDealReturnedGet(returnedId);
            if (!returned.IsLoaded)
            {
                //查無退貨單
                View.ShowMessage("查無退貨單。");
                return;
            }

            //Einvoice_main
            EinvoiceMain einvoice = op.EinvoiceMainGetById(einvoiceId);


            //折讓單PDF URL
            var pageId = HiDealReturnedManager.GetRefundFormPageId(returnedId, orderPk, userName);
            string invoiceMailbackAllowanceUrl;
            if (einvoice.CouponId != null)
            {
                invoiceMailbackAllowanceUrl = cp.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&cid=" + einvoice.CouponId + "&id=" + pageId;
            }
            else
            {
                invoiceMailbackAllowanceUrl = cp.SiteUrl + "/service/hidealrefundform.ashx?FormType=DSF&id=" + pageId;
            }


            //查詢退貨明細
            var returnedDetails = hp.HiDealReturnedDetailGetListByReturnedId(returnedId);
            string dealName = string.Empty;
            string productName = string.Empty;

            foreach (var returnedDetail in returnedDetails)
            {
                var deal = hp.HiDealDealGet(returnedDetail.HiDealId);
                if (returnedDetail.ProductType == (int)HiDealProductType.Product)
                {
                    dealName = deal.Name;
                    productName = returnedDetail.ProductName;
                }
            }

            Member member = mp.MemberGet(returned.UserId);

            var mailData = new DiscountSingleFormNotificationInformation()
            {
                BuyerName = member.DisplayName,
                OrderItemsCount = (einvoice.CouponId != null) ? 1 : returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                RefundItemName = string.Format("{0} {1}", dealName, productName),
                RefundItemsCount = (einvoice.CouponId != null) ? 1 : returnedDetails.Where(x => x.ProductType == (int)HiDealProductType.Product).Sum(x => x.ItemQuantity),
                InvoiceMailbackAllowanceUrl = invoiceMailbackAllowanceUrl,
                HDmailHeaderUrl = cp.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Header.png",
                HDmailFooterUrl = cp.SiteUrl + "/Themes/HighDeal/images/mail/HDmail_Footer.png"
            };

            HiDealMailFacade.SendDiscountSingleFormNotification(member.UserEmail, mailData);
        }

        protected void OnUpdateEInvoiceMainInfo(object sender, DataEventArgs<EInvoiceMainInfo> e)
        {
            EInvoiceMainInfo eInvoiceMainInfo = (EInvoiceMainInfo)e.Data;
            op.EinvoiceUpdateComInfo(eInvoiceMainInfo.EInvoiceId, eInvoiceMainInfo.IsInvoiceMailbackPaper, eInvoiceMainInfo.IsInvoiceMailbackAllowance);
            GetHiDealOrderDetailData();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class MiniBankManagePresenter : Presenter<IMiniBankManageView>
    {
        const int DEFAULT_TRANSAC_SHOWN = 5;

        private IMemberProvider mp;
        public MiniBankManagePresenter()
        {
            mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetAccountGrid();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.UpdateAccount += OnUpdateAccount;
            View.DeleteAccount += OnDeleteAccount;
            View.ShowTransactionDetail += OnShowTransactionDetail;
            View.CloseTransactionDetail += OnCloseTransactionDetail;
            return true;
        }

        private void OnCloseTransactionDetail(object sender, EventArgs e)
        {
            View.DetailId = null;
        }

        private void OnShowTransactionDetail(object sender, DataEventArgs<KeyValuePair<int, bool>> e)
        {
            View.DetailId = e.Data.Key;
            SetDetailGrid(e.Data.Key, e.Data.Value);
        }

        private void SetAccountGrid()
        {
            View.SetAccountGrid(mp.ViewMiniBankSummaryGetList(View.UserId, ViewMiniBankSummary.Columns.AccountName));
        }

        private void SetDetailGrid(int id, bool showAll)
        {
            ViewMiniBankTransactionCollection col = showAll ? mp.ViewMiniBankTransactionGetList(id, ViewMiniBankTransaction.Columns.CreateTime) : mp.ViewMiniBankTransactionGetListLastN(id, ViewMiniBankTransaction.Columns.CreateTime, DEFAULT_TRANSAC_SHOWN);
            col.Sort("Id",true);
            View.SetDetailGrid(col);
        }

        private void OnDeleteAccount(object sender, DataEventArgs<int> e)
        {
            mp.MiniBankDelete(e.Data);
            SetAccountGrid();
        }

        private void OnUpdateAccount(object sender, DataEventArgs<KeyValuePair<string, decimal>> e)
        {
            if (e.Data.Value == 0) return;

            string comment = e.Data.Value > 0 ? View.Localization.GetString("Deposit") : View.Localization.GetString("Withdraw");
            int mbId = mp.MiniBankTransactionUpdate(View.UserId, e.Data.Key, comment, e.Data.Value, DateTime.Now);
            SetAccountGrid();
            if (View.DetailId.HasValue && View.DetailId.Value == mbId)
                SetDetailGrid(mbId, false);
        }
    }
}

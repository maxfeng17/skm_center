﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System.Text;
using System.IO;
using NewtonsoftJson = Newtonsoft.Json;

namespace LunchKingSite.WebLib.Presenters
{
    public class BrandSetUpPresenter : Presenter<IBrandSetUp>
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ICmsProvider cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        public ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.GetStringBrandDIscountStatus += OnGetStringBrandDIscountStatus;
            GetBrandList();
            View.InitBrandCities(GetPponCities());
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveBrand += OnSaveBrand;
            View.UpdateBrandStatus += OnUpdateBrandStatus;
            View.SetBrandShowInApp += OnSetBrandShowInApp;
            View.SetBrandShowInWeb += OnSetBrandShowInWeb;
            View.Search += OnSearch;
            View.ChangeMode += OnChangeMode;
            View.GetBrand += OnGetBrand;
            
            View.ImportBidList += OnImportBidList;
            View.UpdateBrandItemStatus += OnUpdateBrandItemStatus;
            View.UpdateSeqStatus += OnUpdateSeqStatus;
            View.GetBrandItemStatus += OnGetBrandItemStatus;
            View.GetBrandItemCategory += OnGetBrandItemCategory;
            View.UpdateBrandItemSeq += OnUpdateBrandItemSeq;
            View.UpdateBrandItemCategorySeq += OnUpdateBrandItemCategorySeq;
            View.UpdateBrandItemCategoryName += OnUpdateBrandItemCategoryName;
            View.DeleteBrandItemCategory += OnDeleteBrandItemCategory;
            View.SearchBrandItem += OnSearchBrandItem;
            View.SaveBrandItemCategory += OnSaveBrandItemCategory;
            View.BatchEditBrandItemCategory += OnBatchEditBrandItemCategory;
            View.DeleteBrandItemList += OnDeleteBrandItemList;

            if (View.BrandId > 0) //For Dialog調整
            {
                View.SetBrandItemCategoryForDialog(pp.GetBrandItemCategoryByBrandId(View.BrandId));
            }
            return true;
        }

        public void OnSearch(object sender, EventArgs e)
        {
            GetBrandList();
        }
        public void OnChangeMode(object sender, EventArgs e)
        {
            View.ChangePanel(View.Mode);
            if (View.Mode == 1) //add curation mode
            {
                View.SetBrandCities(GetPponCities(), new ViewCmsRandomCollection());
            }
        }
        public void OnGetBrand(object sender, EventArgs e)
        {
            Brand brand = pp.GetBrand(View.BrandId);
            View.SetBrandCities(GetPponCities(), cmp.ViewCmsRandomGetCollectionByBrandId(View.BrandId));
            View.SetBrand(brand);
            View.SetBrandItemCategory(pp.GetBrandItemCategoryByBrandId(View.BrandId));
            View.SetBrandItemList(GetBrandItemList());
        }
        public void OnUpdateBrandStatus(object sender, EventArgs e)
        {
            pp.UpdateBrandStatus(View.BrandId);
            //CmsRandomContent需同步
            var brand = pp.GetBrand(View.BrandId);
            var crc = cmp.CmsRandomContentGetByBrandId(View.BrandId);
            crc.Status = brand.Status;
            cmp.CmsRandomContentSet(crc);

            ClearBrandCache();
            GetBrandList();
        }
        public void OnSetBrandShowInApp(object sender, DataEventArgs<bool> e)
        {
            Brand brand = pp.GetBrand(View.BrandId);
            if (brand.IsLoaded)
            {
                brand.ShowInApp = e.Data;
                MarketingFacade.SaveCurationAndResetMemoryCache(brand, EventPromoEventType.CurationTwo);
            }
            GetBrandList();
        }
        public void OnSetBrandShowInWeb(object sender, DataEventArgs<bool> e)
        {
            Brand brand = pp.GetBrand(View.BrandId);
            if (brand.IsLoaded)
            {
                brand.ShowInWeb = e.Data;
                MarketingFacade.SaveCurationAndResetMemoryCache(brand, EventPromoEventType.CurationTwo);
            }
            GetBrandList();
        }
        public void OnSaveBrand(object sender, DataEventArgs<BrandUpdateModel> e)
        {
            Brand brand;
            if (e.Data.MainBrand.Id != 0)
            {
                brand = pp.GetBrand(e.Data.MainBrand.Id);
                brand.Mk1ActName = e.Data.MainBrand.Mk1ActName;
                brand.Mk1Url = e.Data.MainBrand.Mk1Url;
                brand.Mk2ActName = e.Data.MainBrand.Mk2ActName;
                brand.Mk2Url = e.Data.MainBrand.Mk2Url;
                brand.Mk3ActName = e.Data.MainBrand.Mk3ActName;
                brand.Mk3Url = e.Data.MainBrand.Mk3Url;
                brand.Mk4ActName = e.Data.MainBrand.Mk4ActName;
                brand.Mk4Url = e.Data.MainBrand.Mk4Url;
                brand.Mk5ActName = e.Data.MainBrand.Mk5ActName;
                brand.Mk5Url = e.Data.MainBrand.Mk5Url;
                brand.BrandName = e.Data.MainBrand.BrandName;
                brand.Cpa = e.Data.MainBrand.Cpa;
                brand.Url = e.Data.MainBrand.Url;
                brand.StartTime = e.Data.MainBrand.StartTime;
                brand.EndTime = e.Data.MainBrand.EndTime;
                brand.Bgpic = e.Data.MainBrand.Bgpic;
                brand.Bgcolor = e.Data.MainBrand.Bgcolor;
                brand.HotItemBid1 = e.Data.MainBrand.HotItemBid1;
                brand.HotItemBid2 = e.Data.MainBrand.HotItemBid2;
                brand.HotItemBid3 = e.Data.MainBrand.HotItemBid3;
                brand.HotItemBid4 = e.Data.MainBrand.HotItemBid4;
                brand.BtnOriginalUrl = e.Data.MainBrand.BtnOriginalUrl;
                brand.BtnHoverUrl = e.Data.MainBrand.BtnHoverUrl;
                brand.BtnActiveUrl = e.Data.MainBrand.BtnActiveUrl;
                brand.BtnOriginalFontColor = e.Data.MainBrand.BtnOriginalFontColor;
                brand.BtnHoverFontColor = e.Data.MainBrand.BtnHoverFontColor;
                brand.BtnActiveFontColor = e.Data.MainBrand.BtnActiveFontColor;
                brand.Description = e.Data.MainBrand.Description;
                brand.DealPromoTitle = e.Data.MainBrand.DealPromoTitle;
                brand.DealPromoDescription = e.Data.MainBrand.DealPromoDescription;
                brand.DiscountList = e.Data.MainBrand.DiscountList;
                brand.MetaDescription = e.Data.MainBrand.MetaDescription;
                MarketingFacade.SaveCurationAndResetMemoryCache(brand, EventPromoEventType.CurationTwo);
            }
            else
            {
                MarketingFacade.SaveCurationAndResetMemoryCache(e.Data.MainBrand, EventPromoEventType.CurationTwo);
                brand = pp.GetBrand(e.Data.MainBrand.Id);
                View.BrandId = e.Data.MainBrand.Id;

                try
                {
                    #region 自動建立 Curation Banner
                    CmsRandomContent content = new CmsRandomContent();
                    content.Title = brand.BrandName;
                    content.ContentTitle = "Curation Banner";
                    content.ContentName = "curation";
                    string url = string.Format("{0}/event/brandevent.aspx?u={1}{2}", config.SiteUrl, brand.Url, (!string.IsNullOrEmpty(brand.Cpa) ? "&cpa=" + brand.Cpa : ""));
                    content.Body = string.Format("<a href='{0}'>{1}</a>", url, brand.BrandName);
                    content.Locale = "zh_TW";
                    content.Status = true;
                    content.CreatedOn = DateTime.Now;
                    content.CreatedBy = View.UserName;
                    content.ModifiedOn = DateTime.Now;
                    content.ModifiedBy = View.UserName;
                    content.Type = (int)RandomCmsType.PponMasterPage;
                    content.BrandId = brand.Id;
                    cmp.CmsRandomContentSet(content);
                    #endregion
                }
                catch (Exception ex)
                {
                    var jsonS = new JsonSerializer();
                    log4net.LogManager.GetLogger("LogToDb")
                        .Error(
                            string.Format(
                                "BrandSetUpPresenter.cs Curation Banner,error:Exception:{0},login name:{1}",
                                ex.Message,
                                HttpContext.Current.User.Identity.Name));

                }
            }

            if (e.Data.UpdateCmsRandomCities != null && e.Data.UpdateCmsRandomCities.Update)
            {
                CmsRandomContent content = cmp.CmsRandomContentGetByBrandId(brand.Id);
                if (content.IsLoaded)
                {
                    string url = string.Format("{0}/event/brandevent.aspx?u={1}{2}", config.SiteUrl, brand.Url, (!string.IsNullOrEmpty(brand.Cpa) ? "&cpa=" + brand.Cpa : ""));
                    content.Title = brand.BrandName;
                    content.Body = string.Format("<a href='{0}'>{1}</a>", url, brand.BrandName);
                    cmp.CmsRandomContentSet(content);

                    Dictionary<int, int> dicOldSeq = new Dictionary<int, int>();
                    foreach (var vcr in cmp.ViewCmsRandomGetCollectionByBrandId(brand.Id))
                    {
                        dicOldSeq.Add(vcr.CityId,vcr.Seq.GetValueOrDefault()); //保留原有策展排序
                        cmp.CmsRandomCityDelete(vcr.Id);
                    }
                    foreach (var cityId in e.Data.UpdateCmsRandomCities.CityIds)
                    {
                        CmsRandomCity city = new CmsRandomCity();
                        city.Pid = content.Id;
                        city.StartTime = brand.StartTime;
                        city.EndTime = brand.EndTime;
                        city.Status = true;
                        city.CreatedBy = View.UserName;
                        city.CreatedOn = DateTime.Now;
                        city.Ratio = 1;
                        city.CityId = cityId;                        
                        city.Type = (int)RandomCmsType.PponMasterPage;
                        city.CityName = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityId).CityName;

                        foreach (KeyValuePair<int, int> item in dicOldSeq)
                        {
                            if (cityId == item.Key) city.Seq = item.Value;
                        }
                        cmp.CmsRandomCitySet(city);
                    }
                }
            }

            if (e.Data.MainPicPostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.MainPicPostedFile, UploadFileType.BrandWebImage, brand.Id.ToString()+ "_WebPic");
                brand.MainPic = imagePath;
            }
            else
            {
                if (View.IsDelMainPic == "true")
                {
                    brand.MainPic = "";
                }
            }

            if (e.Data.MainBackgroundPicPostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.MainBackgroundPicPostedFile, UploadFileType.BrandWebImage, brand.Id.ToString() + "_WebBackPic");
                brand.MainBackgroundPic = imagePath;
            }
            else
            {
                if (View.IsDelMainBackgroundPic == "true")
                {
                    brand.MainBackgroundPic = "";
                }
            }

            if (e.Data.MobileMainPicPostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.MobileMainPicPostedFile, UploadFileType.BrandWebImage, brand.Id.ToString() + "_MPic");
                brand.MobileMainPic = imagePath;
            }
            else
            {
                if (View.IsDelMobileMainPic == "true")
                {
                    brand.MobileMainPic = "";
                }
            }

            if (e.Data.AppBannerPostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.AppBannerPostedFile, UploadFileType.EventPromoAppBanner, brand.Id.ToString());
                brand.AppBannerImage = imagePath;
            }
            else
            {
                if (View.IsDelAppBannerImage == "true")
                {
                    brand.AppBannerImage = "";
                }
            }

            if (e.Data.AppPromoPostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.AppPromoPostedFile, UploadFileType.EventPromoAppMainImage, brand.Id.ToString());
                brand.AppPromoImage = imagePath;
            }
            else
            {
                if (View.IsDelAppPromoImage == "true")
                {
                    brand.AppPromoImage = "";
                }
            }

            int iBannerType = 0;
            if (brand.AppPromoImage != null)
            {
                iBannerType += (int)EventBannerType.MainUp;
            }
            if (brand.AppBannerImage != null)
            {
                foreach (int i in e.Data.BannerType) {
                    iBannerType += i;
                }
            }
            brand.BannerType = iBannerType;

            if (e.Data.MarketingWebPostedFile1 != null)
            {
                brand.Mk1WebImage = brand.Mk1AppImage = e.Data.MarketingWebPostedFile1;
            }
            else
            {
                if (!string.IsNullOrEmpty(View.IsDelImgWeb1))
                {
                    brand.Mk1WebImage = brand.Mk1AppImage = "";
                }
            }

            if (e.Data.MarketingWebPostedFile2 != null)
            {
                brand.Mk2WebImage = brand.Mk2AppImage = e.Data.MarketingWebPostedFile2;
            }
            else
            {
                if (!string.IsNullOrEmpty(View.IsDelImgWeb2))
                {
                    brand.Mk2WebImage = brand.Mk2AppImage = "";

                }
            }

            if (e.Data.MarketingWebPostedFile3 != null)
            {
                brand.Mk3WebImage = brand.Mk3AppImage = e.Data.MarketingWebPostedFile3;
            }
            else
            {
                if (string.IsNullOrEmpty(View.IsDelImgWeb3))
                {
                    brand.Mk3WebImage = brand.Mk3AppImage = "";
                }
            }

            if (e.Data.MarketingWebPostedFile4 != null)
            {
                brand.Mk4WebImage = brand.Mk4AppImage = e.Data.MarketingWebPostedFile4;
            }
            else
            {
                if (string.IsNullOrEmpty(View.IsDelImgWeb4))
                {
                    brand.Mk4WebImage = brand.Mk4AppImage = "";
                }
            }

            if (e.Data.MarketingWebPostedFile5 != null)
            {
                brand.Mk5WebImage = brand.Mk5AppImage = e.Data.MarketingWebPostedFile5;
            }
            else
            {
                if (string.IsNullOrEmpty(View.IsDelImgWeb5))
                {
                    brand.Mk5WebImage = brand.Mk5AppImage = "";
                }
            }

            if (e.Data.DealPromoImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.DealPromoImagePostedFile, UploadFileType.DealPromoImage, brand.Id.ToString());
                brand.DealPromoImage = imagePath;
            }
            else
            {
                if (View.IsDelDealPromoImage == "true")
                {
                    brand.DealPromoImage = "";
                }
            }

            if (e.Data.DiscountBannerImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.DiscountBannerImagePostedFile, UploadFileType.BrandDiscountBannerImage, brand.Id.ToString());
                brand.DiscountBannerImage = imagePath;
            }
            else
            {
                if (View.IsDelDiscountBannerImage == "true")
                {
                    brand.DiscountBannerImage = "";
                }
            }

            if (e.Data.EventListImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.EventListImagePostedFile, UploadFileType.EventListImage, brand.Id.ToString());
                brand.EventListImage = imagePath;
            }
            else
            {
                if (View.IsDelEventListImage == "true")
                {
                    brand.EventListImage = "";
                }
            }

            if (e.Data.ChannelListImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.ChannelListImagePostedFile, UploadFileType.ChannelListImage, brand.Id.ToString());
                brand.ChannelListImage = imagePath;
            }
            else
            {
                if (View.IsDelChannelListImage == "true")
                {
                    brand.ChannelListImage = "";
                }
            }

            if (e.Data.WebRelayImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.WebRelayImagePostedFile, UploadFileType.WebRelayImage, brand.Id.ToString());
                brand.WebRelayImage = imagePath;
            }
            else
            {
                if (View.IsDelWebRelayImage == "true")
                {
                    brand.WebRelayImage = "";
                }
            }

            if (e.Data.MobileRelayImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.MobileRelayImagePostedFile, UploadFileType.MobileRelayImage, brand.Id.ToString());
                brand.MobileRelayImage = imagePath;
            }
            else
            {
                if (View.IsDelMobileRelayImage == "true")
                {
                    brand.MobileRelayImage = "";
                }
            }

            if (e.Data.FbShareImagePostedFile != null)
            {
                string imagePath = UploadImageFile(e.Data.FbShareImagePostedFile, UploadFileType.FbShareImage, brand.Id.ToString());
                brand.FbShareImage = imagePath;
            }
            else
            {
                if (View.IsDelFbShareImage == "true")
                {
                    brand.FbShareImage = "";
                }
            }

            MarketingFacade.SaveCurationAndResetMemoryCache(brand, EventPromoEventType.CurationTwo);

            string s = BrandToJson(brand);
            pp.ChangeLogInsert("Brand", View.BrandId.ToString(), s);

            OnGetBrand(sender, e);
            GetBrandList();
            View.AlertMsg("已更新完成!");
        }
        protected void GetBrandList()
        {
            List<Brand> brands = null;
            bool isSearch = true;
            if (View.SearchCityId == "" && string.IsNullOrWhiteSpace(View.SearchWord)) isSearch = false;

            if (isSearch)
            {
                List<int> brandIdList = cmp.GetBrandListBySearch(View.SearchWord, View.SearchCityId, RandomCmsType.PponMasterPage);
                if (brandIdList.Count > 0)
                {
                    if (View.IsLimitInThreeMonth)
                    {
                        brands = pp.BrandGetByBrandIdList(brandIdList).Where(x => x.StartTime >= DateTime.Now.AddMonths(-3)).OrderByDescending(x => x.Id).ToList();
                    }
                    else
                    {
                        brands = pp.BrandGetByBrandIdList(brandIdList).OrderByDescending(x => x.Id).ToList();
                    }
                }
            }
            else
            {
                if (View.IsLimitInThreeMonth)
                {
                    brands = pp.BrandGetList(Brand.Columns.StartTime + ">=" + DateTime.Now.AddMonths(-3)).ToList();
                }
                else
                {
                    brands = pp.BrandGetList().OrderByDescending(x => x.Id).ToList();
                }
            }

            View.SetBrandList(brands);
        }
        public void OnImportBidList(object sender, DataEventArgs<List<string>> e)
        {
            string alertMsg = string.Empty;
            int itemCatagoryAllId = 0;
            BrandItemCollection itemCollection = PponFacade.ValidateAndGetItemListAndAlertMsg(e.Data, View.BrandId, View.UserName, out alertMsg);

            if (!string.IsNullOrWhiteSpace(alertMsg))
            {
                View.AlertMsg(alertMsg);
            }

            if (itemCollection.Count > 0)
            {
                pp.SaveBrandItemList(itemCollection);
            }

            var  itemCatagoryAll = pp.GetBrandItemCategoryByBrandId(View.BrandId).Where(x => x.Name.Equals("全部")).First();
            if (itemCatagoryAll != null) //所有匯入商品要綁定"全部"分類
            {
                int.TryParse(itemCatagoryAll.Id.ToString(), out itemCatagoryAllId);
                if (itemCatagoryAllId > 0)
                {
                    PponFacade.EditBrandItemCategory("Add", itemCatagoryAllId, itemCollection.Select(x => x.Id).ToList(), View.BrandId);
                }
            }

            ClearBrandCache();
            View.SetBrandItemList(GetBrandItemList());
            View.AlertMsg("已匯入完成!");
        }
        protected ViewBrandItemCollection GetBrandItemList()
        {
            var items = pp.GetViewBrandItemListbySearch(View.BrandId, View.SearchItemName, View.SearchItemCid);            
            if (config.EnableGrossMarginRestrictions)
            {
                foreach (var vepctmp in items)
                {
                    vepctmp.BusinessHourStatus = op.GetHourStatusBybid(vepctmp.BusinessHourGuid).BusinessHourStatus;
                    vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.DisplayGrossMargin;
                    if ((vepctmp.BusinessHourStatus & (int)BusinessHourStatus.LowGrossMarginAllowedDiscount) > 0)
                    {
                        vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.UseDiscount;
                    }
                    vepctmp.GrossMargin = Math.Round(PponFacade.GetDealMinimumGrossMargin(vepctmp.BusinessHourGuid) * 100, 2 , MidpointRounding.AwayFromZero);

                    if (vepctmp.GrossMargin <= Convert.ToDecimal(config.GrossMargin))
                    {
                        vepctmp.GrossMarginStatus = vepctmp.GrossMarginStatus | (int)EventPromoSetUpDiscountType.RedWord;
                    }

                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vepctmp.BusinessHourGuid);
                    if (deal != null && deal.OrderedTotal != null)
                    {
                        vepctmp.OrderedTotal = Convert.ToDecimal(deal.OrderedTotal);
                        vepctmp.DiscountLimtType = op.DiscountLimitEnabledGetByBid(vepctmp.BusinessHourGuid);
                    }
                }
            }
            return items;
        }
        public void OnUpdateBrandItemSeq(object sender, DataEventArgs<KeyValuePair<int, Dictionary<int, int>>> e)
        {
            int mainId = e.Data.Key;
            Dictionary<int, int> dictionaryIdAndSeq = e.Data.Value;
            UpdateBrandItemSeq(mainId, dictionaryIdAndSeq);
            ClearBrandCache();
        }
        public void OnUpdateBrandItemCategorySeq(object sender, DataEventArgs<KeyValuePair<int, Dictionary<int, int>>> e)
        {
            int mainId = e.Data.Key;
            Dictionary<int, int> dictionaryIdAndSeq = e.Data.Value;
            UpdateBrandItemCategorySeq(mainId, dictionaryIdAndSeq);
            ClearBrandCache();
        }
        public void OnUpdateBrandItemCategoryName(object sender, DataEventArgs<KeyValuePair<int, Dictionary<int, string>>> e)
        {
            int mainId = e.Data.Key;
            Dictionary<int, string> dictionaryIdAndName = e.Data.Value;
            UpdateBrandItemCategoryName(mainId, dictionaryIdAndName);
            ClearBrandCache();
        }
        public void OnDeleteBrandItemCategory(object sender, DataEventArgs<KeyValuePair<int, List<int>>> e)
        {
            int mainId = e.Data.Key;
            List<int> idList = e.Data.Value;
            DeleteBrandItemCategory(mainId, idList);
            ClearBrandCache();
        }
        public void OnUpdateBrandItemStatus(object sender, EventArgs e)
        {
            pp.UpdateBrandItemStatus(View.ItemId);
            ClearBrandCache();
            View.SetBrandItemList(GetBrandItemList());
        }
        public void OnUpdateSeqStatus(object sender, EventArgs e)
        {
            pp.UpdateSeqStatus(View.ItemId);
            ClearBrandCache();
            View.SetBrandItemList(GetBrandItemList());
        }
        public string OnGetBrandItemStatus(ViewBrandItem item)
        {
            DateTime startDate = item.BusinessHourOrderTimeS;
            DateTime endDate = item.BusinessHourOrderTimeE;
            return GetBrandItemStatus(item, startDate, endDate);
        }
        public string OnGetBrandItemCategory(int brandItemId)
        {
            string category = "";
            List<ViewBrandCategory> vbcList = pp.GetViewBrandCategoryByBrandItemId(brandItemId).Where(x => x.CategoryName != "全部").OrderBy(y => y.BrandItemCategoryId).ToList();
            category = string.Join("/", vbcList);
            return category;
        }
        private string GetBrandItemStatus(ViewBrandItem brandItem, DateTime startDate, DateTime endDate)
        {
            string status = "";

            if (DateTime.Now < startDate)
            {
                status = "未上檔";
            }
            else
            {
                IViewPponDeal pponDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(brandItem.BusinessHourGuid);

                if (pponDeal.GroupOrderStatus != null && Helper.IsFlagSet((long)pponDeal.GroupOrderStatus, GroupOrderStatus.Completed))
                {
                    status = "已結檔";
                }
                else if (DateTime.Now >= endDate)
                {
                    status = "已下檔";
                }
                else
                {
                    if (pponDeal != null && (pponDeal.OrderedQuantity >= pponDeal.OrderTotalLimit))
                    {
                        status = "已售完";
                    }
                    else
                    {
                        status = "已上檔";
                    }
                }

            }

            return status;
        }

        protected void UpdateBrandItemSeq(int mainId, Dictionary<int, int> dictionaryIdAndSeq)
        {
            BrandItemCollection brandItems = pp.GetBrandItemList(mainId);

            foreach (BrandItem item in brandItems)
            {
                if (dictionaryIdAndSeq.Keys.Contains(item.Id))
                {
                    int newSeq;
                    dictionaryIdAndSeq.TryGetValue(item.Id, out newSeq);
                    item.Seq = newSeq;
                }
            }

            pp.SaveBrandItemList(brandItems);
            View.SetBrandItemList(GetBrandItemList());
            View.AlertMsg("排序已更新！");
        }
        protected void UpdateBrandItemCategorySeq(int mainId, Dictionary<int, int> dictionaryIdAndSeq)
        {
            BrandItemCategoryCollection brandItemCategorys = pp.GetBrandItemCategoryByBrandId(mainId);

            foreach (BrandItemCategory itemCategory in brandItemCategorys)
            {
                if (dictionaryIdAndSeq.Keys.Contains(itemCategory.Id))
                {
                    int newSeq;
                    dictionaryIdAndSeq.TryGetValue(itemCategory.Id, out newSeq);
                    itemCategory.Seq = newSeq;
                }
            }

            pp.SaveBrandItemCategoryList(brandItemCategorys);
            View.SetBrandItemCategory(pp.GetBrandItemCategoryByBrandId(View.BrandId));
            View.AlertMsg("分類排序已更新！");
        }

        protected void UpdateBrandItemCategoryName(int mainId, Dictionary<int, string> dictionaryIdAndName)
        {
            BrandItemCategoryCollection brandItemCategorys = pp.GetBrandItemCategoryByBrandId(mainId);

            foreach (BrandItemCategory itemCategory in brandItemCategorys)
            {
                if (dictionaryIdAndName.Keys.Contains(itemCategory.Id))
                {
                    string newName;
                    dictionaryIdAndName.TryGetValue(itemCategory.Id, out newName);
                    itemCategory.Name = newName;
                }
            }

            pp.SaveBrandItemCategoryList(brandItemCategorys);
            View.AlertMsg("分類名稱已更新！");
            View.SetBrandItemCategory(pp.GetBrandItemCategoryByBrandId(View.BrandId));
        }
        protected void DeleteBrandItemCategory(int mainId, List<int> idList)
        {
            var count = 1;
            foreach (var cid in idList)
            {
                if (cid!=0)
                {
                    pp.DeleteBrandItemCategoryByCategoryId(mainId, cid);
                }
            }

            BrandItemCategoryCollection brandItemCategorys = pp.GetBrandItemCategoryByBrandId(mainId).OrderByAsc(BrandItemCategory.Columns.Seq);

            foreach (var brandItemCategory in brandItemCategorys)
            {
                if (brandItemCategory.Seq != 0)
                {
                    brandItemCategory.Seq = count;
                    count++;
                }
            }

            pp.SaveBrandItemCategoryList(brandItemCategorys);
            Brand brand = pp.GetBrand(View.BrandId);
            View.SetBrand(brand);
            View.AlertMsg("分類已刪除！");
        }

        private string UploadImageFile(HttpPostedFile uploadedFile, UploadFileType fileType, string prefix = "")//, ref PponDeal entity)
        {
            if (uploadedFile != null)
            {
                string fileName = prefix + "_" + fileType.ToString();
                string fileNameWithExtension = fileName + "." + Helper.GetExtensionByContentType(uploadedFile.ContentType) + "?ts=" + DateTime.Now.Ticks.ToString("x"); //for 圖片被 瀏覽器 或 APP Cache住舊圖加上時間雜湊函數
                ImageUtility.UploadFile(uploadedFile.ToAdapter(), fileType, "Brand", fileName);

                return ImageFacade.GenerateMediaPath("Brand", fileNameWithExtension);
            }
            return string.Empty;
        }
        private static List<PponCity> GetPponCities()
        {
            List<PponCity> cities = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting();
            string[] cityUnwant = {
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.PEZ.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.Tmall.CityCode };
            return cities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();
        }
        
        public void OnSearchBrandItem(object sender, EventArgs e)
        {
            View.SetBrandItemList(GetBrandItemList());
        }

        public void OnBatchEditBrandItemCategory(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            //取得勾選的商品
            List<int> selectItems = View.SelectedBrandItem;

            int bicId = int.TryParse(e.Data.Value, out bicId) ? bicId : 0;
            PponFacade.EditBrandItemCategory(e.Data.Key, bicId, selectItems, View.BrandId);

            ClearBrandCache();
            View.SetBrandItemList(GetBrandItemList());

            if (e.Data.Key == "Replace")
            {
                View.AlertMsg("取代商品類別頁籤：已完成！");
            }
            else if (e.Data.Key == "Add")
            {
                View.AlertMsg("新增商品類別頁籤：已完成！");
            }
        }

        public void OnSaveBrandItemCategory(object sender, DataEventArgs<KeyValuePair<int, string>> e)
        {
            if (!pp.GetBrandItemCategoryByName(e.Data.Value, View.BrandId).Any())
            {
                BrandItemCategory bic = new BrandItemCategory();
                bic.BrandId = View.BrandId;
                bic.Name = e.Data.Value;
                bic.CreateId = View.UserName;
                bic.CreateTime = DateTime.Now;
                bic.Seq = pp.BrandItemCategoryGetMaxSequenceByBrandId(View.BrandId) + 1;
                pp.SaveBrandItemCategory(bic);
                ClearBrandCache();

                View.SetBrandItemCategory(pp.GetBrandItemCategoryByBrandId(View.BrandId));
                View.AlertMsg("類別：" + e.Data.Value + "已新增！");
            }
            else
            {
                View.AlertMsg("此類別重複,無法新增！");
            }
        }

        

        private int count;
        public void OnDeleteBrandItemList(object sender, EventArgs e)
        {
            //取得勾選的商品
            List<int> selectItems = View.SelectedBrandItem;            
            count = 0;
            foreach (var itemId in selectItems)
            {
                DeleteBrandItem(itemId);
                count ++;
            }
            ClearBrandCache();

            pp.ChangeLogInsert("BrandDeleteItems", View.BrandId.ToString(), string.Format("已刪除{0}筆商品資料 : ", count) + NewtonsoftJson.JsonConvert.SerializeObject(selectItems));

            View.AlertMsg(string.Format("已刪除{0}筆商品資料", count));
            View.SetBrandItemList(GetBrandItemList());
        }

        protected void DeleteBrandItem(int id)
        {
            BrandItem item = pp.GetBrandItem(id);
            if (item != null)
            {
                pp.DeleteBrandItem(item);
                pp.DeleteBrandItemCategoryDependency(id); //刪除策展商品分類資料
            }
        }
        
        public string OnGetStringBrandDIscountStatus(int brandId)
        {
            if (op.DiscountLimitEnabledGetByBrandId(brandId))
            {
                return "<br /><font color='red'>(內有禁用檔次請調整)</font>";
            }
            else
            {
                return "";
            }
        }
        
        private void ClearBrandCache()
        {
            var cacheKey = "B" + View.BrandId;
            SystemFacade.ClearCacheByObjectNameAndCacheKey("ApiEventPromo", cacheKey);
        }

        private string BrandToJson(Brand brand)
        {
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            using (NewtonsoftJson.JsonWriter writer = new NewtonsoftJson.JsonTextWriter(sw))
            {
                writer.Formatting = NewtonsoftJson.Formatting.None;
                writer.WriteStartObject();
                writer.WritePropertyName("brand_name");
                writer.WriteValue(brand.BrandName);
                writer.WritePropertyName("url");
                writer.WriteValue(brand.Url);
                writer.WritePropertyName("cpa");
                writer.WriteValue(brand.Cpa);
                writer.WritePropertyName("start_time");
                writer.WriteValue(brand.StartTime == null ? "n/a" : brand.StartTime.ToString("o"));
                writer.WritePropertyName("end_time");
                writer.WriteValue(brand.EndTime == null ? "n/a" : brand.EndTime.ToString("o"));
                writer.WritePropertyName("status");
                writer.WriteValue(brand.Status);
                writer.WritePropertyName("main_pic");
                writer.WriteValue(brand.MainPic);
                writer.WritePropertyName("mobile_main_pic");
                writer.WriteValue(brand.MobileMainPic);
                writer.WritePropertyName("show_in_web");
                writer.WriteValue(brand.ShowInWeb);
                writer.WritePropertyName("show_in_app");
                writer.WriteValue(brand.ShowInApp);
                writer.WritePropertyName("bgpic");
                writer.WriteValue(brand.Bgpic);
                writer.WritePropertyName("bgcolor");
                writer.WriteValue(brand.Bgcolor);
                writer.WritePropertyName("app_banner_image");
                writer.WriteValue(brand.AppBannerImage);
                writer.WritePropertyName("app_promo_Image");
                writer.WriteValue(brand.AppPromoImage);
                writer.WritePropertyName("mk1_act_name");
                writer.WriteValue(brand.Mk1ActName);
                writer.WritePropertyName("mk1_url");
                writer.WriteValue(brand.Mk1Url);
                writer.WritePropertyName("mk1_web_image");
                writer.WriteValue(brand.Mk1WebImage);
                writer.WritePropertyName("mk1_app_image");
                writer.WriteValue(brand.Mk1AppImage);
                writer.WritePropertyName("mk2_act_name");
                writer.WriteValue(brand.Mk2ActName);
                writer.WritePropertyName("mk2_url");
                writer.WriteValue(brand.Mk2Url);
                writer.WritePropertyName("mk2_web_image");
                writer.WriteValue(brand.Mk2WebImage);
                writer.WritePropertyName("mk2_app_image");
                writer.WriteValue(brand.Mk2AppImage);
                writer.WritePropertyName("mk3_act_name");
                writer.WriteValue(brand.Mk3ActName);
                writer.WritePropertyName("mk3_url");
                writer.WriteValue(brand.Mk3Url);
                writer.WritePropertyName("mk3_web_image");
                writer.WriteValue(brand.Mk3WebImage);
                writer.WritePropertyName("mk3_app_image");
                writer.WriteValue(brand.Mk3AppImage);
                writer.WritePropertyName("mk4_act_name");
                writer.WriteValue(brand.Mk4ActName);
                writer.WritePropertyName("mk4_url");
                writer.WriteValue(brand.Mk4Url);
                writer.WritePropertyName("mk4_web_image");
                writer.WriteValue(brand.Mk4WebImage);
                writer.WritePropertyName("mk4_app_image");
                writer.WriteValue(brand.Mk4AppImage);
                writer.WritePropertyName("mk5_act_name");
                writer.WriteValue(brand.Mk5ActName);
                writer.WritePropertyName("mk5_url");
                writer.WriteValue(brand.Mk5Url);
                writer.WritePropertyName("mk5_web_image");
                writer.WriteValue(brand.Mk5WebImage);
                writer.WritePropertyName("mk5_app_image");
                writer.WriteValue(brand.Mk5AppImage);
                writer.WritePropertyName("hot_item_bid1");
                writer.WriteValue(brand.HotItemBid1 == null ? "" : brand.HotItemBid1.ToString());
                writer.WritePropertyName("hot_item_bid2");
                writer.WriteValue(brand.HotItemBid2 == null ? "" : brand.HotItemBid2.ToString());
                writer.WritePropertyName("hot_item_bid3");
                writer.WriteValue(brand.HotItemBid3 == null ? "" : brand.HotItemBid3.ToString());
                writer.WritePropertyName("hot_item_bid4");
                writer.WriteValue(brand.HotItemBid4 == null ? "" : brand.HotItemBid4.ToString());
                writer.WritePropertyName("btn_original_url");
                writer.WriteValue(brand.BtnOriginalUrl);
                writer.WritePropertyName("btn_original_font_color");
                writer.WriteValue(brand.BtnOriginalFontColor);
                writer.WritePropertyName("btn_hover_url");
                writer.WriteValue(brand.BtnHoverUrl);
                writer.WritePropertyName("btn_hover_font_color");
                writer.WriteValue(brand.BtnOriginalFontColor);
                writer.WritePropertyName("btn_active_url");
                writer.WriteValue(brand.BtnActiveUrl);
                writer.WritePropertyName("btn_active_font_color");
                writer.WriteValue(brand.BtnActiveFontColor);
                writer.WritePropertyName("description");
                writer.WriteValue(brand.Description);
                writer.WritePropertyName("deal_promo_title");
                writer.WriteValue(brand.DealPromoTitle);
                writer.WritePropertyName("deal_promo_image");
                writer.WriteValue(brand.DealPromoImage);
                writer.WritePropertyName("deal_promo_description");
                writer.WriteValue(brand.DealPromoDescription);
                writer.WritePropertyName("banner_type");
                writer.WriteValue(brand.BannerType);
                writer.WritePropertyName("discount_list");
                writer.WriteValue(brand.DiscountList);
                writer.WritePropertyName("discount_banner_image");
                writer.WriteValue(brand.DiscountBannerImage);
                writer.WritePropertyName("event_list_image");
                writer.WriteValue(brand.EventListImage);
                writer.WritePropertyName("fb_share_image");
                writer.WriteValue(brand.FbShareImage);
                writer.WritePropertyName("channel_list_image");
                writer.WriteValue(brand.ChannelListImage);
                writer.WritePropertyName("web_relay_image");
                writer.WriteValue(brand.WebRelayImage);
                writer.WritePropertyName("mobile_relay_image");
                writer.WriteValue(brand.MobileRelayImage);
                writer.WritePropertyName("main_background_pic");
                writer.WriteValue(brand.MainBackgroundPic);
                writer.WriteEndObject();
            }
            return sb.ToString();
        }
    }
}

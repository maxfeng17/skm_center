using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;


namespace LunchKingSite.WebLib.Presenters
{
    public class StorePresenter : Presenter<IStoreView>
    {
        private IEventProvider ep;
        private ICmsProvider cmp;
        private ISellerProvider sp;
        private IPponProvider pp;

        public StorePresenter()
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            cmp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            if (View.SellerGuid == Guid.Empty)
            {
                return false;
            }

            Seller s = sp.SellerGet(View.SellerGuid);
            if (s != null)
            {
                if (string.IsNullOrWhiteSpace(s.SellerInfo)) return false;

                View.SellerName = s.SellerName;
                View.SellerIntroduct = s.SellerInfo;

                string Availability = "[";
                var jsonS = new JsonSerializer();
                foreach (Seller sc in SellerFacade.GetChildrenSellers(View.SellerGuid))
                {
                    Availability += jsonS.Serialize(new { N = sc.SellerName, P = sc.StoreTel, A = sc.StoreAddress, OT = sc.OpenTime, OD = "", U = sc.WebUrl, R = sc.StoreRemark, CD = sc.CloseDate, MR = sc.Mrt, CA = sc.Car, BU = sc.Bus, OV = sc.OtherVehicles, FB = sc.FacebookUrl, BL = sc.BlogUrl, OL = sc.OtherUrl });
                    Availability += ",";
                }
                Availability += "]";
                Availability.Replace(",]", "]");
                View.SetSellerStoreList(Availability);
            }
            else
            {
                return false;
            }

            int _city = View.DealCityId = View.CityId;
            int _categoryId = View.CategoryID;
            var workCityId = _city == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                                               ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                                               : _city;
            int _subRegionCategoryId = View.SubRegionCategoryId;
            var totalmultideals = SetMultipleDeals(0, workCityId, _subRegionCategoryId);

            ViewPponDealCollection vpc = pp.ViewPponDealGetListOnUseBySellerGuid(View.SellerGuid);
            ViewPponDealCollection tempvpc = new ViewPponDealCollection();

            DealTimeSlotCollection dtsc = pp.DealTimeSlotGetToday();

            foreach (var deal in vpc)
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealSub) > 0) continue;  //將子檔過濾掉
                if (!dtsc.Where(x => x.BusinessHourGuid == deal.BusinessHourGuid).Any()) continue;  //將隱藏檔次和過期檔次過濾掉
                deal.DefaultDealImage = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto)
                .FirstOrDefault();
                tempvpc.Add(deal);
            }
            View.SetMutilpStoreDeals(tempvpc);

            #region 所選取區域的檔次

            var multideals = totalmultideals;
            if (_categoryId != 0)
            {
                multideals = totalmultideals.Where(x => x.DealCategoryIdList.Contains(_categoryId)).ToList();
            }

            //今日熱銷
            int hotSaleBigTopSaleCount = 20;
            var hotDeals = multideals.OrderByDescending(x => x.PponDeal.ItemPrice * x.PponDeal.OrderedQuantity)
                                     .Take(multideals.Count > hotSaleBigTopSaleCount ? hotSaleBigTopSaleCount : multideals.Count)
                                     .ToList();
            View.SetHotSaleMutilpMainDeals(hotDeals);

            //最新上檔
            var todayDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeS < DateTime.Now).OrderBy(x => DateTime.Now - x.PponDeal.BusinessHourOrderTimeS).ToList();
            todayDeals = todayDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetToDayMutilpMainDeals(todayDeals);

            //最後倒數
            var lastdaydDeals = multideals.Where(x => x.PponDeal.BusinessHourOrderTimeE > DateTime.Now).OrderBy(x => x.PponDeal.BusinessHourOrderTimeE - DateTime.Now).ToList();
            lastdaydDeals = lastdaydDeals.Take(10).OrderBy(x => System.Guid.NewGuid().ToString()).ToList();
            View.SetLastDayMutilpMainDeals(lastdaydDeals);
            #endregion

            return true;
        }

        private List<MultipleMainDealPreview> SetMultipleDeals(int _categoryId, int workCityId, int subRegionCategoryId)
        {
            int? selectedCategoryId = null;
            int? selectedSubRegionId = null;
            // 使用多主檔排列
            if (_categoryId > 0)
            {
                selectedCategoryId = _categoryId;
            }


            List<MultipleMainDealPreview> multideals;

            //取得目前城市
            PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            //目前城市沒有對應的頻道，目前應該只有24或72小時會這樣，直接設定為台北
            if (!city.ChannelId.HasValue)
            {
                city = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            //需依據不同的狀況設定區域的categoryId，主要是針對旅遊需另外判斷
            int? areaCategoryId = null;
            //判斷是否為旅遊頻道
            if (workCityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                if (View.TravelCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    //針對舊的TravelCategoryId區域，因紀錄於cookie所以須進行處理，避免查無資料
                    areaCategoryId = View.TravelCategoryId;
                }
            }
            else if (workCityId == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                if (View.FemaleCategoryId != CategoryManager.GetDefaultCategoryNode().CategoryId)
                {
                    areaCategoryId = View.FemaleCategoryId;
                }
            }
            else
            {
                areaCategoryId = city.ChannelAreaId;
            }

            List<int> categorylist = new List<int>();

            if (selectedCategoryId.HasValue)
            {
                categorylist.Add(selectedCategoryId.Value);
            }

            if (subRegionCategoryId > 0)
            {
                categorylist.Add(subRegionCategoryId);
                selectedSubRegionId = subRegionCategoryId;
            }
            
            multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(city.ChannelId.Value, areaCategoryId, categorylist);
            
            List<MultipleMainDealPreview> areamultideals;
            if (selectedCategoryId.HasValue)
            {
                areamultideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByCategoryCriteria(city.ChannelId.Value, areaCategoryId, new List<int>());
            }
            else
            {
                areamultideals = multideals;
            }
            return multideals;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
    }
}
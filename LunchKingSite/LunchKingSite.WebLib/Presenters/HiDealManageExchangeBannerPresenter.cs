﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class HiDealManageExchangeBannerPresenter : Presenter<IHiDealManageExchangeBannerView>
    {
        readonly ICmsProvider cmsProv;
        public HiDealManageExchangeBannerPresenter()
        {            
            cmsProv = ProviderFactory.Instance().GetProvider<ICmsProvider>();            
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.InitTimeControl();
            ShowBannerList();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.BannerEditing += OnBannerEditing;
            View.BannerDeleting += OnBannerDeleting;
            View.BannerSaving += OnBannerSaving;
            View.BannerUpdating += OnBannerUpdating;
            View.BannerCancel += OnBannerCancel;
            return true;
        }

        protected void OnBannerCancel(object sender, EventArgs e)
        {
            View.NewBanner();
        }

        protected void OnBannerSaving(object sender, EventArgs e)
        {
            CmsRandomContent cms = new CmsRandomContent();
            cms.Type = (int)RandomCmsType.VBO;
            cms.Status = true;
            cms.Body = View.Content;
            cms.Locale = "zh_TW";
            cms.CreatedOn = DateTime.Now;
            cms.CreatedBy = View.UserName;
            cms.ModifiedOn = DateTime.Now;
            cms.ModifiedBy = View.UserName;

            CmsRandomCity city = new CmsRandomCity();
            //寫死
            city.Ratio = 1;
            city.Status = true;
            city.CityId = -1;
            city.CityName = "不分區";
            city.Type = (int)RandomCmsType.VBO;
            city.StartTime = View.StartTime.Value;
            city.EndTime = View.EndTime.Value;
            city.CreatedOn = DateTime.Now;
            city.CreatedBy = View.UserName;
            city.ModifiedOn = DateTime.Now;
            city.ModifideBy = View.UserName;

            SaveBanner(city, cms);
            ShowBannerList();
        }

        protected void OnBannerUpdating(object sender, EventArgs e)
        {
            int id;
            if (int.TryParse(View.ContentID, out id))
            {
                UpdateBanner(id);
            }
            else
            {
                throw new Exception(string.Format("ContentID必需為數字，目前的值為'{0}'", View.ContentID));
            }
            ShowBannerList();
        }

        protected void OnBannerEditing(object sender, DataEventArgs<int> e)
        {
            int pid = e.Data;
            View.ContentID = pid.ToString();
            CmsRandomContent cms = cmsProv.CmsRandomContentGetById(pid);
            CmsRandomCity city = cmsProv.CmsRandomCityGetByPidNType(cms.Id, cms.Type);
            View.Content = cms.Body;

            View.StartTime = city.StartTime;
            View.EndTime = city.EndTime;
        }

        protected void OnBannerDeleting(object sender, DataEventArgs<int> e)
        {
            DeleteBanner(e.Data);
            ShowBannerList();
        }

        private void SaveBanner(CmsRandomCity city, CmsRandomContent cms)
        {
            //TOOD need transaction ?
            cmsProv.CmsRandomContentSet(cms);
            city.Pid = cms.Id;
            cmsProv.CmsRandomCitySet(city);
        }

        private void UpdateBanner(int id)
        {
            CmsRandomContent cms = cmsProv.CmsRandomContentGetById(id);
            cms.Body = View.Content;
            cms.ModifiedOn = DateTime.Now;
            cms.ModifiedBy = View.UserName;
            CmsRandomCity city = cmsProv.CmsRandomCityGetByPidNType(cms.Id, cms.Type);

            if (View.StartTime != null)
            {
                city.StartTime = View.StartTime.Value;
            }
            if (View.EndTime != null)
            {
                city.EndTime = View.EndTime.Value;
            }

            city.ModifiedOn = DateTime.Now;
            city.ModifideBy = View.UserName;

            cmsProv.CmsRandomContentUpdate(cms);
            cmsProv.CmsRandomCityUpdate(city);
        }

        private void DeleteBanner(int cid)
        {
            CmsRandomContent cms = cmsProv.CmsRandomContentGetById(cid);
            cmsProv.CmsRandomContentDelete(cid);
            cmsProv.CmsRandomCityDelete(cms.Id, cms.Type);
        }

        public void ShowBannerList()
        {
            IList<ViewCmsRandom> banners = cmsProv.GetViewCmsRandomCollectionAll(RandomCmsType.VBO)
                .OrderBy(t => t.StartTime).ToList();
            View.FillBannerList(banners);
        }

    }
}

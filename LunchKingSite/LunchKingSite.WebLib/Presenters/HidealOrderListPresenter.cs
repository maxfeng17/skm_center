﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Presenters
{
    public class HidealOrderListPresenter :Presenter<IHidealOrderListView>
    {
        protected IHiDealProvider hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        protected ISystemProvider sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnGetHidealOrderCount += OnGetHidealOrderCount;
            View.HidealOrderPageChanged += OnHidealOrderPageChanged;
            return true;
        }

        #region enum
        public enum FilterType
        {
            OrderId = 0,
            MemberEmail = 1,
            MemberName = 2,
            Mobile = 3,
            ProductName = 4,
            HiDealId = 5,
            ProductId = 6
        }
        #endregion

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetHiDealOrderData(1);
        }
        #endregion

        #region method
        protected void GetHiDealOrderData(int pageNumber)
        {
            ViewHiDealOrderMemberOrderShowCollection dataList = new ViewHiDealOrderMemberOrderShowCollection();
            View.GetHiDealOrderList(hp.ViewHiDealOrderMemberOrderShowDialogDataGet(pageNumber, View.PageSize, GetFilter(), ViewHiDealOrderMemberOrderShow.Columns.CreateTime + " desc "));
        }

        protected void OnGetHidealOrderCount(object sender, DataEventArgs<int> e)
        {
            e.Data = hp.ViewHiDealOrderMemberOrderShowDialogDataGetCount(GetFilter());
        }

        protected void OnHidealOrderPageChanged(object sender, DataEventArgs<int> e)
        {
            GetHiDealOrderData(e.Data);
        }
        #endregion

        #region private method
        private string[] GetFilter()
        {
            IList<string> filter = new List<string>();
            string column = string.Empty;
            switch ((FilterType)View.FilterTypeValue)
            {
                case FilterType.OrderId:
                    column = ViewHiDealOrderMemberOrderShow.Columns.OrderId;
                    break;
                case FilterType.MemberEmail:
                    column = ViewHiDealOrderMemberOrderShow.Columns.UserName;
                    break;
                case FilterType.MemberName:
                    column = ViewHiDealOrderMemberOrderShow.Columns.MemberName;
                    break;
                case FilterType.Mobile:
                    column = ViewHiDealOrderMemberOrderShow.Columns.Mobile;
                    break;
                case FilterType.ProductName:
                    column = ViewHiDealOrderMemberOrderShow.Columns.ProductName;
                    break;
                case FilterType.HiDealId:
                    column = ViewHiDealOrderMemberOrderShow.Columns.HiDealId;
                    break;
                case FilterType.ProductId:
                    column = ViewHiDealOrderMemberOrderShow.Columns.ProductId;
                    break;
                default:
                    break;
            }

            if (!string.IsNullOrEmpty(View.FilterText))
            {
                string value = string.Empty;

                value = View.FilterText;
                value = value.Replace("*", "%");
                value = value.Replace("?", "_");
                filter.Add(column + " like " + value);
            }

            // 購買日期
            if (!string.IsNullOrEmpty(View.FilterInternal))
                filter.Add(View.FilterInternal);

            return filter.ToArray();
        }
        #endregion
    }
}

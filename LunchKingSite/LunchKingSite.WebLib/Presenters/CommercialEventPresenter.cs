﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class CommercialEventPresenter : Presenter<ICommercialEventView>
    {
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            if (!string.IsNullOrWhiteSpace(View.EventCode))
            {
                SetEventPromos();
            }
            else
            {
                View.ShowEventExpire();
            }
            return true;
        }

        #region method
        public void SetEventPromos()
        {
            if (View.CommercialCode != 0)
            {
                CategoryCollection categoryList = sp.CategoryGetList((int)CategoryType.CommercialCircle);
                Category p_cate = categoryList.Where(x => x.Code == View.CommercialCode).FirstOrDefault();
                if (p_cate.IsLoaded)
                {
                    const int takeCount = 3;
                    View.CommercialName = p_cate.Name;
                    Dictionary<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, IViewPponDeal>> dataList = new Dictionary<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, IViewPponDeal>>();
                    EventPromoCollection commList = pp.GetEventPromoList(EventPromoTemplateType.Commercial); // 取出event_promo建好的各館別資料
                    IEnumerable<Category> sub_cates = categoryList.Where(x => x.ParentCode == View.CommercialCode && (commList.Where(y => y.StartDate < DateTime.Now && DateTime.Now <= y.EndDate.AddDays(1) && y.Url.IndexOf(x.Code.ToString(), StringComparison.OrdinalIgnoreCase) >= 0).Select(y => y.Url).Any()));

                    foreach (EventPromo comm in commList.Where(x => x.StartDate < DateTime.Now && DateTime.Now <= x.EndDate.AddDays(1)))
                    {
                        int code = int.TryParse(comm.Url.Replace(View.EventCode, string.Empty), out code) ? code : 0;
                        if (code != 0)
                        {
                            int cateId = sub_cates.Where(x => x.Code.Value == code).FirstOrDefault().Id;
                            Dictionary<EventPromoItem, IViewPponDeal> items = new Dictionary<EventPromoItem, IViewPponDeal>();
                            EventPromoItemCollection exist_items = pp.GetEventPromoItemList(comm.Id, EventPromoItemType.Ppon);
                            if (exist_items.Count > 0)
                            {
                                int takeItem = 0;
                                foreach (EventPromoItem exist_item in exist_items.Where(x => x.Status).OrderBy(x => x.Seq))
                                {
                                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(exist_item.ItemId);
                                    if (deal != null &&  (deal.BusinessHourOrderTimeS <= DateTime.Now && DateTime.Now < deal.BusinessHourOrderTimeE ))
                                    {
                                        items.Add(exist_item, deal);
                                        takeItem++;
                                    }

                                    if (takeItem == takeCount)
                                    {
                                        break;
                                    }
                                }

                                dataList.Add(new KeyValuePair<int, EventPromo>(cateId, comm), items);
                            }
                            else
                            {
                                sub_cates.ToList().RemoveAll(x => x.Id == cateId);
                            }
                        }
                    }

                    View.SetCategory(sub_cates);
                    View.SetEventPromo(dataList);
                }
                else
                {
                    View.ShowEventExpire();
                }
            }
            else
            {
                View.ShowEventExpire();
            }
        }
        #endregion
    }
}

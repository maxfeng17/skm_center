﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class LionTravelCouponListPresenter : Presenter<ILionTravelCouponListView>
    {
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchCoupons += OnSearchCoupons;
            return true;
        }

        void OnSearchCoupons(object sender, DataEventArgs<Coupon> e)
        {
            List<string> filter = new List<string>() { ViewOrderCorrespondingCoupon.Columns.Type + "=" + (int)View.OrderClassType };
            if (!string.IsNullOrEmpty(e.Data.SequenceNumber))
            {
                filter.Add(ViewOrderCorrespondingCoupon.Columns.SequenceNumber + "=" + e.Data.SequenceNumber);
            }
            else
            {
                filter.Add(ViewOrderCorrespondingCoupon.Columns.Code + "=" + e.Data.Code);
            }
            ViewOrderCorrespondingCouponCollection data = op.ViewOrderCorrespondingCouponGetList(filter.ToArray());
            Dictionary<Guid, CashTrustLogCollection> cash_trust_logs = new Dictionary<Guid, CashTrustLogCollection>();
            Dictionary<Guid, CashTrustLogCollection> return_cash_trust_logs = new Dictionary<Guid, CashTrustLogCollection>();
            foreach (var item in data.Select(x => x.OrderGuid).Distinct())
            {
                cash_trust_logs.Add(item, mp.CashTrustLogGetListByOrderGuid(item, OrderClassification.LkSite));
                return_cash_trust_logs.Add(item, mp.PponRefundingCashTrustLogGetListByOrderGuid(item));
            }
            View.SetCoupons(data, cash_trust_logs, return_cash_trust_logs);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System.Data;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Facade;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class PiinlifeListPresenter : Presenter<IPiinlifeListView>
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public PiinlifeListPresenter()
        {
            
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetCategories(View.Channel.CategoryId);
            if (View.BusinessHourGuid != Guid.Empty)
            {
                SetMainDeal(View.BusinessHourGuid);
            }
            else
            {
                SetMutilpMainDeals(View.Channel.CategoryId, View.CategoryId);
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        /// <summary>
        /// 取得分類清單
        /// </summary>
        /// <param name="channelId">頻道</param>
        /// <returns></returns>
        private void SetCategories(int channelId)
        {
            CategoryDealCountNode node = PponDealPreviewManager.GetCategoryDealCountNodeByChannel(channelId, null);

            // 新增一項預設分類
            CategoryDealCount allDealCount = new CategoryDealCount(0, "全部");
            allDealCount.DealCount = node.TotalCount;
            node.DealCountList.Insert(0, allDealCount);

            List<CategoryDealCount> filterNodes =
                PponDealPreviewManager.GetSpecialDealCountListByChannel(channelId, null).Where(x => x.DealCount > 0).ToList();
            View.SetMultipleCategories(node.DealCountList, filterNodes);
        }

        /// <summary>
        /// 設定檔次資料
        /// </summary>
        /// <param name="channelId">頻道</param>
        /// <param name="categoryId">分類(傳入null取得全部檔次資料)</param>
        /// <returns></returns>
        private void SetMutilpMainDeals(int channelId, int? categoryId)
        {
            Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>> dataList = new Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>>();
            List<MultipleMainDealPreview> multipleDeals =
                ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(channelId, null, categoryId);

            SetSubscribeDealContent(multipleDeals);

            foreach (MultipleMainDealPreview mainDeal in multipleDeals)
            {
                IList<ViewComboDeal> combodeals = new List<ViewComboDeal>();
                if (Helper.IsFlagSet(mainDeal.PponDeal.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    combodeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(mainDeal.PponDeal.BusinessHourGuid);
                }
                else
                {
                    combodeals.Add(GetViewComboDealByViewPponDeal(mainDeal.PponDeal));
                }
                dataList.Add(mainDeal, combodeals);
            }
            View.SetMutilpMainDeals(dataList);
        }

        private void SetMainDeal(Guid bid)
        {
            Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>> dataList = new Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>>();
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);

            // check piinlife pponcity
            List<int> citys = new JsonSerializer().Deserialize<List<int>>(deal.CityList);
            if (!citys.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CityId))
            {
                View.RedirectToPponDefault();
                return;
            }

            MultipleMainDealPreview mainDeal = new MultipleMainDealPreview(View.Channel.CityId.Value, 0, deal, new List<int>(), DealTimeSlotStatus.Default, ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid).BaseGrossMargin);
            ViewComboDealCollection combodeals = new ViewComboDealCollection();
            combodeals.Add(GetViewComboDealByViewPponDeal(mainDeal.PponDeal));
            dataList.Add(mainDeal, combodeals);
            View.SetMutilpMainDeals(dataList);
        }

        private static ViewComboDeal GetViewComboDealByViewPponDeal(IViewPponDeal mainDeal)
        {
            int position = mainDeal.ItemName.IndexOf('-') + 1;
            string itemname = position > 0 ? mainDeal.ItemName.Substring(position, mainDeal.ItemName.Length - position) : mainDeal.ItemName;
            ViewComboDeal main = new ViewComboDeal()
            {
                BusinessHourGuid = mainDeal.BusinessHourGuid,
                MainBusinessHourGuid = mainDeal.BusinessHourGuid,
                Title = itemname,
                BusinessHourOrderMinimum = mainDeal.BusinessHourOrderMinimum,
                BusinessHourStatus = mainDeal.BusinessHourStatus,
                OrderTotalLimit = mainDeal.OrderTotalLimit,
                BusinessHourOrderTimeS = mainDeal.BusinessHourOrderTimeS,
                BusinessHourOrderTimeE = mainDeal.BusinessHourOrderTimeE,
                BusinessHourDeliverTimeS = mainDeal.BusinessHourDeliverTimeS,
                BusinessHourDeliverTimeE = mainDeal.BusinessHourDeliverTimeE,
                QuantityMultiplier = mainDeal.QuantityMultiplier,
                UniqueId = mainDeal.UniqueId ?? 0,
                ItemOrigPrice = mainDeal.ItemOrigPrice,
                ItemPrice = mainDeal.ItemPrice,
                Slug = mainDeal.Slug,
                OrderedQuantity = mainDeal.OrderedQuantity ?? 0,
                IsContinuedQuantity = mainDeal.IsContinuedQuantity ?? false,
                ContinuedQuantity = mainDeal.ContinuedQuantity ?? 0,
                CouponUsage = mainDeal.CouponUsage
            };
            return main;
        }

        private void SetSubscribeDealContent(List<MultipleMainDealPreview> multipleDeals)
        {
            Dictionary<Guid, string[]> dealsContent = new Dictionary<Guid, string[]>();
            foreach (ViewPponDeal vpd in multipleDeals.Select(x => x.PponDeal).OrderByDescending(x => x.OrderedQuantity).Take(3))
            {
                string url = ImageFacade.GetMediaPathsFromRawData(vpd.EventImagePath, MediaType.PponDealPhoto).Skip(3).DefaultIfEmpty(config.SiteUrl + "/Themes/default/images/17Life/EDM/EDMDefault.jpg").First();
                string[] dealDesc = new string[] { url, vpd.EventName, vpd.EventTitle, vpd.BusinessHourGuid.ToString() };
                dealsContent.Add(vpd.BusinessHourGuid, dealDesc);
            }
            View.SetSubscribeDealContent(dealsContent);
        }
    }
}

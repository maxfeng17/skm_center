﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Mongo.Services;
using MongoDB.Bson;
using System.Net.Mail;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesBusinessOrderDetailPresenter : Presenter<ISalesBusinessOrderDetailView>
    {
        private ISysConfProvider _config;
        private IHumanProvider _humanProv;
        private ISystemProvider _sysProv;
        private IOrderProvider _ordProv;
        private IPponProvider _pponProv;
        private ISellerProvider _sellerProv;
        private SalesService _salesServ;
        private ProvisionService _provServ;

        public SalesBusinessOrderDetailPresenter(IHumanProvider hp, ISystemProvider sp, IOrderProvider ordProv, IPponProvider pponProv, ISellerProvider sellerProv, SalesService salesServ, ProvisionService provServ)
        {
            _humanProv = hp;
            _sysProv = sp;
            _salesServ = salesServ;
            _provServ = provServ;
            _ordProv = ordProv;
            _pponProv = pponProv;
            _sellerProv = sellerProv;
            _config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetSelectableZone(PponCityGroup.DefaultPponCityGroup.GetPponcityForPublic());
            View.SetSelectableChannel(CategoryManager.PponChannelCategoryTree);
            SetupSelectableTravelCategories();
            SetupSelectableCommercialCategories();
            View.GetSalesDepartment(_humanProv.DepartmentGetListByEnabled(true).ToList().Union(_humanProv.DepartmentGetListByEnabled(true, EmployeeDept.M000)).ToList());
            if (_config.IsProvisionSwitchToSql)
            {
                View.SetProvisionDepartmentsModel(PponFacade.GetAllProvisionDepartmentModel());
            }
            else
            {
                View.SetProvisionDepartments(_provServ.GetProvisionDepartments());
            }            
            View.GetAccBusinessGroupGetList(_sysProv.AccBusinessGroupGetList());
            GetAccountingBankInfo();
            SetupDealLabels();
            SetupDealTypes();
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                SetupDealTypes(businessOrder.DealType);
                GetAccountingBranchInfo(businessOrder.AccountingBankId);
                View.SetBusinessOrderDetail(businessOrder);
                AuthorizationGroupSet();
                // 若工單審核區域不在主管審核權限內則跳出(排除業務主管自己建的單)
                if (View.UserId != businessOrder.CreateId && (!string.IsNullOrEmpty(View.SalesMemberArea) && !View.SalesMemberArea.Split(',').Contains(businessOrder.AuthorizationGroup)))
                {
                    View.RedirectSalesManagementPage();
                }
                // 檢查業務助理僅能查看本區工單 || 自己建的單(有可能從別區轉過來)
                if (View.IsSalesAssistant)
                {
                    if (businessOrder.CreateId != View.UserId)
                    {
                        ViewEmployee emp = _humanProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserId);
                        if (emp.IsLoaded)
                        {
                            if (!string.IsNullOrWhiteSpace(emp.DeptId) && emp.DeptId != businessOrder.SalesDeptId)
                            {
                                View.RedirectSalesManagementPage();
                            }
                        }
                        else
                        {
                            View.RedirectSalesManagementPage();
                        }
                    }
                }
                // 若業務姓名不等於登入人員的姓名則跳出
                if (View.IsSales && !View.IsSalesAssistant && !(!string.IsNullOrEmpty(View.SalesMemberArea) && businessOrder.Status == BusinessOrderStatus.Send))
                {
                    ViewEmployee emp = _humanProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserId);
                    if (emp.IsLoaded)
                    {
                        if (emp.EmpName != businessOrder.SalesName)
                        {
                            View.RedirectSalesManagementPage();
                        }
                    }
                }
            }
            else if (!string.IsNullOrEmpty(View.SellerId))
            {
                SetupSellerInfo(View.SellerId);
            }
            else
            {
                SetSalesMemberData();
            }
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SaveClicked += OnSaveClicked;
            View.DeleteClicked += OnDeleteClicked;
            View.StoreDeleteClicked += OnStoreDeleteClicked;
            View.CopyClicked += OnCopyClicked;
            View.DownloadStoreClicked += OnExportToExcelClicked;
            View.ImportClicked += OnImportClicked;
            View.RefreshStoreClicked += OnRefreshStoreClicked;
            View.ReferredClicked += OnReferredClicked;
            View.DownloadClicked += OnDownloadClicked;
            SetLocationDropDownList();
            return true;
        }

        #region event
        protected void OnAccountingBankSelectedIndexChanged(object sender, DataEventArgs<string> e)
        {
            GetAccountingBranchInfo(e.Data);
        }
        protected void OnSaveClicked(object sender, DataEventArgs<string> e)
        {
            bool isSendMail = false;
            BusinessOrder changeset = new BusinessOrder();
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                // 編輯工單
                BusinessOrder item = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);

                // 由畫面取得工單資訊
                changeset = View.GetBusinessOrderData(item, false, e.Data);

                // 查找檔號
                if (!changeset.BusinessHourGuid.Equals(Guid.Empty) && changeset.UniqueId.Equals(0))
                {
                    DealProperty dealProperty = _pponProv.DealPropertyGet(changeset.BusinessHourGuid);
                    if (dealProperty.IsLoaded)
                        changeset.UniqueId = dealProperty.UniqueId;
                }

                // 工單狀態變更or審核區域異動 時，發送通知信
                isSendMail = !changeset.Status.Equals(item.Status) || changeset.AuthorizationGroup != item.AuthorizationGroup;

                // 設定工單資料
                changeset.Id = item.Id;
                changeset.BusinessOrderId = item.BusinessOrderId;
                changeset.SequenceNumber = item.SequenceNumber;
                changeset.SalesDeptId = item.SalesDeptId;
                changeset.IsNoteHistory = changeset.IsNoteHistory || item.IsNoteHistory;
                changeset.CopyType = item.CopyType;
                changeset.IsContinuedSequence = item.IsContinuedSequence;
                changeset.IsContinuedQuantity = item.IsContinuedQuantity;
                changeset.CopyBusinessOrderId = item.CopyBusinessOrderId;
                changeset.CopyBusinessHourGuid = item.CopyBusinessHourGuid;
                changeset.Store = item.Store;
                changeset.CreateId = item.CreateId;
                changeset.CreateTime = item.CreateTime;
                changeset.ModifyId = View.UserId;
                changeset.ModifyTime = DateTime.Now;
            }
            else
            {
                CreateBusinessOrder(ref changeset, false, false);
            }

            _salesServ.SaveBusinessOrder(changeset);

            if (isSendMail)
                SendEmail(changeset);

            if (ObjectId.Equals(ObjectId.Empty, View.BusinessOrderObjectId))
            {
                View.ReloadPage(changeset.Id);
            }
            else
            {
                View.SetBusinessOrderDetail(changeset);
            }
        }
        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                BusinessOrder item = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                _salesServ.DeleteBusinessOrder(item);
            }
        }
        protected void OnStoreDeleteClicked(object sender, DataEventArgs<ObjectId> e)
        {
            if (!e.Data.Equals(ObjectId.Empty))
            {
                BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                SalesStore store = businessOrder.Store.Where(x => x.Id.Equals(e.Data)).FirstOrDefault();
                if (store != null)
                {
                    businessOrder.Store.Remove(store);
                    _salesServ.SaveBusinessOrder(businessOrder);
                }
            }
        }
        protected void OnCopyClicked(object sender, DataEventArgs<BusinessCopyType> e)
        {
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                // 複製工單
                BusinessOrder item = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                // 查找檔號
                //DealProperty dealProperty = _pponProv.DealPropertyGet(View.BusinessHourGuid);
                string type = string.Empty;
                BusinessOrderStatus status = BusinessOrderStatus.Temp;
                bool isNoteHistory = false;
                bool isContinuedSequence = false;
                bool isContinuedQuantity = false;
                //Guid AncestorQuantityBusinessHourGuid = Guid.Empty;

                if (!e.Data.Equals(BusinessCopyType.None))
                {
                    // 複製類型
                    switch (e.Data)
                    {
                        case BusinessCopyType.General:
                            status = BusinessOrderStatus.Temp;
                            type = I18N.Phrase.BusinessOrderCopyTypeGeneral;
                            break;
                        case BusinessCopyType.Again:
                            status = BusinessOrderStatus.Temp;
                            type = I18N.Phrase.BusinessOrderCopyTypeAgain;
                            isNoteHistory = true; // 啟用歷史紀錄註記
                            //isContinuedSequence = View.IsContinuedSequence;
                            //複製時，[接續數量BID]則帶入上一複製來源ID位置的BID值，所以複製時一定會先押true，他要改再自己去更新
                            if (item.BusinessType == BusinessType.Ppon)
                            {
                                isContinuedSequence = true;
                            }
                            else {
                                //宅配不會有憑證號碼
                                isContinuedSequence = false;
                            }                            
                            isContinuedQuantity = View.IsContinuedQuantity;
                            break;
                        default:
                            break;
                    }
                    CreateBusinessOrder(ref item, true, isNoteHistory);
                    item.IsContinuedSequence = isContinuedSequence;
                    item.IsContinuedQuantity = isContinuedQuantity;
                    item.Status = status;
                    item.CopyType = e.Data; // 註記關聯類型
                    item.CopyBusinessOrderId = View.BusinessOrderId; // 註記關聯工單編號
                    item.CopyBusinessHourGuid = View.BusinessOrderObjectId; // 註記關聯工單Key值
                    item.HistoryList = new List<History>() { new History() { ModifyId = View.UserId, ModifyTime = DateTime.Now, Changeset = string.Format("{0}工單，來自：{1}", type + "", View.BusinessOrderId) } }; // 歷史紀錄清空，並註記複製資料
                    // 複製分店資訊
                    BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                    if (businessOrder.Store.Count > 0)
                        item.Store = businessOrder.Store;
                    // 清除後台對應資訊
                    item.BusinessHourGuid = Guid.Empty;
                    item.DealId = 0;
                    item.UniqueId = 0;
                    item.IsContractSend = false;
                    item.NoContractReason = string.Empty;
                    item.BusinessHourTimeS = null;
                    //複製時，[接續數量BID]則帶入上一複製來源ID位置的BID值
                    item.AncestorQuantityBusinessHourGuid = View.AncestorSequenceBid;
                    if (item.BusinessType == BusinessType.Ppon) {
                        item.AncestorSequenceBusinessHourGuid = View.BusinessHourGuid;
                    }
                    else
                    {
                        item.AncestorSequenceBusinessHourGuid = Guid.Empty;
                    }                    
                    _salesServ.SaveBusinessOrder(item);
                }
                View.RedirectSalesManagementPage();
            }
        }
        protected void OnImportClicked(object sender, DataEventArgs<List<SalesStore>> e)
        {
            if (!View.BusinessOrderObjectId.Equals(ObjectId.Empty))
            {
                BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
                foreach (SalesStore store in e.Data)
                {
                    businessOrder.Store.Add(store);
                }
                _salesServ.SaveBusinessOrder(businessOrder);
                GetSalesStoreData(businessOrder.Store);
            }
        }
        protected void OnRefreshStoreClicked(object sender, EventArgs e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            if (businessOrder != null)
                GetSalesStoreData(businessOrder.Store);

        }
        protected void OnExportToExcelClicked(object sender, EventArgs e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            if (businessOrder.Store.Count > 0)
                View.ImportStoreList(businessOrder.Store);
        }
        protected void OnDownloadClicked(object sender, EventArgs e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            View.Download(businessOrder);
        }
        protected void OnReferredClicked(object sender, DataEventArgs<string> e)
        {
            BusinessOrder businessOrder = _salesServ.GetBusinessOrder(View.BusinessOrderObjectId);
            ViewEmployee employee = _humanProv.ViewEmployeeCollectionGetByDepartment(EmployeeDept.S000)
                .FirstOrDefault(x => x.Email.Equals(e.Data, StringComparison.OrdinalIgnoreCase));
            if (employee != null)
            {
                businessOrder.CreateId = employee.Email;
                businessOrder.HistoryList.Add(new History() { Id = ObjectId.GenerateNewId(), Changeset = "轉件", ModifyId = View.UserId, ModifyTime = DateTime.Now });
                _salesServ.SaveBusinessOrder(businessOrder);

                // 通知信
                List<string> user = new List<string>() { e.Data };
                string subject = string.Format("【{0}】{1} {2} [{3}] {4} <{5}>", I18N.Phrase.BusinessOrder, businessOrder.SalesDeptName.Replace("業務部", string.Empty), businessOrder.BusinessOrderId, GetBusinessTypeDesc(businessOrder.BusinessType), businessOrder.SellerName, "轉件通知");
                Send(user, businessOrder.SalesName, View.UserId, user, subject, businessOrder.SellerName, businessOrder.HistoryList);

                View.RedirectSalesManagementPage();
            }
            else
                View.ShowMessage(I18N.Message.BusinessOrderNoSalesData);
        }
        #endregion

        #region Private method

        private void SetupDealLabels()
        {
            View.SetDealLabel(_sellerProv.GetDealLabelCollection());
        }

        private void SetupDealTypes(int? DealTypeId = null)
        {
            if (DealTypeId != null)
            {
                SystemCode sc = _sysProv.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), (int)DealTypeId);
                View.SetDealType((int)sc.CodeId, (int)DealTypeId, SystemCodeManager.GetDealType().OrderBy(x => x.Seq).ToList());
            }
            else
            {
                View.SetDealType(0, 0, SystemCodeManager.GetDealType().OrderBy(x => x.Seq).ToList());
            }
        }

        private void SetupSelectableTravelCategories()
        {
            View.SetSelectableTravelCategory(ViewPponDealManager.DefaultManager.TravelCategories);
        }

        private void SetupSelectableCommercialCategories()
        {
            Dictionary<Category, List<Category>> dataList = new Dictionary<Category, List<Category>>();
            CategoryCollection categories = _sellerProv.CategoryGetList((int)CategoryType.CommercialCircle);
            foreach (Category item in categories.Where(x => x.ParentCode == null))
            {
                dataList.Add(item, categories.Where(x => x.ParentCode.Equals(item.Code)).ToList());
            }
            View.SetSelectableCommercialCategory(dataList);
        }

        private void SetSalesMemberData()
        {
            ViewEmployee emp = _humanProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserId);
            if (emp.IsLoaded)
            {
                if (!View.IsSalesAssistant)
                {
                    View.SalesName = emp.EmpName;
                    View.SalesPhone = emp.Mobile;
                }
                View.AuthorizationGroup = emp.DeptId;
            }
            View.SalesDept = emp.DeptId;
        }

        private void GetSalesStoreData(List<SalesStore> storeList)
        {
            if (storeList.Count > 0)
                View.SetStoreList(storeList);
        }
        /// <summary>
        /// 核准工單權限設定
        /// </summary>
        private void AuthorizationGroupSet()
        {
            SalesMember salesMember = _salesServ.GetSalesMember(View.UserId);
            if (salesMember != null)
            {
                // 業務主管
                View.SalesLevel = salesMember.Level;
                if (salesMember.Level.Equals(Level.Manager) || salesMember.Level.Equals(Level.Leader))
                    View.SalesMemberArea = salesMember.Area;
            }
        }
        /// <summary>
        /// 根據傳入參數內容新增一筆工單紀錄
        /// </summary>
        /// <param name="changeset">工單異動項目</param>
        /// <param name="isNoteHistory">是否需註記異動紀錄</param>
        /// <returns></returns>
        private void CreateBusinessOrder(ref BusinessOrder changeset, bool isCopy, bool isNoteHistory)
        {
            changeset = View.GetBusinessOrderData(changeset, isCopy, string.Empty);
            int id = _salesServ.GetSequenceNumber(CounterType.BusinessOrder);
            changeset.SequenceNumber = id;
            ViewEmployee emp = _humanProv.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserId);
            if (emp.IsLoaded)
            {
                changeset.SalesDeptId = emp.DeptId;
            }
            changeset.BusinessOrderId = changeset.SalesDeptId + "-" + string.Format("{0:000000}", id);
            changeset.IsNoteHistory = isNoteHistory;
            changeset.CreateId = View.UserId;
            changeset.CreateTime = DateTime.Now;
            changeset.ModifyId = View.UserId;
            changeset.ModifyTime = DateTime.Now;
        }

        private string GetBusinessTypeDesc(BusinessType type)
        {
            string typeDesc = string.Empty;
            switch (type)
            {
                case BusinessType.Ppon:
                    return I18N.Phrase.BusinessOrderTypePpon;
                case BusinessType.Product:
                    return I18N.Phrase.BusinessOrderTypeProduct;
                case BusinessType.None:
                default:
                    return string.Empty;
            }
        }

        private void SendEmail(BusinessOrder changeset)
        {
            string statusDesc = string.Empty;
            List<string> accessUserList = new List<string>();
            SalesMember salesMember = _salesServ.GetSalesMemberByDept(changeset.AuthorizationGroup);

            #region get status description
            switch (changeset.Status)
            {
                case BusinessOrderStatus.Temp:
                    accessUserList.Add(changeset.CreateId);
                    if (View.UserId.Equals(changeset.CreateId))
                        statusDesc = I18N.Phrase.BusinessOrderRetrieve;
                    else
                        statusDesc = I18N.Phrase.BusinessOrderReturned;
                    break;
                case BusinessOrderStatus.Send:
                    accessUserList.Add((salesMember != null) ? salesMember.SalesId : string.Empty);
                    statusDesc = I18N.Phrase.BusinessOrderSend;
                    break;
                case BusinessOrderStatus.Approve:
                    accessUserList = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.ArrangeDeals);
                    statusDesc = I18N.Phrase.BusinessOrderApprove;
                    break;
                case BusinessOrderStatus.Review:
                    statusDesc = I18N.Phrase.BusinessOrderReview;
                    break;
                case BusinessOrderStatus.Await:
                    statusDesc = I18N.Phrase.BusinessOrderAwait;
                    break;
                case BusinessOrderStatus.Create:
                    statusDesc = I18N.Phrase.BusinessOrderCreate;
                    break;
                default:
                    break;
            }
            #endregion

            Department dept = _humanProv.DepartmentGet(changeset.SalesDeptId);
            string subject = string.Format("【{0}】{1} {2} [{3}] {4} <{5}>", I18N.Phrase.BusinessOrder, dept.DeptName.Replace("業務部", string.Empty), changeset.BusinessOrderId, GetBusinessTypeDesc(changeset.BusinessType), changeset.SellerName, statusDesc);
            // 被通知人
            Send(accessUserList, changeset.SalesName, changeset.CreateId, accessUserList, subject, changeset.SellerName, changeset.HistoryList);
            // 業務
            Send(new List<string>() { changeset.CreateId }, changeset.SalesName, changeset.CreateId, accessUserList, subject, changeset.SellerName, changeset.HistoryList);

            // 核准檢查通知
            if (changeset.Status.Equals(BusinessOrderStatus.Approve))
            {
                #region 若發票類型為免稅/其他，則發信至具免稅通知權限的人員

                string invoiceTitle = string.Empty;
                if (changeset.Invoice.Type == InvoiceType.DuplicateUniformInvoice && changeset.Invoice.InTax == "0")
                {
                    invoiceTitle = "<" + I18N.Phrase.BusinessOrderNoInTax + ">";
                }
                else if (changeset.Invoice.Type == InvoiceType.Other)
                {
                    invoiceTitle = "<" + I18N.Phrase.BusinessOrderOtherTax + ">";
                }

                if (!string.IsNullOrEmpty(invoiceTitle))
                {
                    accessUserList = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.TaxFreeNotice);
                    Send(accessUserList, changeset.SalesName, changeset.CreateId, accessUserList, invoiceTitle + subject, changeset.SellerName, changeset.HistoryList);
                }

                #endregion

                #region 若出帳方式非預設值，則發信至具出帳方式通知權限的人員

                string remittanceTitle = string.Empty;

                if (changeset.BusinessType == BusinessType.Ppon && changeset.IsTravelType)
                {
                    // 好康旅遊(預設 人工每月出帳)
                    if (changeset.RemittancePaidType != RemittanceType.ManualMonthly)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }
                else if (changeset.BusinessType == BusinessType.Ppon && changeset.AccBusinessGroupId == "6")
                {
                    // 品生活好康(預設 人工每週出帳)
                    if (changeset.RemittancePaidType != RemittanceType.ManualWeekly)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }
                else if (changeset.BusinessType == BusinessType.Ppon)
                {
                    // 好康(預設 ACH每週出帳)
                    if (changeset.RemittancePaidType != RemittanceType.AchWeekly)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }
                else if (changeset.BusinessType == BusinessType.Product && changeset.IsTravelType)
                {
                    // 旅遊商品(預設 其他付款方式)
                    if (changeset.RemittancePaidType != RemittanceType.Others)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }
                else if (changeset.BusinessType == BusinessType.Product && changeset.AccBusinessGroupId == "6")
                {
                    // 品生活商品(預設 其他付款方式)
                    if (changeset.RemittancePaidType != RemittanceType.Others)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }
                else if (changeset.BusinessType == BusinessType.Product)
                {
                    // 商品(預設 商品(暫付70%))
                    if (changeset.RemittancePaidType != RemittanceType.ManualPartially)
                    {
                        remittanceTitle = "<" + Helper.GetLocalizedEnum(changeset.RemittancePaidType) + ">";
                    }
                }

                if (!string.IsNullOrEmpty(remittanceTitle))
                {
                    accessUserList = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.RemittanceNotice);
                    Send(accessUserList, changeset.SalesName, changeset.CreateId, accessUserList, remittanceTitle + subject, changeset.SellerName, changeset.HistoryList);
                }

                #endregion
            }
        }

        /// <summary>
        /// 寄通知信
        /// </summary>
        /// <param name="mailToUser">email收件者</param>
        /// <param name="createId">負責業務</param>
        /// <param name="accessUser">被通知人</param>
        /// <param name="typeDesc">工單類型</param>
        /// <param name="sellerName">店家名稱</param>
        /// <param name="statusDesc">狀態</param>
        /// <param name="historyList">歷史紀錄</param>
        private void Send(List<string> mailToUser, string salesName, string createId, List<string> accessUser, string subject, string sellerName, List<History> historyList)
        {
            foreach (string user in mailToUser)
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(_config.SystemEmail);
                msg.To.Add(user);
                msg.Subject = subject;
                string url = _config.SiteUrl + @"/Sal/BusinessOrderDetail.aspx?boid=" + View.BusinessOrderObjectId;
                msg.Body = HttpUtility.HtmlDecode(GetHeaderHtml(sellerName, salesName, createId, View.UserId, string.Join(",", accessUser), url, historyList));
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        private string GetHeaderHtml(string sellerName, string salesName, string createId, string loginUser, string accessUser, string url, List<History> history)
        {
            string html = I18N.Message.BusinessOrderSendEmailHtml;
            return string.Format(html, sellerName, salesName, createId, loginUser, accessUser, url, string.Join("<br />", history.Select(x => string.Format("{0} 人員{1} 異動紀錄：{2}", x.ModifyTime, x.ModifyId, x.Changeset))));
        }

        private void GetAccountingBankInfo()
        {
            View.SetAccountingBankInfo(_ordProv.BankInfoGetMainList());
        }

        private void GetAccountingBranchInfo(string bankNo)
        {
            View.SetAccountingBranchInfo(_ordProv.BankInfoGetBranchList(bankNo));
        }

        private void SetLocationDropDownList()
        {

            var townships = CityManager.TownShipGetListByCityId(CityManager.Citys.Where(x => x.Code != "SYS").FirstOrDefault().Id);
            if (View.AddressCityId != -1)
            {
                var nowCitys = CityManager.Citys.Where(x => x.Id == View.AddressCityId).ToList();
                if (nowCitys.Count > 0)
                {
                    townships = CityManager.TownShipGetListByCityId(nowCitys.First().Id);
                }
            }

            View.GetLocationDropDownList(CityManager.Citys.Where(x => x.Code != "SYS").ToList(), townships);

            var jsonCol = from x in CityManager.Citys.Where(x => x.Code != "SYS")
                          select new
                          {
                              id = x.Id.ToString(),
                              name = x.CityName,
                              Townships = (from y in CityManager.TownShipGetListByCityId(x.Id) select new { id = y.Id.ToString(), name = y.CityName })
                          };
            View.CityJSonData = new JsonSerializer().Serialize(jsonCol);
        }

        private void SetupSellerInfo(string sellerId)
        {
            Seller seller = _sellerProv.SellerGet(Seller.Columns.SellerId, sellerId);
            if (seller.IsLoaded)
                View.SetSellerData(seller);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class EventRedirectionPresenter : Presenter<IEventRedirectionView>
    {
        IEventProvider ep;
        IMemberProvider mp;

        public EventRedirectionPresenter()
        {
            ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            EventContent ec = null;
            string referenceUserName = string.Empty;
            string changeUrl = View.WebRoot + "/event/";

            if (View.EventId != Guid.Empty)
            {
               ec = ep.EventContentGet(View.EventId);
               View.SetPageMeta(ec);
                changeUrl += ec.Url;
            }

            if (!string.IsNullOrEmpty(View.ReferenceId))
            {
                if (View.SetCookie(View.ReferenceId, DateTime.Now.AddHours(72), LkSiteCookie.ReferenceId)) //cookie的值為推薦人MemberId, cookie有效期限預設為72小時
                {
                    //顯示提示訊息，告訴使用者於72小時內購物，將會發放紅利給推薦人
                    string[] referenceId = View.ReferenceId.Split('|');
                    string externalId = referenceId[1];
                    int iSingleSign = int.Parse(referenceId[0]);
                    if (Enum.IsDefined(typeof(SingleSignOnSource), iSingleSign))
                    {
                        referenceUserName = MemberFacade.GetUserNameFromMemberLinkByExternalId(externalId,
                                                                                               (SingleSignOnSource)
                                                                                               iSingleSign);
                        MemberCollection memberCollection = mp.MemberGetList(Member.Columns.UserName,
                                                                             referenceUserName);

                        if (memberCollection.Count > 0)
                        {
                            View.RedirectPage(changeUrl);
                        }
                        else
                        {
                            View.ShowMessage("查無邀請人資料。", changeUrl);
                        }
                    }
                    else
                    {
                        View.ShowMessage("查無邀請人資料。", changeUrl);
                    }
                }
            }
            else
            {
                View.ShowMessage("連結內容有誤，未完成邀請作業，請重新取得連結。", changeUrl);
            }

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }
    }
}

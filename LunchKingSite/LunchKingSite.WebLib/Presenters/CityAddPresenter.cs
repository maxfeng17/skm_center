﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class CityAddPresenter : Presenter<ICityAddView>
    {
        protected ILocationProvider lp;

        public CityAddPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetCityList();
            View.SetCityList(lp.CityGetList(View.SelectCity, City.Columns.Id));
            View.SetUpdatePanel(false);

            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.AddCity += OnAddCity;
            View.UpdateCity += OnUpdateCity;
            View.SearchCity += OnSearchCity;
            View.DeleteCity += OnDeleteCity;
            return true;
        }

        protected void SetupProviders()
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        }

        protected void SetCityList()
        {
            View.SetCityDropDown(CityManager.Citys.Where(x => !x.Code.Equals("sys", StringComparison.OrdinalIgnoreCase)).ToDictionary(x => x.Id, x => x.CityName));
        }

        public void OnAddCity(object sender, DataEventArgs<CityInfo> e)
        {
            City c = e.Data.City;
            c.MarkNew();
            lp.CitySet(c); // we need to save to db to get the identity key back

            if (e.Data.PFile != null)
            {
                c.ImgPath = string.Format("city,{0}.{1}", c.Id, Helper.GetExtensionByContentType(e.Data.PFile.ContentType));
                lp.CitySet(c);
                View.SetImageFile(e.Data.PFile, c.Id.ToString());
            }

            OnSearchCity(sender, e);
        }

        public void OnSearchCity(object sender, EventArgs e)
        {
            if (View.SelectCity.ToString() == "com")
            {
                View.SetCityList(lp.CityGetListTopLevelForCompany(CityStatus.Virtual, -1));
            }
            else
            {
                View.SetCityList(lp.CityGetList(View.SelectCity, City.Columns.Id));
            }
            View.SetUpdatePanel(false);
        }

        public void OnDeleteCity(object sender, DataEventArgs<int> e)
        {
            if (lp.CityDelete(e.Data))
            {
                OnSearchCity(sender, e);
            }
        }

        public void OnUpdateCity(object sender, DataEventArgs<City> e)
        {
            City c = lp.CityGet(e.Data.Id);
            if (c.CityName != e.Data.CityName)
            {
                c.CityName = e.Data.CityName;
            }

            if (c.Code != e.Data.Code)
            {
                c.Code = e.Data.Code;
            }

            if (c.ImgPath != e.Data.ImgPath && e.Data.ImgPath != null)
            {
                c.ImgPath = e.Data.ImgPath;
            }

            if (c.IsDirty)
            {
                lp.CitySet(c);
            }

            View.SetUpdatePanel(false);
            OnSearchCity(sender, e);
        }
    }
}
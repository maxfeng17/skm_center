using System;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponOrderListPresenter : Presenter<IPponOrderListView>
    {
        #region members
        public const int DEFAULT_PAGE_SIZE = 15;

        protected IPponProvider pp = null;
        protected IOrderProvider op = null;
        #endregion

        public PponOrderListPresenter()
        {
            SetupProviders();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            SetStatusFilter();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.GetOrderCount += OnGetOrderCount;
            View.SortClicked += OnGridUpdated;
            View.SearchClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            View.GetCurrentQuantity += OnGetCurrentQuantity;
            return true;
        }

        protected void SetupProviders()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        protected void SetFilterTypes()
        {
            View.SetFilterTypeDropDown(View.FilterTypes);
        }

        protected void SetStatusFilter()
        {
            View.SetStatusFilter(View.StatusFilter);
        }

        protected void OnGetOrderCount(object sender, DataEventArgs<int> e)
        {
            e.Data = pp.ViewPponDealGetCount(GetFilter());
        }

        protected void OnGetCurrentQuantity(object sender, DataEventArgs<Guid> e)
        {
            View.CurrentQuantity = pp.ViewPponCashTrustLogGetList(e.Data).Where(x =>
                (x.CashTrustLogStatus == (int)TrustStatus.Initial
                || x.CashTrustLogStatus == (int)TrustStatus.Trusted 
                || x.CashTrustLogStatus == (int)TrustStatus.Verified)
                && Helper.IsFlagSet(x.OrderStatus, OrderStatus.Complete)
                && !Helper.IsFlagSet(x.CashTrustLogSpecialStatus,TrustSpecialStatus.Freight)
                ).Count();
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            LoadData(1);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        protected void LoadData(int pageNumber)
        {
            View.SetPponList(pp.ViewPponDealGetListPaging(pageNumber, View.PageSize, View.SortExpression, GetFilter()));
        }

        protected string[] GetFilter()
        {
            string[] filter = new string[View.FilterInternal.Length + 1];

            if (!string.IsNullOrEmpty(View.FilterUser))
            {
                filter[0] = View.FilterUser;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = View.FilterType != Coupon.Columns.SequenceNumber ? View.FilterType + " like " + filter[0]
                          : ViewPponDeal.Columns.OrderGuid + " like " + op.OrderDetailGet(pp.CouponGet(Coupon.Columns.SequenceNumber, filter[0]).OrderDetailId).OrderGuid;
            }

            if (View.FilterInternal != null && View.FilterInternal.Length > 0)
                Array.Copy(View.FilterInternal, 0, filter, 1, View.FilterInternal.Length);

            if (View.SelectedStatusFilter.Count > 0)
            {
                int originallength = filter.Length;

                Array.Resize(ref filter,filter.Length + View.SelectedStatusFilter.Count);
                Array.Copy(View.SelectedStatusFilter.ToArray(), 0, filter, originallength, View.SelectedStatusFilter.Count);
            }


            if (View.FilterPiinlife)
            {
                string[] piinFilter = new string[1];
                piinFilter[0] = ViewPponDeal.Columns.CityList + " like %" + PponCityGroup.DefaultPponCityGroup.Piinlife.CityId.ToString() + "%";
                int originallength = filter.Length;

                Array.Resize(ref filter, filter.Length + piinFilter.Length);
                Array.Copy(piinFilter, 0, filter, originallength, piinFilter.Length);
            }

            return filter;
        }
    }
}

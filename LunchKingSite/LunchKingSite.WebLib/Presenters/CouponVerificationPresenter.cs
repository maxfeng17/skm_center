﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace LunchKingSite.WebLib.Presenters
{
    public class CouponVerificationPresenter : Presenter<ICouponVerificationView>
    {
        private IPponProvider pp;
        private IMemberProvider mp;
        private IHiDealProvider hp;

        public CouponVerificationPresenter()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetFilterTypes();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchCoupon += OnSearchCoupon;
            View.SearchHiDealCoupon += OnSearchHiDealCoupon;
            View.ForceRefund += OnForceRefund;
            View.ForceVerify += OnForceVerify;
            View.Verify += OnVerify;
            View.Unverify += OnUnverify;
            View.SearchTmallCoupon += OnSearchTmallCoupon;
            View.SearchCouponByType += OnSearchCouponByType;
            return true;
        }

        public void OnSearchTmallCoupon(object sender, DataEventArgs<string> e)
        {
            ViewPponCashTrustLogCollection viewLog = pp.ViewPponCashTrustLogGetTmall(e.Data);
            SearchCouponProcess(viewLog);
        }

        public void OnSearchHiDealCoupon(object sender, DataEventArgs<string> e)
        {
            Collection<VerifyingCoupon> coupons = new Collection<VerifyingCoupon>();
            string prefix = string.Empty;
            if (e.Data.Length > 1 && RegExRules.StringIsEng(e.Data.Substring(0, 1)))
            {
                prefix = e.Data.Substring(0, 1);
                e.Data = e.Data.Remove(0, 1);
            }
            ViewHiDealCouponTrustLogCollection result = hp.ViewHiDealCouponTrustStatusLogGet(prefix, e.Data, View.GetFilterTypeDropDown());

            //照抄P好康的Search
            foreach (ViewHiDealCouponTrustLog log in result)
            {
                VerifyingCoupon coupon = new VerifyingCoupon
                {
                    OrderClassification = (int)OrderClassification.HiDeal,
                    pid = log.Pid.ToString(),
                    CouponId = log.CouponId ?? 0,
                    CouponSn = log.Prefix + log.Sequence,
                    CouponCode = log.Code,
                    OrderId = log.OrderId,
                    OrderGuid = log.OrderGuid.HasValue ? log.OrderGuid.Value : Guid.Empty,
                    DealNoRefund = log.IsNoRefund,
                    DealExpireNoRefund = log.IsExpireNoRefund,
                    DealVerificationLost = false,
                    DealVerified = false,
                };

                switch ((TrustStatus)log.LogSataus)
                {
                    //未核銷
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        coupon.CouponUseStatus = CouponUsageStatus.Unverified;
                        break;
                    //核銷
                    case TrustStatus.Verified:
                        if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.ForcedVerified;
                        }
                        else if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.LostVerified;
                            coupon.DealVerificationLost = true;
                        }
                        else
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.Verified;
                        }
                        break;
                    //退貨
                    case TrustStatus.Refunded:
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.ForcedRefunded;
                        }
                        else
                        {
                            //為避免payment_transaction中找無trans_type為退貨(ex:折價券折抵購買；退貨時不退回給消費者)
                            //的資料而出現錯誤，讀取退貨時間改讀取cash_trust_satatus_log(同樣狀態下可能有多筆資料；只抓最後一筆)
                            Guid trsutId = log.TrustId.HasValue ? log.TrustId.Value : Guid.Empty;
                            CashTrustStatusLog ctsl = mp.CashTrustStatusLogGetList(trsutId).Where(x => x.Status == log.LogSataus).OrderByDescending(x => x.ModifyTime).FirstOrDefault();
                            if (ctsl != null)
                            {
                                if (DateTime.Compare(ctsl.ModifyTime, log.DealEndTime.Value) < 0)
                                {
                                    coupon.CouponUseStatus = CouponUsageStatus.RefundedBeforeClose;
                                }
                                else
                                {
                                    coupon.CouponUseStatus = CouponUsageStatus.Refunded;
                                }
                            }
                            else
                            {
                                coupon.CouponUseStatus = CouponUsageStatus.Refunded;
                            }
                        }
                        break;
                }

                coupons.Add(coupon);
            }
            View.SetCouponList(coupons);
        }

        //OnSearchCouponByType（查詢） & OnSearchCoupon（強制XX...）
        //2014/4/15 新增需求，憑證查詢可依憑證碼或檢查碼查詢
        //所以新增一個方法來查詢，其餘功能不變，故並存兩個方法

        public void OnSearchCouponByType(object sender, EventArgs e)
        {
            ViewPponCashTrustLogCollection viewLog = null;
            switch (View.GetFilterTypeDropDown())
            {
                case "sequence":
                    viewLog = pp.ViewPponCashTrustLogGet(View.CouponSn);
                    break;
                case "code":
                    viewLog = pp.ViewPponCashTrustLogGetByCode(View.CouponSn);
                    break;
                default:
                    break;

            }

            SearchCouponProcess(viewLog);
        }

        public void OnSearchCoupon(object sender, DataEventArgs<string> e)
        {
            ViewPponCashTrustLogCollection viewLog = pp.ViewPponCashTrustLogGet(e.Data);
            SearchCouponProcess(viewLog);
        }

        private void SearchCouponProcess(ViewPponCashTrustLogCollection trustlogs)
        {
            Collection<VerifyingCoupon> coupons = new Collection<VerifyingCoupon>();

            ViewPponCashTrustLogCollection viewLog = trustlogs;
            foreach (ViewPponCashTrustLog log in viewLog)
            {
                VerifyingCoupon coupon = new VerifyingCoupon
                {
                    OrderClassification = (int)OrderClassification.LkSite,
                    CouponId = log.CouponId ?? 0,
                    CouponSn = log.CouponSequenceNumber,
                    CouponCode = log.CouponCode,
                    OrderId = log.OrderId,
                    OrderGuid = log.Oid,
                    DealNoRefund = false,
                    DealExpireNoRefund = false,
                    DealVerificationLost = false,
                    DealVerified = false,
                    pid = log.UniqueId.ToString(),
                    IsRefundCashOnly = PaymentFacade.IsRefundCashOnly(log)
                };

                if ((log.DealAccountingFlag & ((int)AccountingFlag.VerificationClosed | (int)AccountingFlag.VerificationFinished)) > 0)
                {
                    coupon.DealVerified = true;
                }

                switch ((TrustStatus)log.CashTrustLogStatus)
                {
                    //未核銷
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        coupon.CouponUseStatus = CouponUsageStatus.Unverified;
                        if (log.GroupOrderStatus.HasValue)
                        {
                            coupon.DealNoRefund = ((GroupOrderStatus)log.GroupOrderStatus.Value).HasFlag(GroupOrderStatus.NoRefund);
                            coupon.DealExpireNoRefund = ((GroupOrderStatus)log.GroupOrderStatus.Value).HasFlag(GroupOrderStatus.ExpireNoRefund);
                        }
                        break;
                    //核銷
                    case TrustStatus.Verified:
                        if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.ForcedVerified;
                        }
                        else if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.LostVerified;
                            coupon.DealVerificationLost = true;
                        }
                        else
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.Verified;
                        }
                        break;
                    //退貨
                    case TrustStatus.Refunded:
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet((TrustSpecialStatus)log.CashTrustLogSpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            coupon.CouponUseStatus = CouponUsageStatus.ForcedRefunded;
                        }
                        else
                        {
                            //為避免payment_transaction中找無trans_type為退貨(ex:折價券折抵購買；退貨時不退回給消費者)
                            //的資料而出現錯誤，讀取退貨時間改讀取cash_trust_satatus_log(同樣狀態下可能有多筆資料；只抓最後一筆)
                            CashTrustStatusLog ctsl = mp.CashTrustStatusLogGetList(log.TrustId).Where(x => x.Status == log.CashTrustLogStatus).OrderByDescending(x => x.ModifyTime).FirstOrDefault();
                            if (ctsl != null)
                            {
                                if (DateTime.Compare(ctsl.ModifyTime, log.OrderTimeE) < 0)
                                {
                                    coupon.CouponUseStatus = CouponUsageStatus.RefundedBeforeClose;
                                }
                                else
                                {
                                    coupon.CouponUseStatus = CouponUsageStatus.Refunded;
                                }
                            }
                            else
                            {
                                coupon.CouponUseStatus = CouponUsageStatus.Refunded;
                            }
                        }
                        break;
                }

                coupons.Add(coupon);
            }

            View.SetCouponList(coupons);
        }

        public void OnForceRefund(object sender, DataEventArgs<KeyValuePair<int, bool>> e)
        {
            var refundReason = "強制退貨";
            int? returnFormId = null;
            CreateReturnFormResult result;
            var ctlog = mp.CashTrustLogGetByCouponId(e.Data.Key);
            CashTrustLogCollection newctlc = new CashTrustLogCollection();

            if (!ctlog.IsLoaded)
            {
                result = CreateReturnFormResult.ProductsUnreturnable;
            }
            else
            {
                //成套票券憑證強制退貨得另做判斷
                //if (Helper.IsFlagSet(ctlog.SpecialStatus, (int)TrustSpecialStatus.Giveaway) || Helper.IsFlagSet(ctlog.SpecialStatus, (int)TrustSpecialStatus.SpecialDiscount))
                //{
                //    OrderFacade.ChangeCtlCouponNumber(ctlog, (int)TrustStatus.Returned, true);
                //}
                result = ReturnService.CreateCouponReturnForm(ctlog.OrderGuid, new List<int> { e.Data.Key }, e.Data.Value, View.UserName, refundReason, out returnFormId, false);
            }

            if (result == CreateReturnFormResult.Created)
            {
                var auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.Value, refundReason);
                CommonFacade.AddAudit(ctlog.OrderGuid, AuditType.Refund, auditMessage, View.UserName, false);

                //if (newctlc.Count > 0)
                //    mp.CashTrustLogCollectionSet(newctlc);

                #region 立即退貨 & 強制退貨

                ReturnFormEntity returnEntity = ReturnFormRepository.FindById(returnFormId.Value);

                ReturnService.Refund(returnEntity, View.UserName, true, false, true);

                #endregion 立即退貨 & 強制退貨

                OrderFacade.VerificationSatisticsLogSetRightNow(ctlog.BusinessHourGuid.Value, View.UserName, VerificationStatisticsLogStatus.ForcedReturned);
            }
            else
            {
                var auditMessage = "#" + ctlog.CouponSequenceNumber + " 建立退貨單失敗，原因:" + GetCreateReturnFormFailMessage(result);
                CommonFacade.AddAudit(ctlog.OrderGuid, AuditType.Refund, auditMessage, View.UserName, false);
            }

            OnSearchCoupon(null, new DataEventArgs<string>(ctlog.CouponSequenceNumber));
            View.SetProcessResultMessage("請至訂單明細確認。");
        }
        
        public void OnForceVerify(object sender, DataEventArgs<int> e)
        {
            if (View.orderClassification == (int)OrderClassification.LkSite)
            {
                ViewPponCoupon vpc = pp.ViewPponCouponGet(e.Data);
                //if (Helper.IsFlagSet(vpc.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon))
                //{
                //    CashTrustLog ctlog = mp.CashTrustLogGetByCouponId(e.Data);
                //    OrderFacade.ChangeCtlCouponNumber(ctlog, (int)TrustStatus.Verified, true);
                //}
                OrderFacade.VerifyCoupon(vpc.CouponId.Value, vpc.CouponCode, vpc.SellerGuid, View.UserName, null, true, View.UserName, (OrderClassification)View.orderClassification);
                OrderFacade.VerificationSatisticsLogSetRightNow(vpc.BusinessHourGuid, View.UserName, VerificationStatisticsLogStatus.ForcedVerified);
            }
            else
            {
                ViewHiDealCoupon hdc = hp.ViewHiDealCouponGet(e.Data);
                OrderFacade.VerifyCoupon(Convert.ToInt32(hdc.CouponId.Value), hdc.Code, hdc.SellerGuid, View.UserName, null, true, View.UserName, (OrderClassification)View.orderClassification);
            }

            ////更新畫面
            //if (View.CouponSn != null)
            //{
            //    if (View.orderClassification == (int)OrderClassification.LkSite)
            //    {
            //        OnSearchCoupon(null, new DataEventArgs<string>(View.CouponSn));
            //    }
            //    else
            //    {
            //        OnSearchHiDealCoupon(null, new DataEventArgs<string>(View.CouponSn));
            //    }
            //}
            View.SetProcessResultMessage("請至訂單明細確認。");
        }

        public void OnVerify(object sender, DataEventArgs<int> e)
        {
            if (View.orderClassification == (int)OrderClassification.LkSite)
            {
                ViewPponCoupon vpc = pp.ViewPponCouponGet(e.Data);
                if (vpc.CouponStatus != (int)CouponStatus.Locked)
                {
                    OrderFacade.VerifyCoupon(vpc.CouponId.Value, vpc.SellerGuid, View.UserName, "", View.UserName, OrderClassification.LkSite);
                }
            }
            else
            {
                ViewHiDealCoupon hdc = hp.ViewHiDealCouponGet(e.Data);
                OrderFacade.VerifyCoupon(Convert.ToInt32(hdc.CouponId.Value), hdc.SellerGuid, View.UserName, "", View.UserName, OrderClassification.HiDeal);
            }

            ////更新畫面
            //if (View.CouponSn != null)
            //{
            //    if (View.orderClassification == (int)OrderClassification.LkSite)
            //    {
            //        OnSearchCoupon(null, new DataEventArgs<string>(View.CouponSn));
            //    }
            //    else
            //    {
            //        OnSearchHiDealCoupon(null, new DataEventArgs<string>(View.CouponSn));
            //    }
            //}
            View.SetProcessResultMessage("請至訂單明細確認。");
        }

        public void OnUnverify(object sender, DataEventArgs<int> e)
        {
            CashTrustLog ctLog = mp.CashTrustLogGetByCouponId(e.Data, (OrderClassification)View.orderClassification);
            if (ctLog == null || ctLog.IsNew)
            {
                View.SetProcessResultMessage("查無資料。請洽技術部！");
                return;
            }

            if (!OrderFacade.UndoVerifiedStatus(ctLog.TrustId, View.UserName, (OrderClassification)View.orderClassification))
            {
                View.SetProcessResultMessage("此憑證不符合取消核銷的條件。請洽技術部！");
                return;
            }

            if (View.CouponSn != null)
            {
                if (View.orderClassification == (int)OrderClassification.LkSite)
                {
                    OnSearchCoupon(null, new DataEventArgs<string>(View.CouponSn));
                }
                else
                {
                    OnSearchHiDealCoupon(null, new DataEventArgs<string>(View.CouponSn));
                }
            }
            View.SetProcessResultMessage("請至訂單明細確認。");
        }

        protected void SetFilterTypes()
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("sequence", "依憑證編號");
            data.Add("code", "依確認碼");
            View.SetFilterTypeDropDown(data);
        }

        private string GetCreateReturnFormFailMessage(CreateReturnFormResult result)
        {
            string errMsg;
            switch (result)
            {
                case CreateReturnFormResult.ProductsUnreturnable:
                    errMsg = "欲退項目的狀態無法退貨";
                    break;
                case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                    errMsg = "ATM訂單, 請先設定ATM退款帳號";
                    break;
                case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                    errMsg = "舊訂單, 請先設定ATM退款帳號";
                    break;
                case CreateReturnFormResult.InvalidArguments:
                    errMsg = "無法建立退貨單, 請洽技術部";
                    break;
                case CreateReturnFormResult.ProductsIsReturning:
                    errMsg = "仍有退款中之退貨單";
                    break;
                case CreateReturnFormResult.OrderNotCreate:
                    errMsg = "訂單未完成付款, 無法建立退貨單";
                    break;
                default:
                    errMsg = "不明錯誤, 請洽技術部";
                    break;
            }

            return errMsg;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Model;
using MongoDB.Bson;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class ProvisionDescSetPresenter : Presenter<IProvisionDescSetView>
    {
        private ProvisionService _provServ;
        private IPponProvider pp;
        private ISysConfProvider config;

        public ProvisionDescSetPresenter(ProvisionService provServ)
        {
            _provServ = provServ;
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetProvisionData();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ProvisionSelectedIndexChanged += OnProvisionSelectedIndexChanged;
            View.SaveClicked += OnSaveClicked;
            View.MongoSyncSqlClicked += OnMongoSyncSqlClicked;
            View.SortClicked += OnSortClicked;
            View.SaveSortClicked += OnSaveSortClicked;
            View.DeleteClicked += OnDeleteClicked;
            return true;
        }

        #region enum
        private enum SaveMode
        {
            Update,
            Delete,
        }
        #endregion

        #region event
        protected void OnProvisionSelectedIndexChanged(object sender, EventArgs e)
        {
            SetProvisionData();
        }

        protected void OnSaveClicked(object sender, EventArgs e)
        {
            if (config.IsProvisionSwitchToSql)
            {
                ProvisionDescriptionCollection pds = pp.ProvisionDescriptionGetByRootId(View.DepartmentId.ToString());

                ProvisionDescription pd = pds.Where(x => x.Type == (int)View.Type && x.Id == View.Id.ToString()).FirstOrDefault();
                if (pd == null)
                {
                    pd = new ProvisionDescription();
                    pd.Id = ObjectId.GenerateNewId().ToString();
                    pd.RootId = View.DepartmentId.ToString();
                    pd.Type = (int)View.Type;                    
                }
                switch (View.Type)
                {
                    case ProvisionType.Category:
                        pd.ParentId = View.DepartmentId.ToString();
                        break;
                    case ProvisionType.Item:
                        pd.ParentId = View.CategoryId.ToString();
                        break;
                    case ProvisionType.Content:
                        pd.ParentId = View.ItemId.ToString();
                        break;
                    case ProvisionType.Description:
                        pd.ParentId = View.ContentId.ToString();
                        pd.Status = (int)View.Status;
                        break;
                    default:
                        break;
                }

                pd.Name = View.EditText;
                pd.ModifyTime = DateTime.Now;
                pd.ModifyUser = View.UserName;
                pp.ProvisionDescriptionSet(pd);
            }
            else
            {
                ProvisionDepartment department;
                ProvisionCategory category;
                ProvisionItem item;
                ProvisionContent content;
                ProvisionDesc desc;
                GetProvisionData(View.Type, View.Id, out department, out category, out item, out content, out desc);

                switch (View.Type)
                {
                    case ProvisionType.Department:
                        if (department == null)
                        {
                            department = new ProvisionDepartment();
                            department.Name = View.EditText;
                        }
                        else
                            department.Name = View.EditText;
                        break;
                    case ProvisionType.Category:
                        if (category == null || category.Id.Equals(ObjectId.Empty))
                        {
                            category = new ProvisionCategory();
                            category.Id = ObjectId.GenerateNewId();
                            category.Name = View.EditText;
                            department.Categorys.Add(category);
                        }
                        else
                            category.Name = View.EditText;
                        break;
                    case ProvisionType.Item:
                        if (item == null || item.Id.Equals(ObjectId.Empty))
                        {
                            item = new ProvisionItem();
                            item.Id = ObjectId.GenerateNewId();
                            item.Name = View.EditText;
                            category.Items.Add(item);
                        }
                        else
                            item.Name = View.EditText;
                        break;
                    case ProvisionType.Content:
                        if (content == null || content.Id.Equals(ObjectId.Empty))
                        {
                            content = new ProvisionContent();
                            content.Id = ObjectId.GenerateNewId();
                            content.Name = View.EditText;
                            item.Contents.Add(content);
                        }
                        else
                            content.Name = View.EditText;
                        break;
                    case ProvisionType.Description:
                        if (desc == null || desc.Id.Equals(ObjectId.Empty))
                        {
                            desc = new ProvisionDesc();
                            desc.Id = ObjectId.GenerateNewId();
                            desc.Description = View.EditText;
                            desc.Status = View.Status;
                            content.ProvisionDescs.Add(desc);
                        }
                        else
                            desc.Status = View.Status;
                        desc.Description = View.EditText;
                        break;
                    case ProvisionType.None:
                    default:
                        break;
                }
                _provServ.SaveProvisionDepartment(department);
            }
            
            SetProvisionData();
        }

        protected void OnMongoSyncSqlClicked(object sender, EventArgs e)
        {
            IEnumerable<ProvisionDepartment> departments = _provServ.GetProvisionDepartments();

            foreach (var department in departments)
            {
                ProvisionDescriptionCollection pdcs = pp.ProvisionDescriptionGetByRootId(department.Id.ToString());
                //Department
                var dept = pdcs.Where(x => x.Id == department.Id.ToString()).FirstOrDefault();
                if (dept == null)
                {
                    dept = new ProvisionDescription();
                    dept.Id = department.Id.ToString();
                }
                dept.Name = department.Name.ToString();
                dept.Type = (int)ProvisionType.Department;
                dept.ModifyTime = DateTime.Now;
                dept.ModifyUser = View.UserName;
                dept.Rank = default(int);
                dept.RootId = department.Id.ToString();
                pp.ProvisionDescriptionSet(dept);

                //Category
                foreach (ProvisionCategory oriCategory in department.Categorys)
                {
                    var category = pdcs.Where(x => x.Id == oriCategory.Id.ToString() && x.ParentId == dept.Id).FirstOrDefault();
                    if (category == null)
                    {
                        category = new ProvisionDescription();
                        category.Id = oriCategory.Id.ToString();
                    }
                    category.Name = oriCategory.Name.ToString();
                    category.Type = (int)ProvisionType.Category;
                    category.ParentId = department.Id.ToString();
                    category.ModifyTime = DateTime.Now;
                    category.ModifyUser = View.UserName;
                    category.Rank = oriCategory.Rank;
                    category.RootId = department.Id.ToString();
                    pp.ProvisionDescriptionSet(category);

                    //Item,
                    foreach (ProvisionItem oriItem in oriCategory.Items)
                    {
                        var item = pdcs.Where(x => x.Id == oriItem.Id.ToString() && x.ParentId == oriCategory.Id.ToString()).FirstOrDefault();

                        if (item == null)
                        {
                            item = new ProvisionDescription();
                            item.Id = oriItem.Id.ToString();
                        }
                        item.Name = oriItem.Name.ToString();
                        item.Type = (int)ProvisionType.Item;
                        item.ParentId = oriCategory.Id.ToString();
                        item.ModifyTime = DateTime.Now;
                        item.ModifyUser = View.UserName;
                        item.Rank = oriItem.Rank;
                        item.RootId = department.Id.ToString();
                        pp.ProvisionDescriptionSet(item);

                        //Content
                        foreach (ProvisionContent oriContent in oriItem.Contents)
                        {
                            var content = pdcs.Where(x => x.Id == oriContent.Id.ToString() && x.ParentId == oriItem.Id.ToString()).FirstOrDefault();

                            if (content == null)
                            {
                                content = new ProvisionDescription();
                                content.Id = oriContent.Id.ToString();
                            }
                            content.Name = oriContent.Name.ToString();
                            content.Type = (int)ProvisionType.Content;
                            content.ParentId = oriItem.Id.ToString();
                            content.ModifyTime = DateTime.Now;
                            content.ModifyUser = View.UserName;
                            content.Rank = oriContent.Rank;
                            content.RootId = department.Id.ToString();
                            pp.ProvisionDescriptionSet(content);

                            //Description
                            foreach (ProvisionDesc oriDesc in oriContent.ProvisionDescs)
                            {
                                var desc = pdcs.Where(x => x.Id == oriDesc.Id.ToString() && x.ParentId == oriContent.Id.ToString()).FirstOrDefault();

                                if (desc == null)
                                {
                                    desc = new ProvisionDescription();
                                    desc.Id = oriDesc.Id.ToString();
                                }
                                desc.Name = oriDesc.Description.ToString();
                                desc.Type = (int)ProvisionType.Description;
                                desc.ParentId = oriContent.Id.ToString();
                                desc.Status = (int)oriDesc.Status;
                                desc.ModifyTime = DateTime.Now;
                                desc.ModifyUser = View.UserName;
                                desc.Rank = default(int);
                                desc.RootId = department.Id.ToString();
                                pp.ProvisionDescriptionSet(desc);
                            }
                        }
                    }
                }
            }
        }
        protected void OnSortClicked(object sender, EventArgs e)
        {
            Dictionary<ObjectId, string[]> dataList = new Dictionary<ObjectId, string[]>();
            if (config.IsProvisionSwitchToSql)
            {
                ProvisionDescriptionCollection pds = pp.ProvisionDescriptionGetByRootId(View.DepartmentId.ToString());
                switch (View.Type)
                {
                    case ProvisionType.Category:
                        dataList = pds.Where(x => x.Type == (int)ProvisionType.Category && x.ParentId == View.DepartmentId.ToString()).ToDictionary(x => ObjectId.Parse(x.Id), x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Item:
                        dataList = pds.Where(x => x.Type == (int)ProvisionType.Item && x.ParentId == View.CategoryId.ToString()).ToDictionary(x => ObjectId.Parse(x.Id), x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Content:
                        dataList = pds.Where(x => x.Type == (int)ProvisionType.Content && x.ParentId == View.ItemId.ToString()).ToDictionary(x => ObjectId.Parse(x.Id), x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Description:
                    case ProvisionType.Department:
                    case ProvisionType.None:
                    default:
                        break;
                }
            }
            else
            {
                ProvisionDepartment department;
                ProvisionCategory category;
                ProvisionItem item;
                ProvisionContent content;
                ProvisionDesc desc;
                GetProvisionData(View.Type, View.Id, out department, out category, out item, out content, out desc);
                
                switch (View.Type)
                {
                    case ProvisionType.Category:
                        dataList = department.Categorys.ToDictionary(x => x.Id, x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Item:
                        dataList = category.Items.ToDictionary(x => x.Id, x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Content:
                        dataList = item.Contents.ToDictionary(x => x.Id, x => new string[] { x.Rank.ToString(), x.Name });
                        break;
                    case ProvisionType.Description:
                    case ProvisionType.Department:
                    case ProvisionType.None:
                    default:
                        break;
                }
            }
            
            View.SetProvisionSortRank(dataList);
        }

        protected void OnSaveSortClicked(object sender, DataEventArgs<Dictionary<ObjectId, int>> e)
        {
            if (config.IsProvisionSwitchToSql)
            {
                ProvisionDescriptionCollection pds = pp.ProvisionDescriptionGetByRootId(View.DepartmentId.ToString());

                foreach (KeyValuePair<ObjectId, int> dataItem in e.Data)
                {
                    ProvisionDescription data = pds.Where(x => x.Id.Equals(dataItem.Key.ToString())).FirstOrDefault();
                    if(data != null)
                    {
                        data.Rank = dataItem.Value;
                        pp.ProvisionDescriptionSet(data);
                    }
                }
            }
            else
            {
                ProvisionDepartment department;
                ProvisionCategory category;
                ProvisionItem item;
                ProvisionContent content;
                ProvisionDesc desc;
                GetProvisionData(View.Type, View.Id, out department, out category, out item, out content, out desc);

                switch (View.Type)
                {
                    case ProvisionType.Category:
                        foreach (KeyValuePair<ObjectId, int> dataItem in e.Data)
                        {
                            category = department.Categorys.Where(x => x.Id.Equals(dataItem.Key)).FirstOrDefault();
                            category.Rank = dataItem.Value;
                        }
                        break;
                    case ProvisionType.Item:
                        foreach (KeyValuePair<ObjectId, int> dataItem in e.Data)
                        {
                            item = category.Items.Where(x => x.Id.Equals(dataItem.Key)).FirstOrDefault();
                            item.Rank = dataItem.Value;
                        }
                        break;
                    case ProvisionType.Content:
                        foreach (KeyValuePair<ObjectId, int> dataItem in e.Data)
                        {
                            content = item.Contents.Where(x => x.Id.Equals(dataItem.Key)).FirstOrDefault();
                            content.Rank = dataItem.Value;
                        }
                        break;
                    case ProvisionType.Description:
                    case ProvisionType.Department:
                    case ProvisionType.None:
                    default:
                        break;
                }
                _provServ.SaveProvisionDepartment(department);
            }
            
            SetProvisionData();
        }

        protected void OnDeleteClicked(object sender, EventArgs e)
        {
            if (config.IsProvisionSwitchToSql)
            {
                ProvisionDescriptionCollection pds = pp.ProvisionDescriptionGetByRootId(View.DepartmentId.ToString());

                ProvisionDescription pd = pds.Where(x => x.Type == (int)View.Type && x.Id == View.Id.ToString()).FirstOrDefault();
                if (pd != null)
                {
                    pp.ProvisionDescriptionDelete(pd);
                }
            }
            else
            {
                ProvisionDepartment department;
                ProvisionCategory category;
                ProvisionItem item;
                ProvisionContent content;
                ProvisionDesc desc;
                GetProvisionData(View.Type, View.Id, out department, out category, out item, out content, out desc);

                switch (View.Type)
                {
                    case ProvisionType.Department:
                        _provServ.DeleteProvisionDepartment(department);
                        break;
                    case ProvisionType.Category:
                        department.Categorys.Remove(category);
                        _provServ.SaveProvisionDepartment(department);
                        break;
                    case ProvisionType.Item:
                        category.Items.Remove(item);
                        _provServ.SaveProvisionDepartment(department);
                        break;
                    case ProvisionType.Content:
                        item.Contents.Remove(content);
                        _provServ.SaveProvisionDepartment(department);
                        break;
                    case ProvisionType.Description:
                        content.ProvisionDescs.Remove(desc);
                        _provServ.SaveProvisionDepartment(department);
                        break;
                    case ProvisionType.None:
                    default:
                        break;
                }
            }
            SetProvisionData();
        }
        #endregion

        #region Private method
        private void SetProvisionData()
        {           
            if (config.IsProvisionSwitchToSql)
            {
                View.SetMsProvisionDropDownListData(PponFacade.GetAllProvisionDepartmentModel());
            }
            else
            {
                View.SetProvisionDropDownListData(_provServ.GetProvisionDepartments());
            }
        }

        

        private void GetProvisionData(ProvisionType type, ObjectId id, out ProvisionDepartment department, out ProvisionCategory category, out ProvisionItem item, out ProvisionContent content, out ProvisionDesc desc)
        {
            department = new ProvisionDepartment();
            category = new ProvisionCategory();
            item = new ProvisionItem();
            content = new ProvisionContent();
            desc = new ProvisionDesc();

            department = _provServ.GetProvisionDepartment(type.Equals(ProvisionType.Department) ? id : View.DepartmentId);
            if (department != null)
            {
                category = department.Categorys.Where(x => x.Id.Equals(type.Equals(ProvisionType.Category) ? id : View.CategoryId)).FirstOrDefault();
                if (category != null)
                {
                    item = category.Items.Where(x => x.Id.Equals(type.Equals(ProvisionType.Item) ? id : View.ItemId)).FirstOrDefault();
                    if (item != null)
                    {
                        content = item.Contents.Where(x => x.Id.Equals(type.Equals(ProvisionType.Content) ? id : View.ContentId)).FirstOrDefault();
                        if (content != null)
                            desc = content.ProvisionDescs.Where(x => x.Id.Equals(type.Equals(ProvisionType.Description) ? id : View.DescId)).FirstOrDefault();
                    }
                }
            }
        }
        #endregion
    }
}

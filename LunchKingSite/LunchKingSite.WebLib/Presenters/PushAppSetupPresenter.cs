﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Presenters
{
    public class PushAppSetupPresenter : Presenter<IPushAppSetupView>
    {
        private INotificationProvider _notifyProv;
        private IPponProvider _pponProv;
        private ILocationProvider _locProv;
        private IEventProvider _eventProv;
        private ISellerProvider _selProv;
        private IMemberProvider _mp;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            GetPushAppList(1);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.OnSearchClicked += OnSearchClicked;
            View.OnSaveClicked += OnSaveClicked;
            View.GetPushAppData += OnGetPushAppData;
            View.Push += OnPush;
            View.PushAppDelete += OnPushAppDelete;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }


        public PushAppSetupPresenter(INotificationProvider notifyProv, IPponProvider pponProv, ILocationProvider locProv, IEventProvider eventProv, ISellerProvider selProv)
        {
            _notifyProv = notifyProv;
            _pponProv = pponProv;
            _locProv = locProv;
            _eventProv = eventProv;
            _selProv = selProv;
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        #region event
        protected void OnSearchClicked(object sender, EventArgs e)
        {
            GetPushAppList(1);
        }
        protected void OnSaveClicked(object sender, EventArgs e)
        {
            string errorMsg = CheckData();
            if (string.IsNullOrWhiteSpace(errorMsg))
            {
                PushApp push = _notifyProv.GetPushApp(View.PushId);
                push.PushArea = View.PushArea;
                push.PushType = (int)View.PushType;
                push.PushDate = View.SendDate;
                push.BusinessHourGuid = View.BusinessHourGuid;
                push.CityId = View.PponCity;
                push.SellerId = View.SellerId;
                push.VourcherId = View.VourcherId;
                push.EventPromoId = View.EventPromoId;
                push.BrandPromoId = View.BrandPromoId;
                push.CustomUrl = View.CustomUrl;
                push.Description = View.Description;
                _notifyProv.PushAppSet(push);

                var actionEventPushMsgId = GenerateActionEventPushMessage(push);
                if (actionEventPushMsgId > 0)
                {
                    push.ActionEventPushMessageId = actionEventPushMsgId;
                    _notifyProv.PushAppSet(push);
                }

                GetPushAppList(1);
                View.ShowPushList();
            }
            else
            {
                View.ShowMessage(errorMsg);
            }
        }
        protected void GetPushAppList(int pageNumber)
        {
            var pushApps = _notifyProv.GetPushAppList(pageNumber, View.PageSize, PushApp.Columns.PushDate + " desc ", GetFilter());
            DateTime now = DateTime.Now;
            foreach (var pushApp in pushApps)
            {
                if (pushApp.CompleteTime == null)
                {
                    continue;
                }
                //資料超過2個小時沒更新，即更新
                //if (pushApp.ModifyTime != null && (now - pushApp.ModifyTime.Value).TotalHours < 2)
                //{
                //    continue;
                //}
                int orderCount;
                int turnover;
                _locProv.TryGetDevicePushOrderInfo(pushApp.Id, out orderCount, out turnover);
                pushApp.OrderCount = orderCount;
                pushApp.Turnover = turnover;
                pushApp.ModifyTime = now;
                _notifyProv.PushAppSet(pushApp);
            }
            View.SetPushAppList(pushApps);
        }
        protected void OnGetPushAppData(object sender, DataEventArgs<int> e)
        {
            PushApp push = _notifyProv.GetPushApp(e.Data);
            View.PushId = push.Id;
            View.SendDate = push.PushDate;
            View.PushArea = push.PushArea;
            View.PushType = (PushAppType)push.PushType;
            if (push.BusinessHourGuid != null)
            {
                View.BusinessHourGuid = push.BusinessHourGuid.Value;
            }
            else if (push.CityId != null)
            {
                View.PponCity = push.CityId.Value;
            }
            else if (push.VourcherId != null)
            {
                View.VourcherId = push.VourcherId.Value;
            }
            else if (!string.IsNullOrEmpty(push.SellerId))
            {
                View.SellerId = push.SellerId;
            }
            else if (push.PushType == (int)PushAppType.EventPromo && push.EventPromoId.HasValue)
            {
                View.EventPromoId = push.EventPromoId;
            }
            else if (push.PushType == (int)PushAppType.BrandPromo && push.BrandPromoId.HasValue)
            {
                View.BrandPromoId = push.BrandPromoId;
            }
            else if (!string.IsNullOrEmpty(push.CustomUrl))
            {
                View.CustomUrl = push.CustomUrl;
            }


            if (!string.IsNullOrEmpty(push.Description))
            {
                View.Description = push.Description;
            }
            View.IsEnabledEdit = push.RealPushTime == null;
            View.IsEnabledUpdate = !View.IsEnabledEdit;
        }
        protected void OnPush(object sender, DataEventArgs<int> e)
        {
            NotificationFacade.PushAppSchedulMessage(e.Data, false);
            GetPushAppList(1);
        } 
        protected void OnPushAppDelete(object sender, DataEventArgs<int> e)
        {
            _notifyProv.DeletePushApp(e.Data);
            GetPushAppList(1);
        }
        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = _notifyProv.GetPushAppListCount(GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            GetPushAppList(e.Data);
        }
        #endregion

        #region method
        #endregion

        #region Private Method
        private string[] GetFilter()
        {
            List<string> list = new List<string>();
            if (!View.SendDateSearchS.Equals(DateTime.MinValue))
                list.Add(PushApp.Columns.PushDate + ">=" + View.SendDateSearchS.ToString("yyyy/MM/dd"));
            if (!View.SendDateSearchE.Equals(DateTime.MinValue))
                list.Add(PushApp.Columns.PushDate + "<=" + View.SendDateSearchE.ToString("yyyy/MM/dd"));
            if (!string.IsNullOrEmpty(View.DescriptionSearch))
                list.Add(PushApp.Columns.Description + " like " + "%" + View.DescriptionSearch + "%");
            if (!View.PushTypeSearch.Equals(-1))
                list.Add(PushApp.Columns.PushType + "=" + View.PushTypeSearch);
            return list.ToArray();
        }

        private string CheckData()
        {
            string msg = string.Empty;
            if (View.BusinessHourGuid != null)
            {
                if (!Guid.Equals(View.BusinessHourGuid, Guid.Empty))
                {
                    BusinessHour businessHour = _pponProv.BusinessHourGet(View.BusinessHourGuid.Value);
                    if (businessHour == null || !businessHour.IsLoaded)
                        msg = I18N.Message.PushAppErrorBidMsg;
                }
                else
                    msg = I18N.Message.PushAppErrorBidMsg;
            }
            else if (View.PponCity != null)
            {
                City city = _locProv.CityGet(View.PponCity.Value);
                if (city == null || !city.IsLoaded)
                    msg = I18N.Message.PushAppErrorCityIdMsg;
            }
            else if (View.VourcherId != null)
            {
                VourcherEvent vourcher = _eventProv.VourcherEventGetById(View.VourcherId.Value);
                if (vourcher == null || !vourcher.IsLoaded)
                    msg = I18N.Message.PushAppErrorVourcherIdMsg;
            }
            else if (!string.IsNullOrWhiteSpace(View.SellerId))
            {
                Seller seller = _selProv.SellerGet(Seller.Columns.SellerId, View.SellerId);
                if (seller == null || !seller.IsLoaded)
                    msg = I18N.Message.PushAppErrorSellerIdMsg;
            }
            else if (View.EventPromoId.HasValue)
            {
                EventPromo eventPromo = _pponProv.GetEventPromo(View.EventPromoId.Value);
                if (eventPromo == null || !eventPromo.IsLoaded)
                {
                    msg = I18N.Message.PushAppErrorEventPromoIdMsg;
                }
                else if (!eventPromo.ShowInApp)
                {
                    msg = I18N.Message.PushAppErrorEventPromoNoShowMsg;
                }
            }
            else if (View.BrandPromoId.HasValue)
            {
                Brand brand = _pponProv.GetBrand(View.BrandPromoId.Value);
                if (brand == null || !brand.IsLoaded)
                {
                    msg = I18N.Message.PushAppErrorBrandPromoIdMsg;
                }
                else if (!brand.ShowInApp)
                {
                    msg = I18N.Message.PushAppErrorBrandPromoNoShowMsg;
                }
            }
            else if (!string.IsNullOrEmpty(View.CustomUrl))
            {
                if (View.CustomUrl.IndexOf("http://",StringComparison.OrdinalIgnoreCase)<0 &&
                    View.CustomUrl.IndexOf("https://", StringComparison.OrdinalIgnoreCase) < 0
                    )
                {
                    msg = I18N.Message.PushAppErrorCustomUrl;
                }
            }
            else
            {
                msg = I18N.Message.PushAppErrorNoParametersMsg;
            }
            return msg;
        }

        private int GenerateActionEventPushMessage(PushApp pushApp)
        {
            Member mem = _mp.MemberGet(View.UserName);

            ActionEventPushMessage actionEventPushMsg;
            if (pushApp.ActionEventPushMessageId.HasValue)
            {
                //update
                actionEventPushMsg = _mp.ActionEventPushMessageGetByActionEventPushMessageId(pushApp.ActionEventPushMessageId.Value);
            }
            else
            {
                //insert
                actionEventPushMsg = new ActionEventPushMessage();
            }

            actionEventPushMsg.Status = true;
            actionEventPushMsg.CreateTime = DateTime.Now;
            actionEventPushMsg.SendStartTime = pushApp.PushDate;
            actionEventPushMsg.SendEndTime = pushApp.PushDate.AddDays(1);
            actionEventPushMsg.CreateId = mem.UniqueId;
            actionEventPushMsg.Subject = actionEventPushMsg.Content = pushApp.Description;

            if (pushApp.BusinessHourGuid != null && !pushApp.BusinessHourGuid.Equals(Guid.Empty))
            {
                actionEventPushMsg.EventType = (int) ActionEventType.RemotePushBid;
                actionEventPushMsg.BusinessHourGuid = pushApp.BusinessHourGuid;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;
            }
            else if (pushApp.CityId != null)
            {
                //城市轉換為頻道的處理
                PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(pushApp.CityId.Value);
                if (city != null)
                {
                    if (city.ChannelId.HasValue)
                    {
                        actionEventPushMsg.ChannelId = city.ChannelId.Value;
                        if (city.ChannelAreaId.HasValue)
                        {
                            actionEventPushMsg.AreaId = city.ChannelAreaId.Value;
                        }
                        else
                        {
                            actionEventPushMsg.AreaId = 0;
                        }

                        actionEventPushMsg.EventType = (int)ActionEventType.RemotePushChannel;
                        
                        _mp.ActionEventPushMessageSet(actionEventPushMsg);
                        return actionEventPushMsg.Id;
                    }
                }
            }
            else if (pushApp.VourcherId != null)
            {
                actionEventPushMsg.EventType = (int)ActionEventType.RemotePushVourcher;
                actionEventPushMsg.VourcherId = pushApp.VourcherId;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;
            }
            else if (!string.IsNullOrEmpty(pushApp.SellerId))
            {
                actionEventPushMsg.EventType = (int)ActionEventType.RemotePushSeller;
                actionEventPushMsg.SellerId = pushApp.SellerId;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;

            }
            else if (pushApp.EventPromoId.HasValue && pushApp.PushType == (int)PushAppType.EventPromo)
            {
                actionEventPushMsg.EventType = (int)ActionEventType.RemotePushEvnetPromo;
                actionEventPushMsg.EventPromoId = pushApp.EventPromoId;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;

            }
            else if (pushApp.BrandPromoId.HasValue && pushApp.PushType == (int)PushAppType.BrandPromo)
            {
                actionEventPushMsg.EventType = (int)ActionEventType.RemotePushBrandPromo;
                actionEventPushMsg.BrandPromoId = pushApp.BrandPromoId;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;

            }
            else if (!string.IsNullOrEmpty(pushApp.CustomUrl))
            {
                actionEventPushMsg.EventType = (int)ActionEventType.RemotePushCustomUrl;
                actionEventPushMsg.CustomUrl = pushApp.CustomUrl;
                _mp.ActionEventPushMessageSet(actionEventPushMsg);
                return actionEventPushMsg.Id;
                
            }

            return 0;
        }

        #endregion
    }
}

using LunchKingSite.WebLib.Views;
using System.Data;
using System.Data.SqlClient;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Elmah;

namespace LunchKingSite.WebLib.Presenters
{
    public class ErrorPresenter : Presenter<IErrorView>
    {
        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            ShowMessage(View.RequestMode);
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();  
            return true;
        }

        protected void ShowMessage(string mode)
        {
            // maintain original message content for invalid requests
            if (!string.IsNullOrEmpty(mode))
                switch (mode)
                {
                    case "sgo": // simple group order
                        View.ErrorMessage = View.Message.GetString("MsgGroupOrderError");
                        break;
                    case "404": // page not found
                        View.ErrorMessage = string.IsNullOrEmpty(View.ErrorPath) ? View.Message.GetString("MsgPageNotFound").Replace(" ({0}) ", "") : string.Format(View.Message.GetString("MsgPageNotFound"), View.ErrorPath);
                        break;
                    case "403":
                        View.ErrorMessage = string.IsNullOrEmpty(View.ErrorPath) ? View.Message.GetString("MsgNoAccess").Replace(" ({0}) ", "") : string.Format(View.Message.GetString("MsgNoAccess"), View.ErrorPath);
                        break;
                    case "403.2":
                    case "403.6":
                        View.ErrorTitle = "短時間內嘗試太多次登入了，請休息一陣子後再試";
                        View.ErrorMessage = "";
                        break;
                    case "ndt":
                        View.ErrorMessage = View.Message.GetString("MsgNoDeliveryTimeError");
                        break;
                    case "nm":
                        View.ErrorMessage = View.Message.GetString("MsgNoMenu");
                        break;
                    case "bhc":
                        View.ErrorMessage = View.Message.GetString("MsgBizHourClosed");
                        break;
                    case "bhf":
                        View.ErrorMessage = View.Message.GetString("MsgBizHourFull");
                        break;
                    case "loginfailure":
                        View.ErrorMessage = View.Message.GetString("MsgLoginFail");
                        break;
                    case "deal-not-found":
                        View.ErrorMessage = View.Message.GetString("MsgDealNotFound");
                        break;
                    default:
                        break;
                }
        }
    }
}

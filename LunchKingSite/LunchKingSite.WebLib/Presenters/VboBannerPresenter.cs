﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class VboBannerPresenter : Presenter<IVboBannerView>
    {
        RandomCmsType randomcmstype = RandomCmsType.VBO;
        readonly ICmsProvider cmsProv;
        public VboBannerPresenter()
        {
            cmsProv = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        }

        public IList<ViewCmsRandom> GetPreviewData()
        {
            IList<ViewCmsRandom> items = cmsProv.GetViewCmsRandomCollection("", randomcmstype).OrderBy(t => t.StartTime).ToList();
            return items;
        }

        public void SetPreviewDataToView()
        {
            IList<ViewCmsRandom> items = cmsProv.GetViewCmsRandomCollection("", randomcmstype).OrderBy(t => t.StartTime).ToList();
            View.FillPreviewData(items);
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            // 或許可能會用到，用jsonp 的方式來取資料 /piinlife/VobBanner.aspx?output=jsonp
            if (this.View.OutputFormat == "jsonp")
            {
                var items = (from t in GetPreviewData() select new { Content = t.Body }).ToArray();
                string json = new JsonSerializer().Serialize(items);
                json = string.Format("{0}({1})", View.CallbackKey, json);
                View.OutputJsonp(json);
            }
            else
            {
                SetPreviewDataToView();
            }
            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using LunchKingSite.BizLogic.Component;
using System.Web;
using System.IO;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesBusinessListPresenter : Presenter<ISalesBusinessListView>
    {
        private ISellerProvider sp;
        private IHumanProvider hp;
        private IPponProvider pp;
        private ISystemProvider sysp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Search += OnSearch;
            View.Export += OnExport;
            View.Import += OnImport;
            View.OnGetCount += OnGetCount;
            View.PageChanged += OnPageChanged;
            return true;
        }

        public SalesBusinessListPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IPponProvider pponProv, ISystemProvider sysProv)
        {
            sp = sellerProv;
            hp = humProv;
            pp = pponProv;
            sysp = sysProv;
        }

        #region event

        protected void OnSearch(object sender, EventArgs e)
        {
            LoadData(1);
        }
        protected void OnExport(object sender, EventArgs e)
        {
            ExportData(1);
        }
        protected void OnImport(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(View.FUExport.PostedFile.FileName))
            {
                if (Path.GetExtension(View.FUExport.PostedFile.FileName) == ".xls")
                {
                    HSSFWorkbook workbook = new HSSFWorkbook(View.FUExport.PostedFile.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);
                    Row row;
                    List<string> ImportError = new List<string>();
                    int EmportCount = 0;
                    for (int i = 1; i <= currentSheet.LastRowNum; i++)
                    {
                        row = currentSheet.GetRow(i);
                        if (row.GetCell(0) == null || string.IsNullOrEmpty(row.GetCell(0).ToString()) && (row.GetCell(1) == null || string.IsNullOrEmpty(row.GetCell(1).ToString())) &&
                            (row.GetCell(2) == null || string.IsNullOrEmpty(row.GetCell(2).ToString())) && (row.GetCell(3) == null || string.IsNullOrEmpty(row.GetCell(3).ToString())) &&
                            (row.GetCell(4) == null || string.IsNullOrEmpty(row.GetCell(4).ToString())) && (row.GetCell(5) == null || string.IsNullOrEmpty(row.GetCell(5).ToString())) &&
                            (row.GetCell(6) == null || string.IsNullOrEmpty(row.GetCell(6).ToString())) && (row.GetCell(7) == null || string.IsNullOrEmpty(row.GetCell(7).ToString())) &&
                            (row.GetCell(8) == null || string.IsNullOrEmpty(row.GetCell(8).ToString())) && (row.GetCell(9) == null || string.IsNullOrEmpty(row.GetCell(9).ToString())) &&
                            (row.GetCell(10) == null || string.IsNullOrEmpty(row.GetCell(10).ToString())))
                        {
                            //空白行即結束
                            break;
                        }

                        string BusinessHourGuid = row.GetCell(1).ToString();
                        string deSalesEmail = "";
                        string opSalesEmail = "";

                        if (row.GetCell(8) == null)
                            deSalesEmail = "";
                        else
                            deSalesEmail = row.GetCell(8).ToString();

                        if (row.GetCell(10) == null)
                            opSalesEmail = "";
                        else
                            opSalesEmail = row.GetCell(10).ToString();

                        bool checkDe = false;
                        bool checkOP = false;


                        Guid bid = Guid.Empty;
                        Guid.TryParse(BusinessHourGuid, out bid);

                        if (!string.IsNullOrEmpty(BusinessHourGuid))
                        {
                            string check = SellerFacade.CheckSales(1, deSalesEmail, opSalesEmail);

                            if (string.IsNullOrEmpty(check))
                            {
                                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, deSalesEmail);
                                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, opSalesEmail);

                                if (deSalesEmp.IsLoaded)
                                {
                                    checkDe = true;


                                    //是否要匯入業務2
                                    bool isImportopSalesEmp = false;
                                    if (opSalesEmp.IsLoaded)
                                    {
                                        isImportopSalesEmp = true;
                                        checkOP = true;
                                    }
                                    else if (string.IsNullOrEmpty(opSalesEmail))
                                    {
                                        //業務2非必填
                                        checkOP = true;
                                    }


                                    if (checkDe && checkOP)
                                    {
                                        //兩個業務正確才匯入
                                        ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(bid);
                                        if (vcdc.Count > 0)
                                        {
                                            #region 多檔次
                                            foreach (ViewComboDeal vcd in vcdc)
                                            {
                                                //DealProperty
                                                DealProperty dp = pp.DealPropertyGet(vcd.BusinessHourGuid);
                                                dp.DealEmpName = deSalesEmp.EmpName;
                                                dp.EmpNo = deSalesEmp.EmpNo;
                                                dp.DevelopeSalesId = deSalesEmp.UserId;
                                                if (isImportopSalesEmp)
                                                {
                                                    dp.OperationSalesId = opSalesEmp.UserId;
                                                }
                                                else
                                                {
                                                    dp.OperationSalesId = null;
                                                }

                                                pp.DealPropertySet(dp);
                                                //DealAccounting
                                                DealAccounting da = pp.DealAccountingGet(vcd.BusinessHourGuid);
                                                da.SalesId = deSalesEmp.EmpName;
                                                if (isImportopSalesEmp)
                                                {
                                                    da.OperationSalesId = opSalesEmp.EmpName;
                                                }
                                                else
                                                {
                                                    da.OperationSalesId = null;
                                                }
                                                pp.DealAccountingSet(da);
                                            }
                                            #endregion

                                        }
                                        else
                                        {
                                            #region 單檔次
                                            DealProperty dp = pp.DealPropertyGet(bid);
                                            dp.DealEmpName = deSalesEmp.EmpName;
                                            dp.EmpNo = deSalesEmp.EmpNo;
                                            dp.DevelopeSalesId = deSalesEmp.UserId;
                                            if (isImportopSalesEmp)
                                            {
                                                dp.OperationSalesId = opSalesEmp.UserId;
                                            }
                                            else
                                            {
                                                dp.OperationSalesId = null;
                                            }
                                            pp.DealPropertySet(dp);
                                            //DealAccounting
                                            DealAccounting da = pp.DealAccountingGet(bid);
                                            da.SalesId = deSalesEmp.EmpName;
                                            if (isImportopSalesEmp)
                                            {
                                                da.OperationSalesId = opSalesEmp.EmpName;
                                            }
                                            else
                                            {
                                                da.OperationSalesId = null;
                                            }
                                            pp.DealAccountingSet(da);
                                            #endregion
                                        }


                                        pp.ChangeLogInsert("ViewPponDeal", bid.ToString(), "批次變更開發業務為=>" + deSalesEmp.EmpName);
                                        if (isImportopSalesEmp)
                                        {
                                            pp.ChangeLogInsert("ViewPponDeal", bid.ToString(), "批次變更經營業務為=>" + opSalesEmp.EmpName);
                                        }
                                        else if (string.IsNullOrEmpty(opSalesEmail))
                                        {
                                            //業務2非必填
                                            pp.ChangeLogInsert("ViewPponDeal", bid.ToString(), "批次變更經營業務為=>null");
                                        }

                                        EmportCount++;
                                    }
                                }
                                else
                                {
                                    ImportError.Add("第" + i.ToString() + "列");
                                }
                            }
                            else
                            {
                                ImportError.Add("第" + i.ToString() + "列");
                            }
                        }
                        else
                        {
                            ImportError.Add("第" + i.ToString() + "列");
                        }
                    }
                    if (ImportError.Count > 0)
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆\\n失敗筆數：" + ImportError.Count.ToString() + "筆\\n失敗清單：" + string.Join("、", ImportError));
                    }
                    else
                    {
                        View.ShowMessage("成功筆數：" + EmportCount.ToString() + "筆");
                    }
                }
                else
                {
                    View.ShowMessage("請使用匯出時的檔案格式做匯入");
                }
            }
            else
            {
                View.ShowMessage("請選擇匯入檔案");
            }
        }

        protected void OnGetCount(object sender, DataEventArgs<int> e)
        {
            e.Data = pp.ViewPponDealNoSubGetCount(View.EmpUserId, View.CrossDeptTeam, View.SalesId, GetFilter());
        }
        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            LoadData(e.Data);
        }

        private string[] GetFilter()
        {
            List<string> filter = new List<string>();
            if (!string.IsNullOrWhiteSpace(View.ItemName))
            {
                filter.Add(ViewPponDealCalendar.Columns.ItemName + " like %" + View.ItemName + "%");
            }
            if (View.Bid != Guid.Empty)
            {
                filter.Add(ViewPponDealCalendar.Columns.BusinessHourGuid + "=" + View.Bid);
            }
            if (!string.IsNullOrWhiteSpace(View.SellerName))
            {
                filter.Add(ViewPponDealCalendar.Columns.SellerName + " like %" + View.SellerName + "%");
            }
            if (View.OrderTimeS != DateTime.MinValue)
            {
                filter.Add(ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + ">=" + View.OrderTimeS);
            }
            if (View.OrderTimeE != DateTime.MinValue)
            {
                filter.Add(ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + "<" + View.OrderTimeE);
            }
            filter.Add(ViewPponDealCalendar.Columns.GroupOrderStatus + " is not null ");
            if (View.IsOrderTimeSet)
            {
                filter.Add(ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + "<" + DateTime.MaxValue.Date);
            }

            return filter.ToArray();
        }

        #endregion

        #region Private Method

        private void LoadData(int pageNumber)
        {         
            Dictionary<ViewPponDealCalendar, Proposal> dataList = new Dictionary<ViewPponDealCalendar, Proposal>();
            ViewPponDealCalendarCollection vpdc = pp.ViewPponDealGetListPagingNoSub(pageNumber, View.PageSize, ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " desc ", View.EmpUserId, View.CrossDeptTeam, View.SalesId, GetFilter());
            foreach (ViewPponDealCalendar vpd in vpdc)
            {
                Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, vpd.BusinessHourGuid);
                dataList.Add(vpd, pro);
            }
            View.SetBusinessList(dataList);
        }

        private void ExportData(int pageNumber)
        {
            Dictionary<ViewPponDealCalendar, Proposal> dataList = new Dictionary<ViewPponDealCalendar, Proposal>();
            ViewPponDealCalendarCollection vpdc = pp.ViewPponDealGetListPagingNoSub(1, 9999, ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " desc ", View.EmpUserId, View.CrossDeptTeam, View.SalesId, GetFilter());

            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("批次轉換提案單");

            sheet.SetColumnWidth(0, 3000);
            sheet.SetColumnWidth(1, 10000);
            sheet.SetColumnWidth(2, 10000);
            sheet.SetColumnWidth(3, 3000);
            sheet.SetColumnWidth(4, 3000);
            sheet.SetColumnWidth(5, 3000);
            sheet.SetColumnWidth(6, 3000);
            sheet.SetColumnWidth(7, 3000);
            sheet.SetColumnWidth(8, 10000);
            sheet.SetColumnWidth(9, 3000);
            sheet.SetColumnWidth(10, 10000);
            sheet.SetColumnWidth(11, 3000);


            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("商家名稱");
            cols.CreateCell(1).SetCellValue("Bid");
            cols.CreateCell(2).SetCellValue("母檔Bid");
            cols.CreateCell(3).SetCellValue("專案內容");
            cols.CreateCell(4).SetCellValue("上檔起日");
            cols.CreateCell(5).SetCellValue("上檔迄日");
            cols.CreateCell(6).SetCellValue("狀態");
            cols.CreateCell(7).SetCellValue("負責業務1");
            cols.CreateCell(8).SetCellValue("負責業務1Email");
            cols.CreateCell(9).SetCellValue("負責業務2");
            cols.CreateCell(10).SetCellValue("負責業務2Email");
            cols.CreateCell(11).SetCellValue("檔次類型");

            int i = 0;
            foreach (ViewPponDealCalendar vpd in vpdc)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vpd.BusinessHourGuid);
                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vpd.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vpd.OperationSalesId ?? 0);
                ViewComboDealCollection comboDeals = pp.GetViewComboDealByBid(vpd.BusinessHourGuid, false);
                Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, vpd.BusinessHourGuid);
                Guid MainBusinessHourGuid = Guid.Empty;
                ViewPponDeal v = pp.ViewPponDealGetByBusinessHourGuid(vpd.BusinessHourGuid);
                foreach(ViewComboDeal combo in comboDeals)
                {
                    MainBusinessHourGuid = combo.MainBusinessHourGuid ?? Guid.Empty;
                    break;
                }
                if(comboDeals.Count > 0)
                {
                    if (vpd.BusinessHourGuid != comboDeals[0].MainBusinessHourGuid)
                    {
                        //子檔不匯出
                        continue;
                    }
                }


                SystemCode sc = sysp.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), (int)(vpd.DealType ?? 0));
                cols = sheet.CreateRow(i + 1);
                cols.CreateCell(0).SetCellValue(vpd.SellerName);
                cols.CreateCell(1).SetCellValue(vpd.BusinessHourGuid.ToString());
                cols.CreateCell(2).SetCellValue(MainBusinessHourGuid != Guid.Empty ? MainBusinessHourGuid.ToString() : "");
                cols.CreateCell(3).SetCellValue(PponFacade.EventNameGet(v));
                cols.CreateCell(4).SetCellValue(vpd.BusinessHourOrderTimeS.Date == DateTime.MaxValue.Date ? string.Empty : vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd"));
                cols.CreateCell(5).SetCellValue(vpd.BusinessHourOrderTimeS.Date == DateTime.MaxValue.Date ? string.Empty : vpd.BusinessHourOrderTimeE.ToString("yyyy/MM/dd"));
                cols.CreateCell(6).SetCellValue(this.GetStatus(vpd.BusinessHourOrderTimeS, vpd.BusinessHourOrderTimeE, pro));
                cols.CreateCell(7).SetCellValue(deSalesEmp.EmpName);
                cols.CreateCell(8).SetCellValue(deSalesEmp.Email);
                cols.CreateCell(9).SetCellValue(opSalesEmp.EmpName);
                cols.CreateCell(10).SetCellValue(opSalesEmp.Email);
                cols.CreateCell(11).SetCellValue(vpd.DealType == null ? string.Empty : sysp.SystemCodeGetByCodeGroupId("DealType", sc.CodeId).CodeName);

                pp.ChangeLogInsert("ViewPponDeal", vpd.BusinessHourGuid.ToString(), "檔次資料批次匯出");
                i++;
            }

            //下載檔案
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}", HttpUtility.UrlEncode(@"ExportDeal.xls")));
            workbook.Write(HttpContext.Current.Response.OutputStream);
        }

        /// <summary>
        /// 取得檔次狀態
        /// </summary>
        /// <param name="businessHourOrderTimeS">上檔起日</param>
        /// <param name="businessHourOrderTimeE">上檔迄日</param>
        /// <param name="pro">Proposal Data</param>
        /// <returns></returns>
        private string GetStatus(DateTime businessHourOrderTimeS, DateTime businessHourOrderTimeE, Proposal pro)
        {
            string rtnStatus = string.Empty;

            if (businessHourOrderTimeS > DateTime.Now)
            {
                if (pro.IsLoaded && !Helper.IsFlagSet(pro.BusinessCreateFlag, ProposalBusinessCreateFlag.SettingCheck))
                {
                    rtnStatus = "未確認"; 
                }
                else
                {
                    rtnStatus = businessHourOrderTimeS < DateTime.MaxValue.Date ? "已排檔" : "待排檔";
                }
            }
            else
            {
                rtnStatus = businessHourOrderTimeE < DateTime.Now ? "已結檔" : "搶購中";
            }

            return rtnStatus;
        }
        #endregion
    }
}

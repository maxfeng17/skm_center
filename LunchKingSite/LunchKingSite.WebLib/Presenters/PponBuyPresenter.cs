﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.WebLib.Component.MemberActions;
using PaymentType = LunchKingSite.Core.PaymentType;
using System.Web;
using System.Security.Principal;
using System.Threading;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponBuyPresenter : Presenter<IPponBuyView>
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(PponBuyPresenter).Name);

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            PponPaymentDTO paymentDTO = null;
            logger.Info("BuyTest " + View.TicketId + " OnViewInitialized1");

            //未知原因造成部分OTP回來後身分不見(猜測shopback回來的)
            if (!string.IsNullOrEmpty(View.TicketId) && !View.IsAuthenticated)
            {
                paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(View.TicketId);
                logger.Info("BuyTest " + paymentDTO.State.ToString() + " insert auth RedirectToLogin");
                if (paymentDTO != null && paymentDTO.State == PponPaymentDTOState.OTP)
                {

                    Member member = MemberFacade.GetMember(paymentDTO.DeliveryInfo.BuyerUserId);
                    bool diffIdentify = HttpContext.Current.User == null ||
                        HttpContext.Current.User.Identity.IsAuthenticated == false ||
                        HttpContext.Current.User.Identity.Name.Equals(member.UserName, StringComparison.OrdinalIgnoreCase) == false;
                    //無效會員或非會員token
                    if (member != null && member.IsLoaded)
                    {
                        IIdentity identity = new PponIdentity(member, PponIdentity._USER);
                        HttpContext.Current.Items.Add("Identity", identity);
                        IPrincipal principal = null;
                        if (identity.AuthenticationType == "Token")
                        {
                            principal = new PponPrincipal(identity, new[] { "Token" });
                        }
                        else if (identity.AuthenticationType == "User")
                        {
                            RoleCollection rc = MemberFacade.GetMemberRoleCol(identity.Name);
                            principal = new PponPrincipal(identity, rc.Select(a => a.RoleName).ToArray());
                        }

                        Thread.CurrentPrincipal = principal;
                        HttpContext.Current.User = principal;

                        if (diffIdentify)
                        {
                            MemberUtility.UserForceSignInForAuthModule(member.UserName);
                        }
                    }
                }
            }

            // user is not logged in
            if (View.IsAuthenticated == false && config.GuestBuyEnabled == false && View.AllowGuestBuy == false)
            {
                logger.Info("BuyTest " + View.TicketId + " " + View.IsAuthenticated.ToString() + " " + View.AllowGuestBuy.ToString() + " RedirectToLogin");
                View.RedirectToLogin();
                return false;
            }
            if (string.IsNullOrEmpty(View.TicketId) == false)
            {
                paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(View.TicketId);
                if (paymentDTO == null || paymentDTO.DeliveryInfo == null)
                {
                    View.ShowCreditcardChargeInfo();
                    return true;
                }
                if (paymentDTO.State == PponPaymentDTOState.Paid)
                {
                    View.GoToPponDefaultPage(View.BusinessHourGuid, "此筆交易已完成。", true);
                    return false;
                }
                View.BusinessHourGuid = paymentDTO.BusinessHourGuid;
            }
            else if (View.BusinessHourGuid != Guid.Empty)
            {
                View.TicketId = OrderFacade.MakeRegularTicketId();
                paymentDTO = new PponPaymentDTO()
                {
                    BusinessHourGuid = View.BusinessHourGuid,
                    TicketId = View.TicketId,
                    State = PponPaymentDTOState.Init
                };
                PponBuyFacade.SetPponPaymentDTOToSession(View.TicketId, paymentDTO);
            }
            else
            {
                View.GoToPponDefaultPage(View.BusinessHourGuid, string.Empty, true);
                return false;
            }
            logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + "OnViewInitialized2");
            ComboDeal comboDeal = pp.GetComboDeal(View.BusinessHourGuid);
            View.MainGuid = (comboDeal.IsLoaded && comboDeal.MainBusinessHourGuid != null) ? comboDeal.MainBusinessHourGuid.Value : View.BusinessHourGuid;

            View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid, true);
            View.AllowGuestBuy = View.TheDeal.AllowGuestBuy ?? false;
            View.IsBankDeal = View.TheDeal.IsBankDeal ?? false;
            logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + "OnViewInitialized3");
            if (Helper.IsFlagSet(View.TheDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb)
                && GameFacade.HasGameDealBuyPermission(View.UserId, View.TheDeal.BusinessHourGuid, paymentDTO.OrderGuid) == false)
            {
                if (HttpContext.Current.Request["force"] != "open" + DateTime.Today.ToString("yyyyMMdd"))
                {
                    View.GoToPponDefaultPage(View.BusinessHourGuid, string.Empty, true);
                    return false;
                }
            }

            // APP only
            if (PponDealPreviewManager.GetDealCategoryIdList(View.BusinessHourGuid)
                                      .IndexOf(CategoryManager.Default.AppLimitedEdition.CategoryId) > -1)
            {
                View.RedirectToAppLimitedEdtionPage();
                return false;
            }

            if (View.IsAuthenticated)
            {
                MemberUtility.SetUserSsoInfoReady(View.UserId);
            }
            else if (config.GuestBuyEnabled || View.AllowGuestBuy)
            {
                MemberUtility.SetUserEmptySsoInfo();
            }

            //View.FillContentData()            
            if (View.TaishinCreditCardOnly)
            {
                View.TaishinCreditcards = CreditCardPremiumManager.GetCreditcardBankBinsByBankId(1);
            }

            logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + paymentDTO.State.ToString());
            if (paymentDTO.State == PponPaymentDTOState.Init || paymentDTO.State == PponPaymentDTOState.Loaded)
            {
                logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + paymentDTO.State.ToString());
                var theDeal = View.TheDeal;
                DealCheck(theDeal, View.UserName);

                View.CheckRefUrl();

                #region 檢查檔次是否為開賣(放)狀態，或者已售完

                PponDealStage pponDealStage = theDeal.GetDealStage();
                if (pponDealStage == PponDealStage.RunningAndFull)
                {
                    View.GoToPponDefaultPage(View.BusinessHourGuid, "此檔好康已銷售一空喔。", true);
                    return true;
                }
                else if ((theDeal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                {
                    View.GoToPponDefaultPage(View.BusinessHourGuid, string.Empty, true);
                    return true;
                }
                else if (!View.IsPreview && pponDealStage != PponDealStage.Running && pponDealStage != PponDealStage.RunningAndOn)
                {
                    return false;
                }

                #endregion 檢查檔次是否為開賣(放)狀態，或者已售完

                #region 多重選項資訊
                AccessoryGroupCollection viagc = new AccessoryGroupCollection();
                bool isHouseNewVersion = false;//判斷是否為新版提案單
                if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                {
                    isHouseNewVersion = true;
                }
                if (isHouseNewVersion)
                {
                    //新版(統一改抓ppon_option)
                    viagc = PponFacade.AccessoryGroupGetByHouseDeal(theDeal);
                }
                else
                {
                    //舊版
                    viagc = ip.AccessoryGroupGetListByItem(theDeal.ItemGuid);
                    foreach (AccessoryGroup ag in viagc)
                    {
                        ag.members = new List<ViewItemAccessoryGroup>();
                        ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(theDeal.ItemGuid, ag.Guid);
                        foreach (ViewItemAccessoryGroup vig in vigc)
                        {
                            ag.members.Add(vig);
                        }
                    }
                }
                

                #endregion 多重選項資訊

                #region 免運費資訊

                // 階梯式運費
                View.TheCouponFreight = pp.CouponFreightGetList(theDeal.BusinessHourGuid, CouponFreightType.Income);

                #endregion 免運費資訊

                #region ATM

                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);
                bool isATMAvailable = PponBuyFacade.IsDealATMAvailable(deal);

                #endregion ATM

                var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(View.BusinessHourGuid, VbsRightFlag.VerifyShop);
                View.NotDeliveryIslands = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NotDeliveryIslands);
                View.IsShoppingCart = theDeal.ShoppingCart.HasValue ? theDeal.ShoppingCart.Value : false;
                View.BaseGrossMargin = PponFacade.GetMinimumGrossMarginFromBids(View.BusinessHourGuid, true) * 100; //取百分比                
                View.TripleData = MemberFacade.GetEinvoiceTriple(View.UserId);
                
                var ttmodel = new TrackTagInputModel
                {
                    ViewPponDealData = theDeal,
                    Config = config,
                    GtagPage = GtagPageType.Cart,
                    YahooEa = YahooEaType.AddToCart,
                    ScupioEventCategory = ScupioEventCategory.PageView,
                    ScupioPageType = ScupioPageType.CheckoutPage,
                };

                View.GtagDataJson = TagManager.CreateFatory(ttmodel, TagProvider.GoogleGtag).GetJson();
                View.YahooDataJson = TagManager.CreateFatory(ttmodel, TagProvider.Yahoo).GetJson();
                View.ScupioCheckoutPageJson = TagManager.CreateFatory(ttmodel, TagProvider.Scupio).GetJson();

                // 讀取會員資料，購物金，己購買、發票相關的個人訊息
                ViewMemberBuildingCity m;
                int alreadyboughtcount = 0;
                if (View.IsAuthenticated == false)
                {
                    View.bcash = 0;
                    View.scash = 0;
                    m = new ViewMemberBuildingCity();
                }
                else
                {
                    View.bcash = MemberFacade.GetAvailableMemberPromotionValue(View.UserName);
                    decimal e7scash;
                    decimal pscash;
                    View.scash = OrderFacade.GetSCashSum2(View.UserId, out e7scash, out pscash);
                    m = mp.ViewMemberBuildingCityGet(View.UserName);
                    alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, (bool)theDeal.IsDailyRestriction);
                    SetMemberInfo();
                }

                #region MasterPass PreCheckout

                if (config.EnableMasterpassPreCheckout)
                {
                    var preCheckout = op.MasterPassPreCheckoutTokenGet(View.UserId);
                    if (preCheckout.IsLoaded && preCheckout.IsUsed == false)
                    {
                        var masterPass = new MasterPass();
                        var checkResult = masterPass.PreCheckout(preCheckout, View.UserId);
                        if (checkResult.IsCheckoutPass)
                        {
                            View.SetMasterPassPreCheckoutData(checkResult);
                        }
                    }
                }

                #endregion MasterPass PreCheckout

                List<BuyPromotionText> buyPromotionTexts =
                    SystemDataManager.GetFromCache<List<BuyPromotionText>>(SystemDataManager._BUY_PROMOTION_TEXT);

                PponPickupStore pickupStoreUI = View.GetPponPickupStoreFromRequest();
                ProductDeliveryType productDeliveryType = pickupStoreUI == null ? ProductDeliveryType.Normal : pickupStoreUI.ProductDeliveryType;

                MemberCheckoutInfo mci = mp.MemberCheckoutInfoGet(View.UserId);
                ProductDeliveryType? memberLastProductDeliveryType = null;
                if (mci.IsLoaded)
                {
                    memberLastProductDeliveryType = (ProductDeliveryType)mci.LastProductDeliveryType;
                }

                View.SetContent(deal, mp.ViewMemberBuildingCityGet(View.UserName), viagc, alreadyboughtcount, stores,
                    config.DiscountCodeUsed, (EntrustSellType)theDeal.EntrustSell.GetValueOrDefault(), 
                    (bool)theDeal.IsDailyRestriction, View.TripleData,
                    PromotionFacade.GetMemberDiscountList(View.UserId, View.BusinessHourGuid),
                    (bool)theDeal.Installment3months, (bool)theDeal.Installment6months, 
                    (bool)theDeal.Installment12months, buyPromotionTexts,
                    isATMAvailable, MemberFacade.GetOrUpdatePponPickupStore(View.UserId, pickupStoreUI),
                    productDeliveryType, deal.IspQuantityLimit, memberLastProductDeliveryType);
                if (paymentDTO.State == PponPaymentDTOState.Loaded)
                {
                    View.FillContentData(paymentDTO);
                }
                logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + paymentDTO.State.ToString());
            }
            else
            {
                logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + paymentDTO.State.ToString());
                try
                {
                    View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
                }
                catch (Exception)
                {
                    logger.Info("tickedId=" + View.TicketId);
                    throw;
                }

                int userId = View.UserId;
                if (View.IsAuthenticated == false && (config.GuestBuyEnabled || View.AllowGuestBuy))
                { 
                    userId = paymentDTO.DeliveryInfo.BuyerUserId;
                }

                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);

                //step 1
                if (paymentDTO.OrderGuid == Guid.Empty)
                {
                    //MasterPAss取消操作 回購買頁
                    if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.MasterPass && View.IsMasterPassCancel)
                    {
                        View.GoToPponDefaultPage(theDeal.BusinessHourGuid, string.Empty, false);
                    }

                    //建立訂單
                    MakeOrderResult makeOrderStatus = new PponBuyCore(View).MakeOrder(paymentDTO, userId);
                    logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + makeOrderStatus.ToString());
                    if (makeOrderStatus == MakeOrderResult.Success)
                    {
                        //刷卡表單頁面，需要的資料
                        View.Installment = paymentDTO.DeliveryInfo.CreditCardInstallment;
                        View.CreditcardsAllowInstallment = CreditCardPremiumManager.GetCreditcardBankBinsByInstallment();
                        if (View.TaishinCreditCardOnly)
                        {
                            View.TaishinCreditcards = CreditCardPremiumManager.GetCreditcardBankBinsByBankId(1);
                        }
                    }
                    else if (makeOrderStatus == MakeOrderResult.UserNotLogin)
                    {
                        View.GoToPponDefaultPage(theDeal.BusinessHourGuid, null, false);
                        return true;
                    }
                    else if (makeOrderStatus == MakeOrderResult.DiscountCodeTooFull)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "很抱歉！該折價券使用數量已達上限，系統會將您跳轉至購買頁面，請您再試一次。");
                        return true;
                    }
                    else if (makeOrderStatus == MakeOrderResult.PendingAmountWasNegative)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "付款金額為負數，交易失敗。");
                        return true;
                    }
                    else if (makeOrderStatus == MakeOrderResult.TotalAmountNotMatch)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "付款總額不一致，交易失敗。");
                        return true;
                    }
                    else if (makeOrderStatus == MakeOrderResult.PendingAmountNotMatch)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "付款金額不一致，交易失敗");
                        return true;
                    }
                    else if (makeOrderStatus == MakeOrderResult.OtherError)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "系統發生錯誤，交易失敗，請稍後再試");
                        return true;
                    }
                }
                else if (paymentDTO.OrderGuid != Guid.Empty && paymentDTO.State != PponPaymentDTOState.OTP && !paymentDTO.IsThirdPartyPay)
                {
                    //導頁至OTP又back,paymenttrans 會重複產
                    //跑OTP回來不執行
                    //thirdparty不執行
                    PaymentTransaction oldPayment = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
                    if (oldPayment.IsLoaded && oldPayment.Message == "OTP驗證中")
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "因前次交易之3D驗證未完成，請重新購買。");
                        return true;
                    }
                }
                //step 2.1
                if (paymentDTO.ThirdPartyPaymentSystem != ThirdPartyPayment.None)
                {
                    if (paymentDTO.ThirdPartyParameters == null)
                    {
                        paymentDTO.ThirdPartyParameters = new ThirdPartyParameters();
                        PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);

                        RequestThridPartyPaymentFullResult thirdPartyPayment = new PponBuyCore(View).RequestThirdPartyPayment(paymentDTO, userId);
                        if (thirdPartyPayment.Result == RequestThridPartyPaymentResult.ThirdPartyPayFail)
                        {
                            View.ShowResult(PaymentResultPageType.ThirdPartyPayFail, null, paymentDTO, theDeal, 0, thirdPartyPayment.Message);
                            return true;
                        }
                        if (thirdPartyPayment.Result == RequestThridPartyPaymentResult.ThirdPartyPaymentNotFound)
                        {
                            View.SetThirdPartyPayFailContent("找不到所選的支付方式", paymentDTO);
                            return true;
                        }
                        if (thirdPartyPayment.Result == RequestThridPartyPaymentResult.TaishinPay)
                        {
                            //至台新平台請求消費者同意支付
                            View.GoToTaishinPay(thirdPartyPayment.TaishinPay.PayCode, thirdPartyPayment.TaishinPay.TahshinOrderId);
                            return true;
                        }
                        if (thirdPartyPayment.Result == RequestThridPartyPaymentResult.LinePay)
                        {
                            View.GoToLinePay(thirdPartyPayment.Data);
                            return true;
                        }
                    }
                    else
                    {
                        paymentDTO.ThirdPartyParameters.IsLineCancel = View.IsLineCancel;
                        paymentDTO.ThirdPartyParameters.LineTransactionId = View.LineTransactionId;
                        PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);
                    }
                }
                //setp 2.2
                logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + paymentDTO.State.ToString());
                if (paymentDTO.State == PponPaymentDTOState.Paying)
                {
                    MakePaymentFullResult paymentResut = new PponBuyCore(View).MakePayment(paymentDTO, userId);
                    if (paymentResut.Result == MakePaymentResult.BeginOTPAuth)
                    {
                        View.RedirectToOTPPage(paymentResut.Url);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.UserNotLogin)
                    {
                        View.GoToPponDefaultPage(theDeal.BusinessHourGuid, null, false);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.ATMFull)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "很抱歉！目前ATM轉帳名額已額滿，將改用其它方式付款。");
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.PCashNotSupported)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "很抱歉！您的PayEasy購物金餘額不足，系統會將您跳轉至購買頁面，請您再試一次。");
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.ThirdPartyPaymentNotFound)
                    {
                        View.SetThirdPartyPayFailContent("找不到所選的支付方式", paymentDTO);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.ThirdPartyPaymentPayFail)
                    {
                        View.ShowResult(PaymentResultPageType.ThirdPartyPayFail, new MemberLinkCollection(), paymentDTO, theDeal, 0,
                            paymentResut.Message);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.ThirdPartyPaymentPayCancel)
                    {
                        View.ShowResult(PaymentResultPageType.ThirdPartyPayCancel, null, paymentDTO, theDeal, 0);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.DiscountCodeTooFull)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "很抱歉！該折價券使用數量已達上限，系統會將您跳轉至購買頁面，請您再試一次。");
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.DiscountCodeError)
                    {
                        View.RestartBuy(config.DiscountCodeUsed, theDeal.BusinessHourGuid,
                            "很抱歉，此張折價券無法使用");
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.OutofLimitBlackCreditcardOrderError)
                    {
                        string message = I18N.Message.OutofLimitBlackCreditcardOrderError;
                        View.AlertMessageAndGoToDefault(message, paymentDTO.BusinessHourGuid);
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.PickupCard)
                    {
                        View.ShowMessageAndLogout("您的卡片為掛失卡，請重新交易。");
                        return true;
                    }
                    if (paymentResut.Result == MakePaymentResult.AlreadyPaid)
                    {
                        View.ShowResult(PaymentResultPageType.CreditcardSuccess, null, paymentDTO, theDeal, 0);
                        return true;
                    }

                    PponBuyFacade.SetPponPaymentDTOToSession(View.TicketId, paymentDTO);
                }
                else if (paymentDTO.State == PponPaymentDTOState.OTP)
                {
                    //3D OTP
                    logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + "QueryOTP1");
                    paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(View.TicketId);
                    string userName = MemberFacade.GetUserName(paymentDTO.DeliveryInfo.BuyerUserId);
                    logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + "QueryOTP2");
                    var result = CreditCardUtility.QueryOTP(paymentDTO.CreditCardInfo,
                         paymentDTO.OrderGuid, userName, OrderClassification.LkSite, CreditCardOrderSource.Ppon);
                    logger.Info("BuyTest " + paymentDTO.TransactionId + "：" + "QueryOTP3");
                    int rlt;
                    PaymentTransaction ptCreditCard;
                    CheckCreditCardAuthStatus creditCardPayStatus = new PponBuyCore(View).CheckResultAndUpdatePaymentTransaction(
                        paymentDTO.TransactionId, false,
                        result, paymentDTO.CreditCardInfo, userId, paymentDTO.CreditCardInfo.CreditCardGuid, out rlt, out ptCreditCard);
                    if (creditCardPayStatus == CheckCreditCardAuthStatus.Success)
                    {
                        new PponBuyCore(View).MemoryMemberCreditcardNumber(paymentDTO, userId, userName);
                    }
                }


                //step 3
                new PponBuyCore(View).CompleteOrder(paymentDTO.OrderGuid, paymentDTO.TransactionId, paymentDTO, userId);
            }

            return true;
        }

        private void SetMemberInfo()
        {
            Member mem = mp.MemberGet(View.UserName);

            #region Carrier

            CarrierType carrierType = CarrierType.Member; // 預設為會員載具
            if (Enum.IsDefined(typeof(CarrierType), mem.CarrierType))
            {
                carrierType = (CarrierType)mem.CarrierType;
            }
            if (CarrierType.Member == carrierType)
            {
                View.Carrier = new KeyValuePair<CarrierType, string>(carrierType, mem.UniqueId.ToString());
            }
            else
            {
                View.Carrier = new KeyValuePair<CarrierType, string>(carrierType, mem.CarrierId);
            }

            #endregion Carrier

            #region UserInfo

            if (mem.IsLoaded)
            {
                int cityId = 0;
                int areaId = 0;
                string address = string.Empty;
                Guid buildingGuid = Guid.Empty;
                if (mem.BuildingGuid.HasValue && MemberUtilityCore.DefaultBuildingGuid != mem.BuildingGuid.Value)
                {
                    Building bu = lp.BuildingGet(mem.BuildingGuid.Value);
                    City area = lp.CityGet(bu.CityId);
                    City city = lp.CityGet(area.ParentId.GetValueOrDefault());
                    if (!string.IsNullOrEmpty(mem.CompanyAddress))
                    {
                        address = mem.CompanyAddress.Replace(city.CityName, string.Empty).Replace(area.CityName, string.Empty);
                    }
                    cityId = city.Id;
                    areaId = area.Id;
                    buildingGuid = bu.Guid;
                }
                View.UserInfo = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}",
                    mem.LastName,
                    mem.FirstName,
                    mem.Mobile,
                    cityId,
                    areaId,
                    buildingGuid,
                    address,
                    mem.DisplayName,
                    mem.UserEmail,
                    mem.UserName);
            }
            #endregion UserInfo
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.CheckOut += OnCheckOut;
            View.GetOrAddGuestMemberThenSetInfo += OnGetOrAddGuestMemberThenSetInfo;
            View.ResetContent += OnResetContent;
            return true;
        }

        void OnGetOrAddGuestMemberThenSetInfo(object sender, GuestMemberEventArgs e)
        {
            Member mem = mp.MemberGet(e.UserName);
            if (mem.IsLoaded)
            {
                View.GuestMemberId = mem.UniqueId;
                //check if member is inactive , downgrade to 0 level member
                MemberFacade.DowngradeGuestMember(mem);
            }
            else
            {
                var reply = new GuestMemberRegister(e.UserName, e.DisplayName, e.Mobile).Process(false);
                if (reply == MemberRegisterReplyType.RegisterSuccess)
                {
                    View.GuestMemberId = MemberFacade.GetUniqueId(e.UserName);
                }
                else if (reply == MemberRegisterReplyType.EmailError)
                {
                    throw new Exception("Email格式有問題，請您再確認");
                }
                else
                {
                    throw new Exception("系統發生錯誤，目前無法建立會員資料.");
                }
            }
        }

        protected void OnCheckOut(object sender, DataEventArgs<PponPaymentDTO> e)
        {
            string ticketId = e.Data.TicketId;
            PponPaymentDTO paymentDTO = e.Data;
            PponDeliveryInfo deliveryInfo = paymentDTO.DeliveryInfo;
            View.TheDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(e.Data.BusinessHourGuid);
            IViewPponDeal theDeal = View.TheDeal;
            if (config.EnableGrossMarginRestrictions)
            {
                View.BaseGrossMargin = PponFacade.GetMinimumGrossMarginFromBids(View.BusinessHourGuid, true) * 100; //取百分比
            }

            // 為了讓DeliveryInfo會有值
            View.EntrustSell = (EntrustSellType)theDeal.EntrustSell.GetValueOrDefault();

            View.TicketId = ticketId;

            int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;

            int qty = saleMultipleBase > 0 ? saleMultipleBase : deliveryInfo.Quantity;
            if (View.TripleData == null)
            {
                View.TripleData = MemberFacade.GetEinvoiceTriple(deliveryInfo.BuyerUserId);
            }

            string strResult = CheckData(theDeal, (int)(Math.Ceiling((double)qty / (theDeal.ComboPackCount ?? 1))),
                deliveryInfo.BonusPoints, deliveryInfo.SCash, deliveryInfo.DiscountCode, deliveryInfo.StoreGuid, deliveryInfo.TotalAmount,
                theDeal.IsDailyRestriction.GetValueOrDefault());

            if (string.IsNullOrWhiteSpace(strResult))
            {
                JsonSerializer serializer = new JsonSerializer();
                string infoStr = serializer.Serialize(e.Data.DeliveryInfo);

                List<ItemEntry> ies = PaymentFacade.ProcessSubItemsAndAccessoriesWithComboPack(
                    paymentDTO.ItemOptions, ticketId, theDeal, deliveryInfo.StoreGuid, infoStr);

                if (!string.IsNullOrEmpty(deliveryInfo.DiscountCode))
                {
                    ViewDiscountDetail discountCode = op.ViewDiscountDetailByCode(deliveryInfo.DiscountCode);
                    e.Data.IsVisaDiscountCode = Helper.IsFlagSet(discountCode.Flag, DiscountCampaignUsedFlags.VISA);
                }

                try
                {
                    OrderFacade.PutItemToCart(ticketId, ies);
                    paymentDTO.State = PponPaymentDTOState.Loaded;
                    PponBuyFacade.SetPponPaymentDTOToSession(ticketId, paymentDTO);
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format("Something wrong while put items to cart : ticketId={0}, bid={1}", ticketId,
                            theDeal.BusinessHourGuid), ex);
                    throw;
                }

                if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.MasterPass)
                {
                    MasterPass mPass = new MasterPass();
                    var masterPassData = mPass.GetRequestToken(deliveryInfo.PendingAmount, ticketId);

                    paymentDTO.State = PponPaymentDTOState.Paying;
                    PponBuyFacade.SetPponPaymentDTOToSession(ticketId, paymentDTO);
                    View.GotoMasterPass(masterPassData, ticketId);
                }
                else if (deliveryInfo.PendingAmount > 0 && (
                             deliveryInfo.PaymentMethod == BuyTransPaymentMethod.CreaditCardInstallment ||
                             deliveryInfo.PaymentMethod == BuyTransPaymentMethod.CreaditCardPayOff))
                {
                    View.ShowTransactionPanel(paymentDTO);
                }
                else
                {
                    paymentDTO.State = PponPaymentDTOState.Paying;
                    PponBuyFacade.SetPponPaymentDTOToSession(ticketId, paymentDTO);
                    View.GotoPay(ticketId);
                }
            }
            else
            {
                View.AlertMessage(strResult);
                OnResetContent(this, null);
            }
        }

        private string CheckData(IViewPponDeal vpd, int qty, int bonusCash, decimal scash, string discountCode,
            Guid storeGuid, int totalAmount, bool isDailyRestriction)
        {
            bool isGroupCoupon = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            if (isGroupCoupon)
            {
                var info = PponFacade.GetApiGroupCouponInfo(vpd);
                if (qty > (info.MaxQuantity * vpd.SaleMultipleBase))
                {
                    logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3} #1-1",
                        View.UserName,
                        vpd.BusinessHourGuid,
                        qty,
                        (info.MaxQuantity * vpd.SaleMultipleBase));
                    return "每單訂購數量最多" + info.MaxQuantity * vpd.SaleMultipleBase + "份，請重新輸入數量";
                }
            }
            else if (qty > (vpd.MaxItemCount ?? int.MaxValue))
            {
                logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3} #1-2",
                    View.UserName,
                    vpd.BusinessHourGuid,
                    qty,
                    vpd.MaxItemCount);
                return "每單訂購數量最多" + vpd.MaxItemCount + "份，請重新輸入數量";
            }
            int already =
                pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid, isDailyRestriction)
                * (isGroupCoupon ? vpd.SaleMultipleBase ?? 1 : 1);

            if (vpd.ItemDefaultDailyAmount != null)
            {
                if (qty + already > vpd.ItemDefaultDailyAmount.Value)
                {
                    logger.InfoFormat("{0} 購買 {1}. 參數 {2}, {3}, {4} #1-3",
                        View.UserName,
                        vpd.BusinessHourGuid,
                        qty,
                        already,
                        vpd.ItemDefaultDailyAmount.Value);
                    return "每人訂購數量最多" + vpd.ItemDefaultDailyAmount + "份，您已訂購" + already + "份，請重新輸入數量";
                }
            }
            if (bonusCash > Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(View.UserName) / 10))
            {
                return "您的可用紅利點數換算可抵用金額最多為" + Math.Floor(MemberFacade.GetAvailableMemberPromotionValue(View.UserName) / 10) + "元，請重新輸入金額";
            }
            if (bonusCash < 0)
            {
                return "紅利點數金額不可為負，請重新輸入金額";
            }
            decimal e7scash;
            decimal pscash;
            decimal generalScash = OrderFacade.GetSCashSum2(View.UserId, out e7scash, out pscash);
            if (scash > generalScash)
            {
                return string.Format("您的可用購物金可抵用金額最多為{0}元，請重新輸入金額", generalScash);
            }
            if (PponFacade.CheckExceedOrderedQuantityLimit(vpd, qty))
            {
                return "很抱歉!已超過最大訂購數量";
            }
            if (PponFacade.CheckExceedStoreTotalQuantityLimit(vpd, storeGuid, qty))
            {
                return "很抱歉!已超過分店最大訂購數量";
            }
            if (DateTime.Now > vpd.BusinessHourOrderTimeE)
            {
                return "很抱歉~此好康已結束";
            }

            if (!string.IsNullOrWhiteSpace(discountCode))
            {
                decimal amount = totalAmount;
                int minimumAmount;
                var discountCodeStatus = PromotionFacade.GetDiscountCodeStatus(
                    discountCode, DiscountCampaignUsedFlags.Ppon, amount, View.UserName, out minimumAmount, vpd.BusinessHourGuid.ToString());
                if (discountCodeStatus == DiscountCodeStatus.None)
                {
                    return I18N.Phrase.DiscountCodeNone;
                }
                else if (discountCodeStatus == DiscountCodeStatus.UpToQuantity)
                {
                    return I18N.Phrase.DiscountCodeUptoQuantity;
                }
                else if (discountCodeStatus == DiscountCodeStatus.IsUsed)
                {
                    return I18N.Phrase.DiscountCodeIsUsed;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Disabled)
                {
                    return I18N.Phrase.DiscountCodeDisabled;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OverTime)
                {
                    return I18N.Phrase.DiscountCodeOverTime;
                }
                else if (discountCodeStatus == DiscountCodeStatus.Restricted)
                {
                    return string.Format(I18N.Phrase.DiscountCodeRestricted, "品生活");
                }
                else if (discountCodeStatus == DiscountCodeStatus.UnderMinimumAmount)
                {
                    return string.Format(I18N.Phrase.DiscountCodeUnderMinimumAmount, minimumAmount);
                }
                else if (discountCodeStatus == DiscountCodeStatus.DiscountCodeCategoryWrong)
                {
                    return I18N.Phrase.DiscountCodeCategoryWrong;
                }
                else if (discountCodeStatus == DiscountCodeStatus.OwnerUseOnlyWrong)
                {
                    return I18N.Phrase.DiscountCodeOwnerUseOnlyWrong;
                }
                else if (discountCodeStatus != DiscountCodeStatus.CanUse)
                {
                    return I18N.Phrase.DiscountCodeCanNotUse;
                }
                else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                {
                    return string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(discountCode) + "使用");
                }
                else if (discountCodeStatus == DiscountCodeStatus.CategoryUseLimit)
                {
                    return string.Format("僅限" + PromotionFacade.GetDiscountCategoryString(discountCode) + "使用");
                }
            }
            return string.Empty;
        }

        protected void OnResetContent(object sender, object e)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(View.BusinessHourGuid);

            if (View.TripleData == null)
            {
                View.TripleData = MemberFacade.GetEinvoiceTriple(View.UserId);
            }
            View.bcash = MemberFacade.GetAvailableMemberPromotionValue(View.UserName);
            AccessoryGroupCollection viagc = ip.AccessoryGroupGetListByItem(deal.ItemGuid);
            foreach (AccessoryGroup ag in viagc)
            {
                ag.members = new List<ViewItemAccessoryGroup>();
                ViewItemAccessoryGroupCollection vigc = ip.ViewItemAccessoryGroupGetList(deal.ItemGuid, ag.Guid);
                foreach (ViewItemAccessoryGroup vig in vigc)
                {
                    ag.members.Add(vig);
                }
            }

            View.bcash = MemberFacade.GetAvailableMemberPromotionValue(View.UserName);
            decimal e7scash;
            decimal pscash;
            View.scash = OrderFacade.GetSCashSum2(View.UserId, out e7scash, out pscash);

            SetMemberInfo();

            int alreadyboughtcount = pp.ViewPponOrderDetailGetCountByUser(View.UserName, View.BusinessHourGuid,
                (bool)deal.IsDailyRestriction);
            var stores = ViewPponStoreManager.DefaultManager.ViewPponStoreGetList(View.BusinessHourGuid, VbsRightFlag.VerifyShop);

            List<BuyPromotionText> buyPromotionTexts = SystemDataManager.GetFromCache<List<BuyPromotionText>>(SystemDataManager._BUY_PROMOTION_TEXT);

            bool isATMAvailable = PponBuyFacade.IsDealATMAvailable(deal);

            var pickupStoreUI = View.GetPponPickupStoreFromRequest();
            ProductDeliveryType productDeliveryType = pickupStoreUI == null ? ProductDeliveryType.Normal : pickupStoreUI.ProductDeliveryType;

            MemberCheckoutInfo mci = mp.MemberCheckoutInfoGet(View.UserId);
            ProductDeliveryType? memberLastProductDeliveryType = null;
            if (mci.IsLoaded)
            {
                memberLastProductDeliveryType = (ProductDeliveryType)mci.LastProductDeliveryType;
            }

            View.SetContent(deal, mp.ViewMemberBuildingCityGet(View.UserName), viagc, alreadyboughtcount, stores,
                config.DiscountCodeUsed, (EntrustSellType)deal.EntrustSell.GetValueOrDefault(), 
                (bool)deal.IsDailyRestriction, View.TripleData,
                PromotionFacade.GetMemberDiscountList(View.UserId, View.BusinessHourGuid),
                (bool)deal.Installment3months, (bool)deal.Installment6months, (bool)deal.Installment12months, 
                buyPromotionTexts,
                isATMAvailable, MemberFacade.GetOrUpdatePponPickupStore(View.UserId, pickupStoreUI),
                productDeliveryType, deal.IspQuantityLimit, memberLastProductDeliveryType);
        }

        /// <summary>
        /// 根據不同的檔次設定決定是否需導頁至不同購買頁
        /// </summary>
        private void DealCheck(IViewPponDeal theDeal, string userName)
        {
            // 檢查是否為0元檔次
            if (decimal.Equals(0, theDeal.ItemPrice))
            {
                View.GoToZeroActivity();
            }

            // 檢查是否為捐款檔次
            if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) > 0)
            {
                View.GoToKindPay();
            }

            // 檢查是否為PEZ限定活動
            if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZeventWithUserId) > 0)
            {
                string pezExternalUserid = MemberFacade.CheckIsPEZUserID(userName);
                if (string.IsNullOrEmpty(pezExternalUserid))
                {
                    View.RedirectToLoginOnlyPEZ(I18N.Message.PponBuyPezOnlyLoginLimit, theDeal.BusinessHourGuid);
                }
                Peztemp peztemp = OrderFacade.GetPezTemp(theDeal.BusinessHourGuid, pezExternalUserid);
                if (string.IsNullOrEmpty(peztemp.PezCode))
                {
                    View.RedirectToLoginOnlyPEZ(I18N.Message.NoPEZeventWithUserIdCouponCode, theDeal.BusinessHourGuid);
                }
            }
        }
    }

    public enum MakeOrderResult
    {
        Undefined,
        Success,
        UserNotLogin,
        DiscountCodeTooFull,
        MasterPassCancel,
        PendingAmountWasNegative,
        TotalAmountNotMatch,
        PendingAmountNotMatch,
        OtherError
    }

    public class MakePaymentFullResult
    {
        public MakePaymentResult Result { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }


        public static MakePaymentFullResult Create(MakePaymentResult result, string message = "")
        {
            return new MakePaymentFullResult { Result = result, Message = message };
        }

        public override string ToString()
        {
            return Result.ToString();
        }
    }

    public enum RequestThridPartyPaymentResult
    {
        None,
        UserNotLogin,
        ThirdPartyPaymentFail,
        ThirdPartyPayFail,
        TaishinPay,
        ThirdPartyPaymentNotFound,
        ThirdPartyPaymentPayFail,
        LinePay
    }

    public class RequestThridPartyPaymentFullResult
    {
        public RequestThridPartyPaymentResult Result { get; set; }
        public TaishinPayModel TaishinPay { get; set; }
        public string Message { get; set; }
        public string Data { get; set; }

        public static RequestThridPartyPaymentFullResult Create(RequestThridPartyPaymentResult result, string message = "")
        {
            return new RequestThridPartyPaymentFullResult { Result = result, Message = message };
        }

        public static RequestThridPartyPaymentFullResult CreateLinePay(string data)
        {
            return new RequestThridPartyPaymentFullResult { Result = RequestThridPartyPaymentResult.LinePay, Data = data };
        }

        public static RequestThridPartyPaymentFullResult Create(RequestThridPartyPaymentResult result, TaishinPayModel taishinPay)
        {
            return new RequestThridPartyPaymentFullResult { Result = result, TaishinPay = taishinPay };
        }

        public override string ToString()
        {
            return Result.ToString();
        }
    }

    public enum MakePaymentResult
    {
        Undefined,
        Success,
        UserNotLogin,
        ThirdPartyPaymentNotFound,
        ThirdPartyPaymentPayFail,
        ThirdPartyPaymentPayCancel,
        ATMFull,
        PCashNotSupported,
        DiscountCodeTooFull,
        DiscountCodeError,
        OutofLimitBlackCreditcardOrderError,
        PickupCard,
        AlreadyPaid,
        BeginOTPAuth
    }

    /// <summary>
    /// 建立訂單、付款資料
    /// </summary>
    public class PponBuyCore
    {
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static ILog logger = LogManager.GetLogger(typeof(PponBuyCore).Name);
        private static IPponEntityProvider pponEntity = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();

        private IPponBuyView View { get; set; }
        public PponBuyCore(IPponBuyView view)
        {
            this.View = view;
        }



        public MakeOrderResult MakeOrder(PponPaymentDTO paymentDTO, int userId)
        {
            PponDeliveryInfo deliveryInfo = paymentDTO.DeliveryInfo;
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);

            Member mem = mp.MemberGet(userId);
            string userName = mp.MemberGet(userId).UserName;
            if (userId == 0 || mem.IsLoaded == false || mem.IsApproved == false)
            {
                return MakeOrderResult.UserNotLogin;
            }

            //applepay已先建好transactionId
            bool isApplePay = paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.ApplePay;
            if (isApplePay)
            {
                if (paymentDTO.PrebuiltOrderGuid == null)
                {
                    return MakeOrderResult.OtherError;
                }
                paymentDTO.TransactionId = PaymentFacade.GetTransactionIdByOrderGuid(paymentDTO.PrebuiltOrderGuid.Value);
            }
            else
            {
                paymentDTO.TransactionId = PaymentFacade.GetTransactionId();
            }

            if (string.IsNullOrEmpty(deliveryInfo.DiscountCode) == false)
            {
                DiscountCode discountCode;
                //discountCode實際扣抵金額
                int discountAmount;
                if (PromotionFacade.GetDiscountCodeAmount(deliveryInfo.DiscountCode, out discountAmount, out discountCode) == false)
                {
                    return MakeOrderResult.DiscountCodeTooFull;
                }
                deliveryInfo.DiscountAmount = discountAmount;
            }
            //訂單總金額
            int orderTotalAmount = 0;
            int orderPendingAmount = 0;

            ShoppingCartCollection scc = op.ShoppingCartGetList(ShoppingCart.Columns.TicketId, paymentDTO.TicketId);
            foreach (ShoppingCart sc in scc)
            {
                int x = (int)Math.Round(sc.ItemUnitPrice * ((theDeal.SaleMultipleBase ?? 0) > 0 ? 1 : sc.ItemQuantity));
                orderPendingAmount += x;
                orderTotalAmount += x;
            }
            int deliveryCharge = PponBuyFacade.GetDealDeliveryCharge(orderTotalAmount, theDeal);
            orderTotalAmount = orderTotalAmount + deliveryCharge;
            orderPendingAmount = orderPendingAmount + deliveryCharge;

            orderPendingAmount -= deliveryInfo.DiscountAmount;
            orderPendingAmount -= deliveryInfo.BonusPoints;
            orderPendingAmount -= deliveryInfo.PayEasyCash;
            orderPendingAmount -= (int)deliveryInfo.SCash;
            if (orderPendingAmount < 0)
            {
                return MakeOrderResult.PendingAmountWasNegative;
            }
            if (deliveryInfo.TotalAmount != orderTotalAmount)
            {
                //throw new Exception("付款總額不一致，交易失敗");
                return MakeOrderResult.TotalAmountNotMatch;
            }
            if (deliveryInfo.PendingAmount != orderPendingAmount)
            {
                string msg =
                    "付款金額不一致，交易失敗\r\n" + ProviderFactory.Instance().GetSerializer().Serialize(deliveryInfo) + "\r\n" + paymentDTO.ItemOptions;
                logger.Info(msg);
                //throw new Exception(msg);
                return MakeOrderResult.PendingAmountNotMatch;
            }

            DeliveryInfo di = new DeliveryInfo();
            di.DeliveryCharge = deliveryCharge;
            string usermemo = deliveryInfo.DeliveryDateString + deliveryInfo.DeliveryTimeString + deliveryInfo.Notes;
            if (theDeal.DeliveryType == (int)DeliveryType.ToHouse)
            {
                di.ProductDeliveryType = deliveryInfo.ProductDeliveryType;
                di.DeliveryId = deliveryInfo.MemberDeliveryID;

                di.CustomerName = deliveryInfo.WriteName;
                di.CustomerMobile = deliveryInfo.Phone;
                #region 一般宅配地址

                if (deliveryInfo.ProductDeliveryType == ProductDeliveryType.Normal)
                {
                    string deliveryAddress = paymentDTO.DeliveryInfo.WriteAddress;
                    if (string.IsNullOrWhiteSpace(paymentDTO.DeliveryInfo.WriteZipCode))
                    {
                        //停用BUILDING，嘗試由 paymentDTO的addressInfo取得資料
                        LkAddress address = paymentDTO.ConvertToPaymentDTO().GetReceiverAddress();
                        if (address != null)
                        {
                            City township = CityManager.TownShipGetById(address.TownshipId);
                            if (township != null)
                            {
                                deliveryAddress = township.ZipCode + " " + address.CompleteAddress;
                            }
                        }
                    }
                    else
                    {
                        deliveryAddress = paymentDTO.DeliveryInfo.WriteZipCode + " " + deliveryAddress;
                    }
                    di.DeliveryAddress = deliveryAddress;
                }
                #endregion
                #region 超取店舖資訊
                else
                {             
                    di.PickupStore = new PickupStore
                    {
                        StoreId = deliveryInfo.PickupStore.StoreId,
                        StoreName = deliveryInfo.PickupStore.StoreName,
                        StoreTel = deliveryInfo.PickupStore.StoreTel,
                        StoreAddr = deliveryInfo.PickupStore.StoreAddr
                    };
                }
                #endregion
            }
            else
            {
                di.DeliveryAddress = PponBuyFacade.GetOrderDesc(deliveryInfo);
            }

            // make order
            Guid oid = paymentDTO.OrderGuid = OrderFacade.MakeOrder(userName, paymentDTO.TicketId,
                DateTime.Now, usermemo, di, theDeal, Helper.GetOrderFromType(), paymentDTO.PrebuiltOrderGuid);

            //save wms (pchome)
            if (theDeal.IsWms)
            {
                PponBuyFacade.SaveWmsOrder(paymentDTO.OrderGuid);
            }

            PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);
            Order o = op.OrderGet(oid);

            // 更新 Order 營業額, 即扣掉折價券的金額。
            if (deliveryInfo.DiscountAmount > 0)
            {
                o.Turnover = (int)(o.Total - deliveryInfo.DiscountAmount);
            }

            o.OrderMemo = ((int)deliveryInfo.ReceiptsType.GetValueOrDefault()).ToString() + "|" + deliveryInfo.InvoiceType + "|" + deliveryInfo.UnifiedSerialNumber + "|" + deliveryInfo.CompanyTitle;
            o.Installment = (int)deliveryInfo.CreditCardInstallment;
            o.MobilePayType = isApplePay ? (int?)MobilePayType.ApplePay : null;
            op.OrderSet(o);

            return MakeOrderResult.Success;
        }

        public RequestThridPartyPaymentFullResult RequestThirdPartyPayment(PponPaymentDTO paymentDTO, int userId)
        {
            PponDeliveryInfo deliveryInfo = paymentDTO.DeliveryInfo;
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            Member mem = mp.MemberGet(userId);
            string userName = mp.MemberGet(userId).UserName;
            Order o = op.OrderGet(paymentDTO.OrderGuid);
            if (userId == 0 || mem.IsLoaded == false || mem.IsApproved == false)
            {
                return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.UserNotLogin);
            }
            string transactionId = paymentDTO.TransactionId;

            if (paymentDTO.IsThirdPartyPay && paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
            {
                var tempType = PaymentFacade.GetPaymentTypeByThirdPartyPayment(paymentDTO.ThirdPartyPaymentSystem);
                if (tempType == null)
                {
                    return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.ThirdPartyPaymentNotFound);
                }
                PaymentTransaction ptPay = OrderFacade.MakeTransaction(transactionId, (PaymentType)tempType, deliveryInfo.PendingAmount,
                    (DepartmentTypes)theDeal.Department, userName, o.Guid);

                #region TaishinPay

                //pt 1.11 taishin
                ptPay.ApiProvider = (int)PaymentAPIProvider.TaishinPay;
                ptPay.Status = Helper.SetPaymentTransactionPhase(ptPay.Status, PayTransPhase.Requested);
                op.PaymentTransactionSet(ptPay);
                string accessToken = MemberFacade.GetTaishinPayAccessToken(userId);
                if (string.IsNullOrEmpty(accessToken))
                {
                    return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.ThirdPartyPaymentFail);
                }

                string tahshinOrderId = o.OrderId.Substring(0, 20); //台新儲值支付orderId最長只到20
                                                                    //新增TransLog
                var transLog = new ThirdPartyPayTransLog()
                {
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    UserId = userId,
                    OrderGuid = o.Guid,
                    OrderId = tahshinOrderId,
                    OrderName = theDeal.ItemName,
                    Amount = (int)ptPay.Amount,
                    TransId = transactionId,
                    TicketId = paymentDTO.TicketId,
                    PaymentStatus = (int)TaishinPayTransStatus.StartPay,
                    CreateTime = DateTime.Now
                };
                int transLogId = op.ThirdPartyPayTransLogSet(transLog);
                transLog.Id = transLogId;

                //pt 2.11 taishin
                //取得PayCode
                int rtnCode;
                string payCode = TaishinPayUtility.RequestPayCodeFromTaishin(
                    accessToken, tahshinOrderId, theDeal.ItemName, ptPay.Amount, userId, theDeal.BusinessHourGuid, transLogId, out rtnCode);
                transLog.ReturnCode = rtnCode;
                if (string.IsNullOrEmpty(payCode) == false)
                {
                    transLog.OneTimeCode = payCode;
                    transLog.PaymentStatus = (int)TaishinPayTransStatus.ReceivePayCode;
                    op.ThirdPartyPayTransLogSet(transLog);

                    return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.TaishinPay, new TaishinPayModel
                    {
                        PayCode = payCode,
                        TahshinOrderId = tahshinOrderId
                    });
                }
                else
                {
                    transLog.PaymentStatus = (int)TaishinPayTransStatus.PayFail;
                    op.ThirdPartyPayTransLogSet(transLog);

                    string reason = string.Format("{0}授權失敗。", Helper.GetEnumDescription(ThirdPartyPayment.TaishinPay));
                    if (Enum.IsDefined(typeof(TaishinPayReturnCodeAndMsg), rtnCode))
                    {
                        reason = Helper.GetEnumDescription((TaishinPayReturnCodeAndMsg)rtnCode);
                    }
                    return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.ThirdPartyPayFail, reason);
                }

                #endregion
            }

            #region LinePay            
            if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.None)
            {
                return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.ThirdPartyPaymentNotFound);
            }
            if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
            {
                PaymentTransaction ptPay = OrderFacade.MakeTransaction(transactionId, PaymentType.LinePay, deliveryInfo.PendingAmount,
                    (DepartmentTypes)theDeal.Department, userName, o.Guid);

                //pt 1.10 linepay
                var lineTrans = LinePayUtility.MakeLinePayTranscation(paymentDTO.TicketId, transactionId);
                var reserve = new ReserveInputModel
                {
                    ProductName = theDeal.CouponUsage,
                    ProductImageUrl = config.LinePayUsingLogo,
                    Amount = deliveryInfo.PendingAmount,
                    Currency = "TWD",
                    OrderId = o.OrderId,
                    ConfirmUrl = string.Format("{0}/ppon/buy.aspx?TransId={1}&TicketId={2}", config.SiteUrl, transactionId, paymentDTO.TicketId),
                    CancelUrl =
                        string.Format("{0}/ppon/buy.aspx?TransId={1}&LineCancel=true&TicketId={2}", config.SiteUrl, transactionId, paymentDTO.TicketId),
                    LangCd = config.LinePayLangCd,
                    Capture = !lineTrans.IsCaptureSeparate
                };

                var reserveResult = LinePayUtility.CreateReserve(lineTrans, reserve, false);
                if (reserveResult.Key == false)
                {
                    return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.ThirdPartyPaymentPayFail, reserveResult.Value);
                }
                else
                {
                    ptPay.ApiProvider = (int)PaymentAPIProvider.LinePay;
                    ptPay.Status = Helper.SetPaymentTransactionPhase(ptPay.Status, PayTransPhase.Requested);
                    op.PaymentTransactionSet(ptPay);
                    return RequestThridPartyPaymentFullResult.CreateLinePay(data: reserveResult.Value);
                }
            }

            #endregion

            return RequestThridPartyPaymentFullResult.Create(RequestThridPartyPaymentResult.None);
        }

        public MakePaymentFullResult MakePayment(PponPaymentDTO paymentDTO, int userId)
        {
            PponDeliveryInfo deliveryInfo = paymentDTO.DeliveryInfo;
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            Member mem = mp.MemberGet(userId);
            string userName = mp.MemberGet(userId).UserName;
            Order o = op.OrderGet(paymentDTO.OrderGuid);
            if (userId == 0 || mem.IsLoaded == false || mem.IsApproved == false)
            {
                return MakePaymentFullResult.Create(MakePaymentResult.UserNotLogin);
            }
            string transactionId = paymentDTO.TransactionId;

            #region 17Life 紅利金折抵

            if (deliveryInfo.PayEasyCash > 0 && MemberFacade.GetPayeasyCashPoint(userId) < deliveryInfo.PayEasyCash)
            {
                return MakePaymentFullResult.Create(MakePaymentResult.PCashNotSupported);
            }

            #region DiscountCode

            //discountCode實際扣抵金額

            PaymentTransaction ptDiscount;
            DiscountCode discountCode;
            int discountAmt;
            int paymentTotal = paymentDTO.DeliveryInfo.TotalAmount;

            if (string.IsNullOrEmpty(deliveryInfo.DiscountCode) == false)
            {
                if (PromotionFacade.GetDiscountCodeAmount(deliveryInfo.DiscountCode, out discountAmt, out discountCode) == false)
                {
                    return MakePaymentFullResult.Create(MakePaymentResult.DiscountCodeTooFull);
                }
                if (discountAmt < deliveryInfo.DiscountAmount)
                {
                    return MakePaymentFullResult.Create(MakePaymentResult.DiscountCodeError);
                }
            }
            else
            {
                discountAmt = 0;
                discountCode = new DiscountCode();
            }

            if (!string.IsNullOrWhiteSpace(deliveryInfo.DiscountCode) && deliveryInfo.DiscountAmount > 0 &&
                discountCode.Amount.GetValueOrDefault() >= deliveryInfo.DiscountAmount && discountCode.CampaignId != null)
            {
                //pt 1.7 init discount code
                int statusInit = Helper.SetPaymentTransactionPhase(
                    Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), PayTransPhase.Created);
                ptDiscount = PaymentFacade.NewTransaction(transactionId, o.Guid, PaymentType.DiscountCode, deliveryInfo.DiscountAmount,
                    null, PayTransType.Authorization, DateTime.Now, userName, "DiscountCode折抵" + deliveryInfo.DiscountCode.ToString(), statusInit, PayTransResponseType.OK);
                try
                {
                    //pt 2.7 do discount code
                    var updateCount = op.DiscountCodeUpdateStatusSet((int)discountCode.CampaignId, discountCode.Code,
                        deliveryInfo.DiscountAmount, userId, o.Guid, paymentTotal, deliveryInfo.PendingAmount);

                    if (updateCount == 1)
                    {
                        int statusSuccess = Helper.SetPaymentTransactionPhase(
                            Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), PayTransPhase.Successful);
                        PaymentFacade.UpdateTransaction(ptDiscount.Id, statusSuccess);
                    }
                    else if (updateCount == 0)
                    {
                        int statusFail = Helper.SetPaymentTransactionPhase(
                            Helper.SetPaymentTransactionDepartmentTypes(0, DepartmentTypes.Ppon), PayTransPhase.Failed);
                        PaymentFacade.UpdateTransaction(ptDiscount.Id, statusFail);
                        //找無折價券或是已使用完畢
                        return MakePaymentFullResult.Create(MakePaymentResult.DiscountCodeTooFull);
                    }
                    else if (updateCount > 1)
                    {
                        //Error:更新多筆折價券
                        logger.Error(string.Format(
                            "折價券更新使用狀態ERROR：=>PponBuyPresenter.cs，更新{0}筆異常，UseTime:{1},UseAmount:{2},UseId:{3},OrderGuid:{4},OrderAmount{5},OrderCost:{6}",
                            updateCount, discountCode.Code, deliveryInfo.DiscountAmount, userId, o.Guid, paymentTotal, deliveryInfo.PendingAmount));
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(string.Format(
                        "折價券更新使用狀態ERROR：PponBuyPresenter.cs=>UseTime:{0},UseAmount:{1},UseId:{2},OrderGuid:{3},OrderAmount{4},OrderCost:{5}",
                        discountCode.Code, deliveryInfo.DiscountAmount, userId, o.Guid, paymentTotal, deliveryInfo.PendingAmount), ex);
                    return MakePaymentFullResult.Create(MakePaymentResult.DiscountCodeTooFull);
                }
            }

            #endregion DiscountCode

            if (deliveryInfo.BonusPoints > 0)
            {
                decimal bonusCash = deliveryInfo.BonusPoints;
                string action = "折抵:" + (theDeal.EventName.Length <= 30 ? theDeal.EventName : (theDeal.EventName.Substring(0, 30) + "..."));
                //pt 1.3 bcash
                OrderFacade.MakeTransaction(transactionId, PaymentType.BonusPoint, deliveryInfo.BonusPoints,
                    (DepartmentTypes)theDeal.Department, userName, o.Guid);
                //pt 2.3 bcash
                MemberFacade.DeductibleMemberPromotionDeposit(o.UserId, Convert.ToDouble(bonusCash * 10), action,
                    BonusTransactionType.OrderAmtRedeeming, o.Guid, MemberFacade.GetUserName(o.UserId), false, false);
            }

            #endregion 17Life 紅利金折抵

            #region 17購物金折抵

            if (deliveryInfo.SCash > 0)
            {
                //2.5 do scash
                decimal scash;
                decimal pscash;
                OrderFacade.PortionGeneralScash(deliveryInfo.SCash, userId, out scash, out pscash);
                //pt 1.5 scash
                if (scash > 0)
                {
                    OrderFacade.MakeTransaction(transactionId, PaymentType.SCash, (int)scash,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);
                    OrderFacade.ScashUsed(scash, userId, o.Guid,
                        "折抵購物金:" + theDeal.SellerName, userName, OrderClassification.CashPointOrder);
                }
                if (pscash > 0)
                {
                    OrderFacade.MakeTransaction(transactionId, PaymentType.Pscash, (int)pscash,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);
                    OrderFacade.PscashUsed(pscash, userId, o.Guid, "折抵購物金:" + theDeal.SellerName, userName);
                }
            }

            #endregion 17購物金折抵
            //有金額
            if (deliveryInfo.PendingAmount > 0)
            {
                #region 信用卡/ATM/LinePay/TaishinPay/MasterPass 付款

                if (deliveryInfo.PaymentMethod  == BuyTransPaymentMethod.CreaditCardPayOff && config.BypassPaymentProcess)
                {
                    #region For test purpose

                    OrderFacade.MakeTransaction(transactionId, PaymentType.ByPass, deliveryInfo.PendingAmount, (DepartmentTypes)theDeal.Department, userName, o.Guid);
                    return MakePaymentFullResult.Create(MakePaymentResult.Success);

                    #endregion For test purpose
                }
                if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm)
                {
                    #region ATM

                    //pt 1.4 atm
                    PaymentTransaction ptAtm = OrderFacade.MakeTransaction(transactionId, PaymentType.ATM, deliveryInfo.PendingAmount,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);

                    if (PaymentFacade.CtAtmCheck(paymentDTO.BusinessHourGuid))
                    {
                        paymentDTO.ATMAccount = PaymentFacade.GetAtmAccount(ptAtm.TransId, userId,
                            deliveryInfo.PendingAmount, theDeal.BusinessHourGuid, o.OrderId, o.Guid);
                        PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);

                        if (paymentDTO.ATMAccount != null && paymentDTO.ATMAccount.Length == 14)
                        {
                            PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.ATM, PayTransType.Authorization);
                            int status = Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Requested);

                            PaymentFacade.UpdateTransaction(paymentDTO.TransactionId, pt.OrderGuid, PaymentType.ATM,
                                Convert.ToDecimal(paymentDTO.DeliveryInfo.PendingAmount), string.Empty,
                                PayTransType.Authorization, DateTime.Now, paymentDTO.ATMAccount, status, (int)PayTransResponseType.OK);
                        }
                        else
                        {
                            return MakePaymentFullResult.Create(MakePaymentResult.ATMFull);
                        }
                    }
                    else
                    {
                        return MakePaymentFullResult.Create(MakePaymentResult.ATMFull);
                    }

                    #endregion
                }
                else if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.FamilyIsp)
                {
                    #region 全家超取店付

                    PaymentTransaction ptFamilyIsp = OrderFacade.MakeTransaction(transactionId, PaymentType.FamilyIsp, deliveryInfo.PendingAmount,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);

                    #endregion
                }
                else if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.SevenIsp)
                {
                    #region 全家超取店付

                    PaymentTransaction ptSevenIsp = OrderFacade.MakeTransaction(transactionId, PaymentType.SevenIsp, deliveryInfo.PendingAmount,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);

                    #endregion

                }
                else if (paymentDTO.ThirdPartyPaymentSystem != ThirdPartyPayment.None)
                {
                    #region TaishinPay 台新儲值支付

                    if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
                    {
                        PaymentTransaction pt = op.PaymentTransactionGet(transactionId, PaymentType.TaishinPay, PayTransType.Authorization);
                        PaymentTransaction ptThirdPartyCharging = null;
                        //if (!View.IsTaishinPayFailure)
                        if (paymentDTO.ThirdPartyParameters != null && string.IsNullOrEmpty(paymentDTO.ThirdPartyParameters.PayCode) == false &&
                            paymentDTO.ThirdPartyParameters.PayFailure == false)
                        {
                            using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                            {
                                //更新授權成功
                                PaymentFacade.UpdateTransaction(
                                    transactionId, pt.OrderGuid, PaymentType.TaishinPay, pt.Amount, string.Empty
                                    , PayTransType.Authorization, DateTime.Now
                                    , "台新儲值支付-授權"
                                    , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Successful)
                                    , (int)PayTransResponseType.OK, PaymentAPIProvider.TaishinPay);
                                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);

                                //新增付款成功 PayTransType.Charging PayTransPhase.Successful
                                ptThirdPartyCharging = PaymentFacade.NewTransaction(
                                    pt.TransId, pt.OrderGuid ?? Guid.Empty, PaymentType.TaishinPay, pt.Amount, pt.AuthCode,
                                    PayTransType.Charging, DateTime.Now, userName, "台新儲值支付-請款",
                                    Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful),
                                    PayTransResponseType.OK, (OrderClassification)(pt.OrderClassification ?? (int)OrderClassification.LkSite), null,
                                    (PaymentAPIProvider)(pt.ApiProvider ?? (int)PaymentAPIProvider.TaishinPay));
                                trans.Complete();
                            }
                        }
                        else
                        {
                            string reason = string.Format("{0}授權失敗。", Helper.GetEnumDescription(ThirdPartyPayment.TaishinPay));
                            string rtnMsg = "未記錄到rtnCode，可能是消費者取消交易";
                            var transLog = op.ThirdPartyPayTransLogGetByOrgTransId(PaymentAPIProvider.TaishinPay, transactionId);
                            if (transLog.IsLoaded)
                            {
                                if (transLog.ReturnCode != null)
                                {
                                    rtnMsg = transLog.ReturnCode.ToString();
                                    if (Enum.IsDefined(typeof(TaishinPayReturnCodeAndMsg), transLog.ReturnCode))
                                    {
                                        reason = Helper.GetEnumDescription((TaishinPayReturnCodeAndMsg)transLog.ReturnCode);
                                    }
                                }
                            }

                            var msg = string.Format("台新儲值支付-扣款失敗, 回傳錯誤訊息:「{0}」, ticketId:{1}", rtnMsg, paymentDTO.TicketId);

                            PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.TaishinPay, pt.Amount
                                , string.Empty
                                , PayTransType.Authorization, DateTime.Now
                                , msg
                                , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                                , 0, PaymentAPIProvider.TaishinPay);

                            return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayFail, reason);
                        }
                    }

                    #endregion

                    #region ThirdPartyPay 取得授權&請款

                    //台新儲值支付，請款後有值，在稍後轉購物金會使用
                    var thirdPartyPaymentType = PaymentFacade.GetPaymentTypeByThirdPartyPayment(paymentDTO.ThirdPartyPaymentSystem);
                    if (paymentDTO.IsThirdPartyPay)
                    {
                        if (thirdPartyPaymentType == null)
                        {
                            return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayFail);
                        }

                        PaymentTransaction pt = op.PaymentTransactionGet(transactionId, (PaymentType)thirdPartyPaymentType, PayTransType.Authorization);

                        #region LinePay

                        if (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
                        {
                            var linePayTrans = op.LinePayTransLogGet(paymentDTO.TicketId);
                            logger.Info("Line Response:" + System.Web.HttpContext.Current.Request.RawUrl);
                            if (string.IsNullOrEmpty(paymentDTO.ThirdPartyParameters.LineTransactionId))
                            {
                                var msg = string.Format("LinePay 無法取得交易序號 ticketId:{0}", paymentDTO.TicketId);
                                linePayTrans.TransStatus = (byte)LinePayTransStatus.RequestTransactionIdFail;
                                linePayTrans.TransMsg = msg;
                                op.LinePayTransLogSet(linePayTrans);
                                PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, (PaymentType)thirdPartyPaymentType, pt.Amount, string.Empty
                                , PayTransType.Authorization, DateTime.Now
                                , msg
                                , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                                , 0, PaymentAPIProvider.LinePay);
                                return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayFail);
                            }

                            //消費者取消交易
                            if (paymentDTO.ThirdPartyParameters.IsLineCancel)
                            {
                                var msg = string.Format("消費者取消交易{0}", linePayTrans.LinePayTransactionId);
                                linePayTrans.TransStatus = (byte)LinePayTransStatus.UserCancel;
                                linePayTrans.TransMsg = msg;
                                op.LinePayTransLogSet(linePayTrans);
                                PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, (PaymentType)thirdPartyPaymentType, pt.Amount, string.Empty
                                    , PayTransType.Authorization, DateTime.Now
                                    , msg
                                    , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Canceled)
                                    , 0, PaymentAPIProvider.LinePay);

                                return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayCancel);
                            }
                            //檢查Line導過來的transactionid 是否與建立 reserve 時一致
                            if (paymentDTO.ThirdPartyParameters.LineTransactionId != linePayTrans.LinePayTransactionId)
                            {
                                var msg = string.Format("交易序號不一致 reserve:{0}, confirm:{1}", linePayTrans.LinePayTransactionId,
                                    paymentDTO.ThirdPartyParameters.LineTransactionId);
                                linePayTrans.TransStatus = (byte)LinePayTransStatus.LinePayTransactionIdNotEqual;
                                linePayTrans.TransMsg = msg;
                                op.LinePayTransLogSet(linePayTrans);
                                PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, (PaymentType)thirdPartyPaymentType, pt.Amount, string.Empty
                                    , PayTransType.Authorization, DateTime.Now
                                    , msg
                                    , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                                    , 0, PaymentAPIProvider.LinePay);
                                return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayFail);
                            }

                            //取得Line授權/請款 (LinePay尚未支援授權與請款分開)
                            var confirmResult = LinePayUtility.Confirm(linePayTrans, (int)pt.Amount);
                            if (confirmResult.Item1 == false)
                            {
                                PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, (PaymentType)thirdPartyPaymentType, pt.Amount
                                    , confirmResult.Item2.ToString()
                                    , PayTransType.Authorization, DateTime.Now
                                    , confirmResult.Item3
                                    , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Failed)
                                    , confirmResult.Item2, PaymentAPIProvider.LinePay);
                                return MakePaymentFullResult.Create(MakePaymentResult.ThirdPartyPaymentPayFail);
                            }

                            PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, (PaymentType)thirdPartyPaymentType, pt.Amount
                                , confirmResult.Item2.ToString()
                                , PayTransType.Authorization, DateTime.Now
                                , confirmResult.Item3
                                , Helper.SetPaymentTransactionPhase(pt.Status, PayTransPhase.Successful)
                                , confirmResult.Item2, PaymentAPIProvider.LinePay);

                            if (linePayTrans.IsCaptureSeparate == false)
                            {
                                //授權一併請款時直接新增請款
                                DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(pt.Status);
                                PaymentFacade.NewTransaction(pt.TransId, pt.OrderGuid ?? Guid.Empty, PaymentType.LinePay, pt.Amount,
                                    pt.AuthCode, PayTransType.Charging, DateTime.Now, userName, "LinePay 授權一併請款",
                                    Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful),
                                    PayTransResponseType.OK, (OrderClassification)pt.OrderClassification.GetValueOrDefault(), null
                                    , (PaymentAPIProvider)pt.ApiProvider.GetValueOrDefault());
                            }
                        }

                        #endregion LinePay
                    }

                    #endregion ThirdPartyPay
                }
                else
                {
                    //if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.ApplePay)
                    //{
                    //    PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
                    //    //update creditcardorder orderguid
                    //    CreditcardOrder co = op.CreditcardOrderGetByOrderGuid(pt.OrderGuid.Value);
                    //    co.OrderGuid = o.Guid;
                    //    op.SetCreditcardOrder(co);
                    //    //只為了更新orderguid，再想想有沒有更好的做法
                    //    PaymentFacade.UpdateTransaction(pt.TransId, o.Guid, (PaymentType)pt.PaymentType, pt.Amount, pt.AuthCode,
                    //        (PayTransType)pt.TransType, pt.TransTime.Value, pt.Message, pt.Status, pt.Result.Value);
                    //}
                    //else
                    //{

                    //pt 1.1 creditcard
                    if (deliveryInfo.PaymentMethod != BuyTransPaymentMethod.ApplePay)
                    {
                        OrderFacade.MakeTransaction(transactionId, PaymentType.Creditcard, deliveryInfo.PendingAmount,
                        (DepartmentTypes)theDeal.Department, userName, o.Guid);
                    }


                    //}

                    //刷卡
                    #region MasterPass

                    if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.MasterPass &&
                        !string.IsNullOrEmpty(View.MasterPass_OAuthToken) &&
                        !string.IsNullOrEmpty(View.MasterPass_OAuthVerifier) &&
                        !string.IsNullOrEmpty(View.MasterPass_CheckoutResourceUrl))
                    {
                        MasterPass mpass = new MasterPass();

                        if (!string.IsNullOrEmpty(View.MasterPassPairingToken) &&
                            !string.IsNullOrEmpty(View.MasterPassPairingVerifier))
                        {
                            mpass.GetPairingToken(View.MasterPassPairingToken,
                                View.MasterPassPairingVerifier, userId);
                        }

                        CreditCardInfo cardInfo = mpass.MasterPassCardInfo(
                            View.MasterPass_OAuthToken, View.MasterPass_OAuthVerifier,
                            View.MasterPass_CheckoutResourceUrl);

                        paymentDTO.MasterPassInfo = cardInfo;
                        PponBuyFacade.SetPponPaymentDTOToSession(paymentDTO.TicketId, paymentDTO);
                    }
                    #endregion

                    //MasterPass
                    if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.MasterPass && paymentDTO.MasterPassInfo != null)
                    {
                        paymentDTO.CopyMasterPassInfoToCreditCardInfo();
                        paymentDTO.IsSaveCreditCardInfo = false;
                    }
                    else if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.CreaditCardInstallment ||
                             paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.CreaditCardPayOff)
                    {
                        paymentDTO.CreditCardInfo.TransactionId = paymentDTO.TransactionId;
                    }

                    if (paymentDTO.CreditCardInfo != null && paymentDTO.CreditCardInfo.IsValid && paymentDTO.DeliveryInfo.PendingAmount > 0)
                    {
                        bool isMasterPass = paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.MasterPass;
                        // 2014-10-23 決議 MasterPass 的交易都走 piinlife 的特店

                        bool isOTP = PaymentFacade.IsOTP(userId, paymentDTO.CreditCardInfo.CardNumber, paymentDTO.DeliveryInfo.DealAccBusinessGroupId, paymentDTO.DeliveryInfo.RootDealType, paymentDTO.DeliveryInfo.TotalAmount,paymentDTO.BusinessHourGuid, paymentDTO.OrderGuid);
                        if (isOTP)
                        {
                            paymentDTO.CreditCardInfo.OTP = new BizLogic.Component.OTPAuthParam
                            {
                                Enabled = true,
                                ReturnUrl = Helper.CombineUrl(config.SiteUrl, "api/payService/taishinOTPResultUrl"),
                                PostBackUrl = Helper.CombineUrl(config.SiteUrl, string.Format("ppon/buy.aspx?TransId={0}&TicketId={1}", paymentDTO.TransactionId, paymentDTO.TicketId))
                            };
                        }

                        CreditCardAuthResult result = CreditCardUtility.Authenticate(
                            paymentDTO.CreditCardInfo,
                            paymentDTO.OrderGuid,
                            userName,
                            OrderClassification.LkSite, isMasterPass ? CreditCardOrderSource.PiinLife : CreditCardOrderSource.Ppon);

                        if (result.IsOTP && result.TransPhase == PayTransPhase.Successful)
                        {
                            var paymentResult = MakePaymentFullResult.Create(MakePaymentResult.BeginOTPAuth);
                            if (result.APIProvider == PaymentAPIProvider.CathayPaymnetInstallmentWithOtp || result.APIProvider == PaymentAPIProvider.CathayPaymnetOTPGateway)
                            {
                                result.HppUrl = result.HppUrl + string.Format("?TransId={0}&TicketId={1}", paymentDTO.TransactionId, View.TicketId);
                            }
                            paymentResult.Url = result.HppUrl;
                            paymentDTO.State = PponPaymentDTOState.OTP;
                            PponBuyFacade.SetPponPaymentDTOToSession(View.TicketId, paymentDTO);
                            logger.Info("[OTP URL]" + result.HppUrl);
                            return paymentResult;
                        }

                        int riskScore;
                        CreditCardUtility.AutoBlockCreditcardFraudSuspect(
                            paymentDTO.CreditCardInfo, result, paymentDTO.BusinessHourGuid, out riskScore);
                        if (riskScore >= -2)
                        {
                            CreditCardUtility.PushToLineBot(paymentDTO.CreditCardInfo, result, paymentDTO.BusinessHourGuid);
                        }

                        int rlt;
                        PaymentTransaction ptCreditCard;
                        CheckCreditCardAuthStatus creditCardPayStatus = CheckResultAndUpdatePaymentTransaction(transactionId, isMasterPass,
                            result, paymentDTO.CreditCardInfo, userId, paymentDTO.CreditCardInfo.CreditCardGuid, out rlt, out ptCreditCard);

                        if (PponBuyFacade.CheckBlackCreditcardOrderTimesMoreThan3(paymentDTO.CreditCardInfo, result))
                        {
                            PaymentTransaction pt = op.PaymentTransactionGet(paymentDTO.TransactionId, PaymentType.Creditcard, PayTransType.Authorization);
                            CreditCardUtility.AuthenticationReverse(paymentDTO.CreditCardInfo.TransactionId, pt.AuthCode);
                            return MakePaymentFullResult.Create(MakePaymentResult.OutofLimitBlackCreditcardOrderError);
                        }

                        if (creditCardPayStatus == CheckCreditCardAuthStatus.PickupCard)
                        {
                            return MakePaymentFullResult.Create(MakePaymentResult.PickupCard);
                        }

                        if (creditCardPayStatus == CheckCreditCardAuthStatus.AlreadyPaid)
                        {
                            return MakePaymentFullResult.Create(MakePaymentResult.AlreadyPaid);
                        }
                        if (creditCardPayStatus == CheckCreditCardAuthStatus.Success)
                        {
                            MemoryMemberCreditcardNumber(paymentDTO, userId, userName);
                        }
                        if (isMasterPass)
                        {
                            LogMasterPass(result, paymentDTO.MasterPassInfo, paymentDTO.CreditCardInfo, userId, rlt, paymentDTO.OrderGuid);
                        }
                    }
                }

                #endregion 信用卡/ATM/LinePay/TaishinPay/MasterPass 付款
            }
            return MakePaymentFullResult.Create(MakePaymentResult.Success);
        }

        public void MemoryMemberCreditcardNumber(PponPaymentDTO paymentDTO, int userId, string userName)
        {
            //記錄卡號
            if (paymentDTO.IsSaveCreditCardInfo)
            {
                string errorMessage;
                MemberFacade.AddOrUpdateMemberCreditCard(paymentDTO.CreditCardInfo.CreditCardGuid, userName, paymentDTO.CreditCardInfo.CardNumber,
                    paymentDTO.CreditCardInfo.ExpireYear, paymentDTO.CreditCardInfo.ExpireMonth,
                    paymentDTO.CreditCardInfo.CreditCardName, out errorMessage);
            }

            //紀錄最後使用卡片
            if (paymentDTO.CreditCardInfo.CreditCardGuid != Guid.Empty)
            {
                MemberFacade.LastUseCarditCard(paymentDTO.CreditCardInfo.CreditCardGuid, userId);
            }
        }

        public void CompleteOrder(Guid orderGuid, string transactionId, PponPaymentDTO paymentDTO, int userId)
        {
            PponDeliveryInfo deliveryInfo = paymentDTO.DeliveryInfo;
            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(paymentDTO.BusinessHourGuid);
            if (orderGuid == Guid.Empty)
            {
                throw new Exception("系統發生錯誤，訂單未成立");
            }
            Member mem = mp.MemberGet(userId);
            // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
            if (mem.IsLoaded == false)
            {
                View.ShowResult(PaymentResultPageType.CreditcardFailed, new MemberLinkCollection(), paymentDTO, theDeal, 0);
                return;
            }
            string userName = mem.UserName;
            MemberLinkCollection mlc = mp.MemberLinkGetList(userId);

            // 避免使用者在交易過程中改變自己的 email，導致之後在用 email 反查 unique id 時查不到資料填 0
            if (int.Equals(0, userId))
            {
                View.ShowResult(PaymentResultPageType.CreditcardFailed, mlc, paymentDTO, theDeal, 0);
                return;
            }

            PaymentTransactionCollection ptc = op.PaymentTransactionGetListByTransIdAndTransType(transactionId, PayTransType.Authorization);
            PaymentTransaction ptCreditcard = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.Creditcard);
            PaymentTransaction ptScash = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.SCash);
            PaymentTransaction ptPscash = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.Pscash);
            PaymentTransaction ptDiscount = null;
            PaymentTransaction ptAtm = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.ATM);
            PaymentTransaction ptThirdParty = null;
            PaymentTransaction ptFamilyIsp = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.FamilyIsp);
            PaymentTransaction ptSevenIsp = ptc.FirstOrDefault(t => t.PaymentType == (int)PaymentType.SevenIsp);

            Order o = op.OrderGet(orderGuid);

            PaymentTransaction ptThirdPartyCharging = null;
            var thirdPartyPaymentType = PaymentFacade.GetPaymentTypeByThirdPartyPayment(paymentDTO.ThirdPartyPaymentSystem);
            if (thirdPartyPaymentType != null)
            {
                ptThirdParty = ptc.First(t => t.PaymentType == (int)(PaymentType)thirdPartyPaymentType && t.TransType == (int)PayTransType.Authorization);
                ptThirdPartyCharging = op.PaymentTransactionGet(transactionId, (PaymentType)thirdPartyPaymentType, PayTransType.Charging);
            }

            PayTransResponseType theResponseType = PayTransResponseType.OK;

            foreach (var pt in ptc)
            {
                if (pt.Result != (int)PayTransResponseType.OK)
                {
                    theResponseType = PayTransResponseType.GenericError;

                    if (pt.PaymentType == (int)PaymentType.Creditcard)
                    {
                        PaymentFacade.DeleteWhiteList(userId, pt.PaymentType, pt.Result ?? 0);
                    }
                }
            }

            if (theResponseType != PayTransResponseType.OK || theDeal.DealIsOrderable() == false)
            {
                OrderFacade.DeleteItemInCart(paymentDTO.TicketId);
                PponBuyFacade.DeletePponDeliveryInfoAtSession(paymentDTO.TicketId);
                DoRefund(transactionId);

                View.ShowResult(PaymentResultPageType.CreditcardFailed, mlc, paymentDTO, theDeal, 0);
                return;
            }

            #region Transaction Scope

            OrderDetailCollection odCol = op.OrderDetailGetList(1, theDeal.MaxItemCount ?? 10, string.Empty, o.Guid, OrderDetailTypes.Regular);
            int trustCreditCardAmount = ptCreditcard == null ? 0 : (int)ptCreditcard.Amount;
            int atmAmount = ptAtm == null ? 0 : (int)ptAtm.Amount;
            int thirdPartyPayAmount = ptThirdParty == null ? 0 : (int)ptThirdParty.Amount;
            int familyIspAmount = ptFamilyIsp == null ? 0 : (int)ptFamilyIsp.Amount;
            int sevenIspAmount = ptSevenIsp == null ? 0 : (int)ptSevenIsp.Amount;

            #region 檢查付款金額與份數是否相符
            int quantityTotal = (int)odCol.Sum(x => x.ItemQuantity * x.ItemUnitPrice);
            int deliveryCharge = PponBuyFacade.GetDealDeliveryCharge(quantityTotal, theDeal);
            quantityTotal = quantityTotal + deliveryCharge;

            int paymentTotal = trustCreditCardAmount + (int)deliveryInfo.SCash + deliveryInfo.PayEasyCash +
                               deliveryInfo.BonusPoints + deliveryInfo.DiscountAmount + atmAmount + thirdPartyPayAmount +
                               familyIspAmount + sevenIspAmount;

            if (quantityTotal != paymentTotal)
            {
                logger.Error("付款金額與實際金額不符");

                #region LinePay 取消授權/請款

                if (thirdPartyPayAmount > 0 && paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
                {
                    var linePayTrans = op.LinePayTransLogGet(paymentDTO.TicketId);
                    if (linePayTrans.IsCaptureSeparate) //取消授權
                    {
                        LinePayUtility.VoidTrans(linePayTrans);
                    }
                    else //退款
                    {
                        LinePayUtility.BuyFailRefund(linePayTrans, thirdPartyPayAmount, false);
                    }

                    PaymentFacade.UpdateTransaction(transactionId, ptThirdParty.OrderGuid, PaymentType.LinePay, ptThirdParty.Amount
                        , ptThirdParty.AuthCode
                        , PayTransType.Authorization, DateTime.Now
                        , "LinePay 付款金額與份數不符，自動退款/取消授權"
                        , Helper.SetPaymentTransactionPhase(ptThirdParty.Status, PayTransPhase.Failed)
                        , (int)ptThirdParty.Result, PaymentAPIProvider.LinePay);
                }

                #endregion LinePay 取消授權/請款

                #region ApplePay 取消授權

                if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.ApplePay)
                {
                    PaymentFacade.CancelPaymentTransactionByTransId(transactionId, "sys", "Unorderable. ");
                    CreditCardUtility.AuthenticationReverse(transactionId);
                }

                #endregion

                View.ShowResult(
                    (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay ||
                     paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
                        ? PaymentResultPageType.ThirdPartyPayFail
                        : PaymentResultPageType.CreditcardFailed,
                    mlc, paymentDTO, theDeal, 0);
                return;
            }

            #endregion 檢查付款金額與份數是否相符

            #region LinePay 請款 (授權與請款分開的情況下)

            if (thirdPartyPayAmount > 0 && paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay)
            {
                var linePayTrans = op.LinePayTransLogGet(paymentDTO.TicketId);

                if (linePayTrans.IsCaptureSeparate)
                {
                    //授權與請款分開
                    if (LinePayUtility.Capture(linePayTrans, thirdPartyPayAmount))
                    {
                        DepartmentTypes dType = Helper.GetPaymentTransactionDepartmentTypes(ptThirdParty.Status);
                        int status = Helper.SetPaymentTransactionPhase(Helper.SetPaymentTransactionDepartmentTypes(0, dType), PayTransPhase.Successful);
                        PaymentFacade.NewTransaction(ptThirdParty.TransId, ptThirdParty.OrderGuid.Value, PaymentType.LinePay, thirdPartyPayAmount,
                            ptThirdParty.AuthCode, PayTransType.Charging, DateTime.Now, userName, "LinePay 請款", status,
                            PayTransResponseType.OK, (OrderClassification)ptThirdParty.OrderClassification.Value, null
                            , (PaymentAPIProvider)ptThirdParty.ApiProvider.Value);
                    }
                }
            }

            #endregion LinePay 請款 (授權與請款分開的情況下)

            #region TaishinPay 驗證PayCode

            if (thirdPartyPayAmount > 0 && paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
            {
                if (paymentDTO.ThirdPartyParameters != null && string.IsNullOrEmpty(paymentDTO.ThirdPartyParameters.PayCode))
                {
                    string errMsg = string.Format("台新交易碼不存在, transId={0}", ptThirdParty.TransId);
                    logger.Error(errMsg);

                    //TODO 待退貨API實作退款

                    View.ShowResult(PaymentResultPageType.ThirdPartyPayFail, mlc, paymentDTO, theDeal, 0);
                    return;
                }
            }

            #endregion TaishinPay 驗證PayCode

            int quantity = odCol.Sum(x => x.ItemQuantity);
            try
            {
                PponBuyFacade.DeletePponDeliveryInfoAtSession(paymentDTO.TicketId);
                using (var transScope = TransactionScopeBuilder.CreateReadCommitted(TimeSpan.FromSeconds(30)))
                {

                    #region 扣除選項
                    TempSessionCollection tsc = pp.TempSessionGetList("Accessory" + paymentDTO.TicketId);
                    if (tsc.Count > 0)
                    {
                        #region 多規格

                        //多重選項
                        foreach (TempSession ts in tsc)
                        {
                            if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                            {
                                #region 新版提案單

                                Guid option_guid = Guid.Empty;
                                Guid.TryParse(ts.Name, out option_guid);
                                
                                PponOption opt1 = pp.PponOptionGetByGuid(option_guid);
                                int forOut;
                                int.TryParse(ts.ValueX, out forOut);
                                if (opt1 != null && opt1.IsLoaded)
                                {
                                    if (opt1.ItemGuid != null)
                                    {
                                        ProductItem pdi = pp.ProductItemGet(opt1.ItemGuid.Value);
                                        if (pdi != null && pdi.IsLoaded)
                                        {
                                            PponBuyFacade.UpdatePchomeInventory(theDeal, pdi);

                                            int stock = pdi.Stock - forOut;
                                            if (stock >= 0)
                                            {
                                                pdi.Stock = pdi.Stock - forOut;
                                                pdi.Sales = pdi.Sales + forOut;
                                                pp.ProductItemSet(pdi);

                                                PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                            }
                                            else
                                            {
                                                string errMsg = string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName);
                                                logger.Error(errMsg);

                                                PponFacade.UpdateDealMaxItemCount(theDeal, opt1.ItemGuid.Value); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                View.ShowResult(PaymentResultPageType.CreditcardFailed, mlc, paymentDTO, theDeal, 0);
                                                return;
                                            }
                                        }
                                    }
                                }
                                #endregion
                            }
                            else
                            {
                                #region 舊版

                                int forOut;
                                AccessoryGroupMember agm = ip.AccessoryGroupMemberGet(new Guid(ts.Name));
                                if (!agm.IsLoaded)
                                    continue;

                                if (agm.Quantity != null && int.TryParse(ts.ValueX, out forOut))
                                {
                                    agm.Quantity = agm.Quantity - int.Parse(ts.ValueX);

                                    int? optId = PponOption.FindId(agm.Guid, theDeal.BusinessHourGuid);
                                    if (optId.HasValue)
                                    {
                                        PponOption opt = pp.PponOptionGet(optId.Value);
                                        if (opt != null && opt.IsLoaded)
                                        {
                                            opt.Quantity = agm.Quantity;
                                            pp.PponOptionSet(opt);
                                        }
                                    }
                                }

                                ip.AccessoryGroupMemberSet(agm);

                                #endregion
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region 單一規格

                        //無多重選項
                        if (ProposalFacade.IsVbsProposalNewVersion() && theDeal.IsHouseDealNewVersion())
                        {
                            ProposalMultiDeal multiDeal = sp.ProposalMultiDealGetAllByBid(theDeal.BusinessHourGuid);
                            if (multiDeal.IsLoaded)
                            {
                                List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(multiDeal.Options);
                                if (specs == null)
                                {
                                    string errorMessage = string.Format("{0} 沒有設定產品選項，購買失敗。", theDeal.BusinessHourGuid);
                                    logger.Error(errorMessage);
                                    throw new Exception(errorMessage);
                                }
                                if (specs.Count > 0)
                                {
                                    ProposalMultiDealsSpec spc = specs.FirstOrDefault();
                                    if (spc != null)
                                    {
                                        ProductItem pdi = pp.ProductItemGet(spc.Items.FirstOrDefault().item_guid);
                                        if (pdi != null && pdi.IsLoaded)
                                        {
                                            int stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                            if (stock >= 0)
                                            {
                                                pdi.Stock = pdi.Stock - (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                                pdi.Sales = pdi.Sales + (quantity * theDeal.QuantityMultiplier.GetValueOrDefault(1));
                                                pp.ProductItemSet(pdi);

                                                PponFacade.UpdateDealMaxItemCount(theDeal, spc.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                            }
                                            else
                                            {
                                                string errMsg = string.Format("{0} 庫存不足，{1}{2}", pdi.SpecName, theDeal.BusinessHourGuid, theDeal.ItemName);
                                                logger.Error(errMsg);

                                                PponFacade.UpdateDealMaxItemCount(theDeal, spc.Items.FirstOrDefault().item_guid); //將相關檔次的最大購買數量改為0，避免前台繼續銷售
                                                View.ShowResult(PaymentResultPageType.CreditcardFailed, mlc, paymentDTO, theDeal, 0);
                                                return;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        #endregion
                    }

                    pp.TempSessionDelete("Accessory" + paymentDTO.TicketId);

                    #endregion 扣除選項

                    #region 分店銷售數量

                    if (deliveryInfo.StoreGuid != Guid.Empty)
                    {
                        int k = (int)(Math.Ceiling((double)deliveryInfo.Quantity / (theDeal.ComboPackCount ?? 1)));
                        k = (theDeal.SaleMultipleBase ?? 0) > 0 ? theDeal.SaleMultipleBase.Value : k;
                        pp.PponStoreUpdateOrderQuantity(theDeal.BusinessHourGuid, deliveryInfo.StoreGuid, k);
                    }

                    #endregion 分店銷售數量


                    #region 信用卡轉購物金

                    if (ptCreditcard != null)
                    {
                        OrderFacade.ExchangeSCash(transactionId, ptCreditcard, ptScash, o, theDeal, userId, userName);
                    }

                    #endregion 信用卡轉購物金

                    #region ThirdPartyPay 轉購物金

                    if (thirdPartyPayAmount > 0)
                    {
                        OrderFacade.ExchangeSCash(transactionId, ptThirdParty, ptScash, o, theDeal, userId, userName, ptThirdPartyCharging);
                    }

                    #endregion ThirdPartyPay 轉購物金

                    

                    #region make cash trust log

                    #region payments

                    Dictionary<PaymentType, int> payments = new Dictionary<PaymentType, int>();

                    decimal scash = (ptScash != null && ptScash.IsLoaded) ? ptScash.Amount : 0;
                    decimal pscash = (ptPscash != null && ptPscash.IsLoaded) ? ptPscash.Amount : 0;
                    if (scash > 0)
                    {
                        payments.Add(PaymentType.SCash, (int)scash);
                    }
                    if (pscash > 0)
                    {
                        payments.Add(PaymentType.Pscash, (int)pscash);
                    }
                    if (deliveryInfo.BonusPoints > 0)
                    {
                        payments.Add(PaymentType.BonusPoint, deliveryInfo.BonusPoints);
                    }
                    if (trustCreditCardAmount > 0)
                    {
                        payments.Add(PaymentType.Creditcard, trustCreditCardAmount);
                    }
                    if (deliveryInfo.DiscountAmount > 0)
                    {
                        payments.Add(PaymentType.DiscountCode, deliveryInfo.DiscountAmount);
                    }
                    if (atmAmount > 0)
                    {
                        payments.Add(PaymentType.ATM, atmAmount);
                    }
                    if (thirdPartyPayAmount > 0)
                    {
                        var thirdType = PaymentFacade.GetPaymentTypeByThirdPartyPayment(paymentDTO.ThirdPartyPaymentSystem);
                        if (thirdType != null)
                        {
                            payments.Add((PaymentType)thirdType, thirdPartyPayAmount);
                        }
                    }
                    if (familyIspAmount > 0)
                    {
                        payments.Add(PaymentType.FamilyIsp, familyIspAmount);
                    }
                    if (sevenIspAmount > 0)
                    {
                        payments.Add(PaymentType.SevenIsp, sevenIspAmount);
                    }

                    #endregion payments

                    int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;
                    //if weeklypay deal
                    int trustCheckoutType = 0;
                    if (((theDeal.BusinessHourStatus) & (int)BusinessHourStatus.WeeklyPay) > 0)
                        trustCheckoutType = (int)TrustCheckOutType.WeeklyPay;

                    CashTrustInfo cti = new CashTrustInfo
                    {
                        OrderGuid = o.Guid,
                        OrderDetails = odCol,
                        CreditCardAmount = trustCreditCardAmount,
                        SCashAmount = (int)scash,
                        PscashAmount = (int)pscash,
                        BCashAmount = deliveryInfo.BonusPoints,
                        Quantity = saleMultipleBase > 0 ? saleMultipleBase : quantity,
                        DeliveryCharge = deliveryCharge,
                        DeliveryType = theDeal.DeliveryType == null ? DeliveryType.ToShop : (DeliveryType)theDeal.DeliveryType.Value,
                        DiscountAmount = deliveryInfo.DiscountAmount,
                        AtmAmount = atmAmount,
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        CheckoutType = trustCheckoutType,
                        TrustProvider = Helper.GetBusinessHourTrustProvider(theDeal.BusinessHourStatus),
                        ItemName = theDeal.ItemName,
                        ItemPrice = (int)theDeal.ItemPrice,
                        ItemOriPrice = (int)theDeal.ItemOrigPrice,
                        User = mp.MemberGet(userName),
                        Payments = payments,
                        ThirdPartyCashAmount = thirdPartyPayAmount,
                        ThirdPartySystem = paymentDTO.ThirdPartyPaymentSystem,
                        FamilyIspAmount = familyIspAmount,
                        SevenIspAmount = sevenIspAmount,
                        PresentQuantity = theDeal.PresentQuantity ?? 0,
                        IsGroupCoupon = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon),
                        GroupCouponType = (GroupCouponDealType)(theDeal.GroupCouponDealType ?? 0)
                    };

                    CashTrustLogCollection ctCol = OrderFacade.MakeCashTrust(cti);

                    #endregion make cash trust log

                    #region 發票/代收轉付

                    if (deliveryInfo.EntrustSell == EntrustSellType.No)
                    {
                        #region 三聯式發票儲存

                        if (deliveryInfo.InvoiceType == "3")
                        {
                            EinvoiceTriple data = new EinvoiceTriple();
                            if (deliveryInfo.IsInvoiceSave)
                            {

                                int tmpData;
                                if (deliveryInfo.InvoiceSaveId != "0")
                                {
                                    int.TryParse(deliveryInfo.InvoiceSaveId, out tmpData);
                                    data.Id = tmpData;
                                    data.IsNew = false;
                                    data.IsLoaded = true;
                                }
                                data.Title = deliveryInfo.CompanyTitle.Trim();
                                data.VatNumber = int.TryParse(deliveryInfo.UnifiedSerialNumber, out tmpData) ? deliveryInfo.UnifiedSerialNumber : "0";
                                data.UserName = deliveryInfo.InvoiceBuyerName.Trim();
                                data.Adress = deliveryInfo.InvoiceBuyerAddress.Trim();
                                data.Type = int.TryParse(deliveryInfo.InvoiceType, out tmpData) ? tmpData : 0;
                                data.UserId = userId;
                                data.ModificationTime = DateTime.Now;
                                MemberFacade.SaveEinvoiceTriple(data);
                            }
                            else
                            {
                                if (deliveryInfo.InvoiceSaveId != "0")
                                {
                                    int tmpData;
                                    int.TryParse(deliveryInfo.InvoiceSaveId, out tmpData);
                                    MemberFacade.EinvoiceTripleDelete(tmpData);
                                }
                            }

                        }

                        #endregion 三聯式發票儲存

                        #region 新制憑證發票

                        string creditCardTail = string.Empty;
                        if (paymentDTO.CreditCardInfo != null && paymentDTO.CreditCardInfo.IsValid)
                        {
                            //取卡號後四碼
                            creditCardTail = paymentDTO.CreditCardInfo.CardNumber.Substring(12);
                        }

                        if (paymentDTO.DeliveryInfo.PaymentMethod == BuyTransPaymentMethod.ApplePay)
                        {
                            creditCardTail = CreditCardUtility.GetCreditcardOrderCardTail(paymentDTO.OrderGuid);
                        }

                        if (ctCol.Count > 0 && deliveryInfo.DeliveryType == DeliveryType.ToShop)
                        {
                            foreach (var ct in ctCol.OrderBy(x => x.UninvoicedAmount))
                            {
                                int uninvoicedAmount = saleMultipleBase > 0 ? ((ctCol.Count > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0) : ct.UninvoicedAmount;
                                if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm)
                                {
                                    EinvoiceFacade.SetEinvoiceMain(o, deliveryInfo, transactionId, 1, theDeal.ItemName, uninvoicedAmount,
                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                        userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete,
                                        InvoiceVersion.CarrierEra, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                }
                                else if (uninvoicedAmount > 0)
                                {
                                    EinvoiceFacade.SetEinvoiceMain(o, deliveryInfo, transactionId, 1, theDeal.ItemName, uninvoicedAmount,
                                        Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                        userName, false, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.Initial,
                                        InvoiceVersion.CarrierEra, Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon));
                                }
                                if (saleMultipleBase > 0)
                                    break;
                            }
                        }

                        #endregion 新制憑證發票

                        #region 新制宅配發票

                        int uninvoicedTotal = (ctCol.Count > 0) ? ctCol.Sum(x => x.UninvoicedAmount) : 0;
                        if (deliveryInfo.DeliveryType == DeliveryType.ToHouse)
                        if (deliveryInfo.DeliveryType == DeliveryType.ToHouse)
                        {
                            if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm ||
                                deliveryInfo.PaymentMethod == BuyTransPaymentMethod.FamilyIsp ||
                                deliveryInfo.PaymentMethod == BuyTransPaymentMethod.SevenIsp)
                            {
                                EinvoiceFacade.SetEinvoiceMain(o, deliveryInfo, transactionId, quantity, theDeal.ItemName, uninvoicedTotal,
                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                    userName, true, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.NotComplete, InvoiceVersion.CarrierEra);
                            }
                            else if (uninvoicedTotal > 0)
                            {
                                EinvoiceFacade.SetEinvoiceMain(o, deliveryInfo, transactionId, quantity, theDeal.ItemName, uninvoicedTotal,
                                    Helper.IsFlagSet(theDeal.GroupOrderStatus ?? 0, GroupOrderStatus.NoTax),
                                    userName, true, OrderClassification.LkSite, creditCardTail, (int)EinvoiceType.Initial, InvoiceVersion.CarrierEra);
                            }
                        }

                        #endregion 新制宅配發票
                    }
                    else
                    {
                        //代收轉付以收據代替發票。
                        OrderFacade.EntrustSellReceiptSet(deliveryInfo, o, odCol, ctCol,
                            theDeal.ItemName, userName);
                    }

                    #endregion

                    transScope.Complete();
                }
            }
            catch (Exception ex)
            {
                logger.Error(string.Format("Buy Presenter Transaction Scope error: UserName:{0}, SessionId:{1}, Cpa:{2}, OrderGuid:{3}, Exception:{4}"
                    , userName, View.SessionId, View.NewCpa, o.Guid, ex));
                //交易失敗退款
                PponBuyFacade.DeletePponDeliveryInfoAtSession(paymentDTO.TicketId);
                DoRefund(transactionId);
                View.ShowResult(
                    (paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.LinePay ||
                     paymentDTO.ThirdPartyPaymentSystem == ThirdPartyPayment.TaishinPay)
                        ? PaymentResultPageType.ThirdPartyPayFail
                        : PaymentResultPageType.CreditcardFailed,
                    mlc, paymentDTO, theDeal, 0);
                return;
            }

            #endregion Transaction Scope

            #region cpa stuff

            //有cpa的值才寫cpa_order table，並清除ReferrerSourceId cookie
            string cpaKey = "";
            if (!string.IsNullOrEmpty(CookieManager.GetCpaKey()))
            {
                cpaKey = CookieManager.GetCpaKey();
                //存入cpa order
                CpaUtility.RecordCpaOrderByReferrer(cpaKey, o);

                //若為單次會清除ReferrerSourceId cookie
                CpaUtility.RecordOrderByReferrer(View.ReferrerSourceId, o, theDeal,
                    pp.DealPropertyGet(theDeal.BusinessHourGuid));
            }
            else if (!string.IsNullOrEmpty(View.RsrcSession)) //add for BBH share Money , died 2011.05.19
            {
                CpaUtility.RecordOrderByReferrer(View.RsrcSession, o, theDeal,
                    pp.DealPropertyGet(theDeal.BusinessHourGuid));
            }
            #endregion

            #region 銀行照會

            CreditcardOrderCollection oldOrders = op.GetCreditcardOrderByOrderGuid(o.Guid);
            if (oldOrders != null && oldOrders.Count > 0)
            {
                CreditcardOrder orderInfo = oldOrders[0];
                CreditcardResult result;
                if (config.EnableCreditcardRefer)
                {
                    if (orderInfo.Result == 0 || orderInfo.Result == -1)   //0是授權正常，-1是初始值
                    {
                        if (orderInfo.CreditcardAmount >= config.CreditcardReferAmount)
                        {
                            orderInfo.Result = (int)CreditcardResult.OverQuantity;

                            if (PaymentFacade.IsCreditcardRefer(o.Guid))
                            {
                                orderInfo = PaymentFacade.SetCreditcardRefer(orderInfo, o.Guid);
                            }
                        }
                        else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime, 24, 10000))
                        {
                            orderInfo.Result = (int)CreditcardResult.OverQuantityWithin24Hr;

                            if (PaymentFacade.IsCreditcardRefer(o.Guid))
                            {
                                orderInfo.IsReference = true;
                                orderInfo.ReferenceId = "sys";
                                orderInfo.ReferenceTime = DateTime.Now;
                            }
                        }
                        else if (op.IsCreditcardOrderOverAmount(orderInfo.CreditcardInfo, orderInfo.CreditcardAmount, orderInfo.CreateTime, 1,
                            config.CreditcardReferAmount))
                        {
                            orderInfo.Result = (int)CreditcardResult.OverQuantityWithin1Hr;
                        }
                        else if (PaymentFacade.IsSuspectCreditcardFraud(deliveryInfo.DeliveryType, deliveryInfo.ProductDeliveryType, orderInfo.ConsumerIp, o.DeliveryAddress, userId, o.SellerGuid, out result) && !deliveryInfo.IsApplePay)
                        {
                            orderInfo.Result = (int)result;

                            if (PaymentFacade.IsCreditcardRefer(o.Guid))
                            {
                                //宅配
                                orderInfo = PaymentFacade.SetCreditcardRefer(orderInfo, o.Guid);
                            }
                            else if (deliveryInfo.DeliveryType == DeliveryType.ToShop)
                            {
                                //憑證
                                PaymentFacade.PponCreditcardFraud(o.CreateTime, o.CreateId, o.UserId, o.OrderId, theDeal.ItemName, orderInfo.ConsumerIp);
                            }
                        }
                    }
                }
                
                op.SetCreditcardOrder(orderInfo);
            }

            #endregion

            #region 通路商相關

            var agent = ChannelFacade.GetOrderClassificationByHeaderToken();
            if (agent != AgentChannel.NONE)
            {
                int tokenClientId = ChannelFacade.GetOauthClientIdByHeaderToken();
                if (tokenClientId != 0)
                {
                    //避免開發者抓到token而算入帶銷訂單
                    OrderCorresponding oc = new OrderCorresponding()
                    {
                        CreatedTime = DateTime.Now,
                        UserId = o.UserId,
                        Mobile = MemberFacade.GetUserMobile(o.UserId),
                        OrderGuid = o.Guid,
                        RelatedOrderId = o.Guid.ToString(),
                        OrderId = o.OrderId,
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        UniqueId = theDeal.UniqueId.Value,
                        Memo = Helper.GetEnumDescription(agent),
                        Type = (int)agent,
                        TokenClientId = tokenClientId
                    };

                    op.OrderCorrespondingSet(oc);
                }

            }

            #endregion            

            #region New Cpa

            if (config.NewCpaEnable)
            {
                try
                {
                    if (View.NewCpa != null && View.NewCpa.Count > 0)
                    {
                        View.NewCpa = PponFacade.CpaSetDetailToDB(userName, View.SessionId, Helper.GetClientIP(), View.NewCpa, o.Guid);
                    }
                }
                catch (Exception ex)
                {
                    string cpaJson = ProviderFactory.Instance().GetSerializer().Serialize(View.NewCpa);
                    logger.WarnFormat("CpaSetDetailToDB error: UserName:{0}, SessionId:{1}, Cpa:{2}, OrderGuid:{3}, Exception:{4}",
                        userName, View.SessionId, cpaJson, o.Guid, ex);
                }
            }

            #endregion New Cpa
            
            #region iChannel
            if (o.Subtotal > 0 && !string.IsNullOrWhiteSpace(View.CookieIChannelGid))
            {
                OrderFacade.SaveIChannelInfo(o, theDeal.BusinessHourGuid, theDeal.DeliveryType, View.CookieIChannelGid);
            }
            #endregion

            #region shopBack
            if (o.Subtotal > 0 && !string.IsNullOrWhiteSpace(View.CookieShopBackGid))
            {
                OrderFacade.SaveShopBackInfo(o, theDeal, View.CookieShopBackGid);
            }
            #endregion

            #region Payeasy 導購
            if (o.Subtotal > 0 && !string.IsNullOrWhiteSpace(View.CookiePezChannelGid))
            {
                OrderFacade.SavePezChannel(o, theDeal.BusinessHourGuid, theDeal.DeliveryType, View.CookiePezChannelGid);
            }
            #endregion

            #region Line 導購
            if (o.Subtotal > 0 && !string.IsNullOrWhiteSpace(View.CookieLineShopGid))
            {
                IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(theDeal.MainBid ?? theDeal.BusinessHourGuid);
                OrderFacade.SaveLineShopInfo(o, mainDeal, View.CookieLineShopGid);
            }
            #endregion

            #region Affiliates 導購
            if (o.Subtotal > 0 && !string.IsNullOrWhiteSpace(View.CookieAffiliatesGid))
            {
                OrderFacade.SaveAffiliatesInfo(o, theDeal.BusinessHourGuid, theDeal.DeliveryType, View.CookieAffiliatesGid);
            }
            #endregion

            // save delivery info
            string invoiceAddress = string.Empty;
            if (!string.IsNullOrEmpty(deliveryInfo.InvoiceBuyerAddress))
            {
                var invoiceAddressArray = deliveryInfo.InvoiceBuyerAddress.Split('@');
                if (invoiceAddressArray.Count() > 1)
                {
                    invoiceAddress = invoiceAddressArray[1];
                }
                else
                {
                    invoiceAddress = deliveryInfo.InvoiceBuyerAddress;
                }
            }
            else
            {
                invoiceAddress = deliveryInfo.InvoiceBuyerAddress;
            }
            PponBuyFacade.SaveDeliveryInfo(userId, o, paymentDTO.BuyerAddressInfo, theDeal.DeliveryType,
                deliveryInfo.CarrierType, deliveryInfo.CarrierId, deliveryInfo.InvoiceBuyerName, invoiceAddress);


            // set deal close time
            OrderFacade.PponSetCloseTimeIfJustReachMinimum(theDeal, quantity);

            #region 廠商序號(Peztemp)成套票券

            //全家成套票券(咖啡寄杯)
            if ((theDeal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 &&
                Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;
                if (!FamiGroupCoupon.MakeFamiportPincodeTrans(theDeal, o, saleMultipleBase, userId))
                {
                    var pinTrans = FamiGroupCoupon.GetFamilyNetPincodeTrans(o.Guid);
                    if (pinTrans != null)
                    {
                        logger.ErrorFormat(
                            "全家寄杯取Pin失敗 familyEvent:{0}, order:{1},replyCode:{2}, desc:{3}, transId:{4}",
                            pinTrans.FamilyNetEventId, pinTrans.OrderGuid, pinTrans.ReplyCode,
                            pinTrans.ReplyDesc, pinTrans.Id);
                    }
                    else
                    {
                        logger.Error("全家寄杯取Pin失敗, 請查看 server log 檔");
                    }

                    View.ShowResult(PaymentResultPageType.FamilyNetGetPincodeFail, mlc, paymentDTO, theDeal, 0);
                    return;
                }
            }

            #endregion

            //產生憑證號碼，含產生全家Peztemp
            //TODO 將送簡訊跟產憑證拆開，憑證移進 transaction。
            CouponFacade.GenCouponWithSmsWithGeneratePeztemp(o, theDeal, deliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm);

            //萊爾富咖啡寄杯
            if (config.EnableHiLifeDealSetup && (theDeal.GroupOrderStatus & (int)GroupOrderStatus.HiLifeDeal) > 0 &&
                Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                (theDeal.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                int saleMultipleBase = Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.GroupCoupon) ? theDeal.SaleMultipleBase ?? 0 : 0;
                if (!HiLifeFacade.MakeHiLifePincode(o, saleMultipleBase, userId))
                {
                    logger.Error("萊爾富寄杯insert pincode table失敗, 請查log");
                    View.ShowResult(PaymentResultPageType.HiLifeGetPincodeFail, mlc, paymentDTO, theDeal, 0);
                    return;
                }
            }

            //寄付款成功信
            #region send deal-on mail

            if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.Atm)
            {
                PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.BuyerUserId, o.Guid, PaymentType.ATM);
                //加註訂單為ATM付款
                OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.ATMOrder, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    }
                );
                var userEmail = MemberFacade.GetUserEmail(userName);
                
                OrderFacade.SendATMOrderNotifyMail(mem.DisplayName, userEmail, theDeal.ItemName,
                    paymentDTO.ATMAccount, atmAmount.ToString(), theDeal, o, quantity);
            }
            else if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.FamilyIsp)
            {
                PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.BuyerUserId, o.Guid, PaymentType.FamilyIsp);
                OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.FamilyIspOrder, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    }
                );
                OrderFacade.SendPponMail(MailContentType.Authorized, o.Guid);
            }
            else if (deliveryInfo.PaymentMethod == BuyTransPaymentMethod.SevenIsp)
            {
                PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.BuyerUserId, o.Guid, PaymentType.SevenIsp);
                OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.SevenIspOrder, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    }
                );
                OrderFacade.SendPponMail(MailContentType.Authorized, o.Guid);
            }
            else
            {
                PayEvents.OnOrderCreated(theDeal.BusinessHourGuid, paymentDTO.DeliveryInfo.BuyerUserId, o.Guid, PaymentType.Creditcard);
                OrderFacade.SetOrderStatus(o.Guid, userName, OrderStatus.Complete, true,
                    new SalesInfoArg
                    {
                        BusinessHourGuid = theDeal.BusinessHourGuid,
                        Quantity = odCol.OrderedQuantity,
                        Total = odCol.OrderedTotal
                    });
                OrderFacade.SendPponMail(MailContentType.Authorized, o.Guid);
            }

            #endregion

            // 只要他一日為訪客會員，就會不斷的寄發此會員認證信
            if (mem.IsGuest)
            {
                MemberUtility.SendAccountConfirmMail(mem.UserName);
            }

            #region 推薦送折價券

            //有推薦人資料，發放推薦折價券
            if (!string.IsNullOrWhiteSpace(View.CookieReferenceId))
            {
                PromotionFacade.ReferenceDiscountCheckAndPay(View.CookieReferenceId, o, theDeal, userName);
            }

            #endregion 推薦送折價券

            #region 首購送折價券

            PromotionFacade.FirstBuyDiscountCheckAndPay(o, theDeal);

            #endregion 首購送折價券            

            #region Retargeting
            try
            {
                var ttmodel = new TrackTagInputModel
                {
                    ViewPponDealData = theDeal,
                    Config = config,
                    GtagPage = GtagPageType.Purchase,
                    YahooEa = YahooEaType.Purchase,
                    OrderGuid = o.Guid,
                    OrderTotalAmountWithoutDiscount = paymentTotal - deliveryInfo.DiscountAmount,
                    ScupioEventCategory = ScupioEventCategory.Purchase,
                    OrderDetails = odCol.ToList(),
                };

                if (!string.IsNullOrWhiteSpace(View.ReferrerSourceId))
                {
                    View.OrderArgs = RetargetingUtility.GetOrderArgs(ttmodel, theDeal, o, cpaKey, View.ReferrerSourceId);
                }
                else if (!string.IsNullOrEmpty(View.RsrcSession))
                {
                    View.OrderArgs = RetargetingUtility.GetOrderArgs(ttmodel, theDeal, o, cpaKey);
                }

                View.GtagDataJson = TagManager.CreateFatory(ttmodel, TagProvider.GoogleGtag).GetJson();
                View.GtagConversionJson = TagManager.CreateFatory(ttmodel, TagProvider.GoogleGtagConversion).GetJson();
                View.UrADGtagConversionJson = TagManager.CreateFatory(ttmodel, TagProvider.UrADGoogleGtagConversion).GetJson();

                SaveADSLog(ttmodel);
                View.YahooDataJson = TagManager.CreateFatory(ttmodel, TagProvider.Yahoo).GetJson();
            }
            catch (Exception ex)
            {
                logger.InfoFormat("Retargeting Exception -> {0}. {1}", ex.Message, ex.StackTrace);
            }
            #endregion Retargeting

            #region 建立pchome訂單
            if (PponBuyFacade.AddPchomeOrder(theDeal, o.Guid, o) == false)
            {
                OrderFacade.DeleteItemInCart(paymentDTO.TicketId);
                PponBuyFacade.DeletePponDeliveryInfoAtSession(paymentDTO.TicketId);
                //測試購物金退不回，另有job [DataIntegrityMonitor] 會處理
                DoRefundByPc(transactionId);

                View.ShowResult(PaymentResultPageType.CreditcardFailed, mlc, paymentDTO, theDeal, 0);
                return;
            }
            #endregion 建立pchome訂單

            PaymentResultPageType paymentResult = (thirdPartyPayAmount > 0) ? PaymentResultPageType.ThirdPartyPaySuccess : PaymentResultPageType.CreditcardSuccess;
            View.ShowResult(paymentResult, mlc, paymentDTO, theDeal, atmAmount, null, o);
        }

        private void SaveADSLog(TrackTagInputModel ttmodel)
        {
            if (!string.IsNullOrEmpty(View.GtagConversionJson))
            {
                pponEntity.SaveADSLog(new AdsLog()
                {
                    ItemName = ttmodel.ViewPponDealData.ItemName,
                    TotalAmount = ttmodel.OrderTotalAmountWithoutDiscount,
                    SendContent = View.GtagConversionJson,
                    CreateTime = DateTime.Now,
                });
            }
        }

        public CheckCreditCardAuthStatus CheckResultAndUpdatePaymentTransaction(string transactionId, bool isMasterPass,
            CreditCardAuthResult result, PponCreditCardInfo ccao, int userId, Guid creditCardGuid, out int rlt, out PaymentTransaction pt)
        {
            pt = op.PaymentTransactionGet(transactionId, PaymentType.Creditcard, PayTransType.Authorization);
            int status = Helper.SetPaymentTransactionPhase(pt.Status, result.TransPhase);
            rlt = 0;
            try
            {
                rlt = int.Parse(result.ReturnCode);
            }
            catch
            {
                rlt = -9527;
            }

            if (Helper.GetPaymentTransactionPhase(pt.Status) != PayTransPhase.Created)
            {
                logger.Info("trans_id: " + pt.TransId + " 被拒絕覆蓋已回覆的授權紀錄\n result: " + rlt + "\n message: " + result.TransMessage
                            + "\n api_provider: " + (int)result.APIProvider + "\n original_apiprovider: " + pt.ApiProvider + "\n status:" + pt.Status);
                if (Helper.GetPaymentTransactionPhase(pt.Status) == PayTransPhase.Successful)
                {
                    return CheckCreditCardAuthStatus.AlreadyPaid;
                }
                else
                {
                    return CheckCreditCardAuthStatus.AlreadyFail;
                }
            }

            int? bankId = null;
            if (result.InstallmentPeriod > 0)
            {
                bankId = CreditCardPremiumManager.GetBankIdByCardNumber(result.CardNumber);
                if (bankId == null)
                {
                    logger.Error("此筆交易分期,但付款卡號卻在資料庫查不到對映銀行, 前6碼=" + result.CardNumber.Substring(0, 6) + ", transId" +
                                 pt.TransId);
                }
            }

            PaymentFacade.UpdateTransaction(transactionId, pt.OrderGuid, PaymentType.Creditcard, ccao.Amount,
                result.AuthenticationCode, result.TransType, result.OrderDate, result.TransMessage + " " + result.CardNumber,
                status, rlt, result.APIProvider, bankId, result.InstallmentPeriod);

            if (rlt == 0)
            {
                return CheckCreditCardAuthStatus.Success;
            }
            if (rlt == (int)CreditcardResult.PickupCard)
            {
                return CheckCreditCardAuthStatus.PickupCard;
            }
            return CheckCreditCardAuthStatus.Fail;
        }

        private void LogMasterPass(CreditCardAuthResult result, CreditCardInfo cardInfo, PponCreditCardInfo ccao, int userId, int rlt,
            Guid orderGuid)
        {
            if (cardInfo == null)
            {
                throw new Exception("MasterPass data not found.");
            }
            MasterPass mpass = new MasterPass();
            try
            {
                mpass.MasterPassTransLog(cardInfo.MasterPassTransId, cardInfo.PayPassWalletIndicator, ccao.Amount, result.AuthenticationCode
                    , rlt, orderGuid, userId, cardInfo.OauthToken, cardInfo.CardBrandId
                    , cardInfo.CardBrandName, cardInfo.CardHolder, ccao.CardNumber);
            }
            catch (Exception error)
            {
                string errormessage = string.Format("masterpass log error:{0},masterpasstransid:{1},authenticationcode:{2},resultcode:{3}orderguid:{4}"
                    , error.Message, cardInfo.MasterPassTransId, result.AuthenticationCode, rlt, orderGuid);
                mpass.MasterPassTransErrorLog(orderGuid, userId, errormessage);
                logger.Error(errormessage);
            }
        }

        /// <summary>
        /// 進行退款作業
        /// </summary>
        /// <param name="transId"></param>
        private void DoRefund(string transId)
        {
            PaymentTransaction pt = op.PaymentTransactionGetListByTransIdAndTransType(transId, PayTransType.Authorization).FirstOrDefault();
            if (pt == null)
            {
                throw new Exception("DoRefund 找不到 Order, transId=" + transId);
            }
            Guid oid = pt.OrderGuid.GetValueOrDefault();
            if (oid == Guid.Empty)
            {
                throw new Exception("DoRefund 找到 Order 但 transId  未設定 OrderGuid, transId=" + transId);
            }
            Order o = op.OrderGet(oid);
            PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys"); //自動退貨
        }

        private void DoRefundByPc(string transId)
        {
            PaymentTransactionCollection pts = op.PaymentTransactionGetListByTransIdAndTransType(transId, PayTransType.Authorization);
            if (pts.Count == 0)
            {
                throw new Exception("DoRefund 找不到 Order, transId=" + transId);
            }
            Order o = null;
            foreach (PaymentTransaction pt in pts)
            {
                Guid oid = pt.OrderGuid.GetValueOrDefault();
                o = op.OrderGet(oid);
                if(o != null && o.IsLoaded)
                {
                    break;
                }
            }
            if (o != null)
            {
                PaymentFacade.CancelPaymentTransactionByOrderForIncompleteOrder(o, "sys"); //自動退貨
            }                
        }
    }

    public class TaishinPayModel
    {
        public string PayCode { get; set; }
        public string TahshinOrderId { get; set; }
    }
}
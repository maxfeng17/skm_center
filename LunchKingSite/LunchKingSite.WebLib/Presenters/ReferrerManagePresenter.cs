﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.WebLib.Presenters
{
    public class ReferrerManagePresenter : Presenter<IReferrerManageView>
    {
        private static CpaService _cpaServ;
        private static IMarketingProvider marketingProv;
        private static IPponProvider pp;
                
        public ReferrerManagePresenter(CpaService cpaSrv, IMarketingProvider marketing)
        {
            _cpaServ = cpaSrv;
            marketingProv = marketing;
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        public override bool OnViewInitialized()
        {
            
            if (View.ReferrerId == Guid.Empty)
            {
                //1-點了 Referrer 名稱進入 Campaign 列表
                View.SetReferrerList(marketingProv.MemberReferralGetList().Where(t=>t.IsDeleted == false).OrderByDescending(t=>t.CreateTime).ToList());
            }
            else
            {
                var referrer = marketingProv.MemberReferralGet(View.ReferrerId);
                if (!referrer.IsLoaded)
                {
                    //0-顯示初始進入CPA管理頁 Referrer 列表
                    View.ShowReferrerListing();
                    return false;
                }
                List<ReferralCampaign> campaigns = marketingProv.ReferralCampaignGetListByReferralGuid(View.ReferrerId)
                    .Where(t=>t.IsDeleted == false).ToList();
                if (string.IsNullOrWhiteSpace(View.CampaignCode))
                {
                    View.SetReferrerDetail(referrer, campaigns);
                    return true;
                }
                var campaignSel = campaigns.Where(x => x.Code == View.CampaignCode).ToList();

                if (campaignSel.Count == 0)
                {
                    View.SetReferrerDetail(referrer, campaigns);
                    return false;
                }
                var campaign = campaignSel.First();

                GetCampaignDetail(referrer, campaign);

                return true;
            }
            return base.OnViewInitialized();
        }

        public override bool OnViewLoaded()
        {
            View.AddReferrer += AddReferrer;
            View.AddCampaign += AddCampaign;
            View.ExportToExcel += ExportToExcel;
            View.QueryCampaignRecord += View_QueryCampaignRecord;
            return base.OnViewLoaded();
        }

        private void View_QueryCampaignRecord(object sender, DataEventArgs<Tuple<Guid, string>> e)
        {
            MemberReferral referrer = marketingProv.MemberReferralGet(View.ReferrerId);
            if (!referrer.IsLoaded)
            {
                //0-顯示初始進入CPA管理頁 Referrer 列表
                View.ShowReferrerListing();
                return;
            }
            var campaigns = marketingProv.ReferralCampaignGetListByReferralGuid(View.ReferrerId).Where(t=>t.IsDeleted == false).ToList();
            if (string.IsNullOrWhiteSpace(View.CampaignCode))
            {
                View.SetReferrerDetail(referrer, campaigns);
                return;
            }
            var campaignSel = campaigns.Where(x => x.Code == View.CampaignCode).ToList();

            if (campaignSel.Count == 0)
            {
                View.SetReferrerDetail(referrer, campaigns);
                return;
            }
            var campaign = campaignSel.First();

            GetCampaignDetail(referrer, campaign, false);
        }

        private void ExportToExcel(object sender, DataEventArgs<Tuple<Guid, string>> e)
        {
            MemberReferral referrer = marketingProv.MemberReferralGet(View.ReferrerId);
            if (!referrer.IsLoaded)
            {
                //0-顯示初始進入CPA管理頁 Referrer 列表
                View.ShowReferrerListing();
                return;
            }
            var campaigns = marketingProv.ReferralCampaignGetListByReferralGuid(View.ReferrerId).Where(t=>t.IsDeleted == false).ToList();
            if (string.IsNullOrWhiteSpace(View.CampaignCode))
            {
                View.SetReferrerDetail(referrer, campaigns);
                return;
            }
            var campaign = campaigns.FirstOrDefault(x => x.Code == View.CampaignCode);

            if (campaign == null)
            {
                View.SetReferrerDetail(referrer, campaigns);
                return;
            }

            GetCampaignDetail(referrer, campaign, true);
        }

        /// <summary>
        /// 以時間區間查詢CampaignCode記錄
        /// </summary>
        /// <param name="referrer"></param>
        /// <param name="campaign"></param>
        /// <param name="exportToExcel"></param>
        private void GetCampaignDetail(MemberReferral referrer, ReferralCampaign campaign, bool exportToExcel = false)
        {
            if (!referrer.IsLoaded || !campaign.IsLoaded)
            {
                View.ShowReferrerListing();
                return;
            }
            DateTime startTime, endTime;
            string srcId = _cpaServ.ComposeReferenceCode(referrer.Code, campaign.Code);

            if (DateTime.TryParse(View.QueryStartTime, out startTime) == false)
            {
                startTime = DateTime.Now.AddDays(-45);
            }
            if (DateTime.TryParse(View.QueryEndTime, out endTime) == false)
            {
                endTime = DateTime.Now;
            }
            endTime = endTime.Date.AddDays(1).AddSeconds(-1);

            var odDic = pp.OrderDiscountListGetByReferenceIdAndTime(srcId, startTime, endTime)
                .ToDictionary(x => x.OrderId);
            ReferrerActionType? actionType = null;
            int actionTypeTemp;
            if (int.TryParse(View.ActionType, out actionTypeTemp))
            {
                actionType = (ReferrerActionType)actionTypeTemp;
            }
            List<ReferralAction> actions = _cpaServ.GetActionsByReferenceSourceAndTime(
                actionType, srcId, startTime, endTime);

            var items = ProcessReferralActions(actions, odDic, true);
            View.SetCampaignDetail(referrer, campaign, items , exportToExcel);
        }

        /// <summary>
        /// 處理 ReferralAction 折扣後金額
        /// </summary>
        /// <param name="actions"></param>
        /// <param name="odDic"></param>
        /// <param name="isOrder"></param>
        /// <returns></returns>
        private static List<ReferralAction> ProcessReferralActions(List<ReferralAction> actions, 
            Dictionary<string, OrderDiscount> odDic, bool isOrder = false)
        {
            var newActions = actions.Select(x =>
            {
                string total = string.Empty;
                if (isOrder)
                {
                    total = x.Total;
                    var od = odDic.Keys.Contains(x.ReferenceId) ? odDic[x.ReferenceId] : null;
                    if (od != null)
                    {
                        total = od.AfterDiscountAmount.ToString("#.0000");
                    }
                }

                return new ReferralAction
                {
                    ReferenceId = x.ReferenceId,
                    Department = x.Department,
                    City = x.City,
                    ItemName = x.ItemName,
                    Price = x.Price,
                    SubTotal = x.SubTotal,
                    Total = total, //折扣後金額
                    Remark = x.Remark,
                    CreateTime = x.CreateTime,
                    ActionType = x.ActionType,
                    DealType = x.DealType,
                    AccBusinessGroupId = x.AccBusinessGroupId
                };
            }).ToList();
            return newActions;
        }

        private void SaveReferrer(bool isAdd, string code, string name)
        {
            if (string.IsNullOrWhiteSpace(code) || string.IsNullOrWhiteSpace(name))
            {
                return;
            }

            //Referrer data = _cpaServ.GetReferrerByCode(code);

            MemberReferral data = marketingProv.MemberReferralGetList().FirstOrDefault(x => x.Code.ToLower() == code.ToLower());

            #region MemberReferral的處理

            var referral = new MemberReferral();
            if (data != null)
            {
                //referral = marketingProv.MemberReferralGet(new Guid(data.LkGuid));
                referral = data;
                referral.ModifyId = View.UserName;
                referral.ModifyTime = DateTime.Now;
            }
            else
            {
                referral.CreateId = View.UserName;
                referral.CreateTime = DateTime.Now;
            }
            referral.Code = code;
            referral.Name = name;
            marketingProv.MemberReferralSet(referral);

            #endregion MemberReferral的處理

            // if it's add mode, we need data to be null (can't find the record) in order to be proceeded
            // if it's edit mode, we need to find the record to proceed
            // that's why we use a xor here
            if (isAdd ^ (data == null))
            {
                return;
            }
            else
            {
                View.ShowReferrerDetail(referral.Guid);
            }
        }

        private void SaveCampaign(bool isAdd, ReferralCampaign data, Guid referrerId)
        {
            if (string.IsNullOrWhiteSpace(data.Name) || string.IsNullOrEmpty(data.Code))
            {
                return;
            }

            var referrA = marketingProv.MemberReferralGet(referrerId);
            if (!referrA.IsLoaded)
            {
                return;
            }

            #region LK部分的處理

            var campaign = marketingProv.ReferralCampaignGetByReferralGuidAndCode(referrA.Guid, data.Code);

            //有查到資料
            if (campaign.IsLoaded)
            {
                campaign.IsDeleted = false;
                campaign.ModifyId = View.UserName;
                campaign.ModifyTime = DateTime.Now;
            }
            else
            {
                campaign.ReferrerGuid = referrA.Guid;
                campaign.CreateId = View.UserName;
                campaign.CreateTime = DateTime.Now;
            }

            campaign.Code = data.Code.TrimStrict();
            campaign.Rsrc = string.Format("{0}_{1}", referrA.Code, campaign.Code);
            campaign.Name = data.Name.TrimStrict();
            campaign.SessionDuration = data.SessionDuration;
            campaign.OneShotAction = data.OneShotAction;
            campaign.IsExternal = data.IsExternal;
            marketingProv.ReferralCampaignSet(campaign);

            #endregion LK部分的處理

            if (isAdd)
            {
                View.ShowReferrerDetail(referrA.Guid);
            }
            else
            {
                View.ShowCampaign(referrA.Guid, data.Code);
            }
        }

        #region handlers

        private void AddReferrer(object sender, DataEventArgs<KeyValuePair<string, string>> e)
        {
            SaveReferrer(true, e.Data.Key, e.Data.Value);
        }

        private void AddCampaign(object sender, DataEventArgs<ReferralCampaign> e)
        {
            SaveCampaign(true, e.Data, View.ReferrerId);
        }

        #endregion handlers
    }
}
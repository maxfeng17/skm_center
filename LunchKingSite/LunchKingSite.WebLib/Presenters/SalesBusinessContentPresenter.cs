﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;
using System.Transactions;
using LunchKingSite.BizLogic.Model;
using System.Net.Mail;
using System.Web;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesBusinessContentPresenter : Presenter<ISalesBusinessContentView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp;
        private IPponProvider pp;
        private IOrderProvider op;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private ISystemProvider sysmp;
        private IItemProvider imp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.BusinessHourGuid);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.Save += OnSave;
            View.ReCreateProposal += OnReCreateProposal;
            View.PponStoreSave += OnPponStoreSave;
            View.VbsSave += OnVbsSave;
            View.SettingCheck += OnSettingCheck;
            View.Referral += OnReferral;
            View.ChangeLog += OnChangeLog;
            return true;
        }

        public SalesBusinessContentPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IOrderProvider odrProv, IMemberProvider memProv, ISystemProvider sysmProv, IPponProvider pponProv, IItemProvider itemProv)
        {
            sp = sellerProv;
            pp = pponProv;
            hp = humProv;
            op = odrProv;
            mp = memProv;
            sysmp = sysmProv;
            imp = itemProv;
        }

        #region event

        protected void OnSave(object sender, EventArgs e)
        {
            SalesBusinessModel model = new SalesBusinessModel();

            model.Business = pp.BusinessHourGet(View.BusinessHourGuid);
            BusinessHour oriBusiness = model.Business.Clone();
            Proposal pro = ProposalFacade.ProposalGet(View.BusinessHourGuid);

            CategoryDealCollection categories = pp.CategoryDealsGetList(View.BusinessHourGuid);
            CategoryDealCollection oricategories = categories.Clone();

            if (model.Business.IsLoaded)
            {
                #region get model data

                model.SellerProperty = sp.SellerGetByBid(View.BusinessHourGuid);
                Seller oriSeller = model.SellerProperty.Clone();

                model.Group = op.GroupOrderGetByBid(View.BusinessHourGuid);
                GroupOrder oriGroup = model.Group.Clone();

                model.ItemProperty = imp.ItemGetByBid(View.BusinessHourGuid);
                Item oriItem = model.ItemProperty.Clone();

                model.Property = pp.DealPropertyGet(View.BusinessHourGuid);
                DealProperty oriProperty = model.Property.Clone();

                model.Accounting = pp.DealAccountingGet(View.BusinessHourGuid);
                DealAccounting oriAccounting = model.Accounting.Clone();

                model.Costs = pp.DealCostGetList(View.BusinessHourGuid);
                DealCostCollection oriCosts = model.Costs.Clone();

                model.Freight = pp.CouponFreightGetList(View.BusinessHourGuid);
                CouponFreightCollection oriFreight = model.Freight.Clone();

                model.ContentProperty = pp.CouponEventContentGetByBid(View.BusinessHourGuid);
                CouponEventContent oriCec = model.ContentProperty.Clone();
                model.ContentProperty.Description = HttpUtility.HtmlDecode(View.ContentDescription);
                model.ContentProperty.Remark =PponDealHelper.PutDealContentRemark(
                    model.ContentProperty.Remark,
                    HttpUtility.HtmlDecode(View.ContentRemark));

                model.Pro = pro;
                Proposal oriPro = model.Pro.Clone();

                model = View.GetSalesBusinessModelData(model);

                AccBusinessGroupCollection abgc = sysmp.AccBusinessGroupGetList();
                AccBusinessGroup travelAbg = abgc.Where(x => x.AccBusinessGroupName.Equals("旅遊", StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (travelAbg.AccBusinessGroupId == model.Property.DealAccBusinessGroupId.GetValueOrDefault(0))
                {
                    //旅遊主動勾起鎖定
                    model.Property.IsReserveLock = true;
                }
                else
                {
                    model.Property.IsReserveLock = false;
                }

                #endregion

                #region check data

                #region 續接檢查

                int ancQuantity;
                string msg = PponFacade.CheckAncestorBusinessHourGuid(View.BusinessHourGuid, model.Property.AncestorBusinessHourGuid, model.Property.AncestorSequenceBusinessHourGuid, out ancQuantity);
                if (!string.IsNullOrWhiteSpace(msg))
                {
                    View.ShowMessage(msg, BusinessContentMode.DataError);
                    return;
                }
                else if (ancQuantity != 0)
                {
                    model.Property.ContinuedQuantity = ancQuantity;
                }
                #endregion

                #endregion

                #region save

                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    model.Business.ModifyId = model.ItemProperty.ModifyId = model.Property.ModifyId = View.UserName;
                    model.Business.ModifyTime = model.ItemProperty.ModifyTime = model.Property.ModifyTime = DateTime.Now;

                    // BusinessHour
                    sp.BusinessHourSet(model.Business);

                    // GroupOrder
                    op.GroupOrderSet(model.Group);

                    // Item
                    imp.ItemSet(model.ItemProperty);

                    // DealProperty
                    pp.DealPropertySet(model.Property);

                    // DealAccounting
                    pp.DealAccountingSet(model.Accounting);

                    // Pro
                    ProposalFacade.ProposalSet(model.Pro);

                    // DealCost
                    pp.DealCostDeleteListByBid(View.BusinessHourGuid);
                    pp.DealCostSetList(model.Costs);

                    //CouponEventContent
                    pp.CouponEventContentSet(model.ContentProperty);

                    //CategoryDeal
                    categories = View.GetCategoryDeals();
                    //宅配
                    if (model.Property.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        //快速出貨
                        if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //宅配
                            bool flag88 = false;
                            foreach (CategoryDeal c in categories)
                            {
                                if (c.Cid == 88)
                                {
                                    flag88 = true;
                                }
                            }
                            if (!flag88)
                            {
                                categories.Add(new CategoryDeal
                                {
                                    Bid = View.BusinessHourGuid,
                                    Cid = 88
                                });
                            }
                            //24H出貨
                            bool flag139 = false;
                            foreach (CategoryDeal c in categories)
                            {
                                if (c.Cid == 139)
                                {
                                    flag139 = true;
                                }
                            }
                            if (!flag139)
                            {
                                categories.Add(new CategoryDeal
                                {
                                    Bid = View.BusinessHourGuid,
                                    Cid = 139
                                });
                            }
                            //快速到貨                            
                            bool flag221 = false;
                            foreach (CategoryDeal c in categories)
                            {
                                if (c.Cid == 221)
                                {
                                    flag221 = true;
                                }
                            }
                            if (!flag221)
                            {
                                categories.Add(new CategoryDeal
                                {
                                    Bid = View.BusinessHourGuid,
                                    Cid = 221
                                });
                            }
                        }
                    }

                    if (pro.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        //即買即用
                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.OnlineUse))
                        {
                            CategoryCollection category = sp.CategoryGetAll(Category.Columns.Code);
                            Category cc = category.Where(x => x.Name == "即買即用" && x.Type == (int)CategoryType.DealSpecialCategory).FirstOrDefault();

                            if (cc != null)
                            {
                                if (categories.Where(t => t.Cid == cc.Id).FirstOrDefault() == null)
                                {
                                    categories.Add(new CategoryDeal()
                                    {
                                        Bid = View.BusinessHourGuid,
                                        Cid = cc.Id
                                    });
                                }
                            }
                        }
                    }

                    pp.DeleteCategoryDealsByCategoryType(View.BusinessHourGuid, CategoryType.PponChannel);
                    pp.DeleteCategoryDealsByCategoryType(View.BusinessHourGuid, CategoryType.PponChannelArea);
                    pp.DeleteCategoryDealsByCategoryType(View.BusinessHourGuid, CategoryType.DealCategory);
                    pp.DeleteCategoryDealsByCategoryType(View.BusinessHourGuid, CategoryType.DealSpecialCategory);
                    pp.CategoryDealSetList(categories);

                    scope.Complete();
                }

                // CouponFreight
                CouponFreightSave(model.Freight);


                #endregion

                #region 檢查是否有子檔，須一起同步
                ViewComboDealCollection vcds = pp.GetViewComboDealAllByBid(View.BusinessHourGuid);
                if (vcds.Count > 0)
                {
                    DealProperty mdp = pp.DealPropertyGet(View.BusinessHourGuid);
                    DealAccounting mda = pp.DealAccountingGet(View.BusinessHourGuid);
                    BusinessHour mbh = pp.BusinessHourGet(View.BusinessHourGuid);
                    CategoryDealCollection mcdc = pp.CategoryDealsGetList(View.BusinessHourGuid);
                    GroupOrder mgo = op.GroupOrderGetByBid(View.BusinessHourGuid);

                    foreach (ViewComboDeal vcd in vcds)
                    {
                        if (vcd.BusinessHourGuid == View.BusinessHourGuid)
                        {
                            continue;
                        }
                        CategoryDealCollection cds = new CategoryDealCollection();
                        DealProperty dp = pp.DealPropertyGet(vcd.BusinessHourGuid);
                        DealAccounting da = pp.DealAccountingGet(vcd.BusinessHourGuid);
                        BusinessHour bh = pp.BusinessHourGet(vcd.BusinessHourGuid);
                        GroupOrder go = op.GroupOrderGetByBid(vcd.BusinessHourGuid);

                        #region CategoryDeal
                        //上檔頻道
                        foreach (CategoryDeal dc in mcdc)
                        {
                            cds.Add(new CategoryDeal() { Bid = vcd.BusinessHourGuid, Cid = dc.Cid });
                        }
                        if (cds != null)
                        {
                            PponFacade.SaveCategoryDeals(cds, vcd.BusinessHourGuid);
                        }
                        #endregion


                        #region Business_Hour
                        //搜尋關鍵字
                        bh.PicAlt = mbh.PicAlt;
                        pp.BusinessHourSet(bh);
                        #endregion

                        #region Deal_Property
                        //銷售分析
                        dp.DealAccBusinessGroupId = mdp.DealAccBusinessGroupId;
                        //成套app style
                        if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                        {
                            Dictionary<int, string> stylelist = new Dictionary<int, string>();
                            stylelist.Add(CategoryManager.Default.PponDeal.CategoryId, CategoryManager.Default.PponDeal.CategoryName);
                            stylelist.Add(CategoryManager.Default.Delivery.CategoryId, CategoryManager.Default.Delivery.CategoryName);
                            stylelist.Add(CategoryManager.Default.Travel.CategoryId, CategoryManager.Default.Travel.CategoryName);
                            stylelist.Add(CategoryManager.Default.Beauty.CategoryId, CategoryManager.Default.Beauty.CategoryName);
                            stylelist.Add(CategoryManager.Default.Family.CategoryId, CategoryManager.Default.Family.CategoryName);
                            var accName = sysmp.AccBusinessGroupGet(mdp.DealAccBusinessGroupId ?? 0).AccBusinessGroupName;
                            dp.GroupCouponAppStyle = stylelist.Any(x => Regex.Replace(x.Value, @"[\W_]+", "") == accName) ? stylelist.FirstOrDefault(x => Regex.Replace(x.Value, @"[\W_]+", "") == accName).Key : 0;
                        }
                        //分類
                        dp.DealType = mdp.DealType;
                        dp.IsReserveLock = mdp.IsReserveLock;

                        pp.DealPropertySet(dp);
                        #endregion

                        #region deal_accounting
                        //業績歸屬
                        da.DeptId = mda.DeptId;
                        da.Paytocompany = mda.Paytocompany;
                        da.VendorBillingModel = mda.VendorBillingModel;
                        da.RemittanceType = mda.RemittanceType;
                        da.VendorReceiptType = mda.VendorReceiptType;
                        da.Message = mda.Message;
                        da.IsInputTaxRequired = mda.IsInputTaxRequired;

                        pp.DealAccountingSet(da);

                        go.Status = mgo.Status;
                        op.GroupOrderSet(go);
                        #endregion
                    }
                }

                #endregion

                #region compare logs

                // auto compare
                CompareObject(oriBusiness, model.Business);
                CompareObject(oriItem, model.ItemProperty);
                CompareObject(oriProperty, model.Property);
                CompareObject(oriAccounting, model.Accounting);
                CompareObject(oriCec, model.ContentProperty);
                CompareCategoryDeal(oricategories, categories);

                // custom compare
                GroupOrderChangeLog(oriGroup, model.Group);
                DealCostChangeLog(oriCosts, model.Costs);
                CouponFreightChangeLog(oriFreight, model.Freight);
                MohistChangeLog(oriPro, model.Pro);

                #endregion
            }

            View.ShowMessage(string.Empty, BusinessContentMode.Reload, TabName.MainSet.ToString());
        }

        protected void OnReCreateProposal(object sender, DataEventArgs<KeyValuePair<Guid, ProposalCopyType>> e)
        {
            int pid = PponFacade.CreateProposalByCloneDeal(e.Data.Key, e.Data.Value, View.UserName);
            if (pid != 0)
            {
                View.ShowMessage(string.Empty, BusinessContentMode.GoToProposal, pid.ToString());
            }
            else
            {
                View.ShowMessage("提案產生錯誤。", BusinessContentMode.DataError);
            }
        }

        protected void OnPponStoreSave(object sender, EventArgs e)
        {
            List<Guid> bids = new List<Guid>();
            ViewComboDealCollection vcdc = pp.GetViewComboDealByBid(View.BusinessHourGuid, false);
            if (vcdc.Count > 0)
            {
                //多檔
                bids = vcdc.Select(x => x.BusinessHourGuid).ToList();
                bids.Add(View.BusinessHourGuid);
            }
            else
            {
                //單檔
                bids.Add(View.BusinessHourGuid);
            }


            foreach (Guid bid in bids)
            {
                //現況資料
                PponStoreCollection pponStore = View.GetPponStores(bid);
                PponStoreCollection tmppponstore = pponStore.Clone();
                //原始資料
                PponStoreCollection oriStore = pp.PponStoreGetListByBusinessHourGuid(bid);
                PponStoreCollection tmporiStore = oriStore.Clone();
                //儲存資料
                PponStoreCollection savePponStore = new PponStoreCollection();
                //刪除被勾掉的分店
                PponStoreCollection delePponStore = new PponStoreCollection();

                foreach (PponStore store in oriStore)
                {
                    PponStore s = pponStore.Where(x => x.StoreGuid == store.StoreGuid).FirstOrDefault();
                    if (s != null)
                    {
                        //location還在
                        store.BusinessHourGuid = bid;
                        store.ResourceGuid = Guid.NewGuid();
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(true, store.VbsRight, VbsRightFlag.Location));
                    }
                    else
                    {
                        //location取消
                        if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Verify) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Accouting) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.ViewBalanceSheet) || Helper.IsFlagSet(store.VbsRight, VbsRightFlag.VerifyShop))
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Location));
                        else
                            delePponStore.Add(store);//刪掉
                    }
                    savePponStore.Add(store);
                }
                pp.PponStoreDeleteList(delePponStore);

                // 新增被勾選的分店
                foreach (PponStore store in pponStore)
                {
                    if (!oriStore.Any(x => x.StoreGuid == store.StoreGuid))
                    {
                        store.BusinessHourGuid = bid;
                        store.ResourceGuid = Guid.NewGuid();
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag(true, store.VbsRight, VbsRightFlag.Location));
                        savePponStore.Add(store);
                    }
                }
                pp.PponStoreSetList(savePponStore);

                if (View.BusinessHourGuid == bid)
                    CompareLocationSeller(tmppponstore, tmporiStore);
            }

            View.ShowMessage(string.Empty, BusinessContentMode.Reload, TabName.PponStoreSet.ToString());
        }

        protected void OnVbsSave(object sender, DataEventArgs<int> e)
        {
            List<Guid> bids = new List<Guid>();
            ViewComboDealCollection vcdc = pp.GetViewComboDealByBid(View.BusinessHourGuid, false);
            if (vcdc.Count > 0)
            {
                //多檔
                bids = vcdc.Select(x => x.BusinessHourGuid).ToList();
                bids.Add(View.BusinessHourGuid);
            }
            else
            {
                //單檔
                bids.Add(View.BusinessHourGuid);
            }


            foreach (Guid bid in bids)
            {
                //現況資料
                PponStoreCollection pponStore = View.GetVbs(bid);
                PponStoreCollection tmppponStore = pponStore.Clone();
                //原始資料
                PponStoreCollection oriStore = pp.PponStoreGetListByBusinessHourGuid(bid);
                PponStoreCollection tmporistore = oriStore.Clone();
                //儲存資料
                PponStoreCollection savePponStore = new PponStoreCollection();
                //刪除被勾掉的分店
                PponStoreCollection delePponStore = new PponStoreCollection();

                BusinessHour b = sp.BusinessHourGet(bid);
                DealAccounting da = pp.DealAccountingGet(bid);
                List<Guid> verifyshopGuids = new List<Guid>();
                List<Guid> verifyGuids = new List<Guid>();
                List<Guid> accountingGuids = new List<Guid>();
                List<Guid> balanceSheetGuids = new List<Guid>();
                List<Guid> BalanceSheetHideFromDealSellerGuids = new List<Guid>();
                List<Guid> HideFromVerifyShopGuids = new List<Guid>();


                foreach (PponStore store in oriStore)
                {
                    PponStore s = pponStore.Where(x => x.StoreGuid == store.StoreGuid).FirstOrDefault();
                    if (s != null)
                    {
                        store.TotalQuantity = s.TotalQuantity;
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.VerifyShop) > 0 ? true : false, store.VbsRight, VbsRightFlag.VerifyShop));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.Verify) > 0 ? true : false, store.VbsRight, VbsRightFlag.Verify));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.Accouting) > 0 ? true : false, store.VbsRight, VbsRightFlag.Accouting));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0 ? true : false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((s.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0 ? true : false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));

                        if ((s.VbsRight & (int)VbsRightFlag.VerifyShop) > 0)
                            verifyshopGuids.Add(s.StoreGuid);
                        if ((s.VbsRight & (int)VbsRightFlag.Verify) > 0)
                            verifyGuids.Add(s.StoreGuid);
                        if ((s.VbsRight & (int)VbsRightFlag.Accouting) > 0)
                            accountingGuids.Add(s.StoreGuid);
                        if ((s.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0)
                            balanceSheetGuids.Add(s.StoreGuid);
                        if ((s.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0)
                            BalanceSheetHideFromDealSellerGuids.Add(s.StoreGuid);
                    }
                    else
                    {
                        if (Helper.IsFlagSet(store.VbsRight, VbsRightFlag.Location))
                        {
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.VerifyShop));
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Verify));
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.Accouting));
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                            store.VbsRight = Convert.ToInt32(Helper.SetFlag(false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));
                        }
                        else
                            delePponStore.Add(store);//刪掉
                    }
                    savePponStore.Add(store);
                }

                // 新增被勾選的分店
                foreach (PponStore store in pponStore)
                {
                    if (!oriStore.Any(x => x.StoreGuid == store.StoreGuid))
                    {
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.VerifyShop) > 0 ? true : false, store.VbsRight, VbsRightFlag.VerifyShop));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.Verify) > 0 ? true : false, store.VbsRight, VbsRightFlag.Verify));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.Accouting) > 0 ? true : false, store.VbsRight, VbsRightFlag.Accouting));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0 ? true : false, store.VbsRight, VbsRightFlag.ViewBalanceSheet));
                        store.VbsRight = Convert.ToInt32(Helper.SetFlag((store.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0 ? true : false, store.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller));

                        if ((store.VbsRight & (int)VbsRightFlag.VerifyShop) > 0)
                            verifyshopGuids.Add(store.StoreGuid);
                        if ((store.VbsRight & (int)VbsRightFlag.Verify) > 0)
                            verifyGuids.Add(store.StoreGuid);
                        if ((store.VbsRight & (int)VbsRightFlag.Accouting) > 0)
                            accountingGuids.Add(store.StoreGuid);
                        if ((store.VbsRight & (int)VbsRightFlag.ViewBalanceSheet) > 0)
                            balanceSheetGuids.Add(store.StoreGuid);
                        if ((store.VbsRight & (int)VbsRightFlag.BalanceSheetHideFromDealSeller) > 0)
                            BalanceSheetHideFromDealSellerGuids.Add(store.StoreGuid);

                        savePponStore.Add(store);
                    }
                }
                var vbsRightInfo = new Dictionary<VbsRightFlag, IEnumerable<Guid>>();
                vbsRightInfo.Add(VbsRightFlag.VerifyShop, verifyshopGuids);
                vbsRightInfo.Add(VbsRightFlag.Verify, verifyGuids);
                vbsRightInfo.Add(VbsRightFlag.Accouting, accountingGuids);
                vbsRightInfo.Add(VbsRightFlag.ViewBalanceSheet, balanceSheetGuids);
                vbsRightInfo.Add(VbsRightFlag.BalanceSheetHideFromDealSeller, BalanceSheetHideFromDealSellerGuids);
                vbsRightInfo.Add(VbsRightFlag.HideFromVerifyShop, HideFromVerifyShopGuids);

                string checkErrorMsg = "";
                bool vbs = SellerFacade.ValiadateSellerTreeSetUp(b.SellerGuid, vbsRightInfo, out checkErrorMsg);
                if (!vbs && e.Data == 0)
                    View.ShowMessage(checkErrorMsg, BusinessContentMode.DataError);
                else
                {
                    pp.PponStoreDeleteList(delePponStore);
                    pp.PponStoreSetList(savePponStore);
                    if (View.BusinessHourGuid == bid)
                        CompareVbsSeller(tmppponStore, tmporistore);
                    //設定匯款方式
                    da.Paytocompany = (int)SellerFacade.GetPayToCompany(b.SellerGuid, oriStore);
                    pp.DealAccountingSet(da);

                    //檢查匯款對象商家之財務資料
                    ProposalFacade.CheckProposalFinanceCheck(2, View.UserName, 0, View.BusinessHourGuid);

                    View.ShowMessage(string.Empty, BusinessContentMode.Reload, TabName.PponStoreSet.ToString());
                }
            }
            View.ShowMessage(string.Empty, BusinessContentMode.Reload, TabName.PponStoreSet.ToString());
        }

        protected void OnSettingCheck(object sender, DataEventArgs<Guid> e)
        {
            Proposal pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, e.Data);
            if (pro.IsLoaded)
            {
                Seller s = sp.SellerGet(pro.SellerGuid);

                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    if (!Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractSend) || !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PicCheck))
                    {
                        View.ShowMessage("「合約寄出」及「圖檔確認」尚未確認。", BusinessContentMode.DataError);
                        return;
                    }
                }


                if (pro.DeliveryType != (int)DeliveryType.ToHouse || (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType == (int)ProposalDealType.Travel))
                {
                    //除一般宅配外，送出排檔自動勾起提案稽核
                    //pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));
                }
                else
                {
                    if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller)
                        {
                            //若走商家覆核流程的提案單，則在業務觸發【送件QC】後，以下程序全部同步自動打勾
                            pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));     //提案稽核
                        }
                    }
                }


                CouponEventContent cec = pp.CouponEventContentGetByBid(e.Data);
                if (string.IsNullOrWhiteSpace(cec.Restrictions))
                {
                    View.ShowMessage("未填寫權益說明。", BusinessContentMode.DataError);
                    return;
                }

                if (!Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck) && !Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractNotCheck))
                {
                    View.ShowMessage("「附約已回」或「同意合約未回先上檔」尚未確認。", BusinessContentMode.DataError);
                    return;
                }



                ProposalBusinessCreateFlag flag = ProposalBusinessCreateFlag.SettingCheck;
                pro.BusinessCreateFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessCreateFlag, flag));
                pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit)); //自動勾起提案稽核
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractInspect)); //自動勾起合約檢核
                pro.Status = SellerFacade.GetFlagForProposalFlagCheck(pro);
                pro.ModifyId = View.UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                #region 若為送出排檔，則寄送通知信予商審或主管

                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                List<string> mailToUser = new List<string>();

                if (pro.DeliveryType == (int)DeliveryType.ToHouse)
                {
                    //宅配寄給商審
                    EmployeeCollection emps = HumanFacade.EmployeeGetListByDept(EmployeeDept.Q000);
                    if (emps != null)
                    {
                        foreach (Employee em in emps)
                        {
                            var _member = hp.ViewEmployeeGet("emp_id", em.EmpId);
                            if (RegExRules.CheckEmail(_member.Email))
                            {
                                mailToUser.Add(_member.Email);//商審人員email 
                            }
                        }
                    }
                }
                else
                {
                    //搜尋上檔頻道
                    CategoryDealCollection cdc = pp.CategoryDealsGetList(View.BusinessHourGuid);
                    var piinlife = cdc.Where(x => x.Cid == 148);
                    var travel = cdc.Where(x => x.Cid == 90);

                    //品生活寄給品生活主管，旅遊寄給旅遊主管，其餘寄給該區域主管
                    string DeptId = "";
                    if (piinlife.Count() > 0)
                        DeptId = EmployeeChildDept.S012.ToString();
                    else if (travel.Count() > 0)
                        DeptId = EmployeeChildDept.S008.ToString();
                    else
                        DeptId = deSalesEmp.DeptId;

                    var emps = hp.EmployeeCollectionGetByFilter("dept_id", DeptId).Where(x => x.DeptManager != null);
                    if (emps != null)
                    {
                        IEnumerable<Employee> manager = emps.Where(x => x.DeptManager.Contains(DeptId));
                        foreach (Employee m in manager)
                        {
                            var _manager = hp.ViewEmployeeGet("emp_id", m.EmpId);
                            if (RegExRules.CheckEmail(_manager.Email))
                            {
                                mailToUser.Add(_manager.Email);//主管email
                            }
                        }
                    }
                }


                string subject = string.Format("【{0}】 單號 NO.{1}", Helper.GetLocalizedEnum(ProposalBusinessCreateFlag.SettingCheck), pro.Id);
                string url = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", config.SiteUrl, pro.Id);
                ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url));



                //憑證要檢查店家經緯度
                if (pro.DeliveryType == (int)DeliveryType.ToShop)
                {
                    SendCoordinateNullMail(pro.Id, config.CodLocalEmail);
                }

                #endregion

                #region 提案單文案規格內容帶入上檔後台

                if (string.IsNullOrEmpty(cec.Description))
                {
                    //僅帶第一次
                    cec.Description = SetDescription(pro);
                    pp.CouponEventContentSet(cec);
                }

                #endregion
                ProposalFacade.ProposalLog(pro.Id, Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, flag), View.UserName);
                View.ShowMessage(string.Empty, BusinessContentMode.Redirect, e.Data.ToString());
            }
            else
            {
                View.ShowMessage("提案單資訊不存在，請由後台建檔。", BusinessContentMode.GoToSetup, pro.BusinessHourGuid.ToString());
                return;
            }
        }


        protected void OnReferral(object sender, DataEventArgs<KeyValuePair<string, SellerSalesType>> e)
        {
            SellerSalesType type = e.Data.Value;
            ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, e.Data.Key);
            if (emp.IsLoaded)
            {
                List<Department> dept = HumanFacade.GetSalesDepartment();
                if (emp.DeptId.EqualsAny(dept.Select(x => x.DeptId).ToArray()))
                {
                    if (type == SellerSalesType.Develope)
                    {
                        ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(View.BusinessHourGuid);
                        if (vcdc.Count > 0)
                        {
                            //多檔次
                            foreach (ViewComboDeal vcd in vcdc)
                            {
                                DealAccounting da = pp.DealAccountingGet(vcd.BusinessHourGuid);
                                da.SalesId = emp.EmpName;
                                pp.DealAccountingSet(da);

                                DealProperty dp = pp.DealPropertyGet(vcd.BusinessHourGuid);
                                dp.DealEmpName = emp.EmpName;
                                dp.DevelopeSalesId = emp.UserId;

                                pp.DealPropertySet(dp);

                                BusinessHourLog(View.BusinessHourGuid, "變更 負責業務1 為 " + emp.EmpName);
                                LoadData(View.BusinessHourGuid);
                            }
                        }
                        else
                        {
                            DealAccounting da = pp.DealAccountingGet(View.BusinessHourGuid);
                            da.SalesId = emp.EmpName;
                            pp.DealAccountingSet(da);

                            DealProperty dp = pp.DealPropertyGet(View.BusinessHourGuid);
                            dp.DealEmpName = emp.EmpName;
                            dp.DevelopeSalesId = emp.UserId;

                            pp.DealPropertySet(dp);

                            BusinessHourLog(View.BusinessHourGuid, "變更 負責業務1 為 " + emp.EmpName);
                            LoadData(View.BusinessHourGuid);
                        }
                    }
                    else
                    {
                        ViewComboDealCollection vcdc = pp.GetViewComboDealAllByBid(View.BusinessHourGuid);
                        if (vcdc.Count > 0)
                        {
                            //多檔次
                            foreach (ViewComboDeal vcd in vcdc)
                            {
                                DealAccounting da = pp.DealAccountingGet(vcd.BusinessHourGuid);
                                da.OperationSalesId = emp.EmpName;
                                pp.DealAccountingSet(da);

                                DealProperty dp = pp.DealPropertyGet(vcd.BusinessHourGuid);
                                dp.OperationSalesId = emp.UserId;

                                pp.DealPropertySet(dp);

                                BusinessHourLog(View.BusinessHourGuid, "變更 負責業務2 為 " + emp.EmpName);
                                LoadData(View.BusinessHourGuid);
                            }
                        }
                        else
                        {
                            DealAccounting da = pp.DealAccountingGet(View.BusinessHourGuid);
                            da.OperationSalesId = emp.EmpName;
                            pp.DealAccountingSet(da);

                            DealProperty dp = pp.DealPropertyGet(View.BusinessHourGuid);
                            dp.OperationSalesId = emp.UserId;

                            pp.DealPropertySet(dp);

                            BusinessHourLog(View.BusinessHourGuid, "變更 負責業務2 為 " + emp.EmpName);
                            LoadData(View.BusinessHourGuid);
                        }
                    }
                }
                else
                {
                    View.ShowMessage("無法轉件於非業務，請重新檢視。", BusinessContentMode.DataError);
                }
            }
            else
            {
                View.ShowMessage("Email帳號錯誤，請重新檢視。", BusinessContentMode.DataError);
            }
        }

        protected void OnChangeLog(object sender, DataEventArgs<string> e)
        {
            BusinessHourLog(View.BusinessHourGuid, e.Data);
            View.ShowMessage(string.Empty, BusinessContentMode.Reload, TabName.HistorySet.ToString());
        }

        #endregion

        #region Private Method

        private void LoadData(Guid bid)
        {
            #region get data

            AccBusinessGroupCollection acc = sysmp.AccBusinessGroupGetList();
            DepartmentCollection depts = hp.DepartmentGetListBySalesAndMarketing(true);
            View.SetAccField(acc, depts);

            Proposal pro;
            SalesBusinessModel model;
            DealAccounting da;
            SellerCollection stores;
            PponStoreCollection psc;
            //CouponEventContent cec;
            GetPponDealData(bid, out pro, out model, out da, out stores, out psc);

            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.EmpName, da.SalesId);
            if (!emp.IsLoaded)
            {
                emp = hp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
            }

            ViewComboDealCollection combo = pp.GetViewComboDealByBid(bid, true);


            CategoryDealCollection cdc = pp.CategoryDealsGetList(bid);

            BusinessChangeLogCollection logs = sp.BusinessChangeLogGetList(bid);

            #endregion

            if (model.Business.Guid != Guid.Empty && model.Group.Status.HasValue && pro.Id != 0)
            {
                if (Helper.IsFlagSet(model.Business.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                {
                    ComboDeal sub = pp.GetComboDeal(model.Business.Guid);
                    View.ShowMessage(string.Empty, BusinessContentMode.Redirect, sub.MainBusinessHourGuid.Value.ToString());
                    return;
                }

                //整棵tree
                SellerCollection tcsc = new SellerCollection();
                Guid root = SellerFacade.GetRootSellerGuid(pro.SellerGuid);
                var AllChrildren = SellerFacade.GetChildrenSellerGuid(root);
                tcsc.Add(sp.SellerGet(root));
                foreach (var children in AllChrildren)
                    tcsc.Add(sp.SellerGet(children));
                View.SetBusinessContent(model, combo, tcsc, cdc, psc, pro, emp, logs);
            }
            else
            {
                View.ShowMessage("查無檔次資料，或該檔次非提案單所建立。", BusinessContentMode.BusinessNotFound);
            }
        }

        private void GetPponDealData(Guid bid, out Proposal pro, out SalesBusinessModel model, out DealAccounting da, out SellerCollection stores, out PponStoreCollection psc)
        {
            pro = sp.ProposalGet(Proposal.Columns.BusinessHourGuid, bid);

            model = new SalesBusinessModel();

            #region Get Model Data

            BusinessHour b = pp.BusinessHourGet(bid);
            Seller s = sp.SellerGet(b.SellerGuid);
            GroupOrder g = op.GroupOrderGetByBid(bid);
            Item item = imp.ItemGetByBid(bid);
            DealProperty dp = pp.DealPropertyGet(bid);
            da = pp.DealAccountingGet(bid);
            DealCostCollection dcc = pp.DealCostGetList(bid);
            CouponFreightCollection cfc = pp.CouponFreightGetList(bid);
            ViewComboDealCollection vcdc = pp.GetViewComboDealByBid(bid, false);
            CouponEventContent cec = pp.CouponEventContentGetByBid(bid);

            model.Business = b;
            model.Combodeals = vcdc;
            model.SellerProperty = s;
            model.Group = g;
            model.ItemProperty = item;
            model.Property = dp;
            model.Accounting = da;
            model.Costs = dcc;
            model.Freight = cfc;
            model.ContentProperty = cec;

            #endregion

            psc = pp.PponStoreGetListByBusinessHourGuid(bid);

            stores = sp.SellerGetListBySellerGuidList(psc.Select(x => x.StoreGuid).ToList());
        }

        private void CouponFreightSave(CouponFreightCollection freight)
        {
            pp.CouponFreightDeleteListByBid(View.BusinessHourGuid);
            pp.CouponFreightSetList(freight);
        }

        private void GroupOrderChangeLog(GroupOrder oriGroup, GroupOrder groupOrder)
        {
            if (oriGroup.Status.Value != groupOrder.Status.Value
                && !(Helper.IsFlagSet(oriGroup.Status.Value, GroupOrderStatus.NoTax) && Helper.IsFlagSet(groupOrder.Status.Value, GroupOrderStatus.NoTax)))
            {
                BusinessHourLog(View.BusinessHourGuid, "變更 開立免稅發票給消費者 為 " + (Helper.IsFlagSet(groupOrder.Status.Value, GroupOrderStatus.NoTax) ? "有" : "無"));
            }
        }

        private void MohistChangeLog(Proposal oriPro, Proposal pro)
        {
            if (oriPro.Mohist != null && pro.Mohist != null)
            {
                if ((bool)oriPro.Mohist ^ (bool)pro.Mohist)
                {
                    BusinessHourLog(View.BusinessHourGuid, "變更 出帳方式-墨攻 為 " + ((bool)(pro.Mohist) ? "是" : "否"));
                }
            }
        }

        private void DealCostChangeLog(DealCostCollection oriDcs, DealCostCollection dcs)
        {
            List<string> changeList = new List<string>();
            DealCost dc = dcs.Where(x => x.Cost != 0).FirstOrDefault();
            DealCost slottingFee = dcs.Where(x => x.Cost == 0).FirstOrDefault();
            DealCost oriDc = oriDcs.Where(x => x.Cost != 0).FirstOrDefault();
            DealCost oriSlottingFee = oriDcs.Where(x => x.Cost == 0).FirstOrDefault();

            if (dcs.Count > 1)
            {
                if (dc != null && dc.Cost.HasValue && slottingFee.Quantity.HasValue)
                {
                    if (oriDcs.Count == 1)
                    {
                        // 新增了上架費
                        changeList.Add(string.Format("變更 上架費 為 {0} 份", slottingFee.Quantity.Value));
                    }
                    else
                    {
                        // 上架費變更
                        if (oriSlottingFee != null && oriSlottingFee.Quantity.HasValue)
                        {
                            if (slottingFee.Quantity != oriSlottingFee.Quantity)
                            {
                                changeList.Add(string.Format("變更 上架費 為 {0} 份", slottingFee.Quantity.Value));
                            }
                        }
                    }

                    // 進貨價變更
                    if (oriDc != null && dc.Cost != null && oriDc.Cost != dc.Cost)
                    {
                        changeList.Add(string.Format("變更 進貨價 為 {0}", dc.Cost.Value.ToString("F0")));
                    }
                }
            }
            else if (dcs.Count == 1)
            {
                // 進貨價變更
                if (dc.Cost.HasValue && oriDc.Cost.HasValue && oriDc.Cost != dc.Cost)
                {
                    changeList.Add(string.Format("變更 進貨價 為 {0}", dc.Cost.Value.ToString("F0")));
                }

                // 上架費移除
                if (oriDcs.Count > 1)
                {
                    changeList.Add("變更 上架費 為 無");
                }
            }

            foreach (var change in changeList)
            {
                BusinessHourLog(View.BusinessHourGuid, change);
            }
        }

        private void CouponFreightChangeLog(CouponFreightCollection orifc, CouponFreightCollection fc)
        {
            List<string> changeList = new List<string>();
            CouponFreight f = fc.Where(x => x.FreightAmount != 0).FirstOrDefault();
            CouponFreight nf = fc.Where(x => x.FreightAmount == 0).FirstOrDefault();
            CouponFreight orif = orifc.Where(x => x.FreightAmount != 0).FirstOrDefault();
            CouponFreight orinf = orifc.Where(x => x.FreightAmount == 0).FirstOrDefault();

            if (fc.Count > 1)
            {
                if (f != null)
                {
                    if (orifc.Count == 1)
                    {
                        // 新增了免運門檻
                        changeList.Add(string.Format("變更 免運門檻 為 {0} 元", nf.StartAmount));
                    }
                    else
                    {
                        // 免運門檻變更
                        if (orinf != null)
                        {
                            if (nf.StartAmount != orinf.StartAmount)
                            {
                                changeList.Add(string.Format("變更 免運門檻 為 {0} 份", nf.StartAmount));
                            }
                        }
                    }

                    // 運費變更
                    if (orif != null && orif.FreightAmount != f.FreightAmount)
                    {
                        changeList.Add(string.Format("變更 運費 為 {0}", f.FreightAmount.ToString("F0")));
                    }
                }
            }
            else if (fc.Count == 1)
            {
                if (f != null)
                {
                    // 運費變更
                    if (orif.FreightAmount != f.FreightAmount)
                    {
                        changeList.Add(string.Format("變更 運費 為 {0}", f.FreightAmount.ToString("F0")));
                    }

                    // 免運門檻移除
                    if (orifc.Count > 1)
                    {
                        changeList.Add("變更 免運門檻 為 無");
                    }
                }
            }
            else if (orifc.Count > 0)
            {
                changeList.Add("變更為 免運");
            }

            foreach (var change in changeList)
            {
                BusinessHourLog(View.BusinessHourGuid, change);
            }
        }

        private void CompareObject(object obj, object newObj)
        {
            List<string> changeList = new List<string>();
            foreach (var item in PponFacade.ColumnValueComparison(obj, newObj))
            {
                changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
            }
            if (changeList.Count > 0)
            {
                BusinessHourLog(View.BusinessHourGuid, string.Join("|", changeList));
            }
        }

        private void CompareCategoryDeal(CategoryDealCollection oricategories, CategoryDealCollection categories)
        {
            List<string> changeList = new List<string>();
            List<string> categoryname = new List<string>();
            List<string> oricategoryname = new List<string>();

            foreach (var c in categories)
            {
                categoryname.Add(sp.CategoryGet(c.Cid).Name);
            }

            foreach (var oc in oricategories)
            {
                oricategoryname.Add(sp.CategoryGet(oc.Cid).Name);
            }

            if (string.Join("、", categoryname) != string.Join("、", oricategoryname))
                changeList.Add(string.Format("變更 {0} 為 {1}", "上檔頻道", string.Join("、", categoryname)));


            if (changeList.Count > 0)
            {
                BusinessHourLog(categories[0].Bid, string.Join("|", changeList));
            }
        }

        private void CompareLocationSeller(PponStoreCollection proposalStore, PponStoreCollection oristore)
        {
            List<Guid> proposalstoreguid = new List<Guid>();
            List<Guid> oristoreguid = new List<Guid>();
            List<string> changeList = new List<string>();
            List<string> savepponstorename = new List<string>();
            proposalstoreguid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Location)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            oristoreguid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Location)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();


            if (string.Join("、", proposalstoreguid) != string.Join("、", oristoreguid))
            {
                foreach (var s in proposalstoreguid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                changeList.Add(string.Format("變更 {0} 為 {1}", "營業據點", string.Join("、", savepponstorename)));
            }

            if (changeList.Count > 0)
            {
                BusinessHourLog(proposalStore[0].BusinessHourGuid, string.Join("、", changeList));
            }
        }

        private void CompareVbsSeller(PponStoreCollection proposalStore, PponStoreCollection oristore)
        {
            List<Guid> VerifyGuid = new List<Guid>();
            List<Guid> AccoutingGuid = new List<Guid>();
            List<Guid> ViewBalanceSheetGuid = new List<Guid>();
            List<Guid> BalanceSheetHideFromDealSellerGuid = new List<Guid>();
            List<Guid> VerifyShopGuid = new List<Guid>();

            List<Guid> OriVerifyGuid = new List<Guid>();
            List<Guid> OriAccoutingGuid = new List<Guid>();
            List<Guid> OriViewBalanceSheetGuid = new List<Guid>();
            List<Guid> OriBalanceSheetHideFromDealSellerGuid = new List<Guid>();
            List<Guid> OriVerifyShopGuid = new List<Guid>();


            List<string> changeList = new List<string>();
            List<string> savepponstorename = new List<string>();


            VerifyGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Verify)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriVerifyGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Verify)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            AccoutingGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriAccoutingGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            ViewBalanceSheetGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.ViewBalanceSheet)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriViewBalanceSheetGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.ViewBalanceSheet)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            BalanceSheetHideFromDealSellerGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriBalanceSheetHideFromDealSellerGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.BalanceSheetHideFromDealSeller)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();

            VerifyShopGuid = proposalStore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.VerifyShop)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();
            OriVerifyShopGuid = oristore.Where(X => Helper.IsFlagSet(X.VbsRight, VbsRightFlag.VerifyShop)).Select(x => x.StoreGuid).OrderBy(x => x).ToList();


            if (string.Join("、", VerifyGuid) != string.Join("、", OriVerifyGuid))
            {
                foreach (var s in VerifyGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                changeList.Add(string.Format("變更 {0} 為 {1}", "憑證核實", string.Join("、", savepponstorename)));

                savepponstorename.Clear();
            }

            if (string.Join("、", AccoutingGuid) != string.Join("、", OriAccoutingGuid))
            {
                foreach (var s in AccoutingGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);


                changeList.Add(string.Format("變更 {0} 為 {1}", "匯款對象", string.Join("、", savepponstorename)));
                savepponstorename.Clear();
            }

            if (string.Join("、", ViewBalanceSheetGuid) != string.Join("、", OriViewBalanceSheetGuid))
            {
                foreach (var s in ViewBalanceSheetGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "檢視對帳單", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "檢視對帳單", "\"移除檢視對帳單\""));

                savepponstorename.Clear();
            }

            if (string.Join("、", BalanceSheetHideFromDealSellerGuid) != string.Join("、", OriBalanceSheetHideFromDealSellerGuid))
            {
                foreach (var s in BalanceSheetHideFromDealSellerGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "鎖住檢視對帳單", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "鎖住檢視對帳單", "\"移除鎖住檢視對帳單\""));

                savepponstorename.Clear();
            }

            if (string.Join("、", VerifyShopGuid) != string.Join("、", OriVerifyShopGuid))
            {
                foreach (var s in VerifyShopGuid)
                    savepponstorename.Add(sp.SellerGet(s).SellerName);

                if (savepponstorename.Count() != 0)
                    changeList.Add(string.Format("變更 {0} 為 {1}", "銷售店舖", string.Join("、", savepponstorename)));
                else
                    changeList.Add(string.Format("變更 {0} 為 {1}", "銷售店舖", "\"移銷售店舖\""));

                savepponstorename.Clear();
            }

            if (changeList.Count > 0)
            {
                BusinessHourLog(proposalStore[0].BusinessHourGuid, string.Join("|", changeList));
            }
        }

        private void BusinessHourLog(Guid bid, string changeLog)
        {
            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.Email, View.UserName);
            BusinessChangeLog log = new BusinessChangeLog();
            log.BusinessHourGuid = bid;
            log.ChangeLog = changeLog;
            log.Dept = emp.DeptName;
            log.CreateId = View.UserName;
            log.CreateTime = DateTime.Now;
            sp.BusinessChangeLogSet(log);
        }

        private Dictionary<string, string> GetPponDealDictionary(SalesBusinessModel model, Proposal pro, Seller store, bool isSingalStore)
        {
            string newLine = "\n";

            Dictionary<string, string> dataList = new Dictionary<string, string>();
            if (isSingalStore && model.Property.DeliveryType.Equals((int)DeliveryType.ToShop))
            {
                #region 分店資料套入

                dataList.Add("SellerName", string.Format("{0}_{1}", model.SellerProperty.SellerName, store.SellerName));
                dataList.Add("CompanyName", store.SellerName);
                dataList.Add("SignCompanyId", store.CompanyID);
                dataList.Add("BossName", store.CompanyBossName);
                dataList.Add("ContactPerson", store.CompanyBossName);
                dataList.Add("BossMobile", string.Empty);
                dataList.Add("BossTel", store.StoreTel);
                dataList.Add("BossEmail", store.CompanyEmail);
                dataList.Add("BossFax", string.Empty);
                dataList.Add("BossAddress", store.StoreTownshipId.HasValue ? (CityManager.CityTownShopStringGet(store.StoreTownshipId.Value) + store.StoreAddress) : string.Empty);
                dataList.Add("StoreName", store.SellerName);
                dataList.Add("SellerAddress", store.StoreTownshipId.HasValue ? (CityManager.CityTownShopStringGet(store.StoreTownshipId.Value) + store.StoreAddress) : string.Empty);

                dataList.Add("AccountingName", store.AccountantName);
                dataList.Add("AccountingTel", store.AccountantTel);
                dataList.Add("AccountingEmail", store.CompanyEmail);
                dataList.Add("AcctBankId", store.CompanyBankCode);
                dataList.Add("AcctBankDesc", VourcherFacade.ApiBankNoGet(store.CompanyBankCode).BankName);
                dataList.Add("AcctBranchId", store.CompanyBranchCode);
                dataList.Add("AcctBranchDesc", VourcherFacade.ApiBranchNoGet(store.CompanyBankCode, store.CompanyBranchCode).BranchName);
                dataList.Add("AccountingId", store.SignCompanyID);
                dataList.Add("AccountingText", store.CompanyAccountName);
                dataList.Add("AccountText", store.CompanyAccount);

                #endregion
            }
            else
            {
                #region 帶入總店資料                
                dataList.Add("BrandName", model.ItemProperty.ItemName);
                dataList.Add("SellerName", model.SellerProperty.SellerName);
                dataList.Add("CompanyName", model.SellerProperty.CompanyName);
                dataList.Add("SignCompanyId", model.SellerProperty.CompanyID);
                dataList.Add("BossName", model.SellerProperty.CompanyBossName);
                dataList.Add("ContactPerson", model.SellerProperty.SellerContactPerson);
                dataList.Add("BossMobile", model.SellerProperty.SellerMobile);
                dataList.Add("BossTel", model.SellerProperty.SellerTel);
                dataList.Add("BossEmail", model.SellerProperty.SellerEmail);
                dataList.Add("BossFax", model.SellerProperty.SellerFax);
                dataList.Add("BossAddress", CityManager.CityTownShopStringGet(model.SellerProperty.CityId) + model.SellerProperty.SellerAddress);
                dataList.Add("StoreName", model.SellerProperty.SellerName);
                dataList.Add("SellerAddress", CityManager.CityTownShopStringGet(model.SellerProperty.CityId) + model.SellerProperty.SellerAddress);

                dataList.Add("AccountingName", model.SellerProperty.AccountantName);
                dataList.Add("AccountingTel", model.SellerProperty.AccountantTel);
                dataList.Add("AccountingEmail", model.SellerProperty.CompanyEmail);
                dataList.Add("AcctBankId", model.SellerProperty.CompanyBankCode);
                dataList.Add("AcctBankDesc", VourcherFacade.ApiBankNoGet(model.SellerProperty.CompanyBankCode).BankName);
                dataList.Add("AcctBranchId", model.SellerProperty.CompanyBranchCode);
                dataList.Add("AcctBranchDesc", VourcherFacade.ApiBranchNoGet(model.SellerProperty.CompanyBankCode, model.SellerProperty.CompanyBranchCode).BranchName);
                dataList.Add("AccountingId", model.SellerProperty.SignCompanyID);
                dataList.Add("AccountingText", model.SellerProperty.CompanyAccountName);
                dataList.Add("AccountText", model.SellerProperty.CompanyAccount);

                #endregion
            }

            if (model.Property.DeliveryType == (int)DeliveryType.ToShop)
            {
                dataList.Add("OpeningTime", store.OpenTime);
                dataList.Add("CloseDate", store.CloseDate);
                string vehicleInfo = string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(store.Mrt) ? "捷運：" + store.Mrt + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(store.Car) ? "開車：" + store.Car + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(store.Bus) ? "公車：" + store.Bus + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(store.OtherVehicles) ? "其他：" + store.OtherVehicles : string.Empty;
                dataList.Add("VehicleInfo", vehicleInfo);
                string webUrl = string.Empty;
                webUrl += !string.IsNullOrEmpty(store.WebUrl) ? "官網網址：" + store.WebUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(store.FacebookUrl) ? "FaceBook網址：" + store.FacebookUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(store.BlogUrl) ? "部落格網址：" + store.BlogUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(store.OtherUrl) ? "其他網址：" + store.OtherUrl : string.Empty;
                dataList.Add("WebUrl", webUrl);
            }

            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);

            #region Part1 基本資料

            dataList.Add("BusinessType", Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DeliveryType)model.Property.DeliveryType));
            dataList.Add("ReturnedPerson", model.SellerProperty.ReturnedPersonName);
            dataList.Add("ReturnedTel", model.SellerProperty.ReturnedPersonTel);
            dataList.Add("ReturnedEmail", model.SellerProperty.ReturnedPersonEmail);
            if (pro.DealType == (int)ProposalDealType.Travel && Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Travel))
            {
                dataList.Add("HotelInfo", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Travel) && specialText.ContainsKey(ProposalSpecialFlag.Travel) ? specialText[ProposalSpecialFlag.Travel] : string.Empty);
            }
            else
            {
                dataList.Add("HotelInfo", string.Empty);
            }
            dataList.Add("DealAccountingPayType", Helper.GetLocalizedEnum((DealAccountingPayType)model.Accounting.Paytocompany));

            string invoice = string.Empty;
            switch ((VendorReceiptType)model.Accounting.VendorReceiptType)
            {
                case VendorReceiptType.Other:
                    invoice = model.Accounting.Message;
                    break;
                case VendorReceiptType.Receipt:
                case VendorReceiptType.Invoice:
                    invoice = Helper.GetLocalizedEnum((VendorReceiptType)model.Accounting.VendorReceiptType);
                    break;
                default:
                    break;
            }
            if (!model.Accounting.IsInputTaxRequired)
            {
                invoice += "(免稅)";
            }
            dataList.Add("Invoice", invoice);
            dataList.Add("FounderInfo", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InvoiceFounder) && specialText.ContainsKey(ProposalSpecialFlag.InvoiceFounder) ? specialText[ProposalSpecialFlag.InvoiceFounder] : string.Empty);

            string provisionList = string.Empty;
            int index = 1;
            if (model.ContentProperty.Restrictions != null)
            {
                foreach (var provi in Regex.Replace(model.ContentProperty.Restrictions, @"<[^>]*>", string.Empty).Split(newLine))
                {
                    provisionList += provi.Replace("&nbsp;", "") + newLine;
                    index++;
                }
                dataList.Add("ProvisionList", provisionList.TrimEnd(newLine.ToCharArray()));
            }
            else
            {
                dataList.Add("ProvisionList", string.Empty);
            }

            #endregion Part1 基本資料

            #region Part2 好康優惠

            ProposalMultiDealCollection multiDeal = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);

            string dealDesc = string.Empty;
            string[] paragraph = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i < multiDeal.Count; i++)
            {
                dealDesc += string.Format("{0}.1　", paragraph[i]);
                dealDesc += string.Format("賣價${0}；原價${1}；", multiDeal[i].ItemPrice.ToString("F0"), multiDeal[i].OrigPrice.ToString("F0"));
                dealDesc += string.Format("優惠活動：{0}", multiDeal[i].ItemName);
                dealDesc += string.Format(newLine + "{0}.2　最高購買數量{1}單位；每人限購{2}", paragraph[i], multiDeal[i].OrderTotalLimit.ToString("F0"), multiDeal[i].OrderMaxPersonal);
                dealDesc += string.Format(newLine + "{0}.3　上架費份數：{1}；進貨價格：{2}(含稅)", paragraph[i], multiDeal[i].SlottingFeeQuantity, multiDeal[i].Cost.ToString("F0"));

                int num = 4;
                if (model.Property.DeliveryType == (int)DeliveryType.ToHouse && multiDeal[i].Freights != 0)
                {
                    dealDesc += string.Format(newLine + "{0}.{1}　運費：{2}元整", paragraph[i], num, multiDeal[i].Freights.ToString("F0"));
                    if (multiDeal[i].NoFreightLimit != 0)
                    {
                        dealDesc += string.Format("；{0}份免運", multiDeal[i].NoFreightLimit.ToString("F0"));
                    }
                    num++;
                }
                if (!string.IsNullOrWhiteSpace(multiDeal[i].Options))
                {
                    dealDesc += string.Format(newLine + "{0}.{1}　款式選項：{2}", paragraph[i], num, multiDeal[i].Options);
                }
                dealDesc += newLine + newLine;
            }
            dataList.Add("DealDesc", dealDesc);

            if (model.Property.DeliveryType == (int)DeliveryType.ToShop)
            {
                #region 好康相關資訊

                dataList.Add("StartUseDesc", string.Format("活動開始兌換後{0}{1}皆可使用", pro.StartUseText, Helper.GetLocalizedEnum((ProposalStartUseUnitType)pro.StartUseUnit)));
                dataList.Add("ReservationTel", store.StoreTel);

                #endregion
            }
            else if (model.Property.DeliveryType == (int)DeliveryType.ToHouse)
            {

                #region 商品相關資訊

                dataList.Add("IsParallelProduct", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation) ? "是" : "否");
                dataList.Add("IsExpiringItems", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) ? "是" : "否");
                dataList.Add("ExpiringItemsDate", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct) && specialText.ContainsKey(ProposalSpecialFlag.ExpiringProduct) ? specialText[ProposalSpecialFlag.ExpiringProduct] : string.Empty);
                dataList.Add("ProductSpec", !string.IsNullOrEmpty(pro.ProductSpec) ? pro.ProductSpec.Replace(System.Environment.NewLine, newLine) : "無");
                dataList.Add("ProductIngredients", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Beauty) && specialText.ContainsKey(ProposalSpecialFlag.Beauty) ? specialText[ProposalSpecialFlag.Beauty] : string.Empty);
                dataList.Add("InspectRule", Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InspectRule) && specialText.ContainsKey(ProposalSpecialFlag.InspectRule) ? specialText[ProposalSpecialFlag.InspectRule] : string.Empty);
                dataList.Add("DeliveryMethod", !pro.DeliveryMethod.ToString().Equals("") ? Helper.GetLocalizedEnum((ProposalDeliveryMethod)pro.DeliveryMethod) : "");

                string businessOrderUseTimeSet = string.Empty;
                List<string> bList = new List<string>();
                //bList.Add("商品將依照訂單出貨，於上檔後開始陸續出貨。");
                if (pro.DealType == (int)ProposalDealType.Piinlife)
                {
                    bList.Add(I18N.Message.SalesBusinessOrderPiinlifeDeliveryDesc);
                }
                else if ((int)pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    bList.Add("24小時出貨不適用區域：離島及部分偏遠地區。");
                    bList.Add("商品最晚於購買成功後24小時出貨完畢(不含假日)，出貨後配送約2~3個工作日，恕無法指定送達時間。");
                    bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                    bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                }
                else
                {
                    if (!string.IsNullOrEmpty(pro.ShipText1))
                        bList.Add(string.Format("最早出貨日：上檔後+{0}個工作日；最晚出貨日：統一結檔後+{1}個工作日，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText1, pro.ShipText2));
                    else
                        bList.Add(string.Format("商品最晚於購買成功{0}個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間(限台灣本島配送)", pro.ShipText3));
                    bList.Add("供應商應於出貨後1天內，登入本平台商家系統回報出貨狀況並回填出貨單號。");
                    bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受合約第六條第3項第甲款賠償金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                }
                bList.Add("週休假日或例假日成立之訂單，均視為次一工作天之訂單。");
                if (!(pro.TrialPeriod ?? false))
                {
                    bList.Add("鑑賞期：消費者收到貨品後七天內(含六日)。");
                }
                bList.Add("商品退貨：於鑑賞期內完成退貨申請，鑑賞期後除商品瑕疵外，不再接受消費者退貨。");

                for (int i = 0; i < bList.Count; i++)
                {
                    businessOrderUseTimeSet += "(" + (i + 1) + ").　" + bList[i] + newLine;
                }
                dataList.Add("BusinessOrderUseTimeSet", businessOrderUseTimeSet);
                dataList.Add("ProductGuaranteeClauseDesc", I18N.Message.SalesBusinessOrderProductGuaranteeClauseDesc);

                #endregion
            }

            string othersInfo = "無";
            if (pro.DeliveryType == (int)ProposalDealType.Travel)
            {
                othersInfo = "◆ 此優惠房型皆由飯店視現場情況作調整及安排，恕不接受指定房型。";
            }
            dataList.Add("OthersInfo", othersInfo);
            dataList.Add("OtherMemo", pro.ContractMemo);

            #endregion Part2 好康優惠

            return dataList;
        }

        private void SendCoordinateNullMail(int pid, string mailToUser)
        {
            PponStoreCollection psList = View.GetPponStores(View.BusinessHourGuid);
            string coordinateNullBody = "";
            foreach (PponStore ps in psList)
            {
                Seller s = sp.SellerGet(ps.StoreGuid);
                if (string.IsNullOrEmpty(s.Coordinate))
                {
                    coordinateNullBody += string.Format("店家名稱:{2}, {0}/controlroom/seller/seller_add.aspx?sid={1}", config.SiteUrl, s.Guid, s.SellerName, "<br />");
                }
            }

            if (!string.IsNullOrEmpty(coordinateNullBody))
            {
                MailMessage msg = new MailMessage();
                msg.From = new MailAddress(config.SystemEmail);
                msg.To.Add(mailToUser);
                msg.Subject = string.Format("【請填入經緯度】單號 NO.{0}", pid.ToString());
                msg.Body = "請至以下網址填寫店家經緯度資訊<br />" + coordinateNullBody;
                msg.IsBodyHtml = true;
                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
            }
        }

        /// <summary>
        /// 提案單文案規格內容帶入上檔後台
        /// </summary>
        /// <param name="pro">提案單資訊</param>
        /// <returns></returns>
        private static string SetDescription(Proposal pro)
        {
            string strDescription = "";

            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
            Dictionary<ProposalDealCharacter, string> dealcharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(pro.DealCharacterFlagText);

            //本檔主打星
            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.DealStar))
            {
                strDescription += specialText[ProposalSpecialFlag.DealStar] + "<br />";
                strDescription += "<br />";
            }


            //檔次特色
            string strDealCharacter = "";
            if (pro.DeliveryType == (int)DeliveryType.ToHouse || pro.DealType == (int)ProposalDealType.Travel)
            {

                //宅配or旅遊
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToHouse1))
                {
                    strDealCharacter += "1." + dealcharacterText[ProposalDealCharacter.ToHouse1] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToHouse2))
                {
                    strDealCharacter += "2." + dealcharacterText[ProposalDealCharacter.ToHouse2] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToHouse3))
                {
                    strDealCharacter += "3." + dealcharacterText[ProposalDealCharacter.ToHouse3] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToHouse4))
                {
                    strDealCharacter += "4." + dealcharacterText[ProposalDealCharacter.ToHouse4] + "<br />";
                }
                if (!string.IsNullOrEmpty(strDealCharacter))
                {
                    strDealCharacter = "檔次特色：<br />" + strDealCharacter + "<br />";
                    strDescription += strDealCharacter;
                }
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType != (int)ProposalDealType.Travel && pro.DealType != (int)ProposalDealType.PBeauty)
            {
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToShopFood))
                {
                    strDealCharacter += "菜色：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.ToShopFood] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToShopSpecialFood))
                {
                    strDealCharacter += "特殊食材：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.ToShopSpecialFood] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToShopCook))
                {
                    strDealCharacter += "烹調方式：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.ToShopCook] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToShopEnvironment))
                {
                    strDealCharacter += "環境：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.ToShopEnvironment] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToShopOther))
                {
                    strDealCharacter += "其他：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.ToShopOther] + "<br />";
                }

                if (!string.IsNullOrEmpty(strDealCharacter))
                {
                    strDealCharacter = strDealCharacter + "<br />";
                    strDescription += strDealCharacter;
                }
            }
            else if (pro.DeliveryType == (int)DeliveryType.ToShop && pro.DealType == (int)ProposalDealType.PBeauty)
            {
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.PBeautyDevice))
                {
                    strDealCharacter += "設備/器材：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.PBeautyDevice] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.PBeautyProduct))
                {
                    strDealCharacter += "商品：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.PBeautyProduct] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.PBeautyLocation))
                {
                    strDealCharacter += "地點：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.PBeautyLocation] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.PBeautyEnvironment))
                {
                    strDealCharacter += "環境：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.PBeautyEnvironment] + "<br />";
                }
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.PBeautyOther))
                {
                    strDealCharacter += "其他：";
                    strDealCharacter += dealcharacterText[ProposalDealCharacter.PBeautyOther] + "<br />";
                }

                if (!string.IsNullOrEmpty(strDealCharacter))
                {
                    strDealCharacter = strDealCharacter + "<br />";
                    strDescription += strDealCharacter;
                }
            }

            //規格功能/主要成份
            if (pro.DeliveryType == (int)DeliveryType.ToHouse)
            {
                if (!string.IsNullOrEmpty(pro.ProductSpec))
                {
                    strDescription += "規格功能/主要成份：";
                    strDescription += pro.ProductSpec.Replace("\n", "<br />") + "<br />";
                    strDescription += "<br />";
                }
            }


            if (pro.DeliveryType == (int)DeliveryType.ToHouse && pro.DealType != (int)ProposalDealType.Travel)
            {
                //一般宅配
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Beauty))
                {
                    strDescription += "醫療/妝廣字號：";
                    strDescription += specialText[ProposalSpecialFlag.Beauty] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct))
                {
                    strDescription += "即期品：";
                    strDescription += specialText[ProposalSpecialFlag.ExpiringProduct] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InspectRule))
                {
                    strDescription += "檢驗規定：";
                    strDescription += specialText[ProposalSpecialFlag.InspectRule] + "<br />";
                    strDescription += "<br />";
                }
            }
            else if (pro.DealType == (int)ProposalDealType.Travel)
            {
                //旅遊宅配
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.RecommendFood))
                {
                    strDescription += "推薦菜色/食材：";
                    strDescription += specialText[ProposalSpecialFlag.RecommendFood] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.EnvironmentIntroduction))
                {
                    strDescription += "環境介紹：";
                    strDescription += specialText[ProposalSpecialFlag.EnvironmentIntroduction] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.AroundScenery))
                {
                    strDescription += "周邊景點：";
                    strDescription += specialText[ProposalSpecialFlag.AroundScenery] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.SeasonActivity))
                {
                    strDescription += "季節/配合活動：";
                    strDescription += specialText[ProposalSpecialFlag.SeasonActivity] + "<br />";
                    strDescription += "<br />";
                }
                if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Travel))
                {
                    strDescription += "旅遊業登記：";
                    strDescription += specialText[ProposalSpecialFlag.Travel] + "<br />";
                    strDescription += "<br />";
                }
            }

            //店家/品牌故事
            if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.StoreStory))
            {
                strDescription += "店家/品牌故事：";
                strDescription += specialText[ProposalSpecialFlag.StoreStory] + "<br />";
                strDescription += "<br />";
            }

            //媒體/報導
            if (Helper.IsFlagSet(pro.MediaReportFlag, ProposalMediaReport.Pic))
            {
                strDescription += "媒體/報導：依FTP";
                strDescription += "<br />";
            }
            else if (Helper.IsFlagSet(pro.MediaReportFlag, ProposalMediaReport.Link))
            {
                strDescription += "媒體/報導：" + "<br />";
                List<ProposalMediaReportLink> mediareportText = new JsonSerializer().Deserialize<List<ProposalMediaReportLink>>(pro.MediaReportFlagText);
                foreach (var m in mediareportText)
                {
                    strDescription += m.Link + "　" + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalMediaReport)m.Type) + "<br />";
                }
                strDescription += "<br />";

            }

            return strDescription;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Views;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebLib.Presenters
{
    public class CouponDetailPresenter : Presenter<ICouponDetailView>
    {
        private IPponProvider _pponProv;
        private IOrderProvider _ordProv;
        private ISysConfProvider _config;
        private IMGMProvider mgm;
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IFamiportProvider fp = ProviderFactory.Instance().GetProvider<IFamiportProvider>();

        public CouponDetailPresenter(IPponProvider pponProv, IOrderProvider ordProv)
        {
            _config = ProviderFactory.Instance().GetConfig();
            mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
            _pponProv = pponProv;
            _ordProv = ordProv;

        }

        public override bool OnViewInitialized()
        {
            View.GoogleApiKey = _config.GoogleMapsAPIKey;
            ViewPponCoupon coupon = null;

            if (View.CouponId != null && !string.IsNullOrEmpty(View.SequenceNumber) && !string.IsNullOrEmpty(View.CouponCode) && !string.IsNullOrEmpty(View.CreateTime))
            {
                #region 未登入索取憑證

                coupon = _pponProv.ViewPponCouponGetBySeqCodeCreateTime(View.CouponId ?? 0, View.SequenceNumber, View.CouponCode, View.CreateTime);
                if (!coupon.IsLoaded)
                {
                    return false;
                }
                else
                {
                    if (coupon.CouponId == null)
                    {
                        return false;
                    }
                    else
                    {
                        if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 &&
                            Helper.IsFlagSet(coupon.BusinessHourStatus, BusinessHourStatus.GroupCoupon) &&
                            (coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
                        {
                            var pin = fp.GetFamilyNetPincodeByCouponId(coupon.CouponId ?? 0);
                            if (pin == null || pin.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Completed)
                            {
                                return false;
                            }
                        }
                        if (!string.IsNullOrEmpty(View.MemberName)) 
                        {
                            coupon.MemberName = View.MemberName;
                        }
                        var detail = mp.GetCouponListSequenceByOid(coupon.OrderGuid, (int)coupon.CouponId);
                        var ouponListDetailInfo = OrderFacade.GetCouponListDetailStatus(null, detail);

                        if (!ouponListDetailInfo.IsEnabledDownLoad)
                        {
                            return false; //憑證已失效
                        }
                    }
                }

                #endregion
            }
            else if (!((View.CouponId == null || string.IsNullOrEmpty(View.UserName)) && View.PreviewBizHourId == null))
            {
                #region 登入索取憑證

                if (View.PreviewBizHourId == null)
                {
                    coupon = _pponProv.ViewPponCouponGet(View.CouponId ?? 0);
                    if (!coupon.IsLoaded ||
                        string.Equals(coupon.MemberEmail, View.UserName, StringComparison.OrdinalIgnoreCase) == false)
                    {
                        return false;
                    }
                }
                else if (mgm.GiftGetByCouponId(View.CouponId ?? 0).IsLoaded) //判斷是否為送禮憑證
                {
                    return false;
                }
                else
                {
                    if (View.CouponId != null && !string.IsNullOrEmpty(View.SequenceNumber) &&
                        !string.IsNullOrEmpty(View.CouponCode) && !string.IsNullOrEmpty(View.MemberName))
                    {
                        coupon = _pponProv.ViewPponCouponGet(View.CouponId ?? 0);
                        if (coupon.BusinessHourGuid != View.PreviewBizHourId)
                            return false;
                        coupon.SequenceNumber = View.SequenceNumber;
                        coupon.CouponCode = View.CouponCode;
                        coupon.MemberName = View.MemberName;
                    }
                    else
                    {
                        ViewPponDeal deal = _pponProv.ViewPponDealGetByBusinessHourGuid(View.PreviewBizHourId.Value);
                        if (deal == null || !deal.IsLoaded)
                            return false;

                        if (Mapper.FindTypeMapFor<ViewPponDeal, ViewPponCoupon>() == null)
                            Mapper.CreateMap<ViewPponDeal, ViewPponCoupon>()
                                .ForMember(dest => dest.ItemUnitPrice, opt => opt.MapFrom(src => src.ItemPrice));
                        coupon = Mapper.Map<ViewPponDeal, ViewPponCoupon>(deal);
                        coupon.SequenceNumber = "0000-0000";
                        coupon.CouponCode = "000000";
                        coupon.CouponStoreSequence = null;
                        coupon.MemberName = I18N.Phrase.Unknown;
                        View.CodeType = (CouponCodeType)Enum.ToObject(typeof(CouponCodeType), deal.CouponCodeType);
                    }
                }

                #endregion
            }
            else
            {
                return false;
            }


            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(coupon.BusinessHourGuid);

            View.IsZeroActivityShowCoupon = (vpd != null && vpd.IsLoaded) && ((vpd.IsZeroActivityShowCoupon == true && vpd.ItemPrice == 0));
            View.CodeType = (vpd != null && vpd.IsLoaded) && (vpd.CouponCodeType != null) ?
                (CouponCodeType)Enum.ToObject(typeof(CouponCodeType), vpd.CouponCodeType) : CouponCodeType.None;

            Guid storeGuid = Guid.Empty;
            if (coupon.StoreGuid != null)
            {
                storeGuid = coupon.StoreGuid.Value;
            }
            else
            {
                var stores = pp.ViewPponStoreGetListByBidWithNoLock(coupon.BusinessHourGuid, VbsRightFlag.VerifyShop);
                if (stores.Count > 0)
                {
                    storeGuid = stores[0].StoreGuid;
                }
            }

            if (storeGuid != Guid.Empty)
            {
                string useTime = string.Empty;
                var store = sp.SellerGet(storeGuid);
                ViewPponStore pponStore = ViewPponStoreManager.DefaultManager.ViewPponStoreGet(vpd.BusinessHourGuid, store.Guid);
                if (pponStore.IsLoaded && !string.IsNullOrWhiteSpace(pponStore.UseTime))
                {
                    useTime = pponStore.UseTime;
                }
                var availables = new List<AvailableInformation>();
                var tmp = new AvailableInformation()
                {
                    N = store.SellerName,
                    P = store.StoreTel,
                    A = (CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) + store.StoreAddress),
                    OT = store.OpenTime,
                    UT = useTime,
                    U = store.WebUrl,
                    R = store.StoreRemark,
                    CD = store.CloseDate,
                    MR = store.Mrt,
                    CA = store.Car,
                    BU = store.Bus,
                    OV = store.OtherVehicles,
                    FB = store.FacebookUrl,
                    PL = string.Empty,
                    BL = store.BlogUrl,
                    OL = store.OtherUrl
                };
                availables.Add(tmp);
                View.AvailablesInfo = new JsonSerializer().Serialize(availables);
            }

            long pezcode;

            if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 && long.TryParse(coupon.CouponCode, out pezcode))
            {
                View.EncryptedCouponCode = "--" + new EasyDigitCipher().Encrypt(coupon.CouponCode);
            }
            else if ((coupon.GroupOrderStatus & (int)GroupOrderStatus.PEZevent) > 0)
            {
                if (long.TryParse(coupon.CouponCode, out pezcode))
                    View.EncryptedCouponCode = "--" + new EasyDigitCipher().Encrypt(coupon.CouponCode.Trim());
                else if (Regex.IsMatch(coupon.CouponCode, "[0-9]{4}-[0-9]{4}-[0-9]{6}"))
                    View.EncryptedCouponCode = coupon.CouponCode.Substring(0, 10) + new EasyDigitCipher().Encrypt(coupon.CouponCode.Substring(10, 6));
            }
            else
            {
                View.EncryptedCouponCode = coupon.SequenceNumber + "-" + new EasyDigitCipher().Encrypt(coupon.CouponCode);
            }

            //子檔權益說明改抓母檔
            if (vpd.MainBid != null)
            {
                var mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vpd.MainBid.Value);
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(mainDeal);
                coupon.Introduction = contentLite.Introduction;
            }

            View.SetDetail(coupon, vpd);
            return true;
        }
    }
}

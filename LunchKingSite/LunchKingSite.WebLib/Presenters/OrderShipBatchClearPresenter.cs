﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Presenters
{
    public class OrderShipBatchClearPresenter : Presenter<IOrderShipBatchClearView>
    {
        #region property

        protected static IOrderProvider op;
        protected static IPponProvider pp;
        protected static IVerificationProvider vp;
        
        public OrderShipBatchClearPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized(); 
            SetDealType();
            SetFilterTypes();
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SearchClicked += OnSearchClicked;
            View.OrderShipClearClicked += OnOrderShipClearClicked;
            return true;
        }

        #endregion property

        #region event

        protected void OnSearchClicked(object sender, EventArgs e)
        {
            DateTime? queryStartTime = null, queryEndTime = null;
            var deals = new ViewVbsToHouseDealCollection();
            var dealInfos = new List<ShipDealInfo>();
            var dealInfo = new ShipDealInfo();

            if (!string.IsNullOrEmpty(View.FilterQueryStartTime))
            {
                queryStartTime = DateTime.Parse(View.FilterQueryStartTime);
            }
            if (!string.IsNullOrEmpty(View.FilterQueryEndTime))
            {
                queryEndTime = DateTime.Parse(View.FilterQueryEndTime);
            }

            deals = pp.ViewVbsToHouseDealGetList(View.FilterQueryOption, View.FilterKeyWord, View.FilterQueryTime, queryStartTime, queryEndTime);

            if (deals.Count > 0)
            {
                List<Guid> bids = deals.Where(x => x.DealType.Equals((int)OrderClassification.LkSite)).Select(x => x.ProductGuid).Distinct().ToList();
                List<int> pids = deals.Where(x => x.DealType.Equals((int)OrderClassification.HiDeal)).Select(x => x.ProductId).Distinct().ToList();

                //verification summary (verified, unverified, returned)  
                Dictionary<Guid, VendorDealSalesCount> pponSaleSummary =
                    bids.Count > 0
                    ? vp.GetPponToHouseDealSummary(bids)
                        .ToDictionary(x => x.MerchandiseGuid, x => x)
                    : null;
                Dictionary<Guid, VendorDealSalesCount> piinSaleSummary =
                    pids.Count > 0
                    ? vp.GetHidealToHouseDealSummary(pids)
                        .ToDictionary(x => x.MerchandiseGuid, x => x)
                    : null;

                foreach (var deal in deals)
                {
                    int shipCount = 0;
                    int unshipCount = 0;
                    int returnCount = 0;
                    VendorDealSalesCount dealSaleSummary = new VendorDealSalesCount();

                    if (deal.DealType.Equals((int)OrderClassification.LkSite))
                    {
                        dealSaleSummary = pponSaleSummary.ContainsKey(deal.ProductGuid) ? pponSaleSummary[deal.ProductGuid] : null;
                    }
                    else if (deal.DealType.Equals((int)OrderClassification.HiDeal))
                    {
                        dealSaleSummary = piinSaleSummary.ContainsKey(deal.ProductGuid) ? piinSaleSummary[deal.ProductGuid] : null;
                    }

                    if (dealSaleSummary != null)
                    {
                        shipCount = dealSaleSummary.VerifiedCount;
                        unshipCount = dealSaleSummary.UnverifiedCount;
                        returnCount = dealSaleSummary.ReturnedCount;
                    }

                    dealInfo = new ShipDealInfo
                    {
                        OrderClassification = (int)deal.DealType,
                        ProductGuid = deal.ProductGuid,
                        ProductName = deal.ProductName,
                        UniqueId = deal.ProductId,
                        SellerName = deal.SellerName,
                        UseStartTime = deal.UseStartTime.Value,
                        UseEndTime = deal.UseEndTime.Value,
                        ShipCount = shipCount,
                        UnShipCount = unshipCount,
                        ReturnCount = returnCount,
                        TotalCount = shipCount + unshipCount + returnCount,
                        ShippedDate = deal.ShippedDate //已有壓記出貨回覆日檔次 不允許異動出貨資訊
                    };
                    dealInfos.Add(dealInfo);
                }
            }

            View.SetDealList(dealInfos);
        }

        protected void OnOrderShipClearClicked(object sender, EventArgs e)
        {
            Guid productGuid = new Guid();
            OrderClassification orderClassification = new OrderClassification();
            List<ViewOrderShipList> orderShipList = new List<ViewOrderShipList>();
            OrderShip os = new OrderShip();
            bool result = true;

            if (Guid.TryParse(((RepeaterCommandEventArgs)e).CommandArgument.ToString(), out productGuid))
            {
                if (int.Parse(View.FilterDealType) == (int)DepartmentTypes.Ppon)
                {
                    orderClassification = OrderClassification.LkSite;
                }
                else
                {
                    orderClassification = OrderClassification.HiDeal;
                }


                orderShipList = op.ViewOrderShipListGetListByProductGuid(productGuid, orderClassification)
                                .Where(x =>
                                {
                                    if (!x.OrderShipId.HasValue)
                                    {
                                        return false;
                                    }
                                                                
                                    if (!x.ShipCompanyId.HasValue && string.IsNullOrEmpty(x.ShipNo) && !x.ShipTime.HasValue)
                                    {
                                        return false;
                                    }

                                    return true;
                                }).ToList();
                //抓取訂單換貨記錄
                var exchangeLogs = PponOrderManager.GetHasExchangeLogOrderInfos(new List<Guid> { productGuid });

                using (TransactionScope transScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TransactionOptions { IsolationLevel = IsolationLevel.ReadCommitted }))
                {
                    foreach (ViewOrderShipList item in orderShipList)
                    {
                        //若訂單已有換貨紀錄(排除換貨取消狀態) 則不允許異動原始出貨資訊
                        if (exchangeLogs.Contains(item.OrderGuid))
                        {
                            continue;
                        }

                        os = op.GetOrderShipById(item.OrderShipId.Value);
                        os.ShipCompanyId = null;
                        os.ShipNo = null;
                        os.ShipTime = null;
                        os.ModifyId = View.CurrentUser;
                        os.ModifyTime = DateTime.Now;
                        os.ShipMemo = null;
                        if (!OrderShipUtility.OrderShipUpdate(os))
                        {
                            result = false;
                            break;
                        }
                    }
                    transScope.Complete();
                }

                if (result)
                    View.ShowMessage("刪除成功");
                else
                    View.ShowMessage("刪除失敗");
            }
        }
        #endregion

        #region method

        protected void SetDealType()
        {
            View.SetDealType(View.DealTypeInfos);
        }

        protected void SetFilterTypes()
        {
            View.SetFilterTypeDropDown(View.QueryOptionInfo);
        }

        #endregion
    }
}

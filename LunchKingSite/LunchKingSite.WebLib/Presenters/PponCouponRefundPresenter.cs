﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Presenters
{
    public class PponCouponRefundPresenter : Presenter<IPponCouponRefundView>
    {
        protected IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        protected IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        protected IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected ISysConfProvider conf = ProviderFactory.Instance().GetConfig();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            return true;
        }
        
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.RequestRefund += OnRequestRefund;

            return true;
        }

        private void OnRequestRefund(object sender, DataEventArgs<bool> e)
        {
            View.Message = string.Empty;
            CreateReturnFormResult result;
            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(View.DealGuid);
            string[] orderIds = View.OrderList.Split("\r\n");
            string errMsg = string.Empty;
            bool isRefundSCash = e.Data;
            int? returnFormId = null;

            #region 依憑證編號(好康)退貨

            if (View.DealType == (int)DeliveryType.ToShop)
            {
                //判斷輸入之bid是否為憑證檔次
                if (int.Equals((int)DeliveryType.ToShop, deal.DeliveryType.Value))
                {
                    int refundSuccess = 0;
                    ViewPponCouponCollection tmpCol = pp.ViewPponCouponGetList(ViewPponCoupon.Columns.BusinessHourGuid, View.DealGuid);

                    foreach (var couponNumber in orderIds)
                    {
                        if (!tmpCol.Any(x => x.SequenceNumber == couponNumber))
                        {
                            orderIds = orderIds.Where(val => val != couponNumber).ToArray();
                            errMsg += "查無憑證編號#" + couponNumber + "<br/>";
                        }
                    }

                    var couponOrders = tmpCol.Where(x => orderIds.Contains(x.SequenceNumber))
                                             .GroupBy(x => x.OrderGuid)
                                             .ToDictionary(x => x.Key, x => x.Select(c => new { CouponId = c.CouponId.Value, SequenceNumber = c.SequenceNumber }).ToList());

                    foreach (var orderGuid in couponOrders.Keys)
                    {
                        if (couponOrders[orderGuid].Any())
                        {
                            result = ReturnService.CreateCouponReturnForm(orderGuid, couponOrders[orderGuid].Select(x => x.CouponId), isRefundSCash, View.UserName, View.ReturnReason, out returnFormId, true);
                        }
                        else
                        {
                            result = CreateReturnFormResult.ProductsUnreturnable;
                        }

                        if (result != CreateReturnFormResult.Created)
                        {
                            foreach (var couponNumber in couponOrders[orderGuid].Select(x => x.SequenceNumber))
                            {
                                errMsg += "#" + couponNumber + " 建立退貨單失敗，原因:" + GetCreateReturnFormFailMessage(result) + "<br/>"; 
                            }
                        }
                        else
                        {
                            SetCreateReturnFormSuccessLog(orderGuid, returnFormId.Value);

                            #region 立即退貨

                            ReturnFormEntity returnEntity = ReturnFormRepository.FindById(returnFormId.Value);

                            ReturnService.Refund(returnEntity, View.UserName, true, false);

                            #endregion 立即退貨

                            refundSuccess += returnEntity.ReturnFormRefunds.Where(x => x.IsRefunded == true).Count();

                            if (returnEntity.ReturnFormRefunds.Any(x => x.IsRefunded == false))
                            {
                                List<ReturnFormRefund> returnFormRefunds = returnEntity.ReturnFormRefunds.Where(x => x.IsRefunded == false).ToList();
                                Dictionary<Guid, string> unRefundCouponNumbers = mp.CashTrustLogGetList(returnFormRefunds.Select(x => x.TrustId))
                                                                                     .ToDictionary(x => x.TrustId, x => x.CouponSequenceNumber);

                                foreach (var failRefund in returnFormRefunds)
                                {
                                    errMsg += "#" + unRefundCouponNumbers[failRefund.TrustId] + " 退貨失敗，原因:" + failRefund.FailReason + "<br/>";
                                }
                            }
                        }
                    }

                    errMsg += "共" + refundSuccess.ToString() + "張憑證退貨成功<br/>";
                }
                else
                {
                    errMsg = "輸入之檔次非好康憑證<br/>";
                }
            }

            #endregion 依憑證編號(好康)退貨

            #region 依訂單號碼(商品)退貨

            else if (View.DealType == (int)DeliveryType.ToHouse)
            {
                //判斷輸入之bid是否為商品(非憑證)檔次
                if (!int.Equals(View.DealType, deal.DeliveryType.Value))
                {
                    errMsg = "輸入之檔次非商品訂單<br/>";
                }
                else
                {
                    int refundSuccess = 0;
                    ViewPponOrderCollection tmpOrderCol = pp.ViewPponOrderGetList(1, -1, ViewPponOrder.Columns.OrderId, ViewPponOrder.Columns.BusinessHourGuid + "=" + deal.BusinessHourGuid);

                    foreach (string orderId in orderIds)
                    {
                        //判斷輸入之訂單編號是否與bid中的訂單相符
                        ViewPponOrder c = tmpOrderCol.Where(x => x.OrderId == orderId).FirstOrDefault();
                        if (c != null)
                        {
                            //宅配退貨申請單，抓出 1.非退貨中 2.非已退貨 3.非換貨中 
                            IEnumerable<int> orderProductIds = op.OrderProductGetListByOrderGuid(c.Guid).Where(x => x.IsCurrent && !x.IsReturning && !x.IsReturned && !x.IsExchanging).Select(x => x.Id).ToList();
                            if (orderProductIds.Any())
                            {
                                result = ReturnService.CreateRefundForm(c.Guid, orderProductIds, isRefundSCash, View.UserName, View.ReturnReason,
                                    null, null, null, null, out returnFormId, true);
                            }
                            else
                            {
                                result = CreateReturnFormResult.ProductsUnreturnable;
                            }
                            
                            if (result != CreateReturnFormResult.Created)
                            {
                                errMsg += "#" + orderId + " 建立退貨單失敗，原因:" + GetCreateReturnFormFailMessage(result) + "<br/>"; 
                            }
                            else
                            {
                                SetCreateReturnFormSuccessLog(c.Guid, returnFormId.Value);
                                
                                #region 立即退貨

                                ReturnFormEntity returnEntity = ReturnFormRepository.FindById(returnFormId.Value);

                                ReturnService.Refund(returnEntity, View.UserName, true, false);

                                #endregion 立即退貨

                                if (returnEntity.ReturnFormRefunds.Any(x => x.IsRefunded == true))
                                {
                                    refundSuccess++;
                                }
                                else
                                {
                                    foreach (var failRefund in returnEntity.ReturnFormRefunds.Where(x => x.IsRefunded == false))
                                    {
                                        errMsg += "#" + orderId + " - TrsutId:" + failRefund.TrustId + "退貨失敗，原因:" + failRefund.FailReason + "<br/>";
                                    }
                                }
                            }
                        }
                        else
                        {
                            errMsg += "查無訂單編號#" + orderId + "<br/>";
                        }
                    }

                    errMsg += "共" + refundSuccess.ToString() + "張訂單退貨成功<br/>";
                }

            }

            #endregion 依訂單號碼(商品)退貨
            #region 依訂單號碼(好康)退貨 不檢查bid
            else
            {
                int refundSuccess = 0;
                ViewPponCouponCollection tmpCol = pp.ViewPponCouponGetList(ViewPponCoupon.Columns.OrderId, orderIds);

                foreach (var c in tmpCol.GroupBy(x => new { x.OrderId, x.SequenceNumber }).Select(x => new { OrderId = x.Key.OrderId, SequenceNumber = x.Key.SequenceNumber }))
                {
                    if (string.IsNullOrEmpty(c.SequenceNumber))
                    {
                        errMsg += "非憑證型訂單:" + c.OrderId + "<br/>";
                    }
                }

                var couponOrders = tmpCol.Where(x => !string.IsNullOrEmpty(x.SequenceNumber))
                                            .GroupBy(x => x.OrderGuid)
                                            .ToDictionary(x => x.Key, x => x.Select(c => new { CouponId = c.CouponId.Value, SequenceNumber = c.SequenceNumber }).ToList());

                foreach (var orderGuid in couponOrders.Keys)
                {
                    if (couponOrders[orderGuid].Any())
                    {
                        result = ReturnService.CreateCouponReturnForm(orderGuid, couponOrders[orderGuid].Select(x => x.CouponId), isRefundSCash, View.UserName, View.ReturnReason, out returnFormId, true);
                    }
                    else
                    {
                        result = CreateReturnFormResult.ProductsUnreturnable;
                    }

                    if (result != CreateReturnFormResult.Created)
                    {
                        foreach (var couponNumber in couponOrders[orderGuid].Select(x => x.SequenceNumber))
                        {
                            errMsg += "#" + couponNumber + " 建立退貨單失敗，原因:" + GetCreateReturnFormFailMessage(result) + "<br/>";
                        }
                    }
                    else
                    {
                        SetCreateReturnFormSuccessLog(orderGuid, returnFormId.Value);

                        #region 立即退貨

                        ReturnFormEntity returnEntity = ReturnFormRepository.FindById(returnFormId.Value);

                        ReturnService.Refund(returnEntity, View.UserName, true, false);

                        #endregion 立即退貨

                        refundSuccess += returnEntity.ReturnFormRefunds.Where(x => x.IsRefunded == true).Count();

                        if (returnEntity.ReturnFormRefunds.Any(x => x.IsRefunded == false))
                        {
                            List<ReturnFormRefund> returnFormRefunds = returnEntity.ReturnFormRefunds.Where(x => x.IsRefunded == false).ToList();
                            Dictionary<Guid, string> unRefundCouponNumbers = mp.CashTrustLogGetList(returnFormRefunds.Select(x => x.TrustId))
                                                                                    .ToDictionary(x => x.TrustId, x => x.CouponSequenceNumber);

                            foreach (var failRefund in returnFormRefunds)
                            {
                                errMsg += "#" + unRefundCouponNumbers[failRefund.TrustId] + " 退貨失敗，原因:" + failRefund.FailReason + "<br/>";
                            }
                        }
                    }
                }

                errMsg += "共" + refundSuccess.ToString() + "張憑證退貨成功<br/>";
               
            }
            #endregion 依訂單號碼(好康)退貨 不檢查bid

            View.Message = errMsg;
        }

        private string GetCreateReturnFormFailMessage(CreateReturnFormResult result)
        {
            string errMsg;
            switch (result)
            {
                case CreateReturnFormResult.ProductsUnreturnable:
                    errMsg = "欲退項目的狀態無法退貨";
                    break;
                case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                    errMsg = "ATM訂單, 請先設定ATM退款帳號";
                    break;
                case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                    errMsg = "舊訂單, 請先設定ATM退款帳號";
                    break;
                case CreateReturnFormResult.InvalidArguments:
                    errMsg = "無法建立退貨單, 請洽技術部";
                    break;
                case CreateReturnFormResult.ProductsIsReturning:
                    errMsg = "仍有退款中之退貨單";
                    break;
                case CreateReturnFormResult.OrderNotCreate:
                    errMsg = "訂單未完成付款, 無法建立退貨單";
                    break;
                default:
                    errMsg = "不明錯誤, 請洽技術部";
                    break;
            }

            return errMsg;
        }

        private void SetCreateReturnFormSuccessLog(Guid orderGuid, int returnFormId)
        {
            string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId.ToString(), View.ReturnReason);
            CommonFacade.AddAudit(orderGuid, AuditType.Refund, auditMessage, View.UserName, false);
        }
    }
}

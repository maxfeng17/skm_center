﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class BookingSystemReservationPresenter : Presenter<IBookingSystemReservationView>
    {
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private IBookingSystemProvider bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();

            Seller seller = sp.SellerGet("seller_id", Helper.Decrypt(View.ApiKey));
            if (seller.IsLoaded)
            {
                View.SellerName = seller.SellerName;

                ViewBookingSystemStoreCollection stores = bp.ViewBookingSystemStoreGetBySellerGuid(seller.Guid, BookingType.Coupon);
                if (stores.Any()) 
                {
                    View.SetStoreList(stores);
                    View.FullBookingDate = BookingSystemFacade.GetFullBookingDate(1, View.StoreGuid, BookingType.Coupon);
                }
            }

            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            return true;
        }

        #region method

        #endregion
    }
}

﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesCalendarPresenter : Presenter<ISalesCalendarView>
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ChangePager += OnChangePager;
            return true;
        }

        public SalesCalendarPresenter(IPponProvider pponProv)
        {
            pp = pponProv;
        }

        #region method
        private void LoadData()
        {
            string[] filter = new string[2];
            filter[0] = SalesCalendarEvent.Columns.Type + "=" + (int)SalesCalendarType.Todo;
            filter[1] = SalesCalendarEvent.Columns.CreateUser + "=" + View.UserName;
            SalesCalendarEventCollection cals = pp.SalesCalendarEventGet(View.CurrentPage, 10, filter);

            int TotalCount = pp.SalesCalendarEventGetCount(filter);
            View.SetCalendarContent(cals, TotalCount);
        }
        #endregion

        protected void OnChangePager(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}

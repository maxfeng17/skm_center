﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Mongo.Services;
using LunchKingSite.WebLib.Views;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Bson;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Component;
using System.Web.UI.WebControls;
using System.IO;
using System.Transactions;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Presenters
{
    public class SalesProposalMultiDealsPresenter : Presenter<ISalesProposalMultiDealsView>
    {
        private ISellerProvider sp;
        private IPponProvider pp;
        private IOrderProvider op;
        private IHumanProvider hp;
        private IMemberProvider mp;
        private ISystemProvider sysmp;
        private IItemProvider imp;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            LoadData(View.ProposalId);
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.MultiSave += OnMultiSave;
            View.MultiDelete += OnMultiDelete;
            View.Edit += OnEdit;
            return true;
        }

        public SalesProposalMultiDealsPresenter(ISellerProvider sellerProv, IHumanProvider humProv, IOrderProvider odrProv, IMemberProvider memProv, ISystemProvider sysmProv, IPponProvider pponProv, IItemProvider itemProv)
        {
            sp = sellerProv;
            pp = pponProv;
            hp = humProv;
            op = odrProv;
            mp = memProv;
            sysmp = sysmProv;
            imp = itemProv;
        }

        #region event

        protected void OnMultiSave(object sender, DataEventArgs<int> e)
        {
            //ProposalMultiDealSave(e.Data, false);
        }

        protected void OnMultiDelete(object sender, DataEventArgs<int> e)
        {
            //ProposalMultiDealSave(e.Data, true);
        }

        protected void OnEdit(object sender, DataEventArgs<int> e)
        {
            //Proposal pro = sp.ProposalGet(View.ProposalId);
            //if (pro.IsLoaded)
            //{
            //    List<ProposalMultiDealModel> multiDeals = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(pro.MultiDeals);
            //    if (multiDeals != null && multiDeals.Count > 0 && e.Data > 0)
            //    {
            //        View.SetProposalMultiDealModel(multiDeals.ElementAt(e.Data - 1));
            //    }
            //}
        }

        #endregion

        #region Private Method

        private void LoadData(int pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            View.SetProposalMultiDeals(pro);
        }

        //private void ProposalMultiDealSave(int index, bool isDelete)
        //{
        //    Proposal pro = sp.ProposalGet(View.ProposalId);
        //    Proposal ori = pro.Clone();
        //    if (pro.IsLoaded)
        //    {
        //        ProposalMultiDealModel model = View.GetProposalMultiDealModelData();
        //        List<ProposalMultiDealModel> multiDeals = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(pro.MultiDeals);
        //        if (multiDeals == null)
        //        {
        //            multiDeals = new List<ProposalMultiDealModel>();
        //        }
        //        if (index != 0)
        //        {
        //            multiDeals.RemoveAt(index - 1);
        //        }
        //        if (!isDelete)
        //        {
        //            multiDeals.Add(model);
        //        }
        //        multiDeals = multiDeals.OrderBy(x => x.ItemPrice).ToList();
        //        for (int i = 0; i < multiDeals.Count; i++)
        //        {
        //            multiDeals[i].Id = i + 1;
        //        }
        //        pro.MultiDeals = new JsonSerializer().Serialize(multiDeals);
        //        sp.ProposalSet(pro);

        //        if (pro.MultiDeals != ori.MultiDeals)
        //        {
        //            List<ProposalMultiDealModel> oriMulti = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(ori.MultiDeals);

        //            #region 檢查變更/新增的檔次

        //            foreach (ProposalMultiDealModel m in multiDeals)
        //            {
        //                if (oriMulti != null)
        //                {
        //                    ProposalMultiDealModel orim = oriMulti.Where(x => x.ItemPrice == m.ItemPrice).FirstOrDefault();
        //                    if (orim != null)
        //                    {
        //                        ProposalFacade.CompareMultiDealModel(m, orim, View.ProposalId, View.UserName);
        //                    }
        //                    else
        //                    {
        //                        ProposalFacade.ProposalLog(View.ProposalId, "新增檔次 售價: " + m.ItemPrice, View.UserName);
        //                    }
        //                }
        //                else
        //                {
        //                    ProposalFacade.ProposalLog(View.ProposalId, "新增檔次 售價: " + m.ItemPrice, View.UserName);
        //                }
        //            }

        //            #endregion

        //            #region 檢查刪除的檔次

        //            if (oriMulti != null)
        //            {
        //                foreach (ProposalMultiDealModel orim in oriMulti)
        //                {
        //                    ProposalMultiDealModel m = multiDeals.Where(x => x.ItemPrice == orim.ItemPrice).FirstOrDefault();
        //                    if (m == null)
        //                    {
        //                        ProposalFacade.ProposalLog(View.ProposalId, "檔次刪除 原售價: " + orim.ItemPrice, View.UserName);
        //                    }
        //                }
        //            }

        //            #endregion
        //        }

        //        LoadData(View.ProposalId);
        //    }
        //}

        #endregion
    }
}

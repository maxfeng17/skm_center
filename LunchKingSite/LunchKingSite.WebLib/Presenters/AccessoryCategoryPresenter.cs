﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.Presenters
{
    public class AccessoryCategoryPresenter : Presenter<IAccessoryCategoryView>
    {
        public bool UpdateMessage;

        protected ILocationProvider lp;
        protected ISellerProvider sp;
        protected IItemProvider ip;

        public const int DEFAULT_PAGE_SIZE = 15;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            View.SetAccessoryCategoryList(ip.AccessoryCategoryGetList());
            View.SetAccessoryList(ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid));
            View.SetAccessoryGroupMemberList(SetAccessoryInMember());
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SetAccessoryCategory += OnSetAccessoryCategory;
            View.DeleteAccessoryCategory += OnDeleteAccessoryCategory;
            View.SelectAccessory += OnSelectAccessory;
            View.SetAccessory += OnSetAccessory;
            View.DeleteAccessory += OnDeleteAccessory;
            return true;
        }

        public AccessoryCategoryPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
        }

        protected void OnSetAccessoryCategory(object sender, DataEventArgs<AccessoryCategory> e)
        {
            if (e.Data.IsNew)
                ip.AccessoryCategorySet(e.Data);
            else
            {
                AccessoryCategory info = ip.AccessoryCategoryGet(e.Data.Guid);
                if (info.AccessoryCategoryName != e.Data.AccessoryCategoryName)
                    info.AccessoryCategoryName = e.Data.AccessoryCategoryName;
                if (info.ModifyTime != e.Data.ModifyTime)
                    info.ModifyTime = e.Data.ModifyTime;
                if (info.IsDirty)
                    ip.AccessoryCategorySet(info);
            }
            View.SetAccessoryCategoryList(ip.AccessoryCategoryGetList());
        }

        protected void OnDeleteAccessoryCategory(object sender, DataEventArgs<Guid> e)
        {
            AccessoryCollection acl = ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid);
            foreach(Accessory ac in acl)
                ip.AccessoryDelete(Accessory.Columns.Guid, ac.Guid);

            ip.AccessoryCategoryDelete(AccessoryCategory.Columns.Guid, e.Data);

            View.SetAccessoryCategoryList(ip.AccessoryCategoryGetList());
            View.SetAccessoryList(ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid));
            View.SetAccessoryGroupMemberList(SetAccessoryInMember());
        }

        protected void OnSetAccessory(object sender, DataEventArgs<Accessory> e)
        {
            if (e.Data.IsNew)
                ip.AccessorySet(e.Data);
            else
            {
                Accessory info = ip.AccessoryGet(e.Data.Guid);
                if (info.AccessoryName != e.Data.AccessoryName)
                    info.AccessoryName = e.Data.AccessoryName;
                if (info.ModifyTime != e.Data.ModifyTime)
                    info.ModifyTime = e.Data.ModifyTime;
                if (info.IsDirty)
                    ip.AccessorySet(info);
            }
            View.SetAccessoryList(ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid));
        }

        protected void OnSelectAccessory(object sender, DataEventArgs<Guid> e)
        {
            View.SetAccessoryList(ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, e.Data));
            View.SetAccessoryGroupMemberList(SetAccessoryInMember());
        }

        protected void OnDeleteAccessory(object sender, DataEventArgs<Guid> e)
        {
            if(ip.AccessoryDelete(Accessory.Columns.Guid, e.Data))
                View.SetAccessoryList(ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid));
        }

        private bool SetAccessoryInMember()
        {
            AccessoryCollection AccessoryList = ip.AccessoryGetList(Accessory.Columns.AccessoryCategoryGuid, View.AccessoryCategoryGuid);
            foreach (Accessory ac in AccessoryList)
            {
                if (ip.AccessoryGroupMemberGetList(AccessoryGroupMember.Columns.AccessoryGuid, ac.Guid).Count > 0)
                    return false;
            }
            return true;
        }
    }
}

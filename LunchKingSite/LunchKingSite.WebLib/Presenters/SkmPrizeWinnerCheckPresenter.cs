﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Views;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.BizLogic.Model;
using System.Net.Mail;
using System.Web;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebLib.Presenters
{
    public class SkmPrizeWinnerCheckPresenter : Presenter<ISkmPrizeWinnerCheckView>
    {
        private ISysConfProvider _cp;
        private IOrderProvider _op;
        private IEventProvider _ep;
        private IMemberProvider _mp;
        private ILocationProvider _lp;

        public SkmPrizeWinnerCheckPresenter(ISysConfProvider cp, IOrderProvider op, IEventProvider ep, IMemberProvider mp, ILocationProvider lp)
        {
            _cp = cp;
            _op = op;
            _ep = ep;
            _mp = mp;
            _lp = lp;
        }

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            // user is not logged in
            //if (string.IsNullOrEmpty(View.UserName))
            //{
            //    View.RedirectToLogin();
            //}
            return true;
        }

        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.ShowEditContent();
            //View.Exchange += OnExchange;
            View.SendWinnerInfo += OnSendWinnerInfo;
            return true;
        }

        protected void OnSendWinnerInfo(object sender, DataEventArgs<SkmPrizeWinnerData> e)
        {
           SkmEventPrizeWinner skmEventPrizeWinner = PromotionFacade.GetSkmEventPrizeWinner(e.Data.PrizeName, e.Data.PrizeIDNumber);

            if (skmEventPrizeWinner != null && skmEventPrizeWinner.IsLoaded)
            {

                if (skmEventPrizeWinner.IsUsed)
                {
                    View.ShowNameError("此姓名已領取過獎項");
                    View.ShowIDNumberError("此身分證字號已領取過獎項");
                }
                else
                {
                    skmEventPrizeWinner.IsUsed = true;
                    skmEventPrizeWinner.VerificationName = e.Data.PrizeName;
                    skmEventPrizeWinner.VerificationIdNumber = e.Data.PrizeIDNumber;
                    skmEventPrizeWinner.VerificationEmail = e.Data.PrizeEmail;
                    skmEventPrizeWinner.VerificationTime = DateTime.Now;

                    _ep.SkmEventPrizeWinnerSet(skmEventPrizeWinner);

                    View.ShowSuccessContent(e.Data.PrizeEmail);
                }
            }
            else
            {
                View.ShowEditContent(true);
                View.ShowNameError("此姓名並非17Life折價券$50得獎人");
                View.ShowIDNumberError("此身分證字號並非17Life折價券$50得獎人");
            }
        }
    }
}

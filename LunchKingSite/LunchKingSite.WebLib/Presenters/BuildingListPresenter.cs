﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Data.SqlTypes;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Views;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.WebLib.Presenters
{
    public class BuildingListPresenter : Presenter<IBuildingListView>
    {
        protected ILocationProvider ilp = null;
        protected ISellerProvider sp = null;

        public override bool OnViewInitialized()
        {
            base.OnViewInitialized();
            SetBuildingList(1);
            SetCityList();
            SetZoneList();
            SetFilterTypes();
            SetStatusFilter();
            return true;
        }
        public override bool OnViewLoaded()
        {
            base.OnViewLoaded();
            View.SortClicked += OnGridUpdated;
            View.PageChanged += OnPageChanged;
            View.GetBuildingCount += OnGetBuildingCount;
            View.SearchClicked += OnGridUpdated;
            View.SelectCityChanged += OnSelectCityChanged;
            return true;
        }

        public BuildingListPresenter()
        {
            SetupProviders();
        }

        protected void SetupProviders()
        {
            ilp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        }

        protected string[] GetFilter()
        {
            string[] filter = new string[3];
            if (!string.IsNullOrEmpty(View.Filter))
            {
                filter[0] = View.Filter;
                filter[0] = filter[0].Replace("*", "%");
                filter[0] = filter[0].Replace("?", "_");
                filter[0] = View.FilterType + " like %" + filter[0] + "%";
            }

            if (View.SelectCity != null)
                filter[1] = View.SelectCity.Value.Key + " = " + View.SelectCity.Value.Value;

            filter[2] = Building.Columns.BuildingOnline + " = " + View.Status;
            return filter;
        }

        protected void OnGetBuildingCount(object sender, DataEventArgs<int> e)
        {
            e.Data = GetBuildingList(1, -1).Count;
        }

        protected void OnGridUpdated(object sender, EventArgs e)
        {
            SetBuildingList(1);
        }

        protected void OnPageChanged(object sender, DataEventArgs<int> e)
        {
            SetBuildingList(e.Data);
        }

        protected void OnSelectCityChanged(object sender, EventArgs e)
        {
            SetZoneList();
        }

        protected void SetBuildingList(int pageNumber)
        {
            View.SetBuildingList(GetBuildingList(pageNumber, View.PageSize));
        }

        protected ViewBuildingCityCollection GetBuildingList(int pageNumber, int pageSize)
        {
            ViewBuildingCityCollection buildingCityList;
            SqlGeography geo = SqlGeography.STPointFromText(new SqlChars((!string.IsNullOrEmpty(sp.SellerGet(View.SellerGuid).Coordinate)) ? sp.SellerGet(View.SellerGuid).Coordinate : "POINT EMPTY"), 4326);

            if (View.BusinessHourBindMode == ShowListMode.All)
                buildingCityList = ilp.ViewBuildingCityGetListPaging(pageNumber, pageSize, ViewBuildingCity.Columns.BuildingRank, GetFilter());
            else
                buildingCityList = ilp.ViewBuildingCityGetListByBusinessHour(pageNumber, pageSize, View.SortExpression, View.BusinessHourGuid, View.BusinessHourBindMode, geo, View.Distance, GetFilter());
            return buildingCityList;
        }

        protected void SetFilterTypes() 
        {
            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add(ViewBuildingCity.Columns.BuildingName, View.Localization.GetString("RoadName"));
            View.SetFilterTypeDropDown(data);
        }

        protected void SetStatusFilter()
        {
            Dictionary<bool, string> data = new Dictionary<bool, string>();
            data.Add(true, View.Localization.GetString("Online"));
            data.Add(false, View.Localization.GetString("Offline"));
            View.SetStatusFilter(data);
        }

        protected void SetZoneList()
        {
            var zone = View.SelectCity == null ? new List<City>() : ilp.CityGetList(View.SelectCity.Value.Value, City.Columns.Rank).ToList();
            zone.Insert(0, new City { CityName = "全部", Id = 0 });
            View.SetZoneDropDown(zone);
        }

        protected void SetCityList()
        {
            var llcCity = ilp.CityGetListTopLevel().ToList();
            llcCity.Insert(0, new City { CityName = "全部區域", Id = 0 });
            View.SetCityDropDown(llcCity);
        }
    }
}

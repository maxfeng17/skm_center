﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface ISystemFunctionAddView
    {
        #region props
        SystemFunctionAddPresenter Presenter { get; set; }
        string LoginUser { get; }
        int PageSize { get; set; }
        string DisplayNameSearch { get; }
        string FuncNameSearch { get; }
        string ParentFuncSearch { get; }
        #endregion

        #region event
        event EventHandler<EventArgs> SearchClicked;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler<DataEventArgs<int>> GetSystemFunctionData;
        event EventHandler<DataEventArgs<int>> SystemFunctionDelete;
        event EventHandler<DataEventArgs<List<SystemFunction>>> SystemFunctionSave;
        #endregion

        #region method
        void GetSystemFunctionList(SystemFunctionCollection functionList);
        void SetSystemFunctioData(SystemFunction systemFunction);
        void ShowMessage(string msg);
        #endregion

        #region private method
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IMiniBankManageView : ILocalizedView
    {
        MiniBankManagePresenter Presenter { get; set; }
        string OwnerUserName { get; }
        int UserId { get; }
        int? DetailId { get; set; }

        void SetAccountGrid(ViewMiniBankSummaryCollection data);
        void SetDetailGrid(ViewMiniBankTransactionCollection data);

        event EventHandler<DataEventArgs<KeyValuePair<string, decimal>>> UpdateAccount;
        event EventHandler<DataEventArgs<int>> DeleteAccount;
        event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> ShowTransactionDetail;
        event EventHandler CloseTransactionDetail;
    }
}

﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IVboBannerView
    {
        VboBannerPresenter Presenter { get; set; }
        void FillPreviewData(IList<ViewCmsRandom> items);
        string OutputFormat { get; }
        void OutputJsonp(string json);

        string CallbackKey { get; }
    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Models.Ppon;
using Newtonsoft.Json;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponBuyView
    {
        event EventHandler<DataEventArgs<PponPaymentDTO>> CheckOut;

        event EventHandler<GuestMemberEventArgs> GetOrAddGuestMemberThenSetInfo;

        event EventHandler ResetContent;

        PponBuyPresenter Presenter { get; }

        Guid BusinessHourGuid { get; set; }

        string SessionId { get; }

        string UserName { get; }

        int UserId { get; }

        bool IsAuthenticated { get; }

        string BackupTicketIdForHistoryBack { get; set; }

        OrderInstallment Installment { get; set; }

        string CreditcardsAllowInstallment { get; set; }

        bool TaishinCreditCardOnly { get; }

        string TaishinCreditcards { get; set; }

        double bcash { get; set; }

        decimal scash { get; set; }

        string UserMemo { get; }

        string ItemOptionsJson { get; set; }

        string BuyerInfoJson { get; set; }

        string OtherInfoJson { get; set; }

        Guid SelectedStoreGuid { get; }

        string GtagDataJson { get; set; }

        string GtagConversionJson { get; set; }

        string UrADGtagConversionJson { get; set; }

        string YahooDataJson { get; set; }

        string ScupioJson { get; set; }

        string ScupioCheckoutPageJson { get; set; }

        CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa { get; set; }

        string MemberLastProductDeliveryType { get; set; }

        PponDeliveryInfo DeliveryInfo { get; }

        string TicketId { get; set; }

        KeyValuePair<CarrierType, string> Carrier { get; set; }

        string ReferrerSourceId { get; }

        string CookieIChannelGid { get; }

        string CookieShopBackGid { get; }

        string CookiePezChannelGid { get; }

        string CookieLineShopGid { get; }

        string CookieAffiliatesGid { get; }

        IViewPponDeal TheDeal { get; set; }

        bool AllowGuestBuy { get; set; }

        bool IsBankDeal { get; set; }

        int CookieCityId { get; }

        string CookieReferenceId { get; }

        PaymentResultPageType PaymentResult { get; set; }

        string RsrcSession { get; }

        bool IsPreview { get; }

        string DiscountCode { get; }

        bool IsApplePay { get; set; }

        bool IsLineCancel { get; }

        bool IsTaishinPayFailure { get; }

        ThirdPartyPayment ThirdPartyPaymentSystem { get; }

        string LineTransactionId { get; }

        BuyAddressInfo AddressInfo { get; }

        string UserInfo { get; set; }

        CouponFreightCollection TheCouponFreight { get; set; }

        bool IsShoppingCart { get; set; }

        EntrustSellType EntrustSell { get; set; }

        string MasterPass_OAuthToken { get; }

        string MasterPass_OAuthVerifier { get; }

        string MasterPass_CheckoutResourceUrl { get; }

        string MasterPassPairingVerifier { get; }

        string MasterPassPairingToken { get; }

        bool IsMasterPassCancel { get; }

        decimal BaseGrossMargin { set; get; }

        Guid MainGuid { set; get; }

        EinvoiceTriple TripleData { set; get; }

        void ReEnterPaymentGate(PaymentType paymentType, string transId);

        void RestartBuy(bool isUseDiscountCode, Guid bid, string alertMessage);

        void RestartAtmToCreditcard(string message = "");

        void ShowTransactionPanel(PponPaymentDTO paymentDTO);

        void ShowATMTransactionPanel(PponPaymentDTO paymentDTO);

        void ShowResult(PaymentResultPageType result, MemberLinkCollection mlc, PponPaymentDTO paymentDTO,
            IViewPponDeal pponDeal, int atmAmount, string reason = null, Order o = null);

        void ShowCreditcardChargeInfo();

        void GotoPay(string ticketId);

        void GotoMasterPass(MasterPassData data, string ticketId);

        // add end

        void SetContent(IViewPponDeal deal, ViewMemberBuildingCity deliveryInfo, AccessoryGroupCollection multOption,
            int AlreadyBoughtCount, ViewPponStoreCollection stores, bool isUseDiscountCode,
            EntrustSellType entrustSell, bool isDailyRestriction, EinvoiceTriple EinvoiceTriple,
            IEnumerable<ViewDiscountDetail> discountList,
            bool installment3Months, bool installment6Months, bool installment12Months,
            List<BuyPromotionText> buyPromotionTexts, bool isATMAvailable,
            PponPickupStores pickupStores, ProductDeliveryType productDeliveryInfo, int ispQuantityLimit,
            ProductDeliveryType? memberLastProductDeliveryType);

        void FillContentData(PponPaymentDTO dto);

        void AlertMessage(string msg);

        void AlertMessageAndGoToDefault(string msg, Guid bid);

        void ShowMessageAndLogout(string msg);

        /// <summary>
        /// 顯示訊息，然後回好康Default頁面，若message 為空，則直接回好康Default頁
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="message"></param>
        void GoToPponDefaultPage(Guid bid, string message, bool isdefault);

        void RedirectToLogin();

        void CheckRefUrl();

        void GoToZeroActivity();

        void GoToKindPay();

        void RedirectToLoginOnlyPEZ(string message, Guid bid);

        bool NotDeliveryIslands { get; set; }

        /// <summary>
        /// 埋Code
        /// </summary>
        string OrderArgs { set; get; }

        void RedirectToAppLimitedEdtionPage();

        int GuestMemberId { get; set; }

        void SetThirdPartyPayFailContent(string reason, PponPaymentDTO paymentDTO);

        void GoToLinePay(string url);

        void SetMasterPassPreCheckoutData(MasterPassPreCheckoutData preCheckoutData);

        void GoToTaishinPay(string payCode, string shortOrderId);

        Guid BuyTransactionGuid { get; }

        BuyTransPaymentMethod BuyTransPaymentMethod { get; set; }

        bool IsSaveCreditCardInfo { get; }

        string CreditCardName { get; }

        PponPickupStore GetPponPickupStoreFromRequest();
        void RedirectToOTPPage(string url);
    }

    public class CheckDataEventArgs : EventArgs
    {
        public int Quantity { get; set; }

        public int BCash { get; set; }

        public int SCash { get; set; }

        public bool PaidByAtm { get; set; }

        public int SubTotal { get; set; }

        public bool IsMasterPass { get; set; }

        public bool IsThirdPartyPay { set; get; }

        public ThirdPartyPayment ThirdPartyPaymentSystem { set; get; }

        public PponDeliveryInfo Info { get; set; }
    }

    public class ShowResultEventArgs : EventArgs
    {
        public PaymentResultPageType PageType { get; set; }
    }

    public class GuestMemberEventArgs : EventArgs
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string Mobile { get; set; }
    }

    public class MemoryCart
    {
        public List<MemoryCartItem> Items { get; set; }
        public int Version { get; set; }
    }

    public class MemoryCartItem
    {
        [JsonProperty("qty")]
        public int Qty { get; set; }
        [JsonProperty("opts")]
        public List<Guid> Opts { get; set; }
        [JsonProperty("bid")]
        public string Bid { get; set; }
    }
}
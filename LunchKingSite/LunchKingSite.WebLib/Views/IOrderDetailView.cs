using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IOrderDetailView
    {
        event EventHandler<DataEventArgs<UpdatableOrderInfo>> UpdateOrderInfo;
        event EventHandler<DataEventArgs<OrderDetail>> UpdateOrderDetail;
        event EventHandler<DataEventArgs<OrderShipArgs>> UpdateOrderShip;
        event EventHandler<DataEventArgs<OrderShipArgs>> DeleteOrderShip;
        event EventHandler<DataEventArgs<OrderShipArgs>> AddOrderShip;
        event EventHandler<DataEventArgs<int>> GetOrderDetailCount;
        event EventHandler<DataEventArgs<int>> OrderDetailPageChanged;
        event EventHandler SendRefundResult;
        event EventHandler<DataEventArgs<string>> ReturnApplicationResend;
        event EventHandler<DataEventArgs<string>> ReturnDiscountResend;
        event EventHandler<DataEventArgs<int>> GetCashTrustLog;
        event EventHandler CouponAvailableToggle;
        event EventHandler<DataEventArgs<Dictionary<int, string>>> CouponBound;
        event EventHandler<DataEventArgs<int>> GetSMS;
        event EventHandler RecoveredConfirm;
        event EventHandler<DataEventArgs<int>> InvoiceMailBackSet;
        event EventHandler<DataEventArgs<CtAtmRefund>> SetAtmRefund;
        event EventHandler<DataEventArgs<ServiceMessage>> ServiceMsgSet;
        event EventHandler<DataEventArgs<int>> ReceiptMailBackSet;
        event EventHandler<DataEventArgs<int>> ReceiptMailBackAllowanceSet;
        event EventHandler<DataEventArgs<int>> ReceiptMailBackPaperSet;
        event EventHandler CashTrustlogFilled;
        event EventHandler<ReturnFormArgs> CreateReturnForm;
        event EventHandler<ReturnFormReceiverArgs> UpdateReturnFormReceiver;
        event EventHandler<CouponReturnFormArgs> CreateCouponReturnForm;
        event EventHandler<DataEventArgs<int>> ImmediateRefund;
        event EventHandler<DataEventArgs<int>> ReceivedCreditNote;
        event EventHandler<DataEventArgs<int>> ChangeToElcAllowance;
        event EventHandler<DataEventArgs<int>> ChangeToPaperAllowance;
        event EventHandler<DataEventArgs<int>> CancelReturnForm;
        event EventHandler<DataEventArgs<int>> UpdateAtmRefund;
        event EventHandler<DataEventArgs<int>> RecoverVendorProgress;
        event EventHandler<DataEventArgs<int>> ProcessedVendorUnreturnable;
        event EventHandler<DataEventArgs<int>> SCashToCash;
        event EventHandler<DataEventArgs<int>> CashToSCash;
        event EventHandler<DataEventArgs<int>> ArtificialRefund;
        event EventHandler<DataEventArgs<int>> RecoverToRefundScash;
        event EventHandler<DataEventArgs<string>> RequestInvoice;
        event EventHandler<DataEventArgs<OrderUserMemoList>> OrderUserMemoSet;
        event EventHandler<DataEventArgs<int>> OrderUserMemoDelete;
        event EventHandler CreditcardReferSet;
        event EventHandler<DataEventArgs<OrderFreezeInfo>> OrderFreezeSet;
        event EventHandler<ExchangeLogArgs> CreateExchangeLog;
        event EventHandler<UpdateExchangeLogArgs> UpdateExchangeLog;
        event EventHandler<DataEventArgs<int>> CompleteExchangeLog;
        event EventHandler<DataEventArgs<int>> CancelExchangeLog;
        event EventHandler<DataEventArgs<Guid>> CancelOrder;
        event EventHandler<DataEventArgs<Guid>> WithdrawCancelOrder;
        event EventHandler<DataEventArgs<int>> DownloadCouponZip;

        OrderDetailPresenter Presenter { get; }
        IAuditBoardView AuditBoardView { get; }
        string CurrentUser { get; }
        Guid OrderId { get; set; }
        Guid BusinessHourGuid { get; set; }
        int CashTrustLogStatus { get; set; }
        int ShipMemoLimit { get; }
        bool OldInvoiceAllowance { get; set; }
        bool OldInvoicePaper { get; set; }
        bool OldReturnPaper { get; set; }
        string OrderSituation { get; set; }
        //string ReturnMessage { get; }
        string alertMessage { set; }
        List<string> alertMultiMessage { set; }
        bool IsExpired { get; set; }
        bool IsCoupon { get; set; }
        string DealFreeze { set; }
        bool FreezeVisible { set; }
        bool CancelFreezeVisible { set; }
        CashTrustLog CtLog { get; set; }
        int TotalQuantity { get; set; }
        int SmsCount { get; set; }
        string RecoveredInfo { get; set; }
        EinvoiceMain Invoice { get; set; }
        EntrustSellReceipt Receipt { get; set; }
        bool IsEntrustSell { get; set; }
        string MemberEmail { get; set; }
        bool AllowanceSentBack { get; set; }
        bool InvoiceSentBack { get; set; }
        CtAtmRefundCollection AtmRefundCol { get; set; }
        ShipCompanyCollection ShipCompanyCol { get; set; }
        bool IsUserInRole(string role);
        bool IsToHouse { get; set; }
        int GoodsReturnStatus { get; set; }
        DateTime ShipStartDate { get; set; }
        bool CheckIsShipable { get; set; }
        bool CheckIsExchanging { get; set; }
        DropDownList ddlServiceCategory { get; }
        DropDownList ddlServiceSubCategory { get; }
        bool IsPaperAllowance { get; set; }
        bool IsEnableElecAllowance { get; set; }
        List<EinvoiceFacade.EinvoiceChangeLogMain> EinvoiceChangeLogMainData { get; set; }

        void ShowHomeDeliveryInfo();
        void ShowCouponInfo();

        void SetOrderInfo(ViewOrderMemberBuildingSeller odr, ViewPponDeal deal, ViewPponOrderStatusLog firstOrderLog,
            ViewPponOrderStatusLog lastOrderLog, ViewPponCouponCollection coupons, PaymentTransactionCollection paymentsPT,
            ViewOrderMemberPaymentCollection paymentsCS, EinvoiceMain einvoice, EntrustSellType entrustSell,
            EntrustSellReceipt receipt, DealProperty property, GroupOrder go, BankInfoCollection bks,
            DateTime newInvoiceDate, DealAccounting accounting, EinvoiceMainCollection einvCol, CashTrustLogCollection ctls, OrderCorresponding oc, bool is_masterpass,
            VendorPaymentChangeCollection vpcs, Order o);
        void SetOrderStatusList(Dictionary<int, string> data, int selected, bool isCanceling);
        void SetDeliverData(ViewOrderMemberBuildingSeller orderMemberBuilding, ViewOrderShipListCollection osCol);
        void SetOrderItemList(ViewPponOrderDetailCollection odrCol);
        void SetOrderStatusLog(OrderStatusLogCollection ol);
        void SetOrderExportLog(string exportlog);
        void SetIsCanceling(bool isCanceling);
        /// <summary>
        /// 顯示資策會訂單的資料
        /// </summary>
        /// <param name="exportlog"></param>
        void SetIDEASOrderData(CompanyUserOrder order);
        void Exit();

        //config.EnabledExchangeProcess:false
        Dictionary<string, string> FilterTypes { get; }
        //config.EnabledExchangeProcess:false
        void SetFilterTypeDropDown(Dictionary<string, string> types);

        void SetHomeDeliveryProductInfo();
        void SetHomeDeliverReturnInfo();
        void SetCouponReturnInfo();
        void SetCouponInfo(ViewPponCouponCollection coupons);
        void SetOrderUserMemoInfo(OrderUserMemoListCollection orderUserMemos);
        void ReturnFormCreated(DeliveryType deliveryType);
        void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates, DropDownList ddl);
        void SetHomeDeliverExchangeInfo();
        void SetVendorFine();
        void DownloadZip(byte[] zipFile);
    }

    public sealed class UpdatableOrderInfo
    {
        #region props

        public string CustomerPhone { get; set; }

        public string CustomerMobile { get; set; }

        public string DeliveryAddress { get; set; }

        public string ReviewReply { get; set; }

        public string OrderMemo { get; set; }

        public string UserMemo { get; set; }

        public string UniCode { get; set; }

        public string Title { get; set; }

        public int DrType { get; set; }

        public string InvoiceType { get; set; }

        public CarrierType CarrierType { get; set; }
        public string CarrierId { get; set; }

        public bool IsUpdateInvoice { get; set; }
        public string EinvoiceId { get; set; }
        public string EinvoiceName { get; set; }
        public string EinvoiceAddress { get; set; }
        public string EinvoiceMessage { get; set; }
        public bool InvoiceMailbackAllowance { get; set; }  //折讓單
        public bool InvoiceMailbackPaper { get; set; }      //發票
        public bool ReturnPaper { get; set; }               //退貨申請書
        //config.EnabledExchangeProcess = false
        public int ReturnSituation { get; set; }
        public string ReturnReason { get; set; }

        #region 代收轉付 props

        //己開立代收轉付收據
        public bool ReceiptIsGenerated { get; set; }
        //代收轉付收據號碼
        public string ReceiptCode { get; set; }
        //代收轉付收據註記
        public string ReceiptRemark { get; set; }
        //需寄送收據
        public bool ReceiptIsPhysical { get; set; }
        //買受人
        public string ReceiptTitle { get; set; }
        //地址
        public string ReceiptAddress { get; set; }
        //統編
        public string ReceiptNumber { get; set; }
        //折讓單已寄回
        public bool ReceiptMailbackAllowance { get; set; }
        ///收據已寄回
        public bool ReceiptMailbackReceipt { get; set; }
        //申請書已寄回
        public bool ReceiptMailbackPaper { get; set; }

        #endregion 代收轉付 props

        #endregion
    }

    public sealed class OrderShipArgs
    {
        public int Id { get; set; }
        public int ShipCompanyId { get; set; }
        public DateTime ShipTime { get; set; }
        public string ShipNo { get; set; }
        public string ShipMemo { get; set; }
    }

    public sealed class ReturnFormArgs : EventArgs
    {
        public Guid OrderGuid { get; set; }
        public IEnumerable<int> OrderProductIds { get; set; }
        public bool RefundSCash { get; set; }
        public string RefundReason { get; set; }
        public bool IncludeFreight { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public Guid Bid { get; set; }
        public AgentChannel ChannelAgent { get; set; }
    }

    public sealed class ReturnFormReceiverArgs : EventArgs
    {
        public int ReturnFormId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
    }

    public sealed class CouponReturnFormArgs : EventArgs
    {
        public Guid OrderGuid { get; set; }
        public bool RefundSCash { get; set; }
        public string RefundReason { get; set; }
        public IEnumerable<int> CouponIds { get; set; }
    }

    public sealed class InvoiceMessageArgs
    {
        public string InvoiceMessage { get; set; }
        public int CouponId { get; set; }

        public InvoiceMessageArgs(int CouponId, string InvoiceMessage)
        {
            this.CouponId = CouponId;
            this.InvoiceMessage = InvoiceMessage;
        }
    }


    public sealed class OrderFreezeInfo
    {
        public Guid OrderGuid { get; set; }
        public FreezeType FreezeType { get; set; }
        public string Reason { get; set; }
        public string UserId { get; set; }
    }

    public sealed class ExchangeLogArgs : EventArgs
    {
        public Guid OrderGuid { get; set; }
        public string Reason { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
        public Guid Bid { get; set; }
    }

    public sealed class UpdateExchangeLogArgs : EventArgs
    {
        public int ExchangeLogId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface INotClaimedView
    {
        #region property
        NotClaimedPresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }

        int SearchCategory { get; }
        Guid SearchOrderGuid { get; }
        string SearchServiceNo { get; }
        string Mail { get; }
        string SDate { get; }
        string EDate { get; }
        string ChoseBox { get; }
        int UserId { get; }

        #endregion
        #region event
        event EventHandler AddClaimed;
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> ClaimedCount;
        event EventHandler<DataEventArgs<int>> ClaimedPageChanged;

        #endregion

        #region method
        void MessageList(ViewCustomerServiceListCollection MessageList);
        #endregion

    }

}

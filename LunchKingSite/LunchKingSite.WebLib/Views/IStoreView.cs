using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.WebLib.Views
{
    public interface IStoreView
    {
        StorePresenter Presenter { get; set; }
        int CityId { get; set; }
        int DealCityId { get; set; }
        int TravelCategoryId { get; }
        int FemaleCategoryId { get; }
        int CategoryID { get; }
        int SubRegionCategoryId { get; }

        bool ShowEventEmail { get; set; }
        string SellerIntroduct { get; set; }
        string SellerName { get; set; }

        Guid SellerGuid { get; }

        void SetCity(int value);
        void SetSellerStoreList(string Availability); //設定分店資訊

        void SetMutilpStoreDeals(ViewPponDealCollection storedeals);
        void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> hotdeals);
        void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals);
        void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals);

        //跳窗
        void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IGiftBannerView
    {
        #region property
        GiftBannerPresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }

        int Id { get;}
        string TxtTitle { get; }
        string Content { get; }
        string ImageUrl { get; }
        FileUpload Image { get; }
        string TextColor { get; }
        int ShowType { get; }
        bool Online { get; }

        string NoGiftOn { get; }
        string HasGiftOn { get; }


        string SearchTitle { get; }
        int SearchOnline { get; }
        int SearchShowType { get; }
        int Searchbtn { get;}
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<string>> Save;
        event EventHandler<DataEventArgs<int>> MgmGiftHeaderListGet;
        event EventHandler<DataEventArgs<int>> MgmGiftHeaderListGetCount;
        event EventHandler<DataEventArgs<int>> MgmGiftHeaderListPageChanged;
        event EventHandler<DataEventArgs<int>> MgmGiftHeaderListDelete;
        #endregion

        #region method
        void SetMgmHeaderList(MgmGiftHeaderCollection mgmGiftHeaderList);

        void MgmGiftHeaderEdit(MgmGiftHeader item);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;


namespace LunchKingSite.WebLib.Views
{
    public interface IBuildingAddView
    {
        event EventHandler<DataEventArgs<UpdatableBuildingInfo>> UpdataBuildingInfo;
        event EventHandler SelectCityChanged;
        event EventHandler<DataEventArgs<BuildingDelivery>> UpdateBuildingDelivery;
        event EventHandler<DataEventArgs<BuildingDelivery>> DeleteBuildingDelivery;
        event EventHandler DeleteBuilding;

        BuildingAddPresenter Presenter { get; }
        Guid BuildingGuid { get; }
        int SelectCity { get; }

        void SetZoneDropDown(Dictionary<int, string> zone);
        void SetCityDropDown(Dictionary<int, string> city);
        void SetBuildingBind(ViewBuildingCity BuildingBind);
    }

    public sealed class UpdatableBuildingInfo
    {
        #region props
        private Guid _BuildingGuid;
        public Guid BuildingGuid
        {
            get { return _BuildingGuid; }
            set { _BuildingGuid = value; }
        }

        private string _BuildingStreetName;
        public string BuildingStreetName
        {
            get { return _BuildingStreetName; }
            set { _BuildingStreetName = value; }
        }

        private string _BuildingAddressNumber;
        public string BuildingAddressNumber
        {
            get { return _BuildingAddressNumber; }
            set { _BuildingAddressNumber = value; }
        }

        private string _BuildingName;
        public string BuildingName
        {
            get { return _BuildingName; }
            set { _BuildingName = value; }
        }
        private int _BuildingRank;
        public int BuildingRank
        {
            get { return _BuildingRank; }
            set { _BuildingRank = value; }
        }
        private string _BuildingId;
        public string BuildingId
        {
            get { return _BuildingId; }
            set { _BuildingId = value; }
        }
        public int CityId { get; set; }

        private bool _BuildingOnline;
        public bool BuildingOnline
        {
            get { return _BuildingOnline; }
            set { _BuildingOnline = value; }
        }
        private string _BuildingAtitude;
        public string BuildingAtitude
        {
            get { return _BuildingAtitude; }
            set { _BuildingAtitude = value; }
        }
        private string _BuildingLongitude;
        public string BuildingLongitude
        {
            get { return _BuildingLongitude; }
            set { _BuildingLongitude = value; }
        }
        private string _CreateId;
        public string CreateId
        {
            get { return _CreateId; }
            set { _CreateId = value; }
        }
       
        #endregion
    }
}

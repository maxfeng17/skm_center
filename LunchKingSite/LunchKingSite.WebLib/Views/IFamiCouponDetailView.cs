﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IFamiCouponDetailView
    {
        #region property
        int? CouponId { get; }
        string UserName { get; }
        string SiteUrl { set; get; }
        bool EnableFami3Barcode { set; get; }
        CouponCodeType CodeType { set; get; }
        string PinCode { set; get; }

        #endregion

        #region event
        #endregion

        #region method
        void SetFamiCouponContent(ViewPponCoupon coupon, IViewPponDeal vpd);
        #endregion
    }
}

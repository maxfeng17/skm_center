﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEdmDetailsSetupView
    {
        #region property
        EdmDetailsSetupPresenter Presenter { get; set; }
        int CityId { get; set; }
        bool IsShowSortType { get; set; }
        int EdmSetupCityId { get; set; }
        int SortType { get; set; }
        DateTime DeliveryDate { get; set; }
        DateTime? DealDate { get; set; }
        int Pid { get; set; }
        int MainDetailType { get; set; }
        int MainDetailItemType { get; set; }
        int RowNumber { get; set; }
        int ColumnNumber { get; set; }
        
        #endregion
        
        #region event

        event EventHandler GetViewPponDealTimeSlotCollection;
        event EventHandler<DataEventArgs<KeyValuePair<Guid, int>>> GetViewPponDealByBid;
        
        #endregion
        
        #region method

        void GetViewPponDealTimeSlotCollectionByCityDate(ViewPponDealTimeSlotCollection vpdts);
        void RetrieveViewPponDealByBid(ViewPponDeal deal, int control_sequence);        
        #endregion
    }
}

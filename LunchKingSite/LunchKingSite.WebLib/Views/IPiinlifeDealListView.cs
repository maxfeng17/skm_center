﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;
using LunchKingSite.BizLogic.Component;


namespace LunchKingSite.WebLib.Views
{
    public interface IPiinlifeListView
    {
        PiinlifeListPresenter Presenter { get; set; }

        CategoryNode Channel { get; }

        int? CategoryId { get; }

        Guid BusinessHourGuid { get; }

        void SetSubscribeDealContent(Dictionary<Guid, string[]> imgUrls);

        void SetMultipleCategories(List<CategoryDealCount> categoryDealCounts, List<CategoryDealCount> filterNodes);

        void SetMutilpMainDeals(Dictionary<MultipleMainDealPreview, IList<ViewComboDeal>> multiplemaindeals);

        void RedirectToPponDefault();
    }
}

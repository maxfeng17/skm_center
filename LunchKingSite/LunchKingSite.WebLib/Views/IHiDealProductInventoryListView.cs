﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealProductInventoryListView
    {
        HiDealProductInventoryListPresenter Presenter { get; set; }
        string Productlid { get; }
        string Message { get; set; }
        string ShippedDate { get; set; }

        #region event
        event EventHandler<DataEventArgs<int>> GetShopReportDetail;
        event EventHandler<DataEventArgs<int>> GetHouseReportDetail;
        event EventHandler<DataEventArgs<DateTime>> SaveShippedDate;
        void ExportProductShopData(ViewHiDealProductShopInfoCollection productShopInfo);
        void ExportProductHouseData(ViewHiDealProductHouseInfoCollection productHuoseInfo);
        #endregion

        #region method
        void showShopData(ViewHiDealProductShopInfoCollection vhidealproductshop);
        void showHouseData(ViewHiDealProductHouseInfoCollection vhidealproducthouse);
        #endregion
    }
}

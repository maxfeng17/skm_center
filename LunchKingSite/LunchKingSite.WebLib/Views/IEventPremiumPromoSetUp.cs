﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventPremiumPromoSetUp
    {
        #region property
        
        int Mode { get; set; }
        string UserName { get; }
        string SiteUrl { get; }
        int EventPremiumId { get; set; }
        bool IsLimitInThreeMonth { get; }

        #endregion property

        #region event

        event EventHandler ChangeMode;
        event EventHandler<DataEventArgs<EventPremiumPromo>> SaveEventPremiumPromo;
        event EventHandler GetEventPremiumPromo;
        event EventHandler UpdateEventPremiumPromoStatus;

        #endregion event

        #region method

        void ChangePanel(int mode, string return_message = "");
        void SetEventPremiumPromoList(EventPremiumPromoCollection premiumPromos);
        void SetEventPremiumPromo(EventPremiumPromo premiumPromo);

        #endregion method
    }

}

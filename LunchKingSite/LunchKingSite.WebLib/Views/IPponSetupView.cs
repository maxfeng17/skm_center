﻿using System;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.SellerModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponSetupView
    {
        event EventHandler<DataEventArgs<PponSetupViewModel>> SaveDeal;
        event EventHandler<ComboDealEventArgs> CopyDeal;
        event EventHandler CreateOrder;
        event EventHandler GenerateCoupon;
        event EventHandler DeleteDeal;
        event EventHandler CreateDealTimeSlot;
        event EventHandler SetBBH;
        event EventHandler<DataEventArgs<Guid>> ManualDealClose;
        event EventHandler<DataEventArgs<Guid>> SyncPChome1;
        event EventHandler<DataEventArgs<Guid>> SyncPChome2;
        event EventHandler<DataEventArgs<KeyValuePair<string, int>>> ImportBusinessOrderInfo;
        event EventHandler ProposalImport;
        event EventHandler RreceiveType_TextChanged;
        event EventHandler<DataEventArgs<string>> CheckFlag;
        event EventHandler DeleteCDNImages;
        event EventHandler SetShoppingCart;

        /// <summary>
        /// 賣家名稱
        /// </summary>
        string SellerName { set; }
        Guid SellerGuid { get;set; }
        PponSetupPresenter Presenter { get; set; }
        Guid BusinessHourId { get; }
        Guid SellerId { get; }
        string purchasePrice { get; }
        string slottingFeeQuantity { get; }
        Dictionary<int, int[]> SelectedCommercialCategoryId { get; set; }
        /// <summary>
        /// 選取的頻道、區域等資料
        /// </summary>
        Dictionary<int, List<int>> SelectedChannelCategories { get; set; }

        Dictionary<int, List<int>> SelectedChannelSpecialRegionCategories { get; set; }
        List<int> SelectedChannelSpecialRegionSubCategories { get; set; }

        /// <summary>
        /// 選取的分類
        /// </summary>
        List<int> SelectedDealCategories { get; set; }

        /// <summary>
        /// 目前選取的取貨類型ID
        /// </summary>
        int SelectedDeliveryCategoryId { get; }
        /// <summary>
        /// 選取的特殊Category
        /// </summary>
        List<int> SelectedSpecialCategories { get; set; }
        /// <summary>
        /// 依據條件，預設的ICON狀態
        /// </summary>
        List<int> SelectedDefaultIcon { set; get; }

        string UserName { get; }
        TimeSpan DefaultOnTime { set; }
        TimeSpan DefaultEndTime { set; }
        bool IsHiddenFromRecentDeals { get; }
        string SMSContent { get; }
        string BusinessHourUniqueId { set; get; }
        string DevelopeSales { get; set; }
        string OperationSales { get; set; }
        string MenuContent { get; set; }
        bool DisableSMS { get; set; }
        bool NoRefund { get; set; }
        bool DaysNoRefund { get; set; }
        bool ExpireNoRefund { get; set; }
        bool NoRefundBeforeDays { get; set; }
        string QuantityToShow { get; }
        bool WeeklyPay { get; }
        bool PriceZeorShowOriginalPrice { get; set; }
        bool NoDiscountShown { get; set; }
        bool LowGrossMarginAllowedDiscount { get; set; }
        bool NotAllowedDiscount { get; set; }
        bool BuyoutCoupon { get; set; }
        bool TmallDeal { get; set; }
        bool DonotInstallment { get; set; }
        bool IsInputTaxNotRequired { get; set; }
        bool NoTax { get; set; }
        bool NoInvoice { get; set; }
        bool NoShippingFeeMessage { get; set; }
        bool NotDeliveryIslands { get; set; }
        bool PEZevent { get; set; }
        bool PEZeventCouponDownload { get; set; }
        bool ZeroActivityShowCoupon { get; set; }
        bool PEZeventWithUserId { get; set; }
        bool IsDepositCoffee { get; set; }
        /// <summary>
        /// 使用反向核銷
        /// </summary>
        bool IsReverseVerify { get; set; }
        bool FamiDeal { get; set; }
        bool HiLifeDeal { get; set; }
        bool SkmDeal { get; set; }
        bool SkmExperience { get; set; }
        int SkmDiscountType { get; set; }
        int SkmDiscount { get; set; }
        string SkmTags { set; }
        bool IsCloseMenu { get; set; }
        bool IsMenuLogo { get; set; }
        int FamiDealType { get; set; }
        bool IsKindDeal { get; set; }
        string btnNotInBBH { set; }
        int AccBusinessGroupNo { get; set; }

        decimal GrossMarginVal { set; }
        decimal MinGrossMarginVal { set; }
        /// <summary>
        /// 系統訂定的最低毛利率(參照config)
        /// </summary>
        decimal GrossMarginCriterion { get; }
        string IsMulti { set; }

        int DealTypeNewValue { get; set; }
        //DealAccountingPayType PayToCompany { get; set; }
        RemittanceType PaidType { get; set; }

        int? SellerRemittanceType { get; }
        VendorBillingModel BillingModel { get; set; }
        VendorReceiptType ReceiptType { get; set; }
        string AccountingMessage { get; set; }
        EntrustSellType EntrustSell { get; set; }
        TrustProvider TrustType { get; set; }
        string TravelPlace { get; set; }
        int SellerVerifyType { get; set; }
        string DeptId { get; set; }
        int AccCityId { get; set; }
        DeliveryType TheDeliveryType { get; set; }//消費方式
        bool DShoppingCart { get; set; }
        int ComboPackCount { get; set; }
        string AncestorBusinessHourGuid { get; set; } //接續檔次的 bid(數量)
        string AncestorSequenceBusinessHourGuid { get; set; } //接續檔次的 bid(憑證)
        string AncestorCouponCount { set; }
        string AncestorOrderedQuantity { set; }
        bool Is_Continued_Quantity { get; set; }
        bool Is_Continued_Quantity_Visible { set; }
        bool Is_Continued_Sequence { get; set; }
        bool Is_Continued_Sequence_Visible { set; }
        /// <summary>
        /// 是否每日限量
        /// </summary>
        bool IsDailyRstriction { set; get; }
        string SuccessorCouponCount { set; }
        string SuccessorOrderedQuantity { set; }
        /// <summary>
        /// 0元好康活動完成頁大圖連結
        /// </summary>
        string ActivityUrl { set; get; }
        /// <summary>
        /// 是否啟用輪播大圖自動壓Logo
        /// </summary>
        bool IsPrintWatermark { get; }

        /// <summary>
        /// 大於1，銷售量以購買份數顯示，等於1，以購買人數顯示 ，組合數
        /// </summary>
        int? QuantityMultiplier { get; set; }
        /// <summary>
        /// 是否顯示銷售倍數
        /// </summary>
        bool IsQuantityMultiplier { get; set; }
        /// <summary>
        /// 是否顯示均價
        /// </summary>
        bool IsAveragePrice { get; set; }

        //是否為對開發票
        bool IsMutualInvoice { get; set; }
        int Commission { get; set; }

        string ItemName { get; }
        bool IsAllowedToReviseCPC { set; }
        int SaleMultipleBase { get; set; }
        int PresentQuantity { get; set; }
        bool GroupCoupon { get; set; }
        int GroupCouponType { get; set; }
        int GroupCouponAppStyle { get; set; }
        /// <summary>
        /// 運費收入
        /// </summary>
        List<PponSetupViewFreight> FreightIncome { get; set; }
        /// <summary>
        /// 運費支出
        /// </summary>
        List<PponSetupViewFreight> FreightCost { get; set; }
        /// <summary>
        /// 進貨價
        /// </summary>
        List<PponSetupViewMultiCost> MultiCost { get; set; }
        /// <summary>
        /// 調整過的有效截止日
        /// </summary>
        DateTime? ChangedExpireDate { get; set; }

        /// <summary>
        /// 購買截止日
        /// </summary>
        DateTime OrderTimeE { get; set; }
        /// <summary>
        /// 購買開始日
        /// </summary>
        DateTime OrderTimeS { get; set; }
        /// <summary>
        /// 有效/配送截止日
        /// </summary>
        DateTime? DeliverTimeE { get; set; }
        /// <summary>
        /// 有效/配送開始日
        /// </summary>
        DateTime? DeliverTimeS { get; set; }
        DateTime? SettlementTime { get; set; }
        /// <summary>
        /// 是否開檔
        /// </summary>
        bool IsDealOpened { get; }
        /// <summary>
        /// 是否已經結檔
        /// </summary>
        bool IsDealClosed { get; set; }

        string[] SalesmanNameArray { set; }

        int ProposalId { get; }

        int ProposalCreateType { get; }

        int ProposalConsignment { get; }


        /// <summary>
        /// 是否有超多家分店
        /// </summary>
        bool MultipleBranch { get; set; }

        /// <summary>
        /// 是否完全複製檔
        /// </summary>
        bool CompleteCopy { get; set; }

        /// <summary>
        /// 工單編號
        /// </summary>
        string BusinessOrderId { set; get; }

        /// <summary>
        /// APP限定檔次
        /// </summary>
        bool IsAppLimitedEdition { set; get; }

        /// <summary>
        /// 憑證檔次tag標籤
        /// </summary>
        int[] SelectedCouponDealTag { set; get; }
        /// <summary>
        /// 憑證旅遊檔tag標籤
        /// </summary>
        int[] SelectedCouponTravelDealTag { set; get; }
        /// <summary>
        /// 宅配檔次tag標籤
        /// </summary>
        int[] SelectedDeliveryDealTag { set; get; }
        /// <summary>
        /// 宅配旅遊檔tag標籤
        /// </summary>
        int[] SelectedDeliveryTravelDealTag { set; get; }
        /// <summary>
        /// 單一序號
        /// </summary>
        bool IsSinglePinCode { set; get; }
        /// <summary>
        /// 檔次配合使用訂位系統
        /// </summary>
        int BookingSystemType { set; get; }
        /// <summary>
        /// 提前預約天日
        /// </summary>
        int AdvanceReservationDays { set; get; }
        /// <summary>
        /// 預約系統-優惠券使用人數
        /// </summary>
        int CouponUsers { set; get; }
        /// <summary>
        /// 商家系統-預約設定-訂房鎖定
        /// </summary>
        bool IsReserveLock { get; set; }
        /// <summary>
        /// 自定檔次說明標籤
        /// </summary>
        string CustomTag { set; get; }

        /// <summary>
        /// 天貓檔次人民幣售價
        /// </summary>
        decimal TmallRmbPrice { get; set; }
        /// <summary>
        /// 天貓檔次人民幣兌換匯率
        /// </summary>
        decimal TmallRmbExchangeRate { get; set; }
        /// <summary>
        /// 全家兌換價
        /// </summary>
        decimal ExchangePrice { get; set; }
        /// <summary>
        /// 銀行檔次(不顯示於一般頻道)
        /// </summary>
        bool IsBankDeal { get; set; }
        /// <summary>
        /// 展演型檔次
        /// </summary>
        bool IsPromotionDeal { get; set; }
        /// <summary>
        /// 展覽型檔次
        /// </summary>
        bool IsExhibitionDeal { get; set; }
        /// <summary>
        /// CChannel
        /// </summary>
        bool IsCChannel { get; set; }
        /// <summary>
        /// CChannel影片連結
        /// </summary>
        string CChannelLink { get; set; }
        /// <summary>
        /// 活動遊戲檔次
        /// </summary>
        bool IsGame { get; set; }
        /// <summary>
        /// 台新特談商品
        /// </summary>
        bool IsTaishinChosen { get; set; }
        /// <summary>
        /// 活動遊戲檔次
        /// </summary>
        bool IsChannelGift { get; set; }
        /// <summary>
        /// 24H 到貨
        /// </summary>
        bool IsWms { get; set; }
        /// <summary>
        /// 允許訪客購買
        /// </summary>
        bool AllowGuestBuy { get; set; }
        /// <summary>
        /// 是否為子檔
        /// </summary>
        bool isSub { get; set; }
        /// <summary>
        /// 商家系統品項統計方式
        /// </summary>
        bool IsMergeCount { get; set; }

        //全家商品類型
        int FamilyDealType { get; set; }
        //全家商品鎖定
        bool FamilyIsLock { get; set; }
        /// <summary>
        /// 子檔同步項目
        /// </summary>
        bool IsSaveComboDeals { get; }
        bool IsSaveComboDeals_Sales { get; }
        bool IsSaveComboDeals_Channel { get; }
        bool IsSaveComboDeals_SEO { get; }
        bool IsSaveComboDeals_NotDeliveryIslands { get; }
        bool IsSaveComboDeals_AccBusGroup { get; }
        bool IsSaveComboDeals_SalesBelong { get; }
        bool IsSaveComboDeals_DealType { get; }
        bool IsSaveComboDeals_EntrustSell { get; }
        bool IsSaveComboDeals_PaySet { get; }
        bool IsSaveComboDeals_OS { get; }
        bool IsSaveComboDeals_OE { get; }
        bool IsSaveComboDeals_UseDiscount { get;}
        bool IsSaveComboDeals_DS { get; }
        bool IsSaveComboDeals_DE { get; }
        bool IsSaveComboDeals_ChangedExpireDate { get; }
        bool IsSaveComboDeals_NoRefund { get; }
        bool IsSaveComboDeals_DaysNoRefund { get; }
        bool IsSaveComboDeals_NoRefundBeforeDays { get; }
        bool IsSaveComboDeals_ExpireNoRefund { get; }
        bool IsSaveComboDealsPeZevent { get; }
        bool IsSaveComboDeals_PEZeventCouponDownload { get; }
        bool IsSaveComboDealsDepositCoffee { get; }
        bool IsSaveComboDealsVerifyActionType { get; }
        bool IsSaveComboDeals_ReserveLock { get; }
        bool IsSaveComboDeals_BuyoutCoupon { get; }
        bool IsSaveComboDeals_IsLongContract { get; }
        bool IsSaveComboDeals_DonotInstallment { get; }
        bool IsSaveComboDeals_ShipType { get; }
        bool IsSaveComboDeals_TaiShinCreditCard { get; }
        /// <summary>
        /// 長期約
        /// </summary>
        bool IsLongContract { set; get; }
        string AgentChannels { get; set; }

        DealShipType OrderShipType { get; set; }
        DealShippingDateType DealShippingDateType { get; set; }
        int ShippingDate { get; set; }
        int ProductUseDateStartSet { get; set; }
        int ProductUseDateEndSet { get; set; }

        int? SellerPaidType { get; set; }
        bool HasUseDiscountSetPrivilege { get; }

        /// <summary>
        /// 通用券商品
        /// </summary>
        bool NoRestrictedStore { get; set; }
        /// <summary>
        /// 檔次資訊不出現於Web
        /// </summary>
        bool NotShowDealDetailOnWeb { get; set; }
        /// <summary>
        /// 全部的分類資料，先行撈取以節省進一步需要跟DB頻繁讀取
        /// </summary>
        List<ViewCategoryDependency> AllViewCategoryDependencies { get; set; }
        /// <summary>
        /// 商品寄倉
        /// </summary>
        bool Consignment { get; set; }
        /// <summary>
        /// 限刷台新信用卡
        /// </summary>
        LimitBankIdModel LimitCreditCardBankId { get; set; }
        /// <summary>
        /// 序號分隔位數
        /// </summary>
        int? CouponSeparateDigits { get; set; }
        bool IsAgreePponNewContractSeller { get;}
        /// <summary>
        /// 不顯示折後價於前台(折後價黑名單)
        /// </summary>
        bool IsDealDiscountPriceBlacklist { get; set; }

        // 接續檔次設定
        void SetSuccessorBid(DealPropertyCollection bids);
        void SetInitStores(IEnumerable<Seller> sellers);
        void SetDealView(PponDeal deal, string accessoryText, CouponEventContent cec, PponStoreCollection pponStores, bool isPChomeChannelProd);
        void SetAccountingData(DealAccounting dAccounting, DealCostCollection dCost);
        void SetDealStage(PponDealStage stage);
        void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories);
        void SetGroupCouponAppStyleList(Dictionary<int, string> list);
        /// <summary>
        /// 設定預設的ICON顯示列表
        /// </summary>
        /// <param name="codeList"></param>
        void SetDefaultIconSystemCode(List<KeyValuePair<int, string>> codeList);
        /// <summary>
        /// 列出所有能設定的DealSpecialCategory
        /// </summary>
        /// <param name="list"></param>
        void SetSelectableDealSpecialCategory(List<Category> list);
        /// <summary>
        /// 列出目前能選的項目，其他不能選取
        /// </summary>
        /// <param name="deliveryCategory">目前設定的取貨方式</param>
        /// <param name="list">此取貨方式可設定之DealSpecialCategory</param>
        void SetUnhiddenDealSpecialCategory(Category deliveryCategory, List<Category> list);
        void SetSellerTreeView(IEnumerable<SellerNode> sellerTrees);

        void JumpTo(PponSetupMode mode, string argument);
        void SetSMSContent(SmsContent sms);
        void ShowMessage(string msg);
        void CopyDealLink(Guid copydealguid);
        void SetAccField(AccBusinessGroupCollection accGroups, List<City> citys, DepartmentCollection depts);
        void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes);
        void SetSellerField(SystemCodeCollection sellerVerifyType);
        void SetProposalImport(ViewProposalSeller pro);
        void GetProposalData(ViewProposalSeller pro);
        void SetSendPageConfirmMailButton(ViewProposalSeller pro);
        void SetShoppingCartOption(List<KeyValuePair<string, string>> listitem, string selectValue);
        /// <summary>
        /// 處理好康區域及其子項目類別的設定畫面
        /// </summary>
        /// <param name="channelTypeNode"></param>
        /// <param name="allViewCategoryDependencies"></param>
        void SetSelectableChannel(CategoryTypeNode channelTypeNode, List<ViewCategoryDependency> allViewCategoryDependencies);
        //多檔次設定
        void SetInitComboDeals(ViewPponDealCollection deals, PponDeal theDeal, PponDeal mainDeal, List<ViewComboDeal> comboDeals,
            List<PponDeal> comboEntityDeals, CategoryDealCollection mainCategory, List<CategoryDealCollection> comboCategoryDeals,
            DealAccounting mainAccounting, List<DealAccounting> comboAccountingDeals, List<ViewPponDeal> comboViewPponDeal, ViewPponDeal pponOrder,
            List<Category> categoryList);
        void SetBusinessOrderInfo(BusinessOrder businessOrder, VacationCollection recentHolidays);
        void SetProposalInfo(Proposal pro, VacationCollection recentHolidays);
        void SetBusinesssOrderDeal(BusinessOrder businessOrder, VacationCollection recentHolidays, int index);
        void SetDealLabel(DealLabelCollection dealLabels);
        void SetExpireRedirectDisplay();
    }

    public class PponSetupViewModel
    {
        public PponDeal Deal { get; set; }
        public HttpPostedFile UploadedFile { get; set; }
        public PponSetupImageUploadType ImageUploadType { get; set; }
        public string AccessoryString { get; set; }
        public List<PponSetupPponStore> PponSetupPponStore { get; set; }
        public ComboData ComboData { get; set; }
        public ImageData ImageData { get; set; }
        public int? FreightsId { get; set; }
        public bool IsTaiShinChosen { get; set; }
        public PponSetupViewModel(PponDeal deal, HttpPostedFile upload, string accessoryString,
            List<PponSetupPponStore> pponStores, ComboData comboData, PponSetupImageUploadType imageUploadType, ImageData imageData, int? freightsId)
        {
            Deal = deal;
            UploadedFile = upload;
            ImageUploadType = imageUploadType;
            AccessoryString = accessoryString;
            PponSetupPponStore = pponStores;
            ComboData = comboData;
            ImageData = imageData;
            FreightsId = freightsId;
        }
    }

    public class ImageData
    {
        public string AppDealPic { get; set; }
        public int CropX { get; set; }
    }

    public enum PponSetupMode
    {
        GenericError = -1,
        DealNotFound = 0,
        SellerNotFound = 1,
        DealPage = 2,
        ProposalUnCheck = 3
    }

    public enum PponSetupImageUploadType
    {
        /// <summary>
        /// 未上傳
        /// </summary>
        None,
        /// <summary>
        /// 按順序上圖(1,2,3,其他輪播大圖)
        /// </summary>
        ByORder,
        /// <summary>
        /// 上輪播大圖 
        /// </summary>
        BigPic,
        /// <summary>
        /// 上APP行銷圖 
        /// </summary>
        APP,
        /// <summary>
        /// 旅遊eDM專屬大圖
        /// </summary>
        TravelEdmSpecialBigPic,
    }

    public class PponSetupPponStore
    {
        public Guid StoreGuid { get; set; }
        public int? TotalQuantity { get; set; }
        public int? SortOrder { get; set; }
        public DateTime? ChangedExpireTime { get; set; }
        public string UseTime { get; set; }
        public int VbsRight { get; set; }
    }

    public class PponSetupViewFreight
    {
        public decimal StartAmount { get; set; }
        public decimal FreightAmount { get; set; }
        public string PayTo { get; set; }
    }

    public class PponSetupViewMultiCost
    {
        public int Id { get; set; }
        public decimal? Cost { get; set; }
        public int? Quantity { get; set; }
        public int? Cumulative_quantity { get; set; }
        public int? Lower_cumulative_quantity { get; set; }
    }

    public class ComboDealEventArgs : EventArgs
    {
        public bool IsCopyAsSubDeal { get; set; }

        public bool IsCopyComboDeals { get; set; }
    }

}

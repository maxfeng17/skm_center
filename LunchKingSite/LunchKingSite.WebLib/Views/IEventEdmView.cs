﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventEdmView
    {
        EventEdmPresenter Presenter { get; set; }
        event EventHandler AreaChange;
        int CityID { get; set; }
        void Default_SetUp();
        void FillDeal(ViewPponDealTimeSlotCollection dtsc);
    }
}

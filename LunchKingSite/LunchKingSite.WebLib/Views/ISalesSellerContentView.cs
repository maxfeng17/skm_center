﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesSellerContentView
    {
        #region property
        SalesSellerContentPresenter Presenter { get; set; }
        string UserName { get; }
        Guid SellerGuid { get; }
        bool NoCompanyIDCheck { get; }
        short DevelopStatus { get; }
        short SellerGrade { get; }
        string SellerList { set; get; }
        int ProposalDeliveryType { set; get; }
        bool IsTravelProposal { get; set; }
        bool IsPiinLifeProposal { get; set; }
        bool IsPBeautyProposal { get; set; }
        bool IsWms { get; set; }
        string[] SalesmanNameArray { set; }
        string hid { set; get; }
        int ReceiptType { get;}
        string AccountingMessage { get; }   
        string ReferralSale { get; }
        int ReferralPercent { get; }
        bool IsReferralModifyMail { get; }
        StoreStatus Status { get; set; }
        bool IsCloseDown { get; set; }
        DateTime? CloseDownDate { get; set; }
        string BusinessHour { get; set; }
        string CloseDateInformation { get; set; }
        Guid ParentGuid { get; set; }    
        string DevelopeSales { get; set; }
        string OperationSales { get; set; }

        string SelectDevelopeSales { get; set; }
        string SelectOperationSales { get; set; }
        string SecondService { get; set; }
        ViewVbsInstorePickupCollection ViewVbsInstorePickupViewData { get; set; }
        ViewVbsInstorePickup ViewVbsInstorePickupFamily { get; set; }
        ViewVbsInstorePickup ViewVbsInstorePickupSeven { get; set; }
        bool IsISPVerifyFamily { get; set; }
        bool IsISPEnabledFamily { get; set; }
        bool IsISPVerifySeven { get; set; }
        bool IsISPEnabledSeven { get; set; }
        bool IsWmsEnabled { get; set; }
        int ReturnCycleFamily { get; }
        int ReturnTypeFamily { get; }
        string ReturnOtherFamily { get; }
        int ReturnCycleSeven { get; }
        int ReturnTypeSeven { get; }
        string ReturnOtherSeven { get; }
        string ReturnAddress { get; }
        string ContactName { get; }
        string ContactTel { get; }
        string ContactEmail { get; }
        string ContactMobile { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<SellerTempStatus>> SellerApprove;
        event EventHandler<DataEventArgs<Guid>> CreateProposal;
        event EventHandler<EventArgs> Save;
        event EventHandler<EventArgs> SaveLocation;
        event EventHandler<DataEventArgs<PhotoInfo>> UpdatePhoto;
        event EventHandler<DataEventArgs<string>> ImageListChanged;
        event EventHandler<DataEventArgs<KeyValuePair<bool, ServiceChannel>>> ISPApply;
        event EventHandler<DataEventArgs<KeyValuePair<string, ServiceChannel>>> ISPReject;
        #endregion

        #region method
        Seller GetSellerData(Seller s);
        Seller GetSellerLocationData(Seller s);
        void RedirectProposal(Proposal pro);
        void RedirectSeller(Guid sid);
        void SetSellerContent(Seller seller, ViewEmployee emp, SellerChangeLogCollection changeLogs,
                                SellerManageLogCollection manageLogs, SellerContractFileCollection scfs, 
                                Dictionary<Seller, Seller> stc, List<ShoppingCartManageList> freiList,
                                ShoppingCartFreightsLogCollection scLogs);
        void ShowMessage(string msg, SellerContentMode mode, string parms = "");

        void SetImageGrid(string rawDataPath);

        void SetImageFile(PhotoInfo pi);
        #endregion
    }

    public enum SellerContentMode
    {
        GenericError = -1,
        PrivilegeError = 0,
        CompanyIdCheck = 1,
        CompanyIdExist = 2,
        SellerNotFound = 3,
        SalesError = 4,
        TurnUrl = 5,
        CopyTurnUrl = 6,
    }
}

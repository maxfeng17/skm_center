using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponOrderListView
    {
        event EventHandler<DataEventArgs<int>> GetOrderCount;
        event EventHandler<DataEventArgs<Guid>> GetCurrentQuantity;
        event EventHandler SortClicked;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SearchClicked;

        PponOrderListPresenter Presenter { get; }
        string SortExpression { get; set; }
        string FilterUser { get; set; }
        string[] FilterInternal { get;}
        string FilterType { get; set; }
        bool FilterPiinlife { get; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        int CurrentQuantity { get; set; }
        List<string> SelectedStatusFilter { get;}
        void SetPponList(ViewPponDealCollection orders);
        void SetStatusFilter(Dictionary<int, string> status);
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        string OrderByInternal { get;}
        Dictionary<int, string> StatusFilter { get;}
        Dictionary<string, string> FilterTypes { get;}
    }
}

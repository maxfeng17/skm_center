﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserRefund
    {
        #region property
        UserRefundPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        string PanelMode { get; set; }
        bool IsCoupon { get; set; }
        Guid OrderGuid { get; set; }
        string ReceiverName { get; }
        string ReceiverAddress { get; }
        string OrderID { get; }
        bool IsRefundCashOnly { get; set; }
        bool IsPaidByCash { get; set; }
        TextBox TxtOrderId { get; }
        TextBox TxtAccountId { get; }
        TextBox TxtMobile { get; }
        string alertMessage { set; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<string>> GetOrderInfo;
        event EventHandler<DataEventArgs<KeyValuePair<OrderStatusLog, RefundType>>> RequestRefund;
        event EventHandler<DataEventArgs<KeyValuePair<CtAtmRefund, KeyValuePair<OrderStatusLog, RefundType>>>> RequestRefundATM;
        event EventHandler<DataEventArgs<string>> CancelNotCompleteOrder;
        event EventHandler<DataEventArgs<string>> GetATMBranch;
        #endregion
        #region method
        void SetOrderInfo(RefundOrderInfo orderinfo, ViewOrderShipList osInfo);
        void ReturnPanel(string panelmode);
        void SetATMBranch(BankInfoCollection bankinfo);
        void SetBankInfo(BankInfoCollection bankinfo);
        #endregion
    }
}

﻿using LunchKingSite.DataOrm;
using System;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventView
    {
        event EventHandler<DataEventArgs<EventEmailList>> EmailEnter;

        event EventHandler Reload;

        string Url { get; }

        string eid { get; }

        bool EmailSuccess { get; set; }

        string ShortUrl { get; set; }
        string SeoKeyword { get; set; }
        string SeoDescription { get; set; }

        void FillForm(EventContent ec);

        void ReturnDefault();

        void CheckLogin();

        void DoClear();
    }
}
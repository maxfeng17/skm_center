﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealSetupView
    {
        Guid Sid { get; }

        int Did { get; set; }

        string UserName { get; }

        string DealName { get; set; }

        string DealPromoName { get; set; }

        string DealPromoShortName { get; set; }

        Dictionary<int, string> DealRegions { get; set; }

        List<int> DealCategories { get; set; }

        List<int> DealDiscounts { get; set; }

        bool IsOpen { get; set; }

        bool IsAlwaysMain { get; set; }

        bool IsVerifyByList { get; set; }

        bool IsVerifyByPad { get; set; }

        string DealStartDate { get; set; }

        string DealStartTime { get; set; }

        string DealEndDate { get; set; }

        string DealEndTime { get; set; }

        DataTable StoreSettings { get; set; }

        string OrderedStoreSettings { get; }    //format:  "store1_guid, seq; store2_guid, seq"

        string TagXml { get; set; }

        string BigPictureUrl { get; set; }  // application relative Url: "~\..."

        string BigCandidatePictureUrl { get; set; }

        DateTime DealModifyTime { get; set; }

        List<string> SmallPictureUrls { get; set; }

        string SelectedPrimarySmallPictureUrl { get; set; }

        FileUpload BigPictureFileUpload { get; }

        List<HttpPostedFile> SmallPictureFileUpload { get; }

        string SalesName { get; set; }

        KeyValuePair<string, string> SalesDepartment { get; set; }

        KeyValuePair<string, string> SalesAccountableBU { get; set; }

        KeyValuePair<string, string> SalesBelongCity { get; set; }

        KeyValuePair<string, string> SalesCatg1 { get; set; }

        KeyValuePair<string, string> SalesCatg2 { get; set; }
        
        KeyValuePair<string, string> HdSalesCatg2 { get; set; }

        List<Product> Products { get; set; }

        Guid DealGuid { set; }

        string[] SalesmanNameArray { set; }

        #region event

        event EventHandler SaveDeal;

        event EventHandler<DataEventArgs<Product>> AddNewProduct;

        event EventHandler UploadBigPicture;

        event EventHandler UploadSmallPicture;

        event EventHandler GenCoupon;

        #endregion event

        #region method

        void ShowMessage(string msg);

        void ShowCouponList(ViewHiDealCouponStoreListCountCollection createcouponlistcollection);

        void ShowCouponNotMatchSettingMessage(HiDealCouponCountType hiDealCouponCountType);

        /// <summary>
        /// 建構銷售類別
        /// </summary>
        /// <param name="parentId">母類別Id</param>
        /// <param name="codeId">子類別Id</param>
        void BuildDdlSalesCatg2(int codeId, SystemCodeCollection scs);

        #endregion method
    }

    public class TagInformation
    {
        public int Id { get; set; }

        public int Seq { get; set; }

        public string Name { get; set; }

        public string Context { get; set; }
    }

    [Serializable]
    public class Product
    {
        public bool IsShow { get; set; }

        public int Id { get; set; }

        public Guid ProdGuid { get; set; }

        public int Seq { get; set; }

        public string Name { get; set; }
    }

    /// <summary>
    /// 檔次設定頁憑證資訊統計異常訊息提示類型
    /// </summary>
    public enum HiDealCouponCountType
    {
        /// <summary>
        /// （庫存中 + 已分配 = 後台設定數）
        /// </summary>
        GenateSettingMattch = 0,
        /// <summary>
        /// （庫存中 + 已分配 != 後台設定數）& 退貨單中無 退貨中 或 取消退貨  狀態
        /// </summary>
        GenateCouponSettingNotMattch = 1,
        /// <summary>
        /// （庫存中 + 已分配 != 後台設定數）& 退貨單中有 退貨中 或 取消退貨  狀態
        /// </summary>
        HaveRefundFormCoupon = 2,
    }
}
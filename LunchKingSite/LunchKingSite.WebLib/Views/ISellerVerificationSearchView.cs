using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Data;

namespace LunchKingSite.WebLib.Views
{
    public interface ISellerVerificationSearchView
    {
        #region prop

        Dictionary<string, string> FilterTypes { get; }
        string FilterType { get; }
        string FilterText { get; }

        #endregion prop

        #region method

        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void SetVerificationData(List<PponDealSalesInfo> dealSalesInfo);

        #endregion

        #region event

        event EventHandler<EventArgs> OkButtonClick;

        #endregion
    }

    public class PponDealSalesInfo
    {
        public Guid MerchandiseGuid { get; set; }
        
        public string ItemName { get; set; }

        public string SellerName { get; set; }

        public int UniqueId { get; set; }

        public bool IsDealClose { get; set; }

        public int UnverifiedCount { get; set; }

        public int VerifiedCount { get; set; }

        public int ReturnedCount { get; set; }

        public int BeforeDealEndReturnedCount { get; set; }

        public int ForceReturnCount { get; set; }

        public int ForceVerifyCount { get; set; }
    }
}

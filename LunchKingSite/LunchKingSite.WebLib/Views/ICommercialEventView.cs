﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface ICommercialEventView
    {
        #region property
        string EventCode { get; }
        int CommercialCode { get; }
        string CommercialName { get; set; }
        #endregion
        #region method
        void SetCategory(IEnumerable<Category> list);
        void SetEventPromo(Dictionary<KeyValuePair<int, EventPromo>, Dictionary<EventPromoItem, IViewPponDeal>> list);
        void ShowEventExpire();
        #endregion
    }
}

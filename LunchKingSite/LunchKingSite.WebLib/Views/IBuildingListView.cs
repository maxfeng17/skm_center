﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IBuildingListView : ILocalizedView
    {
        event EventHandler<DataEventArgs<int>> GetBuildingCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SortClicked;
        event EventHandler SearchClicked;
        event EventHandler SelectCityChanged;

        BuildingListPresenter Presenter { get; }
        string SortExpression { get; set; }
        string Filter { get; set; }
        bool Status { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        string FilterType { get; set; }
        KeyValuePair<string, int>? SelectCity { get; }
        Guid BusinessHourGuid { get; }
        ShowListMode BusinessHourBindMode { get; set; }
        Guid SellerGuid { get; }
        int Distance { get; }

        void SetZoneDropDown(List<City> zone);
        void SetCityDropDown(List<City> city);
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void SetStatusFilter(Dictionary<bool, string> status);
        void SetBuildingList(ViewBuildingCityCollection BuildingCityLists);
    }
}

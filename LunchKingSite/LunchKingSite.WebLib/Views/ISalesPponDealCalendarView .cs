﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesPponDealCalendarView
    {
        #region property
        SalesPponDealCalendarPresenter Presenter { get; set; }
        string UserName { get; }
        int MonthCount { get; }
        string Dept { get; }
        string EmpDept { get; }
        int EmpUserId { get; }
        int DealType1 { get; }
        int DealType2 { get; }
        int Year { get; }
        int Month { get; }
        string CrossDeptTeam { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<string>> DeptChanged;
        event EventHandler<DataEventArgs<string>> DealTypeChanged;
        event EventHandler PreviousSearch;
        event EventHandler NextSearch;
        #endregion

        #region method
        void SetPponDealCalendar(Dictionary<ViewPponDealCalendar, Proposal> vpdc);
        #endregion
    }
}

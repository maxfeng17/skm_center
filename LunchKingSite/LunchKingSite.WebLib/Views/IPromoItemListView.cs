﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPromoItemListView
    {
        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler<DataEventArgs<int>> Print;
        event EventHandler<DataEventArgs<int>> Send;
        #endregion
        #region property
        PromoItemListPresenter Presenter { get; set; }
        string UserName { get; }
        EventActivity Activity { get; set; }
        int EventId { get; }
        int UserId { get; }
        int PageCount { get; set; }
        int PageSize { get; }
        int CurrentPage { get; set; }
        #endregion
        #region method
        void PromoItemGetListPaging(PromoItemCollection promoItemList);
        void ShowMessage(string message);
        void Download(string eventName, string template);
        #endregion
    }
}

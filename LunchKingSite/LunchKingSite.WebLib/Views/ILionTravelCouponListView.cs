﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ILionTravelCouponListView
    {
        #region property
        LionTravelCouponListPresenter Presenter { get; set; }
        AgentChannel OrderClassType { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<Coupon>> SearchCoupons;
        #endregion

        #region method
        void SetCoupons(ViewOrderCorrespondingCouponCollection coupons, Dictionary<Guid, CashTrustLogCollection> cash_trust_logs, Dictionary<Guid, CashTrustLogCollection> return_cash_trust_logs);
        #endregion
    }
}

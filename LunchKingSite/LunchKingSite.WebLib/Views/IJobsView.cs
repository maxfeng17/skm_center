﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IJobsView
    {
        JobsPresenter Presenter { get; set; }
        string JobName { get; }
        bool IsJobLoadBalanceProceed { get; set; }
        void SetListMode(List<JobContext> jobs);
        void SetDetailMode(JobContext job);

        event EventHandler PlayPauseAll;
        event EventHandler<DataEventArgs<string>> TimerTick;
        event EventHandler<DataEventArgs<string>> Fire;
        event EventHandler<DataEventArgs<string>> PlayPause;
        event EventHandler<DataEventArgs<string>> Remove;
    }
}

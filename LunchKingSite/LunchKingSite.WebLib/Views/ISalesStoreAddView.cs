﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;
using MongoDB.Bson;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesStoreAddView
    {
        #region property
        SalesStoreAddPresenter Presenter { get; set; }
        string UserId { get; }
        ObjectId BusinessOrderObjectId { get; }
        string CityJSonData { get; set; }
        int AddressCityId { get; set; }
        int AddressTownshipId { get; set; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<string>> AccountingBankSelectedIndexChanged;
        event EventHandler<DataEventArgs<ObjectId>> EditClick;
        event EventHandler<DataEventArgs<SalesStore>> SaveClicked;
        event EventHandler<DataEventArgs<ObjectId>> DeleteClicked;
        #endregion

        #region method
        void SetSalesStoreData(SalesStore store);
        void SetAccountingBankInfo(BankInfoCollection bankList);
        void SetAccountingBranchInfo(BankInfoCollection branchList);
        void GetLocationDropDownList(List<City> citys, List<City> locations);
        void SetStoreList(List<SalesStore> storeList);
        void ShowMessage(string message);
        #endregion
    }
}

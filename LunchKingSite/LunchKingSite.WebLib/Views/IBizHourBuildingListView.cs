﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IBizHourBuildingListView : ILocalizedView
    {
        event EventHandler<DataEventArgs<int>> GetSellerCount;
        event EventHandler<CommandEventArgs> PageChanged;
        event EventHandler<CommandEventArgs> SortClicked;
        event EventHandler<CommandEventArgs> SearchClicked;

        BizHourBuildingListPresenter Presenter { get; }
        Guid BuildingGuid { get; }
        string SortExpression { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        string FilterType { get; set; }
        string Filter { get; set; }
        BizHourBuildingListPresenter.BizHourBuildingListType QueryType { get; }

        void SetSearchDropDown(Dictionary<string, string> SearchType);
        void SetSellerCityBizHourList(ViewSellerCityBizHourCollection SellerCityBizHourList);

        
    }
}

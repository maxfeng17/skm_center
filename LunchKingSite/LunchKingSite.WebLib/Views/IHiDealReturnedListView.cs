﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealReturnedListView
    {
        #region property
        HiDealReturnedListPresenter Presenter { get; set; }
        string FilterType { get; set; }
        string FilterUser { get; set; }
        #endregion property

        #region event
        event EventHandler<DataEventArgs<HiDealReturnedLightListViewSearchRequest>> SearchClicked;
        event EventHandler<DataEventArgs<HiDealReturnedLightListViewSearchRequest>> ExportClicked;
        #endregion

        #region method
        void ShowMessage(string message);
        void ShowData(ViewHiDealReturnedLightCollection dataCollection, int dataCount);
        Dictionary<string, string> FilterTypes { get; }
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void Export(ViewHiDealReturnedLightCollection rtnCols);
        #endregion
    }

    /// <summary>
    /// For ViewHiDealReturnedLight
    /// </summary>
    public class HiDealReturnedLightListViewSearchRequest
    {
        /// <summary>
        /// 檔次名稱
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        public string OrderId { get; set; }
        /// <summary>
        /// HiDealId
        /// </summary>
        public string HiDealId { get; set; }

        /// <summary>
        /// 退貨申請時間 起
        /// </summary>
        public DateTime? ApplicationTimeStart { get; set; }

        /// <summary>
        /// 退貨申請時間 迄
        /// </summary>
        public DateTime? ApplicationTimeEnd { get; set; }

        public HiDealReturnedStatus? ReturnedStatus { get; set; } 

        public int PageNum;
        public int PageSize;
    }
}

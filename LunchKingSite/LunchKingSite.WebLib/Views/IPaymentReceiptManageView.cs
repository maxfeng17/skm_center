﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPaymentReceiptManageView
    {

        event EventHandler SearchClicked;
        event EventHandler QueryClicked;
        event EventHandler QueryUnReceiptReceivedInfoClicked;
        event EventHandler QueryRemainderBillClicked;
        event EventHandler DeleteBill;
        event EventHandler ExportBsBillInfoClicked;
        event EventHandler GetConfirmed;
        event EventHandler<DataEventArgs<string[]>> GetSheet;
        event EventHandler<DataEventArgs<Dictionary<int, DateTime>>> UpdateBillSentDate;
        event EventHandler<DataEventArgs<Dictionary<int, DateTime>>> UpdateFinanceGetDate;
        event EventHandler<DataEventArgs<int>> PageChanged;

        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void SetFilterReceiptType(Dictionary<int, string> types);
        void SetFilterRemittanceType(Dictionary<int, string> types);
        void SetFilterBuyerType(Dictionary<string, string> types);
        void SetFilterQueryTypes(Dictionary<string, string> types);
        void SetFilterDepartment(Dictionary<string, string> types);
        void SetFilterDeliveryType(Dictionary<int, string> types);
        void SetStoreList(DataTable bsLists);
        void SetSheetList(List<BalanceSheetBillListInfo> stores, List<BalanceSheetBillListInfo> wmsSheet);
        void SetInvoiceQuery(List<BalanceSheetBillListInfo> sheet, List<BalanceSheetBillListInfo> wmsSheet);
        void SetUnConfirmBalanceSheetInfo(List<UnConfirmBalanceSheet> bsInfos);
        void SetRemainderBill(ViewRemainderBillListCollection bills);

        Dictionary<string, string> FilterTypes { get; }
        Dictionary<int, string> DealTypeInfos { get; }
        Dictionary<int, string> FilterReceiptType { get; }
        Dictionary<string, string> FilterBuyerType { get; }
        Dictionary<int, string> FilterRemittanceType { get; }
        Dictionary<string, string> FilterQueryTypes { get; }
        Dictionary<int, string> FilterDeliveryType { get; }

        string FilterType { get; set; }
        string FilterUser { get; set; }
        string FilterQueryUser { get; set; }
        string FilterQueryType { get; set; }
        void ShowMessage(string msg);
        string[] FilterInternal { get; }
        string[] FilterSellerStore { get; }
        string[] FilterInfo { get; }
        string[] FilterUnReceiptReceivedInfo { get; }
        string[] FilterRemainderBillInfo { get; }
        bool IsFilterUnReceiptReceivedInfoFilled { get; }
        string FilterBillId { get; }
        int FilterAddBillReceiptType { get; }
        PaymentReceiptManageMode ManagementMode { get; set; }
        int PageSize { get; set; }
        int PageDataCount { get; set; }
        string UserName { get; }
    }

    public enum PaymentReceiptManageMode
    {
        Unknown = 0,
        /// <summary>
        /// 沒在編輯單據
        /// </summary>
        NotEditing = 1,
        /// <summary>
        /// 新增單據資料
        /// </summary>
        CreateRelationship = 2,
        /// <summary>
        /// 修改單據資料
        /// </summary>
        ModifyBill = 3
    }

    public class UnConfirmBalanceSheet
    {
        public Guid ProductGuid { get; set; }
        public int ProductType { get; set; }
        public int BalanceSheetId { get; set; }
        /// <summary>
        /// 單據Id
        /// </summary>
        public int? BillId { get; set; }
        /// <summary>
        /// 對帳單年份
        /// </summary>
        public int? Year { get; set; }
        /// <summary>
        /// 對帳單月份
        /// </summary>
        public int? Month { get; set; }
        public DateTime IntervalEnd { get; set; }
        /// <summary>
        /// 檔號
        /// </summary>
        public int UniqueId { get; set; }
        /// <summary>
        /// 檔名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 分店
        /// </summary>
        public string StoreName { get; set; }
        public Guid? StoreGuid { get; set; }
        /// <summary>
        /// 單據開立方式
        /// </summary>
        public int VendorReceiptType { get; set; }
        /// <summary>
        /// 單據開立方式為"其他"時的備註
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 出帳方式
        /// </summary>
        public int RemittanceType { get; set; }
        public int GenerationFrequency { get; set; }
        /// <summary>
        /// 帳務聯絡人
        /// </summary>
        public string AccountantName { get; set; }
        /// <summary>
        /// 帳務連絡電話
        /// </summary>
        public string AccountantTel { get; set; }
        /// <summary>
        /// 帳務連絡Email
        /// </summary>
        public string AccountantEmail { get; set; }
        /// <summary>
        /// 對帳單總額
        /// </summary>
        public int? BalanceSheetTotal { get; set; }
        /// <summary>
        /// 單據未回總額
        /// </summary>
        public int? ResidualBalanceSheetTotal { get; set; }
        public string EmpName { get; set; }
    }

    /// <summary>
    /// 合併對帳單使用的類別
    /// </summary>
    public class SheetListMergerArgs
    {
        /// <summary>
        /// 分項金額加總
        /// </summary>
        public int sumSubTotal { get; set; }
        /// <summary>
        /// 分項稅額加總
        /// </summary>
        public int sumGstTotal { get; set; }
        /// <summary>
        /// 分項總額加總
        /// </summary>
        public int sumInvoiceTotal { get; set; }
        /// <summary>
        /// 累積金額(分項總額加總÷1.05)
        /// </summary>
        public int accSubTotal { get; set; }
        /// <summary>
        /// 累積稅額(累積金額*0.05)
        /// </summary>
        public int accGstTotal { get; set; }
        /// <summary>
        /// (總額÷1.05) - 分項金額加總
        /// </summary>
        public int accSubTotalDiff { get; set; }
        /// <summary>
        /// (總額÷1.05*0.05) - 分項稅額加總
        /// </summary>
        public int accGstTotalDiff { get; set; }
        /// <summary>
        /// 運費加總
        /// </summary>
        public int sumfreightAmountsTotal { get; set; }
        /// <summary>
        /// 異動金額加總
        /// </summary>
        public int sumadjustmentAmountTotal { get; set; }
        /// <summary>
        /// 逾期出貨金額加總
        /// </summary>
        public int sumPositivePaymentOverdueAmountTotal { get; set; }
        /// <summary>
        /// 逾期出貨金額加總
        /// </summary>
        public int sumNegativePaymentOverdueAmountTotal { get; set; }
        /// <summary>
        /// 全家物流處理費用總額
        /// </summary>
        public int sumispFamilyAmountTotal { get; set; }
        /// <summary>
        /// 7-11物流處理費用總額
        /// </summary>
        public int sumispSevenAmountTotal { get; set; }
        /// <summary>
        /// PCCHOME倉儲費
        /// </summary>
        public int sumWmsAmountTotal { get; set; }
        /// <summary>
        /// PCCHOME倉儲費
        /// </summary>
        public int sumWmsOrderAmountTotal { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealMemberOrderListView
    {
        #region property
        HiDealMemberOrderListPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        bool IsLatest { get; }
        CouponListFilterType FilterType { get; }
        int PageCount { get; set; }
        int PageSize { get; set; }
        string ApiKey { get; set; }

        #endregion
        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler FilterChange;
        #endregion
        #region method
        void GetOrderList(HiDealOrderShowCollection data);
        void SetUpPageCount();
        #endregion
    }
}
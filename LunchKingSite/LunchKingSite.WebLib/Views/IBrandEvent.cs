﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IBrandEvent
    {
        #region property
        bool DiscountPageShow { get; set; }
        string Url { get; }
        string Preview { get; }
        int UserId { get; }
        int BrandId { get; set; }
        string Rsrc { get; set; }
        string BrandTitle { get; set; }
        string OgTitle { get; set; }
        string OgUrl { set; get; }
        string OgDescription { set; get; }
        string OgImage { set; get; }
        int OgImageWidth { set; get; }
        int OgImageHeight { set; get; }
        string LinkCanonicalUrl { set; get; }
        string LinkImageSrc { set; get; }
        string Share_FB_Url { set; get; }
        bool EnableMobileEventPromo { set; get; }
        string BackMainUrl { set; get; }
        string jsContentIds { set; get; }
        string nsContentIds { set; get; }
        Uri LatestUrl { get; }
        string MetaDescription { get; set; }

        #endregion

        #region method
        void ShowBrandEvent(Brand brand, List<ViewBrandCategory> list);
        void ShowEventExpire();
        #endregion

        void SetMemberCollection(List<Guid> memberCollectDealGetOnlineDealGuids);
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;


namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealDefaultView
    {
        event EventHandler<DataEventArgs<string[]>> AddSubscript;

        HiDealDefaultPresenter Presenter { get; set; }

        string CityName { get; set; }
        int CityId { get; }
        string CategoryId { get; }
        int? SpecialId { get; }
        /// <summary>
        /// 設定畫面顯示是一般模式或預覽模式。
        /// </summary>
        HiDealDefaultViewPageMode PageMode { get; }
        /// <summary>
        /// 預覽模式下，會傳入deal guid指定要顯示的檔次
        /// </summary>
        Guid DealGuid { get; }
        /// <summary>
        /// CPA編碼
        /// </summary>
        string ReferrerSourceId { get; }

        void SetContent(ViewHiDealCollection MainDeals, ViewHiDealCollection LastDeals);
        void SetCitys(SystemCodeCollection citys);
        void SetCategory(DataTable Category);
        void SetSpecials(Dictionary<HiDealSpecialDiscountType, int> specials);
        void ShowAlert(string message);
    }

    public enum HiDealDefaultViewPageMode
    {
        /// <summary>
        /// 正常模式
        /// </summary>
        General,
        /// <summary>
        /// 預覽模式
        /// </summary>
        Preview,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealTimeSlotView
    {
        #region property
        HiDealTimeSlotPresenter Presenter { get; set; }
        DateTime StartDate { get; }
        DateTime EndDate { get; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<HiDealTimeSlotViewUpdateSlotEntity>> UpdateTimeSlotSequence;
        event EventHandler<DataEventArgs<int>> SelectRegionIdChange;
        #endregion
        #region method
        void GetHiDealTimeSlot(IList<ViewHiDealCityseq> mainData, IList<ViewHiDealCityseq> subData,
                               ViewHiDealCollection freshVisaPriorityDeals,
                               ViewHiDealCollection staleVisaPriorityDeals);
        void SetRegionData(SystemCodeCollection data, int? defRegion);

        #endregion
    }


    public class HiDealTimeSlotViewUpdateSlotEntity
    {
        public List<KeyValuePair<int, int>> NewSlotList { get; set; }
        public int SelectRegionId { get; set; }
    }
}

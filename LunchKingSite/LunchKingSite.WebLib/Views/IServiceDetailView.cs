﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IServiceDetailView
    {
        #region property
        ServiceDetailPresenter Presenter { get; set; }
        int UserId { get; }
        string ServiceNo { get; }
        string UserData { get; }
        int Priority { get; }
        int Status { get; }
        int MainCategory { get; }
        int SubCategory { get; }
        string ImageUrl { get; }
        FileUpload Image { get;}
        string Content { get; }
        string CustomerTxtArea { get; }
        string ContactTarget { get;}
        string alertMessage { set; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<string>> Save;
        event EventHandler Search;
        event EventHandler Claim;
        event EventHandler<DataEventArgs<int>> Join;
        event EventHandler<DataEventArgs<int>> Notify;
        #endregion

        #region method
        void SetMemberData(ViewMemberBuildingCityParentCity m, MobileMember mm);
        void SetMemberHide();
        void SetMemberAddress(string address);
        void SetMemberLinkList(MemberLinkCollection mlc);
        void SetMemberBonus(ViewMemberPromotionTransactionCollection vmptc);
        void SetMemberScash(decimal scash, decimal scashE7, decimal scashPez);
        void SetServiceDetail(ViewCustomerServiceList vscls,ViewCustomerServiceInsideLogCollection vcsi,ViewCustomerServiceOutsideLogCollection vsco);

        void MessageList(ViewCustomerServiceListCollection MessageList);
        #endregion

    }

}

using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IErrorView : ILocalizedView
    {
        ErrorPresenter Presenter { get; }
        string RequestMode { get; }
        string ErrorPath { get; }
        string ErrorMessage { get; set; }
        string ErrorCode { get; }
        string ErrorTitle { get; set; }
    }
}

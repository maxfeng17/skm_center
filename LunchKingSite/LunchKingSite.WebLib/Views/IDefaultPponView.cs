﻿using System;
using System.Collections.Generic;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Views
{
    public interface IDefaultPponView
    {
        #region Event
        event EventHandler CheckEventMail;
        #endregion

        #region Property

        DefaultPponPresenter Presenter { get; set; }
        Guid BusinessHourId { get; set; }
        int BusinessHourUniqueId { get; set; }
        CategorySortType SortType { get; set; }
        int Encores { get; set; }
        string UserName { get; }
        int UserId { get; }
        DeliveryType[] RepresentedDeliverytype { get; } //20120314 前端以憑證宅配撈取
        bool IsDeliveryDeal { set; get; }
        bool TwentyFourHours { get; set; }
        string HamiTicketId { get; }
        int? DealType { get; set; }
        //bool EntrustSell { set; }
        string PicAlt { get; set; }
        bool isUseNewMember { get; set; }
        bool isMultipleMainDeals { get; set; }
        bool BypassSsoVerify { get; set; }
        //IExpireNotice ExpireNotice { get; }
        bool ShowEventEmail { get; set; }
        string EdmPopUpCacheName { get; set; }

        /// <summary>
        /// 埋Code
        /// </summary>
        string DealArgs { set; get; }

        bool IsOpenNewWindow { get; set; }
        bool IsMobileBroswer { get; }

        #region city, category相關

        int CityId { get; set; }
        int CategoryIdInPiinLife { get; }
        int TravelCategoryId { get; }
        int FemaleCategoryId { get; }
        int SubRegionCategoryId { get; }
        List<int> FilterCategoryIdList { get; }
        List<CategoryNode> DealChannelList { get; set; }
        /// <summary>
        /// 當前頁面所選城市的Id
        /// </summary>
        int SelectedCityId { get; }
        /// <summary>
        /// 當前頁面所選頻道的Id
        /// </summary>
        int SelectedChannelId { get; }
        /// <summary>
        /// 當前頁面所選地區的Id
        /// </summary>
        int SelectedRegionId { get; }
        /// <summary>
        /// 當前頁面所選分類的Id
        /// </summary>
        int SelectedSubcategoryId { get; }
        /// <summary>
        /// 當前頁面所選新版分類的Id
        /// </summary>
        int SelectedDealCategoryId { get; }

        #endregion city, category相關

        #endregion

        #region Method

        void SetMailInfo(int cityId);
        bool SetCookie(string value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message);
        void NoRobots();    //2010.03.02 增加福利網不被搜尋引擎index meta .Died 
        void SeoSetting(ViewPponDeal d, string appendTitle = ""); //2011.01.04 SEO .Died

        //新首頁
        void SetMultipleCategories(List<CategoryViewItem> categoryInfos,
            List<CategoryDealCount> filterItemList, SystemCodeCollection piinlifeLinks);
        void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals);
        void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals);
        void SetMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        void SetMemberCollection(List<Guid> collectBids);
        //跳窗
        void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair);
        void RedirectToPiinlife();
        void RedirectToDefault();

        #endregion
    }

    public enum CategoryOpenPageType
    {
        Redirect, OpenNew, Ajax
    }

    public class CategoryViewItem : CategoryDealCount
    {
        public CategoryViewItem(SystemCode code) : base(code.CodeId, code.CodeName)
        {
            this.IsPponCategory = false;
            this.Children = new List<CategoryViewItem>();
            this.OpenPage = CategoryOpenPageType.OpenNew;
        }
        public CategoryViewItem(CategoryDealCount categoryInfo) : base(categoryInfo.CategoryId, categoryInfo.CategoryName)
        {
            this.DealCount = categoryInfo.DealCount;
            this.IsPponCategory = true;
            this.Children = new List<CategoryViewItem>();
            this.OpenPage = CategoryOpenPageType.Redirect;
        }
        public CategoryViewItem(int categoryId, string categoryName) : base(categoryId, categoryName)
        {
            this.IsPponCategory = true;
            this.Children = new List<CategoryViewItem>();
            this.OpenPage = CategoryOpenPageType.Redirect;
        }

        [JsonIgnore]
        public bool Selected { get; set; }
        public string ClassNames
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                if (this.Children.Count > 0)
                {
                    sb.Append("sub-arrow");
                }
                if (this.Selected)
                {
                    if (sb.Length > 0)
                    {
                        sb.Append(' ');
                    }
                    sb.Append("on");
                }
                return sb.ToString();
            }
        }
        public string IconHtml
        {
            get
            {
                if (IsPponCategory)
                {
                    return GetCategoryIconClass(this.CategoryId);
                }
                else
                {
                    return GetPiinlifeCategoryIconClass(this.CategoryName);
                }
            }
        }

        public string Url { get; set; }
        [JsonIgnore]
        public List<CategoryViewItem> Children { get; set; }

        [JsonIgnore]
        public bool IsPponCategory { get; set; }
        
        [JsonIgnore]
        public CategoryOpenPageType OpenPage { get; set; }

        private string GetCategoryIconClass(int categoryId)
        {
            if (categoryId == 0)
            {
                return string.Empty;
            }
            string cssClss = string.Empty;
            switch (CategoryManager.GetCategoryIconTypeById(categoryId))
            {
                case CategoryIconType.Hot:
                    return "<div class='sortrank-hot'></div>";
                case CategoryIconType.New:
                    return "<div class='sortrank-new'></div>";
                case CategoryIconType.None:
                default:
                    return string.Empty;
            }
        }

        private string GetPiinlifeCategoryIconClass(string categoryName)
        {
            if (categoryName == "頂級餐廳" || categoryName == "每周推薦")
            {
                return "<div class='sortrank-new'></div>";
            }
            return "<div class='sortrank-hot'></div>";
        }

        public string GetTarget()
        {
            if (this.OpenPage == CategoryOpenPageType.OpenNew)
            {
                return "_blank";
            }
            return string.Empty;
        }

        public string GetHref()
        {
            if (this.OpenPage == CategoryOpenPageType.Ajax)
            {
                return "javascript:void(0)";
            }
            return this.Url;
        }

        public string GetDataUrl()
        {
            if (this.OpenPage == CategoryOpenPageType.Ajax)
            {
                return this.Url;
            }
            return string.Empty;
        }

        public override string ToString()
        {
            if (this.Selected)
            {
                return base.ToString() + " selected";
            }
            return base.ToString();
        }

    }
}

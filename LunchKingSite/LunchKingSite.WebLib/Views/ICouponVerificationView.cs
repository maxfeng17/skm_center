﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using SubSonic;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ICouponVerificationView
    {
        #region events
        event EventHandler<DataEventArgs<string>> SearchCoupon;
        event EventHandler<EventArgs> SearchCouponByType;
        event EventHandler<DataEventArgs<string>> SearchHiDealCoupon;
        event EventHandler<DataEventArgs<string>> SearchTmallCoupon;
        event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> ForceRefund;
        event EventHandler<DataEventArgs<int>> ForceVerify;
        event EventHandler<DataEventArgs<int>> Verify;
        event EventHandler<DataEventArgs<int>> Unverify;
        #endregion

        #region properties
        string UserName { get; }
        string CouponSn { get; }
        int orderClassification { get; }
        CouponVerificationPresenter Presenter { get; set; }
        #endregion

        #region methods
        void SetCouponList(Collection<VerifyingCoupon> couponInfo);
        void SetProcessResultMessage(string msg);
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        string GetFilterTypeDropDown();
        #endregion

    }

    public class VerifyingCoupon
    {
        public int OrderClassification { get; set; }
        public string pid { get; set; }
        public int CouponId { get; set; }
        public string CouponSn { get; set; }
        public string CouponCode { get; set; }
        public string OrderId { get; set; }
        public Guid OrderGuid { get; set; }
        public CouponUsageStatus CouponUseStatus { get; set; }
        public bool DealNoRefund { get; set; }                   //CouponUseStatus = 6 時, 需要去查設定: 不給退(GroupOrderStatus.NoRefund)
        public bool DealExpireNoRefund { get; set; }       //CouponUseStatus = 6 時, 需要去查設定: 過期不給退 (GroupOrderStatus.ExpireNoRefund)
        public bool DealVerificationLost { get; set; }        //是否清冊遺失  (AccountingFlag.DealVerificationLost)
        public bool DealVerified { get; set; }                       //檔次是否核銷中  (!AccountingFlag.DealVerificationFinished && !AccountingFlag.DealVerificationClosed)
        public bool IsRefundCashOnly { get; set; }
    }

    public enum CouponUsageStatus
    {
        Verified = 1,   //(TrustStatus.Verified)       [go to Enumeration MemberConfig for more details]
        Refunded = 2,   //(TrustStatus.Returned || TrustStatus.Refunded)
        ForcedVerified = 3, //(TrustSpecialStatus.MandatoryVerification && TrustStatus.Verified )
        ForcedRefunded = 4, //( TrustSpecialStatus.MandatoryReturn && (TrustStatus.Returned || TrustStatus.Refunded))
        LostVerified = 5,   //(TrustStatus.Verified && AccountingFlag.DealVerificationLost)
        Unverified = 6,  //(TrustStatus.Initial || TrustStatus.Trusted)
        RefundedBeforeClose = 7  //結檔前退貨  退貨時間 < 結檔時間
    };
}

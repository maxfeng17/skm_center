﻿using System;
using System.IO;

namespace LunchKingSite.WebLib.Views
{
    public interface IAtmAchView
    {
        event EventHandler<DataEventArgs<StreamReader>> BatchImportClicked;
        event EventHandler<DataEventArgs<StreamReader>> BatchImportManualClicked;

        string UserName { get; }
        string FundTransferType { get; }

        string ImportResult { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IVourcherEventEditView
    {
        #region property
        VourcherEventEditPresenter Presenter { get; set; }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        string UserName { get; }
        /// <summary>
        /// 總資料數量
        /// </summary>
        int PageCount { get; set; }
        /// <summary>
        /// 分頁的每頁大小
        /// </summary>
        int PageSize { get; }
        /// <summary>
        /// 搜尋優惠券的欄位和輸入資料
        /// </summary>
        KeyValuePair<string, string> SearchKeys { get; }
        /// <summary>
        /// 優惠券活動Id
        /// </summary>
        int VourcherEventId { get; set; }
        /// <summary>
        /// 賣家Guid
        /// </summary>
        Guid SellerGuid { get; set; }
        /// <summary>
        /// 優惠券狀態(搜尋條件)
        /// </summary>
        int Status { get; }
        #endregion
        #region event
        /// <summary>
        /// 撈取分頁
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChanged;
        /// <summary>
        /// 計算總資料數(分頁用)
        /// </summary>
        event EventHandler GetDataCount;
        /// <summary>
        /// 依賣家Id搜尋賣家資料
        /// </summary>
        event EventHandler<DataEventArgs<string>> GetSellerById;
        /// <summary>
        /// 傳入優惠券內容和分店儲存
        /// </summary>
        event EventHandler<DataEventArgs<KeyValuePair<VourcherEvent, VourcherStoreCollection>>> SaveVourcherEventStores;
        /// <summary>
        /// 撈取優惠券
        /// </summary>
        event EventHandler<DataEventArgs<int>> GetVourcherEvent;
        /// <summary>
        /// 搜尋優惠券內容
        /// </summary>
        event EventHandler SearchVourcherEvent;
        /// <summary>
        /// 更新優惠券狀態
        /// </summary>
        event EventHandler<DataEventArgs<KeyValuePair<int, string>>> UpdateVourcherEventStatus;
        #endregion
        #region method
        /// <summary>
        /// 回傳賣家和其分店及賣家的修改資料
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="stores">分店資料</param>
        void SetSellerStores(Seller seller, StoreCollection stores);
        /// <summary>
        /// 回傳優惠券資料和其對應分店資料
        /// </summary>
        /// <param name="vourcher_event">優惠券資料</param>
        /// <param name="vourcher_stores">對應分店資料</param>
        void SetVourcherEventStores(Seller seller, StoreCollection stores, VourcherEvent vourcher_event, VourcherStoreCollection vourcher_stores);
        /// <summary>
        /// 回傳搜尋的優惠券內容
        /// </summary>
        /// <param name="vourcher_events">優惠券列表</param>
        void SetVourcherEventCollection(VourcherEventCollection vourcher_events);
        /// <summary>
        /// 回傳退件的優惠券
        /// </summary>
        /// <param name="vourcher_events">退件的優惠券</param>
        void SetApplyCaseVourcherEvent(VourcherEventCollection vourcher_events);
        /// <summary>
        /// 更新優惠券審核按鈕狀態
        /// </summary>
        /// <param name="status">目前優惠券狀態</param>
        void SetVourcherEventStatus(int status, string message);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserExchange
    {
        #region property

        UserExchangePresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        string PanelMode { get; set; }
        Guid OrderGuid { get; set; }
        string OrderID { get; }
        TextBox TxtOrderId { get; }
        string alertMessage { set; }
        bool IsCoupon { get; set; }
        #endregion property

        #region event

        event EventHandler<DataEventArgs<string>> GetOrderInfo;
        event EventHandler<ExchangeFormArgs> RequestExchange;

        #endregion event

        #region method

        void SetOrderInfo(ViewCouponListMain orderinfo, ViewOrderShipList osInfo);
        void ExchangePanel(string panelmode);

        #endregion method
    }

    public sealed class ExchangeFormArgs : EventArgs
    {
        public string ExchangeReason { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverAddress { get; set; }
    }
}

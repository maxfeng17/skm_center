﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHidealOrderDetailListView
    {
        #region property
        HidealOrderDetailListPresenter Presenter { get; set; }
        /// <summary>
        /// 訂單狀態紀錄顯示
        /// </summary>
        IAuditBoardView AuditBoardView { get; }
        /// <summary>
        /// 操作人員帳號
        /// </summary>
        string UserName { get; }
        Guid OrderGuid { get; }
        int OrderPk { get; }
        int ProductId { get; }
        string RefundFormUrl { get; }
        bool IsRefundCashOnly { get; set; }
        ShipCompanyCollection ShipCompanyCol { get; set; }
        string alertMessage { set; }
        string CurrentUser { get; }
        string ShipCompanyName { get; set; }
        int GoodsReturnStatus { get; set; }
        DateTime ShipStartDate { get; set; }
        int ShipMemoLimit { get; }
        bool IsLockRefund { set; get; }
        DropDownList ddlServiceCategory { get; }
        DropDownList ddlServiceSubCategory { get; }

        #endregion

        #region event
        event EventHandler<DataEventArgs<int>> ReturnCash;
        event EventHandler<DataEventArgs<int>> ReturnComplete;
        /// <summary>
        /// 強制退貨，退回現金(刷退貨ATM  ATM還未實作)
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> ReturnAllPassAndCashBack;
        /// <summary>
        /// 強制退貨，退回購物金
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> ReturnAllPassAndPointBack;
        /// <summary>
        /// 建立退貨單，走正常退貨流程。
        /// </summary>
        event EventHandler<DataEventArgs<HidealOrderDetailListViewMakeReturnedForm>> MakeReturnedForm;

        /// <summary>
        /// 發票相關修正
        /// </summary>
        event EventHandler<DataEventArgs<InvoiceUpdateData>> InvoiceUpdate;
        /// <summary>
        /// 取消退貨單作業。
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> ReturnedFormCancel;
        /// <summary>
        /// 新增客服紀錄作業
        /// </summary>
        event EventHandler<DataEventArgs<ServiceMessage>> OnSetServiceMsgClicked;
        /// <summary>
        /// 新增出貨紀錄
        /// </summary>
        event EventHandler<DataEventArgs<OrderShipArgs>> AddOrderShip;
        /// <summary>
        /// 刪除出貨紀錄
        /// </summary>
        event EventHandler<DataEventArgs<OrderShipArgs>> DeleteOrderShip;
        /// <summary>
        /// 修改出貨紀錄
        /// </summary>
        event EventHandler<DataEventArgs<OrderShipArgs>> UpdateOrderShip;

        /// <summary>
        /// 寄發退貨申請書Email
        /// </summary>
        event EventHandler<DataEventArgs<RefundFormPageInfo>> SendRefundApplicationFormNotification;

        /// <summary>
        /// 寄發折讓單Email
        /// </summary>
        event EventHandler<DataEventArgs<RefundFormPageInfo>> SendDiscountSingleFormNotification;
        /// <summary>
        /// 更新發票的折讓單是否寄回及紙本發票是否寄回資訊
        /// </summary>
        event EventHandler<DataEventArgs<EInvoiceMainInfo>> UpdateEInvoiceMainInfo;

        /// <summary>
        /// 退貨單資料更新
        /// </summary>
        event EventHandler<DataEventArgs<RefundFormBackInfo>> UpdateReturned;

        #endregion

        #region method

        /// <summary>
        /// 處理訂單畫面的顯示
        /// </summary>
        /// <param name="hodc">訂單明細</param>
        /// <param name="ho">訂單</param>
        /// <param name="m">會員資料</param>
        /// <param name="couponList">憑證資料</param>
        /// <param name="hdrc">退貨單資料</param>
        /// <param name="inv">發票資料</param>
        /// <param name="paymentTransColl">付款紀錄</param>
        /// <param name="product">商品資料</param>
        void ShowPageData(HiDealOrderDetailCollection hodc, HiDealOrder ho, Member m,
            ViewHiDealCouponTrustStatusCollection couponList, HiDealReturnedCollection hdrc,
            EinvoiceMainCollection invCol, PaymentTransactionCollection paymentTransColl,
            HiDealProduct product, List<HiDealInvoiceGoodsInfo> hiDealInvoiceGoodsInfos,
            List<HiDealInvoiceCouponInfo> hiDealInvoiceCouponInfos);
        void ShowMessage(string message);
        void SetDeliverData(ViewOrderShipListCollection osCol);
        void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates, DropDownList ddl);

        #endregion
    }

    public class HidealOrderDetailListViewMakeReturnedForm
    {
        /// <summary>
        /// 要退貨的訂單編號
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 是否退回現金。
        /// </summary>
        public bool IsCashBack { get; set; }
    }


    public class InvoiceUpdateData
    {
        /// <summary>
        /// 折讓單寄回
        /// </summary>
        public bool AllowanceFormBack { get; set; }
        /// <summary>
        /// 發票寄回
        /// </summary>
        public bool InvoiceFormBack { get; set; }
        /// <summary>
        /// 購買人
        /// </summary>
        public string BuyerName { get; set; }
        /// <summary>
        /// 購買人地址
        /// </summary>
        public string BuyerAddress { get; set; }
        /// <summary>
        /// 公司統編
        /// </summary>
        public string CompanyId { get; set; }
        /// <summary>
        /// 公司抬頭
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 發票註記
        /// </summary>
        public string Message { get; set; }

        public InvoiceMode2 InvoiceType { get; set; }

        public CarrierType CarrierType { get;set; }

        public string CarrierId { get; set; }
    }

    /// <summary>
    /// PiinLife訂單憑證發票相關資訊
    /// </summary>
    public class HiDealInvoiceCouponInfo
    {
        /// <summary>
        /// 憑證編號
        /// </summary>
        public string CouponSequence { get; set; }
        /// <summary>
        /// 憑證確認碼
        /// </summary>
        public string CouponCode { get; set; }
        /// <summary>
        /// 憑證狀態
        /// </summary>
        public int? CouponStatus { get; set; }
        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNumber { get; set; }
        /// <summary>
        /// 發票是否中獎
        /// </summary>
        public bool InvoiceWinning { get; set; }
        /// <summary>
        /// 檢視折讓單URL
        /// </summary>
        public string DiscountSingleFormURL { get; set; }
        /// <summary>
        /// 折讓單是否已寄回
        /// </summary>
        public bool? IsInvoiceMailbackAllowance { get; set; }
        /// <summary>
        /// 紙本發票是否已寄回
        /// </summary>
        public bool? IsInvoiceMailbackPaper { get; set; }
        /// <summary>
        /// (EInvoice)發票主檔Id
        /// </summary>
        public int EInvoiceId { get; set; }

        public bool IsReservationLock { get; set; }

        public HiDealInvoiceCouponInfo() { }

        public HiDealInvoiceCouponInfo(string couponSequence, string couponCode, int couponStatus, string invoiceNumber, bool invoiceWinning, string discountSingleFormURL, bool isInvoiceMailbackAllowance, bool isInvoiceMailbackPaper, int einvoiceId, bool isReservationLock)
        {
            CouponSequence = couponSequence;
            CouponCode = couponCode;
            CouponStatus = couponStatus;
            InvoiceNumber = invoiceNumber;
            InvoiceWinning = invoiceWinning;
            DiscountSingleFormURL = discountSingleFormURL;
            IsInvoiceMailbackAllowance = isInvoiceMailbackAllowance;
            IsInvoiceMailbackPaper = isInvoiceMailbackPaper;
            EInvoiceId = einvoiceId;
            IsReservationLock = isReservationLock;
        }
    }

    /// <summary>
    /// PiinLife訂單商品發票相關資訊
    /// </summary>
    public class HiDealInvoiceGoodsInfo
    {
        // 商品名稱	商品總數	狀態	發票號碼	中獎發票	折讓單	折讓單已回	發票已回
        /// <summary>
        /// 訂單商品名稱
        /// </summary>
        public string OrderItemName { get; set; }
        /// <summary>
        /// 訂單商品總數
        /// </summary>
        public int OrderItemQuantity { get; set; }
        /// <summary>
        /// 商品憑證狀態
        /// </summary>
        public int? CouponStatus { get; set; }
        /// <summary>
        /// 發票號碼
        /// </summary>
        public string InvoiceNumber { get; set; }
        /// <summary>
        /// 發票是否中獎
        /// </summary>
        public bool InvoiceWinning { get; set; }
        /// <summary>
        /// 檢視折讓單URL
        /// </summary>
        public string DiscountSingleFormURL { get; set; }
        /// <summary>
        /// 折讓單是否已寄回
        /// </summary>
        public bool? IsInvoiceMailbackAllowance { get; set; }
        /// <summary>
        /// 紙本發票是否已寄回
        /// </summary>
        public bool? IsInvoiceMailbackPaper { get; set; }
        /// <summary>
        /// (EInvoice)發票主檔Id
        /// </summary>
        public int EInvoiceId { get; set; }

        public HiDealInvoiceGoodsInfo() { }

        public HiDealInvoiceGoodsInfo(string orderItemName, int orderItemQuantity, int couponStatus, string invoiceNumber, bool invoiceWinning, string discountSingleFormURL, bool isInvoiceMailbackAllowance, bool isInvoiceMailbackPaper)
        {
            OrderItemName = orderItemName;
            OrderItemQuantity = orderItemQuantity;
            CouponStatus = couponStatus;
            InvoiceNumber = invoiceNumber;
            InvoiceWinning = invoiceWinning;
            DiscountSingleFormURL = discountSingleFormURL;
            IsInvoiceMailbackAllowance = isInvoiceMailbackAllowance;
            IsInvoiceMailbackPaper = isInvoiceMailbackPaper;
        }
    }

    public class EInvoiceMainInfo
    {
        public int EInvoiceId { get; set; }
        public bool IsInvoiceMailbackAllowance { get; set; }
        public bool IsInvoiceMailbackPaper { get; set; }
    }

    public class RefundFormPageInfo
    {
        public int ReturnedId { get; set; }
        public int OrderPk { get; set; }
        public string UserName { get; set; }
        public int EinvoiceId { get; set; }
    }
    public class RefundFormBackInfo
    {
        public int ReturnedId { get; set; }
        public bool IsRefundFormBack { get; set; }
    }
}

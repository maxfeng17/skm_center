﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface ISkmView
    {
        bool IsOpenNewWindow { get; set; }
        string SiteUrl { get; set; }
        CategorySortType SortType { get; set; }
        int CityId { get; }
        int AreaId { get; }
        int CategoryId { get; }
        bool IsMobileBroswer { get; }

        void SetSkmDeals(List<LunchKingSite.BizLogic.Component.MultipleMainDealPreview> SkmDeals);

        void SetMultipleCategories(List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>> dealCategoryCountList, List<CategoryDealCount> filterItemList, SystemCodeCollection piinlifeLinks);
    }
}

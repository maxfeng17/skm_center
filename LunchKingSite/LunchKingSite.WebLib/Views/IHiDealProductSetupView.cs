﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealProductSetupView
    {
        int Pid { get; }
        Guid Gpid { get; set; }
        int Did { get; set; }
        string ProductName { get; set; }
        string ProductDescription { get; set; }
        bool IsShow { get; set; }
        string UseStartDate { get; set; }
        string UseEndDate { get; set; }
        string ChangeExpireDate { get; set; }
        string OriginalPrice { get; set; }
        string Price { get; set; }
        string TotalQuantity { get; set; }
        string PerOrderLimit { get; set; }
        string PerBuyerLimit { get; set; }
        bool IsNoRefund { get; set; }
        bool IsDaysNoRefund { set; get; }
        bool IsExpireNoRefund { get; set; }
        bool IsTaxFree { get; set; }
        bool IsInputTaxFree { get; set; }
        bool IsHomeDelivery { get; set; }
        string HomeDeliveryDescription { get; set; }
        int? HomeDeliveryQuantity { get; set; }
        bool IsInStore { get; set; }
        string InStoreDescription { get; set; }
        DataTable StoreSettings { get; set; }   //columns: [QuantityLimit] [Sequence] [StoreGuid] [StoreName] [StoreAddress]
        string OrderedStoreSettings { get; }    //format (quan_limit is optional):  "store1_guid, seq, quan_limit; store2_guid, seq, quan_limit"  
        bool IsCouponBySpecifics { get; }
        string SmsTitle { get; set; }
        string SmsPrefix { get; set; }
        string SmsInformation { get; set; }
        bool DisableSMS { get; set; }
        bool NotDeliveryIslands { get; set; }
		bool IsShowDiscount { get; set; }
        /// <summary>
        ///開立發票註記
        /// </summary>
        bool IsInvoiceCreate { get; set; }
        /// <summary>
        /// 檔次配合使用訂位系統
        /// </summary>
        int BookingSystemType { set; get; }
        /// <summary>
        /// 提前預約天日
        /// </summary>
        int AdvanceReservationDays { set; get; }
        /// <summary>
        /// 優惠券使用人數
        /// </summary>
        int CouponUsers { set; get; }
        /// <summary>
        /// 商家系統-預約設定-訂房鎖定
        /// </summary>
        bool IsReserveLock { get; set; }

        /* XML格式, 但特殊字元 '<' '>' 用 '[' ']' 取代, 避開傳輸時 ASP.NET 安全性檢查的問題
        [Optionals]
            [OptCatg catgName="顏色"]
                [OptItem]紅[/OptItem]
                [OptItem]白[/OptItem]
            [/OptCatg]
            [OptCatg catgName="大小"]
                [OptItem quantity="100"]大[/OptItem]
                [OptItem quantity="120"]中[/OptItem]
                [OptItem quantity="60"]小[/OptItem]
            [/OptCatg]
        [/Optionals]
         */
        string OptionalSettings { get; set; }
        bool PayToCompany { get; set; }
        /// <summary>
        /// 成套販售，此模式下使用者不能選擇購買份數，購買數為每單限購數。
        /// </summary>
        bool IsCombo { get; set; }
        VendorBillingModel BillingModel { get; set; }
        RemittanceType PaidType { get; set; }
        VendorReceiptType ReceiptType { get; set; }
        string AccountingMessage { get; set; }

        Collection<Freight> FreightIncome { get; set; }
        Collection<Freight> FreightExpense { get; set; }
        Collection<ProductCost> ProductCosts  { get; set; }    

        event EventHandler SaveProduct;
        event EventHandler<DataEventArgs<CostArgs>> NewProductPurchaseCost;
        event EventHandler DeleteProductPurchaseCost;
        event EventHandler<DataEventArgs<FreightArgs>> NewFreightIncome;
        event EventHandler DeleteFreightIncome;
        event EventHandler<DataEventArgs<FreightArgs>> NewFreightExpense;
        event EventHandler DeleteFreightExpense;

        void RefreshProductCost();
        void RefreshFreightIncome();
        void RefreshFreightExpense();
    }

    [Serializable]
    public class Freight
    {
        public int Id { get; set; }
        public float FrieghtValue { get; set; }
        public float ThresholdAmount { get; set; }
        public string Action { get; set; }  // "add", "delete"
    }

    [Serializable]
    public class ProductCost
    {
        public int Id { get; set; }
        public string Cost { get; set; }
        public int Quantity { get; set; }
        public int CumulativeQuantity { get; set; }
        public string Action { get; set; }
    }

    public class CostArgs
    {
        public string Cost { get; private set; }
        public string Quantity { get; private set; }
        
        public CostArgs(string cost, string quantity)
        {
            if(string.IsNullOrEmpty(cost))
                throw new ArgumentNullException(cost, "\"進貨價格\"必須有值!");
            if(string.IsNullOrEmpty(quantity))
                throw new ArgumentNullException(quantity, "\"進貨數量\"必須有值!");
            
            Cost = cost;
            Quantity = quantity;
        }
    }

    public class FreightArgs
    {
        public string FreightValue { get; private set; }
        public string ThresholdAmount { get; private set; }
        public FreightArgs(string freightValue, string thresholdAmount)
        {
            if(string.IsNullOrEmpty(freightValue))
                throw new ArgumentNullException(freightValue, "\"運費\"必須有值!");
            if(string.IsNullOrEmpty(thresholdAmount))
                throw new ArgumentNullException(thresholdAmount, "\"購買金額\"必須有值!");

            FreightValue = freightValue;
            ThresholdAmount = thresholdAmount;
        }
    }
}

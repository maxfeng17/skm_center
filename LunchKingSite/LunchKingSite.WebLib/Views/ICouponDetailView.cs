﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ICouponDetailView
    {
        CouponDetailPresenter Presenter { get; set; }
        string UserName { get; }
        Guid? PreviewBizHourId { get; }
        int? CouponId { get; }
        string GoogleApiKey { set; }
        string EncryptedCouponCode { set; }
        bool ForceStaticMap { get; }
        string SequenceNumber { get; }
        string CouponCode { get; }
        string MemberName { get; }
        string DealTags { set; }
        CouponCodeType CodeType { set; get; }
        void SetDetail(ViewPponCoupon coupon, IViewPponDeal vpd);
        string AvailablesInfo { set; get; }
        bool IsZeroActivityShowCoupon { set; get; }
        string CreateTime { get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IReturnApplicationView
    {
        ReturnApplicationPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        Guid OrderGuid { get; }

        DateTime ReturnTime { get; set; }
        string CouponList { get; set; }
        string DealNumber { get; set; }
        string DealName { set; }
        string OrderId { get; set; }
        string message { set; }
        bool isleagl { set; }
        bool IsAtm { set; }

        void SetAtm(CtAtmRefund atm);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model;
using System.ComponentModel;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserServiceOutgrateView
    {
        #region property
        UserServiceOutgratePresenter Presenter { get; set; }
        int FilterTypeValue { get; }
        UserServiceIntergrateIDEASFilterType IDEASFilterType { get; }
        UserServiceIntergrateTmallFilterType TmallFilterType { get; }
        /// <summary>
        /// 查詢資策會訂單時使用的條件
        /// </summary>
        string IDEASSearchData { get; }
        /// <summary>
        /// 天貓檔次搜尋條件
        /// </summary>
        string TmallSearchData { get; }
        int UserId { get; }
        string UserData { get; set; }
        string UserDataView { get; set; }
        Dictionary<Subscription, bool> UnsubscribeList { get; }
        Dictionary<HiDealSubscription, bool> UnHidealsubscribeList { get; }
        string ExternalUserId { get; set; }
        int PageSize { get; set; }
        string SortExpression { get; }
        string HidealSortExpression { get; }
        MemberPromotionDeposit MemberPromotionDeposit { get; }
        bool IfReceiveEDM { get; set; }
        bool IfReceiveStartedSell { get; set; }
        bool IfReceivePiinlifeEDM { get; set; }
        bool IfReceivePiinlifeStartedSell { get; set; }
        int RefundScashAmount { get; }
        string CreateId { get; }
        bool CBIsFunds { get; }
        bool IsBcashPayByVendor { get; }
        Guid? DealGuid { get; }
        string GetStatusTypeDropDown { get; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnStatusSelected;
        event EventHandler OnIDEASSearchClicked;
        event EventHandler OnBcashAddClicked;
        event EventHandler OnRefundScashClicked;
        event EventHandler OnTmallSearchClicked;
        event EventHandler<DataEventArgs<ServiceMessage>> OnSetServiceMsgClicked;
        event EventHandler<EventArgs> OnSubscribeUpdateClick;
        event EventHandler<DataEventArgs<int>> OnGetOrderCount;
        event EventHandler<DataEventArgs<int>> OrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetHidealOrderCount;
        event EventHandler<DataEventArgs<int>> HidealOrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetServiceCount;
        event EventHandler<DataEventArgs<int>> ServicePageChanged;
        event EventHandler<DataEventArgs<int>> OnGetLunchKingCount;
        event EventHandler<DataEventArgs<string>> GetBonusListInfo;
        event EventHandler<DataEventArgs<int>> OnGetIDEASOrderCount;
        event EventHandler<DataEventArgs<int>> OnIDEASOrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetTmallCount;
        event EventHandler<DataEventArgs<int>> OnTmallPageChanged;

        event EventHandler<DataEventArgs<string>> GetMembershipCardListInfo;
        event EventHandler<DataEventArgs<int>> OnGetMembershipCardCount;
        event EventHandler<DataEventArgs<int>> MembershipCardPageChanged;

        #endregion

        #region method
        void SetUserName(MemberCollection mc);
        void SetMemberData(ViewMemberBuildingCityParentCity m, MobileMember mm);
        void SetMemberAddress(string address);
        void SetMemberLinkList(MemberLinkCollection mlc);
        void SetMemberBonus(ViewMemberPromotionTransactionCollection vmptc);
        void SetMemberScash(decimal scash);
        void SetOrderData(DataTable dt);

        void SetHidealOrderList(ViewHiDealOrderDealOrderReturnedCollection vhodor);

        void SetLunchKingOrderData(DataTable dt);

        void SetBonusList(AuditCollection data);
        void SetBonusData(AuditCollection data, Dictionary<ViewMemberPromotionTransaction, string> dataList, ViewMemberCashpointListCollection vmclc, List<UserScashTransactionInfo> vstc, ViewOrderMemberPaymentCollection vompcp, ViewOrderMemberPaymentCollection vompcc, ViewDiscountOrderCollection vdoc);

        void SetMembershipCardData(List<ViewMembershipCardLog> membbershipCardList);
        void SetServiceMessageList(Dictionary<ServiceMessage, Guid> dataList);

        void SetSubscriptionList(SubscriptionCollection subscriptionList, HiDealSubscriptionCollection hidealSubscriptionList);

        void SetIDEASOrderList(ViewCompanyUserOrderCollection orderCollection);
        void SetTmallList(ViewOrderCorrespondingSmsLogCollection logCollection);
        void SetStatusList(Dictionary<string, string> list);
        void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates);
        #endregion

    }

}

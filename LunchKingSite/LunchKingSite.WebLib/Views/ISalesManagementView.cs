﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesManagementView
    {
        #region property
        SalesManagementPresenter Presenter { get; set; }
        string SalesId { get; }
        string LoginUser { get; }
        string SellerName { get; }
        string CompanyName { get; }
        string SalesName { get; }
        string CreateIdSearch { get; }
        string BusinessOrderId { get; }
        DateTime? CreateTimeStart { get; }
        DateTime? CreateTimeEnd { get; }
        DateTime? BusinessStartTimeS { get; }
        DateTime? BusinessStartTimeE { get; }
        List<int> Status { get; }
        List<string> SalesDept { get; }
        int PageSize { get; set; }
        string Separator { get; }
        Level SalesLevel { get; set; }
        string SalesArea { get; set; }
        BusinessType BusinessType { get; }
        bool IsTravelType { get; }
        bool IsMohistVerify { get; }
        bool IsTrustHwatai { get; }
        InvoiceType InvoiceType { get; }
        bool IsNotContractSend { get; }
        bool IsViewer { get; }
        bool IsSalesAssistant { get; }
        bool IsDownloadPrivilege { get; }
        string EmpName { get; set; }
        string EmpDeptId { get; set; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler<DataEventArgs<Dictionary<string, string>>> OnDeptSaveClicked;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> OnDownload;
        event EventHandler<DataEventArgs<string>> OnShow;
        #endregion

        #region method
        void GetSalesDepartment(List<Department> deptc);
        void GetSalesLeaderList(Dictionary<SalesMember, List<Department>> leaderList);
        void GetBusinessList(IEnumerable<BusinessOrder> dataList);
        void DownloadTemplate(BusinessOrderDownloadType downloadType, BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList);
        void OpenHtmlWindow(BusinessType type, Dictionary<string, string> dataList, List<SalesStore> storeList);
        void ShowMessage(string msg);
        #endregion
    }
}

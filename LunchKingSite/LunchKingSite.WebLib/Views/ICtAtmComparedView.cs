﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface ICtAtmComparedView
    {
        #region event
        event EventHandler<DataEventArgs<StreamReader>> ImportClicked;
        event EventHandler Search;
        #endregion event

        #region property
        string AtmDate { get; }
        #endregion property

        void SetContentList(List<string> cols);
    }
}

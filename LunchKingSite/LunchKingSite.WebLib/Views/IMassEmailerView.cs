﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IMassEmailerView
    {
        event EventHandler<DataEventArgs<string>> Send;
        event EventHandler<DataEventArgs<string>> EdmSend;

        MassEmailerPresenter Presenter { get; set; }
        MassMailType MailType { get; }
        string Subject { get; }
        string Description { get; }
        DateTime? SendTime { get; set; }
        MassMailFilterType FilterType { get; }
        List<string> CityBuildingFilter { get; }
        List<Guid> SellerFilter { get; }
        bool IsContainSubscription { get; }

        void SetTopCityList(List<City> topCities);
        void SetResultMessage(string resultMsg);
    }

    public enum MassMailType
    {
        [Localization("Announcement")]
        Announcement = 0,
        [Localization("Advertisement")]
        Advertisement,
    }

    public enum MassMailFilterType
    {
        None = 0,
        CityArea,
        SellerRange
    }
}
using System;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IParagraphView
    {
        ParagraphPresenter Presenter { get; set; }

        event EventHandler GetDataForCache;
        string ContentName { get; set; }
        string ContentControlId { get; }
        string ContentBody { get; set; }
        string UserName { get; }
        string UniqueName { get; }
        string[] UserRoles { get; }
        bool ExternalCache { get; set; }

        string GetDataFromCache(string key);
        void SetCache(string key, string content);

        void SetData(string contentBody);
    }
}

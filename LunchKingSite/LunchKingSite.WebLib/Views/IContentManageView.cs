﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IContentManageView
    {
        event EventHandler<DataEventArgs<ContentManageSave>> SaveContent;

        ContentManagePresenter Presenter { get; set; }
        int? ContentId { get; }
        string UserName { get; }

        void SetContentList(CmsContentCollection contents);
        void SetContent(CmsContent content, PromoSeoKeyword seo);
        string PreviewContent(int id);
        void ClearCacheDependency(string key);
    }

    public class ContentManageSave
    {
        /// <summary>
        /// 標題
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// UserID
        /// </summary>
        public string UserID { get; set; }
        /// <summary>
        /// SeoDescription
        /// </summary>
        public string SeoDescription { get; set; } 
        //public HttpPostedFile DealPromoImagePostedFile { set; get; }
        /// <summary>
        /// SeoKeyWords
        /// </summary>
        public string SeoKeyWords { get; set; } 
        /// <summary>
        /// 行銷內容
        /// </summary>
        public string PromoContent { set; get; }
    }
}

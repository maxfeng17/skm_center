﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IBuildingImportView
    {     
        event EventHandler<DataEventArgs<UpdatableBuildingInfo>> UpdataBuildingInfo;
    }
}

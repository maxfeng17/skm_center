﻿using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ICreditcardBankSetUpView
    {
        void SetCreditcardBank(CreditcardBankCollection banks);
        void SetCreditcardBankInfo(List<CreditcardBankInfoModel> info);
        FileUpload FUImport { get; }
        string BankId { get; }
        string BankName { get; }
        int InsCreditcardBank { get; }
        string CreditCardBin { get; }
        int CardType { get; }

        void ShowMessage(string msg);
        
        string UserName { get; }

        #region event
        event EventHandler<DataEventArgs<int>> Search;
        event EventHandler Import;
        event EventHandler BankInsert;
        event EventHandler CreditcardInsert;
        #endregion event
    }
}

using System;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    /// <summary>
    /// event args for order mode panel click event
    /// </summary>
    public class OrderModeClickEventArgs : EventArgs
    {
        public OrderModeClickEventArgs(OrderModeType mode)
        {
            _mode = mode;
        }

        private OrderModeType _mode = OrderModeType.Error;
        public OrderModeType Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }
    }

    public class DataEventArgs<TData> : EventArgs
    {
        protected TData _data;
        protected bool _cancel = false;

        public DataEventArgs(TData data)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            _data = data;
        }

        public TData Data
        {
            get { return _data; }
            set { _data = value; }
        }

        public bool Cancel
        {
            get { return _cancel; }
            set { _cancel = value; }
        }

        public override string ToString()
        {
            return _data.ToString();
        }
    }
}

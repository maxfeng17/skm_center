﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealManageExchangeBannerView
    {
        HiDealManageExchangeBannerPresenter Presenter { get; set; }
        string UserName { get; }
        string ContentID { get; set; }
        DateTime? StartTime { get; set; }
        DateTime? EndTime { get; set; }
        string Content { get; set; }

        void NewBanner();
        void InitTimeControl();
        void FillBannerList(IList<ViewCmsRandom> banners);
        event EventHandler<DataEventArgs<int>> BannerEditing;
        event EventHandler<DataEventArgs<int>> BannerDeleting;
        event EventHandler BannerSaving;
        event EventHandler BannerUpdating;
        event EventHandler BannerCancel;
    }
}

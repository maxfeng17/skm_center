﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealUnsubscriptionView
    {
        HiDealUnsubscriptionPresenter presenter { get; set; }
        event EventHandler<DataEventArgs<string[]>> UnSubscribe;
        string theEmail { get; set; }
        void ShowAlert(int value);
    }
}

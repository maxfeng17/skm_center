﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponCouponRefundView
    {
        #region Property

        PponCouponRefundPresenter Presenter { get; set; }
        string UserName { get; }
        Guid DealGuid { get; set; }
        int DealType { get; set; }
        string OrderList { get; set; }
        string ReturnReason { get; set; }
        string Message { set; }
        
        #endregion Property

        #region Event

        event EventHandler<DataEventArgs<bool>> RequestRefund;

        #endregion Event
    }
}

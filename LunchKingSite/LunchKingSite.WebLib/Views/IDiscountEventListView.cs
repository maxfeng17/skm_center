﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IDiscountEventListView
    {
        #region property
        DiscountEventListPresenter Presenter { get; set; }
        string UserName { get; }
        string SearchName { get; }
        string SearchEventCode { get; }
        DateTime SearchEndDateS { get; }
        DateTime SearchEndDateE { get; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        int EventId { get; }
        int Eid { get; }
        string EventName { get; }
        DateTime StartTime { get; }
        DateTime EndTime { get; }
        DiscountEventStatus Status { get; }
        DiscountEventType Type { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> Save;
        event EventHandler<DataEventArgs<int>> DiscountEventGet;
        event EventHandler<DataEventArgs<int>> DiscountEventGetCount;
        event EventHandler<DataEventArgs<int>> DiscountEventPageChanged;
        event EventHandler<DataEventArgs<List<int>>> DiscountUsersImport;
        event EventHandler ImportAllMembers;
        event EventHandler<DataEventArgs<int>> DiscountEventCampaignDelete;
        event EventHandler UpdateCampaignExpireTime;
        #endregion

        #region method
        void SetEventList(DiscountEventCollection eventList);
        void SetEventEdit(DiscountEvent item, ViewDiscountEventCampaignCollection campaignList, int usersCount, int sentQty, int shortageQty);
        #endregion
    }
}

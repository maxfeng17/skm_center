﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEntrustSellDailyReportView
    {
        DateTime? FilterStartTime{ get; set; }
        DateTime? FilterEndTime { get; set; }

        event EventHandler SearchClicked;
        event EventHandler ExportToExcel;

        void Export(List<ViewEntrustSellDailyPayment> list);
        void SetContent(List<ViewEntrustSellDailyPayment> list);
    }
}

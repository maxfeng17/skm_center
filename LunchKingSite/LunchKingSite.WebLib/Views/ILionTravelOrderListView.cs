﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ILionTravelOrderListView
    {
        #region property
        LionTravelOrderListPresenter Presenter { get; set; }
        int PageCount { get; set; }
        int FilterDeliveryType { get; }
        int FilterType { get; }
        string FilterData { get; }
        DateTime? StartDate { get; }
        DateTime? EndDate { get; }
        AgentChannel OrderClassType { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler Search;
        #endregion

        #region method
        void GetCouponList(ViewLiontravelCouponListMainCollection data, Dictionary<Guid, int> is_partial_list);
        void SetUpPageCount();
        #endregion
    }
}

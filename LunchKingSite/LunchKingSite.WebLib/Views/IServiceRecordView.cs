﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IServiceRecordView
    {
        #region property
        ServiceRecordPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        int PageSize { get; set; }
        int RecordPeriod { get; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion
        #region method
        void SetServiceRecord(Dictionary<KeyValuePair<ServiceMessage, Guid>, List<ServiceLog>> data);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IVerificationBanks
    {
        string UserName { get; }
        TrustBankType TrustType { get; }
        TrustLogType LogType { get; }
        TrustProvider Provider { get; set; }
        VerificationBanksPresenter Presenter { get; set; }
        event EventHandler<DataEventArgs<string>> SearchLog;
        void SetAmountSum(int amountsum, int totalcount, DateTime basedate);
        void SetAmountSum(int sumamount, int orderAmount, int verifyAmount, int refundAmount);
        void SetCouponTrustLog(CashTrustLogCollection cashlogs, TrustVerificationReport report);
        void SetUserTrustLog(UserCashTrustLogCollection userlogs, TrustVerificationReport report);
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealDealView
    {
        HiDealDealPresenter Presenter { get; set; }

        string did { get; }

        string pid { get; }

        Guid ProductGuid { get; }

        bool IsPreviewMode { get; }

        string FacebookAppId { get; }

        string FacebookOgTitle { get; set; }

        string FacebookOgImage { get; set; }

        HiDealRegionCollection Regions { get; set; }

        void SetDealContent(HiDealDeal deal, HiDealSpecialDiscountType specialType);

        void SetProductContent(HiDealDeal deal, HiDealProductCollection products);

        void SetTap(HiDealContentCollection contents, HiDealDeal deal);

        void SetStoresInfo(ViewHiDealStoresInfoCollection stores);

        void RedirectToDefault();
    }
}
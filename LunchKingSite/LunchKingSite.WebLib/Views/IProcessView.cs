﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IProcessView
    {
        #region property
        ProcessPresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }

        string SearchServiceNo { get; }
        Guid SearchOrderGuid { get; }
        string Mail { get; }
        string SDate { get; }
        string EDate { get; }
        int SearchUserId { get; }
        int SearchPriority { get; }
        int SearchCategory { get;}
        int SearchType { get;}
        bool IsCustomerCare { get; }
        bool IsCustomerCare2 { get; }
        int UserId { get; }

        #endregion
        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> ProcessCount;
        event EventHandler<DataEventArgs<int>> ProcessPageChanged;

        #endregion

        #region method
        void ProcessList(ViewCustomerServiceListCollection MessageList);
        #endregion

    }

}

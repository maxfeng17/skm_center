﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IMagazineInfoView
    {
        #region property
        MagazineInfoPresenter Presenter { get; set; }
        string SellerName { get; }
        string BusinessOrderId { get; }
        DateTime PreferentialDateTime { get; }
        DateTime CreateTimeStart { get; }
        DateTime CreateTimeEnd { get; }
        int PageSize { get; set; }
        DateTime ModifyTimeStart { get; }
        DateTime ModifyTimeEnd { get; }
        IEnumerable<string> DeptIdList { get; }
        ExportType Type { get; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnPezStoreSearchClicked;
        event EventHandler OnExportToExcelClicked;
        event EventHandler OnPezStoreExportToExcelClicked;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        void SetDepartmentData(DepartmentCollection deptList);
        void GetBusinessList(Dictionary<Preferential, BusinessOrder> dataList);
        void SetGridViewListData(Dictionary<Preferential, BusinessOrder> dataList);
        void ExportPezStoreToExcel(IEnumerable<BusinessOrder> dataList);
        void GetBusinessPezStoreList(IEnumerable<BusinessOrder> dataList);
        #endregion
    }

    #region enum
    public enum ExportType
    {
        Magazine,
        PezStore
    }
    #endregion
}

using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.WebLib.Views
{
    public interface ITodayDealPponView
    {
        event EventHandler CheckEventMail;

        TodayDealPponPresenter Presenter { get; set; }
        int CityId { get; set; }
        int DealCityId { get; set; }
        int TravelCategoryId { get; }
        int FemaleCategoryId { get; }
        int SubRegionCategoryId { get; }
        List<int> FilterCategoryIdList { get; }
        List<CategoryNode> DealChannelList { get; set; }
        Guid BusinessHourId { get; set; }
        int BusinessHourUniqueId { get; set; }
        CategorySortType SortType { get; set; }
        int Encores { get; set; }
        string UserName { get; }
        int UserId { get; }
        DeliveryType[] RepresentedDeliverytype { get; } //20120314 前端以憑證宅配撈取
        bool IsDeliveryDeal { set; get; }

        bool TwentyFourHours { get; set; }
        string HamiTicketId { get; }
        int? DealType { get; set; }
        //bool EntrustSell { set; }
        string PicAlt { get; set; }
        bool isUseNewMember { get; set; }
        int CategoryID { get; }
        bool isMultipleMainDeals { get; set; }
        bool BypassSsoVerify { get; set; }
        //IExpireNotice ExpireNotice { get; }
        bool ShowEventEmail { get; set; }
        string EdmPopUpCacheName { get; set; }
        //string CityName { set; }

        /// <summary>
        /// 埋Code
        /// </summary>
        string DealArgs { set; get; }

        bool IsOpenNewWindow { get; set; }
        bool IsMobileBroswer { get; }
        bool SetCookie(string value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message);
        void NoRobots();    //2010.03.02 增加福利網不被搜尋引擎index meta .Died 
        void SeoSetting(ViewPponDeal d,string appendTitle = ""); //2011.01.04 SEO .Died
        

        //新首頁
        //void SetMultipleCategories(List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>> dealCategoryCountList, List<CategoryDealCount> filterItemList, SystemCodeCollection piinlifeLinks);
        void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        void SetMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        //跳窗
        void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair);
        void RedirectToPiinlife();
        void RedirectToDefault();
        void SetMemberCollection(List<Guid> memberCollectDealGetOnlineDealGuids);
    }
}

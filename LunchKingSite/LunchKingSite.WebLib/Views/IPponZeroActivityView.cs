﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponZeroActivityView
    {
        #region event

        event EventHandler<EventArgs> CheckOut;

        #endregion

        #region properties

        PponZeroActivityPresenter Presenter { get; }
        Guid BusinessHourGuid { get; }
        string SessionId { get; }
        string TransactionId { get; }
        string UserName { get; }
        int UserId { get; }
        string RsrcSession { get; }
        string TicketId { get; }
        IViewPponDeal TheDeal { get; set; }
        PaymentResultPageType PaymentResult { get; set; }
        string ReferrerSourceId { get; }
        bool IsPreview { get; }
        string IsHami { get; }
        OrderFromType FromType { get; }
        Guid StoreGuid { get; }
        List<CategoryNode> DealChannelList { get; set; }

        #endregion

        #region page event

        /// <summary>
        /// 設定零元好康結果內容
        /// </summary>
        /// <param name="coupon">coupon資訊</param>
        void SetDealCoupon(Coupon coupon, GroupOrderStatus groupOrderStatus, bool isEnableFami3Barcode);
        void SetSkmDealCoupon();
        void SetDealCoupon(Coupon coupon, ViewPponOrder vpo);
        void ShowResult(PaymentResultPageType result);
        void ShowSkmResult(PaymentResultPageType result);
        void SetContent(int alreadyBoughtCount, bool isDailyRestriction, ViewPponStoreCollection stores, bool isMultipleBranch);
        void AlertMessage(string msg);
        void ReEnterPaymentGate(PaymentType paymentType, string transId);
        /// <summary>
        /// 設定全家零元好康 barcode 內容
        /// </summary>
        void SetFamiDealBarcodeContent(IViewPponDeal vpd);

        #endregion

        #region cpa

        CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa { get; set; }

        #endregion

        #region redirect

        /// <summary>
        /// 顯示訊息，然後回好康Default頁面，若message 為空，則直接回好康Default頁
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="message"></param>
        void GoToPponDefaultPage(Guid bid, string message);
        void GoToLoginPage();
        void GoToClaim(string ticketId);
        void GoToGet(string ticketId, string transactionId);
        void GoToPay(Guid bid);
        void RedirectToAppLimitedEdtionPage();

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IGiftSearchView
    {
        #region property
        GiftSearchPresenter Presenter { get; set; }

        int PageSize { get; set; }
        int CurrentPage { get; }
        int UserId { get; }
        string ProductName { get; }
        string TbOS { get; }
        string TbOE { get; }
        string SequenceNumber { get; }
        string Mobile { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<string>> ViewMgmGiftListGet;
        event EventHandler<DataEventArgs<int>> ViewMgmGiftListGetCount;
        event EventHandler<DataEventArgs<int>> ViewMgmGiftListPageChanged;
        #endregion

        #region method
        void SetMgmGiftList(ViewMgmGiftBacklistCollection viewMgmGiftBacklist);

        void SetViewMgmGiftBacklist(ViewMgmGiftCollection viewMgmGiftList,string giftId,string account);
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IReturnDiscountView
    {
        ReturnDiscountPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        Guid OrderGuid { get; }
        void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus);
        void SetRefundFormInfoForMultiInvoices(List<EinvoiceMain> einvoices, CashTrustLogCollection cs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus);
        void SetEntrustSellRefundFormInfo(List<EntrustSellReceipt> receipts, Order o, CashTrustLogCollection cs, DealProperty dp, bool isleagl, string message, int returnFormStatus);
        string message { set; }
        bool isleagl { set; }
    }
    public class RefundDiscountListClass
    {
        public int Id { get; set; }
        public string DealNumber { get; set; }
        public string OrderId { get; set; }
        public string CouponSequence { get; set; }
        public int EinvoiceMode { get; set; }
        public DateTime EinvoiceDate { get; set; }                  //若EInvoice_Main 的InvoiceNumberTime 是Null, 則取OrderTime
        public bool IsInvoiceNumberDateNull { get; set; }           //若EInvoice_Main 的InvoiceNumberTime 是Null, 就是true
        public string EinvoiceNumberDate_Year { get; set; }
        public string EinvoiceNumberDate_Month { get; set; }
        public string EinvoiceNumberDate_Day { get; set; }
        public string EinvoiceNumber { get; set; }
        public string EinvoiceItemName { get; set; }
        public int ItemAmount { get; set; }
        public int ItemNoTaxAmount { get; set; }
        public int ItemTax { get; set; }
        public bool IsTax { get; set; }
        public bool IsProduct { get; set; }
        public string breakstring { get; set; }
        public RefundDiscountListClass(int id,string dealnumber, string orderid, string couponsequence,
            int einvoicemode, DateTime einvoicedate, bool isInvoiceNumberDateNull, string einvoicenumber, string einvoiceitemname,
            int itemamount, int itemnotaxamount, int itemtax, bool istax, bool isproduct)
        {
            Id = id;
            DealNumber = dealnumber;
            OrderId = orderid;
            CouponSequence = couponsequence;
            EinvoiceMode = einvoicemode;
            EinvoiceDate = einvoicedate;
            EinvoiceNumberDate_Year = IsInvoiceNumberDateNull ? string.Empty : einvoicedate.Year.ToString();
            EinvoiceNumberDate_Month = IsInvoiceNumberDateNull ? string.Empty : einvoicedate.Month.ToString();
            EinvoiceNumberDate_Day = IsInvoiceNumberDateNull ? string.Empty : einvoicedate.Day.ToString();
            EinvoiceNumber = einvoicenumber;
            EinvoiceItemName = einvoiceitemname;
            ItemAmount = itemamount;
            ItemNoTaxAmount = itemnotaxamount;
            ItemTax = itemtax;
            IsTax = istax;
            IsProduct = isproduct;
        }
    }
}

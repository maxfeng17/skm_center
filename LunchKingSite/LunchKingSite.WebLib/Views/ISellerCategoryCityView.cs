﻿using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;


namespace LunchKingSite.WebLib.Views
{
    public interface ISellerCategoryCityView
    {
        SellerCategoryCityPresenter Presenter { get; }
        int CategoryId { get; }
        void SetSellers(List<CitySellers> data);
    }

    public sealed class CitySellers
    {
        public CitySellers(string cityName)
        {
            _cityName = cityName;
        }

        private string _cityName;
        public string CityName
        {
            get { return _cityName; }
            set { _cityName = value; }
        }

        private ViewSellerCategoryCityCollection _sellers;
        public ViewSellerCategoryCityCollection Sellers
        {
            get { return _sellers; }
            set { _sellers = value; }
        }
    }
}

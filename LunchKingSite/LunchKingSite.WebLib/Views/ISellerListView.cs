﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface ISellerListView : ILocalizedView
    {
        event EventHandler<DataEventArgs<int>> GetDataCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SortClicked;
        event EventHandler SearchClicked;

        SellerListPresenter Presenter { get; }
        string SortExpression { get; set; }
        string Filter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        string FilterType { get; set; }

        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void SetStatusFilter(Dictionary<bool, string> status);
        void SetSellerList(SellerCollection SellerLists);
    }
}

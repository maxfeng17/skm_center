﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealCouponDetailView
    {
        HidealCouponDetailPresenter Presenter { get; set; }
        string UserName { get; }
        int? CouponId { get; }
        string ActionMode { get; }
        int? DealId { get; }
        int? ProductId { get; }
        string GoogleApiKey { set; }
        string EncryptedCouponCode { set; }
        bool ForceStaticMap { get; }

        void SetDetail(ViewHiDealCoupon coupon, HiDealContent content, List<ViewHiDealStoresInfo> storeList);
    }
}

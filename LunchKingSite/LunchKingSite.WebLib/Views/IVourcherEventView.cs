﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IVourcherEventView
    {
        #region property
        VourcherEventPresenter Presenter { get; set; }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        string UserName { get; }
        /// <summary>
        /// 總資料數量
        /// </summary>
        int PageCount { get; set; }
        int PageReturnCaseCount { get; set; }
        /// <summary>
        /// 分頁的每頁大小
        /// </summary>
        int PageSize { get; }
        int PageReturnCaseSize { get; }
        /// <summary>
        /// 搜尋優惠券的欄位和輸入資料
        /// </summary>
        KeyValuePair<string, string> SearchKeys { get; }
        /// <summary>
        /// 優惠券活動Id
        /// </summary>
        int VourcherEventId { get; set; }
        /// <summary>
        /// 賣家Guid
        /// </summary>
        Guid SellerGuid { get; set; }
        /// <summary>
        /// 賣家Id
        /// </summary>
        string SellerId { get; set; }
        /// <summary>
        /// 新增或查詢模式
        /// </summary>
        string Mode { get; }
        /// <summary>
        /// 優惠券類型
        /// </summary>
        VourcherEventType EventType { get; set; }
        /// <summary>
        /// 優惠券使用數量模式
        /// </summary>
        VourcherEventMode EventMode { get; set; }
        /// <summary>
        /// 優惠券狀態
        /// </summary>
        VourcherEventStatus EventStatus { get; }
        /// <summary>
        /// 業務清單
        /// </summary>
        string SalesEmailArray { get; set; }
        /// <summary>
        /// 使否為編審腳色
        /// </summary>
        bool IsAdmin { get; }
        /// <summary>
        /// 生活商圈
        /// </summary>
        Dictionary<int, int[]> SelectedCommercialCategoryId { get; set; }
        List<int> SelectedCategory { get; set; }
        #endregion
        #region event
        /// <summary>
        /// 撈取分頁
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler<DataEventArgs<int>> PageReturnCaseChanged;
        /// <summary>
        /// 計算總資料數(分頁用)
        /// </summary>
        event EventHandler GetDataCount;
        event EventHandler GetReturnCaseDataCount;
        /// <summary>
        /// 依賣家Id搜尋賣家資料
        /// </summary>
        event EventHandler<DataEventArgs<string>> GetSellerById;
        /// <summary>
        /// 傳入優惠券內容和分店儲存
        /// </summary>
        event EventHandler<DataEventArgs<VourcherEventInfo>> SaveVourcherEventStores;
        /// <summary>
        /// 撈取優惠券
        /// </summary>
        event EventHandler<DataEventArgs<int>> GetVourcherEvent;
        /// <summary>
        /// 搜尋優惠券內容
        /// </summary>
        event EventHandler SearchVourcherEvent;
        /// <summary>
        /// 依優惠券狀態搜尋
        /// </summary>
        event EventHandler SearchVourcherByStatus;
        /// <summary>
        /// 退回申請，存入備註訊息
        /// </summary>
        event EventHandler<DataEventArgs<string>> ReturnApply;
        /// <summary>
        /// 賣家審核通過
        /// </summary>
        event EventHandler SellerPass;
        /// <summary>
        /// 分店審核通過
        /// </summary>
        event EventHandler StorePass;
        /// <summary>
        /// 通過申請
        /// </summary>
        event EventHandler ApproveApply;
        /// <summary>
        /// 隱藏或顯示活動
        /// </summary>
        event EventHandler<DataEventArgs<bool>> EnableEvent;
        #endregion
        #region method
        /// <summary>
        /// 回傳賣家和其分店及賣家的修改資料
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="stores">分店資料</param>
        void SetSellerStores(Seller seller, StoreCollection stores);
        /// <summary>
        /// 回傳優惠券資料和其對應分店資料
        /// </summary>
        /// <param name="vourcher_event">優惠券資料</param>
        /// <param name="vourcher_stores">對應分店資料</param>
        void SetVourcherEventStores(Seller seller, StoreCollection stores, VourcherEvent vourcher_event, VourcherStoreCollection vourcher_stores);
        /// <summary>
        /// 回傳搜尋的優惠券內容
        /// </summary>
        /// <param name="vourcher_events">優惠券列表</param>
        void SetVourcherEventSellerCollection(ViewVourcherSellerCollection vourcher_events);
        /// <summary>
        /// 回傳退件的優惠券
        /// </summary>
        /// <param name="vourcher_events">退件的優惠券</param>
        void SetReturnCaseVourcherEvent(ViewVourcherSellerCollection vourcher_events);
        /// <summary>
        /// 設定生活商圈資料
        /// </summary>
        /// <param name="categories"></param>
        void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories, Dictionary<int, string> category_list);
        #endregion
    }
    /// <summary>
    /// 新增或更新優惠券內容
    /// </summary>
    public class VourcherEventInfo
    {
        public VourcherEvent Vourcher_Event { get; set; }
        public VourcherStoreCollection Vourcher_Store_Collection { get; set; }
        public List<PhotoInfo> Photo_Infos { get; set; }
        //使否繼續新增
        public bool AddNext { get; set; }
        //使否有刪除的圖片
        public List<int> DeleteImgs { get; set; }
        public VourcherEventInfo(VourcherEvent vourcher_event, VourcherStoreCollection vourcher_stores, List<PhotoInfo> photo_infos, List<int> delete_imgs, bool addnext)
        {
            Vourcher_Event = vourcher_event;
            Vourcher_Store_Collection = vourcher_stores;
            Photo_Infos = photo_infos;
            DeleteImgs = delete_imgs;
            AddNext = addnext;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IDiscountEventCampaignSetView
    {
        #region property
        DiscountEventCampaignSetPresenter Presenter { get; set; }
        string UserName { get; }
        int EventId { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<Dictionary<int, int>>> Save;
        #endregion

        #region method
        void SetDiscountCampaighList(ViewDiscountEventCampaignCollection campaignList);
        void ShowMessage(string msg);
        #endregion
    }
}

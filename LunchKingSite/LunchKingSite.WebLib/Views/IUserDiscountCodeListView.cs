﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserDiscountCodeListView
    {
        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        #endregion
        #region property
        UserDiscountCodeListPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        int PageCount { get; set; }
        int PageSize { get; }
        int CurrentPage { get; set; }
        #endregion
        #region method
        void ViewDiscountDetailGetList(ViewDiscountDetailCollection discountdetails);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IYahooBigSetupView
    {
        #region property
        YahooBigSetupPresenter Presenter { get; set; }
        string UserId { get; }
        YahooPropertyType SearchType { get; }
        string DealId { get; }
        string DealName { get; }
        DateTime DealStartDate { get; }
        DateTime DealEndDate { get; }
        int PageSize { get; set; }
        int CurrentPage { get; set; }
        YahooPropertyAction Action { get; set; }
        bool IsMarkDelete { get; set; }
        bool IsPrintDiscountMark { get; }
        bool IsPrintLogoWaterMark { get; }
        bool IsCutCenter { get; }
        FileUpload ImgUploadFile { get; }
        bool IsShowYahooPropertyEditPanel { set; }
        string SearchMode { get; set; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnSortClicked;
        event EventHandler<DataEventArgs<int>> GetPponYahooPropertyBySort;
        event EventHandler<DataEventArgs<int>> CheckPponYahooPropertyAction;
        event EventHandler<DataEventArgs<List<int>>> CheckSelectedPponYahooPropertyAction;
        
        event EventHandler<DataEventArgs<Dictionary<int, int?[]>>> OnSortSaveClicked;
        event EventHandler<DataEventArgs<Guid>> GetPponYahooProperty;
        event EventHandler<DataEventArgs<int>> GetPiinlifeYahooProperty;
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> SaveYahooProperty;
        event EventHandler<DataEventArgs<int>> OnPponGetCount;
        event EventHandler<DataEventArgs<int>> PponPageChanged;
        event EventHandler<DataEventArgs<List<string>>> ImportBidList;
        #endregion

        #region method
        void SetPponDealList(Dictionary<ViewPponDeal, KeyValuePair<YahooProperty, string[]>> dataList);
        void SetPiinlifeDealList(Dictionary<HiDealProduct, KeyValuePair<YahooProperty, string[]>> dataList);
        void SetSortData(IEnumerable<YahooProperty> ypsN, IEnumerable<YahooProperty> ypsC, IEnumerable<YahooProperty> ypsS);
        void SetYahooProperty(YahooProperty yp);
        void ShowMessage(string msg);
        YahooProperty GetYahooPropertyData(YahooProperty yp);
        void AlertImportErrorMsg(string errorMsg);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesStoreContentView
    {
        #region property
        SalesStoreContentPresenter Presenter { get; set; }
        string UserName { get; }
        Guid SellerGuid { get; }
        Guid StoreGuid { get; }
        #endregion

        #region event
        event EventHandler<EventArgs> Save;
        event EventHandler<DataEventArgs<SellerTempStatus>> StoreApprove;
        event EventHandler<DataEventArgs<string>> ChangeLog;
        #endregion

        #region method
        Store GetStoreData(Store s);
        void RedirectStore(Guid stid);
        void SetStoreContent(Seller seller, Store Store, ViewEmployee emp, StoreChangeLogCollection changeLogs, StoreCollection sts);
        void ShowMessage(string msg, StoreContentMode mode, string parms = "");
        #endregion
    }

    public enum StoreContentMode
    {
        GenericError = -1,
        PrivilegeError = 0,
        StoreNotFound = 1,
    }
}

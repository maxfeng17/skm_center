﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesRestrictionsGeneratorView
    {
        #region property
        SalesRestrictionsGeneratorPresenter Presenter { get; set; }
        string UserName { get; }
        Guid Bid { get; }
        int Pid { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<string>> Save;
        event EventHandler<EventArgs> ProvisionSelectedIndexChanged;
        #endregion

        #region method
        void SetRestrictions(IEnumerable<ProvisionDepartment> departments, DealAccounting da, BusinessHour bh, Proposal pro, string restrictions);
        void SetRestrictionsModel(IEnumerable<ProvisionDepartmentModel> departments, DealAccounting da, BusinessHour bh, Proposal pro, string restrictions);
        void ShowMessage(string msg, BusinessContentMode mode, string parms = "");
        void SetProvisionDropDownListData(IEnumerable<ProvisionDepartment> department);
        void SetProvisionDropDownListModelData(IEnumerable<ProvisionDepartmentModel> department);
        #endregion
    }
}

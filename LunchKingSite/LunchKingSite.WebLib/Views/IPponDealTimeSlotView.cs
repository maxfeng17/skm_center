﻿using System;
using System.Linq;
using LunchKingSite.DataOrm;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponDealTimeSlotView
    {
        void ShowPponDealTimeSlotData(PponDealCalendarPage page);
        void ShowHotDealSetting(HotDealConfig hotDealConf);
        void ShowPponDealTimeSlotWeights(List<DealTimeSlotSortElement> weights, List<DealTimeSlotSortElementRank> scoreRank);
        event EventHandler<DataEventArgs<PponDealTimeSlotViewSearchRequest>> SelectCityIdChange;
    }

    public sealed class PponDealCalendarPage
    {
        private int _slotDateLeagth = 7;
        public int SlotDateLeagth
        {
            get
            {
                return _slotDateLeagth;
            }
        }

        private PponDealCalendarColumn[] _pponColumns;
        public PponDealCalendarColumn[] PponColumns
        {
            get
            {
                return _pponColumns;
            }
        }

        public PponDealCalendarRow AllCountryData { get; set; }
        public PponDealCalendarRow TaipeiData { get; set; }
        public PponDealCalendarRow NewTaipeiCityData { get; set; }
        public PponDealCalendarRow TaoyuanData { get; set; }
        public PponDealCalendarRow HsinchuData { get; set; }
        public PponDealCalendarRow TaichungData { get; set; }
        public PponDealCalendarRow TainanData { get; set; }
        public PponDealCalendarRow KaohsiungData { get; set; }
        public PponDealCalendarRow TravelData { get; set; }
        public PponDealCalendarRow PeautyData { get; set; }
        public PponDealCalendarRow ComData { get; set; }
        public PponDealCalendarRow DepositCoffeeData { get; set; }
        public PponDealCalendarRow FamilyData { get; set; }
        public PponDealCalendarRow Tmall { get; set; }
        public PponDealCalendarRow Piinlife { get; set; }
        public PponDealCalendarRow SkmData { get; set; }
        public IList<IList<Guid>> NewDealData { get; set; }

        public PponDealCalendarPage(int slotDateLeagth)
        {
            _slotDateLeagth = slotDateLeagth;

            AllCountryData = new PponDealCalendarRow(_slotDateLeagth);
            TaipeiData = new PponDealCalendarRow(_slotDateLeagth);
            NewTaipeiCityData = new PponDealCalendarRow(_slotDateLeagth);
            TaoyuanData = new PponDealCalendarRow(_slotDateLeagth);
            HsinchuData = new PponDealCalendarRow(_slotDateLeagth);
            TaichungData = new PponDealCalendarRow(_slotDateLeagth);
            TainanData = new PponDealCalendarRow(_slotDateLeagth);
            KaohsiungData = new PponDealCalendarRow(_slotDateLeagth);
            TravelData = new PponDealCalendarRow(_slotDateLeagth);
            PeautyData = new PponDealCalendarRow(_slotDateLeagth);
            ComData = new PponDealCalendarRow(_slotDateLeagth);
            DepositCoffeeData = new PponDealCalendarRow(_slotDateLeagth);
            FamilyData = new PponDealCalendarRow(_slotDateLeagth);
            Tmall = new PponDealCalendarRow(_slotDateLeagth);
            Piinlife = new PponDealCalendarRow(_slotDateLeagth);
            SkmData = new PponDealCalendarRow(_slotDateLeagth);

            NewDealData = new List<IList<Guid>>(_slotDateLeagth);
            
            _pponColumns = new PponDealCalendarColumn[_slotDateLeagth];
            for (int i = 0; i < _slotDateLeagth; i++)
            {
                _pponColumns[i] = new PponDealCalendarColumn() { ColumnIndex = i };
                NewDealData.Add(new List<Guid>());
            }
        }
    }

    public sealed class PponDealCalendarColumn
    {
        public string TitleName;
        public int ColumnIndex;

        public PponDealCalendarColumn()
        {
            TitleName = "";
            ColumnIndex = -1;
        }

    }

    /// <summary>
    /// PponDealTimeSlot於 上檔排程表顯示的資料
    /// </summary>
    public sealed class PponDealCalendarRow
    {
        private string _cityName = string.Empty;
        public string CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                _cityName = value;
            }
        }

        private ViewPponDealTimeSlotCollection[] _timeSlots;// = new ViewPponDealTimeSlotCollection[5];

        public ViewPponDealTimeSlotCollection[] TimeSlots
        {
            get
            {
                return _timeSlots;
            }
        }

        private int _colLeagth;

        public PponDealCalendarRow(int Leagth)
        {
            _colLeagth = Leagth;
            _timeSlots = new ViewPponDealTimeSlotCollection[_colLeagth];

            for (int i = 0; i < _colLeagth; i++)
            {
                _timeSlots[i] = new ViewPponDealTimeSlotCollection();
            }
        }

        public int GetDealTimeSlotTotalDataCount()
        {
            return _timeSlots.Sum(x => x.Count);
        }
    }

    public sealed class PponDealTimeSlotViewSearchRequest
    {
        public bool IsAllCity;
        public int? CityId;
    }

}

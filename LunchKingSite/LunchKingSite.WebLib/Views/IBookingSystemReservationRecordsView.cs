﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IBookingSystemReservationRecordsView
    {
        #region property

        string UserName { get; }
        bool IsExpiredRecord { set; get; }
        string ApiKey { get; }
        string MemberKey { set; get; }

        #endregion

        #region method

        void RedirectToLogin();
        void BindReservationRecord(ViewBookingSystemReservationRecordCollection ReservationRecords);
        
        #endregion

        #region event

        event EventHandler<DataEventArgs<int>> IsExpiredReservationChange;
        event EventHandler<DataEventArgs<int>> CancelReservation;
        
        #endregion
    }
}

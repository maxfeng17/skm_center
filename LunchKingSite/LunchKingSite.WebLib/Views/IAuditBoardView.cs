using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IAuditBoardView
    {
        AuditBoardPresenter Presenter { get; set; }
        string ReferenceId { get; }
        AuditType ReferenceType { get; }
        bool ShowAllRecords { get; set; }
        bool EnableAdd { get; set; }
        bool HideView { get; set; }

        void SetRecordGrid(AuditCollection data);

        event EventHandler<DataEventArgs<AuditEntry>> AddClick;
        event EventHandler<DataEventArgs<KeyValuePair<int, bool>>> EditClick;
    }

    public class AuditEntry
    {
        private string _msg;
        public string Message
        {
            get { return _msg; }
            set { _msg = value; }
        }

        private string _creator;
        public string CreateId
        {
            get { return _creator; }
            set { _creator = value; }
        }

        private bool _visible;
        public bool VisibleInViewMode
        {
            get { return _visible; }
            set { _visible = value; }
        }

        public AuditEntry(string creator, string message, bool visibleInView)
        {
            this._creator = creator;
            this._msg = message;
            this._visible = visibleInView;
        }
    }
}

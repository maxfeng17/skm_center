﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IDiscountUserExchangeView
    {
        #region property
        DiscountUserExchangePresenter Presenter { get; set; }
        string UserName { get; }
        string EventCode { get; }
        string Code { get; }
        #endregion

        #region event
        event EventHandler Send;
        #endregion

        #region method
        void ShowMessage(string msg);
        void RedirectDiscountList();
        #endregion
    }
}

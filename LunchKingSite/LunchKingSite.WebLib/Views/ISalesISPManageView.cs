﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesISPManageView
    {
        SalesISPManagePresenter Presenter { get; set; }

        string SellerName { get; }
        string SellerId { get; }
        string ISPStatus { get; }
        string ServiceChannel { get;}
        string StartDate { get; }
        string EndDate { get; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        FileUpload FUExport { get; }
        string UserName { get; }

        event EventHandler<DataEventArgs<int>> Search;
        event EventHandler Export;
        event EventHandler Import;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;

        void SetISPList(ViewVbsInstorePickupCollection data);
        void ShowMessage(string msg);
    }
}

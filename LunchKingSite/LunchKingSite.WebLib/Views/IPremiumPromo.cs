﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPremiumPromo
    {
        #region property

        string Url { get; }
        string Preview { get; }
        int UserId { get; }
        string UserName { get; }
        string BgCss { get; set; }
        string MainBanner { set; }
        string BannerFontColor { get; set; }
        string ButtonBackgroundColor { get; set; }
        string ButtonFontColor { get; set; }
        string ButtonBackgroundHoverColor { get; set; }

        int PremiumPromoId { get; set; }
        string Rsrc { get; set; }
        string PremiumTitle { get; set; }
        int AppliedPremiumAmount { get; set; }
        string ApplySuccessContent { set; }

        #endregion property

        #region method

        void ShowPremiumExpire();
        void ShowLogin();
        void ShowEventFinish();
        void EventPremiumPromoSetting(EventPremiumPromo premiumpromo);
        void AwardUserInfoSetting(ViewMemberBuildingCityParentCity vmbcpc);
        void ShowOverUserAmount();
        void ShowApplyUserInfo();
        void ShowSuccess();
        void ShowError();

        #endregion method

        event EventHandler<DataEventArgs<EventPremiumPromoItem>> SendAwardUserInfo;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserCollectView
    {
        #region property

        UserCollectPresenter Presenter { get; set; }
        bool IsAuthenticated { get; }
        string UserName { get; }
        int CollectCount { get; set; }
        int PageSize { get; }
        int UserId { get; set; }
        int OutOfDateDealCount { set; get; }
        int CurrentPage { get; }
        string CollectTimeOrderByStatus { set; get; }
        string BusinessHourOrderTimeEndOrderByStatus { set; get; }
        string FirstOrderByColumn { set; get; }
        string SecondOrderByColumn { set; get; }
        bool MemberCollectDealExpireMessage { set; get; }
        string CollectBidList { set; get; }

        #endregion

        #region event

        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler CancelOutOfDateCollectDeal;
        event EventHandler<DataEventArgs<Guid>> RemoveCollectDeal;

        #endregion

        #region method

        void RedirectToLogin();
        void SetDealCollectList(ViewMemberCollectDealCollection vmcdc);
        void ReloadPagerTotalCount();

        #endregion
    }
}

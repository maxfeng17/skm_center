﻿using System;
using System.Collections.Generic;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IAppLimitedEdition
    {
        AppLimitedEditionPresenter Presenter { get; set; }
        Guid BusinessHourGuid { get; }
        bool IsAppLimitedEditionDeal { set; get; }

        #region Method

        void RenderViewContent(IViewPponDeal vpd);
        void RedirectToDefault(string siteUrl, Guid bid);

        #endregion
    }
}

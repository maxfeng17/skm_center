﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace LunchKingSite.WebLib.Views
{
    public interface IEvaluateStar
    {
        event EventHandler SaveFeedback;
        event EventHandler SaveOpenEvaluteMail;

        Guid tId { get; }

        string flag { get; }

        int DataType { get; }        

        int fromType { get; }

        string QTable { get; set; }

        string ItemName { get; set; }

        Guid orderGuid { get; }

        XElement QuestionResult { get; }

        void SayMessage(string msg);
    }
}

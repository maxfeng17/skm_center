﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponAccountingView
    {
        event EventHandler QueryCost;
        event EventHandler<DataEventArgs<UpdatableAccountingInfo>> UpdateAccounting;
        event EventHandler<DataEventArgs<DealCost>> CreateCost;
        event EventHandler DeleteCost;

        Guid BusinessHourGuid { get; }

        PponAccountingPresenter Presenter { get; set; }

        void SetData(DealAccounting dealAccounting, DealCostCollection dealCost);
        void SetGrid(DealCostCollection dealCost);
    }

    public sealed class UpdatableAccountingInfo
    {
        #region props
        private string _si;
        public string SalesId
        {
            get { return _si; }
            set { _si = value; }
        }
        private string _cm;
        public string Commission
        {
            get { return _cm; }
            set { _cm = value; }
        }
        private string _bn;
        public string Bonus
        {
            get { return _bn; }
            set { _bn = value; }
        }
        private string _cd;
        public string ChargingDate
        {
            get { return _cd; }
            set { _cd = value; }
        }
        private string _ed;
        public string EnterDate
        {
            get { return _ed; }
            set { _ed = value; }
        }
        private string _id;
        public string InvoiceDate
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _af;
        public string APFormula
        {
            get { return _af; }
            set { _af = value; }
        }
        private string _cs;
        public string Cost
        {
            get { return _cs; }
            set { _cs = value; }
        }
        #endregion
    }
}

﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IAccessoryGroupView
    {
        event EventHandler<DataEventArgs<int>> GetBuildingCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SearchClicked;
        event EventHandler<DataEventArgs<AccessoryGroup>> SetAccessoryGroup;

        AccessoryGroupresenter Presenter { get; set;}

        Guid SellerGuid { get; }
        string Filter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }

        void SetAccessoryGroupList(AccessoryGroupCollection AccessoryGroupList);
    }
}

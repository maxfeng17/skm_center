﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealProductInventoryView
    {
        #region property
        HiDealProductInventoryPresenter Presenter { get; set; }
        string Dealid { get; }
        #endregion

        #region event
        /// <summary>
        /// 觸發主檔執行下載excel
        /// </summary>
        event EventHandler<DataEventArgs<int>> GetDealReportDetail;
        /// <summary>
        /// 觸發商品檔執行下載excel
        /// </summary>
        event EventHandler<DataEventArgs<int>> GetProductReportDetail;
        
        
        #endregion

        #region method
        void showBusinessData(ViewHiDealSellerProductCollection vhidealseller);
        void showProductData(ViewHiDealOrderReturnQuantityCollection hdpc);
        void ExportProductData(ViewHiDealProductShopInfoCollection productShopInfo, ViewHiDealProductHouseInfoCollection productHuoseInfo,ViewHiDealOrderReturnQuantityCollection dealInfo);
        void ExportDealData(List<HiDealProductInventoryViewDealProductInfo> datas);
        #endregion
    }

    public class HiDealProductInventoryViewDealProductInfo
    {
        public ViewHiDealProductHouseInfoCollection ProductHouseInfos{get;set;}
        public ViewHiDealProductShopInfoCollection ProductShopInfos { get; set; }
        public HiDealDeal Deal { get; set; }
        public HiDealProduct Product { get; set; }
    }
}

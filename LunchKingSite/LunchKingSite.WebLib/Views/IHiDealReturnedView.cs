﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealReturnedView
    {
        event EventHandler<DataEventArgs<string>> OrderIdInput;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ReasonInput;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> CouponReturn;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> GoodsReturn;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ToCouponRefund;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> ToGoodsRefund;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> CouponCashBackConfirm;
        event EventHandler<DataEventArgs<HiDealReturnedViewReturnOrderData>> GoodsCashBackConfirm;

        string UserName { get; }
        int UserId { get; }
        /// <summary>
        /// 退貨申請書網址。
        /// </summary>
        string RefundFormUrl { get; }
        /// <summary>
        /// 是否只能退現金。
        /// </summary>
        bool IsRefundCashOnly { get; set; }
        /// <summary>
        /// 是否已產發票
        /// </summary>
        bool IsCreateEinvoice { get; set; }
        /// <summary>
        /// 訂單是否使用刷卡付款
        /// </summary>
        bool IsCreditCardPay { get; set; }
        /// <summary>
        /// 轉址到登入網頁
        /// </summary>
        void RedirectToLogin();
        /// <summary>
        /// orderID檢查後發現錯誤，回傳錯誤訊息。
        /// </summary>
        /// <param name="errorMessage">錯誤訊息</param>
        void ShowOrderIdInputError(string errorMessage);
        /// <summary>
        /// 進入退貨理由輸入畫面
        /// </summary>
        /// <param name="data">此次申請退貨的訂單編號</param>
        /// <param name="message">欲顯示於畫面上的訊息</param>
        void ShowReturnReasonPage(HiDealReturnedViewReturnOrderData data, string message);
        /// <summary>
        /// 轉只到憑證退貨頁
        /// </summary>
        void ShowCouponReturnPage(HiDealReturnedViewReturnOrderData data);
        /// <summary>
        /// 顯示憑證退回購物金完成的畫面
        /// </summary>
        void ShowCouponPointBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);
        /// <summary>
        /// 顯示憑證刷退申請畫面
        /// </summary>
        /// <param name="data"></param>
        void ShowCouponCashBack(HiDealReturnedViewReturnOrderData data);

        /// <summary>
        /// 顯示憑證刷退完成頁
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        void ShowCouponCashBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);
        /// <summary>
        /// 顯示憑證刷退完成頁-結檔前
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        void ShowCouponCashBackSuccessBeforeClose(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);

        /// <summary>
        /// 轉只到商品退貨頁
        /// </summary>
        void ShowGoodsReturnPage(HiDealReturnedViewReturnOrderData data);
        /// <summary>
        /// 顯示商品退回購物金完成的畫面
        /// </summary>
        void ShowGoodsPointBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);
        /// <summary>
        /// 顯示商品刷退申請畫面
        /// </summary>
        /// <param name="data"></param>
        void ShowGoodsCashBack(HiDealReturnedViewReturnOrderData data);

        /// <summary>
        /// 顯示商品刷退完成頁
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        void ShowGoodsCashBackSuccess(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);
        /// <summary>
        /// 顯示商品刷退完成頁-結檔前
        /// </summary>
        /// <param name="data"></param>
        /// <param name="returned"></param>
        void ShowGoodsCashBackSuccessBeforeClose(HiDealReturnedViewReturnOrderData data, HiDealReturned returned);
    }

    public enum HiDealReturnedViewWorkType
    {
        /// <summary>
        /// 輸入訂單編號
        /// </summary>
        OrderIdInput = 1,
        /// <summary>
        /// 輸入退貨原因
        /// </summary>
        ReasonInput,
        /// <summary>
        /// 憑證退貨(取貨方式為到店)
        /// </summary>
        CouponReturn,
        /// <summary>
        /// 憑證退貨退回購物金
        /// </summary>
        CouponPointBackSuccess,
        /// <summary>
        /// 憑證退貨刷退
        /// </summary>
        CouponCashBack,
        /// <summary>
        /// 憑證退貨成功
        /// </summary>
        CouponCashBackSuccess,
        /// <summary>
        /// 憑證刷退成功-結檔前
        /// </summary>
        CouponCashBackSuccessBeforeClose,
        /// <summary>
        /// 憑證退貨ATM退款
        /// </summary>
        CouponAtmBack,
        /// <summary>
        /// 憑證退貨ATM退款帳號輸入
        /// </summary>
        CouponAtmDataInput,
        /// <summary>
        /// 憑證退貨ATM退款帳號確認
        /// </summary>
        CouponAtmDataCheck,
        /// <summary>
        /// 憑證退貨ATM退款完成
        /// </summary>
        CouponAtmBackSuccess,
        /// <summary>
        /// 商品退貨(取貨方式為宅配)
        /// </summary>
        GoodsReturn,
        /// <summary>
        /// 商品退貨退紅利
        /// </summary>
        GoodsPointBackSuccess,
        /// <summary>
        /// 商品退貨刷退
        /// </summary>
        GoodsCashBack,
        /// <summary>
        /// 商品退貨退完成
        /// </summary>
        GoodsCashBackSuccess,
        /// <summary>
        /// 商品刷退成功-結檔前
        /// </summary>
        GoodsCashBackSuccessBeforeClose,
        /// <summary>
        /// 商品退貨ATM退款
        /// </summary>
        GoodsAtmBack,
        /// <summary>
        /// 商品退貨ATM資料輸入
        /// </summary>
        GoodsAtmDataInput,
        /// <summary>
        /// 商品退貨ATM資料確認
        /// </summary>
        GoodsAtmDataCheck,
        /// <summary>
        /// 商品退貨ATM退款完成
        /// </summary>
        GoodsAtmBackSuccess,
    }

    [Serializable]
    public class HiDealReturnedViewReturnOrderData
    {
        public string OrderId { get; set; }
        public int OrderPk { get; set; }
        public int OrderDetailId { get; set; }
        public Guid OrderDetailGuid { get; set; }
        public string Reason { get; set; }
        /// <summary>
        /// 結檔時間。
        /// </summary>
        public DateTime DealEndTime { get; set; }
    }
    public enum HiDealReturnedViewErrorType
    {
        /// <summary>
        /// 訂單編號錯誤
        /// </summary>
        OrderIdError,
        /// <summary>
        /// 退貨原因錯誤
        /// </summary>
        ReasonError,
        /// <summary>
        /// 退款作業失敗
        /// </summary>
        RefundError,
    }
}

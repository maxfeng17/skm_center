using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IRandomParagraphView
    {
        RandomParagraphPresenter Presenter { get; set; }
        string ContentName { get; set; }
        string ContentBody { get; set; }
        int CityId { get; set; }
        RandomCmsType Type { get; }
        string UniqueName { get; }
        void SetContent(ViewCmsRandomCollection data);
    }

    public interface IMasterPageTopBanner
    {
        int CityId { get; set; }
        RandomCmsType Type { get; set; }
    }
}

using System;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core;
using System.Collections;

namespace LunchKingSite.WebLib.Views
{
    public interface IVerificationInventoryView
    {
        Guid BusinessHourGuid { get; }
        event EventHandler<DataEventArgs<int>> GetInventoryCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        void SetInventoryList(ViewPponCouponCollection vpc);
        void SetViewPponDealData(ViewPponDeal vpd);
        void SetDealAccountingData(DealAccounting da);
        void SetVerificationData(PponDealSalesInfo obj);
        void SetVerificationRecordData(VerificationStatisticsLog obj);
        int PageSize { get; set; }
        int CurrentPage { get; }
        string SortExpression { get; set; }
        Guid SellerGuid { get; set; }
        string LoginUserId { get; }
        string ClientIp { get; }
        string SellerVerifiedQuantity { get; set; }
        int DealAccountingFlag { get; set; }
        int SalesNumber { get; set; }
        Dictionary<int, string> SelectedItems { get; set; }
        Dictionary<int, string> VerifyCouponList { get; }
        Dictionary<int, string> EnforceCouponList { get; }
        event EventHandler<EventArgs> OkButtonClick;
        event EventHandler<EventArgs> InventoryLostClick;
        event EventHandler<EventArgs> SelectAllClick;
    }
}

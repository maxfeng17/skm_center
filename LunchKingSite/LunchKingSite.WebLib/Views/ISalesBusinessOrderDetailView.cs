﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using MongoDB.Bson;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesBusinessOrderDetailView
    {
        #region property
        SalesBusinessOrderDetailPresenter Presenter { get; set; }
        ObjectId BusinessOrderObjectId { get; }
        string BusinessOrderId { get; }
        Guid BusinessHourGuid { get; }
        Guid AncestorSequenceBid { get; }
        Guid AncestorQuantityBid { get; }
        string SellerId { get; }
        string SalesId { get; }
        string UserId { get; }
        string AccountBankNo { get; }
        string AuthorizationGroup { get; set; }
        /// <summary>
        /// 選取的頻道、區域等資料
        /// </summary>
        Dictionary<int, List<int>> SelectedChannelCategories { get; set; }

        Dictionary<int, List<int>> SelectedChannelSpecialRegionCategories { get; set; }

        List<int> SelectedDealCategories { get; set; }

        int[] SelectedTravelCategoryId { get; set; }
        Dictionary<int, int[]> SelectedCommercialCategoryId { get; set; }
        string SalesDept { get; set; }
        string SalesMemberArea { get; set; }
        Level SalesLevel { get; set; }
        string CityJSonData { get; set; }
        string SalesName { get; set; }
        string SalesPhone { get; set; }
        bool IsContinuedSequence { get; }
        bool IsContinuedQuantity { get; }
        int AddressCityId { get; set; }
        int AddressTownshipId { get; set; }
        string SalesEmailArray { get; set; }
        string SalesNameArray { get; set; }
        string SalesInfoArray { set; }
        bool IsSalesAssistant { get; }
        bool IsSales { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<string>> SaveClicked;
        event EventHandler<EventArgs> DeleteClicked;
        event EventHandler<DataEventArgs<ObjectId>> StoreDeleteClicked;
        event EventHandler<DataEventArgs<BusinessCopyType>> CopyClicked;
        event EventHandler<EventArgs> DownloadStoreClicked;
        event EventHandler<DataEventArgs<List<SalesStore>>> ImportClicked;
        event EventHandler<EventArgs> RefreshStoreClicked;
        event EventHandler<EventArgs> DownloadClicked;
        event EventHandler<DataEventArgs<string>> ReferredClicked;
        #endregion

        #region method
        void GetLocationDropDownList(List<City> citys, List<City> locations);
        void SetSelectableZone(List<PponCity> cities);
        void SetSelectableChannel(CategoryTypeNode channelTypeNode);
        void SetSelectableTravelCategory(List<Category> categories);
        void SetSelectableCommercialCategory(Dictionary<Category, List<Category>> categories);
        void SetAccountingBankInfo(BankInfoCollection bankList);
        void SetAccountingBranchInfo(BankInfoCollection branchList);
        void GetSalesDepartment(List<Department> deptc);
        void GetAccBusinessGroupGetList(AccBusinessGroupCollection abgc);
        void SetBusinessOrderDetail(BusinessOrder data);
        void SetStoreList(IEnumerable<SalesStore> storeLsit);
        BusinessOrder GetBusinessOrderData(BusinessOrder item, bool isCopy, string historyMemo);
        void SetSellerData(Seller seller);
        void RedirectSalesManagementPage();
        void SetProvisionDepartments(IEnumerable<ProvisionDepartment> departments);
        void SetProvisionDepartmentsModel(IEnumerable<ProvisionDepartmentModel> departments);
        void ImportStoreList(List<SalesStore> storeList);
        void ShowMessage(string msg, string redirect = "");
        void Download(BusinessOrder businessOrder);
        void ReloadPage(ObjectId boid);
        void SetDealLabel(DealLabelCollection dealLabels);
        void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes);
        #endregion
    }
}

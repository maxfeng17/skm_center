﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IHistoryView
    {
        #region property
        HistoryPresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        /// <summary>
        /// 案件編號
        /// </summary>
        string SearchServiceNo { get; }
        /// <summary>
        /// 訂單編號
        /// </summary>
        Guid SearchOrderGuid { get; }
        /// <summary>
        /// 帳號
        /// </summary>
        string Mail { get; }
        string SDate { get; }
        string EDate { get; }
        string SHour { get; }
        string SMinute { get; }
        string EHour { get; }
        string EMinute { get; }
        /// <summary>
        /// 指定處理人員userid
        /// </summary>
        int SearchUserId { get; }
        /// <summary>
        /// 指定處理人員userid2
        /// </summary>
        int SearchSecUserId { get; }
        /// <summary>
        /// 問題等級
        /// </summary>
        int SearchPriority { get; }
        /// <summary>
        /// 問題分類
        /// </summary>
        int SearchCategory { get;}
        /// <summary>
        /// 有無客服一線權限
        /// </summary>
        bool IsCustomerCare { get; }
        /// <summary>
        /// 有無客服二線權限
        /// </summary>
        bool IsCustomerCare2 { get; }
        /// <summary>
        /// 案件狀態
        /// </summary>
        int SearchStatus { get; }
        /// <summary>
        /// 檔次類型
        /// </summary>
        int SearchPponDealType { get; }
        /// <summary>
        /// 案件來源
        /// </summary>
        int MessageType { get; }
        /// <summary>
        /// 裝置平台
        /// </summary>
        int CaseSource { get; }
        /// <summary>
        /// 檔號
        /// </summary>
        string UniqueId { get; }
        /// <summary>
        /// 供應商
        /// </summary>
        string Supplier { get; }
        /// <summary>
        /// 主因
        /// </summary>
        int SubCategory { get; }
        int UserId { get; }
        /// <summary>
        /// 選取的認領案件編號
        /// </summary>
        string ChoseBox { get; }
        /// <summary>
        /// 來源平台
        /// </summary>
        int IssueFromType { get; }
        /// <summary>
        /// 台新帳號
        /// </summary>
        string TsMemberNo { get; }

        #endregion
        #region event
        event EventHandler Search;
        event EventHandler Export;
        event EventHandler AddClaimed;
        event EventHandler<DataEventArgs<int>> HistoryCount;
        event EventHandler<DataEventArgs<int>> HistoryPageChanged;

        #endregion

        #region method
        void HistoryList(ViewCustomerServiceListCollection MessageList);
        #endregion

    }

}

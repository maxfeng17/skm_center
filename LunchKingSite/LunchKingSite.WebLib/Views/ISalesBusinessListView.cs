﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesBusinessListView
    {
        #region property
        SalesBusinessListPresenter Presenter { get; set; }
        string UserName { get; }
        int? SalesId { get; }
        string ItemName { get; }
        Guid Bid { get; }
        string SellerName { get; }
        DateTime OrderTimeS { get; }
        DateTime OrderTimeE { get; }
        bool IsOrderTimeSet { get; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        int EmpUserId { get;}
        string CrossDeptTeam { get;}
        FileUpload FUExport { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler Export;
        event EventHandler Import;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        void SetBusinessList(Dictionary<ViewPponDealCalendar, Proposal> dataList);
        void ShowMessage(string msg);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.WebLib.Views
{
    public interface IVbsBulletinBoardSetUpView
    {

        #region Event

        /// <summary>
        /// 儲存公布訊息
        /// </summary>
        event EventHandler<PublishingArgs> SetPublishing;
        /// <summary>
        /// 將公布訊息置頂
        /// </summary>
        event EventHandler<DataEventArgs<int>> StickPublishing;
        event EventHandler<BulletinBoardListGetEventArgs> BulletinBoardListGet;
        /// <summary>
        /// 換頁
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChanged;
        /// <summary>
        /// 取得總訊息數，Pager 用
        /// </summary>
        event EventHandler<DataEventArgs<int>> GetPublishCount;
        event EventHandler<DataEventArgs<int>> DeletePublishing;

        #endregion

        #region Property

        VbsBulletinBoardSetUpPresenter Presenter { get; set; }
        /// <summary>
        /// 公告內容最大的限制長度
        /// </summary>
        int ContentMaxLength { get; }
        int FilterSellerType { get; }
        /// <summary>
        /// 公告內容
        /// </summary>
        string PublishContent { get; set; }
        /// <summary>
        /// 是否對 "憑證店家" 公布
        /// </summary>
        bool IsPublishToShop { get; }
        /// <summary>
        /// 是否對 "宅配店家" 公布
        /// </summary>
        bool IsPublishToHouse { get; }
        /// <summary>
        /// 公告時間
        /// </summary>
        DateTime PublishTime { get; }
        string LinkTitle { get; set; }
        string LinkUrl { get; set; }
        string UserName { get; }

        #region GridPager
        int PageSize { get; set; }
        int CurrentPage { get; }
        #endregion

        #endregion

        #region Methods

        void Alert(string msg);
        /// <summary>
        /// 動態建構 "公告對象" 的CheckBox
        /// </summary>
        void BuildCbPublishSellerType();
        void BuildDdlSellerType();
        /// <summary>
        /// 限制公告內容的字數
        /// </summary>
        void AddContentMaxLength();
        /// <summary>
        /// 更新完公告訊息後, 回應結果給使用者
        /// </summary>
        /// <param name="isSuccess">是否成功更新公告訊息</param>
        /// <param name="mode">更新型態</param>
        /// <param name="message">額外訊息</param>
        void PublishSetDone(bool isSuccess, UpdateMode mode, string message);
        void SetBulletinBoardList(IEnumerable<VbsBulletinBoard> boardCols);

        #endregion

    }

    #region Internal Class

    public class PublishingArgs : EventArgs
    {
        /// <summary>
        /// 異動模式, 新增/修改/刪除
        /// </summary>
        public UpdateMode Mode;
            
    }

    public class BulletinBoardListGetEventArgs : EventArgs 
    { 
        //
        public int CurrentPage;
        public int SellerType;
    }

    public enum UpdateMode 
    { 
        Add,
        Update,
        Delete,
        Stick
    }

    public enum RowCommand 
    { 
        /// <summary>
        /// 置頂
        /// </summary>
        Stick,
        /// <summary>
        /// 刪除
        /// </summary>
        Del,
    }

    #endregion
}

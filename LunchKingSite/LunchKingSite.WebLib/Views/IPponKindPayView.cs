﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponKindPayView
    {
        event EventHandler<CheckDataEventArgs> CheckOut;

        event EventHandler ResetContent;

        event EventHandler<ShowResultEventArgs> ShowPageResult;

        PponKindPayPresenter Presenter { get; }

        Guid BusinessHourGuid { get; }

        string SessionId { get; }

        string TransactionId { get; set; }

        string UserName { get; }

        int UserId { get; }

        Guid OrderGuid { get; set; }

        Guid ParentOrderId { get; set; }

        bool IsTest { get; set; }

        ExternalMemberInfo SsoMemberInfo { get; }

        string UserMemo { get; set; }

        Guid SelectedStoreGuid { get; }

        CpaClientMain<CpaClientEvent<CpaClientDeail>> NewCpa { get; set; }

        PponDeliveryInfo DeliveryInfo { get; }

        string TicketId { get; }

        int Amount { get; set; }

        int DeliveryCharge { get; set; }

        string ReferrerSourceId { get; }

        IViewPponDeal TheDeal { get; set; }

        int CookieCityId { get; }

        string CookieReferenceId { get; }

        PaymentResultPageType PaymentResult { get; set; }

        string RsrcSession { get; }

        bool IsPreview { get; }

        bool IsATMLimitZero { get; set; }

        bool IsATMAvailable { get; set; }

        bool IsPaidByATM { get; }

        string ATMAccount { get; set; }

        string AddressInfo { get; set; }

        string UserInfo { get; set; }

        string IsHami { get; }

        OrderFromType FromType { get; }

        CouponFreightCollection TheCouponFreight { get; set; }

        EntrustSellType EntrustSell { get; set; }

        void ReEnterPaymentGate(PaymentType paymentType, string transId);

        void ShowTransactionPanel();

        void ShowATMTransactionPanel();

        /// <summary>
        /// 設定零元好康結果內容
        /// </summary>
        /// <param name="coupon">coupon資訊</param>
        void SetZeroDealResult(CouponCollection coupons, GroupOrderStatus groupOrderStatus);

        void ShowResult(PaymentResultPageType result, IViewPponDeal pponDeal, int atmAmount);

        void ShowCreditcardChargeInfo();

        void GotoATMPay(string ticketId);

        void GotoCreditCardPay(string ticketId);

        // add end

        void SetContent(IViewPponDeal deal, AccessoryGroupCollection multOption, int AlreadyBoughtCount, ViewPponStoreCollection stores, EntrustSellType entrustSell, bool isDailyRestriction);

        void AlertMessage(string msg);

        /// <summary>
        /// 顯示訊息，然後回好康Default頁面，若message 為空，則直接回好康Default頁
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="message"></param>
        void GoToPponDefaultPage(Guid bid, string message);

        void RedirectToLogin();

        void RedirectToLoginOnlyPEZ(string message, Guid bid);

        void CheckRefUrl();

        void GoToDefaulPage();

        void GoToZeroActivity();

        /// <summary>
        /// 無法配送離島
        /// </summary>
        bool NotDeliveryIslands { get; set; }

        bool MultipleBranch { get; set; }

        /// <summary>
        /// 是否為宅配檔
        /// </summary>
        bool IsItem { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface ICityAddView
    {
        event EventHandler SearchCity;
        event EventHandler<DataEventArgs<CityInfo>> AddCity;
        event EventHandler<DataEventArgs<City>> UpdateCity;
        event EventHandler<DataEventArgs<int>> DeleteCity;

        CityAddPresenter Presenter { get; }
        int SelectCity { get; }

        void SetCityList(CityCollection c);
        void SetCityDropDown(Dictionary<int, string> city);
        void SetUpdatePanel(bool b);
        void SetImageFile(HttpPostedFile pFile, string destFileName);
    }


    public sealed class CityInfo
    {
        private HttpPostedFile _pFile;
        public HttpPostedFile PFile
        {
            get { return _pFile; }
            set { _pFile = value; }
        }

        private City _city;
        public City City
        {
            get { return _city; }
            set { _city = value; }
        }
    }
}

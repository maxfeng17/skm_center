﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesProposalContentView
    {
        #region property
        SalesProposalContentPresenter Presenter { get; set; }
        string UserName { get; }
        int ProposalId { get; }
        Guid BusinessHourGuid { get; }
        Guid SellerGuid { get; }
        string AncestorBid { get; }
        string AncestorSeqBid { get; }
        bool IsUrgent { get; set; }
        bool IsOrderTimeSave { get; set; }
        Dictionary<int, string> ReturnReason { get; }
        string[] SalesmanNameArray { set; }
        Guid CheckSellerGuid { get; }
        decimal MinGrossMargin { get; }
        /// <summary>
        /// 是否具有權限設定[低毛利率可使用折價券]、[本檔不可使用折價券]
        /// </summary>
        bool HasUseDiscountSetPrivilege { get; }
        int ReceiptType { get; }
        bool IsListCheck { get; }
        /// <summary>
        /// 退件-資料修正細項Id的陣列字串
        /// </summary>
        /// <example>[1, 2, 3]</example>
        string ReturnDetailDataModifyValue { get; }
        /// <summary>
        /// 退件-資料修正細項名稱的字串 (chang_log用)
        /// </summary>
        /// <example>資料錯誤修正：原價 / 售價 / 店家電話</example>
        string ReturnDetailDataModifyTxt { get; }
        /// <summary>
        /// 展演類型檔次
        /// </summary>
        bool IsPromotionDeal { get; }
        /// <summary>
        /// 展覽類型檔次
        /// </summary>
        bool IsExhibitionDeal { get; }
        /// <summary>
        /// 遊戲檔次
        /// </summary>
        bool IsGame { get; }
        /// <summary>
        /// 遊戲檔次
        /// </summary>
        bool IsChannelGift { get; }
        /// <summary>
        /// 代銷通路
        /// </summary>
        List<int> AgentChannels { get; }
        #endregion

        #region event
        event EventHandler<EventArgs> Save;
        event EventHandler<DataEventArgs<DateTime>> OrderTimeSave;
        event EventHandler<DataEventArgs<string>> ChangeSeller;
        event EventHandler<EventArgs> Urgent;
        event EventHandler<EventArgs> Delete;
        event EventHandler<EventArgs> CreateBusinessHour;
        event EventHandler<DataEventArgs<KeyValuePair<string, SellerSalesType>>> Referral;
        event EventHandler<EventArgs> ProposalSend;
        event EventHandler<DataEventArgs<string>> ProposalReturned;
        event EventHandler<DataEventArgs<KeyValuePair<int, List<string>>>> Assign;
        event EventHandler<DataEventArgs<string>> ChangeLog;
        event EventHandler<DataEventArgs<string>> CheckFlag;
        event EventHandler<DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>> ReCreateProposal;
        event EventHandler<DataEventArgs<KeyValuePair<int, ProposalCopyType>>> CloneProposal;
        event EventHandler<DataEventArgs<int>> SellerProposalSaleEditor;
        event EventHandler<DataEventArgs<int>> SellerProposalCheckWaitting;
        event EventHandler<EventArgs> PponStoreSave;
        event EventHandler<DataEventArgs<int>> VbsSave;

        event EventHandler<DataEventArgs<string>> SellerProposalReturned;
        event EventHandler<DataEventArgs<Guid>> Download;
        event EventHandler<EventArgs> UpdateSellerGuid;
        #endregion

        #region method
        void SetProposalContent(SalesBusinessModel model, ViewProposalSeller pro, ProposalLogCollection logs, ProposalLogCollection assignLogs,
            SellerCollection tcsc, ProposalStoreCollection psc, ProposalCouponEventContent pcec, ProposalContractFileCollection pcfc, ProposalCategoryDealCollection pcdc,
            Seller seller, ProposalSellerFileCollection sellerFiles, List<ShoppingCartFreight> scfs);
        Proposal GetProposalData(Proposal pro);
        void RedirectProposal(int id, string anchor = "");
        void RedirectSellerList();
        void RedirectBusinessHour(Guid bid);
        void ShowMessage(string msg, ProposalContentMode mode, string parms = "");
        ProposalStoreCollection GetPponStores();
        ProposalStoreCollection GetVbs();
        void DownloadWord(Proposal pro, DeliveryType type, Dictionary<string, string> dataList, string fileName, string contacts, List<Seller> LocationstoreList, List<Seller> AccountingstoreList);
        ProposalCategoryDealCollection GetCategoryDeals();
        #endregion
    }

    public enum ProposalContentMode
    {
        DataError = 0,
        BackToList = 1,
        GoToProposal = 2,
        AlertGoToProposal = 3,
        ProposalNotFound = 4,
        GotoHouseProposal = 5,
    }
}

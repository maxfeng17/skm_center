﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesBusinessContentView
    {
        #region property
        SalesBusinessContentPresenter Presenter { get; set; }
        string UserName { get; }
        Guid BusinessHourGuid { get; }
        bool IsInputTaxRequired { get; set; }
        int ProposalId { get; }
        string ContentDescription { get; set; }
        string ContentRemark { get; set; }

        #endregion

        #region event
        event EventHandler<EventArgs> Save;
        event EventHandler<DataEventArgs<KeyValuePair<Guid, ProposalCopyType>>> ReCreateProposal;
        event EventHandler<EventArgs> PponStoreSave;
        event EventHandler<DataEventArgs<int>> VbsSave;
        event EventHandler<DataEventArgs<Guid>> SettingCheck;
        event EventHandler<DataEventArgs<KeyValuePair<string, SellerSalesType>>> Referral;
        event EventHandler<DataEventArgs<string>> ChangeLog;
        #endregion

        #region method
        void SetAccField(AccBusinessGroupCollection accGroups, DepartmentCollection depts);
        void SetBusinessContent(SalesBusinessModel model, ViewComboDealCollection combo, SellerCollection tscs, CategoryDealCollection cdc, PponStoreCollection psc, Proposal pro, ViewEmployee emp, BusinessChangeLogCollection logs);
        SalesBusinessModel GetSalesBusinessModelData(SalesBusinessModel model);
        CategoryDealCollection GetCategoryDeals();
        PponStoreCollection GetPponStores(Guid bid);
        PponStoreCollection GetVbs(Guid bid);
        void ShowMessage(string msg, BusinessContentMode mode, string parms = "");

        #endregion
    }

    public class SalesBusinessModel
    {
        public SalesBusinessModel()
        {
            Business = new BusinessHour();
            Combodeals = new ViewComboDealCollection();
            SellerProperty = new Seller();
            Group = new GroupOrder();
            ItemProperty = new Item();
            Property = new DealProperty();
            Accounting = new DealAccounting();
            Costs = new DealCostCollection();
            Freight = new CouponFreightCollection();
            ContentProperty = new CouponEventContent();
        }

        public BusinessHour Business { get; set; }
        public ViewComboDealCollection Combodeals { get; set; }
        public Seller SellerProperty { get; set; }
        public GroupOrder Group { get; set; }
        public Item ItemProperty { get; set; }
        public DealProperty Property { get; set; }
        public DealAccounting Accounting { get; set; }
        public DealCostCollection Costs { get; set; }
        public CouponFreightCollection Freight { get; set; }
        public CouponEventContent ContentProperty { get; set; }
        public Proposal Pro { get; set; }
    }



    public enum BusinessContentMode
    {
        DataError = 0,
        BackToList = 1,
        GoToSetup = 2,
        GoToProposal = 3,
        Reload = 4,
        Redirect = 5,
        BusinessNotFound = 6
    }

    public enum TabName
    {
        MainSet,
        ChannelSet,
        MultiDealSet,
        PponStoreSet,
        HistorySet
    }
}

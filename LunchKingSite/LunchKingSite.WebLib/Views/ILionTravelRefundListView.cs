﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.ControlRoom.Order;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ILionTravelRefundListView
    {
        #region property
        LionTravelRefundListPresenter Presenter { get; set; }
        ProgressState ProdressStatus { get; }
        int PageCount { get; set; }
        int FilterDeliveryType { get;}
        int FilterType { get; }
        string FilterData { get; }
        DateTime? StartDate { get; }
        DateTime? EndDate { get; }
        AgentChannel OrderClassType { get; }
        #endregion

        #region method
        void SetRefundForms(ViewLiontravelOrderReturnFormListCollection data, Dictionary<int, int> applicationProductCountInfos, Dictionary<int, int> refundedProductCountInfos);
        void SetUpPageCount();
        #endregion

        #region event
        event EventHandler SearchRefundForms;
        event EventHandler<DataEventArgs<int>> PageChange;
        #endregion
    }
}

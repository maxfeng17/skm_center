﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface IProposalAssignFalgView
    {
        ProposalAssignFalgPresenter Presenter { get; set; }

        #region method
        void SetProposalAssignContent(ProposalCollection pc);
        #endregion
    }
}

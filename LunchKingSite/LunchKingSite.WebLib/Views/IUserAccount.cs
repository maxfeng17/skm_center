﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserAccount
    {
        #region property

        UserAccountPresenter Presenter { get; set; }

        string UserName { get; }

        int UserId { get; }

        bool Mode { get; set; }

        int MID { get; set; }

        Dictionary<int, bool> SubscribeRegions { get; set; }

        bool IfReceiveEDM { get; }

        bool IfReceiveStartedSell { get; }

        MobileMember MobileMember { get; set; }
        string CityListStr { get; set; }

        #endregion property

        #region method

        void SetEinvoiceTriple(EinvoiceTriple data);

        void SetEinvoiceWinner(EinvoiceWinner data);

        void SetCreditCardForm(List<dynamic> creditcardViews);

        void SetDropDownList(List<City> cities);

        void GetMemberInfo(Member m, MemberDeliveryCollection mdc, bool lifemember, MemberLinkCollection mlc,
            MemberAuthInfo mai);

        void SetAddressInfo(string address, Building bld, City city);

        void GetCityChange(CityCollection cities);

        void GetAreaChange(ListItemCollection buildings);

        void GetBuildingChange(Building building);

        void SetDeliveryInfo(MemberDelivery md, Building bld, City city);

        void SetPponCityGroupList(List<CategoryNode> list);

        void SetPasswordUpdateResult(bool updateResult);

        void SetChangeEmailError(Core.ReAuthStatus changeResult);

        void SetChangeEmailAlert(Core.ReAuthStatus changeResult);

        void SetBindEmailAlert(Core.ReAuthStatus changeResult);

        void SetDeleteCreditCardAlert(bool result, string msg);
        void ShowMessage( string msg);
        #endregion method

        #region event

        event EventHandler<DataEventArgs<KeyValuePair<Guid, string>>> GetAddressInfo;

        event EventHandler<DataEventArgs<int>> CityChange;

        event EventHandler<DataEventArgs<int>> AreaChange;

        event EventHandler<DataEventArgs<Guid>> BuildingChange;

        event EventHandler<DataEventArgs<Member>> UpdateMemberInfo;

        event EventHandler<DataEventArgs<EinvoiceTriple>> UpdateEinvoiceTriple;

        event EventHandler<DataEventArgs<EinvoiceWinner>> UpdateWinEinvoice;

        event EventHandler<EventArgs> UpdateSubscription;

        event EventHandler<DataEventArgs<UserEditCreditCard>> AddCreditCard;

        event EventHandler<DataEventArgs<Guid>> DeleteCreditCard;

        event EventHandler<DataEventArgs<List<string>>> UpdateMemberPassword;

        event EventHandler<DataEventArgs<MemberDelivery>> UpdateMemberDelivery;

        event EventHandler DeleteDeliveryInfo;

        event EventHandler<DataEventArgs<string>> ChangeMemberEmail;

        event EventHandler ChangeMemberEmailResend;

        event EventHandler ChangeMemberEmailCancel;

        event EventHandler BindingMemberEmailResend;

        event EventHandler BindingMemberEmailCancel;

        event EventHandler<DataEventArgs<LunchKingSite.BizLogic.Model.PponDeliveryInfo>> UpdateMemberEInvoiceInfo;
        #endregion event
    }

    public class UserEditCreditCard
    {
        public string Name { get; set; }
        public string Number { get; set; }
        public string Year { get; set; }
        public string Month { get; set; }
    }

    public class UserDeleteCreditCard
    {
        public string UserName { get; set; }
        public Guid CardGuid { get; set; }
    }
}
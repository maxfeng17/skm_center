﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventRedirectionView
    {
        string ReferenceId { get; }
        string WebRoot { get; }
        Guid EventId { get; }
        string UserName { get; }

        bool SetCookie(string value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message, string goToUrl);
        void SetPageMeta(EventContent ec);
        void RedirectPage(string goToUrl);
    }
}

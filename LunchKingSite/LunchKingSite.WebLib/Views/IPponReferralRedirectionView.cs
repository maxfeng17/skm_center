﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponReferralRedirectionView
    {
        string ReferenceId { get; }
        string WebRoot { get; }
        Guid BusinessHourId { get; }
        string UserName { get; }

        bool SetCookie(string value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message,string goToUrl);
        void SetPageMeta(IViewPponDeal mainDeal);
        void RedirectPage(string goToUrl);
    }
}

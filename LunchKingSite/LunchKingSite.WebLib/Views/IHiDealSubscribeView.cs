﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;


namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealSubscribeView
    {
        HiDealSubscribePresenter Presenter { get; set; }
        string Cpa { get; }
        string UserName { get; }
        int RegionId { get; }
        event EventHandler<EventArgs> AddSubscript;
        void ShowAlert();
    }
}

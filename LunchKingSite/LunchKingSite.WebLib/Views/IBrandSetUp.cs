﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Component;
using System.Collections.Generic;
using System.Web;

namespace LunchKingSite.WebLib.Views
{
    public delegate string GetBrandItemStatusHandler(ViewBrandItem x);
    public delegate string GetBrandItemCategoryHandler(int y);
    public delegate string GetStringBrandItemDIscountStatusHandler(int brandItemId);
    public delegate string GetStringBrandDIscountStatusHandler(int brandId);
    public interface IBrandSetUp
    {

        #region property
        int Mode { get; set; }
        string UserName { get; }
        int BrandId { get; set; }
        int ItemId { get; set; }
        int ItemCount { get; set; }
        string SearchWord { get; }
        string SearchCityId { get; }
        bool IsLimitInThreeMonth { get; }
        List<int> SelectedBrandItem { get; }
        string SearchItemName { get; }
        int SearchItemCid { get; }
        string IsDelMainPic { get; }
        string IsDelMainBackgroundPic { get; }
        string IsDelMobileMainPic { get; }
        string IsDelAppBannerImage { get; }
        string IsDelAppPromoImage { get; }
        string IsDelImgWeb1 { get; }
        string IsDelImgWeb2 { get; }
        string IsDelImgWeb3 { get; }
        string IsDelImgWeb4 { get; }
        string IsDelImgWeb5 { get; }
        string IsDelDealPromoImage { get; }
        string IsDelDiscountBannerImage { get; }
        string IsDelEventListImage { get; }
        string IsDelChannelListImage { get; }
        string IsDelWebRelayImage { get; }
        string IsDelMobileRelayImage { get; }
        string IsDelFbShareImage { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<bool>> SetBrandShowInApp;
        event EventHandler<DataEventArgs<bool>> SetBrandShowInWeb;
        event EventHandler UpdateBrandStatus;
        event EventHandler Search;
        event EventHandler ChangeMode;
        event EventHandler GetBrand;
        event EventHandler<DataEventArgs<BrandUpdateModel>> SaveBrand;
        event EventHandler<DataEventArgs<List<string>>> ImportBidList;
        event EventHandler UpdateBrandItemStatus;
        event EventHandler UpdateSeqStatus;
        event GetBrandItemStatusHandler GetBrandItemStatus;
        event GetBrandItemCategoryHandler GetBrandItemCategory;
        event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateBrandItemSeq;
        event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateBrandItemCategorySeq;
        event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, string>>>> UpdateBrandItemCategoryName;
        event EventHandler<DataEventArgs<KeyValuePair<int, List<int>>>> DeleteBrandItemCategory;
        event EventHandler SearchBrandItem;
        event EventHandler<DataEventArgs<KeyValuePair<int, string>>> SaveBrandItemCategory;
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> BatchEditBrandItemCategory;
        event EventHandler DeleteBrandItemList;
        event GetStringBrandDIscountStatusHandler GetStringBrandDIscountStatus;
        #endregion

        #region method
        void ChangePanel(int mode);
        void SetBrand(Brand brand);
        void SetBrandList(List<Brand> brands);
        void SetBrandItemList(ViewBrandItemCollection items);
        void InitBrandCities(List<PponCity> pponCities);
        void SetBrandCities(List<PponCity> getPponCities, ViewCmsRandomCollection viewCmsRandomGetCollectionByBrandId);
        void SetBrandItemCategory(BrandItemCategoryCollection bicList);
        void SetBrandItemCategoryForDialog(BrandItemCategoryCollection bicList);
        void AlertMsg(string Msg);
        #endregion
    }
    public class BrandUpdateModel
    {
        /// <summary>
        /// 異動的廠商品牌館主檔
        /// </summary>
        public Brand MainBrand { get; set; }
        /// <summary>
        /// 上傳MainPic圖片的物件
        /// </summary>
        public HttpPostedFile MainPicPostedFile { get; set; }
        /// <summary>
        /// 上傳MainBackgroundPic圖片的物件
        /// </summary>
        public HttpPostedFile MainBackgroundPicPostedFile { get; set; }
        /// <summary>
        /// 上傳MobileMainPic圖片的物件
        /// </summary>
        public HttpPostedFile MobileMainPicPostedFile { get; set; }
        /// <summary>
        /// 上傳AppBanner圖片的物件
        /// </summary>
        public HttpPostedFile AppBannerPostedFile { get; set; }
        /// <summary>
        /// 上傳AppPromo主要圖片的物件
        /// </summary>
        public HttpPostedFile AppPromoPostedFile { get; set; }
        /// <summary>
        /// 上傳行銷BannerWeb主要圖片的物件1
        /// </summary>
        public string MarketingWebPostedFile1 { get; set; }
        /// <summary>
        /// 上傳行銷BannerWeb主要圖片的物件2
        /// </summary>
        public string MarketingWebPostedFile2 { get; set; }
        /// <summary>
        /// 上傳行銷BannerWeb主要圖片的物件3
        /// </summary>
        public string MarketingWebPostedFile3 { get; set; }
        /// <summary>
        /// 上傳行銷BannerWeb主要圖片的物件4
        /// </summary>
        public string MarketingWebPostedFile4 { get; set; }
        /// <summary>
        /// 上傳行銷BannerWeb主要圖片的物件5
        /// </summary>
        public string MarketingWebPostedFile5 { get; set; }
        /// <summary>
        /// 上傳行銷活動訊息活動LOGO
        /// </summary>
        public HttpPostedFile DealPromoImagePostedFile { get; set; }
        /// <summary>
        /// 策展2.0綁定折價券活動Banner
        /// </summary>
        public HttpPostedFile DiscountBannerImagePostedFile { get; set; }
        /// <summary>
        /// 策展列表頁活動Banner
        /// </summary>
        public HttpPostedFile EventListImagePostedFile { get; set; }
        /// <summary>
        /// 策展頻道頁活動Banner
        /// </summary>
        public HttpPostedFile ChannelListImagePostedFile { get; set; }
        /// <summary>
        /// WEB策展中繼頁圖片
        /// </summary>
        public HttpPostedFile WebRelayImagePostedFile { get; set; }
        /// <summary>
        /// M版策展中繼頁圖片
        /// </summary>
        public HttpPostedFile MobileRelayImagePostedFile { get; set; }
        /// <summary>
        /// FB分享預覽圖
        /// </summary>
        public HttpPostedFile FbShareImagePostedFile { get; set; }
        /// <summary>
        /// 策展2.0顯示頻道
        /// </summary>
        public UpdateCmsRandomCitiesModel UpdateCmsRandomCities { get; set; }
        /// <summary>
        /// 策展2.0Banner顯示區域
        /// </summary>
        public List<int> BannerType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEinvoiceWinnerList
    {
        #region property
        DateTime InvoiceWinnerDate { get; }
        string Filter { get; }
        int PageCount { get; set; }
        bool? IsSingleSearch { get; set; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler GetWinnerList;
        event EventHandler<DataEventArgs<string>> GetEinvoice;
        event EventHandler<DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>> ChangeInfo;
        event EventHandler<DataEventArgs<string>> GeneratePrint;
        event EventHandler<DataEventArgs<string>> ExportEinvoice;
        #endregion
        #region method
        void GetEinvoiceWinnerList(ViewEinvoiceWinnerCollection data);
        void SetUpPageCount();
        void ResponsePrintInfo(string printinfo);
        #endregion
    }
}

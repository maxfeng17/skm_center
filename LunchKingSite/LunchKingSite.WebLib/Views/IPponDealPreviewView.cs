﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponDealPreviewView
    {
        PponDealPreviewPresenter Presenter { get; }

        Guid BusinessHourGuid { get; }

        CouponFreightCollection TheCouponFreight { get; set; }

        bool MultipleBranch { get; set; }

        bool IsShoppingCart { get; set; }

        // add end

        void SetContent(IViewPponDeal deal, ViewMemberBuildingCity deliveryInfo, AccessoryGroupCollection multOption,
            int AlreadyBoughtCount, ViewPponStoreCollection stores, bool isUseDiscountCode, EntrustSellType entrustSell, bool isDailyRestriction);

        /// <summary>
        /// 顯示訊息，然後回好康Default頁面，若message 為空，則直接回好康Default頁
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="message"></param>
        void GoToPponDefaultPage(Guid bid, string message);

        /// <summary>
        /// APP限定頁導頁
        /// </summary>
        void RedirectToAppLimitedEdtionPage();
    }
}
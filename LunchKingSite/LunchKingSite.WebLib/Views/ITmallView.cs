﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Views
{
    public interface ITmallView
    {
        #region property
        /// <summary>
        /// 選取的分店Guid
        /// </summary>
        Guid SelectedStoreGuid { get; }
        /// <summary>
        /// 紀錄商品選項
        /// </summary>
        string UserMemo { get; set; }
        Guid BusinessHourGuid { get; set; }
        /// <summary>
        /// 紀錄後台操作人員
        /// </summary>
        string UserName { get; }
        string SessionId { get; }
        int PageCount { get; set; }
        /// <summary>
        /// 搜尋列表選項
        /// </summary>
        int SearchFilter { get; }
        /// <summary>
        /// 搜尋列表條件
        /// </summary>
        string SearchKey { get; }
        /// <summary>
        /// 依簡訊狀態搜尋選項
        /// </summary>
        int SmsFilter { get; }
        /// <summary>
        /// 產生簡訊期間(始)
        /// </summary>
        DateTime SmsStart { get; }
        /// <summary>
        /// 產生簡訊期間(止)
        /// </summary>
        DateTime SmsEnd { get; }
        int PageSize { get; }
        /// <summary>
        /// 判斷搜尋的種類(輸入條件或是依簡訊狀態)
        /// </summary>
        bool IsSearchByKey { get; set; }
        #endregion

        #region Tmall default property 天貓檔次預設的訂單屬性
        string DefaultDeliveryAddress { get; }
        /// <summary>
        /// 數量預設為1
        /// </summary>
        int DefaultQuanty { get; }
        /// <summary>
        /// 金額預設為0元
        /// </summary>
        int DefaultTotal { get; }
        /// <summary>
        /// 運費預設為0元
        /// </summary>
        int DefaultDeliveryCharge { get; }
        /// <summary>
        /// 預設0元不信託
        /// </summary>
        EntrustSellType DefaultEntrustSell { get; }
        /// <summary>
        /// 預設為P好康
        /// </summary>
        DepartmentTypes DefaultSellerType { get; }
        /// <summary>
        /// 預設為到店
        /// </summary>
        DeliveryType DefaultDeliveryType { get; }
        /// <summary>
        /// 預設的購買人
        /// </summary>
        string DefaultUserName { get; set; }
        /// <summary>
        /// 預設宅配購買人姓名
        /// </summary>
        string DefaultWriteName { get; }
        /// <summary>
        /// 簡訊備註
        /// </summary>
        string SmsMemo { get; }
        #endregion

        #region method
        /// <summary>
        /// 回傳檔次資料，核銷統計，分店選項和商品選項
        /// </summary>
        /// <param name="deal">檔次</param>
        /// <param name="verify_summary">核銷統計</param>
        /// <param name="multOption">商品選項</param>
        /// <param name="stores">分店選項</param>
        void SearchDealById(ViewPponDeal deal, VendorDealSalesCount verify_summary, AccessoryGroupCollection multOption, ViewPponStoreCollection stores, int sms_fali_total, int sms_success_total, int sms_queue_total);
        /// <summary>
        /// 顯示警示資訊
        /// </summary>
        /// <param name="message"></param>
        void ShowErrorMessage(string message);
        /// <summary>
        /// 搜尋列表
        /// </summary>
        /// <param name="list">天貓訂單對應P好康訂單相關資料</param>
        void GetSearchList(ViewOrderCorrespondingSmsLogCollection list);
        /// <summary>
        /// 分頁計算總筆數
        /// </summary>
        void SetPageCount();
        /// <summary>
        /// 訂單成立成功回傳資訊
        /// </summary>
        void OrderMakeSuccess(string order_id);
        /// <summary>
        /// 撈取發送失敗列表
        /// </summary>
        /// <param name="list"></param>
        void GetFailSmsList(ViewOrderCorrespondingSmsLogCollection list);
        #endregion

        #region event
        /// <summary>
        /// 用檔次uid搜尋檔次資料
        /// </summary>
        event EventHandler<DataEventArgs<int>> SearchDealByUid;
        /// <summary>
        /// 用檔次bid搜尋檔次資料
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> SearchDealByBid;
        /// <summary>
        /// 產生P好康訂單，和對應天貓訂單資料，並發送簡訊;傳入天貓訂單號碼和手機號碼
        /// </summary>
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> MakeTmallOrder;
        /// <summary>
        /// 搜尋訂單對應列表
        /// </summary>
        event EventHandler SearchList;
        /// <summary>
        /// 列表分頁改變
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChange;
        /// <summary>
        /// 重新發送簡訊
        /// </summary>
        event EventHandler<DataEventArgs<int>> ReSendCouponSms;
        #endregion
    }
}

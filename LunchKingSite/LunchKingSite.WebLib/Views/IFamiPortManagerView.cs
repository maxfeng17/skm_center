﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IFamiPortManagerView
    {
        #region property
        FamiPortManagerPresenter Presenter { get; set; }
        Guid BusinessHourGuid { get; }
        string EventId { get; }
        string Password { get; }
        string LoginUser { get; }
        int PageSize { get; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnSaveClicked;
        event EventHandler<DataEventArgs<string>> OnDeleteClicked;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> OnPageChanged;
        #endregion

        #region method
        void GetFamiPortList(Dictionary<Famiport, KeyValuePair<Guid, string>> dataList);
        void ShowMessage(string msg);
        #endregion
    }
}

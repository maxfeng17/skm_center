﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ICustomerCategoryView
    {
        #region property
        CustomerCategoryPresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        string Mode { get; }
        int UserId { get; }
        bool IsNeedOrderId { get; }
        bool IsShowFrontEnd { get; }
        string CategoryName { get; }
        string SubjectContent { get;}
        int MainCategoryId { get; }

        #endregion
        #region event
        event EventHandler AddCustomerCategory;
        event EventHandler<DataEventArgs<int>> DeleteCustomerCategory;
        event EventHandler<DataEventArgs<int>> GetCustomerCategoryCount;
        event EventHandler<DataEventArgs<int>> CategoryPageChanged;

        #endregion

        #region method
        void CategoryList(CustomerServiceCategoryCollection categoryList);
        #endregion

    }

}

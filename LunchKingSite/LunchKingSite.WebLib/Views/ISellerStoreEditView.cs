﻿using LunchKingSite.Core;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface ISellerStoreEditView
    {
        event EventHandler SaveSellerStore;

        event EventHandler CancelEdit;

        event EventHandler ReturnApply;        

        Guid SellerGuid { get; }

        Guid StoreGuid { get; }

        string UserName { get; }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        string SellerName { set; }

        SellerStoreEditViewWorkType WorkType { get; }

        /// <summary>
        /// 分店名
        /// </summary>
        string BranchName { get; set; }

        /// <summary>
        /// 電話
        /// </summary>
        string StoreTel { get; set; }

        /// <summary>
        /// 地址的城市ID
        /// </summary>
        int AddressCityId { get; set; }

        /// <summary>
        /// 地址的鄉鎮市區ID
        /// </summary>
        int? AddressTownshipId { get; set; }

        /// <summary>
        /// 地址
        /// </summary>
        string StoreAddress { get; set; }

        /// <summary>
        /// 經緯度
        /// </summary>
        KeyValuePair<string, string> LongitudeLatitude { get; set; }

        /// <summary>
        /// 營業時間
        /// </summary>
        string BusinessHour { get; set; }

        /// <summary>
        /// 公休日
        /// </summary>
        string CloseDateInformation { get; set; }

        /// <summary>
        /// 捷運
        /// </summary>
        string TrafficMrt { get; set; }

        /// <summary>
        /// 開車
        /// </summary>
        string TrafficCar { get; set; }

        /// <summary>
        /// 公車
        /// </summary>
        string TrafficBus { get; set; }

        /// <summary>
        /// 其他交通方是
        /// </summary>
        string TrafficOther { get; set; }

        /// <summary>
        /// 官網網址
        /// </summary>
        string WebUrl { get; set; }

        /// <summary>
        /// FaceBook網址
        /// </summary>
        string FaceBookUrl { get; set; }

        /// <summary>
        /// Plurk網址
        /// </summary>
        string PlurkUrl { get; set; }

        /// <summary>
        /// 部落格網址
        /// </summary>
        string BlogUrl { get; set; }

        /// <summary>
        /// tbOtherUrl
        /// </summary>
        string OtherUrl { get; set; }

        /// <summary>
        /// 備註
        /// </summary>
        string Remark { get; set; }

        bool IsOpenBooking { get; set; }

        /// <summary>
        /// 分店是否顯示預約系統設定
        /// </summary>
        bool IsOpenReservationSetting { get; set; }

        /// <summary>
        /// 狀態
        /// </summary>
        StoreStatus Status { get; set; }

        /// <summary>
        /// 結束營業狀態
        /// </summary>
        bool IsCloseDown { get; set; }

        /// <summary>
        /// 刷卡服務
        /// </summary>
        bool CreditCardAvailable { get; set; }

        /// <summary>
        /// 結束營業日
        /// </summary>
        DateTime? CloseDownDate { get; set; }

        IAuditBoardView AuditBoardView { get; }

        string CompanyName { get; set; }

        string CompanyBossName { get; set; }

        string CompanyID { get; set; }

        string SignCompanyID { get; set; }

        string CompanyBankCode { get; set; }

        string CompanyBranchCode { get; set; }

        string CompanyAccount { get; set; }

        string CompanyAccountName { get; set; }

        string AccountantName { get; set; }

        string AccountantTel { get; set; }

        string CompanyEmail { get; set; }

        string CompanyNotice { get; set; }

        string Message { get; set; }
        
        /// <summary>
        /// 店鋪鄰近捷運站分類List
        /// </summary>
        List<int> MRTLocationCategory { get; set; }
        List<KeyValuePair<string,int>> MRTLocationChecked { get; set; }

        void GoToSellerStoreList();

        void ShowMessage(string message);

        /// <summary>
        /// 設定下拉選單的初始值
        /// </summary>
        void SetLocationDropDownList();

        

        /// <summary>
        /// 回傳store分店資料
        /// </summary>
        /// <param name="store">分店資料</param>
        void SetStore(LunchKingSite.DataOrm.Store store, LunchKingSite.DataOrm.ChangeLogCollection changelogs, LunchKingSite.DataOrm.StoreCollection sts);

        string StoreRelationCode { get; set; }
    }

    public enum SellerStoreEditViewWorkType
    {
        Add,
        Modify,
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponReturnListView
    {
        int PageSize { get; set; }
        OrderLogStatus ReturnProcess { get; set; }
        string ProcessMessage { get; set; }
        string ItemName { get; set; }
        string ApplicationTime { get; set; }
        string Reason { get; set; }
        int OsLogCount { get; set; }
        string SortExpression { get; set; }
        ViewPponCouponCollection Coupons { get; set; }
        string ApplyTimeS { get; }
        string ApplyTimeE { get; }
        Dictionary<int, string> StatusFilter { get; }
        Dictionary<int, string> DepartmentFilter { get; }
        int StatusSelected { get; }
        int DepartmentSelected { get; }
        string FilterType { get; set; }
        string FilterUser { get; set; }
        bool SelectAll { get; }

        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler<DataEventArgs<Guid>> GetProcessMessage;
        event EventHandler<DataEventArgs<Guid>> GetOrderCoupons;
        event EventHandler<DataEventArgs<Guid>> GetApplicationTime;
        event EventHandler SortClicked;
        event EventHandler SearchClicked;
        event EventHandler ExportToExcel;
        event EventHandler<DataEventArgs<Guid>> GetReason;

        void SetGridReturn(ViewPponOrderReturnListCollection orders);
        void SetStatusFilter(Dictionary<int, string> status);
        void SetDepartmentFilter(Dictionary<int, string> department);
        void Export(ViewPponOrderReturnListCollection osCol);
        Dictionary<string, string> FilterTypes { get; }
        void SetFilterTypeDropDown(Dictionary<string, string> types);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IContactUsView
    {
        #region property

        ContactUsPresenter Presenter { get; set; }
        int CityId { get; set; }
        int TravelCategoryId { get; }
        int CategoryID { get; }

        #endregion

        #region method
        void SetTopSalesDeal(List<IViewPponDeal> vpd);
        #endregion
    }
}

﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesPhotographerCalenderView
    {
        #region property
        SalesPhotographerCalenderPresenter Presenter { get; set; }
        string UserName { get; }
        int MonthCount { get; }
        int DealType1 { get; }
        int DealType2 { get; }
        string Dept { get; }
        string EmpDept { get; }
        int EmpUserId { get; }
        int Year { get;}
        int Month { get;}
        int PhotoSource { get; }
        int? SalesId { get;}
        string Photographer { get; }


        #endregion

        #region event
        event EventHandler Search;
        event EventHandler PreviousSearch;
        event EventHandler NextSearch;
        #endregion

        #region method
        void SetPponDealCalendar(Dictionary<ViewPponDealCalendar, Proposal> vpdc);
        #endregion
    }
}

using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IBonusExchangeBackView
    {
        event EventHandler<DataEventArgs<Guid>> RedeemOrderDetailClicked;
        event EventHandler<DataEventArgs<UpdatableRedeemOrderInfo>> RedeemOrderUpdated;
        event EventHandler<DataEventArgs<int>> GetOrderCount;
        event EventHandler<DataEventArgs<int>> PageChanged;

        BonusExchangeBackPresenter Presenter { get; set; }
        string RedeemOrderId { get; }
        int PageSize { get; }

        void SetOrderListView(RedeemOrderCollection records);
        void SetOrderDetailView(RedeemOrder theOrder, RedeemOrderDetailCollection theDetail);
    }

    public sealed class UpdatableRedeemOrderInfo
    {
        #region props
        private Guid _id;
        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        private string _n;
        public string CustomerName
        {
            get { return _n; }
            set { _n = value; }
        }

        private string _bp;
        public string CustomerPhone
        {
            get { return _bp; }
            set { _bp = value; }
        }

        private string _bm;
        public string CustomerMobile
        {
            get { return _bm; }
            set { _bm = value; }
        }

        private string _da;
        public string DeliveryAddress
        {
            get { return _da; }
            set { _da = value; }
        }

        private DateTime? _dt;
        public DateTime? DeliveryTime
        {
            get { return _dt; }
            set { _dt = value; }
        }

        private string _memo;
        public string OrderMemo
        {
            get { return _memo; }
            set { _memo = value; }
        }
	
        #endregion
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserDiscountListView
    {
        event EventHandler<DataEventArgs<int>> GetDiscountListCount;

        event EventHandler<DataEventArgs<int>> PageChange;

        event EventHandler<EventArgs> UseStatusChanged;

        event EventHandler<EventArgs> SortTypeChanged;

        UserDiscountListPresenter Presenter { get; set; }

        string UserName { get; }

        FilterStatus DiscountStatus { get; set; }

        SortType Sort { get; }

        int UserId { get; }

        int CurrentPage { get; }

        int PageSize { get; set; }

        void SetDiscountList(Dictionary<ViewDiscountDetail, bool> discounts);
    }

    public enum FilterStatus
    {
        Unused,
        Expire,
        Used,
    }

    public enum SortType
    {
        SendDateDesc,
        AmountDesc,
        AmountAsc,
        ExpireDesc,
    }
}
﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IPiinlifeDealView
    {
        PiinlifeDealPresenter Presenter { get; set; }

        Guid BusinessHourGuid { get; }

        string MicroDataJson { get; set; }
        /// <summary>
        /// 埋Code
        /// </summary>
        string DealArgs { set; get; }

        int UserId { get; }

        void SetDealContent(IViewPponDeal mainDeal, Dictionary<IViewPponDeal, string> subDeals);

        void SeoSetting(IViewPponDeal deal,string appendTitle = "");

        void RedirectToPiinlifeDefault();

        void RedirectToComboDealMain(Guid? mainbid, Guid? subbid);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponDealReportsView
    {
        event EventHandler<DataEventArgs<DateTime[]>> SearchByDepartment;

        void SetGvDepartment(DataTable dt);
    }
}

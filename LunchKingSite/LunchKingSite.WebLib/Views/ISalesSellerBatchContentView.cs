﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    /// <summary>
    /// 商家批次處理介面
    /// </summary>
    public interface ISalesSellerBatchContentView
    {
        #region Propertys
        /// <summary>
        /// 使用者名稱
        /// </summary>
        string UserName { get; }
        /// <summary>
        /// 檔案上傳控制項
        /// </summary>
        FileUpload FUExport { get; }
        /// <summary>
        /// 錯誤清單
        /// </summary>
        List<SellerBatchErrorData> ErrData { get; set; }
        /// <summary>
        /// 一頁幾筆
        /// </summary>
        int PageSize { get; set; }
        #endregion

        #region event
        /// <summary>
        /// 取得筆數
        /// </summary>
        event EventHandler<DataEventArgs<int>> OnGetCount;
        /// <summary>
        /// 換頁
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChanged;
        /// <summary>
        /// 上傳
        /// </summary>
        event EventHandler Upload;
        #endregion
        
        #region method
        /// <summary>
        /// 設定錯誤列表
        /// </summary>
        /// <param name="dataList">錯誤列表資料</param>
        void SetErrorList(List<SellerBatchErrorData> errData);
        /// <summary>
        /// 顯示訊息
        /// </summary>
        /// <param name="msg">訊息</param>
        void ShowMessage(string msg);
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model;
using System.ComponentModel;
using LunchKingSite.BizLogic.Models.BounPoints;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserServiceIntergrateView
    {
        #region property
        UserServiceIntergratePresenter Presenter { get; set; }
        int FilterTypeValue { get; }
        UserServiceIntergrateIDEASFilterType IDEASFilterType { get; }
        UserServiceIntergrateTmallFilterType TmallFilterType { get; }
        /// <summary>
        /// 查詢資策會訂單時使用的條件
        /// </summary>
        string IDEASSearchData { get; }
        /// <summary>
        /// 天貓檔次搜尋條件
        /// </summary>
        string TmallSearchData { get; }
        int UserId { get; }
        string UserData { get; set; }
        string UserDataView { get; set; }
        Dictionary<Subscription, bool> UnsubscribeList { get; }
        Dictionary<HiDealSubscription, bool> UnHidealsubscribeList { get; }
        int PageSize { get; set; }
        string SortExpression { get; }
        string HidealSortExpression { get; }
        MemberPromotionDeposit MemberPromotionDeposit { get; }
        bool IfReceiveEDM { get; set; }
        bool IfReceiveStartedSell { get; set; }
        bool IfReceivePiinlifeEDM { get; set; }
        bool IfReceivePiinlifeStartedSell { get; set; }
        int RefundScashAmount { get; }
        string CreateId { get; }
        bool CBIsFunds { get; }
        bool IsBcashPayByVendor { get; }
        int Fine { get; }
        Guid? DealGuid { get; }
        string GetStatusTypeDropDown { get; }
        Guid OrderGuid { get; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnStatusSelected;
        event EventHandler OnIDEASSearchClicked;
        event EventHandler OnBcashAddClicked;
        event EventHandler OnRefundScashClicked;
        event EventHandler OnTmallSearchClicked;
        event EventHandler<DataEventArgs<ServiceMessage>> OnSetServiceMsgClicked;
        event EventHandler<EventArgs> OnSubscribeUpdateClick;
        event EventHandler<DataEventArgs<int>> OnGetOrderCount;
        event EventHandler<DataEventArgs<int>> OrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetHidealOrderCount;
        event EventHandler<DataEventArgs<int>> HidealOrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetServiceCount;
        event EventHandler<DataEventArgs<int>> ServicePageChanged;
        event EventHandler<DataEventArgs<int>> OnGetNotClosedCount;
        event EventHandler<DataEventArgs<int>> NotClosedPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetClosedCount;
        event EventHandler<DataEventArgs<int>> ClosedPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetLunchKingCount;
        event EventHandler<DataEventArgs<string>> GetBonusListInfo;
        event EventHandler<DataEventArgs<int>> OnGetIDEASOrderCount;
        event EventHandler<DataEventArgs<int>> OnIDEASOrderPageChanged;
        event EventHandler<DataEventArgs<int>> OnGetTmallCount;
        event EventHandler<DataEventArgs<int>> OnTmallPageChanged;

        event EventHandler<DataEventArgs<string>> GetMembershipCardListInfo;
        event EventHandler<DataEventArgs<int>> OnGetMembershipCardCount;
        event EventHandler<DataEventArgs<int>> MembershipCardPageChanged;

        #endregion

        #region method
        void SetUserName(MemberCollection mc);
        void SetMemberData(ViewMemberBuildingCityParentCity m, MobileMember mm);
        void SetMemberAddress(string address);
        void SetMemberLinkList(MemberLinkCollection mlc);
        //void SetMemberBonus(ViewMemberPromotionTransactionCollection vmptc);
        void SetMemberBonus(double promotionvalue);
        void SetMemberScash(decimal scash, decimal scashE7, decimal scashPez, string pezMemNum);
        void SetOrderData(DataTable dt);

        void SetHidealOrderList(ViewHiDealOrderDealOrderReturnedCollection vhodor);

        void SetLunchKingOrderData(DataTable dt);

        void SetPayeasyScashRecords(List<PezScashTransaction> items);

        void SetBonusList(AuditCollection data);
        void SetBonusData(AuditCollection data, Dictionary<ViewMemberPromotionTransaction, string> dataList, ViewMemberCashpointListCollection vmclc, List<UserScashTransactionInfo> vstc, ViewOrderMemberPaymentCollection vompcp, ViewOrderMemberPaymentCollection vompcc, ViewDiscountOrderCollection vdoc);

        void SetMembershipCardData(List<ViewMembershipCardLog> membbershipCardList);
        void SetServiceMessageList(Dictionary<ServiceMessage, Guid> dataList);

        void SetCustomerServiceMessageList(Dictionary<CustomerServiceMessage, Guid> dataList);
        void SetViewMergeOldNewServiceMessageList(Dictionary<ViewMergeOldNewServiceMessage, Guid> dataList);

        void SetSubscriptionList(SubscriptionCollection subscriptionList, HiDealSubscriptionCollection hidealSubscriptionList);

        void SetIDEASOrderList(ViewCompanyUserOrderCollection orderCollection);
        void SetTmallList(ViewOrderCorrespondingSmsLogCollection logCollection);
        void SetStatusList(Dictionary<string, string> list);
        void BuildServiceCategory(ServiceMessageCategoryCollection serviceCates);
        #endregion

    }
    #region enum
    public enum UserServiceIntergrateIDEASFilterType
    {
        /// <summary>
        /// 訂單編號
        /// </summary>
        OrderId = 0,
        /// <summary>
        /// 資策會購買人email
        /// </summary>
        PurchaserEmail = 1,
        /// <summary>
        /// 資策會購買人手機
        /// </summary>
        PurchaserMobile = 2,
        /// <summary>
        /// 資策會購買人姓名
        /// </summary>
        PurchaserName = 3
    }

    public enum UserServiceIntergrateTmallFilterType
    {
        OrderId = 0,
        Mobile = 1,
        CouponSequence = 2
    }

    public enum FilterType
    {
        [Description("EMAIL")]
        UserName = 0,
        [Description("手機")]
        Mobile = 1,
        [Description("UserId")]
        UserId = 2,
        [Description("認證手機")]
        RegisteredMobile = 3
    }
    #endregion enum
}

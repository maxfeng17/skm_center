﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.WebLib.Views
{
    public interface ILastDayDealPponView
    {
        event EventHandler CheckEventMail;

        LastDayDealPponPresenter Presenter { get; set; }
        int CityId { get; set; }
        int DealCityId { get; set; }
        int TravelCategoryId { get; }
        int FemaleCategoryId { get; }
        int SubRegionCategoryId { get; }
        List<int> FilterCategoryIdList { get; }
        List<CategoryNode> DealChannelList { get; set; }
        Guid BusinessHourId { get; set; }
        int BusinessHourUniqueId { get; set; }
        CategorySortType SortType { get; set; }
        int Encores { get; set; }
        string UserName { get; }
        int UserId { get; }
        DeliveryType[] RepresentedDeliverytype { get; } //20120314 前端以憑證宅配撈取
        bool IsDeliveryDeal { set; get; }

        bool TwentyFourHours { get; set; }
        string HamiTicketId { get; }
        int? DealType { get; set; }
        bool EntrustSell { set; }
        string PicAlt { get; set; }
        bool isUseNewMember { get; set; }
        int CategoryID { get; }
        bool isMultipleMainDeals { get; set; }
        bool BypassSsoVerify { get; set; }
        IExpireNotice ExpireNotice { get; }
        bool ShowEventEmail { get; set; }
        string EdmPopUpCacheName { get; set; }
        string CityName { set; }

        /// <summary>
        /// 埋Code
        /// </summary>
        string DealArgs { set; get; }

        bool IsOpenNewWindow { get; set; }
        bool IsMobileBroswer { get; }
        void PopulateDealInfo(ViewPponDeal mainDeal, MemberLinkCollection memberLinks);
        void SetMailInfo(int cityId);
        bool SetCookie(String value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message);
        void NoRobots();    //2010.03.02 增加福利網不被搜尋引擎index meta .Died 
        void SeoSetting(ViewPponDeal d, string appendTitle = ""); //2011.01.04 SEO .Died
        /// <summary>
        /// 設定頁面粉絲團的資訊要顯示哪個
        /// </summary>
        /// <param name="type"></param>
        /// <param name="businessHourGuid">主DEAL的BID</param>
        void SetFaceBookPageShow(Guid businessHourGuid);
        void SetCity(int value);
        void ShowATMIcon();
        void SetHami();
        //多選項檔次
        void RedirectToComboDealMain(Guid? businessHourGuid);
        void SetComboDeals(ViewComboDealCollection combodeals);
        void SetComboDeals(ViewComboDealCollection combodeals, ViewPponDeal mainDeal);
        //新首頁
        void SetMultipleCategories(List<KeyValuePair<CategoryDealCount, List<CategoryDealCount>>> dealCategoryCountList, List<CategoryDealCount> filterItemList, SystemCodeCollection piinlifeLinks);
        void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        void SetMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        //跳窗
        void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair);
        void ShowTravelcardIcon(bool show);
        void RedirectToPiinlife();
        void RedirectToDefault();
    }
}

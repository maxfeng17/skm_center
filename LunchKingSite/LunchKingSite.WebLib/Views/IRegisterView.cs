﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IRegisterView
    {
        event EventHandler Submit;
        event EventHandler MergeTwoMail;

        int UserId { get; }
        string UserName { get; }
        string InputEmail { get; set; }
        ExternalMemberInfo SsoMemberInfo { get; }
        string SelectedEmail { get; }
        string NotSelectedEmail { get; }
        bool IsMergeFromOtherPage { get; }
        string ReferrerSourceId { get; }
        string ReferreriChannelId { get; }

        void ShowMessage(string message);
        /// <summary>
        /// 輸入email的畫面
        /// </summary>
        void ShowInputEmailPage(string predefinedMail);
        /// <summary>
        /// 與輸入EMAIL相同的其他登入選擇
        /// </summary>
        /// <param name="linkingSrc"></param>
        /// <param name="disconnectedLinks"></param>
        void ShowAccountLinkingPage(SingleSignOnSource linkingSrc, IList<SingleSignOnSource> linksToShow);

        void ShowVerifyWithExistingAccountPage(SingleSignOnSource linkingSrc, IList<SingleSignOnSource> linksToShow);

        void RedirectToLogin();

        void StartiChannelReg(MemberLink ml, string ichannelId, string backcode);

        /// <summary>
        /// 串接兩個已存在的帳號，當email不同時，用來選擇的畫面。
        /// </summary>
        /// <param name="mail1"></param>
        /// <param name="mail2"></param>
        void ShowMailSelect(string mail1, string mail2);
        /// <summary>
        /// 顯示錯誤畫面
        /// </summary>
        /// <param name="errMessage"></param>
        void ShowLoginError(string errMessage);
        /// <summary>
        /// 轉址到指定網址
        /// </summary>
        void SendUserBackToPreviousPage();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IPushAppSetupView
    {
        #region property
        PushAppSetupPresenter Presenter { get; set; }
        int PushId { get; set; }
        Guid? BusinessHourGuid { get; set; }
        int? PponCity { get; set; }
        string SellerId { get; set; }
        int? VourcherId { get; set; }
        int? EventPromoId { get; set; }
        int? BrandPromoId { get; set; }
        string CustomUrl { get; set; }
        DateTime SendDate { get; set; }
        DateTime SendDateSearchS { get; }
        DateTime SendDateSearchE { get; }
        int PageSize { get; set; }
        string PushArea { get; set; }
        PushAppType PushType { get; set; }
        int PushTypeSearch { get; }
        string Description { get; set; }
        string DescriptionSearch { get; }
        bool IsEnabledEdit { get; set; }
        bool IsEnabledUpdate { get; set; }

        string UserName { get; }
        string PushToAndroid { get; }
        string PushToIOS { get; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler OnSaveClicked;
        event EventHandler<DataEventArgs<int>> GetPushAppData;
        event EventHandler<DataEventArgs<int>> Push;
        event EventHandler<DataEventArgs<int>> PushAppDelete;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        void SetPushAppList(PushAppCollection deptList);
        void ShowMessage(string msg);
        void ShowPushList();
        #endregion

    }
}

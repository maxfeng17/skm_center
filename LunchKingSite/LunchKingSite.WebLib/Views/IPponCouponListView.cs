﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponCouponListView
    {
        #region property
        PponCouponListPresenter Presenter { get; set; }
        int UserId { get; }
        string UserName { get; }
        string UserEmail { get; }
        bool IsLatest { get; }
        CouponListFilterType FilterType { get; }
        NewCouponListFilterType NewFilterType { get; }
        MemberOrderMainFilterType MainFilterType { get; }
        int PageCount { get; set; }
        int Department { get; }
        int OrderStatus { get; }
        int GroupStatus { get; }
        int CancelStatus { get; }
        int BusinessHourStatus { get; }
        int CurrentQuantity { get; set; }
        DateTime ExpiredTime { get; }
        DateTime OrderEndDate { get; }
        Guid OrderGuid { get; }
        int Slug { get; }
        int OrderMin { get; }
        int ItemPrice { get; }
        int AccBusinessGroupId { get; set; }
        string OrderId { get; set; }
        ThirdPartyPayment ThirdPartyPaymentSystem { set; get; }

        #endregion

        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler FilterChange;
        event EventHandler<DataEventArgs<Guid>> ServiceAsk;
        #endregion

        #region method
        void InitFilterDropDwonList();
        void GetCouponList(ViewCouponListMainCollection data);
        void SetUpPageCount();
        void SetUserMobileNumber();
        void ShowAlert(string message);
        #endregion
    }
    public class SMSQuery
    {
        public string CouponId { get; set; }
        public string Bid { get; set; }
        public string Mobile { get; set; }
        public string OrderGuid { get; set; }

        public SMSQuery(string couponid, string bid, string mobile)
        {
            CouponId = couponid;
            Bid = bid;
            Mobile = mobile;
        }
        public SMSQuery(string couponid, string bid, string mobile, string orderGuid)
        {
            CouponId = couponid;
            Bid = bid;
            Mobile = mobile;
            OrderGuid = orderGuid;
        }
    }
    public class EinvoiceRequestQuery
    {
        public string EinvoiceNumber { get; set; }
        public string Receiver { get; set; }
        public string Address { get; set; }
        public EinvoiceRequestQuery(string einvoicenumber, string receiver, string address)
        {
            EinvoiceNumber = einvoicenumber;
            Receiver = receiver;
            Address = address;
        }
    }

    public class MultipleEinvoiceRequestQuery
    {
        public Guid OrderGuid { get; set; }
        public string Receiver { get; set; }
        public string Address { get; set; }
        public MultipleEinvoiceRequestQuery(Guid orderGuid, string receiver, string address)
        {
            OrderGuid = orderGuid;
            Receiver = receiver;
            Address = address;
        }
    }

    public class RefundLog
    {
        public string RequestTime { get; set; }
        public string Sequence { get; set; }
        public int Pcash { get; set; }
        public int Bcash { get; set; }
        public int Scash { get; set; }
        public int Ccash { get; set; }
        public int Atm { get; set; }
        public string Message { get; set; }
        public bool ShowCash { get; set; }
        public RefundLog(string requesttime, string sequence, int pcash, int bcash, int scash, int ccash, int atm, string message, bool showcash)
        {
            RequestTime = requesttime;
            Sequence = sequence;
            Pcash = pcash;
            Bcash = bcash;
            Scash = scash;
            Ccash = ccash;
            Atm = atm;
            Message = message;
            ShowCash = showcash;
        }
    }

    public class ReturnFormLog
    {
        public string RequestTime { get; set; }
        public string RequestedItems { get; set; }
        public string ReturnedItems { get; set; }
        public int Pcash { get; set; }
        public int Bcash { get; set; }
        public int Scash { get; set; }
        public int Pscash { get; set; }
        public int Ccash { get; set; }
        public int Atm { get; set; }
        public int TCash { get; set; }
        public string Message { get; set; }
        public bool ShowCash { get; set; }
        public bool IsCreditNoteReceived { get; set; }

        public ReturnFormLog(string requesttime, string requestedItems, string returnedItems,
            int pcash, int bcash, int scash, int pscash, int ccash, int atm, int tcash, string message, bool showcash, bool isCreditNoteReceived)
        {
            RequestTime = requesttime;
            RequestedItems = requestedItems;
            ReturnedItems = returnedItems;
            Pcash = pcash;
            Bcash = bcash;
            Scash = scash;
            Ccash = ccash;
            Pscash = pscash;
            Atm = atm;
            Message = message;
            ShowCash = showcash;
            TCash = tcash;
            IsCreditNoteReceived = isCreditNoteReceived;
        }
    }
}

using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IOrderListView
    {
        event EventHandler<DataEventArgs<int>> GetOrderCount;
        event EventHandler SortClicked;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SearchClicked;
        
        OrderListPresenter Presenter { get; }
        string SortExpression { get; set; }
        string FilterUser { get; set; }
        string[] FilterInternal { get;}
        string FilterType { get; set; }   
        int PageSize { get; set; }
        int CurrentPage { get; }
        string DateType { get; }
        string ShipType { get; }
        string CsvType { get; }
        void SetOrderList(ViewOrderMemberBuildingSellerCollection orders);
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        string OrderByInternal { get;}
        /*
        bool ExcludeFamilyMart { get; }
        bool PaymentFreeze { get; }
        */
        DateTime? MinOrderCreateTime { get; }
        DateTime? MaxOrderCreateTime { get; }
        string SearchExpression { get; }
        string GetFilterTypeDropDown();
    }
}

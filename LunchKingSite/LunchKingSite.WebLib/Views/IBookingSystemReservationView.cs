﻿using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IBookingSystemReservationView
    {
        #region property

        string ApiKey { get; set; }
        string SellerName { set; }
        string FullBookingDate { set; }
        Guid? StoreGuid { get; }

        #endregion

        #region method
        void SetStoreList(ViewBookingSystemStoreCollection stores);
        #endregion

        #region event
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IEntrustSellOrderListView
    {
        EntrustSellOrderListPresenter Presenter { get; set; }
        string FilterOrderStatus { get; }
        string FilterColumn { get; }
        string FilterValue { get; }
        DateTime? FilterStartTime { get; }
        DateTime? FilterEndTime { get; }

        event EventHandler SearchClicked;
        event EventHandler ExportToExcel;

        void Export(ViewEntrustsellOrderCollection orders);
        void SetOrderList(ViewEntrustsellOrderCollection orders);
    }
}

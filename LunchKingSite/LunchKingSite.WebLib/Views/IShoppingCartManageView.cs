﻿using System;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Data;
using LunchKingSite.Core;
using System.Collections;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IShoppingCartManageView
    {
        #region property
        ShoppingCartManagePresenter Presenter { get; set; }
        string SearchName { get; }
        string UserName { get; }
        #endregion

        #region method
        void SetShoppintCartData(List<ShoppingCartManageList> pros);
        #endregion

        #region event
        event EventHandler Search;
        #endregion
    }
}

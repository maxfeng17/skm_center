﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IAccessoryCategoryView
    {
        event EventHandler<DataEventArgs<Guid>> SelectAccessory;
        event EventHandler<DataEventArgs<AccessoryCategory>> SetAccessoryCategory;
        event EventHandler<DataEventArgs<Guid>> DeleteAccessoryCategory;
        event EventHandler<DataEventArgs<Accessory>> SetAccessory;
        event EventHandler<DataEventArgs<Guid>> DeleteAccessory;


        AccessoryCategoryPresenter Presenter { get; set;}

        Guid AccessoryCategoryGuid { get; }

        void SetAccessoryCategoryList(AccessoryCategoryCollection AccessoryCategoryList);
        void SetAccessoryList(AccessoryCollection AccessoryList);
        void SetAccessoryGroupMemberList(bool CanDelete);
    }
}

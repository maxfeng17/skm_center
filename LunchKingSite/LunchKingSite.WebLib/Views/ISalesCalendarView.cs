﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesCalendarView
    {
        string UserName { get; }
        void SetCalendarContent(SalesCalendarEventCollection cals, int TotalCount);
        int CurrentPage { get; }

        #region event
        event EventHandler<EventArgs> ChangePager;
        #endregion
    }
}

﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace LunchKingSite.WebLib.Views
{
    public interface IPicSetupView
    {
        #region property
        PicSetupPresenter Presenter { get; set; }
        Guid BusinessHourGuid { get; }
        bool IsPrintWatermark { get; }
        string ImagePath { get; }
        string SpecialImagePath { get; }
        string TravelEdmSpecialImagePath { get; }
        List<HttpPostedFile> PostedFiles { get; }
        PponSetupImageUploadType SelectImageUploadType { get; }
        #endregion

        #region event
        event EventHandler SavePic;
        event EventHandler GetCouponEventContent;
        #endregion

        #region method
        void GetCouponEventContentInfo(CouponEventContent content, string message);
        #endregion
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesProductionBusinessListView
    {

        #region property
        string UserName { get; }
        int CurrentPage { get; }
        int PageSize { get; set; }
        string City { get;}
        DateTime DateStart { get; }
        DateTime DateEnd { get; }
        int DateType { get; }
        int StatusType { get; }
        int Status { get; }
        string Emp { get; }
        string OrderField { get; }
        string OrderBy { get; }
        bool ChkProduction { get; }
        int DealProperty { get;}
        int PicType { get; }
        string AssignPro { get; }
        string AssignFlag { get; }
        int DealType { get;}
        int DealSubType { get; }
        bool MarketingResource { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        void SetBusinessList(Dictionary<ViewProposalSeller, ProposalAssignLogCollection> dataList);
        void ShowMessage(string msg);
        #endregion
    }
}

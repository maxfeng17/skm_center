﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Model;
namespace LunchKingSite.WebLib.Views
{
    public interface ISkmPrizeWinnerCheckView
    {

        SkmPrizeWinnerCheckPresenter Presenter { get; set; }
        int UserId { get; }
        string UserName { get; }

        void RedirectToLogin();

        void ShowEditContent(bool isShowError =false);
        void ShowSuccessContent(string prizeEmail);
        void ShowNameError(string errorMsg);
        void ShowIDNumberError(string errorMsg);

        event EventHandler<DataEventArgs<SkmPrizeWinnerData>> SendWinnerInfo;

    }

    public class SkmPrizeWinnerData
    {
        public string PrizeName { get; set;}
        public string PrizeIDNumber { get; set; }
        public string PrizeEmail { get; set; }
    }
}

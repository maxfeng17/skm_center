﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IRefundForm
    {
        RefundFormPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        Guid OrderGuid { get; }
        void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, bool isleagl, string message);
    }
    public class RefundFormListClass
    {
        public int Id { get; set; }
        public string OrderId { get; set; }
        public string CouponSequence { get; set; }
        public int EinvoiceMode { get; set; }
        public DateTime EinvoiceDate { get; set; }                  //若EInvoice_Main 的InvoiceNumberTime 是Null, 則取OrderTime
        public string EinvoiceNumberDate_Year { get; set; }         
        public string EinvoiceNumberDate_Month { get; set; }
        public string EinvoiceNumberDate_Day { get; set; }
        public string EinvoiceNumber { get; set; }
        public string EinvoiceItemName { get; set; }
        public int ItemAmount { get; set; }
        public int ItemNoTaxAmount { get; set; }
        public int ItemTax { get; set; }
        public bool IsTax { get; set; }
        public bool IsProduct { get; set; }
        public string breakstring { get; set; }
        
        public RefundFormListClass(int id, string orderid, string couponsequence,
            int einvoicemode, DateTime einvoicedate, bool isInvoiceNumberDateNull, string einvoicenumber, string einvoiceitemname,
            int itemamount, int itemnotaxamount, int itemtax, bool istax, bool isproduct)
        {
            Id = id;
            OrderId = orderid;
            CouponSequence = couponsequence;
            EinvoiceMode = einvoicemode;
            EinvoiceDate = einvoicedate;
            EinvoiceNumberDate_Year = isInvoiceNumberDateNull ? string.Empty : einvoicedate.Year.ToString();
            EinvoiceNumberDate_Month = isInvoiceNumberDateNull ? string.Empty : einvoicedate.Month.ToString();
            EinvoiceNumberDate_Day = isInvoiceNumberDateNull ? string.Empty : einvoicedate.Day.ToString();
            EinvoiceNumber = einvoicenumber;
            EinvoiceItemName = einvoiceitemname;
            ItemAmount = itemamount;
            ItemNoTaxAmount = itemnotaxamount;
            ItemTax = itemtax;
            IsTax = istax;
            IsProduct = isproduct;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IExhibition
    {
        #region property
        string Url { get; }
        string Preview { get; }
        int UserId { get; }
        int EventId { get; set; }
        string Rsrc { get; set; }
        string EventTitle { get; set; }
        string OgTitle { get; set; }
        string OgUrl { set; get; }
        string OgDescription { set; get; }
        string OgImage { set; get; }
        string LinkCanonicalUrl { set; get; }
        string LinkImageSrc { set; get; }
        string SeoKeyword { set; get; }
        string SeoDescription { set; get; }
        LunchKingSite.Core.EventPromoTemplateType TemplateType { set; get; }
        bool EnableMobileEventPromo { set; get; }
        bool IsCuration { set; get; }
        #endregion
        #region method
        void SetCategory(EventPromoItemCollection list);
        void SetAllCategory(EventPromo eventPromo, List<ViewEventPromo> dataList);
        void ShowEventPromo(EventPromo eventPromo, List<ViewEventPromo> list);
        void ShowVourcherEventPromo(Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> dataList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> lotteryList, Dictionary<EventPromoItem, KeyValuePair<VourcherEvent, bool>> storeList);
        void ShowPiinlifeEventPromo(List<ViewEventPromo> dataList);
        void ShowPiinlifeEventPromo(EventPromo eventPromo, List<ViewEventPromo> dataList);
        void ShowEventExpire();
        #endregion
    }
}

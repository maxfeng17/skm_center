﻿using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.Core.Enumeration;
using Seller = LunchKingSite.DataOrm.Seller;

namespace LunchKingSite.WebLib.Views
{
    public interface ISellerAddView
    {
        event EventHandler<DataEventArgs<Seller>> UpdateSellerInfo;
        
        event EventHandler<DataEventArgs<Dictionary<int, bool>>> UpdateSellerCategory;

        event EventHandler<DataEventArgs<Guid>> DeleteAccessoryGroup;

        event EventHandler<DataEventArgs<string>> ImageListChanged;

        event EventHandler<DataEventArgs<PhotoInfo>> UpdatePhoto;

        event EventHandler SelectCityChanged;

        event EventHandler CreateSellerID;

        event EventHandler<DataEventArgs<string>> ImportBusinessOrderInfo;

        event EventHandler SellerDetailSave;

        void GoToSellerStoreList();

        void ShowMessage(string message);
        void CopySellerLink(Guid copySellerGuid);

        event EventHandler ImportIntroduct;

        event EventHandler<DataEventArgs<int>> DealPagerGetCount;

        event EventHandler<DataEventArgs<int>> DealPagerChanged;
        
        event EventHandler<DataEventArgs<SellerTempStatus>> SellerApprove;

        event EventHandler CopySeller;
        
        SellerAddPresenter Presenter { get; set; }

        Guid SellerGuid { get; }

        string UserName { get; }

        int Tc { get; }

        Guid CBizHourGuid { get; set; }

        int SelectCity { get; }

        int SelectZone { get; }

        string SellerID { get; }

        Dictionary<int, bool> SellerCategoryList { get; }

        string Filter { get; set; }
        
        string SellerIntroduct { get; set; }

        string ImagePath { get; }

        HttpPostedFile FileUpload { get; }

        int PageSizeOfPpon { get; }

        KeyValuePair<string, string> LongitudeLatitude { set; get; }

        string BusinessOrderId { set; get; }
        
        void SetTabPanel(int panelNumber);

        void SetZoneDropDown(Dictionary<int, string> zone);

        void SetCityDropDown(List<LunchKingSite.DataOrm.City> city);
		
        void SetSellerID(string sellerID);

        void SetSellerGet(Seller sellerData, IEnumerable<Audit> auditLogs, List<KeyValuePair<Guid, string>> storeLists);
        
        void SetPponList(ViewPponDealCollection orders);
        
        void SetImageGrid(string rawDataPath);

        void SetImageFile(PhotoInfo pi);

        void SetSellerImageDetail(object images);

        void SetBusinessOrderInfo(BusinessOrder businessOrder);

        void ReloadSeller();

        /// <summary>
        /// 店鋪鄰近捷運站分類List
        /// </summary>
        List<int> MRTLocationCategory { get; set; }
        List<KeyValuePair<string, int>> MRTLocationChecked { get; set; }

        KeyValuePair<string, string> ParentSellerInfo { get; set; }
        Guid? ParentSellerGuid { get; set; }
    }

    public sealed class PhotoInfo
    {
        private HttpPostedFile _pFile;

        public HttpPostedFile PFile
        {
            get
            {
                return _pFile;
            }
            set
            {
                _pFile = value;
            }
        }

        private UploadFileType _type;

        public UploadFileType Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        private string _destFilePath;

        public string DestFilePath
        {
            get
            {
                return _destFilePath;
            }
            set
            {
                _destFilePath = value;
            }
        }

        private string _destFileName;

        public string DestFileName
        {
            get
            {
                return _destFileName;
            }
            set
            {
                _destFileName = value;
            }
        }
    }
}
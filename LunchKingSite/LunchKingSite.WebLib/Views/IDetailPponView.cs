﻿using System;
using System.Collections.Generic;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.WebLib.UI;

namespace LunchKingSite.WebLib.Views
{
    public interface IDetailPponView
    {
        #region Event
        event EventHandler CheckEventMail;
        #endregion

        #region Property
        DetailPponPresenter Presenter { get; set; }
        Guid BusinessHourId { get; set; }
        int BusinessHourUniqueId { get; set; }
        string MicroDataJson { get; set; }
        string ScupioDataJson { get; set; }
        string ScupioCartPageDataJson { get; set; }
        string GtagDataJson { get; set; }
        string YahooDataJson { get; set; }
        CategorySortType SortType { get; set; }
        int Encores { get; set; }
        string UserName { get; }
        int UserId { get; }
        DeliveryType[] RepresentedDeliverytype { get; } //20120314 前端以憑證宅配撈取
        bool IsDeliveryDeal { set; get; }

        bool TwentyFourHours { get; set; }
        string HamiTicketId { get; }
        int? DealType { get; set; }
        bool EntrustSell { set; }
        string PicAlt { get; set; }
        bool isUseNewMember { get; set; }
        bool isMultipleMainDeals { get; set; }
        bool BypassSsoVerify { get; set; }
        IExpireNotice ExpireNotice { get; }
        bool ShowEventEmail { get; set; }
        string EdmPopUpCacheName { get; set; }
        bool IsHiddenBanner { set; }
        /// <summary>
        /// 埋Code
        /// </summary>
        string DealArgs { set; get; }
        bool IsOpenNewWindow { get; set; }
        bool IsMobileBroswer { get; }

        #region city, category相關

        int CityId { get; set; }
        int CategoryID { get; }
        int DealCityId { get; set; }
        int TravelCategoryId { get; }
        int FemaleCategoryId { get; }

        PponDealEvaluateStar EvaluateAverage { get; set; }


        List<CategoryNode> DealChannelList { get; set; }

        /// <summary>
        /// 當前頁面所選城市的Id
        /// </summary>
        int SelectedCityId { get; }
        /// <summary>
        /// 當前頁面所選頻道的Id
        /// </summary>
        int SelectedChannelId { get; }
        /// <summary>
        /// 當前頁面所選地區的Id
        /// </summary>
        int SelectedRegionId { get; }
        #endregion city, category相關

        PcwebMainDealModel Model { get; }
        string ExpireRedirectDisplay { get; set; }
        string ExpireRedirectUrl { get; set; }
        bool? UseExpireRedirectUrlAsCanonical { get; set; }
        /// <summary>
        /// 跳過快取，強制從資料庫載入
        /// </summary>
        bool ForceReload { get; }

        #endregion

        #region Method
        void PopulateDealInfo(IViewPponDeal mainDeal, MemberLinkCollection memberLinks, Dictionary<Guid, string> relatedDeals);
        bool SetCookie(string value, DateTime expireTime, LkSiteCookie cookieField);
        void ShowMessage(string message);
        void NoRobots();    //2010.03.02 增加福利網不被搜尋引擎index meta .Died 
        void SeoSetting(IViewPponDeal d);
        void ShowATMIcon();
        void SetHami();
        //多選項檔次
        void RedirectToComboDealMain(Guid? mainBid, Guid bid);
        void SetProductEntries(IList<ViewComboDeal> combodeals, IViewPponDeal mainDeal);

        //新首頁
        void SetHotSaleMutilpMainDeals(List<MultipleMainDealPreview> multiplemaindeals);
        void SetToDayMutilpMainDeals(List<MultipleMainDealPreview> todaymultiplemaindeals);
        void SetLastDayMutilpMainDeals(List<MultipleMainDealPreview> lastdaymultiplemaindeals);

        //跳窗
        void SetEventMainActivities(KeyValuePair<EventActivity, CmsRandomCityCollection> pair);
        void RedirectToPiinlife();
        void RedirectToDefault();
        void RedirectNoShowToDefault(string nsUrl);
        void RedirectToGameDetail(Guid bid);
        #endregion

    }

    public class PponDealProductEntry
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        public string Bid { get; set; }
        public string Title { get; set; }
        public string Freight { get; set; }
        public string Discount { get; set; }
        public string Price { get; set; }
        public string OrigPrice { get; set; }
        public string AveragePriceTag { get; set; }
        public string AveragePrice { get; set; }
        public bool IsSoldOut { get; set; }
        public bool IsEnableIsp { get; set; }
        public bool IsExpiredDeal { get; set; }
        public string BuyUrl { get; set; }

        public static List<PponDealProductEntry> ConvertFrom(IViewPponDeal theDeal)
        {
            List<PponDealProductEntry> result = new List<PponDealProductEntry>();
            bool isSoldOut = DateTime.Now >= theDeal.BusinessHourOrderTimeE || theDeal.OrderedQuantity >= theDeal.OrderTotalLimit.GetValueOrDefault();
            var entry = new PponDealProductEntry
            {
                Bid = theDeal.BusinessHourGuid.ToString(),
                Title = theDeal.ItemName,
                Discount = theDeal.ItemOrigPrice > 0
                    ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(theDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                        theDeal.ItemPrice, theDeal.ItemOrigPrice, false)
                    : string.Empty,
                OrigPrice = theDeal.ItemOrigPrice.ToString("N0"),
                Freight = GetDealFreight(theDeal),
                Price = ChecksubComboDealItemPrice(theDeal),
                AveragePriceTag = GetAveragePriceTag(theDeal),
                AveragePrice = GetAveragePriceLab(theDeal),
                IsSoldOut = isSoldOut,
                IsEnableIsp = theDeal.EnableIsp,
                IsExpiredDeal = theDeal.IsExpiredDeal.GetValueOrDefault(false),
                BuyUrl = Helper.CombineUrl(config.SSLSiteUrl, "ppon/buy.aspx?bid=" + theDeal.BusinessHourGuid)
            };
            entry.IsEnableIsp = theDeal.EnableIsp;
            result.Add(entry);
            return result;
        }

        public static List<PponDealProductEntry> ConvertFrom(IList<ViewComboDeal> vcdCol)
        {
            List<PponDealProductEntry> result = new List<PponDealProductEntry>();
            if (vcdCol != null)
            {
                foreach (var vcd in vcdCol)
                {
                    bool isSoldOut = DateTime.Now >= vcd.BusinessHourOrderTimeE || vcd.OrderedQuantity >= vcd.OrderTotalLimit.GetValueOrDefault();
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid);
                    PponDealProductEntry entry = new PponDealProductEntry
                    {
                        Bid = vcd.BusinessHourGuid.ToString(),
                        Title = vcd.Title,
                        Discount = vcd.ItemOrigPrice > 0
                            ? ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(vcd.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                                vcd.ItemPrice, vcd.ItemOrigPrice, false)
                            : string.Empty,
                        OrigPrice = vcd.ItemOrigPrice.ToString("N0"),
                        AveragePriceTag = GetAveragePriceTag(vcd),
                        AveragePrice = GetAveragePriceLab(vcd),
                        Freight = GetDealFreight(vcd),
                        Price = ChecksubComboDealItemPrice(vcd),
                        IsSoldOut = isSoldOut,
                        IsEnableIsp = vcd.EnableIsp,
                        IsExpiredDeal = deal.IsExpiredDeal.GetValueOrDefault(false),
                        BuyUrl = Helper.CombineUrl(config.SSLSiteUrl, "ppon/buy.aspx?bid=" + deal.BusinessHourGuid)
                    };
                    entry.IsEnableIsp = vcd.EnableIsp;

                    result.Add(entry);
                }
            }
            return result;
        }

        private static string GetAveragePriceTag(ViewComboDeal deal)
        {
            return IsShowAveragePriceInfo(deal) ? "均價" : string.Empty;
        }
        private static string GetAveragePriceTag(IViewPponDeal theDeal)
        {
            return IsShowAveragePriceInfo(theDeal) ? "均價" : string.Empty;
        }

        private static bool IsShowAveragePriceInfo(ViewComboDeal deal)
        {
            return (deal.IsAveragePrice && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }
        private static bool IsShowAveragePriceInfo(IViewPponDeal deal)
        {
            return ((deal.IsAveragePrice ?? false) && deal.QuantityMultiplier != null && deal.QuantityMultiplier >= 1);
        }
        private static string GetAveragePriceLab(ViewComboDeal deal)
        {
            var averagePrice = CheckZeroPriceToShowAveragePrice(deal);
            var isGroupCouponB = deal.GroupCouponDealType == (int)GroupCouponDealType.CostAssign;
            return (IsShowAveragePriceInfo(deal) && averagePrice > 0) ? "$" + averagePrice.ToString("F0") + (isGroupCouponB ? "起" : string.Empty) : string.Empty;
        }
        private static string GetAveragePriceLab(IViewPponDeal theDeal)
        {
            var averagePrice = CheckZeroPriceToShowAveragePrice(theDeal);
            var isGroupCouponB = theDeal.GroupCouponDealType == (int)GroupCouponDealType.CostAssign;
            return (IsShowAveragePriceInfo(theDeal) && averagePrice > 0) ? "$" + averagePrice.ToString("F0") + (isGroupCouponB ? "起" : string.Empty) : string.Empty;
        }
        private static decimal CheckZeroPriceToShowAveragePrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }
        private static decimal CheckZeroPriceToShowAveragePrice(IViewPponDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                return deal.ItemPrice;
            }
            else
            {
                if (IsShowAveragePriceInfo(deal))
                {
                    return Math.Ceiling(deal.ItemPrice / deal.QuantityMultiplier.Value);
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
        }
        private static string GetDealFreight(ViewComboDeal vcd)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid, true);
            return GetDealFreight(deal);
        }
        private static string GetDealFreight(IViewPponDeal deal)
        {
            if (deal == null || deal.FreightAmount == 0)
            {
                return string.Empty;
            }
            return string.Format("| 運費 ${0}", (int)deal.FreightAmount);
        }
        /// <summary>
        /// 多檔次均價版型售價顯示
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private static string ChecksubComboDealItemPrice(ViewComboDeal deal)
        {
            if (deal.BusinessHourOrderTimeE > DateTime.Now)
            {
                if (deal.OrderedQuantity < (deal.OrderTotalLimit ?? 0))
                {
                    return "$" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
                else
                {
                    return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
            }
            else
            {
                return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                ;
            }
        }
        private static string ChecksubComboDealItemPrice(IViewPponDeal deal)
        {
            if (deal.BusinessHourOrderTimeE > DateTime.Now)
            {
                if (deal.OrderedQuantity < (deal.OrderTotalLimit ?? 0))
                {
                    return "$" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
                else
                {
                    return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                }
            }
            else
            {
                return "售完 $" + CheckZeroPriceToShowPrice(deal).ToString("F0");
                ;
            }
        }
        private static decimal CheckZeroPriceToShowPrice(IViewPponDeal deal)
        {
            //若是全家檔次須將CityId改成全家CityId
            if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0)
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal, PponCityGroup.DefaultPponCityGroup.Family.CityId);
            }
            else
            {
                return PponFacade.CheckZeroPriceToShowPrice(deal);
            }
        }

        private static decimal CheckZeroPriceToShowPrice(ViewComboDeal deal)
        {
            if (decimal.Equals(0, deal.ItemPrice))
            {
                if ((deal.BusinessHourStatus & (int)BusinessHourStatus.PriceZeorShowOriginalPrice) > 0)
                {
                    return deal.ItemOrigPrice;
                }
                else
                {
                    return deal.ItemPrice;
                }
            }
            else
            {
                return deal.ItemPrice;
            }
        }

    }

    public class PcwebMainDealModel
    {
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        public PcwebMainDealModel()
        {
            EventTitle = string.Empty;
            ItemName = string.Empty;
            CityName = string.Empty;
            PicAlt = string.Empty;
            MainPics = new List<string>();
            MainPicBadgeHtml = string.Empty;
            MainPicSitckerHtml = string.Empty;
            MainPicActiveLogoHtml = string.Empty;
            BuyBtnHtml = string.Empty;
            CountdownText = "限時優惠中";
            ItemPriceLabel = "好康價";
            ItemPriceFrom = "起";
            DiscountString = string.Empty;
            IsKindDeal = false;
        }
        public string _eventTitle;
        public string EventTitle
        {
            get
            {
                return HttpUtility.HtmlEncode(_eventTitle);
            }
            set
            {
                _eventTitle = value;
            }
        }
        private string _itemName;
        public string ItemName
        {
            get
            {
                return HttpUtility.HtmlEncode(_itemName);
            }
            set
            {
                _itemName = value;
            }
        }

        private string _cityName;
        public string CityName
        {
            get
            {
                return HttpUtility.HtmlEncode(_cityName);
            }
            set
            {
                _cityName = value;
            }
        }

        public List<string> MainPics { get; set; }

        public string MainPicBadgeHtml { get; set; }
        public string MainPicSitckerHtml { get; set; }
        public string MainPicActiveLogoHtml { get; set; }


        private string _picAlt;
        public string PicAlt
        {
            get
            {
                return HttpUtility.HtmlEncode(_picAlt);
            }
            set
            {
                _picAlt = value;
            }
        }

        private string _tagHtml;
        public string TagHtml
        {
            get
            {
                //回傳的內容是html，不用特意做HtmlEncode
                return _tagHtml;
            }
            set
            {
                _tagHtml = value;
            }
        }
        private IViewPponDeal _mainDeal;

        public IViewPponDeal MainDeal
        {
            get
            {
                if (_mainDeal == null)
                {
                    _mainDeal = new ViewPponDeal();
                }
                return _mainDeal;
            }
            set
            {
                _mainDeal = value;
                if (_mainDeal.BusinessHourGuid != Guid.Empty)
                {
                    InitStarRating(_mainDeal.BusinessHourGuid);
                    InitPrice(_mainDeal);
                    InitCountdown(_mainDeal);

                    IsKindDeal = Helper.IsFlagSet(_mainDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                }
            }
        }

        private void InitCountdown(IViewPponDeal deal)
        {
            TimeSpan ts = (deal.ChangedExpireDate ?? deal.BusinessHourOrderTimeE) - DateTime.Now;
            if (ts.TotalSeconds < 0)
            {
                this.Countdown = null;
                this.CountdownText = "已結束販售!";
            }
            else if (ts.Days >= 3)
            {
                this.Countdown = null;
                this.CountdownText = "限時優惠中!";
            }
            else
            {
                this.Countdown = (int)ts.TotalSeconds * 1000;
                this.CountdownText = string.Empty;
            }
        }

        private void InitPrice(IViewPponDeal deal)
        {
            ItemOrigPrice = deal.ItemOrigPrice;
            ItemPrice = PponFacade.CheckZeroPriceToShowPrice(deal);
            DiscountPrice = deal.DiscountPrice;
            if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
            {
                ItemPriceFrom = "起";
            }
            else
            {
                ItemPriceFrom = string.Empty;
            }
            if ((deal.GroupOrderStatus & (int)GroupOrderStatus.FamiDeal) > 0 && deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
            {
                ItemPriceLabel = "全家兌換價";
            }
            else
            {
                ItemPriceLabel = "好康價";
            }
            DiscountString = ViewPponDealManager.GetDealDiscountStringLite(deal.ItemPrice, deal.ItemOrigPrice, deal.ExchangePrice);
            if (string.IsNullOrEmpty(DiscountString) == false)
            {
                DiscountString += "折";
            }
        }

        public decimal RatingScore { get; set; }
        public int RatingNumber { get; set; }
        public bool RatingShow { get; set; }
        /// <summary>
        /// 購買按鈕的Html
        /// </summary>
        public string BuyBtnHtml { get; set; }

        /// <summary>
        /// 原價
        /// </summary>
        public decimal ItemOrigPrice { get; set; }
        /// <summary>
        /// 售價(均價)
        /// </summary>
        public decimal ItemPrice { get; set; }
        public string ItemPriceLabel { get; set; }
        public string ItemPriceFrom { get; set; }
        /// <summary>
        /// 抵折扣券後價
        /// </summary>
        public decimal? DiscountPrice { get; set; }
        /// <summary>
        /// 頁面倒數 ticks , null 不倒數，維持顯示 CountdownText , 0 倒數完
        /// </summary>
        public int? Countdown { get; set; }
        public string CountdownText { get; set; }

        public string DiscountString { get; set; }
        /// <summary>
        /// 是否為公益檔
        /// </summary>
        public bool IsKindDeal { get; set; }

        private void InitStarRating(Guid bid)
        {
            decimal ratingScore;
            int ratingNumber;
            bool ratingShow;
            PponFacade.GetDealStarRating(
                bid, "pc",
                out ratingScore, out ratingNumber, out ratingShow);

            RatingScore = ratingScore;
            RatingNumber = ratingNumber;
            RatingShow = ratingShow;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesSellerListView
    {
        #region property
        SalesSellerListPresenter Presenter { get; set; }
        string UserName { get; }
        string BossName { get; }
        string SellerName { get; }
        string CompanyName { get; }
        string SignCompanyId { get; }
        int? SalesId { get; }
        SellerTempStatus? TempStaus { get; }
        int RowCount { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        /// <summary>
        /// [搜尋條件]自己的
        /// </summary>
        bool IsPrivate { get; }
        /// <summary>
        /// [搜尋條件]公池
        /// </summary>
        bool IsPublic { get; }
        int EmpUserId { get; }
        string CrossDeptTeam { get; }
        string Dept { get; }
        int CityId { get; }
        string SellerLevelList { get; }
        string SellerPorperty { get; }
        int SellerManagLogNumber { get; }
        string SellerManagLogDatepart { get; }
        string ExportSellerList { get; }
        FileUpload FUExport { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler Export;
        event EventHandler Import;
        #endregion

        #region method
        void SetSellerList(List<Seller> dataList);
        void ShowMessage(string msg);
        #endregion
    }
}

﻿using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IUserBounsListPponView
    {
        event EventHandler GoBackToListClicked;
        event EventHandler UnbindTaishinPay;

        event EventHandler<DataEventArgs<int>> PageChange;

        UserBonusListPponPresenter Presenter { get; set; }

        Dictionary<BounsListType, int> PageCount { get; set; }

        Dictionary<BounsListType, int> CurrentPage { get; set; }

        Dictionary<BounsListType, int> PageSize { get; set; }

        string UserName { get; }

        int UserId { get; }

        void TransferToPponDealPage(string bid);

        void PopulateGeneralScashRecords(List<UserScashTransactionInfo> records);

        void Populate_rro2(ViewMemberPromotionTransactionCollection records_2);
        
        void Populate_SumValue(decimal scash, decimal scashE7, decimal scashPez, decimal userTotalBonusPoint, 
            decimal userTaishinPayCashPoint, bool isTaishinPayLinked);
    }

    public enum MyStatus
    {
        Orders = 0,
        OrderDetail,
        MySetup,
        MyRedeem,
    }
}
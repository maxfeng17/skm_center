﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IBackendRecentDeals
    {
        DateTime? BusinessOrderTimeS { get; }
        DateTime? BusinessOrderTimeE { get; }
        DateTime? BusinessDeliverTimeS { get; }
        DateTime? BusinessDeliverTimeE { get; }
        ChannelType Channel { get; }
        int CategoryId { get; }
        int? Dealtype1 { get; }
        int? Dealtype2 { get; }
        bool IsCloseDeal { get; }
        bool IsOnDeal { get; }
        string SellerName { get; }
        int? SalesId { get; }
        Guid SellerGuid { get; }
        string DealName { get; }
        int CurrentPage { get;}
        int PageSize { get;}
        string OrderField { get; }
        string OrderBy { get; }
        string UserName { get; }
        ViewEmployee Emp { get; set; }
        int UniqueId { get; }
        int Wms { get; }
        string BusinessHourGuid { get; }
        string CrossDeptTeam { get; }

        event EventHandler SearchClicked;
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler<DataEventArgs<int>> GetCount;

        void SetSelectableZone(List<PponCity> cities);
        void SetPponDeals(Dictionary<ViewPponDealCalendar, List<ViewPponDealCalendar>> deallist);
        void SetPiinLifeDeals(ViewHiDealSellerProductCollection recentdeallist);
        void SetDealType(int parentId, int codeId, List<SystemCode> dealTypes);
    }

    public enum ChannelType
    {
        /// <summary>
        /// P好康
        /// </summary>
        Ppon,
        /// <summary>
        /// 品生活
        /// </summary>
        PiinLife
    }
}

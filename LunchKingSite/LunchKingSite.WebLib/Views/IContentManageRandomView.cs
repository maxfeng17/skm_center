﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using System.IO;
using System.Web;
using LunchKingSite.BizLogic.Model;


namespace LunchKingSite.WebLib.Views
{
    public interface IContentManageRandomView
    {
        ContentManageRandomPresenter Presenter { get; set; }
        string UserName { get; }

        string ContentName { get; }
        string ContentName2 { get; }
        int Id { get; set; }
        int ContentId { get; set; }
        string Mode { get; set; }
        string UploadType { get; }
        RandomCmsType Type { get; }
        void SetUploadTypeDDL();
        void SetFileList(List<ImageFile> filelist);
        void GetCmsCollection(CmsRandomContentCollection data);
        void ReturnMessage(string message);
        void GetCmsById(CmsRandomContent cms);
        void GetCmsCityById(CmsRandomCity city);
        void GetCmsRandomTitleNId(DataTable dt, string ddl);
        void GetCmsContentById(string content);
        void GetCmsRandom(CmsRandomContent content, CmsRandomCityCollection cities);
        void GetViewCmsRandom(ViewCmsRandomCollection data);
        void ClearChaheByName(string name);
        void SetUpCityDDL(List<PponCity> citygroup, SystemCodeCollection codecollection);
        event EventHandler<DataEventArgs<string>> GetFileList;
        event EventHandler<DataEventArgs<CmsRandomContent>> SaveCmsRandomContent;
        event EventHandler<DataEventArgs<KeyValuePair<CmsRandomCity, List<RandomSelectedCity>>>> SaveCmsRandomCity;
        event EventHandler<DataEventArgs<CmsRandomCity>> UpdateCmsRandomCity;
        event EventHandler GetCmsRandomById;
        event EventHandler GetCmsRandomCityById;
        event EventHandler<DataEventArgs<KeyValuePair<int, string>>> GetCmsRandomContentById;
        event EventHandler<DataEventArgs<string>> ChangeCmsRandom;
        event EventHandler GetCacheName;
        event EventHandler ChangeRandomCmsType;
        event EventHandler<DataEventArgs<KeyValuePair<int, int>>> DeleteCmsRandomCity;
        event EventHandler<DataEventArgs<HttpPostedFile>> UploadFile;
        event EventHandler<DataEventArgs<string>> DelFile;
    }
    public class RandomSelectedCity
    {
        public int CityId { get; set; }
        public string CityName { get; set; }
        public int Ratio { get; set; }
        public RandomSelectedCity(int cityid, string cityname, int ratio)
        {
            CityId = cityid;
            CityName = cityname;
            Ratio = ratio;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
namespace LunchKingSite.WebLib.Views
{
    public interface IVourcherEventPrintView
    {
        #region property
        VourcherEventPrintPresenter Presenter { get; set; }
        /// <summary>
        /// 賣家GUID
        /// </summary>
        Guid SellerGuid { get; }
        /// <summary>
        /// 活動ID
        /// </summary>
        int EventId { get; }
        /// <summary>
        /// 使用者ID
        /// </summary>
        string LoginUser { get; }

        int UserId { get; }

        #endregion
        #region event
        /// <summary>
        /// 依賣家GUID搜尋優惠券
        /// </summary>
        event EventHandler GetVourcherEventBySellerGuid;
        /// <summary>
        /// 依優惠券ID搜尋內容
        /// </summary>
        event EventHandler GetVourcherEventById;
        /// <summary>
        /// 印出選取的優惠券內容
        /// </summary>
        event EventHandler<DataEventArgs<List<int>>> GetPrintVourcherEvent;
        #endregion
        #region method
        /// <summary>
        /// 依賣家搜尋所有優惠券
        /// </summary>
        /// <param name="seller">賣家</param>
        /// <param name="vourcher_events">優惠券</param>
        void SetSellerVourcherEventCollection(Seller seller, ViewVourcherSellerCollection vourcher_events);
        void SetSellerVourcherEventSotres(Seller seller, List<VourcherEventStoreInfo> info, string salesName);
        #endregion
    }
    public class VourcherEventStoreInfo
    {
        public VourcherEvent Vourcher_Event { get; set; }
        public ViewVourcherStoreCollection View_Vourcher_Stores { get; set; }
        public VourcherEventStoreInfo(VourcherEvent vourcher_event, ViewVourcherStoreCollection view_vourcher_store_collection)
        {
            Vourcher_Event = vourcher_event;
            View_Vourcher_Stores = view_vourcher_store_collection;
        }
    }
}

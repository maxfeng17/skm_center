﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPromoItemExchangeView
    {
        #region event
        event EventHandler<DataEventArgs<string>> Exchange;
        event EventHandler<EventArgs> ExchangeInfo;
        #endregion
        #region property
        PromoItemExchangePresenter Presenter { get; set; }
        string UserName { get; }
        EventActivity Activity { get; set; }
        string CodeName { get; set; }
        int EventId { get; }
        int UserId { get; }
        #endregion
        #region method
        void ShowExchange();
        void PromoItemGetList(Dictionary<string, int> dataList);
        void ShowMessage(string message, System.Drawing.Color color);
        void AlertMessage(string message);
        #endregion
    }
}

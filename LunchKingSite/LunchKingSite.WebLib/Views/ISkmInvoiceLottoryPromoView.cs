﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Model;
namespace LunchKingSite.WebLib.Views
{
    public interface ISkmInvoiceLottoryPromoView
    {
        #region property
        SkmInvoiceLottoryPromoPresenter Presenter { get; set; }
        int UserId { get; }
        string UserName { get; }
        string UserInfo { get; set; }
        bool IsPromoLogin { get; set; }
        #endregion
        #region event
        event EventHandler<DataEventArgs<SkmLottoryInvoice>> Exchange;
        #endregion
        #region method
        void ShowNoEvent();
        void ShowChoseEvent();
        void ShowExchange();
        void ShowSuccess();
        #endregion
    }

    public class SkmLottoryInvoice
    {
        /// <summary>
        /// 新光商圈活動-消費卡號
        /// </summary>
        public string CardNumber { get; set; }
        /// <summary>
        /// 新光商圈活動-店家名稱
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 新光商圈活動-登錄發票號碼
        /// </summary>
        public string InvoiceNumber { get; set; }
        /// <summary>
        /// 新光商圈活動-聯絡人姓名
        /// </summary>
        public string LoginUserName { get; set; }
        /// <summary>
        /// 新光商圈活動-聯絡人地址
        /// </summary>
        public string LoginUserAddress { get; set; }
        /// <summary>
        /// 新光商圈活動-聯絡人電話
        /// </summary>
        public string LoginUserMobile { get; set; }
    }
}

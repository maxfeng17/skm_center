﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IElmahLogListView
    {
        #region property
        int PageCount { get; set; }
        DateTime? StartDate { get; }
        DateTime? EndDate { get; }
        string Source { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<int>> PageChange;
        event EventHandler Search;
        #endregion

        #region method
        void GetElmahLogList(ElmahErrorCollection data);
        void SetUpPageCount();
        #endregion
    }
}

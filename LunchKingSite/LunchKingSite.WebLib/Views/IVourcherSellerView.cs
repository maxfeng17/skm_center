﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IVourcherSellerView
    {
        #region property
        VourcherSellerPresenter Presenter { get; set; }
        /// <summary>
        /// 使用者名稱
        /// </summary>
        string UserName { get; }
        /// <summary>
        /// 賣家Guid
        /// </summary>
        Guid SellerGuid { get; set; }
        /// <summary>
        /// 賣家Id
        /// </summary>
        string SellerId { get; set; }
        /// <summary>
        /// 分店Guid
        /// </summary>
        Guid StoreGuid { get; set; }
        /// <summary>
        /// 分頁頁數大小
        /// </summary>
        int PageSize { get; }
        /// <summary>
        /// 總資料數量
        /// </summary>
        int PageCount { get; set; }
        /// <summary>
        /// 搜尋賣家的欄位名稱和值
        /// </summary>
        KeyValuePair<string, string> SearchKeys { get; }
        SellerTempStatus TempStatus { get; }
        #endregion
        #region event
        /// <summary>
        /// 搜尋賣家
        /// </summary>
        event EventHandler SearchSellerInfo;
        /// <summary>
        ///新增賣家
        /// </summary>
        event EventHandler<DataEventArgs<KeyValuePair<Seller, List<PhotoInfo>>>> SaveTempSeller;
        /// <summary>
        /// 新增分店
        /// </summary>
        event EventHandler<DataEventArgs<Store>> SaveTempStore;
        /// <summary>
        /// 依賣家Guid搜尋賣家資料
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> GetSellerByGuid;
        /// <summary>
        /// 依分店Guid搜尋分店資料
        /// </summary>
        event EventHandler<DataEventArgs<Guid>> GetStoreByGuid;
        /// <summary>
        /// 撈取分頁
        /// </summary>
        event EventHandler<DataEventArgs<int>> PageChanged;
        /// <summary>
        /// 計算總資料數(分頁用)
        /// </summary>
        event EventHandler GetDataCount;
        event EventHandler GetTempStatusSellerStore;

        #endregion
        #region method
        /// <summary>
        /// 回傳賣家和其分店及賣家的修改資料
        /// </summary>
        /// <param name="seller">賣家資料</param>
        /// <param name="stores">分店資料</param>
        /// <param name="changeitems">修改賣家的資料</param>
        void SetSellerStores(Seller seller, StoreCollection stores, ChangeLogCollection changelogs);
        /// <summary>
        /// 回傳賣家資料(搜尋賣家)
        /// </summary>
        /// <param name="sellers">賣家</param>
        void SetSellerCollection(SellerCollection sellers);
        /// <summary>
        /// 回傳分店資料及修改分店資料
        /// </summary>
        /// <param name="store">分店資料</param>
        /// <param name="changeitems">修改分店資料</param>
        void SetStore(Store store, ChangeLogCollection changelogs);
        /// <summary>
        /// 回傳選定狀態賣家和分店資料
        /// </summary>
        /// <param name="temp_seller">選定狀態的賣家資料</param>
        /// <param name="temp_stores">選定狀態的分店資料</param>
        void GetReturnCase(SellerCollection temp_seller, StoreCollection temp_stores);
        /// <summary>
        /// 顯示重複的賣家
        /// </summary>
        /// <param name="repeat_sellers">重複的賣家</param>
        void ShowRepeaterSeller(SellerCollection repeat_sellers);
        #endregion
    }
}

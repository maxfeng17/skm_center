using System.Resources;

namespace LunchKingSite.WebLib.Views
{
    public interface ILocalizedView
    {
        ResourceManager Localization { get; }
        ResourceManager Message { get; }
    }
}

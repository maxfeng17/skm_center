﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IOrderShipBatchClearView
    {
        event EventHandler SearchClicked;
        event EventHandler OrderShipClearClicked;

        void SetDealType(Dictionary<int, string> types);
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        
        void SetDealList(IEnumerable<ShipDealInfo> dealInfo);
        void ShowMessage(string msg);

        string CurrentUser { get; }
        string FilterDealType { get; set; }
        string FilterQueryOption { get; set; }
        string FilterQueryTime { get; set; }
        string FilterKeyWord { get; set; }
        string FilterQueryStartTime { get; }
        string FilterQueryEndTime { get; }

        Dictionary<int, string> DealTypeInfos { get; }
        Dictionary<string, string> QueryOptionInfo { get; }

    }

    public class ShipDealInfo
    {
        public int OrderClassification { get; set; }
        public string SellerName { get; set; }
        public int UniqueId { get; set; }
        public Guid ProductGuid { get; set; }
        public string ProductName { get; set; }
        public DateTime UseStartTime { get; set; }
        public DateTime UseEndTime { get; set; }
        public int ShipCount { get; set; }
        public int UnShipCount { get; set; }
        public int ReturnCount { get; set; }
        public int TotalCount { get; set; }
        public DateTime? ShippedDate { get; set; }
    }
}

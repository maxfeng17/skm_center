﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesProposalProgressView
    {
        #region property
        SalesProposalProgressPresenter Presenter { get; set; }
        string UserName { get; }
        string CrossDeptTeam { get; }
        int EmpUserId { get; }
        int Monthly { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<string>> MonthChanged;
        #endregion

        #region method
        void SetProposalProgress(IEnumerable<ViewProposalSeller> pros);
        #endregion
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesCalendarContentView
    {
        int Id { get; }
        void SetCalendarContent(SalesCalendarEvent cal);
        short DevelopStatus { get; }
        short SellerGrade { get; }
    }
}

﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface IBlogListView
    {
        string UserName { get; }
        int PageSize { get; }
        int CurrentPage { get; }

        string BlogTitle { get; }
        DateTime BeginDate { get; }
        DateTime EndDate { get;  }

        #region Event
        void SetBlogListContent(List<BlogPost> blogList);
        event EventHandler Search;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion Event
    }
}

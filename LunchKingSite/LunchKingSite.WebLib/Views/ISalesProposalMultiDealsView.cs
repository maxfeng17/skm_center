﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesProposalMultiDealsView
    {
        #region property
        SalesProposalMultiDealsPresenter Presenter { get; set; }
        string UserName { get; }
        int ProposalId { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<int>> MultiSave;
        event EventHandler<DataEventArgs<int>> MultiDelete;
        event EventHandler<DataEventArgs<int>> Edit;
        #endregion

        #region method
        void SetProposalMultiDeals(Proposal pro);
        //ProposalMultiDealModel GetProposalMultiDealModelData();
        //void SetProposalMultiDealModel(ProposalMultiDealModel model);
        void RedirectProposalMultiDeals(int pid);
        #endregion
    }
}

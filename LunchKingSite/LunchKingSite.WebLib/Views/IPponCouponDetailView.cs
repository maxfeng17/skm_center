﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponCouponDetailView
    {
        #region property
        PponCouponDetailPresenter Presenter { get; set; }
        int UserId { get; }
        string UserName { get; }
        string UserEmail { get; }
        int PageCount { get; set; }
        bool PanelType { get; set; }
        int Department { get; }
        int OrderStatus { get; }
        int GroupStatus { get; }
        int CancelStatus { get; }
        int BusinessHourStatus { get; }
        int CurrentQuantity { get; set; }
        bool IsFamiportGroupCoupon { get; set; }
        DateTime ExpiredTime { get; }
        DateTime OrderEndDate { get; }
        Guid OrderGuid { get; }
        int Slug { get; }
        int OrderMin { get; }
        int ItemPrice { get; }
        int AccBusinessGroupId { get; set; }
        string OrderId { get; set; }
        ThirdPartyPayment ThirdPartyPaymentSystem { set; get; }
        bool ShowLinePoints { get; }
        string LinePoinstDetailsLink { get; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<bool>> PanelChange;
        event EventHandler<DataEventArgs<SMSQuery>> SendSMS;
        event EventHandler<DataEventArgs<SMSQuery>> GetSMS;
        event EventHandler<DataEventArgs<MultipleEinvoiceRequestQuery>> RequestEinvoice;
        event EventHandler<Guid> GetViewPponDealGetByBid;
        event EventHandler<string> RequestInvoicePaper;
        #endregion

        #region method
        void SetCouponListSequence(ViewCouponListSequenceCollection data, EinvoiceMainCollection einvoice, ViewPponOrderDetailCollection od_system,
            PaymentTransactionCollection pt, PaymentTransactionCollection pt_atm, CashTrustLogCollection os, IEnumerable<OrderReturnList> orToExchange,
            List<EinvoiceAllowanceInfo> allowances, CtAtmRefund ctatmrefund, bool receiptCodeGenerated, IList<ReturnFormEntity> returnForms, Order o, DealProperty dp, List<ViewMgmGift> mgmGifts);
        void SetRefundFormInfo(EinvoiceMain einvoice, CashTrustLogCollection cs, OldCashTrustLogCollection ocs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus);
        void SetRefundFormInfoForMultiInvoices(List<EinvoiceMain> einvoices, CashTrustLogCollection cs, CtAtmRefund atmrefund, DealProperty dp, Order o, bool isleagl, string message, int returnFormStatus);
        void SetEntrustSellRefundFormInfo(List<EntrustSellReceipt> receipts, Order o, CashTrustLogCollection cs, DealProperty dp, bool isleagl, string message, int returnFormStatus);
        void SetShipInfo(ViewOrderShipList osInfo, DateTime? deliveryEndDate, ShipInfo shipInfo, string orderDesc);
        void ShowAlert(string message);
        void FinishEinvoiceRequestRequest();
        void GetAlreadySmsCount(int count, int type);
        void SetReceiptDealInfo(EntrustSellType entrustSell,
            bool booked, DateTime? vacationStartEnd, DateTime? vacationEndTime, bool isEntrusted, bool applyRefund);

        void ShowFamiTea(string message, bool show);
        bool ShowDetail(Guid orderGuid, ViewCouponListMain main);

        #endregion
    }
}

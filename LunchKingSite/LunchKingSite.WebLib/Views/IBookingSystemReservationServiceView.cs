﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IBookingSystemReservationServiceView
    {
        #region property

        Guid? OrderGuid { get; set; }
        string UserEmail { set; get; }
        string CouponUsage { set; get; }
        string UserName { get; }
        Guid? StoreGuid { get; set; }
        int BookingSystemType { set; get; }
        int ReservationMinDay { set; get; }
        int CouponUsers { set; }
        int MemberKey { set; }
        string StoreAddress { set; }
        string StoreName { set; }
        string StorePhoneNumber { set; }
        string ApiKey { get; set; }
        string CouponLink { set; }
        string FullBookingDate { set; }
        string CouponCanUseDate { set; }

        #endregion

        #region method

        void SetCouponSequenceCheckBox(CashTrustLogCollection CouponSequencesCol);
        void RedirectToLogin();
        void SetDefaultValue(string reservationName, int? gender, string reservationPhoneNumber, Dictionary<string, string> sellers, bool IsNoRestrictedStore);

        #endregion

        #region event
        

        
        #endregion
    }
}
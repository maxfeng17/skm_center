﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;
using System.Web.UI.WebControls;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IEmpSetView
    {
        event EventHandler<DataEventArgs<Employee>> SaveEmp;
        event GridViewCommandEventHandler ShowEmp;
        event EventHandler OnSearchClicked;
        event EventHandler OnParentDeptSearchSelectedIndexChanged;
        event EventHandler OnParentDeptSelectedIndexChanged;

        EmpSetViewWorkType ViewWorkType { get; set; }
        string EmpId { get; }
        string UserName { get; }
        bool KPIVisible { get; set; }
        List<KeyValuePair<string, object>> filter { get; }
        EmployeeDept parentDeptId { get; set; }
        EmployeeDept parentDeptIdSearch { get; }
        bool CrossDeptTeam { get; }
        bool IsGrpPerformance { get; set; }
        bool IsOfficial { get; set; }

        void SetPageWorkType(EmpSetViewWorkType workType);
        void ShowList(ViewEmployeeCollection viewEmpCol);
        void ShowEmpData(Employee emp);
        void SetParentDepartment(DepartmentCollection parentDeptList);
        void SetDepartment(DepartmentCollection deptList);
        void SetDeptManager(DepartmentCollection deptList);
        void SetCrossDeptTeam(DepartmentCollection deptList);
        void SetDepartmentSearch(DepartmentCollection deptList);
        void SetEmpLevel();
        void ShowMessage(string msg);
    }

    public enum EmpSetViewWorkType
    {
        List,
        Add,
        Modify,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealCreditcardPay
    {
        event EventHandler GetAuthorization;

        string UserName { get; }
        int? Pid { get; }

        string TransactionId { get; }
        string TicketId { get; }
        string CardNo { get; }
        string ExpirationYear { get; }
        string ExpirationMonth { get; }
        PaymentAPIProvider APIProvider { get; set; }
        HiDealDeliveryInfo PayViewSessionData { get; }

        void RedirectToErrorPage();
        void RedirectToDealPage();
        void RedirectToLogin();
    }
}

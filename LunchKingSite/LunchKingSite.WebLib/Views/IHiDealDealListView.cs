﻿using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealDealListView
    {
        #region event
        event EventHandler<DataEventArgs<HiDealDealListViewSearchRequest>> SearchClicked;
        #endregion


        #region method
        void ShowMessage(string message);
        void ShowData(ViewHiDealSellerProductCollection dataCollection, int dataCount);
        #endregion

    }

    public enum  HiDealDealListViewSearchType
    {
        /// <summary>
        /// 依據商家名稱查詢
        /// </summary>
        SellerName,
        /// <summary>
        /// 依據檔次名稱查詢
        /// </summary>
        DealName,
        /// <summary>
        /// 商品名稱
        /// </summary>
        ProductName,
        /// <summary>
        /// 檔號
        /// </summary>
        DealId,
        /// <summary>
        /// 商品檔號
        /// </summary>
        ProductId,
    }

    public enum HiDealDealListViewDealWorkingType
    {
        /// <summary>
        /// 略過
        /// </summary>
        None = 0,
        /// <summary>
        /// 尚未開始
        /// </summary>
        Before = 1,
        /// <summary>
        /// 進行中
        /// </summary>
        Working = 2,
        /// <summary>
        /// 截止
        /// </summary>
        End = 3,
    }

    public class HiDealDealListViewSearchRequest
    {
        /// <summary>
        /// 查詢方式
        /// </summary>
        public HiDealDealListViewSearchType SearchType { get; set; }
        /// <summary>
        /// 查詢方式對應用的條件
        /// </summary>
        public string FilterString { get; set; }
        /// <summary>
        /// 檔次開始時間 起
        /// </summary>
        public DateTime? StartTimeBegin { get; set; }
        /// <summary>
        /// 檔次開始時間 迄
        /// </summary>
        public DateTime? StartTimeEnd { get; set; }
        /// <summary>
        /// 檔次截止時間 起
        /// </summary>
        public DateTime? EndTimeBegin { get; set; }
        /// <summary>
        /// 檔次截止時間 迄
        /// </summary>
        public DateTime? EndTimeEnd { get; set; }
        /// <summary>
        /// 檔次販賣狀態的設定
        /// </summary>
        public HiDealDealListViewDealWorkingType DealWorkingType;

        public int PageNum;
        public int PageSize;
    }
}

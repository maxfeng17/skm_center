﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using System.Web.Security;

namespace LunchKingSite.WebLib.Views
{
    public interface IMemberRoleManagementView
    {
        #region props
        MemberRoleManagementPresenter Presenter { get; set; }
        string LoginUser { get; }
        int PageSize { get; set; }
        string MemberEmail { get; }
        string OrgName { get; set; }
        string OrgDisplayName { set; }
        string FuncNameSearch { get; }
        #endregion

        #region event
        event EventHandler<EventArgs> MemberSearchClicked;
        event EventHandler<DataEventArgs<int>> OnGetMemberCount;
        event EventHandler<DataEventArgs<int>> MemberPageChanged;
        event EventHandler<DataEventArgs<string>> GetUserInRolesData;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> MemberLeftSearch;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> MemberRightSave;
        event EventHandler<DataEventArgs<string>> GetMemberPrivilege;
        event EventHandler<DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>> MemberPrivilegeRightSave;

        event EventHandler<EventArgs> RoleSearchClicked;
        event EventHandler<DataEventArgs<string>> GetMemberRolesData;
        event EventHandler<DataEventArgs<string>> DeleteMemberRoles;
        event EventHandler<DataEventArgs<string>> AddMemberRoles;
        event EventHandler<DataEventArgs<Organization>> RoleAddClicked;
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> RoleNameEdit;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> OrgLeftSearch;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> OrgRightSave;
        event EventHandler<DataEventArgs<string>> GetOrgPrivilege;
        event EventHandler<DataEventArgs<KeyValuePair<string, Dictionary<int, string>>>> OrgPrivilegeRightSave;

        event EventHandler<EventArgs> FuncSearchClicked;
        event EventHandler<DataEventArgs<int>> OnGetFuncCount;
        event EventHandler<DataEventArgs<int>> FuncPageChanged;
        event EventHandler<DataEventArgs<int>> GetPrivilegeData;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> SysOrgPrivilegeLeftSearch;
        event EventHandler<DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>> SysOrgPrivilegeRightSave;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<string>>>> SysPrivilegeLeftSearch;
        event EventHandler<DataEventArgs<KeyValuePair<KeyValuePair<int, string>, List<string>>>> SysPrivilegeRightSave;
        #endregion

        #region method
        void GetMemberList(MemberCollection memberList);
        void SetUserInRolesData(Dictionary<string, string> roles, string userName);
        void SetMemberLeftSearchResult(Dictionary<string, string> resultList);
        void SetMemberPrivilegeLeftSearchResult(Dictionary<string, string> resultList);
        void SetMemberPrivilegeData(IOrderedEnumerable<KeyValuePair<int, string>> privileges, string userName, IOrderedEnumerable<KeyValuePair<int, string>> allFuncs);

        void GetRoleList(IOrderedEnumerable<KeyValuePair<Organization, string>> dataList);
        void SetOrganizationData(List<string> users, Organization mainOrg);
        void SetParentOrganizationInfo(Organization org);
        void SetOrgLeftSearchResult(List<Member> resultList);
        void SetOrgPrivilegeLeftSearchResult(List<OrgPrivilege> resultList);
        void SetOrgPrivilegeData(IOrderedEnumerable<KeyValuePair<int, string>> orgPrivileges, Organization mainOrg, IOrderedEnumerable<KeyValuePair<int, string>> allFuncs);

        void GetSystemFuncList(SystemFunctionCollection dataList);
        void SetSystemFunctionInfo(ViewPrivilegeCollection privileges, IOrderedEnumerable<KeyValuePair<Organization, string>> orgPrivileges, SystemFunction func);
        void SetSysOrgPrivilegeLeftSearchResult(Dictionary<string, string> resultList);
        void SetSysPrivilegeLeftSearchResult(List<Member> resultList);

        void ShowMessage(string msg);
        #endregion

        #region private method
        #endregion
    }
}

﻿using System;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System.Data;
using System.Web.UI;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealManualUnsubscriptionView
    {
        event EventHandler<DataEventArgs<string>> UnSubscribe;    
        string theEmail { get; set; }
        string UserName { get; }
        int UserId { get; }
       
        void ShowTheSuccess();
        void ShowTheError();
        void ShowTheUnSubscribe();
    }
}

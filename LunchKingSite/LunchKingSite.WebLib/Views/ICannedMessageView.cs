﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ICannedMessageView
    {
        #region property
        CannedMessagePresenter Presenter { get; set; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        int CategoryId { get;}
        int InCategoryId { get; }
        string Content { get; }
        int UserId { get; }
        string UserName { get; }
        string Mode { get; }
        int SampleId { get; }
        string Subject { get; }
        int FineId { get; }
        string FineContent { get; }
        int FineType { get; }
        string FineMode { get; }
        #endregion
        #region event
        event EventHandler Search;
        event EventHandler AddService;
        event EventHandler AddFine;
        event EventHandler<DataEventArgs<int>> EditCannedMessage;
        event EventHandler<DataEventArgs<int>> DeleteCannedMessage;
        event EventHandler<DataEventArgs<int>> GetCannedMessageCount;
        event EventHandler<DataEventArgs<int>> CannedMessagePageChanged;

        #endregion

        #region method
        void CannedMessageList(CustomerServiceCategorySampleCollection categorySampleList);
        void CustomerServiceCategorySampleEdit(CustomerServiceCategorySample entity);
        void VendorFineCategoryGetList(VendorFineCategoryCollection fineCategory);
        #endregion

    }

}

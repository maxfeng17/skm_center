﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IHidealOrderListView
    {
        #region property
        HidealOrderListPresenter Presenter { get; set; }
        int FilterTypeValue { get; }
        string FilterText { get; }
        string FilterInternal { get; }
        int PageSize { get; set; }
        #endregion

        #region event
        event EventHandler OnSearchClicked;
        event EventHandler<DataEventArgs<int>> OnGetHidealOrderCount;
        event EventHandler<DataEventArgs<int>> HidealOrderPageChanged;
        #endregion

        #region method
        void GetHiDealOrderList(ViewHiDealOrderMemberOrderShowCollection dataList);
        #endregion
    }
}

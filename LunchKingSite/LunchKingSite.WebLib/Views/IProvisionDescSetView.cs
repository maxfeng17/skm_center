﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;

namespace LunchKingSite.WebLib.Views
{
    public interface IProvisionDescSetView
    {
        #region property
        ProvisionDescSetPresenter Presenter { get; set; }
        string UserName { get; }
        ProvisionType Type { get; }
        ObjectId Id { get; }
        ObjectId DepartmentId { get; set; }
        ObjectId CategoryId { get; set; }
        ObjectId ItemId { get; set; }
        ObjectId ContentId { get; set; }
        ObjectId DescId { get; set; }
        string EditText { get; }
        ProvisionDescStatus Status { get; }
        #endregion

        #region event
        event EventHandler<EventArgs> ProvisionSelectedIndexChanged;
        event EventHandler<EventArgs> SaveClicked;
        event EventHandler<EventArgs> MongoSyncSqlClicked;
        event EventHandler<EventArgs> DeleteClicked;
        event EventHandler<EventArgs> SortClicked;
        event EventHandler<DataEventArgs<Dictionary<ObjectId, int>>> SaveSortClicked;
        #endregion

        #region method
        void SetProvisionDropDownListData(IEnumerable<ProvisionDepartment> department);
        void SetMsProvisionDropDownListData(IEnumerable<ProvisionDepartmentModel> department);
        void SetProvisionSortRank(Dictionary<ObjectId, string[]> dataList);
        #endregion
    }
}

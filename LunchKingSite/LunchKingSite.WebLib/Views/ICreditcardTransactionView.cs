﻿using LunchKingSite.DataOrm;
using System;
using System.Data;

namespace LunchKingSite.WebLib.Views
{
    public interface ICreditcardTransactionView
    {
        string CurrentUser { get; }

        string OrderId { get; set; }

        string TransId { get; set; }

        string CreateId { get; set; }

        string TransTimeBegin { get; set; }

        string TransTimeEnd { get; set; }

        string Msg { get; set; }

        string AuthCode { get; set; }

        int? Classification { get; set; }

        string ErrShow { get; set; }

        int PageSize { get; set; }

        string BatchResult { get; set; }

        string Oid { get; set; }

        Guid OrderGuid { get; set; }

        event EventHandler SearchClicked;

        event EventHandler SearchPCashClicked;

        event EventHandler RefundPCashClicked;

        event EventHandler<DataEventArgs<int>> GetTransCount;

        event EventHandler<DataEventArgs<int>> PageChanged;

        event EventHandler<DataEventArgs<string[]>> CancelPcash;

        event EventHandler<DataEventArgs<string[]>> ChargePcash;

        event EventHandler<DataEventArgs<string>> ChargePcashBatch;

        void SetGridTrans(ViewPaymentTransactionLeftjoinOrderCollection orders);

        void SetPcashResult(string result);

        void SetPcashResultC(string result);

        void SetPCashGridView(PaymentTransactionCollection ptc);

        void SetRefundPcash(DataTable dt);
    }
}
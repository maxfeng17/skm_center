﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface IPponOptionContnetView
    {
        #region property
        Guid BusinessHourGuid { get; }
        string UserName { get; }
        #endregion

        #region event
        #endregion

        #region method
        void SetPponOptionContent(PponOptionItems poi);
        #endregion
    }
}

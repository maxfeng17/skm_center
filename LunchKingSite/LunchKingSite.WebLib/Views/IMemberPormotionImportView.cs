﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LunchKingSite.WebLib.Views
{
    public interface IMemberPormotionImportView
    {
        #region props

        string UserName { get; }
        string ActionName { get; }
        double Amount { get; }
        DateTime StartTime { get; }
        DateTime EndTime { get; }

        #endregion

        #region event

        event EventHandler<DataEventArgs<List<int>>> Import;

        #endregion

        #region method

        void ShowMessage(string msg);

        #endregion
    }
}

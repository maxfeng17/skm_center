﻿using System;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using System.Web.UI.WebControls;

namespace LunchKingSite.WebLib.Views
{
    public interface ISalesProposalListView
    {
        #region property
        SalesProposalListPresenter Presenter { get; set; }
        string UserName { get; }
        int ProposalId { get; }
        Guid Bid { get; }
        Guid Sid { get; }
        string SellerName { get; }
        string BrandName { get; }
        string DealContent { get; }
        DateTime OrderTimeS { get; }
        DateTime OrderTimeE { get; }
        bool IsOrderTimeSet { get; }
        string MarketingResource { get; }
        int SpecialFlag { get; }
        int? SalesId { get; }
        int EmpUserId { get; }
        string Dept { get; }
        ProposalStatus Status { get; }
        DeliveryType? Type { get; }
        int? DeliveryNewType { get; }
        int? NewHouseStatus { get; }
        ProposalCopyType? CopyType { get; }
        int DealType { get; }
        int DealType1 { get; }
        int DealSubType { get; }
        int CheckFlag { get; }
        bool ShowAppley { get; }
        bool ShowChecked { get; }
        bool ShowHouseChecked { get; }
        bool NoOrderTime { get; }
        int PageSize { get; set; }
        int CurrentPage { get; }
        string StatusFlag { get; }
        int ProposalCreatedType { get; }
        int UniqueId { get; }
        string CrossDeptTeam { get; }
        int Photographer { get; }
        int PhotoType { get; }
        string DealStatus { get; }
        FileUpload FUExport { get; }
        #endregion

        #region event
        event EventHandler Search;
        event EventHandler Export;
        event EventHandler Import;
        event EventHandler<DataEventArgs<int>> OnGetCount;
        event EventHandler<DataEventArgs<int>> PageChanged;
        #endregion

        #region method
        void SetProposalList(ViewProposalSellerCollection dataList);
        void ShowMessage(string msg);
        #endregion
    }
}

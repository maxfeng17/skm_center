﻿using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.Views
{
    public interface ILionTravelOrderDetailView
    {
        #region property
        LionTravelOrderDetailPresenter Presenter { get; set; }
        Guid OrderGuid { get; }
        AgentChannel OrderClassType { get; }
        int OrderDeliveryType { set; }
        #endregion

        #region event
        event EventHandler<DataEventArgs<OrderUserMemoList>> OrderUserMemoSet;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<int>>>> RefundCoupon;
        event EventHandler<DataEventArgs<KeyValuePair<string, List<int>>>> ReturnProduct;
        event EventHandler<DataEventArgs<int>> ReSendSms;
        event EventHandler<DataEventArgs<int>> DeleteUserMemo;
        #endregion

        #region method
        void SetOrderInfo(OrderCorresponding order_corresponding, ViewPponDeal deal, OrderUserMemoListCollection user_memos);
        void SetOrderUserMemoInfo(OrderUserMemoListCollection user_memos);
        void SetCouponInfo(ViewPponCouponCollection view_ppon_coupons, CashTrustLogCollection cash_trust_logs, CashTrustLogCollection refund_cash_trust_logs, Dictionary<int, int> sms_count_list, IList<ReturnFormEntity> return_forms);
        void SetProductInfo(IEnumerable<SummarizedProductSpec> source, IList<ReturnFormEntity> return_forms);
        void SetDeliverInfo(ViewOrderShipListCollection osCol);
        void ShowNoData();
        void ShowMessage(string message);
        #endregion
    }
}

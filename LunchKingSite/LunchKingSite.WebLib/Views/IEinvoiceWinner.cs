﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEinvoiceWinner
    {
        EinvoiceWinnerPresenter Presenter { get; set; }
        string UserName { get; }
        int UserId { get; }
        event EventHandler<DataEventArgs<KeyValuePair<string, KeyValuePair<string, string>>>> UpdateWinnerInfo;
        void GetEinvoiceMain(EinvoiceMainCollection data);
        void FinishUpdate();

        void ShowMessage(string msg);
    }
}

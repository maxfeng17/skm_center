﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventEditorView
    {
        event EventHandler<DataEventArgs<EventContentSave>> SaveEvent;
        
        EventEditorPresenter Presenter { get; set; }
        string EventId { get; set; }

        void LoadEvent(EventContent ec, PromoSeoKeyword seo);
    }
    public class EventContentSave
    {
        /// <summary>
        /// UserId
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// SeoDescription
        /// </summary>
        public string SeoDescription { get; set; }
        //public HttpPostedFile DealPromoImagePostedFile { set; get; }
        /// <summary>
        /// SeoKeyWords
        /// </summary>
        public string SeoKeyWords { get; set; }
        /// <summary>
        /// 行銷內容
        /// </summary>
        public EventContent EventContent { set; get; }
    }
}

using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;

namespace LunchKingSite.WebLib.Views
{
    public interface IOrderReturnView
    {
        event EventHandler<DataEventArgs<int>> GetOrderCount;
        event EventHandler SortClicked;
        event EventHandler<DataEventArgs<int>> PageChanged;
        event EventHandler SearchClicked;
        event EventHandler ExportToExcel;

        OrderReturnPresenter Presenter { get; }
        string SortExpression { get; set; }
        string FilterUser { get; set; }
        //string[] FilterInternal { get;}
        string FilterType { get; set; }   
        int PageSize { get; set; }
        int CurrentPage { get; }
        string DateType { get; }
        string CsvType { get; }
        void SetOrderList(ViewOrderMemberBuildingSellerCollection orders);
        //string OrderByInternal { get;}
        /*
        bool ExcludeFamilyMart { get; }
        bool PaymentFreeze { get; }
        */
        void SetFilterTypeDropDown(Dictionary<string, string> types);
        void Export(ViewOrderMemberBuildingSellerCollection orders);
        DateTime? MinOrderCreateTime { get; }
        DateTime? MaxOrderCreateTime { get; }
        string SearchExpression { get; }
        string GetFilterTypeDropDown();
    }
}

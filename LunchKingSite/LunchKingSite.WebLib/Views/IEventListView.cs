﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEventListView
    {
        event EventHandler<DataEventArgs<int>> PageChange;

        int PageSize { get; set; }

        void SetEventList(EventContentCollection ecc);
    }
}

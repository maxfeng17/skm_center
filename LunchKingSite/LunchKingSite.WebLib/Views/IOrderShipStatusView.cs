﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.WebLib.Presenters;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IOrderShipStatusView
    {
        OrderShipStatusPresenter Presenter { get; }
        string UserName { get; }
        Guid? Bid { get; set; }
        int PageCount { get; set; }
        int CurrentPage { get; set; }
        event EventHandler<DataEventArgs<Guid>> SearchPponOrderList;
        event EventHandler<DataEventArgs<KeyValuePair<OrderStatusLog, int>>> UpdateShipOrderStatus;
        event EventHandler<DataEventArgs<int>> PageChange;
        void SetOrderList(List<ClassForViewPponOrder_OrderStatusLog> lvpoc, ViewPponDeal vpd);
    }
    public class ClassForViewPponOrder_OrderStatusLog
    {
        public ViewPponOrder Viewpponorder { get; set; }
        public OrderStatusLog Orderstatuslog { get; set; }
        public ClassForViewPponOrder_OrderStatusLog(ViewPponOrder vpo, OrderStatusLog osl)
        {
            Viewpponorder = vpo;
            Orderstatuslog = osl;
        }
        public ClassForViewPponOrder_OrderStatusLog() { }
    }
}

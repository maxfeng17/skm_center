﻿using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebLib.Views
{
    public interface IReferrerManageView
    {
        event EventHandler<DataEventArgs<KeyValuePair<string, string>>> AddReferrer;

        event EventHandler<DataEventArgs<ReferralCampaign>> AddCampaign;

        event EventHandler<DataEventArgs<Tuple<Guid, string>>> ExportToExcel;

        event EventHandler<DataEventArgs<Tuple<Guid, string>>> QueryCampaignRecord;

        ReferrerManagePresenter Presenter { get; set; }

        Guid ReferrerId { get; }

        string CampaignCode { get; }

        string ActionType { get; }

        string QueryStartTime { get; }

        string QueryEndTime { get; }

        string UserName { get; }

        /// <summary>
        /// 顯示 Campaign 列表
        /// </summary>
        /// <param name="referrers"></param>
        void SetReferrerList(List<MemberReferral> referrers);

        void SetReferrerDetail(MemberReferral data, List<ReferralCampaign> campaigns);

        /// <summary>
        /// 顯示 Campaign 記錄
        /// </summary>
        /// <param name="campaign"></param>
        /// <param name="actions"></param>
        /// <param name="exportToExcel"></param>
        void SetCampaignDetail(MemberReferral referrer, ReferralCampaign campaign, List<ReferralAction> actions, bool exportToExcel);

        /// <summary>
        /// 顯示初始進入CPA管理頁的 Referrer 列表
        /// </summary>
        void ShowReferrerListing();

        void ShowReferrerDetail(Guid referrerId);

        void ShowCampaign(Guid referrerId, string campaignCode);

    }
}
﻿using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Presenters;
using System;

namespace LunchKingSite.WebLib.Views
{
    public interface IHiDealMemberCouponListView
    {
        #region property

        HiDealMemberCouponListPresenter Presenter { get; set; }

        int OrderStatus { get; }

        int OrderShowStatus { get; set; }

        string UserName { get; }

        int UserId { get; }

        DateTime OrderEndDate { get; set; }

        Guid OrderGuid { get; }

        int? OrderPkCondition { get; }

        string Mobile { set; get; }

        string ProductName { set; get; }

        string OrderId { set; get; }

        int OrderPk { set; get; }

        bool CouponListVisible { set; get; }

        bool HidealSmsVisible { set; get; }

        #endregion property

        #region event

        event EventHandler<DataEventArgs<HiDealSMSQuery>> SendSMS;

        event EventHandler<DataEventArgs<HiDealSMSQuery>> GetSMS;

        event EventHandler<DataEventArgs<MultipleEinvoiceRequestQuery>> RequestEinvoice;

        #endregion event

        #region method

        void SetCouponList(ViewHiDealCouponListSequenceCollection vhcsc);

        void SetShipInfo(ViewOrderShipList osInfo, DateTime? deliveryEndTime);

        void SetOrderDetail(HiDealOrderDetailCollection hdodc);

        void SetOrderCategory(HiDealOrderDetailCollection hdodc);

        void SetOrderPaymentDetail(CashTrustLogCollection ctlc, DiscountCode dc);

        void SetOrderReturned(HiDealReturnedCollection hdrc, CashTrustLogCollection ctlc);

        void SetEnvoiceDetail(List<ViewEinvoiceMail> viewEinvoices, List<EinvoiceAllowanceInfo> veac);

        void SetNewEInvoiceDetail(ViewEinvoiceMailCollection mails, CashTrustLogCollection ctlc);

        void ShowAlert(string message);

        void FinishEinvoiceRequestRequest();

        void GetAlreadySmsCount(int count, int type);

        void IlleagalUser();

        #endregion method
    }

    public class HiDealSMSQuery
    {
        public string CouponId { get; set; }

        public string Mobile { get; set; }

        public HiDealSMSQuery(string couponid, string mobile)
        {
            CouponId = couponid;
            Mobile = mobile;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public interface IEntrustSellReceiptListView
    {
        string FilterColumn { get; set; }
        string FilterValue { get; set; }
        DateTime? FilterVerifiedStartTime { get; set; }
        DateTime? FilterVerifiedEndTime { get; set; }
        bool? FilterIsPhysical { get; }
        bool? FilterIsExported { get; }
        bool? FilterHasReceiptCode { get; }

        event EventHandler SearchClicked;
        event EventHandler ExportClicked;

        void SetContent(List<ViewEntrustSellReceipt> receipts);
        void SetMessage(string msg);

    }
}

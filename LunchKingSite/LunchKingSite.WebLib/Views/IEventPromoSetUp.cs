﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Views
{
    public delegate string GetEventPromoItemStatusHandler(ViewEventPromoItem x);

    public interface IEventPromoSetUp
    {
        #region property
        int Mode { get; set; }
        string UserName { get; }
        int EventId { get; set; }
        int ItemId { get; set; }
        int ItemCount { get; set; }
        string ItemTitle { set; }
        string ItemDescription { set; }
        string VourcherTitle { set; }
        string VourcherDescription { set; }
        string VourcherCategory { set; }
        string VourcherSubCategory { set; }
        string VourcherSeq { set; }
        int VourcherEventPromoItemId { set; get; }
        int VourcherEventPromoItemItemId { set; get; }

        EventPromoType EventType { get; }
        bool IsLimitInThreeMonth { get; }
        string ItemStatus { get; set; }
        bool EventUseDiscount { get; set; }
        bool IsCurationFunction { get; }

        #endregion
        #region event
        event EventHandler<DataEventArgs<EventPromoUpdateModel>> SaveEventPromo;
        event EventHandler ChangeMode;
        event EventHandler GetEventPromo;
        event EventHandler UpdateEventPromoStatus;
        event EventHandler GetEventPromoItemList;
        event EventHandler UpdateEventPromoItemStatus;
        event EventHandler<DataEventArgs<KeyValuePair<int, EventPromoItemType>>> GetPponDeal;
        event EventHandler<DataEventArgs<EventPromoItemUpdateMode>> SaveEventPromoItem;
        event EventHandler GetEventPromoItem;
        event EventHandler<DataEventArgs<string>> WeChatSendTestMessage;
        event EventHandler GetEventPromoList;
        event EventHandler<DataEventArgs<bool>> SetEventPromoShowInApp;
        event EventHandler<DataEventArgs<bool>> SetEventPromoShowInWeb;
        event EventHandler<DataEventArgs<int>> DeleteEventPromoItem;
        event EventHandler<DataEventArgs<List<Tuple<string, string, string>>>> ImportBidList;
        event EventHandler<DataEventArgs<KeyValuePair<int, Dictionary<int, int>>>> UpdateEventPromoItemSeq;
        event GetEventPromoItemStatusHandler GetEventPromoItemStatus;
        #endregion
        
        #region method

        void ChangePanel(int mode, string return_message = "");
        void SetEventPromoList(EventPromoCollection promos);
        void SetEventPromo(PromoSeoKeyword seo, EventPromo promo, bool enableBindingCategory = false);
        void SetEventPromoItemList(ViewEventPromoItemCollection items);
        void SeTEventPromoItemList(EventPromoItemCollection items);
        void SetVotePicEventPromoItemList(ViewEventPromoItemPicVoteCollection items);
        void SetVourcherEventPromoItemList(Dictionary<EventPromoItem, DateTime> items);
        void SetEventPromoItem(DateTime dateStart);
        void SetEventPromoItem(EventPromoItem item, EventPromoItem linkItem, DateTime dateStart, DateTime linkItemDateStart, EventPromoTemplateType tempLateType);
        void SetBindingCategory(CategoryCollection categories);
        void SetVourcherEventPromoItem(DateTime dateStart);
        void AlertImportErrorMsg(string errorMsg);
        #endregion

        void SetEventPromoCities(List<PponCity> getPponCities, ViewCmsRandomCollection viewCmsRandomGetCollectionByEventPromoId);
    }


    public class EventPromoUpdateModel
    {
        /// <summary>
        /// 異動的商品主題活動主檔
        /// </summary>
        public EventPromo MainEvent { get; set; }
        /// <summary>
        /// 上傳AppBanner圖片的物件
        /// </summary>
        public HttpPostedFile AppBannerPostedFile { get; set; }
        /// <summary>
        /// 上傳AppPromo主要圖片的物件
        /// </summary>
        public HttpPostedFile AppPromoPostedFile { get; set; }
        /// <summary>
        /// 上傳行銷活動訊息活動LOGO
        /// </summary>
        public HttpPostedFile DealPromoImagePostedFile { set; get; }
        /// <summary>
        /// 行銷SeoKeyword
        /// </summary>
        public PromoSeoKeyword SeoKeyword { set; get; }
        /// <summary>
        /// 策展1.0顯示頻道
        /// </summary>
        public UpdateCmsRandomCitiesModel UpdateCmsRandomCities { get; set; }
        /// <summary>
        /// 策展1.0Banner顯示區域
        /// </summary>
        public List<int> BannerType { get; set; }
    }

    public class UpdateCmsRandomCitiesModel
    {
        public bool Update { get; set; }
        public List<int> CityIds { get; set; }
    }

    public class EventPromoItemUpdateMode
    {
        /// <summary>
        /// 原 EventPromoItem obj
        /// </summary>
        public List<EventPromoItem> PromoItems { get; set; }
        /// <summary>
        /// 上傳圖片投票
        /// </summary>
        public HttpPostedFile VotePic { set; get; }
    }
}

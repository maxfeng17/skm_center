﻿using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebLib.Views
{
    public interface IBlogContentView
    {
        string UserName { get; }
        int Id { get; }
        Guid BlogGid { get; set; }
        string BlogTitle { get; }
        string BlogKeyWord { get; }
        string BlogDescription { get; }
        DateTime BeginDate { get; }
       
        string BlogContentText { get; }

        #region Event
        void SetBlogContent(BlogPost blog);
        void ShowMessage(string msg);
        event EventHandler Save;
        #endregion Event
    }
}

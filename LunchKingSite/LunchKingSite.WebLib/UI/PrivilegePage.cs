﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core.UI;
using LunchKingSite.BizLogic.Facade;
using System.Web;

namespace LunchKingSite.Core.UI
{
    public abstract class PrivilegePage : BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!IsPostBack)
            {
                if (!CommonFacade.IsInSystemFunctionPrivilege(HttpContext.Current.Request.Url.LocalPath, User.Identity.Name))
                    Response.Redirect("/PEZIM.aspx");
            }
        }
    }
}

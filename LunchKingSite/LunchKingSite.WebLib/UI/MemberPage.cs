using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebLib.UI
{
    public class MemberPage : LocalizedBasePage
    {
        public int UserId
        {
            get
            {
                if (this.User.Identity.IsAuthenticated == false)
                {
                    return 0;

                }
                PponPrincipal user = this.User as PponPrincipal;
                if (user != null)
                {
                    return user.Identity.Id;
                }
                else
                {
                    return MemberFacade.GetUniqueId(this.User.Identity.Name);
                }
            }
            private set { }
        }
        public SingleSignOnSource LoginSource { get; private set; }

        protected override void OnInit(System.EventArgs e)
        {
            PrepareData();
            base.OnInit(e);
        }

        private void PrepareData()
        {
            if (this.User.Identity.IsAuthenticated == false)
            {
                this.UserId = 0;
                return;
            }
            PponPrincipal user = this.User as PponPrincipal;
            if (user != null)
            {
                this.UserId = user.Identity.Id;
            }
            else
            {
                this.UserId = MemberFacade.GetUniqueId(this.User.Identity.Name);
            }

            this.LoginSource = CookieManager.GetLoginSource();
        }
    }
}
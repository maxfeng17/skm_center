using System.Resources;
using LunchKingSite.Core.UI;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebLib.UI
{
    /// <summary>
    /// because ASP.NET 2.0 Resources are not exposed to outer assemblies, so we have to manually expose it, therefore pages
    /// that wish to let Presenter or other outside objects to access resource files should inherit from this class
    /// </summary>
    public abstract class LocalizedBasePage : BasePage, ILocalizedView
    {
        public ResourceManager Localization
        {
            get { return I18N.Phrase.ResourceManager; }
        }

        public ResourceManager Message
        {
            get { return I18N.Message.ResourceManager; }
        }
    }
}

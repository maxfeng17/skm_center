﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;

namespace LunchKingSite.WebLib.UI
{
    public interface IExpireNotice
    {
        /// <summary>
        /// 此 user control 的模式, 決定 render 出的 HTML
        /// </summary>
        ExpireNoticeMode ControlMode { get; set; }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        string SellerName { get; set; }

        /// <summary>
        /// 賣家結束營業日
        /// </summary>
        DateTime? SellerCloseDownDate { get; set; }

        /// <summary>
        /// 分店結束營業日 - key: 分店名稱 value: 分店結束營業日
        /// </summary>
        IDictionary<string, DateTime> StoreCloseDownDates { get; set; }

        /// <summary>
        /// 憑證原始使用截止日
        /// </summary>
        DateTime DealOriginalExpireDate { get; set; }

        /// <summary>
        /// 檔次調整截止日
        /// </summary>
        DateTime? DealChangedExpireDate { get; set; }


        /// <summary>
        /// 分店調整截止日 - key: 分店名稱 value: 分店調整過的截止日
        /// </summary>
        IDictionary<string, DateTime> StoreChangedExpireDates { get; set; }
    }

    public enum ExpireNoticeMode
    {
        WebPpon,
        MobilePpon,
        WebPiinLife,
        MobilePiinLife
    }
}

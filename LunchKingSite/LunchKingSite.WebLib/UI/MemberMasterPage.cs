using System;
using System.Web.UI;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebLib.UI
{
    public class MemberMasterPage : MasterPage
    {
        protected override void OnInit(EventArgs e)
        {
            if (Page.User.Identity.IsAuthenticated == false)
            {
                return;
            }

            LoadMemberData();          

            base.OnInit(e);
        }

        private void LoadMemberData()
        {
            PponIdentity pponUser = Page.User.Identity as PponIdentity;
            if (pponUser == null)
            {
                pponUser = PponIdentity.Get(Page.User.Identity.Name);
            }
            this.MemberName = pponUser.DisplayName;
            this.MemberPic = pponUser.Pic;
        }

        public string MemberName { get;private set; }
        public string MemberPic { get;private set; }
    }
}
using System;
using System.Web.UI;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.UI
{
    [SwitchableHttps]
    public abstract class BasePage : System.Web.UI.Page
    {
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected override void OnInit(EventArgs e)
        {
            if (config.SwitchableHttpsEnabled && Request.IsSecureConnection == false)
            {
                SwitchableHttpsAttribute secureAttribute = (SwitchableHttpsAttribute)
                    Attribute.GetCustomAttribute(GetType(), typeof(SwitchableHttpsAttribute));
                if (secureAttribute != null)
                {
                    //Build your https://Secure.xxx.com url
                    UriBuilder httpsUrl = new UriBuilder(Request.Url) { Scheme = Uri.UriSchemeHttps, Port = 443 };
                    Response.Redirect(httpsUrl.Uri.ToString());
                }
            }
            base.OnInit(e);
        }
    }

    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
    public sealed class SwitchableHttpsAttribute : Attribute
     { 
    }
}

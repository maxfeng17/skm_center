using System.Web.UI.WebControls;

namespace LunchKingSite.Core.UI
{
    public abstract class BaseCustomControl : WebControl
    {
        protected string _imageBaseUrl = null;
        [System.ComponentModel.Browsable(true)]
        public string ImageBaseUrl
        {
            get { return _imageBaseUrl; }
            set
            {
                _imageBaseUrl = ResolveUrl(value);
                if (_imageBaseUrl[_imageBaseUrl.Length - 1] != '/')
                    _imageBaseUrl += "/";
            }
        }

    }
}

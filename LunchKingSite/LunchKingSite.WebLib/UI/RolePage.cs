﻿using System;
using LunchKingSite.BizLogic.Facade;
using System.Web;
using System.Web.Security;
using LunchKingSite.Core.Component;

namespace LunchKingSite.Core.UI
{
    public abstract class RolePage : BasePage
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                if (config.EnablePageAccessControl)
                {
                    if (!CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Read))
                    {
                        CommonFacade.AddAudit(Guid.Empty, AuditType.RolePage, HttpContext.Current.Request.Url.LocalPath, User.Identity.Name, true);
                        Response.Redirect("~/default.aspx");
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Web;
using LunchKingSite.Core;
using log4net;
using LunchKingSite.Core.Component;
using System.Web.Security;

namespace LunchKingSite.WebLib.WebModules
{
    /// <summary>
    /// https://localhost/defense/black?ip=64.56.122.50
    /// https://localhost/defense/list
    /// </summary>
    public class DefenseModule : IHttpModule
    {
        protected enum LoginServeBy
        {
            NotFound,
            LoginPage,
            WebService,
            Any,
        }

        protected class LoginInfo
        {
            public string UserName { get; set; }
            public string Password { get; set; }
            public DateTime LoginTime { get; set; }
            public LoginServeBy Service { get; set; }
            public string ReqPath { get; set; }

            public override string ToString()
            {
                if (string.IsNullOrEmpty(ReqPath))
                {
                    return string.Format("{0},{1}:{2} / {3}",
                        this.LoginTime.ToString("HH:mm:ss"),
                        Service.ToString()[0],
                        this.UserName,
                        this.Password);
                }
                else
                {
                    return string.Format("{0},{1}: {2}",
                        this.LoginTime.ToString("HH:mm:ss"),
                        Service.ToString()[0],
                        this.ReqPath);
                }
            }
        }

        protected enum HttpMethodMode
        {
            GET,
            POST,
            BOTH
        }

        protected class ProtectivePage
        {
            public LoginServeBy ServeBy { get; set; }
            public string Page { get; set; }
            public HttpMethodMode Method { get; set; }
            public bool LikeMatch { get; set; }

            public bool Match(string url, string method)
            {
                if (Method == HttpMethodMode.GET && method != "GET")
                {
                    return false;
                }
                if (Method == HttpMethodMode.POST && method != "POST")
                {
                    return false;
                }
                if (LikeMatch)
                {
                    return url.Contains(Page, StringComparison.OrdinalIgnoreCase);
                }
                return Page.Equals(url, StringComparison.OrdinalIgnoreCase);
            }

            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                ProtectivePage castObj = obj as ProtectivePage;
                if (castObj == null) return false;
                return castObj.GetHashCode().Equals(this.GetHashCode());
            }

            public override int GetHashCode()
            {
                if (string.IsNullOrEmpty(this.Page))
                {
                    return 0;
                }
                return this.Page.GetHashCode();
            }

            public override string ToString()
            {
                return string.Format("{0}, {1}", this.Page, this.Method);
            }
        }

        protected enum DangerousLevel
        {
            No,
            Info,
            Warning,
            Fatal
        }

        public class BlackList
        {
            const string _BLACK_LIST_KEY = "DefenseModule::BlackList";
            private static BlackList _instance = new BlackList();
            private static object shareLock = new object();
            private BlackList()
            {
                timer.Elapsed += Timer_Elapsed;
                timer.AutoReset = false;
                timer.Start();
                RefreshData();
            }

            public static BlackList Current
            {
                get
                {
                    return _instance;
                }
            }

            private Timer timer = new Timer(60 * 1000);            

            private ConcurrentDictionary<string, BlackIpInfo> blackIps = new ConcurrentDictionary<string, BlackIpInfo>();

            public bool ShareList { get; set; }
            private static object thisShareLock = new object();

            private void Timer_Elapsed(object sender, ElapsedEventArgs e)
            {
                RefreshData();
            }

            private void RefreshData()
            {
                if (ShareList == false)
                {
                    return;
                }
                try
                {
                    lock (thisShareLock)
                    {
                        ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
                        List<BlackIpInfo> cacheItems = cache.Get<List<BlackIpInfo>>(_BLACK_LIST_KEY);
                        ConcurrentDictionary<string, BlackIpInfo> newOne = new ConcurrentDictionary<string, BlackIpInfo>();
                        if (cacheItems != null)
                        {
                            foreach (BlackIpInfo info in cacheItems)
                            {
                                newOne.TryAdd(info.Ip, info);
                            }
                        }
                        blackIps = newOne;
                    }
                }
                finally
                {
                    timer.Start();
                }
            }

            public KeyValuePair<string, BlackIpInfo>[] ToArray()
            {
                return blackIps.ToArray();
            }

            public void Remove(string removedIp)
            {
                BlackIpInfo ipinfo;
                blackIps.TryRemove(removedIp, out ipinfo);

                if (ShareList)
                {
                    lock (thisShareLock)
                    {
                        ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
                        var cacheItems = cache.Get<List<BlackIpInfo>>(_BLACK_LIST_KEY);
                        if (cacheItems != null && cacheItems.Count > 0)
                        {
                            cacheItems.RemoveAll(t => t.Ip == removedIp);
                        }
                        if (cacheItems == null)
                        {
                            cacheItems = new List<BlackIpInfo>();
                        }
                        cache.Set(_BLACK_LIST_KEY, cacheItems, TimeSpan.FromDays(1));
                    }
                }

            }

            public void Clear()
            {
                blackIps.Clear();
            }

            public bool TryGetValue(string ip, out BlackIpInfo ipInfo)
            {
                return blackIps.TryGetValue(ip, out ipInfo);
            }

            public void Add(string ip, DateTime unlockTime, bool showFriendlyPage)
            {
                var blackInfo = new BlackIpInfo
                {
                    Ip = ip,
                    ShowFriendlyPage = showFriendlyPage,
                    UnlockTime = unlockTime
                };
                blackIps.TryAdd(ip, blackInfo);
                if (ShareList)
                {
                    ICacheProvider cache = ProviderFactory.Instance().GetProvider<ICacheProvider>();
                    lock (shareLock)
                    {                        
                        var cacheItems = cache.Get<List<BlackIpInfo>>(_BLACK_LIST_KEY);
                        if (cacheItems == null)
                        {
                            cacheItems = new List<BlackIpInfo>();
                        }
                        cacheItems.RemoveAll(t => t.Ip == ip);
                        cacheItems.Add(blackInfo);
                        cache.Set(_BLACK_LIST_KEY, cacheItems, TimeSpan.FromDays(1));
                    }
                }
            }
        }

        protected interface IDetective
        {
            void AddClue(string ip, LoginServeBy serveBy, string reqPath);
            DangerousLevel CheckDangerous(string ip);
            void Report(string ip, DangerousLevel dangerousLevel);
            void Reset(string ip);
        }

        protected class LoginDetective : IDetective
        {
            private static ConcurrentDictionary<string, List<LoginInfo>> loginIps =
                new ConcurrentDictionary<string, List<LoginInfo>>();

            private readonly HttpContext _context;

            public LoginDetective(HttpContext context)
            {
                this._context = context;
            }

            public DangerousLevel CheckDangerous(string ip)
            {
                //10分鍾內登10次，通知
                //10分鍾內登30次，封鎖
                DateTime alertTime = DateTime.Now.AddMinutes(-10);
                int alertLimit = 10;
                int blockLimit = 30;

                List<LoginInfo> infos = loginIps[ip];                
                lock (infos)
                {
                    var recentInfos = infos.Where(t => t.LoginTime > alertTime).ToList();
                    if (recentInfos.Count < alertLimit)
                    {
                        return DangerousLevel.No;
                    }
                    if (recentInfos.Count >= alertLimit && recentInfos.Count < blockLimit)
                    {
                        int userNameCount = GetUserCountForAttemptToLogin(recentInfos);
                        if (userNameCount <= 3)
                        {
                            return DangerousLevel.No;
                        }
                        return DangerousLevel.Info;
                    }
                    if (recentInfos.Count >= blockLimit)
                    {
                        if (GetUserCountForAttemptToLogin(recentInfos) <= 5)
                        {
                            return DangerousLevel.Warning;
                        }
                        return DangerousLevel.Fatal;
                    }

                    return DangerousLevel.No;
                }
            }

            private int GetUserCountForAttemptToLogin(List<LoginInfo> infos)
            {
                HashSet<string> users = new HashSet<string>();
                foreach (LoginInfo info in infos)
                {
                    if (users.Contains(info.UserName) == false)
                    {
                        users.Add(info.UserName);
                    }
                }
                return users.Count;
            }

            public void AddClue(string ip, LoginServeBy serveBy, string reqPath)
            {
                var infos = loginIps.GetOrAdd(ip, new List<LoginInfo>());
                lock (infos)
                {
                    if (serveBy == LoginServeBy.LoginPage)
                    {
                        infos.Add(new LoginInfo
                        {
                            UserName = _context.Request.Form["txtEmail"],
                            Password = _context.Request.Form["txtPassword"],
                            LoginTime = DateTime.Now,
                            Service = LoginServeBy.LoginPage
                        });
                    }
                    else if (serveBy == LoginServeBy.WebService)
                    {
                        infos.Add(new LoginInfo
                        {
                            UserName = _context.Request["userName"],
                            Password = _context.Request["password"],
                            LoginTime = DateTime.Now,
                            Service = LoginServeBy.WebService
                        });
                    }
                }
            }

            public void Report(string ip, DangerousLevel dangerousLevel)
            {
                StringBuilder sb = new StringBuilder();
                if (dangerousLevel == DangerousLevel.Info)
                {
                    sb.AppendFormat("來自 {0} 多次登入，提醒注意\r\n", ip);
                } else
                {
                    sb.AppendFormat("來自 {0} 多次登入，IP暫時封鎖，請多留意\r\n", ip);
                }
                sb.AppendFormat("{0} 相關資訊:\r\n", ip);
                List<LoginInfo> infos = loginIps[ip];
                int rank = 0;
                lock (infos)
                {
                    foreach (LoginInfo info in infos)
                    {
                        rank++;
                        sb.AppendFormat("{0} {1}\r\n", 
                            rank.ToString().PadLeft(2, '0'), info);
                    }
                }
                if (dangerousLevel == DangerousLevel.Info)
                {
                    LogManager.GetLogger(LineAppender._LINE).Info(sb.ToString());
                }
                else if (dangerousLevel == DangerousLevel.Warning)
                {
                    logger.Warn(sb.ToString());
                }
                else if (dangerousLevel == DangerousLevel.Fatal)
                {
                    logger.WarnFormat("【嚴重】{0}", sb);
                }                
            }

            public void Reset(string ip)
            {
                List<LoginInfo> infos;
                loginIps.TryRemove(ip, out infos);
            }
        }

        protected class DDOSDetective : IDetective
        {
            private static ConcurrentDictionary<string, List<LoginInfo>> loginIps =
                new ConcurrentDictionary<string, List<LoginInfo>>();

            public void AddClue(string ip, LoginServeBy serveBy, string reqPath)
            {
                var infos = loginIps.GetOrAdd(ip, new List<LoginInfo>());
                lock (infos)
                {
                    infos.Add(new LoginInfo
                    {
                        LoginTime = DateTime.Now,
                        ReqPath = reqPath
                    });
                }
            }

            public DangerousLevel CheckDangerous(string ip)
            {
                var recentInfos = new List<LoginInfo>();
                const int trySecondsLimit = 30; //30秒
                const int tryCountLimit = 300;

                List<LoginInfo> infos = loginIps[ip];
                lock (infos)
                {
                    if (infos.Count < tryCountLimit)
                    {
                        return DangerousLevel.No;
                    }
                    //最近(300次)登入動作，是否發生在(30秒)內
                    int startPos = infos.Count - tryCountLimit;
                    int endPos = infos.Count - 1;
                    DateTime startTime = infos[startPos].LoginTime;
                    DateTime endTime = infos[endPos].LoginTime;
                    if ((endTime - startTime).TotalSeconds < trySecondsLimit)
                    {
                        return DangerousLevel.Fatal;
                    }
                    return DangerousLevel.No;
                }
            }

            public void Report(string ip, DangerousLevel dangerousLevel)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendFormat("偵測到來自 {0} 的挑釁，暫時封鎖，請多留意\r\n", ip);
                sb.AppendFormat("{0} 嘗試連到的頁面如下:\r\n", ip);
                List<LoginInfo> infos = loginIps[ip];
                int rank = 0;
                lock (infos)
                {
                    foreach (LoginInfo info in infos)
                    {
                        rank++;
                        sb.AppendFormat("{0}.{1}\r\n", rank, info);
                    }
                    if (dangerousLevel == DangerousLevel.Fatal)
                    {
                        logger.WarnFormat("【DDOS】{0}", sb);
                    }
                }
            }

            public void Reset(string ip)
            {
                List<LoginInfo> infos;
                loginIps.TryRemove(ip, out infos);
            }
        }

        private static ILog logger = LogManager.GetLogger(typeof(DefenseModule));
        private static HashSet<ProtectivePage> protectivePages = new HashSet<ProtectivePage>();

        public class BlackIpInfo
        {
            public string Ip { get; set; }
            public DateTime UnlockTime { get; set; }
            public bool ShowFriendlyPage { get; set; }
        }

        private static BlackList blackList = BlackList.Current;
        private static readonly TimeSpan BlockTimeLength = new TimeSpan(2, 0, 0);
        private static readonly TimeSpan BlockShortTimeLength = new TimeSpan(0, 30, 0);

        const string _ERROR_PAGE = "/error";

        private static System.Timers.Timer resetTimer;

        static DefenseModule()
        {
            //pez/fb 串接註冊登入
            protectivePages.Add(new ProtectivePage
            {
                ServeBy = LoginServeBy.LoginPage,
                Page = "confirmmember.aspx",
                LikeMatch = true,
                Method = HttpMethodMode.POST
            });
            //登入頁
            protectivePages.Add(new ProtectivePage
            {
                ServeBy = LoginServeBy.LoginPage,
                Page = "/m/login",
                LikeMatch = true,
                Method = HttpMethodMode.POST
            });

            //手機略過
            protectivePages.Add(new ProtectivePage
            {
                ServeBy = LoginServeBy.WebService,
                Page = "ContactLogin",
                LikeMatch = true,
                Method = HttpMethodMode.BOTH
            });

            resetTimer = new System.Timers.Timer
            {
                AutoReset = false,
                Interval = 60000
            };
            resetTimer.Start();

            resetTimer.Elapsed += delegate(object sender, System.Timers.ElapsedEventArgs e)
            {
                List<string> removedList = new List<string>();
                foreach (var pair in blackList.ToArray())
                {
                    //浸水桶時間到
                    if (DateTime.Now > pair.Value.UnlockTime)
                    {
                        removedList.Add(pair.Key);
                    }
                }
                foreach (string removedItem in removedList)
                {
                    blackList.Remove(removedItem);
                    logger.InfoFormat("解開封鎖 {0}", removedItem);
                }
                resetTimer.Start();
            };
        }

        public void Dispose()
        {
        }

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += context_AuthenticateRequest;
        }

        void context_AuthenticateRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this);
            try
            {
                HttpContext context = ((HttpApplication)sender).Context;
                //for elmah
                if (context.Request.Url.LocalPath.Contains(_ERROR_PAGE))
                {
                    return;
                }

                if (Helper.IsManagedHandler() == false)
                {
                    return;
                }

                if (context.Request.Url.LocalPath == "/defense/list")
                {
                    context.Response.ContentType = "text/plain";
                    context.Response.Write("封鎖的IP:\r\n");
                    foreach (var pair in blackList.ToArray())
                    {
                        string msg = string.Format("{0} 於 {1} 解鎖\r\n",
                            pair.Key, pair.Value.UnlockTime.ToString("MM/dd HH:mm"));
                        context.Response.Write(msg);
                    }
                    context.Response.End();
                }
                else if (context.Request.Url.LocalPath == "/defense/black")
                {
                    bool isAdmin = Roles.IsUserInRole("Administrator");
                    if (isAdmin == false)
                    {
                        context.Response.StatusCode = 403;
                        context.Response.End();
                        return;
                    }
                    string blackIp = context.Request.QueryString["ip"];
                    if (string.IsNullOrEmpty(blackIp) == false)
                    {
                        BlackList.Current.Add(blackIp, DateTime.Now.AddDays(1), false);
                    }
                    context.Response.ContentType = "text/plain";                    
                    context.Response.Write(blackIp + " is on the blacklist now.\r\n");
                    context.Response.End();
                    return;
                }

                ISysConfProvider config = ProviderFactory.Instance().GetConfig();
                blackList.ShareList = config.DefenseModuleShareBlackList;
                blackList.ShareList = true;

                string ip = Helper.GetClientIP(simple: true);

                if (WhiteIps.Match(ip))
                {
                    return;
                }
                BlackIpInfo blackIpInfo;
                if (blackList.TryGetValue(ip, out blackIpInfo))
                {
                    logger.InfoFormat("黑名單:阻擋{0}訪問{1}", ip, context.Request.RawUrl);
                    if (blackIpInfo.ShowFriendlyPage == false)
                    {
                        throw new HttpException((int)HttpStatusCode.Forbidden, "短時間內嘗試太多次登入了，請休息一陣子後再試");
                    }
                    else
                    {
                        context.Response.Redirect("/error.aspx?action=403.6");
                    }
                    return;
                }
                
                if (config.DefenseModuleEnabledLevel >= 1)
                {
                    CheckManyLogin(context, ip);
                    if (config.DefenseModuleEnabledLevel >= 2)
                    {
                        CheckDDOS(context, ip);
                    }
                }
            }
            finally
            {
                ModuleLogger.MarkEnd(this);
            }
        }

        private void CheckDDOS(HttpContext context, string ip)
        {
            string url = context.Request.RawUrl;
            IDetective detective = new DDOSDetective();
            detective.AddClue(ip, LoginServeBy.Any, url);
            DangerousLevel dangerousLevel = detective.CheckDangerous(ip);
            if (dangerousLevel != DangerousLevel.No)
            {
                //blackList.Add(ip, new BlackIpInfo {UnlockTime = DateTime.Now.Add(BlockTimeLength), ShowFriendlyPage = false});
                blackList.Add(ip, DateTime.Now.Add(BlockTimeLength), false );
                detective.Report(ip, dangerousLevel);
                detective.Reset(ip);
            }
        }

        private static void CheckManyLogin(HttpContext context, string ip)
        {
            string url = context.Request.RawUrl;
            LoginServeBy serveBy = LoginServeBy.NotFound;
            foreach (var page in protectivePages)
            {
                if (page.Match(url, context.Request.HttpMethod))
                {
                    serveBy = page.ServeBy;
                    break;
                }
            }

            if (serveBy != LoginServeBy.NotFound)
            {
                logger.DebugFormat("痕跡:{0}", url);

                IDetective detective = new LoginDetective(context);
                detective.AddClue(ip, serveBy, string.Empty);
                DangerousLevel dangerousLevel = detective.CheckDangerous(ip);
                if (dangerousLevel == DangerousLevel.No)
                {
                    //skip
                }
                else if (dangerousLevel == DangerousLevel.Info)
                {
                    detective.Report(ip, dangerousLevel);
                }
                else if (dangerousLevel == DangerousLevel.Warning)
                {
                    blackList.Add(ip, DateTime.Now.Add(BlockShortTimeLength), true);
                    detective.Report(ip, dangerousLevel);
                    detective.Reset(ip);
                }
                else if (dangerousLevel == DangerousLevel.Fatal)
                {
                    blackList.Add(ip, DateTime.Now.Add(BlockTimeLength), true);
                    detective.Report(ip, dangerousLevel);
                    detective.Reset(ip);
                }
            }
        }

        public static void ClearBlackIps()
        {
            blackList.Clear();
        }

    }

    public static class WhiteIps
    {
        #region 白名單 from payeasy
        private static HashSet<string> ips = new HashSet<string>(new string[]
        {
            "61.67.148.32",
            "202.6.104.2",
            "60.248.185.172",
            "60.248.185.173",
            "60.248.185.174",
            "220.132.36.51",
            "61.14.165.2",
            "60.244.121.97",
            "220.128.63.116",
            "218.210.214.221",
            "220.228.158.231",
            "220.128.63.205",
            "60.248.185.176",
            "203.112.84.139",
            "203.112.90.136",
            "203.112.90.138",
            "203.112.84.138",
            "203.67.36.2",
            "211.20.110.190",
            "220.135.250.109",
            "61.30.74.55",
            "220.128.63.105",
            "59.124.65.128",
            "220.135.250.124",
            "220.135.250.107",
            "218.211.224.185",
            "220.135.250.122",
            "210.202.75.225",
            "61.67.10.18",
            "218.210.60.194",
            "220.228.150.131",
            "220.228.150.132",
            "220.228.149.146",
            "220.228.149.150",
            "219.87.157.82",
            "219.87.157.86",
            "219.87.28.86",
            "219.87.28.2",
            "210.61.82.125",
            "210.61.82.119",
            "60.244.123.152",
            "60.244.123.129",
            "222.156.254.85",
            "59.124.5.97",
            "59.124.107.190",
            "122.146.11.186",
            "202.39.146.195",
            "202.79.203.43",
            "203.74.119.62",
            "210.80.95.80",
            "210.83.229.254",
            "211.72.248.93",
            "220.128.121.14",
            "220.128.230.151",
            "220.132.90.99",
            "220.135.250.111",
            "60.250.209.163",
            "60.251.154.220", // 13F
            "60.251.16.199",
            "61.14.165.44",
            "61.57.226.1",
            "61.30.74.241",
            "60.251.210.126",
            "111.251.193.42",
            "111.251.211.197",
            "114.32.180.185",
            "114.32.180.186",
            "114.32.180.187",
            "114.32.181.64",
            "114.32.218.71",
            "114.32.89.43",
            "114.37.132.196",
            "114.37.136.209",
            "114.37.137.52",
            "114.37.30.46",
            "114.43.33.107",
            "114.43.33.11",
            "114.43.35.20",
            "114.43.40.40",
            "114.43.42.237",
            "114.43.42.8",
            "114.43.44.221",
            "114.43.44.91",
            "118.171.116.16",
            "122.116.154.142",
            "122.116.199.179",
            "122.116.209.75",
            "122.117.162.167",
            "122.117.83.199",
            "122.121.202.172",
            "122.146.114.158",
            "122.146.126.126",
            "122.146.2.130",
            "122.146.20.2",
            "122.146.201.162",
            "122.146.24.2",
            "122.146.30.2",
            "122.147.0.19",
            "122.147.144.226",
            "123.193.106.6",
            "124.10.77.67",
            "124.219.2.29",
            "124.9.11.251",
            "167.191.252.7",
            "113.196.132.50",
            "192.11.225.111",
            "192.11.225.112",
            "199.197.152.1",
            "202.132.100.81",
            "202.133.252.2",
            "202.154.218.253",
            "202.39.58.2",
            "203.66.10.113",
            "203.66.206.201",
            "203.66.245.250",
            "203.66.251.14",
            "203.67.34.209",
            "203.69.223.54",
            "203.69.97.52",
            "203.73.128.163",
            "203.74.180.10",
            "210.202.224.1",
            "210.208.212.253",
            "210.243.144.17",
            "210.243.149.23",
            "210.243.228.49",
            "210.243.232.29",
            "210.59.146.58",
            "210.66.111.20",
            "210.66.145.74",
            "210.66.147.131",
            "210.66.185.109",
            "210.80.67.18",
            "210.80.72.178",
            "210.80.88.2",
            "211.20.103.2",
            "211.21.127.208",
            "211.21.154.218",
            "211.21.187.178",
            "211.22.58.60",
            "211.22.7.250",
            "211.23.86.187",
            "211.72.229.163",
            "211.78.172.186",
            "211.78.172.2",
            "211.79.9.2",
            "218.210.104.18",
            "218.210.124.200",
            "218.210.172.200",
            "218.210.94.5",
            "218.211.224.193",
            "218.211.227.156",
            "218.211.243.146",
            "218.211.64.191",
            "218.32.81.100",
            "218.32.81.99",
            "219.87.139.83",
            "219.87.144.214",
            "219.87.147.157",
            "219.87.152.131",
            "219.87.154.190",
            "219.87.157.139",
            "219.87.157.67",
            "219.87.177.194",
            "219.87.79.205",
            "219.87.79.206",
            "219.87.80.61",
            "219.87.93.2",
            "220.128.117.19",
            "220.128.123.202",
            "220.128.143.52",
            "220.128.164.194",
            "220.128.190.162",
            "220.128.225.178",
            "220.128.228.118",
            "220.128.228.160",
            "220.128.229.127",
            "220.128.236.71",
            "220.128.56.125",
            "220.128.63.106",
            "220.130.149.223",
            "220.130.168.160",
            "220.130.168.162",
            "220.130.183.68",
            "220.130.183.74",
            "220.130.193.113",
            "220.130.197.25",
            "220.130.50.251",
            "220.130.6.64",
            "220.132.135.130",
            "220.132.135.131",
            "220.135.212.152",
            "220.228.148.13",
            "220.228.148.226",
            "220.228.149.46",
            "220.228.150.66",
            "220.228.151.221",
            "220.228.225.34",
            "220.228.56.41",
            "220.229.32.61",
            "220.229.42.33",
            "220.229.43.153",
            "222.156.254.82",
            "59.120.103.209",
            "59.120.109.150",
            "59.120.15.27",
            "59.120.160.3",
            "59.120.182.50",
            "59.120.184.171",
            "59.120.188.28",
            "59.120.190.23",
            "59.120.214.31",
            "59.120.241.214",
            "59.120.39.133",
            "59.120.53.118",
            "59.120.56.194",
            "59.120.63.10",
            "59.120.67.245",
            "59.120.69.34",
            "59.124.1.64",
            "59.124.129.194",
            "59.124.147.220",
            "59.124.151.61",
            "59.124.155.152",
            "59.124.155.158",
            "59.124.155.159",
            "59.124.176.219",
            "59.124.181.10",
            "59.124.185.115",
            "59.124.186.194",
            "59.124.194.131",
            "59.124.205.73",
            "59.124.43.200",
            "59.124.5.232",
            "59.124.58.98",
            "59.125.1.133",
            "59.125.123.121",
            "59.125.211.36",
            "59.125.213.80",
            "59.125.41.218",
            "59.125.82.148",
            "59.126.128.120",
            "60.199.65.125",
            "60.244.116.49",
            "60.248.0.208",
            "60.248.166.61",
            "60.248.181.42",
            "60.248.187.109",
            "60.248.40.18",
            "60.248.80.54",
            "60.248.84.61",
            "60.248.88.106",
            "60.249.10.29",
            "60.249.136.193",
            "60.249.157.226",
            "60.249.220.38",
            "60.249.237.35",
            "60.249.238.102",
            "60.249.51.162",
            "60.249.70.34",
            "60.249.72.146",
            "60.250.0.66",
            "60.250.140.195",
            "60.250.143.149",
            "60.250.154.118",
            "60.250.164.201",
            "60.250.166.194",
            "60.250.209.162",
            "60.250.250.131",
            "60.250.251.164",
            "60.250.41.162",
            "60.250.46.230",
            "60.250.46.242",
            "60.250.53.6",
            "60.250.59.162",
            "60.250.89.181",
            "60.251.192.93",
            "60.251.197.118",
            "60.251.236.175",
            "60.251.247.2",
            "60.251.31.206",
            "60.251.55.226",
            "61.14.177.226",
            "61.218.107.173",
            "61.218.107.206",
            "61.218.59.3",
            "61.219.108.109",
            "61.219.108.145",
            "61.220.176.169",
            "61.220.232.118",
            "61.220.240.179",
            "61.220.253.253",
            "61.220.26.43",
            "61.220.27.217",
            "61.222.121.126",
            "61.222.78.190",
            "61.222.86.33",
            "61.30.11.208",
            "61.30.139.242",
            "61.30.141.123",
            "61.30.74.205",
            "61.56.159.27",
            "61.57.149.215",
            "61.57.159.1",
            "61.62.159.128",
            "61.66.17.92",
            "61.66.243.96",
            "61.67.145.125",
            "61.71.104.161",
            "123.193.111.125",
            "122.147.143.237",
            "122.147.143.241",
            "122.158.60.18",
            "122.158.60.216",
            "122.158.60.78",
            "211.22.125.39",
            "220.180.227.35",
            "61.64.61.129",
            "175.98.146.2",
            "103.10.47",
            "175.98.146.34",
            "61.30.74.241",
            "192.163.20.231",
            "59.120.191.244",
            "113.196.35.70",
            "211.75.6.121",
            "59.125.218.25",
            "124.219.66.1",
            "210.242.67.115",
            "68.255.220.125",
            "61.14.165.80",
            "114.32.200.94",
            "211.78.88.19",
            "122.147.178.102",
            "219.87.82.135",
            "61.67.221.102",
            "203.70.132.32",
            "218.211.152.216",
            "61.58.41.99",
            "210.59.146.56",
            "211.72.117.55",
            "122.116.196.71",
            "220.132.230.38",
            "60.199.248.193",
            "202.167.250.43",
            "114.25.224.158",
            "219.87.154.182",
            "59.125.101.61",
            "59.124.251.227",
            "218.210.124.195",
            "210.59.249.84",
            "61.218.221.3",
            "60.251.213.5",
            "199.197.159.1",
            "220.130.50.63",
            "60.251.187.178",
            "61.221.50.55",
            "211.20.180.125",
            "60.251.192.95",
            "219.87.146.98",
            "218.210.94.4",
            "59.120.66.82",
            "211.21.127.209",
            "203.66.246.4",
            "122.146.71.32",
            "210.242.233.210",
            "59.124.68.58",
            "59.120.3.190",
            "218.210.172.204",
            "60.250.154.119",
            "122.146.57.196",
            "124.219.31.93",
            "61.218.53.60",
            "210.242.62.139",
            "211.20.4.82",
            "61.218.89.186",
            "60.249.237.59",
            "113.196.35.68",
            "122.116.24.233",
            "114.34.222.199",
            "61.63.73.9", 
            "210.59.219.101", //宇匯
            "210.59.219.102", //宇匯
            "210.59.219.103", //宇匯
            "210.59.219.104", //宇匯
            "210.59.219.105", //宇匯
            "60.251.43.244", //台灣大洗衣
            "60.251.43.245", //台灣大洗衣
            "124.219.47.97", //台灣大洗衣
            "124.219.47.98", //台灣大洗衣
            "124.219.47.99", //台灣大洗衣
            "124.219.47.100", //台灣大洗衣
            "123.51.223.4", //ETU
            "220.133.105.119", //ETU
            "60.248.96.211", //ETU
            "210.63.38.209", //ETU
        });
        #endregion

        public static bool Match(string ip)
        {
            return ips.Contains(ip);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.WebModules
{
    public class CountTimeModule : IHttpModule
    {
        private const string _ITEM_KEY = "CountTimeModule::TimeStamp";
        private const string _ERROR_PAGE = "/error";
        private const string _CT_PAGE = "ct.axd";
        public const double _TIMEOUT_SECONDS = 60;
        public static DateTime StartTime = DateTime.Now;

        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;            
            context.EndRequest += context_EndRequest;
            context.Error += context_Error;
        }

        void context_Error(object sender, EventArgs e)
        {
            
        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this, "BeginRequest");
            try
            {
                HttpContext context = ((HttpApplication)sender).Context;
                //for elmah
                if (context.Request.Url.LocalPath.Contains(_ERROR_PAGE))
                {
                    return;
                }
                //for count time
                if (context.Request.Url.LocalPath.Contains(_CT_PAGE))
                {
                    if (context.Request["do"] == "reset")
                    {
                        RequestCountTimeManager.Clear();
                        StartTime = DateTime.Now;
                        context.Response.Redirect("ct.axd");
                    }
                    return;
                }

                if (Helper.IsManagedHandler() == false)
                {
                    return;
                }

                context.Items[_ITEM_KEY] = DateTime.Now;
            }
            finally
            {
                ModuleLogger.MarkEnd(this, "BeginRequest");
            }
        }

        void context_EndRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this, "EndRequest");
            try
            {
                HttpContext context = ((HttpApplication)sender).Context;
                if (context.Items[_ITEM_KEY] == null)
                {
                    return;
                }

                DateTime timeStart = (DateTime)context.Items[_ITEM_KEY];
                TimeSpan elapsedTime = DateTime.Now - timeStart;
                string pageName = context.Request.Url.LocalPath;

                RequestCountTimeManager.Append(pageName, elapsedTime.TotalSeconds);
            }
            finally
            {
                ModuleLogger.MarkEnd(this, "EndRequest");
            }
        }

    }


    public static class RequestCountTimeManager
    {
        private static Dictionary<string, RequestCountTime> _indexer
            = new Dictionary<string, RequestCountTime>(StringComparer.OrdinalIgnoreCase);

        private static object thisLock = new object();

        public static void Append(string pageName, double elapsedSeconds)
        {
            lock (thisLock)
            {
                double updateElapsedSeconds = elapsedSeconds;
                if (_indexer.ContainsKey(pageName))
                {
                    if (elapsedSeconds >= CountTimeModule._TIMEOUT_SECONDS)
                    {
                        _indexer[pageName].TimeoutHits ++;
                    }
                    else
                    {
                        _indexer[pageName].Hits++;
                        _indexer[pageName].TotalSeconds += updateElapsedSeconds;
                        _indexer[pageName].AvgSeconds = _indexer[pageName].TotalSeconds / _indexer[pageName].Hits;
                    }
                }
                else
                {
                    if (elapsedSeconds >= CountTimeModule._TIMEOUT_SECONDS)
                    {
                        _indexer[pageName] = new RequestCountTime
                        {
                            PageName = pageName,
                            TimeoutHits = 1
                        };
                    }
                    else
                    {
                        _indexer[pageName] = new RequestCountTime
                        {
                            PageName = pageName,
                            Hits = 1,
                            TotalSeconds = elapsedSeconds,
                            AvgSeconds = elapsedSeconds
                        };
                    }
                }
            }
        }

        public static List<RequestCountTime> ToList(int pageSize, int min)
        {
            lock (thisLock)
            {
            if (min > 0)
            {
                    return
                        _indexer.Values.Where(t => t.Hits >= min)
                            .OrderByDescending(t => t.AvgSeconds)
                            .Take(pageSize)
                            .ToList();
            }
            else
            {
                return _indexer.Values.OrderByDescending(t => t.AvgSeconds).Take(pageSize).ToList();
            }
        }
        }

        public static void Clear()
        {
            lock (thisLock)
            {
                _indexer.Clear();
            }
        }
    }

    public class RequestCountTime
    {
        public string PageName { get; set; }
        public double TotalSeconds { get; set; }
        public int Hits { get;set; }
        public double AvgSeconds { get;set; }
        public int TimeoutHits { get;set; }
        public override string ToString()
        {
            return string.Format("{0:0.##}", AvgSeconds);
        }
    }


    public class CountTimeHandler : IHttpHandler
    {

        public bool IsReusable
        {
            get { return false; }
        }

        public void ProcessRequest(HttpContext context)
        {

            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            response.Clear();

            response.ContentType = MimeTypes.Html;
            int pageSize = 200;
            if (request["size"] != null)
            {
                pageSize = int.Parse(request["size"]);
            }
            int min = 0;
            if (request["min"] != null)
            {
                min = int.Parse(request["min"]);
            }
            response.Write(new BuildHtml().ToHtml(RequestCountTimeManager.ToList(pageSize, min)));

            response.Flush();
            response.End();
        }
    }

    public class BuildHtml
    {
        private string GetHitsContext(RequestCountTime item)
        {
            if (item.TimeoutHits > 0)
            {
                return string.Format("<span title='Hits: {0}, Timeout: {1}'>{0}*</span>", item.Hits, item.TimeoutHits);
            }
            return item.Hits.ToString();
        }
        public string CssStyle
        {
            get
            {
                return
@"
body {
	background: #fafafa url(http://jackrugile.com/images/misc/noise-diagonal.png);
	color: #444;
	font: 100%/30px 'Helvetica Neue', helvetica, arial, sans-serif;
	text-shadow: 0 1px 0 #fff;
}

strong {
	font-weight: bold; 
}

em {
	font-style: italic; 
}

table {
	background: #f5f5f5;
	border-collapse: separate;
	box-shadow: inset 0 1px 0 #fff;
	font-size: 12px;
	line-height: 24px;
	margin: 30px auto;
	text-align: left;
	width: 800px;
}	

th {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png), linear-gradient(#777, #444);
	border-left: 1px solid #555;
	border-right: 1px solid #777;
	border-top: 1px solid #555;
	border-bottom: 1px solid #333;
	box-shadow: inset 0 1px 0 #999;
	color: #fff;
  font-weight: bold;
	padding: 10px 15px;
	position: relative;
	text-shadow: 0 1px 0 #000;	
}

th:after {
	background: linear-gradient(rgba(255,255,255,0), rgba(255,255,255,.08));
	content: '';
	display: block;
	height: 25%;
	left: 0;
	margin: 1px 0 0 0;
	position: absolute;
	top: 25%;
	width: 100%;
}

th:first-child {
	border-left: 1px solid #777;	
	box-shadow: inset 1px 1px 0 #999;
}

th:last-child {
	box-shadow: inset -1px 1px 0 #999;
}

td {
	border-right: 1px solid #fff;
	border-left: 1px solid #e8e8e8;
	border-top: 1px solid #fff;
	border-bottom: 1px solid #e8e8e8;
	padding: 10px 15px;
	position: relative;
	transition: all 300ms;
}

td:first-child {
	box-shadow: inset 1px 0 0 #fff;
}	

td:last-child {
	border-right: 1px solid #e8e8e8;
	box-shadow: inset -1px 0 0 #fff;
}	

tr {
	background: url(http://jackrugile.com/images/misc/noise-diagonal.png);	
}

tr:nth-child(odd) td {
	background: #f1f1f1 url(http://jackrugile.com/images/misc/noise-diagonal.png);	
}

tr:last-of-type td {
	box-shadow: inset 0 -1px 0 #fff; 
}

tr:last-of-type td:first-child {
	box-shadow: inset 1px -1px 0 #fff;
}	

tr:last-of-type td:last-child {
	box-shadow: inset -1px -1px 0 #fff;
}
.ct-footer {
  position: fixed;
  bottom: 10px;
  right: 10px;
}
";
            }
        }
        public string ToHtml(List<RequestCountTime> items)
        {
            StringBuilder sbData = new StringBuilder();
            for (int i=0;i<items.Count;i++)
            {
                var item = items[i];
                sbData.AppendFormat(
                    @"        <tr>
          <td><strong>{0}</strong></td>
          <td>{1}</td>
          <td>{2}</td>
          <td>{3:0.##}</td>
        </tr>
", i + 1, item.PageName, GetHitsContext(item), item.AvgSeconds);
            }

            
            return string.Format(
      @"<!DOCTYPE HTML>
<html>
<head>
<meta charset='UTF-8'>
<meta name=viewport content='width=device-width, initial-scale=1.0' />
<title>CountTime 列表</title>
<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css'>
<style>
{0}
</style>
<head>

<body>
<div class='container'>
    <div class='text-center'>
        <h1>{1}個最慢的頁面({2})</h1>
    </div>
    <table>
      <thead>
        <tr>
          <th>排名</th>
          <th>頁面</th>          
          <th>次數</th>
          <th>平均秒數</th>
        </tr>
      </thead>
      <tbody>
        {3}
      </tbody>
    </table>
    <div class='text-center ct-footer'>
        <a href='ct.axd?do=reset'>clear</a>
    </div>
    <span style='position:absolute; left:0px; top:0px;color:#ccc;'>{4}</span>
</div>
</body>
</html>
", CssStyle, items.Count, Dns.GetHostName(), sbData.ToString(),
    CountTimeModule.StartTime.ToString("yyyy-MM-dd HH:mm:ss"));
        }
    }
}

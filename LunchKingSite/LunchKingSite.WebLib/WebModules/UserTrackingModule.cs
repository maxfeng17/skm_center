﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Model.QueueModels;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.QueueModels;
using LunchKingSite.WebLib.Component;
using System.IO;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.WebModules
{
    public class ModuleLogger
    {
        protected class TimePeriod
        {
            public string Name { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public object ElapsedSeconds
            {
                get { return (EndTime - StartTime).TotalSeconds.ToString("0.##"); }
            }

            public override string ToString()
            {
                return string.Format("{0}: {1}", Name, ElapsedSeconds);
            }
        }

        private static ILog logger = LogManager.GetLogger(typeof(ModuleLogger));

        private static List<TimePeriod> GetOrNewTimePeriods()
        {
            List<TimePeriod> timePeriods = HttpContext.Current.Items["ModuleLoggerList"] as List<TimePeriod>;
            if (timePeriods == null)
            {
                timePeriods = new List<TimePeriod>();
                HttpContext.Current.Items["ModuleLoggerList"] = timePeriods;
            }
            return timePeriods;
        }

        public static void MarkStart(IHttpModule module, string subTitle = "")
        {
            string itemKey = module.GetType().Name;
            if (string.IsNullOrEmpty(subTitle) == false)
            {
                itemKey += "-" + subTitle;
            }
            var items = GetOrNewTimePeriods();
            var item = items.FirstOrDefault(t => t.Name == itemKey);
            if (item != null)
            {
                logger.Info(itemKey + " 竟然已存在");
            }
            items.Add(new TimePeriod
            {
                Name = itemKey,
                StartTime = DateTime.Now,
                EndTime = DateTime.MinValue
            });
        }
        
        public static void FlushLog(bool isDebugLevel, Guid userTrackingGuid, double elapsedSecs)
        {
            var items = GetOrNewTimePeriods();
            if (items.Count == 0)
            {
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.AppendLine(userTrackingGuid.ToString());
            sb.AppendLine(HttpContext.Current.Request.RawUrl);
            sb.AppendLine(elapsedSecs.ToString("0.###"));
            sb.AppendLine("========");
            foreach (var item in items)
            {
                sb.AppendLine(item.ToString());
            }
            if (isDebugLevel)
            {
                logger.Debug(sb.ToString());
            }
            else
            {
                logger.Info(sb.ToString());
            }
        }

        public static void MarkEnd(IHttpModule module, string subTitle = "")
        {
            string itemKey = module.GetType().Name;
            if (string.IsNullOrEmpty(subTitle) == false)
            {
                itemKey += "-" + subTitle;
            }
            var items = GetOrNewTimePeriods();
            var item = items.FirstOrDefault(t => t.Name == itemKey);
            if (item == null)
            {
                logger.Info(itemKey + " 竟然為NULL");
            }
            else
            {
                item.EndTime = DateTime.Now;
            }
            HttpContext.Current.Items["ModuleLoggerList"] = items;
        }

    }

    public class UserTrackingModule : IHttpModule
    {
        private DateTime now;
        private Guid userTrackingGuid;

        private static readonly string sessionCookieName;
        private const string _ERROR_PAGE = "/error";
        static ILog logger = LogManager.GetLogger(typeof(UserTrackingModule));
        private StreamWatcher _watcher;

        static UserTrackingModule()
        {
            SessionStateSection section = (SessionStateSection)WebConfigurationManager.GetSection("system.web/sessionState");
            sessionCookieName = section.CookieName;
        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;
            context.EndRequest += context_EndRequest;
        }


        void context_BeginRequest(object sender, EventArgs e)
        {
            now = DateTime.Now;
            HttpApplication context = sender as HttpApplication;
            userTrackingGuid = Guid.NewGuid();
            CookieManager.SetVisitorIdentity();
            //這邊config的取得，不要移到外層， Module的初始化推測比Global裡初始ProviderFactory還早
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            string localPath = context.Request.Url.LocalPath.ToLower();
            if (config.EnableUserTrackingLogOutput &&
                (localPath.StartsWith("/api") || localPath.StartsWith("/webbervice")) &&
                context.Request.Url.LocalPath.StartsWith("/api/skm") == false)
            {
                _watcher = new StreamWatcher(context.Response.Filter);
                context.Response.Filter = _watcher;
            }
        }

        private void context_EndRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this);

            HttpApplication context = sender as HttpApplication;
            if (context == null)
            {
                return;
            }
            //for elmah
            if (context.Request.Url.LocalPath.Contains(_ERROR_PAGE))
            {
                return;
            }
            ////暫時不想記新光的api
            //if (context.Request.Url.LocalPath.StartsWith("/api/skm"))
            //{
            //    return;
            //}
            //這邊config的取得，不要移到外層， Module的初始化推測比Global裡初始ProviderFactory還早
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            if (config.EnableUserTrackingModule == false)
            {
                return;
            }

            double elapsedSecs = (DateTime.Now - now).TotalSeconds;

            string sessionId = string.Empty;
            if (context.Request.Cookies[sessionCookieName] != null)
            {
                sessionId = context.Request.Cookies[sessionCookieName].Value;
            }

            int userId = 0;
            if (context.User != null && context.User.Identity.IsAuthenticated)
            {
                PponIdentity identity = context.User.Identity as PponIdentity;
                if (identity != null)
                {
                    userId = identity.Id;
                }
                else
                {
                    userId = MemberFacade.GetUniqueId(context.User.Identity.Name);
                }
            }

            Guid? bid = null;
            Guid parseBid;
            object routeDataBid = context.Request.RequestContext.RouteData.Values["bid"];
            string requestPath = string.Empty;
            string queryString = string.Empty;
            if (routeDataBid != null)
            {
                System.Web.Routing.PageRouteHandler castHandler =
                    context.Request.RequestContext.RouteData.RouteHandler as System.Web.Routing.PageRouteHandler;
                if (castHandler != null)
                {
                    requestPath = castHandler.VirtualPath.TrimStart('~');
                }
                
                queryString = Helper.ConvertDictionaryToQueryString(context.Request.RequestContext.RouteData.Values);
                if (Guid.TryParse(routeDataBid.ToString(), out parseBid))
                {
                    bid = parseBid;
                }
            }
            else
            {
                requestPath = context.Request.Path;
                queryString = context.Request.QueryString.ToString();
            }
            if (bid == null)
            {
                string queryStringBid = context.Request.QueryString["bid"];
                if (queryStringBid != null)
                {
                    if (Guid.TryParse(queryStringBid, out parseBid))
                    {
                        bid = parseBid;
                    }
                }
            }
            if (bid == null)
            {
                object contextItemBid = Helper.GetContextItem(LkSiteContextItem.Bid);
                if (contextItemBid is Guid)
                {
                    bid = (Guid)contextItemBid;
                }
            }

            IMessageQueueProvider mqp = ProviderFactory.Instance().GetProvider<IMessageQueueProvider>();

            HttpCookie rsrcCookie = context.Request.Cookies[LkSiteCookie.ReferrerSourceId.ToString()];
            var userTracking = new UserTracking();
            userTracking.Guid = userTrackingGuid;
            userTracking.StartTime = DateTime.Now;
            userTracking.SessionId = sessionId;
            userTracking.Request = requestPath;
            userTracking.QueryString = queryString;
            userTracking.UserAgent = context.Request.UserAgent;
            userTracking.UserIp = context.Request.UserHostAddress;
            userTracking.RequestType = context.Request.RequestType;
            userTracking.UrlReferrer = context.Request.UrlReferrer == null ? string.Empty : context.Request.UrlReferrer.AbsoluteUri;
            userTracking.UserId = userId;
            userTracking.ElapsedSecs = elapsedSecs;
            userTracking.ServerBy = Environment.MachineName;
            userTracking.ReferrerSourceId = rsrcCookie == null ? string.Empty : rsrcCookie.Value;
            userTracking.DeviceType = (int)Helper.GetOrderFromType();
            userTracking.VisitorIdentity = CookieManager.GetVisitorIdentity();
            userTracking.Bid = bid;
            userTracking.IsHttps = context.Request.IsSecureConnection;

            if (config.EnableUserTrackingMQSend)
            {
                mqp.Send(UserTracking._QUEUE_NAME, userTracking);
            }
            if (logger.IsDebugEnabled)
            {
                StringBuilder sbLog = new StringBuilder();
                if (context.Request.HttpMethod == "GET")
                {
                    sbLog.AppendLine("GET " + context.Request.RawUrl);
                }
                else
                {
                    sbLog.AppendLine("POST " + context.Request.RawUrl);
                }
                sbLog.AppendLine(JsonConvert.SerializeObject(userTracking, Newtonsoft.Json.Formatting.Indented));

                sbLog.AppendLine("[HEADER]");
                string cookiePackage = string.Empty;
                foreach (string key in context.Request.Headers) {
                    if (key.Equals("Cookie", StringComparison.OrdinalIgnoreCase))
                    {
                        cookiePackage = context.Request.Headers["Cookie"];
                        continue;
                    }
                    sbLog.AppendFormat("{0}: {1}\r\n", key, context.Request.Headers[key]);
                }
                if (string.IsNullOrEmpty(cookiePackage) == false)
                {
                    sbLog.AppendLine("[COOKIE]");
                    sbLog.AppendLine(cookiePackage);
                }
                if (context.Request.HttpMethod == "POST")
                {
                    var inp = context.Request.InputStream;
                    using (var sr = new StreamReader(inp))
                    {
                        string postBody = sr.ReadToEnd();
                        sbLog.AppendLine("[POST]");
                        sbLog.AppendLine(postBody);
                    }
                }
                if (_watcher != null )
                {
                    sbLog.AppendLine("[OUTPUT]");
                    sbLog.AppendLine(_watcher.ToString());
                }
                
                logger.Debug(sbLog.ToString());
            }

            if (config.IsSetBuyTransaction)
            {
                var buyContextItem = context.Context.Items[LkSiteContextItem.BuyTransaction];
                if (buyContextItem != null)
                {
                    BuyTransactionModel buyTransaction = (BuyTransactionModel)buyContextItem;
                    buyTransaction.UserTrackingGuid = userTrackingGuid;
                    mqp.Send(BuyTransactionModel._QUEUE_NAME, buyTransaction);
                }
            }

            ModuleLogger.MarkEnd(this);
            if (elapsedSecs > 0.5)
            {
                ModuleLogger.FlushLog(isDebugLevel: false, userTrackingGuid: userTrackingGuid, elapsedSecs: elapsedSecs);
            }
            else
            {
                ModuleLogger.FlushLog(isDebugLevel: true, userTrackingGuid: userTrackingGuid, elapsedSecs: elapsedSecs);
            }
            
        }

        public void Dispose()
        {

        }
    }



    public class StreamWatcher : Stream
    {
        private Stream _base;
        private MemoryStream _memoryStream = new MemoryStream();

        public StreamWatcher(Stream stream)
        {
            _base = stream;
        }

        public override void Flush()
        {
            _base.Flush();
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            return _base.Read(buffer, offset, count);
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            _memoryStream.Write(buffer, offset, count);
            _base.Write(buffer, offset, count);
        }

        public override string ToString()
        {
            return Encoding.UTF8.GetString(_memoryStream.ToArray());
        }

        #region Rest of the overrides
        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanWrite
        {
            get { throw new NotImplementedException(); }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion
    }
}

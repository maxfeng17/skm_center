﻿using System;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.WebModules
{
    /// <summary>
    /// 驗證使用者資料, 確保在 Cookie的使用者版本跟在DB的使用者版本一致
    /// 當不一致的情況發生，譬如更改密碼後，使用者資料在DB的版本會加1，即以訪客登入。
    /// 
    /// PponPrincipal 存在的目的，在於既然要查詢 member，即順便取得會員的基本資料放置之，
    /// 頁面即以判斷 Page.User 是否為 PponPrincipal型別，如是，即可不透過db而透過Page.User 
    /// 取得 Pic, UserId 等會員基本資料。
    /// </summary>
    public class UserAuthenticationModule : IHttpModule
    {
        private static ILog logger = LogManager.GetLogger("userAuth");
        private static ILog oauthLog = LogManager.GetLogger("Oauth");
        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += context_AuthenticateRequest;
        }

        void context_AuthenticateRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this);

            HttpContext context = ((HttpApplication)sender).Context;
            
            string url = context.Request.RawUrl;
            //不處理商家系統的使用者
            if (url.Contains("/vbs", StringComparison.OrdinalIgnoreCase) &&
                !url.Contains("m/vbs", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }
            if (url.Contains(".axd?"))
            {
                return;
            }
            if (url.Contains("error.aspx", StringComparison.OrdinalIgnoreCase))
            {
                return;
            }

            try
            {
                bool isLogingComplete = false;
                string authHeader = context.Request.Headers["Authorization"];
                if (string.IsNullOrEmpty(authHeader))
                {
                    authHeader = context.Request.QueryString["accessToken"];
                    if (!string.IsNullOrEmpty(authHeader))
                    {
                        authHeader = string.Format("Bearer {0}", authHeader);
                    }
                }

                if (!string.IsNullOrEmpty(authHeader))
                {
                    oauthLog.DebugFormat("Oauth Start => {0}", authHeader);
                    var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);
                    var token = OAuthFacade.ExtractToken(authHeaderVal);
                    OAuthTokenModel authToken = OAuthFacade.GetTokenByAccessToken(token);
                    if (authToken != null && authToken.UserId != null && authToken.IsValid && !authToken.IsExpired && authToken.UserId != 0)
                    {
                        Member member = MemberFacade.GetMember(authToken.UserId ?? 0);
                        bool diffIdentify = HttpContext.Current.User == null || 
                            HttpContext.Current.User.Identity.IsAuthenticated == false ||
                            HttpContext.Current.User.Identity.Name.Equals(member.UserName, StringComparison.OrdinalIgnoreCase) == false;
                        //無效會員或非會員token
                        if (member != null && member.IsLoaded)
                        {
                            IIdentity identity = new PponIdentity(member, PponIdentity._USER);
                            HttpContext.Current.Items.Add("Identity", identity);
                            IPrincipal principal = null;
                            if (identity.AuthenticationType == "Token")
                            {
                                principal = new PponPrincipal(identity, new[] { "Token" });
                            }
                            else if (identity.AuthenticationType == "User")
                            {
                                RoleCollection rc = MemberFacade.GetMemberRoleCol(identity.Name);
                                principal = new PponPrincipal(identity, rc.Select(a => a.RoleName).ToArray());
                            }

                            Thread.CurrentPrincipal = principal;
                            HttpContext.Current.User = principal;
                            HttpContext.Current.Items["AppId"] = authToken.AppId;
                            HttpContext.Current.Items["AccessToken"] = token;

                            if (diffIdentify)
                            {
                                MemberUtility.UserForceSignInForAuthModule(member.UserName);
                            }

                            isLogingComplete = true;
                            oauthLog.DebugFormat("Oauth UserId => {0}", member.UniqueId);
                            oauthLog.DebugFormat("Oauth Item => {0}", HttpContext.Current.Items["AccessToken"].ToString());
                        }
                    }
                    else
                    {
                        oauthLog.InfoFormat("Oauth Fail => {0}", new JsonSerializer().Serialize(authToken));
                    }
                }

                if (!isLogingComplete && context.User != null && context.User.Identity.IsAuthenticated)
                {
                    if (FormsAuthentication.SlidingExpiration)
                    {
                        SlidingExpiration(LkSiteCookie.CookieVersion);
                        SlidingExpiration(LkSiteCookie.LoginSource);
                    }

                    logger.DebugFormat("user {0}{1} access {2}",
                        context.User.Identity.IsAuthenticated ? "(1)" : "(0)",
                        context.User.Identity.Name, url);

                    PponIdentity pponUser = PponIdentity.Get(context.User.Identity.Name);
                    if (pponUser == null || pponUser.IsLockedOut || pponUser.IsOutOfDate)
                    {
                        //logger.InfoFormat("user {0} not found.", context.User.Identity.Name);
                        Thread.CurrentPrincipal = new GenericPrincipal(new GenericIdentity(""), null); //set guest
                        context.User = Thread.CurrentPrincipal;
                        NewMemberUtility.UserSignOutLite();
                    }
                    else
                    {
                        Thread.CurrentPrincipal = new PponPrincipal(pponUser);
                        context.User = Thread.CurrentPrincipal;
                    }
                }
            }
            finally
            {
                ModuleLogger.MarkEnd(this);
            }
        }

        public void Dispose()
        {

        }

        private void SlidingExpiration(LkSiteCookie cookieName)
        {
            var cookie = HttpContext.Current.Request.Cookies[cookieName.ToString()];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value)) return;

            int temp;
            if (int.TryParse(cookie.Value, out temp))
            {
                return;
            }

            var cookieValue = cookie.Value.Split(',').Length > 0 ? cookie.Value.Split(',')[0] : cookie.Value;

            CookieModel data;

            try
            {
                data = new JsonSerializer().Deserialize<CookieModel>(Helper.Decrypt(cookieValue));
            }
            catch (Exception ex)
            {
                logger.InfoFormat("cookie catch name:{0}, path:{1}, value:{2}, ex:{3}", cookie.Name, cookie.Path, cookieValue, ex.StackTrace);
                return;
            }

            if (data == null || !data.IsKeepCookie || data.Expires == null) return;

            DateTime expriedFrom = data.Expires.Value.AddMinutes((double)FormsAuthentication.Timeout.Minutes / 2);
            DateTime expriedTo = data.Expires.Value.AddMinutes((double)FormsAuthentication.Timeout.Minutes);

            if (DateTime.Now >= expriedFrom && DateTime.Now < expriedTo)
            {
                switch (cookieName)
                {
                    case LkSiteCookie.CookieVersion:
                        if (int.TryParse(data.Value, out temp))
                        {
                            CookieManager.SetVersion(int.Parse(data.Value), true);
                        }
                        break;
                    case LkSiteCookie.LoginSource:
                        if (int.TryParse(data.Value, out temp))
                        {
                            CookieManager.SetMemberSource((SingleSignOnSource)int.Parse(data.Value), true);
                        }
                        break;
                }
            }
        }
    }
}

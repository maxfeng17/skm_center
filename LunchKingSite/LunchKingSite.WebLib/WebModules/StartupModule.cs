﻿using System;
using System.Net;
using System.Web;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.WebModules
{
    public class StartupModule : IHttpModule
    {
        private static readonly object thisLock = new object();        
        private string startupImageUrl;
        private static ISysConfProvider config;

        public static DateTime StartTime = DateTime.Now;

        public void Init(HttpApplication context)
        {
            config = ProviderFactory.Instance().GetConfig();
            //startupImageUrl = Helper.CombineUrl(conf.SiteUrl, "images/startup.gif");
            startupImageUrl = "/images/startup.gif";
            context.AuthenticateRequest += context_AuthenticateRequest;
        }

        private void context_AuthenticateRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this);

            if (ViewPponDealManager.DefaultManager.IsLoaded == false)
            {
                if (Helper.IsManagedHandler() == false)
                {
                    return;
                }
                Action act = LoadData;
                act.BeginInvoke(null, null);

                HttpResponse response = HttpContext.Current.Response;
                response.Clear();
                if (string.IsNullOrEmpty(HttpContext.Current.Request.ContentType) == false &&
                    HttpContext.Current.Request.ContentType.Equals("application/json", StringComparison.OrdinalIgnoreCase))
                {
                    WriteSystemBusyToApp(response);
                }
                else
                {

                    WringFirstLadyRunning(response);
                }
                response.End();
            }

            ModuleLogger.MarkEnd(this);
        }

        private void WriteSystemBusyToApp(HttpResponse response)
        {
            response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.AppendHeader("Expires", "0"); // Proxies.
            response.ContentType = "application/json; charset=utf-8";
            ApiResult apiResult = new ApiResult
            {
                Code = ApiResultCode.SystemBusy,
                Message = "系統繁忙中，請30秒後再試一次。"
            };
            response.Write(ProviderFactory.Instance().GetSerializer().Serialize(apiResult));
            /*
            Cache-Control: no-cache
            Pragma: no-cache
            Content-Type: application/json; charset=utf-8
            Expires: -1

            
                         */
        }

        private void WringFirstLadyRunning(HttpResponse response)
        {
            response.AppendHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
            response.AppendHeader("Pragma", "no-cache"); // HTTP 1.0.
            response.AppendHeader("Expires", "0"); // Proxies.
            response.ContentType = "text/html; charset=utf-8";
            response.Write("<html>");
            response.Write("<head>");
            response.Write("<meta http-equiv='Content-Type' content='text/html; charset=utf-8'>");
            response.Write("<meta http-equiv='refresh' content='3;url=" + HttpContext.Current.Request.RawUrl + "'>");
            response.Write(@"<style>");
            response.Write("</style>");
            response.Write("<body>");
            if (ViewPponDealManager.LastError != null)
            {
                string addr = HttpContext.Current.Request.UserHostAddress;
                // 錯誤訊息只在開發或測試時顯示
                if (addr == "::1" || addr == "127.0.0.1" || (addr != null && addr.StartsWith("192.168")))
                {
                    response.Write(
                        "<div style='border:1px solid #eee; padding:5px 20px 10px 20px;border-left-width:5px;border-left-color:#d9534f;border-radius:3px'>");
                    response.Write("<h4 style='color:#d9534f'>");
                    response.Write(ViewPponDealManager.LastError.Message);
                    response.Write("</h4>");
                    response.Write("<p>");
                    response.Write(ViewPponDealManager.LastError.StackTrace);
                    response.Write("</p>");
                    response.Write("</div>");
                }
            }
            string imgHtml = string.Format(
                "<img src='{0}' style='margin:auto;position:absolute;top:0px;bottom:0px;left:0px;right:0px'/>",
                startupImageUrl);
            response.Write(imgHtml);
            response.Write("<body>");
            response.Write("</head>");
            response.Write("</html>");
        }

        public void LoadData()
        {
            lock (thisLock)
            {
                if (ViewPponDealManager.DefaultManager.IsLoaded == false)
                {
                    ViewPponStoreManager.ReloadDefaultManager();
                    ViewPponDealManager.ReloadDefaultManager();
                }
            }
        }

        public void Dispose()
        {

        }
    }    
}

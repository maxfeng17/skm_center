﻿using System;
using System.Web;
using System.Web.Routing;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Models.Mobile;

namespace LunchKingSite.WebLib.WebModules
{
    public class CpaModules : IHttpModule
    {
        private ISysConfProvider config;
        private static ILog logger = LogManager.GetLogger(typeof (PponFacade).Name);
        public void Init(HttpApplication context)
        {
            config = ProviderFactory.Instance().GetConfig();
            context.AuthenticateRequest += context_AuthenticateRequest;
        }
        
        void context_AuthenticateRequest(object sender, EventArgs e)
        {
            ModuleLogger.MarkStart(this);

            if (config.EnableIChannel == false && config.EnableCpa == false
                && !config.EnableShopBack)
            {
                return;
            }

            HttpContext context = ((HttpApplication)sender).Context;
            if (config.EnableCpa)
            {
                RegisterCpaRsrc(context);
            }

            if (config.EnableIChannel)
            {
                RegisterIChannelGid(context);
            }

            if (config.EnableShopBack)
            {
                RegisterShopBackGid(context);
            }
            if (config.EnablePezChannel)
            {
                RegisterPezChannelGid(context);
            }

            if (config.EnableAffiliates)
            {
                RegisterAffiliatesGid(context);
            }

            if (config.EnableLineShop)
            {
                RegisterLineShopGid(context);
            }
            ModuleLogger.MarkEnd(this);
        }

        public void Dispose()
        {
        }

        #region iChannel

        public void RegisterIChannelGid(HttpContext context)
        {
            try
            {
                string ichannelGid = GetIChannelGid(context);
                if (!string.IsNullOrEmpty(ichannelGid))
                {
                    HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.iChannelGid.ToString());
                    cookie.Expires = DateTime.Now.AddDays(config.IChannelCookieExpires);
                    cookie.Value = ichannelGid;
                    context.Response.SetCookie(cookie);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        private string GetIChannelGid(HttpContext context)
        {
            if (context == null)
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(context.Request.QueryString["ichannel_gid"]))
            {
                return context.Request.QueryString["ichannel_gid"];
            }
            return string.Empty;
        }

        #endregion iChannel

        #region shopBack

        private void RegisterShopBackGid(HttpContext context)
        {
            try
            {
                string shopBackGid = GetShopBackGid(context);
                if (!string.IsNullOrEmpty(shopBackGid))
                {
                    HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.shopBackGid.ToString());
                    cookie.Expires = DateTime.Now.AddDays(config.ShopBackCookieExpires);
                    cookie.Value = shopBackGid;
                    context.Response.SetCookie(cookie);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        private string GetShopBackGid(HttpContext context)
        {
            const string shopBackGid = "sbid";
            if (context == null)
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(context.Request.QueryString[shopBackGid]))
            {
                return context.Request.QueryString[shopBackGid];
            }
            return string.Empty;
        }

        #endregion shopBack

        #region Affiliates

        private void RegisterAffiliatesGid(HttpContext context)
        {
            try
            {
                string affiliatesGid = GetAffiliatesBid(context);
                if (!string.IsNullOrEmpty(affiliatesGid))
                {
                    HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.AffiliatesGid.ToString());
                    cookie.Expires = DateTime.Now.AddDays(config.AffiliatesCookieExpires);
                    cookie.Value = affiliatesGid;
                    context.Response.SetCookie(cookie);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        private string GetAffiliatesBid(HttpContext context)
        {
            const string affiliatesBid = "affiliates_bid";
            if (context == null)
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(context.Request.QueryString[affiliatesBid]))
            {
                return context.Request.QueryString[affiliatesBid];
            }
            return string.Empty;
        }

        #endregion shopBack

        #region payeasy 導購

        public void RegisterPezChannelGid(HttpContext context)
        {
            try
            {
                string pezChannelGid = GetPezChannelGid(context);
                if (!string.IsNullOrEmpty(pezChannelGid))
                {
                    HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.PezChannelGid.ToString());
                    cookie.Expires = DateTime.Now.AddDays(config.PezChannelCookieExpires);
                    cookie.Value = pezChannelGid;
                    context.Response.SetCookie(cookie);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        private string GetPezChannelGid(HttpContext context)
        {
            const string pezChannelGid = "payeasy_gid";
            if (context == null)
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(context.Request.QueryString[pezChannelGid]))
            {
                return context.Request.QueryString[pezChannelGid];
            }
            return string.Empty;
        }

        #endregion payeasy

        #region LINE導購
        public void RegisterLineShopGid(HttpContext context)
        {
            try
            {
                string LineShopGid = GetLineShopGid(context);
                if (!string.IsNullOrEmpty(LineShopGid))
                {
                    HttpCookie cookie = CookieManager.NewCookie(LkSiteCookie.LineShopGid.ToString());
                    cookie.Expires = DateTime.Now.AddHours(config.LineShopCookieExpires);
                    cookie.Value = LineShopGid;
                    context.Response.SetCookie(cookie);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }
        private string GetLineShopGid(HttpContext context)
        {
            const string LineShop = "ecid";
            if (context == null)
            {
                return string.Empty;
            }
            if (!string.IsNullOrEmpty(context.Request.QueryString[LineShop]))
            {
                return context.Request.QueryString[LineShop];
            }
            return string.Empty;
        }
        #endregion

        #region rsrc

        public void RegisterCpaRsrc(HttpContext context)
        {
            try
            {
                MobileManager.RegisterReferrerSource(GetRsrc(context));
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

        private string GetRsrc(HttpContext context)
        {
            if (context == null)
            {
                return string.Empty;
            }

            string qrsrc = context.Request.QueryString["rsrc"];
            var routeData = RouteTable.Routes.GetRouteData(new HttpContextWrapper(context));
            if (routeData != null && routeData.Values["rsrc"] != null) ///處理 {bid}/YH_BMC?rsrc=G_search 情況
            {
                if (!string.IsNullOrEmpty(qrsrc))
                {
                    if (qrsrc.ToLower() == "G_search".ToLower())
                    {
                        return "G_search";
                    }
                }
                return routeData.Values["rsrc"].ToString();
            }

            if (string.IsNullOrEmpty(qrsrc)) return string.Empty;

            if (qrsrc.IndexOf(',') > 0) //處理 rsrc=YH_BMC,G_search 情況
            {
                Boolean isFirst = true;
                string rightrsrc = string.Empty;
                foreach (string temprsrc in qrsrc.Split(','))
                {
                    if (isFirst)
                    {
                        rightrsrc = temprsrc;
                    }

                    if (temprsrc.ToLower() == "G_search".ToLower())
                    {
                        return "G_search";
                    }
                    isFirst = false;
                }
                return rightrsrc;
            }

            if (qrsrc.IndexOf('?') > 0) //處理?rsrc=YH_BMC?rsrc=G_search 情況
            {
                string[] SPrsrc = qrsrc.Split('?');
                if (SPrsrc[1].Replace("rsrc=", "").ToLower() == "G_search".ToLower())
                {
                    return "G_search";
                }
                return SPrsrc[0];
            }
            return qrsrc;
        }

        #endregion rsrc

    }
}

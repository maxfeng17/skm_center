﻿using System;
using System.Web;

namespace LunchKingSite.WebLib.WebModules
{
    public class UrlRedirectModule : IHttpModule
    {
        public void Dispose()
        {
            
        }

        public void Init(HttpApplication context)
        {
            context.AuthenticateRequest += context_AuthenticateRequest;
        }

        void context_AuthenticateRequest(object sender, EventArgs e)
        {
            //別加太多 效能會影響的
            HttpContext context = ((HttpApplication)sender).Context;
            if (context.Request.RawUrl.Contains("deal/58b4b442-2789-4e0f-8b09-08ba8e4511f9"))
            {
                context.Response.RedirectPermanent("https://www.17life.com/deal/bb822bed-9577-4bbf-a6df-1d0c16042e8f");
                context.Response.End();
            }
        }
    }
}
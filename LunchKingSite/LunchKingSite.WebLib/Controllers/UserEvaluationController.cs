﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.Evaluate;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class UserEvaluationController : BaseController
    {
        #region 評價系統

        [VbsAuthorize]
        public ActionResult Evaluate(int? page)
        {
            if (VbsCurrent.Is17LifeEmployee)
            {
                return RedirectToAction("EvaluateSearch");
            }
            else
            {
                return RedirectToAction("EvaluateStoreList");
            }
        }

        [VbsAuthorize]
        public JsonResult EvaluateGetData(int range, int order, int page)
        {
            ConditionObj condition = new ConditionObj
            {
                Order = order,
                Range = range,
                Page = page
            };

            EvaluateData result = new EvaluateFacade().GetEvaluateProductData(VbsCurrent.AccountId, VbsCurrent.Is17LifeEmployee, condition);

            switch (order)
            {
                case (int)EvaluateOrderDesc.DeliveryEndNearToFar:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.DeliverTimeE).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryEndFarToNear:
                    result.DetailData = result.DetailData.OrderBy(x => x.DeliverTimeE).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryStartNearToFar:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.DeliverTimeS).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryStartFarToNear:
                    result.DetailData = result.DetailData.OrderBy(x => x.DeliverTimeS).ToList();
                    break;
                case (int)EvaluateOrderDesc.TotalSatisfactoryHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateOrderDesc.TotalSatisfactoryLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateOrderDesc.ComeAgainHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgComeAgain).ToList();
                    break;
                case (int)EvaluateOrderDesc.ComeAgainLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgComeAgain).ToList();
                    break;
            }
            int pageContain = 5;
            result.DetailData = result.DetailData.Skip((page - 1) * pageContain).Take(pageContain).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [VbsAuthorize]
        public ActionResult EvaluateShowProducts(Guid sid, int? page)
        {
            ViewBag.ShowRangeList = new SelectList(GetEvaluateShowDataRangeEnum(), "key", "value",3);
            ViewBag.OrderByCol = new SelectList(GetEvaluateOrderDescEnum(), "key", "value");

            SetBasicSessionParam();

            ConditionObj condition = new ConditionObj
            {
                Order = 1,
                Range = 1,
                Page = 1
            };

            Guid? seller_guid = null;
            if (!string.IsNullOrEmpty(this.Request.QueryString["seller_guid"]))
            {
                seller_guid = new Guid(this.Request.QueryString["seller_guid"]);
            }

            Session[VbsSession.ParentActionName.ToString()] = "EvaluateShowProducts";
            ViewBag.StoreDetail = new EvaluateFacade().GetEvaluateStoreListForCustomer(VbsCurrent.AccountId, VbsCurrent.Is17LifeEmployee, condition, seller_guid).DetailData.Where(x => x.StoreId == sid).FirstOrDefault();
            return View();
        }

        [VbsAuthorize]
        public JsonResult EvaluateShowProductsGetData(Guid sid, int range, int order, int page)
        {
            ConditionObj condition = new ConditionObj
            {
                Order = order,
                Range = range,
                Page = page
            };

            EvaluateData result = new EvaluateFacade().GetEvaluateAllBidsData(VbsCurrent.AccountId, VbsCurrent.Is17LifeEmployee, condition, sid);
            if (result == null)
            {
                result = new EvaluateData() { totalCount = 0 };
                return Json(result, JsonRequestBehavior.AllowGet);
            }


            switch (order)
            {
                case (int)EvaluateOrderDesc.DeliveryEndNearToFar:
                    result.DetailData = result.DetailData.OrderBy(x => x.DeliverTimeE).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryEndFarToNear:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.DeliverTimeE).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryStartNearToFar:
                    result.DetailData = result.DetailData.OrderBy(x => x.DeliverTimeS).ToList();
                    break;
                case (int)EvaluateOrderDesc.DeliveryStartFarToNear:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.DeliverTimeS).ToList();
                    break;
                case (int)EvaluateOrderDesc.TotalSatisfactoryHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateOrderDesc.TotalSatisfactoryLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgTotalSatisfactory).ToList();
                    break;
                case (int)EvaluateOrderDesc.ComeAgainHighToLow:
                    result.DetailData = result.DetailData.OrderByDescending(x => x.AvgComeAgain).ToList();
                    break;
                case (int)EvaluateOrderDesc.ComeAgainLowToHight:
                    result.DetailData = result.DetailData.OrderBy(x => x.AvgComeAgain).ToList();
                    break;
            }
            int pageContain = 10;
            result.DetailData = result.DetailData.Skip((page - 1) * pageContain).Take(pageContain).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [VbsAuthorize]
        public ActionResult EvaluateDetail(Guid bid, Guid sid)
        {

            SetBasicSessionParam();

            if (VbsCurrent.Is17LifeEmployee)
            {
                ViewBag.UpperUrl = "/vbs/UserEvaluation/" + Session[VbsSession.ParentActionName.ToString()];
            }
            else
            {
                ViewBag.UpperUrl = "/vbs/UserEvaluation/EvaluateShowProducts?sid=" + sid + "&page=";
            }

            //檢查該帳號是否有分店、檔次的權限

            return View();
        }

        [VbsAuthorize]
        public JsonResult EvaluateGetDetailData(Guid bid, Guid sid)
        {
            ConditionObj condition = new ConditionObj
            {
                Order = (int)EvaluateOrderDesc.DeliveryEndNearToFar,
                Range = (int)EvaluateShowDataRange.All,
                Page = 1
            };
            EvaluateData result = new EvaluateFacade().GetEvaluateDetailData(VbsCurrent.AccountId, VbsCurrent.Is17LifeEmployee, condition, bid, sid);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [VbsAuthorize]
        public ActionResult EvaluateSearch([Bind(Prefix = "showlist")] EvaluateCondition ConditionIndex = EvaluateCondition.Name, [Bind(Prefix = "queryKeyword")] string queryKeyword = "", [Bind(Prefix = "pager")] int page = 1)
        {
            if (!VbsCurrent.Is17LifeEmployee)
            {
                return RedirectToAction("EvaluateStoreList");
            }

            ViewBag.ConditionList = new SelectList(GetEvaluateConditionEnum(), "key", "value", 1);

            SetBasicSessionParam();

            if (string.Equals("POST", HttpContext.Request.HttpMethod))
            {

                EvaluateStoreData result = new EvaluateFacade().GetEvaluateStoreList(VbsCurrent.AccountId, VbsCurrent.Is17LifeEmployee, ConditionIndex, queryKeyword);

                if (string.Equals("POST", HttpContext.Request.HttpMethod) && int.Equals(0, result.TotalCount))
                {
                    result.IsResultEmpty = true;
                }

                if (ConditionIndex == EvaluateCondition.UniqueId && string.IsNullOrWhiteSpace(queryKeyword))
                {
                    result.IsResultEmpty = true;
                }

                int pageContain = 10;
                result.DetailData = result.DetailData.Skip((page - 1) * pageContain).Take(pageContain).ToList();

                return View(result);
            }

            return View();
        }
        
        [VbsAuthorize]
        public ActionResult EvaluateStoreList([Bind(Prefix = "hiddenValue")] string seller_id, [Bind(Prefix = "showlist")] int range = 1, [Bind(Prefix = "ordercol")] int order = 1, [Bind(Prefix = "pager")] int page = 1)
        {
            ViewBag.ShowRangeList = new SelectList(GetEvaluateShowStoreDataRangeEnum(), "key", "value", 1);
            ViewBag.OrderByCol = new SelectList(GetEvaluateStoreOrderDescEnum(), "key", "value");

            SetBasicSessionParam();
            string AccountId = VbsCurrent.AccountId;

            ConditionObj condition = new ConditionObj
            {
                Order = order,
                Range = range,
                Page = page
            };


            Guid? seller_guid = (VbsCurrent.Is17LifeEmployee && !string.IsNullOrEmpty(this.Request.QueryString["seller_guid"])) ? new Guid(this.Request.QueryString["seller_guid"]) : ((!string.IsNullOrEmpty(seller_id)) ? new Guid(seller_id) : Guid.Empty);

            EvaluateStoreData result = new EvaluateFacade().GetEvaluateStoreListForCustomer(AccountId, VbsCurrent.Is17LifeEmployee, condition, seller_guid);

            int pageContain = 10;
            result.DetailData = result.DetailData.Skip((page - 1) * pageContain).Take(pageContain).ToList();

            return View(result);

        }
        
        [VbsAuthorize]
        public JsonResult EvaluateGetDetailTable(Guid bid, Guid sid)
        {
            List<EvaluateDetailRating> result = new EvaluateFacade().GetEvaluateDetailData(bid, sid);

            return Json(result, JsonRequestBehavior.AllowGet);
        }
        
        #endregion


        #region Common
        private void SetBasicSessionParam()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = (Session[VbsSession.UserId.ToString()] != null ? Session[VbsSession.UserId.ToString()] : VbsCurrent.AccountId);
            ViewBag.Is17LifeEmployee = (Session[VbsSession.Is17LifeEmployee.ToString()] != null && VbsCurrent.Is17LifeEmployee);
        }

        private bool Is17LifeEmployee()
        {
            return (Session[VbsSession.Is17LifeEmployee.ToString()] != null && VbsCurrent.Is17LifeEmployee);
        }

        private Dictionary<string, string> GetEvaluateShowDataRangeEnum()
        {
            Dictionary<string, string> result = Enum.GetValues(typeof(EvaluateShowDataRange))
                .Cast<EvaluateShowDataRange>()
                .ToDictionary(x => ((int)x).ToString(), x => GetEnumDescription(x));

            return result;
        }

        private Dictionary<string, string> GetEvaluateShowStoreDataRangeEnum()
        {
            Dictionary<string, string> result = Enum.GetValues(typeof(EvaluateShowStoreDataRange))
                .Cast<EvaluateShowStoreDataRange>()
                .ToDictionary(x => ((int)x).ToString(), x => GetEnumDescription(x));

            return result;
        }

        private Dictionary<string, string> GetEvaluateOrderDescEnum()
        {
            Dictionary<string, string> result = Enum.GetValues(typeof(EvaluateOrderDesc))
                .Cast<EvaluateOrderDesc>()
                .ToDictionary(x => ((int)x).ToString(), x => GetEnumDescription(x));

            return result;
        }

        private Dictionary<string, string> GetEvaluateStoreOrderDescEnum()
        {
            Dictionary<string, string> result = Enum.GetValues(typeof(EvaluateStoreOrderDesc))
                .Cast<EvaluateStoreOrderDesc>()
                .ToDictionary(x => ((int)x).ToString(), x => GetEnumDescription(x));

            return result;
        }

        private Dictionary<string, string> GetEvaluateConditionEnum()
        {
            Dictionary<string, string> result = Enum.GetValues(typeof(EvaluateCondition))
                           .Cast<EvaluateCondition>()
                           .ToDictionary(x => ((int)x).ToString(), x => GetEnumDescription(x));

            return result;
        }

        private string GetEnumDescription(Enum value)
        {
            FieldInfo fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes =
                (DescriptionAttribute[])fi.GetCustomAttributes(
                typeof(DescriptionAttribute),
                false);

            if (attributes != null &&
                attributes.Length > 0)
                return attributes[0].Description;
            else
                return value.ToString();
        }

        #endregion
    }
}

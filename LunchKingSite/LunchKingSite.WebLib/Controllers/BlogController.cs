﻿using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    public class BlogController : BaseController
    {
        #region property
        protected static ISysConfProvider _config;
        protected static IBlogProvider _blogProv;
        protected static ILog logger = LogManager.GetLogger(typeof(BaseProposalController));

        #endregion property

        static BlogController()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _blogProv = ProviderFactory.Instance().GetProvider<IBlogProvider>();
        }

        public ActionResult Index(Guid blogid)
        {
            BlogPost blog = _blogProv.BlogPostGetByGid(blogid);
            ViewBag.SiteUrl = _config.SiteUrl;
            ViewBag.FB_AppId = ProviderFactory.Instance().GetConfig().FacebookApplicationId;
            ViewBag.DefaultImage = string.Empty;
            ViewBag.WebUrl = string.Empty;

            if (blog.IsLoaded)
            {
                BlogPageView pageView = new BlogPageView();
                pageView.BlogId = blog.Id;
                pageView.Ip = Helper.GetClientIP();
                pageView.CreateTime = DateTime.Now;
                if (HttpContext.User != null)
                {
                    pageView.CreateId = HttpContext.User.Identity.Name;
                }
                _blogProv.BlogPageViewSet(pageView);

                ViewBag.WebUrl = WebUtility.RequestShortUrl(string.Format("{0}/blog/{1}", _config.SiteUrl, blog.Guid));
                ViewBag.DefaultImage = GetDefaultImage(HttpUtility.HtmlDecode(blog.BlogContent));
                blog.BlogContent = PponFacade.ReplaceMobileYoutubeTag(HttpUtility.HtmlDecode(blog.BlogContent));
                return View(blog);
            }
            else
            {
                blog = new BlogPost();
                return View(blog);
            }
        }

        private string GetDefaultImage(string content)
        {
            string defaultImage = string.Empty;
            Match match = Regex.Match(content, ".+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase);
            if(match.Length > 0)
            {
                defaultImage = match.Groups[1].Value;
            }
            return defaultImage;
        }
    }
}

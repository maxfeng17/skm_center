﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebLib.ActionFilter;

namespace LunchKingSite.WebLib.Controllers
{
    public class QuestionnaireController : QuestionnaireBase
    {
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();

        [HttpGet]
        [RequireHttps]
        public ActionResult Event(string token)
        {
            ViewBag.SslSiteUrl = _cp.SSLSiteUrl;
            var ev = Qp.GetQuestionEvent(token);
            return View(ev);
        }

        [HttpPost]
        [RequireHttps]
        public ActionResult Volume(FormCollection form)
        {
            var model = new QuestionnaireValume();
            ViewBag.Message = string.Empty;
            string message;
            QuestionEvent ev;

            if (!QuestEventCheck(form, out ev, out message))
            {
                ViewBag.Message = message;
                return View(model);
            }

            string redirectLoginUrl;
            if (!AuthLoginMode(ev, out redirectLoginUrl))
            {
                return new RedirectResult(redirectLoginUrl);
            }

            int itemTotal = 0;
            var category = Qp.GetQuestionItemCategory(ev.Id);
            foreach (var cat in category)
            {
                var items = Qp.GetViewQuestionItem(ev.Id, cat.Id);
                model.QuestionData.Add(new QuestionCategoryModel
                {
                    Category = cat,
                    QuestionItems = items.Select(x=> new QuestionItemModel(x)).ToList()
                });
                itemTotal += items.Count;
            }

            model.ItemTotal = itemTotal;
            model.IsLoaded = model.QuestionData.Any();
            model.EventToken = form["token"];
            if (ev.LoginMode == (byte) QuestionEventLoginMode.MemberOnly)
            {
                var checkAnswer = Qp.GetQuestionAnswerByEventId(MemberFacade.GetUniqueId(User.Identity.Name), ev.Id);
                if (checkAnswer.Any())
                {
                    ViewBag.Message = "您已進行過本問卷調查活動";
                    model.AlreadyJoin = true;
                    return View(model);
                }
            }

            return View(model);
        }

        [HttpPost]
        [RequireHttps]
        public ActionResult HandIn(FormCollection form)
        {
            ViewBag.Message = string.Empty;
            ViewBag.RedirectUrl = string.Format("{0}/Questionnaire/Event?token={1}", _cp.SSLSiteUrl, form["token"]);
            ViewBag.Success = false;
            string message;
            QuestionEvent ev;

            if (!QuestEventCheck(form, out ev, out message))
            {
                ViewBag.Message = message;
                return View();
            }

            string redirectLoginUrl;
            if (!AuthLoginMode(ev, out redirectLoginUrl))
            {
                return new RedirectResult(redirectLoginUrl);
            }

            var answer = new QuestionAnswerModel
            {
                UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                EventToken = form["token"],
                IpAddress = Helper.GetClientIP(),
                FromType = (byte)Helper.GetOrderFromType(),
                Volume = GetAnswerVolume(ev.Id, form)
            };

            ApiResultCode code = PromotionFacade.AnswerQuestion(answer);

            switch (code)
            {
                case ApiResultCode.InputError:
                    ViewBag.Message = "Input error.";
                    break;
                case ApiResultCode.DataNotFound:
                    ViewBag.Message = "Event is not exist.";
                    break;
                case ApiResultCode.UserNoSignIn:
                    ViewBag.Message = "請登入17Life會員後再進行問卷";
                    break;
                case ApiResultCode.DataIsExist:
                    ViewBag.Message = "您已進行過本問卷活動";
                    break;
                case ApiResultCode.SaveFail:
                    ViewBag.Message = "問卷儲存失敗，請重新送出";
                    break;
                case ApiResultCode.Success:
                    ViewBag.Message = string.Empty;
                    ViewBag.RedirectUrl = string.Format("{0}/User/DiscountList.aspx", _cp.SiteUrl);
                    ViewBag.Success = true;
                    break;
            }

            return View();
        }

        private List<AnswerVolume> GetAnswerVolume(int eventId, FormCollection form)
        {
            var items = Qp.GetViewQuestionItem(eventId);
            var result = new List<AnswerVolume>();

            foreach (var i in items)
            {
                string inputId = string.Format("ans{0}", i.ItemId);
                string passId = string.Format("passItem{0}", i.ItemId);

                //被跳題
                if (form[passId] == "1")
                {
                    continue;
                }

                string inputValue = form[inputId];
                if (!string.IsNullOrEmpty(inputValue))
                {
                    QuestionType qt = (QuestionType)i.QuestionType;
                    if (qt == QuestionType.Default)
                    {
                        string txtOptId = string.Format("txtOptId{0}", i.ItemId);
                        int optId;
                        if (int.TryParse(form[txtOptId], out optId))
                        {
                            result.Add(new AnswerVolume
                            {
                                OptionId = optId,
                                AnswerContent = inputValue
                            });
                        }
                    }
                    else
                    {
                        var opts = Qp.GetQuestionItemOptions(i.ItemId);
                        var ans = inputValue.Split(',').ToList();
                        if (qt == QuestionType.Multiple)
                        {
                            foreach (var a in ans)
                            {
                                int optId;
                                if (int.TryParse(a.Trim(), out optId))
                                {
                                    var opt = opts.FirstOrDefault(x => x.Id == optId) ?? new QuestionItemOption();
                                    result.Add(new AnswerVolume
                                    {
                                        OptionId = optId,
                                        AnswerContent = opt.OtherInput ? form[string.Format("otherInput{0}", optId)] : string.Empty
                                    });
                                }
                            }
                        }
                        else if (qt == QuestionType.Radio)
                        {
                            int optId;
                            if (int.TryParse(ans[0].Trim(), out optId))
                            {
                                var opt = opts.FirstOrDefault(x => x.Id == optId) ?? new QuestionItemOption();
                                result.Add(new AnswerVolume
                                {
                                    OptionId = optId,
                                    AnswerContent = opt.OtherInput ? form[string.Format("otherInput{0}", optId)] : string.Empty
                                });
                            }
                        }
                    }
                }
            }

            return result;
        }
    }
}

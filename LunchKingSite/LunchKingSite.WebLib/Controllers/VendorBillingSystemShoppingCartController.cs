﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class VendorBillingSystemShoppingCartController : BaseController
    {
        #region property

        private static IPponProvider _pp;
        private static IVerificationProvider _vp;
        private static IMemberProvider _mp;
        private static ISellerProvider _sp;
        private static IHiDealProvider _hp;
        private static IAccountingProvider _ap;
        private static ISysConfProvider config;
        private static IOrderProvider _op;
        private static IBookingSystemProvider _bp;
        private static IHumanProvider _hmp;

        static VendorBillingSystemShoppingCartController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        }
        #endregion property

        #region Action
        #region 購物車
        [VbsAuthorize]
        public ActionResult ShoppingCartManage()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            Dictionary<Guid, string> sellers = SellersGet(UserId, true);
            List<Seller> newSellers = new List<Seller>();
            foreach (var da in sellers)
            {
                Seller s = _sp.SellerGet(da.Key);
                if (s != null && s.IsLoaded)
                {
                    newSellers.Add(s);
                }
            }

            ShoppingCartFreightCollection scfs = _sp.ShoppingCartFreightsGetBySellerGuid(
                                                        sellers.Select(x => x.Key).Distinct().ToList());

            List<ShoppingCartManageList> list = new List<ShoppingCartManageList>();

            List<ShoppingCartFreight> freights = _sp.ShoppingCartFreightsGetBySellerGuid(sellers.Select(x => x.Key).Distinct().ToList()).ToList();

            int changes = 0;
            foreach (ShoppingCartFreight sf in freights)
            {
                list.Add(new ShoppingCartManageList()
                {
                    Id = sf.Id,
                    UniqueId = sf.UniqueId,
                    SellerName = SellerNameGet(sf.SellerGuid),
                    FreightsStatus = sf.FreightsStatus,
                    FreightsName = sf.FreightsName,
                    NoFreightLimit = sf.NoFreightLimit,
                    Freights = sf.Freights
                });
                changes += _sp.ShoppingCartFreightsItemGetByFid(sf.Id).Where(x => x.ItemStatus == (int)ShoppingCartFreightStatus.Init).Count();
            }

            ViewBag.Changes = changes;
            ViewBag.Sellers = newSellers;
            ViewBag.Freights = list;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            return View();
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult CreateShoppingCartFreight(string sellerId, string FreightsName, int NoFreightLimit, int Freights)
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();
            bool flag = true;
            Dictionary<Guid, string> sellers = SellersGet(UserId, true);

            List<ShoppingCartFreight> freights = _sp.ShoppingCartFreightsGetBySellerGuid(sellers.Select(x => x.Key).Distinct().ToList()).ToList();

            string tid = (Convert.ToInt32(freights.Max(x => x.UniqueId.Substring(x.UniqueId.Length - 3))) + 1).ToString().PadLeft(3, '0');
            foreach (var freight in freights)
            {
                if (freight.FreightsName == FreightsName &&
                    freight.NoFreightLimit == NoFreightLimit &&
                    freight.Freights == Freights)
                {
                    flag = false;
                }
            }
            if (flag)
            {
                Guid sid = Guid.NewGuid();
                Guid.TryParse(sellerId, out sid);
                if (sid != Guid.Empty)
                {
                    Seller seller = _sp.SellerGet(sid);
                    if (seller != null && seller.IsLoaded)
                    {
                        ShoppingCartFreight scf = new ShoppingCartFreight();
                        scf.UniqueId = seller.SellerId.Replace("-", "") + tid;
                        scf.SellerGuid = sid;
                        scf.FreightsStatus = (int)ShoppingCartFreightStatus.Init;
                        scf.FreightsName = FreightsName;
                        scf.NoFreightLimit = NoFreightLimit;
                        scf.Freights = Freights;
                        scf.CreateTime = DateTime.Now;
                        scf.CreateUser = UserId;

                        _sp.ShoppingCartFreightsSet(scf);

                        ShoppingCartFreightsLogSet(scf.Id, "【新增購物車】" + scf.Id);
                    }
                }
            }

            return RedirectToAction("ShoppingCartManage");
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult CheckFreightData(string FreightsName, int NoFreightLimit, int Freights)
        {
            bool flag = true;
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            Dictionary<Guid, string> sellers = SellersGet(UserId, true);

            List<ShoppingCartFreight> freights = _sp.ShoppingCartFreightsGetBySellerGuid(sellers.Select(x => x.Key).Distinct().ToList()).ToList();


            foreach (var freight in freights)
            {
                if (freight.FreightsName == FreightsName &&
                    freight.NoFreightLimit == NoFreightLimit &&
                    freight.Freights == Freights)
                {
                    flag = false;
                }
            }

            return Json(new { IsSuccess = flag });
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult DeleteFreight(int id)
        {
            bool flag = true;
            string ErrorMessage = "";
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            ShoppingCartFreight freight = _sp.ShoppingCartFreightsGet(id);

            if (freight != null && freight.IsLoaded)
            {
                ShoppingCartFreightsItemCollection items = _sp.ShoppingCartFreightsItemGetByFid(id);
                if (items.Count > 0)
                {
                    //購物車下尚有檔次,無法刪除
                    flag = false;
                    ErrorMessage = "此購物車仍有商品套用,無法刪除,\r\n請將商品移至其他購物車再進行刪除!";
                }
                else
                {
                    _sp.ShoppingCartFreightsDelete(freight);
                    ShoppingCartFreightsLogSet(id, "【刪除購物車】(" + id + ")" + freight.FreightsName);
                }
            }

            return Json(new { IsSuccess = flag, Message = ErrorMessage });
        }

        [VbsAuthorize]
        public ActionResult ShoppingCartConfirm()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            Dictionary<Guid, string> sellers = SellersGet(UserId, true);

            List<ShoppingCartFreight> freights = _sp.ShoppingCartFreightsGetBySellerGuid(
                            sellers.Select(x => x.Key).Distinct().ToList()).ToList();

            List<ShoppingCartConfirmModel> items = new List<ShoppingCartConfirmModel>();
            foreach (ShoppingCartFreight sf in freights)
            {
                var datas = _sp.ShoppingCartFreightsItemGetByFid(sf.Id).Where(x => x.ItemStatus == (int)ShoppingCartFreightStatus.Init);
                foreach (var data in datas)
                {
                    ShoppingCartConfirmModel item = new ShoppingCartConfirmModel
                    {
                        Id = data.Id,
                        BusinessHourGuid = data.BusinessHourGuid,
                        UniqueId = data.UniqueId,
                        DealName = DealPropertyGet(data.BusinessHourGuid),
                        ChangeLog = ShoppingCartChangeLogGet(data.FreightsId),
                        CreateTime = data.CreateTime
                    };
                    items.Add(item);
                }
            }

            ViewBag.FreightsItem = items;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            return View();
        }


        private string DealPropertyGet(Guid BusinessHourGuid)
        {
            string result = "";
            ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(BusinessHourGuid);
            if (vpd.IsLoaded)
            {
                result = PponFacade.PponNewContentGet(vpd.BusinessHourGuid);
            }
            return result;
        }

        private string ShoppingCartChangeLogGet(int id)
        {
            string result = "";
            ShoppingCartFreight scf = _sp.ShoppingCartFreightsGet(id);
            if (scf.IsLoaded)
            {
                result = string.Format("[{0}]{1}", scf.UniqueId, scf.FreightsName);
            }
            return result;
        }

        [VbsAuthorize]
        public ActionResult ShoppingCartImport()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            return View();
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ShoppingCartFreightsUpload()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();
            Core.ModelPartial.AjaxSellerContractResult result = new Core.ModelPartial.AjaxSellerContractResult();

            Dictionary<Guid, string> sellers = SellersGet(UserId, true);
            List<Guid> newSellers = new List<Guid>();
            //找出賣家及下層賣家
            foreach (var s in sellers)
            {
                newSellers.Add(s.Key);
                newSellers.AddRange(_sp.SellerTreeGetSellerGuidListByRootSellerGuid(s.Key));
            }

            try
            {
                StringBuilder sb = new StringBuilder();

                StringBuilder errBuilder = new StringBuilder();
                Dictionary<int, string> data = new Dictionary<int, string>();
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    HSSFWorkbook hssfworkbook = new HSSFWorkbook(file.InputStream);
                    Sheet sheet = hssfworkbook.GetSheetAt(0);
                    Row row;

                    for (int k = 1; k <= sheet.LastRowNum; k++)
                    {
                        row = sheet.GetRow(k);
                        //check
                        if (IsNullOrEmpty(row.GetCell(0)) == "")
                        {
                            errBuilder.AppendLine("必須填寫檔號" + Environment.NewLine);
                        }
                        if (IsNullOrEmpty(row.GetCell(2)) == "")
                        {
                            errBuilder.AppendLine("必須填寫購物車" + Environment.NewLine);
                        }
                        string oriUniqueId = row.GetCell(0).ToString();    //檔號
                        string Freights = row.GetCell(2).ToString();    //所屬運費

                        int UniqueId = 0;
                        int.TryParse(oriUniqueId, out UniqueId);

                        if (UniqueId == 0)
                        {
                            errBuilder.AppendLine(oriUniqueId + "=>檔次異常。" + Environment.NewLine);
                        }

                        data.Add(UniqueId, Freights);
                    }

                    DealPropertyCollection dps = _pp.DealPropertyGet(data.Select(x => x.Key).Distinct().ToList());
                    ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(dps.Select(x => x.BusinessHourGuid).Distinct().ToList());

                    Dictionary<int, List<ShoppingCartFreightsItem>> items = new Dictionary<int, List<ShoppingCartFreightsItem>>();
                    ShoppingCartFreightCollection scfs = _sp.ShoppingCartFreightsGetByUniqueId(data.Select(x => x.Value).Distinct().ToList());
                    int itemCount = 0;

                    foreach (var obj in data)
                    {
                        //檢查檔號是否存在

                        ViewPponDeal vpd = vpds.Where(x => x.UniqueId == obj.Key).FirstOrDefault();
                        if (vpd == null)
                        {
                            errBuilder.AppendLine(obj.Key + "=>該檔次不存在，無法匯入。" + Environment.NewLine);
                        }else
                        {
                            if(vpd.DeliveryType != (int)DeliveryType.ToHouse)
                            {
                                errBuilder.AppendLine(obj.Key + "=>該檔次非宅配檔次，無法匯入。" + Environment.NewLine);
                            }
                        }
                        //檢查檔號是否屬於商家所屬賣家
                        var tmpSeller = newSellers.Where(x => x == vpd.SellerGuid);

                        if (tmpSeller.Count() == 0)
                        {
                            errBuilder.AppendLine(obj.Key + "=>設定的購物車非同賣家,無法移動,請重新確認購物車!。" + Environment.NewLine);
                        }
                        //檢查該運費是否存在
                        ShoppingCartFreight scf = scfs.Where(x => x.UniqueId == obj.Value).FirstOrDefault();
                        if (scf == null)
                        {
                            errBuilder.AppendLine(obj.Value + "=>查無購物車序號,請重新確認!" + Environment.NewLine);
                        }
                        else
                        {
                            if (scf.FreightsStatus != (int)ShoppingCartFreightStatus.Approve)
                            {
                                errBuilder.AppendLine(obj.Value + "=>:購物車狀態非啟用,無法移動,請重新確認購物車!。" + Environment.NewLine);
                            }
                        }

                        if (errBuilder.Length == 0)
                        {
                            if (items.ContainsKey(scf.Id))
                            {
                                //已存在
                                var itemList = items.Where(x => x.Key == scf.Id).Select(x => x.Value).FirstOrDefault();
                                ShoppingCartFreightsItem item = new ShoppingCartFreightsItem();

                                item.FreightsId = scf.Id;
                                item.ItemStatus = (int)ShoppingCartFreightStatus.Approve;
                                item.UniqueId = vpd.UniqueId.Value;
                                item.BusinessHourGuid = vpd.BusinessHourGuid;
                                item.CreateTime = DateTime.Now;
                                item.CreateUser = UserId;

                                itemCount++;
                                itemList.Add(item);
                            }
                            else
                            {
                                //不存在
                                List<ShoppingCartFreightsItem> itemList = new List<ShoppingCartFreightsItem>();
                                ShoppingCartFreightsItem item = new ShoppingCartFreightsItem();

                                item.FreightsId = scf.Id;
                                item.ItemStatus = (int)ShoppingCartFreightStatus.Approve;
                                item.UniqueId = vpd.UniqueId.Value;
                                item.BusinessHourGuid = vpd.BusinessHourGuid;
                                item.CreateTime = DateTime.Now;
                                item.CreateUser = UserId;

                                itemList.Add(item);
                                itemCount++;
                                items.Add(scf.Id, itemList);
                            }
                        }
                    }


                    if (errBuilder.Length == 0)
                    {
                        foreach (var it in items)
                        {
                            //刪除現有item
                            _sp.ShoppingCartFreightsItemDelete(it.Value.Select(x => x.BusinessHourGuid).Distinct().ToList());
                            ShoppingCartFreightsLogSet(it.Key, "系統刪除現有資料" +
                                new JsonSerializer().Serialize(it.Value.Select(x => x.BusinessHourGuid).Distinct().ToList()),
                                ShoppingCartFreightLogType.System);
                            //_sp.ShoppingCartFreightsItemDelete(items.Select(x => x.FreightsId).Distinct().ToList());
                            //重新insert
                            _sp.ShoppingCartFreightsItemSetBulkInsert(it.Value);
                            ShoppingCartFreightsLogSet(it.Key, "【匯入資料】：\r\n" + string.Format("共匯入{0}筆資料。",
                                itemCount + "\r\n" +
                                new JsonSerializer().Serialize(it.Value.Select(x => x.UniqueId).Distinct().ToList())));
                        }


                        result.IsSuccess = true;
                        result.Message = string.Format("共匯入{0}筆資料。", itemCount);

                    }
                    else
                    {
                        throw new Exception(errBuilder.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return Json(result);
        }

        private void ShoppingCartFreightsLogSet(int id, string contentLog, ShoppingCartFreightLogType type = ShoppingCartFreightLogType.User)
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();
            SellerFacade.ShoppingCartFreightsLogSet(UserId, id, contentLog, type);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ApplyAllFreights(int id)
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();
            Core.ModelPartial.AjaxSellerContractResult result = new Core.ModelPartial.AjaxSellerContractResult();

            try
            {
                List<ShoppingCartFreightsItem> items = new List<ShoppingCartFreightsItem>();
                ShoppingCartFreight freights = _sp.ShoppingCartFreightsGet(id);
                if (freights.FreightsStatus != (int)ShoppingCartFreightStatus.Approve)
                {
                    throw new Exception("購物車尚未審核通過!");
                }
                if (freights != null && freights.IsLoaded)
                {
                    ViewPponDealCollection vpds = _pp.ViewPponDealGetBySellerGuid(freights.SellerGuid);
                    foreach (ViewPponDeal vpd in vpds)
                    {
                        if(freights.Freights != 0)
                        {
                            if (vpd.DeliveryType != (int)DeliveryType.ToHouse)
                            {
                                //宅配才能設定運費                            
                                continue;
                            }
                        }
                        
                        ShoppingCartFreightsItem item = new ShoppingCartFreightsItem();
                        item.FreightsId = id;
                        item.ItemStatus = (int)ShoppingCartFreightStatus.Approve;
                        item.UniqueId = vpd.UniqueId.Value;
                        item.BusinessHourGuid = vpd.BusinessHourGuid;
                        item.CreateTime = DateTime.Now;
                        item.CreateUser = UserId;

                        items.Add(item);
                    }
                }

                //刪除現有item
                _sp.ShoppingCartFreightsItemDelete(items.Select(x => x.BusinessHourGuid).Distinct().ToList());
                //_sp.ShoppingCartFreightsItemDelete(items.Select(x => x.FreightsId).Distinct().ToList());
                ShoppingCartFreightsLogSet(id, "【套用所有商品】：\r\n系統刪除現有資料", ShoppingCartFreightLogType.System);

                //重新insert
                _sp.ShoppingCartFreightsItemSetBulkInsert(items);

                ShoppingCartFreightsLogSet(id, "【套用所有商品】：\r\n" +
                    new JsonSerializer().Serialize(items.Select(x => x.UniqueId).Distinct().ToList()));

                result.IsSuccess = true;
                result.Message = string.Format("共{0}個商品 成功套用購物車「{1}」。", items.Count().ToString(), freights.FreightsName);

            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return Json(result);
        }

        [VbsAuthorize]
        public void ShoppingCartExportExcel()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("購物車管理");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("檔號");
            cols.CreateCell(1).SetCellValue("方案");
            cols.CreateCell(2).SetCellValue("購物車");

            Dictionary<Guid, string> sellers = SellersGet(UserId, true);

            ShoppingCartFreightCollection scfs = _sp.ShoppingCartFreightsGetBySellerGuid(
                                                        sellers.Select(x => x.Key).Distinct().ToList());


            List<ShoppingCartFreightsItem> items = new List<ShoppingCartFreightsItem>();
            foreach (ShoppingCartFreight scf in scfs)
            {
                items.AddRange(_sp.ShoppingCartFreightsItemGetByFid(scf.Id)
                            .Where(x => x.ItemStatus == (int)ShoppingCartFreightStatus.Approve).ToList());
            }

            ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(
                                items.Select(x => x.BusinessHourGuid).Distinct().ToList());

            int rowIdx = 1;
            foreach (ShoppingCartFreightsItem item in items)
            {
                ViewPponDeal vpd = vpds.Where(x => x.BusinessHourGuid == item.BusinessHourGuid).FirstOrDefault();
                ShoppingCartFreight scf = scfs.Where(x => x.Id == item.FreightsId).FirstOrDefault();

                if (vpd != null && vpd.IsLoaded && scf != null && scf.IsLoaded)
                {
                    if(vpd.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        Row row = sheet.CreateRow(rowIdx);
                        row.CreateCell(0).SetCellValue(vpd.UniqueId.ToString());
                        row.CreateCell(1).SetCellValue(PponFacade.PponNewContentGet(vpd.BusinessHourGuid));
                        row.CreateCell(2).SetCellValue(scf.UniqueId);

                        rowIdx++;
                    }
                }
            }

            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                workbook.Write(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", DateTime.Now.ToString("yyyyMMdd")));
                Response.ContentType = "application/ms-excel";
                Response.Flush();
                Response.End();
            }

            workbook.Dispose();
        }

        private string SellerNameGet(Guid g)
        {
            string result = "";
            Seller seller = _sp.SellerGet(g);
            if (seller.IsLoaded)
            {
                result = string.Format("{0}-{1}", seller.SellerId, seller.SellerName);
            }
            return result;
        }

        [VbsAuthorize]
        public ActionResult ShoppingCartFreightsCheck(string ids)
        {

            bool IsSuccess = false;
            string Message = "";
            try
            {
                List<int> data = new JsonSerializer().Deserialize<List<int>>(ids);
                foreach (int id in data)
                {
                    ShoppingCartFreightsItem scfi = _sp.ShoppingCartFreightsItemGet(id);
                    if (scfi.IsLoaded)
                    {
                        scfi.ItemStatus = (int)ShoppingCartFreightStatus.Approve;
                        _sp.ShoppingCartFreightsItemSet(scfi);
                        ShoppingCartFreightsLogSet(scfi.FreightsId, "【購物車異動確認】" + scfi.Id);
                    }
                }
                IsSuccess = true;
                Message = "已成功異動" + data.Count + "個檔次!";
            }
            catch
            {
                IsSuccess = false;
                Message = "已成功異動0個檔次!";
            }

            return Json(new { IsSuccess = IsSuccess, Message = Message });
        }
        #endregion 購物車
        #endregion

        #region method
        private Dictionary<Guid, string> SellersGet(string UserId, bool chkStoreStatus = false)
        {
            Dictionary<Guid, string> dicSeller = new Dictionary<Guid, string>();
            VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(UserId);
            //var data = aclModel.GetAllowedDeals().GroupBy(x => x.SellerGuid ).ToList();
            var data = aclModel.GetAllowedDealStores().ToList();
            List<Guid> sid = new List<Guid>();
            sid = data.Distinct().ToList();
            //foreach (var perm in data)
            //{
            //    if (!sid.Contains(perm.SellerGuid))
            //    {
            //        sid.Add(perm.SellerGuid);
            //    }

            //}
            //var resource = SellerFacade.GetResourceAclList(UserId).Select(x => new { ResourceGuid = x.ResourceGuid }).Distinct();

            //return _sp.SellerGetList(resource.Select(x => x.ResourceGuid).ToList()).ToDictionary(x => x.Guid, x => x.SellerName);
            if (chkStoreStatus)
            {
                var rtn = _sp.SellerGetList(sid).Where(x => x.StoreStatus == (int)StoreStatus.Available).ToDictionary(x => x.Guid, x => x.SellerName);
                return rtn;
            }
            else
            {
                var rtn = _sp.SellerGetList(sid).ToDictionary(x => x.Guid, x => x.SellerName);
                return rtn;
            }

        }
        private string IsNullOrEmpty(object obj)
        {
            return (obj ?? "").ToString();
        }
        #endregion

    }
}

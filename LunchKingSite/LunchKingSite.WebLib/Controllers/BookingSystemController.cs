﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BookingSystem;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.BookingSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class BookingSystemController : BaseController
    {

        private static IMemberProvider _mp;
        private static IBookingSystemProvider _bp;
        private static ISellerProvider _sp;
        private static IOrderProvider _op;
        private static IPponProvider _pp;
        private static IHiDealProvider _hp;
        private static ISysConfProvider _conf;

        private const int SELLER_PAGE_NUMBER = 8;
        private const int STORE_PAGE_NUMBER = 8;
        private const int RESERVATION_CALENDAR_PAGE_NUMBER = 30;
        private const int DAILY_RESERVATION_RECORD_PAGE_NUMBER = 20;

        static BookingSystemController()
        {
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
        }

        #region Action

        //商家帳號權限導向
        [VbsAuthorize]
        public ActionResult BookingSystemAuthorize()
        {
            BookingSystemCommonInfo("BookingSystemAuthorize");

            if (Is17LifeEmployee())
            {
                return RedirectToAction("BookingSystemSearch");
            }
            else
            {
                var aclCol = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId);
                var sellerAcls = aclCol.Where(x => x.ResourceType == (int)ResourceType.Seller);

                //商家帳號多賣家情況
                if (sellerAcls.Count() > 1)
                {
                    var viewModel = new BookingSellerListViewModel();
                    List<BookingSellerListModel> bookingSellerList = new List<BookingSellerListModel>();
                    var sellers = _sp.SellerGetList(sellerAcls.Select(x => x.ResourceGuid)).ToDictionary(x => x.Guid, x => x);
                    foreach (var acl in sellerAcls)
                    {
                        if (!sellers.ContainsKey(acl.ResourceGuid))
                        {
                            continue;
                        }
                        var seller = sellers[acl.ResourceGuid];
                        bookingSellerList.Add(new BookingSellerListModel { SellerGuid = seller.Guid, SellerName = seller.SellerName });
                    }

                    int pageSize = SELLER_PAGE_NUMBER; //一頁有幾筆
                    int pageNumber = 1;
                    int pageIndex = pageNumber - 1;

                    var query = (from t in bookingSellerList select t);
                    var paging = new PagerList<BookingSellerListModel>(
                    query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                    new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                    pageIndex,
                    query.ToList().Count);

                    viewModel.SellerListModels = paging;
                    TempData["bookingSellerList"] = bookingSellerList;

                    return View("BookingSellerList", viewModel);
                }
                else if (sellerAcls.Count() == 1)
                {
                    //一賣家權限的情況
                    return RedirectToAction("BookingStore", new { sellerGuid = sellerAcls.First().ResourceGuid });
                }

                //Store權限 

                var storeAcls = aclCol.Where(x => x.ResourceType == (int)ResourceType.Store);

                if (storeAcls.Any())
                {
                    //檢查分店設定
                    var storeList = new List<Seller>();

                    foreach (var storeAcl in storeAcls)
                    {
                        var store = _sp.SellerGet(storeAcl.ResourceGuid);

                        if (store.IsOpenBooking)
                        {
                            storeList.Add(store);
                        }
                    }

                    if (storeList.Count() > 1)
                    {
                        var vStores = new List<BookingSystemStoreViewModel>();
                        foreach (var store in storeList)
                        {
                            var viewModel = new BookingSystemStoreViewModel { StoreName = store.SellerName, StoreGuid = store.Guid, IsDefalutSetting = _bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(store.Guid).Any() };
                            vStores.Add(viewModel);
                        }

                        TempData["bookingStoreList"] = vStores;

                        return RedirectToAction("BookingStores", "BookingSystem");

                    }
                    else if (storeList.Count() == 1)
                    {
                        return RedirectToAction("BookingStoreReservationList", new { storeGuid = storeList.First().Guid });
                    }

                }
            }

            return RedirectToAction("BookingNoPermission");
        }

        #region  BookingError

        [VbsAuthorize]
        public ActionResult BookingError()
        {
            BookingSystemCommonInfo("BookingError");
            return View();
        }

        #endregion BookingError

        #region BookingNoPermission

        [VbsAuthorize]
        public ActionResult BookingNoPermission()
        {
            BookingSystemCommonInfo("BookingNoPermission");

            return View();
        }

        #endregion BookingNoPermission

        #region BookingSystemSearch

        [ActionName("BookingSystemSearch")]
        [VbsAuthorize]
        public ActionResult BookingSystemSearchSellers(SearchSellerViewModel viewModel, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingSystemSearch");

            if (pageNumber != null && TempData["TotalSeller"] != null)
            {
                List<BookingSellerModel> bookingSellers = TempData["TotalSeller"] as List<BookingSellerModel>;
                viewModel.BookingSearchType = (TempData["BookingSearchType"] == null) ? default(int) : (int)TempData["BookingSearchType"];
                viewModel.SearchExpression = (TempData["SearchExpression"] == null) ? string.Empty : TempData["SearchExpression"] as string;
                if (bookingSellers != null)
                {

                    int pageSize = SELLER_PAGE_NUMBER; //一頁有幾筆
                    int pageIndex = pageNumber.Value - 1;

                    var query = (from t in bookingSellers select t);
                    var paging = new PagerList<BookingSellerModel>(
                        query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                        new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                        pageIndex,
                        query.ToList().Count);

                    viewModel.BookingSellerModels = paging;
                    TempData["TotalSeller"] = bookingSellers;
                    TempData["BookingSearchType"] = viewModel.BookingSearchType;
                    TempData["SearchExpression"] = viewModel.SearchExpression;
                }
            }

            return View(viewModel);
        }

        [VbsAuthorize]
        [HttpPost]
        public ActionResult BookingSystemSearch(SearchSellerViewModel viewModel)
        {
            BookingSystemCommonInfo("BookingSystemSearch");
            ViewBag.Message = "";
            SellerCollection sellerCol;
            switch (viewModel.BookingSearchType)
            {
                case (int)BookingSearchType.SignCompanyId:
                    sellerCol = _sp.SellerGetParentList("SignCompanyID =" + viewModel.SearchExpression);
                    break;
                case (int)BookingSearchType.SellerId:
                    sellerCol = _sp.SellerGetList(1, 100, "", "seller_id =" + viewModel.SearchExpression);
                    break;
                default:
                    ViewBag.Message = "查詢方式異常，請重新搜尋";
                    return View(viewModel);
            }

            if (!sellerCol.Any())
            {
                ViewBag.Message = "查無相關資訊，請重新搜尋";
                return View(viewModel);
            }
            else if (sellerCol.Count == 1)
            {
                //only one seller
                return RedirectToAction("BookingStore", new { sellerGuid = sellerCol.First().Guid });
            }
            else
            {
                //multiple seller
                List<BookingSellerModel> bookingSellers = new List<BookingSellerModel>();
                foreach (var seller in sellerCol)
                {
                    bookingSellers.Add(new BookingSellerModel { SellerName = seller.SellerName, SellerGuid = seller.Guid });
                }

                int pageSize = SELLER_PAGE_NUMBER; //一頁有幾筆
                int pageNumber = 1;
                int pageIndex = pageNumber - 1;

                var query = (from t in bookingSellers select t);
                var paging = new PagerList<BookingSellerModel>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                pageIndex,
                query.ToList().Count);

                viewModel.BookingSellerModels = paging;

                TempData["TotalSeller"] = bookingSellers;
                TempData["BookingSearchType"] = viewModel.BookingSearchType;
                TempData["SearchExpression"] = viewModel.SearchExpression;
                return View(viewModel);
            }
        }

        #endregion BookingSystemSearch

        #region BookingSellerList

        [VbsAuthorize]
        public ActionResult BookingSellerList(BookingSellerListViewModel viewModel, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingSellerList");

            if (pageNumber != null && TempData["bookingSellerList"] != null)
            {
                List<BookingSellerListModel> bookingSellerList = TempData["bookingSellerList"] as List<BookingSellerListModel>;
                if (bookingSellerList != null)
                {

                    int pageSize = SELLER_PAGE_NUMBER; //一頁有幾筆
                    int pageIndex = pageNumber.Value - 1;

                    var query = (from t in bookingSellerList select t);
                    var paging = new PagerList<BookingSellerListModel>(
                        query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                        new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                        pageIndex,
                        query.ToList().Count);

                    viewModel.SellerListModels = paging;
                    TempData["bookingSellerList"] = bookingSellerList;
                }
            }

            return View(viewModel);
        }

        #endregion BookingSellerList

        #region BookingStore

        [VbsAuthorize]
        public ActionResult BookingStores()
        {
            BookingSystemCommonInfo("BookingStores");

            var viewModel = new BookingSystemStoresViewModel();
            if (TempData["bookingStoreList"] != null)
            {
                List<BookingSystemStoreViewModel> bookingStoreList = TempData["bookingStoreList"] as List<BookingSystemStoreViewModel>;

                int pageSize = STORE_PAGE_NUMBER; //一頁有幾筆
                int pageIndex = 0;

                var query = (from t in bookingStoreList select t);
                var paging = new PagerList<BookingSystemStoreViewModel>(
                    query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                    new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                    pageIndex,
                    query.ToList().Count);

                viewModel.Stores = paging;
                TempData["bookingStoreList"] = bookingStoreList;
                return View("BookingStore", viewModel);
            }

            return RedirectToAction("BookingNoPermission");
        }

        [VbsAuthorize]
        public ActionResult BookingStore(Guid? sellerGuid, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingStore");

            if (TempData["bookingStoreList"] != null && !sellerGuid.HasValue)
            {
                var tempModel = new BookingSystemStoresViewModel();
                List<BookingSystemStoreViewModel> bookingStoreList = TempData["bookingStoreList"] as List<BookingSystemStoreViewModel>;

                int pageSize = STORE_PAGE_NUMBER; //一頁有幾筆
                pageNumber = pageNumber ?? 1;
                int pageIndex = pageNumber.Value - 1;

                var query = (from t in bookingStoreList select t);
                var paging = new PagerList<BookingSystemStoreViewModel>(
                    query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                    new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                    pageIndex,
                    query.ToList().Count);

                tempModel.Stores = paging;
                TempData["bookingStoreList"] = bookingStoreList;
                return View("BookingStore", tempModel);
            }
            var stores = new List<Seller>();

            var seller = _sp.SellerGet(sellerGuid.ToGuid());
            if (seller.IsLoaded && seller != null)
            {
                if (seller.IsOpenBooking && !string.IsNullOrEmpty(seller.StoreAddress))
                {
                    stores.Add(seller);
                }
            }

            stores.AddRange(SellerFacade.GetChildrenSellers(sellerGuid.ToGuid()));

            if ((stores.Where(x => x.IsOpenBooking)).Any())
            {
                if ((stores.Where(x => x.IsOpenBooking)).Count() > 1)
                {
                    List<BookingSystemStoreViewModel> vStores = new List<BookingSystemStoreViewModel>();
                    foreach (var store in stores.Where(x => x.IsOpenBooking).OrderBy(m => m.CreateTime))
                    {
                        BookingSystemStoreViewModel viewModel = new BookingSystemStoreViewModel();
                        viewModel.StoreName = store.SellerName;
                        viewModel.StoreGuid = store.Guid;
                        viewModel.IsDefalutSetting =
                            _bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(store.Guid).Any();
                        vStores.Add(viewModel);
                    }
                    BookingSystemStoresViewModel vModel = new BookingSystemStoresViewModel();

                    int pageSize = STORE_PAGE_NUMBER;
                    pageNumber = pageNumber ?? 1;
                    int pageIndex = pageNumber.Value - 1;

                    var query = (from t in vStores select t);
                    var paging = new PagerList<BookingSystemStoreViewModel>(
                    query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                    new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                    pageIndex,
                    query.ToList().Count);

                    vModel.Stores = paging;

                    return View(vModel);

                }
                else
                {
                    if (IsStoreDefaultSetting((stores.Where(x => x.IsOpenBooking)).First().Guid))
                    {
                        //有設定值，導向BookingStoreReservationList
                        return RedirectToAction("BookingStoreReservationList", new { storeGuid = (stores.Where(x => x.IsOpenBooking)).First().Guid });
                    }
                    else
                    {
                        //無設定值，進入首次設定模式
                        return RedirectToAction("BookingFirst", new { storeGuid = (stores.Where(x => x.IsOpenBooking)).First().Guid });
                    }

                }
            }

            //檢查帳號是否屬於店鋪類型

            return RedirectToAction("BookingNoPermission");

        }

        #endregion BookingStore

        #region BookingFirst

        [VbsAuthorize]
        public ActionResult BookingFirst(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingFirst");

            //檢查storeGuid
            if (!IsEmptyOrDefaultGuid(storeGuid))
            {
                var bookingStores = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid());

                BookingFirstViewModel vModel = new BookingFirstViewModel();
                vModel.StoreGuid = storeGuid.ToGuid();
                vModel.IsSetBookingReservationSetting = bookingStores.Where(x => x.BookingType == (int)BookingType.Coupon).Any();
                vModel.IsSetBookingTravelSetting = bookingStores.Where(x => x.BookingType == (int)BookingType.Travel).Any();

                return View(vModel);
            }

            return RedirectToAction("BookingSystemAuthorize");

            //return RedirectToAction("BookingError");
        }

        #endregion BookingFirst

        #region BookingReservationSetup

        [VbsAuthorize]
        public ActionResult BookingReservationSetup(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingReservationSetup");

            CouponBookingDetailModel viewModel = new CouponBookingDetailModel();

            viewModel.StoreGuid = storeGuid.ToGuid();

            //檢查是否已有bookingStoreDate資料

            BookingSystemStore bookingSystemStore = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid(), BookingType.Coupon);

            if (bookingSystemStore.IsLoaded)
            {
                viewModel.StoreGuid = bookingSystemStore.StoreGuid;
                viewModel.BookingStoreId = bookingSystemStore.BookingId;
                viewModel.BookingSystemDateId = _bp.BookingSystemDateGetByStoreBookingId(bookingSystemStore.BookingId).Id;

                viewModel.BookingDateTimeStartHour = bookingSystemStore.BookingOpeningTimeS.ToString("HH");
                viewModel.BookingDateTimeStartMinute = bookingSystemStore.BookingOpeningTimeS.ToString("mm");

                viewModel.BookingDateTimeEndHour = bookingSystemStore.BookingOpeningTimeE.ToString("HH");
                viewModel.BookingDateTimeEndMinute = bookingSystemStore.BookingOpeningTimeE.ToString("mm");
                viewModel.TimeSpan = bookingSystemStore.TimeSpan;
                viewModel.DefaultMaxNumberOfPeople = bookingSystemStore.DefaultMaxNumberOfPeople;

                viewModel.BookingType = bookingSystemStore.BookingType;
                viewModel.Sun = viewModel.SunIsOpening = bookingSystemStore.Sun;
                viewModel.Mon = viewModel.MonIsOpening = bookingSystemStore.Mon;
                viewModel.Tue = viewModel.TueIsOpening = bookingSystemStore.Tue;
                viewModel.Wed = viewModel.WedIsOpening = bookingSystemStore.Wed;
                viewModel.Thu = viewModel.ThuIsOpening = bookingSystemStore.Thu;
                viewModel.Fri = viewModel.FriIsOpening = bookingSystemStore.Fri;
                viewModel.Sat = viewModel.SatIsOpening = bookingSystemStore.Sat;


                //DetailedSetting

                if (bookingSystemStore.IsDetailedSetting)
                {
                    var monBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.MON, null);
                    if (monBookingSystemDate.Id > 0)
                    {
                        viewModel.MonBookingSystemDateId = monBookingSystemDate.Id;
                    }

                    var tueBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.TUE, null);
                    if (tueBookingSystemDate.Id > 0)
                    {
                        viewModel.TueBookingSystemDateId = tueBookingSystemDate.Id;
                    }

                    var wedBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.WED, null);
                    if (wedBookingSystemDate.Id > 0)
                    {
                        viewModel.WedBookingSystemDateId = wedBookingSystemDate.Id;
                    }

                    var thuBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.THU, null);
                    if (thuBookingSystemDate.Id > 0)
                    {
                        viewModel.ThuBookingSystemDateId = thuBookingSystemDate.Id;
                    }

                    var friBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.FRI, null);
                    if (friBookingSystemDate.Id > 0)
                    {
                        viewModel.FriBookingSystemDateId = friBookingSystemDate.Id;
                    }

                    var satBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.SAT, null);
                    if (satBookingSystemDate.Id > 0)
                    {
                        viewModel.SatBookingSystemDateId = satBookingSystemDate.Id;
                    }

                    var sunBookingSystemDate = _bp.BookingSystemDateGet(bookingSystemStore.BookingId, DayType.SUN, null);
                    if (sunBookingSystemDate.Id > 0)
                    {
                        viewModel.SunBookingSystemDateId = sunBookingSystemDate.Id;
                    }
                }
            }

            return View(viewModel);
        }

        [ActionName("BookingReservationSetup")]
        [VbsAuthorize]
        [HttpPost]
        public ActionResult SetCouponBooking(CouponBookingDetailModel bookingDetailModel)
        {
            BookingSystemCommonInfo("BookingReservationSetup");
            ViewBag.ShowAlert = false;
            ViewBag.Message = null;

            int sHour = GetBookingOpeningTime(bookingDetailModel.BookingDateTimeStartHour);
            int sMinute = GetBookingOpeningTime(bookingDetailModel.BookingDateTimeStartMinute);
            int eHour = GetBookingOpeningTime(bookingDetailModel.BookingDateTimeEndHour);
            int eMinute = GetBookingOpeningTime(bookingDetailModel.BookingDateTimeEndMinute);

            #region    計算 TimeSlot 動作

            var timeSlots = new Dictionary<string, int>();

            var startTime = (DateTime.Now).SetTime(sHour, sMinute, 00);
            var endTime = (DateTime.Now).SetTime(eHour, eMinute, 00);

            var defaultStartTime = DateTime.Now.Date.SetTime(0, sMinute, 0);
            var defaultEndTime = DateTime.Now.AddDays(1).Date;

            do
            {
                if (startTime <= defaultStartTime && defaultStartTime <= endTime)
                {
                    timeSlots.Add(defaultStartTime.ToString("HH:mm"), bookingDetailModel.DefaultMaxNumberOfPeople);
                }
                else
                {
                    //未設定預設值的區間
                    timeSlots.Add(defaultStartTime.ToString("HH:mm"), 0);
                }

                defaultStartTime = defaultStartTime.AddMinutes(bookingDetailModel.TimeSpan);
            } while (defaultStartTime < defaultEndTime);

            //抓取已訂位人數資訊
            bookingDetailModel.ReservationInfo = BookingSystemFacade.GetReservationListInfo(bookingDetailModel.BookingStoreId);

            #endregion 計算 TimeSlot 動作

            var tempBookingSystemTimeSlotList = new List<BookingSystemTimeSlot>();

            foreach (var timeslot in timeSlots)
            {
                var item = new BookingSystemTimeSlot
                {
                    Id = 0,
                    BookingSystemDateId = 0,
                    TimeSlot = timeslot.Key,
                    MaxNumberOfPeople = timeslot.Value
                };
                tempBookingSystemTimeSlotList.Add(item);
            }

            bookingDetailModel.DefaultTimeSlotList = tempBookingSystemTimeSlotList;

            #region detailedTimeSlotSetup

            #region Mon

            if (bookingDetailModel.MonBookingSystemDateId > 0)
            {
                var tempMonTimeSlotList = new List<BookingSystemTimeSlot>();
                var monTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.MonBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = monTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempMonTimeSlotList.Add(item);
                }
                bookingDetailModel.MonTimeSlotList = tempMonTimeSlotList;
            }
            else
            {
                bookingDetailModel.MonTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.MonIsOpening = bookingDetailModel.Mon;

            #endregion Mon

            #region Tue

            if (bookingDetailModel.TueBookingSystemDateId > 0)
            {
                var tempTueTimeSlotList = new List<BookingSystemTimeSlot>();
                var tueTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.TueBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = tueTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempTueTimeSlotList.Add(item);
                }
                bookingDetailModel.TueTimeSlotList = tempTueTimeSlotList;
            }
            else
            {
                bookingDetailModel.TueTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.TueIsOpening = bookingDetailModel.Tue;

            #endregion Tue

            #region Wed

            if (bookingDetailModel.WedBookingSystemDateId > 0)
            {
                var tempWedTimeSlotList = new List<BookingSystemTimeSlot>();
                var wedTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.WedBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = wedTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempWedTimeSlotList.Add(item);
                }
                bookingDetailModel.WedTimeSlotList = tempWedTimeSlotList;
            }
            else
            {
                bookingDetailModel.WedTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.WedIsOpening = bookingDetailModel.Wed;

            #endregion Wed

            #region Thu

            if (bookingDetailModel.ThuBookingSystemDateId > 0)
            {
                var tempThuTimeSlotList = new List<BookingSystemTimeSlot>();
                var thuTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.ThuBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = thuTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempThuTimeSlotList.Add(item);
                }
                bookingDetailModel.ThuTimeSlotList = tempThuTimeSlotList;
            }
            else
            {
                bookingDetailModel.ThuTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.ThuIsOpening = bookingDetailModel.Thu;

            #endregion Thu

            #region Fri

            if (bookingDetailModel.FriBookingSystemDateId > 0)
            {
                var tempFriTimeSlotList = new List<BookingSystemTimeSlot>();
                var friTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.FriBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = friTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempFriTimeSlotList.Add(item);
                }
                bookingDetailModel.FriTimeSlotList = tempFriTimeSlotList;
            }
            else
            {
                bookingDetailModel.FriTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.FriIsOpening = bookingDetailModel.Fri;

            #endregion Fri

            #region Sat

            if (bookingDetailModel.SatBookingSystemDateId > 0)
            {
                var tempSatTimeSlotList = new List<BookingSystemTimeSlot>();
                var satTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.SatBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = satTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempSatTimeSlotList.Add(item);
                }
                bookingDetailModel.SatTimeSlotList = tempSatTimeSlotList;
            }
            else
            {
                bookingDetailModel.SatTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.SatIsOpening = bookingDetailModel.Sat;

            #endregion Sat

            #region Sun

            if (bookingDetailModel.SunBookingSystemDateId > 0)
            {
                var tempSunTimeSlotList = new List<BookingSystemTimeSlot>();
                var sunTimeSlotList = BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(bookingDetailModel.SunBookingSystemDateId).ToList();

                foreach (var timeslot in timeSlots)
                {
                    var tempTimeSlot = sunTimeSlotList.SingleOrDefault(x => x.TimeSlot == timeslot.Key);

                    var item = new BookingSystemTimeSlot
                    {
                        Id = tempTimeSlot == null ? 0 : tempTimeSlot.Id,
                        BookingSystemDateId = tempTimeSlot == null ? 0 : tempTimeSlot.BookingSystemDateId,
                        TimeSlot = timeslot.Key,
                        MaxNumberOfPeople = timeslot.Value
                    };

                    tempSunTimeSlotList.Add(item);
                }
                bookingDetailModel.SunTimeSlotList = tempSunTimeSlotList;
            }
            else
            {
                bookingDetailModel.SunTimeSlotList = tempBookingSystemTimeSlotList;
            }
            bookingDetailModel.SunIsOpening = bookingDetailModel.Sun;

            #endregion Sun

            #endregion detailedTimeSlotSetup

            return View("BookingReservationDetailSetup", bookingDetailModel);
        }

        #endregion BookingReservationSetup

        #region BookingReservationDetailSetup

        [VbsAuthorize]
        public ActionResult BookingReservationDetailSetup(CouponBookingDetailModel couponBookingDetail)
        {
            BookingSystemCommonInfo("BookingReservationDetailSetup");
            ViewBag.BookingStoreGuid = couponBookingDetail.StoreGuid;

            return View(couponBookingDetail);
        }

        [ActionName("BookingReservationDetailSetup")]
        [VbsAuthorize]
        [HttpPost]
        public ActionResult SetCouponDetailBooking(CouponBookingDetailModel couponBookingDetail)
        {
            BookingSystemCommonInfo("BookingReservationDetailSetup");

            var changelogs = new List<KeyValuePair<string, string>>();

            changelogs.Add(new KeyValuePair<string, string>("DefaultSetting", BookingType.Coupon.ToString()));

            BookingSystemStore store;
            store = _bp.BookingSystemStoreGetByStoreGuid(couponBookingDetail.StoreGuid, BookingType.Coupon);
            if (!store.IsLoaded)
            {
                store = new BookingSystemStore();
                store.StoreGuid = couponBookingDetail.StoreGuid;
            }
            store.BookingType = (int)BookingType.Coupon;

            #region ChangeLog

            if (store.Mon != couponBookingDetail.Mon)
            {
                changelogs.Add(new KeyValuePair<string, string>("Mon", store.Mon + "-->" + couponBookingDetail.Mon));
            }
            if (store.Tue != couponBookingDetail.Tue)
            {
                changelogs.Add(new KeyValuePair<string, string>("Tue", store.Tue + "-->" + couponBookingDetail.Tue));
            }
            if (store.Wed != couponBookingDetail.Wed)
            {
                changelogs.Add(new KeyValuePair<string, string>("Wed", store.Wed + "-->" + couponBookingDetail.Wed));
            }
            if (store.Thu != couponBookingDetail.Thu)
            {
                changelogs.Add(new KeyValuePair<string, string>("Thu", store.Thu + "-->" + couponBookingDetail.Thu));
            }
            if (store.Fri != couponBookingDetail.Fri)
            {
                changelogs.Add(new KeyValuePair<string, string>("Fri", store.Fri + "-->" + couponBookingDetail.Fri));
            }
            if (store.Sat != couponBookingDetail.Sat)
            {
                changelogs.Add(new KeyValuePair<string, string>("Sat", store.Sat + "-->" + couponBookingDetail.Sat));
            }
            if (store.Sun != couponBookingDetail.Sun)
            {
                changelogs.Add(new KeyValuePair<string, string>("Sun", store.Sun + "-->" + couponBookingDetail.Sun));
            }
            if (store.TimeSpan != couponBookingDetail.TimeSpan)
            {
                changelogs.Add(new KeyValuePair<string, string>("TimeSpan", store.TimeSpan + "-->" + couponBookingDetail.TimeSpan));
            }
            if (store.DefaultMaxNumberOfPeople != couponBookingDetail.DefaultMaxNumberOfPeople)
            {
                changelogs.Add(new KeyValuePair<string, string>("DefaultMaxNumberOfPeople", store.DefaultMaxNumberOfPeople + "-->" + couponBookingDetail.DefaultMaxNumberOfPeople));
            }

            #endregion ChangeLog

            store.Mon = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.MonIsOpening : couponBookingDetail.Mon;
            store.Tue = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.TueIsOpening : couponBookingDetail.Tue;
            store.Wed = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.WedIsOpening : couponBookingDetail.Wed;
            store.Thu = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.ThuIsOpening : couponBookingDetail.Thu;
            store.Fri = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.FriIsOpening : couponBookingDetail.Fri;
            store.Sat = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.SatIsOpening : couponBookingDetail.Sat;
            store.Sun = couponBookingDetail.IsDetailedSetting ? couponBookingDetail.SunIsOpening : couponBookingDetail.Sun;

            store.IsDetailedSetting = (store.IsDetailedSetting || couponBookingDetail.IsDetailedSetting);

            int sHour = GetBookingOpeningTime(couponBookingDetail.BookingDateTimeStartHour);
            int sMinute = GetBookingOpeningTime(couponBookingDetail.BookingDateTimeStartMinute);
            int eHour = GetBookingOpeningTime(couponBookingDetail.BookingDateTimeEndHour);
            int eMinute = GetBookingOpeningTime(couponBookingDetail.BookingDateTimeEndMinute);

            store.BookingOpeningTimeS = (DateTime.Now).SetTime(sHour, sMinute, 00);
            store.BookingOpeningTimeE = (DateTime.Now).SetTime(eHour, eMinute, 00);

            store.TimeSpan = couponBookingDetail.TimeSpan;
            store.DefaultMaxNumberOfPeople = couponBookingDetail.DefaultMaxNumberOfPeople;

            Dictionary<string, int> timeSlots = new Dictionary<string, int>();

            foreach (var item in couponBookingDetail.DefaultTimeSlotList.Where(x => x.MaxNumberOfPeople > 0).OrderBy(x => x.TimeSlot))
            {
                timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
            }

            couponBookingDetail.BookingStoreId = BookingSystemFacade.MakeBookingSystemStoreDefaultSetting(store, timeSlots, ref changelogs);


            #region DetailedSetup 詳細設定


            #region DayType.MON

            if (couponBookingDetail.IsDetailedSetting)
            { 
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.MonTimeSlotList
                                        .Where(x => x.MaxNumberOfPeople > 0)
                                        .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.MON, null,
                                                                store.Mon, timeSlots,
                                                                ref changelogs);

            #endregion DayType.MON

            #region DayType.TUE

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.TueTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.TUE, null,
                                                                store.Tue, timeSlots,
                                                                ref changelogs);

            #endregion DayType.TUE

            #region DayType.WED

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.WedTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.WED, null,
                                                                store.Wed, timeSlots,
                                                                ref changelogs);

            #endregion DayType.WED

            #region DayType.THU

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.ThuTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.THU, null,
                                                                store.Thu, timeSlots,
                                                                ref changelogs);

            #endregion DayType.THU

            #region DayType.FRI

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.FriTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.FRI, null,
                                                                store.Fri, timeSlots,
                                                                ref changelogs);

            #endregion DayType.FRI

            #region DayType.SAT

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.SatTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.SAT, null,
                                                                store.Sat, timeSlots,
                                                                ref changelogs);

            #endregion DayType.SAT

            #region DayType.SUN

            if (couponBookingDetail.IsDetailedSetting)
            {
                timeSlots.Clear();
                foreach (var item in couponBookingDetail.SunTimeSlotList
                    .Where(x => x.MaxNumberOfPeople > 0)
                    .OrderBy(x => x.TimeSlot))
                {
                    timeSlots.Add(item.TimeSlot, item.MaxNumberOfPeople);
                }
            }
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.SUN, null,
                                                                store.Sun, timeSlots,
                                                                ref changelogs);

            #endregion DayType.SUN
          
            #endregion DetailedSetup

            if (couponBookingDetail.BookingStoreId > 0)
            {
                if (changelogs.Count > 1)
                {
                    var viewBookingSystemStore = _bp.ViewBookingSystemStoreGetByStoreGuid(couponBookingDetail.StoreGuid,
                                                                                          BookingType.Coupon);
                    var defaultBookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, DayType.DefaultSet, null);

                    var bookingSettingChangeLogMailInfo = new BookingSettingChangeLogMailInfomation
                    {
                        VbsAccount = VbsCurrent.AccountId,
                        SellerName = viewBookingSystemStore.SellerName,
                        StoreName = viewBookingSystemStore.StoreName,
                        BookingSystemStoreId =
                            viewBookingSystemStore.BookingId,
                        BookingSystemDateId = defaultBookingSystemDate.Id,
                        ModifyTime = DateTime.Now,
                        ChangeSettingLogs = changelogs
                    };

                    BookingSystemFacade.SendSettingChangeLogMail(_conf.BookingSystemChangeNoticeEmail,
                                                                 bookingSettingChangeLogMailInfo);
                }
                return RedirectToAction("BookingStoreReservationList", new { storeGuid = couponBookingDetail.StoreGuid });
            }

            return RedirectToAction("BookingError");
        }

        #endregion BookingReservationDetailSetup

        #region BookingTravelSetup

        [VbsAuthorize]
        public ActionResult BookingTravelSetup(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingTravelSetup");

            var viewModel = new TravelBookingDetailModel { StoreGuid = storeGuid.ToGuid(), BookingType = (int)BookingType.Travel };

            //檢查是否已有bookingStoreDate資料

            BookingSystemStore bookingSystemStore = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid(), BookingType.Travel);

            if (bookingSystemStore.IsLoaded)
            {
                viewModel.StoreGuid = bookingSystemStore.StoreGuid;
                viewModel.BookingStoreId = bookingSystemStore.BookingId;
                viewModel.BookingSystemDateId = _bp.BookingSystemDateGetByStoreBookingId(bookingSystemStore.BookingId).Id;

                viewModel.DefaultMaxNumberOfPeople = bookingSystemStore.DefaultMaxNumberOfPeople;
                viewModel.BookingType = bookingSystemStore.BookingType;

                ViewBookingSystemStoreDateCollection viewBookingSystemStoreDates = _bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(storeGuid.ToGuid());

                viewModel.MonMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.MON).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.TueMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.TUE).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.WedMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.WED).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.ThuMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.THU).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.FriMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.FRI).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.SatMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.SAT).First().BookingSystemDateId).First().MaxNumberOfPeople;
                viewModel.SunMaxNumberOfPeople = _bp.BookingSystemTimeSlotGet(viewBookingSystemStoreDates.Where(x => x.DayType == (int)DayType.SUN).First().BookingSystemDateId).First().MaxNumberOfPeople;
            }

            return View(viewModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult BookingTravelSetup(TravelBookingDetailModel model)
        {
            BookingSystemCommonInfo("BookingTravelSetup");

            var changelogs = new List<KeyValuePair<string, string>>();

            changelogs.Add(new KeyValuePair<string, string>(model.StoreGuid.ToString(), BookingType.Travel.ToString()));

            BookingSystemStore bookingSystemStore;

            bookingSystemStore = _bp.BookingSystemStoreGetByStoreGuid(model.StoreGuid, BookingType.Travel);

            //1.建立預設的 DayType=8
            if (!bookingSystemStore.IsLoaded)
            {
                bookingSystemStore = new BookingSystemStore();
                bookingSystemStore.StoreGuid = model.StoreGuid;
            }
            bookingSystemStore.BookingType = (int)BookingType.Travel;
            bookingSystemStore.Mon = (model.MonMaxNumberOfPeople > 0);
            bookingSystemStore.Tue = (model.TueMaxNumberOfPeople > 0);
            bookingSystemStore.Wed = (model.WedMaxNumberOfPeople > 0);
            bookingSystemStore.Thu = (model.ThuMaxNumberOfPeople > 0);
            bookingSystemStore.Fri = (model.FriMaxNumberOfPeople > 0);
            bookingSystemStore.Sat = (model.SatMaxNumberOfPeople > 0);
            bookingSystemStore.Sun = (model.SunMaxNumberOfPeople > 0);

            Dictionary<string, int> traveltimeslot = new Dictionary<string, int>();
            traveltimeslot.Add("00:00", 0);

            BookingSystemFacade.MakeBookingSystemStoreDefaultSetting(bookingSystemStore, traveltimeslot, ref changelogs);

            //2.建立 DayType=1~7，星期設定
            var logs = new List<KeyValuePair<string, string>>();

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.MonMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.MON, null, (model.MonMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.TueMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.TUE, null, (model.TueMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.WedMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.WED, null, (model.WedMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.ThuMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.THU, null, (model.ThuMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.FriMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.FRI, null, (model.FriMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.SatMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.SAT, null, (model.SatMaxNumberOfPeople > 0), traveltimeslot, ref logs);

            traveltimeslot.Clear();
            traveltimeslot.Add("00:00", model.SunMaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(bookingSystemStore.BookingId, DayType.SUN, null, (model.SunMaxNumberOfPeople > 0), traveltimeslot, ref logs);



            return RedirectToAction("BookingStoreReservationList", "BookingSystem", new { storeGuid = model.StoreGuid });
        }

        #endregion BookingTravelSetup

        #region BookingStoreReservationList

        [VbsAuthorize]
        public ActionResult BookingStoreReservationList(Guid? storeGuid, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingStoreReservationList");

            if (storeGuid != null && !IsStoreDefaultSetting(storeGuid.ToGuid()))
            {
                return RedirectToAction("BookingFirst", new { storeGuid = storeGuid.ToGuid() });
            }

            ViewBag.IsOpenSetting = IsStoreOpenReservationSetting(storeGuid.ToGuid());

            var storeReservationDateInfos = new List<StoreReservationDateInfo>();
            DateTime todayDate = DateTime.Now.Date;
            DateTime validDate = DateTime.Now.AddDays(15).Date;

            #region 未來90天的預約資訊

            BookingSystemStoreCollection stores = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid());
            ViewBookingSystemStoreDateCollection storeDateSettings = _bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(storeGuid.ToGuid());

            for (int i = 0; i < 90; i++)
            {
                StoreReservationDateInfo bookingDate = new StoreReservationDateInfo();
                DateTime date = todayDate.AddDays(i).Date;
                bookingDate.BookingDate = date;
                bookingDate.IsValidBooking = (date <= validDate);

                #region 訂位預約設定

                int couponMaxOfNumber = default(int);
                //1.先抓  DayType.AssignDay
                var couponAssignDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)DayType.AssignDay && x.EffectiveDate == date);
                if (couponAssignDateSettings.Any())
                {
                    var assignSetting = couponAssignDateSettings.First();
                    bookingDate.BookingSystemDateId = assignSetting.BookingSystemDateId;
                    bookingDate.IsOpenBooking = assignSetting.IsOpening;

                    ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(assignSetting.BookingSystemStoreBookingId);
                    couponMaxOfNumber = (cols.Any(x => x.DayType == (int)DayType.AssignDay && x.EffectiveDate == date)) ? cols.Single(x => x.DayType == (int)DayType.AssignDay && x.EffectiveDate == date).MaxNumberOfPeople ?? 0 : 0;
                }
                else
                {
                    var couponWeekDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)BookingSystemFacade.GetBookingDayType(date));
                    if (couponWeekDateSettings.Any())
                    {
                        //2.抓 DayType.DayOfWeek
                        var dayOfWeekSetting = couponWeekDateSettings.First();
                        bookingDate.BookingSystemDateId = dayOfWeekSetting.BookingSystemDateId;
                        bookingDate.IsOpenBooking = dayOfWeekSetting.IsOpening;

                        ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(dayOfWeekSetting.BookingSystemStoreBookingId);
                        couponMaxOfNumber = (cols.Any(x => x.DayType == (int)BookingSystemFacade.GetBookingDayType(date))) ? cols.Single(x => x.DayType == (int)BookingSystemFacade.GetBookingDayType(date)).MaxNumberOfPeople ?? 0 : 0;

                    }
                    else
                    {
                        //3.抓 DayType.DefaultSet
                        var bookingDefaultDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)DayType.DefaultSet);
                        if (bookingDefaultDateSettings.Any())
                        {
                            var defaultSetting = bookingDefaultDateSettings.First();
                            bookingDate.BookingSystemDateId = defaultSetting.BookingSystemDateId;
                            bookingDate.IsOpenBooking = (bool)defaultSetting.GetColumnValue(date.DayOfWeek.ToString().Substring(0, 3));

                            ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(defaultSetting.BookingSystemStoreBookingId);
                            couponMaxOfNumber = (cols.Any(x => x.DayType == (int)DayType.DefaultSet)) ? cols.Single(x => x.DayType == (int)DayType.DefaultSet).MaxNumberOfPeople ?? 0 : 0;
                        }
                    }
                }

                if (stores.Any(x => x.BookingType == (int)BookingType.Coupon))
                {
                    List<ViewBookingSystemStoreReservationCount> storeCouponReservations = _bp.ViewBookingSystemReservationCountGet((stores.Where(x => x.BookingType == (int)BookingType.Coupon)).First(), date, date.AddDays(1).AddSeconds(-1)).ToList();
                    bookingDate.CouponBookingCount = (storeCouponReservations.Any()) ? (storeCouponReservations.First().NumberOfPeople ?? default(int)) : 0;
                    bookingDate.IsCouponBookingOver = bookingDate.CouponBookingCount >= couponMaxOfNumber;
                    var reservationCouponList = _bp.ViewBookingSystemReservationListGet((stores.Where(x => x.BookingType == (int)BookingType.Coupon)).First(), date);
                    bookingDate.IsCancelCouponReservation = reservationCouponList.Any(x => x.IsCancel);
                }

                #endregion 訂位預約設定

                #region 訂房預約設定
                int travelMaxOfNumber = default(int);
                //1.先抓  DayType.AssignDay
                var bookingAssignDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Travel && x.DayType == (int)DayType.AssignDay && x.EffectiveDate == date);
                if (bookingAssignDateSettings.Any())
                {
                    var assignSetting = bookingAssignDateSettings.First();
                    bookingDate.BookingSystemDateId = assignSetting.BookingSystemDateId;
                    bookingDate.IsOpenBooking = bookingDate.IsOpenBooking | assignSetting.IsOpening;

                    BookingSystemTimeSlotCollection cols = _bp.BookingSystemTimeSlotGet(assignSetting.BookingSystemDateId);
                    travelMaxOfNumber = (cols.Any(x => x.TimeSlot == "00:00")) ? cols.Single(x => x.TimeSlot == "00:00").MaxNumberOfPeople : 0;

                }
                else
                {
                    //2.抓 DayType.DefaultSet
                    var bookingDefaultDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Travel && x.DayType == (int)DayType.DefaultSet);
                    if (bookingDefaultDateSettings.Any())
                    {
                        var defaultSetting = bookingDefaultDateSettings.First();
                        bookingDate.BookingSystemDateId = defaultSetting.BookingSystemDateId;
                        bookingDate.IsOpenBooking = bookingDate.IsOpenBooking | (bool)defaultSetting.GetColumnValue(date.DayOfWeek.ToString().Substring(0, 3));

                        BookingSystemDate travelDate = _bp.BookingSystemDateGet(stores.Where(x => x.BookingType == (int)BookingType.Travel).Select(x => x.BookingId).First(), BookingSystemFacade.GetBookingDayType(date), null);
                        if (travelDate.IsLoaded)
                        {
                            BookingSystemTimeSlotCollection cols = _bp.BookingSystemTimeSlotGet(travelDate.Id);
                            travelMaxOfNumber = (cols.Any(x => x.TimeSlot == "00:00")) ? cols.Single(x => x.TimeSlot == "00:00").MaxNumberOfPeople : 0;
                        }
                    }

                }

                if (stores.Where(x => x.BookingType == (int)BookingType.Travel).Any())
                {
                    List<ViewBookingSystemStoreReservationCount> storeTravelReservations = _bp.ViewBookingSystemReservationCountGet(stores.Where(x => x.BookingType == (int)BookingType.Travel).First(), date, date.AddDays(1).AddSeconds(-1)).ToList();
                    bookingDate.TravelBookingCount = (storeTravelReservations.Any()) ? (storeTravelReservations.First().NumberOfPeople ?? default(int)) : 0;
                    bookingDate.IsTravelBookingOver = bookingDate.TravelBookingCount >= travelMaxOfNumber;

                    var reservationTravelList = _bp.ViewBookingSystemReservationListGet((stores.Where(x => x.BookingType == (int)BookingType.Travel)).First(), date);
                    bookingDate.IsCancelCouponReservation = reservationTravelList.Any(x => x.IsCancel);
                }

                #endregion 訂房預約設定

                storeReservationDateInfos.Add(bookingDate);
            }

            #endregion 未來90天的預約資訊

            StoreReservationListViewModel vModel = new StoreReservationListViewModel();
            vModel.StoreGuid = storeGuid.ToGuid();
            //vModel.StoreReservationDateInfos = BookingSystemFacade.GetPagerList(storeReservationDateInfos,);

            int pageSize = RESERVATION_CALENDAR_PAGE_NUMBER; //一頁有幾筆
            pageNumber = pageNumber ?? 1;
            int pageIndex = pageNumber.Value - 1;

            var query = (from t in storeReservationDateInfos select t);
            var paging = new PagerList<StoreReservationDateInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
            pageIndex,
            query.ToList().Count);

            vModel.StoreReservationDateInfos = paging;

            return View(vModel);
        }

        [VbsAuthorize]
        [HttpPost]
        public ActionResult BookingStoreReservationList(StoreReservationListViewModel viewModel)
        {
            BookingSystemCommonInfo("BookingStoreReservationList");
            ViewBag.IsOpenSetting = true;

            return View(viewModel);
        }

        #endregion BookingStoreReservationList

        #region BookingSetupType

        [VbsAuthorize]
        public ActionResult BookingSetupType(Guid? storeGuid, int bookingDateId, DateTime effectiveDate)
        {
            BookingSystemCommonInfo("BookingSetupType");

            BookingSetupTypeViewModel vModel = new BookingSetupTypeViewModel();
            vModel.StoreGuid = storeGuid.ToGuid();
            vModel.BookingSystemDateId = bookingDateId;
            vModel.EffectiveDate = effectiveDate;

            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult BookingSetupType(BookingSetupTypeViewModel model)
        {
            BookingSystemCommonInfo("BookingSetupType");

            if (model.BookingType == (int)BookingType.Coupon)
            {
                return RedirectToAction("BookingDailyCouponSetup", "BookingSystem",
                                   new { storeGuid = model.StoreGuid, bookingDateId = model.BookingSystemDateId, effectiveDate = model.EffectiveDate, bookingType = model.BookingType });
            }
            else
            {
                return RedirectToAction("BookingDailyTravelSetup", "BookingSystem",
                                  new { storeGuid = model.StoreGuid, bookingDateId = model.BookingSystemDateId, effectiveDate = model.EffectiveDate, bookingType = model.BookingType });
            }
        }

        #endregion BookingSetupType

        #region BookingDailyCouponSetup

        [VbsAuthorize]
        public ActionResult BookingDailyCouponSetup(Guid? storeGuid, int bookingDateId, DateTime effectiveDate, int bookingType)
        {
            BookingSystemCommonInfo("BookDailyCouponSetup");

            BookingSystemStore store = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid(), BookingType.Coupon);

            DailyStoreBookingTimeSlot vModel = new DailyStoreBookingTimeSlot();
            vModel.StoreGuid = storeGuid.ToGuid();
            vModel.EffectiveDate = effectiveDate;

            BookingSystemDate bookingSystemDate;
            //1，先找有無DayType.AssignDayg設定
            bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, DayType.AssignDay, effectiveDate);
            if (bookingSystemDate.IsLoaded)
            {
                vModel.IsOpen = bookingSystemDate.IsOpening;
            }
            else
            {
                //2.訂房類初始化先找有無預設星期幾的預設值
                bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, BookingSystemFacade.GetBookingDayType(effectiveDate), null);
                if (!bookingSystemDate.IsLoaded)
                {
                    bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, DayType.DefaultSet, null);
                }
                vModel.IsOpen = (bool)store.GetColumnValue(effectiveDate.DayOfWeek.ToString().Substring(0, 3));
            }
            vModel.BookingSystemDateId = bookingSystemDate.Id;


            List<ViewBookingSystemStoreReservationCount> storeReservations = _bp.ViewBookingSystemReservationCountGet(store, effectiveDate, effectiveDate.AddDays(1).AddSeconds(-1)).ToList();
            List<ViewBookingSystemReservationList> reservationList = _bp.ViewBookingSystemReservationListGet(store, effectiveDate).ToList();
            vModel.ReservationTotal = (storeReservations.Any()) ? (storeReservations.First().NumberOfPeople ?? default(int)) : 0;

            List<BookingSystemReservationTimeSlot> reservationTimeSlots = new List<BookingSystemReservationTimeSlot>();

            #region    計算 TimeSlot 動作

            Dictionary<string, int> timeSlots = new Dictionary<string, int>();

            DateTime defaultStartTime = (store.BookingOpeningTimeS.Minute == 30) ? DateTime.Now.Date.SetTime(0, store.BookingOpeningTimeS.Minute, 0) : DateTime.Now.Date;
            DateTime defaultEndTime = DateTime.Now.AddDays(1).Date;

            do
            {
                //ALL Time Slot區間
                timeSlots.Add(defaultStartTime.ToString("HH:mm"), 0);

                defaultStartTime = defaultStartTime.AddMinutes(store.TimeSpan);
            } while (defaultStartTime < defaultEndTime);

            #endregion 計算 TimeSlot 動作

            var exitTimeSlot = _bp.BookingSystemTimeSlotGet(bookingSystemDate.Id).ToList();
            foreach (var item in timeSlots)
            {
                BookingSystemReservationTimeSlot reservationTimeSlot = new BookingSystemReservationTimeSlot();
                if (exitTimeSlot.Any(x => x.TimeSlot == item.Key))
                {
                    reservationTimeSlot.Id = (exitTimeSlot.Where(x => x.TimeSlot == item.Key)).First().Id;
                    reservationTimeSlot.TimeSlot = (exitTimeSlot.Where(x => x.TimeSlot == item.Key)).First().TimeSlot;
                    reservationTimeSlot.BookingSystemDateId = (exitTimeSlot.Where(x => x.TimeSlot == item.Key)).First().BookingSystemDateId;
                    reservationTimeSlot.MaxNumberOfPeople = (exitTimeSlot.Where(x => x.TimeSlot == item.Key)).First().MaxNumberOfPeople;

                    var reservationOfPeoples =
                        (reservationList.Where(x => x.TimeSlot == (exitTimeSlot.Where(t => t.TimeSlot == item.Key)).First().TimeSlot && !x.IsCancel)
                                        .GroupBy(x => x.TimeSlot)
                                        .Select(x => new { ReservationOfPeople = x.Sum(p => p.NumberOfPeople) }));

                    reservationTimeSlot.ReservationOfPeople = (reservationOfPeoples.Any()) ? reservationOfPeoples.First().ReservationOfPeople : 0;
                }
                else
                {
                    reservationTimeSlot.Id = 0;
                    reservationTimeSlot.TimeSlot = item.Key;
                    reservationTimeSlot.BookingSystemDateId = 0;
                    reservationTimeSlot.MaxNumberOfPeople = (bookingSystemDate.DayType == (int)DayType.DefaultSet) ? item.Value : 0;

                    reservationTimeSlot.ReservationOfPeople = 0;
                }
                reservationTimeSlots.Add(reservationTimeSlot);
            }

            vModel.TimeSlots = reservationTimeSlots;



            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult BookingDailyCouponSetup(DailyStoreBookingTimeSlot model)
        {
            BookingSystemCommonInfo("BookDailyCouponSetup");
            ViewBag.Message = "";

            BookingSystemStore store = _bp.BookingSystemStoreGetByStoreGuid(model.StoreGuid, BookingType.Coupon);

            Dictionary<string, int> timeSlots = new Dictionary<string, int>();

            foreach (var timeSlot in model.TimeSlots.Where(x => x.MaxNumberOfPeople > 0))
            {
                timeSlots.Add(timeSlot.TimeSlot, timeSlot.MaxNumberOfPeople);
            }

            var changelogs = new List<KeyValuePair<string, string>>();

            changelogs.Add(new KeyValuePair<string, string>("日期", model.EffectiveDate.ToString("yyyy/MM/dd")));

            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.AssignDay, model.EffectiveDate, model.IsOpen,
                                                             timeSlots, ref changelogs);
            if (changelogs.Count > 1)
            {
                var viewBookingSystemStore = _bp.ViewBookingSystemStoreGetByStoreGuid(model.StoreGuid, BookingType.Coupon);
                var bookingSettingChangeLogMailInfo = new BookingSettingChangeLogMailInfomation
                {
                    VbsAccount = VbsCurrent.AccountId,
                    SellerName = viewBookingSystemStore.SellerName,
                    StoreName = viewBookingSystemStore.StoreName,
                    BookingSystemStoreId = viewBookingSystemStore.BookingId,
                    BookingSystemDateId = model.BookingSystemDateId,
                    ModifyTime = DateTime.Now,
                    ChangeSettingLogs = changelogs
                };

                BookingSystemFacade.SendSettingChangeLogMail(_conf.BookingSystemChangeNoticeEmail, bookingSettingChangeLogMailInfo);
            }
            BookingSystemFacade.UpdateBookingSystemReservation(store, model.EffectiveDate);

            return RedirectToAction("BookingStoreReservationList", "BookingSystem", new { storeGuid = model.StoreGuid });

        }

        #endregion BookingDailySetup

        #region BookingDailyTravelSetup

        [VbsAuthorize]
        public ActionResult BookingDailyTravelSetup(Guid? storeGuid, int bookingDateId, DateTime effectiveDate, int bookingType)
        {
            BookingSystemCommonInfo("BookDailyTravelSetup");

            var vModel = new BookingDailyTravelSetupViewModel { StoreGuid = storeGuid.ToGuid(), EffectiveDate = effectiveDate, BookingSystemDateId = bookingDateId };

            BookingSystemStore store = _bp.BookingSystemStoreGetByStoreGuid(storeGuid.ToGuid(), BookingType.Travel);

            BookingSystemDate bookingSystemDate;
            //1，先找有無DayType.AssignDayg設定
            bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, DayType.AssignDay, effectiveDate);
            if (!bookingSystemDate.IsLoaded)
            {
                //2.訂房類初始化先找有無預設星期幾的預設值
                bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, BookingSystemFacade.GetBookingDayType(effectiveDate), null);
                if (!bookingSystemDate.IsLoaded)
                {
                    bookingSystemDate = _bp.BookingSystemDateGet(store.BookingId, DayType.DefaultSet, null);
                }
            }

            vModel.IsOpen = bookingSystemDate.IsOpening;
            BookingSystemTimeSlotCollection timeSlots = _bp.BookingSystemTimeSlotGet(bookingSystemDate.Id);

            vModel.MaxNumberOfPeople = (timeSlots.Any()) ? timeSlots.First().MaxNumberOfPeople : 0;

            List<ViewBookingSystemStoreReservationCount> storeReservations = _bp.ViewBookingSystemReservationCountGet(store, effectiveDate, effectiveDate.AddDays(1).AddSeconds(-1)).ToList();
            vModel.ReservationTotal = (storeReservations.Any()) ? (storeReservations.First().NumberOfPeople ?? default(int)) : 0;

            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult BookingDailyTravelSetup(BookingDailyTravelSetupViewModel vModel)
        {
            BookingSystemCommonInfo("BookDailyTravelSetup");
            ViewBag.Message = "";
            //DayType.AssignDay 的新增 or 更新
            BookingSystemStore store = _bp.BookingSystemStoreGetByStoreGuid(vModel.StoreGuid, BookingType.Travel);

            var changelogs = new List<KeyValuePair<string, string>>();

            changelogs.Add(new KeyValuePair<string, string>("日期", vModel.EffectiveDate.ToString("yyyy/MM/dd")));

            Dictionary<string, int> timeSlots = new Dictionary<string, int>();
            timeSlots.Add("00:00", vModel.MaxNumberOfPeople);
            BookingSystemFacade.MakeBookingSystemDateSetting(store.BookingId, DayType.AssignDay, vModel.EffectiveDate, vModel.IsOpen,
                                                             timeSlots, ref changelogs);

            if (changelogs.Count > 1)
            {
                var viewBookingSystemStore = _bp.ViewBookingSystemStoreGetByStoreGuid(vModel.StoreGuid, BookingType.Coupon);

                var bookingSettingChangeLogMailInfo = new BookingSettingChangeLogMailInfomation
                {
                    VbsAccount = VbsCurrent.AccountId,
                    SellerName = viewBookingSystemStore.SellerName,
                    StoreName = viewBookingSystemStore.StoreName,
                    BookingSystemStoreId = viewBookingSystemStore.BookingId,
                    BookingSystemDateId = vModel.BookingSystemDateId,
                    ModifyTime = DateTime.Now,
                    ChangeSettingLogs = changelogs
                };

                BookingSystemFacade.SendSettingChangeLogMail("sam_lee@17life.com.tw", bookingSettingChangeLogMailInfo);
            }
            return RedirectToAction("BookingStoreReservationList", "BookingSystem", new { storeGuid = vModel.StoreGuid });
        }

        #endregion BookingDailyTravelSetup

        #region BookingDailyReservation

        [VbsAuthorize]
        public ActionResult BookingDailyReservation(Guid? storeGuid, DateTime effectiveDate, int? pageNumber, bool isExpiredSearch = false)
        {
            BookingSystemCommonInfo("BookingDailyReservation");

            var vModel = new BookingDailyReservationViewModel { StoreGuid = storeGuid.ToGuid(), EffectiveDate = effectiveDate, IsExpiredSearch = isExpiredSearch };

            var reservationList = (_bp.ViewBookingSystemReservationListGet(storeGuid.ToGuid(), effectiveDate)).OrderBy(m => m.TimeSlot).ThenBy(m => m.CreateDatetime).ToList();
            var reservationInfos = new List<ReservationInfoModel>();

            foreach (var item in reservationList)
            {
                var isHidePersonalInfo = item.IsCancel || DateTime.Compare(DateTime.Today, item.ReservationDate) > 0;
                var reservationInfoModel = new ReservationInfoModel
                {
                    BookingSystemReservationId = item.BookingSystemReservationId,
                    BookingSystemStoreBookingId = item.BookingSystemStoreBookingId,
                    ReservationDate = item.ReservationDate,
                    NumberOfPeople = item.NumberOfPeople,
                    Remark = item.Remark,
                    ContactName = isHidePersonalInfo ? string.Format("{0}**", item.ContactName.Substring(0, 1)) : item.ContactName,
                    ContactGender = item.ContactGender,
                    ContactNumber = isHidePersonalInfo ? string.Format("{0}******", item.ContactNumber.Substring(0, 4)) : item.ContactNumber,
                    IsCheckin = item.IsCheckin,
                    IsCancel = item.IsCancel,
                    IsLock = item.IsLock,
                    IsReturn = item.IsReturn,
                    IsPartialCancel = item.IsPartialCancel,
                    CreateDatetime = item.CreateDatetime,
                    TimeSlot = item.TimeSlot,
                    CouponInfo = item.CouponInfo,
                    CouponLink = item.CouponLink,
                    CouponSequenceNumbers = GetReservationCouponSeqenceNumberList(item.BookingSystemReservationId)
                };
                reservationInfos.Add(reservationInfoModel);
            }

            int pageSize = DAILY_RESERVATION_RECORD_PAGE_NUMBER;
            pageNumber = pageNumber ?? 1;
            int pageIndex = pageNumber.Value - 1;

            var query = (from t in reservationInfos select t);
            var paging = new PagerList<ReservationInfoModel>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
            pageIndex,
            query.ToList().Count);

            vModel.ReservationInfos = paging;

            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult CancelTravelReservationLockStatus(int reservationId)
        {
            var reservation = _bp.BookingSystemReservationGet(reservationId);

            if (reservation.IsLoaded)
            {
                reservation.IsLock = false;
                _bp.BookingSystemReservationSet(reservation);

                return Json(new { CancelReservationLockSuccess = true });

            }

            return Json(new { CancelReservationLockSuccess = false });
        }

        [VbsAuthorize, HttpPost]
        public ActionResult CancelBookingReservationRecord(int reservationId)
        {
            ViewBookingSystemReservationList vbsr = _bp.ViewBookingSystemReservationListGet(reservationId);

            if (vbsr != null)
            {
                BookingSystemReservation record = _bp.BookingSystemReservationGet(reservationId);

                if (string.IsNullOrEmpty(record.MemberKey))
                {
                    _bp.SetReservationRecordCancel(reservationId, UserName);
                }
                else
                {
                Member m = _mp.MemberGet(int.Parse(record.MemberKey));
                    string gender = (m.Gender.GetValueOrDefault(0) == (int)GenderType.Unknown)
                                        ? ""
                                        : (m.Gender.GetValueOrDefault(0) == (int)GenderType.Male) ? "先生" : "小姐";

                    ViewBookingSystemStore store =
                        _bp.ViewBookingSystemStoreGetByStoreGuid(
                            _bp.BookingSystemStoreGetBookingId(vbsr.BookingSystemStoreBookingId).StoreGuid,
                            BookingType.Coupon);

                //ReservationCancelMail
                var reservationCancelMailInfo = new ReservationCancelMailInfomation
                {
                    MemberName = m.DisplayName + " " + gender,
                    SellerName = store.SellerName,
                    StoreName = store.StoreName,
                };
                _bp.SetReservationRecordCancel(reservationId, UserName);

                BookingSystemFacade.SendReservationCancelMail(m.UserEmail, reservationCancelMailInfo);
                }
            }


            return Json(new { CancelReservationSuccess = true });
        }

        #endregion BookingDailyReservation

        #region BookingPhoneReservation

        [VbsAuthorize]
        public ActionResult BookingPhoneReservation(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingPhoneReservation");

            var vModel = new BookingPhoneReservationViewModel { StoreGuid = storeGuid.ToGuid() };

            return View(vModel);
        }

        #endregion BookingPhoneReservation

        #region BookingPhoneCouponReservation

        [VbsAuthorize]
        public ActionResult BookingPhoneCouponReservation(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingPhoneCouponReservation");

            var vModel = new BookingPhoneCouponReservationViewModel { StoreGuid = storeGuid.ToGuid(), ContactGender = true, BookingSystemApiKey = Helper.Encrypt("17Life"), FullBookingDate = BookingSystemFacade.GetFullBookingDate(0, storeGuid.ToGuid(), BookingType.Coupon) };

            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult GetStoreTimeSlot(string storeGuid, string queryDate, int bookingType, int numberOfPeople)
        {
            DateTime checkDate = DateTime.Now;
            DateTime.TryParse(queryDate, out checkDate);
            int dateId = 0;
            if (BookingSystemFacade.IsOpenReservationServiceByDate((BookingType)bookingType, storeGuid, checkDate, out dateId))
            {
                var timeSlotCol = (BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(dateId).ToList()).Where(x => x.MaxNumberOfPeople > 0).OrderBy(x => x.TimeSlot).ToList();
                for (int i = 0; i < timeSlotCol.Count; i++)
                {
                    int assignDayReservationOfPeople = BookingSystemFacade.GetReservationPeopleOfNumberByDate(timeSlotCol.ElementAt(i).Id, checkDate);

                    if ((assignDayReservationOfPeople + numberOfPeople) > timeSlotCol.ElementAt(i).MaxNumberOfPeople)
                    {
                        timeSlotCol.RemoveAt(i);
                        i--;
                    }
                }
                return Json(new JsonSerializer().Serialize(timeSlotCol.OrderBy(x => x.TimeSlot)));
            }
            else
            {
                return Json(new JsonSerializer().Serialize(new BookingSystemTimeSlotCollection()));
            }

        }


        [VbsAuthorize, HttpPost]
        public ActionResult MakeReservationRecord(int bookingType, int timeSlot, string reservationDate, int numberOfPeople, string remark, string contactName, bool contactGender, string contactNumber, string memberKey, string orderKey, string couponInfo, string couponLink, string apiKey, string[] couponsSequence, string email, string storeGuid)
        {
            if ((BookingSystemFacade.GetReservationPeopleOfNumberByDate(timeSlot, DateTime.Parse(reservationDate)) + numberOfPeople) > BookingSystemFacade.GetTimeSlotMaxNumberOfPeople(timeSlot))
            {
                return Json(new JsonSerializer().Serialize(new { Success = false, Message = "該時段訂位人數已滿!" }));
            }
            else
            {
                Guid guid;
                if (Guid.TryParse(storeGuid, out guid))
                {
                    BookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreByStoreGuid(guid, (BookingType)bookingType);

                    int dateId;
                    if (!BookingSystemFacade.IsOpenReservationServiceByDate((BookingType)bookingType, storeGuid,
                                                                           DateTime.Parse(reservationDate), out dateId))
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "預約已額滿或店家尚不接受預約!" }));
                    }

                    if (BookingSystemFacade.GetBookingSystemTimeSlotListByBookingSystemDateId(dateId).All(x => x.Id != timeSlot))
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "日期與時段錯誤，請重新選擇日期時段!" }));
                    }
                    
                    BookingSystemServiceName serviceName = BookingSystemFacade.GetBookingSystemServiceName(apiKey, BookingSystemApiServiceType.PponService);

                    BookingSystemMakeReservationRecordResult reservationResult = BookingSystemFacade.MakeReservationRecord((BookingType)bookingType, DateTime.Parse(reservationDate), numberOfPeople, timeSlot, remark, contactName, contactGender, contactNumber, orderKey, memberKey, couponInfo, couponLink, serviceName, couponsSequence, email, guid);
                    if (reservationResult == BookingSystemMakeReservationRecordResult.Success)
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = true, Message = "訂位成功!" }));
                    }
                    else if (reservationResult == BookingSystemMakeReservationRecordResult.DoubleUsingCoupon)
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "您選取的憑證編號有部份已訂過位!" }));
                    }
                    else
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "訂位失敗!" }));
                    }
                }
                else
                {
                    return Json(new JsonSerializer().Serialize(new { Success = false, Message = "該店家沒有提供訂位服務!" }));
                }
            }

        }

        [VbsAuthorize, HttpPost]
        public ActionResult CheckCouponSequence(string storeGuid, string couponSequenceFirst, string couponSequenceSecound)
        {
            Guid tempStoreGuid;
            string couponInfo = string.Empty;
            string couponLink = string.Empty;
            string memberUniqueId = string.Empty;
            string bookingSystemApiKey = BookingSystemFacade.GetApiKey(BookingSystemServiceName._17Life);

            if (!Guid.TryParse(storeGuid, out tempStoreGuid))
            {
                return Json(new JsonSerializer().Serialize(new { Success = false, Message = "StoreGuid格式有誤", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
            }

            VbsMembership member = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
            bool is17LifeEmployee = member.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount;

            var allowedDealUnits = (new VbsVendorAclMgmtModel(member.AccountId)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();

            List<ReserveCouponInfo> couponInfos = BookingSystemFacade.GetAccessibleReserveCoupon(is17LifeEmployee, allowedDealUnits, couponSequenceFirst, couponSequenceSecound);

            foreach (var coupon in couponInfos)
            {
                CashTrustLog couponTrustLog = _mp.CashTrustLogGetByCouponId(coupon.CouponId, (coupon.DealType == BusinessModel.Ppon) ? OrderClassification.LkSite : OrderClassification.HiDeal);
                memberUniqueId = couponTrustLog.UserId.ToString();

                Guid bid = Guid.Empty;
                if(couponTrustLog.BusinessHourGuid != null)
                {
                    bid = couponTrustLog.BusinessHourGuid.Value;
                }
                ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(bid);

                if (Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore))
                {
                    //通用券商品
                    couponTrustLog.StoreGuid = tempStoreGuid;
                    _mp.CashTrustLogSet(couponTrustLog);
                }

                if (couponTrustLog.StoreGuid == tempStoreGuid)
                {
                    if (couponTrustLog.Status == (int)TrustStatus.Returned || couponTrustLog.Status == (int)TrustStatus.Refunded)
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "憑證已退貨", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
                    }
                    if (couponTrustLog.Status == (int)TrustStatus.Verified)
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "憑證已使用", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
                    }
                    if (couponTrustLog.Status == (int)TrustStatus.ATM)
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "該憑證尚未匯款", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
                    }


                    var reservations = _bp.ViewBookingSystemCouponReservationGetBySequenceNumber(couponSequenceFirst + "-" + couponSequenceSecound);
                    if (reservations != null && reservations.Any(x => !x.IsCancel))
                    {
                        return Json(new JsonSerializer().Serialize(new { Success = false, Message = "憑證已預約", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
                    }

                    if (coupon.DealType == BusinessModel.Ppon)
                    {
                        ViewCouponListMainCollection main = _mp.GetCouponListMainListByOrderID(couponTrustLog.UserId, _op.OrderGet(couponTrustLog.OrderGuid).OrderId);

                        if (main.Any())
                        {
                            couponInfo = (string.IsNullOrEmpty(main.ElementAtOrDefault(0).AppTitle)) ? main.ElementAtOrDefault(0).CouponUsage : main.ElementAtOrDefault(0).AppTitle;
                            couponLink = string.Format("{0}/{1}", _conf.SiteUrl, main.ElementAtOrDefault(0).BusinessHourGuid);
                        }

                    }
                    else
                    {
                        ViewHiDealOrderMemberOrderShowCollection vhomCol = _hp.ViewHiDealOrderMemberOrderShowGetByOrderGuid(couponTrustLog.OrderGuid);
                        if (vhomCol.Any())
                        {
                            couponInfo = vhomCol[0].ProductName;
                            couponLink = string.Format("{0}/piinlife/HiDeal.aspx?regid=2&did={1}", _conf.SiteUrl, vhomCol[0].HiDealId);
                            bookingSystemApiKey = BookingSystemFacade.GetApiKey(BookingSystemServiceName.HiDeal);
                        }

                    }

                    return Json(new JsonSerializer().Serialize(new { Success = true, Message = "", OrderKey = couponTrustLog.OrderGuid.ToString(), CouponInfo = couponInfo, CouponLink = couponLink, MemberKey = memberUniqueId, BookingSystemApiKey = bookingSystemApiKey }));
                }
                else
                {
                    return Json(new JsonSerializer().Serialize(new { Success = false, Message = "該憑證非本分店使用之憑證", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
                }
            }
            return Json(new JsonSerializer().Serialize(new { Success = false, Message = "查無此憑證", OrderKey = "", CouponInfo = "", CouponLink = "", MemberKey = "", BookingSystemApiKey = bookingSystemApiKey }));
        }

        [VbsAuthorize, HttpPost]
        public ActionResult GetFullBookingDate(int advanceReservationMinDay, string storeGuid, int bookingType)
        {
            Guid sGuid;
            if (!Guid.TryParse(storeGuid, out sGuid))
            {
                return Json(new JsonSerializer().Serialize(new { Success = false, Message = "GUID format exception.", Data = string.Empty }));
            }

            BookingSystemStore store = BookingSystemFacade.GetBookingSystemStoreByStoreGuid(sGuid, (BookingType)bookingType);
            if (!store.IsLoaded)
            {
                return Json(new JsonSerializer().Serialize(new { Success = false, Message = "查無此店家", Data = string.Empty }));
            }

            DateTime indexDay = DateTime.Now.AddDays((double)advanceReservationMinDay);
            DateTime maxDate = DateTime.Now.AddDays(15);
            List<string> fullDate = new List<string>();

            while (indexDay <= maxDate)
            {
                int dateId = 0;
                if (!BookingSystemFacade.CheckDayTypeIsOpen(store, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (!BookingSystemFacade.IsOpenReservationServiceByDate((BookingType)bookingType, sGuid.ToString(), indexDay, out dateId))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                else if (BookingSystemFacade.IsFullReservation(sGuid.ToString(), (BookingType)bookingType, indexDay))
                {
                    fullDate.Add(indexDay.ToString("yyyy/MM/dd"));
                }
                indexDay = indexDay.AddDays(1);
            }
            return Json(new JsonSerializer().Serialize(new { Success = true, Message = "Success", Data = string.Join(",", fullDate) }));
        }


        #endregion BookingPhoneCouponReservation

        #region BookingPhoneTravelReservation

        [VbsAuthorize]
        public ActionResult BookingPhoneTravelReservation(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingPhoneTravelReservation");

            var vModel = new BookingPhoneTravelReservationViewModel { StoreGuid = storeGuid.ToGuid() };

            return View(vModel);
        }

        #endregion BookingPhoneTravelReservation

        #region  BookingSystemReservationSearch

        [VbsAuthorize]
        public ActionResult BookingReservationSearch(Guid? storeGuid)
        {
            BookingSystemCommonInfo("BookingReservationSearch");

            var vModel = new BookingReservationSearchViewModel { StoreGuid = storeGuid.ToGuid() };

            return View(vModel);
        }

        [VbsAuthorize, HttpPost]
        public ActionResult BookingReservationSearch(BookingReservationSearchViewModel vModel)
        {
            BookingSystemCommonInfo("BookingReservationSearch");

            DateTime reservationStartTime, reservationEndTime;

            if (!DateTime.TryParse(vModel.ReservationStartDate, out reservationStartTime) || !DateTime.TryParse(vModel.ReservationEndDate, out reservationEndTime))
            {
                ViewBag.Message = "查詢日期格式不正確，請重新設定。";
                return View(vModel);
            }

            var bookingTimeExpiredViewModel = new BookingTimeExpiredViewModel();
            bookingTimeExpiredViewModel.StoreGuid = vModel.StoreGuid;
            bookingTimeExpiredViewModel.ReservationStartDate = reservationStartTime;
            bookingTimeExpiredViewModel.ReservationEndDate = reservationEndTime;
            return RedirectToAction("BookingTimeExpired", "BookingSystem", bookingTimeExpiredViewModel);

        }

        #endregion BookingSystemReservationSearch

        #region  BookingTimeExpired

        [VbsAuthorize]
        public ActionResult BookingTimeExpired(BookingTimeExpiredViewModel vModel, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingTimeExpired");

            var storeReservationDateInfos = new List<StoreReservationDateInfo>();

            #region 過去預約資訊搜尋

            BookingSystemStoreCollection stores = _bp.BookingSystemStoreGetByStoreGuid(vModel.StoreGuid);

            ViewBookingSystemStoreDateCollection storeDateSettings = _bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(vModel.StoreGuid);

            DateTime startDateTime = vModel.ReservationStartDate.Date;
            do
            {
                var bookingDate = new StoreReservationDateInfo();

                bookingDate.BookingDate = startDateTime;
                bookingDate.IsValidBooking = false;


                #region 訂位預約設定

                int couponMaxOfNumber = default(int);
                //1.先抓  DayType.AssignDay
                var couponAssignDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)DayType.AssignDay && x.EffectiveDate == startDateTime);
                if (couponAssignDateSettings.Any())
                {
                    var assignSetting = couponAssignDateSettings.First();
                    bookingDate.BookingSystemDateId = assignSetting.BookingSystemDateId;
                    bookingDate.IsOpenBooking = assignSetting.IsOpening;

                    ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(assignSetting.BookingSystemStoreBookingId);

                    couponMaxOfNumber = 0;
                    if (cols != null && cols.Any(x => x.DayType == (int)DayType.AssignDay && x.EffectiveDate == startDateTime))
                    {
                        couponMaxOfNumber = (cols.Where(x => x.DayType == (int)DayType.AssignDay && x.EffectiveDate == startDateTime)).GroupBy(x => x.EffectiveDate).Select(x => new { couponMaxOfNumber = x.Sum(p => p.MaxNumberOfPeople ?? 0) }).First().couponMaxOfNumber;
                    }
                }
                else
                {
                    var couponWeekDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)BookingSystemFacade.GetBookingDayType(startDateTime));
                    if (couponWeekDateSettings.Any())
                    {
                        //2.抓 DayType.DayOfWeek
                        var dayOfWeekSetting = couponWeekDateSettings.First();
                        bookingDate.BookingSystemDateId = dayOfWeekSetting.BookingSystemDateId;
                        bookingDate.IsOpenBooking = dayOfWeekSetting.IsOpening;

                        ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(dayOfWeekSetting.BookingSystemStoreBookingId);
                        couponMaxOfNumber = (cols.Any(x => x.DayType == (int)BookingSystemFacade.GetBookingDayType(startDateTime))) ? cols.Single(x => x.DayType == (int)BookingSystemFacade.GetBookingDayType(startDateTime)).MaxNumberOfPeople ?? 0 : 0;

                    }
                    else
                    {
                        //3.抓 DayType.DefaultSet
                        var bookingDefaultDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Coupon && x.DayType == (int)DayType.DefaultSet);
                        if (bookingDefaultDateSettings.Any())
                        {
                            var defaultSetting = bookingDefaultDateSettings.First();
                            bookingDate.BookingSystemDateId = defaultSetting.BookingSystemDateId;
                            bookingDate.IsOpenBooking = (bool)defaultSetting.GetColumnValue(startDateTime.DayOfWeek.ToString().Substring(0, 3));

                            ViewBookingSystemMaxNumberCollection cols = _bp.ViewBookingSystemMaxNumberListGet(defaultSetting.BookingSystemStoreBookingId);
                            couponMaxOfNumber = ((cols.Where(x => x.DayType == (int)DayType.DefaultSet)).Any()) ? (cols.Where(x => x.DayType == (int)DayType.DefaultSet)).First().MaxNumberOfPeople ?? 0 : 0;
                        }
                    }
                }

                if (stores.Any(x => x.BookingType == (int)BookingType.Coupon))
                {
                    List<ViewBookingSystemStoreReservationCount> storeCouponReservations = _bp.ViewBookingSystemReservationCountGet((stores.Where(x => x.BookingType == (int)BookingType.Coupon)).First(), startDateTime, startDateTime.AddDays(1).AddSeconds(-1)).ToList();
                    bookingDate.CouponBookingCount = (storeCouponReservations.Any()) ? (storeCouponReservations.First().NumberOfPeople ?? default(int)) : 0;
                    bookingDate.IsCouponBookingOver = bookingDate.CouponBookingCount >= couponMaxOfNumber;
                    var reservationCouponList = _bp.ViewBookingSystemReservationListGet((stores.Where(x => x.BookingType == (int)BookingType.Coupon)).First(), startDateTime);
                    bookingDate.IsCancelCouponReservation = reservationCouponList.Any(x => x.IsCancel);
                }

                #endregion 訂位預約設定

                #region 訂房預約設定
                int travelMaxOfNumber = default(int);
                //1.先抓  DayType.AssignDay
                var bookingAssignDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Travel && x.DayType == (int)DayType.AssignDay && x.EffectiveDate == startDateTime);
                if (bookingAssignDateSettings.Any())
                {
                    var assignSetting = bookingAssignDateSettings.First();
                    bookingDate.BookingSystemDateId = assignSetting.BookingSystemDateId;
                    bookingDate.IsOpenBooking = bookingDate.IsOpenBooking | assignSetting.IsOpening;

                    BookingSystemTimeSlotCollection cols = _bp.BookingSystemTimeSlotGet(assignSetting.BookingSystemDateId);
                    travelMaxOfNumber = ((cols.Where(x => x.TimeSlot == "00:00")).Any()) ? (cols.Where(x => x.TimeSlot == "00:00")).First().MaxNumberOfPeople : 0;
                }
                else
                {
                    //2.抓 DayType.DefaultSet
                    var bookingDefaultDateSettings = storeDateSettings.Where(x => x.BookingType == (int)BookingType.Travel && x.DayType == (int)DayType.DefaultSet);
                    if (bookingDefaultDateSettings.Any())
                    {
                        var defaultSetting = bookingDefaultDateSettings.First();
                        bookingDate.BookingSystemDateId = defaultSetting.BookingSystemDateId;
                        bookingDate.IsOpenBooking = bookingDate.IsOpenBooking | (bool)defaultSetting.GetColumnValue(startDateTime.DayOfWeek.ToString().Substring(0, 3));

                        BookingSystemDate travelDate = _bp.BookingSystemDateGet(stores.Where(x => x.BookingType == (int)BookingType.Travel).Select(x => x.BookingId).First(), BookingSystemFacade.GetBookingDayType(startDateTime), null);
                        if (travelDate.IsLoaded)
                        {
                            BookingSystemTimeSlotCollection cols = _bp.BookingSystemTimeSlotGet(travelDate.Id);
                            travelMaxOfNumber = ((cols.Where(x => x.TimeSlot == "00:00")).Any()) ? (cols.Where(x => x.TimeSlot == "00:00")).First().MaxNumberOfPeople : 0;
                        }
                    }

                }

                if (stores.Where(x => x.BookingType == (int)BookingType.Travel).Any())
                {
                    List<ViewBookingSystemStoreReservationCount> storeTravelReservations = _bp.ViewBookingSystemReservationCountGet(stores.Where(x => x.BookingType == (int)BookingType.Travel).First(), startDateTime, startDateTime.AddDays(1).AddSeconds(-1)).ToList();
                    bookingDate.TravelBookingCount = (storeTravelReservations.Any()) ? (storeTravelReservations.First().NumberOfPeople ?? default(int)) : 0;
                    bookingDate.IsTravelBookingOver = bookingDate.TravelBookingCount >= travelMaxOfNumber;

                    var reservationTravelList = _bp.ViewBookingSystemReservationListGet((stores.Where(x => x.BookingType == (int)BookingType.Travel)).First(), startDateTime);
                    bookingDate.IsCancelCouponReservation = reservationTravelList.Any(x => x.IsCancel);
                }

                #endregion 訂房預約設定


                if (bookingDate.CouponBookingCount + bookingDate.TravelBookingCount > 0)
                {
                    storeReservationDateInfos.Add(bookingDate);
                }
                startDateTime = startDateTime.AddDays(1).Date;
            } while (startDateTime <= vModel.ReservationEndDate.Date);

            #endregion 過去預約資訊搜尋

            int pageSize = RESERVATION_CALENDAR_PAGE_NUMBER;
            pageNumber = pageNumber ?? 1;
            int pageIndex = pageNumber.Value - 1;

            var query = (from t in storeReservationDateInfos select t);
            var paging = new PagerList<StoreReservationDateInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
            pageIndex,
            query.ToList().Count);

            vModel.StoreReservationDateInfos = paging;

            return View(vModel);
        }

        #endregion BookingTimeExpired

        #region BookingReservationExpired

        [VbsAuthorize]
        public ActionResult BookingReservationExpired(BookingReservationExpiredViewModel vModel, int? pageNumber)
        {
            BookingSystemCommonInfo("BookingReservationExpired");

            var reservations = _bp.ViewBookingSystemReservationListGet(vModel.StoreGuid, vModel.ContactName, vModel.ContactNumber, vModel.CouponSequence);
            var reservationInfos = new List<ReservationInfoModel>();

            foreach (var item in reservations)
            {
                var reservationInfoModel = new ReservationInfoModel();
                reservationInfoModel.BookingSystemReservationId = item.BookingSystemReservationId;
                reservationInfoModel.BookingSystemStoreBookingId = item.BookingSystemStoreBookingId;
                reservationInfoModel.ReservationDate = item.ReservationDate;
                reservationInfoModel.NumberOfPeople = item.NumberOfPeople;
                reservationInfoModel.Remark = item.Remark;
                reservationInfoModel.ContactName = item.ContactName;
                reservationInfoModel.ContactGender = item.ContactGender;
                reservationInfoModel.ContactNumber = item.ContactNumber;
                reservationInfoModel.IsCheckin = item.IsCheckin;
                reservationInfoModel.IsCancel = item.IsCancel;
                reservationInfoModel.IsLock = item.IsLock;
                reservationInfoModel.IsReturn = item.IsReturn;
                reservationInfoModel.IsPartialCancel = item.IsPartialCancel;
                reservationInfoModel.CreateDatetime = item.CreateDatetime;
                reservationInfoModel.TimeSlot = item.TimeSlot;
                reservationInfoModel.CouponInfo = item.CouponInfo;
                reservationInfoModel.CouponLink = item.CouponLink;
                reservationInfoModel.CouponSequenceNumbers = GetReservationCouponSeqenceNumberList(item.BookingSystemReservationId);
                reservationInfos.Add(reservationInfoModel);
            }

            int pageSize = DAILY_RESERVATION_RECORD_PAGE_NUMBER;
            pageNumber = pageNumber ?? 1;
            int pageIndex = pageNumber.Value - 1;

            var query = (from t in reservationInfos select t);
            var paging = new PagerList<ReservationInfoModel>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
            pageIndex,
            query.ToList().Count);

            vModel.ReservationInfos = paging;

            return View(vModel);
        }


        #endregion BookingReservationExpired

        #endregion Action

        private static bool IsEmptyOrDefaultGuid(Guid? nGuid)
        {
            return (nGuid.ToGuid() == default(Guid));
        }

        private static bool IsStoreDefaultSetting(Guid storeGuid)
        {
            return (_bp.ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(storeGuid).Any());
        }

        private static List<string> GetReservationCouponSeqenceNumberList(int bookingSystemReservationId)
        {
            var couponlist = new List<string>();
            var bookingSystemCoupons = _bp.BookingSystemCouponGetListByReservationId(bookingSystemReservationId);

            if (bookingSystemCoupons != null && bookingSystemCoupons.Any())
            {
                foreach (var bookingSystemCoupon in bookingSystemCoupons)
                {
                    couponlist.Add((string.IsNullOrEmpty(bookingSystemCoupon.Prefix) ? "" : bookingSystemCoupon.Prefix) + bookingSystemCoupon.CouponSequenceNumber);
                }
            }
            return couponlist;
        }

        private bool IsStoreOpenReservationSetting(Guid storeGuid)
        {
            bool result = default(bool);
            if (Is17LifeEmployee())
            {
                result = true;
            }
            else
            {
                var store = _sp.SellerGet(storeGuid);
                result = store.IsOpenReservationSetting;
            }
            return result;
        }

        private bool Is17LifeEmployee()
        {
            return (Session[VbsSession.Is17LifeEmployee.ToString()] != null && VbsCurrent.Is17LifeEmployee);
        }

        private int GetBookingOpeningTime(string time)
        {
            int result;
            return int.TryParse(time, out result) ? result : 0;
        }


        private void BookingSystemCommonInfo(string parentActionName)
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.Title = parentActionName;
            Session[VbsSession.ParentActionName.ToString()] = parentActionName;
        }
    }

    public static class Extension
    {
        public static Guid ToGuid(this Guid? source)
        {
            return source ?? Guid.Empty;
        }
    }
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.WebLib.ActionFilter;
using LunchKingSite.WebLib.Models.Beacon;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;


namespace LunchKingSite.WebLib.Controllers
{
    public class BeaconController : BaseController
    {
        private static IPponProvider _pp;
        private static ISellerProvider _sp;
        private static ISystemProvider _sysp;
        private static ISysConfProvider _config;
        private static ISkmProvider _skm;
        private static IBeaconProvider _bp;
        private static IMemberProvider _mp;
        private static IChannelProvider _cp;
        const string FULLSHOP = "fullshop"; //全館
        const double ELECTRIC_POWER_DEFAULT = 3.0;
        readonly string SKM_DEFAULT_SELLER = _config.SkmDefaultSellerGuid.ToString(); //新光預設店

        static BeaconController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            _cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            _bp = ProviderFactory.Instance().GetProvider<IBeaconProvider>();
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        public ActionResult Logout(string functionName)
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("EventList", "Beacon", new { FunctionName = functionName });
        }

        /// <summary>
        /// 重設密碼
        /// </summary>
        /// <returns></returns>
        public ActionResult SetPassword()
        {
            return Redirect("/User/UserAccount.aspx");
        }

        /// <summary>
        /// 賣家首頁
        /// </summary>
        /// <param name="functionName"></param>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, 17LifeBeaconManager, ME2O, Production")]
        public ActionResult Trigger(string functionName)
        {
            //首頁不應有參數
            if (functionName != null)
            {
                return Redirect("../../Beacon/Trigger");
            }

            return View();
        }

        #region Event

        /// <summary>
        /// 事件列表
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="shopCode"></param>
        /// <param name="page"></param>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult EventList(string sellerGuid, string shopCode, int page = 1, int triggerAppId = 0)
        {
            //下拉選單
            InitDropDownList(sellerGuid, shopCode, string.Empty, triggerAppId);

            ViewBag.Page = page;
            return View();
        }

        /// <summary>
        /// 搜尋事件列表
        /// </summary>
        /// <param name="groupCode"></param>
        /// <param name="orderField"></param>
        /// <param name="isDesc"></param>
        /// <param name="triggerAppId"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBeaconEventListInfo(string groupCode, string orderField, bool isDesc, int triggerAppId, int page = 1)
        {
            bool isRootPower = SkmFacade.CheckSkmHqPower(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
            List<BeaconEventInfo> beaconEventInfoList = new List<BeaconEventInfo>();
            var beaconEventList = _bp.GetBeaconEventList(triggerAppId, groupCode);

            foreach (var beaconEventInfo in beaconEventList)
            {
                beaconEventInfoList.Add(new BeaconEventInfo
                {
                    EventId = beaconEventInfo.EventId,
                    BeginDate = beaconEventInfo.EffectiveDateStart,
                    EndDate = beaconEventInfo.EffectiveDateEnd,
                    Subject = beaconEventInfo.Subject,
                    Kinds = _bp.GetBeaconSubeventIds(beaconEventInfo.EventId).Count, //多檔次
                    State = beaconEventInfo.Enabled,
                    IsRootPower = _bp.GetSkmBeaconPropertyByEventId(beaconEventInfo.EventId).IsRootPower && !isRootPower
                });
            }

            #region 資料篩選

            //排序
            IEnumerable<BeaconEventInfo> tempeventList;
            switch (orderField)
            {
                case "eventBeginDate":
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.BeginDate) : beaconEventInfoList.OrderByDescending(x => x.BeginDate);
                    break;

                case "eventEndDate":
                case "isExpired":
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.EndDate) : beaconEventInfoList.OrderByDescending(x => x.EndDate);
                    break;

                case "eventSubject":
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.Subject) : beaconEventInfoList.OrderByDescending(x => x.Subject);
                    break;

                case "isSubEvent":
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.Kinds) : beaconEventInfoList.OrderByDescending(x => x.Kinds);
                    break;

                case "evnetState":
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.State) : beaconEventInfoList.OrderByDescending(x => x.State);
                    break;

                default:
                    tempeventList = isDesc ? beaconEventInfoList.OrderBy(x => x.EventId) : beaconEventInfoList.OrderByDescending(x => x.EventId);
                    break;

            }

            //分頁
            var partEventList = tempeventList.Skip((page - 1) * 10).Take(10).ToList();

            #endregion

            return Json(new { partEventList, beaconEventInfoList.Count });
        }

        /// <summary>
        /// 事件列表修改狀態
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="state"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditBeaconEventForEnabled(int eventId, bool state)
        {
            var message = string.Empty;
            var isSuccess = true;

            try
            {
                var beaconEvent = _bp.GetBeaconEvent(eventId);
                beaconEvent.Enabled = !state;
                _bp.InsertOrUpdateBeaconEventEntity(beaconEvent);
            }
            catch (Exception ex)
            {
                message = ex.ToString();
                isSuccess = false;
            }

            return Json(new { Success = isSuccess, Message = message });
        }

        /// <summary>
        /// 事件修改頁
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="eventId"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="shopCode"></param>
        /// <param name="isView"></param>
        /// <param name="page"></param>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult EventEdit(string functionName, int? eventId, string sellerGuid, string shopCode, bool? isView, int page = 1, int triggerAppId = 0)
        {
            //下拉選單
            InitDropDownList(sellerGuid, shopCode, string.Empty, triggerAppId);

            //無此權限直接導回EventList頁
            bool isRootPower = SkmFacade.CheckSkmHqPower(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
            if (_bp.GetSkmBeaconPropertyByEventId(eventId ?? 0).IsRootPower)
            {
                if (!isRootPower)
                {
                    return RedirectToAction("EventList", "Beacon", new { FunctionName = functionName });
                }
            }

            BeaconEventListModel model = new BeaconEventListModel();
            ViewBag.isView = isView ?? false;
            ViewBag.TriggerAppId = triggerAppId;
            ViewBag.Groups = _bp.GetBeaconGroupListByAuxiliaryCode(triggerAppId, FULLSHOP).OrderBy(t => t.GroupCode).ToDictionary(x => x.GroupCode.ToString(), x => x.GroupRemark);
            ViewBag.Page = page;
            ViewBag.IsRoot = isRootPower;

            var trigglerApp = _bp.GetBeaconTriggerAppByFunctionName(functionName);
            model.SubEventMaxCount = trigglerApp.SubeventMaxCount;
            model.SubEventMinCount = trigglerApp.SubeventMinCount;

            if (eventId != null)
            {
                var date = _bp.GetBeaconEvent(eventId ?? 0);
                var subDates = _bp.GetBeaconSubeventList(eventId ?? 0);
                var groupDates = _bp.GetBeaconEventGroupByEventId(eventId ?? 0, (int)LinkType.Group);
                var beaconDates = _bp.GetBeaconEventDeviceLinkListByEventId(eventId ?? 0);
                model.EventId = eventId ?? 0;
                model.BeginDate = date.EffectiveDateStart;
                model.EndDate = date.EffectiveDateEnd;
                model.BeginTime = date.EffectiveTimeStart;
                model.EndTime = date.EffectiveTimeEnd;
                model.State = date.Enabled;
                model.EventType = ((EventType)date.EventType).ToString();
                model.LinkInput = GetLinkInputData((EventType)date.EventType, date.ActionBid.ToString(), date.ActionUrl);
                model.Subject = date.Subject;
                model.Content = date.Content;
                List<SubEvent> subEvneList = subDates.Select(subDate => new SubEvent
                {
                    SubEventId = subDate.SubeventId,
                    SubEventType = ((EventType)subDate.EventType).ToString(),
                    SubLinkInput = GetLinkInputData((EventType)subDate.EventType,
                    subDate.ActionBid.ToString(), subDate.ActionUrl),
                    SubSubject = subDate.Subject,
                    SubContent = subDate.Content
                }).ToList();

                List<BeaconGroupModel> beaconGroupModelList = groupDates.Select(groupDate => new BeaconGroupModel
                {
                    GroupCode = groupDate.GroupCode,
                    AuxiliaryCode = groupDate.AuxiliaryCode
                }).ToList();

                List<BeaconDeivceModel> beaconDeivceModelList = beaconDates.Select(beaconDate => new BeaconDeivceModel
                {
                    Major = beaconDate.Major.ToString(),
                    Minor = beaconDate.Minor.ToString()
                }).ToList();

                model.SubEventList = JsonConvert.SerializeObject(subEvneList);
                model.GroupList = JsonConvert.SerializeObject(beaconGroupModelList);
                model.BeaconList = JsonConvert.SerializeObject(beaconDeivceModelList);

                //客群數
                List<int> eventIds = new List<int> { eventId ?? 0 };
                model.UpdateFileCount = _bp.GetBeaconEventTargetCount(eventIds);
            }
            return View("EventEdit" + ((BeaconTriggerAppFunctionName)triggerAppId), model);
        }

        /// <summary>
        /// 修改、新增事件
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="eventId"></param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="status"></param>
        /// <param name="mainEventType"></param>
        /// <param name="mainLinkInput"></param>
        /// <param name="mainSubject"></param>
        /// <param name="mainContent"></param>
        /// <param name="subEventList"></param>
        /// <param name="beaconIdList"></param>
        /// <param name="groupIdList"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult EditBeaconEvent(string functionName, int eventId, string beginDate, string endDate, string beginTime, string endTime, bool status,
            string mainEventType, string mainLinkInput, string mainSubject, string mainContent, string subEventList, string beaconIdList, string groupIdList)
        {
            int eventIdflag = 0;
            DateTime nowTime = DateTime.Now;

            try
            {
                BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);
                BeaconEvent beaconEvent = _bp.GetBeaconEvent(eventId);

                #region Save BeaconEvent
                Guid actionBid;
                string actionUrl;

                beaconEvent.TriggerAppId = bta.TriggerAppId;
                beaconEvent.Subject = mainSubject;
                beaconEvent.Content = mainContent;
                beaconEvent.EventType = (byte)(int)Enum.Parse(typeof(EventType), mainEventType);
                GetEventTypeData((EventType)Enum.Parse(typeof(EventType), mainEventType), mainLinkInput, out actionBid, out actionUrl);
                beaconEvent.ActionBid = actionBid;
                beaconEvent.ActionUrl = actionUrl;
                beaconEvent.EffectiveDateStart = Convert.ToDateTime(beginDate);
                beaconEvent.EffectiveDateEnd = Convert.ToDateTime(endDate);
                beaconEvent.EffectiveTimeStart = beginTime;
                beaconEvent.EffectiveTimeEnd = endTime;
                beaconEvent.Enabled = status;
                beaconEvent.CreateDate = nowTime;
                beaconEvent.CreateId = Convert.ToInt32(User.Identity.GetPropertyValue("Id"));
                _bp.InsertOrUpdateBeaconEventEntity(beaconEvent);
                eventIdflag = beaconEvent.EventId;
                #endregion

                #region Save and Delete beaconSubevent 
                //List<int> beaconSubeventIds = _bp.GetBeaconSubeventIds(beaconEvent.EventId);

                if ((EventType)(beaconEvent.EventType) == EventType.SubEvent)
                {
                    List<SubEvent> subEventDates = JsonConvert.DeserializeObject<List<SubEvent>>(subEventList);

                    foreach (var subEventDate in subEventDates)
                    {
                        BeaconSubevent beaconSubevent = new BeaconSubevent();

                        beaconSubevent.SubeventId = subEventDate.SubEventId;
                        beaconSubevent.EventId = beaconEvent.EventId;
                        beaconSubevent.Subject = subEventDate.SubSubject;
                        beaconSubevent.Content = subEventDate.SubContent;
                        beaconSubevent.EventType = (byte)(int)Enum.Parse(typeof(EventType), subEventDate.SubEventType);
                        GetEventTypeData((EventType)Enum.Parse(typeof(EventType), subEventDate.SubEventType), subEventDate.SubLinkInput, out actionBid, out actionUrl);
                        beaconSubevent.ActionBid = actionBid;
                        beaconSubevent.ActionUrl = actionUrl;

                        _bp.InsertOrUpdateBeaconSubeventEntity(beaconSubevent);

                        //beaconSubeventIds.Remove(beaconSubevent.SubeventId);  //挑出要刪除的Id
                    }
                }
                //_bp.DeleteBeaconSubeventEntity(beaconSubeventIds);  //刪除
                #endregion

                # region Delete and Save beaconEventLink
                _bp.DeleteBeaconEventGroupLinkByEventId(beaconEvent.EventId);
                List<BeaconGroupModel> groupArray = JsonConvert.DeserializeObject<List<BeaconGroupModel>>(groupIdList);
                List<BeaconDeivceModel> beaconArray = JsonConvert.DeserializeObject<List<BeaconDeivceModel>>(beaconIdList);
                List<string> groupCodeArray = new List<string>();
                List<BeaconGroupModel> beaconGroupModelArray = new List<BeaconGroupModel>();
                var allGroup = _bp.GetBeaconGroupList(bta.TriggerAppId);

                //先塞選出 全館/全樓層 
                foreach (var group in groupArray.Distinct())
                {
                    var groupCode = group.GroupCode;
                    var auxiliaryCode = group.AuxiliaryCode;
                    if (auxiliaryCode == FULLSHOP)
                    {
                        groupCodeArray.Add(groupCode);
                    }
                    else
                    {
                        BeaconGroupModel beaconGroupModel = new BeaconGroupModel();
                        beaconGroupModel.GroupCode = groupCode;
                        beaconGroupModel.AuxiliaryCode = auxiliaryCode;
                        beaconGroupModelArray.Add(beaconGroupModel);
                    }
                }

                //全群組
                List<BeaconGroupModel> beaconGroupListHasId = new List<BeaconGroupModel>();

                foreach (var group in groupArray.Distinct().ToList())
                {
                    var firstOrDefault = allGroup.FirstOrDefault(x => x.GroupCode == group.GroupCode && x.AuxiliaryCode == group.AuxiliaryCode);
                    if (firstOrDefault != null)
                    {
                        var groupId = firstOrDefault.GroupId;
                        beaconGroupListHasId.Add(new BeaconGroupModel
                        {
                            GroupId = groupId,
                            GroupCode = group.GroupCode,
                            AuxiliaryCode = group.AuxiliaryCode
                        });
                    }
                }

                foreach (var beaconGroup in beaconGroupListHasId)
                {
                    if (beaconGroup.AuxiliaryCode == FULLSHOP || !groupCodeArray.Contains(beaconGroup.GroupCode))
                    {
                        BeaconEventGroupLink beaconEventGrouplink = new BeaconEventGroupLink
                        {
                            EventId = beaconEvent.EventId,
                            GroupId = beaconGroup.GroupId,
                            LinkType = (int)LinkType.Group
                        };

                        _bp.InsertOUpdateBeaconEventGroupLink(beaconEventGrouplink);
                    }
                }

                //群組內 && 單獨
                _bp.DeleteBeaconEventDeviceLinkByEventId(beaconEvent.EventId);

                List<BeaconDeivceModel> beaconDeivceListHasId = new List<BeaconDeivceModel>();
                var beaconGroupIdDeivceIds = _bp.GetBeaconGroupIdList(bta.TriggerAppId);
                var beaconDeviceList = _bp.GetBeaconDevice();

                foreach (var beaconDeivceModel in beaconArray.Distinct().ToList())
                {
                    var firstOrDefault = beaconDeviceList.FirstOrDefault(x => x.Major == int.Parse(beaconDeivceModel.Major) && x.Minor == int.Parse(beaconDeivceModel.Minor));
                    if (firstOrDefault != null)
                    {
                        var deviceId = firstOrDefault.DeviceId;
                        beaconDeivceListHasId.Add(new BeaconDeivceModel { DeviceId = deviceId, Major = beaconDeivceModel.Major, Minor = beaconDeivceModel.Minor });
                    }
                }

                foreach (var beacon in beaconDeivceListHasId)
                {
                    //依major,minor篩選出非全館的Group
                    var groupIds = beaconGroupIdDeivceIds.Where(x => x.DeviceId == beacon.DeviceId).Select(x => x.GroupId).ToList();
                    var group = allGroup.FirstOrDefault(x => groupIds.Contains(x.GroupId) && x.AuxiliaryCode != FULLSHOP);

                    //檢查全館or全樓層是否已勾選(有群組)
                    bool isGroupInclud = false;
                    if (group != null)
                    {
                        //檢查全館
                        if (groupCodeArray.Any(groupCode => group.GroupCode == groupCode))
                        {
                            isGroupInclud = true;
                        }

                        //檢查樓層
                        if (beaconGroupModelArray.Any(
                            beaconGroupModel => group.GroupCode == beaconGroupModel.GroupCode &&
                                group.AuxiliaryCode == beaconGroupModel.AuxiliaryCode))
                        {
                            isGroupInclud = true;
                        }

                        //有群組
                        if (!isGroupInclud)
                        {
                            BeaconEventGroupLink begl = new BeaconEventGroupLink();
                            BeaconEventDeviceLink bedl = new BeaconEventDeviceLink();
                            begl.EventId = beaconEvent.EventId;
                            begl.GroupId = group.GroupId;
                            begl.LinkType = (int)LinkType.GroupInCustom;
                            _bp.InsertOUpdateBeaconEventGroupLink(begl);

                            bedl.EventId = beaconEvent.EventId;
                            bedl.DeviceId = beacon.DeviceId;
                            bedl.EventGroupLinkId = begl.Id;
                            _bp.InsertOUpdateBeaconEventDeviceLink(bedl);
                        }
                    }

                    //單獨(無群組)
                    if (groupIds.Count == 0)
                    {
                        BeaconEventDeviceLink bedl = new BeaconEventDeviceLink
                        {
                            EventId = beaconEvent.EventId,
                            DeviceId = beacon.DeviceId,
                            EventGroupLinkId = 0
                        };
                        _bp.InsertOUpdateBeaconEventDeviceLink(bedl);
                    }
                }
                #endregion

                # region Delete and Save BeaconEventTimeSlot
                List<BeaconEventTimeSlot> oldBeaconEventTimeSlots = _bp.GetEventTimeSlotByToDay(eventId);
                _bp.DeleteEventTimeSlotByEventId(beaconEvent.EventId);

                //取得Beacon對應的group
                var bgs = _bp.GetBeaconGroupListByEventId(beaconEvent.EventId);
                foreach (var bg in bgs.Distinct())
                {
                    //判斷是否為全館

                    if (bg.AuxiliaryCode == FULLSHOP)
                    {
                        var bgByGroupCodes = _bp.GetBeaconGroupListNotContainAuxiliaryCode(bta.TriggerAppId, bg.GroupCode, FULLSHOP);
                        foreach (var bgByGroupCode in bgByGroupCodes)
                        {
                            //在時間區間內每一天建一筆資資料
                            DateTime startDate = Convert.ToDateTime(beginDate);
                            DateTime stopDate = Convert.ToDateTime(endDate);
                            while (DateTime.Compare(startDate, stopDate) < 0)
                            {
                                BeaconEventTimeSlot beaconEventTimeSlot = new BeaconEventTimeSlot();
                                beaconEventTimeSlot.EventId = beaconEvent.EventId;
                                beaconEventTimeSlot.EffectiveDate = startDate;
                                beaconEventTimeSlot.CreateTime = nowTime;
                                beaconEventTimeSlot.GroupId = bg.GroupId;
                                beaconEventTimeSlot.Floor = bgByGroupCode.AuxiliaryCode;
                                beaconEventTimeSlot.Sequence = 99999;
                                beaconEventTimeSlot.Status = 1;

                                //檢查是否已有舊資料，有則回填排序跟狀態
                                foreach (var oldBeaconEventTimeSlot in oldBeaconEventTimeSlots)
                                {
                                    if (oldBeaconEventTimeSlot.EffectiveDate == startDate &&
                                        oldBeaconEventTimeSlot.EventId == beaconEvent.EventId &&
                                        oldBeaconEventTimeSlot.GroupId == bg.GroupId &&
                                        oldBeaconEventTimeSlot.Floor == bgByGroupCode.AuxiliaryCode)
                                    {
                                        beaconEventTimeSlot.Sequence = oldBeaconEventTimeSlot.Sequence;
                                        beaconEventTimeSlot.Status = oldBeaconEventTimeSlot.Status;
                                    }
                                }

                                _bp.InsertOrUpdateEventTimeSlot(beaconEventTimeSlot);
                                startDate = startDate.AddDays(1);
                            }
                        }
                    }
                    else //非全館只需寫入該層
                    {

                        //在時間區間內每一天建一筆資資料
                        DateTime startDate = Convert.ToDateTime(beginDate);
                        DateTime stopDate = Convert.ToDateTime(endDate);
                        while (DateTime.Compare(startDate, stopDate) < 0)
                        {
                            BeaconEventTimeSlot beaconEventTimeSlot = new BeaconEventTimeSlot
                            {
                                EventId = beaconEvent.EventId,
                                EffectiveDate = startDate,
                                CreateTime = nowTime,
                                GroupId = bg.GroupId,
                                Floor = bg.AuxiliaryCode,
                                Sequence = 99999,
                                Status = 1
                            };

                            //檢查是否已有舊資料，有則回填排序跟狀態
                            foreach (var oldBeaconEventTimeSlot in oldBeaconEventTimeSlots)
                            {
                                if (oldBeaconEventTimeSlot.EffectiveDate == startDate &&
                                    oldBeaconEventTimeSlot.EventId == beaconEvent.EventId &&
                                    oldBeaconEventTimeSlot.GroupId == bg.GroupId &&
                                    oldBeaconEventTimeSlot.Floor == bg.AuxiliaryCode)
                                {
                                    beaconEventTimeSlot.Sequence = oldBeaconEventTimeSlot.Sequence;
                                    beaconEventTimeSlot.Status = oldBeaconEventTimeSlot.Status;
                                }
                            }

                            _bp.InsertOrUpdateEventTimeSlot(beaconEventTimeSlot);
                            startDate = startDate.AddDays(1);
                        }
                    }
                }

                #endregion

                #region Save BeaconProperty
                if (functionName == BeaconTriggerAppFunctionName.skm.ToString())
                {
                    SkmBeaconProperty skmBeaconProperty = _bp.GetSkmBeaconPropertyByEventId(beaconEvent.EventId);

                    skmBeaconProperty.EventId = beaconEvent.EventId;
                    if (eventId == 0)  //初次建立時，才修改
                    {
                        skmBeaconProperty.IsRootPower = SkmFacade.CheckSkmHqPower(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
                    }

                    _bp.InsertOrUpdateSkmBeaconProperty(skmBeaconProperty);
                }
                #endregion

            }
            catch
            {
                // ignored
            }
            return Json(eventIdflag);
        }

        #endregion

        #region Power

        /// <summary>
        /// 電量列表顯示
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="ddlFloor"></param>
        /// <param name="triggerAppId"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult PowerList(string functionName, string sellerGuid, string shopCode, string ddlFloor, int triggerAppId = 0)
        {
            //下拉選單
            InitDropDownList(sellerGuid, shopCode, ddlFloor, triggerAppId);

            ViewBag.ElectricLevels = GetElectricLevels();
            ViewBag.FunctionName = functionName;
            return View();
        }

        /// <summary>
        /// 電量圖片顯示
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="shopCode"></param>
        /// <param name="ddlFloor"></param>
        /// <param name="functionName"></param>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult PowerMaps(string functionName, string sellerGuid, string shopCode, string ddlFloor, int triggerAppId = 0)
        {
            InitDropDownList(sellerGuid, shopCode, ddlFloor, triggerAppId);

            ViewBag.FunctionName = functionName;
            return View();
        }

        /// <summary>
        /// 電量通知
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult PowerNotification()
        {
            Guid sellerGuid = Guid.Parse(Request.Form["sellerGuid"]);
            var shopCodeList = _skm.SkmShoppeGetShopCodeBySellerGuid(sellerGuid);
            string systemDataName = string.Format("{0}{1}", SkmFacade.BeaconPowerNoticeSystemData, shopCodeList.First());
            SystemData systemData = _sysp.SystemDataGet(systemDataName);
            var setting = JsonConvert.DeserializeObject<Dictionary<string, string>>(systemData.Data);

            if (!systemData.IsLoaded)
            {
                InitPowerNotificationSetting(systemDataName, systemData);
            }
            else
            {
                ViewBag.BeaconPowerLevel = setting["beaconPowerLevel"];
                ViewBag.NotificationEmails = setting["notificationEmails"];
                ViewBag.EletricPowerNotUpdateWaitDay = setting["eletricPowerNotUpdateWaitDay"];
            }

            ViewBag.ShopCode = string.Join(",", shopCodeList);
            ViewBag.ElectricLevels = GetElectricLevels();                        

            return View();
        }

        /// <summary>
        /// 電量通知設定
        /// </summary>
        /// <param name="powerLevel"></param>
        /// <param name="emails"></param>
        /// <param name="days"></param>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PowerNotificationSetting(int powerLevel, string emails, int days, string shopCode)
        {
            bool updateflag = true;
            foreach (var sc in shopCode.Split(','))
            {
                JsonSerializer json = new JsonSerializer();
                string systemDataName = string.Format("{0}{1}", SkmFacade.BeaconPowerNoticeSystemData, sc);
                SystemData systemData = _sysp.SystemDataGet(systemDataName);
                if (!systemData.IsLoaded)
                {
                    InitPowerNotificationSetting(systemDataName, systemData);
                }
                systemData.ModifyId = this.HttpContext.User.Identity.Name;
                systemData.ModifyTime = DateTime.Now;

                try
                {
                    var setting = json.Deserialize<Dictionary<string, string>>(systemData.Data);
                    setting["beaconPowerLevel"] = powerLevel.ToString();
                    setting["notificationEmails"] = emails;
                    setting["eletricPowerNotUpdateWaitDay"] = days.ToString();
                    systemData.Data = json.Serialize(setting);

                    _sysp.SystemDataSet(systemData);
                }
                catch
                {
                    updateflag = false;
                }
            }
            return Json(updateflag);
        }

        #endregion

        #region Schedule
        /// <summary>
        /// 排程管理
        /// </summary>
        /// <param name="seller">僅只有skm有</param>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <param name="beginDate"></param>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult Schedule(string seller, string groupCode, string auxiliaryCode, string beginDate, int triggerAppId = 0)
        {
            InitDropDownList(seller, groupCode, auxiliaryCode, triggerAppId);

            //預設為當週檔次時程
            DateTime dateTime = DateTime.Now;
            if (beginDate != null)
            {
                DateTime.TryParse(beginDate, out dateTime);
            }

            bool isRootPower = SkmFacade.CheckSkmHqPower(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
            var shopGroup = _bp.GetBeaconGroupList(triggerAppId, groupCode);
            var fullshopGroup = shopGroup.FirstOrDefault(x => x.AuxiliaryCode == FULLSHOP);
            var floorgroup = shopGroup.FirstOrDefault(x => x.AuxiliaryCode == auxiliaryCode);
            DateTime newBeginDate = DateTime.Parse(dateTime.AddDays(1 - (int)dateTime.DayOfWeek).ToString("yyyy/MM/dd"));
            List<SkmDealSort> dealSort = new List<SkmDealSort>();
            List<ViewBeaconEventTimeSlot> smcs = new List<ViewBeaconEventTimeSlot>();
            var fullshopGroupId = 0;
            var floorgroupGroupId = 0;

            if (fullshopGroup != null)
            {
                fullshopGroupId = fullshopGroup.GroupId;
            }

            if (floorgroup != null)
            {
                floorgroupGroupId = floorgroup.GroupId;
            }

            smcs = _bp.GetEventTimeSlotByToDay(dateTime.AddDays(0), dateTime.AddDays(7), fullshopGroupId, floorgroupGroupId, auxiliaryCode).ToList();

            for (var w = 0; w < 7; w++)
            {
                List<SeqDeals> skmDeals = new List<SeqDeals>();
                if (smcs.Count() != 0)
                {
                    foreach (ViewBeaconEventTimeSlot smc in smcs.Where(p =>
                        p.EffectiveDate.Date == dateTime.AddDays(w).Date ||
                        p.EffectiveDate.Date == dateTime.AddDays(w).Date))
                    {
                        skmDeals.Add(new SeqDeals
                        {
                            timeSlotId = smc.TimeSlotId,
                            ItemName = smc.Subject,
                            DealDate = dateTime.AddDays(w).ToString("yyyy/MM/dd HH:mm:ss"),
                            Sequence = smc.Sequence,
                            Status = smc.Enabled ? 1 : 0,
                            Floor = smc.Floor,
                            IsLcock = _bp.GetSkmBeaconPropertyByEventId(smc.EventId).IsRootPower && !isRootPower
                        });
                    }
                }

                dealSort.Add(new SkmDealSort
                {
                    DealDate = dateTime.AddDays(w),
                    Deals = skmDeals,
                });
            }

            ViewBag.DealSort = dealSort;
            ViewData["BeginDate"] = newBeginDate.ToString("yyyy/MM/dd");
            return View();
        }

        /// <summary>
        /// 儲存排程列表
        /// </summary>
        /// <param name="data"></param>
        /// <param name="isDeal"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult SaveSoftSchedule(string data, bool isDeal)
        {
            bool isSuccess = true;
            string message = string.Empty;

            try
            {
                List<SeqDeals> sdsus = new JsonSerializer().Deserialize<List<SeqDeals>>(data);
                foreach (SeqDeals sdsu in sdsus)
                {
                    var timeSlot = _bp.GetEventTimeSlotBytimeSlotId(sdsu.timeSlotId);
                        
                    if (timeSlot != null)
                    {
                        timeSlot.Sequence = sdsu.Sequence;
                        _bp.InsertOrUpdateEventTimeSlot(timeSlot);
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                message = ex.Message;
            }

            return Json(new { Success = isSuccess, Message = message });
        }

        #endregion

        #region GroupManage

        /// <summary>
        /// 群組管理
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="name"></param>
        /// <param name="memberRoleCollection"></param>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult GroupManage(string functionName, string name, RoleCollection memberRoleCollection, int triggerAppId = 0)
        {
            InitDropDownList(null, null, null, triggerAppId);
            string[] beaconTriggerAppFunctionNames = Enum.GetNames(typeof(BeaconTriggerAppFunctionName));
            const string life17BeaconManager = "17LifeBeaconManager"; //17lifeBeacon管理者

            ViewBag.Store = beaconTriggerAppFunctionNames.ToDictionary(beaconTriggerAppFunctionName => (int)Enum.Parse(typeof(BeaconTriggerAppFunctionName), beaconTriggerAppFunctionName), beaconTriggerAppFunctionName => Helper.GetEnumDescription((BeaconTriggerAppFunctionName)Enum.Parse(typeof(BeaconTriggerAppFunctionName), beaconTriggerAppFunctionName)));

            switch (triggerAppId)
            {
                case (int)BeaconTriggerAppFunctionName.skm:
                    ViewBag.GroupManageBeaconAdd = false;
                    ViewBag.GroupManageGroupAdd = false;
                    ViewBag.GroupManageChange = memberRoleCollection.Any(p => p.RoleName == life17BeaconManager);
                    ViewBag.GroupManageMaps = true;
                    return View("GroupManage" + (name ?? "Maps"));
                case (int)BeaconTriggerAppFunctionName.life17:
                    ViewBag.GroupManageBeaconAdd = true;
                    ViewBag.GroupManageGroupAdd = true;
                    ViewBag.GroupManageChange = true;
                    ViewBag.GroupManageMaps = true;
                    return View("GroupManage" + (name ?? "BeaconAdd"));
                default:
                    ViewBag.GroupManageBeaconAdd = false;
                    ViewBag.GroupManageGroupAdd = false;
                    ViewBag.GroupManageChange = false;
                    ViewBag.GroupManageMaps = true;
                    return View("GroupManage" + (name ?? "Maps"));
            }
        }

        /// <summary>
        /// 取得Beacon群組列資訊
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="page"></param>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <param name="groupName"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult GetBeaconGroupListInfo(int triggerAppId, int page, string groupCode, string auxiliaryCode, string groupName)
        {
            List<BeaconGroup> beaconGroupInfoList = new List<BeaconGroup>();

            var beaconGroup = _bp.GetBeaconGroupList(triggerAppId);
            beaconGroupInfoList.AddRange(beaconGroup.Where(x => x.AuxiliaryCode != FULLSHOP));

            #region 資料篩選
            if (!string.IsNullOrEmpty(groupCode))
            {
                beaconGroupInfoList = beaconGroupInfoList.Where(x => x.GroupCode == groupCode).ToList();
            }

            if (!string.IsNullOrEmpty(auxiliaryCode))
            {
                beaconGroupInfoList = beaconGroupInfoList.Where(x => x.AuxiliaryCode == auxiliaryCode).ToList();
            }

            if (!string.IsNullOrEmpty(groupName))
            {
                beaconGroupInfoList = beaconGroupInfoList.Where(x => x.GroupRemark.Contains(groupName)).ToList();
            }
            #endregion

            //分頁
            var partBeaconGroupInfoList = beaconGroupInfoList.Skip((page - 1) * 10).Take(10).ToList();

            return Json(new { data = partBeaconGroupInfoList, count = beaconGroupInfoList.Count });
        }

        /// <summary>
        /// 編輯Beacon群組
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupId"></param>
        /// <param name="groupRemark"></param>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult EditBeaconGroupListInfo(int triggerAppId, int groupId, string groupRemark, string groupCode, string auxiliaryCode)
        {
            BeaconGroup beaconGroup = new BeaconGroup
            {
                TriggerAppId = triggerAppId,
                GroupId = groupId,
                GroupRemark = groupRemark,
                GroupCode = groupCode,
                AuxiliaryCode = auxiliaryCode
            };

            //檢查是否已有重複Code
            var allGroup = _bp.GetBeaconGroupList(triggerAppId);
            if (allGroup.Any(x => x.GroupCode == groupCode && x.AuxiliaryCode == auxiliaryCode && x.GroupId != groupId))
            {
                return Json(-1);
            }

            //檢查是否已有重複名稱
            if (allGroup.Any(x => x.GroupCode == groupCode && x.GroupRemark != groupRemark))
            {
                return Json(-2);
            }   

            List<BeaconGroup> beaconGroups = new List<BeaconGroup> { beaconGroup };
            bool isSuccess = _bp.InsertOrUpdateBeaconGroupList(beaconGroups);

            //檢查群組數量兩個以上時,追加FULLSHOP
            var beaconGroupCount = _bp.GetBeaconGroupList(triggerAppId, groupCode).Count;
            if (beaconGroupCount == 2)
            {
                BeaconGroup beaconGroupFullShop = new BeaconGroup
                {
                    TriggerAppId = triggerAppId,
                    GroupRemark = groupRemark,
                    GroupCode = groupCode,
                    AuxiliaryCode = FULLSHOP
                };
                List<BeaconGroup> beaconGroupFullShops = new List<BeaconGroup> { beaconGroupFullShop };
                isSuccess = isSuccess && _bp.InsertOrUpdateBeaconGroupList(beaconGroupFullShops);
            }

            return Json(isSuccess ? beaconGroup.GroupId : 0);
        }

        public struct FileOfBeaconGroupRow
        {
            public const int GroupName = 0;
            public const int GroupCode = 1;
            public const int AuxiliaryCode = 2;
            public const int FinishRow = 3;
        }

        /// <summary>
        /// 批次匯入Beacon群組
        /// <param name="triggerAppId"></param>
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult EditBeaconGroupListInfoByFile(int triggerAppId)
        {
            try
            {
                List<BeaconGroup> beaconGroupList = new List<BeaconGroup>();
                bool isRepeat = false;
                const string repeatMessage = "群組代碼已存在或群組代碼相同，但群組名稱不同的資料將自動忽略";

                if (Request.Files.Count == 0)
                    return Json(new { Success = false, Message = "未選擇任何檔案。" });

                //支援多檔上傳
                for (var i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    //檢查附檔名
                    if (Path.GetExtension(file.FileName) != ".xls")
                        return Json(new { Success = false, Message = "請確認檔案格式是否正確。" });

                    //選擇工作表
                    HSSFWorkbook workbook = new HSSFWorkbook(file.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);

                    //從第二列開始讀取Beacon資料
                    var allGroup = _bp.GetBeaconGroupList(triggerAppId);
                    for (var j = 1; j <= currentSheet.LastRowNum; j++)
                    {
                        Row row = currentSheet.GetRow(j);

                        //GroupCode與AuxiliaryCoded重複 或 GroupCode空白 則跳過
                        if (string.IsNullOrEmpty(CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupCode)))) continue;
                        if (allGroup.Any(x => x.GroupCode == CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupCode)) && x.AuxiliaryCode == CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.AuxiliaryCode))))
                        {
                            isRepeat = true;
                            continue;
                        }

                        //檢查是否已有重複名稱
                        if (allGroup.Any(x => x.GroupCode == CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupCode)) && x.GroupRemark != CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupName))))
                        {
                            isRepeat = true;
                            continue;
                        }

                        BeaconGroup beaconGroup = new BeaconGroup();
                        int cnt = 0;
                        while (cnt < FileOfBeaconGroupRow.FinishRow)
                        {
                            switch (cnt % FileOfBeaconGroupRow.FinishRow)
                            {
                                case FileOfBeaconGroupRow.GroupName:
                                    beaconGroup.GroupRemark = CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupName));
                                    break;
                                case FileOfBeaconGroupRow.GroupCode:
                                    beaconGroup.GroupCode = CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.GroupCode));
                                    break;
                                case FileOfBeaconGroupRow.AuxiliaryCode:
                                    beaconGroup.AuxiliaryCode = CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconGroupRow.AuxiliaryCode));
                                    break;
                            }
                            cnt++;
                        }
                        beaconGroup.TriggerAppId = triggerAppId;
                        beaconGroupList.Add(beaconGroup);
                        allGroup.Add(beaconGroup);  //加到AllGroup中做比對,避免重複的資料寫入
                    }
                }
                //新增 Beacon_Group 相關資料
                _bp.InsertOrUpdateBeaconGroupList(beaconGroupList);

                //檢查群組數量兩個以上時,追加FULLSHOP
                List<BeaconGroup> beaconGroupFullShopList = new List<BeaconGroup>();
                var newAllGroup = _bp.GetBeaconGroupList(triggerAppId);

                foreach (var beaconGroup in beaconGroupList)
                {
                    var thisBeaconGroup = newAllGroup.Where(x => x.GroupCode == beaconGroup.GroupCode).ToList();
                    if (thisBeaconGroup.Count >= 2 && thisBeaconGroup.All(x => x.AuxiliaryCode != FULLSHOP) && !beaconGroupFullShopList.Any(x => thisBeaconGroup.Select(y => y.GroupCode).Contains(x.GroupCode)))
                    {
                        BeaconGroup beaconGroupFullShop = new BeaconGroup
                        {
                            TriggerAppId = triggerAppId,
                            GroupRemark = beaconGroup.GroupRemark,
                            GroupCode = beaconGroup.GroupCode,
                            AuxiliaryCode = FULLSHOP
                        };
                        beaconGroupFullShopList.Add(beaconGroupFullShop);
                    }
                }
                _bp.InsertOrUpdateBeaconGroupList(beaconGroupFullShopList.Distinct().ToList());

                return Json(new { Success = true, Info = beaconGroupList, Message = "已成功新增 " + beaconGroupList.Count + " 筆群組資料。" + (isRepeat ? repeatMessage : string.Empty) });
            }
            catch
            {
                return Json(new { Success = false, Message = "儲存失敗，請再次確認檔案格式與內容是否正確。" });
            }
        }

        /// <summary>
        /// 刪除Beacon群組
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult DeleteBeaconGroupListInfo(int groupId)
        {
            bool isSuccess = false;

            try
            {
                var beaconGroup = _bp.GetBeaconGroupByGroupId(groupId);

                //刪除 beacon group
                _bp.DeleteBeaconEventGroupIdList(groupId);

                //檢查群組數量剩一個時(不包含FULLSHOP本身),刪除FULLSHOP
                var beaconGroupCount = _bp.GetBeaconGroupList(beaconGroup.TriggerAppId, beaconGroup.GroupCode).Where(x => x.AuxiliaryCode != FULLSHOP).ToList().Count;
                if (beaconGroupCount == 1)
                {
                    beaconGroup.AuxiliaryCode = FULLSHOP;
                    _bp.DeleteBeaconEventGroupIdList(groupId);
                }

                //刪除beacon event group link相關資料
                _bp.DeleteBeaconEventGroupLink(groupId);

                //刪除beacon group device link相關資料
                _bp.DeleteBeaconGroupDeviceLink(groupId);

                isSuccess = true;
            }
            catch
            {
                //ignore
            }
            return Json(isSuccess);
        }

        /// <summary>
        /// 取得Beacon裝置列表資訊
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="page"></param>
        /// <param name="ddlGroupCode"></param>
        /// <param name="ddlAuxiliaryCode"></param>
        /// <param name="deviceName"></param>
        /// <param name="beginMinor"></param>
        /// <param name="endMinor"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult GetBeaconDeviceListInfo(int triggerAppId, int page, string ddlGroupCode, string ddlAuxiliaryCode, string deviceName, string beginMinor, string endMinor, string field)
        {
            List<BeaconDeiceManageInfo> beaconDeiceManageInfoList = new List<BeaconDeiceManageInfo>();

            //無場域的Beacon
            var beaconDeviceWithAllGroupList = _bp.GetBeaconDevice();
            beaconDeiceManageInfoList.AddRange(beaconDeviceWithAllGroupList.Select(beaconDeviceWithAllGroup => new BeaconDeiceManageInfo
            {
                DeviceId = beaconDeviceWithAllGroup.DeviceId,
                DeviceName = beaconDeviceWithAllGroup.DeviceName,
                Major = beaconDeviceWithAllGroup.Major,
                Minor = beaconDeviceWithAllGroup.Minor,
                MacAddress = beaconDeviceWithAllGroup.ElectricMacAddress,
                IsField = false
            }));

            //有場域的Beacon
            var beaconDeviceWithoutGroupList = _bp.GetBeaconDeviceList(triggerAppId);
            foreach (var beaconDeiceManageInfo in beaconDeiceManageInfoList)
            {
                if (beaconDeviceWithoutGroupList.Any(x => x.DeviceId == beaconDeiceManageInfo.DeviceId))
                {
                    beaconDeiceManageInfo.IsField = true;
                }
            }

            #region 資料篩選
            if (!string.IsNullOrEmpty(beginMinor) && !string.IsNullOrEmpty(endMinor))
            {
                beaconDeiceManageInfoList = beaconDeiceManageInfoList.Where(x => x.Minor >= int.Parse(beginMinor) && x.Minor <= int.Parse(endMinor)).ToList();
            }

            if (!string.IsNullOrEmpty(deviceName))
            {
                beaconDeiceManageInfoList = beaconDeiceManageInfoList.Where(x => x.DeviceName.Contains(deviceName)).ToList();
            }

            if (!string.IsNullOrEmpty(field))
            {
                switch (field)
                {
                    case "isNotField":
                        beaconDeiceManageInfoList = beaconDeiceManageInfoList.Where(x => x.IsField == false).ToList();
                        break;
                    case "isField":
                        beaconDeiceManageInfoList = beaconDeiceManageInfoList.Where(x => x.IsField == true).ToList();
                        break;
                    default:
                        break;
                }
            }
            #endregion

            //分頁
            var partBeaconDeiceManageInfoList = beaconDeiceManageInfoList.Skip((page - 1) * 10).Take(10).ToList();

            return Json(new { data = partBeaconDeiceManageInfoList, count = beaconDeiceManageInfoList.Count });
        }

        /// <summary>
        /// 編輯Beacon設備
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="deviceId"></param>
        /// <param name="deviceName"></param>
        /// <param name="major"></param>
        /// <param name="minor"></param>
        /// <param name="macAddress"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult EditBeaconDeviceListInfo(int triggerAppId, int deviceId, string deviceName, int major, int minor, string macAddress)
        {
            try
            {
                //檢查是否重複
                if (deviceId == 0 && _bp.GetBeaconDeviceId(major, minor) != 0) return Json(-1);

                //編輯 Beacon_Device 相關資料
                BeaconDevice beaconDevice = new BeaconDevice()
                {
                    DeviceId = deviceId,
                    DeviceName = deviceName,
                    Major = major,
                    Minor = minor,
                    ElectricMacAddress = macAddress,
                    ElectricPower = ELECTRIC_POWER_DEFAULT,
                    LastUpdateTime = DateTime.Now
                };
                _bp.InsertOrUpdateBeaconDevice(beaconDevice);

                //編輯 Beacon_Field_Device_Link 相關資料
                if (deviceId == 0)  //為新增Beacon時，才順便加入link field
                {
                    List<int> deviceIds = new List<int> { beaconDevice.DeviceId };
                    _bp.InsertOrUpdateBeaconFieldDeviceLink(triggerAppId, deviceIds);
                }
                return Json(beaconDevice.DeviceId);
            }
            catch
            {
                return Json(0);
            }
        }

        public struct FileOfBeaconDeviceRow
        {
            public const int DeviceName = 0;
            public const int Major = 1;
            public const int Minor = 2;
            public const int MacAddress = 3;
            public const int FinishRow = 4;
        }

        /// <summary>
        /// 批次匯入Beacon設備
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult EditBeaconDeviceListInfoByFile(int triggerAppId)
        {
            try
            {
                List<BeaconDevice> beaconDeviceList = new List<BeaconDevice>();
                var nowTime = DateTime.Now;
                var repeat = string.Empty;

                if (Request.Files.Count == 0)
                    return Json(new { Success = false, Message = "未選擇任何檔案。" });

                //支援多檔上傳
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    //檢查附檔名
                    if (Path.GetExtension(file.FileName) != ".xls")
                        return Json(new { Success = false, Message = "請確認檔案格式是否正確。" });

                    //選擇工作表
                    HSSFWorkbook workbook = new HSSFWorkbook(file.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);

                    //從第二列開始讀取Beacon資料
                    var allDevice = _bp.GetBeaconDevice();
                    for (var j = 1; j <= currentSheet.LastRowNum; j++)
                    {
                        Row row = currentSheet.GetRow(j);

                        //Major或Minor為空 或 已存在 則跳過
                        if (string.IsNullOrEmpty(CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconDeviceRow.Major)))) continue;
                        if (string.IsNullOrEmpty(CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconDeviceRow.Minor)))) continue;
                        if (allDevice.Any(x => x.Major == (int)row.GetCell(FileOfBeaconDeviceRow.Major).NumericCellValue && x.Minor == (int)row.GetCell(FileOfBeaconDeviceRow.Minor).NumericCellValue))
                        {
                            repeat = "(Major, Minor重複的Beacon將自動跳過)";
                            continue;
                        }

                        BeaconDevice beaconDevice = new BeaconDevice();
                        int cnt = 0;
                        while (cnt < FileOfBeaconDeviceRow.FinishRow)
                        {
                            switch (cnt % FileOfBeaconDeviceRow.FinishRow)
                            {
                                case FileOfBeaconDeviceRow.DeviceName:
                                    beaconDevice.DeviceName = CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconDeviceRow.DeviceName));
                                    break;
                                case FileOfBeaconDeviceRow.Major:
                                    beaconDevice.Major = (int)row.GetCell(FileOfBeaconDeviceRow.Major).NumericCellValue;
                                    break;
                                case FileOfBeaconDeviceRow.Minor:
                                    beaconDevice.Minor = (int)row.GetCell(FileOfBeaconDeviceRow.Minor).NumericCellValue;
                                    break;
                                case FileOfBeaconDeviceRow.MacAddress:
                                    beaconDevice.ElectricMacAddress = CommonFacade.GetSheetCellValueString(row.GetCell(FileOfBeaconDeviceRow.MacAddress));
                                    break;
                            }
                            cnt++;
                        }
                        beaconDevice.ElectricPower = ELECTRIC_POWER_DEFAULT;
                        beaconDevice.LastUpdateTime = nowTime;
                        beaconDeviceList.Add(beaconDevice);
                    }
                }
                //編輯 Beacon_Device 相關資料
                _bp.InsertOrUpdateBeaconDeviceList(beaconDeviceList);

                //編輯 Beacon_Field_Device_Link 相關資料
                var devicdIds = beaconDeviceList.Select(x => x.DeviceId).ToList();
                if (devicdIds.Count > 0)
                    _bp.InsertOrUpdateBeaconFieldDeviceLink(triggerAppId, devicdIds);

                return Json(new { Success = true, Info = beaconDeviceList, Message = "已成功新增 " + beaconDeviceList.Count + " 筆Beacon資料。" + repeat });
            }
            catch
            {
                return Json(new { Success = false, Message = "儲存失敗，請再次確認檔案格式與內容是否正確。" });
            }
        }

        /// <summary>
        /// 刪除BeaconDevice相關資料
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult DeleteBeaconDeviceListInfo(int deviceId)
        {
            bool isSuccess = false;

            try
            {
                //刪除beacon device
                _bp.DeleteBeaconDevice(deviceId);

                //刪除beacon field device link相關資料
                _bp.DeleteBeaconFieldDeviceLink(deviceId);

                //刪除beacon event device link相關資料
                _bp.DeleteBeaconEventDeviceLink(deviceId);

                //刪除beacon group device link相關資料
                _bp.DeleteBeaconGroupDeviceLinkByDeviceId(deviceId);

                isSuccess = true;
            }
            catch
            {
                // ignored
            }

            return Json(isSuccess);
        }

        /// <summary>
        /// 授權廠商使用Beacon Device的場域權限
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="deviceId"></param>
        /// <param name="isField"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult AuthorizeBeaconDeviceField(int triggerAppId, int deviceId, bool isField)
        {
            try
            {
                if (isField)
                {
                    _bp.DeleteBeaconFieldDeviceLink(triggerAppId, deviceId);
                    _bp.DeleteBeaconGroupDeviceLinkByDeviceId(triggerAppId, deviceId);
                }
                else
                {
                    var beaconDeviceList = _bp.GetBeaconDeviceList(triggerAppId).FirstOrDefault(x => x.DeviceId == deviceId);

                    if (beaconDeviceList == null)
                    {
                        List<int> deviceIds = new List<int> { deviceId };
                        _bp.InsertOrUpdateBeaconFieldDeviceLink(triggerAppId, deviceIds);
                    }
                }
                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        /// <summary>
        /// 取得Beacon群組管理列表資料
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="ddlGroupCode"></param>
        /// <param name="ddlAuxiliaryCode"></param>
        /// <param name="deviceName"></param>
        /// <param name="beginMinor"></param>
        /// <param name="endMinor"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult GetBeaconChangeListInfo(string functionName, string ddlGroupCode, string ddlAuxiliaryCode, string deviceName, string beginMinor, string endMinor, string group)
        {
            BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);

            //有群組
            var beaconChangeManageInfoList = _bp.GetBeaconChangeManageInfoList(bta.TriggerAppId);
            var beaconDeviceList = _bp.GetBeaconDeviceList(bta.TriggerAppId);

            //無群組
            foreach (var beaconDevice in beaconDeviceList)
            {
                if (beaconChangeManageInfoList.Any(x => x.DeviceId == beaconDevice.DeviceId)) continue; //已有群組就跳過

                var beaconChangeManageInfo = new BeaconChangeManageInfo
                {
                    DeviceId = beaconDevice.DeviceId,
                    DeviceName = beaconDevice.DeviceName,
                    Major = beaconDevice.Major,
                    Minor = beaconDevice.Minor,
                    GroupCode = string.Empty,
                    GroupRemark = string.Empty,
                    AuxiliaryCode = string.Empty,
                };
                beaconChangeManageInfoList.Add(beaconChangeManageInfo);

            }

            #region 資料篩選
            if (!string.IsNullOrEmpty(beginMinor) && !string.IsNullOrEmpty(endMinor))
            {
                beaconChangeManageInfoList = beaconChangeManageInfoList.Where(x => x.Minor >= int.Parse(beginMinor) && x.Minor <= int.Parse(endMinor)).ToList();
            }

            if (!string.IsNullOrEmpty(deviceName))
            {
                beaconChangeManageInfoList = beaconChangeManageInfoList.Where(x => x.DeviceName.Contains(deviceName)).ToList();
            }

            if (!string.IsNullOrEmpty(group))
            {
                switch (group)
                {
                    case "isNotGroup":
                        beaconChangeManageInfoList = beaconChangeManageInfoList.Where(x => string.IsNullOrEmpty(x.GroupCode)).ToList();
                        break;
                    case "isGroup":
                        beaconChangeManageInfoList = beaconChangeManageInfoList.Where(x => !string.IsNullOrEmpty(x.GroupCode)).ToList();
                        break;
                    default:
                        break;
                }
            }
            #endregion

            return Json(new { data = beaconChangeManageInfoList, count = beaconChangeManageInfoList.Count });
        }

        /// <summary>
        /// 更換Beacon群組
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="deviceList"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult BeaconDeivceChangeGroup(string functionName, string deviceList)
        {
            try
            {
                if (string.IsNullOrEmpty(deviceList) || deviceList == "[]")
                {
                    return Json(new { Success = false, Message = "更新失敗，沒有選取任何資料" });
                }

                BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);
                List<BeaconDeviceChangeInfo> beaconDeviceList = JsonConvert.DeserializeObject<List<BeaconDeviceChangeInfo>>(deviceList);
                List<BeaconGroup> beaconGroupList = _bp.GetBeaconGroupList(bta.TriggerAppId);
                List<BeaconGroupDeviceLink> beaconGroupDeviceLinkList = new List<BeaconGroupDeviceLink>();
                List<BeaconEventGroupLink> beaconEventGroupLinkList = new List<BeaconEventGroupLink>();

                foreach (var beaconDevice in beaconDeviceList)
                {
                    //Save BeaconGroupDeviceLink
                    var groupDeviceLink = _bp.GetBeaconGroupDeviceLinkList(bta.TriggerAppId, beaconDevice.DeviceId).FirstOrDefault();
                    var xtop = 0;
                    var xleft = 0;
                    if (groupDeviceLink != null)
                    {
                        xtop = groupDeviceLink.Xtop;
                        xleft = groupDeviceLink.Xleft;
                    }

                    //先刪除既有的BeaconGroupDeviceLink
                    _bp.DeleteBeaconGroupDeviceLinkByDeviceId(beaconDevice.DeviceId);

                    //重建BeaconGroupDeviceLink
                    var group = beaconGroupList.FirstOrDefault(x => x.GroupCode == beaconDevice.GroupCode && x.AuxiliaryCode == beaconDevice.AuxiliaryCode);
                    var groupFullShop = beaconGroupList.FirstOrDefault(x => x.GroupCode == beaconDevice.GroupCode && x.AuxiliaryCode == FULLSHOP);

                    if (groupFullShop != null)
                    {
                        BeaconGroupDeviceLink beaconGroupDeviceLinkByFullShop = new BeaconGroupDeviceLink
                        {
                            GroupId = groupFullShop.GroupId,
                            DeviceId = beaconDevice.DeviceId,
                            Floor = beaconDevice.AuxiliaryCode,
                            Xtop = xtop,
                            Xleft = xleft
                        };
                        beaconGroupDeviceLinkList.Add(beaconGroupDeviceLinkByFullShop);
                    }

                    BeaconGroupDeviceLink beaconGroupDeviceLink = new BeaconGroupDeviceLink
                    {
                        GroupId = group.GroupId,
                        DeviceId = beaconDevice.DeviceId,
                        Floor = beaconDevice.AuxiliaryCode,
                        Xtop = xtop,
                        Xleft = xleft
                    };

                    beaconGroupDeviceLinkList.Add(beaconGroupDeviceLink);

                    //Save BeaconEventGroupLink
                    var beaconEventDevicdLinklist = _bp.GetBeaconEventDeviceLink(beaconDevice.DeviceId);

                    if (beaconEventDevicdLinklist == null) continue;

                    beaconEventGroupLinkList.AddRange(beaconEventDevicdLinklist.Select(beaconEventDevicdLink => new BeaconEventGroupLink()
                    {
                        Id = beaconEventDevicdLink.EventGroupLinkId,
                        EventId = beaconEventDevicdLink.EventId,
                        GroupId = beaconGroupDeviceLink.GroupId,
                        LinkType = (int)LinkType.GroupInCustom
                    }));
                }
                _bp.InsertOUpdateBeaconGroupDeviceLinkList(beaconGroupDeviceLinkList);
                _bp.InsertOUpdateBeaconEventGroupLinkList(beaconEventGroupLinkList);

                return Json(new { Success = true, Message = "更新成功" });
            }
            catch
            {
                return Json(new { Success = false, Message = "更新失敗，請聯絡技術人員" });
            }
        }

        #endregion

        #region TriggerLog

        /// <summary>
        /// 觸發紀錄
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <returns></returns>
        [BeaconIdentification]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult TriggerLog(int triggerAppId = 0)
        {
            InitDropDownList(null, null, null, triggerAppId);

            return View();
        }

        /// <summary>
        /// 取得事件資料
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="ddlGroupCode"></param>
        /// <param name="ddlAuxiliaryCode"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult GetEventDate(string functionName, string ddlGroupCode, string ddlAuxiliaryCode)
        {
            BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);
            var eventList = _bp.GetBeaconEventList(bta.TriggerAppId, ddlGroupCode, ddlAuxiliaryCode).ToDictionary(data => data.Subject, data => data.EventId);

            return Json(eventList);
        }

        /// <summary>
        /// 取得Beacon觸發紀錄資料
        /// </summary>
        /// <param name="functionName"></param>
        /// <param name="eventId"></param>
        /// <param name="eventType"></param>
        /// <param name="onlyTarget"></param>
        /// <param name="ddlAuxiliaryCode"></param>
        /// <param name="deviceId"></param>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <param name="userId"></param>
        /// <param name="ddlGroupCode"></param>
        /// <param name="isMember"></param>
        /// <param name="orderByCol"></param>
        /// <param name="orderByDesc"></param>
        /// <param name="orderByInfoCol"></param>
        /// <param name="orderByInfoDesc"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult GetBeaconLog(string functionName, int eventId, int eventType, int userId, string ddlGroupCode, bool onlyTarget, string ddlAuxiliaryCode, int deviceId, string beginDate, string endDate, int isMember, string orderByCol, bool orderByDesc, string orderByInfoCol, bool orderByInfoDesc)
        {
            BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);

            #region 抓取beaconLog資料

            var beaconGroups = _bp.GetBeaconGroupList(bta.TriggerAppId, ddlGroupCode);
            var beaconGroup = beaconGroups.FirstOrDefault(y => y.AuxiliaryCode != FULLSHOP);
            var beaconGroupIds = beaconGroups.Select(x => x.GroupId).ToList();
            var beaconTriggerLogInfoList = _bp.GetTriggerLogInfo(bta.TriggerAppId, beaconGroupIds, beaconGroup.GroupRemark, DateTime.Parse(beginDate), DateTime.Parse(endDate), eventId, userId, deviceId, (BeaconIsMember)isMember, orderByCol, orderByDesc);
  
            #endregion

            #region 抓取beaconEvent資料

            List<BeaconTriggerLogEventInfo> beaconTriggerLogEventInfoList = new List<BeaconTriggerLogEventInfo>();
            List<string> auxiliaryCodes = new List<string> {ddlAuxiliaryCode, FULLSHOP};
            var beaconEventList = _bp.GetBeaconEventList(bta.TriggerAppId, ddlGroupCode, auxiliaryCodes);
            var beaconEventListInfo = beaconEventList.Distinct().Where(x => x.EffectiveDateStart <= DateTime.Parse(endDate) && x.EffectiveDateEnd >= DateTime.Parse(beginDate)).ToList();
            var beaconTargetList = _bp.GetBeaconEventTargetList(beaconEventListInfo.Select(x => x.EventId).ToList()).Select(x => x.EventId).ToList(); //指定客群資料

            foreach (var beaconEventInfo in beaconEventListInfo)  //事件啟用區間
            {

                var beaconGroupLinkList = _bp.GetBeaconEventGroupList(beaconEventInfo.EventId, beaconGroup.GroupId);
                List<int> beaconEventInfoDeviceId = new List<int>();

                if (beaconGroupLinkList.Any(x => x.LinkType == (int)LinkType.Group))
                {
                    beaconEventInfoDeviceId = new List<int>();
                }
                else
                {
                    beaconEventInfoDeviceId.AddRange(beaconGroupLinkList.Select(beaconGroupLink => _bp.GetBeaconEventDeviceLinkForDeviceId(beaconGroupLink.Id)));
                }

                beaconTriggerLogEventInfoList.Add(new BeaconTriggerLogEventInfo
                {
                    EventId = beaconEventInfo.EventId,
                    EventSubject = beaconEventInfo.Subject,
                    EventType = beaconEventInfo.EventType,
                    BeginDate = beaconEventInfo.EffectiveDateStart,
                    EndDate = beaconEventInfo.EffectiveDateEnd,
                    State = beaconEventInfo.Enabled,
                    DeviceId = beaconEventInfoDeviceId

                });
            }

            #region 資料篩選

            //條件篩選
            if (eventId != 0)
            {
                beaconTriggerLogEventInfoList = beaconTriggerLogEventInfoList.Where(p => p.EventId == eventId).ToList();
            }

            if (eventType != 0)
            {
                beaconTriggerLogEventInfoList = beaconTriggerLogEventInfoList.Where(p => p.EventType == eventType).ToList();
            }

            if (onlyTarget)
            {
                beaconTriggerLogEventInfoList = beaconTriggerLogEventInfoList.Where(p => beaconTargetList.Contains(p.EventId)).ToList();
            }

            //排序
            switch (orderByInfoCol)
            {
                case "eventSubject":
                    beaconTriggerLogEventInfoList = orderByInfoDesc ? beaconTriggerLogEventInfoList.OrderBy(x => x.EventSubject).ToList() : beaconTriggerLogEventInfoList.OrderByDescending(x => x.EventSubject).ToList();
                    break;

                case "eventType":
                    beaconTriggerLogEventInfoList = orderByInfoDesc ? beaconTriggerLogEventInfoList.OrderBy(x => x.EventType).ToList() : beaconTriggerLogEventInfoList.OrderByDescending(x => x.EventType).ToList();
                    break;

                case "eventDate":
                    beaconTriggerLogEventInfoList = orderByInfoDesc ? beaconTriggerLogEventInfoList.OrderBy(x => x.BeginDate).ToList() : beaconTriggerLogEventInfoList.OrderByDescending(x => x.BeginDate).ToList();
                    break;

                case "eventState":
                    beaconTriggerLogEventInfoList = orderByInfoDesc ? beaconTriggerLogEventInfoList.OrderBy(x => x.State).ToList() : beaconTriggerLogEventInfoList.OrderByDescending(x => x.State).ToList();
                    break;

                default:
                    beaconTriggerLogEventInfoList = orderByInfoDesc ? beaconTriggerLogEventInfoList.OrderBy(x => x.EventId).ToList() : beaconTriggerLogEventInfoList.OrderByDescending(x => x.EventId).ToList();
                    break;
            }
            #endregion

            #endregion

            return Json(new { beaconTriggerLogInfoList, beaconTriggerLogEventInfoList });
        }

        #endregion

        #region 共用

        /// <summary>
        /// SKM 館別下拉選單
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetShopCodes(int triggerAppId, string sellerGuid)
        {
            var data = GetShopCodeDropDownList(triggerAppId, sellerGuid, string.Empty);
            return Json(data);
        }

        /// <summary>
        /// SKM 樓層下拉選單
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetFloorCodes(int triggerAppId, string groupCode)
        {
            var data = GetFloorCodeDropDownList(triggerAppId, groupCode, string.Empty);
            return Json(data);
        }

        /// <summary>
        /// 17life的AuxiliaryCode下拉選單
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupCode"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetAuxiliaryCodes(int triggerAppId, string groupCode)
        {
            var data = GetAuxiliaryCodeDropDownList(triggerAppId, groupCode);
            return Json(data);
        }


        /// <summary>
        /// 儲存圖資管理
        /// </summary>
        /// <param name="shopCode"></param>
        /// <param name="floor"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SaveBeaconDeviceMapInfo(string shopCode, string floor, string data)
        {
            bool isSuccess = false;
            try
            {
                List<ActionEventDeviceInfo> beaconList = new JsonSerializer().Deserialize<List<ActionEventDeviceInfo>>(data);

                foreach (var beacon in beaconList)
                {
                    List<BeaconGroupDeviceLink> beaconInfos = _bp.GetBeaconGroupDeviceLinkList().Where(x => x.DeviceId == beacon.Id).ToList();

                    foreach (var beaconInfo in beaconInfos)
                    {
                        beaconInfo.Xtop = beacon.Xtop ?? 0;
                        beaconInfo.Xleft = beacon.Xleft ?? 0;
                        _bp.InsertOUpdateBeaconGroupDeviceLink(beaconInfo);
                    }

                    BeaconDevice beaconDevice = _bp.GetBeaconDevice(beacon.Id);
                    if (beaconDevice != null)
                    {
                        beaconDevice.DeviceName = beacon.DeviceName;
                        beaconDevice.LastUpdateTime = DateTime.Now;
                        _bp.InsertOrUpdateBeaconDevice(beaconDevice);
                    }
                }
                isSuccess = true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            return Json(isSuccess);
        }

        /// <summary>
        /// 檢查Bid是否存在
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBeaconBid(string bid)
        {
            if (IsGuid(bid))
            {
                var ved = _pp.ViewExternalDealGetByBid(Guid.Parse(bid));

                if (ved.IsLoaded)
                {
                    return Json(ved);
                }
            }
            return Json(0);
        }

        /// <summary>
        /// 取得Beacon資料
        /// </summary>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <param name="functionName"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult BindBeacon(string groupCode, string auxiliaryCode, string functionName)
        {
            BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);
            List<BeaconDevice> beacons = new List<BeaconDevice>();

            beacons = string.IsNullOrEmpty(groupCode) ? _bp.GetSingleBeaconDeviceList(bta.TriggerAppId, 0).ToList() : _bp.GetBeaconDeviceListByBeaconGroup(bta.TriggerAppId, groupCode, auxiliaryCode).ToList();

            return Json(beacons);
        }

        /// <summary>
        /// 取的檔次Bid資料
        /// </summary>
        /// <param name="beginDate"></param>
        /// <param name="endDate"></param>
        /// <param name="sellerGuid"></param>
        /// <param name="brandCounter"></param>
        /// <param name="actionType"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult BindBidDate(string beginDate, string endDate, string sellerGuid, string brandCounter, string actionType)
        {
            DateTime timeS = DateTime.Parse(beginDate);
            DateTime timeE = DateTime.Parse(endDate);
            var skmAllShopes = _skm.SkmShoppeGetAllShoppe(Guid.Parse(sellerGuid));
            ViewExternalDealCollection vedList = new ViewExternalDealCollection();

            foreach (var skmAllShope in skmAllShopes)
            {
                vedList.AddRange(_pp.ViewExternalDealGetList(skmAllShope.ShopCode).Where(x => (x.BusinessHourOrderTimeS <= timeE && x.BusinessHourOrderTimeE >= timeS) && x.Bid != null).Distinct());
            }

            #region  篩選資料

            IEnumerable<ViewExternalDeal> ivedc = vedList;

            //全業種
            if (brandCounter != string.Empty)
            {
                ivedc = ivedc.Where(x => x.CategoryList.IndexOf(brandCounter, StringComparison.Ordinal) > 0);
            }

            //全檔次類型
            if (actionType != string.Empty)
            {
                ivedc = ivedc.Where(x => x.TagList.IndexOf(actionType, StringComparison.Ordinal) > 0);
            }

            #endregion

            return Json(ivedc.Select(x => new { Bid = x.Bid, Title = x.Title }).Distinct());
        }

        /// <summary>
        /// 取的已選擇的Beacon資料(checked box)
        /// </summary>
        /// <param name="beacons"></param>
        /// <param name="groups"></param>
        /// <param name="functionName"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult BindBeaconData(string beacons, string groups, string functionName)
        {
            BeaconTriggerApp bta = _bp.GetBeaconTriggerAppByFunctionName(functionName);
            List<BeaconGroupModel> groupsArray = JsonConvert.DeserializeObject<List<BeaconGroupModel>>(groups);
            List<BeaconDeivceModel> beaconsArray = JsonConvert.DeserializeObject<List<BeaconDeivceModel>>(beacons);
            List<string> groupCodeArray = new List<string>();
            List<BeaconGroupModel> beaconGroupModelArray = new List<BeaconGroupModel>();
            List<BindBeaconData> rets = new List<BindBeaconData>();
            var allGroup = _bp.GetBeaconGroupList(bta.TriggerAppId);

            //先塞選出 全館/全樓層 
            foreach (var group in groupsArray)
            {
                var groupCode = group.GroupCode;
                var auxiliaryCode = group.AuxiliaryCode;
                if (auxiliaryCode == FULLSHOP)
                {
                    groupCodeArray.Add(groupCode);
                }
                else
                {
                    BeaconGroupModel beaconGroupModel = new BeaconGroupModel
                    {
                        GroupCode = groupCode,
                        AuxiliaryCode = auxiliaryCode
                    };
                    beaconGroupModelArray.Add(beaconGroupModel);
                }
            }

            //全群組
            if (!string.IsNullOrEmpty(groups))
            {
                foreach (var group in groupsArray)
                {
                    var groupCode = group.GroupCode;
                    var auxiliaryCode = group.AuxiliaryCode;
                    BeaconGroup groupOne = null;
                    var floor = string.Empty;

                    if (auxiliaryCode == FULLSHOP)
                    {
                        groupOne = allGroup.FirstOrDefault(x => x.GroupCode == groupCode && x.AuxiliaryCode == auxiliaryCode);
                    }
                    else
                    {
                        groupOne = allGroup.FirstOrDefault(x => x.GroupCode == groupCode && x.AuxiliaryCode == auxiliaryCode && !groupCodeArray.Contains(x.GroupCode));
                        floor = "_" + auxiliaryCode;
                    }


                    if (groupOne != null)
                    {
                        BindBeaconData ret = new BindBeaconData
                        {
                            DeviceName = groupOne.GroupRemark + floor,
                            Major = 0,
                            Minor = 0,
                            GroupRemark = auxiliaryCode,
                            Floor = string.Empty,
                            Address = 0 + "," + 0
                        };
                        rets.Add(ret);
                    }
                }
            }

            //非全群組/單獨
            if (!string.IsNullOrEmpty(beacons))
            {
                List<BeaconDeivceModel> beaconDeivceListHasId = new List<BeaconDeivceModel>();
                var beaconGroupIdDeivceIds = _bp.GetBeaconGroupIdList(bta.TriggerAppId);
                var beaconDeviceList = _bp.GetBeaconDevice();

                foreach (var beaconDeivceModel in beaconsArray.Distinct().ToList())
                {
                    var firstOrDefault = beaconDeviceList.FirstOrDefault(x => x.Major == int.Parse(beaconDeivceModel.Major) && x.Minor == int.Parse(beaconDeivceModel.Minor));
                    if (firstOrDefault != null)
                    {
                        var deviceId = firstOrDefault.DeviceId;
                        beaconDeivceListHasId.Add(new BeaconDeivceModel { DeviceId = deviceId, Major = beaconDeivceModel.Major, Minor = beaconDeivceModel.Minor });
                    }
                }

                var beaconDeviceLink = _bp.GetBeaconGroupDeviceLinkList();

                foreach (var beacon in beaconDeivceListHasId)
                {
                    //Beacon設備座標資料
                    var thisBeaconDeviceLink = beaconDeviceLink.FirstOrDefault(x => x.DeviceId == beacon.DeviceId);
                    var thisBeaconDevices = beaconDeviceList.FirstOrDefault(x => x.DeviceId == beacon.DeviceId);

                    //檢查全店
                    var groupIds = beaconGroupIdDeivceIds.Where(x => x.DeviceId == beacon.DeviceId).Select(x => x.GroupId).ToList();
                    var group = allGroup.FirstOrDefault(x => x.AuxiliaryCode != FULLSHOP && groupIds.Contains(x.GroupId) && !groupCodeArray.Contains(x.GroupCode));

                    //檢查全樓層
                    if (group != null)
                    {
                        if (beaconGroupModelArray.Any(
                            beaconGroupModel =>
                            group.GroupCode == beaconGroupModel.GroupCode &&
                            group.AuxiliaryCode == beaconGroupModel.AuxiliaryCode))
                        {
                            group = null;
                        }
                    }

                    var groupName = string.Empty;
                    var floor = string.Empty;
                    int xtop = 0;
                    int xleft = 0;

                    if (group != null && thisBeaconDeviceLink != null)
                    {
                        groupName = group.GroupRemark;
                        floor = thisBeaconDeviceLink.Floor;
                        xtop = thisBeaconDeviceLink.Xleft;
                        xleft = thisBeaconDeviceLink.Xtop;
                    }

                    //beaconDeviceLink == null (無群組單顆)  group != null (群組內單顆)
                    if (beaconDeviceLink == null || group != null)
                    {
                        BindBeaconData ret = new BindBeaconData
                        {
                            DeviceName = thisBeaconDevices.DeviceName,
                            Major = thisBeaconDevices.Major,
                            Minor = thisBeaconDevices.Minor,
                            GroupRemark = groupName,
                            Floor = floor,
                            Address = xtop + "," + xleft
                        };
                        rets.Add(ret);
                    }

                }
            }
            return Json(rets);
        }

        /// <summary>
        /// 取得新光分店資料
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult BindSkmShoppeInfo(string sellerGuid)
        {
            Dictionary<string, string> categories = new Dictionary<string, string>();
            _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryType == (int)CategoryType.DealCategory && x.CategoryStatus == (int)CategoryStatus.Enabled)
                .ForEach(x => categories.Add(x.CategoryId.ToString(), x.CategoryName));

            Dictionary<string, string> tags = new Dictionary<string, string>();
            Enum.GetNames(typeof(SkmDealTypes))
                .ForEach(x => tags.Add(((int)Enum.Parse(typeof(SkmDealTypes), x)).ToString(), Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SkmDealTypes)Enum.Parse(typeof(SkmDealTypes), x))));

            return Json(new { categories, tags });
        }

        /// <summary>
        /// 取得樓層地圖對應的Beacon設備資料
        /// </summary>
        /// <param name="fuctionName"></param>
        /// <param name="shopCode"></param>
        /// <param name="floor"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ShoppMapGet(string fuctionName, string shopCode, string floor)
        {
            List<ViewBeaconDeviceGroup> vbdg = new List<ViewBeaconDeviceGroup>();

            if (floor != FULLSHOP)
            {
                var bat = _bp.GetBeaconTriggerAppByFunctionName(fuctionName);
                vbdg = _bp.GetBeaconDeviceGroupList(bat.TriggerAppId, null, shopCode, floor, "DeviceName", true);
            }
            return Json(vbdg);
        }


        /// <summary>
        /// 取得Beacon地圖
        /// </summary>
        /// <param name="fuctionName"></param>
        /// <param name="shopCode"></param>
        /// <param name="floor"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult MapImagesGet(string fuctionName, int shopCode, string floor)
        {
            if (floor == FULLSHOP)
            {
                return Json(string.Empty);
            }
            floor = floor.ToLower().Replace("f", string.Empty);
            if (floor.Substring(0, 1) == "0" && floor.Length > 1) floor = floor.Substring(1, floor.Length - 1);
            string src = "/Themes/Beacon/map/" + string.Format("{0}/{1}/{2}.jpg", fuctionName, shopCode, floor);
            if (!System.IO.File.Exists(Server.MapPath("~" + src)))
            {
                return Json(string.Empty);
            }
            return Json("../.." + src);
        }

        /// <summary>
        /// 搜尋Beacon設備資訊
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <param name="electricPower"></param>
        /// <param name="orderField"></param>
        /// <param name="isDesc"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult GetBeaconDeviceInfo(int triggerAppId, string groupCode, string auxiliaryCode, int electricPower, string orderField, bool isDesc)
        {
            List<ViewBeaconDeviceGroup> beaconDeviceGroupList = _bp.GetBeaconDeviceGroupList(triggerAppId,
                (BeaconPowerLevel)Enum.Parse(typeof(BeaconPowerLevel), electricPower.ToString()), groupCode,
                auxiliaryCode, orderField, isDesc);

            List<BeaconDeviceInfo> beaconDeviceInfoList = new List<BeaconDeviceInfo>();
            foreach (var beaconDeviceInfo in beaconDeviceGroupList)
            {
                beaconDeviceInfoList.Add(new BeaconDeviceInfo
                {
                    Major = beaconDeviceInfo.Major,
                    Minor = beaconDeviceInfo.Minor,
                    ElectricPower = beaconDeviceInfo.ElectricPower.ToString(),
                    DeviceName = beaconDeviceInfo.DeviceName,
                    Floor = beaconDeviceInfo.Floor,
                    LastUpdateTime = beaconDeviceInfo.LastUpdateTime.ToString("yyyy/MM/dd HH:mm:ss")
                });
            }
            return Json(beaconDeviceInfoList);
        }

        /// <summary>
        /// 匯入客群資料
        /// </summary>
        /// <param name="eventId"></param>
        /// <param name="uploaded"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BeaconFileUpload(int eventId, int uploaded)
        {
            if (eventId == 0) return Json(0);
            if (uploaded == 0)
            {
                _bp.DeleteBeaconEventTargetByEventId(eventId);
            }

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                int idx = 0;
                StreamReader reader = new StreamReader(file.InputStream, System.Text.Encoding.Default);
                while (!reader.EndOfStream)
                {

                    var line = reader.ReadLine();
                    if (idx == 0)
                    {
                        idx++;
                        continue;
                    }

                    string[] data = line.Split(",");

                    if (data.Length < 1 || string.IsNullOrEmpty(data[0]) || string.IsNullOrEmpty(data[1]))
                    {
                        idx++;
                        continue;
                    }
                    var beacontarget = new BeaconEventTarget
                    {
                        EventId = eventId,
                        TargetUserToken = data[0],
                        Remark = data[1]
                    };


                    _bp.InsertOrUpdateBeaconEventTarget(beacontarget);

                    idx++;
                }
            }
            return Json(1);
        }

        #endregion

        /// <summary>
        /// 初始化電量管理
        /// </summary>
        /// <param name="systemDataName"></param>
        /// <param name="systemData"></param>
        /// <returns></returns>
        private SystemData InitPowerNotificationSetting(string systemDataName, SystemData systemData)
        {
            systemData.Name = systemDataName;
            Dictionary<string, string> settingDictionary = new Dictionary<string, string>
            {
                {"beaconPowerLevel", "0"},
                {"notificationEmails", ""},
                {"eletricPowerNotUpdateWaitDay", "30"}
            };
            systemData.Data = JsonConvert.SerializeObject(settingDictionary);
            systemData.CreateId = "sys@17life.com";
            systemData.CreateTime = DateTime.Now;
            return systemData;
        }

        /// <summary>
        /// 電量標準
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetElectricLevels()
        {
            Dictionary<string, string> electricLevels = new Dictionary<string, string>
            {
                {"2", "充足電量"},
                {"1", "一般電量"},
                {"0", "低電量"}
            };
            return electricLevels;
        }

        /// <summary>
        /// 初始化下拉選單資訊
        /// </summary>
        /// <param name="sellerGuid">skm才有</param>
        /// <param name="groupCode"></param>
        /// <param name="auxiliaryCode"></param>
        /// <param name="triggerAppId"></param>
        private void InitDropDownList(string sellerGuid, string groupCode, string auxiliaryCode, int triggerAppId)
        {
            ViewBag.TriggerAppId = triggerAppId;

            if (triggerAppId == (int)BeaconTriggerAppFunctionName.skm)
            {
                //1.sellerGuid >> 預設顯示
                //2.sellerGuid >> 上一頁
                //3.sellerGuid >> 權限

                if (string.IsNullOrEmpty(sellerGuid))
                    sellerGuid = SKM_DEFAULT_SELLER;    //沒資料無條件帶預設

                var sellerCodes = GetSkmSellerDropDownList(sellerGuid);
                ViewBag.SellerCodes = sellerCodes;

                if (sellerCodes.All(p => p.Value != sellerGuid))//若已選擇seller不在權限sellers內
                {
                    sellerGuid = sellerCodes.FirstOrDefault().Value;//則預設權限sellers內的第一筆為  shopcode下拉的sellerGuid
                }

                //第二層下拉
                var shopCodes = GetShopCodeDropDownList(triggerAppId, sellerGuid, groupCode);
                ViewBag.ShopCodes = shopCodes;

                //第三層下拉
                ViewBag.FloorCodes = GetFloorCodeDropDownList(triggerAppId, shopCodes.FirstOrDefault().Value, auxiliaryCode);
            }
            else
            {
                Dictionary<string, string> groupCodes = GetGroupCodeDropDownList(triggerAppId);
                ViewBag.GroupCodes = groupCodes;
                ViewBag.AuxiliaryCodes = GetAuxiliaryCodeDropDownList(triggerAppId, string.IsNullOrEmpty(groupCode) ? groupCodes.FirstOrDefault().Key : groupCode);
            }
        }

        /// <summary>
        ///  群組第一層下拉選單
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> GetGroupCodeDropDownList(int triggerAppId)
        {
            var beaconGroups = _bp.GetBeaconGroupList(triggerAppId);
            var grouplist = beaconGroups.Select(x => new { groupCode = x.GroupCode, groupRemark = x.GroupRemark });
            return grouplist.Distinct().ToDictionary(data => data.groupCode, data => data.groupRemark); ;
        }

        /// <summary>
        /// 群組第二層下拉選單
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupCode"></param>
        /// <returns></returns>
        private Dictionary<string, string> GetAuxiliaryCodeDropDownList(int triggerAppId, string groupCode)
        {
            Dictionary<string, string> auxiliaryCodeList = new Dictionary<string, string>();
            auxiliaryCodeList = _bp.GetBeaconGroupList(triggerAppId, groupCode).ToDictionary(data => data.AuxiliaryCode, data => data.AuxiliaryCode);
            return auxiliaryCodeList;
        }

        /// <summary>
        /// SKM第一層下拉選單
        /// </summary>
        /// <returns></returns>
        public List<SelectListItem> GetSkmSellerDropDownList(string defaultSellerGuid)
        {
            //取得所有分店
            Dictionary<string, string> allSeller = _sp.SellerGetSkmParentList()
                .OrderByDescending(x => string.Format("{0},{1}", LocationFacade.GetGeographyByCoordinate(x.Coordinate).Lat.ToString(), LocationFacade.GetGeographyByCoordinate(x.Coordinate).Long.ToString())).ToDictionary(x => x.Guid.ToString(), x => x.SellerName);

            //取得有權限的seller
            int userId = Convert.ToInt32(User.Identity.GetPropertyValue("Id"));
            if (userId == 0) userId = MemberFacade.GetUniqueId(User.Identity.Name);

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, _config.SkmRootSellerGuid).ToList();
            var mainStoreRoles = ChannelFacade.GetOwnOneDownSeller(sellerRoleGuids, _config.SkmRootSellerGuid);

            var dl = allSeller.Where(item => mainStoreRoles.Contains(new Guid(item.Key))).Select(u => new SelectListItem
            {
                Text = u.Value, //這裡的key與value沒寫錯, 別誤會
                Value = u.Key,
                Selected = u.Key == defaultSellerGuid
            });
            return dl.ToList();
        }


        /// <summary>
        ///  SKM第二層下拉選單
        /// <param name="triggerAppId"></param>
        /// <param name="sellerGuid"></param>
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> GetShopCodeDropDownList(int triggerAppId, string sellerGuid, string defaultShopCode)
        {
            List<SelectListItem> groupList = new List<SelectListItem>();
            if (string.IsNullOrEmpty(sellerGuid))
            {
                groupList = _bp.GetBeaconGroupList((int)BeaconTriggerAppFunctionName.skm).Where(p => p.AuxiliaryCode == FULLSHOP)
                         //.ToDictionary(d => d.GroupCode, d => d.GroupRemark);
                         .Select(u => new SelectListItem
                         {
                             Value = u.GroupCode,
                             Text = u.GroupRemark,
                             Selected = u.GroupCode == defaultShopCode
                         }).ToList();
            }
            else
            {
                List<string> shopCodeList = _skm.SkmShoppeGetShopCodeBySellerGuid(new Guid(sellerGuid));
                groupList = _bp.GetBeaconGroupListByAuxiliaryCode((int)BeaconTriggerAppFunctionName.skm, FULLSHOP).Where(p => shopCodeList.Contains(p.GroupCode))
                        //.ToDictionary(d => d.GroupCode, d => d.GroupRemark);
                        .Select(u => new SelectListItem
                        {
                            Value = u.GroupCode,
                            Text = u.GroupRemark,
                            Selected = u.GroupCode == defaultShopCode
                        }).ToList();
            }
            return groupList;
        }

        /// <summary>
        /// SKM第三層下拉選單
        /// </summary>
        /// <param name="triggerAppId"></param>
        /// <param name="groupCode"></param>
        /// <param name="defaultFloorCode"></param>
        /// <returns></returns>
        private List<SelectListItem> GetFloorCodeDropDownList(int triggerAppId, string groupCode, string defaultFloorCode)
        {
            List<SelectListItem> groupList = new List<SelectListItem>();
            groupList = _bp.GetBeaconGroupListNotContainAuxiliaryCode(triggerAppId, groupCode, FULLSHOP)
                //.ToDictionary(d => d.AuxiliaryCode, d => d.AuxiliaryCode);
                .Select(u => new SelectListItem
                {
                    Value = u.AuxiliaryCode,
                    Text = u.AuxiliaryCode,
                    Selected = u.AuxiliaryCode == defaultFloorCode
                }).ToList();

            return groupList;
        }

        /// <summary>
        /// 判斷EventType並填入DB欄位
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="linkInput"></param>
        /// <param name="actionBid"></param>
        /// <param name="actionUrl"></param>
        private void GetEventTypeData(EventType eventType, string linkInput, out Guid actionBid, out string actionUrl)
        {
            switch (eventType)
            {
                case EventType.Bid:
                    actionBid = Guid.Parse(linkInput);
                    actionUrl = string.Empty;
                    break;
                case EventType.Url:
                    actionBid = Guid.Empty;
                    actionUrl = linkInput;
                    break;
                default:
                    actionBid = Guid.Empty;
                    actionUrl = string.Empty;
                    break;
            }
        }

        /// <summary>
        /// 判斷EventType並取得DB的資料
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="actionBid"></param>
        /// <param name="actionUrl"></param>
        private string GetLinkInputData(EventType eventType, string actionBid, string actionUrl)
        {
            switch (eventType)
            {
                case EventType.Bid:
                    return actionBid;
                case EventType.Url:
                    return actionUrl;
                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// 判斷是否為GUID格式
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private bool IsGuid(string bid)
        {
            Guid g = Guid.Empty;
            return Guid.TryParse(bid, out g);
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebLib.Controllers
{
    public class BaseProposalController : BaseController
    {
        #region property
        protected static ISysConfProvider _config;
        protected static ISellerProvider sp;
        protected static IPponProvider pp;
        protected static IOrderProvider op;
        protected static IHumanProvider hmp;
        protected static IMemberProvider mp;
        protected static IItemProvider ip;
        protected static ISystemProvider sysProv;
        protected static IPponEntityProvider pep;
        protected static IWmsProvider wp;
        protected static ILog logger = LogManager.GetLogger(typeof(BaseProposalController));

        #endregion property

        static BaseProposalController()
        {
            _config = ProviderFactory.Instance().GetConfig();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            sysProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
            wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        #region action
        #region 提案單
        public virtual ActionResult HouseProposalContent(int? pid)
        {
            if (pid == null)
            {
                throw new Exception("pid 為空!");
            }
            SellerProposalHouseModel sProposal = new SellerProposalHouseModel();
            Proposal pro = sp.ProposalGet(pid.GetValueOrDefault());
            ProposalContractFileCollection pcfc = sp.ProposalContractFileGetListBypid(pro.Id);
            ProposalCategoryDealCollection pcdc = sp.ProposalCategoryDealsGetList(pro.Id);

            if (pro.IsLoaded)
            {
                ChangeToComboDeals(pro, UserName);
                TryParseProposal(pro, out sProposal);

                #region 不顯示折後價於前台
                if (pro.BusinessHourGuid.HasValue)
                    sProposal.IsDealDiscountPriceBlacklist = pep.GetDealDiscountPriceBlacklist(pro.BusinessHourGuid.Value) != null;

                #endregion

                #region 下拉選項
                List<SystemCode> dealType = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null && x.CodeId != 1999).ToList();

                Dictionary<int, string> agentChannels = ProposalFacade.GetAgentChannels();

                #region 在地宅配的選單
                List<string> pponDealType = GetPponDealType();
                #endregion 在地宅配的選單

                //商品類型
                Dictionary<int, string> deliveryMethod = GetDeliveryMethod();

                //商品來源
                Dictionary<int, string> dealSource = GetDealSource();

                //合約類型
                Dictionary<int, string> contractType = new Dictionary<int, string>();
                foreach (var item in Enum.GetValues(typeof(SellerContractType)))
                {
                    if ((SellerContractType)item == SellerContractType.Flat || (SellerContractType)item == SellerContractType.PairActivity || (SellerContractType)item == SellerContractType.Pair || (SellerContractType)item == SellerContractType.Draft)
                        contractType.Add(((int)item), Helper.GetEnumDescription((SellerContractType)item));
                }
                #endregion 下拉選項

                #region 商家聯絡人
                Seller seller = sp.SellerGet(sProposal.SellerGuid);
                List<Seller.MultiContracts> sellerContact = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                string contact = string.Empty;
                if (sellerContact != null)
                {
                    foreach (var c in sellerContact)
                    {
                        contact += c.ContactPersonName + "/";
                    }
                }

                List<string> dealTypeHideList = SellerFacade.GetProposalItemPriceContent(seller.Guid);
                if (dealTypeHideList == null)
                {
                    dealTypeHideList = new List<string>();
                    //dealTypeHideList.Add("0000");//隨便塞一個，免得前端js出錯
                }

                if (!string.IsNullOrEmpty(contact))
                    contact = contact.TrimEnd("/");
                if (contact.Length > 50)
                    contact = contact.Substring(0, 50);
                #endregion

                #region 上檔頻道
                List<CategoryNode> nodeData = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq).ToList();
                nodeData = nodeData.Where(x => x.CategoryId.EqualsNone(91, 92, 93)).ToList();
                #endregion

                //業務
                ProposalSalesModel proposalSalesModel = ProposalFacade.ProposalSaleGetByPid(pro.Id);


                //是否為已確認(草稿)
                ViewBag.DownloadContract = "草稿列印";
                ViewBag.DownloadContract = "合約列印";

                #region 檔次資料
                string dealUniqueId = string.Empty;
                string bid = string.Empty;
                bool isCloseDeal = false;
                bool isRunning = false;
                int dealState = (int)PponDealStage.Running;
                if (pro.BusinessHourGuid != null)
                {
                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                    if (vpd.IsLoaded)
                    {
                        bid = vpd.BusinessHourGuid.ToString();
                        dealUniqueId = vpd.UniqueId.GetValueOrDefault().ToString();

                        DealTimeSlotCollection dts = pp.DealTimeSlotGetAllCol(vpd.BusinessHourGuid);

                        isRunning = dts.Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.EffectiveStart <= DateTime.Now && x.EffectiveEnd >= DateTime.Now).Count() > 0;

                        if (!isRunning)
                        {
                            //暫停銷售
                            dealState = (int)PponDealStage.ClosedAndFail;
                        }

                        if (vpd.BusinessHourOrderTimeE < DateTime.Now)
                        {
                            //已結檔
                            dealState = (int)PponDealStage.ClosedAndOn;
                        }
                        else if (vpd.BusinessHourOrderTimeS > DateTime.Now)
                        {
                            //尚未開賣
                            dealState = (int)PponDealStage.Ready;
                        }
                    }

                    isCloseDeal = (vpd.Slug != null);
                }

                foreach (var item in Enum.GetValues(typeof(SellerProposalFlag)))
                {
                    if (Helper.IsFlagSet(pro.SellerProposalFlag, (SellerProposalFlag)item))
                    {
                        sProposal.StatusName = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerProposalFlag)item);    //提案狀態
                    }
                }

                #endregion 檔次資料

                bool isDealOn = false;
                if (pro.BusinessHourGuid != null)
                {
                    BusinessHour bh = pp.BusinessHourGet(pro.BusinessHourGuid.Value);
                    if (bh.IsLoaded)
                    {
                        if (bh.BusinessHourOrderTimeS <= DateTime.Now && bh.BusinessHourOrderTimeE >= DateTime.Now)
                        {
                            isDealOn = true;
                        }
                    }
                }

                bool isCombol = false;
                if (pro.BusinessHourGuid != null)
                {
                    ComboDealCollection comboDeal = pp.GetComboDealByBid(pro.BusinessHourGuid.Value);
                    if (comboDeal.Count > 0)
                    {
                        isCombol = true;
                    }
                }

                bool applyServiceChannel = false;
                ViewVbsInstorePickupCollection vips = sp.ViewVbsInstorePickupCollectionGet(pro.SellerGuid);
                var vip = vips.Where(x => x.IsEnabled == true).FirstOrDefault();
                if (vip != null)
                {
                    applyServiceChannel = true;
                }

                ViewBag.ApplyServiceChannel = applyServiceChannel;
                bool isFinance = false;
                List<string> financeUser = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.TaxFreeNotice, "/sal/ProposalContent.aspx");
                if (financeUser.Contains(UserName))
                {
                    isFinance = true;
                }

                ViewBag.IsFinance = isFinance;
                ViewBag.IsCombol = isCombol;
                ViewBag.IsDealOn = isDealOn;
                ViewBag.IsProposalProductionComplete = ProposalFacade.IsProposalProductionComplete(pro);
                ViewBag.IsProposalGrossMarginRestrictionComplete = ProposalFacade.IsProposalGrossMarginRestrictionComplete(pro);
                ViewBag.DealTypeHideList = new JsonSerializer().Serialize(dealTypeHideList);
                ViewBag.DealState = dealState;
                ViewBag.IsCloseDeal = isCloseDeal;
                ViewBag.BusinessHourGuid = bid;
                ViewBag.DealUniqueId = dealUniqueId;
                ViewBag.Proposal = sProposal;
                ViewBag.ProposalContractFile = pcfc;
                ViewBag.PponDealType = new JsonSerializer().Serialize(pponDealType);
                ViewBag.AgentChannels = agentChannels;
                ViewBag.DealType = dealType;
                ViewBag.DeliveryMethod = deliveryMethod;
                ViewBag.DealSource = dealSource;
                ViewBag.ContractType = contractType;
                ViewBag.Contact = contact;
                ViewBag.UserName = UserName;
                ViewBag.CategoryNode = nodeData;
                ViewBag.Category = pcdc;
                ViewBag.Sales = proposalSalesModel;
                ViewBag.IsProductionEmp = User.IsInRole("Production");
                ViewBag.IsSalesManagerEmp = User.IsInRole("SalesManager") || User.IsInRole("SalesLeader");
                ViewBag.IsSalesEmp = User.IsInRole("Sales") || User.IsInRole("SalesAssistant");
                ViewBag.IsSalesAssistantEmp = User.IsInRole("SalesAssistant");
                ViewBag.IsAgreeNewContractSeller = SellerFacade.IsAgreeNewContractSeller(pro.SellerGuid, (int)DeliveryType.ToHouse);
                ViewBag.IsWms = pro.IsWms;
                ViewBag.IsConfirmWms = WmsFacade.isConfirmWms(pro.SellerGuid, string.Empty);
                //ViewBag.UserId = UserId.ToString();
            }
            else
            {
                throw new Exception("提案單號異常");
            }

            return null;
        }


        /// <summary>
        /// 頁面預覽
        /// </summary>
        /// <param name="pid">提案單單號</param>
        /// <returns></returns>
        public virtual ActionResult ProposalPreview(int pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            decimal discounted = 10;
            int itemPrice = default(int);
            int origPrice = default(int);
            decimal avgPrice = 0;

            //先判斷他是單檔還是多檔
            List<ProposalMultiDeal> multiDeals = sp.ProposalMultiDealGetByPid(pid).ToList();
            bool isMultiDeal = false;
            if (multiDeals.Count >= 1)
            {
                isMultiDeal = true;
            }

            ProposalMultiDeal pmd = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();
            if (pmd != null)
            {
                itemPrice = Convert.ToInt32(pmd.ItemPrice);
                origPrice = Convert.ToInt32(pmd.OrigPrice);
            }

            foreach (ProposalMultiDeal p in multiDeals)
            {
                int quantityMultiplier = 0;
                int.TryParse(p.QuantityMultiplier, out quantityMultiplier);
                if (quantityMultiplier == 0)
                {
                    p.QuantityMultiplier = "1";
                }

                if (p.OrigPrice == 0)
                {
                    p.OrigPrice = 1;
                }
            }

            if (isMultiDeal)
            {
                //母子檔
                var avgItem = multiDeals.Select(x => new { AvgPrice = Math.Ceiling(x.ItemPrice / Convert.ToInt16(x.QuantityMultiplier)) }).OrderBy(y => y.AvgPrice).FirstOrDefault();
                avgPrice = avgItem.AvgPrice;

                ProposalMultiDeal minProposalMultiDeal = multiDeals.FirstOrDefault();
                foreach (ProposalMultiDeal deal in multiDeals)
                {
                    decimal minDiscounted = 0;
                    if (deal.OrigPrice > 0)
                    {
                        minDiscounted = Math.Round((deal.ItemPrice / deal.OrigPrice) * 10, 1);
                    }

                    if (minDiscounted < discounted)
                    {
                        discounted = minDiscounted;
                        minProposalMultiDeal = deal;
                    }
                }

                itemPrice = Convert.ToInt32(Math.Round(minProposalMultiDeal.ItemPrice / decimal.Parse(minProposalMultiDeal.QuantityMultiplier), 0));
                origPrice = Convert.ToInt32(Math.Round(minProposalMultiDeal.OrigPrice / decimal.Parse(minProposalMultiDeal.QuantityMultiplier), 0));
            }
            else
            {
                //單檔
                if (origPrice > 0)
                {
                    discounted = Convert.ToDecimal(Math.Round(Convert.ToDouble(itemPrice.ToString()) / Convert.ToDouble(origPrice.ToString()) * 10, 1));
                }
                avgPrice = itemPrice;
            }



            #region 黑標
            string eventName = ProposalFacade.GetProEventName(pro, multiDeals);
            #endregion 黑標

            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);

            string imagePath = string.Empty;
            if (pcec.ImagePath != null)
            {
                string[] images = ImageFacade.GetMediaPathsFromRawData(pcec.ImagePath, MediaType.PponDealPhoto);
                imagePath = images.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.Index != 1 && x.Index != 2 && x.ImgPath != string.Empty).Select(x => x.ImgPath).FirstOrDefault();
            }


            ViewBag.Title = pro.BrandName + pro.DealName;
            ViewBag.IsMultiDeal = isMultiDeal;
            ViewBag.EventName = eventName;
            ViewBag.DealContent = pcec.Title;
            ViewBag.ProposalMultiDeal = multiDeals;
            ViewBag.ImagePath = imagePath;
            ViewBag.ItemPrice = itemPrice;
            ViewBag.OrigPrice = origPrice;
            ViewBag.Discounted = discounted;
            ViewBag.AvgPrice = avgPrice;
            ViewBag.ItemName = pcec.AppTitle;
            ViewBag.Restrictions = pcec.Restrictions;

            string description = string.Empty;
            if (pro.Cchannel && !string.IsNullOrEmpty(pro.CchannelLink))
            {
                string result = PponFacade.GetCchannelLink(pro.CchannelLink);
                description += result;
            }
            description += pcec.Description;

            ViewBag.Description = description;  //商品詳細介紹

            ViewBag.ProductSpec = pcec.ProductSpec;  //商品規格
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
            if (specialText != null && specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.InspectRule))
                ViewBag.ProductSpec += "\n" + specialText[(ProposalSpecialFlag)ProposalSpecialFlag.InspectRule];

            ViewBag.Remark = pcec.Remark;    //一姬說好康(必買特色)
            return View();
        }

        #endregion 提案單

        #region 商品資料
        /// <summary>
        /// 商品詳細頁
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="pid"></param>
        /// <param name="did"></param>
        /// <returns></returns>
        public virtual ActionResult ProductOptionContent(Guid sid, Guid? pid, Guid? did)
        {
            Seller seller = sp.SellerGet(sid);
            if (!seller.IsLoaded)
            {
                throw new Exception("賣家資料異常!");
            }
            ProductInfo pi = new ProductInfo();
            List<ProductItem> pdis = new List<ProductItem>();
            List<ProductSpec> specs = new List<ProductSpec>();
            if (pid != null)
            {
                pi = pp.ProductInfoGet(pid.Value);
                if (pi.IsLoaded)
                {
                    pdis = pp.ProductItemListGetByProductGuid(pi.Guid)
                        .Where(t => t.ItemStatus != (int)ProductItemStatus.Deleted).OrderBy(x => x.Sort).ToList();
                    if (pdis.Count > 0)
                    {
                        specs = pp.ProductSpecListGetByItemGuid(pdis.Select(x => x.Guid).ToList()).OrderBy(t => t.Sort).ToList();
                    }
                }
                else
                {
                    //throw new Exception("");
                }
            }
            if (pdis.Count == 0)
            {
                pdis.Add(new ProductItem() { });
            }
            ProductItemContentModel products = new ProductItemContentModel
            {
                Products = pi,
                ProductItems = pdis,
                Specs = specs
            };
            ViewBag.Products = products;
            ViewBag.EditMode = (pid == null ? false : true);
            ViewBag.ProductGuid = pid ?? Guid.Empty;
            ViewBag.ProductDetailGuid = did ?? Guid.Empty;
            ViewBag.VbsSellerGuid = sid;
            ViewBag.IsWms = pi.WarehouseType == (int)WarehouseType.Wms ? true : false;
            ViewBag.IsConfirmWms = WmsFacade.isConfirmWms(sid, string.Empty);
            ViewBag.IsEditProductName = pdis.Where(x => x.PchomeProdId != null).Any() ? false : true;
            return View();
        }
        public virtual ActionResult ExportProductFailList(Guid gid)
        {
            HSSFWorkbook workbook;
            using (FileStream file = new FileStream(Server.MapPath(@"~/template/SalesDocs/商品匯入表格範本.xls"), FileMode.Open, FileAccess.Read))
            {
                workbook = new HSSFWorkbook(file);
            }

            Sheet sheet = workbook.GetSheetAt(0);


            ProductInfoImportLogCollection piils = pp.ProductInfoImportLogListGet(gid);
            int rowIdx = 4;
            foreach (ProductInfoImportLog piil in piils)
            {
                Row cols = sheet.CreateRow(rowIdx);
                cols.CreateCell(0).SetCellValue(piil.GroupId);          //群組編號
                cols.CreateCell(1).SetCellValue(piil.SpecType);         //規格類型
                cols.CreateCell(2).SetCellValue(piil.ProductBrandName); //品牌名稱
                cols.CreateCell(3).SetCellValue(piil.ProductName);      //商品名稱
                cols.CreateCell(4).SetCellValue(piil.CatgName);         //規格名稱
                cols.CreateCell(5).SetCellValue(piil.SpecName);         //規格內容
                cols.CreateCell(6).SetCellValue(piil.Gtins);            //GTINs
                cols.CreateCell(7).SetCellValue(piil.ProductCode);      //貨號
                cols.CreateCell(8).SetCellValue(piil.Stock);            //庫存數
                cols.CreateCell(9).SetCellValue(piil.SaftyStock);       //安全庫存數
                cols.CreateCell(10).SetCellValue(piil.Mpn);             //mpn
                cols.CreateCell(11).SetCellValue(piil.WarehouseType);   //使用類型
                cols.CreateCell(12).SetCellValue(piil.Price);           //保存價值
                cols.CreateCell(13).SetCellValue(piil.ShelfLife);       //有效天數
                cols.CreateCell(14).SetCellValue(piil.Memo);            //備註
                rowIdx++;
            }

            using (MemoryStream MyMemoryStream = new MemoryStream())
            {
                workbook.Write(MyMemoryStream);
                MyMemoryStream.WriteTo(Response.OutputStream);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}.xls", DateTime.Now.ToString("yyyyMMddHHmmssfff")));
                Response.ContentType = "application/ms-excel";
                Response.Flush();
                Response.End();
            }

            workbook.Dispose();

            return null;
        }
        #endregion 商品資料

        #endregion action

        #region web method

        #endregion web method

        #region private method
        protected ActionResult DealPauseSet(int pid, bool pause)
        {
            Proposal pro = sp.ProposalGet(pid);
            if (pro != null && pro.IsLoaded)
            {
                if (pro.BusinessHourGuid != null)
                {
                    string action = pause ? "暫停銷售" : "上架銷售";

                    DealTimeSlotCollection dts = pp.DealTimeSlotGetAllCol(pro.BusinessHourGuid.Value);
                    var data = dts.Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.EffectiveEnd >= DateTime.Now);
                    if (pause)
                    {
                        #region 暫停銷售
                        if (data.Count() == 0)
                        {
                            return Json(new
                            {
                                IsSuccess = false,
                                Message = "目前不在架上"
                            });
                        }
                        else
                        {
                            //檔次隱藏
                            var dataStart = data.Select(x => x.EffectiveStart).Min();
                            IEnumerable<int> a = data.Select(x => x.CityId).Distinct();
                            foreach (var cityId in data.Select(x => x.CityId).Distinct())
                            {
                                OrderFacade.SetStatusToDealTimeSlotData(pro.BusinessHourGuid.Value, cityId, dataStart, DealTimeSlotStatus.NotShowInPponDefault, true);
                            }
                            //不顯示於Web
                            BusinessHour bh = pp.BusinessHourGet(pro.BusinessHourGuid.Value);
                            if (bh.IsLoaded)
                            {
                                bh.BusinessHourStatus = (int)Helper.SetFlag(true, bh.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                                pp.BusinessHourSet(bh);

                                ComboDealCollection cdc = pp.GetComboDealByBid(bh.Guid, false);
                                foreach (ComboDeal cd in cdc)
                                {
                                    BusinessHour bhc = pp.BusinessHourGet(cd.BusinessHourGuid);
                                    if (bhc.IsLoaded)
                                    {
                                        bhc.BusinessHourStatus = (int)Helper.SetFlag(true, bhc.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                                        pp.BusinessHourSet(bhc);
                                    }
                                }
                            }

                            dts = pp.DealTimeSlotGetAllCol(pro.BusinessHourGuid.Value);
                            data = dts.Where(x => x.EffectiveEnd >= DateTime.Now);
                            IEnumerable<string> b = data.Select(x => x.CityId + "-" + x.Status.ToString()).Distinct();
                            IEnumerable<string> c = data.Select(x => x.EffectiveStart + "-" + x.EffectiveEnd + "-" + x.CityId + "-" + x.Status.ToString()).Distinct();
                        }
                        #endregion 暫停銷售
                    }
                    else
                    {
                        #region 繼續銷售
                        //檔次隱藏
                        var d = dts.Where(y => y.EffectiveEnd >= DateTime.Now);
                        var dataStart = d.Select(x => x.EffectiveStart).Min();
                        IEnumerable<int> a = d.Select(x => x.CityId).Distinct();
                        foreach (var cityId in d.Select(x => x.CityId).Distinct())
                        {
                            OrderFacade.SetStatusToDealTimeSlotData(pro.BusinessHourGuid.Value, cityId, dataStart, DealTimeSlotStatus.Default, true);
                        }
                        //不顯示於Web
                        BusinessHour bh = pp.BusinessHourGet(pro.BusinessHourGuid.Value);
                        if (bh.IsLoaded)
                        {
                            bh.BusinessHourStatus = (int)Helper.SetFlag(false, bh.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                            pp.BusinessHourSet(bh);

                            try
                            {
                                //先隱藏檔次，再新增子檔檔次時，子檔的不顯示於Web會被勾起，故再次上架時，需連子檔一併打開
                                ViewComboDealCollection vcdc = pp.GetViewComboDealByBid(bh.Guid, false);
                                foreach (ViewComboDeal vcd in vcdc)
                                {
                                    if (vcd.Slug != null)
                                    {
                                        continue;
                                    }
                                    BusinessHour bhd = pp.BusinessHourGet(vcd.BusinessHourGuid);
                                    if (bhd != null && bhd.IsLoaded)
                                    {
                                        if (Helper.IsFlagSet(bhd.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
                                        {
                                            bhd.BusinessHourStatus = (int)Helper.SetFlag(false, bhd.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb);
                                            pp.BusinessHourSet(bhd);
                                        }
                                    }
                                }
                            }
                            catch { }
                        }

                        dts = pp.DealTimeSlotGetAllCol(pro.BusinessHourGuid.Value);
                        d = dts.Where(y => y.EffectiveEnd >= DateTime.Now);
                        IEnumerable<string> b = d.Select(x => x.CityId + "-" + x.Status.ToString()).Distinct();
                        IEnumerable<string> c = d.Select(x => x.EffectiveStart + "-" + x.EffectiveEnd + "-" + x.CityId + "-" + x.Status.ToString()).Distinct();
                        #endregion 繼續銷售
                    }

                    ProposalFacade.ProposalLog(pid, string.Format("[{0}] ", action), UserName, ProposalLogType.Initial);
                }
            }
            return Json(new
            {
                IsSuccess = true,
                Message = string.Empty
            });
        }
        protected ActionResult ProductBusinessHourGet(int pro_no)
        {
            ProductItem pitem = pp.ProductItemGetByNo(pro_no);
            List<int> ids = sp.ProposalMultiOptionSpecGetByItem(pitem.Guid).Select(x => x.MultiOptionId).Distinct().ToList();
            List<int> pids = sp.ProposalMultiDealGetPidListById(ids).Distinct().ToList();


            List<object> objs = new List<object>();
            foreach (int pid in pids)
            {
                Proposal pro = sp.ProposalGet(pid);
                Guid bid = Guid.Empty;
                if (pro.BusinessHourGuid != null)
                {
                    bid = pro.BusinessHourGuid.Value;
                }

                if (bid != Guid.Empty)
                {
                    string dealStatus = string.Empty;
                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                    if (Helper.IsFlagSet(vpd.GroupOrderStatus.GetValueOrDefault(0), GroupOrderStatus.Completed))
                    {
                        dealStatus = "結束銷售";
                    }
                    else
                    {
                        DealTimeSlotCollection dts = pp.DealTimeSlotGetAllCol(vpd.BusinessHourGuid);
                        var online = dts.Where(x => x.Status == (int)DealTimeSlotStatus.Default && x.EffectiveStart <= DateTime.Now && x.EffectiveEnd >= DateTime.Now).Count() > 0;

                        if (online)
                        {
                            dealStatus = "銷售中";
                        }
                        else
                        {
                            dealStatus = "暫停銷售";
                            if (!vpd.BusinessHourDeliverTimeS.HasValue)
                            {
                                dealStatus = "預備中";
                            }
                            else
                            {
                                if (vpd.BusinessHourOrderTimeE < DateTime.Now)
                                {
                                    dealStatus = "結束銷售";
                                }
                                else
                                {
                                    if (vpd.BusinessHourOrderTimeS > DateTime.Now)
                                    {
                                        dealStatus = "預備中";
                                    }
                                }
                            }
                        }
                    }

                    objs.Add(new
                    {
                        UniqueId = vpd.UniqueId.ToString(),
                        Bid = bid.ToString(),
                        Pid = pid,
                        DealName = (!string.IsNullOrEmpty(vpd.EventName) ? vpd.EventName : string.Empty),
                        DealStatus = dealStatus
                    });
                }
                else
                {
                    objs.Add(new
                    {
                        UniqueId = string.Empty,
                        Bid = string.Empty,
                        Pid = pid,
                        DealName = string.Empty,
                        DealStatus = "預備中"
                    });
                }

            }

            return Json(objs);
        }
        protected Dictionary<int, string> GetStockFilter()
        {
            // 特殊標記
            Dictionary<int, string> stockFilter = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProductStockFilter)))
            {
                stockFilter[(int)item] = Helper.GetDescription((ProductStockFilter)item);
            }
            return stockFilter;
        }
        protected ActionResult UploadBaseProposalDealSource(int pid)
        {
            bool flag = false;
            List<string> ZipExtension = new List<string>
            {
                ".zip",".rar",".7z",".tar"
            };

            List<string> DenyFiles = new List<string>
            {
                ".exe"
            };

            string ProposalId = pid.ToString().PadLeft(7, '0');

            try
            {
                StringBuilder sb = new StringBuilder();
                Proposal pro = sp.ProposalGet(pid);
                if (pro != null && pro.IsLoaded)
                {
                    Seller seller = sp.SellerGet(pro.SellerGuid);

                    string FolderFile = ""; //單號-商家名稱-品牌名稱
                    if (pro.IsLoaded && pro != null)
                    {
                        for (int i = 0; i < Request.Files.Count; i++)
                        {
                            HttpPostedFileBase file = Request.Files[i];
                            string oriFileName = Request.Files[i].FileName;
                            string _Extension = oriFileName.Substring(oriFileName.LastIndexOf("."));
                            string realFileName = DateTime.Now.ToString("yyyyMMddHHmmss_") + oriFileName;

                            if (_config.SellerContractIsFtp)
                            {
                                //FTP
                                string ftpFolder = "Seller_Files/" + ProposalId;
                                SimpleFtpClient cli = new SimpleFtpClient(new Uri(_config.SellerContractFtpUri + ftpFolder));
                                FolderFile = ftpFolder + "/" + realFileName;
                                cli.MakeDirectory(ftpFolder);
                                cli.SimpleUpload(file, ftpFolder, realFileName);
                                flag = true;
                            }
                            else
                            {
                                //UNC
                                string BaseDir = string.IsNullOrEmpty(_config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : _config.CkFinderBaseDir;
                                string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ProposalId);

                                if (!System.IO.Directory.Exists(Dir))
                                {
                                    System.IO.Directory.CreateDirectory(Dir);
                                }
                                string fname = System.IO.Path.Combine(Dir, realFileName);

                                if (DenyFiles.Contains(_Extension))
                                {
                                    continue;
                                }
                                file.SaveAs(fname);
                                flag = true;
                            }

                            //寫入資料庫
                            if (flag)
                            {
                                ProposalSellerFile sellerFile = new ProposalSellerFile()
                                {
                                    ProposalId = pro.Id,
                                    OriFileName = oriFileName,
                                    RealFileName = realFileName,
                                    CreateId = UserName,
                                    CreateTime = DateTime.Now,
                                    FileStatus = (int)ProposalFileStatus.Nomal,
                                    FileSize = unchecked((int)file.InputStream.Length)
                                };
                                sp.ProposalSellerFileSet(sellerFile);

                                //寫入log
                                ProposalFacade.ProposalLog(pro.Id, "[新增檔案]：" + oriFileName + "", UserName, ProposalLogType.Initial);
                            }
                        }
                    }
                }
            }
            catch
            {

            }
            finally
            {

            }

            var rtns = GetBaseProposalDealSource(pid);
            return Json(rtns);
        }

        protected ActionResult DownloadBaseProposalDealSource(int pid, int id)
        {
            ProposalSellerFile files = sp.ProposalSellerFileGet(id);
            if (files.IsLoaded)
            {
                if (files.ProposalId == pid)
                {
                    string ftpFolder = "Seller_Files/" + files.ProposalId.ToString().PadLeft(7, '0') + "/" + files.RealFileName;
                    string FileName = files.OriFileName;

                    if (_config.SellerContractIsFtp)
                    {
                        //Use FTP
                        SimpleFtpClient cli = new SimpleFtpClient(new Uri(_config.SellerContractFtpUri + ftpFolder));
                        cli.SimpleDownload(FileName, ftpFolder);
                    }
                    else
                    {
                        //Use Folder
                        string BaseDir = string.IsNullOrEmpty(_config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : _config.CkFinderBaseDir;
                        string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, files.ProposalId.ToString().PadLeft(7, '0'));

                        System.Net.WebClient wc = new System.Net.WebClient();
                        byte[] byteFiles = null;
                        string FilePath = System.IO.Path.Combine(Dir, files.RealFileName);
                        byteFiles = wc.DownloadData(FilePath);
                        string fileName = System.IO.Path.GetFileName(FilePath);
                        Response.AddHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(fileName));
                        Response.ContentType = "application/octet-stream";
                        Response.BinaryWrite(byteFiles);
                        Response.End();
                    }
                }
            }
            return null;
        }
        protected ActionResult DeleteBaseProposalDealSource(int pid, int id)
        {
            ProposalSellerFile files = sp.ProposalSellerFileGet(id);
            if (files.IsLoaded)
            {
                if (files.ProposalId == pid)
                {
                    ProposalFacade.ProposalSellerFileDelete(id, UserName, ProposalLogType.Initial);
                }
            }
            return GetBaseProposalDealSource(pid);
        }
        protected ActionResult GetBaseProposalDealSource(int pid)
        {
            List<ProposalSellerFile> fileList = new List<ProposalSellerFile>();
            fileList = sp.ProposalSellerFileListGet(pid).ToList();
            List<object> rtns = new List<object>();
            foreach (var f in fileList)
            {
                rtns.Add(new
                {
                    Id = f.Id,
                    OriFileName = f.OriFileName,
                    RealFileName = f.RealFileName,
                    CreateTime = f.CreateTime.ToString("yyyy/MM/dd HH:mm:ss")
                });
            }
            return Json(rtns);
        }
        protected ActionResult GetBaseProposalMessage(int pageStart, int pageLength, int pid)
        {
            List<int> logTypes = new List<int>()
            {
                (int)ProposalLogType.Message
            };

            var plcs = sp.ProposalLogGetList(pid, logTypes).OrderByDescending(x => x.CreateTime);
            int totalCount = plcs.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var logs = plcs.Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(y => new
            {
                CreateId = y.CreateId,
                ChangeLog = y.ChangeLog,
                CreateTime = y.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                Dept = y.Dept ?? string.Empty,
                VbsRead = y.VbsRead,
                SaleRead = y.SaleRead
            });

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = logs,
                VbsRead = (plcs.Where(x => x.VbsRead.Equals(false)).Count() > 0),
                SaleRead = (plcs.Where(x => x.SaleRead.Equals(false)).Count() > 0),
            };

            return Json(obj);
        }
        protected ActionResult ReadBaseProposalMessage(int pid, bool isVbs)
        {
            if (!(Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True"))
            {
                List<int> logTypes = new List<int>()
                {
                    (int)ProposalLogType.Message
                };
                var logs = sp.ProposalLogGetList(pid, logTypes).OrderByDescending(x => x.CreateTime);

                foreach (ProposalLog log in logs)
                {
                    if (isVbs)
                    {
                        log.VbsRead = true;
                    }
                    else
                    {
                        log.SaleRead = true;
                    }
                    sp.ProposalLogSet(log);
                }
            }

            return Json(true);
        }

        /// <summary>
        /// 顯示業務&商家系統異動記錄
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        protected ActionResult GetBaseProposalLogs(int pageStart, int pageLength, int pid)
        {
            List<int> logTypes = new List<int>()
            {
                (int)ProposalLogType.Initial,
                (int)ProposalLogType.Seller,
            };

            var plcs = sp.ProposalLogGetList(pid, logTypes).OrderByDescending(x => x.CreateTime);
            int totalCount = plcs.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var logs = plcs.Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(y => new
            {
                CreateId = y.CreateId,
                ChangeLog = y.ChangeLog,
                CreateTime = y.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                Dept = y.Dept ?? string.Empty
            });

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart,
                Data = logs
            };

            return Json(obj);
        }
        protected bool LeaveMessageSet(int pid, string message, string dept)
        {
            bool flag = false;
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                ProposalFacade.ProposalLog(pid, message, UserName, ProposalLogType.Message);
            }
            else
            {
                flag = false;
            }
            return flag;
        }
        /// <summary>
        /// 檢查商品是否已無庫存
        /// </summary>
        /// <returns></returns>
        protected bool CheckMultiOptionSpecStock(int pid, out string message)
        {
            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pid);
            foreach (ProposalMultiDeal pmd in pmds)
            {
                ProposalMultiOptionSpecCollection specs = sp.ProposalMultiOptionSpecGetByMid(pmd.Id);
                foreach (ProposalMultiOptionSpec spec in specs)
                {
                    ProductItem item = pp.ProductItemGet(spec.ItemGuid);
                    ProductInfo product = pp.ProductInfoGet(item.InfoGuid);
                    string brandName = string.Empty;
                    if (product.IsLoaded)
                    {
                        brandName = product.ProductBrandName + product.ProductName;
                    }
                    if (item.IsLoaded)
                    {
                        //檢查規格不能為停用
                        if (item.ItemStatus != (int)ProductItemStatus.Normal)
                        {
                            message = string.Format("{0}已停用", brandName + item.SpecName);
                            return false;
                        }
                        //檢查庫存不能為0
                        if (item.Stock <= 0)
                        {
                            //改為提示
                            //message = string.Format("{0}已無庫存", brandName + item.SpecName);
                            //return false;
                        }
                    }
                    else
                    {
                        message = string.Format("{0}不存在", spec.ItemGuid);
                        return false;
                    }
                }
            }
            message = string.Empty;
            return true;
        }

        protected bool CheckMultiOptionSpecStockForCreateProposal(int pid, out string message)
        {
            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pid);
            foreach (ProposalMultiDeal pmd in pmds)
            {
                ProposalMultiOptionSpecCollection specs = sp.ProposalMultiOptionSpecGetByMid(pmd.Id);
                foreach (ProposalMultiOptionSpec spec in specs)
                {
                    ProductItem item = pp.ProductItemGet(spec.ItemGuid);
                    ProductInfo product = pp.ProductInfoGet(item.InfoGuid);
                    string brandName = string.Empty;
                    if (product.IsLoaded)
                    {
                        brandName = product.ProductBrandName + product.ProductName;
                    }
                    if (item.IsLoaded)
                    {
                        //檢查規格不能為停用
                        if (item.ItemStatus != (int)ProductItemStatus.Normal)
                        {
                            message = string.Format("{0}已停用", brandName + item.SpecName);
                            return false;
                        }
                        //檢查庫存不能為0
                        if (item.Stock <= 0)
                        {
                            message = string.Format("{0}已無庫存", brandName + item.SpecName);
                            return false;
                        }
                    }
                    else
                    {
                        message = string.Format("{0}不存在", spec.ItemGuid);
                        return false;
                    }
                }
            }
            message = string.Empty;
            return true;
        }

        private List<string> GetPponDealType()
        {
            List<string> pponDealType = new List<string>();
            pponDealType.Add("1001"); //餐廳
            pponDealType.Add("1009"); //SPA/醫美
            pponDealType.Add("1010"); //住宿/門票
            pponDealType.Add("1011"); //課程

            return pponDealType;
        }
        private Dictionary<int, string> GetDeliveryMethod()
        {
            //配送溫層
            Dictionary<int, string> deliveryMethod = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDeliveryMethod)))
            {
                deliveryMethod.Add((int)item, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDeliveryMethod)item));
            }
            return deliveryMethod;
        }
        private Dictionary<int, string> GetDealSource()
        {
            Dictionary<int, string> dealSource = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(ProposalDealSource)))
            {
                dealSource.Add((int)item, Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealSource)item));
            }
            return dealSource;
        }
        protected string CheckHouseProposalSave(SellerProposalHouseModel model, Seller seller)
        {
            if (seller == null || !seller.IsLoaded)
            {
                return "賣家不存在!";
            }
            if (!string.IsNullOrEmpty(model.BusinessHourOrderTimeS))
            {
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(model.BusinessHourOrderTimeS, out dt);
                if (dt.Year == DateTime.MinValue.Year)
                {
                    return "起始日期異常!";
                }
                if (string.IsNullOrEmpty(model.BusinessHourOrderTimeE))
                {
                    return "請輸入結束日期!";
                }
            }
            if (!string.IsNullOrEmpty(model.BusinessHourOrderTimeE))
            {
                DateTime dt = DateTime.MinValue;
                DateTime.TryParse(model.BusinessHourOrderTimeE, out dt);
                if (dt.Year == DateTime.MinValue.Year)
                {
                    return "結束日期異常!";
                }
                if (string.IsNullOrEmpty(model.BusinessHourOrderTimeS))
                {
                    return "請輸入起始日期!";
                }
            }
            if (!string.IsNullOrEmpty(model.BusinessHourOrderTimeS) && !string.IsNullOrEmpty(model.BusinessHourOrderTimeE))
            {
                DateTime dtS = DateTime.Parse(model.BusinessHourOrderTimeS);
                DateTime dtE = DateTime.Parse(model.BusinessHourOrderTimeE);
                if (dtE < dtS)
                {
                    return "結束日期需大於起始日期!";
                }
            }

            if (!string.IsNullOrEmpty(model.BusinessHourDeliveryTimeS) && !string.IsNullOrEmpty(model.BusinessHourDeliveryTimeE))
            {
                DateTime deliveryTimeS = DateTime.Parse(model.BusinessHourDeliveryTimeS);
                DateTime deliveryTimeE = DateTime.Parse(model.BusinessHourDeliveryTimeE);
                if (deliveryTimeE < deliveryTimeS)
                {
                    return "配送結束日期需大於配送起始日期!";
                }
            }
            if (model.ProShipType == 2)
            {
                //選其他
                if (!string.IsNullOrEmpty(model.BusinessHourOrderTimeE) && !string.IsNullOrEmpty(model.BusinessHourDeliveryTimeE))
                {
                    DateTime dtE = DateTime.Parse(model.BusinessHourOrderTimeE);
                    DateTime deliveryTimeE = DateTime.Parse(model.BusinessHourDeliveryTimeE);

                    if (dtE > deliveryTimeE)
                        return "「檔期結束日」不可大於等於「最晚出貨日」";

                }
            }
            if (!string.IsNullOrEmpty(model.PicAlt))
            {
                if (model.PicAlt.Length > 100)
                {
                    return "關鍵字請勿超過100字元!";
                }
            }

            List<ProposalMultiDealModel> multiDeals = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(model.MultiDeals);
            if (multiDeals.Count == 0)
            {
                //return "至少填寫一項銷售方案";
            }
            else
            {
                string dealMsg = string.Empty;
                int idx = 1;
                foreach (ProposalMultiDealModel deal in multiDeals)
                {
                    bool isMulti = ProposalFacade.IsProposalMultiSpec(deal.Options);
                    if (!isMulti)
                    {
                        if (deal.ComboPackCount != "1")
                        {
                            return "單規格商品，結帳倍數請填入1";
                        }
                    }
                    int i;
                    if (deal.OrigPrice == 0 || !Regex.IsMatch(deal.OrigPrice.ToString(), "^[0-9]*[1-9][0-9]*$"))
                    {
                        return "巿價不可為0或非正整數";
                    }
                    if (deal.ItemPrice == 0 || !Regex.IsMatch(deal.ItemPrice.ToString(), "^[0-9]*[1-9][0-9]*$"))
                    {
                        return "建議售價不可為0或非正整數";
                    }
                    if (deal.Cost == 0 || !Regex.IsMatch(deal.Cost.ToString(), "^[0-9]*[1-9][0-9]*$"))
                    {
                        return "進貨價不可為0或非正整數";
                    }
                    if (!Regex.IsMatch(deal.OrderMaxPersonal.ToString(), "^\\d+$"))
                    {
                        return "每人限購不可為非正整數";
                    }
                    idx++;
                }
            }
            //網站露出
            if (string.IsNullOrEmpty(model.AppTitle))
            {
                return "請填寫網站露出商品名稱。";
            }
            //網站露出(中文*2+英數*1)
            if (System.Text.Encoding.Default.GetBytes(model.AppTitle.ToCharArray()).Length > 36)
            {
                return "網站露出商品名稱不得大於18中文字。";
            }
            if (System.Text.Encoding.Default.GetBytes(model.BrandName.ToCharArray()).Length > 24)
            {
                return "品牌名稱不得大於12中文字。";
            }
            if (System.Text.Encoding.Default.GetBytes(model.DealName.ToCharArray()).Length > 80)
            {
                return "檔次名稱不得大於40中文字。";
            }
            if ((model.BrandName.Length + model.DealName.Length) > 50)
            {
                return "商品名稱總字數不得大於50字。";
            }
            return null;
        }

        /// <summary>
        /// 商家區塊儲存
        /// </summary>
        /// <param name="model"></param>
        /// <param name="seller"></param>
        /// <param name="modifyId"></param>
        /// <param name="multiDeals"></param>
        /// <returns></returns>
        protected string HouseProposalContentVbsSave(SellerProposalHouseModel model, Seller seller, string modifyId,
            out List<ProposalMultiDealModel> multiDeals)
        {
            int pid = model.ProId;
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                #region 處理圖片
                string pcImagePath = string.Empty;
                string pcOriImagePath = string.Empty;
                string appImagePath = string.Empty;
                string removeBGImagePath = string.Empty;

                int imagePcCnt = 0;
                int.TryParse(model.ImagePcCnt, out imagePcCnt);
                if (imagePcCnt > 0)
                {
                    //檔案有變更才要上傳
                    if (Request.Files["ImagePc"] != null)
                    {
                        var file = Request.Files["ImagePc"];
                        var img = Image.FromStream(file.InputStream, true, true);
                        if (img.Width > 500)
                        {
                            multiDeals = new List<ProposalMultiDealModel>();
                            return "PC圖片大小不符合";
                        }
                        pcImagePath = Component.ImageUtility.UploadPponImageAndGetNewPath(file, seller.SellerId, pro.BusinessHourGuid.ToString(), string.Empty, true, true, false);
                        pcImagePath = Component.ImageUtility.UploadPponImageAndGetNewPath(file, seller.SellerId, pro.BusinessHourGuid.ToString(), pcImagePath, true, true, true);
                        string tmpOriImagePath = Component.ImageUtility.UploadPponImageAndGetNewPath(file, seller.SellerId, pro.BusinessHourGuid.ToString(), pcImagePath, false, false, false);
                        if (!string.IsNullOrEmpty(tmpOriImagePath))
                        {
                            pcOriImagePath = tmpOriImagePath.Substring(tmpOriImagePath.LastIndexOf("||") + 2);
                        }
                    }
                }
                int imageAppCnt = 0;
                int.TryParse(model.ImageAppCnt, out imageAppCnt);
                if (imageAppCnt > 0)
                {
                    if (Request.Files["ImageApp"] != null)
                    {
                        var file = Request.Files["ImageApp"];

                        var img = Image.FromStream(file.InputStream, true, true);
                        if (img.Width > 600)
                        {
                            multiDeals = new List<ProposalMultiDealModel>();
                            string logImgPath = Path.Combine(ProviderFactory.Instance().GetConfig().WebTempPath,
                                "checkImageSize." + Helper.GetExtensionByContentType(file.ContentType));
                            try
                            {
                                file.SaveAs(logImgPath);
                            } catch
                            {

                            }
                            return "APP圖片大小不符合, 偵測到的尺寸為 (" + img.Width + ", " + img.Height + "), 暫存到 " + logImgPath;
                        }
                        string baseDirectoryPath = Path.Combine(Path.Combine(Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/media/"), seller.SellerId)), "source");
                        string appDealPicImagePath = Path.Combine(baseDirectoryPath, Guid.NewGuid().ToString() + "." + Helper.GetExtensionByContentType(file.ContentType));

                        if (!Directory.Exists(baseDirectoryPath))
                        {
                            Directory.CreateDirectory(baseDirectoryPath);
                        }

                        file.SaveAs(appDealPicImagePath);

                        if (ImageFacade.GetImageSizeFast(appDealPicImagePath).X > CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM)
                        {
                            ImageFacade.CropImage(appDealPicImagePath, appDealPicImagePath, new Rectangle
                            {
                                X = 0,
                                Y = 0,
                                Size = new Size(CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM,
                                            CouponEventContent._APP_DEAL_PIC_MAX_PIXELS_MINIMUM)
                            });
                        }

                        Component.ImageUtility.UploadFile(new PhysicalPostedFileAdapter(appDealPicImagePath), UploadFileType.PponEvent, seller.SellerId,
                        Path.GetFileNameWithoutExtension(appDealPicImagePath), false);
                        appImagePath = string.Format("{0}?ts={1}",
                            ImageFacade.GenerateMediaPath(seller.SellerId, Path.GetFileName(appDealPicImagePath)),
                            Helper.GetCurrentTimeStampHashCode());
                        System.IO.File.Delete(appDealPicImagePath);
                    }
                }
                int imageRemoveBGCnt = 0;
                int.TryParse(model.ImageRemoveBGCnt, out imageRemoveBGCnt);
                if (imageRemoveBGCnt > 0)
                {
                    //檔案有變更才要上傳
                    if (Request.Files["ImageRemoveBG"] != null)
                    {
                        var file = Request.Files["ImageRemoveBG"];
                        var img = Image.FromStream(file.InputStream, true, true);
                        if (img.Width < 500 || img.Height < 500 || img.Width != img.Height)
                        {
                            multiDeals = new List<ProposalMultiDealModel>();
                            return "Google廣告去背圖大小不符合";
                        }
                        removeBGImagePath = Component.ImageUtility.UploadRemoveBGImageAndGetNewPath(file, seller.SellerId, false);

                    }
                }
                #endregion 處理圖片


                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    Proposal oriPro = pro.Clone();
                    if (pro.BusinessHourGuid == null)
                    {
                        pro.BusinessHourGuid = Guid.NewGuid();
                    }
                    pro.OperationSalesId = model.OpSalesId;
                    if (pro.DevelopeSalesId == 0)
                    {
                        pro.DevelopeSalesId = model.OpSalesId;
                    }
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Saved));
                    if (pro.DealType1 != model.DealType1)
                        pro.DealType2 = 0;//若有更改，商品分線要歸0
                    pro.DealType1 = model.DealType1;     //商品類型
                    pro.BrandName = ProposalFacade.RemoveSpecialCharacter(model.BrandName.Trim());     //品牌名稱
                    pro.DealName = ProposalFacade.RemoveSpecialCharacter(model.DealName);       //檔次名稱
                    pro.VendorReceiptType = model.VendorReceiptType;    //請款單據
                    if (model.VendorReceiptType == (int)VendorReceiptType.Other)
                    {
                        pro.Othermessage = model.AccountingMessage; //其他請款原因
                    }
                    if (model.VendorReceiptType == ((int)VendorReceiptType.NoTaxInvoice))
                    {
                        //免稅
                        pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SpecialFlag, ProposalSpecialFlag.FreeTax));
                    }
                    else
                    {
                        pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.FreeTax));
                    }
                    if (model.ProShipType == 1)
                    {
                        //有選擇出貨天數
                        if (model.ProShipDays == "1")
                        {
                            //快速出貨
                            pro.ShipType = (int)DealShipType.Ship72Hrs;
                            pro.ShipText1 = string.Empty;
                            pro.ShipText2 = string.Empty;
                            pro.ShipText3 = string.Empty;
                            pro.ShipOther = string.Empty;
                        }
                        else
                        {
                            //一般出貨
                            pro.ShipType = (int)DealShipType.Normal;
                            pro.ShipText1 = string.Empty;
                            pro.ShipText2 = string.Empty;
                            pro.ShipText3 = model.ProShipDays;
                            pro.ShipOther = string.Empty;
                            pro.ShippingdateType = (int)DealShippingDateType.Normal;
                        }
                    }
                    else if (model.ProShipType == 2)
                    {
                        //其他出貨方式
                        pro.ShipType = (int)DealShipType.Other;
                        pro.ShipText1 = string.Empty;
                        pro.ShipText2 = string.Empty;
                        pro.ShipText3 = string.Empty;
                        pro.ShipOther = string.Empty;
                    }
                    else if (model.ProShipType == 3)
                    {
                        //24到貨
                        pro.ShipType = (int)DealShipType.Wms;
                        pro.ShipText1 = string.Empty;
                        pro.ShipText2 = string.Empty;
                        pro.ShipText3 = string.Empty;
                        pro.ShipOther = string.Empty;

                    }
                    if (_config.IsRemittanceFortnightly)
                    {
                        pro.RemittanceType = model.RemittanceType;
                    }
                    pro.DeliveryMethod = model.DeliveryMethod;  //配送溫層
                    pro.DealSource = model.DealSource;  //商品來源

                    if (model.AgentChannels.EndsWith(","))
                    {
                        model.AgentChannels = model.AgentChannels.Substring(0, model.AgentChannels.Length - 1);
                    }
                    pro.AgentChannels = model.AgentChannels;

                    DateTime businessHourOrderTimeS = DateTime.MinValue;
                    DateTime businessHourOrderTimeE = DateTime.MinValue;
                    DateTime BusinessHourDeliveryTimeS = DateTime.MinValue;
                    DateTime BusinessHourDeliveryTimeE = DateTime.MinValue;

                    DateTime.TryParse(model.BusinessHourOrderTimeS, out businessHourOrderTimeS);
                    DateTime.TryParse(model.BusinessHourOrderTimeE, out businessHourOrderTimeE);
                    DateTime.TryParse(model.BusinessHourDeliveryTimeS, out BusinessHourDeliveryTimeS);
                    DateTime.TryParse(model.BusinessHourDeliveryTimeE, out BusinessHourDeliveryTimeE);

                    if (businessHourOrderTimeS != DateTime.MinValue && businessHourOrderTimeE != DateTime.MinValue)
                    {
                        pro.OrderTimeS = businessHourOrderTimeS;
                        pro.OrderTimeE = businessHourOrderTimeE;
                    }

                    if (BusinessHourDeliveryTimeS != DateTime.MinValue && BusinessHourDeliveryTimeE != DateTime.MinValue)
                    {
                        //有指定配送日期
                        pro.DeliveryTimeS = BusinessHourDeliveryTimeS;
                        pro.DeliveryTimeE = BusinessHourDeliveryTimeE;
                    }
                    else
                    {
                        //配送日期系統算出
                        pro.DeliveryTimeS = null;
                        pro.DeliveryTimeE = null;
                    }

                    Dictionary<ProposalSpecialFlag, string> specialText = new Dictionary<ProposalSpecialFlag, string>();

                    if (!string.IsNullOrEmpty(model.InspectRule))
                    {
                        specialText.Add(ProposalSpecialFlag.InspectRule, System.Web.HttpUtility.HtmlDecode(model.InspectRule));
                        pro.SpecialFlagText = new JsonSerializer().Serialize(specialText);
                    }
                    else
                    {
                        pro.SpecialFlagText = string.Empty;
                    }
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(!string.IsNullOrEmpty(model.InspectRule), pro.SpecialFlag, ProposalSpecialFlag.InspectRule));


                    pro.ModifyId = modifyId;
                    pro.ModifyTime = DateTime.Now;


                    #region MultiDeals
                    ProposalMultiDealCollection pmdc = sp.ProposalMultiDealGetByPid(pro.Id);
                    ProposalMultiDealCollection oriPmdc = pmdc.Clone();


                    ProposalFacade.GetProposalMultiDeal(pro.Id, model.MultiDeals, pmdc, modifyId);
                    sp.ProposalMultiDealSetList(pmdc);

                    #region proposal_multi_option_spec

                    foreach (ProposalMultiDeal pmd in pmdc)
                    {
                        ProposalMultiOptionSpecCollection options = sp.ProposalMultiOptionSpecGetByMid(pmd.Id);
                        List<ProposalMultiDealsSpec> pmdm = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options);

                        #region 刪除不存在的資料
                        foreach (ProposalMultiOptionSpec option in options)
                        {
                            var res = pmdm.Where(t => t.Sort == option.GroupId)
                                .SelectMany(x => x.Items)
                                .Where(y => y.product_guid == option.ProductGuid && y.item_guid == option.ItemGuid).FirstOrDefault();
                            if (res == null)
                            {
                                sp.ProposalMultiOptionSpecDelete(option.MultiOptionId, option.ProductGuid, option.ItemGuid);
                            }
                        }
                        #endregion 刪除不存在的資料

                        foreach (ProposalMultiDealsSpec md in pmdm)
                        {
                            int group_id = md.Sort;
                            int sidx = 1;
                            foreach (ProposalMultiDealsItem pmi in md.Items)
                            {
                                ProposalMultiOptionSpec spec = options.Where(x => x.ProductGuid == pmi.product_guid && x.ItemGuid == pmi.item_guid && x.MultiOptionId == pmd.Id && x.GroupId == group_id).FirstOrDefault();
                                if (spec == null)
                                {
                                    spec = new ProposalMultiOptionSpec();
                                }
                                spec.MultiOptionId = pmd.Id;
                                spec.ProductGuid = pmi.product_guid;
                                spec.ItemGuid = pmi.item_guid;
                                spec.GroupId = group_id;
                                spec.Sort = sidx;
                                spec.BusinessHourGuid = pro.BusinessHourGuid.Value;
                                sp.ProposalMultiOptionSpecSet(spec);
                                sidx++;
                            }
                        }

                    }
                    #endregion proposal_multi_option_spec

                    if (oriPmdc == null)
                    {
                        oriPmdc = new ProposalMultiDealCollection();
                    }
                    //比較優惠內容修改Log
                    ProposalFacade.CompareMultiDeals(pro.Id, pmdc, oriPmdc, modifyId, ProposalSourceType.House, ProposalLogType.Initial);


                    #endregion MultiDeals

                    sp.ProposalSet(pro);


                    ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                    ProposalCouponEventContent oriPcec = pcec.Clone();
                    if (!pcec.IsLoaded)
                    {
                        pcec = new ProposalCouponEventContent();
                    }
                    pcec.AppTitle = ProposalFacade.RemoveSpecialCharacter(model.AppTitle);    //網站露出商品名稱
                    pcec.Title = ProposalFacade.RemoveSpecialCharacter(model.DealContent);    //檔次行銷文案
                    pcec.ProposalId = pro.Id;
                    pcec.Description = System.Web.HttpUtility.HtmlDecode(model.Description);  //商品詳細介紹
                    pcec.ProductSpec = System.Web.HttpUtility.HtmlDecode(model.ProductSpec);    //商品規格
                    string remark = model.Remark;
                    //remark = remark.TrimStart(System.Web.HttpUtility.HtmlEncode("<br />").ToCharArray()).TrimStart("\r\n".ToCharArray());
                    //remark = remark.TrimEnd("&amp;nbsp;".ToCharArray()).TrimStart("\r\n".ToCharArray()).TrimEnd(System.Web.HttpUtility.HtmlEncode("<br />").ToCharArray());

                    pcec.Remark = System.Web.HttpUtility.HtmlDecode(remark);    //必買特色
                    pp.ProposalCouponEventContentSet(pcec);

                    if (!string.IsNullOrEmpty(pcImagePath))
                    {
                        pcec.ImagePath = string.IsNullOrEmpty(pcImagePath) ? null : pcImagePath;
                    }
                    if (!string.IsNullOrEmpty(pcOriImagePath))
                    {
                        pcec.OriImagePath = string.IsNullOrEmpty(pcOriImagePath) ? null : pcOriImagePath;
                    }
                    if (!string.IsNullOrEmpty(appImagePath))
                    {
                        pcec.AppDealPic = string.IsNullOrEmpty(appImagePath) ? null : appImagePath;
                    }
                    if (!string.IsNullOrEmpty(removeBGImagePath))
                    {
                        pcec.RemoveBgPic = string.IsNullOrEmpty(removeBGImagePath) ? null : removeBGImagePath;
                    }

                    pp.ProposalCouponEventContentSet(pcec);



                    //提案單異動紀錄
                    ProposalFacade.CompareProposal(pro, oriPro, modifyId);

                    //ProposalCouponEventContent異動紀錄
                    ProposalFacade.CompareProposalCouponEventContent(pcec, oriPcec, modifyId);

                    ts.Complete();
                }
            }



            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pro.Id);
            List<ProposalMultiDealModel> pmdList = new List<ProposalMultiDealModel>();
            TryMultiDeals(pmds, out pmdList);
            multiDeals = pmdList;
            return null;
        }


        /// <summary>
        /// 業務區塊儲存
        /// </summary>
        /// <param name="model"></param>
        protected string HouseProposalContentSalesSave(SellerProposalHouseModel model)
        {
            string message = string.Empty;
            int pid = model.ProId;
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                {
                    List<int> categortList = new JsonSerializer().Deserialize<List<int>>(model.Category);
                    Proposal oriPro = pro.Clone();
                    pro.ContractMemo = model.ContractMemo;
                    pro.ModifyId = UserName;
                    pro.ModifyTime = DateTime.Now;
                    pro.DealType2 = DealTypeFacade.GetLvl2ParentValue(model.DealType2);//商品分線 (第2層)
                    pro.DealTypeDetail = model.DealType2;//商品分線
                    pro.Consignment = model.Consignment;
                    pro.IsBankDeal = model.IsBankDeal;
                    if (categortList.Contains(_config.EveryDayNewDealsCategoryId))
                    {
                        pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal));
                    }
                    else
                    {
                        pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.EveryDayNewDeal));

                    }
                    pro.TrialPeriod = model.TrialPeriod;
                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.NotAllowedDiscount, pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount));
                    pro.NoDiscountShown = model.NoDiscountShown;
                    pro.DeliveryIslands = model.DeliveryIslands;
                    pro.MarketingResource = model.MarketingResource;
                    pro.AncestorBusinessHourGuid = model.AncestorBid;
                    pro.PicAlt = model.PicAlt;
                    pro.SaleMarketAnalysis = model.SaleMarketAnalysis;
                    pro.NoTax = model.NoTax;
                    pro.IsProduction = model.IsProduction;
                    pro.ProductionDescription = model.ProductionDescription;
                    pro.Cchannel = model.CChannel;
                    pro.CchannelLink = model.CChannelLink;
                    pro.NoTaxNotification = model.NoTaxNotification;
                    pro.IsGame = model.IsGame;
                    pro.IsWms = model.IsWms;
                    pro.NoAtm = model.NoATM;
                    if (_config.IsRemittanceFortnightly)
                    {

                    }
                    else
                    {
                        pro.RemittanceType = model.RemittanceType;
                    }


                    sp.ProposalSet(pro);

                    ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                    ProposalCouponEventContent oriPcec = pcec.Clone();
                    if (!pcec.IsLoaded)
                    {
                        pcec = new ProposalCouponEventContent();
                    }


                    List<string> pponDealType = GetPponDealType();
                    string restrictions = string.Empty;
                    if (pponDealType.Contains(pro.DealType1.ToString()))
                    {
                        //在地宅配
                        if (string.IsNullOrEmpty(model.Restrictions))
                        {
                            restrictions = GetRestriction(pro);
                        }
                        else
                        {
                            restrictions = model.Restrictions;
                        }
                        model.Restrictions = restrictions;
                        pcec.Restrictions = System.Web.HttpUtility.HtmlDecode(model.Restrictions);  //權益說明
                    }
                    else
                    {
                        //一般宅配
                        model.Restrictions = GetRestriction(pro);
                        pcec.Restrictions = System.Web.HttpUtility.HtmlDecode(model.Restrictions);  //權益說明
                    }

                    pp.ProposalCouponEventContentSet(pcec);


                    //提案單異動紀錄
                    ProposalFacade.CompareProposal(pro, oriPro, UserName);

                    //ProposalCouponEventContent異動紀錄
                    ProposalFacade.CompareProposalCouponEventContent(pcec, oriPcec, UserName);

                    #region 上檔頻道異動紀錄
                    ProposalCategoryDealCollection rtn = new ProposalCategoryDealCollection();
                    foreach (int cid in categortList)
                    {
                        rtn.Add(new ProposalCategoryDeal()
                        {
                            Pid = pid,
                            Cid = cid
                        });
                    }

                    ProposalFacade.ProposalCategoryDealSave(pro, rtn, UserName);


                    #endregion

                    #region 不顯示折後價於前台

                    if (pro.BusinessHourGuid.HasValue) //有bid才存
                    {
                        var dealDiscountPriceBlacklists = new DealDiscountPriceBlacklist
                        {
                            Bid = (pro.BusinessHourGuid ?? Guid.Empty),
                            CreatedTime = DateTime.Now,
                            CreateId = UserName,
                        };

                        PponFacade.UpdateDealDiscountPriceBlacklist(model.IsDealDiscountPriceBlacklist, dealDiscountPriceBlacklists);
                    }

                    #endregion
                    ts.Complete();
                }

                //同步後臺檔次
                message = ProposalFacade.ProposalFlowCompleted(pro, false, UserName);
            }
            return message;
        }

        /// <summary>
        /// 取得提案單對應商品
        /// </summary>
        /// <param name="sid"></param>
        /// <param name="infoGuid"></param>
        /// <param name="productBrandName"></param>
        /// <param name="productName"></param>
        /// <param name="productCode"></param>
        /// <param name="warehouseType"></param>
        /// <returns></returns>
        protected dynamic GetBaseSellerProducts(Guid sid, Guid infoGuid, string productBrandName, string productName, string productCode, int warehouseType)
        {
            List<Guid> sids = new List<Guid>();
            sids.Add(sid);
            List<ViewProductItem> vpis = pp.ViewProductItemListGet(1, 999, sids, infoGuid, default(int), productBrandName, productName, string.Empty, productCode, string.Empty, default(int), warehouseType, true, true);

            var dists = vpis.Select(x => new
            {
                InfoGuid = x.InfoGuid.Value,
                InfoName = string.Format("{0} {1}", x.ProductBrandName, x.ProductName),
                IsMulti = x.IsMulti
            }).Distinct().ToList();

            List<dynamic> list = new List<dynamic>();

            foreach (var dist in dists)
            {
                var items = vpis.Where(x => x.InfoGuid == dist.InfoGuid).OrderBy(x => x.Sort).ToList();
                List<object> itemList = new List<object>();
                foreach (var item in items)
                {
                    itemList.Add(new
                    {
                        ItemGuid = item.ItemGuid,
                        ItemName = item.ItemName
                    });
                }
                list.Add(new
                {
                    InfoGuid = dist.InfoGuid,
                    InfoName = dist.InfoName,
                    IsMulti = dist.IsMulti,
                    ItemList = itemList
                });
            }

            return Json(list);
        }
        protected dynamic DeleteBaseProduct(Guid pid, string modifyId)
        {
            try
            {
                ProductInfo pi = pp.ProductInfoGet(pid);
                if (pi.IsLoaded)
                {

                    #region Check
                    string usedPid = string.Empty;
                    ProductItemCollection pdis = pp.ProductItemListGetByProductGuid(pid);

                    bool chkUserd = CheckProductUsedByProposal(pid.ToString(), out usedPid);
                    if (chkUserd)
                    {
                        return Json(new
                        {
                            Message = string.Format("提案單[{0}]已使用，無法刪除", usedPid),
                            IsSuccess = false
                        });
                    }
                    foreach (ProductItem pdi in pdis)
                    {
                        chkUserd = CheckProductUsedWmsPurchaseOrder(pdi.Guid.ToString());
                        if (chkUserd)
                        {
                            return Json(new
                            {
                                Message = string.Format("已有進倉單商品，無法刪除"),
                                IsSuccess = false
                            });
                        }
                    }
                    #endregion Check

                    pi.Status = (int)ProductItemStatus.Deleted;
                    pi.ModifyId = modifyId;
                    pi.ModifyTime = DateTime.Now;
                    pp.ProductInfoSet(pi);


                    foreach (ProductItem pdi in pdis)
                    {
                        pdi.ItemStatus = (int)ProductItemStatus.Deleted;
                        pdi.ModifyId = modifyId;
                        pdi.ModifyTime = DateTime.Now;
                        pp.ProductItemSet(pdi);

                        ProductSpecCollection specs = pp.ProductSpecListGetByItemGuid(pdi.Guid);
                        foreach (ProductSpec spec in specs)
                        {
                            spec.SpecStatus = (int)ProductItemStatus.Deleted;
                            spec.ModifyId = modifyId;
                            spec.ModifyTime = DateTime.Now;
                            pp.ProductSpecSet(spec);
                        }
                    }
                }
                return Json(new
                {
                    Message = string.Empty,
                    IsSuccess = true
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new
                {
                    Message = "請聯絡IT人員",
                    IsSuccess = false
                });
            }
        }

        protected dynamic DisabledBaseProduct(Guid did, int status, string modifyId)
        {
            try
            {
                ProductItem pdi = pp.ProductItemGet(did);
                if (pdi.IsLoaded)
                {
                    #region Check
                    if (status == (int)ProductItemStatus.Disabled)
                    {
                        string usedPid = string.Empty;
                        if (CheckProductUsedWmsPurchaseOrder(did.ToString()))
                        {
                            return Json(new
                            {
                                Message = "已建立進倉單，無法停用",
                                IsSuccess = false
                            });
                        }

                        bool chkUserd = CheckItemUsedByProposal(did.ToString(), out usedPid);
                        if (chkUserd)
                        {
                            return Json(new
                            {
                                Message = string.Format("提案單[{0}]已使用，無法停用", usedPid),
                                IsSuccess = false
                            });
                        }
                    }
                    #endregion Check

                    pdi.ItemStatus = status;
                    pdi.ModifyTime = DateTime.Now;
                    pdi.ModifyId = modifyId;
                    pp.ProductItemSet(pdi);

                    ProductSpecCollection specs = pp.ProductSpecListGetByItemGuid(pdi.Guid);
                    foreach (ProductSpec spec in specs)
                    {
                        spec.SpecStatus = status;
                        spec.ModifyTime = DateTime.Now;
                        spec.ModifyId = modifyId;
                        pp.ProductSpecSet(spec);
                    }
                }
                return Json(new
                {
                    Message = string.Empty,
                    IsSuccess = true
                });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new
                {
                    Message = "請聯絡IT人員",
                    IsSuccess = false
                });
            }
        }

        protected string GetExcelCellValue(object obj)
        {
            return (obj == null ? string.Empty : obj.ToString().Trim());
        }

        //還有再用嗎?
        //業務系統 & 商家系統 共用查詢庫存
        //改以 PponFacade.GetProductOptionListForVbs 及 PponFacade.GetProductOptionListForSal 取代
        //protected dynamic GetBaseProductOption(int pageStart, int pageLength, List<Guid> sellerGuids, int? productNo, string productBrandName,
        //    string productName, string gtin, string mpn, string productCode, string items, int? stockStatus, int warehouseType, bool? hideDisabled, Guid? bid, int? uniqueId)
        //{
        //    List<ViewProductItem> vpis = GetProductOptionList(pageStart, pageLength, sellerGuids, productNo, productBrandName,
        //        productName, gtin, productCode, items, stockStatus, warehouseType, hideDisabled);
        //    int totalCount = vpis.Count();
        //    int totalPage = (totalCount / pageLength);
        //    if ((totalCount % pageLength) > 0)
        //    {
        //        totalPage += 1;
        //    }
        //    if (totalCount == 0)
        //    {
        //        totalPage = 1;
        //    }

        //    var rtnData = vpis.OrderBy(z => z.ProductNo).OrderBy(y => y.ProductBrandName).OrderBy(x => x.ProductName)
        //        .Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(x => new
        //        {
        //            InfoGuid = x.InfoGuid,
        //            ItemGuid = x.ItemGuid,
        //            Sid = x.SellerGuid,
        //            ProductNo = x.ProductNo,
        //            ProductBrandName = x.ProductBrandName,
        //            ProductName = x.ProductName,
        //            ItemName = x.ItemName,
        //            Gtins = x.Gtins,
        //            Mpn = x.Mpn,
        //            ProductCode = x.ProductCode,
        //            Stock = x.Stock,
        //            SaftyStock = x.SafetyStock,
        //            ItemStatus = x.ItemStatus
        //        }).ToList();

        //    dynamic obj = new
        //    {
        //        TotalCount = totalCount,
        //        TotalPage = totalPage,
        //        CurrentPage = pageStart,
        //        Data = rtnData
        //    };
        //    return Json(obj);
        //}

        //private List<ViewProductItem> GetProductOptionList(int pageStart, int pageLength, List<Guid> sellerGuids, int? productNo,
        //    string productBrandName, string productName, string gtin, string productCode, string items, int? stockStatus, int warehouseType, bool? hideDisabled)
        //{
        //    List<ViewProductItem> vpis = pp.ViewProductItemListGet(pageStart, pageLength, sellerGuids, null, productNo.GetValueOrDefault(),
        //        productBrandName, productName, gtin, productCode, items, stockStatus.GetValueOrDefault(), warehouseType, hideDisabled.GetValueOrDefault());
        //    return vpis;
        //}


        private void TryParseProposal(Proposal pro, out SellerProposalHouseModel spro)
        {
            SellerProposalHouseModel sProposal = new SellerProposalHouseModel();
            Seller seller = sp.SellerGet(pro.SellerGuid);
            if (pro.IsLoaded)
            {
                sProposal.ProId = pro.Id;
                sProposal.SellerGuid = pro.SellerGuid;
                sProposal.SellerId = seller.SellerId;
                sProposal.SellerName = seller.SellerName;
                sProposal.SellerProposalFlag = pro.SellerProposalFlag;
                sProposal.BusinessHourId = (pro.BusinessHourGuid ?? Guid.Empty).ToString();
                sProposal.SpecialFlag = pro.SpecialFlag;
                sProposal.AgentChannels = pro.AgentChannels;
                if (pro.OperationSalesId != 0)
                {
                    ViewEmployee opSalesEmp = hmp.ViewEmployeeGet(Employee.Columns.UserId, pro.OperationSalesId);
                    if (opSalesEmp != null && opSalesEmp.IsLoaded)
                    {
                        sProposal.OpSalesId = opSalesEmp.UserId;                    //負責業務
                        sProposal.OpSalesName = opSalesEmp.EmpName;
                    }
                }
                sProposal.DealType1 = pro.DealType1;  //商品類型
                sProposal.DealType2 = pro.DealTypeDetail ?? pro.DealType2;  //商品分線
                sProposal.BrandName = pro.BrandName;    //品牌名稱
                sProposal.DealName = pro.DealName;  //商品名稱
                sProposal.VendorReceiptType = pro.VendorReceiptType;//請款單據
                sProposal.AccountingMessage = pro.Othermessage;//其他請款方式
                //出貨類型
                if (pro.IsWms)
                {
                    sProposal.ProShipType = 3;
                }
                else
                {
                    if (pro.ShipType == (int)DealShipType.Normal || pro.ShipType == (int)DealShipType.Ship72Hrs)
                    {
                        sProposal.ProShipType = 1;
                        if (pro.ShipType == (int)DealShipType.Normal)
                        {
                            sProposal.ProShipDays = pro.ShipText3;
                        }
                    }
                    else
                    {
                        sProposal.ProShipType = 2;
                    }
                }


                Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
                if (specialText != null && specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.InspectRule))
                    sProposal.InspectRule = specialText[(ProposalSpecialFlag)ProposalSpecialFlag.InspectRule];


                ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                sProposal.DeliveryMethod = pro.DeliveryMethod;//配送溫層
                sProposal.AppTitle = pcec.AppTitle;    //品牌名稱
                sProposal.DealContent = pcec.Title;//檔次行銷文案
                sProposal.DealSource = pro.DealSource;//商品來源                
                sProposal.Description = pcec.Description;  //商品詳細介紹
                sProposal.ProductSpec = pcec.ProductSpec;    //商品規格
                sProposal.Remark = PponFacade.GetPponRemark(pcec.Remark);    //必買特色
                sProposal.Restrictions = pcec.Restrictions;//權益說明
                sProposal.ContractMemo = pro.ContractMemo;

                sProposal.CChannel = pro.Cchannel;  //Cchannel
                sProposal.CChannelLink = pro.CchannelLink;
                sProposal.NoTaxNotification = pro.NoTaxNotification;//免稅商品通報
                sProposal.IsGame = pro.IsGame;//活動遊戲
                sProposal.IsWms = pro.IsWms;//24到貨
                if (pro.OrderTimeS != null)
                {
                    sProposal.BusinessHourOrderTimeS = pro.OrderTimeS.Value.ToString("yyyy/MM/dd HH:mm");
                }
                if (pro.OrderTimeE != null)
                {
                    sProposal.BusinessHourOrderTimeE = pro.OrderTimeE.Value.ToString("yyyy/MM/dd HH:mm");
                }

                if (pro.DeliveryTimeS != null)
                {
                    sProposal.BusinessHourDeliveryTimeS = pro.DeliveryTimeS.Value.ToString("yyyy/MM/dd HH:mm");
                }
                if (pro.DeliveryTimeE != null)
                {
                    sProposal.BusinessHourDeliveryTimeE = pro.DeliveryTimeE.Value.ToString("yyyy/MM/dd HH:mm");
                }

                #region 狀態
                sProposal.ApplyFlag = pro.ApplyFlag;
                sProposal.ApproveFlag = pro.ApproveFlag;
                sProposal.BusinessCreateFlag = pro.BusinessCreateFlag;
                sProposal.BusinessFlag = pro.BusinessFlag;
                sProposal.EditFlag = pro.EditFlag;
                sProposal.ListingFlag = pro.ListingFlag;
                #endregion 狀態

                sProposal.ProposalCreatedType = pro.ProposalCreatedType;  //商家提案狀態(商家或業務提案)
                sProposal.TrialPeriod = pro.TrialPeriod ?? false;
                sProposal.CreateId = pro.CreateId;

                bool isCreateBusinessHour = false;
                if (pro.BusinessHourGuid != null)
                {
                    BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                    if (bh != null && bh.IsLoaded)
                    {
                        isCreateBusinessHour = true;
                    }
                }
                sProposal.IsCreateBusinessHour = isCreateBusinessHour;//判斷是否已建檔

                #region 圖片
                string baseDirectoryPath = Path.Combine(Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/media/"), seller.SellerId));

                if (!string.IsNullOrEmpty(pcec.ImagePath))
                {
                    string[] images = ImageFacade.GetMediaPathsFromRawData(pcec.ImagePath, MediaType.PponDealPhoto);
                    var imagePath = images.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.Index != 1 && x.Index != 2 && x.ImgPath != string.Empty).Select(x => x.ImgPath).FirstOrDefault();

                    string a = pcec.ImagePath.Split('|')[0].Split(',')[1];
                    var b = a.Split('?')[0];

                    if (System.IO.File.Exists(Path.Combine(baseDirectoryPath, b)))
                    {
                        byte[] imageArray = System.IO.File.ReadAllBytes(Path.Combine(baseDirectoryPath, b));
                        sProposal.ImagePath = Convert.ToBase64String(imageArray);
                    }
                }

                if (!string.IsNullOrEmpty(pcec.AppDealPic))
                {
                    string appDealPic = ImageFacade.GetMediaPathsFromRawData(pcec.AppDealPic, MediaType.PponDealPhoto).FirstOrDefault();

                    if (pcec.AppDealPic.Split(',').Length > 1)
                    {
                        string a = pcec.AppDealPic.Split(',')[1];
                        var b = a.Split('?')[0];
                        if (System.IO.File.Exists(Path.Combine(baseDirectoryPath, b)))
                        {
                            byte[] imageArray = System.IO.File.ReadAllBytes(Path.Combine(baseDirectoryPath, b));

                            sProposal.AppDealPic = Convert.ToBase64String(imageArray);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(pcec.RemoveBgPic))
                {
                    string removeBgPic = ImageFacade.GetMediaPathsFromRawData(pcec.RemoveBgPic, MediaType.PponDealPhoto).FirstOrDefault();

                    if (pcec.RemoveBgPic.Split(',').Length > 1)
                    {
                        string a = pcec.RemoveBgPic.Split(',')[1];
                        var b = a.Split('?')[0];
                        if (System.IO.File.Exists(Path.Combine(baseDirectoryPath, b)))
                        {
                            byte[] imageArray = System.IO.File.ReadAllBytes(Path.Combine(baseDirectoryPath, b));

                            sProposal.RemoveBGPic = Convert.ToBase64String(imageArray);
                            sProposal.RemoveBGPicUrl = removeBgPic;
                        }
                    }
                }
                #endregion 圖片


                sProposal.Consignment = pro.Consignment;
                sProposal.IsBankDeal = pro.IsBankDeal;
                sProposal.NoDiscountShown = pro.NoDiscountShown;
                sProposal.DeliveryIslands = pro.DeliveryIslands;
                sProposal.MarketingResource = pro.MarketingResource;
                sProposal.PicAlt = pro.PicAlt;
                sProposal.AncestorBid = pro.AncestorBusinessHourGuid ?? null;
                sProposal.SaleMarketAnalysis = pro.SaleMarketAnalysis;
                sProposal.NoTax = pro.NoTax;
                sProposal.IsProduction = pro.IsProduction;
                sProposal.ProductionDescription = pro.ProductionDescription;
                sProposal.RemittanceType = pro.RemittanceType;
                sProposal.PassSeller = Convert.ToBoolean(pro.PassSeller);
                sProposal.FlowCompleteTimes = pro.FlowCompleteTimes;
                sProposal.NoATM = pro.NoAtm;

                #region 指派
                List<string> assignList = new List<string>();
                List<ProposalAssignLog> pals = sp.ProposalAssignLogGetList(pro.Id)
                                                .OrderBy(x => x.AssignFlag).ToList();
                foreach (ProposalAssignLog item in pals)
                {
                    assignList.Add(Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalEditorFlag)item.AssignFlag) + " " + item.AssignEmail);

                }
                sProposal.ProductionAssignList = assignList;
                #endregion 指派


                #region MultiDeals
                ProposalMultiDealCollection oriMultiDeals = sp.ProposalMultiDealGetByPid(pro.Id);
                List<ProposalMultiDealModel> pmdList = new List<ProposalMultiDealModel>();
                TryMultiDeals(oriMultiDeals, out pmdList);
                sProposal.MultiDeals = new JsonSerializer().Serialize(pmdList);
                #endregion MultiDeals
            }
            spro = sProposal;
        }
        private void TryMultiDeals(ProposalMultiDealCollection oriMultiDeals, out List<ProposalMultiDealModel> result)
        {
            List<ProposalMultiDealModel> pmdList = new List<ProposalMultiDealModel>();
            foreach (ProposalMultiDeal deal in oriMultiDeals)
            {
                string strOption = string.Empty;
                string uniqueId = string.Empty;
                try
                {
                    List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(deal.Options);
                    List<ProductInfo> products = pp.ProductInfoListGet(specs.SelectMany(x => x.Items)
                                                        .Select(y => y.product_guid).ToList()).ToList();


                    List<ProductItem> items = pp.ProductItemGetList(specs.SelectMany(x => x.Items)
                                                        .Select(y => y.item_guid).ToList()).ToList();
                    //pp.ProductSpecGet()

                    foreach (ProposalMultiDealsItem item in specs.SelectMany(x => x.Items))
                    {
                        ProductInfo product = products.Where(x => x.Guid == item.product_guid).FirstOrDefault();

                        List<ProductItem> pis = items.Where(x => x.Guid == item.item_guid).ToList();


                        if (product != null)
                        {
                            if (product.IsMulti == false)
                            {
                                item.name = product.ProductBrandName + product.ProductName;
                            }
                            else
                            {
                                item.name = product.ProductBrandName + product.ProductName + "_" + string.Join("_", pis.Select(x => x.SpecName));
                            }
                        }
                    }
                    strOption = new JsonSerializer().Serialize(specs).Replace("Items", "items");
                }
                catch
                {

                }

                if (deal.Bid != null)
                {
                    DealProperty dp = pp.DealPropertyGet(deal.Bid.Value);
                    if (dp.IsLoaded)
                    {
                        uniqueId = dp.UniqueId.ToString();
                    }
                }
                ProposalMultiDealModel pmd = new ProposalMultiDealModel()
                {
                    Id = deal.Id,
                    ItemName = deal.ItemName,
                    BrandName = deal.BrandName,
                    OrigPrice = deal.OrigPrice,
                    ItemPrice = deal.ItemPrice,
                    Cost = deal.Cost,
                    OrderTotalLimit = deal.OrderTotalLimit,
                    AtmMaximum = deal.AtmMaximum,
                    Freights = deal.Freights,
                    NoFreightLimit = deal.NoFreightLimit,
                    Options = strOption,
                    SellerOptions = strOption,
                    Sort = deal.Sort,
                    OrderMaxPersonal = deal.OrderMaxPersonal.ToString(),
                    QuantityMultiplier = deal.QuantityMultiplier,
                    ComboPackCount = deal.ComboPackCount,
                    Unit = deal.Unit,
                    Gifts = deal.Gifts,
                    UniqueId = uniqueId,
                    BoxQuantityLimit = deal.BoxQuantityLimit
                };
                pmdList.Add(pmd);
            }
            result = pmdList;
        }
        protected ActionResult GetBaseProductItem(List<ProductItemSourceModel> psms)
        {
            List<SerializeableCategoryItem> oplist = new List<SerializeableCategoryItem>();
            int arrLen = psms.Count;

            var repCatgName = psms.GroupBy(x => x.CatgName)
                                .Where(g => g.Count() > 1)
                                .Select(y => new { data = y.Key }).ToList();

            if (repCatgName.Count() > 0)
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = string.Format("{0}重複", string.Join(",", repCatgName.Select(x => x.data)))
                });
            }

            foreach (ProductItemSourceModel psm in psms)
            {
                foreach (ItemCategory item in psm.Items)
                {
                    List<ItemCategory> ics = new List<ItemCategory>();
                    ics.Add(new ItemCategory
                    {
                        OptionGuid = Guid.NewGuid(),
                        OptionName = item.OptionName
                    });
                    var data = psms.Where(x => x.CatgName != psm.CatgName && x.Index > psm.Index).ToList();
                    if (data.Count > 0)
                    {
                        SpreadItems(data, oplist, ics, arrLen);
                    }
                    else
                    {
                        if (ics.Count == arrLen)
                        {
                            oplist.Add(new SerializeableCategoryItem
                            {
                                Id = Guid.NewGuid(),
                                Items = ics
                            });
                        }
                    }
                }
            }
            return Json(oplist);
        }
        protected dynamic ImportProductInfoExcel(Guid sid, string modifyId)
        {
            int successCount = 0;
            int failCount = 0;
            string message = string.Empty;
            Guid eid = Guid.NewGuid();

            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();
            Dictionary<string, List<ProductExcelBatchModel>> failureList = new Dictionary<string, List<ProductExcelBatchModel>>();
            try
            {
                StringBuilder sb = new StringBuilder();
                Seller seller = sp.SellerGet(sid);
                Dictionary<string, List<ProductExcelBatchModel>> groupList = new Dictionary<string, List<ProductExcelBatchModel>>();

                int rowCnt = 0;
                ViewProductItemCollection vpis = pp.ViewProductItemListGetBySellerGuid(sid);
                bool isConfirmWms = WmsFacade.isConfirmWms(sid, string.Empty);

                if (seller != null && seller.IsLoaded)
                {
                    StringBuilder errBuilder = new StringBuilder();
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        HSSFWorkbook hssfworkbook = new HSSFWorkbook(file.InputStream);
                        Sheet sheet = hssfworkbook.GetSheetAt(0);
                        Row row;

                        #region 將Excel中的資料分群
                        for (int k = 4; k <= sheet.LastRowNum; k++)
                        {
                            row = sheet.GetRow(k);
                            if (row != null)
                            {
                                ProductExcelBatchModel data = new ProductExcelBatchModel();
                                string groupId = GetExcelCellValue(row.GetCell(0));//群組編號
                                string specType = GetExcelCellValue(row.GetCell(1));
                                string saftyStock = string.Empty;
                                string memo = string.Empty;

                                if (row.GetCell(9) != null)
                                {
                                    if (row.GetCell(9).CellType == CellType.FORMULA)
                                    {
                                        //公式
                                        saftyStock = row.GetCell(9).NumericCellValue.ToString();
                                    }
                                    else
                                    {
                                        saftyStock = GetExcelCellValue(row.GetCell(9));
                                    }
                                }

                                data = new ProductExcelBatchModel
                                {
                                    GroupId = groupId,    //群組編號
                                    SpecType = GetExcelCellValue(row.GetCell(1)),//規格類型
                                    ProductBrandName = GetExcelCellValue(row.GetCell(2)),//品牌名稱
                                    ProductName = GetExcelCellValue(row.GetCell(3)),//商品名稱
                                    CatgName = GetExcelCellValue(row.GetCell(4)),//規格名稱
                                    SpecName = GetExcelCellValue(row.GetCell(5)),//規格內容
                                    Gtins = GetExcelCellValue(row.GetCell(6)),//Gtins
                                    ProductCode = GetExcelCellValue(row.GetCell(7)),//貨號
                                    Stock = GetExcelCellValue(row.GetCell(8)),//庫存
                                    SaftyStock = saftyStock, //安全庫存
                                    Mpn = GetExcelCellValue(row.GetCell(10)),//Mpn
                                    WarehouseType = GetExcelCellValue(row.GetCell(11)),//使用類型
                                    Price = GetExcelCellValue(row.GetCell(12)),//保存價值
                                    ShelfLife = GetExcelCellValue(row.GetCell(13)),//有效天數
                                };

                                if (string.IsNullOrEmpty(data.GroupId)
                                    && string.IsNullOrEmpty(data.SpecType)
                                    && string.IsNullOrEmpty(data.ProductBrandName)
                                    && string.IsNullOrEmpty(data.ProductName)
                                    && string.IsNullOrEmpty(data.CatgName)
                                    && string.IsNullOrEmpty(data.SpecName)
                                    && string.IsNullOrEmpty(data.Gtins)
                                    && string.IsNullOrEmpty(data.ProductCode)
                                    && string.IsNullOrEmpty(data.Stock)
                                    && string.IsNullOrEmpty(data.SaftyStock)
                                    && string.IsNullOrEmpty(data.Mpn)
                                    && string.IsNullOrEmpty(data.WarehouseType)
                                    && string.IsNullOrEmpty(data.Price)
                                    && string.IsNullOrEmpty(data.ShelfLife))
                                {
                                    continue;
                                }

                                List<ProductExcelBatchModel> tmpList = new List<ProductExcelBatchModel>();
                                if (groupList.ContainsKey(groupId))
                                {
                                    tmpList = groupList[groupId];
                                    tmpList.Add(data);
                                }
                                else
                                {
                                    tmpList.Add(data);
                                    groupList.Add(groupId, tmpList);
                                }
                                rowCnt++;
                            }
                        }
                        #endregion 將Excel中的資料分群
                    }

                    #region 嘗試將資料寫入資料庫
                    //取出該賣家所有商品
                    // List<ViewProductItem> vpis = pp.ViewProductItemListGetBySellerGuid(seller.Guid).Where(x => x.ProductStatus == (int)ProductItemStatus.Normal).ToList();

                    using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        try
                        {
                            foreach (var group in groupList)
                            {
                                List<ProductExcelBatchModel> dataList = (List<ProductExcelBatchModel>)group.Value;
                                Guid pid = Guid.NewGuid();

                                int itemIdx = 1;
                                foreach (ProductExcelBatchModel data in dataList)
                                {
                                    try
                                    {
                                        //Check Error
                                        data.Memo = string.Empty;
                                        bool flag = CheckProductImportError(sid, group.Value, data, vpis, isConfirmWms);

                                        List<ProductExcelBatchModel> tmpList = new List<ProductExcelBatchModel>();
                                        if (failureList.ContainsKey(group.Key))
                                        {
                                            tmpList = failureList[group.Key];
                                            tmpList.Add(data);
                                        }
                                        else
                                        {
                                            tmpList.Add(data);
                                            failureList.Add(group.Key, tmpList);
                                        }

                                        if (!flag)
                                        {
                                            failCount++;
                                            continue;
                                        }

                                        bool isMulti = false;
                                        if (data.SpecType.ToLower() == "b")
                                        {
                                            isMulti = true;
                                        }

                                        #region 寫入資料庫
                                        ProductInfo pi = pp.ProductInfoGet(pid);
                                        if (pi == null || !pi.IsLoaded)
                                        {
                                            pi = new ProductInfo()
                                            {
                                                Guid = pid,
                                                SellerGuid = sid,
                                                IsMulti = isMulti,
                                                ProductBrandName = data.ProductBrandName,
                                                ProductName = data.ProductName,
                                                Status = (int)ProductItemStatus.Normal,
                                                CreateTime = DateTime.Now,
                                                CreateId = modifyId,
                                                ModifyTime = DateTime.Now,
                                                ModifyId = modifyId,
                                                DataSource = ((int)ProductDataSource.Import).ToString(),
                                                WarehouseType = data.WarehouseType == "1" ? (int)WarehouseType.Normal : (int)WarehouseType.Wms
                                            };
                                            pp.ProductInfoSet(pi);
                                        }

                                        int stock = 0;
                                        int saftyStock = 0;
                                        int price = 0;
                                        int shelfLife = 0;
                                        int.TryParse(data.Stock, out stock);
                                        int.TryParse(data.SaftyStock, out saftyStock);
                                        int.TryParse(data.Price, out price);
                                        int.TryParse(data.ShelfLife, out shelfLife);
                                        ProductItem item = new ProductItem()
                                        {
                                            InfoGuid = pi.Guid,
                                            Guid = Guid.NewGuid(),
                                            ProductCode = data.ProductCode,
                                            Gtins = data.Gtins,
                                            Mpn = data.Mpn,
                                            OriStock = stock,
                                            Stock = stock,
                                            Sales = 0,
                                            SafetyStock = saftyStock,
                                            WarehouseType = data.WarehouseType == "1" ? (int)WarehouseType.Normal : (int)WarehouseType.Wms,
                                            Price = price,
                                            ShelfLife = shelfLife == 0 ? null : (int?)shelfLife,
                                            Sort = itemIdx,
                                            ItemStatus = (int)ProductItemStatus.Normal,
                                            SpecName = data.SpecName,
                                            CreateTime = DateTime.Now,
                                            CreateId = modifyId,
                                            ModifyTime = DateTime.Now,
                                            ModifyId = modifyId
                                        };
                                        pp.ProductItemSet(item);

                                        if (isMulti)
                                        {
                                            string[] catgs = data.CatgName.Split("_");
                                            string[] specs = data.SpecName.Split("_");
                                            for (int ci = 0; ci < catgs.Length; ci++)
                                            {
                                                ProductSpec spec = new ProductSpec()
                                                {
                                                    ItemGuid = item.Guid,
                                                    Guid = Guid.NewGuid(),
                                                    CatgName = catgs[ci],
                                                    SpecName = specs[ci],
                                                    Sort = ci,
                                                    SpecStatus = (int)ProductItemStatus.Normal,
                                                    CreateTime = DateTime.Now,
                                                    CreateId = modifyId,
                                                    ModifyTime = DateTime.Now,
                                                    ModifyId = modifyId
                                                };
                                                pp.ProductSpecSet(spec);
                                            }
                                        }
                                        //List<ProductExcelBatchModel> successList = new List<ProductExcelBatchModel>();
                                        //successList = failureList[group.Key];
                                        //successList.Add(data);
                                        //failureList.Add(group.Key, successList);

                                        itemIdx++;
                                        successCount++;

                                        #endregion 寫入資料庫
                                    }
                                    catch
                                    {
                                        failCount++;
                                        List<ProductExcelBatchModel> tmpList = new List<ProductExcelBatchModel>();
                                        if (failureList.ContainsKey(group.Key))
                                        {
                                            tmpList = failureList[group.Key];
                                            tmpList.Add(data);
                                        }
                                        else
                                        {
                                            tmpList.Add(data);
                                            failureList.Add(group.Key, tmpList);
                                        }
                                    }
                                }
                            }
                            if (failureList.SelectMany(x => x.Value).Where(y => y.Memo != string.Empty).ToList().Count > 0)
                            {
                                throw new Exception("資料異常");
                            }
                            ts.Complete();
                        }
                        catch
                        {

                        }
                    }
                    #endregion 嘗試將資料寫入資料庫

                    #region Fail List
                    if (failureList.SelectMany(x => x.Value).Where(y => y.Memo != string.Empty).ToList().Count > 0)
                    {
                        foreach (var list in failureList)
                        {
                            foreach (ProductExcelBatchModel p in list.Value)
                            {
                                ProductInfoImportLog pilog = new ProductInfoImportLog()
                                {
                                    Guid = eid,
                                    GroupId = p.GroupId,
                                    SpecType = p.SpecType,
                                    ProductBrandName = p.ProductBrandName,
                                    ProductName = p.ProductName,
                                    CatgName = p.CatgName,
                                    SpecName = p.SpecName,
                                    Gtins = p.Gtins,
                                    Mpn = p.Mpn,
                                    ProductCode = p.ProductCode,
                                    Stock = p.Stock,
                                    SaftyStock = p.SaftyStock,
                                    WarehouseType = p.WarehouseType,
                                    Price = p.Price,
                                    ShelfLife = p.ShelfLife,
                                    Memo = p.Memo,
                                    CreateId = modifyId,
                                    CreateTime = DateTime.Now
                                };
                                pp.ProductInfoImportLogSet(pilog);
                            }
                        }
                    }
                    #endregion Fail List
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            stopwatch.Stop();
            TimeSpan tsp = stopwatch.Elapsed;
            if (tsp.Seconds <= 5)
            {
                //睡2秒，讓前端 Loading 特效出現
                System.Threading.Thread.Sleep(2000);
            }

            dynamic obj = new
            {
                SuccessCount = successCount,
                FailCount = failureList.SelectMany(x => x.Value).Where(y => y.Memo != string.Empty).ToList().Count,
                Eid = (failCount > 0 ? eid.ToString() : string.Empty),
                Message = message
            };
            return Json(obj);
        }
        protected bool CheckProductImportError(Guid sid, List<ProductExcelBatchModel> group,
            ProductExcelBatchModel data, ViewProductItemCollection vpis, bool isConfirmWms)
        {
            #region Check Error
            if (string.IsNullOrEmpty(data.GroupId))
            {
                data.Memo = "群組編號空白";
                return false;
            }
            int groupId = 0;
            int.TryParse(data.GroupId, out groupId);
            {
                if (groupId == 0 || groupId > 9999)
                {
                    data.Memo = "群組編號須為1~9999的數字";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(data.SpecType))
            {
                data.Memo = "規格類型空白";
                return false;
            }
            if (data.SpecType.ToLower() == "ａ" || data.SpecType.ToUpper() == "A")
            {
                data.SpecType = "a";
            }
            if (data.SpecType.ToLower() == "ｂ" || data.SpecType.ToUpper() == "B")
            {
                data.SpecType = "b";
            }
            if (data.SpecType.ToLower() != "a" && data.SpecType.ToLower() != "b")
            {
                data.Memo = "規格類型須為A或B";
                return false;
            }
            //品牌名稱不可為空值
            //if (string.IsNullOrEmpty(data.ProductBrandName))
            //{
            //    data.Memo = "品牌名稱空白";
            //    return false;
            //}
            //商品名稱不可為空值
            if (string.IsNullOrEmpty(data.ProductName))
            {
                data.Memo = "商品名稱空白";
                return false;
            }
            if (!string.IsNullOrEmpty(data.ProductCode))
            {
                if (CheckEngNum(data.ProductCode))
                {
                    data.Memo = "貨號(只能輸入英數字)";
                    return false;
                }
                if (data.ProductCode.Length > 50)
                {
                    data.Memo = "貨號至多50字元";
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(data.Gtins))
            {
                if (CheckEngNum(data.Gtins))
                {
                    data.Memo = "Gtins(只能輸入英數字)";
                    return false;
                }
                if (data.Gtins.Length > 50)
                {
                    data.Memo = "全球交易品項識別碼 至多50字元";
                    return false;
                }
            }
            if (!string.IsNullOrEmpty(data.Mpn))
            {
                if (CheckEngNum(data.Mpn))
                {
                    data.Memo = "MPN(只能輸入英數字)";
                    return false;
                }
                if (data.Mpn.Length > 70)
                {
                    data.Memo = "製造商零件編號 至多70字元";
                    return false;
                }
            }
            if (CheckNumber(data.Stock))
            {
                data.Memo = "庫存數(只能輸入數字)";
                return false;
            }
            if (string.IsNullOrEmpty(data.Stock))
            {
                data.Stock = "0";
            }
            if (string.IsNullOrEmpty(data.SaftyStock))
            {
                //當《庫存量》有資料值，但《安全庫存量》沒有輸入為空白時，匯入系統後，該欄位自動帶庫存量的十分之一數字
                data.SaftyStock = Convert.ToString(int.Parse(data.Stock) / 10);
            }
            if (CheckNumber(data.SaftyStock))
            {
                data.Memo = "安全庫存數(只能輸入數字)";
                return false;
            }
            if (int.Parse(data.SaftyStock) > int.Parse(data.Stock))
            {
                data.Memo = "安全庫存數不得大於庫存數";
                return false;
            }
            int stock = 0;
            int.TryParse(data.Stock, out stock);
            if (stock > 99999 || stock < 0)
            {
                data.Memo = "庫存數需介於0~99999";
                return false;
            }
            int saftyStock = 0;
            int.TryParse(data.SaftyStock, out saftyStock);
            if (saftyStock > 99999 || saftyStock < 0)
            {
                data.Memo = "安全庫存數需介於0~99999";
                return false;
            }
            if (data.CatgName.Length > 35)
            {
                data.Memo = "規格名稱 (每個規格最多35個字)";
                return false;
            }
            if (data.SpecName.Length > 35)
            {
                data.Memo = "規格內容 (每個規格內容最多35個字)";
                return false;
            }
            if (string.IsNullOrEmpty(data.WarehouseType))
            {
                data.Memo = "使用類型為必填";
                return false;
            }
            if (CheckNumber(data.WarehouseType) || (data.WarehouseType != "1" && data.WarehouseType != "2"))
            {
                data.Memo = "使用類型為1或2";
                return false;
            }
            if (!isConfirmWms && data.WarehouseType == "2")
            {
                data.Memo = "尚未同意24到貨合約，無法匯入24到貨商品";
                return false;
            }
            if (data.WarehouseType == "2")
            {
                if (data.SpecType == "a")
                {
                    data.Memo = "使用類型為2，24H到貨，其規格類型必定為B";
                    return false;
                }

                if (string.IsNullOrEmpty(data.Price))
                {
                    data.Memo = "使用類型2，保存價值為必填";
                    return false;
                }
                if (CheckNumber(data.Price))
                {
                    data.Memo = "保存價值(只能輸入數字)";
                    return false;
                }
                if (!string.IsNullOrEmpty(data.Stock))
                {
                    data.Memo = "使用類型2，不需輸入庫存數";
                    return false;
                }

            }
            if (!string.IsNullOrEmpty(data.Stock) && CheckNumber(data.ShelfLife))
            {
                data.Memo = "有效天數(只能輸入數字)";
                return false;
            }
            if (data.SpecType.ToLower() == "a")
            {
                //單一規格
                if (!string.IsNullOrEmpty(data.CatgName))
                {
                    data.Memo = "單一規格不需填入規格名稱";
                    return false;
                }
                if (!string.IsNullOrEmpty(data.SpecName))
                {
                    data.Memo = "單一規格不需填入規格內容";
                    return false;
                }
            }
            else
            {
                //多規格
                if (string.IsNullOrEmpty(data.CatgName))
                {
                    data.Memo = "多規格請填入規格名稱";
                    return false;
                }
                if (string.IsNullOrEmpty(data.SpecName))
                {
                    data.Memo = "多規格請填入規格內容";
                    return false;
                }
                if (data.CatgName.Replace("_", "").Length > 35)
                {
                    data.Memo = "規格名稱 (每個規格最多35個字)";
                    return false;
                }
                if (data.SpecName.Replace("_", "").Length > 35)
                {
                    data.Memo = "規格內容 (每個規格內容最多35個字)";
                    return false;
                }
            }
            #region Check Group
            if (group.Where(x => x.SpecType.ToLower() != data.SpecType.ToLower()).Count() > 0)
            {
                data.Memo = "同一群組編號，規格類型需一致";
                return false;
            }
            if (group.Where(x => x.ProductBrandName != data.ProductBrandName).Count() > 0)
            {
                data.Memo = "同一群組編號，品牌名稱需一致";
                return false;
            }
            if (group.Where(x => x.ProductName != data.ProductName).Count() > 0)
            {
                data.Memo = "同一群組編號，商品名稱需一致";
                return false;
            }
            if (group.Where(x => x.CatgName != data.CatgName).Count() > 0)
            {
                data.Memo = "同一群組編號，規格名稱需一致";
                return false;
            }

            if (!string.IsNullOrEmpty(data.CatgName))
            {
                string[] catgs = group.Select(x => x.CatgName).FirstOrDefault().Split("_");
                string[] specs = group.Select(x => x.SpecName).FirstOrDefault().Split("_");

                if (catgs.Length != specs.Length)
                {
                    data.Memo = string.Format("規格名稱({0})，規格名稱({1})分隔數量不一致", catgs.Length, specs.Length);
                    return false;
                }

                if (catgs.Length > 4)
                {
                    data.Memo = string.Format("規格名稱({0})至多分隔四個", catgs.Length);
                    return false;
                }
            }
            var repSpecName = group.Where(t => !string.IsNullOrEmpty(t.SpecName)).GroupBy(x => x.SpecName).Where(g => g.Count() > 1).Select(y => new { data = y.Key }).ToList();
            if (repSpecName.Count() >= 1)
            {
                data.Memo = string.Format("規格內容{0}重複輸入", repSpecName.FirstOrDefault().data);
                return false;
            }
            var repGTINs = group.Where(t => !string.IsNullOrEmpty(t.Gtins)).GroupBy(x => x.Gtins).Where(g => g.Count() > 1).Select(y => new { data = y.Key }).ToList();
            if (repGTINs.Count() >= 1)
            {
                data.Memo = string.Format("全球交易品項識別碼 (GTINs){0}重複輸入", repGTINs.FirstOrDefault().data);
                return false;
            }
            var repProductCode = group.Where(t => !string.IsNullOrEmpty(t.ProductCode)).GroupBy(x => x.ProductCode).Where(g => g.Count() > 1).Select(y => new { data = y.Key }).ToList();
            if (repProductCode.Count() >= 1)
            {
                data.Memo = string.Format("貨號{0}重複輸入", repProductCode.FirstOrDefault().data);
                return false;
            }

            #endregion Check Group

            #region Check DB

            if (vpis.Where(p => p.ProductBrandName == data.ProductBrandName.ToString() && p.ProductName == data.ProductName.ToString()).Count() > 0)
            {
                data.Memo = "商品資料重複匯入";
                return false;
            }
            //以貨號/Gtins判斷
            if (vpis.Where(x => !string.IsNullOrEmpty(x.ProductCode) && x.ProductCode == data.ProductCode).Count() > 0)
            {
                data.Memo = string.Format("({0})貨號資料重複匯入", data.ProductCode);
                return false;
            }
            if (vpis.Where(x => !string.IsNullOrEmpty(x.Gtins) && x.Gtins == data.Gtins).Count() > 0)
            {
                data.Memo = string.Format("({0})Gtins資料重複匯入", data.Gtins);
                return false;
            }
            if (!string.IsNullOrEmpty(data.Memo))
            {
                return false;
            }
            #endregion Check DB

            #endregion Check Error
            return true;
        }

        /// <summary>
        /// 儲存商品資料
        /// </summary>
        /// <param name="product"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        protected dynamic SaveProducts(ProductInfoModel product, string modifyId)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Begin]");
            Guid pid = Guid.Empty;
            List<Guid> itemGuids = new List<Guid>();
            Dictionary<Guid, string> syncBids = new Dictionary<Guid, string>();
            try
            {
                Guid.TryParse(product.ProductGuid.ToString(), out pid);
                if (pid == null || pid == Guid.Empty)
                {
                    pid = Guid.NewGuid();
                }

                if ((product.ProductBrandName + product.ProductName).Length > 25)
                {
                    return new
                    {
                        Pid = pid,
                        Sid = product.SellerGuid,
                        IsSuccess = false,
                        Message = string.Format("「{0}」 請勿超過25字", product.ProductBrandName + product.ProductName)
                    };
                }


                #region ProductInfo
                ProductInfo pi = pp.ProductInfoGet(pid);

                if (pi == null || !pi.IsLoaded)
                {
                    pi = new ProductInfo();
                    pi.Guid = pid;
                    pi.CreateId = modifyId;
                    pi.CreateTime = DateTime.Now;
                    pi.WarehouseType = product.WarehouseType;
                    pi.DataSource = ((int)ProductDataSource.KeyIn).ToString();
                }

                #region Check
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ViewProductItemListGetBySellerGuid][Begin]");
                ViewProductItemCollection vpis = pp.ViewProductItemListGetBySellerGuid(product.SellerGuid);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ViewProductItemListGetBySellerGuid][End]");
                bool chkFalg = true;
                string chkMesage = string.Empty;
                //品牌+商品名稱不可重複
                var pis = vpis.Where(x => x.InfoGuid != pi.Guid).Where(x => x.WarehouseType == pi.WarehouseType).ToList();
                if (pis.Where(x => x.ProductBrandName == product.ProductBrandName && x.ProductName == product.ProductName).FirstOrDefault() != null)
                {
                    chkFalg = false;
                    chkMesage = "商品名稱已存在";
                }
                //貨號不可重複
                foreach (var item in product.ProductItems)
                {
                    if (pis.Where(x => x.ProductCode == item.ProductCode && !string.IsNullOrEmpty(item.ProductCode)).FirstOrDefault() != null)
                    {
                        chkFalg = false;
                        chkMesage = "貨號已存在";
                    }
                    if (pis.Where(x => x.Gtins == item.GTINs && !string.IsNullOrEmpty(item.GTINs)).FirstOrDefault() != null)
                    {
                        chkFalg = false;
                        chkMesage = "原廠編號/ (GTINs)已存在";
                    }
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[檢查貨號][End]");
                //檢查本次內容不可重複規格/Gtins/貨號
                if (product.IsMulti)
                {
                    ProductItemCollection oriItems = pp.ProductItemListGetByProductGuid(product.ProductGuid);
                    foreach (ProductItem oriItem in oriItems)
                    {
                        var data = product.ProductItems
                            .Where(x => x.ProductNo == oriItem.ProductNo.ToString())
                            .FirstOrDefault();
                        if (data == null)
                        {
                            oriItem.ItemStatus = (int)ProductItemStatus.Deleted;
                            pp.ProductItemSet(oriItem);
                        }
                    }
                    List<string> chks_list = new List<string>();
                    foreach (var p in product.ProductItems)
                    {
                        foreach (var s in p.ProductOptions)
                        {
                            if (string.IsNullOrEmpty(s.ItemName.Trim()))
                            {
                                return new
                                {
                                    Pid = pid,
                                    Sid = product.SellerGuid,
                                    IsSuccess = false,
                                    Message = "規格不可為空"
                                };
                            }
                        }
                        string specName = string.Join(",", p.ProductOptions.Select(x => x.ItemName));
                        if (chks_list.Contains(specName))
                        {
                            return new
                            {
                                Pid = pid,
                                Sid = product.SellerGuid,
                                IsSuccess = false,
                                Message = string.Format("規格不可為重複「{0}」", specName)
                            };
                        }
                        else
                        {
                            chks_list.Add(specName);
                        }

                        string outSepcName = string.Empty;
                        if (CheckProductItemLength(product, specName, out outSepcName) == false)
                        {
                            return new
                            {
                                Pid = pid,
                                Sid = product.SellerGuid,
                                IsSuccess = false,
                                Message = string.Format("規格名稱「{0}」不可超過50字", outSepcName)
                            };
                        }
                    }
                    var repGTINs = product.ProductItems.Where(t => !string.IsNullOrEmpty(t.GTINs)).GroupBy(x => x.GTINs).Where(g => g.Count() > 1).Select(y => new { data = y.Key }).ToList();
                    if (repGTINs.Count() >= 1)
                    {
                        chkFalg = false;
                        chkMesage = string.Format("原廠編號/ (GTINs){0}重複輸入", repGTINs.FirstOrDefault().data);
                    }
                    var repProductCode = product.ProductItems.Where(t => !string.IsNullOrEmpty(t.ProductCode)).GroupBy(x => x.ProductCode).Where(g => g.Count() > 1).Select(y => new { data = y.Key }).ToList();
                    if (repProductCode.Count() >= 1)
                    {
                        chkFalg = false;
                        chkMesage = string.Format("貨號{0}重複輸入", repProductCode.FirstOrDefault().data);
                    }
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[檢查貨號2][End]");
                if (!chkFalg)
                {
                    return new
                    {
                        Pid = pid,
                        Sid = product.SellerGuid,
                        IsSuccess = false,
                        Message = chkMesage
                    };
                }
                #endregion Check


                pi.SellerGuid = product.SellerGuid;
                pi.IsMulti = product.IsMulti;
                pi.ProductBrandName = product.ProductBrandName;
                pi.ProductName = product.ProductName;
                pi.ModifyId = modifyId;
                pi.ModifyTime = DateTime.Now;
                pp.ProductInfoSet(pi);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProductInfoSet][End]");
                #endregion ProductInfo

                #region ProductItem
                ProductItemCollection pdis = pp.ProductItemListGetByProductGuid(pid);
                List<ProductItemModel> pits = product.ProductItems;
                List<int> changePid = new List<int>();
                int didx = 1;
                foreach (ProductItemModel pit in pits)
                {
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[A1][Begin]");
                    using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        bool oldInfo = false;
                        ProductItem pdi = pdis.Where(x => x.ProductNo.ToString() == pit.ProductNo).FirstOrDefault();
                        if (pdi != null)
                        {
                            //對到17Life編號
                            oldInfo = true;
                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(pit.ProductCode))
                            {
                                pdi = pdis.Where(x => x.ProductCode == pit.ProductCode).FirstOrDefault();
                                if (pdi != null)
                                {
                                    //對到原廠貨號
                                    oldInfo = true;
                                }
                                else
                                {
                                    //找不到貨號就算新的
                                }
                            }
                            else
                            {
                                //用規格名稱比對(多規格才需要比較規格)
                                if (product.IsMulti)
                                {
                                    List<ProductSpec> specs = pp.ProductSpecListGetByItemGuid(pit.Gid).ToList();

                                    var data1 = new JsonSerializer().Serialize(specs.Select(x => x.SpecName));
                                    var data2 = new JsonSerializer().Serialize(pit.ProductOptions.Select(x => x.ItemName));
                                    if (data1 == data2)
                                    {
                                        oldInfo = true;
                                    }
                                }
                            }
                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[A2]");
                        if (oldInfo == false)
                        {
                            int safetyStock = pit.SafetyStock;
                            if (safetyStock == 0 && pit.OriStock > 0)
                            {
                                safetyStock = pit.OriStock / 10;
                            }
                            pdi = new ProductItem()
                            {
                                InfoGuid = pid,
                                Guid = Guid.NewGuid(),
                                ProductCode = pit.ProductCode,
                                Gtins = pit.GTINs,
                                Mpn = pit.Mpn,
                                OriStock = pit.OriStock,
                                Stock = pit.OriStock,
                                Sales = default(int),
                                SafetyStock = safetyStock,
                                Sort = didx,
                                ItemStatus = (int)ProductItemStatus.Normal,
                                SpecName = pi.IsMulti ? string.Join("_", pit.ProductOptions.OrderBy(y => y.Index).Select(x => x.ItemName)) : (pi.ProductBrandName + pi.ProductName),
                                ShelfLife = pit.ShelfLife,
                                WarehouseType = pit.WarehouseType,
                                Price = pit.Price,
                                CreateId = modifyId,
                                CreateTime = DateTime.Now,
                                ModifyId = modifyId,
                                ModifyTime = DateTime.Now

                            };
                            pp.ProductItemSet(pdi);

                            #region ProductSpec
                            int idx = 1;
                            if (product.IsMulti)
                            {
                                foreach (ProductOptionModel ci in pit.ProductOptions)
                                {
                                    ProductSpec spec = new ProductSpec()
                                    {
                                        ItemGuid = pdi.Guid,
                                        Guid = Guid.NewGuid(),
                                        CatgName = ci.CatgName,
                                        SpecName = ci.ItemName,
                                        SpecStatus = (int)ProductItemStatus.Normal,
                                        Sort = idx,
                                        CreateId = modifyId,
                                        CreateTime = DateTime.Now,
                                        ModifyId = modifyId,
                                        ModifyTime = DateTime.Now
                                    };
                                    pp.ProductSpecSet(spec);
                                    idx++;
                                }
                            }
                            #endregion ProductSpec
                        }
                        else
                        {
                            #region 檢查庫存是否異動
                            int adjustStock = pit.AdjustType == "+" ? pdi.Stock + pit.Stock : pdi.Stock - pit.Stock;
                            if (adjustStock < 0)
                            {
                                return new
                                {
                                    Pid = pid,
                                    Sid = product.SellerGuid,
                                    IsSuccess = false,
                                    Message = "庫存不可小於0"
                                };
                            }
                            //if (pdi.Stock != pit.Stock)
                            if (pdi.Stock != adjustStock)
                            {
                                //pdi.Stock pit.Stock
                                ProductItemStock stock = new ProductItemStock()
                                {
                                    Guid = Guid.NewGuid(),
                                    InfoGuid = product.ProductGuid,
                                    ItemGuid = pdi.Guid,
                                    BeginningStock = pdi.Stock,
                                    AdjustStock = pit.Stock,
                                    EndingStock = adjustStock,
                                    CreateId = modifyId,
                                    CreateTime = DateTime.Now,
                                    ModifyId = modifyId,
                                    ModifyTime = DateTime.Now
                                };
                                pp.ProductItemStockSet(stock);

                                itemGuids.Add(pdi.Guid);
                            }
                            #endregion 檢查庫存是否異動

                            int safetyStock = pit.SafetyStock;
                            if (safetyStock == 0 && adjustStock > 0)
                            {
                                safetyStock = adjustStock / 10;
                            }
                            pdi.ProductCode = pit.ProductCode;
                            pdi.Gtins = pit.GTINs;
                            pdi.Mpn = pit.Mpn;
                            //pdi.OriStock = detail.Stock;
                            pdi.Stock = adjustStock;
                            pdi.SafetyStock = safetyStock;
                            pdi.Sort = didx;
                            pdi.SpecName = string.Join("_", pit.ProductOptions.OrderBy(y => y.Index).Select(x => x.ItemName));
                            pdi.ShelfLife = pit.ShelfLife;
                            pdi.WarehouseType = pit.WarehouseType;
                            pdi.Price = pit.Price;
                            pdi.ModifyId = modifyId;
                            pdi.ModifyTime = DateTime.Now;

                            pp.ProductItemSet(pdi);

                            #region ProductSpec
                            if (product.IsMulti)
                            {
                                ProductSpecCollection specs = pp.ProductSpecListGetByItemGuid(pdi.Guid);
                                if (specs.Count == 0)
                                {
                                    int idx = 1;
                                    foreach (ProductOptionModel ci in pit.ProductOptions)
                                    {
                                        ProductSpec spec = new ProductSpec()
                                        {
                                            ItemGuid = pdi.Guid,
                                            Guid = Guid.NewGuid(),
                                            CatgName = ci.CatgName,
                                            SpecName = ci.ItemName,
                                            SpecStatus = (int)ProductItemStatus.Normal,
                                            Sort = idx,
                                            CreateId = modifyId,
                                            CreateTime = DateTime.Now,
                                            ModifyId = modifyId,
                                            ModifyTime = DateTime.Now
                                        };
                                        pp.ProductSpecSet(spec);
                                        idx++;
                                    }
                                }
                                else
                                {
                                    foreach (var spec in specs)
                                    {
                                        spec.SpecStatus = (int)ProductItemStatus.Deleted;
                                        spec.ModifyId = modifyId;
                                        spec.ModifyTime = DateTime.Now;
                                        pp.ProductSpecSet(spec);
                                    }
                                    int idx = 1;
                                    foreach (ProductOptionModel ci in pit.ProductOptions)
                                    {
                                        ProductSpec spec = specs.Where(x => x.Guid.Equals(ci.Gid)).FirstOrDefault();
                                        if (spec == null)
                                        {
                                            spec = specs.Where(x => x.SpecName.Equals(ci.ItemName)).FirstOrDefault();
                                        }
                                        if (spec == null)
                                        {
                                            spec = new ProductSpec();
                                            spec.Guid = Guid.NewGuid();
                                        }
                                        spec.ItemGuid = pdi.Guid;
                                        spec.CatgName = ci.CatgName;
                                        spec.SpecName = ci.ItemName;
                                        spec.SpecStatus = (int)ProductItemStatus.Normal;
                                        spec.Sort = idx;
                                        spec.ModifyId = modifyId;
                                        spec.ModifyTime = DateTime.Now;
                                        pp.ProductSpecSet(spec);
                                        idx++;
                                    }
                                }
                            }
                            #endregion ProductSpec
                        }
                        didx++;

                        #region 同步相關檔次 
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[同步相關檔次][Begin]");
                        //有用到該規格的提案單專案內容
                        ProposalMultiOptionSpecCollection pmosc = sp.ProposalMultiOptionSpecGetByProductItem(product.ProductGuid, pdi.Guid);
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProposalMultiOptionSpecGetByProductItem][End]");
                        foreach (ProposalMultiOptionSpec pmos in pmosc)
                        {
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[pmos]");
                            int MultiOptionId = pmos.MultiOptionId;
                            ProposalMultiDeal pmd = sp.ProposalMultiDealGetById(MultiOptionId);
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[pmos][1]");
                            Guid bid = Guid.Empty;
                            if (Guid.TryParse(pmd.Bid.ToString(), out bid))
                            {
                                //更新ProposalMultiDeal
                                //List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options);
                                //List<ProductInfo> products = pp.ProductInfoListGet(specs.SelectMany(x => x.Items)
                                //                                    .Select(y => y.product_guid).ToList()).ToList();


                                //List<ProductItem> items = pp.ProductItemGetList(specs.SelectMany(x => x.Items)
                                //                                    .Select(y => y.item_guid).ToList()).ToList();
                                //foreach (ProposalMultiDealsItem item in specs.SelectMany(x => x.Items))
                                //{
                                //    ProductInfo productInfo = products.Where(x => x.Guid == item.product_guid).FirstOrDefault();

                                //    List<ProductItem> productItem = items.Where(x => x.Guid == item.item_guid).ToList();


                                //    if (product != null)
                                //    {
                                //        if (product.IsMulti == false)
                                //        {
                                //            item.name = product.ProductBrandName + product.ProductName;
                                //        }
                                //        else
                                //        {
                                //            item.name = product.ProductBrandName + product.ProductName + "_" + string.Join("_", productItem.Select(x => x.SpecName));
                                //        }
                                //    }
                                //}
                                //ProposalMultiDealCollection oriPmdc = sp.ProposalMultiDealGetByPid(pmd.Pid);

                                //pmd.Options = new JsonSerializer().Serialize(specs).Replace("Items", "items").Replace("Sort", "sort");
                                //pmd.SellerOptions = new JsonSerializer().Serialize(specs).Replace("Items", "items").Replace("Sort", "sort");
                                //sp.ProposalMultiDealsSet(pmd);

                                //ProposalMultiDealCollection pmdc = sp.ProposalMultiDealGetByPid(pmd.Pid);


                                //更新PponOption
                                if (pi.IsMulti)
                                {
                                    if (!syncBids.ContainsKey(bid))
                                    {
                                        syncBids.Add(bid, pmd.Options);
                                    }
                                }

                                //log
                                //ProposalFacade.CompareMultiDeals(pmd.Pid, pmdc, oriPmdc, UserName, ProposalSourceType.House, ProposalLogType.Initial);

                                if (!changePid.Contains(pmd.Pid))
                                    changePid.Add(pmd.Pid);
                            }

                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[同步相關檔次][End]");

                        #endregion
                        ts.Complete();
                    }
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[A1][End]");
                }

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[同步庫存]");
                foreach (int PrpopsalId in changePid)
                {
                    ProposalFacade.ProposalLog(PrpopsalId, "同步庫存", UserName);
                }

                #endregion ProductItem

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[CompareProductModel]");
                ProposalFacade.CompareProductModel(product, pid, modifyId);

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProcessMultiSpecToPponOption][Begin]");
                foreach (var sync in syncBids)
                {
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[sync.Key]" + sync.Key);
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[sync.Value]" + sync.Value);
                    ProposalFacade.ProcessMultiSpecToPponOption(sync.Value, sync.Key, UserName);
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProcessMultiSpecToPponOption][End]");
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[ProcessMultiSpecToPponOption][End]");

                //更新最大購買數量
                //foreach(Guid gid in itemGuids)
                //{
                //    PponFacade.UpdateDealMaxItemCount(null, gid); //
                //}
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[UpdateDealAllMaxItemCount]");
                PponFacade.UpdateDealAllMaxItemCount(product.ProductGuid);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[End]");
                ProposalFacade.ProposalPerformanceLogSet("SaveProducts", builder.ToString(), UserName);

                return new
                {
                    Pid = pid,
                    Sid = product.SellerGuid,
                    IsSuccess = true,
                    Message = string.Empty
                };
            }
            catch (Exception ex)
            {
                ProposalFacade.ProposalPerformanceLogSet("SaveProducts", ex.Message, UserName);
                logger.Error(ex);
                return new
                {
                    Pid = pid,
                    Sid = product.SellerGuid,
                    IsSuccess = false,
                    Message = "請聯絡IT人員"
                };
            }
        }

        private bool CheckProductItemLength(ProductInfoModel product, string specName, out string outSepcName)
        {
            bool flag = true;
            string newSpecName = string.Empty;
            if ((product.ProductBrandName + product.ProductName + "_" + specName).Length < 45)
            {
                newSpecName = product.ProductBrandName + product.ProductName;
                if (!string.IsNullOrEmpty(newSpecName))
                {
                    newSpecName += "_" + specName; ;
                }
            }
            else if ((product.ProductName + specName).Length < 45)
            {
                newSpecName = product.ProductName;
                if (!string.IsNullOrEmpty(specName))
                {
                    newSpecName += "_" + specName;
                }
            }
            if (string.IsNullOrEmpty(newSpecName))
            {
                newSpecName = product.ProductName;
                if (!string.IsNullOrEmpty(specName))
                {
                    newSpecName += "_" + specName;
                }
            }

            if (newSpecName.Length >= 50)
            {
                flag = false;
            }
            outSepcName = newSpecName;
            return flag;
        }

        private bool CheckEngNum(string sPattern)
        {
            Regex regexPass = new Regex(@"^[0-9a-zA-Z]$", RegexOptions.IgnoreCase);
            return regexPass.Match(sPattern).Success;
        }
        private bool CheckNumber(string sPattern)
        {
            Regex rexgexNumber = new Regex(@"\D");
            return rexgexNumber.Match(sPattern).Success;
        }
        private void SpreadItems(
            List<ProductItemSourceModel> psms,
            List<SerializeableCategoryItem> oplist,
            List<ItemCategory> ori,
            int arrLen)
        {
            foreach (ProductItemSourceModel psm in psms)
            {
                foreach (ItemCategory item in psm.Items)
                {
                    List<ItemCategory> ics = ori.ToList();
                    ics.Add(new ItemCategory
                    {
                        OptionGuid = Guid.NewGuid(),
                        OptionName = item.OptionName
                    });
                    var data = psms.Where(x => x.CatgName != psm.CatgName && x.Index > psm.Index).ToList();
                    if (data.Count > 0)
                    {
                        SpreadItems(data, oplist, ics, arrLen);
                    }
                    else
                    {
                        if (ics.Count == arrLen)
                        {
                            oplist.Add(new SerializeableCategoryItem
                            {
                                Id = Guid.NewGuid(),
                                Items = ics
                            });
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 取得宅配業務清單
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        protected ActionResult GetBaseEmployeeNameArray(string empName)
        {
            empName = empName.Replace("'", "").Replace("<", "").Replace(">", "").Replace("\\", "");

            //宅配部門
            List<Department> dept = new List<Department>();
            dept.Add(HumanFacade.GetDepartmentByDeptId(EmployeeChildDept.S010.ToString()));


            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(empName) && empName.Length >= 1 && empName.Length < 6)
            {
                var emps = SellerFacade.GetEmployeeNameArrayByDept(empName, dept);
                foreach (var e in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = e.Key.ToString(),
                        Label = e.Value
                    });
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得宅配員工資料
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        protected ActionResult GetBaseEmployeeName(string empName)
        {
            if (!string.IsNullOrEmpty(empName))
            {
                var emp = SellerFacade.GetToHouseSalesmanName(empName).FirstOrDefault();
                return Json(emp, JsonRequestBehavior.AllowGet);
            }
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 刪除提案單
        /// </summary>
        /// <returns></returns>
        protected ActionResult DeleteBaseProposal(int pid, string modifyId)
        {
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    ProposalFacade.DeleteProposal(pid, pro.BusinessHourGuid, modifyId);
                }
            }
            return Json(true);
        }

        /// <summary>
        /// 複製提案單
        /// </summary>
        /// <returns></returns>
        protected ActionResult CloneBaseProposal(int pid, ProposalCopyType proposalCopyType, bool IsSellerProposal, string modifyId, bool isWms)
        {
            int proId = default(int);
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                proId = PponFacade.CreateProposalByCloneSellerPro(pid, proposalCopyType, modifyId, IsSellerProposal, isWms);
            }
            return Json(proId, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 檢查商品是否已被提案單使用
        /// </summary>
        /// <returns></returns>
        protected bool CheckProductUsedByProposal(string item_guid, out string message)
        {
            Guid gid = Guid.Empty;
            Guid.TryParse(item_guid, out gid);
            ProposalMultiOptionSpecCollection specs = sp.ProposalMultiOptionSpecGetByProduct(gid);
            if (specs.Count() > 0)
            {
                ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetListById(specs.Select(x => x.MultiOptionId).ToList());
                message = string.Join(",", pmds.Select(x => x.Pid).ToList());
                return true;
            }
            message = string.Empty;
            return false;
        }

        protected bool CheckProductUsedWmsPurchaseOrder(string item_guid)
        {
            Guid gid = Guid.Empty;
            Guid.TryParse(item_guid, out gid);
            List<Guid> guids = new List<Guid>();
            guids.Add(gid);
            ViewWmsPurchaseOrderCollection vwpoc = wp.ViewWmsPurchaseOrderListGetByItemGuids(guids);
            if (vwpoc.Count() > 0)
            {
                return true;
            }
            return false;
        }

        protected bool CheckItemUsedByProposal(string item_guid, out string message)
        {
            Guid gid = Guid.Empty;
            Guid.TryParse(item_guid, out gid);
            ProposalMultiOptionSpecCollection specs = sp.ProposalMultiOptionSpecGetByItem(gid);
            if (specs.Count() > 0)
            {
                ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetListById(specs.Select(x => x.MultiOptionId).ToList());
                message = string.Join(",", pmds.Select(x => x.Pid).ToList());
                return true;
            }
            message = string.Empty;
            return false;
        }

        /// <summary>
        /// 檢查提案送審資料是否填寫完整
        /// </summary>
        /// <returns></returns>
        protected bool CheckSendData(Proposal pro, out string message)
        {
            if (pro.OperationSalesId == null || pro.OperationSalesId == 0)
            {
                message = "請輸入負責業務";
                return false;
            }
            if (string.IsNullOrEmpty(pro.BrandName.Trim()))
            {
                //message = "請輸入品牌名稱";
                //return false;
            }
            if (string.IsNullOrEmpty(pro.DealName.Trim()))
            {
                message = "請輸入商品名稱";
                return false;
            }
            if (pro.DealType1.GetValueOrDefault(0) == 0)
            {
                message = "請輸入檔次類型";
                return false;
            }
            if (pro.VendorReceiptType == null)
            {
                message = "請選擇請款單據";
                return false;
            }
            else
            {
                if (pro.VendorReceiptType.Value == (int)VendorReceiptType.Other)
                {
                    if (string.IsNullOrEmpty(pro.Othermessage.Trim()))
                    {
                        message = "請款單據請填寫其他方式";
                        return false;
                    }
                }
            }
            if (pro.DeliveryMethod == null)
            {
                message = "請選擇配送溫層";
                return false;
            }
            if (pro.DealSource == null)
            {
                //message = "請選擇商品來源";
                //return false;
            }
            else
            {
                if (pro.DealSource.Value == (int)ProposalDealSource.ParallelImportation)
                {
                    ProposalSellerFileCollection psf = sp.ProposalSellerFileListGet(pro.Id);
                    if (psf.Count() == 0)
                    {
                        message = "商品來源為「平行輸入」，請協助上傳切結書";
                        return false;
                    }
                }
            }
            if (pro.ShipType == (int)DealShipType.Other)
            {
                if (pro.DeliveryTimeS == null || pro.DeliveryTimeE == null)
                {
                    message = "出貨類型選擇「其他」，請填寫配送時間";
                    return false;
                }
                if (pro.DeliveryTimeE != null)
                {
                    if (pro.DeliveryTimeE.Value < DateTime.Now)
                    {
                        message = "出貨類型，「最晚出貨日」不可小於今日";
                        return false;
                    }
                }
            }


            ProposalMultiDealCollection multiDeals = sp.ProposalMultiDealGetByPid(pro.Id);
            if (multiDeals.Count == 0)
            {
                message = "至少填寫一項銷售方案";
                return false;
            }
            else
            {
                string dealMsg = string.Empty;
                int idx = 1;
                foreach (ProposalMultiDeal deal in multiDeals)
                {
                    //if (string.IsNullOrEmpty(deal.BrandName))
                    //{
                    //    dealMsg += "品牌名稱/";
                    //}
                    if (string.IsNullOrEmpty(deal.ItemName))
                    {
                        dealMsg += "商品名稱/";
                    }
                    if (string.IsNullOrEmpty(deal.QuantityMultiplier))
                    {
                        dealMsg += "數字/";
                    }
                    if (string.IsNullOrEmpty(deal.Unit))
                    {
                        dealMsg += "單位/";
                    }
                    //if (deal.OrigPrice == 0)
                    //{
                    //    dealMsg += "巿價/";
                    //}
                    //if (deal.ItemPrice == 0)
                    //{
                    //    dealMsg += "建議售價/";
                    //}
                    //if (deal.Cost == 0)
                    //{
                    //    dealMsg += "進貨價/";
                    //}
                    if (deal.OrderTotalLimit == 0)
                    {
                        dealMsg += "每人限購需大於0/";
                    }
                    //if (deal.Freights == 0)
                    //{
                    //    dealMsg += "運費/";
                    //}
                    if (!string.IsNullOrEmpty(dealMsg))
                    {
                        dealMsg = string.Format("{0}{1}", "方案" + idx + "請輸入：", dealMsg.Substring(0, dealMsg.Length - 1));
                    }
                    bool isMulti = ProposalFacade.IsProposalMultiSpec(deal.Options);
                    if (!isMulti)
                    {
                        if (deal.ComboPackCount != "1")
                        {
                            dealMsg += "單規格商品，結帳倍數請填入1/";
                        }
                    }
                    idx++;
                }
                if (!string.IsNullOrEmpty(dealMsg))
                {
                    message = dealMsg;
                    return false;
                }

                foreach (var deal in multiDeals)
                {
                    if (string.IsNullOrEmpty(deal.Options) || deal.Options.Length < 5)
                    {
                        message = string.Format("{0}尚未選擇「銷售商品與規格」", deal.ItemName);
                        return false;
                    }
                }
            }

            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
            if (string.IsNullOrEmpty(pcec.Description))
            {
                message = "請輸入商品詳細介紹";
                return false;
            }
            else
            {
                if (pcec.Description.Length < 50)
                {
                    message = "商品詳細介紹字數至少50字元";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(pcec.Title.Trim()))
            {
                message = "請輸入檔次行銷文案";
                return false;
            }
            if (string.IsNullOrEmpty(pcec.ProductSpec))
            {
                message = "請輸入商品規格";
                return false;
            }
            else
            {
                if (pcec.ProductSpec.Length < 50)
                {
                    message = "商品規格字數至少50字元";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(pcec.Remark))
            {
                message = "請輸入必買特色";
                return false;
            }
            else
            {
                if (pcec.Remark.Length < 10)
                {
                    message = "必買特色字數至少10字元";
                    return false;
                }
            }
            if (string.IsNullOrEmpty(pcec.ImagePath))
            {
                message = "請上傳「電腦版網頁主圖」";
                return false;
            }
            if (string.IsNullOrEmpty(pcec.AppDealPic))
            {
                message = "請上傳「APP/行動版網頁主圖」";
                return false;
            }

            if (string.IsNullOrEmpty(pcec.RemoveBgPic))
            {
                message = "請上傳「Google廣告去背圖」";
                return false;
            }

            if (pro.ShipType == (int)DealShipType.Other && pro.DeliveryTimeS == null && pro.DeliveryTimeE == null)
            {
                message = "出貨類型選擇其他時請一併輸入配送時間";
                return false;
            }

            bool isDealOn = false;
            if (pro.BusinessHourGuid != null)
            {
                BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                if (bh.IsLoaded)
                {
                    if (bh.BusinessHourOrderTimeS <= DateTime.Now && bh.BusinessHourOrderTimeE >= DateTime.Now)
                    {
                        isDealOn = true;
                    }
                }
            }
            if (!isDealOn)
            {
                bool isStock = CheckMultiOptionSpecStock(pro.Id, out message);
                if (!isStock)
                {
                    return false;
                }
            }

            message = string.Empty;
            return true;
        }


        public string GetRestriction(Proposal pro)
        {
            string html = string.Empty;

            //配送時間
            if (pro.ShipType == (int)DealShipType.Ship72Hrs)
            {
                html += "●配送時間：商品最晚於購買成功日後24小時內出貨完畢(不含例假日)，出貨後配送約2~3個工作日，恕無法指定送達時間<br />";
            }
            else if (pro.ShipType == (int)DealShipType.Normal)
            {
                html += "●配送時間：商品最晚於購買成功日後" + pro.ShipText3 + "個工作日內出貨完畢(不含例假日)，出貨後配送約2~3個工作日，恕無法指定送達時間<br />";
            }
            else if (pro.ShipType == (int)DealShipType.Wms)
            {
                html += "●配送時間：訂單成立後24小時內到貨。<a href='https://www.17life.com/ppon/newbieguide.aspx?s=20662&q=20665' target='_blank'><span style='color:#0000cd;'>例外說明</span></a><br />";
                html += "●配送方式：商品透過PChome物流配送。除網頁另有特別標示外，均為常溫配送。<br />";
                html += "●商品出貨速度快，恕無法「取消訂單」亦無法「變更配送地址」，若收件人資訊不完整或配送不成功，商品退回後訂單將自動進行退貨及退款。<br />";
            }
            else if (pro.ShipType == (int)DealShipType.Other)
            {
                if (pro.DeliveryTimeS == null || pro.DeliveryTimeE == null)
                    html += "●配送時間：商品將於yyyy/mm/dd起開始陸續出貨，最晚將於yyyy/mm/dd前出貨完畢；出貨後配送約2~3個工作日，恕無法指定配達時間<br />";
                else
                    html += "●配送時間：商品將於" + pro.DeliveryTimeS.Value.ToString("yyyy/MM/dd") + "起開始陸續出貨，最晚將於" + pro.DeliveryTimeE.Value.ToString("yyyy/MM/dd") + "前出貨完畢；出貨後配送約2~3個工作日，恕無法指定配達時間<br />";
            }


            html += "<br />";
            //配送方式
            if (pro.DeliveryMethod != null)
            {
                html += "●配送方式：" + Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDeliveryMethod)pro.DeliveryMethod) + "宅配<br /><br />";
            }
            //運費
            ProposalMultiDealCollection pmdc = sp.ProposalMultiDealGetByPid(pro.Id);
            if (pmdc.Count() == 1)
            {
                //單一檔次
                if (pmdc[0].Freights != 0)
                {
                    if ((pmdc[0].NoFreightLimit - 1) > 1)
                    {
                        html += "●運費計算：1 ~ " + (pmdc[0].NoFreightLimit - 1).ToString() + "組運費";
                    }
                    else
                    {
                        html += "●運費計算：1 組運費";
                    }
                    html += Convert.ToInt32(pmdc[0].Freights) + "元 / 次，購滿" + pmdc[0].NoFreightLimit + "組可享免運優惠(限同一地址)<br />";
                }
            }
            else
            {
                //多檔次
                string[] Code = { "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P",
                                           "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "a", "b", "c", "d",
                                           "e", "f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "u",
                                           "v", "w", "x", "y", "z"};
                int idx = 0;
                string htmlMulti = string.Empty;
                foreach (var pmd in pmdc)
                {
                    if (pmd.Freights != 0)
                    {
                        if ((pmd.NoFreightLimit - 1) > 1)
                        {
                            if (htmlMulti.IndexOf("運費計算") >= 0)
                            {
                                htmlMulti += "(" + Code[idx] + ")1 ~ " + (pmd.NoFreightLimit - 1).ToString() + "組運費";
                            }
                            else
                            {
                                if (htmlMulti.IndexOf("運費計算") >= 0)
                                {
                                    htmlMulti += "(" + Code[idx] + ")1 ~ " + (pmd.NoFreightLimit - 1).ToString() + "組運費";
                                }
                                else
                                {
                                    htmlMulti += "●運費計算：(" + Code[idx] + ")1 ~ " + (pmd.NoFreightLimit - 1).ToString() + "組運費";
                                }
                            }
                        }
                        else
                        {
                            if (htmlMulti.IndexOf("運費計算") >= 0)
                            {
                                htmlMulti += "(" + Code[idx] + ")1 組運費";
                            }
                            else
                            {
                                htmlMulti += "●運費計算：(" + Code[idx] + ")1 組運費";
                            }
                        }
                        htmlMulti += Convert.ToInt32(pmd.Freights) + "元/次，購滿" + pmd.NoFreightLimit + "組可享免運優惠(限同一地址)/";
                    }
                    idx++;
                }

                if (!string.IsNullOrEmpty(htmlMulti))
                    html += htmlMulti.TrimEnd("/") + "<br />";
            }

            //配送區域
            if (!pro.DeliveryIslands)
                html += "●此宅配商品恕不適用離島、外島地區<br /><br />";

            //猶豫期
            if (pro.TrialPeriod == null || !pro.TrialPeriod.Value)
                html += "●本商品<span style='font-weight:bold;'>可享七天猶豫期<span style='color:red;'>（注意！猶豫期非試用期）</span></span>，辦理退貨時，商品必須為全新、未拆封、包裝完整，否則將會影響您退貨的權益，看<a href='https://www.17life.com/ppon/newbieguide.aspx?s=20603' target='_blank'><span style='color:#0000cd;'>更多詳細說明。</span></a><br /><br />";
            else
                html += "●本商品<span style='font-weight:bold;'>排除七天猶豫期<span style='color:red;'>（注意！猶豫期非試用期）</span></span>，辦理退貨時，商品必須為全新、未拆封、包裝完整，否則將會影響您退貨的權益，看<a href='https://www.17life.com/ppon/newbieguide.aspx?s=20603' target='_blank'><span style='color:#0000cd;'>更多詳細說明。</span></a><br /><br />";

            html += "●請您於收到商品盡速檢查，若有毀損、異常等瑕疵之情形，欲更換或退貨，請注意：<br />";
            html += ">>請將貨品原狀（包裝留存）拍照提供給客服中心，並將該商品妥善保存；若瑕疵商品為冷凍、冷藏、生鮮等食品請於收到商品後<span style='color:red;'>24小時內聯絡</span><a href='https://www.17life.com/User/ServiceList' target='_blank'><span style='color:#0000cd;'>客服中心。</span></a>該商品請以冷凍或冷藏方式妥善儲存，商品未妥善儲存恐影響您退貨之權益，看<a href='https://www.17life.com/ppon/newbieguide.aspx?s=20603&q=20678' target='_blank'><span style='color:#0000cd;'>更多詳細說明。</span></a><br />";
            html += ">>請務必留下可配合隨時收貨之聯絡電話與地址，以加速您退換貨之流程。<br /><br />";
            html += "●網頁商品顏色可能因各顯示器設定而使圖片呈現略有差異，請以實品顏色為準。";
            return html;
        }

        protected bool IsInSystemFunctionPrivilege(string email, SystemFunctionType type, string path)
        {
            return CommonFacade.IsInSystemFunctionPrivilege(email, type, path);
        }


        /// <summary>
        /// 新版宅配單檔自動長母子檔結構
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="userName"></param>
        private void ChangeToComboDeals(Proposal pro, string userName)
        {
            if (pro.BusinessHourGuid != null)
            {
                Guid mainBid = pro.BusinessHourGuid.Value;
                BusinessHour bh = pp.BusinessHourGet(mainBid);
                if (bh.IsLoaded)
                {
                    GroupOrder go = op.GroupOrderGetByBid(mainBid);
                    if (go.IsLoaded)
                    {
                        //已結檔之檔次不再異動
                        if (go.Slug != null || Helper.IsFlagSet(go.Status.GetValueOrDefault(0), GroupOrderStatus.Completed))
                        {
                            return;
                        }
                    }
                    ComboDealCollection cdc = pp.GetComboDealByBid(mainBid, false);
                    if (cdc.Count == 0)
                    {
                        ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                        if (multiDeals.Count() == 1)
                        {
                            //單檔全部變為母子檔
                            string errorMessages;
                            Guid returnNewGuid;
                            PponFacade.CopyDeal(mainBid, false, false, out errorMessages, out returnNewGuid, userName);
                            Item itemSub = pp.ItemGetByBid(mainBid);

                            #region ComboDeal
                            ComboDeal cdMain = new ComboDeal()
                            {
                                MainBusinessHourGuid = returnNewGuid,
                                BusinessHourGuid = returnNewGuid,
                                Title = "主檔",
                                Sequence = -1,
                                Creator = userName,
                                CreateTime = DateTime.Now
                            };
                            pp.AddComboDeal(cdMain);

                            ComboDeal cdSub = new ComboDeal()
                            {
                                MainBusinessHourGuid = returnNewGuid,
                                BusinessHourGuid = mainBid,
                                Title = itemSub.ItemName,
                                Sequence = 1,
                                Creator = userName,
                                CreateTime = DateTime.Now
                            };
                            pp.AddComboDeal(cdSub);
                            #endregion ComboDeal

                            #region BusinessHour
                            BusinessHour bhMain = pp.BusinessHourGet(returnNewGuid);
                            bhMain.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(true, bhMain.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                            bhMain.ModifyTime = DateTime.Now;
                            bhMain.ModifyId = userName;
                            pp.BusinessHourSet(bhMain);

                            BusinessHour bhSub = pp.BusinessHourGet(mainBid);
                            bhSub.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(false, bhSub.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
                            bhSub.BusinessHourStatus = Convert.ToInt32(Helper.SetFlag(true, bhSub.BusinessHourStatus, BusinessHourStatus.ComboDealSub));
                            bhSub.ModifyTime = DateTime.Now;
                            bhSub.ModifyId = userName;
                            pp.BusinessHourSet(bhSub);
                            #endregion BusinessHour

                            #region DealCost                            
                            pp.DealCostDeleteListByBid(bhMain.Guid);
                            DealCostCollection dcc = new DealCostCollection();
                            dcc.Add(new DealCost() { BusinessHourGuid = bhMain.Guid, Cost = 1, Quantity = 99999, CumulativeQuantity = 99999, LowerCumulativeQuantity = 0 });
                            pp.DealCostSetList(dcc);
                            #endregion DealCost

                            #region DealAccounting
                            DealAccounting mainDA = pp.DealAccountingGet(returnNewGuid);
                            DealAccounting subDA = pp.DealAccountingGet(mainBid);
                            if (mainDA.RemittanceType != subDA.RemittanceType)
                            {
                                mainDA.RemittanceType = subDA.RemittanceType;
                                pp.DealAccountingSet(mainDA);
                            }
                            #endregion DealAccounting


                            #region ProposalMultiOptionSpec
                            //把規格改成新的母檔bid，不然庫存會扣不到
                            ProposalMultiOptionSpecCollection specList = sp.ProposalMultiOptionSpecGetByBid(mainBid);
                            foreach (ProposalMultiOptionSpec spec in specList)
                            {
                                spec.BusinessHourGuid = bhMain.Guid;
                                sp.ProposalMultiOptionSpecSet(spec);
                            }
                            #endregion ProposalMultiOptionSpec

                            #region Proposal
                            pro.BusinessHourGuid = bhMain.Guid;
                            mainBid = pro.BusinessHourGuid.Value;
                            sp.ProposalSet(pro);

                            ProposalFacade.ProposalLog(pro.Id, "單檔轉母子檔", UserName);
                            #endregion Proposal
                        }
                    }
                }
            }
        }
        #endregion private method
    }
}

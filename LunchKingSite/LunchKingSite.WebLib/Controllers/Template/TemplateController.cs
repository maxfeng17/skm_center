﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BookingSystem;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.BookingSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Winnovative.WnvHtmlConvert;


namespace LunchKingSite.WebLib.Controllers.Template
{
    public class TemplateController : BaseController
    {
        private static IMemberProvider _mp;
        private static IBookingSystemProvider _bp;
        private static ISellerProvider _sp;
        private static IOrderProvider _op;
        private static IHiDealProvider _hp;
        private static ISysConfProvider _conf;

        static TemplateController()
        {
            _mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
            _bp = ProviderFactory.Instance().GetDefaultProvider<IBookingSystemProvider>();
            _sp = ProviderFactory.Instance().GetDefaultProvider<ISellerProvider>();
            _op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
            _hp = ProviderFactory.Instance().GetDefaultProvider<IHiDealProvider>();
            _conf = ProviderFactory.Instance().GetDefaultProvider<ISysConfProvider>();
        }

        [HttpGet]
        public ActionResult ApplicationForReturnPurchaseForm(int pcptransactionorderid)
        {
            //web顯示
            //ViewData["content"] = MvcHtmlString.Create(PcpTransactionUtility.ApplicationForReturnPurchaseFormPrint(pcptransactionorderid));
            //return View("TemplateContentOutput");

            var formContent = PcpTransactionUtility.ApplicationForReturnPurchaseFormPrint(pcptransactionorderid);
            return GenerateContentPdfFile("ApplicationForReturnPurchaseForm", formContent);
        }


        [HttpGet]
        public ActionResult CertificateOfPurchaseReturnForm(int pcptransactionorderid)
        {
            //ViewData["content"] = MvcHtmlString.Create(PcpTransactionUtility.CertificateOfPurchaseReturnFormPrint(pcptransactionorderid));
            //return View("TemplateContentOutput");
            var formContent = PcpTransactionUtility.CertificateOfPurchaseReturnFormPrint(pcptransactionorderid);
            return GenerateContentPdfFile("CertificateOfPurchaseReturnForm", formContent);
        }

        private FileContentResult GenerateContentPdfFile(string formName,string formContent)
        {
            PdfConverter pdfc = new PdfConverter();
            pdfc.LicenseKey = @"YklTQlNCVFZCVExSQlFTTFNQTFtbW1s=";
            pdfc.PdfDocumentOptions.PdfPageSize = PdfPageSize.A4;
            pdfc.PdfDocumentOptions.PdfCompressionLevel = PdfCompressionLevel.Normal;
            pdfc.PdfDocumentOptions.PdfPageOrientation = PDFPageOrientation.Portrait;

            // disable unnecessary features to enhance performance
            pdfc.ScriptsEnabled = pdfc.ScriptsEnabledInImage = false;
            pdfc.ActiveXEnabled = pdfc.ActiveXEnabledInImage = false;
            pdfc.PdfDocumentOptions.JpegCompressionEnabled = false;

            //byte[] b = pdfc.GetPdfFromUrlBytes(WebUtility.GetSiteRoot() + requestUrl);
            byte[] formContentBytes = pdfc.GetPdfBytesFromHtmlString(formContent);

            return File(formContentBytes, "application/pdf", formName + ".pdf");

        }


    }
}

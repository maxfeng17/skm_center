﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.WebLib.Controllers
{
    public class SkmEventController : BaseController
    {
        private static IChannelEventProvider _cep = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();
        private static ISkmEfProvider _skmEf = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
        private static ISkmProvider _skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private static ISysConfProvider _cp = ProviderFactory.Instance().GetConfig();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static ILog logger = LogManager.GetLogger("SkmPrizeDraw");

        [HttpGet]
        [RequireHttps]
        public ActionResult Activity(string token)
        {
            #region check member

            var isSkmMember = false;
            var e7Member = MemberFacade.GetMember(User.Identity.Name);
            if (e7Member.IsLoaded)
            {
                var skmMember = _skm.MemberGetByUserId(e7Member.UniqueId);
                isSkmMember = skmMember.IsLoaded;
            }

            #endregion check member
            
            var model = new SkmActivityModel
            {
                UserName = User.Identity.Name,
                CardNo = GetDataFromHeaderOrQueryString("CardNo"),
                DeviceCode = GetDataFromHeaderOrQueryString("DeviceCode"),
                DeviceType = GetDataFromHeaderOrQueryString("DeviceType"),
                MemStatus = string.IsNullOrEmpty(GetDataFromHeaderOrQueryString("MemStatus")) ? 0 : int.Parse(GetDataFromHeaderOrQueryString("MemStatus")),
                AccessToken = GetDataFromHeaderOrQueryString("Authorization"),
                ApiSiteUrl = _cp.SSLSiteUrl
            };

            var act = _skmEf.GetSkmActivity(token);
            if (act.Id > 0)
            {
                model.Act = act;
                model.ActItems = _skmEf.GetSkmActivityItem(act.Id);
                model.RangeList = new List<SkmActivityRangeModel>();
                model.ActEnabled = DateTime.Now.IsBetween(act.JoinStartDate, act.JoinEndDate);
                var logs = isSkmMember ? _skmEf.GetViewSkmActivityLog(act.Id, e7Member.UniqueId) : new List<Core.Models.Entities.ViewSkmActivityLog>();
                var ranges = _skmEf.GetSkmActivityDateRange(act.Id);
                foreach (var r in ranges)
                {
                    var q = 0;
                    if (isSkmMember)
                    {
                        var rangeLog = logs.Where(x => x.CreateTime.IsBetween(r.DateRangeStart, r.DateRangeEnd)).ToList();
                        if (rangeLog.Any())
                        {
                            q = q + rangeLog.Sum(x => x.ChargesVal);
                        }
                    }

                    model.RangeList.Add(new SkmActivityRangeModel
                    {
                        Range = r,
                        Qty = q
                    });
                }
            }
            else
            {
                return new EmptyResult();
            }
            
            return View(model);
        }

        /// <summary>
        /// 抽獎畫面
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet]
        [RequireHttps]
        public ActionResult PrizeDrawEvent(string token)
        {
            logger.Info(string.Format("PrizeDrawEvent.token={0},userName={1}", token, string.IsNullOrEmpty(User.Identity.Name) ? GetDataFromHeaderOrQueryString("UserName") : User.Identity.Name));
            
            #region check member

            var isSkmMember = false;
            var e7Member = MemberFacade.GetMember(string.IsNullOrEmpty(User.Identity.Name) ? GetDataFromHeaderOrQueryString("UserName") : User.Identity.Name);
            if (e7Member.IsLoaded)
            {
                var skmMember = _skm.MemberGetByUserId(e7Member.UniqueId);
                isSkmMember = skmMember.IsLoaded;
            }

            #endregion check member

            if (isSkmMember)
            { 
                var model = new PrizeDrawEventViewModel
                {
                    UserName = string.IsNullOrEmpty(User.Identity.Name) ? GetDataFromHeaderOrQueryString("UserName") : User.Identity.Name,
                    CardNo = GetDataFromHeaderOrQueryString("CardNo"),
                    DeviceCode = GetDataFromHeaderOrQueryString("DeviceCode"),
                    DeviceType = GetDataFromHeaderOrQueryString("DeviceType"),
                    MemStatus =
                            string.IsNullOrEmpty(GetDataFromHeaderOrQueryString("MemStatus"))
                            ? 0
                            : int.Parse(GetDataFromHeaderOrQueryString("MemStatus")),
                    AccessToken = GetDataFromHeaderOrQueryString("Authorization"),
                    //SellerGuid = GetDataFromHeaderOrQueryString("SellerGuid"), 改用prizeEvent的seller_guid設定
                    ApiSiteUrl = _cp.SSLSiteUrl
                };

                logger.Info(string.Format("PrizeDrawEvent.CardNo={0},DeviceCode={1},DeviceType={2},MemStatus={3},Authorization={4},UserName={5}"
                    , model.CardNo, model.DeviceCode, model.DeviceType, model.MemStatus, model.AccessToken, model.UserName));

                var prizeEvent = _cep.GetPrizeDrawEvent(token);
                if (prizeEvent != null)
                {
                    model.WinningSmallImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.WinningSmallImg + "?g=" + Guid.NewGuid();
                    model.WinningBackgroundImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.WinningBackgroundImg + "?g=" + Guid.NewGuid();
                    model.PrizeImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.PrizeImg + "?g=" + Guid.NewGuid();
                    model.EventHtmlContent = prizeEvent.EventContent;
                    model.EventToken = prizeEvent.Token;
                    model.Subject = prizeEvent.Subject;
                    model.NoticeDate = prizeEvent.NoticeDate.HasValue
                         ? prizeEvent.NoticeDate.Value.Month + "月" + prizeEvent.NoticeDate.Value.Day + "日"
                         : prizeEvent.ED.Month + "月" + prizeEvent.ED.Day + "日";

                    #region 顯示抽獎結果info
                    var drawRecords = _cep.GetViewPrizeDrawRecord(prizeEvent.Id, e7Member.UniqueId);
                    //抽獎記錄還是全列
                    model.DrawRecord = drawRecords.OrderBy(p => p.CreateTime).ToList();
                    model.TotalDrawQty = drawRecords.Count();//決定獲獎紀錄開關
                    model.TotalLimit = prizeEvent.TotalLimit;
                    model.DailyLimit = prizeEvent.DailyLimit;

                    //判斷是否具參加資格
                    ApiResultCode verifyPrizeJoinResultCode = ApiResultCode.Success;
                    if (model.DailyLimit != 0 || model.TotalLimit != 0)
                    {
                        model.RemainDrawQty = SkmFacade.GetPrizeRemainQty(prizeEvent.DailyLimit, prizeEvent.TotalLimit,
                            drawRecords);

                        if (model.DailyLimit != 0 && model.RemainDrawQty < 1)
                        {
                            verifyPrizeJoinResultCode = ApiResultCode.DrawEventOverDailyLimit;
                        }

                        if (model.TotalLimit != 0 && model.RemainDrawQty < 1)
                        {
                            verifyPrizeJoinResultCode = ApiResultCode.DrawEventOverTotailLimit;
                        }
                    }
                    else
                    {
                        model.RemainDrawQty = 0;
                    }

                    #endregion

                    model.IsCanJoinPrizeDraw = false;

                    //判斷是否過期
                    DateTime dt = DateTime.Now;
                    if (prizeEvent.SD <= dt && dt <= prizeEvent.ED)
                    {
                        if (verifyPrizeJoinResultCode.Equals(ApiResultCode.Success))
                        {
                            //判斷獎品是否抽完
                            var itemDatas = _cep.GetPrizeDrawItemsWithNoLock(prizeEvent.Id, false).ToList();
                            ApiResultCode verifyPrizeResultCode = SkmFacade.VerifyPrizeDraw(prizeEvent.Id, itemDatas);
                            if (verifyPrizeResultCode.Equals(ApiResultCode.Success))
                            {
                                model.IsCanJoinPrizeDraw = true;
                                model.JoinFee = prizeEvent.JoinFee;
                            }
                            else
                            {
                                model.ErrorCode = ApiResultCode.DrawEventDrawOut;
                            }
                        }
                        else
                        {
                            model.ErrorCode = verifyPrizeJoinResultCode;
                        }
                    }
                    else
                    {
                        model.ErrorCode = ApiResultCode.DrawEventDrawOut;
                    }

                    model.SellerDpList = SkmFacade.GetSkmSellerDpList();
                    model.StoreDpList = SkmFacade.GetSkmStoreDpList();

                    return View(model);
                }
            }

            return new EmptyResult();
        }

        [HttpGet]
        [RequireHttps]
        public ActionResult PrizeDrawEventPreview(string token)
        {
            var model = new PrizeDrawEventViewModel();
            var prizeEvent = _cep.GetPrizeDrawEvent(token);
            if (prizeEvent != null)
            {
                model.WinningSmallImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.WinningSmallImg + "?g=" + Guid.NewGuid();
                model.WinningBackgroundImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.WinningBackgroundImg + "?g=" + Guid.NewGuid();
                model.PrizeImg = _cp.MediaBaseUrl + "PrizeDraw/" + prizeEvent.PrizeImg + "?g=" + Guid.NewGuid();
                model.EventHtmlContent = prizeEvent.EventContent;
                model.EventToken = prizeEvent.Token;
                model.Subject = prizeEvent.Subject;
                model.NoticeDate = prizeEvent.NoticeDate.HasValue
                    ? prizeEvent.NoticeDate.Value.Month + "月" + prizeEvent.NoticeDate.Value.Day + "日"
                    : prizeEvent.ED.Month + "月" + prizeEvent.ED.Day + "日";
                model.IsCanJoinPrizeDraw = false;
                //判斷是否過期或獎品抽完
                //preview 不用驗參加資格
                DateTime dt = DateTime.Now;
                if (prizeEvent.SD <= dt && dt <= prizeEvent.ED)
                {
                    var itemDatas = _cep.GetPrizeDrawItemsWithNoLock(prizeEvent.Id, false).ToList();
                    ApiResultCode verifyPrizeResultCode = SkmFacade.VerifyPrizeDraw(prizeEvent.Id, itemDatas);
                    if (verifyPrizeResultCode.Equals(ApiResultCode.Success))
                    {
                        model.IsCanJoinPrizeDraw = true;
                        model.JoinFee = prizeEvent.JoinFee;
                    }
                }

                return View(model);
            }
            return new EmptyResult();
        }

        private string GetDataFromHeaderOrQueryString(string tag)
        {
            var temp = HttpContext.Request.Headers[tag];
            if (tag == "Authorization" && string.IsNullOrEmpty(temp))
            {
                var token = HttpContext.Request.QueryString["accessToken"];
                return string.IsNullOrEmpty(token)
                    ? string.Empty
                    : (token.Contains("Bearer")
                        ? token
                        : string.Format("Bearer {0}", token));
            }
            return string.IsNullOrEmpty(temp) ? HttpContext.Request.QueryString[tag] : temp;
        }
    }
}

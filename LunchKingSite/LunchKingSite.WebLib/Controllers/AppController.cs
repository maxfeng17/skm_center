﻿using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.App;
using LunchKingSite.BizLogic.Model.API;

namespace LunchKingSite.WebLib.Controllers
{
    public class AppController : BaseController
    {
        private static ISysConfProvider _config;
        private static IPCPProvider _pcpProvider;
        static AppController()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _pcpProvider = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        }

        public ActionResult MembershipCard(int groupId,string refereeId = "")
        {
            MembershipCardModel model = new MembershipCardModel();
            model.MembershipCardPromoUrl = string.Format("{0}/event/PromoMembershipCard.aspx?groupId={1}",_config.SiteUrl, groupId);
            model.AndroidUserAgent = "/" + _config.AndroidUserAgent + "/i";
            model.IOSUserAgent = "/" + _config.iOSUserAgent + "/i";
            model.GroupId = groupId;
            model.RefereeId = refereeId;

            ViewMembershipCardCollection cardList = _pcpProvider.ViewMembershipCardGetList(ViewMembershipCard.Columns.OpenTime +" desc " ,ViewMembershipCard.Columns.CardGroupId +" = "+ groupId);
            //無符合條件資料
            if (cardList.Count == 0)
            {
                return Redirect(_config.SiteUrl);
            }
            //取第一筆資料
            ViewMembershipCard firstCard = cardList.First();
            ViewMembershipCard lastCard = cardList.Where(x => x.VersionId == firstCard.VersionId).OrderBy(x => x.Level).Last();
            VbsMembership vbsMember = VBSFacade.GetVbsMembershipByUserId(firstCard.SellerUserId);

            model.FBTitle= string.Format("{0} - {1}",vbsMember.Name,
                ApiPCPManager.MembershipCardGroupOfferDescription(firstCard.PaymentPercent,firstCard.Others,lastCard.PaymentPercent,lastCard.Others));
            model.FBDescription = I18N.Message.MembershipCardFBDescription;
            model.FBImageUrl = _config.SiteUrl + "/Themes/default/images/17Life/Logo/appcard.jpg";

            return View("MembershipCard",model);
        }

        public ActionResult Open(string code)
        {
            //TODO: 同MembershipCard製作openApp連結，不用做FB分享
            return null;
        }


        [AllowCrossSiteJson]
        public JsonResult GetDealsGeoData()
        {
            var result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = LunchKingSite.BizLogic.Component.API.PponDealApiManager.GetApiPponSimpleData();

            return Json(result);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.Http.Controllers;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Component.MemberActions;
using LunchKingSite.WebLib.Models.Mobile;
using LunchKingSite.WebLib.Views;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebLib.Models.Ppon;
using LunchKingSite.BizLogic.Model;
using System.Runtime.Caching;

namespace LunchKingSite.WebLib.Controllers
{
    [SwitchableHttps]
    public class MobileController : PponControllerBase
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IEventProvider _ep = ProviderFactory.Instance().GetProvider<IEventProvider>();
        protected ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        protected IOAuthProvider oAuthp = ProviderFactory.Instance().GetProvider<IOAuthProvider>();

        private ILog logger = LogManager.GetLogger(typeof(MobileController));

        private const int _DEAL_LIST_PAGE_SIZE = 20;

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (filterContext.IsChildAction == false)
            {
                object oCid = RouteData.Values["cid"];
                if (oCid != null)
                {
                    int cid;
                    if (int.TryParse(oCid.ToString(), out cid))
                    {
                        ViewBag.CityId = cid;
                    }
                }
                ViewBag.MvcAction = RouteData.Values["action"];
                ViewBag.MenuChannels = MenuChannelManager.GetMenuChannels();
            }
            base.OnActionExecuting(filterContext);
        }

        private string PreviousUrl
        {
            get
            {
                string theUrl = (string)Session[LkSiteSession.NowUrl.ToString()];
                return (string.IsNullOrEmpty(theUrl) ? config.SiteUrl : theUrl);
            }
        }

        public ActionResult Home()
        {
            string cacheKey = string.Format("page://m-home/{0}", DateTime.Now.ToString("yyyyMMddHH"));
            string content = MemoryCache.Default.Get(cacheKey) as string;
            if (string.IsNullOrEmpty(content))
            {
                content = System.IO.File.ReadAllText(Helper.MapPath("~/frontend/index.html"), Encoding.UTF8);
                MemoryCache.Default.Set(cacheKey, content, null);
            }
            return Content(content, "text/html");
        }

        /// <summary>
        /// 列表頁
        /// </summary>
        /// <param name="cid">頻道CityId</param>
        /// <param name="cat">分類CategoryId</param>
        /// <param name="rsrc">RSRC</param>
        /// <param name="ch">channel id, top of category</param>
        /// <returns></returns>
        public ActionResult Index(int? cid, int? cat, string rsrc, int? ch)
        {
            //台新商城APP
            if (ChannelFacade.GetOrderClassificationByHeaderToken() == AgentChannel.TaiShinMall)
            {
                return RedirectToAction("TaishinApp", new { cid, cat, rsrc });
            }

            if (!string.IsNullOrEmpty(rsrc))
            {
                ViewBag.Rsrc = rsrc;
            }

            ApiCategoryTypeNode apiCategoryTypeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
            ApiCategoryNode channelNode;
            if (ch == null)
            {
                if (cid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                {
                    channelNode = apiCategoryTypeNode.CategoryNodes
                        .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Delivery.CategoryId);
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
                {
                    channelNode = apiCategoryTypeNode.CategoryNodes
                        .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Beauty.CategoryId);
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
                {
                    channelNode = apiCategoryTypeNode.CategoryNodes
                        .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Travel.CategoryId);
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.Family.CityId)
                {
                    channelNode = apiCategoryTypeNode.CategoryNodes
                        .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Family.CategoryId);
                }
                else
                {
                    channelNode = apiCategoryTypeNode.CategoryNodes
                        .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.PponDeal.CategoryId);
                }
            }
            else
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == ch);
            }

            ViewBag.ChannelId = channelNode.CategoryId;

            var areaNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.PponChannelArea);
            var categoryNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealCategory);
            var specialNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealSpecialCategory);

            if (channelNode.CategoryId == CategoryManager.Default.PponDeal.CategoryId)
            {
                ApiCategoryTypeNode nodeCopied = new ApiCategoryTypeNode(CategoryType.PponChannelArea);
                foreach (ApiCategoryNode node in areaNode.CategoryNodes)
                {
                    int parentId = node.CategoryId;
                    ApiCategoryTypeNode apiNodes = GetDealCategoryWithCopied(parentId, nodeCopied, node);
                }

                //美食以"CityId"記住上次區域
                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(nodeCopied);
                int currentCityId = MobileManager.GetCurrentCityId();
                if (currentCityId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = CategoryManager.Default.Taipei.CategoryId;
                }
                else
                {
                    CategoryNode currentCityNode = CategoryManager.Default.FindByCity(currentCityId);
                    if (currentCityNode == null)
                    {
                        currentCityNode = CategoryManager.Default.Taipei;
                    }
                    ViewBag.AreaId = currentCityNode.CategoryId;
                }
                PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(currentCityId);
                ViewBag.CurrentCityName = (pponCity != null) ? pponCity.CityName : "全部";
            }
            else if (channelNode.CategoryId == CategoryManager.Default.Beauty.CategoryId)
            {
                //完美休閒
                ApiCategoryTypeNode nodeCopied = new ApiCategoryTypeNode(CategoryType.PponChannelArea);
                foreach (ApiCategoryNode node in areaNode.CategoryNodes)
                {
                    int parentId = node.CategoryId;
                    ApiCategoryTypeNode apiNodes = GetDealCategoryWithCopied(parentId, nodeCopied, node);
                }

                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(nodeCopied);
                int currentAreaId = MobileManager.GetCurrentCityId(channelNode.CategoryId);
                if (currentAreaId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = channelNode.CategoryId;
                }
                else
                {
                    ViewBag.AreaId = currentAreaId;
                }

                var firstOrDefault = (areaNode != null) ? areaNode.CategoryNodes.FirstOrDefault(x => x.CategoryId == ViewBag.AreaId) : null;
                ViewBag.CurrentCityName = firstOrDefault != null ? firstOrDefault.CategoryName : "全部";
            }
            else
            {
                //非美食以"Category"記住上次區域
                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(areaNode);
                int currentAreaId = MobileManager.GetCurrentCityId(channelNode.CategoryId);
                if (currentAreaId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = channelNode.CategoryId;
                }
                else
                {
                    ViewBag.AreaId = currentAreaId;
                }

                var firstOrDefault = (areaNode != null) ? areaNode.CategoryNodes.FirstOrDefault(x => x.CategoryId == ViewBag.AreaId) : null;
                ViewBag.CurrentCityName = firstOrDefault != null ? firstOrDefault.CategoryName : "全部";
            }

            ViewBag.CategoryNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(categoryNode);
            ViewBag.SpecialNodesMenuItems = MobileManager.ConvertToSlidingMenuItems(specialNode)
                .Where(t => t.refId != 138 && t.refId != 139)
                .ToList();

            string title, metaDescription;
            GetChannelTitleAndMetaDescription(channelNode.CategoryId, out title, out metaDescription);
            ViewBag.PageTitle = title;
            ViewBag.MetaDescription = metaDescription;

            if (cat.GetValueOrDefault() != 0)
            {
                ViewBag.SubCategoryId = cat.GetValueOrDefault();
            }

            return View();
        }

        private ApiCategoryTypeNode GetDealCategoryWithCopied(int parentId, ApiCategoryTypeNode mainCopied, ApiCategoryNode pNode)
        {
            if (pNode.NodeDatas.Count > 0)
            {
                //Category c = pNode.GetMainCategory();
                int CategoryId = pNode.CategoryId;
                CategoryRegionDisplayRule displayRule = CategoryManager.GetRegionDisplayRuleByCid(CategoryId);
                if (displayRule != null && Helper.IsFlagSet(displayRule.SpecialRuleFlag, CategoryDisplayRuleFlag.DisplaySummaryItem))
                {
                    CategoryId = parentId;
                }
                ApiCategoryNode catNode = new ApiCategoryNode(new Category
                {
                    Id = CategoryId,
                    Name = pNode.CategoryName,
                    Type = (int)pNode.CategoryType,
                });
                catNode.ExistsDeal = pNode.ExistsDeal;
                catNode.Seq = pNode.Seq;
                mainCopied.CategoryNodes.Add(catNode);
                GetDealCategoryNode(parentId, catNode, pNode.NodeDatas);
            }
            else
            {
                ApiCategoryNode catNode = new ApiCategoryNode(new Category
                {
                    Id = pNode.CategoryId,
                    Name = pNode.CategoryName,
                    Type = (int)pNode.CategoryType,
                });
                catNode.ExistsDeal = pNode.ExistsDeal;
                catNode.Seq = pNode.Seq;
                mainCopied.CategoryNodes.Add(catNode);
            }
            return mainCopied;
        }

        private void GetDealCategoryNode(int parentId, ApiCategoryNode mainNode, List<ApiCategoryTypeNode> rNodes)
        {
            foreach (ApiCategoryTypeNode subNode in rNodes)
            {
                foreach (ApiCategoryNode node in subNode.CategoryNodes)
                {
                    if (node.NodeDatas.Count == 0)
                    {
                        ApiCategoryTypeNode tNode;
                        if (mainNode.NodeDatas.Count == 0)
                        {
                            tNode = new ApiCategoryTypeNode(CategoryType.PponChannelArea);
                            mainNode.NodeDatas.Add(tNode);
                        }
                        else
                        {
                            tNode = mainNode.NodeDatas.First();
                        }

                        tNode.CategoryNodes.Add(node);
                    }
                    else
                    {
                        ApiCategoryTypeNode tNode;
                        if (mainNode.NodeDatas.Count == 0)
                        {
                            tNode = new ApiCategoryTypeNode(CategoryType.PponChannelArea);
                            mainNode.NodeDatas.Add(tNode);
                        }
                        else
                        {
                            tNode = mainNode.NodeDatas.First();
                        }
                        GetDealCategoryWithCopied(parentId, tNode, node);
                    }
                }
            }
        }

        private void GetChannelTitleAndMetaDescription(int categoryId, out string title, out string metaDescription)
        {
            title = config.DefaultTitle;
            metaDescription = config.DefaultMetaDescription;

            if (categoryId != 0)
            {
                ChannelCategory channelCategory = (ChannelCategory)categoryId;
                switch (channelCategory)
                {
                    case ChannelCategory.Food:
                        title = string.Format("美食頻道 - {0}", config.Title);
                        metaDescription = "王品集團與數百家知名美食餐廳的【破盤價餐券】就在17life！吃到飽、海鮮、火鍋等人氣美食，即買即用超方便！";
                        break;
                    case ChannelCategory.Delivery:
                        title = string.Format("宅配頻道 - {0}", config.Title);
                        metaDescription = "你想找的人氣團購美食，生活用品通通都在17life！宅配24H快速出貨，免運送到家。天天享有超低驚喜價，讓你輕鬆買到高CP好物！";
                        break;
                    case ChannelCategory.Travel:
                        title = string.Format("旅遊頻道 - {0}", config.Title);
                        metaDescription = "想找高CP值旅遊票券就上17life！飯店、民宿、溫泉、樂園門票應有盡有，還有全網最殺獨家優惠，完美旅程一手搞定！";
                        break;
                    case ChannelCategory.Beauty:
                        title = string.Format("玩美休閒 - {0}", config.Title);
                        metaDescription = "寵愛自己，體驗質感玩美生活就上17life！美髮沙龍、美甲美睫、精油SPA、中泰式按摩，可享全網最低驚喜價！";
                        break;
                    default:
                        title = config.DefaultTitle;
                        metaDescription = config.DefaultMetaDescription;
                        break;
                }
            }
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetPartialDeals(int channelId, int areaId, int? categoryId,
            CategorySortType? sortType, List<int> filters, int? dataStart)
        {
            MobileManager.SetCookeis(channelId, areaId, Response);

            List<MultipleMainDealPreview> multiplemaindeals = MobileManager.GetMultipleDeals(
                channelId, areaId, categoryId.GetValueOrDefault(), filters);

            var rsrc = MobileManager.GetReferrerSource();

            List<MobileMainDealModel> mainDeals = new MobileManager().GetMobileMainDeals(
                ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals,
                    sortType.GetValueOrDefault()), channelId);

            bool isCompleted = dataStart + _DEAL_LIST_PAGE_SIZE >= mainDeals.Count;
            mainDeals = mainDeals.Skip(dataStart.GetValueOrDefault()).Take(_DEAL_LIST_PAGE_SIZE).ToList();

            ViewBag.CityId = MobileManager.GetCityIdByCategoryId(areaId);
            List<ViewCmsRandom> curation = CmsRandomFacade.GetViewCmsRandomsByCity(
                RandomCmsType.PponMasterPage, "curation", ViewBag.CityId ?? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            string div = CmsRandomFacade.RenderCurationAsMobileHtml(curation);

            if (categoryId == null) { categoryId = 0; }
            string contentName = PponFacade.GetContentName(channelId, (int)categoryId);
            string contentCategory = PponFacade.GetContentCategory(channelId, (int)categoryId);
            string jsContentIds = PponFacade.GetPponDealBid(multiplemaindeals, (int)FacebookPixelCode.JsCode);
            jsContentIds = "[" + jsContentIds + "]";

            return Json(
                new { deals = mainDeals, completed = isCompleted, curation = div, jsContentIds = jsContentIds, contentName = contentName, contentCategory = contentCategory }
            );
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult SearchResult()
        {
            var keywords = Request.QueryString["Keywords"] != null ? HttpUtility.UrlDecode(Request.QueryString["Keywords"], Encoding.UTF8) : string.Empty;
            if (string.IsNullOrEmpty(keywords))
            {
                keywords = Request.QueryString["search"] != null ? HttpUtility.UrlDecode(Request.QueryString["search"], Encoding.UTF8) : string.Empty;
            }
            ViewBag.PageTitle = string.Format("{0}{1}熱銷搜尋結果-{2}", keywords, string.IsNullOrEmpty(keywords) ? "" : "-", config.Title);
            ViewBag.MetaDescription = string.Format("你想找的{0}就在17life！這裡的{0}全台線上最優惠，提供多家品牌商品任你挑選，買好物來17Life就對了！", keywords);
            ViewBag.Keywords = keywords;
            ViewBag.SortType = (int)CategorySortType.TopOrderTotal;
            ViewBag.City = (MobileManager.GetCurrentCityId() > 0) ? MobileManager.GetCurrentCityId() : PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            return View();
        }

        public ActionResult Member()
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }
            else
            {
                ViewBag.MemberName = this.UserFullName;
                if (string.IsNullOrEmpty(this.MemberPic))
                {
                    ViewBag.MemberPic = Helper.CombineUrl(Request.ApplicationPath, "Themes/mobile/images/login_17life.jpg");
                }
                else
                {
                    ViewBag.MemberPic = this.MemberPic;
                    List<string> fbKeyWord = new List<string> { "facebook", "fbcdn" };

                    if (fbKeyWord.Any(k => MemberPic.Contains(k, StringComparison.OrdinalIgnoreCase)))
                    {
                        if (!Helper.CheckRemoteImageExists(ViewBag.MemberPic))
                        {
                            var m = MemberFacade.GetMember(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
                            ViewBag.MemberPic = MemberFacade.RefreshFacebookMemberPic(m.UniqueId);
                        }
                    }
                }
                ViewBag.DiscountCount = op.ViewDiscountDetailGetListCount(GetFilter(FilterStatus.Unused, this.UserId));
                MobileMember mm = MemberFacade.GetMobileMember(User.Identity.Name);
                if (mm.IsLoaded)
                {
                    ViewBag.MemberMail = mm.MobileNumber;
                }
                else
                {
                    ViewBag.MemberMail = User.Identity.Name;
                }

                ViewBag.Bonus = Math.Floor(MemberFacade.GetMemberPromotionValue(User.Identity.Name) / 10).ToString("F0");
                decimal scash;
                decimal pscash;
                decimal generalScash = OrderFacade.GetSCashSum2(this.UserId, out scash, out pscash);
                //ToString("F0");
                ViewBag.Cash = generalScash.ToString("F0");
            }

            ViewBag.LogoutUrl = Helper.CombineUrl(Request.ApplicationPath, "NewMember/Logout.aspx?ref=" + HttpUtility.UrlEncode(Request.Url.AbsoluteUri));
            return View();
        }

        [HttpPost]
        public ActionResult Register(string userName, string password, string returnUrl)
        {
            SignInReply signInReply = LoginMember(userName, password);
            if (signInReply == SignInReply.Success)
            {
                TempData["ReturnUrl"] = returnUrl;
                return RedirectToAction("FillRegisterInfo");
            }

            TempData["loginError"] = "會員登入失敗,錯誤訊息:" + signInReply;
            return RedirectToAction("Login");
        }

        /// <summary>
        /// 台新銀行APP (非台新商城)
        /// </summary>
        /// <returns></returns>
        public ActionResult Taishin()
        {
            return Redirect("199?rsrc=" + config.TSBankAppRsrc);
        }

        private string[] GetFilter(FilterStatus status, int userId)
        {
            List<string> filter = new List<string>();

            filter.Add(ViewDiscountDetail.Columns.Owner + "=" + userId);
            filter.Add(ViewDiscountDetail.Columns.UseTime + " is null ");
            filter.Add(ViewDiscountDetail.Columns.EndTime + ">" + DateTime.Now);
            filter.Add(ViewDiscountDetail.Columns.CancelTime + " is null ");

            return filter.ToArray();
        }

        private SignInReply LoginMember(string userName, string password)
        {

            MemberLinkCollection mlCol = null;
            Member mem;
            SingleSignOnSource source;
            return LoginProviders.Login(userName, password, true, ref mlCol, out userName, out source, out mem);
        }

        [ChildActionOnly]
        public ActionResult Paragraph(string contentId)
        {
            string contentName = CmsContentFacade.GetContentNameByContentId(contentId, Request.Path);
            string cmsCache = CmsContentFacade.GetDataFromCache(contentName);
            string contentBody = string.Empty;

            if (Request.HttpMethod != "POST")
            {
                if (cmsCache == null)
                {
                    CmsContentFacade.SetDataToCache(contentId, Request.Path);
                    cmsCache = CmsContentFacade.GetDataFromCache(contentName);
                }
                contentBody = cmsCache;
            }

            return PartialView("_Paragraph", contentBody);
        }

        [ChildActionOnly]
        public ActionResult RandomParagraphBanner(RandomCmsType type, string name, int cid)
        {
            int bannerRatio = 0;
            string urlPath = Request.Path.ToLower();
            StringBuilder sb = new StringBuilder();
            DateTime nowDateTime = DateTime.Now;
            IEnumerable<ViewCmsRandom> vcr;
            bool isWebCurationPage = (urlPath.Contains("themecurationchannel") || urlPath.Contains("event/exhibitionlist.aspx") || urlPath.Contains("event/brandevent.aspx"));

            ViewCmsRandomCollection data = CmsRandomFacade.GetOrAddData(name, type);
            vcr = (cid > 0)
                      ? data.Where(x => (x.CityId == cid || x.CityId < 0) && nowDateTime <= x.EndTime && nowDateTime >= x.StartTime)
                      : data.Where(x => nowDateTime <= x.EndTime && nowDateTime >= x.StartTime);
            //策展項目僅限策展頁
            vcr = (!isWebCurationPage) ? vcr.Where(x => x.CityId != (int)CmsSpecialCityType.CurationPage).ToList() : vcr;

            bannerRatio = vcr.Sum(x => x.Ratio);
            Random r = new Random();
            int randomNumber = r.Next(1, bannerRatio + 1);
            foreach (ViewCmsRandom v in vcr)
            {
                randomNumber -= v.Ratio;
                if (randomNumber <= 0)
                {
                    sb.Append(v.Body);
                    break;
                }
            }

            return PartialView("_RandomParagraphBanner", sb.ToString());
        }

        [ChildActionOnly]
        [OutputCache(Duration = 180)]
        public ActionResult RandomParagraph(RandomCmsType type, string name, int cid)
        {
            ViewCmsRandomCollection data = CmsRandomFacade.GetOrAddData(name, type);
            DateTime nowDateTime = DateTime.Now;

            var rtn = (cid > 0)
                        ? data.Where(x => (x.CityId == cid || x.CityId < 0) && nowDateTime <= x.EndTime && nowDateTime >= x.StartTime)
                        : data.Where(x => nowDateTime <= x.EndTime && nowDateTime >= x.StartTime);

            return View(rtn.OrderBy(t => Guid.NewGuid()).ToList());
        }

        [ChildActionOnly]
        [OutputCache(Duration = 180)]
        public ActionResult RandomParagraphCuration(RandomCmsType type, string name, int cid)
        {
            List<ViewCmsRandom> rtn = CmsRandomFacade.GetViewCmsRandomsByCity(type, name, cid);
            return View(rtn);
        }

        [HttpPost]
        public ActionResult GetKeywordPromptList(string word, int takeCnt)
        {
            var promptLis = pp.KeywordGetPromptList(HttpUtility.UrlDecode(word, Encoding.UTF8)).Where(x => x.Word.Length > 1).OrderBy(x => x.Word.Length).Select(x => x.Word).ToList().Take(takeCnt);
            return Json(new { success = true, promptList = promptLis });
        }

        [HttpPost]
        public ActionResult SearchDeals(int? cityId, int areaid, string querystring, string sortType, int agent)
        {
            querystring = HttpUtility.UrlDecode(querystring, Encoding.UTF8);
            string cpaCode;
            List<IViewPponDeal> searchDeals = SearchFacade.GetSearchDeals(querystring, sortType, null);

            List<MobileMainDealModel> mobileMainSearchDeals;
            if (searchDeals.Any())
            {
                cpaCode = "17_search";
                List<MultipleMainDealPreview> multiplemaindeals =
                    searchDeals.Select(deal => new MultipleMainDealPreview(0, 0, deal, new List<int>(), new DealTimeSlotStatus(), 0)).ToList();
                mobileMainSearchDeals = new MobileManager().GetMobileMainDeals(multiplemaindeals, 0);
                ViewBag.SearchDealsCnt = mobileMainSearchDeals.Count;
                ViewBag.searchString = querystring;
                ViewBag.nsContentIds = PponFacade.GetSearchDealBid(searchDeals, (int)FacebookPixelCode.NsCode);
            }
            else
            {
                cpaCode = "17_searchpromo";
                mobileMainSearchDeals = GetPromoDeals(cityId, areaid);
                ViewBag.SearchDealsCnt = 0;
            }

            foreach (var deal in mobileMainSearchDeals)
            {
                if (deal.Url.IndexOf(cpaCode, StringComparison.OrdinalIgnoreCase) < 0)
                {
                    deal.Url = string.Format("{0}?cpa=cpa-{1}", deal.Url, cpaCode);
                }
            }

            PponFacade.AddPponSearchLog(querystring, mobileMainSearchDeals.Count, Session.SessionID, User.Identity.Name);
            var view = "_DeailsPartial";
            if (agent == (int)AgentChannel.TaiShinMall)
            {
                view = "_TaishinDealsPartial";
                mobileMainSearchDeals = mobileMainSearchDeals.Where(x => x.PriceString != "$0"
                                                                    && !x.ChannelAndRegionCategories.Contains(
                                                                        CategoryManager.Default.AppLimitedEdition
                                                                            .CategoryId)
                                                                    && !x.ChannelAndRegionCategories.Contains(
                                                                        CategoryManager.Default.Family.CategoryId))
                    .ToList();
                ViewBag.SearchDealsCnt = mobileMainSearchDeals.Count;
            }
            return PartialView(view, mobileMainSearchDeals);
        }

        [HttpPost]
        public JsonResult SearchDealsForFacebookPixelCode(string querystring, string sortType)
        {
            string searchString = string.Empty;
            string jsContentIds = string.Empty;
            string nsContentIds = string.Empty;
            querystring = HttpUtility.UrlDecode(querystring, Encoding.UTF8);
            List<IViewPponDeal> searchDeals = SearchFacade.GetSearchDeals(querystring, sortType, null);

            if (searchDeals.Any())
            {
                searchString = querystring;
                jsContentIds = PponFacade.GetSearchDealBid(searchDeals, (int)FacebookPixelCode.JsCode);
            }
            jsContentIds = "[" + jsContentIds + "]";

            return Json(new { success = true, searchString = searchString, jsContentId = jsContentIds });
        }

        private List<MobileMainDealModel> GetPromoDeals(int? cityid, int areaid)
        {
            int workCityId = cityid ?? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;


            var pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid.Value);
            if (areaid == 0)
            {
                areaid = pponCity.CategoryId;
            }

            var dealList = PponDealApiManager.GetTodayChoicePponDealSynopsesByCache(areaid);
            var bids = PponDealApiManager.PponDealSynopsisBasicSkipSelect(dealList, false, 0).Select(t => t.Bid);

            List<MultipleMainDealPreview> multiplemaindeals = new List<MultipleMainDealPreview>();
            foreach (Guid bid in bids)
            {
                var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (deal != null)
                {
                    multiplemaindeals.Add(new MultipleMainDealPreview(0, 0, deal, new List<int>(), new DealTimeSlotStatus(), 0));
                }
            }
            List<MobileMainDealModel> mobileMainPromoDeals = new MobileManager().GetMobileMainDeals(
                ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals,
                    CategorySortType.Default), workCityId);

            return mobileMainPromoDeals;
        }

        public ActionResult TodayDeals()
        {
            //宅配、旅遊、SPA檔次、全家
            //美食、宅配、旅遊、玩美.休閒、全家
            List<int> categoryCriteria = new List<int>();
            categoryCriteria.Add(CategoryManager.Default.PponDeal.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Delivery.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Travel.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Beauty.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Family.CategoryId);

            int workCityId = MobileManager.GetCurrentCityId();
            List<MultipleMainDealPreview> multiplemaindeals = new MobileManager().SetMultipleTodayDeals(workCityId, categoryCriteria);

            List<MobileMainDealModel> mainDeals = new MobileManager().GetMobileMainDeals(
                ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals,
                    CategorySortType.Default),
                workCityId);
            return View(mainDeals);
        }

        public ActionResult LastDeals()
        {
            //宅配、旅遊、SPA檔次、全家
            //美食、宅配、旅遊、玩美.休閒、全家
            List<int> categoryCriteria = new List<int>();
            categoryCriteria.Add(CategoryManager.Default.PponDeal.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Delivery.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Travel.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Beauty.CategoryId);
            categoryCriteria.Add(CategoryManager.Default.Family.CategoryId);

            int workCityId = MobileManager.GetCurrentCityId();
            List<MultipleMainDealPreview> multiplemaindeals = new MobileManager().SetMultipleLastDeals(workCityId, categoryCriteria);

            List<MobileMainDealModel> mainDeals = new MobileManager().GetMobileMainDeals(
                ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals,
                    CategorySortType.Default),
                workCityId);

            return View(mainDeals);
        }

        [HttpGet]
        public ActionResult UpdateMemberPic()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect(FormsAuthentication.LoginUrl);
            }
            Member mem = MemberFacade.GetMember(User.Identity.Name);
            ViewBag.MemberPic = MemberFacade.GetMemberPicUrl(mem);
            return View();
        }

        [RequireHttps]
        public ActionResult EasyLogin()
        {
            string devIp = WebConfigurationManager.AppSettings["DeveloperIp"];
            string userIp = System.Web.HttpContext.Current.Request.UserHostAddress;
            if (!string.Equals(devIp, userIp))
            {
                return RedirectToAction("Login");
            }
            ViewBag.ReCaptchaHackCode = new Random().Next(100000).ToString();
            System.Web.HttpContext.Current.Session[LkSiteSession.CaptchaLogin.ToString()] = ViewBag.ReCaptchaHackCode;
            return View();
        }

        [RequireHttps]
        public ActionResult Login()
        {
            ViewBag.ChannelAgent = ChannelFacade.GetOrderClassificationByHeaderToken();

            //如果會員己在登入狀態，導回上一頁，或首頁
            if (User.Identity.IsAuthenticated)
            {
                // OAuth 給外部網站串接時需再實作
                /*
                var previousUri = System.Web.HttpContext.Current.Request.UrlReferrer;
                if (previousUri != null)
                {
                    //TODO 偵測持續的導出再被導入，以3次為限即改導到首頁.
                    return Redirect(previousUri.AbsolutePath);
                }
                */
                var siteUrl = config.SiteUrl;
                if (ViewBag.ChannelAgent == AgentChannel.TaiShinMall)
                {
                    siteUrl = config.SiteUrl + "/m/TaishinApp";
                }
                return Redirect(siteUrl);
            }

            // TODO 需補上 state的部份 
            string fbRetUrl = Helper.CombineUrl(config.SiteUrl, "/newmember/fbauth.ashx");
            Session[LkSiteSession.NowUrl.ToString()] = Request["returnUrl"];
            //https://www.facebook.com/v2.12/dialog/oauth?client_id=189065144614&redirect_uri=https%3A%2F%2Fwww.17life.com%2F%2Fnewmember%2Ffbauth.ashx&scope=email,public_profile,user_friends

            ViewBag.FbLoginUrl = WebUtility.GetFacebookLoginUrl();
            //Line
            ViewBag.LineLoginUrl = WebUtility.GetLineLoginUrl();

            ViewBag.PezLoginUrl = WebUtility.GetPayEasySsoPath();
            ViewBag.PezMobileLoginUrl = string.Format("{0}/login?mode=mobile&service={1}/NewMember/PezLogin.aspx",
                    config.PayEasyCasServerUrl,
                    config.SiteUrl.ToLower().Replace("http://", "https://"))
                .Replace("//login", "/login").Replace("//NewMember", "/NewMember");

            ViewBag.ApiBaseUrl = config.SSLSiteUrl;
            ViewBag.RegisterUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmail");
            ViewBag.ForgetPasswordUrl = Helper.CombineUrl(config.SSLSiteUrl, "NewMember/ForgetPassword.aspx");

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = (string)TempData["ErrorMessage"];
                ViewBag.MemoryAccount = (string)TempData["MemoryAccount"];
            }
            bool isRedirectByVbs = false;
            if (TempData["IsRedirectByVbs"] != null)
            {
                bool.TryParse(TempData["IsRedirectByVbs"].ToString(), out isRedirectByVbs);
            }
            ViewBag.IsRedirectByVbs = isRedirectByVbs;
            ViewBag.IsCaptchaOpen = PasswordErrorTimesSession.Current.CheckWarning();
            return View();
        }

        [RequireHttps]
        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (PasswordErrorTimesSession.Current.CheckWarning() &&
                PasswordErrorTimesSession.Current.CheckCaptchaMatch(model.captchaResponse) == false)
            {
                TempData["ErrorMessage"] = "圖形驗證輸入錯誤！";
                TempData["MemoryAccount"] = model.txtEmail;
                return RedirectToAction("Login", new { returnUrl = model.returnUrl });
            }

            MemberLinkCollection mlCol = null;
            Member mem;
            string userName;
            SingleSignOnSource source;

            SignInReply reply = LoginProviders.Login(model.txtEmail, model.txtPassword,
                model.chkKeepLogin == "on", ref mlCol, out userName, out source, out mem);
            string replyMessage = reply.GetAttributeOfType<DescriptionAttribute>().Description;
            if (reply == SignInReply.Success)
            {
                PasswordErrorTimesSession.Current.Reset();

                #region 超取後台SSO驗證

                if (!string.IsNullOrEmpty(model.appId))//超取驗證&夾帶參數導回
                {
                    var client = OAuthFacade.GetClient(model.appId);

                    if (client != null)
                    {
                        var code = OAuthFacade.CreateAuthorizeCode(mem.UniqueId, model.appId, OAuthTargetType.Client);
                        model.returnUrl += "?code=" + code;//串回參數代表成功
                        return Redirect(model.returnUrl);
                    }
                }


                #endregion

                if (string.IsNullOrEmpty(model.returnUrl) == false)
                {
                    if (model.returnUrl.StartsWith(config.SiteUrl, StringComparison.OrdinalIgnoreCase))
                    {
                        return Redirect(model.returnUrl);
                    }
                    if (model.returnUrl.StartsWith(config.SSLSiteUrl, StringComparison.OrdinalIgnoreCase))
                    {
                        return Redirect(model.returnUrl);
                    }
                    if (model.returnUrl.StartsWith("http://", StringComparison.OrdinalIgnoreCase) == false &&
                        model.returnUrl.StartsWith("https://", StringComparison.OrdinalIgnoreCase) == false)
                    {
                        return Redirect(model.returnUrl);
                    }
                    return Redirect(config.SiteUrl);
                }

                return Redirect(PreviousUrl);
            }
            else if (reply == SignInReply.MemberLinkExistButWrongWay)
            {
                if (mlCol.Count > 1)
                {
                    replyMessage = replyMessage.FormatWith("其他方式");
                }
                else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.PayEasy)
                {
                    replyMessage = replyMessage.FormatWith("PayEasy");
                }
                else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.Facebook)
                {
                    replyMessage = replyMessage.FormatWith("Facebook");
                }
                else
                {
                    replyMessage = replyMessage.FormatWith("其他方式");
                }
            }
            else if (reply == SignInReply.EmailInactive)
            {
                Session["RegisterByEmailWaitConfirmMssage"] = "請啟用帳號後，再重新綁定";
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                string authKey = mai.AuthKey;
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?send=1&key=" + authKey));
            }
            else if (reply == SignInReply.MobileInactive)
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl,
                    "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(model.txtEmail)));
            }

            if (reply == SignInReply.PasswordError)
            {
                PasswordErrorTimesSession.Current.Increment();
            }

            TempData["ErrorMessage"] = replyMessage;
            TempData["MemoryAccount"] = model.txtEmail;
            return RedirectToAction("Login", new { returnUrl = model.returnUrl, appId = model.appId });
        }

        [HttpGet]
        [RequireHttps]
        public ActionResult FillRegisterInfo()
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?returnUrl=/m/FillRegisterInfo");
            }
            Member mem = mp.MemberGet(User.Identity.Name);
            ViewBag.MemberPic = MemberFacade.GetMemberPicUrl(mem);
            List<City> citySource = CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS").ToList();
            citySource.Insert(0, new City { Id = 0, CityName = "請選擇" });
            ViewBag.CitySource = citySource;
            ViewBag.LastName = mem.LastName;
            ViewBag.FirstName = mem.FirstName;
            ViewBag.UserEmail = mem.UserEmail;
            ViewBag.CityId = mem.CityId.GetValueOrDefault();
            ViewBag.ApiBaseUrl = config.SSLSiteUrl;
            if (TempData["ReturnUrl"] != null)
            {
                ViewBag.GotoUrl = TempData["ReturnUrl"] as string;
            }
            else
            {
                var lastViewedDeal = RecentlyViewedDealsmMnager.GetLast();
                if (lastViewedDeal != null)
                {
                    ViewBag.GotoUrl = "/" + lastViewedDeal.DealId;
                }
                else
                {
                    ViewBag.GotoUrl = "/";
                }
            }
            return View();
        }

        public ActionResult FastDeals(int? cid, int? cat, string rsrc)
        {
            if (!string.IsNullOrEmpty(rsrc))
            {
                ViewBag.Rsrc = rsrc;
            }

            ApiCategoryTypeNode apiCategoryTypeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
            ApiCategoryNode channelNode;
            if (cid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Delivery.CategoryId);
            }
            else
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.PponDeal.CategoryId);
            }

            ViewBag.ChannelId = channelNode.CategoryId;

            var areaNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.PponChannelArea);
            var categoryNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealCategory);
            var specialNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealSpecialCategory);

            if (channelNode.CategoryId == CategoryManager.Default.PponDeal.CategoryId)
            {
                //美食以"CityId"記住上次區域
                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(areaNode);
                int currentCityId = MobileManager.GetCurrentCityId();
                if (currentCityId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = CategoryManager.Default.Taipei.CategoryId;
                }
                else
                {
                    CategoryNode currentCityNode = CategoryManager.Default.FindByCity(currentCityId);
                    if (currentCityNode == null)
                    {
                        currentCityNode = CategoryManager.Default.Taipei;
                    }
                    ViewBag.AreaId = currentCityNode.CategoryId;
                }
                PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(currentCityId);
                ViewBag.CurrentCityName = (pponCity != null) ? pponCity.CityName : "全部";
            }

            ViewBag.CategoryNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(categoryNode);
            ViewBag.SpecialNodesMenuItems = MobileManager.ConvertToSlidingMenuItems(specialNode)
                            .Where(t => t.refId != 138 && t.refId != 139)
                            .ToList();

            if (cat.GetValueOrDefault() != 0)
            {
                ViewBag.SubCategoryId = cat.GetValueOrDefault();
            }

            ViewBag.PageTitle = string.Format("即買即用美食餐券 - {0}", config.Title);
            ViewBag.MetaDescription = "想吃美食不用苦等餐券寄到家，17life電子餐券讓你即刻享受美味生活，記得先跟店家預約訂位唷！";
            return View();
        }


        public ActionResult Deal(Guid? bid, int? cid, string rsrc, string cpa)
        {
            if (bid == null)
            {
                return Redirect("~/");
            }
            int userId = 0;
            if (User.Identity.IsAuthenticated)
            {
                userId = MemberFacade.GetUniqueId(User.Identity.Name);
            }
            IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid.Value, true);
            if (mainDeal == null || mainDeal.IsLoaded == false)
            {
                return Redirect("~/");
            }
            if (GameFacade.HasGameDealViewPermission(userId, mainDeal.BusinessHourGuid))
            {
                return Redirect(string.Format("/game/productDetail?bid={0}", mainDeal.BusinessHourGuid));
            }
            if (Helper.IsFlagSet(mainDeal.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb))
            {
                return Redirect("~/");
            }
            if (mainDeal.MainBid != null && mainDeal.MainBid != mainDeal.BusinessHourGuid)
            {
                return Redirect("~/deal/" + mainDeal.MainBid);
            }
            //天貓
            if ((mainDeal.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
            {
                return Redirect("~/");
            }
            // 若為品生活檔次，轉址為品生活頁面
            var dealCoategoryIdList = ViewPponDealManager.DefaultManager.GetCategoryIds(mainDeal.BusinessHourGuid);
            if (dealCoategoryIdList.Contains(PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
            {
                string newUrl = string.Format("{0}/piinlife/{1}", config.SiteUrl, mainDeal.BusinessHourGuid);
                QueryStringBuilder qs = new QueryStringBuilder(newUrl);
                qs.Append("rsrc", rsrc);
                qs.Append("cpa", cpa);
                return Redirect(qs.ToString());
            }
            //
            RecentlyViewedDealsmMnager.Add(mainDeal);
            //meta
            ViewBag.DealMetas = PponFacade.RenderMetasForDeal(mainDeal);
            ViewBag.CanonicalRel = PponFacade.RenderCanonicalRel(mainDeal);
            ViewBag.LoginUrl = string.Format("{0}?ReturnUrl={1}", FormsAuthentication.LoginUrl, HttpUtility.UrlEncode(this.Request.Url.AbsolutePath));
            //Retargeting
            if (mainDeal.ItemPrice > 0)
            {
                try
                {
                    ViewBag.DealArgs = RetargetingUtility.GetDealArgs(mainDeal, userId);
                }
                catch (Exception ex)
                {
                    logger.Info("Retargeting Exception, bid=" + mainDeal.BusinessHourGuid, ex);
                }
            }
            var model = MainDealModel.Create(mainDeal);

            bool isTestUser = User != null && User.Identity.IsAuthenticated && config.MDealTester.Contains(User.Identity.Name);
            if (!isTestUser)
            {
                model.WatchOtherDeals.Clear();
            }

            string ExpireRedirectDisplay = Helper.GetEnumDescription((ExpireRedirectDisplay)(mainDeal.ExpireRedirectDisplay ?? 1));

            string expireRedirectUrl = mainDeal.ExpireRedirectUrl;
            expireRedirectUrl = PponFacade.GetClosedDealExpireRedirectUrl(mainDeal, expireRedirectUrl);

            string ExpireRedirectUrl = expireRedirectUrl;
            ViewBag.ExpireRedirectDisplay = ExpireRedirectDisplay;
            ViewBag.ExpireRedirectUrl = ExpireRedirectUrl;

            return View(model);
        }

        #region 愛評投票活動

        [HttpPost]
        [AjaxCall]
        public ActionResult GetEvertDeals(string category, string url, int userId, int eventId, int itemType)
        {
            var allVotes = _ep.ViewEventPromoVoteRecordGet(eventId, userId).Where(x => x.ItemType == itemType);
            var userVoucher = _ep.ViewEventPromoVourcherCollectGet(eventId, userId);
            var eventItems = _ep.ViewEventPromoVoteGet(eventId).Where(x => x.Status).Select(item => new ViewMEventPromoVoteCombine
            {
                PromoItem = item,
                IsVoted = (userId != 0) ? userVoucher.Any(x => x.EventId == item.Id) : allVotes.Any(x => x.EventPromoItemId == item.Id),
                VotingData = GetVotingData(url, item.Id, userId, itemType),
                MediaPaths = ImageFacade.GetMediaPathsFromRawData(item.ImagePath, MediaType.PponDealPhoto).DefaultIfEmpty("../Themes/PCweb/images/ppon-M1_pic.jpg").First(),
                IsSoldout = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)item.BusinessHourGuid).OrderedQuantity >= ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)item.BusinessHourGuid).OrderTotalLimit
            });
            eventItems =
                eventItems.Where(x => x.PromoItem.Category.EndsWith(category))
                    .OrderByDescending(x => x.PromoItem.Votes)
                    .ThenBy(x => x.PromoItem.Seq).ToList();
            return Json(
                new { deals = eventItems }
            );
        }


        protected string GetVotingData(string url, int promoItemId, int userId, int itemType)
        {
            var sec = new Security(SymmetricCryptoServiceProvider.AES);

            return
                sec.Encrypt(new JsonSerializer().Serialize(
                new
                {
                    EventUrl = url,
                    PromoItemId = promoItemId,
                    MemberUniqueId = userId,
                    IpAddress = Helper.GetClientIP(),
                    ItemType = itemType
                }));
        }

        #endregion

        #region 台新商城

        [HttpPost]
        [AjaxCall]
        public ActionResult GetTaishinPartialDeals(int channelId, int areaId, int? categoryId,
            CategorySortType? sortType, List<int> filters, int? dataStart)
        {
            MobileManager.SetCookeis(channelId, areaId, Response);

            List<MultipleMainDealPreview> multiplemaindeals = MobileManager.GetMultipleDeals(
                channelId, areaId, categoryId.GetValueOrDefault(), filters);

            var rsrc = MobileManager.GetReferrerSource();

            List<MobileMainDealModel> mainDeals = new MobileManager().GetMobileMainDeals(
                ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(multiplemaindeals,
                    sortType.GetValueOrDefault()), channelId);

            bool isCompleted = dataStart + _DEAL_LIST_PAGE_SIZE >= mainDeals.Count;
            mainDeals = mainDeals.Skip(dataStart.GetValueOrDefault()).Take(_DEAL_LIST_PAGE_SIZE).ToList();

            ViewBag.CityId = MobileManager.GetCityIdByCategoryId(areaId);
            List<ViewCmsRandom> curation = CmsRandomFacade.GetViewCmsRandomsByCity(
                RandomCmsType.PponMasterPage, "curation", ViewBag.CityId ?? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            string div = CmsRandomFacade.RenderCurationAsMobileHtml(curation);

            return Json(
                new { deals = mainDeals, completed = isCompleted, curation = div }
            );
        }

        public ActionResult TaishinApp(int? cid, int? cat, string rsrc)
        {
            //台新商城APP
            if (ChannelFacade.GetOrderClassificationByHeaderToken() != AgentChannel.TaiShinMall)
            {
                return RedirectToAction("Index", new { cid, cat, rsrc });
            }

            if (!string.IsNullOrEmpty(rsrc))
            {
                ViewBag.Rsrc = rsrc;
            }

            ApiCategoryTypeNode apiCategoryTypeNode = PponDealPreviewManager.GetPponChannelCategoryTree();
            ApiCategoryNode channelNode;
            if (cid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Delivery.CategoryId);
            }
            else if (cid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId)
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Beauty.CategoryId);
            }
            else if (cid == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Travel.CategoryId);
            }
            else if (cid == PponCityGroup.DefaultPponCityGroup.Family.CityId)
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.Family.CategoryId);
            }
            else
            {
                channelNode = apiCategoryTypeNode.CategoryNodes
                    .FirstOrDefault(t => t.CategoryId == CategoryManager.Default.PponDeal.CategoryId);
            }

            ViewBag.ChannelId = channelNode.CategoryId;

            var areaNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.PponChannelArea);
            var categoryNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealCategory);
            var specialNode = channelNode.NodeDatas.FirstOrDefault(t => t.NodeType == CategoryType.DealSpecialCategory);

            if (channelNode.CategoryId == CategoryManager.Default.PponDeal.CategoryId)
            {
                //美食以"CityId"記住上次區域
                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(areaNode);
                int currentCityId = MobileManager.GetCurrentCityId();
                if (currentCityId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = CategoryManager.Default.Taipei.CategoryId;
                }
                else
                {
                    CategoryNode currentCityNode = CategoryManager.Default.FindByCity(currentCityId);
                    if (currentCityNode == null)
                    {
                        currentCityNode = CategoryManager.Default.Taipei;
                    }
                    ViewBag.AreaId = currentCityNode.CategoryId;
                }
                PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(currentCityId);
                ViewBag.CurrentCityName = (pponCity != null) ? pponCity.CityName : "全部";
            }
            else
            {
                //非美食以"Category"記住上次區域
                ViewBag.AreaNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(areaNode);
                int currentAreaId = MobileManager.GetCurrentCityId(channelNode.CategoryId);
                if (currentAreaId <= 0) //未取到current預設值
                {
                    ViewBag.AreaId = channelNode.CategoryId;
                }
                else
                {
                    ViewBag.AreaId = currentAreaId;
                }

                var firstOrDefault = (areaNode != null) ? areaNode.CategoryNodes.FirstOrDefault(x => x.CategoryId == ViewBag.AreaId) : null;
                ViewBag.CurrentCityName = firstOrDefault != null ? firstOrDefault.CategoryName : "全部";
            }

            ViewBag.CategoryNodesDataJson = MobileManager.ConvertToSlidingMenuDataJson(categoryNode);
            ViewBag.SpecialNodesMenuItems = MobileManager.ConvertToSlidingMenuItems(specialNode)
                            .Where(t => t.refId != 138 && t.refId != 139)
                            .ToList();

            if (cat.GetValueOrDefault() != 0)
            {
                ViewBag.SubCategoryId = cat.GetValueOrDefault();
            }

            return View();
        }

        #endregion

        public class LoginModel
        {
            public string txtEmail { get; set; }
            public string txtPassword { get; set; }
            public string chkKeepLogin { get; set; }
            public string returnUrl { get; set; }
            public string captchaResponse { get; set; }
            /// <summary>
            /// appId
            /// </summary>
            public string appId { get; set; }
        }

    }

    public class AjaxCallAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (IsAjaxjRequest(filterContext.RequestContext.HttpContext.Request) == false)
            {
                string absPath = string.Empty;
                if (filterContext.RequestContext.HttpContext != null &&
                    filterContext.RequestContext.HttpContext.Request != null &&
                    filterContext.RequestContext.HttpContext.Request.Url != null)
                {
                    absPath = filterContext.RequestContext.HttpContext.Request.Url.AbsolutePath;
                }
                throw new Exception(string.Format("ajax method {0} can't call directly.",
                    absPath));
            }
            base.OnActionExecuting(filterContext);
        }

        public bool IsAjaxjRequest(HttpRequestBase request)
        {
            return request.Headers["x-requested-with"] == "XMLHttpRequest";
        }
    }


    public class ViewMEventPromoVoteCombine
    {
        public ViewMEventPromoVoteCombine()
        {
            IsVoted = false;
        }

        public ViewEventPromoVote PromoItem { set; get; }
        public bool IsVoted { get; set; }
        public string VotingData { set; get; }
        public string MediaPaths { set; get; }
        public bool IsSoldout { get; set; }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.FundMgmtSystem;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    public class HiDealVerificationController : BaseController
    {
        private static IAccountingProvider ap;
        private static IHiDealProvider hp;
        private static IOrderProvider op;
        private static IPponProvider pp;
        private static ISellerProvider sp;
        private static IMemberProvider mp;
        private static ISysConfProvider config;
        private static ILog logger;

        static HiDealVerificationController()
        {
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            config = ProviderFactory.Instance().GetConfig();
            logger = LogManager.GetLogger(typeof(HiDealVerificationController));
        }

        #region 品生活 ACH

        public ActionResult WeeklyPayReport(DateTime? tb_DateStart, DateTime? tb_DateEnd, int? tb_Pid)
        {
            List<ViewHiDealWeeklyPayReport> reports = null;

            if (tb_Pid == null && tb_DateStart == null && tb_DateEnd == null)
            {
                reports = new List<ViewHiDealWeeklyPayReport>();
            }
            else if (tb_Pid != null)
            {
                reports = hp.ViewHiDealWeeklyPayReportGetList(null, null, null, tb_Pid, null, false);
            }
            else
            {
                if (tb_DateStart != null && tb_DateEnd != null)
                {
                    reports = hp.ViewHiDealWeeklyPayReportGetList(tb_DateStart, tb_DateEnd, null, null, null, true);
                    if (reports.Count == 0)
                    {
                        ModelState.AddModelError("error", "查無資料!!");
                    }
                }
                else
                {
                    ModelState.AddModelError("error", "日期格式錯誤!!");
                }
            }

            return View(reports);
        }

        public ActionResult WeeklyPayReportQuerySummary(Guid? reportGuid)
        {
            if (reportGuid == null && Request["reportGuid"] != null)
            {
                reportGuid = Guid.Parse(Request["reportGuid"]);
                logger.Error("請確認 mvc 版本是否為 3.0.2 版 or 已上");// mvc 3.0.1 有無法代入 guid? 的問題
            }
            List<ViewHiDealWeeklyPayReport> reports =
                hp.ViewHiDealWeeklyPayReportGetList(null, null, reportGuid, null, null, true);
            return View("WeeklyPayReport", reports);
        }

        public ActionResult WeeklyPayReportQueryDetail(Guid? reportGuid)
        {
            if (reportGuid == null && Request["reportGuid"] != null)
            {
                reportGuid = Guid.Parse(Request["reportGuid"]);
                logger.Error("請確認 mvc 版本是否為 3.0.2 版 or 已上");// mvc 3.0.1 有無法代入 guid? 的問題
            }
            List<ViewHiDealWeeklyPayReport> reports =
                hp.ViewHiDealWeeklyPayReportGetList(null, null, reportGuid, null, null, false);
            return View("WeeklyPayReport", reports);
        }

        public ActionResult PrintACH(Guid? reportGuid, int? reportId, DateTime? remitDate)
        {
            if (reportGuid == null && Request["reportGuid"] != null)
            {
                reportGuid = Guid.Parse(Request["reportGuid"]);
                logger.Error("請確認 mvc 版本是否為 3.0.2 版 or 已上");// mvc 3.0.1 有無法代入 guid? 的問題
            }

            if (remitDate == null)
            {
                ModelState.AddModelError("error", "請填寫匯款日");
                return View("WeeklyPayReport");
            }
            DateTime target = remitDate.Value.AddHours(17);
            DateTime now = DateTime.Now;
            if (target.DayOfWeek == DayOfWeek.Sunday || target.DayOfWeek == DayOfWeek.Saturday)
            {
                ModelState.AddModelError("error", "退款日不得為假日");
                return View("WeeklyPayReport");
            }

            if ((target - now).Days < 2)
            {
                ModelState.AddModelError("error", @"退款日需比發動日大兩天，" + (target - DateTime.Now).Days + "天");
                return View("WeeklyPayReport");
            }

            if (target.DayOfWeek == DayOfWeek.Monday && (target - now).Days < 4)
            {
                ModelState.AddModelError("error", "處理日不得為假日");
                return View("WeeklyPayReport");
            }

            AchStringBuilder asb = new AchStringBuilder(target);
            List<ViewHiDealWeeklyPayReport> reports = null;
            if (reportGuid != null)
            {
                reports = hp.ViewHiDealWeeklyPayReportGetList(null, null, reportGuid, null, null, false);
            }
            else if (reportId != null)
            {
                reports = hp.ViewHiDealWeeklyPayReportGetList(null, null, null, null, reportId, null);
            }
            else
            {
                throw new Exception("PrintAch 參數未指定");
            }
            string fileName = string.Format("ACHP01_{0}_{1}_01.txt", DateTime.Now.ToString("yyyyMMdd"), config.TriggerCompanyId);
            if (reports != null)
            {
                foreach (var report in reports)
                {
                    asb.Append(report.BankNo, report.BranchNo, report.AccountNo, (int)report.TotalSum, report.AccountId,
                               report.Id, report.ProductId);
                }
            }

            return new FileContentResult(Encoding.UTF8.GetBytes(asb.ToString()), "text/octet-stream") { FileDownloadName = fileName };
        }

        #endregion 品生活 ACH

        #region ACH匯款管理

        public ActionResult AchFundTransferMgmt(AchFundMgmtModel model)
        {
            bool validSearchCriteria = model.Uid.HasValue
                                                || model.Vts.HasValue
                                                || model.Vte.HasValue;

            ViewBalanceSheetFundsTransferPponCollection fundTransfers;

            if (validSearchCriteria)
            {
                bool? transferComplete;
                switch (model.TrxCriteria)
                {
                    case TrxCriteria.Completed:
                        transferComplete = true;
                        break;

                    case TrxCriteria.Others:
                        transferComplete = false;
                        break;

                    case TrxCriteria.All:
                    default:
                        transferComplete = null;
                        break;
                }

                fundTransfers = ap.ViewBalanceSheetFundsTransferPponGetList(model.Biz, model.Uid, model.Vts, model.Vte, transferComplete);

                model.Transfers = ToFundTransferMgmtInfo(fundTransfers);
            }

            return View("AchFundTransferMgmt", model);
        }
        public ActionResult AchNewFundTransferMgmt(AchFundMgmtModel model)
        {
            bool validSearchCriteria = model.Cts.HasValue
                                                || model.Cte.HasValue;

            ViewBalanceSheetBillFundTransferCollection fundTransfersBill;

            if (validSearchCriteria)
            {
                List<BalanceSheetType> bsTypes = new List<BalanceSheetType>{
                    BalanceSheetType.FlexiblePayBalanceSheet,
                    BalanceSheetType.FlexibleToHouseBalanceSheet,
                    BalanceSheetType.WeeklyToHouseBalanceSheet,
                    BalanceSheetType.MonthlyToHouseBalanceSheet,
                    BalanceSheetType.WeeklyPayBalanceSheet,
                    BalanceSheetType.MonthlyPayBalanceSheet,
                };

                //取得轉出Excel的條件:新出帳方式,BusinessModel,檔號,建檔人(沒有這個條件恆null),核銷日(起),核銷日(迄),單據建立日(起),單據建立日(迄),付款狀態(Excel只要尚未付款的資料)
                fundTransfersBill = ap.ViewBalanceSheetBillFundTransferGetList(bsTypes, model.Biz, null, null, null, null, model.Cts, model.Cte, false);

                //請款凍結檔次不產出匯款檔案
                var dealFreezeInfos = pp.DealAccountingGet(fundTransfersBill.Select(x => x.ProductGuid))
                    .ToDictionary(x => x.BusinessHourGuid,
                                       x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));

                var balanceSheets = ap.BalanceSheetGetListByIds(fundTransfersBill.Where(x => x.PayReportId == null)
                    .Where(x => dealFreezeInfos.ContainsKey(x.ProductGuid) && !dealFreezeInfos[x.ProductGuid])
                    .Select(x => x.BalanceSheetId).ToList());

                var bsIsConfirmedReadyToPay = balanceSheets.Where(x => x.IsConfirmedReadyToPay = true).ToDictionary(x => x.Id);

                IEnumerable<ViewBalanceSheetBillFundTransfer> nonZeroTransfers = fundTransfersBill.Where(x => x.PayReportId == null)
                    .Where(x => dealFreezeInfos.ContainsKey(x.ProductGuid) && !dealFreezeInfos[x.ProductGuid])
                    .Where(x => bsIsConfirmedReadyToPay.ContainsKey(x.BalanceSheetId));


                //產weeklyPayReport
                if (bsIsConfirmedReadyToPay.Any())
                {
                    AchRemitNewPaymentType(Guid.NewGuid(), balanceSheets.Where(x => x.IsConfirmedReadyToPay = true));
                }

                Workbook workbook = CovertToExcel(nonZeroTransfers);
                return Excel(workbook, "新出帳方式付款報表.xls");

            }

            return View("AchFundTransferMgmt", model);
        }
        private void AchRemitNewPaymentType(Guid reportGuid, IEnumerable<BalanceSheet> bsCol)
        {
            if (bsCol == null || !bsCol.Any())
            {
                return;
            }

            foreach (var bs in bsCol)
            {
                //產過就不重產了
                if (!bs.PayReportId.HasValue)
                {
                    var bsModel = new BalanceSheetModel(bs.Id);

                    BusinessHour bh = sp.BusinessHourGet(bs.ProductGuid);
                    CashTrustLogCollection ctLogNormal = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Normal);
                    CashTrustLogCollection ctLogDeduction = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Deduction);

                    int payReportId = 0;
                    if (bs.EstAmount != 0)
                    {
                        DealCostCollection costs = pp.DealCostGetList(bs.ProductGuid);
                        DealProperty dp = pp.DealPropertyGet(bs.ProductGuid);
                        decimal cost = (costs.Any() && costs.Max(x => x.Cost).HasValue) ? costs.Max(x => x.Cost).Value : 0;
                        cost = cost > 0 && dp.SaleMultipleBase > 0 ? cost / dp.SaleMultipleBase.Value : cost;
                        //填入CashTrustLog報表代號
                        mp.CashTrustLogToUpdateForAch(reportGuid, bs.Id);

                        #region 新增WeeklyPayReport資料
                        WeeklyPayReport report = new WeeklyPayReport();

                        report.BusinessHourGuid = bs.ProductGuid;
                        report.IntervalStart = bs.IntervalStart;
                        report.IntervalEnd = bs.IntervalEnd;
                        report.CreditCard = ctLogNormal.Sum(x => x.CreditCard) - ctLogDeduction.Sum(x => x.CreditCard);
                        report.Atm = ctLogNormal.Sum(x => x.Atm) - ctLogDeduction.Sum(x => x.Atm);
                        report.ReportGuid = reportGuid;
                        report.IsLastWeek = (bs.IntervalStart <= bh.BusinessHourDeliverTimeE && bh.BusinessHourDeliverTimeE < bs.IntervalEnd);
                        report.Cost = cost;
                        report.TotalCount = ctLogNormal.Count() - ctLogDeduction.Count();
                        report.TotalSum = bsModel.GetAccountsPayable().TotalAmount;
                        report.UserId = config.SystemEmail;
                        report.CreateTime = DateTime.Now;
                        report.StoreGuid = bs.StoreGuid;
                        report.FundTransferType = (int)FundTransferType.FinDept;

                        payReportId = op.SetWeeklyPayReport(report);
                        #endregion
                    }

                    #region 對帳單回填reportId
                    bs.PayReportId = payReportId;
                    ap.BalanceSheetSet(bs);
                    #endregion
                }
            }
        }
        private IEnumerable<FundTransferMgmtInfo> ToFundTransferMgmtInfos(IEnumerable<ViewBalanceSheetFundsTransferPpon> dbFunds)
        {
            List<FundTransferMgmtInfo> result = new List<FundTransferMgmtInfo>();

            List<int> bsIds = new List<int>();

            foreach (var dbFund in dbFunds)
            {
                if (bsIds.Contains(dbFund.BsId))
                {
                    continue;       //multiple bills within one balance sheet
                }
                else
                {
                    bsIds.Add(dbFund.BsId);
                }

                FundTransferMgmtInfo info = new FundTransferMgmtInfo
                {
                    DealId = dbFund.DealId,
                    DealGuid = dbFund.DealGuid,
                    StoreName = dbFund.StoreName,
                    BalanceSheetIntervalStart = dbFund.BsIntervalStart,
                    BalanceSheetIntervalEnd = dbFund.BsIntervalEnd,
                    AccountsPayable = dbFund.TotalSum.GetValueOrDefault(0),
                    AccountsPaid = dbFund.TransferAmount,
                    PaymentId = dbFund.PayId.Value,
                    TransferTime = dbFund.ResponseTime,
                    TransferType = (FundTransferType)dbFund.FundTransferType,
                    Memo = dbFund.Memo,
                    IsAccountsPaidModifiable = IsAccountsPaidModifiable((FundTransferType)dbFund.FundTransferType, new AchResultType(dbFund.AchResponseResult), dbFund.TransferAmount),
                    IsTransferTimeModifiable = IsTransferedTimeModifiable((FundTransferType)dbFund.FundTransferType),
                    IsTransferTypeModifiable = IsTransferTypeModifiable((FundTransferType)dbFund.FundTransferType, new AchResultType(dbFund.AchResponseResult)),
                    Information = GetTransferInformation((FundTransferType)dbFund.FundTransferType, new AchResultType(dbFund.AchResponseResult), dbFund.IsTransferComplete),
                    IsTransferCompleteModifiable = IsTransferCompleteModifiable((FundTransferType)dbFund.FundTransferType, new AchResultType(dbFund.AchResponseResult), dbFund.IsTransferComplete)
                };

                if (!string.IsNullOrWhiteSpace(dbFund.ReceiverCompanyId)
                    || !string.IsNullOrWhiteSpace(dbFund.ReceiverBankNo)
                    || !string.IsNullOrWhiteSpace(dbFund.ReceiverBranchNo)
                    || !string.IsNullOrWhiteSpace(dbFund.ReceiverAccountNo))
                {
                    info.ReceiverTitle = dbFund.ReceiverTitle;
                    info.ReceiverCompanyId = dbFund.ReceiverCompanyId;
                    info.ReceiverBankNo = dbFund.ReceiverBankNo;
                    info.ReceiverBranchNo = dbFund.ReceiverBranchNo;
                    info.ReceiverAccountNo = dbFund.ReceiverAccountNo;
                }
                else
                {
                    info.ReceiverTitle = dbFund.AccountTitle;
                    info.ReceiverCompanyId = dbFund.CompanyId;
                    info.ReceiverBankNo = dbFund.BankNo;
                    info.ReceiverBranchNo = dbFund.BranchNo;
                    info.ReceiverAccountNo = dbFund.AccountNo;
                }
                result.Add(info);
            }

            return result;
        }

        private List<FundTransferMgmtInfo> ToFundTransferMgmtInfo(ViewBalanceSheetFundsTransferPponCollection dbFunds)
        {
            return dbFunds
                            .Where(FundIsTransferable)
                            .Select(fund =>
                            {
                                var result = new FundTransferMgmtInfo
                                {
                                    DealId = fund.DealId,
                                    DealGuid = fund.DealGuid,
                                    StoreName = (string.IsNullOrEmpty(fund.StoreName) ? "" : fund.StoreName),
                                    BalanceSheetIntervalStart = fund.BsIntervalStart,
                                    BalanceSheetIntervalEnd = fund.BsIntervalEnd,
                                    AccountsPayable = fund.EstAmount.GetValueOrDefault(0),
                                    AccountsPaid = fund.TransferAmount.GetValueOrDefault(0),
                                    PaymentId = (fund.PayId == null ? 0 : fund.PayId.Value),
                                    TransferTime = (fund.ResponseTime == null ? null : fund.ResponseTime),
                                    TransferType = (fund.FundTransferType == null ? 0 : (FundTransferType)fund.FundTransferType),
                                    Memo = (string.IsNullOrEmpty(fund.Memo) ? "" : fund.Memo),
                                    IsAccountsPaidModifiable = (fund.FundTransferType == null ? false : IsAccountsPaidModifiable((FundTransferType)fund.FundTransferType, new AchResultType(fund.AchResponseResult), fund.TransferAmount)),
                                    IsTransferTimeModifiable = (fund.FundTransferType == null ? false : IsTransferedTimeModifiable((FundTransferType)fund.FundTransferType)),
                                    IsTransferTypeModifiable = (fund.FundTransferType == null ? false : IsTransferTypeModifiable((FundTransferType)fund.FundTransferType, new AchResultType(fund.AchResponseResult))),
                                    Information = (fund.FundTransferType == null ? "" : GetTransferInformation((FundTransferType)fund.FundTransferType, new AchResultType(fund.AchResponseResult), fund.IsTransferComplete)),
                                    IsTransferCompleteModifiable = (fund.FundTransferType == null ? false : IsTransferCompleteModifiable((FundTransferType)fund.FundTransferType, new AchResultType(fund.AchResponseResult), fund.IsTransferComplete))
                                };

                                if (!string.IsNullOrWhiteSpace(fund.ReceiverCompanyId)
                                    || !string.IsNullOrWhiteSpace(fund.ReceiverBankNo)
                                    || !string.IsNullOrWhiteSpace(fund.ReceiverBranchNo)
                                    || !string.IsNullOrWhiteSpace(fund.ReceiverAccountNo))
                                {
                                    result.ReceiverTitle = fund.ReceiverTitle;
                                    result.ReceiverCompanyId = fund.ReceiverCompanyId;
                                    result.ReceiverBankNo = fund.ReceiverBankNo;
                                    result.ReceiverBranchNo = fund.ReceiverBranchNo;
                                    result.ReceiverAccountNo = fund.ReceiverAccountNo;
                                }
                                else
                                {
                                    result.ReceiverTitle = fund.AccountTitle;
                                    result.ReceiverCompanyId = fund.CompanyId;
                                    result.ReceiverBankNo = fund.BankNo;
                                    result.ReceiverBranchNo = fund.BranchNo;
                                    result.ReceiverAccountNo = fund.AccountNo;
                                }
                                return result;
                            }).ToList();
        }

        private bool FundIsTransferable(ViewBalanceSheetFundsTransferPpon dbFund)
        {
            if (dbFund.PayId.HasValue)
            {
                if (int.Equals(0, dbFund.PayId.Value))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
            //return dbFund.PayId.HasValue        // 有對帳單但沒有匯款資料: 阻擋匯款
            //    && !int.Equals(0, dbFund.PayId.Value);  // payid = 0: 0元對帳單, 不用匯款
        }

        private bool FundIsTransferableForExcel(ViewBalanceSheetFundsTransferPpon dbFund)
        {
            return dbFund.PayId.HasValue        // 有對帳單但沒有匯款資料: 阻擋匯款
                && !int.Equals(0, dbFund.PayId.Value); // payid = 0: 0元對帳單, 不用匯款
            //&& dbFund.FundTransferType==(int)FundTransferType.CtAch;  //排除ACH的部分，excel下載只針對新出帳
        }

        public ActionResult FundsModification(FundModificationRequest request)
        {
            bool isNewFundDataCreated;
            int newFundId = 0;

            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = IsolationLevel.ReadCommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                WeeklyPayReport fund = op.GetWeeklyPayReportById(request.PayId);
                if (fund == null || !fund.IsLoaded)
                {
                    throw new Exception("No fund transfer data found!");
                }

                ViewBalanceSheetFundsTransferPpon bsFundInfo = ap.ViewBalanceSheetFundsTransferPponGetByPaymentId(request.PayId);
                if (bsFundInfo == null || !bsFundInfo.IsLoaded)
                {
                    throw new Exception("No balance sheet found!");
                }

                AchResultType achResult = new AchResultType(bsFundInfo.AchResponseResult);

                //使用者是否要把 "ACH" 付款方式改為 "人工"
                bool isRequestingAchToManual = IsTransferTypeModifiable((FundTransferType)bsFundInfo.FundTransferType, achResult)
                    && (FundTransferType)bsFundInfo.FundTransferType.GetValueOrDefault(0) == FundTransferType.CtAch
                    && request.TransferType == FundTransferType.FinDept;

                if (isRequestingAchToManual)
                {
                    if (achResult.IsNull)
                    {
                        // Ach 匯款前, 可直接改匯款方式
                        fund.FundTransferType = (int)request.TransferType;
                        isNewFundDataCreated = false;
                    }
                    else if (!achResult.IsSuccess)
                    {
                        // Ach 匯款失敗後, 要新增匯款資料, 不可改原本匯款的記錄
                        // 實付金額不要複製到新匯款資料
                        BalanceSheet bs = ap.BalanceSheetGet(bsFundInfo.BsId);
                        WeeklyPayReport newFund = new WeeklyPayReport()
                        {
                            BusinessHourGuid = fund.BusinessHourGuid,
                            IntervalStart = fund.IntervalStart,
                            IntervalEnd = fund.IntervalEnd,
                            CreditCard = fund.CreditCard,
                            Atm = fund.Atm,
                            ReportGuid = fund.ReportGuid,
                            IsLastWeek = fund.IsLastWeek,
                            Cost = fund.Cost,
                            TotalCount = fund.TotalCount,
                            TotalSum = fund.TotalSum,
                            ErrorIn = fund.ErrorIn,
                            ErrorOut = fund.ErrorOut,
                            ErrorInSum = fund.ErrorInSum,
                            ErrorOutSum = fund.ErrorOutSum,
                            UserId = HttpContext.User.Identity.Name,
                            CreateTime = DateTime.Now,
                            StoreGuid = fund.StoreGuid,
                            FundTransferType = (int)FundTransferType.FinDept,
                            Memo = request.Memo,    // 備註
                            UntransferredPayId = fund.Id
                        };
                        newFundId = op.SetWeeklyPayReport(newFund);
                        bs.PayReportId = newFundId;
                        ap.BalanceSheetSet(bs);
                        isNewFundDataCreated = true;
                    }
                    else
                    {
                        throw new Exception("not expected!");
                    }
                }
                else
                {
                    isNewFundDataCreated = false;
                }

                if (IsAccountsPaidModifiable((FundTransferType)bsFundInfo.FundTransferType, achResult, bsFundInfo.TransferAmount)
                    && !isNewFundDataCreated)   // 若有新的匯款紀錄, 舊的就不用填了.
                {
                    fund.TransferAmount = request.AccountsPaid;
                    if (achResult.IsSuccessButAmountMismatch)
                    {
                        fund.Result = "00";
                    }
                }

                if (IsTransferedTimeModifiable((FundTransferType)bsFundInfo.FundTransferType))
                {
                    fund.ResponseTime = request.TransferTime;
                }

                if ((FundTransferType)bsFundInfo.FundTransferType == FundTransferType.FinDept
                    && !bsFundInfo.IsTransferComplete.GetValueOrDefault(false)
                    && request.IsTransferComplete.GetValueOrDefault(false))
                {
                    fund.ReceiverCompanyId = request.ReceiverCompanyId;
                    fund.ReceiverBankNo = request.ReceiverBankNo;
                    fund.ReceiverBranchNo = request.ReceiverBranchNo;
                    fund.ReceiverAccountNo = request.ReceiverAccountNo;
                    fund.ReceiverTitle = request.ReceiverTitle;
                }

                if (!string.Equals(fund.Memo, request.Memo)
                    && !(string.IsNullOrEmpty(fund.Memo) && string.IsNullOrEmpty(request.Memo)))
                {
                    fund.Memo = request.Memo;
                }

                if (IsTransferCompleteModifiable((FundTransferType)bsFundInfo.FundTransferType, new AchResultType(bsFundInfo.AchResponseResult), bsFundInfo.IsTransferComplete))
                {
                    if (request.IsTransferComplete.HasValue)
                    {
                        fund.IsTransferComplete = request.IsTransferComplete.Value;
                    }
                }

                //付款成功且有付款時間, 週對帳單要更新月份
                if (fund.IsTransferComplete
                    && fund.ResponseTime.HasValue
                    && bsFundInfo.BsGenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                {
                    BalanceSheet bs = ap.BalanceSheetGet(bsFundInfo.BsId);
                    if (bs.Year.GetValueOrDefault(0) != fund.ResponseTime.Value.Year
                        || bs.Month.GetValueOrDefault(0) != fund.ResponseTime.Value.Month)
                    {
                        if (bs.BalanceSheetType == (int)BalanceSheetType.WeeklyPayWeekBalanceSheet)
                        {
                            bs.Year = fund.ResponseTime.Value.Year;
                            bs.Month = fund.ResponseTime.Value.Month;
                            ap.BalanceSheetSet(bs);
                        }
                    }
                }

                op.SetWeeklyPayReport(fund);

                scope.Complete();
            }

            int resultPayId = isNewFundDataCreated ? newFundId : request.PayId;
            ViewBalanceSheetFundsTransferPpon resultFundInfo = ap.ViewBalanceSheetFundsTransferPponGetByPaymentId(resultPayId);
            var temp = new ViewBalanceSheetFundsTransferPponCollection();
            temp.Add(resultFundInfo);

            var model = new AchFundMgmtModel()
                            {
                                Transfers = ToFundTransferMgmtInfo(temp),
                                IsModificationSuccess = true
                            };

            return View("AchFundTransferMgmt", model);
        }

        #region private 匯款管理輔助方法

        /// <summary>
        /// 是否可修改實付金額
        /// </summary>
        private bool IsAccountsPaidModifiable(FundTransferType transferType, AchResultType result, decimal? transferAmount)
        {
            if (transferType == FundTransferType.CtAch)
            {
                // Ach 匯款前 || Ach 匯款成功但沒實付金額
                if (result.IsNull || (result.IsSuccess && !transferAmount.HasValue))
                {
                    return true;
                }
                return false;
            }
            else if (transferType == FundTransferType.FinDept)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// 是否可修改匯款時間
        /// </summary>
        private bool IsTransferedTimeModifiable(FundTransferType transferType)
        {
            switch (transferType)
            {
                case FundTransferType.CtAch:
                    // return false;   //匯款時間由 Ach 回覆檔為準
                    return true;    //可改日期
                case FundTransferType.FinDept:
                    return true;

                default:
                    return false;
            }
        }

        private bool IsTransferTypeModifiable(FundTransferType transferType, AchResultType achResult)
        {
            switch (transferType)
            {
                case FundTransferType.CtAch:
                    return !achResult.IsSuccess;   // true: 匯款前 or 匯款後但失敗
                case FundTransferType.FinDept:
                    return false;

                default:
                    return false;
            }
        }

        private string GetTransferInformation(FundTransferType transferType, AchResultType achResult, bool? isTransferComplete)
        {
            string result = string.Empty;

            /*
             * 2013-04-18:
             * 沒有 "暫停付款" 的狀態.
             * 營管在單據管理決定可不可以付款, 暫停付款的話, 就不會有匯款資料.
             */

            switch (transferType)
            {
                case FundTransferType.CtAch:
                    if (achResult.IsNull)
                    {
                        result += "尚未付款";
                    }
                    else
                    {
                        if (achResult.IsSuccess && !achResult.IsSuccessButAmountMismatch && isTransferComplete.GetValueOrDefault(false))
                        {
                            //已經把金額不符的狀況解決了
                            result += "付款成功";
                        }
                        else
                        {
                            if (achResult.IsSuccessButAmountMismatch)
                            {
                                result += achResult.Description;
                            }
                            else
                            {
                                if (achResult.IsSuccess && !isTransferComplete.GetValueOrDefault(false))
                                {
                                    result += "付款狀態尚未確認";
                                }
                                else
                                {
                                    result += "付款失敗-" + achResult.Description;
                                }
                            }
                        }
                    }
                    break;

                case FundTransferType.FinDept:
                    if (isTransferComplete.HasValue && isTransferComplete.Value)
                    {
                        result += "付款成功";
                    }
                    else
                    {
                        result += "尚未付款";
                    }
                    break;
            }

            return result;
        }

        private bool IsTransferCompleteModifiable(FundTransferType transferType, AchResultType achResult, bool? isTransferComplete)
        {
            switch (transferType)
            {
                case FundTransferType.FinDept:
                    if (isTransferComplete.HasValue)
                    {
                        return !isTransferComplete.Value;
                    }
                    return false;

                case FundTransferType.CtAch:
                    return (achResult.IsSuccessButAmountMismatch && !isTransferComplete.GetValueOrDefault(false))
                           || (achResult.IsSuccess && !isTransferComplete.GetValueOrDefault(false));

                default:
                    return false;
            }
        }

        #endregion private 匯款管理輔助方法

        #endregion ACH匯款管理

        #region 整合 ACH

        /*
         * 先有config
         * I. 依據條件, 若沒有匯款資料, 產出 ach 檔案 (直接用 fund_transfer)
         * II. ACH 上傳到 fund_transfer
         * III. 功能正常的話, script 資料轉移
         */

        #endregion


        #region 人工匯款管理

        [HttpGet]
        public ActionResult DirectDepositExcel()
        {
            return new EmptyResult();
        }

        [HttpPost]
        public ActionResult DirectDepositExcel(HttpPostedFileBase excel)
        {
            if (excel == null)
            {
                ViewBag.Message = "匯入失敗! 沒收到檔案.";
                return View("DirectDepositUploadResult");
            }

            if (Path.GetExtension(excel.FileName) != ".xls")
            {
                ViewBag.Message = "匯入失敗! 副檔名不對, 請使用 Excel 97 - 2003.";
                return View("DirectDepositUploadResult");
            }

            HSSFWorkbook workbook;
            try
            {
                workbook = new HSSFWorkbook(excel.InputStream);
            }
            catch (IOException)
            {
                ViewBag.Message = "匯入失敗! 檔案開啟失敗, 請檢查檔案是否毀損!";
                return View("DirectDepositUploadResult");
            }

            Sheet sheet = workbook.GetSheetAt(0);

            if (!CheckHeader(sheet))
            {
                ViewBag.Message = "匯入失敗! 檔案表頭欄位不合! 請使用本功能匯出的檔案.";
                return View("DirectDepositUploadResult");
            }

            Dictionary<int, BalanceSheetUseType> excelBsids = GetBsids(sheet);
            SetupBalanceSheetPool(excelBsids);

            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            var tranDataLookup = new Dictionary<int, DirectDepositData>();
            var invalidBsIds = new List<int>();
            var inconsistentBsIds = new List<int>();

            ViewBalanceSheetListCollection vbslc = ap.ViewBalanceSheetListGetListByBalanceSheetIds(excelBsids.Where(x => x.Value == BalanceSheetUseType.Deal).Select(x => x.Key).ToList());

            int i = 0;  //header
            while (rows.MoveNext())
            {
                var row = (HSSFRow)rows.Current;
                if (i == 0)
                {
                    i++;
                    continue;
                }

                int bsId = row.GetCellValue<int>(DirectDepositColumns.BalanceSheetId);

                /*
                 * 檢查匯入的檔案不要是先收單據後付款的資料
                 * */
                var vbsl = vbslc.Where(x => x.Id.Equals(bsId)).FirstOrDefault();
                if (vbsl != null)
                {
                    if (vbsl.BalanceSheetType != (int)BalanceSheetType.ManualTriggered &&
                        vbsl.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                        vbsl.BalanceSheetType != (int)BalanceSheetType.ManualMonthlyPayBalanceSheet)
                    {
                        inconsistentBsIds.Add(bsId);
                        continue;
                    }
                }

                if (CheckRowBsAgainstDbBs(row, bsId))
                {
                    if (!tranDataLookup.ContainsKey(bsId))
                    {
                        AddData(tranDataLookup, row, bsId);
                        if (tranDataLookup[bsId] == null)
                        {
                            invalidBsIds.Add(bsId);
                        }
                    }
                    else
                    {
                        if (!CheckEquivalent(tranDataLookup, row, bsId))
                        {
                            if (tranDataLookup[bsId] == null)
                            {
                                if (!invalidBsIds.Contains(bsId))
                                {
                                    invalidBsIds.Add(bsId);
                                }
                            }
                            else
                            {
                                if (!inconsistentBsIds.Contains(bsId))
                                {
                                    inconsistentBsIds.Add(bsId);
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (!tranDataLookup.ContainsKey(bsId))
                    {
                        tranDataLookup.Add(bsId, new DirectDepositData
                        {
                            BalanceSheetId = 0,
                            TransferTime = null,
                            TransferAmount = null,
                            TransferMemo = string.Empty
                        });
                    }
                    if (!inconsistentBsIds.Contains(bsId))
                    {
                        inconsistentBsIds.Add(bsId);
                    }
                }
            }

            string message = string.Empty;

            if (invalidBsIds.Count > 0 || inconsistentBsIds.Count > 0)
            {
                message += "請檢查檔案是否有問題!";

                if (invalidBsIds.Count > 0)
                {
                    message += string.Format("對帳單Id {0} 的金額/時間不完整!\r\n", string.Join(",", invalidBsIds));
                }
                if (inconsistentBsIds.Count > 0)
                {
                    message += string.Format("對帳單Id {0} 與對應資訊不合!\r\n", string.Join(",", inconsistentBsIds));
                }
            }


            List<int> failedBsIds;
            if (!TrySaveFundTransfers(tranDataLookup, out failedBsIds))
            {
                message += string.Format("部分失敗! {0} 不成功。請確認後再試一次。", string.Join(",", failedBsIds));
            }

            if (message == string.Empty)
            {
                message = "匯入成功";
            }

            ViewBag.Message = message;
            return View("DirectDepositUploadResult");
        }

        [HttpGet]
        public ActionResult AchExcel()
        {
            return new EmptyResult();
        }
        [HttpPost]
        public ActionResult AchExcel(HttpPostedFileBase excel)
        {
            if (excel == null)
            {
                ViewBag.Message = "匯入失敗! 沒收到檔案.";
                //return View("DirectDepositUploadResult");
            }

            if (Path.GetExtension(excel.FileName) != ".xls")
            {
                ViewBag.Message = "匯入失敗! 副檔名不對, 請使用 Excel 97 - 2003.";
                //return View("DirectDepositUploadResult");
            }

            HSSFWorkbook workbook = null;
            try
            {
                workbook = new HSSFWorkbook(excel.InputStream);
            }
            catch (IOException)
            {
                ViewBag.Message = "匯入失敗! 檔案開啟失敗, 請檢查檔案是否毀損!";
                //return View("DirectDepositUploadResult");
            }

            Sheet sheet = workbook.GetSheetAt(0);

            if (!CheckACHHeader(sheet))
            {
                ViewBag.Message = "匯入失敗! 檔案表頭欄位不合!";
                //return View("DirectDepositUploadResult");
            }


            if (string.IsNullOrEmpty(ViewBag.Message))
            {
                Dictionary<int, BalanceSheetUseType> excelBsids = GetACHBsids(sheet);
                SetupBalanceSheetPool(excelBsids);

                System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

                var tranDataLookup = new Dictionary<int, DirectDepositData>();
                var invalidBsIds = new List<int>();
                var inconsistentBsIds = new List<int>();

                ViewBalanceSheetListCollection vbslc = ap.ViewBalanceSheetListGetListByBalanceSheetIds(excelBsids.Where(x=>x.Value == BalanceSheetUseType.Deal).Select(x=>x.Key).ToList());
                BalanceSheetWmCollection bswc = ap.BalanceSheetWmsGetListByIds(excelBsids.Where(x => x.Value == BalanceSheetUseType.Wms).Select(x => x.Key).ToList());

                int i = 0;  //header
                while (rows.MoveNext())
                {
                    var row = (HSSFRow)rows.Current;
                    if (i == 0)
                    {
                        i++;
                        continue;
                    }

                    if (row.GetCellValue<int>(ACHColumns.BalanceSheetId) == 0 && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferTime)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferAmount)) &&
                        string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverAccountName)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverAccountNo)) &&
                        string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverCompanyId)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverBankNo)) &&
                        string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverBranchNo)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferMemo)) && 
                        string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.BalanceSheetUseType)))
                    {
                        //全為空則停止
                        break;
                    }

                    BalanceSheetUseType useType = (BalanceSheetUseType)row.GetCellValue<int>(ACHColumns.BalanceSheetUseType);
                    if (useType == BalanceSheetUseType.Deal)
                    {
                        int bsId = row.GetCellValue<int>(ACHColumns.BalanceSheetId);

                        /*
                         * 檢查匯入的檔案要是先收單據後付款的資料
                         * */
                        var vbsl = vbslc.Where(x => x.Id.Equals(bsId)).FirstOrDefault();
                        if (vbsl != null)
                        {
                            if (vbsl.BalanceSheetType != (int)BalanceSheetType.FlexiblePayBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.FlexibleToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.WeeklyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.MonthlyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.FortnightlyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.WeeklyPayBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.MonthlyPayBalanceSheet)
                            {
                                inconsistentBsIds.Add(bsId);
                                continue;
                            }
                        }

                        if (CheckBalanceIsExist(bsId, BalanceSheetUseType.Deal))
                        {
                            if (!tranDataLookup.ContainsKey(bsId))
                            {
                                AddACHData(tranDataLookup, row, bsId);
                                if (tranDataLookup[bsId] == null)
                                {
                                    invalidBsIds.Add(bsId);
                                }
                            }
                            else
                            {
                                if (tranDataLookup[bsId].BalanceSheetId != bsId)
                                {
                                    if (tranDataLookup[bsId] == null)
                                    {
                                        if (!invalidBsIds.Contains(bsId))
                                        {
                                            invalidBsIds.Add(bsId);
                                        }
                                    }
                                    else
                                    {
                                        if (!inconsistentBsIds.Contains(bsId))
                                        {
                                            inconsistentBsIds.Add(bsId);
                                        }
                                    }
                                }
                                else
                                {
                                    //若有一樣的直接加總金額
                                    int tranAmount = row.GetCellValue<int>(DirectDepositColumns.TransferAmount);
                                    tranDataLookup[bsId].TransferAmount += tranAmount;
                                }
                            }
                        }
                        else
                        {
                            //沒有該對帳單
                            if (!inconsistentBsIds.Contains(bsId))
                            {
                                inconsistentBsIds.Add(bsId);
                            }
                        }
                    }
                    else
                    {
                        int bsId = row.GetCellValue<int>(ACHColumns.BalanceSheetId);

                        /*
                         * 檢查匯入的檔案要是先收單據後付款的資料
                         * */
                        var vbsl = bswc.Where(x => x.Id.Equals(bsId)).FirstOrDefault();
                        if (vbsl != null)
                        {
                            if (vbsl.BalanceSheetType != (int)BalanceSheetType.FlexiblePayBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.FlexibleToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.WeeklyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.MonthlyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.FortnightlyToHouseBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.WeeklyPayBalanceSheet &&
                                vbsl.BalanceSheetType != (int)BalanceSheetType.MonthlyPayBalanceSheet)
                            {
                                inconsistentBsIds.Add(bsId);
                                continue;
                            }
                        }

                        if (CheckBalanceIsExist(bsId, BalanceSheetUseType.Wms))
                        {
                            if (!tranDataLookup.ContainsKey(bsId))
                            {
                                AddACHData(tranDataLookup, row, bsId);
                                if (tranDataLookup[bsId] == null)
                                {
                                    invalidBsIds.Add(bsId);
                                }
                            }
                            else
                            {
                                if (tranDataLookup[bsId].BalanceSheetId != bsId)
                                {
                                    if (tranDataLookup[bsId] == null)
                                    {
                                        if (!invalidBsIds.Contains(bsId))
                                        {
                                            invalidBsIds.Add(bsId);
                                        }
                                    }
                                    else
                                    {
                                        if (!inconsistentBsIds.Contains(bsId))
                                        {
                                            inconsistentBsIds.Add(bsId);
                                        }
                                    }
                                }
                                else
                                {
                                    //若有一樣的直接加總金額
                                    int tranAmount = row.GetCellValue<int>(DirectDepositColumns.TransferAmount);
                                    tranDataLookup[bsId].TransferAmount += tranAmount;
                                }
                            }
                        }
                        else
                        {
                            //沒有該對帳單
                            if (!inconsistentBsIds.Contains(bsId))
                            {
                                inconsistentBsIds.Add(bsId);
                            }
                        }
                    }
                    

                }

                string message = string.Empty;

                if (invalidBsIds.Count > 0 || inconsistentBsIds.Count > 0)
                {
                    message += "請檢查檔案是否有問題!";

                    if (invalidBsIds.Count > 0)
                    {
                        message += string.Format("對帳單ID：{0} 的金額或銀行代碼不完整!\r\n", string.Join(",", invalidBsIds));
                    }
                    if (inconsistentBsIds.Count > 0)
                    {
                        message += string.Format("對帳單ID：{0} 與對應資訊不合!\r\n", string.Join(",", inconsistentBsIds));
                    }
                }


                List<int> failedWprIds;
                string amountMessage = string.Empty;
                if (!TrySaveAchFundTransfers(tranDataLookup, out failedWprIds, out amountMessage))
                {
                    message += string.Format("部分失敗! 對帳單ID：{0} 不成功。請確認後再試一次。", string.Join(",", failedWprIds));

                }

                if (message == string.Empty)
                {
                    message = "匯入成功\r\n";

                    if (amountMessage != string.Empty)
                        message += amountMessage;
                }

                ViewBag.Message = message;
            }

            AchFundMgmtModel model = new AchFundMgmtModel();


            return View("AchFundTransferMgmt",model);
        }

        private Dictionary<int, BalanceSheetUseType> GetBsids(Sheet sheet)
        {
            var result = new Dictionary<int, BalanceSheetUseType>(0);

            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            int i = 0;  //header
            while (rows.MoveNext())
            {
                var row = (HSSFRow)rows.Current;
                if (i == 0)
                {
                    i++;
                    continue;
                }

                int bsid = row.GetCellValue<int>(DirectDepositColumns.BalanceSheetId);
                result.Add(bsid,BalanceSheetUseType.Deal);
            }

            return result;
        }

        private Dictionary<int,BalanceSheetUseType> GetACHBsids(Sheet sheet)
        {
            var result = new Dictionary<int, BalanceSheetUseType>(0);

            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();

            int i = 0;  //header
            while (rows.MoveNext())
            {
                var row = (HSSFRow)rows.Current;
                if (i == 0)
                {
                    i++;
                    continue;
                }


                if (row.GetCellValue<int>(ACHColumns.BalanceSheetId) == 0 && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferTime)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferAmount)) &&
                    string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverAccountName)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverAccountNo)) && 
                    string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverCompanyId)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverBankNo)) && 
                    string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.ReceiverBranchNo)) && string.IsNullOrEmpty(row.GetCellValue<string>(ACHColumns.TransferMemo)))
                {
                    //全為空則停止
                    break;
                }

                int bsid = row.GetCellValue<int>(ACHColumns.BalanceSheetId);
                BalanceSheetUseType useType = (BalanceSheetUseType)row.GetCellValue<int>(ACHColumns.BalanceSheetUseType);
                if (!result.ContainsKey(bsid))
                    result.Add(bsid, useType);
            }

            return result;
        }

        private Dictionary<int, BalanceSheet> bsPool;
        private Dictionary<int, BalanceSheetWm> bswPool;

        private void SetupBalanceSheetPool(Dictionary<int, BalanceSheetUseType> excelBsids)
        {
            List<int> bs = excelBsids.Where(x => x.Value == BalanceSheetUseType.Deal).Select(x => x.Key).ToList();
            List<int> bsw = excelBsids.Where(x => x.Value == BalanceSheetUseType.Wms).Select(x => x.Key).ToList();
            bsPool = ap.BalanceSheetGetListByIds(bs).ToDictionary(x => x.Id);
            bswPool = ap.BalanceSheetWmsGetListByIds(bsw).ToDictionary(x => x.Id);
        }

        private BalanceSheet GetBalanceSheet(int bsid)
        {
            if (bsPool != null)
            {
                if (!bsPool.ContainsKey(bsid))
                {
                    bsPool.Add(bsid, ap.BalanceSheetGet(bsid));
                }
                return bsPool[bsid];
            }

            return ap.BalanceSheetGet(bsid);
        }

        private BalanceSheetWm GetBalanceSheetWms(int bsid)
        {
            if (bswPool != null)
            {
                if (!bswPool.ContainsKey(bsid))
                {
                    bswPool.Add(bsid, ap.BalanceSheetWmsGet(bsid));
                }
                return bswPool[bsid];
            }

            return ap.BalanceSheetWmsGet(bsid);
        }

        private bool CheckEquivalent(Dictionary<int, DirectDepositData> tranDataLookup, HSSFRow row, int bsId)
        {
            if (tranDataLookup[bsId] == null)
            {
                return false;    // corresponding data already have problem
            }

            DateTime? tranTime = row.GetCellValue<DateTime?>(DirectDepositColumns.TransferTime);
            int? tranAmount = row.GetCellValue<int?>(DirectDepositColumns.TransferAmount);
            string tranMemo = row.GetCellValue<string>(DirectDepositColumns.TransferMemo);

            return DateTime.Equals(tranDataLookup[bsId].TransferTime, tranTime)
                && int.Equals(tranDataLookup[bsId].TransferAmount, tranAmount)
                && string.Equals(tranDataLookup[bsId].TransferMemo, tranMemo);
        }

        /// <summary>
        /// 把該列資料轉成 DirectDepositData 加到字典裡.
        /// 金額, 日期必填, 備註非必要
        /// 若不符合規則, 則會在字典裡加入 null
        /// </summary>
        private void AddData(Dictionary<int, DirectDepositData> tranDataLookup, HSSFRow row, int bsId)
        {
            DateTime tranTime = row.GetCellValue<DateTime>(DirectDepositColumns.TransferTime);
            int? tranAmount = row.GetCellValue<int?>(DirectDepositColumns.TransferAmount);
            string tranMemo = row.GetCellValue<string>(DirectDepositColumns.TransferMemo);

            if (tranMemo != null && tranMemo.Length > 200)
            {
                tranMemo = tranMemo.Substring(0, 200);
            }

            //日期跟金額一定要同時存在, 第一次寫入的時候就會被當作匯款成功
            //後續的 excel 更改只有備註會被更新
            if (tranAmount.HasValue)   //valid excel data
            {
                tranDataLookup.Add(bsId, new DirectDepositData
                {
                    BalanceSheetId = bsId,
                    TransferTime = tranTime,
                    TransferAmount = tranAmount,
                    TransferMemo = tranMemo
                });
            }
            else if (!tranAmount.HasValue) //valid empty excel data (memo will be discarded)
            {
                //use BalanceSheetId == 0 as empty excel data
                tranDataLookup.Add(bsId, new DirectDepositData
                {
                    BalanceSheetId = 0,
                    TransferTime = tranTime,
                    TransferAmount = tranAmount,
                    TransferMemo = tranMemo
                });
            }
            else  // invalid excel data
            {
                tranDataLookup.Add(bsId, null);
            }
        }

        private void AddACHData(Dictionary<int, DirectDepositData> tranDataLookup, HSSFRow row, int bsId)
        {
            DateTime tranTime = row.GetCellValue<DateTime>(ACHColumns.TransferTime);
            int? tranAmount = null;
            if (row.Cells[2].CellType == CellType.STRING)
                tranAmount = Convert.ToInt32(row.GetCellValue<string>(ACHColumns.TransferAmount));
            else if (row.Cells[2].CellType == CellType.NUMERIC)
                tranAmount = row.GetCellValue<int?>(ACHColumns.TransferAmount);
            string tranMemo = row.GetCellValue<string>(ACHColumns.TransferMemo);
            string accountTitle = row.GetCellValue<string>(ACHColumns.ReceiverAccountName);
            string companyId = row.GetCellValue<string>(ACHColumns.ReceiverCompanyId);
            string accountNo = row.GetCellValue<string>(ACHColumns.ReceiverAccountNo);
            string bankNo = row.GetCellValue<string>(ACHColumns.ReceiverBankNo);
            string branchNo = row.GetCellValue<string>(ACHColumns.ReceiverBranchNo);
            BalanceSheetUseType useType = (BalanceSheetUseType)row.GetCellValue<int>(ACHColumns.BalanceSheetUseType);
            if (tranMemo != null && tranMemo.Length > 200)
            {
                tranMemo = tranMemo.Substring(0, 200);
            }

            //日期跟金額一定要同時存在, 第一次寫入的時候就會被當作匯款成功
            //後續的 excel 更改只有備註會被更新

            if (bankNo.Length > 3 || branchNo.Length > 4)
            {
                //檢查銀行代碼長度
                tranDataLookup.Add(bsId, null);
            }
            else if (tranAmount.HasValue)   //valid excel data
            {
                tranDataLookup.Add(bsId, new DirectDepositData
                {
                    BalanceSheetId = bsId,
                    TransferTime = tranTime,
                    TransferAmount = tranAmount,
                    TransferMemo = tranMemo,
                    AccountTitle = accountTitle,
                    CompanyId = companyId,
                    AccountNo = accountNo,
                    BankNo = bankNo,
                    BranchNo = branchNo,
                    UseType = useType
                });
            }
            else if (!tranAmount.HasValue) //valid empty excel data (memo will be discarded)
            {
                //use BalanceSheetId == 0 as empty excel data
                tranDataLookup.Add(bsId, new DirectDepositData
                {
                    BalanceSheetId = 0,
                    TransferTime = tranTime,
                    TransferAmount = tranAmount,
                    TransferMemo = tranMemo,
                    AccountTitle = accountTitle,
                    CompanyId = companyId,
                    AccountNo = accountNo,
                    BankNo = bankNo,
                    BranchNo = branchNo,
                    UseType = useType
                });
            }
            else  // invalid excel data
            {
                tranDataLookup.Add(bsId, null);
            }
        }

        private bool CheckHeader(Sheet sheet)
        {
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            if (rows.MoveNext())
            {
                return CheckHeaderRow((HSSFRow)rows.Current);
            }
            return false;
        }

        private bool CheckACHHeader(Sheet sheet)
        {
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            if (rows.MoveNext())
            {
                return CheckACHHeaderRow((HSSFRow)rows.Current);
            }
            return false;
        }

        /// <summary>
        /// 確認 excel 列對帳單 id 對應的基本資料是否跟資料庫的一致
        /// </summary>
        /// <param name="row"></param>
        /// <param name="bsId"></param>
        /// <returns></returns>
        private bool CheckRowBsAgainstDbBs(HSSFRow row, int bsId)
        {
            BalanceSheet bs = GetBalanceSheet(bsId);

            if (bs == null || !bs.IsLoaded)
            {
                return false;
            }

            return
                row.GetCellValue<DateTime>(DirectDepositColumns.IntervalStart) == bs.IntervalStart
                && row.GetCellValue<DateTime>(DirectDepositColumns.IntervalEnd) == bs.IntervalEnd
                && row.GetCellValue<int>(DirectDepositColumns.DealId) == bs.ProductId;
        }

        /// <summary>
        /// 確認對帳單是否存在
        /// </summary>
        /// <param name="bsId"></param>
        /// <returns></returns>
        private bool CheckBalanceIsExist(int bsId,BalanceSheetUseType useType)
        {
            if (useType == BalanceSheetUseType.Deal)
            {
                BalanceSheet bs = GetBalanceSheet(bsId);
                if (bs.IsLoaded)
                    return true;
                else
                    return false;
            }
            else
            {
                BalanceSheetWm bs = GetBalanceSheetWms(bsId);
                if (bs.IsLoaded)
                    return true;
                else
                    return false;
            }
            
        }

        private bool TrySaveFundTransfers(Dictionary<int, DirectDepositData> excelData, out List<int> failedBsIds)
        {
            bool success = true;
            List<int> failedIds = new List<int>();

            Dictionary<int, DirectDepositData> filteredData =
                excelData.Where(kvp => kvp.Value != null && kvp.Value.BalanceSheetId != 0).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            List<int> bsIds = filteredData.Keys.ToList();

            ViewBalanceSheetFundTransferCollection bsDataAll = ap.ViewBalanceSheetFundTransferGetListByBalanceSheetIds(bsIds);
            IEnumerable<ViewBalanceSheetFundTransfer> bsDataForInserts = bsDataAll.Where(x => !x.PayReportId.HasValue);
            Dictionary<int, FundTransfer> bsNewFundTransfers = PrepareNewFundData(bsDataForInserts, filteredData);
            Dictionary<int, BalanceSheet> sheets = ap.BalanceSheetGetListByIds(bsNewFundTransfers.Keys.ToList()).ToDictionary(x => x.Id);


            System.Threading.Tasks.Parallel.ForEach(bsNewFundTransfers, pair =>
            {
                BalanceSheet sheet = sheets[pair.Key];
                try
                {
                    using (var scope = new TransactionScope())
                    {
                        if (sheet.BalanceSheetType == (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet)
                        {
                            if (pair.Value.TransferTime.HasValue)
                            {
                                sheet.Year = pair.Value.TransferTime.Value.Year;
                                sheet.Month = pair.Value.TransferTime.Value.Month;
                            }
                        }
                        ap.FundTransferSet(pair.Value);
                        sheet.PayReportId = pair.Value.Id;
                        ap.BalanceSheetSet(sheet);
                        scope.Complete();
                    }
                }
                catch
                {
                    success = false;
                    failedIds.Add(sheet.Id);
                }
            });


            IEnumerable<ViewBalanceSheetFundTransfer> bsDataForUpdates = bsDataAll.Where(x => x.PayReportId.HasValue && !int.Equals(x.PayReportId, 0));
            List<int> ftUpdateBsIds;
            FundTransferCollection updateMemos = PrepareMemoUpdates(bsDataForUpdates, filteredData, out ftUpdateBsIds);

            try
            {
                using (var scope = new TransactionScope())
                {
                    ap.FundTransferSetList(updateMemos);
                    scope.Complete();
                }
            }
            catch
            {
                failedIds.AddRange(ftUpdateBsIds);
                success = false;
            }

            failedBsIds = failedIds;
            return success;
        }

        private bool TrySaveAchFundTransfers(Dictionary<int, DirectDepositData> excelData, out List<int> failedWprIds, out string amountMessage)
        {
            bool success = true;
            int successCount = 0;
            List<int> failedIds = new List<int>();

            Dictionary<int, DirectDepositData> filteredData = excelData.Where(kvp => kvp.Value != null && kvp.Value.BalanceSheetId != 0 && kvp.Value.UseType == BalanceSheetUseType.Deal)
                                                                       .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            Dictionary<int, DirectDepositData> filteredWmsData = excelData.Where(kvp => kvp.Value != null && kvp.Value.BalanceSheetId != 0 && kvp.Value.UseType == BalanceSheetUseType.Wms)
                                                                       .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            List<int> bsIds = filteredData.Keys.ToList();
            List<int> bswIds = filteredWmsData.Keys.ToList();

            Guid reportGuid = Guid.NewGuid();

            foreach (int bsId in bsIds)
            {

                BalanceSheet bs = ap.BalanceSheetGet(bsId);
                var balanceSheet = new BalanceSheetModel(bsId);
                //產過就不重產了
                if (!bs.PayReportId.HasValue)
                {
                    int payReportId = 0;
                    if (bs.EstAmount != 0 || balanceSheet.GetAccountsPayable().ReceiptAmount != 0)
                    {
                        BusinessHour bh = sp.BusinessHourGet(bs.ProductGuid);
                        CashTrustLogCollection ctLogNormal = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Normal);
                        CashTrustLogCollection ctLogDeduction = mp.CashTrustLogGetListByBalanceSheetId(bs.Id, BalanceSheetDetailStatus.Deduction);

                        DealCostCollection costs = pp.DealCostGetList(bs.ProductGuid);
                        DealProperty dp = pp.DealPropertyGet(bs.ProductGuid);
                        decimal cost = (costs.Any() && costs.Max(x => x.Cost).HasValue) ? costs.Max(x => x.Cost).Value : 0;
                        cost = cost > 0 && dp.SaleMultipleBase > 0 ? cost / dp.SaleMultipleBase.Value : cost;
                        var bsModel = new BalanceSheetModel(bs.Id);

                        //填入CashTrustLog報表代號
                        mp.CashTrustLogToUpdateForAch(reportGuid, bs.Id);

                        #region 新增WeeklyPayReport資料
                        WeeklyPayReport report = new WeeklyPayReport();

                        report.BusinessHourGuid = bs.ProductGuid;
                        report.IntervalStart = bs.IntervalStart;
                        report.IntervalEnd = bs.IntervalEnd;
                        report.CreditCard = ctLogNormal.Sum(x => x.CreditCard) - ctLogDeduction.Sum(x => x.CreditCard);
                        report.Atm = ctLogNormal.Sum(x => x.Atm) - ctLogDeduction.Sum(x => x.Atm);
                        report.ReportGuid = reportGuid;
                        report.IsLastWeek = (bs.IntervalStart <= bh.BusinessHourDeliverTimeE && bh.BusinessHourDeliverTimeE < bs.IntervalEnd);
                        report.Cost = cost;
                        report.TotalCount = ctLogNormal.Count() - ctLogDeduction.Count();
                        report.TotalSum = bsModel.GetAccountsPayable().ReceiptAmount;
                        report.UserId = UserName;
                        report.CreateTime = DateTime.Now;
                        report.StoreGuid = bs.StoreGuid;
                        report.FundTransferType = (int)FundTransferType.FinDept;

                        payReportId = op.SetWeeklyPayReport(report);
                        #endregion

                    }


                    #region 對帳單回填reportId
                    bs.PayReportId = payReportId;
                    ap.BalanceSheetSet(bs);
                    #endregion



                }

            }


            //參數最多2100,但匯入可能上千筆
            //ViewBalanceSheetFundsTransferPponCollection bsDataAll = ap.ViewBalanceSheetFundsTransferPponGetListByBalanceSheetIds(bsIds);
            ViewBalanceSheetFundsTransferPponCollection bsDataAll = new ViewBalanceSheetFundsTransferPponCollection();
            int cnt = bsIds.Count();
            int idx = 0;
            while (idx <= cnt - 1)
            {
                int batchLimit = 1000;
                var newBsIds = bsIds.Skip(idx).Take(batchLimit);
                bsDataAll.AddRange(ap.ViewBalanceSheetFundsTransferPponGetListByBalanceSheetIds(newBsIds));

                idx += batchLimit;
            }
            
            IEnumerable<ViewBalanceSheetFundsTransferPpon> bsDataForInserts = bsDataAll.Where(x => x.PayId.HasValue);
            string message = string.Empty;
            if (bsDataForInserts.Any()) {
                WeeklyPayReportCollection bsNewFundTransfers = PrepareNewFundsData(bsDataForInserts, filteredData, out message);

                System.Threading.Tasks.Parallel.ForEach(bsNewFundTransfers, pair =>
                {
                    WeeklyPayReport updateReport = pair;
                    try
                    {
                        using (var scope = new TransactionScope())
                        {
                            int result = op.SetWeeklyPayReport(updateReport);
                            successCount += result;
                            scope.Complete();
                        }
                    }
                    catch
                    {
                        success = false;
                        failedIds.Add(updateReport.Id);
                    }
                });
            }
            else
            {
                success = false;
            }


            int wmsPayReportId = 0;
            string noCorrespond = string.Empty;
            string exist = string.Empty;
            try
            {
                if (success || !bsDataForInserts.Any())
                {
                    foreach (int bsId in bswIds)
                    {
                        BalanceSheetWm bs = ap.BalanceSheetWmsGet(bsId);

                        //產過就不重產了
                        if (!bs.PayReportId.HasValue)
                        {
                            
                            if (bs.EstAmount != 0)
                            {
                                #region 新增WeeklyPayReport資料
                                WeeklyPayReportWm report = new WeeklyPayReportWm();

                                decimal amount = (decimal)filteredWmsData[bs.Id].TransferAmount;
                                report.SellerGuid = bs.SellerGuid;
                                report.IntervalStart = bs.IntervalStart;
                                report.IntervalEnd = bs.IntervalEnd;
                                report.TotalSum = 0;
                                report.FundTransferType = (int)FundTransferType.FinDept;
                                wmsPayReportId = op.SetWeeklyPayReportWms(report);

                                report.ResponseTime = filteredWmsData[bs.Id].TransferTime;
                                report.TransferAmount = amount;
                                report.IsTransferComplete = true;
                                report.ReceiverBankNo = filteredWmsData[bs.Id].BankNo;
                                report.ReceiverBranchNo = (filteredWmsData[bs.Id].BranchNo != null && filteredWmsData[bs.Id].BranchNo.Length > 4 ? filteredWmsData[bs.Id].BranchNo.Substring(0, 4) : filteredWmsData[bs.Id].BranchNo);
                                report.ReceiverAccountNo = filteredWmsData[bs.Id].AccountNo;
                                report.ReceiverCompanyId = filteredWmsData[bs.Id].CompanyId;
                                report.ReceiverTitle = filteredWmsData[bs.Id].AccountTitle;
                                report.Memo = filteredWmsData[bs.Id].TransferMemo;
                                report.UserId = UserName;
                                report.CreateTime = DateTime.Now;
                                op.SetWeeklyPayReportWms(report);

                                if (bs.EstAmount != amount)
                                    noCorrespond += "對帳單ID：" + bs.Id.ToString() + " 實際付款總額($" + amount + ") 與 對帳單應付金額($" + bs.EstAmount + ") 不一致\r\n";
                                #endregion
                            }


                            #region 對帳單回填reportId
                            bs.PayReportId = wmsPayReportId;
                            ap.BalanceSheetWmsSet(bs);
                            #endregion
                        }
                        else
                        {
                            exist += bs.Id.ToString() + "、";

                        }

                        if (!string.IsNullOrEmpty(exist))
                        {
                            exist = "對帳單ID：" + exist.TrimEnd("、") + " 已有資訊存入，若需要調整，請於後台「實付總額」欄位進行調整\r\n";
                        }

                    }
                    message += noCorrespond + exist;
                    success = true;

                }
            }
            catch (Exception ex)
            {
                success = false;
                failedIds.Add(wmsPayReportId);
            }
            

            failedWprIds = failedIds;
            amountMessage = message;
            return success;
        }

        private WeeklyPayReportCollection PrepareNewFundsData(IEnumerable<ViewBalanceSheetFundsTransferPpon> bsDataForInserts, Dictionary<int, DirectDepositData> excelDataBsLookup, out string message)
        {
            WeeklyPayReportCollection updates = new WeeklyPayReportCollection();

            DateTime now = DateTime.Now;
            int userId = MemberFacade.GetUniqueId(this.HttpContext.User.Identity.Name);
            string noCorrespond = string.Empty;
            string exist = string.Empty;

            foreach (ViewBalanceSheetFundsTransferPpon bs in bsDataForInserts)
            {
                if (!excelDataBsLookup[bs.BsId].TransferAmount.HasValue
                    || !excelDataBsLookup[bs.BsId].TransferTime.HasValue)
                {
                    continue;
                }

                int amount = excelDataBsLookup[bs.BsId].TransferAmount.Value;
                int? accountId;

                if (amount >= 0)
                {
                    accountId = ap.BankAccountGetId(excelDataBsLookup[bs.BsId].BankNo, excelDataBsLookup[bs.BsId].BranchNo, excelDataBsLookup[bs.BsId].AccountNo, excelDataBsLookup[bs.BsId].CompanyId, excelDataBsLookup[bs.BsId].AccountTitle);
                }
                else
                {
                    accountId = null;
                }

                WeeklyPayReport wpr = op.GetWeeklyPayReportById((int)bs.PayId);
                var balanceSheet = new BalanceSheetModel(bs.BsId);
                if (wpr == null || !wpr.IsLoaded || wpr.Id == 0)
                {
                    continue;
                }
                if (wpr.Result != null)
                {
                    continue;
                }



                if (wpr.IsLoaded && wpr.TransferAmount != null && wpr.ResponseTime != null)
                    exist += bs.BsId.ToString() + "、";
                else
                {
                    if (bs.EstAmount != amount)
                        noCorrespond += "對帳單ID：" + bs.BsId.ToString() + " 實際付款總額($" + amount + ") 與 對帳單應付金額($" + bs.EstAmount + ") 不一致\r\n";

                    wpr.ResponseTime = excelDataBsLookup[bs.BsId].TransferTime;
                    wpr.TransferAmount = (decimal)amount;
                    wpr.IsTransferComplete = true;
                    wpr.ReceiverBankNo = excelDataBsLookup[bs.BsId].BankNo;
                    wpr.ReceiverBranchNo = (excelDataBsLookup[bs.BsId].BranchNo != null && excelDataBsLookup[bs.BsId].BranchNo.Length > 4 ? excelDataBsLookup[bs.BsId].BranchNo.Substring(0, 4) : excelDataBsLookup[bs.BsId].BranchNo);
                    wpr.ReceiverAccountNo = excelDataBsLookup[bs.BsId].AccountNo;
                    wpr.ReceiverCompanyId = excelDataBsLookup[bs.BsId].CompanyId;
                    wpr.ReceiverTitle = excelDataBsLookup[bs.BsId].AccountTitle;
                    wpr.Memo = excelDataBsLookup[bs.BsId].TransferMemo;
                    updates.Add(wpr);
                }
                
            }
            if (!string.IsNullOrEmpty(exist))
            {
                exist = "對帳單ID：" + exist.TrimEnd("、") + " 已有資訊存入，若需要調整，請於後台「實付總額」欄位進行調整\r\n";
            }

            message = noCorrespond + exist;

            return updates;
        }

        private Dictionary<int, FundTransfer> PrepareNewFundData(IEnumerable<ViewBalanceSheetFundTransfer> bsDataForInserts, Dictionary<int, DirectDepositData> excelDataBsLookup)
        {
            var result = new Dictionary<int, FundTransfer>();

            DateTime now = DateTime.Now;
            int userId = MemberFacade.GetUniqueId(this.HttpContext.User.Identity.Name);

            foreach (ViewBalanceSheetFundTransfer bs in bsDataForInserts)
            {
                if (!excelDataBsLookup[bs.BalanceSheetId].TransferAmount.HasValue
                    || !excelDataBsLookup[bs.BalanceSheetId].TransferTime.HasValue)
                {
                    continue;
                }

                int amount = excelDataBsLookup[bs.BalanceSheetId].TransferAmount.Value;
                int? accountId;

                if (amount != 0)
                {
                    accountId = ap.BankAccountGetId(bs.DealBankNo, bs.DealBranchNo, bs.DealAccountNo, bs.DealCompanyId, bs.DealAccountTitle);
                }
                else
                {
                    accountId = null;
                }

                var bsTransfer = new FundTransfer
                {
                    FundTransferType = (amount >= 0 ? (int)FundTransferType.FinDept : (int)FundTransferType.VendorRefund),
                    DealUid = bs.DealId,
                    BizModel = bs.ProductType,
                    IntervalStart = bs.IntervalStart,
                    IntervalEnd = bs.IntervalEnd,
                    CreateId = userId,
                    CreateTime = now,
                    TransferAmount = amount,
                    TransferTime = excelDataBsLookup[bs.BalanceSheetId].TransferTime,
                    Memo = excelDataBsLookup[bs.BalanceSheetId].TransferMemo,
                    IsTransferComplete = true,
                    BankAccountId = accountId
                };

                result.Add(bs.BalanceSheetId, bsTransfer);
            }

            return result;
        }
        private FundTransferCollection PrepareMemoUpdates(IEnumerable<ViewBalanceSheetFundTransfer> bsDataForUpdates, Dictionary<int, DirectDepositData> excelDataBsLookup, out List<int> relatedBalanceSheetIds)
        {
            relatedBalanceSheetIds = new List<int>();
            var result = new FundTransferCollection();

            DateTime now = DateTime.Now;
            int userId = MemberFacade.GetUniqueId(this.HttpContext.User.Identity.Name);

            foreach (var bsData in bsDataForUpdates)
            {
                if (!string.Equals(bsData.FundMemo, excelDataBsLookup[bsData.BalanceSheetId].TransferMemo))
                {
                    FundTransfer fund = ap.FundTransferGet(bsData.PayReportId.Value);
                    fund.Memo = excelDataBsLookup[bsData.BalanceSheetId].TransferMemo;
                    fund.ModifyId = userId;
                    fund.ModifyTime = now;
                    result.Add(fund);
                    relatedBalanceSheetIds.Add(bsData.BalanceSheetId);
                }
            }

            return result;
        }

        private bool CheckHeaderRow(HSSFRow headerRow)
        {
            Dictionary<DirectDepositColumns, string> headers = GetHeaderTexts();

            foreach (KeyValuePair<DirectDepositColumns, string> header in headers)
            {
                if (headerRow.GetCell((int)header.Key).StringCellValue != header.Value)
                {
                    return false;
                }
            }

            return true;
        }

        private bool CheckACHHeaderRow(HSSFRow headerRow)
        {
            Dictionary<ACHColumns, string> headers = GetACHHeaderTexts();

            foreach (KeyValuePair<ACHColumns, string> header in headers)
            {
                if (headerRow.GetCell((int)header.Key) == null || headerRow.GetCell((int)header.Key).StringCellValue != header.Value)
                {
                    return false;
                }
            }

            return true;
        }

        [HttpGet]
        public ActionResult DirectDepositFundTransferMgmt()
        {
            var model = new DirectDepositFundMgmtModel();
            model.TrxCriteria = TrxCriteria.Others;
            model.ShowSysMessage = false;
            return View(model);
        }

        [HttpPost]
        public ActionResult DirectDepositFundTransferMgmt(DirectDepositFundMgmtModel model)
        {
            bool validSearchCriteria =
                model.IsDealSelected ? model.DealId.HasValue : !string.IsNullOrWhiteSpace(model.BillCreator)
                || model.Vts.HasValue
                || model.Vte.HasValue
                || model.Cts.HasValue
                || model.Cte.HasValue;

            if (!validSearchCriteria)
            {
                model.ShowSysMessage = true;
                return View(model);
            }

            ViewBalanceSheetBillFundTransferCollection fundTransfers;

            int? dealId = null;
            string billCreator = null;
            if (model.IsDealSelected)
            {
                dealId = model.DealId;
            }
            else
            {
                billCreator = model.BillCreator;
            }

            bool? transferComplete;
            switch (model.TrxCriteria)
            {
                case TrxCriteria.Completed:
                    transferComplete = true;
                    break;

                case TrxCriteria.Others:
                    transferComplete = false;
                    break;

                case TrxCriteria.All:
                default:
                    transferComplete = null;
                    break;
            }

            List<BalanceSheetType> bsTypes = new List<BalanceSheetType>{
                BalanceSheetType.ManualTriggered,
                BalanceSheetType.ManualMonthlyPayBalanceSheet,
                BalanceSheetType.ManualWeeklyPayWeekBalanceSheet,
            };

            fundTransfers = ap.ViewBalanceSheetBillFundTransferGetList(bsTypes, model.Biz, dealId, billCreator, model.Vts, model.Vte, model.Cts, model.Cte, transferComplete);

            //請款凍結檔次不產出匯款檔案
            var dealFreezeInfos = pp.DealAccountingGet(fundTransfers.Select(x => x.ProductGuid))
                .ToDictionary(x => x.BusinessHourGuid,
                                   x => Helper.IsFlagSet(x.Flag, (int)AccountingFlag.Freeze));


            IEnumerable<ViewBalanceSheetBillFundTransfer> nonZeroTransfers = fundTransfers.Where(x => !int.Equals(x.PayReportId, 0))
                .Where(x => dealFreezeInfos.ContainsKey(x.ProductGuid) && !dealFreezeInfos[x.ProductGuid]);

            if (this.HttpContext.Request.Form["Excel"] == null)
            {
                model.Transfers = CovertToDirectDepositFundTransferInfo(nonZeroTransfers);
                return View(model);
            }
            else
            {
                Workbook workbook = CovertToExcel(nonZeroTransfers);
                return Excel(workbook, "付款報表.xls");
            }
        }

        private Workbook CovertToExcel(IEnumerable<ViewBalanceSheetBillFundTransfer> dbInfos)
        {
            Workbook result = new HSSFWorkbook();
            Dictionary<NpoiCellStyle, CellStyle> styles = CreateStyles(result);

            Sheet sheet = result.CreateSheet("付款頁");
            Row headerRow = sheet.CreateRow(0);
            SetHeader(headerRow, styles);

            int i = 1;
            foreach (var info in dbInfos)
            {
                Row dataRow = sheet.CreateRow(i);
                SetData(dataRow, info, styles);
                i++;
            }

            SetWidth(sheet);

            return result;
        }

        private Dictionary<NpoiCellStyle, CellStyle> CreateStyles(Workbook workbook)
        {
            var result = new Dictionary<NpoiCellStyle, CellStyle>();

            Dictionary<NpoiFont, Font> fonts = CreateFonts(workbook);
            CreationHelper helper = workbook.GetCreationHelper();

            CellStyle datetimeStyle = workbook.CreateCellStyle();
            datetimeStyle.Alignment = HorizontalAlignment.RIGHT;
            datetimeStyle.DataFormat = helper.CreateDataFormat().GetFormat("yyyy/mm/dd h:mm:ss;;");
            datetimeStyle.SetFont(fonts[NpoiFont.Black]);
            result.Add(NpoiCellStyle.DateTime, datetimeStyle);

            CellStyle moneyStyle = workbook.CreateCellStyle();
            moneyStyle.DataFormat = helper.CreateDataFormat().GetFormat("$#,##0;-$#,##0;");
            moneyStyle.SetFont(fonts[NpoiFont.Black]);
            result.Add(NpoiCellStyle.MoneyEmptyWhenZeroOrNull, moneyStyle);

            CellStyle headerStyle = workbook.CreateCellStyle();
            headerStyle.Alignment = HorizontalAlignment.CENTER;
            headerStyle.BackgroundColor(HSSFColor.INDIGO.index);
            headerStyle.SetFont(fonts[NpoiFont.WhiteBold]);
            result.Add(NpoiCellStyle.Header, headerStyle);

            CellStyle dateStyle = workbook.CreateCellStyle();
            dateStyle.Alignment = HorizontalAlignment.RIGHT;
            dateStyle.DataFormat = helper.CreateDataFormat().GetFormat("yyyy/mm/dd;;");
            result.Add(NpoiCellStyle.Date, dateStyle);

            CellStyle moneyZeroWhenNullStyle = workbook.CreateCellStyle();
            moneyZeroWhenNullStyle.DataFormat = helper.CreateDataFormat().GetFormat("$#,##0;-$#,##0;$0");
            moneyZeroWhenNullStyle.SetFont(fonts[NpoiFont.Black]);
            result.Add(NpoiCellStyle.MoneyZeroWhenZeroOrNull, moneyZeroWhenNullStyle);

            return result;
        }

        private Dictionary<NpoiFont, Font> CreateFonts(Workbook workbook)
        {
            var result = new Dictionary<NpoiFont, Font>();

            Font f1 = workbook.CreateFont();
            f1.Color = HSSFColor.WHITE.index;
            f1.Boldweight = (short)FontBoldWeight.BOLD;
            result.Add(NpoiFont.WhiteBold, f1);

            Font f2 = workbook.CreateFont();
            f2.Color = HSSFColor.BLACK.index;
            result.Add(NpoiFont.Black, f2);

            return result;
        }

        private enum NpoiCellStyle
        {
            Header,
            DateTime,
            Date,
            MoneyEmptyWhenZeroOrNull,
            MoneyZeroWhenZeroOrNull
        }

        private enum NpoiFont
        {
            WhiteBold,
            Black
        }

        private enum DirectDepositColumns
        {
            BalanceSheetId = 0,
            TransferTime = 1,
            TransferAmount = 2,
            TransferMemo = 3,
            BillCreateTime = 4,
            Type = 5,
            DealId = 6,
            DealName = 7,
            StoreName = 8,
            IntervalStart = 9,
            IntervalEnd = 10,
            BalanceSheetTotal = 11,
            SignCompanyId = 12,
            SignCompanyName = 13,
            BillNumber = 14,
            BillDate = 15,
            BillSubtotal = 16,
            BillTax = 17,
            BillTotal = 18,
            BillMemo = 19,
            ReceiverAccountName = 20,
            ReceiverCompanyId = 21,
            ReceiverAccountNo = 22,
            ReceiverBankNo = 23,
            ReceiverBranchNo = 24,
            BillCreator = 25,
            SalesName = 26
        }

        private enum ACHColumns
        {
            TransferTime = 0,
            BalanceSheetId = 1,
            TransferAmount = 2,
            ReceiverAccountName = 3,
            ReceiverCompanyId = 4,
            ReceiverAccountNo = 5,
            ReceiverBankNo = 6,
            ReceiverBranchNo = 7,
            TransferMemo = 8,
            BalanceSheetUseType = 9,
        }

        private Dictionary<DirectDepositColumns, string> headerTexts = null;

        private Dictionary<DirectDepositColumns, string> GetHeaderTexts()
        {
            if (headerTexts != null)
            {
                return headerTexts;
            }

            headerTexts = new Dictionary<DirectDepositColumns, string>()
                                        {
                                            {DirectDepositColumns.BalanceSheetId, "對帳單ID"},
                                            {DirectDepositColumns.TransferTime, "付款日期"},
                                            {DirectDepositColumns.TransferAmount, "實付總額"},
                                            {DirectDepositColumns.TransferMemo, "備註(200字以內)"},
                                            {DirectDepositColumns.BillCreateTime, "單據建立時間"},
                                            {DirectDepositColumns.Type, "類型"},
                                            {DirectDepositColumns.DealId, "檔號"},
                                            {DirectDepositColumns.DealName, "檔名"},
                                            {DirectDepositColumns.StoreName, "分店"},
                                            {DirectDepositColumns.IntervalStart, "核銷日(起)"},
                                            {DirectDepositColumns.IntervalEnd, "核銷日(訖)"},
                                            {DirectDepositColumns.BalanceSheetTotal, "對帳單總額"},
                                            {DirectDepositColumns.SignCompanyId, "簽約統編"},
                                            {DirectDepositColumns.SignCompanyName, "簽約公司"},
                                            {DirectDepositColumns.BillNumber, "發票號碼"},
                                            {DirectDepositColumns.BillDate, "發票日期"},
                                            {DirectDepositColumns.BillSubtotal, "未稅金額"},
                                            {DirectDepositColumns.BillTax, "稅額"},
                                            {DirectDepositColumns.BillTotal, "總額"},
                                            {DirectDepositColumns.BillMemo, "單據備註"},
                                            {DirectDepositColumns.ReceiverAccountName, "受款戶名"},
                                            {DirectDepositColumns.ReceiverCompanyId, "受款ID"},
                                            {DirectDepositColumns.ReceiverAccountNo, "受款帳號"},
                                            {DirectDepositColumns.ReceiverBankNo, "銀行代號"},
                                            {DirectDepositColumns.ReceiverBranchNo, "分行代號"},
                                            {DirectDepositColumns.BillCreator, "建表人"},
                                            {DirectDepositColumns.SalesName, "業務"}
                                        };
            return headerTexts;
        }

        private Dictionary<ACHColumns, string> achHeaderTexts = null;

        private Dictionary<ACHColumns, string> GetACHHeaderTexts()
        {
            if (achHeaderTexts != null)
            {
                return achHeaderTexts;
            }

            achHeaderTexts = new Dictionary<ACHColumns, string>()
                                        {
                                            {ACHColumns.TransferTime, "實際付款日期"},
                                            {ACHColumns.BalanceSheetId, "對帳單ID"},
                                            {ACHColumns.TransferAmount, "付款金額(總額)"},
                                            {ACHColumns.ReceiverAccountName, "受款戶名"},
                                            {ACHColumns.ReceiverCompanyId, "受款人ID"},
                                            {ACHColumns.ReceiverAccountNo, "受款帳號"},
                                            {ACHColumns.ReceiverBankNo, "受款銀行代號"},
                                            {ACHColumns.ReceiverBranchNo, "受款分行代號"},
                                            {ACHColumns.TransferMemo, "備註"},
                                            {ACHColumns.BalanceSheetUseType, "對帳單類型(1:檔次2:倉儲)"}
                                        };
            return achHeaderTexts;
        }

        private void SetHeader(Row headerRow, Dictionary<NpoiCellStyle, CellStyle> styleHelper)
        {
            Dictionary<DirectDepositColumns, string> headers = GetHeaderTexts();
            headerRow.CreateCell(DirectDepositColumns.BalanceSheetId).SetValue(headers[DirectDepositColumns.BalanceSheetId]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.TransferTime).SetValue(headers[DirectDepositColumns.TransferTime]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.TransferAmount).SetValue(headers[DirectDepositColumns.TransferAmount]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.TransferMemo).SetValue(headers[DirectDepositColumns.TransferMemo]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillCreateTime).SetValue(headers[DirectDepositColumns.BillCreateTime]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.Type).SetValue(headers[DirectDepositColumns.Type]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.DealId).SetValue(headers[DirectDepositColumns.DealId]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.DealName).SetValue(headers[DirectDepositColumns.DealName]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.StoreName).SetValue(headers[DirectDepositColumns.StoreName]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.IntervalStart).SetValue(headers[DirectDepositColumns.IntervalStart]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.IntervalEnd).SetValue(headers[DirectDepositColumns.IntervalEnd]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BalanceSheetTotal).SetValue(headers[DirectDepositColumns.BalanceSheetTotal]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.SignCompanyId).SetValue(headers[DirectDepositColumns.SignCompanyId]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.SignCompanyName).SetValue(headers[DirectDepositColumns.SignCompanyName]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillNumber).SetValue(headers[DirectDepositColumns.BillNumber]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillDate).SetValue(headers[DirectDepositColumns.BillDate]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillSubtotal).SetValue(headers[DirectDepositColumns.BillSubtotal]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillTax).SetValue(headers[DirectDepositColumns.BillTax]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillTotal).SetValue(headers[DirectDepositColumns.BillTotal]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillMemo).SetValue(headers[DirectDepositColumns.BillMemo]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.ReceiverAccountName).SetValue(headers[DirectDepositColumns.ReceiverAccountName]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.ReceiverCompanyId).SetValue(headers[DirectDepositColumns.ReceiverCompanyId]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.ReceiverAccountNo).SetValue(headers[DirectDepositColumns.ReceiverAccountNo]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.ReceiverBankNo).SetValue(headers[DirectDepositColumns.ReceiverBankNo]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.ReceiverBranchNo).SetValue(headers[DirectDepositColumns.ReceiverBranchNo]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.BillCreator).SetValue(headers[DirectDepositColumns.BillCreator]).SetStyle(styleHelper[NpoiCellStyle.Header]);
            headerRow.CreateCell(DirectDepositColumns.SalesName).SetValue(headers[DirectDepositColumns.SalesName]).SetStyle(styleHelper[NpoiCellStyle.Header]);
        }

        private void SetData(Row dataRow, ViewBalanceSheetBillFundTransfer info, Dictionary<NpoiCellStyle, CellStyle> styleHelper)
        {
            dataRow.CreateCell(DirectDepositColumns.BalanceSheetId, CellType.NUMERIC).SetCellValue(info.BalanceSheetId);
            dataRow.CreateCell(DirectDepositColumns.TransferTime, CellType.NUMERIC).SetValue(info.TransferTime).SetStyle(styleHelper[NpoiCellStyle.Date]);
            dataRow.CreateCell(DirectDepositColumns.TransferAmount, CellType.NUMERIC).SetValue(info.TransferAmount).SetStyle(styleHelper[NpoiCellStyle.MoneyZeroWhenZeroOrNull]);
            dataRow.CreateCell(DirectDepositColumns.TransferMemo, CellType.STRING).SetCellValue(info.FundMemo);
            dataRow.CreateCell(DirectDepositColumns.BillCreateTime, CellType.NUMERIC).SetValue(info.BillCreateTime).SetStyle(styleHelper[NpoiCellStyle.DateTime]);
            dataRow.CreateCell(DirectDepositColumns.Type, CellType.STRING).SetCellValue(GetTypeDesc(info));
            dataRow.CreateCell(DirectDepositColumns.DealId, CellType.STRING).SetCellValue(info.DealId);
            dataRow.CreateCell(DirectDepositColumns.DealName, CellType.STRING).SetCellValue(info.DealName);
            dataRow.CreateCell(DirectDepositColumns.StoreName, CellType.STRING).SetCellValue(info.StoreName);
            dataRow.CreateCell(DirectDepositColumns.IntervalStart, CellType.NUMERIC).SetValue(info.IntervalStart).SetStyle(styleHelper[NpoiCellStyle.DateTime]);
            dataRow.CreateCell(DirectDepositColumns.IntervalEnd, CellType.NUMERIC).SetValue(info.IntervalEnd).SetStyle(styleHelper[NpoiCellStyle.DateTime]);
            dataRow.CreateCell(DirectDepositColumns.BalanceSheetTotal, CellType.NUMERIC).SetValue(GetAccountsPayable(info)).SetStyle(styleHelper[NpoiCellStyle.MoneyZeroWhenZeroOrNull]);
            dataRow.CreateCell(DirectDepositColumns.SignCompanyId, CellType.STRING).SetValue(info.SignCompanyId);
            dataRow.CreateCell(DirectDepositColumns.SignCompanyName, CellType.STRING).SetValue(info.SignCompanyName);
            dataRow.CreateCell(DirectDepositColumns.BillNumber, CellType.STRING).SetValue(info.BillNumber);
            dataRow.CreateCell(DirectDepositColumns.BillDate, CellType.NUMERIC).SetValue(info.BillDate).SetStyle(styleHelper[NpoiCellStyle.Date]);
            dataRow.CreateCell(DirectDepositColumns.BillSubtotal, CellType.NUMERIC).SetValue(info.BillSubtotal).SetStyle(styleHelper[NpoiCellStyle.MoneyEmptyWhenZeroOrNull]);
            dataRow.CreateCell(DirectDepositColumns.BillTax, CellType.NUMERIC).SetValue(info.BillTax).SetStyle(styleHelper[NpoiCellStyle.MoneyEmptyWhenZeroOrNull]);
            dataRow.CreateCell(DirectDepositColumns.BillTotal, CellType.NUMERIC).SetValue(info.BillTotal).SetStyle(styleHelper[NpoiCellStyle.MoneyEmptyWhenZeroOrNull]);
            dataRow.CreateCell(DirectDepositColumns.BillMemo, CellType.STRING).SetValue(info.BillRemark);
            if (GetAccountsPayable(info) != 0)
            {
                dataRow.CreateCell(DirectDepositColumns.ReceiverAccountName, CellType.STRING).SetValue(info.ReceiverAccountTitle ?? info.DealAccountTitle);
                dataRow.CreateCell(DirectDepositColumns.ReceiverCompanyId, CellType.STRING).SetValue(info.ReceiverCompanyId ?? info.DealCompanyId);
                dataRow.CreateCell(DirectDepositColumns.ReceiverAccountNo, CellType.STRING).SetValue(info.ReceiverAccountNo ?? info.DealAccountNo);
                dataRow.CreateCell(DirectDepositColumns.ReceiverBankNo, CellType.STRING).SetValue(info.ReceiverBankNo ?? info.DealBankNo);
                dataRow.CreateCell(DirectDepositColumns.ReceiverBranchNo, CellType.STRING).SetValue(info.ReceiverBranchNo ?? info.DealBranchNo);
            }
            dataRow.CreateCell(DirectDepositColumns.BillCreator, CellType.STRING).SetValue(info.BillCreator);
            dataRow.CreateCell(DirectDepositColumns.SalesName, CellType.STRING).SetValue(info.SalesName);
        }

        private void SetWidth(Sheet sheet)
        {
            sheet.SetColWidth(DirectDepositColumns.BalanceSheetId, 10);
            sheet.SetColWidth(DirectDepositColumns.TransferTime, 18);
            sheet.SetColWidth(DirectDepositColumns.TransferAmount, 10);
            sheet.SetColWidth(DirectDepositColumns.TransferMemo, 30);
            sheet.SetColWidth(DirectDepositColumns.BillCreateTime, 18);
            sheet.SetColWidth(DirectDepositColumns.TransferTime, 18);
            sheet.SetColWidth(DirectDepositColumns.BillCreateTime, 18);
            sheet.SetColWidth(DirectDepositColumns.Type, 12);
            sheet.SetColWidth(DirectDepositColumns.DealId, 10);
            sheet.SetColWidth(DirectDepositColumns.DealName, 40);
            sheet.SetColWidth(DirectDepositColumns.StoreName, 18);
            sheet.SetColWidth(DirectDepositColumns.IntervalStart, 18);
            sheet.SetColWidth(DirectDepositColumns.IntervalEnd, 18);
            sheet.SetColWidth(DirectDepositColumns.BalanceSheetTotal, 12);
            sheet.SetColWidth(DirectDepositColumns.SignCompanyId, 10);
            sheet.SetColWidth(DirectDepositColumns.SignCompanyName, 20);
            sheet.SetColWidth(DirectDepositColumns.BillNumber, 14);
            sheet.SetColWidth(DirectDepositColumns.BillDate, 14);
            sheet.SetColWidth(DirectDepositColumns.BillSubtotal, 10);
            sheet.SetColWidth(DirectDepositColumns.BillTax, 8);
            sheet.SetColWidth(DirectDepositColumns.BillTotal, 10);
            sheet.SetColWidth(DirectDepositColumns.BillMemo, 30);
            sheet.SetColWidth(DirectDepositColumns.ReceiverAccountName, 30);
            sheet.SetColWidth(DirectDepositColumns.ReceiverCompanyId, 12);
            sheet.SetColWidth(DirectDepositColumns.ReceiverAccountNo, 16);
            sheet.SetColWidth(DirectDepositColumns.ReceiverBankNo, 10);
            sheet.SetColWidth(DirectDepositColumns.ReceiverBranchNo, 10);
            sheet.SetColWidth(DirectDepositColumns.BillCreator, 20);
            sheet.SetColWidth(DirectDepositColumns.SalesName, 10);
        }

        private IEnumerable<DirectDepositFundTransferInfo> CovertToDirectDepositFundTransferInfo(IEnumerable<ViewBalanceSheetBillFundTransfer> dbInfos)
        {
            List<DirectDepositFundTransferInfo> result = new List<DirectDepositFundTransferInfo>();

            List<int> bsIds = new List<int>();

            foreach (var dbinfo in dbInfos)
            {
                if (bsIds.Contains(dbinfo.BalanceSheetId))
                {
                    continue;       //multiple bills within one balance sheet
                }
                else
                {
                    bsIds.Add(dbinfo.BalanceSheetId);
                }

                int bsAmount = GetAccountsPayable(dbinfo);

                DirectDepositFundTransferInfo info = new DirectDepositFundTransferInfo
                    {
                        BalanceSheetId = dbinfo.BalanceSheetId,
                        DealId = dbinfo.DealId,
                        StoreName = dbinfo.StoreName,
                        BalanceSheetIntervalStart = dbinfo.IntervalStart.Value,
                        BalanceSheetIntervalEnd = dbinfo.IntervalEnd.Value,
                        AccountsPayable = bsAmount,
                        AccountsPaid = dbinfo.TransferAmount,
                        TransferTime = dbinfo.TransferTime,
                        Memo = dbinfo.FundMemo,
                        IsMemoEditable = dbinfo.PayReportId.HasValue && !int.Equals(0, dbinfo.PayReportId.Value),
                        ReceiverTitle = dbinfo.ReceiverAccountTitle ?? dbinfo.DealAccountTitle,
                        ReceiverCompanyId = dbinfo.ReceiverCompanyId ?? dbinfo.DealCompanyId,
                        ReceiverBankNo = dbinfo.ReceiverBankNo ?? dbinfo.DealBankNo,
                        ReceiverBranchNo = dbinfo.ReceiverBranchNo ?? dbinfo.DealBranchNo,
                        ReceiverAccountNo = dbinfo.ReceiverAccountNo ?? dbinfo.DealAccountNo,
                        PaymentId = dbinfo.PayReportId
                    };

                if (bsAmount == 0)
                {
                    info.ReceiverTitle = string.Empty;
                    info.ReceiverCompanyId = string.Empty;
                    info.ReceiverBankNo = string.Empty;
                    info.ReceiverBranchNo = string.Empty;
                    info.ReceiverAccountNo = string.Empty;
                }

                info.PaymentState = GetPaymentStateDesc(dbinfo);
                info.TypeDesc = GetTypeDesc(dbinfo);

                result.Add(info);
            }

            return result;
        }

        private int GetAccountsPayable(ViewBalanceSheetBillFundTransfer dbinfo)
        {
            int result;

            if (dbinfo.EstAmount.HasValue)
            {
                result = dbinfo.EstAmount.Value;
            }
            else
            {
                var bsModel = new BalanceSheetModel(dbinfo.BalanceSheetId);
                result = (int)bsModel.GetAccountsPayable().TotalAmount;
            }

            return result;
        }

        private string GetTypeDesc(ViewBalanceSheetBillFundTransfer dbinfo)
        {
            if ((BalanceSheetType)dbinfo.BalanceSheetType == BalanceSheetType.ManualTriggered)
            {
                return "人工對帳單";
            }

            if ((BusinessModel)dbinfo.ProductType == BusinessModel.Ppon)
            {
                return "17Life團購";
            }
            else
            {
                return "品生活";
            }
        }

        private string GetPaymentStateDesc(ViewBalanceSheetBillFundTransfer dbinfo)
        {
            if (dbinfo.IsTransferComplete.GetValueOrDefault(false))
            {
                return "付款成功";
            }
            else
            {
                return "尚未付款";
            }
        }

        [HttpPost]
        public ActionResult UpdateDirectDepositMemo(int payId, string memo, string transfertime, string accountpaid)
        {
            FundTransfer ft = ap.FundTransferGet(payId);
            if (ft != null && ft.IsLoaded)
            {
                ft.Memo = memo;
                DateTime _transfertime;
                bool bTransferTime = DateTime.TryParse(transfertime, out _transfertime);
                if (bTransferTime)
                {
                    ft.TransferTime = _transfertime;
                }
                int _accountpaid;
                bool bAccountPaid = int.TryParse(accountpaid, out _accountpaid);
                if (bAccountPaid)
                {
                    ft.TransferAmount = _accountpaid;
                }

                ap.FundTransferSet(ft);
                return Json(new { IsSuccess = true });
            }

            return Json(new { IsSuccess = false });
        }

        #endregion 人工匯款管理
    }

    public class DirectDepositData
    {
        public int BalanceSheetId { get; set; }

        public DateTime? TransferTime { get; set; }

        public int? TransferAmount { get; set; }

        public string TransferMemo { get; set; }

        public string AccountTitle { get; set; }

        public string CompanyId { get; set; }

        public string AccountNo { get; set; }

        public string BankNo { get; set; }

        public string BranchNo { get; set; }

        public BalanceSheetUseType UseType { get; set; }
    }
}
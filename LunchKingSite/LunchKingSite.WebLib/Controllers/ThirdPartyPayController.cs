﻿using System;
using System.Net;
using System.Reflection;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using log4net;
using LunchKingSite.BizLogic.Model;

namespace LunchKingSite.WebLib.Controllers
{
    public class ThirdPartyPayController : ControllerExt
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static readonly ILog logger = LogManager.GetLogger(typeof(ThirdPartyPayController).Name);
		
        public const string _TAISHIN_RETURN_URL = "TaishinReturnUrl";
        public const int _RTN_CODE_OK = 0;



        public ActionResult TaishinBindBegin(string returnUrl)
        {
            int userId = MemberFacade.GetUniqueId(this.User.Identity.Name);
            MemberToken memberToken = mp.MemberTokenGet(userId, ThirdPartyPayment.TaishinPay);
            if (memberToken.IsLoaded)
            {
                logger.Warn("userId: " + userId + " 重覆綁定!" + ThirdPartyPayment.TaishinPay);
                if (returnUrl.IndexOf('?') >= 0)
                {
                    returnUrl += "&";
                }
                else
                {
                    returnUrl += "?";
                }

                return Redirect(returnUrl + "taishinBindStatus=" + Helper.GetEnumDescription(TaishinPayShowResult.NormalAlreadyBind));
            }
            else
            {
                if (User.Identity.IsAuthenticated == false)
                {
                    return Redirect(FormsAuthentication.LoginUrl);
                }
                if (string.IsNullOrEmpty(returnUrl) == false)
                {
                    Session[_TAISHIN_RETURN_URL] = returnUrl;
                }
                else if (Request.UrlReferrer != null)
                {
                    Session[_TAISHIN_RETURN_URL] = Request.UrlReferrer.AbsoluteUri;
                }
                return RedirectToAction("TaishinBindSubmit",
                    new
                    {
                        memberId = userId,
                        returnUrl = returnUrl
                    });
            }
        }

        /// <summary>
        /// 請求綁定
        /// </summary>
        /// <param name="memberId"></param>
        /// <returns></returns>
        public ActionResult TaishinBindSubmit(string memberId)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            
            string userId = memberId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = TaishinPayUtility.GetCurrentTimestamp(DateTime.Now);
            string sucessUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/ThirdPartyPay/TaishinBindAuthSuccess");
            string failureUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/ThirdPartyPay/TaishinBindAuthFailure");

            ViewBag.PostUrl = Helper.CombineUrl(config.TaishinThirdPartyPayUserUrl, "third_party/Bind");
            ViewBag.ClientId = clientId;
            ViewBag.UserId = userId;
            ViewBag.Timestamp = timestamp.ToString();
            ViewBag.SuccessUrl = sucessUrl;
            ViewBag.FailureUrl = failureUrl;

            ViewBag.Signature = TaishinPayUtility.EncytSignature(
                clientId, userId, clientPw, timestamp, sucessUrl, failureUrl);

            return View();
        }

        /// <summary>
        /// 台新平台導回綁定成功
        /// </summary>
        /// <param name="auth_code"></param>
        /// <returns></returns>
        public ActionResult TaishinBindAuthSuccess(string auth_code)
        {
            if (auth_code == null)
            {
                Session["TaishinThirdPartyError"] = "跟台新儲值的串接失敗";
                return Redirect(config.SiteUrl); //TODO 綁定失敗導頁
            }
            
            int userId = MemberFacade.GetUniqueId(this.User.Identity.Name);
            TaishinPayBindResult bindResult = TaishinPayUtility.GetTokenByCode(auth_code, userId);

            if (bindResult == null || bindResult.RtnCode != _RTN_CODE_OK)
            {
                Session["TaishinThirdPartyError"] = "跟台新儲值的串接失敗";
                return Redirect(config.SiteUrl); //TODO 綁定失敗導頁
            }
            else
            {
                MemberToken memberToken = mp.MemberTokenGet(userId, ThirdPartyPayment.TaishinPay);
                if (!memberToken.IsLoaded)
                {
                    memberToken.UserId = userId;
                    memberToken.PaymentOrg = (int)ThirdPartyPayment.TaishinPay;
                    memberToken.CreateTime = DateTime.Now;
                    memberToken.AccessToken = bindResult.AccessToken;
                    mp.MemberTokenSet(memberToken);

                    string errMsg;
                    TaishinMemberInfo taishinMember = TaishinPayUtility.GetTaishinMemberInfo(userId, bindResult.AccessToken, out errMsg);
                    if (taishinMember != null)
                    {
                        memberToken.AccountName = taishinMember.AccountName;
                    }
                    mp.MemberTokenSet(memberToken);
                    mp.AccountAuditSet(User.Identity.Name, Helper.GetClientIP(), AccountAuditAction.TaishinPayBindUser, true, "綁定成功");
                }
            }
            if (Session[_TAISHIN_RETURN_URL] != null)
            {
                string returnUrl = Session[_TAISHIN_RETURN_URL].ToString();

                if (returnUrl.IndexOf('?') >= 0)
                {
                    returnUrl += "&";
                }
                else
                {
                    returnUrl += "?";
                }

                return Redirect(returnUrl + "taishinBindStatus=" + Helper.GetEnumDescription(TaishinPayShowResult.NormalBindSuccess));
            }
            return Redirect(config.SiteUrl);
        }

        /// <summary>
        /// 台新平台導回綁定失敗
        /// /mvc/ThirdPartyPay/TaishinBindAuth?auth=0?rtnCode=-2100&rtnMsg=%E8%B3%87%E6%96%99%E9%A9%97%E8%AD%89%E7%A2%BC%E9%8C%AF%E8%AA%A4
        /// </summary>
        /// <param name="rtnCode"></param>
        /// <param name="rtnMsg"></param>
        /// <returns></returns>
        public ActionResult TaishinBindAuthFailure(int? rtnCode, string rtnMsg)
        {
            string memo = string.Format("{0}: {1}", rtnCode, rtnMsg);

            if (rtnCode == null) 
            {
                memo = "可能是消費者取消綁定";
            }

            mp.AccountAuditSet(User.Identity.Name, Helper.GetClientIP(), AccountAuditAction.TaishinPayBindUser, false, memo);
            if (Session[_TAISHIN_RETURN_URL] != null)
            {
                string returnUrl = Session[_TAISHIN_RETURN_URL].ToString();
                if (string.IsNullOrWhiteSpace(rtnMsg))
                {
                    return Redirect(returnUrl);
                }
                else
                {
                    if (returnUrl.IndexOf('?') >= 0)
                    {
                        returnUrl += "&";
                    }
                    else
                    {
                        returnUrl += "?";
                    }
                    returnUrl += "taishinBindStatus=";
                    
                    if (rtnCode != null && Enum.IsDefined(typeof(TaishinPayReturnCodeAndMsg), rtnCode))
                    {
                        return Redirect(returnUrl + Helper.GetEnumDescription((TaishinPayReturnCodeAndMsg)rtnCode));
                    }

                    return Redirect(returnUrl + Helper.GetEnumDescription(TaishinPayShowResult.NormalBindFaild));
                }
            }

            return Redirect(config.SiteUrl);
        }

        /// <summary>
        /// 以一次性支付Code請求消費者同意
        /// </summary>
        /// <param name="code"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public ActionResult TaishinPaySubmit(string code, string orderId)
        {
            string clientId = config.TaishinThirdPartyPayClientId;
            string clientPw = config.TaishinThirdPartyPayClientPassword;
            int timestamp = TaishinPayUtility.GetCurrentTimestamp(DateTime.Now);
            string signature = TaishinPayUtility.EncytSignature(clientId, orderId, code, clientPw, timestamp);

            ViewBag.PostUrl = config.TaishinThirdPartyPayUserUrl + "/third_party/Pay";
            ViewBag.OrderId = orderId;
            ViewBag.ClientId = clientId;
            ViewBag.Timestamp = timestamp;
            ViewBag.Code = code;
            ViewBag.Signature = signature;

            return View();
        }

        /// <summary>
        /// 台新平台導回扣款成功
        /// </summary>
        /// <param name="order_id"></param>
        /// <param name="code"></param>
        /// <param name="rtnCode"></param>
        /// <param name="rtnMsg"></param>
        /// <returns></returns>
        public ActionResult TaishinPaySuccess(string order_id, string code, int? rtnCode, string rtnMsg)
        {
            var userId= MemberFacade.GetUniqueId(this.User.Identity.Name);

            var transLog = op.ThirdPartyPayTransLogGet(PaymentAPIProvider.TaishinPay, userId, order_id);
            if (transLog.IsLoaded)
            {
                transLog.ReturnCode = rtnCode;
                transLog.PaymentStatus = (int)TaishinPayTransStatus.PaySuccess;
                transLog.ModifyTime = DateTime.Now;
                op.ThirdPartyPayTransLogSet(transLog);

                if (UpdateTempSession(transLog.TicketId, code))
                {
                    return Redirect(Helper.CombineUrl(
                        config.SSLSiteUrl, "ppon/buy.aspx?TransId=" + transLog.TransId + "&TicketId=" + transLog.TicketId));
                }
            }

            logger.Error("台新儲值支付扣款成功，訂單異常中斷! 詳情查閱 ApiLog。");
            SystemFacade.SetApiLog(string.Format("{0}.{1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name),
                    "ThirdPartyPay", new { order_id, code, rtnCode, rtnMsg }, "台新儲值支付扣款成功，訂單異常中斷!", Helper.GetClientIP());
            return Redirect(config.SiteUrl);
        }

        /// <summary>
        /// 台新平台導回支付失敗
        /// </summary>
        /// <param name="code"></param>
        /// <param name="rtnCode"></param>
        /// <param name="rtnMsg"></param>
        /// <param name="orderId"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public ActionResult TaishinPayFailure(string code, int? rtnCode, string rtnMsg, string orderId, string bid)
        {
            var transLog = op.ThirdPartyPayTransLogGet(PaymentAPIProvider.TaishinPay, MemberFacade.GetUniqueId(this.User.Identity.Name), orderId);
            if (transLog.IsLoaded)
            {
                //更新TransLog
                transLog.ReturnCode = rtnCode;
                transLog.PaymentStatus = (int)TaishinPayTransStatus.PayFail;
                transLog.ModifyTime = DateTime.Now;
                op.ThirdPartyPayTransLogSet(transLog);

                //記錄支付code
                if (!string.IsNullOrWhiteSpace(code))
                {
                    UpdateTempSession(transLog.TicketId, code);
                }

                //與台新約定，以GET回傳，認定為消費者取消付款
                string memo = (Request.HttpMethod == WebRequestMethods.Http.Get) ? "消費者取消交易" : string.Empty;

                //記錄ApiLog
                var apiLog = new ThirdPartyPayApiLog()
                {
                    UserId = transLog.UserId,
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = "/mvc/ThirdpartyPay/TaishinPayFailure",
                    ReturnCode = rtnCode,
                    ReturnMsg = rtnMsg,
                    Memo = Dns.GetHostName() + memo,
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);

                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "ppon/buy.aspx?TransId=" + transLog.TransId + "&TicketId=" + transLog.TicketId + "&TaishinPayFailure=true"));
            }
            else
            {
                var apiLog = new ThirdPartyPayApiLog()
                {
                    PaymentOrg = (int)PaymentAPIProvider.TaishinPay,
                    ApiUrl = "/mvc/ThirdpartyPay/TaishinPayFailure",
                    ReturnCode = rtnCode,
                    ReturnMsg = rtnMsg,
                    Memo = Dns.GetHostName() + "TransLog is not found",
                    SourceIp = Helper.GetClientIP()
                };
                op.ThirdPartyPayApiLogSet(apiLog);

                return Redirect(config.SiteUrl);
            }
        }

        private bool UpdateTempSession(string ticketId, string code, bool failure = false)
        {
            PponPaymentDTO paymentDTO = PponBuyFacade.GetPponPaymentDTOFromSession(ticketId);
            if (paymentDTO.DeliveryInfo == null)
            {
                SystemFacade.SetApiLog(string.Format("{0}.{1}", this.GetType().Name, MethodBase.GetCurrentMethod().Name), 
                    "ThirdPartyPay", new { ticketId, code}, "台新儲值支付異常中斷，TempSession為null。", Helper.GetClientIP());
                return false;
            }
            if (paymentDTO.ThirdPartyParameters == null)
            {
                paymentDTO.ThirdPartyParameters = new ThirdPartyParameters();
            }
            paymentDTO.ThirdPartyParameters.PayCode = code;
            paymentDTO.ThirdPartyParameters.PayFailure = failure;
            PponBuyFacade.SetPponPaymentDTOToSession(ticketId, paymentDTO);

            return true;
        }
    }
}

﻿using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System;
using LunchKingSite.Core.Enumeration;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Interface;
using System.Web;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebLib.Component.ReqularsSystem.OAuth;
using LunchKingSite.WebLib.Component;
using System.Drawing;
using log4net;
using LunchKingSite.BizLogic.Models.Regulars;
using LunchKingSite.WebLib.Models.RegularsSystem;
using LunchKingSite.WebLib.ActionFilter;

namespace LunchKingSite.WebLib.Controllers
{
    [RegularsOAuthScope(TokenScope.RefreshToken)]
    [RequireHttps]
    public class RegularsSystemController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IPCPProvider pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
        private static ILog logger = LogManager.GetLogger("RegularsSystemController");
        private const string errorPage = "RegularsErrorPage";

        private string sellerApiToken
        {
            get
            {
                string tempToken = string.Empty;

                if (Session[VbsSession.SellerAppApiToken.ToString()] != null)
                {
                    tempToken = Convert.ToString(Session[VbsSession.SellerAppApiToken.ToString()]);
                }
                
                return tempToken;
            }
            set
            {
                //ViewBag.SellerApiToken = value;
                Session[VbsSession.SellerAppApiToken.ToString()] = value;
            }
        }

        private int sellerUserId
        {
            get
            {
                int tempSellerUserId = Convert.ToInt32(Session[VbsSession.UniqueId.ToString()]);
                if (tempSellerUserId == 0)
                {
                    this.Redirect("/vbs/Login");
                }
                return tempSellerUserId;
            }
        }
        private string sellerAccountId
        {
            get
            {
                string tempSellerAccountId = Convert.ToString(Session[VbsSession.UserId.ToString()]);
                if (string.IsNullOrWhiteSpace(tempSellerAccountId.TrimEnd("@vbs")))
                {
                    this.Redirect("/vbs/Login");
                }
                return tempSellerAccountId;
            }
        }


        #region 熟客卡設定用 (cardGroupId, sellerGuid)
        private string sellerGuidForMembershipCard
        {
            get
            {
                string sellerGuid = Convert.ToString(Session["CardSellerGuid"]);
                if (!string.IsNullOrWhiteSpace(sellerGuid))
                {
                    return sellerGuid;
                }
                return "";
            }
            set
            {
                Session["CardSellerGuid"] = value;
            }
        }
        private int cardGroupId
        {
            get
            {
                int tempGroupId = Convert.ToInt32(Session[VbsSession.MembershipCardGroupId.ToString()]);
                if (tempGroupId == 0)
                {
                    //表示尚未開過熟客卡
                }
                return tempGroupId;
            }
        }
        #endregion

        #region 核銷用 (cardGroupId, sellerGuid)
        private string sellerGuidForVerify
        {
            get
            {
                string sellerGuidForVerify = string.Empty;
                if (Request.Headers.AllKeys.Contains("StoreGuid", StringComparer.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(Request.Headers["StoreGuid"]))
                {
                    sellerGuidForVerify = Convert.ToString(Request.Headers["StoreGuid"]); ;
                }
                else if (!string.IsNullOrWhiteSpace(Request["StoreGuid"]))
                {
                    if (config.SSLSiteUrl != "https://www.17life.com")
                    {
                        //只允許測試環境用queryString方式帶StoreGuid
                        sellerGuidForVerify = Request["StoreGuid"];
                    }
                }

                return sellerGuidForVerify;
            }
        }
        private int cardGroupIdForVerify
        {
            get {
                int cardGroupId = 0;
                Guid guid = new Guid();
                if (Guid.TryParse(sellerGuidForVerify, out guid))
                {
                    MembershipCardGroup cardGroup = PcpFacade.GetMembershipCardGroupBySellerGuid(guid);
                    cardGroupId = cardGroup.Id;
                }
                return cardGroupId;
            }
        }

        private string pcpUserToken
        {
            get
            {
                string pcpUserToken = string.Empty;
                if (Request.Headers.AllKeys.Contains("PcpUserToken", StringComparer.OrdinalIgnoreCase) && !string.IsNullOrWhiteSpace(Request.Headers["PcpUserToken"]))
                {
                    pcpUserToken = Convert.ToString(Request.Headers["PcpUserToken"]); ;
                }
                else if (!string.IsNullOrWhiteSpace(Request["PcpUserToken"]))
                {
                    if (config.SSLSiteUrl != "https://www.17life.com")
                    {
                        //只允許測試環境用queryString方式帶PcpUserToken
                        pcpUserToken = Request["PcpUserToken"];
                    }
                }

                return pcpUserToken;
            }
        }
        #endregion

        #region 錯誤頁
        /// <summary>
        /// 給VbsPermissionAttribute與VbsRoleValidateAttribute驗證失敗的時候重新導頁用
        /// </summary>
        /// <returns></returns>
        public ActionResult RegularsErrorPage()
        {
            if (ViewData != null && ViewData.Count > 0)
            {
                //Attribute的驗證中只能用ViewData，故拿ViewData來設定ViewBag
                object errorMsg = null;
                object scriptFunctionName = null;

                ViewData.TryGetValue("ErrorMsg", out errorMsg);
                ViewBag.ErrorMsg = Convert.ToString(errorMsg);
                ViewData.TryGetValue("ScriptFunctionName", out scriptFunctionName);
                ViewBag.ScriptFunctionName = Convert.ToString(scriptFunctionName);
            }
            return View();
        }
        #endregion

        #region 核銷紀錄
        /// <summary>
        /// 會員核銷紀錄
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <param name="cardGroupId"></param>
        /// <param name="useServiceItem"></param>
        /// <param name="useTimeRange"></param>
        /// <returns></returns>
        public ActionResult MemberVerificationList(ViewVerificationListModel ViewModel)
        {
            if (!ViewModel.pageNumber.HasValue)
                ViewModel.pageNumber = 1;

            var cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(sellerAccountId);
            if (cardGroupList.Any())
            {
                var defaultCardGroupInfo = cardGroupList.FirstOrDefault();
                ViewModel.CardGroupId = ViewModel.CardGroupId == 0 ? defaultCardGroupInfo.CardGroupId : ViewModel.CardGroupId;
                //下拉選單
                ViewModel.CardGroupLevels = cardGroupList.Select(p => new SelectListItem() { Text = p.SellerName, Value = p.CardGroupId.ToString(), Selected = p.CardGroupId == ViewModel.CardGroupId }).ToList();
                ViewModel.StoreLevels = PcpFacade.GetRegularsApplyStoreList(ViewModel.CardGroupId, defaultCardGroupInfo.SellerGuid).Select(p => new SelectListItem() { Text = p.StoreName, Value = p.StoreGuid.ToString(), Selected = (p.StoreGuid == ViewModel.SellerGuid) }).ToList();
            }

            ViewModel.UseServiceItemList = PcpFacade.GetUseServiceItemDropDownList(ViewModel.UseServiceItem, false);
            ViewModel.UseTimeRangeList = PcpFacade.GetUseTimeRangeDropDownList(ViewModel.UseTimeRange);
            
            //熟客核銷資料
            if (ViewModel.IsShowCouponTab != TabShowLogic.ShowFirst)
            {
                ViewModel = PcpFacade.GetVerificationList(ViewModel, ViewModel.SellerGuid, ViewModel.UseServiceItem, ViewModel.UseTimeRange, ViewModel.CardGroupId, ViewModel.pageNumber.Value, ((ViewModel.day.HasValue) ? ViewModel.day.Value : 100));
            }

            //團購資料
            if (ViewModel.IsShowMemberVerifyTab != TabShowLogic.ShowFirst)
            {
                var vms = mp.VbsMembershipGetByUserId(sellerUserId);
                ViewModel.CouponModel = PcpFacade.GetVerificationCouponListByStore(ViewModel.storeId, ViewModel.day, ViewModel.pageNumber, ViewModel.only, ViewModel.mailTo, vms);
            }

            //預設第一次要做判斷
            if (ViewModel.IsShowCouponTab == TabShowLogic.Hide && ViewModel.IsShowMemberVerifyTab == TabShowLogic.Hide)
            {
                #region  Tab顯示邏輯 
                //1.若商家只合作其中一項，就不顯示 tab (團購by線上核銷與核銷查詢   熟客就是身分by 管理者帳號+員工帳號)
                //2.核銷時間最新優先顯示

                var isHasVerifyRoles = (List<VendorRole>)Session[VbsSession.VbsRole.ToString()]; //熟客就是身分by 管理者帳號+員工帳號
                var isCanShowCoupon = mp.VbsMembershipGetByUserId(sellerUserId).ViewVerifyRight; ;//[view_verify_right]=線上核銷與核銷查詢 

                
                if (isHasVerifyRoles.Any() && isCanShowCoupon)//兩者皆可
                {
                    var isHasCoupon = (ViewModel.CouponModel.Coupons != null && ViewModel.CouponModel.Coupons.Any()) ? true : false;

                    var pcpOrder = pcp.ViewPcpOrderInfoCollectionGet(ViewModel.CardGroupId);//用此判斷熟客核銷
                    var isHasVerify = pcpOrder.IsLoaded ? true : false;

                    if (isHasCoupon && isHasVerify)
                    {
                        //判斷誰的核銷時間最新, 則預設顯示哪個tab
                        var verifyFirst = pcpOrder.OrderTime;
                        var couponFirst = ViewModel.CouponModel.Coupons.OrderByDescending(p => p.VerifiedDateTime).FirstOrDefault().VerifiedDateTime;

                        if (couponFirst >= verifyFirst)
                        {
                            ViewModel.IsShowCouponTab = TabShowLogic.ShowFirst;
                            ViewModel.IsShowMemberVerifyTab = TabShowLogic.Show;
                        }
                        else
                        {
                            ViewModel.IsShowCouponTab = TabShowLogic.Show;
                            ViewModel.IsShowMemberVerifyTab = TabShowLogic.ShowFirst;
                        }
                    }
                    else if (isHasCoupon && !isHasVerify)//儘管核銷沒有資料,也要顯示tab
                    {
                        ViewModel.IsShowCouponTab = TabShowLogic.ShowFirst;
                        ViewModel.IsShowMemberVerifyTab = TabShowLogic.Show;
                    }
                    else if (!isHasCoupon && isHasVerify)//儘管團購沒有資料,也要顯示tab
                    {
                        ViewModel.IsShowCouponTab = TabShowLogic.Show;
                        ViewModel.IsShowMemberVerifyTab = TabShowLogic.ShowFirst;
                    }
                    else if (!isHasCoupon && !isHasVerify)
                    {
                        ViewModel.IsShowCouponTab = TabShowLogic.Show;
                        ViewModel.IsShowMemberVerifyTab = TabShowLogic.Show;
                    }
                }
                else if (isCanShowCoupon && !isHasVerifyRoles.Any())//僅團購
                {
                    ViewModel.IsShowCouponTab = TabShowLogic.Show;
                    ViewModel.IsShowMemberVerifyTab = TabShowLogic.Hide;
                }
                else if (!isCanShowCoupon && isHasVerifyRoles.Any())
                {
                    ViewModel.IsShowCouponTab = TabShowLogic.Hide;
                    ViewModel.IsShowMemberVerifyTab = TabShowLogic.Show;
                }

                #endregion
            }

            if (ViewModel.CouponModel != null)
            {
                ViewModel.CouponModel.VerifyTime = new Dictionary<string, string>
                {
                    {"1","24小時以內" },
                    {"3","72小時以內" },
                    {"7","一星期以內" },
                    {"30","一個月以內" },
                    {"90","三個月以內" },
                    {"0","所有可兌換商品" },
                };
            }


            return View(ViewModel);
        }

        [HttpPost]
        public JsonResult GetMemberVerificationList(ViewVerificationListModel ViewModel)
        {
            var cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(sellerAccountId);
            if (cardGroupList.Any())
            {
                var defaultCardGroupInfo = cardGroupList.Where(p=>p.CardGroupId == ViewModel.CardGroupId).FirstOrDefault();
                ViewModel.StoreLevels = PcpFacade.GetRegularsApplyStoreList(ViewModel.CardGroupId, defaultCardGroupInfo.SellerGuid).Select(p => new SelectListItem() { Text = p.StoreName, Value = p.StoreGuid.ToString(), Selected = (p.StoreGuid == ViewModel.SellerGuid) }).ToList();
                ViewModel.StoreLevels.Insert(0, new SelectListItem() { Text = "全部分店", Value = "", Selected = true });
            }
            ViewModel = PcpFacade.GetVerificationList(ViewModel, ViewModel.SellerGuid, ViewModel.UseServiceItem, ViewModel.UseTimeRange, ViewModel.CardGroupId, ViewModel.pageNumber.Value, ViewModel.day.Value);
            return Json(new { ViewList = ViewModel.VerifyModel.ViewList, StoreLevels = ViewModel.StoreLevels, TotalCount = ViewModel.TotalCount, StartDate = ViewModel.StartDate.ToString("yyyy/MM/dd"), EndDate = ViewModel.EndDate.ToString("yyyy/MM/dd") });
        }

        #endregion

        #region 會員資訊

        /// <summary>
        /// 會員資訊首頁
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoView)]
        public ActionResult Member()
        {
            List<ViewMembershipCardGroup> cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(sellerAccountId);

            if (cardGroupList.Count() > 0)
            {
                SetSellerGuidAndCardGroupId(cardGroupList);
                ViewBag.SellerGuidList = GetSellerGuidSelectList(cardGroupList, sellerGuidForMembershipCard);
                return View();
            }
            else
            {
                //沒有一家店有開啟熟客卡功能，提示+導頁
                ViewBag.ErrorMsg = "您的帳號無任何熟客系統的瀏覽或編輯權限";
                ViewBag.ScriptFunctionName = "returnHome";
                return View(errorPage);
            }
        }

        /// <summary>
        /// 取得會員資料
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoView)]
        [HttpPost]
        public JsonResult GetMemberList(MembersInputModel model)
        {
            var result = new ApiResult();

            if (model == null || model.IsValid == false)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "參數錯誤";
                return Json(result);
            }

            var members = PcpFacade.GetViewVbsSellerMemberConsumption(cardGroupId, model.Criteria, model.StartIndex, model.Size, model.OrderByField);
            var memberList = new List<SellerMemberModel>();
            if (members.Any())
            {
                foreach (var m in members)
                {
                    if (string.IsNullOrEmpty(m.SellerMemberFirstName) && m.UserId != 0)
                    {
                        m.SellerMemberFirstName = "未填寫";
                    }
                    SellerMemberModel sellerM = new SellerMemberModel(m);
                    sellerM.SellerMemberName = PcpFacade.GetRegularMemberName(m.SellerMemberFirstName, m.SellerMemberLastName, VendorRole.PcpAdmin);
                    memberList.Add(sellerM);
                }

                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.DataNotFound;
            }

            result.Data = new { Members = memberList, TotalCount = sp.ViewVbsSellerMemberConsumptionCollectionGetCount(cardGroupId, model.Criteria) };
            return Json(result);
        }

        /// <summary>
        /// 會員詳細資料
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="SellerMemberId"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoView)]
        public ActionResult MemberDetail(int source, int? userId = 0)
        {
            BizLogic.Models.Regulars.SellerMemberDetailModel viewModel = new BizLogic.Models.Regulars.SellerMemberDetailModel();
            switch (source)
            {
                case (int)MemberDetailReturnSource.VerifyPortal:
                    #region 從核銷進入 (掃完QRCode的頁面)

                    #region 驗證消費者Token (靠token取得userId)
                    AjaxResult result = new AjaxResult();
                    int tempUserId = GetUserIdByValidatePcpUserToken(out result, true);

                    if (!result.Success)
                    {
                        ViewBag.ErrorMsg = result.Message;
                        ViewBag.ScriptFunctionName = "returnScanCode";
                        return View(errorPage);
                    }
                    #endregion

                    viewModel.CardGroupId = cardGroupIdForVerify;
                    viewModel.UserId = tempUserId;
                    ViewBag.Source = (int)MemberDetailReturnSource.VerifyPortal;
                    #endregion
                    break;
                default:
                    #region 從會員明細進入
                    viewModel.CardGroupId = cardGroupId;
                    viewModel.UserId = userId.GetValueOrDefault();
                    ViewBag.Source = (int)MemberDetailReturnSource.Member;
                    #endregion
                    break;
            }

            var member = sp.ViewVbsSellerMemberConsumptionGet(viewModel.UserId, viewModel.CardGroupId);
            if (member.IsLoaded)
            {
                viewModel = new BizLogic.Models.Regulars.SellerMemberDetailModel(member);
                var roles = (List<VendorRole>)Session[VbsSession.VbsRole.ToString()];
                if (!roles.Contains(VendorRole.PcpAdmin))//PcpEmployee(若僅有核銷權限，姓名、手機號碼需做部份遮蔽)
                {
                    viewModel.MemberName = PcpFacade.GetRegularMemberName(viewModel.FirstName, viewModel.LastName, VendorRole.PcpEmployee);
                    viewModel.Mobile = string.IsNullOrEmpty(viewModel.Mobile) ? string.Empty : (viewModel.Mobile.Count() > 4 ? viewModel.Mobile.Remove(4, 3).Insert(4, "xxx") : viewModel.Mobile);
                }
                else
                {
                    viewModel.MemberName = PcpFacade.GetRegularMemberName(viewModel.FirstName, viewModel.LastName, VendorRole.PcpAdmin);
                }
            }

            return View(viewModel);
        }

        /// <summary>
        /// 會員編輯(目前只有備註)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="cardGroupId"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoEdit)]
        public ActionResult MemberEdit(int source, int userId)
        {
            BizLogic.Models.Regulars.SellerMemberDetailModel viewModel = new BizLogic.Models.Regulars.SellerMemberDetailModel();

            switch (source)
            {
                case (int)MemberDetailReturnSource.VerifyPortal:
                    #region 從核銷進入 (掃完QRCode的頁面)

                    #region 驗證消費者Token (靠token取得userId)
                    AjaxResult result = new AjaxResult();
                    int tempUserId = GetUserIdByValidatePcpUserToken(out result, true);

                    if (!result.Success)
                    {
                        ViewBag.ErrorMsg = result.Message;
                        ViewBag.ScriptFunctionName = "returnScanCode";
                        return View(errorPage);
                    }
                    #endregion

                    viewModel.CardGroupId = cardGroupIdForVerify;
                    ViewBag.Source = (int)MemberDetailReturnSource.VerifyPortal;
                    #endregion
                    break;
                default:
                    #region 從會員明細進入
                    viewModel.CardGroupId = cardGroupId;
                    ViewBag.Source = (int)MemberDetailReturnSource.Member;
                    #endregion
                    break;
            }

            var member = sp.ViewVbsSellerMemberGet(userId, viewModel.CardGroupId);
            if (member.IsLoaded)
            {
                viewModel = new BizLogic.Models.Regulars.SellerMemberDetailModel(member);
            }


            var roles = (List<VendorRole>)Session[VbsSession.VbsRole.ToString()];
            if (!roles.Contains(VendorRole.PcpAdmin))//PcpEmployee(若僅有核銷權限，姓名、手機號碼需做部份遮蔽)
            {
                viewModel.MemberName = PcpFacade.GetRegularMemberName(viewModel.FirstName, viewModel.LastName, VendorRole.PcpEmployee);
                viewModel.Mobile = string.IsNullOrEmpty(viewModel.Mobile) ? string.Empty : (viewModel.Mobile.Count() > 4 ? viewModel.Mobile.Remove(4, 3).Insert(4, "xxx") : viewModel.Mobile);
            }
            else
            {
                viewModel.MemberName = PcpFacade.GetRegularMemberName(viewModel.FirstName, viewModel.LastName, VendorRole.PcpAdmin);
            }
            viewModel.UserId = userId;
            return View(viewModel);
        }

        /// <summary>
        /// 儲存會員備註資料
        /// </summary>
        /// <param name="remark"></param>
        /// <param name="userMembershipCardId"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoEdit)]
        [HttpPost]
        public JsonResult SaveRemark(string remark, int userMembershipCardId)
        {
            string errorMsg = string.Empty;
            try
            {
                var userMemberShipCard = pcp.UserMembershipCardGet(userMembershipCardId);
                userMemberShipCard.Remark = remark;
                pcp.UserMembershipCardSet(userMemberShipCard);
            }
            catch (Exception e)
            {
                errorMsg = e.Message;
            }
            return Json(new { errorMsg = errorMsg });
        }

        /// <summary>
        /// 會員消費使用明細
        /// </summary>
        /// <param name="userId">使用者id</param>
        /// <param name="cgid">keep 回上一頁的參數CardGroupId</param>
        /// <param name="useTimeRange">消費使用期間, 預設24H:0</param>
        /// <param name="useServiceItem">消費服務項目, 預設全部:0</param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMemberInfoView)]
        public ActionResult MemberConsumptionDetail(int source, int userId, int useTimeRange = 0, int useServiceItem = 0)
        {
            ConsumptionDetailModel model = new ConsumptionDetailModel();
            switch (source)
            {
                case (int)MemberDetailReturnSource.VerifyPortal:
                    #region 從核銷進入 (掃完QRCode的頁面)

                    #region 驗證消費者Token (靠token取得userId)
                    AjaxResult result = new AjaxResult();
                    int tempUserId = GetUserIdByValidatePcpUserToken(out result, true);

                    if (!result.Success)
                    {
                        ViewBag.ErrorMsg = result.Message;
                        ViewBag.ScriptFunctionName = "returnScanCode";
                        return View(errorPage);
                    }
                    #endregion

                    model.CardGroupId = cardGroupIdForVerify;
                    ViewBag.Source = (int)MemberDetailReturnSource.VerifyPortal;
                    #endregion
                    break;
                default:
                    #region 從會員明細進入
                    model.CardGroupId = cardGroupId;
                    ViewBag.Source = (int)MemberDetailReturnSource.Member;
                    #endregion
                    break;
            }

            model = PcpFacade.GetMemberConsumptionDetailData(userId, model.CardGroupId, useTimeRange, useServiceItem);
            model.UseServiceItemList = PcpFacade.GetUseServiceItemDropDownList(useServiceItem, true);
            model.UseTimeRangeList = PcpFacade.GetUseTimeRangeDropDownList(useTimeRange);
            return View(model);
        }

        /// <summary>
        /// 新增會員
        /// </summary>
        /// <returns></returns>
        public ActionResult MemberAdd()
        {
            ViewBag.Error = "";
            if (string.IsNullOrEmpty(sellerApiToken))
            {
                ViewBag.Error = "Token為空請重新登入";
                return View();
            }
            else
            {
                ViewBag.SiteUrl = config.SSLSiteUrl;
                ViewBag.Token = sellerApiToken;
                return View();
            }

        }

        /// <summary>
        /// 會員資料儲存
        /// </summary>
        /// <param name="strToken"></param>
        /// <param name="SellerMemberId"></param>
        /// <param name="FirstName">姓</param>
        /// <param name="LastName">名</param>
        /// <param name="Mobile">電話</param>
        /// <param name="Gender">性別</param>
        /// <param name="Birthday">生日</param>
        /// <param name="Remarks">備註</param>
        /// <returns></returns>
        public JsonResult MemberSave(string strToken, string SellerMemberId, string FirstName, string LastName, string Mobile, int Gender, string Birthday, string Remarks)
        {
            try
            {
                //呼叫API
                var argument = new
                {
                    SellerMemberId = SellerMemberId,
                    FirstName = FirstName,
                    LastName = LastName,
                    Mobile = Mobile,
                    Gender = Gender,
                    Birthday = Birthday,
                    Remarks = Remarks
                };
                //TmpToken = strToken;
                ApiResult rest = InvokeAPI("/api/pcp/SetMemberDetail", argument);


                if (rest.Code == ApiResultCode.Success)
                    return Json(new { Success = true, Message = rest.Message });
                else
                    return Json(new { Success = false, Message = rest.Message });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = "儲存失敗：" + ex.Message });
            }

        }
        #endregion

        #region 熟客系統設定
        /// <summary>
        /// 熟客系統設定首頁
        /// </summary>
        /// <param name="uniqueid"></param>
        /// <returns></returns>
        public ActionResult CardDashBoard()
        {
            CardDashBoardViewModel model = new CardDashBoardViewModel();
            List<ViewMembershipCardGroup> cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(sellerAccountId);
            
            if (cardGroupList.Count() > 0)
            {
                SetSellerGuidAndCardGroupId(cardGroupList);
                model.SellerGuidList = GetSellerGuidSelectList(cardGroupList, sellerGuidForMembershipCard);
            }
            else
            {
                //沒有一家店有開啟熟客卡功能，提示+導頁
                ViewBag.ErrorMsg = "您的帳號無任何熟客系統的瀏覽或編輯權限";
                ViewBag.ScriptFunctionName = "returnHome";
                return View(errorPage);
            }

            if (cardGroupId > 0)
            {
                model.CardGroup = cardGroupList.Where(x => x.CardGroupId == cardGroupId).FirstOrDefault();

                #region 功能是否開啟、設定相關
                MembershipCardGroupPermissionCollection permissions = pcp.MembershipCardGroupPermissionGetList(cardGroupId);
                model.IsVipOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Vip);
                model.IsPointOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Point);
                model.IsDepositOn = PcpFacade.IsRegularsServiceOn(cardGroupId, permissions, MembershipService.Deposit);
                model.IsVipSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Vip);
                model.IsPointSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Point);
                model.IsDepositSet = PcpFacade.IsRegularsServiceSet(cardGroupId, MembershipService.Deposit);
                #endregion

                #region 適用分店相關
                Guid storeGuid = new Guid();
                if (Guid.TryParse(sellerGuidForMembershipCard, out storeGuid))
                {
                    var applyStoreList = PcpFacade.GetRegularsApplyStoreList(cardGroupId, storeGuid).Where(x => x.IsSelected);
                    model.ApplyStoreCount = applyStoreList.Count();
                    model.IsApplyStoreSet = applyStoreList.Any();
                }
                #endregion

                #region 顯示文字相關 (商家介紹、跑馬燈)
                model.IsSellerIntroSet = pcp.PcpIntroGetByCardGroupId(PcpIntroType.Seller, cardGroupId).Any();
                model.IsMarqueeSet = pcp.PcpMarqueeGetListByCardGroupId(cardGroupId).Any();
                #endregion

                #region 圖片相關
                var imageList = PcpFacade.GetPcpImageListByCardGroupIdAndType(cardGroupId, PcpImageType.None);
                var corporateImages = imageList.Where(x => x.ImageType == (byte)PcpImageType.CorporateImage).ToList();
                model.IsCorporateImageSet = corporateImages.Any();
                model.ImageUrl = (model.IsCorporateImageSet) ? ImageFacade.GetMediaPath(corporateImages.FirstOrDefault().ImageUrl, MediaType.PCPImage).Replace("http://", "https://") : string.Format("{0}/Themes/default/images/17Life/Regulars/images/img_main.jpg", config.SSLSiteUrl);
                model.IsEnvironmentImageSet = imageList.Where(x => x.ImageType == (byte)PcpImageType.EnvironmentAroundStore).Any();
                model.IsServiceImageSet = imageList.Where(x => x.ImageType == (byte)PcpImageType.Service).Any();
                #endregion

                model.IsPointFirstSetting = PcpFacade.CheckIsFirstSetting(cardGroupId);
            }

            return View(model);
        }

        /// <summary>
        /// 熟客系統預覽
        /// </summary>
        /// <returns></returns>
        public ActionResult RegularsPreview()
        {
            RegularsPreviewViewModel model = new RegularsPreviewViewModel();
            var queryModel = new MembershipCardGroupListQueryModel()
            {
                GroupId = cardGroupId
            };
            model.ApiModel = ApiPCPManager.GetMembershipCardDetail(queryModel, false);
            return View(model);
        }

        public ActionResult Help()
        {
            return View();
        }

        /// <summary>
        /// 發行/顯示/隱藏 熟客卡
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin)]
        public JsonResult SwitchCardGroupStatus(bool enable)
        {
            bool success = PcpFacade.SwitchCardGroupStatus(cardGroupId, enable);
            return Json(new AjaxResult()
            {
                Success = success
            });
        }

        /// <summary>
        /// 切換編輯的熟客卡 (一店對一卡)
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult SwitchRegularsCard(string sellerGuid)
        {
            try
            {
                List<ViewMembershipCardGroup> cardGroupList = PcpFacade.GetSellerListWhichEnabledRegularsSystemByAccountId(sellerAccountId);

                Guid guid = new Guid();
                if (Guid.TryParse(sellerGuid, out guid))
                {
                    if (cardGroupList.Select(x => x.SellerGuid).Contains(guid))
                    {
                        sellerGuidForMembershipCard = sellerGuid;
                    }
                }

                return Json(new AjaxResult() {
                    Success = true
                });
            }
            catch (Exception ex)
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Value = ex.ToString()
                });
            }
        }
        #endregion

        #region 熟客系統設定 > 商家介紹
        /// <summary>
        /// 編輯商家介紹
        /// </summary>
        /// <param name="uniqueid"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicView)]
        public ActionResult RegularsSellerIntro()
        {
            List<PcpIntro> intros = pcp.PcpIntroGetByCardGroupId(PcpIntroType.Seller, cardGroupId).OrderBy(x => x.Id).ToList();//.Select(x => x.IntroContent).ToArray();

            return View(intros);
        }

        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicEdit)]
        public JsonResult RegularsSellerIntroSave(RegularsSellerIntroInputModel inputModel)
        {
            inputModel.Intro = HttpUtility.HtmlDecode(inputModel.Intro);

            bool success = false;
            success = PcpFacade.SellerIntroSet(cardGroupId, inputModel);
            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }
        #endregion

        #region 熟客系統設定 > 適用分店
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicView)]
        public ActionResult RegularsApplyStore()
        {
            RegularsApplyStoreViewModel model = new RegularsApplyStoreViewModel();
            
            Guid sellerGuid = new Guid();
            if (Guid.TryParse(sellerGuidForMembershipCard, out sellerGuid))
            {
                model.ApplyStoreList = PcpFacade.GetRegularsApplyStoreList(cardGroupId, sellerGuid);
            }

            return View(model);
        }

        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicEdit)]
        public JsonResult RegularsApplyStoreSave(RegularsApplyStoreInputModel inputModel)
        {
            var success = PcpFacade.RegularsApplyStoreSave(cardGroupId, inputModel);
            return Json(new AjaxResult()
            {
                Success = success,
                Message = success ? "儲存成功" : "儲存失敗"
            });
        }
        #endregion

        #region 熟客系統設定 > 會員優惠
        /// <summary>
        /// 會員優惠
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardView)]
        public ActionResult CardListPortal()
        {
            return View();
        }

        /// <summary>
        /// 編輯當季優惠/編輯下一季優惠
        /// </summary>
        /// <param name="type">type=next代表是下一季</param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardView)]
        public ActionResult CardList(string type)
        {
            ApiResult rest = InvokeAPI("/api/pcp/GetCardsByQuarter", GetQuarter(type));
            if (rest.Code == ApiResultCode.Success)
            {
                MembershipCardQuarterModel ApiData = new Core.JsonSerializer().Deserialize<MembershipCardQuarterModel>(rest.Data.ToString());
                
                ViewBag.Type = string.IsNullOrEmpty(type) ? string.Empty : "next";
                return View(ApiData);
            }
            else
            {
                return new RedirectResult("CardDashBoard");
            }
        }

        /// <summary>
        /// 卡片瀏覽
        /// </summary>
        /// <param name="cardid"></param>
        /// <param name="type">type=next代表是下一季</param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardView)]
        public ActionResult CardPreview(int cardId, string type)
        {
            //呼叫API
            ApiResult rest = InvokeAPI("/api/pcp/GetCardsByQuarter", GetQuarter(type));
            if (rest.Code == ApiResultCode.Success)
            {
                MembershipCardQuarterModel ApiData = new Core.JsonSerializer().Deserialize<MembershipCardQuarterModel>(rest.Data.ToString());
                ViewBag.CardId = cardId;
                ViewBag.Type = string.IsNullOrEmpty(type) ? string.Empty : "next";

                return View(ApiData);
            }
            else
            {
                return new RedirectResult("CardDashBoard");
            }
        }

        /// <summary>
        /// 卡片編輯
        /// </summary>
        /// <param name="uniqueid"></param>
        /// <param name="cardid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardEdit)]
        public ActionResult CardEdit(int cardId, string type)
        {
            //呼叫API
            ApiResult rest = InvokeAPI("/api/pcp/GetCardsByQuarter", GetQuarter(type));
            if (rest.Code == ApiResultCode.Success)
            {
                MembershipCardQuarterModel ApiData = new Core.JsonSerializer().Deserialize<MembershipCardQuarterModel>(rest.Data.ToString());
                
                //取得該卡資料
                MembershipCardModel msc = ApiData.Cards.Find(x => x.CardId == cardId);
                Dictionary<string, string> DicAvailableDateType = Enum.GetValues(typeof(AvailableDateType))
                                                                      .Cast<AvailableDateType>()
                                                                      .Where(x => x > 0)
                                                                      .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));
                
                ViewBag.AvailableDateTypeList = new SelectList(DicAvailableDateType, "key", "value");
                ViewBag.AvailableDateType = ApiData.AvailableDateType;
                ViewBag.Version = ApiData.Version;
                ViewBag.Status = Convert.ToInt32((MembershipCardStatus)msc.Status);

                ViewBag.Type = string.IsNullOrEmpty(type) ? string.Empty : "next";
                ViewBag.CardId = cardId;
                ViewBag.CombineUse = Convert.ToInt32(ApiData.CombineUse);
                if (msc == null)
                {
                    Response.Redirect("CardListPortal");
                    return View();
                }
                else {
                    return View(msc);
                }   
            }
            else
            {
                return new RedirectResult("CardDashBoard");
            }
        }

        /// <summary>
        /// 新增卡片
        /// </summary>
        /// <param name="uniqueid"></param>
        /// <param name="level"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardEdit)]
        public ActionResult CardAdd(int level, string type)
        {
            //呼叫API
            ApiResult rest = InvokeAPI("/api/pcp/GetCardsByQuarter", GetQuarter(type));
            if (rest.Code == ApiResultCode.Success)
            {
                MembershipCardQuarterModel ApiData = new Core.JsonSerializer().Deserialize<MembershipCardQuarterModel>(rest.Data.ToString());
                Dictionary<string, string> DicAvailableDateType = Enum.GetValues(typeof(AvailableDateType))
                                                                      .Cast<AvailableDateType>()
                                                                      .Where(x => x > 0)
                                                                      .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));

                ViewBag.AvailableDateTypeList = new SelectList(DicAvailableDateType, "key", "value");
                ViewBag.AvailableDateType = ApiData.AvailableDateType;

                ViewBag.Type = string.IsNullOrEmpty(type) ? string.Empty : "next";
                ViewBag.Level = level;
                ViewBag.Version = ApiData.Version;
                ViewBag.CombineUse = Convert.ToInt32(ApiData.CombineUse);

                return View(ApiData);
            }
            else
            {
                return new RedirectResult("CardDashBoard");
            }
        }
        
        #region 會員優惠CRUD
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpMembershipCardEdit)]
        public ActionResult SaveCard(SaveCardInputModel inputModel)
        {
            var success = false;
            string errorMsg = string.Empty;

            if (cardGroupId != 0 && inputModel.Card != null)
            {
                success = PcpFacade.SaveCard(sellerUserId, cardGroupId, inputModel, out errorMsg);
            }

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗",
                    Value = errorMsg
                });
            }
        }
        #endregion
        
        #endregion

        #region 熟客系統設定 > 點數功能
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public ActionResult PointFirstSetting()
        {
            return View();
        }
        [VbsPermission(VbsMembershipPermissionScope.PcpPointView)]
        public ActionResult PointSetting()
        {
            PointSettingViewModel model = new PointSettingViewModel();
            var collectRule = pcp.PcpPointCollectRuleGetListByCardGroupId(cardGroupId).FirstOrDefault();
            model.CollectRule = collectRule ?? new PcpPointCollectRule();
            model.ExchangeRules = pcp.PcpPointExchangeRuleGetListByCardGroupId(cardGroupId).ToList();
            model.Remarks = pcp.PcpPointRemarkGetListByCardGroupId(cardGroupId).ToList();
            return View(model);
        }
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public ActionResult PointCollectRuleEdit()
        {
            PointCollectRuleEditViewModel model = new PointCollectRuleEditViewModel();
            var collectRule = pcp.PcpPointCollectRuleGetListByCardGroupId(cardGroupId).FirstOrDefault();
            model.CollectRule = collectRule ?? new PcpPointCollectRule();
            return View(model);
        }
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public ActionResult PointExchangeRuleEdit()
        {
            PointExchangeRuleEditViewModel model = new PointExchangeRuleEditViewModel();
            model.ExchangeRules = pcp.PcpPointExchangeRuleGetListByCardGroupId(cardGroupId).ToList();
            return View(model);
        }
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public ActionResult PointRemarkEdit()
        {
            PointRemarkEditViewModel model = new PointRemarkEditViewModel();
            model.Remarks = pcp.PcpPointRemarkGetListByCardGroupId(cardGroupId).ToList();
            return View(model);
        }
        [VbsPermission(VbsMembershipPermissionScope.PcpPointView)]
        public ActionResult PointRuleChangeLog()
        {
            PointRuleChangeLogViewModel model = new PointRuleChangeLogViewModel();
            var logs = pcp.PcpPointRuleChangeLogGetListByCardGroupId(cardGroupId);
            model.CollectRuleLogs = logs.Where(x => x.RuleType == (int)PcpPointRuleChangeLogRuleType.CollectRule).ToList();
            model.ExchangeRuleLogs = logs.Where(x => x.RuleType == (int)PcpPointRuleChangeLogRuleType.ExchangeRule).ToList();
            return View(model);
        }

        #region 點數CRUD
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        [HttpPost]
        public JsonResult PointFirstSettingSave(PointFirstSettingInputModel inputModel)
        {
            #region 文字輸入框decode
            inputModel.ExchangeRules = inputModel.ExchangeRules ?? new List<PointExchangeRuleInputModel>();
            foreach (var rule in inputModel.ExchangeRules)
            {
                rule.ItemName = HttpUtility.HtmlDecode(rule.ItemName);
            }
            inputModel.Remarks = inputModel.Remarks ?? new List<PointRemarkInputModel>();
            foreach (var remark in inputModel.Remarks)
            {
                remark.RemarkText = HttpUtility.HtmlDecode(remark.RemarkText);
            }
            #endregion

            var success = PcpFacade.PointFirstSettingSave(sellerUserId, cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }

        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public JsonResult PointCollectRuleSave(int? price)
        {
            var success = PcpFacade.PcpPointCollectRuleSave(sellerUserId, cardGroupId, price);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }

        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public JsonResult PointExchangeRuleSave(PointExchangeRuleParentInputModel inputModel)
        {
            inputModel.ExchangeRules = inputModel.ExchangeRules ?? new List<PointExchangeRuleInputModel>();
            foreach (var rule in inputModel.ExchangeRules)
            {
                rule.ItemName = HttpUtility.HtmlDecode(rule.ItemName);
            }

            var success = PcpFacade.PcpPointExchangeRuleSave(sellerUserId, cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }

        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpPointEdit)]
        public JsonResult PointRemarkSave(PointRemarkParentInputModel inputModel)
        {
            inputModel.RemarkList = inputModel.RemarkList ?? new List<PointRemarkInputModel>();
            foreach (var remark in inputModel.RemarkList)
            {
                remark.RemarkText = HttpUtility.HtmlDecode(remark.RemarkText);
            }

            var success = PcpFacade.PcpPointRemarkSave(sellerUserId, cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }

        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin)]
        public JsonResult PointCollectRuleEnable(int ruleId, bool enable)
        {
            bool success = PcpFacade.PointCollectRuleEnable(sellerUserId, cardGroupId, ruleId, enable);
            string message = string.Empty;
            if (enable)
            {
                message = (success == true) ? "啟用發送點數成功" : "啟用發送點數失敗";
            }
            else
            {
                message = (success == true) ? "暫停發送點數成功" : "暫停發送點數失敗";
            }

            return Json(new AjaxResult()
            {
                Success = success,
                Message = message
            });
        }
        #endregion

        #endregion

        #region 熟客系統設定 > 寄杯功能
        /// <summary>
        /// 寄杯活動列表
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositView)]
        public ActionResult DepositMenuList()
        {
            DepositMenuListViewModel model = new DepositMenuListViewModel();
            model.ItemList = PcpFacade.GetViewPcpSellerMenuItemCollectionByCardGroupId(cardGroupId).ToList();
            model.MenuIdToAmountsDic = PcpFacade.GetViewPcpUserDepositTotalAndRemain(cardGroupId).ToList().GroupBy(x => x.MenuId).ToDictionary(x => Convert.ToInt32(x.Key), x => x.FirstOrDefault());
            return View(model);
        }

        /// <summary>
        /// 新增寄杯活動
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public ActionResult DepositMenuAdd()
        {
            DepositMenuAddViewModel model = new DepositMenuAddViewModel();
            model.TemplateList = PcpFacade.PcpDepositTemplateGetListByCardGroupId(cardGroupId);
            return View(model);
        }
        /// <summary>
        /// 編輯寄杯活動
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public ActionResult DepositMenuEdit(int menuId)
        {
            DepositMenuEditViewModel model = new DepositMenuEditViewModel();
            model.TemplateList = PcpFacade.PcpDepositTemplateGetListByCardGroupId(cardGroupId);
            model.ItemList = new List<ViewPcpSellerMenuItem>();
            if (menuId != 0)
            {
                model.ItemList = PcpFacade.GetViewPcpSellerMenuItemCollectionByCardGroupIdAndMenuId(cardGroupId, menuId).ToList();
            }

            var depositAmounts = PcpFacade.GetViewPcpUserDepositTotalAndRemain(cardGroupId, menuId);
            model.Editable = (depositAmounts.TotalAmount == 0) ? true : false;//已有使用者寄杯就不可編輯
            return View(model);
        }

        /// <summary>
        /// 常用寄杯活動/商品列表
        /// </summary>
        /// <param name="menuAddEditUrl"></param>
        /// <param name="templateType"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositView)]
        public ActionResult DepositTemplateList(string menuAddEditUrl, int templateType)
        {
            DepositTemplateListViewModel model = new DepositTemplateListViewModel();
            model.TemplateType = (PcpDepositTemplateType)templateType;
            model.TemplateList = PcpFacade.PcpDepositTemplateGetListByCardGroupId(cardGroupId).Where(x => x.Type == templateType).ToList();
            ViewBag.MenuAddEditUrl = HttpUtility.UrlDecode(menuAddEditUrl);//前一頁
            return View(model);
        }
        /// <summary>
        /// 編輯常用寄杯活動/商品
        /// </summary>
        /// <param name="menuAddEditUrl"></param>
        /// <param name="templateType"></param>
        /// <param name="templateId"></param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public ActionResult DepositTemplateEdit(string menuAddEditUrl, int templateType, int templateId)
        {
            DepositTemplateEditViewModel model = new DepositTemplateEditViewModel();
            model.DepositTemplate = new PcpDepositTemplate() {
                Type = templateType
            };

            if (templateId != 0)
            {
                model.DepositTemplate = pcp.PcpDepositTemplateGet(cardGroupId, templateId);
            }
            ViewBag.BackUrl = menuAddEditUrl;//前一頁
            //ViewBag.BackUrl = Request.UrlReferrer.PathAndQuery;//※此寫法WebView中UrlReferrer == null會Error
            return View(model);
        }

        #region 寄杯相關CRUD (熟客卡後台)

        /// <summary>
        /// 店家新增寄杯活動
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public JsonResult SellerDepositMenuAdd(SellerDepositAddMenuInputModel inputModel)
        {
            inputModel.SellerUserId = sellerUserId;
            var success = PcpFacade.SellerDepositMenuAdd(cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "新增成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "新增失敗"
                });
            }
        }

        /// <summary>
        /// 店家編輯寄杯活動
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public JsonResult SellerDepositMenuEdit(SellerDepositEditMenuInputModel inputModel)
        {
            var success = PcpFacade.SellerDepositMenuEdit(cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }

        /// <summary>
        /// 店家啟用寄杯活動
        /// </summary>
        /// <param name="menuId"></param>
        /// <param name="enable"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin)]
        public JsonResult SellerDepositMenuEnable(int menuId, int enable)
        {
            bool success = PcpFacade.SellerDepositMenuEnable(cardGroupId, menuId, enable);
            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = (enable == 1) ? " 啟用寄杯活動成功" : "暫停寄杯活動成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = (enable == 1) ? "啟用寄杯活動失敗" : "暫停寄杯活動失敗"
                });
            }
        }

        /// <summary>
        /// 店家刪除寄杯活動
        /// </summary>
        /// <param name="menuId"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public JsonResult SellerDepositMenuDelete(int menuId)
        {
            string message = string.Empty;
            bool success = PcpFacade.SellerDepositMenuDelete(cardGroupId, menuId, out message);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "刪除成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = message,
                    Value = PcpCheckoutResult.Success
                });
            }
        }

        /// <summary>
        /// 店家常用寄杯活動/商品編輯
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public JsonResult DepositTemplateEdit(DepositTemplateEditInputModel inputModel)
        {
            inputModel.Template = HttpUtility.HtmlDecode(inputModel.Template);
            var success = PcpFacade.PcpDepositTemplateAddOrEdit(cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = success,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = success,
                    Message = "儲存失敗"
                });
            }
        }

        /// <summary>
        /// 店家常用寄杯活動/商品刪除
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpDepositEdit)]
        public JsonResult DepositTemplateDelete(int templateId)
        {
            var success = PcpFacade.PcpDepositTemplateDelete(cardGroupId, templateId);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = success,
                    Message = "刪除成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = success,
                    Message = "刪除失敗"
                });
            }
        }

        #endregion

        #endregion

        #region 熟客系統設定 > (編輯形象圖片/編輯環境照片/編輯菜色/服務照片)
        /// <summary>
        /// 圖片上傳功能頁 (1:編輯形象圖片/4:編輯環境照片/3:編輯菜色/服務照片)
        /// </summary>
        /// <param name="imgType">圖片種類(PcpImageType)</param>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicView)]
        public ActionResult RegularsImageUpload(int imgType)
        {
            PcpImageType pcpImgType = (PcpImageType)imgType;
            RegularsImageUploadViewModel model = new RegularsImageUploadViewModel();
            model.ImgType = pcpImgType;
            model.ImageList = new List<string>();

            #region Add Images
            var imageList = PcpFacade.GetPcpImageListByCardGroupIdAndType(cardGroupId, pcpImgType);
            foreach (var image in imageList)
            {
                model.ImageList.Add(ImageFacade.GetMediaPath(image.ImageUrl, MediaType.PCPImage));
            }
            #endregion

            return View(model);
        }
        /// <summary>
        /// 圖片上傳功能
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicEdit)]
        public JsonResult UploadRegularsImage(FormCollection formCollection)
        {
            //※改寫自BackendUtility.UploadPcpImage，因壓縮已移至client端完成，且是透過WebView不是用Native編輯，所以不再經過PCPController這支API處理
            try
            {
                PcpImageType imageType = (PcpImageType)Convert.ToInt32(formCollection["imgType"]);
                Dictionary<string, int> fileNameToSeqDic = new Dictionary<string, int>();//檔名-排序
                DateTime nowTime = DateTime.Now;

                for (int i = 0; i < Request.Files.Count; i++)
                {
                    #region 新上傳的檔案
                    var key = Request.Files.Keys[i];//{file}{order}
                    int seq = Convert.ToInt32(key.Replace("file", "")) + 1;
                    var ticks = Convert.ToString(nowTime.AddSeconds(seq).Ticks);
                    string destPath = Convert.ToString(cardGroupId);
                    string fileName = string.Format("{0}.jpg", ticks);

                    HttpPostedFileBase file = Request.Files[i];
                    Image image = FileToImage(file);
                    ImageUtility.UploadFile(null, UploadFileType.PCPImage, destPath, ticks, false, image);
                    fileNameToSeqDic.Add(fileName, seq);
                    #endregion
                }

                for (int i = 0; i < formCollection.Keys.Count; i++)
                {
                    if (formCollection.Keys[i].StartsWith("file"))
                    {
                        #region 已上傳過的檔案
                        var key = formCollection.Keys[i];//{file}{order}
                        int seq = Convert.ToInt32(key.Replace("file", "")) + 1;
                        string fileName = Path.GetFileName(formCollection[i]);
                        fileNameToSeqDic.Add(fileName, seq);
                        #endregion
                    }
                }

                //更新DB
                List<string> toDeleteFiles = new List<string>();
                bool success = PcpFacade.PcpImageSave(sellerUserId, cardGroupId, imageType, fileNameToSeqDic, out toDeleteFiles);

                //刪除舊的實體檔(圖片)
                if (success)
                {
                    foreach (var fileName in toDeleteFiles)
                    {
                        ImageUtility.DeleteFile(UploadFileType.PCPImage, Convert.ToString(cardGroupId), fileName);
                    }
                    return Json(new AjaxResult()
                    {
                        Success = true,
                        Message = "上傳成功"
                    });
                }
                else
                {
                    return Json(new AjaxResult()
                    {
                        Success = false,
                        Message = "上傳失敗"
                    });
                }
            }
            catch (Exception ex)
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "上傳失敗",
                    Value = ex.ToString()
                });
            }
        }
        private Image FileToImage(HttpPostedFileBase file)
        {
            Image image = null;
            if (file.ContentLength > 0)
            {
                var filename = Path.GetFileName(file.FileName);

                image = Image.FromStream(file.InputStream);

            }
            return image;
        }
        #endregion

        #region 熟客系統設定 > 行銷跑馬燈
        /// <summary>
        /// 跑馬燈設定頁
        /// </summary>
        /// <returns></returns>
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicView)]
        public ActionResult RegularsSellerMarquee()
        {
            List<PcpMarquee> model = pcp.PcpMarqueeGetListByCardGroupId(cardGroupId).ToList();
            return View(model);
        }
        /// <summary>
        /// 編輯跑馬燈儲存
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsPermission(VbsMembershipPermissionScope.PcpBasicEdit)]
        public JsonResult RegularsSellerMarqueeSave(RegularsSellerMarqueeInputModel inputModel)
        {
            inputModel.Marquees = inputModel.Marquees ?? new List<string>();
            inputModel.Marquees = inputModel.Marquees.Select(x => HttpUtility.HtmlDecode(x)).ToList();
            var success = PcpFacade.RegularsSellerMarqueeSave(cardGroupId, inputModel);

            if (success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "儲存成功"
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Success = false,
                    Message = "儲存失敗"
                });
            }
        }
        #endregion

        #region 核銷 (含點數、寄杯的核銷)
        /// <summary>
        /// 熟客核銷功能選擇頁面 (For Web版開發用)
        /// </summary>
        [TestServerOnly]
        public ActionResult RegularsVerifyPortal()
        {
            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            #region 店家下拉選單(手機版不需要)
            var nowTime = DateTime.Now;
            var sellerList = pcp.AllowStoreGetList(sellerUserId);
            List<SelectListItem> selectList = new List<SelectListItem>();
            foreach (Seller seller in sellerList)
            {
                var item = new SelectListItem()
                {
                    Text = seller.SellerName,
                    Value = Convert.ToString(seller.Guid)
                };
                selectList.Add(item);
            }
            var defaultValue = selectList.FirstOrDefault().Value.ToString();
            ViewBag.SellerList = new SelectList(selectList, "Value", "Text", defaultValue);
            #endregion


            return View();
        }

        /// <summary>
        /// 熟客核銷-產生Token用 (For Web版開發用)
        /// </summary>
        [TestServerOnly]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult GetNewPcpUserToken(string emailOrMobile)
        {
            try
            {
                var result = new AjaxResult() {
                    Success = false,
                    Message = "產生PcpUserToken失敗"
                };

                if (!string.IsNullOrWhiteSpace(emailOrMobile) && emailOrMobile.Contains("@"))
                {
                    #region 輸入的是email
                    var member = MemberFacade.GetMember(emailOrMobile);
                    if (member.IsLoaded && member.UniqueId > 0)
                    {
                        PcpUserToken token = PcpFacade.GetPcpUserToken(member.UniqueId);
                        if (token.IsLoaded &&
                            (!string.IsNullOrWhiteSpace(token.AccessToken) || !string.IsNullOrWhiteSpace(token.RefreshToken)))
                        {
                            result.Success = true;
                            result.Message = "產生PcpUserToken成功";
                            result.Value = !string.IsNullOrWhiteSpace(token.RefreshToken) ? token.RefreshToken : token.AccessToken;
                        }
                    }
                    #endregion
                }
                else
                {
                    #region 輸入的是手機
                    var mobileMember = MemberFacade.GetMobileMember(emailOrMobile);
                    if (mobileMember.IsLoaded && mobileMember.UserId > 0)
                    {
                        PcpUserToken token = PcpFacade.GetPcpUserToken(mobileMember.UserId);
                        if (token.IsLoaded &&
                            (!string.IsNullOrWhiteSpace(token.AccessToken) || !string.IsNullOrWhiteSpace(token.RefreshToken)))
                        {
                            result.Success = true;
                            result.Message = "產生PcpUserToken成功";
                            result.Value = !string.IsNullOrWhiteSpace(token.RefreshToken) ? token.RefreshToken : token.AccessToken;
                        }
                    }
                    #endregion
                }

                return Json(result);
            }
            catch (Exception ex)
            {
                return Json(new AjaxResult() {
                    Success = false,
                    Message = "產生PcpUserToken失敗",
                    Value = ex
                });
            }
        }

        private int GetCardGroupIdByVerifySeller()
        {
            int cardGroupId = 0;
            Guid guid = new Guid();
            if (Guid.TryParse(sellerGuidForVerify, out guid))
            {
                MembershipCardGroup cardGroup = PcpFacade.GetMembershipCardGroupBySellerGuid(guid);
                cardGroupId = cardGroup.Id;
            }
            return cardGroupId;
        }

        #region 核銷-寄杯相關頁面

        /// <summary>
        /// 店家寄杯服務-新增寄杯
        /// </summary>
        /// <returns></returns>
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult DepositAdd()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            DepositAddViewModel model = new DepositAddViewModel();
            model.ItemList = PcpFacade.GetViewPcpSellerMenuItemCollectionByCardGroupIdInEffective(cardGroupIdForVerify);

            return View(model);
        }

        /// <summary>
        /// 店家寄杯服務-使用寄杯
        /// </summary>
        /// <returns></returns>
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult DepositUse()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            DepositUseViewModel model = new DepositUseViewModel();
            model.UserDepositList = PcpFacade.GetViewPcpUserDepositListWhichStillHaveRemain(userId, cardGroupIdForVerify);

            return View(model);
        }

        /// <summary>
        /// 店家寄杯服務-更正寄杯
        /// </summary>
        /// <returns></returns>
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult DepositCorrect()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            DepositCorrectViewModel model = new DepositCorrectViewModel();
            model.UserDepositList = PcpFacade.GetViewPcpUserDepositListByUserIdAndCardGroupId(userId, cardGroupIdForVerify).ToList();

            return View(model);
        }

        /// <summary>
        /// 店家寄杯服務-檢視明細
        /// </summary>
        /// <returns></returns>
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult DepositLog()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            DepositLogViewModel model = new DepositLogViewModel();
            model.LogList = PcpFacade.GetViewPcpUserDepositLogCollectionByUserIdAndCardGroupId(userId, cardGroupIdForVerify).AsEnumerable<ViewPcpUserDepositLog>()
                                     .OrderByDescending(x => x.CreateTime)
                                     .ThenByDescending(x => x.DepositType)//新增顯示在寄杯之前 add:0 use:1
                                     .ToList();

            return View(model);
        }

        /// <summary>
        /// 使用者新增寄杯
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult UserDepositAdd(UserDepositAddItemInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion
            
            inputModel.CreateId = sellerUserId;
            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.GroupId = cardGroupIdForVerify;
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.DepositAdd(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "新增寄杯成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "新增寄杯失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }

        /// <summary>
        /// 使用者兌換寄杯
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult UserDepositUse(UserDepositItemParentInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion
            
            inputModel.CreateId = sellerUserId;
            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.GroupId = cardGroupIdForVerify;
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.DepositUse(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "兌換寄杯成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "兌換寄杯失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }

        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult UserDepositCorrect(UserDepositCorrectInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion

            inputModel.CreateId = sellerUserId;
            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.CardGroupId = cardGroupIdForVerify;
            inputModel.Description = HttpUtility.HtmlDecode(inputModel.Description);
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.DepositCorrect(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "更正寄杯成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "更正寄杯失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }
        #endregion

        #region 核銷-點數相關頁面
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult PointAdd()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;

            PcpPointCollectRule model = new PcpPointCollectRule();
            model = pcp.PcpPointCollectRuleGetListByCardGroupId(cardGroupIdForVerify).FirstOrDefault() ?? model;
            return View(model);
        }

        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult PointUse()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;
            PointUseViewModel model = new PointUseViewModel();
            model.UserPoint = PcpFacade.GetUserRemainPoint(cardGroupIdForVerify, userId);
            model.ExchangeRule = pcp.PcpPointExchangeRuleGetListByCardGroupId(cardGroupIdForVerify).ToList() ?? new List<PcpPointExchangeRule>();
            return View(model);
        }

        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public ActionResult PointCorrect()
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);

            if (!result.Success)
            {
                ViewBag.ErrorMsg = result.Message;
                ViewBag.ScriptFunctionName = "returnScanCode";
                return View(errorPage);
            }
            #endregion

            ViewBag.PcpUserToken = pcpUserToken;
            ViewBag.SellerGuid = sellerGuidForVerify;
            PointCorrectViewModel model = new PointCorrectViewModel();
            model.UserPoint = PcpFacade.GetUserRemainPoint(cardGroupIdForVerify, userId);
            return View(model);
        }

        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult UserPointAdd(UserPointAddInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion

            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.CardGroupId = cardGroupIdForVerify;
            inputModel.CreateId = sellerUserId;
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.PointAdd(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "發送點數成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "發送點數失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }

        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult UserPointUse(UserPointUseInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion

            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.CardGroupId = cardGroupIdForVerify;
            inputModel.CreateId = sellerUserId;
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.PointUse(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "兌換點數成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "兌換點數失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }

        [HttpPost]
        [VbsRoleValidate(VendorRole.PcpAdmin, VendorRole.PcpEmployee)]
        public JsonResult UserPointCorrect(UserPointCorrectInputModel inputModel)
        {
            #region 驗證消費者Token
            AjaxResult result = new AjaxResult();
            int userId = GetUserIdByValidatePcpUserToken(out result, true);
            if (!result.Success)
            {
                return Json(result);
            }
            #endregion

            inputModel.SellerGuid = sellerGuidForVerify;
            inputModel.CardGroupId = cardGroupIdForVerify;
            inputModel.CreateId = sellerUserId;
            inputModel.Description = HttpUtility.HtmlDecode(inputModel.Description);
            var pcpApp = ApiPCPManager.CreatePcpService(userId, cardGroupIdForVerify, PcpType.App);
            PcpCheckoutResult check = pcpApp.PointCorrect(inputModel);

            if (check == PcpCheckoutResult.Success)
            {
                return Json(new AjaxResult()
                {
                    Success = true,
                    Message = "更正點數成功",
                    Value = PcpCheckoutResult.Success
                });
            }
            else
            {
                return Json(new AjaxResult()
                {
                    Message = "更正點數失敗",
                    Value = PcpCheckoutResult.PcpOrderTransactionFail
                });
            }
        }
        #endregion

        #endregion

        /// <summary>
        /// 選店的選單 (店Guid決定哪張熟客卡)
        /// </summary>
        private void SetSellerGuidAndCardGroupId(List<ViewMembershipCardGroup> cardGroupList)
        {
            if (string.IsNullOrWhiteSpace(Convert.ToString(sellerGuidForMembershipCard)))
            {
                var cardGroup = cardGroupList.FirstOrDefault();
                sellerGuidForMembershipCard = Convert.ToString(cardGroup.SellerGuid);
                Session[VbsSession.MembershipCardGroupId] = cardGroup.CardGroupId;
            }
            else
            {
                sellerGuidForMembershipCard = Convert.ToString(sellerGuidForMembershipCard);
                Session[VbsSession.MembershipCardGroupId] = cardGroupList.Where(x => x.SellerGuid == Guid.Parse(sellerGuidForMembershipCard))
                                                                         .FirstOrDefault().CardGroupId;
            }
        }

        private List<SelectListItem> GetSellerGuidSelectList(List<ViewMembershipCardGroup> cardGroupList, string defaultValue)
        {
            List<SelectListItem> dropdownList = new List<SelectListItem>();
            foreach (ViewMembershipCardGroup cardGroup in cardGroupList)
            {
                var item = new SelectListItem()
                {
                    Text = cardGroup.SellerName,
                    Value = Convert.ToString(cardGroup.SellerGuid)
                };
                dropdownList.Add(item);
            }

            var selectedListItem = dropdownList.FirstOrDefault(d => d.Value == defaultValue);
            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }

            return dropdownList;
        }

        /// <summary>
        /// 驗證使用者的Token跟Id  //TODO: refactor to Attribute(?)
        /// </summary>
        /// <param name="ajaxResult"></param>
        /// <param name="needCheckAlive">是否要檢查pcp_polling_user_status的alive欄位</param>
        /// <returns>userId</returns>
        private int GetUserIdByValidatePcpUserToken(out AjaxResult ajaxResult, bool needCheckAlive)
        {
            AjaxResult result = new AjaxResult()
            {
                Success = true
            };
            int userId = 0;

            //根據pollUserToken查使用者Id
            var ppus = PcpFacade.GetPollUserStatusByToken(pcpUserToken, needCheckAlive);

            #region 身分、資料驗證 TODO: refactor
            if (!ppus.IsLoaded)
            {
                result.Success = false;
                result.Message = "QRCode已失效，請重新掃碼。";
                result.Value = ApiResultCode.OAuthTokerNotFound;
            }
            else if (ppus.ExpiredTime < DateTime.Now)
            {
                result.Success = false;
                result.Message = "QRCode已過期，請重新掃碼。";
                result.Value = ApiResultCode.OAuthTokenExpired;
            }
            else
            {
                userId = ppus.UserId;
            }
            #endregion

            ajaxResult = result;

            return userId;
        }
        
        /// <summary>
        /// 呼叫API
        /// </summary>
        /// <param name="APIUrl"></param>
        /// <param name="argument"></param>
        /// <returns></returns>
        [HttpPost]
        private ApiResult InvokeAPI(string APIUrl, object argument)
        {
            try
            {
                if (string.IsNullOrEmpty(sellerApiToken))
                {
                    ApiResult rest = new Core.JsonSerializer().Deserialize<ApiResult>("{'Code':" + (int)ApiResultCode.OAuthTokerNotFound + ", 'Data':{},'Message':'Token為空請重新登入'}");
                    return rest;
                }
                else
                {
                    //抓取API資料
                    string url = config.SSLSiteUrl + APIUrl + "?AccessToken=" + sellerApiToken;
                    string result = string.Empty;

                    var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                    httpWebRequest.ContentType = "application/json";
                    httpWebRequest.Method = "POST";

                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        string json = JsonConvert.SerializeObject(argument);

                        streamWriter.Write(json);
                        streamWriter.Flush();
                        streamWriter.Close();
                    }

                    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                    var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                    using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                    {
                        result = streamReader.ReadToEnd();
                    }

                    //反序列化
                    ApiResult rest = new Core.JsonSerializer().Deserialize<ApiResult>(result);
                    return rest;
                }
            }
            catch (Exception ex)
            {
                ApiResult rest = new Core.JsonSerializer().Deserialize<ApiResult>("{'Code':" + (int)ApiResultCode.OAuthTokerNoAuth + ", 'Data':{},'Message':'API呼叫錯誤：" + ex.Message + "'}");
                return rest;
            }



        }

        /// <summary>
        /// 取得當季或下一季
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public object GetQuarter(string type)
        {
            var argument = new
            {
                Year = 0,
                Quarter = 0,
                SellerGuid = sellerGuidForMembershipCard
            };
            //呼叫API
            if (string.IsNullOrEmpty(type))
            {
                argument = new
                {
                    Year = DateTime.Now.Year,
                    Quarter = Helper.GetQuarterByDate(DateTime.Now),
                    SellerGuid = sellerGuidForMembershipCard
                };
            }
            else if (type == "next")
            {

                if (Helper.GetQuarterByDate(DateTime.Now) == 4)
                {
                    argument = new
                    {
                        Year = DateTime.Now.Year + 1,
                        Quarter = 1,
                        SellerGuid = sellerGuidForMembershipCard
                    };
                }
                else
                {
                    argument = new
                    {
                        Year = DateTime.Now.Year,
                        Quarter = Helper.GetQuarterByDate(DateTime.Now) + 1,
                        SellerGuid = sellerGuidForMembershipCard
                    };
                }

            }

            return argument;
        }

    }
    
}

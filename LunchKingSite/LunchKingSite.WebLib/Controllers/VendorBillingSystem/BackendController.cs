﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    public class VendorBillingSystemBackendController : BaseController
    {
        #region Property

        private static ISellerProvider sp;
        private static IMemberProvider mp;
        private static IPponProvider pp;
        private static IHiDealProvider hp;
        private static IAccountingProvider ap;
        private static IOAuthProvider op;
        private static IChannelProvider cp;
        private static Core.Interface.IVbsProvider _vbs;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        protected static ILog logger = LogManager.GetLogger(typeof(BaseProposalController));

        static VendorBillingSystemBackendController()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            op = ProviderFactory.Instance().GetProvider<IOAuthProvider>();
            cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            _vbs = ProviderFactory.Instance().GetProvider<Core.Interface.IVbsProvider>();
        }


        private int UserId
        {
            get
            {
                return MemberFacade.GetUniqueId(HttpContext.User.Identity.Name);
            }
        }

        #endregion Property

        #region Action

        #region 系統對帳單

        [HttpGet]
        public ActionResult SearchBalanceSheet()
        {
            var channelData = ChannelFacade.GetChannelList();
            if (!channelData.Any()) channelData.Add("無資料", new KeyValuePair<int, string>((int)OrderClassification.Other, "0"));
            SelectList channelList = new SelectList(channelData, "Value", "Key");

            ViewBag.ChannelClientProperty = channelList;
            SearchBalanceSheetModel model = new SearchBalanceSheetModel();
            return View(model);
        }

        [HttpPost]
        public ActionResult SearchBalanceSheet(SearchBalanceSheetModel model)
        {
            var channelData = ChannelFacade.GetChannelList();
            if (!channelData.Any()) channelData.Add("無資料", new KeyValuePair<int, string>((int)OrderClassification.Other, "0"));
            SelectList channelList = new SelectList(channelData, "Value", "Key");
            ViewBag.ChannelClientProperty = channelList;
            

            if (model.SearchCondition == BalanceSheetSearchCondition.Unknown)
            {
                return View(model);
            }

            IEnumerable<ViewBalancingDeal> intermidiateResult;

            switch (model.SearchCondition)
            {
                case BalanceSheetSearchCondition.PponUniqueId:
                case BalanceSheetSearchCondition.PiinProductId:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    int dealId;
                    if (int.TryParse(model.SearchExpression, out dealId))
                    {
                        intermidiateResult = ap.ViewBalancingDealGetByDealId(dealId, false, false);
                    }
                    break;
                case BalanceSheetSearchCondition.Bid:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    Guid bid;
                    if (Guid.TryParse(model.SearchExpression, out bid))
                    {
                        DealProperty dp = pp.DealPropertyGet(bid);
                        if (dp != null && dp.IsLoaded)
                        {
                            intermidiateResult = ap.ViewBalancingDealGetByDealId(dp.UniqueId, false, false);
                        }
                    }
                    break;
                case BalanceSheetSearchCondition.PiinDealId:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    int did;
                    if (int.TryParse(model.SearchExpression, out did))
                    {
                        HiDealProductCollection products = hp.HiDealProductCollectionGet(did);
                        foreach (HiDealProduct product in products)
                        {
                            var dbinfo = ap.ViewBalancingDealGetByDealId(product.Id, false, false).FirstOrDefault();
                            if (dbinfo != null && dbinfo.IsLoaded)
                            {
                                ((List<ViewBalancingDeal>)intermidiateResult).Add(dbinfo);
                            }
                        }
                    }
                    break;
                case BalanceSheetSearchCondition.DealName:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListLikeDealName(model.SearchExpression, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListLikeDealName(model.SearchExpression, false, true, BusinessModel.PiinLife);
                    }
                    break;
                case BalanceSheetSearchCondition.SellerName:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListLikeSellerName(model.SearchExpression, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListLikeSellerName(model.SearchExpression, false, true, BusinessModel.PiinLife);
                    }
                    break;
                case BalanceSheetSearchCondition.DealStartTime:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByOrderStartRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByOrderStartRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.PiinLife);
                    }
                    break;
                case BalanceSheetSearchCondition.DealEndTime:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByOrderEndRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByOrderEndRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.PiinLife);
                    }

                    break;
                case BalanceSheetSearchCondition.DealUseStartTime:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByUseStartRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByUseStartRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.PiinLife);
                    }
                    break;
                case BalanceSheetSearchCondition.DealUseEndTime:
                    if (model.BizModel == BusinessModel.Ppon)
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByUseEndRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.Ppon);
                    }
                    else
                    {
                        intermidiateResult = ap.ViewBalancingDealGetListByUseEndRegion(model.SearchStartTime, model.SearchEndTime, false, true, BusinessModel.PiinLife);
                    }
                    break;
                default:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    break;
            }

            List<DealBalanceInfo> searchResults = new List<DealBalanceInfo>();

            // data in intermidiateResult are by deal
            // view needs data by store
            foreach (ViewBalancingDeal deal in intermidiateResult)
            {
                BalanceSheetCollection sheets = ap.BalanceSheetGetListByProductGuid(deal.MerchandiseGuid);
                IEnumerable<BalanceSheet> monthOnlySheets =
                    sheets.Where(sheet =>
                        sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                        sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                        sheet.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet);

                var monthSheetsByStore = monthOnlySheets.GroupBy(sheet => sheet.StoreGuid);
                foreach (IGrouping<Guid?, BalanceSheet> store in monthSheetsByStore)
                {
                    string storeName = string.Empty;
                    DateTime? pponOnlyStoreExpireEndDate = null;
                    if (store.Key != null)
                    {
                        var dbStore = sp.SellerGet(store.Key.Value);
                        storeName = dbStore.SellerName;
                        if (model.BizModel == BusinessModel.Ppon)
                        {
                            PponStore pStore = pp.PponStoreGet(deal.MerchandiseGuid, store.Key.Value);
                            if (pStore != null && pStore.IsLoaded)
                            {
                                pponOnlyStoreExpireEndDate = pStore.ChangedExpireDate;
                            }
                        }
                    }

                    int monthBalanceSheetCount = store.Count();
                    DealBalanceInfo info = new DealBalanceInfo
                                               {
                                                   SellerName = deal.SellerName,
                                                   SellerGuid = deal.SellerGuid,
                                                   StoreGuid = store.Key,
                                                   StoreName = storeName,
                                                   DealId = deal.DealId,
                                                   DealGuid = deal.MerchandiseGuid,
                                                   DealName = deal.DealName,
                                                   DealUseStartTime = deal.DealStartTime,
                                                   DealUseEndTime = pponOnlyStoreExpireEndDate ?? deal.DealEndTime,
                                                   MonthlyBalanceSheetCount = monthBalanceSheetCount,
                                                   RemittanceType = (RemittanceType)deal.RemittanceType
                                               };
                    searchResults.Add(info);
                }
            }
            model.Searched = true;
            model.SearchResult = searchResults;
            return View(model);
        }

        public ActionResult ManageBalanceSheet(ManageBalanceSheetModel model)
        {
            ViewBalancingDeal dbinfo = ap.ViewBalancingDealGetByDealId(model.DealId, false, false).FirstOrDefault();
            if (dbinfo == null || dbinfo.IsNew)
            {
                return View(model);
            }

            model.Biz = (BusinessModel)dbinfo.BusinessModel;

            List<BalanceSheet> allSheets = ap.BalanceSheetGetListByProductGuid(dbinfo.MerchandiseGuid)
                .Where(x => x.StoreGuid == model.Store).ToList();

            List<BalanceSheet> monthOnlySheets = allSheets
                .Where(sheet => (sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                 sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                                 sheet.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet) &&
                                sheet.StoreGuid == model.Store)
                .OrderBy(x => x.IntervalEnd).ToList();

            List<BalanceSheet> triggeredSheets = allSheets
                .Where(sheet =>
                    sheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet
                    && sheet.StoreGuid == model.Store)
                .OrderBy(sheet => sheet.IntervalEnd).ToList();

            //設定檔次資訊
            model.DealInfo = GetDealInfo(dbinfo, monthOnlySheets, model.Store);

            //檢查是否符合產人工對帳單時間條件
            model.IsTriggerManualable = this.IsTriggerManualable(allSheets);

            //檢查是否有可補產週結月對帳單資料 及是否有補產對帳單權限
            model.IsTriggerable = false;
            if (model.DealInfo.RemittanceType == RemittanceType.AchWeekly || model.DealInfo.RemittanceType == RemittanceType.ManualWeekly)
            {
                DateTime generationTime = DateTime.Now.AddMonths(-1);
                model.IsTriggerable = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.TriggerWeeklyMonthBalanceSheet) &&
                                      ap.GetWeekBalanceSheetHasNoMonthBalanceSheet(dbinfo.MerchandiseGuid, model.Biz, new List<Guid?> { model.Store }, generationTime.Year, generationTime.Month).Any();
            }

            //設定對帳單選擇清單
            var monthOptions = new Dictionary<int, string>();
            foreach (var sheet in monthOnlySheets)
            {
                monthOptions.Add(sheet.Id, sheet.IntervalStart.ToString("yyyy/MM/dd"));
            }
            foreach (var sheet in triggeredSheets)
            {
                monthOptions.Add(sheet.Id, string.Format("至{0}", sheet.IntervalEnd.ToString("yyyy/M/d h:mm tt")));
            }
            model.MonthBalanceSheetOptions = monthOptions;

            //  未特別選擇對帳單時, 初始化選擇項目
            if (int.Equals(0, model.SelectedBalanceSheetId)) //balance sheet id not set
            {
                model.SelectedBalanceSheetId = monthOptions.Any()
                                                   ? monthOptions.Keys.Max()
                                                   : -1;    // -1: 尚未列入對帳單清單    
            }

            if (!monthOnlySheets.Select(x => x.Id).Contains(model.SelectedBalanceSheetId)
                && !triggeredSheets.Select(x => x.Id).Contains(model.SelectedBalanceSheetId)) //亂七八糟的 id
            {
                model.SelectedBalanceSheetId = -1;
            }

            model.SlottingFeeQuantity = GetSlottingFeeQuantity(dbinfo.MerchandiseGuid);
            model.UpdateBalanceSheetAmountEnable = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.UpdateBalanceSheetEstAmount);

            //針對選擇的對帳單, 抓出明細
            if (!int.Equals(-1, model.SelectedBalanceSheetId))
            {
                BalanceSheet selectedSheet;
                if (monthOnlySheets.Select(x => x.Id).Contains(model.SelectedBalanceSheetId))
                {
                    selectedSheet = monthOnlySheets
                        .First(sheet => sheet.Id == model.SelectedBalanceSheetId);
                }
                else
                {
                    selectedSheet = triggeredSheets
                        .First(sheet => sheet.Id == model.SelectedBalanceSheetId);
                }

                model.BalanceDetail = GetBalanceDetail(selectedSheet, dbinfo, model.Store);

                //對帳單 summary
                model.SelectedBalanceSheetFixed = selectedSheet.IsDetailFixed;
                model.IntervalStart = selectedSheet.IntervalStart;
                model.IntervalEnd = selectedSheet.IntervalEnd;
                //金額資訊
                var bsModel = new BalanceSheetModel(selectedSheet.Id);
                TotalAmountDetail amountDetail = bsModel.GetAccountsPayable();
                model.BalanceSheetAmount = amountDetail.TotalAmount;
                model.VerifiedAmount = amountDetail.PayAmount;
                model.UndoAmount = amountDetail.DeductAmount;
                model.VerifiedTotalCount = GetVerifiedTotalCount(model.SelectedBalanceSheetId);
            }
            else  //未列入對帳單的明細
            {
                ViewBalanceSheetUndeductedCollection undeductedList = ap.ViewBalanceSheetUndeductedGetList(model.DealInfo.DealGuid, model.Store);
                model.UndoDetail = GetUndoDetail(undeductedList);
                ViewBalanceSheetNoncorrespondedVerifyCollection verifiedList = ap.ViewBalanceSheetNoncorrespondedVerifyGetList(model.DealInfo.DealGuid, model.Store);
                model.VerifyDetail = GetVerifyDetail(verifiedList);
                //金額資訊
                model.VerifiedAmount = model.Biz == BusinessModel.Ppon
                                           ? verifiedList.Sum(x => x.PponCost).Value
                                           : verifiedList.Sum(x => x.PiinCost).Value;
                model.UndoAmount = model.Biz == BusinessModel.Ppon
                                           ? undeductedList.Sum(x => x.PponCost).Value
                                           : undeductedList.Sum(x => x.PiinCost).Value;
                model.BalanceSheetAmount = model.VerifiedAmount - model.UndoAmount;

                BalanceSheet unfixedSheet =
                    monthOnlySheets.FirstOrDefault(sheet => sheet.IsDetailFixed == false);
                if (unfixedSheet != null)
                {
                    model.UnfixedBalanceSheetAvailable = true;
                    model.UnfixedBalanceSheetId = unfixedSheet.Id;
                }
            }

            return View(model);
        }
        
        public JsonResult GenerateManualBalanceSheet(BusinessModel bizModel, int dealId, Guid? storeGuid = null)
        {
            BalanceSheetSpec spec;

            switch (bizModel)
            {
                case BusinessModel.Ppon:
                    var dp = pp.DealPropertyGet(dealId);
                    if (dp != null && dp.IsLoaded)
                    {
                        spec = new BalanceSheetSpec
                        {
                            SheetCategory = BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet,
                            BizModel = bizModel,
                            BusinessHourGuid = dp.BusinessHourGuid,
                            ManualTriggeredStoreGuid = storeGuid
                        };
                    }
                    else
                    {
                        return Json(new { Success = false, FailReason = "檔次資料有問題." });
                    }
                    break;
                case BusinessModel.PiinLife:
                    spec = new BalanceSheetSpec
                    {
                        SheetCategory = BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet,
                        BizModel = bizModel,
                        HiDealProductId = dealId,
                        ManualTriggeredStoreGuid = storeGuid
                    };
                    break;
                default:
                    return Json(new { Success = false, FailReason = "請洽技術部! (unknown bizmodel)" });
            }

            ReadOnlyCollection<BalanceSheetGenerationResult> genResults = BalanceSheetService.Create(spec);
            if (genResults != null && genResults.Count == 1) //人工對帳單要指定分店, 所以只會有一個結果.
            {
                BalanceSheetGenerationResult result = genResults.First();
                if (result.IsSuccess)
                {
                    return Json(new { Success = true, FailReason = "人工對帳單產出成功。" });
                }
                switch (result.Status)
                {
                    case BalanceSheetGenerationResultStatus.Duplication:
                        return Json(new { Success = false, FailReason = "今天已產過人工對帳單了，明天請早。" });
                    case BalanceSheetGenerationResultStatus.ItemsCheckFailed:
                        return Json(new { Success = false, FailReason = "沒有資料可產人工對帳單。" });
                    case BalanceSheetGenerationResultStatus.Success:
                        return Json(new { Success = true });
                    default:
                        return Json(new { Success = false, FailReason = string.Format("請洽技術部! ({0})", result.FailureMessage) });
                }

            }

            return Json(new { Success = false, FailReason = "請洽技術部! (unexpected program flow.)" });
        }

        public JsonResult GenerateTriggerWeeklyPayMonthBalanceSheet(BusinessModel bizModel, int dealId, Guid? storeGuid = null)
        {
            BalanceSheetSpec spec;

            switch (bizModel)
            {
                case BusinessModel.Ppon:
                    var dp = pp.DealPropertyGet(dealId);
                    if (dp != null && dp.IsLoaded)
                    {
                        spec = new BalanceSheetSpec
                        {
                            SheetCategory = BalanceSheetGenerationFrequency.MonthBalanceSheet,
                            BizModel = bizModel,
                            BusinessHourGuid = dp.BusinessHourGuid,
                            TriggerUser = User.Identity.Name,
                            GenOutOfVerificationTimeRange = true
                        };
                    }
                    else
                    {
                        return Json(new { Success = false, FailReason = "檔次資料有問題." });
                    }
                    break;
                case BusinessModel.PiinLife:
                    spec = new BalanceSheetSpec
                    {
                        SheetCategory = BalanceSheetGenerationFrequency.MonthBalanceSheet,
                        BizModel = bizModel,
                        HiDealProductId = dealId,
                        TriggerUser = User.Identity.Name,
                        GenOutOfVerificationTimeRange = true
                    };
                    break;
                default:
                    return Json(new { Success = false, FailReason = "請洽技術部! (unknown bizmodel)" });
            }

            ReadOnlyCollection<BalanceSheetGenerationResult> genResults = BalanceSheetService.Create(spec);
            int successCount = 0;
            int faiilResultCount = 0;
            string failReason = string.Empty;
            bool isSuccess = false;

            if (genResults != null && genResults.Count > 0)
            {
                Dictionary<Guid?, string> storeName = pp.ViewPponDealStoreGetToShopDealList(string.Join(",", genResults.Select(x => x.StoreGuid).ToList()),
                                                                                            ViewPponDealStore.Columns.ProductGuid + " = " + spec.BusinessHourGuid)
                                                        .ToDictionary(x => x.StoreGuid, x => x.StoreName);
                foreach (var result in genResults)
                {
                    if (storeName.ContainsKey(result.StoreGuid))
                    {
                        failReason += string.Format("分店:{0} ", storeName[result.StoreGuid]);
                    }

                    switch (result.Status)
                    {
                        case BalanceSheetGenerationResultStatus.Duplication:
                            faiilResultCount++;
                            failReason += "對帳單已存在。\n";
                            break;
                        case BalanceSheetGenerationResultStatus.ItemsCheckFailed:
                        case BalanceSheetGenerationResultStatus.StopPayment:
                            faiilResultCount++;
                            failReason += "沒有資料可補產。\n";
                            break;
                        case BalanceSheetGenerationResultStatus.Success:
                            isSuccess = true;
                            successCount++;
                            failReason += "補產月對帳單成功。\n";
                            break;
                        default:
                            faiilResultCount++;
                            failReason += string.Format("請洽技術部! ({0})\n", result.FailureMessage);
                            break;
                    }
                }
            }

            failReason += string.Format("補產週結月對帳單: 成功 {0} 筆， 失敗 {1} 筆", successCount, faiilResultCount);

            return Json(new { Success = isSuccess, GenerationDate = DateTime.Now.Date, FailReason = failReason });
        }

        public JsonResult AddVerified(int balanceSheetId, Guid trustId, string reason)
        {
            bool result = BalanceSheetService.AddVerifiedDetailToBs(trustId, reason, balanceSheetId, UserId);

            return Json(new { Success = result });
        }

        public JsonResult AddUndo(int balanceSheetId, Guid trustId, string reason)
        {
            bool result = BalanceSheetService.AddDeductDetailToBs(trustId, reason, balanceSheetId, UserId);

            return Json(new { Success = result });
        }

        public JsonResult UpdateBalanceSheetEstAmountBySlottingFeeQuantity(Guid dealGuid)
        {            
            var result = false;
            var message = new StringBuilder();
            var bsList = ap.BalanceSheetGetListByProductGuid(dealGuid).ToList();
            var bsIds = bsList.Select(x => x.Id).ToList();
            //撈取已有請款單據對帳單
            var bsIdsHasBill = ap.BalanceSheetBillRelationshipGetListByBalanceSheetIds(bsIds)
                                .GroupBy(x => x.BalanceSheetId)
                                .ToDictionary(x => x.Key, x => x.Count());
            var slottingFeeQuantity = GetSlottingFeeQuantity(dealGuid);
            //撈取所有對帳明細
            var details = ap.BalanceSheetDetailGetListByBalanceSheetIds(bsIds).ToList();
            var verifiedCount = details.Count(x => x.Status == (int)BalanceSheetDetailStatus.Normal ||
                                                   x.Status == (int)BalanceSheetDetailStatus.NoPay);
            var noPayCount = details.Count(x => x.Status == (int)BalanceSheetDetailStatus.NoPay);
            //異動對帳明細 只限撈取尚未有請款紀錄之對帳單
            bsList = bsList.Where(x => !bsIdsHasBill.ContainsKey(x.Id)).OrderByDescending(x => x.IntervalStart).ToList();
            details = details.Where(x => !bsIdsHasBill.ContainsKey(x.BalanceSheetId)).ToList();
            var checkDetails = details.ToLookup(x => x.BalanceSheetId, x => x);

            //上架費份數與對帳單已標記上架費份數不一致才須檢查是否更新對帳單明細
            if (slottingFeeQuantity > noPayCount)
            {
                if (verifiedCount > noPayCount)
                {
                    if (details.Any(x => x.Status == (int)BalanceSheetDetailStatus.Normal))
                    {
                        foreach (var bs in bsList)
                        {
                            var normalCount = checkDetails.Contains(bs.Id) ? checkDetails[bs.Id].Count(x => x.Status == (int)BalanceSheetDetailStatus.Normal) : 0;
                            var deductionCount = checkDetails.Contains(bs.Id) ? checkDetails[bs.Id].Count(x => x.Status == (int)BalanceSheetDetailStatus.Deduction) : 0;
                            //避免因上架費異動產出負額對帳單(負額一律由下期依系統出帳規則產出)
                            if (normalCount - deductionCount > 0)
                            {
                                var updateDetails = new BalanceSheetDetailCollection();
                                var tempNoPayCount = 0;
                                if (normalCount - deductionCount <= slottingFeeQuantity - noPayCount)
                                {
                                    tempNoPayCount = normalCount - deductionCount;
                                }
                                else
                                {
                                    tempNoPayCount = slottingFeeQuantity - noPayCount;
                                }

                                updateDetails.AddRange(checkDetails[bs.Id].Where(x => x.Status == (int)BalanceSheetDetailStatus.Normal).Take(tempNoPayCount));

                                var updateFail = false;
                                if (updateDetails.Any())
                                { 
                                    #region  更新對帳明細為上架費份數

                                    foreach (var updateDetail in updateDetails)
                                    {
                                        try
                                        { 
                                            UpdateBalanceSheeetDetailStatus(updateDetail, BalanceSheetDetailStatus.NoPay, "上架費份數異動");
                                            noPayCount++;
                                        }
                                        catch
                                        {
                                            updateFail = true;
                                            message.Append(string.Format("Id:{0} 對帳單明細更新為上架費份數更新失敗，請洽技術部。\r\n", bs.Id));
                                        }
                                    }

                                    #endregion 更新對帳明細為上架費份數

                                    #region 更新對帳單金額 新增對帳單異動紀錄

                                    if (!updateFail)
                                    {
                                        //更新對帳單金額
                                        updateFail = UpdateBalanceSheetAmount(bs, ref message);
                                    }

                                    #endregion 更新對帳單金額 新增對帳單異動紀錄
                                }

                                result = !updateFail;
                            }
                            else
                            {
                                continue;
                            }
                        }
                    }
                    else
                    {
                        result = true;
                    }                           
                }
                else
                { 
                    result = true;
                }
            }
            else if (slottingFeeQuantity < noPayCount)
            {
                var recoverCount = noPayCount - slottingFeeQuantity;
                if (details.Any(x => x.Status == (int)BalanceSheetDetailStatus.NoPay))
                {
                    foreach (var bs in bsList)
                    {
                        var noPayDetailsCount = checkDetails.Contains(bs.Id) ? checkDetails[bs.Id].Count(x => x.Status == (int)BalanceSheetDetailStatus.NoPay) : 0;
                        var updateDetails = new BalanceSheetDetailCollection();
                        var tempNormalCount = 0;
                        if (recoverCount > noPayDetailsCount)
                        {
                            tempNormalCount = noPayDetailsCount;
                        }
                        else
                        {
                            tempNormalCount = recoverCount;
                        }

                        updateDetails.AddRange(checkDetails[bs.Id].Where(x => x.Status == (int)BalanceSheetDetailStatus.NoPay).Take(tempNormalCount));
                        
                        var updateFail = false;
                        if (updateDetails.Any())
                        { 
                            #region 更新對帳明細為正常付款份數

                            foreach (var updateDetail in updateDetails)
                            {
                                try
                                { 
                                    UpdateBalanceSheeetDetailStatus(updateDetail, BalanceSheetDetailStatus.Normal, "上架費份數異動");
                                    recoverCount--;
                                }
                                catch
                                {
                                    updateFail = true;
                                    message.Append(string.Format("Id:{0} 對帳單明細更新為正常付款份數更新失敗，請洽技術部。\r\n", bs.Id));
                                }
                            }

                            #endregion 更新對帳明細為正常付款份數

                            #region 更新對帳單金額 新增對帳單異動紀錄

                            if (!updateFail)
                            {
                                //更新對帳單金額
                                updateFail = UpdateBalanceSheetAmount(bs, ref message);
                            }

                            #endregion 更新對帳單金額 新增對帳單異動紀錄
                        }

                        result = !updateFail;
                    }
                }
                else
                {
                    result = true;
                }
            }

            return Json(new { Success = result && message.Length == 0, Message = message.ToString() });
        }

        public JsonResult UpdateBalanceSheetEstAmountByCost(Guid dealGuid)
        {
            var message = new StringBuilder();
            var bsList = ap.BalanceSheetGetListByProductGuid(dealGuid).ToList();
            var bsIds = bsList.Select(x => x.Id).ToList();
            //撈取已有請款單據對帳單
            var bsIdsHasBill = ap.BalanceSheetBillRelationshipGetListByBalanceSheetIds(bsIds)
                                .GroupBy(x => x.BalanceSheetId)
                                .ToDictionary(x => x.Key, x => x.Count());
            //異動對帳單金額 只限撈取尚未有請款紀錄之對帳單
            bsList = bsList.Where(x => !bsIdsHasBill.ContainsKey(x.Id)).OrderByDescending(x => x.IntervalStart).ToList();

            foreach (var bs in bsList)
            {
                UpdateBalanceSheetAmount(bs, ref message);
            }

            return Json(new { Success = message.Length == 0, Message = message.ToString() });
        }

        #endregion 系統對帳單

        #region 異業合作對帳

        [HttpGet]
        public ActionResult ChannelCheckBillSheet(int type, string appid, string selectedMonth, ChannelCheckBillSheetModel model)
        {
            if (selectedMonth != null)
            {
                var year = selectedMonth.Substring(0, 4);
                var month = selectedMonth.Substring(4, 2);
                selectedMonth = year + "/" + month;
            }
            
            ViewBag.Title = Helper.GetEnumDescription((OrderClassification)type);

            ViewBag.ChannelClientProperty = cp.ChannelClientPropertyGet(appid);
            var generalCommission = ViewBag.ChannelClientProperty.GeneralCommission;
            var minCommission = ViewBag.ChannelClientProperty.MinCommission;


            if (!string.IsNullOrEmpty(selectedMonth))
            {
                decimal? minGross = 0;
                decimal? generalGross = 0;
                var originalOrderList = cp.ViewChannelCheckBillOrderGet(type, selectedMonth);
                var originalVerificationList = cp.ViewChannelCheckBillVerificationGet(type, selectedMonth);
                var originalReturnList = cp.ViewChannelCheckBillReturnGet(type, selectedMonth);
                var originalCancelVerificationList = cp.ViewChannelCheckBillCancelVerificationGet(type, selectedMonth);
                
                #region 銷售

                var selectOrderList = originalOrderList.OrderBy(x => x.BuyDate).ToList();
                var generalOrderList = originalOrderList.Where(x => x.GrossMargin >= (decimal)generalGross).ToList().OrderBy(x => x.BuyDate);
                var lowOrderList = originalOrderList.Where(x => x.GrossMargin >= (decimal)minGross && x.GrossMargin < (decimal)generalGross).ToList().OrderBy(x => x.BuyDate);

                var totalOrderCount = selectOrderList.Count();
                var generalOrderCount = generalOrderList.Count();
                var lowOrderCount = lowOrderList.Count();
                var totalOrderSum = selectOrderList.Sum(ori => ori.Total);
                var generalOrderSum = generalOrderList.Sum(gen => gen.Total);
                var lowOrderSum = lowOrderList.Sum(low => low.Total);

                model.OriginalOrderList = selectOrderList.ToList();
                model.OriginalOrderCount = totalOrderCount;
                model.OriginalOrderTotal = totalOrderSum;
                model.GeneralOrderList = generalOrderList.ToList();
                model.GeneralOrderCount = generalOrderCount;
                model.GeneralOrderTotal = generalOrderSum;
                model.LowOrderList = lowOrderList.ToList();
                model.LowOrderCount = lowOrderCount;
                model.LowOrderTotal = lowOrderSum;



                #endregion

                #region 核銷

                var selectVerificationList = originalVerificationList.OrderBy(x => x.VDate).ToList();
                var generalVerificationList = selectVerificationList.Where(x => x.GrossMargin >= (decimal)generalGross).ToList().OrderBy(x => x.VDate);
                var lowVerificationList = selectVerificationList.Where(x => x.GrossMargin >= (decimal)minGross && x.GrossMargin < (decimal)generalGross).ToList().OrderBy(x => x.VDate);

                var totalVerificationCount = selectVerificationList.Count();
                var generalVerificationCount = generalVerificationList.Count();
                var lowVerificationCount = lowVerificationList.Count();
                var totalVerificationSum = selectVerificationList.Sum(ori => ori.ItemPrice);
                var generalVerificationSum = generalVerificationList.Sum(gen => gen.ItemPrice);
                var lowVerificationSum = lowVerificationList.Sum(low => low.ItemPrice);

                model.OriginalVerificationList = selectVerificationList.ToList();
                model.OriginalVerificationCount = totalVerificationCount;
                model.OriginalVerificationTotal = totalVerificationSum;

                model.GeneralVerificationList = generalVerificationList.ToList();
                model.GeneralVerificationCount = generalVerificationCount;
                model.GeneralVerificationTotal = generalVerificationSum;

                model.LowVerificationList = lowVerificationList.ToList();
                model.LowVerificationCount = lowVerificationCount;
                model.LowVerificationTotal = lowVerificationSum;

                model.GeneralCommission = generalCommission;
                model.LowCommission = minCommission;

                #endregion

                #region 取消核銷

                var selectCancelVerificationList = originalCancelVerificationList.OrderBy(x => x.UndoTime).ToList();
                var generalCancelVerificationList = selectCancelVerificationList.Where(x => x.GrossMargin >= (decimal)generalGross).ToList().OrderBy(x => x.UndoTime);
                var lowCancelVerificationList = selectCancelVerificationList.Where(x => x.GrossMargin >= (decimal)minGross && x.GrossMargin < (decimal)generalGross).ToList()
                    .OrderBy(x => x.UndoTime);

                var totalCancelVerificationCount = selectCancelVerificationList.Count();
                var generalCancelVerificationCount = generalCancelVerificationList.Count();
                var lowCancelVerificationCount = lowCancelVerificationList.Count();
                var totalCancelVerificationSum = selectCancelVerificationList.Sum(ori => ori.ItemPrice);
                var generalCancelVerificationSum = generalCancelVerificationList.Sum(gen => gen.ItemPrice);
                var lowCancelVerificationSum = lowCancelVerificationList.Sum(low => low.ItemPrice);

                model.OriginalCancelVerificationList = selectCancelVerificationList.ToList();
                model.OriginalCancelVerificationCount = totalCancelVerificationCount;
                model.OriginalCancelVerificationTotal = totalCancelVerificationSum;

                model.GeneralCancelVerificationList = generalCancelVerificationList.ToList();
                model.GeneralCancelVerificationCount = generalCancelVerificationCount;
                model.GeneralCancelVerificationTotal = generalCancelVerificationSum;

                model.LowCancelVerificationList = lowCancelVerificationList.ToList();
                model.LowCancelVerificationCount = lowCancelVerificationCount;
                model.LowCancelVerificationTotal = lowCancelVerificationSum;

                model.GeneralCommissionTotal = (generalVerificationSum - generalCancelVerificationSum) * generalCommission;
                model.LowCommissionTotal = (lowVerificationSum - lowCancelVerificationSum) * minCommission;
                model.TotalCommissionTotal = ((generalVerificationSum - generalCancelVerificationSum) * generalCommission) + ((lowVerificationSum - lowCancelVerificationSum) * minCommission);

                #endregion

                #region 退貨

                var selectReturnList =
                    originalReturnList.OrderBy(x => x.ReturnDate);

                var totalReturnCount = selectReturnList.Count();
                var totalReturnSum = selectReturnList.Sum(ori => ori.Prize);

                model.ReturnList = selectReturnList.ToList();
                model.ReturnCount = totalReturnCount;
                model.ReturnTotal = totalReturnSum;

                #endregion

            }

            return View(model);
        }

        #endregion 異業合作對帳

        #region 商家文件管理
        [Authorize]
        public ActionResult VbsDocument()
        {
            List<VbsDocumentCategory> vdcs = _vbs.VbsDocumentCategoryGetList()
                                                    .Where(t => t.Status != -1).OrderBy(s => s.Sort).ToList();

            ViewBag.CategoryList = vdcs;
            return View();
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetVbsDocument()
        {
            VbsDocumentCategoryCollection vdcs = _vbs.VbsDocumentCategoryGetList();
            List<VbsDocumentCategoryEntity> categoryList = vdcs.Where(t => t.Status != -1)
                .Select(x => new VbsDocumentCategoryEntity
                {
                    Id = x.Id,
                    CategoryName = x.CategoryName,
                    Sort = x.Sort,
                    Status = x.Status == -1,
                    CreateName = x.CreateId,
                    CreateDate = x.CreateDate.ToString("yyyy/MM/dd")

                }).OrderBy(s => s.Sort).ToList();
            return Json(categoryList);
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetVbsDocumentFile(string SearchSys, string SearchOrderBy, string SearchStatus)
        {
            List<VbsDocumentFile> vdfs = _vbs.VbsDocumentFileGetList().ToList();
            if(!string.IsNullOrEmpty(SearchSys) && SearchSys != "0")
            {
                if (SearchSys == "1")
                {
                    //憑證
                    vdfs = vdfs.Where(x => x.IsShop == true).ToList();
                }
                else
                {
                    //宅配
                    vdfs = vdfs.Where(x => x.IsHouse == true).ToList();
                }
            }
            if (!string.IsNullOrEmpty(SearchStatus) && SearchStatus != "0")
            {
                if (SearchStatus == "1")
                {
                    //只出現可以顯示的
                    vdfs = vdfs.Where(x => x.Status == 0).ToList();
                }
                else
                {
                    //只出現無法顯示的
                    vdfs = vdfs.Where(x => x.Status == -1).ToList();
                }
            }
            switch (SearchOrderBy)
            {
                case "create_time":
                    vdfs = vdfs.OrderBy(x => x.CreateTime).ToList();
                    break;
                case "category_id":
                    vdfs = vdfs.OrderBy(x => x.CategoryId).ToList();
                    break;
                case "create_id":
                    vdfs = vdfs.OrderBy(x => x.CreateId).ToList();
                    break;
                case "display_name":
                    vdfs = vdfs.OrderBy(x => x.DisplayName).ToList();
                    break;
                default:
                    vdfs = vdfs.OrderBy(x => x.CreateTime).ToList();
                    break;
            }

            List<VbsDocumentFileEntity> fileList = vdfs
                .Select(x => new VbsDocumentFileEntity
                {
                    Id = x.Id,
                    FilePath = x.FilePath,
                    DisplayName = x.DisplayName,
                    PageType = x.PageType,
                    CategoryId = x.CategoryId,
                    IsShop = x.IsShop,
                    IsHouse = x.IsHouse,
                    Status = x.Status == 0,
                    CreateName = x.CreateId.Substring(0, x.CreateId.IndexOf("@")),
                    CreateDate = x.CreateTime.ToString("yyyy/MM/dd"),
                    Sort = x.Sort

                }).OrderBy(s => s.Sort).ToList();
            return Json(fileList);
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateVbsCategory(List<VbsDocumentCategoryEntity> categoryList)
        {
            bool flag = false;
            try
            {
                int idx = 1;
                foreach (VbsDocumentCategoryEntity category in categoryList)
                {
                    if (string.IsNullOrEmpty(category.CategoryName))
                    {
                        continue;
                    }
                    if (category.Status)
                    {
                        VbsDocumentCategory vdc = _vbs.VbsDocumentCategoryGet(category.Id);
                        if (vdc != null && vdc.IsLoaded)
                        {
                            vdc.Status = (category.Status ? -1 : 0);
                            _vbs.VbsDocumentCategorySet(vdc);
                        }
                    }
                    else
                    {
                        VbsDocumentCategory vdc = _vbs.VbsDocumentCategoryGet(category.Id);
                        if (vdc == null || !vdc.IsLoaded)
                        {
                            vdc = new VbsDocumentCategory();
                            vdc.CreateDate = DateTime.Now;
                            vdc.CreateId = UserName;
                        }
                        vdc.CategoryName = category.CategoryName;
                        vdc.Sort = idx;
                        vdc.Status = (category.Status ? -1 : 0);
                        vdc.ModifyDate = DateTime.Now;
                        vdc.ModifyId = UserName;
                        _vbs.VbsDocumentCategorySet(vdc);

                        idx++;
                    }
                }
                flag = true;
            }
            catch(Exception ex)
            {
                logger.Info(ex);
            }

            return Json(new
            {
                Success = flag
            });
        }

        [HttpPost]
        [Authorize]
        public ActionResult UpdateVbsDocument(FormCollection forms)
        {
            bool flag = false;
            string message = string.Empty;
            try
            {
                string idList = forms["Id"];
                string displayNameList = forms["DisplayName"];
                string pageTypeList = forms["PageType"];
                string categoryIdList = forms["CategoryId"];
                string isHouseList = forms["IsHouseTxt"];
                string isShopList = forms["IsShopTxt"];
                string statusList = forms["StatusTxt"];
                int iptCount = displayNameList.Split(",").Length;

                for (int idx = 0; idx < iptCount; idx++)
                {
                    int id = 0;
                    int.TryParse(idList.Split(",")[idx], out id);
                    string displayName = displayNameList.Split(",")[idx];
                    int pageType = (int)Core.Enumeration.DocPageType.System;
                    int.TryParse(pageTypeList.Split(",")[idx], out pageType);
                    int categoryId = 0;
                    int.TryParse(categoryIdList.Split(",")[idx], out categoryId);
                    bool isHouse = false;
                    bool.TryParse(isHouseList.Split(",")[idx], out isHouse);
                    bool isShop = false;
                    bool.TryParse(isShopList.Split(",")[idx], out isShop);
                    bool status = false;
                    bool.TryParse(statusList.Split(",")[idx], out status);
                    System.Web.HttpPostedFileBase file = Request.Files[idx];

                    if (string.IsNullOrEmpty(displayName))
                    {
                        continue;
                    }
                    bool is_updated = false;
                    VbsDocumentFile vdf = _vbs.VbsDocumentFileGet(id);
                    VbsDocumentFile ori_vdf = new VbsDocumentFile();
                    if (vdf == null || !vdf.IsLoaded)
                    {
                        vdf = new VbsDocumentFile();
                        vdf.CreateId = UserName;
                        vdf.CreateTime = DateTime.Now;
                        vdf.ModifyId = UserName;
                        vdf.ModifyTime = DateTime.Now;
                    }
                    else
                    {
                        is_updated = true;
                        ori_vdf = vdf.Clone();
                    }

                    if (!string.IsNullOrEmpty(file.FileName))
                    {
                        int TimeStamp = Convert.ToInt32(Math.Round(DateTime.UtcNow.AddHours(8).Subtract(new DateTime(1970, 1, 1)).TotalSeconds, 0));
                        string realFilePath = RandomTimeStamp(TimeStamp.ToString().Substring(TimeStamp.ToString().Length - 7));
                        string ftpFolder = "VbsDocument";

                        string _Extension = file.FileName.Substring(file.FileName.LastIndexOf("."));
                        string fileName = realFilePath + _Extension;

                        if (_Extension.IndexOf("exe") >= 0)
                        {
                            message = displayName + "：不被允許之附檔名" + _Extension;
                            throw new Exception(displayName + "：不被允許之附檔名" + _Extension);
                        }

                        if (config.SellerContractIsFtp)
                        {
                            //FTP
                            SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));

                            string FolderFile = ftpFolder + "/" + realFilePath;
                            cli.SimpleUpload(file, ftpFolder, fileName);
                        }
                        else
                        {
                            //UNC
                            string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                            string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ftpFolder);

                            if (!System.IO.Directory.Exists(Dir))
                            {
                                System.IO.Directory.CreateDirectory(Dir);
                            }
                            string fname = System.IO.Path.Combine(Dir, fileName);
                            file.SaveAs(fname);
                        }
                        vdf.FileName = fileName;
                        vdf.FilePath = fileName;
                    }

                    vdf.DisplayName = displayName;
                    vdf.PageType = pageType;
                    vdf.CategoryId = categoryId;
                    vdf.IsHouse = isHouse;
                    vdf.IsShop = isShop;
                    vdf.Status = status ? 0 : -1;
                    vdf.Sort = (idx + 1);

                    if (is_updated)
                    {
                        if (ori_vdf.FileName != vdf.FileName ||
                            ori_vdf.DisplayName != vdf.DisplayName ||
                            ori_vdf.PageType != vdf.PageType ||
                            ori_vdf.CategoryId != vdf.CategoryId ||
                            ori_vdf.IsShop != vdf.IsShop ||
                            ori_vdf.IsHouse != vdf.IsHouse ||
                            ori_vdf.Status != vdf.Status)
                        {
                            vdf.ModifyId = UserName;
                            vdf.ModifyTime = DateTime.Now;
                        }
                    }
                    _vbs.VbsDocumentFileSet(vdf);
                }
                flag = true;
            }
            catch(Exception ex)
            {
                flag = false;
                logger.Info(ex);
            }
            
            return Json(new {
                Success = flag,
                Message = message
            });
        }

        [Authorize]
        public ActionResult VbsContactInfo()
        {
            VbsContactInfo info = _vbs.VbsContactInfoGet(1);
            ViewBag.ContactInfo = info;
            return View();
        }

        [HttpPost]
        [Authorize]
        public ActionResult SaveVbsContactInfo(string data)
        {
            VbsContactInfo info = _vbs.VbsContactInfoGet(1);
            if (!info.IsLoaded)
            {
                info = new VbsContactInfo();
            }
            info.ContactInfo = data;
            info.ModifyId = UserName;
            info.ModifyTime = DateTime.Now;
            _vbs.VbsContactInfoSet(info);
            return Json(true);
        }

        [Authorize]
        public ActionResult DownLoadVbsFile(int id)
        {
            VbsDocumentFile vdf = _vbs.VbsDocumentFileGet(id);
            if (vdf.IsLoaded)
            {
                string ftpFolder = "VbsDocument";
                string FileName = vdf.FileName;

                if (config.SellerContractIsFtp)
                {
                    //Use FTP
                    SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + "/" + ftpFolder + "/" + FileName));
                    cli.SimpleDownload(FileName, ftpFolder + "/" + FileName);
                }
                else
                {
                    //Use Folder
                    string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                    string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ftpFolder);

                    System.Net.WebClient wc = new System.Net.WebClient();
                    byte[] byteFiles = null;
                    string FilePath = System.IO.Path.Combine(Dir, vdf.FileName);
                    byteFiles = wc.DownloadData(FilePath);
                    string fileName = System.IO.Path.GetFileName(FilePath);
                    Response.AddHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(fileName));
                    Response.ContentType = "application/octet-stream";
                    Response.BinaryWrite(byteFiles);
                    Response.End();
                }
            }
            return null;
        }

        #endregion 商家文件管理

        #endregion Action

        #region Method

        private bool IsTriggerManualable(List<BalanceSheet> allSheets)
        {
            if (allSheets.Count == 0)
            {
                return false;
            }

            BalanceSheet lastSheet = allSheets
                .Where(x => x.GenerationFrequency != (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
                .OrderByDescending(x => x.IntervalEnd).First();

            //彈性請款對帳單 不限對帳區間皆可產 故無須人工對帳單 惟過核銷區間仍須請財務協助核銷/取消核銷憑證
            if (lastSheet.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet)
            {
                return false;
            }

            VerificationPolicy verificationPolicy = new VerificationPolicy();

            return !verificationPolicy.IsInVerifyTimeRange((BusinessModel)lastSheet.ProductType, lastSheet.ProductGuid,
                                                        lastSheet.StoreGuid, DateTime.Now)
                && !verificationPolicy.IsInVerifyTimeRange((BusinessModel)lastSheet.ProductType, lastSheet.ProductGuid,
                                                        lastSheet.StoreGuid, lastSheet.IntervalEnd, true);
        }

        private List<NonBalanceSheetVerifyInfo> GetVerifyDetail(ViewBalanceSheetNoncorrespondedVerifyCollection verifiedList)
        {
            return verifiedList.Select(x => new NonBalanceSheetVerifyInfo()
            {
                TrustId = x.TrustId,
                CouponSequenceNumber = x.CouponSequenceNumber,
                VerifiedTime = x.VerifyTime,
                VerifiedUser = x.VerifyUser
            }).ToList();
        }

        private List<BalanceSheetDetailInfo> GetUndoDetail(ViewBalanceSheetUndeductedCollection undeductedList)
        {
            return undeductedList.Select(x => new BalanceSheetDetailInfo
            {
                IsUndo = true,
                TrustId = x.TrustId,
                CouponSequenceNumber = x.CouponSequenceNumber,
                VerifiedTime = x.VerifyTime,
                VerifiedUser = x.VerifyUser,
                UndoTime = x.UndoTime,
                UndoUser = x.UndoUser,
                ModifyMessage = x.BsModifyMessage,
                ModifyUser = x.BsModifyUser,
                ModifyTime = x.BsModifyTime
            }).ToList();
        }

        private DealBalanceInfo GetDealInfo(ViewBalancingDeal dbinfo, List<BalanceSheet> monthOnlySheets, Guid? storeGuid)
        {
            DealBalanceInfo dealInfo = new DealBalanceInfo()
            {
                SellerName = dbinfo.SellerName,
                SellerGuid = dbinfo.SellerGuid,
                StoreGuid = storeGuid,
                DealId = dbinfo.DealId,
                DealGuid = dbinfo.MerchandiseGuid,
                DealName = dbinfo.DealName,
                DealUseStartTime = dbinfo.DealUseStartTime,
                DealUseEndTime = dbinfo.DealUseEndTime,
                RemittanceType = (RemittanceType)dbinfo.RemittanceType,
                MonthlyBalanceSheetCount = monthOnlySheets.Count()
            };

            if (storeGuid.HasValue)
            {
                var store = sp.SellerGet(storeGuid.Value);
                dealInfo.StoreName = store.SellerName;
            }
            return dealInfo;
        }

        private List<BalanceSheetDetailInfo> GetBalanceDetail(BalanceSheet selectedSheet, ViewBalancingDeal deal, Guid? storeGuid)
        {
            List<int> balanceSheetIds = new List<int>(); //對帳單 id, 用來撈 detail 資訊
            if ((deal.RemittanceType == (int)RemittanceType.AchWeekly || deal.RemittanceType == (int)RemittanceType.ManualWeekly)
                && (BalanceSheetGenerationFrequency)selectedSheet.GenerationFrequency != BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
            {
                List<int> weekBsIds = ap.BalanceSheetGetWeekBalanceSheetsByMonth(
                    deal.MerchandiseGuid,
                    (BusinessModel)deal.BusinessModel,
                    selectedSheet.Year.Value, selectedSheet.Month.Value)
                    .Where(sheet => sheet.StoreGuid == storeGuid)
                    .Select(sheet => sheet.Id).ToList();
                balanceSheetIds.AddRange(weekBsIds);
            }
            else
            {
                balanceSheetIds.Add(selectedSheet.Id);
            }

            ViewBalanceDetailCollection details = ap.ViewBalanceDetailGetList(balanceSheetIds);
            return details.Select(x => new BalanceSheetDetailInfo()
            {
                IsUndo = (BalanceSheetDetailStatus)x.Status == BalanceSheetDetailStatus.Deduction,
                TrustId = x.TrustId,
                CouponSequenceNumber = x.CouponSequenceNumber,
                VerifiedTime = x.VerifyTime,
                VerifiedUser = x.VerifyUserName,
                UndoTime = x.UndoTime,
                UndoUser = x.UndoUserName,
                ModifyMessage = string.Format("{0}{1}", GetSlottingFeeQuantity(deal.MerchandiseGuid) > 0 && x.Status == (int)BalanceSheetDetailStatus.NoPay ? "上架費份數 " : string.Empty , x.Message)
            }).ToList();
        }

        private int GetSlottingFeeQuantity(Guid dealGuid)
        {
            DealCostCollection dcc = pp.DealCostGetList(dealGuid);
            return dcc.Count > 1 ? dcc.Min(x => x.CumulativeQuantity).Value : 0;
        }

        //統計對帳單明細中的已標記為上架費份數之核銷份數
        private int GetVerifiedTotalCount(int balanceSheetId)
        {
            var statisticLog = pp.VerificationStatisticsLogGetListByBalanceSheetId(balanceSheetId);
            if (statisticLog.IsLoaded)
            {
                return statisticLog.Verified ?? 0;
            }

            return 0;
        }

        private bool UpdateBalanceSheetAmount(BalanceSheet bs, ref StringBuilder message)
        {
            var updateFail = false;
            var bsModel = new BalanceSheetModel(bs.Id);
            var amount = (int)bsModel.GetAccountsPayable().TotalAmount;
            var vendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
            var vendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;

            //更新對帳單金額
            if (bs.EstAmount != amount)
            {
                try
                {
                    var actionDesc = string.Format("Id : {0} UpdateBalanceSheetEstAmount : {1} -> {2}", bs.Id, bs.EstAmount, amount);
                    bs.EstAmount = amount;
                    ap.BalanceSheetEstAmountSet(bs, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount);
                    CommonFacade.AddAudit(bs.ProductGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                }
                catch (Exception)
                {
                    updateFail = true;
                    message.Append(string.Format("Id:{0} 對帳單應付金額更新失敗，請洽技術部。\r\n", bs.Id));
                }
            }

            return updateFail;
        }

        private void UpdateBalanceSheeetDetailStatus(BalanceSheetDetail detail, BalanceSheetDetailStatus status, string message)
        {
            var logStatus = BalanceModificationLogStatus.Unknown;
            switch (status)
            {
                case BalanceSheetDetailStatus.NoPay:
                    logStatus = BalanceModificationLogStatus.NormalToNoPay;
                    break;
                case BalanceSheetDetailStatus.Normal:
                    logStatus = BalanceModificationLogStatus.NoPayToNormal;
                    break;
            }
            var log = new BalanceModificationLog
            {
                BalanceSheetId = detail.BalanceSheetId,
                TrustId = detail.TrustId,
                DetailStatus = detail.Status,
                DetailStatusLogId = detail.CashTrustStatusLogId,
                Message = message,
                CreateId = UserId,
                CreateTime = DateTime.Now
            };

            using (TransactionScope scope = new TransactionScope())
            {
                log.LogStatus = (int)logStatus;
                ap.BalanceModificationLogSet(log);
                ap.BalanceSheetDetailModifyStatus(detail, status);
                scope.Complete();
            }
        }

        private static string RandomTimeStamp(string CodeTimeStamp)
        {
            List<List<int>> cdts = CorrespondenceTable();
            string ret = "";

            //產生六組亂數
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            int len = 7;
            string rRnds = randomNumbers(len);

            int idx = 0;
            for (int i = 0; i < len; i++)
            {
                List<int> cdt = cdts.ElementAt(Convert.ToInt16(rRnds.Substring(idx, 1)));
                ret += cdt.ElementAt(Convert.ToInt16(CodeTimeStamp.Substring(idx, 1))).ToString();
                idx++;
            }
            return ret;
        }
        /// <summary>
        /// 隨機取得N位數字
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public static string randomNumbers(int n)
        {
            Random rand = new Random(Guid.NewGuid().GetHashCode());
            string rRnds = "";
            while (n > 0)
            {
                if (n > 9)
                {
                    rRnds += Enumerable.Range(0, 9)
                             .OrderBy(d => rand.Next())
                             .Take(9).Select(d => d.ToString())
                             .Aggregate(
                                 (Accumulate, intValue) => Accumulate + intValue
                             );
                }
                else
                {
                    rRnds += Enumerable.Range(0, 9)
                             .OrderBy(d => rand.Next())
                             .Take(n).Select(d => d.ToString())
                             .Aggregate(
                                 (Accumulate, intValue) => Accumulate + intValue
                             );
                }

                n -= 9;
            }
            return rRnds;
        }
        /// <summary>
        /// 對照表
        /// </summary>
        /// <returns></returns>
        private static List<List<int>> CorrespondenceTable()
        {
            List<List<int>> table = new List<List<int>>();

            for (int i = 9; i >= 0; i--)
            {
                List<int> cdt = new List<int>();
                for (int j = 0; j < 10; j++)
                {
                    if (i - j >= 0)
                    {
                        cdt.Add(i - j);
                    }
                    else
                    {
                        cdt.Add(i - j + 10);
                    }
                }
                table.Add(cdt);
            }
            return table;
        }
        #endregion Method

    }
}

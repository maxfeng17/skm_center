﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Configuration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.BizLogic.Jobs;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    public class VendorBillingSystemSimulatorController : Controller
    {
        private ISysConfProvider _cfg;
        private IPponProvider _pp;
        private IHiDealProvider _hp;
        private IMemberProvider _mp;
        private ISellerProvider _sp;
        private IAccountingProvider _ap;
        private IOrderProvider _op;
        private IWmsProvider _wp;

        public VendorBillingSystemSimulatorController()
        {
            _cfg = ProviderFactory.Instance().GetConfig();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        #region Action

        public ActionResult BsGenPrep()
        {
            return View();
        }
        
        public ActionResult CouponInfo(BusinessModel bizModel, int dealId)
        {
            //ajax html only!!
            //only part of the rendered view is filled with data
            StoreCollection stores;
            Dictionary<Guid, string> storeNameLookup = new Dictionary<Guid, string>();
            CashTrustLogCollection ctlogs;
            switch(bizModel)
            {
                case BusinessModel.Ppon:
                    DealProperty dp = _pp.DealPropertyGet(dealId);
                    ctlogs = _mp.CashTrustLogGetListByBidWhereOrderIsSuccessful(dp.BusinessHourGuid);
                    PponStoreCollection ps = _pp.PponStoreGetListByBusinessHourGuid(dp.BusinessHourGuid);
                    stores = _sp.StoreGetListByStoreGuidList(ps.Select(x => x.StoreGuid).ToList());
                    foreach(Store store in stores)
                    {
                        storeNameLookup.Add(store.Guid, store.StoreName);
                    }
                    break;
                case BusinessModel.PiinLife:
                    ctlogs = _mp.CashTrustLogGetAllListByHiDealProductId(dealId);
                    HiDealProductStoreCollection pps = _hp.HiDealProductStoreGetListByProductId(dealId);
                    stores = _sp.StoreGetListByStoreGuidList(pps.Select(x => x.StoreGuid).ToList());
                    foreach (Store store in stores)
                    {
                        storeNameLookup.Add(store.Guid, store.StoreName);
                    }
                    break;
                default:
                    ctlogs = new CashTrustLogCollection();
                    break;
            }

            var model = new SimTimeChangerModel();
            model.DealId = dealId;
            model.Items = ctlogs.OrderBy(x => x.StoreGuid).ThenBy(x => x.ModifyTime).Select(x => new TrustableItemInfo
                                                 {
                                                     TrustId = x.TrustId,
                                                     CouponId = x.CouponId,
                                                     CounponSeqNo = x.CouponSequenceNumber,
                                                     ModifyTime = x.ModifyTime,
                                                     StoreName = storeNameLookup.ContainsKey(x.StoreGuid.GetValueOrDefault(Guid.Empty)) 
                                                                                    ? storeNameLookup[x.StoreGuid.GetValueOrDefault(Guid.Empty)]
                                                                                    : string.Empty,
                                                     UsageDesc = GetUsageDesc((TrustStatus) x.Status, x.SpecialStatus)
                                                 }).ToList();
            return View("BsGenPrep", model);
        }

        public JsonResult DealInfo(BusinessModel bizModel, int? uniqueId, int? pid)
        {
            switch(bizModel)
            {
                case BusinessModel.Ppon:
                    if (uniqueId.HasValue)
                    {
                        DealProperty dp = _pp.DealPropertyGet(uniqueId.Value);
                        BusinessHour bh = _pp.BusinessHourGet(dp.BusinessHourGuid);
                        DealAccounting da = _pp.DealAccountingGet(dp.BusinessHourGuid);

                        if(bh == null || !bh.IsLoaded)
                        {
                            break;
                        }

                        BalanceSheetCollection sheets = _ap.BalanceSheetGetListByProductGuid(bh.Guid);

                        return Json(new
                                        {
                                            DealExists = true, 
                                            DealId = dp.UniqueId,
                                            BizModel = (int)bizModel,
                                            DeliveryTypeDesc = GetDelvieryTypeDesc((DeliveryType)dp.DeliveryType.GetValueOrDefault((int)DeliveryType.ToShop)),
                                            BillingModelDesc = GetBillingModelDesc((VendorBillingModel)da.VendorBillingModel),
                                            RemittanceTypeDesc = GetRemittanceTypeDesc((RemittanceType)da.RemittanceType),
                                            UseStartTime = bh.BusinessHourDeliverTimeS.HasValue ? bh.BusinessHourDeliverTimeS.Value.ToString("yyyy/MM/dd") : "沒有設定",
                                            UseEndTime = bh.BusinessHourDeliverTimeE.HasValue ? bh.BusinessHourDeliverTimeE.Value.ToString("yyyy/MM/dd") : "沒有設定",
                                            PayToDesc = int.Equals(1,da.Paytocompany) ? "賣家" : "分店",
                                            BalanceSheets = sheets.Select(GetBsInfo)
                                        });
                    }
                    break;
                case BusinessModel.PiinLife:
                    if(pid.HasValue)
                    {
                        HiDealProduct hdp = _hp.HiDealProductGet(pid.Value);
                        if (hdp == null || !hdp.IsLoaded)
                        {
                            break;
                        }
                        return Json(new
                        {
                            DealExists = true,
                            DealId = hdp.Id,
                            BizModel = (int)bizModel,
                            DeliveryTypeDesc = GetDelvieryTypeDesc(hdp.IsHomeDelivery ? DeliveryType.ToHouse : DeliveryType.ToShop),
                            BillingModelDesc = GetBillingModelDesc((VendorBillingModel)hdp.VendorBillingModel),
                            RemittanceTypeDesc = GetRemittanceTypeDesc((RemittanceType)hdp.RemittanceType),
                            UseStartTime = hdp.UseStartTime.HasValue ? hdp.UseStartTime.Value.ToString("yyyy/MM/dd") : "沒有設定",
                            UseEndTime = hdp.UseEndTime.HasValue ? hdp.UseEndTime.Value.ToString("yyyy/MM/dd") : "沒有設定",
                            PayToDesc = hdp.PayToCompany ? "賣家" : "分店"
                        });
                    }
                    break;
            }
            
            return Json(new {DealExists = false});
        }

        public JsonResult SellerInfo(string sellerGuid)
        {
            Guid sid = Guid.Empty;
            Guid.TryParse(sellerGuid, out sid);


            if (sid!= Guid.Empty)
            {
                Seller s = _sp.SellerGet(sid);
                BalanceSheetWmCollection bsw = _ap.BalanceSheetWmsGetListBySellerGuid(sid);
                return Json(new
                {
                    SellerExists = true,
                    SellerGuid = s.Guid,
                    SellerName = s.SellerName,
                    BalanceSheetWmss = bsw.Select(GetBswInfo)
                });
            }
            

            return Json(new { SellerExists = false });
        }

        public JsonResult RandomizeCashTrustLog(DateTime allocStartDate, DateTime allocEndDate, string trustIdCsv)
        {
            List<Guid> trustIds = trustIdCsv.Split(",").Select(x => new Guid(x)).ToList();
            var randomTest = new Random();

            foreach(Guid trustId in trustIds)
            {
                TimeSpan timeSpan = allocEndDate - allocStartDate;
                TimeSpan newSpan = new TimeSpan(0, 0, randomTest.Next(0, (int)timeSpan.TotalSeconds));
                DateTime newDate = allocStartDate + newSpan;

                CashTrustLog ctlog = _mp.CashTrustLogGet(trustId);
                ctlog.ModifyTime = newDate;
                _mp.CashTrustLogSet(ctlog);
            }

            return Json(new { });
        }

        public JsonResult UpdateLastShipDate(string orderId,string lastShipDate)
        {

            Order o = _op.OrderGet(Order.Columns.OrderId, orderId);
            OrderProductDelivery opd = _op.OrderProductDeliveryGetByOrderGuid(o.Guid);
            if (o.IsLoaded && opd.IsLoaded)
            {
                DateTime newLastShipDate;
                DateTime.TryParse(lastShipDate, out newLastShipDate);
                opd.LastShipDate = DateTime.Parse(newLastShipDate.ToShortDateString().Trim() + " 23:59:59");
                _op.OrderProductDeliverySet(opd);
                return Json(new { IsSuccess = true, Message = "更新成功" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "失敗!" });
            }
        }


        public JsonResult UpdateShipDate(string orderId, string shipDate)
        {

            Order o = _op.OrderGet(Order.Columns.OrderId, orderId);
            OrderShip os = _op.OrderShipGet(o.Guid);
            if (o.IsLoaded && os.IsLoaded)
            {
                DateTime newShipDate;
                DateTime.TryParse(shipDate, out newShipDate);
                os.ShipTime = DateTime.Parse(newShipDate.ToShortDateString().Trim() + " 00:00:00");
                _op.OrderShipSet(os);
                return Json(new { IsSuccess = true, Message = "更新成功" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "失敗!" });
            }
        }

        public JsonResult UpdateShippedDate(string bid, string shippedDate)
        {

            Guid Bid = Guid.Empty;
            Guid.TryParse(bid, out Bid);

            DealAccounting da = _pp.DealAccountingGet(Bid);
            if (da.IsLoaded)
            {
                if (!string.IsNullOrEmpty(shippedDate))
                {
                    DateTime newShippedDate;
                    DateTime.TryParse(shippedDate, out newShippedDate);
                    da.ShippedDate = DateTime.Parse(newShippedDate.ToShortDateString().Trim() + " 00:00:00");
                    _pp.DealAccountingSet(da);
                }
                else
                {
                    da.ShippedDate = null;
                    _pp.DealAccountingSet(da);
                }
                
                return Json(new { IsSuccess = true, Message = "更新成功" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "失敗!" });
            }
        }


        public JsonResult UpdateFinalBalanceSheetDate(string bid, string finalBalanceSheetDate)
        {

            Guid Bid = Guid.Empty;
            Guid.TryParse(bid, out Bid);

            DealAccounting da = _pp.DealAccountingGet(Bid);
            if (da.IsLoaded)
            {
                if (!string.IsNullOrEmpty(finalBalanceSheetDate))
                {
                    DateTime newFinalBalanceSheetDate;
                    DateTime.TryParse(finalBalanceSheetDate, out newFinalBalanceSheetDate);
                    da.FinalBalanceSheetDate = DateTime.Parse(newFinalBalanceSheetDate.ToShortDateString().Trim() + " 00:00:00");
                    _pp.DealAccountingSet(da);
                }
                else
                {
                    da.FinalBalanceSheetDate = null;
                    _pp.DealAccountingSet(da);
                }

                return Json(new { IsSuccess = true, Message = "更新成功" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "失敗!" });
            }
        }

        public JsonResult CleanWmsRefundDate(int returnFormId,bool isClean)
        {

            ReturnForm rf = _op.ReturnFormGet(returnFormId);
            if (rf.IsLoaded)
            {
                WmsRefundOrder order = _wp.WmsRefundOrderGet(returnFormId);
                if (order.IsLoaded)
                {
                    if (isClean)
                    {
                        order.HasPickupTime = null;
                        order.ArrivalTime = null;
                        _wp.WmsRefundOrderSet(order);
                        return Json(new { IsSuccess = true, Message = "清除成功" });
                    }
                    else
                    {
                        return Json(new { IsSuccess = true, Message = order.PchomeRefundId });
                    }
                    
                }
                else
                {
                    return Json(new { IsSuccess = true, Message = "查無對應逆物流退貨單" });
                }
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "查無該退貨id" });
            }
        }

        public ActionResult BsGen()
        {
            return View();
        }

        /// <summary>
        /// 模擬產生對帳單
        /// </summary>
        /// <param name="bizModel"></param>
        /// <param name="uid"></param>
        /// <param name="pid"></param>
        /// <param name="simDate"></param>
        /// <param name="bsOpt"></param>
        /// <returns></returns>
        public JsonResult GenerateBalanceSheet(BusinessModel bizModel, int? uid, int? pid, DateTime? simDate, string bsOpt)
        {
            string resultMessage;
            if(!simDate.HasValue)
            {
                resultMessage = "沒有設定模擬時間";
                return Json(new {resultMessage});
            }
            
            DateTime simTime = new DateTime(simDate.Value.Year, simDate.Value.Month, simDate.Value.Day, 1, 0, 0);
            
            BalanceSheetGenerationFrequency freq;
            switch(bsOpt)
            {
                case "weekly":
                    freq = BalanceSheetGenerationFrequency.WeekBalanceSheet;
                    break;
                case "fortnightly":
                    freq = BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                    break;
                case "monthly":
                    freq = BalanceSheetGenerationFrequency.MonthBalanceSheet;
                    break;
                case "lumpSum":
                    freq = BalanceSheetGenerationFrequency.LumpSumBalanceSheet;
                    break;
                case "flexible":
                    freq = BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
                    break;
                default:
                    freq = BalanceSheetGenerationFrequency.Unknown;
                    break;
            }
            
            BalanceSheetSpec spec;
            IEnumerable<BalanceSheetGenerationResult> genResult;
            switch(bizModel)
            {
                case BusinessModel.Ppon:
                    DealProperty dp = _pp.DealPropertyGet(uid.GetValueOrDefault(0));
                    spec = new BalanceSheetSpec()
                               {
                                   SheetCategory = freq,
                                   BizModel = bizModel,
                                   BusinessHourGuid = dp.BusinessHourGuid
                               };
                    genResult = BalanceSheetService.SimulateCreate(spec, simTime);
                    break;
                default:
                    spec = new BalanceSheetSpec()
                               {
                                   SheetCategory = freq,
                                   BizModel = bizModel,
                                   HiDealProductId = pid.GetValueOrDefault(0)
                               };
                    genResult = BalanceSheetService.SimulateCreate(spec, simTime);
                    break;
            }

            string temp = string.Empty;
            foreach(var genRes in genResult)
            {
                temp += genRes.ToString();
            }
            resultMessage = temp;

            return Json(new { resultMessage });
        }

        public JsonResult SimulatePayment(int balanceSheetId)
        {
            var bs = new BalanceSheetModel(balanceSheetId);
            if (bs.Type == BalanceSheetType.WeeklyPayWeekBalanceSheet ||
                bs.Type == BalanceSheetType.FlexibleToHouseBalanceSheet ||
                bs.Type == BalanceSheetType.MonthlyToHouseBalanceSheet ||
                bs.Type == BalanceSheetType.WeeklyToHouseBalanceSheet)
            {
                var achJob = new GenAchReport();
                achJob.Execute(null);
                var achBs = new BalanceSheetModel(balanceSheetId);
                int payId;
                BalanceSheet dbbs = achBs.GetDbBalanceSheet();
                if (int.TryParse(dbbs.PayReportId.ToString(), out payId))
                {
                    var payReport = _op.GetWeeklyPayReportById(payId);
                    payReport.IsTransferComplete = true;
                    payReport.ResponseTime = dbbs.IntervalEnd.AddDays(2);
                    payReport.TransferAmount = achBs.GetAccountsPayable().TotalAmount;
                    payReport.Result = "00";
                    payReport.ReceiverBankNo = achBs.TransferInfo.ReceiverAccountBankNo;
                    payReport.ReceiverBranchNo = achBs.TransferInfo.ReceiverAccountBranchNo;
                    payReport.ReceiverAccountNo = achBs.TransferInfo.ReceiverAccountNo;
                    payReport.ReceiverCompanyId = achBs.TransferInfo.ReceiverCompanyId;
                    payReport.ReceiverTitle = achBs.TransferInfo.ReceiverTitle;
                    _op.SetWeeklyPayReport(payReport);
                    BalanceSheetService.CompleteWeekBalanceSheetMonthInfo();
                    return Json( new { IsSuccess = true });
                }
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "不支援模擬!" });
            }

            return Json(new { IsSuccess = false, Message="失敗!" });
        }

        public ActionResult BsDelete()
        {
            return View();
        }

        public JsonResult DeleteBalanceSheet(int uniqueId, int deleteBsId)
        {
            try
            {
                BalanceSheet bs = _ap.BalanceSheetGet(deleteBsId);
                Guid productGuid = bs.ProductGuid;
                BalanceSheet lastestBs = _ap.BalanceSheetGetLatest(BusinessModel.Ppon, productGuid, bs.StoreGuid, (BalanceSheetGenerationFrequency)bs.GenerationFrequency);
                if (lastestBs.IntervalEnd != bs.IntervalEnd)
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，請從最後一張開始刪" });
                }
                else if (lastestBs.ProductId != uniqueId)
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，對帳單與檔次不一致" });
                }
                else if (!string.IsNullOrEmpty(bs.ConfirmedUserName) && bs.ConfirmedUserName != "sys")
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，無法刪除已付款之對帳單" });
                }
                else
                {
                    if (BalanceSheetService.DeleteBalanceSheeet(bs.Id, bs.VerificationStatisticsLogId, productGuid, User.Identity.Name))
                        return Json(new { IsSuccess = true, resultMessage = "刪除成功" });
                    else
                        return Json(new { IsSuccess = false, resultMessage = "刪除失敗" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, resultMessage = ex.Message });
            }
            

            
        }

        public JsonResult DeleteBalanceSheetWms(string sellerGuid, int deleteBswId)
        {
            try
            {
                Guid sid = Guid.Empty;
                Guid.TryParse(sellerGuid, out sid);


                BalanceSheetWm bsw = _ap.BalanceSheetWmsGet(deleteBswId);

                BalanceSheetWm lastestBsw = _ap.BalanceSheetWmsGetLatest(sid);
                if (lastestBsw.IntervalEnd != bsw.IntervalEnd)
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，請從最後一張開始刪" });
                }
                else if (lastestBsw.SellerGuid != bsw.SellerGuid)
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，對帳單與商家不一致" });
                }
                else if (!string.IsNullOrEmpty(bsw.ConfirmedUserName) && bsw.ConfirmedUserName != "sys")
                {
                    return Json(new { IsSuccess = false, resultMessage = "刪除失敗，無法刪除已付款之對帳單" });
                }
                else
                {
                    if (BalanceSheetService.DeleteBalanceSheeetWms(bsw.Id, sid, User.Identity.Name))
                        return Json(new { IsSuccess = true, resultMessage = "刪除成功" });
                    else
                        return Json(new { IsSuccess = false, resultMessage = "刪除失敗" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, resultMessage = ex.Message });
            }



        }

        public ActionResult GenBsJob()
        {
            return View();
        }

        /// <summary>
        /// 模擬對帳單JOB
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <param name="simDate"></param>
        /// <param name="genOpt"></param>
        /// <param name="bsOpt"></param>
        /// <param name="outOfRange"></param>
        /// <returns></returns>
        public JsonResult ExecuteJobGenerateBalanceSheet(DeliveryType deliveryType, DateTime? simDate, string genOpt, string bsOpt, bool outOfRange)
        {
            string resultMessage;

            if (deliveryType != DeliveryType.ToShop 
             && deliveryType != DeliveryType.ToHouse)
            {
                resultMessage = "檔次類型設定有誤!!";
                return Json(new { resultMessage });
            }

            if (!simDate.HasValue)
            {
                resultMessage = "請設定模擬時間!!";
                return Json(new { resultMessage });
            }

            Stopwatch timer = new Stopwatch();
            timer.Start();

            ILog logger = LogManager.GetLogger("GenerateBalanceSheet");
            DateTime simTime = new DateTime(simDate.Value.Year, simDate.Value.Month, simDate.Value.Day, 1, 0, 0);

            RemittanceFrequency payFreq;
            switch (genOpt)
            {
                case "weekly":
                    payFreq = RemittanceFrequency.Weekly;
                    break;
                case "fortnightly":
                    payFreq = RemittanceFrequency.Fortnightly;
                    break;
                case "monthly":
                    payFreq = RemittanceFrequency.Monthly;
                    break;
                case "lumpSum":
                    payFreq = RemittanceFrequency.LumpSum;
                    break;
                case "flexible":
                    payFreq = RemittanceFrequency.Flexible;
                    break;
                default:
                    resultMessage = "出帳方式設定有誤!!";
                    return Json(new { resultMessage });
            }

            BalanceSheetGenerationFrequency freq;
            switch (bsOpt)
            {
                case "weekly":
                    freq = BalanceSheetGenerationFrequency.WeekBalanceSheet;
                    break;
                case "fortnightly":
                    freq = BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                    break;
                case "monthly":
                    freq = BalanceSheetGenerationFrequency.MonthBalanceSheet;
                    break;
                case "lumpSum":
                    freq = BalanceSheetGenerationFrequency.LumpSumBalanceSheet;
                    break;
                case "flexible":
                    freq = BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
                    break;
                default:
                    resultMessage = "對帳單類型設定有誤!!";
                    return Json(new { resultMessage });
            }

            if (!ValidateBalanceSheetType(deliveryType, payFreq, freq))
            {
                resultMessage = "目前尚無符合該對帳單格式之排程!\n請重新確認設定是否有誤!!";
                return Json(new { resultMessage });
            }

            VerificationPolicy verification = new VerificationPolicy();
            IEnumerable<BusinessHour> ppons = verification.GetPponVerifyingDealsForVendorBillingSystem(simTime, freq, payFreq, outOfRange, deliveryType).ToList();
            
            logger.InfoFormat(string.Format("後台模擬 [{0}]{1}(模擬產出日期:{2}) 排程開始執行\r\n", GetDelvieryTypeDesc(deliveryType), payFreq, simTime.ToString("yyyy/MM/dd")));
            logger.InfoFormat(string.Format("核銷期間內要產對帳單的檔次:\r\n\t好康共有 {0} 檔\r\n\t", ppons.Count()));

            foreach (BusinessHour deal in ppons)
            {
                try
                {
                    BalanceSheetSpec spec;
                    spec = new BalanceSheetSpec()
                    {
                        SheetCategory = freq,
                        BizModel = BusinessModel.Ppon,
                        BusinessHourGuid = deal.Guid,
                        GenOutOfVerificationTimeRange = outOfRange
                    };

                    var genResult = BalanceSheetService.SimulateCreate(spec, simTime);
                    string temp = string.Empty;
                    foreach (var genRes in genResult)
                    {
                        temp += genRes.ToString();
                    }
                    logger.InfoFormat(temp);
                }
                catch (Exception e)
                {
                    logger.InfoFormat(string.Format("產出bid-{0}對帳單時出現錯誤，錯誤訊息：\n{1}", deal.Guid, e.Message));
                }
            }
            
            timer.Stop();
            logger.InfoFormat(string.Format("總時間： {0:g} \r\n\t", timer.Elapsed));
            logger.InfoFormat(string.Format("後台模擬 [{0}]{1}(模擬產出日期:{2}) 排程執行結束\r\n\r\n", GetDelvieryTypeDesc(deliveryType), payFreq, simTime.ToString("yyyy/MM/dd")));

            return Json(new { resultMessage = "排程執行結果請至Web/Logs/GenerateBalanceSheet中確認" });            
        }

        public JsonResult ExecuteJobGenerateBalanceSheetWms(DateTime simTime, string genOpt, string bsOpt)
        {
            RemittanceFrequency payFreq;
            switch (genOpt)
            {
                case "fortnightly":
                    payFreq = RemittanceFrequency.Fortnightly;
                    break;
                default:
                    return Json(new { resultMessage = "出帳方式設定有誤!!" });
            }

            BalanceSheetGenerationFrequency freq;
            switch (bsOpt)
            {
                case "fortnightly":
                    freq = BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                    break;
                default:
                    return Json(new { resultMessage = "對帳單類型設定有誤!!" });
            }

            List<Guid> sellers = _wp.GetWmsProductItemForBalanceSheetSeller();
            FortnightlyToHouseBalanceSheetWms wms = new FortnightlyToHouseBalanceSheetWms();

            foreach (Guid sellerGuid in sellers)
            {
                try
                {
                    wms.SimulateCreate(freq, sellerGuid, simTime);
                }
                catch (Exception e)
                {
                    return Json(new { resultMessage = string.Format("產出seller_guid-{0}PCHOME產品對帳單時出現錯誤，錯誤訊息：\n{1}", sellerGuid, e.Message) });
                }
            }          

            return Json(new { resultMessage = "排程執行結果請至Web/Logs/GenerateBalanceSheet中確認" });
        }

        #endregion Action

        #region Method

        private dynamic GetBsInfo(BalanceSheet bs)
        {
            var model = new BalanceSheetModel(bs.Id);
            return new
            {
                bs.Id,
                StoreName = GetStoreName(bs.StoreGuid),
                IntervalStart = bs.IntervalStart.ToString("yyyy/MM/dd"),
                IntervalEnd = bs.IntervalEnd.ToString("yyyy/MM/dd"),
                IsConfirmedReadyToPay = bs.IsConfirmedReadyToPay ? "是" : "否",
                bs.Year,
                bs.Month,
                bs.PayReportId,
                bs.EstAmount,
                GetTotalAmount = (int)model.GetAccountsPayable().TotalAmount,
                TransferedAmount = model.TransferInfo.AccountsPaid,
                TransferDesc = model.TransferInfo.TransferCompletionDescription
            };
        }

        private dynamic GetBswInfo(BalanceSheetWm bsw)
        {
            return new
            {
                bsw.Id,
                IntervalStart = bsw.IntervalStart.ToString("yyyy/MM/dd"),
                IntervalEnd = bsw.IntervalEnd.ToString("yyyy/MM/dd"),
                IsConfirmedReadyToPay = bsw.IsConfirmedReadyToPay ? "是" : "否",
                bsw.Year,
                bsw.Month,
                bsw.EstAmount,
                bsw.PayReportId
            };
        }

        private Dictionary<Guid, string> stores;
        private string GetStoreName(Guid? storeGuid)
        {
            if (!storeGuid.HasValue)
            {
                return string.Empty;
            }

            if (stores == null)
            {
                stores = new Dictionary<Guid, string>();
            }

            if (stores.ContainsKey(storeGuid.Value))
            {
                return stores[storeGuid.Value];
            }

            var store = _sp.SellerGet(storeGuid.Value);
            if (store != null && store.IsLoaded)
            {
                stores.Add(storeGuid.Value, store.SellerName);
                return store.SellerName;
            }

            return string.Empty;
        }

        private string GetRemittanceTypeDesc(RemittanceType remittanceType)
        {
            switch (remittanceType)
            {
                case RemittanceType.Others:
                    return "其他付款方式";
                case RemittanceType.AchWeekly:
                    return "ACH每周付款";
                case RemittanceType.AchMonthly:
                    return "ACH每月付款";
                case RemittanceType.ManualPartially:
                    return "部分預付";
                case RemittanceType.ManualWeekly:
                    return "人工每周付款";
                case RemittanceType.ManualMonthly:
                    return "人工每月付款";
                case RemittanceType.Flexible:
                    return "彈性選擇出帳";
                case RemittanceType.Monthly:
                    return "新每月出帳";
                case RemittanceType.Weekly:
                    return "新每週出帳";
                case RemittanceType.Fortnightly:
                    return "雙周週出帳";
                default:
                    return "其他付款方式(無對帳單)";
            }
        }

        private string GetBillingModelDesc(VendorBillingModel vendorBillingModel)
        {
            switch (vendorBillingModel)
            {
                case VendorBillingModel.BalanceSheetSystem:
                    return "新版核銷對帳系統";
                case VendorBillingModel.MohistSystem:
                    return "墨攻核銷對帳系統(無對帳單)";
                default:
                    return "舊版核銷對帳系統(無對帳單)";
            }
        }

        private string GetUsageDesc(TrustStatus status, int specialStatus)
        {
            switch (status)
            {
                case TrustStatus.Verified:
                    if (Helper.IsFlagSet(specialStatus, TrustSpecialStatus.VerificationForced))
                    {
                        return "強制核銷";
                    }
                    return "已核銷";
                case TrustStatus.Returned:
                case TrustStatus.Refunded:
                    if (Helper.IsFlagSet(specialStatus, TrustSpecialStatus.ReturnForced))
                    {
                        return "強制退貨";
                    }
                    return "已退貨";
                default:
                    return "未使用";
            }
        }

        private string GetDelvieryTypeDesc(DeliveryType deliveryType)
        {
            switch (deliveryType)
            {
                case DeliveryType.ToShop:
                    return "憑證";
                case DeliveryType.ToHouse:
                    return "宅配商品";
                default:
                    return "未指定";
            }
        }

        private bool ValidateBalanceSheetType(DeliveryType deliveryType, RemittanceFrequency payFreq, BalanceSheetGenerationFrequency genFreq)
        {
            if (genFreq == BalanceSheetGenerationFrequency.Unknown || payFreq == RemittanceFrequency.Unknown)
            {
                return false;
            }

            if (payFreq == RemittanceFrequency.Monthly)
            {
                return genFreq == BalanceSheetGenerationFrequency.MonthBalanceSheet;
            }

            if (payFreq == RemittanceFrequency.Flexible)
            {
                return genFreq == BalanceSheetGenerationFrequency.FlexibleBalanceSheet;
            }

            if (deliveryType == DeliveryType.ToShop)
            {
                if (payFreq == RemittanceFrequency.LumpSum || genFreq == BalanceSheetGenerationFrequency.LumpSumBalanceSheet)
                {
                    return false;
                }

                if (payFreq == RemittanceFrequency.Weekly)
                {
                    return genFreq == BalanceSheetGenerationFrequency.WeekBalanceSheet
                        || genFreq == BalanceSheetGenerationFrequency.MonthBalanceSheet;
                }

            }
            else if (deliveryType == DeliveryType.ToHouse)
            {
                if (payFreq == RemittanceFrequency.Weekly)
                {
                    return genFreq == BalanceSheetGenerationFrequency.WeekBalanceSheet;
                }

                if (payFreq == RemittanceFrequency.Fortnightly)
                {
                    return genFreq == BalanceSheetGenerationFrequency.FortnightlyBalanceSheet;
                }

                if (payFreq == RemittanceFrequency.LumpSum)
                {
                    return genFreq == BalanceSheetGenerationFrequency.LumpSumBalanceSheet;
                }
            }

            return true;
        }

        #endregion Method

    }
}

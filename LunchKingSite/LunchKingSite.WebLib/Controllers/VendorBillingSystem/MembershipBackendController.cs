﻿using LunchKingSite.Core.Enumeration;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Verification;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using IsolationLevel = System.Transactions.IsolationLevel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.Core.Interface;

namespace LunchKingSite.WebLib.Controllers
{
    public class VendorBillingSystemMembershipController : Controller
    {
        private static ISellerProvider sp;
        private static IMemberProvider mp;
        private static IPponProvider pp;
        private static IHiDealProvider hp;
        private static IOrderProvider op;
        private static ISysConfProvider config;
        private static IPCPProvider pcp;
        private static IVbsProvider vbs;

        static VendorBillingSystemMembershipController()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            config = ProviderFactory.Instance().GetConfig();
            pcp = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            vbs = ProviderFactory.Instance().GetProvider<IVbsProvider>();
        }

        #region 核銷帳號管理

        [HttpGet]
        public ActionResult New()
        {
            return View(new NewMemberModel());
        }

        /// <summary>
        /// 建立內部核銷帳號
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult New(NewMemberModel model)
        {
            NewMemberModelParser mem = new NewMemberModelParser()
            {
                AccountType = VbsMembershipAccountType.VerificationAccount,
                AccountId = model.AccountId,
                UserName = model.AccountName,
                Password = model.Password,
                Email = model.Email,
                UseInspectionCode = false,
                InspectionCode = string.Empty,
                ViewBalanceSheetRight = model.ViewBalanceSheet,
                ViewVerifyRight = model.ViewVerify,
                ViewShipRight = model.ViewShip,
                ViewBookingRight = model.ViewBooking,
                ViewReserveLockRight = model.ViewReserveLock,
                ViewUserEvaluateRight = model.ViewUserEvaluateRight,
                ViewToShop = model.ViewToShop,
                ViewToHouse = model.ViewToHouse
            };

            MembershipCreateStatus createStatus = VbsMemberUtility.RegistersAccount(mem);


            if (createStatus == MembershipCreateStatus.Success)
            {
                model.AccountCreationResult = true;
                model.AccountId = string.Empty;
                model.AccountName = string.Empty;
                model.Email = string.Empty;
                model.Password = string.Empty;
            }
            else
            {
                model.AccountCreationResult = false;
                model.AccounCreationFailReason = GetStatusFailString(createStatus);
            }

            return View(model);
        }

        private string GetStatusFailString(MembershipCreateStatus status)
        {
            if (status == MembershipCreateStatus.DuplicateProviderUserKey)
            {
                return "帳號重複!";
            }

            if (status == MembershipCreateStatus.InvalidPassword)
            {
                return "密碼無效!";
            }

            if (status == MembershipCreateStatus.InvalidAnswer)
            {
                return "查帳碼無效!";
            }

            return "請洽技術部: " + status.ToString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="sc">query string for SearchCondition, 返回列表時用</param>
        /// <param name="se">query string for SearchExpression, 返回列表時用</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Search(MemberSearchCondition sc = MemberSearchCondition.Email, string se = null)
        {
            var model = new SearchMemberModel();

            VbsMembershipCollection dbresult;
            List<VbsMembershipAccountType> accountTypes = new List<VbsMembershipAccountType>();
            accountTypes.Add(VbsMembershipAccountType.VerificationAccount);

            if (!string.IsNullOrWhiteSpace(se))
            {
                model.SearchCondition = sc;
                model.SearchExpression = HttpUtility.UrlDecode(se);

                switch (model.SearchCondition)
                {
                    case MemberSearchCondition.AccountId:
                        dbresult = mp.VbsMembershipGetListLikeAccountId(model.SearchExpression, accountTypes);
                        break;

                    case MemberSearchCondition.Email:
                        dbresult = mp.VbsMembershipGetListByEmail(model.SearchExpression, accountTypes, true);
                        break;

                    case MemberSearchCondition.Name:
                        dbresult = mp.VbsMembershipGetListByAccountName(model.SearchExpression, accountTypes, true);
                        break;

                    default:
                        dbresult = new VbsMembershipCollection();
                        break;
                }
            }
            else
            {
                // no query string, then select all members
                dbresult = mp.VbsMembershipGetList(accountTypes);
            }
            List<MemberSearchInfo> memList = dbresult.Select(mem =>
                    new MemberSearchInfo()
                    {
                        AccountId = mem.AccountId,
                        MemberName = mem.Name,
                        MemberEmail = mem.Email,
                        IsAccountLocked = mem.IsLockedOut
                    }).ToList();

            model.MemberInfos = memList;

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(SearchMemberModel model)
        {
            VbsMembershipCollection dbresult;
            List<VbsMembershipAccountType> accountTypes = new List<VbsMembershipAccountType>();
            accountTypes.Add(VbsMembershipAccountType.VerificationAccount);

            switch (model.SearchCondition)
            {
                case MemberSearchCondition.AccountId:
                    dbresult = mp.VbsMembershipGetListLikeAccountId(model.SearchExpression, accountTypes);
                    break;

                case MemberSearchCondition.Email:
                    dbresult = mp.VbsMembershipGetListByEmail(model.SearchExpression, accountTypes, true);
                    break;

                case MemberSearchCondition.Name:
                    dbresult = mp.VbsMembershipGetListByAccountName(model.SearchExpression, accountTypes, true);
                    break;

                default:
                    dbresult = new VbsMembershipCollection();
                    break;
            }

            List<MemberSearchInfo> memList = dbresult.Select(mem =>
                new MemberSearchInfo()
                {
                    AccountId = mem.AccountId,
                    MemberName = mem.Name,
                    MemberEmail = mem.Email,
                    IsAccountLocked = mem.IsLockedOut
                }).ToList();

            model.MemberInfos = memList;
            return View(model);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="acct"></param>
        /// <param name="sc">query string for SearchCondition, 返回列表時用</param>
        /// <param name="se">query string for SearchExpression, 返回列表時用</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Manage(string acct, MemberSearchCondition sc = MemberSearchCondition.Unknown, string se = null)
        {
            VbsMembership mem = mp.VbsMembershipGetByAccountId(acct);
            ManageMemberModel model = new ManageMemberModel();

            if (mem.IsLoaded)
            {
                model.AccountId = mem.AccountId;
                model.AccountName = mem.Name;
                model.Email = mem.Email;
                model.ViewBalanceSheet = mem.ViewBsRight;
                model.ViewVerify = mem.ViewVerifyRight;
                model.ViewShip = mem.ViewShipRight;
                model.ViewBooking = mem.ViewBookingRight;
                model.ViewReserveLock = mem.ViewReserveLockRight;
                model.ViewUserEvaluate = mem.ViewUserEvaluateRight;
                model.ViewToShop = mem.ViewToShop;
                model.ViewToHouse = mem.ViewToHouse;
            }

            if (!string.IsNullOrWhiteSpace(se) && sc != MemberSearchCondition.Unknown)
            {
                model.HasQueryString = true;
                model.SearchCondition = sc;
                model.SearchExpression = se;
            }

            return View(model);
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="model"></param>
        /// <param name="sc">query string for SearchCondition, 返回列表時用</param>
        /// <param name="se">query string for SearchExpression, 返回列表時用</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Manage(ManageMemberModel model, MemberSearchCondition sc = MemberSearchCondition.Unknown, string se = null)
        {
            switch (model.ManageMode)
            {
                case MemberManageMode.ChangePassword:
                    model.ChangePasswordResult = VbsMemberUtility.AdministrativeChangePassword(model.AccountId, model.Password);
                    break;

                case MemberManageMode.UpdateMemberInformation:
                    VbsMembership member = mp.VbsMembershipGetByAccountId(model.AccountId);
                    member.Name = model.AccountName;
                    member.Email = model.Email;
                    member.ViewBsRight = model.ViewBalanceSheet;
                    member.ViewVerifyRight = model.ViewVerify;
                    member.ViewShipRight = model.ViewShip;
                    member.ViewBookingRight = model.ViewBooking;
                    member.ViewReserveLockRight = model.ViewReserveLock;
                    member.ViewUserEvaluateRight = model.ViewUserEvaluate;
                    member.ViewToShop = model.ViewToShop;
                    member.ViewToHouse = model.ViewToHouse;
                    mp.VbsMembershipSet(member);
                    model.ChangeBasicInfoResult = true;
                    break;

                case MemberManageMode.Unknown:
                default:
                    break;
            }

            if (!string.IsNullOrWhiteSpace(se) && sc != MemberSearchCondition.Unknown)
            {
                model.HasQueryString = true;
                model.SearchCondition = sc;
                model.SearchExpression = se;
            }

            return View(model);
        }

        public JsonResult LockAccount(string accountId)
        {
            var result = new { AccountId = accountId, Result = false };
            if (!string.IsNullOrWhiteSpace(accountId))
            {
                VbsMembership member = mp.VbsMembershipGetByAccountId(accountId);
                if (member.IsLoaded && member.IsLockedOut == false)
                {
                    member.IsLockedOut = true;
                    mp.VbsMembershipSet(member);
                    result = new { AccountId = accountId, Result = true };
                }
            }

            return Json(result);
        }

        public JsonResult UnlockAccount(string accountId)
        {
            var result = new { AccountId = accountId, Result = false };
            if (!string.IsNullOrWhiteSpace(accountId))
            {
                VbsMembership member = mp.VbsMembershipGetByAccountId(accountId);
                if (member.IsLoaded && member.IsLockedOut == true)
                {
                    member.IsLockedOut = false;
                    mp.VbsMembershipSet(member);
                    result = new { AccountId = accountId, Result = true };
                }
            }

            return Json(result);
        }

        #endregion 核銷帳號管理

        #region 商家帳號管理

        public ActionResult SearchVendor(SearchVendorModel model)
        {
            switch (model.SearchCondition)
            {
                case VendorSearchCondition.SellerName:
                    model.AccountSearchResults = AccountSearchResultsBySellerName(model.SearchExpression);
                    break;

                case VendorSearchCondition.PponUniqueId:
                    int uniqueId;
                    if (int.TryParse(model.SearchExpression, out uniqueId))
                    {
                        model.AccountSearchResults = AccountSearchResultsByPponUniqueId(uniqueId);
                    }
                    break;

                case VendorSearchCondition.Bid:
                    Guid bid;
                    if (Guid.TryParse(model.SearchExpression, out bid))
                    {
                        model.AccountSearchResults = AccountSearchResultsByBid(bid);
                    }
                    break;

                case VendorSearchCondition.PiinDealId:
                    int did;
                    if (int.TryParse(model.SearchExpression, out did))
                    {
                        model.AccountSearchResults = AccountSearchResultsByDid(did);
                    }
                    break;

                case VendorSearchCondition.PiinProductId:
                    int pid;
                    if (int.TryParse(model.SearchExpression, out pid))
                    {
                        model.AccountSearchResults = AccountSearchResultsByPid(pid);
                    }
                    break;
            }

            return View(model);
        }

        private List<AccountSearchResult> AccountSearchResultsBySellerName(string sellerName)
        {
            List<AccountSearchResult> tempResults = new List<AccountSearchResult>();

            var sellers = sp.SellerGetListByLikelyName(sellerName);
            if (sellers.Any())
            {
                foreach (Seller seller in sellers)
                {
                    tempResults.Add(new AccountSearchResult()
                                    {
                                        SellerName = string.Format("{0} {1}", seller.SellerId, seller.SellerName),
                                        SellerGuid = seller.Guid,
                                        IsSeller = true,
                                        IsStoreShow = seller.StoreStatus == (int)StoreStatus.Available
                                    });
                }
            }

            return FillAccountIds(tempResults);
        }

        private List<AccountSearchResult> AccountSearchResultsByPponUniqueId(int uniqueId)
        {
            DealProperty dp = pp.DealPropertyGet(uniqueId);
            if (dp.IsLoaded)
            {
                return AccountSearchResultsByBid(dp.BusinessHourGuid);
            }

            return new List<AccountSearchResult>();
        }

        private List<AccountSearchResult> AccountSearchResultsByBid(Guid bid)
        {
            List<AccountSearchResult> tempResults = new List<AccountSearchResult>();

            var sellers = SellerFacade.GetPponStoreSellersByBId(bid);
            if (sellers.Any())
            {
                foreach (Seller seller in sellers)
                {
                    tempResults.Add(new AccountSearchResult()
                    {
                        SellerName = string.Format("{0} {1}", seller.SellerId, seller.SellerName),
                        SellerGuid = seller.Guid,
                        IsSeller = true,
                        IsStoreShow = seller.StoreStatus == (int)StoreStatus.Available
                    });
                }
            }

            return FillAccountIds(tempResults);
        }

        private List<AccountSearchResult> AccountSearchResultsByDid(int did)
        {
            List<AccountSearchResult> tempResults = new List<AccountSearchResult>();
            HiDealDeal deal = hp.HiDealDealGet(did);
            if (deal.IsLoaded)
            {
                Seller s = sp.SellerGet(deal.SellerGuid);
                StoreCollection stores = sp.StoreGetListBySellerGuid(s.Guid);
                tempResults.Add(new AccountSearchResult()
                                    {
                                        SellerName = string.Format("{0} {1}", s.SellerId, s.SellerName),
                                        SellerGuid = s.Guid,
                                        IsSeller = true,
                                        IsStoreShow = s.StoreStatus == (int)StoreStatus.Available
                });
                tempResults.AddRange(stores
                                         .Where(store => store.Status == (int)StoreStatus.Available)
                                         .Select(store => new AccountSearchResult
                                                              {
                                                                  SellerName = store.StoreName,
                                                                  SellerGuid = store.Guid,
                                                                  IsSeller = false,
                                                                  IsStoreShow = store.Status == (int)StoreStatus.Available
                                         }));
            }
            return FillAccountIds(tempResults);
        }

        private List<AccountSearchResult> AccountSearchResultsByPid(int pid)
        {
            List<AccountSearchResult> tempResults = new List<AccountSearchResult>();
            HiDealProduct product = hp.HiDealProductGet(pid);
            if (product.IsLoaded)
            {
                Seller s = sp.SellerGet(product.SellerGuid);
                StoreCollection stores = sp.StoreGetListBySellerGuid(s.Guid);
                tempResults.Add(new AccountSearchResult()
                                    {
                                        SellerName = string.Format("{0} {1}", s.SellerId, s.SellerName),
                                        SellerGuid = s.Guid,
                                        IsSeller = true,
                                        IsStoreShow = s.StoreStatus == (int)StoreStatus.Available
                });
                tempResults.AddRange(stores
                                         .Where(store => store.Status == (int)StoreStatus.Available)
                                         .Select(store => new AccountSearchResult
                                                              {
                                                                  SellerName = store.StoreName,
                                                                  SellerGuid = store.Guid,
                                                                  IsSeller = false,
                                                                  IsStoreShow = store.Status == (int)StoreStatus.Available
                                         }));
            }
            return FillAccountIds(tempResults);
        }

        private List<AccountSearchResult> FillAccountIds(List<AccountSearchResult> tempResults)
        {
            IEnumerable<Guid> resources =
                tempResults.Select(search => search.SellerGuid);
            var acls = mp.ResourceAclGetListByResourceGuidList(resources)
                            .Where(x=> ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                                    && ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read))
                            .GroupBy(x => new { x.AccountId, x.ResourceGuid });

            AccountSearchResult[] accounts = tempResults.ToArray();
            for (int i = 0; i < accounts.Count(); i++)
            {
                AccountSearchResult account = accounts[i];
                Guid resoureGuid = account.SellerGuid;

                IEnumerable<string> accountIds = from acl in acls
                                                 where acl.Key.ResourceGuid == resoureGuid
                                                 select acl.Key.AccountId;
                account.AccountIds = accountIds.ToList();
            }

            return accounts.ToList();
        }

        public ActionResult NewVendor(NewVendorModel model)
        {
            #region 檢查格式

            var formatValidated = new List<Guid>();
            foreach (string sGuid in model.SellerStorePairs)
            {
                Guid sellerGuid;
                if (Guid.TryParse(sGuid, out sellerGuid))
                {
                    formatValidated.Add(sellerGuid);
                }
            }

            #endregion 檢查格式

            var accounts = new List<NewVendorInfo>();
            foreach (var sellerGuid in formatValidated)
            {
                Seller seller = sp.SellerGet(sellerGuid);
                if (!seller.IsLoaded)
                {
                    continue;
                }

                string prefix = VendorBillingAccount.GetPrefix(sellerGuid);
                int prefixCount = VendorBillingAccount.GetPrefixUseCount(prefix);
                                
                NewVendorInfo account = new NewVendorInfo();
                int suffix = prefixCount == 0 ? 0 : 1;
                int total_loop = 0;
                bool exist = true;
                while (exist)
                {
                    if (!VendorBillingAccount.IsAccountIdExist(string.Format("{0}{1:0000}", prefix, suffix)))
                    {
                        account.AccountId = string.Format("{0}{1:0000}", prefix, suffix);
                        exist = false;
                    }
                    else
                    {
                        suffix++;
                        total_loop++;
                        if (total_loop > 500) exist = false;
                    }
                }
                account.AccountName = seller.SellerName;
                account.DefaultResource = seller.Guid;
                account.DefaultResourceType = ResourceType.Seller;
                account.ResourceSellerName = seller.SellerName;
                
                accounts.Add(account);
            }

            model.Accounts = accounts;

            ViewBag.ShoppingCartEnabled = config.ShoppingCartEnabled;
            return View(model);
        }

        /// <summary>
        /// 建立商家帳號
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateVendor(NewVendorModel model)
        {
            //requirement: all or nothing

            ResourceAclCollection acls = new ResourceAclCollection();
            foreach (NewVendorInfo info in model.Accounts)
            {
                ResourceAcl acl = new ResourceAcl()
                                      {
                                          AccountId = info.AccountId,
                                          AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
                                          ResourceGuid = info.DefaultResource,
                                          ResourceType = (int)info.DefaultResourceType,
                                          PermissionType = (int)PermissionType.Read,
                                          PermissionSetting = (int)PermissionType.Read
                                      };
                acls.Add(acl);
                if (info.DefaultResourceType == ResourceType.Seller)
                {
                    if (!model.Accounts.Any(x => x.DefaultResource == info.DefaultResource && 
                                                 x.DefaultResourceType == ResourceType.Store))
                    {
                        var storeAcl = new ResourceAcl()
                        {
                            AccountId = info.AccountId,
                            AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
                            ResourceGuid = info.DefaultResource,
                            ResourceType = (int)ResourceType.Store,
                            PermissionType = (int)PermissionType.Read,
                            PermissionSetting = (int)PermissionType.Read
                        };
                        acls.Add(storeAcl);
                    }
                }

                
                #region 中間層加權限
                //List<Guid> childSellerGuidList = sp.SellerTreeMiddleGetSellerGuid(new List<Guid> { info.DefaultResource }, null).ToList();
                //if (childSellerGuidList.Count() > 0)
                //{

                //    //往下找所有的下層
                //    List<Guid> child = new List<Guid>();
                //    child = sp.SellerTreeMiddleGetSellerGuid(childSellerGuidList, null).ToList();
                //    while (child.Count > 0)
                //    {
                //        childSellerGuidList.AddRange(child);
                //        child = sp.SellerTreeMiddleGetSellerGuid(child, null).ToList();
                //    }


                //    foreach (var childSellerGuid in childSellerGuidList)
                //    {
                //        var childAclSeller = new ResourceAcl()
                //        {
                //            AccountId = info.AccountId,
                //            AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
                //            ResourceGuid = childSellerGuid,
                //            ResourceType = (int)ResourceType.Seller,
                //            PermissionType = (int)PermissionType.Read,
                //            PermissionSetting = (int)PermissionType.Read
                //        };
                //        acls.Add(childAclSeller);

                //        var childAclStore = new ResourceAcl()
                //        {
                //            AccountId = info.AccountId,
                //            AccountType = (int)ResourceAclAccountType.VendorBillingSystem,
                //            ResourceGuid = childSellerGuid,
                //            ResourceType = (int)ResourceType.Store,
                //            PermissionType = (int)PermissionType.Read,
                //            PermissionSetting = (int)PermissionType.Read
                //        };
                //        acls.Add(childAclStore);
                //    }
                //}
                #endregion
            }

            bool allSuccess = true;
            StringBuilder sb = new StringBuilder();
            foreach (NewVendorInfo info in model.Accounts)
            {

                NewMemberModelParser mem = new NewMemberModelParser()
                {
                    AccountType = VbsMembershipAccountType.VendorAccount,
                    AccountId = info.AccountId,
                    UserName = info.AccountName,
                    Password = info.Password,
                    Email = string.Empty,
                    UseInspectionCode = info.UseInspectionCode,
                    InspectionCode = info.InspectionCode,
                    ViewBalanceSheetRight = info.ViewBalanceSheetRight,
                    ViewVerifyRight = info.ViewVerifyRight,
                    ViewShipRight = info.ViewShipRight,
                    ViewBookingRight = info.ViewBookingRight,
                    ViewReserveLockRight = info.ViewReserveLockRight,
                    ViewUserEvaluateRight = info.ViewUserEvaluateRight,
                    ViewProposalRight = info.ViewProposalRight,
                    ViewProposalProductRight = info.ViewProposalProductRight,
                    ViewShoppingCartRight = info.ViewShoppingCartRight,
                    ViewAccountManageRight = info.ViewAccountManageRight,
                    ViewCustomerServiceManageRight = info.ViewCustomerServiceManageRight,
                    ViewWmsRight = info.ViewWmsRight,
                    ViewToShop = info.ViewToShop,
                    ViewToHouse = info.ViewToHouse
                };

                MembershipCreateStatus createStatus = VbsMemberUtility.RegistersAccount(mem);

                if (createStatus == MembershipCreateStatus.Success)
                {
                    SellerFacade.ResourceAclSetList(acls, "新增帳號權限", User.Identity.Name);

                    #region 熟客系統角色與權限
                    var vbsMembership = mp.VbsMembershipGetByAccountId(info.AccountId);

                    //角色
                    string failReason = EditVendorRole(info.VendorRole.ToString(), info.AccountId);
                    if (info.VbsMembershipPermissionScope != null)
                    {
                        //權限
                        SetMembershipPermission(vbsMembership, info.VbsMembershipPermissionScope.Select(x => Convert.ToInt32(x)).ToList());
                    }
                    #endregion

                    allSuccess &= true;
                    sb.Append(string.Format("帳號：{0} 新增成功{1}\\n", mem.AccountId
                        , string.IsNullOrWhiteSpace(failReason) ? "" : "，但" + failReason));
                }
                else
                {
                    allSuccess &= false;
                    sb.Append(string.Format("帳號：{0} 儲存失敗，請稍後再試。\\n", mem.AccountId));
                    break;
                }
            }

            model.CreateVendorResult = allSuccess;
            model.CreateVendorResultMsg = sb.ToString();

            return View("NewVendor", model);
        }

        /// <summary>
        /// Vendor Account Management - single exact AccountId info, i.e. not fuzzy search
        ///列出帳號資訊 & 帳號具有的權限列表
        /// </summary>
        /// <param name="acct">account Id</param>
        [HttpGet]
        public ActionResult ManageVendor(string acct)
        {
            if (string.IsNullOrWhiteSpace(acct))
            {
                return View(new ManageVendorModel());
            }

            List<VbsMembershipAccountType> accTypes = new VbsMembershipAccountType[] { VbsMembershipAccountType.VendorAccount }.ToList();
            VbsMembership mem = mp.VbsMembershipGetByAccountId(acct, accTypes);
            if (mem != null && mem.IsLoaded)
            {
                ManageVendorModel model = new ManageVendorModel()
                {
                    State = ManageVendorAccountState.SingleAccount,
                    AccountId = mem.AccountId,
                    AccountName = mem.Name,
                    HasInspectionCode = mem.HasInspectionCode,
                    ViewBsRight = mem.ViewBsRight,
                    ViewVerifyRight = mem.ViewVerifyRight,
                    ViewShipRight = mem.ViewShipRight,
                    ViewBookingRight = mem.ViewBookingRight,
                    ViewReserveLockRight = mem.ViewReserveLockRight,
                    ViewUserEvaluateRight = mem.ViewUserEvaluateRight,
                    ViewProposalRight = mem.ViewProposalRight,
                    ViewProposalProductRight = mem.ViewProposalProductRight,
                    ViewShoppingCartManageRight = mem.ViewShoppingCartRight,
                    ViewAccountManageRight = mem.ViewAccountManageRight,
                    ViewCustomerServiceRight = mem.ViewCustomerServiceRight,
                    ViewWmsRight = mem.ViewWmsRight,
                    ViewToShop = mem.ViewToShop,
                    ViewToHouse = mem.ViewToHouse
                };

                var perE = Enum.GetNames(typeof(VendorRole)).ToList();
                VbsRoleCollection rc = mp.VbsRoleGetList(mem.UserId.Value);
                var s = rc.Select(a => a.RoleName).ToList().Intersect(perE);
                VendorRole val;
                Enum.TryParse(s.DefaultIfEmpty("").First(), out val);
                model.VendorRole = (int)val;


                VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(model.AccountId);
                model.CurrentPermissions = aclModel.GetAllowedAcl();
                model.AccountIdentity = GetAccountIdentity(model.AccountId);
                model.BindAccount = GetBindAccount(ManageVendorSearchOption.AccountId, model.AccountId);
                ViewBag.ShoppingCartEnabled = config.ShoppingCartEnabled;
                ViewBag.SimulateUserName = User.Identity.Name;
                return View(model);
            }

            return View(new ManageVendorModel());
        }

        /// <summary>
        /// Vendor Account Management - 1. fuzzy search  2. multiple account selection
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ManageVendor(ManageVendorModel model)
        {
            //POST reason: multiple account selection
            if (!string.IsNullOrEmpty(model.SelectedAccountId))
            {
                VbsMembership mem = mp.VbsMembershipGetByAccountId(model.SelectedAccountId);
                if (mem != null && mem.IsLoaded)
                {
                    model.State = ManageVendorAccountState.SingleAccount;
                    model.AccountId = mem.AccountId;
                    model.AccountName = mem.Name;
                    model.HasInspectionCode = mem.HasInspectionCode;
                    VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(model.AccountId);
                    model.CurrentPermissions = aclModel.GetAllowedAcl();
                    model.ViewBsRight = mem.ViewBsRight;
                    model.ViewVerifyRight = mem.ViewVerifyRight;
                    model.ViewShipRight = mem.ViewShipRight;
                    model.ViewBookingRight = mem.ViewBookingRight;
                    model.ViewReserveLockRight = mem.ViewReserveLockRight;
                    model.ViewUserEvaluateRight = mem.ViewUserEvaluateRight;
                    model.ViewProposalRight = mem.ViewProposalRight;
                    model.ViewProposalProductRight = mem.ViewProposalProductRight;
                    model.ViewShoppingCartManageRight = mem.ViewShoppingCartRight;
                    model.ViewAccountManageRight = mem.ViewAccountManageRight;
                    model.ViewCustomerServiceRight = mem.ViewCustomerServiceRight;
                    model.ViewWmsRight = mem.ViewWmsRight;
                    model.ViewToShop = mem.ViewToShop;
                    model.ViewToHouse = mem.ViewToHouse;

                    var perE = Enum.GetNames(typeof(VendorRole)).ToList();
                    VbsRoleCollection rc = mp.VbsRoleGetList(mem.UserId.Value);
                    var s = rc.Select(a => a.RoleName).ToList().Intersect(perE);
                    VendorRole val;
                    Enum.TryParse(s.DefaultIfEmpty("").First(), out val);
                    model.VendorRole = (int)val;
                    model.AccountIdentity = GetAccountIdentity(model.AccountId);
                    model.BindAccount = GetBindAccount(ManageVendorSearchOption.AccountId, model.AccountId);
                }
                return View(model);
            }

            //POST reason: fuzzy search member
            VbsMembershipCollection members = new VbsMembershipCollection();
            List<VbsMembershipAccountType> accountTypes = new VbsMembershipAccountType[] { VbsMembershipAccountType.VendorAccount }.ToList();
            switch (model.AccountSearchOption)
            {
                case ManageVendorSearchOption.AccountId:
                    members = mp.VbsMembershipGetListLikeAccountId(model.AccountSearchExpression, accountTypes, true);
                    break;

                case ManageVendorSearchOption.AccountName:
                    members = mp.VbsMembershipGetListByAccountName(model.AccountSearchExpression, accountTypes, true);
                    break;

                case ManageVendorSearchOption.BindAccount://自訂帳號
                    members = mp.VbsMembershipGetListByBindAccount(model.AccountSearchExpression);
                    break;
            }
            
            if (members.Count == 1)
            {
                VbsMembership mem = members.First();
                
                model.State = ManageVendorAccountState.SingleAccount;
                model.AccountId = mem.AccountId;
                model.AccountName = mem.Name;
                model.HasInspectionCode = mem.HasInspectionCode;
                VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(model.AccountId);
                model.CurrentPermissions = aclModel.GetAllowedAcl();
                model.ViewBsRight = mem.ViewBsRight;
                model.ViewVerifyRight = mem.ViewVerifyRight;
                model.ViewShipRight = mem.ViewShipRight;
                model.ViewBookingRight = mem.ViewBookingRight;
                model.ViewReserveLockRight = mem.ViewReserveLockRight;
                model.ViewUserEvaluateRight = mem.ViewUserEvaluateRight;
                model.ViewProposalRight = mem.ViewProposalRight;
                model.ViewProposalProductRight = mem.ViewProposalProductRight;
                model.ViewShoppingCartManageRight = mem.ViewShoppingCartRight;
                model.ViewAccountManageRight = mem.ViewAccountManageRight;
                model.ViewCustomerServiceRight = mem.ViewCustomerServiceRight;
                model.ViewWmsRight = mem.ViewWmsRight;
                model.ViewToShop = mem.ViewToShop;
                model.ViewToHouse = mem.ViewToHouse;

                var perE = Enum.GetNames(typeof(VendorRole)).ToList();
                VbsRoleCollection rc = mp.VbsRoleGetList(mem.UserId.Value);
                var s = rc.Select(a => a.RoleName).ToList().Intersect(perE);
                VendorRole val;
                Enum.TryParse(s.DefaultIfEmpty("").First(), out val);
                model.VendorRole = (int)val;
                model.AccountIdentity = GetAccountIdentity(model.AccountId);
                //取得自訂帳號
                model.BindAccount = GetBindAccount(model.AccountSearchOption, model.AccountId);
            }

            if (members.Count > 1)
            {
                model.State = ManageVendorAccountState.MultipleAccounts;
                model.SearchedAccounts = members.Select(x => new VendorAccount { AccountId = x.AccountId, AccountName = x.Name }).ToList();
            }

            ViewBag.ShoppingCartEnabled = config.ShoppingCartEnabled;
            ViewBag.SimulateUserName = User.Identity.Name;
            return View(model);
        }

        /// <summary>
        /// 取得綁定帳號
        /// </summary>
        /// <param name="searchOption">查詢條件</param>
        /// <param name="searchExpression">查詢內容值</param>
        /// <returns></returns>
        private string GetBindAccount(ManageVendorSearchOption searchOption, string searchExpression)
        {
            switch (searchOption)
            {
                case ManageVendorSearchOption.AccountId:
                    var baByAccountId = mp.VbsMembershipBindAccountGetByVbsAccountId(searchExpression);
                    if (baByAccountId.IsLoaded)
                    {
                        return VBSFacade.GetVbsBindAccountEmailOrMobileByUserId(baByAccountId.UserId, (VbsBindAccountType)baByAccountId.BindType);
                    }
                    break;
                case ManageVendorSearchOption.AccountName:
                    var baByName = mp.VbsMembershipBindAccountGetByVbsAccountName(searchExpression);
                    if (baByName.IsLoaded)
                    {
                        return VBSFacade.GetVbsBindAccountEmailOrMobileByUserId(baByName.UserId, (VbsBindAccountType)baByName.BindType);
                    }
                    break;
                case ManageVendorSearchOption.BindAccount://自訂帳號
                    return searchExpression;
            }
            return string.Empty;
        }

        /// <summary>
        /// 綁定/解除綁定  自定帳號(利用17life會員登入)
        /// </summary>
        /// <param name="bindAccount">帳號/手機號碼</param>
        /// <param name="isBind">綁訂/解除綁定</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult BindAccount(string bindAccount, string vbsAccountId, bool isBind)
        {
            VbsBindAccountErrorType errorType = VbsBindAccountErrorType.NONE;
            VBSFacade.BindAccount(bindAccount, isBind, vbsAccountId, out errorType);

            return Json(new
            {
                IsSuccess = errorType == VbsBindAccountErrorType.NONE,
                ErrorType = errorType,
            });
        }
        #region 帳號與帳戶明細

        /// <summary>
        /// 帳號與帳戶明細
        /// </summary>
        /// <param name="acct">如有預設帳號就查詢</param>
        /// <returns></returns>
        [Authorize,RolePage]
        public ActionResult ManageVendorDetail()
        {
            Dictionary<string, string> typeList = Enum.GetValues(typeof(ManageVendorSearchOption))
                .Cast<ManageVendorSearchOption>()
                .Where(x => (int)x > 0 && (int)x < 3)
                .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));

            ViewBag.TypeList = new SelectList(typeList, "key", "value");

            Dictionary<string, string> cardType = Enum.GetValues(typeof(PcpCardType))
                .Cast<PcpCardType>()
                .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));

            ViewBag.CardType = new SelectList(cardType, "key", "value");

            return View();
        }

        /// <summary>
        /// 熟客卡查詢
        /// </summary>
        /// <param name="acct">如有預設卡號就查詢</param>
        /// <returns></returns>
        [Authorize,RolePage]        
        public ActionResult SearchRegularsCard(string regularCardNo)
        {
            Dictionary<string, string> typeList = Enum.GetValues(typeof(ManageVendorSearchOption))
                .Cast<ManageVendorSearchOption>()
                .Where(x => (int)x > 0 && (int)x <= 3)
                .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));

            ViewBag.TypeList = new SelectList(typeList, "key", "value", ManageVendorSearchOption.RegularCardNo);
            
            return View(new ManageVendorModel());
        }
        
        /// <summary>
        /// 上傳商家自建會員
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        [Authorize, RolePage]
        public JsonResult UploadSellerMembers()
        {
            //HttpPostedFileBase FileUpload
            var result = new ApiResult() { Code = ApiResultCode.Success };

            int cardId;
            var fileUpload = Request.Files[0];

            if (fileUpload != null && fileUpload.ContentLength > 0 && int.TryParse(Request.Form["cardId"], out cardId))
            {

                string msg;
                var isSuccess = VBSFacade.TryUploadSellerMember(cardId, fileUpload, User.Identity.Name, out msg);

                if (!isSuccess)
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = msg;
                }
            }
            else
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "不符合的檔案格式或內容。";
            }

            return Json(result);
        }

        /// <summary>
        /// 熟客券查詢
        /// </summary>
        /// <param name="acct">如有預設帳號就查詢</param>
        /// <returns></returns>
        [Authorize]
        public ActionResult SearchRegularsTicket(string acct)
        {
            Dictionary<string, string> typeList = Enum.GetValues(typeof(ManageVendorSearchOption))
                .Cast<ManageVendorSearchOption>()
                .Where(x => (int)x > 0 && (int)x < 3)
                .ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));

            ViewBag.TypeList = new SelectList(typeList, "key", "value");

            if (string.IsNullOrWhiteSpace(acct))
            {
                //return Json(new { result = (int)ApiResultCode.Success, data = "資料有誤，請與技術聯絡" });
            }

            var model = VbsMemberUtility.GetManageVender((int)ManageVendorSearchOption.AccountId, acct);

            return View(model);
        }

        /// <summary>
        /// 熟客卡查詢-優惠內容
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardQuery(int? queryVersion)
        {
            //新版用SellerGuid作為條件
            Guid sellerGuid;
            List<PCPRegularsCard> data = new List<PCPRegularsCard>();
            int cardGroupId = 0;
            int cardVersionId = 0;

            if (Guid.TryParse(Request.Form["sellerGuid"], out sellerGuid))
            {
                cardGroupId = pcp.MembershipCardGroupGetBySellerGuid(sellerGuid).Id;
                var membershopCardList = pcp.MembershipCardGetByGroupId(cardGroupId);
                if(!membershopCardList.Any()) { return Json(new { result = (int)ApiResultCode.Error, Message = "此賣家尚未開卡，請至 帳號與帳戶明細 進行開卡作業。" }); }
                cardVersionId = membershopCardList.OrderByDescending(x => x.VersionId).FirstOrDefault().VersionId;
            }
            else
            {
                return Json(new { result = (int)ApiResultCode.Error, Message = "資料有誤，請與技術聯絡" });
            }

            //搜尋版本
            //如果沒有帶就是由帳號或是卡號查詢
            //帶特定版號就是查詢那一版
            cardVersionId = queryVersion ?? cardVersionId;

            data = BackendUtility.GetRegularsCardData(sellerGuid, cardGroupId, cardVersionId);
            if (!data.Any()) { return Json(new { result = (int)ApiResultCode.Error, Message = "此賣家尚未開卡，請至 帳號與帳戶明細 進行開卡作業。" }); }

            //所有等級適用店家相同
            var store = BackendUtility.GetRegularsCardStoreData(data.FirstOrDefault().cardGroupId);

            var parentSellerGuid = sp.SellerTreeGetListBySellerGuid(store.Select(x => x.storeGuid).FirstOrDefault()).Select(x=>x.ParentSellerGuid).FirstOrDefault().ToString();
            var tempList = pcp.MembershipCardGroupStoreCanAddedBySellGuid(parentSellerGuid, Convert.ToInt32(data.FirstOrDefault().cardGroupId));
            SelectList storeList = new SelectList(tempList.OrderBy(x => x.SellerId), "Guid", "SellerName");

            return Json(new { result = (int)ApiResultCode.Success, data = data, store = store ,addStore= storeList });
        }

        /// 熟客卡分店新增
        /// <summary/>
        /// <param name="storeKey">StoreGuid</param>
        /// <param name="cardGroupId">cardGroupId</param>
        /// <returns></returns>
        [Authorize, HttpPost]
        public JsonResult StoreAdd(string storeKey, string cardGroupId)
        {
            var result = new ApiResult();
            Guid storeGuid;
            var groupId=0;
            if(!int.TryParse(cardGroupId, out groupId))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "cardGroupId格式錯誤。";
                return Json(result);
            }
            if (!Guid.TryParse(storeKey, out storeGuid))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "StoreGuid格式錯誤。";
                return Json(result);
            }

            if (!BonusFacade.MembershipCardGroupStoreGuidIsExists(groupId, storeGuid))
            {
                //還可使用及未使用的卡
                var mcCol = pcp.MembershipCardGetByGroupId(groupId).Where(x => x.CloseTime > DateTime.Now).GroupBy(x=>x.VersionId);                
                if (!mcCol.Any())
                {
                    result.Code = ApiResultCode.DataNotFound;
                    result.Message = "找不到未到期的卡片資訊。";
                    return Json(result);
                }
                var storeColl = new MembershipCardGroupStoreCollection() { };

                foreach (var allowStore in mcCol)
                {
                    storeColl.Add(new MembershipCardGroupStore { CardGroupId = groupId, StoreGuid = storeGuid });
                }
                pcp.MembershipCardGroupStoreSet(storeColl);
            }
            else
            {
                result.Code = ApiResultCode.DataIsExist;
                result.Message = "此分店已存在。";
                return Json(result);
            }

            var store = BackendUtility.GetRegularsCardStoreData(cardGroupId);
            return Json(new { result = (int)ApiResultCode.Success, store = store });
        }

        /// 熟客卡分店刪除
        /// <summary/>
        /// <param name="storeKey">StoreGuid</param>
        /// <param name="cardGroupId">cardGroupId</param>
        /// <returns></returns>
        [Authorize, HttpPost]
        public JsonResult StoreRemove(string storeKey,string cardGroupId)
        {
            var result = new ApiResult();
            var storeGuid = Guid.Empty;

            if (!Guid.TryParse(storeKey, out storeGuid))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "StoreGuid格式錯誤。";
                return Json(result);
            }
      
            pcp.MembershipCardGroupStoreDelByStoreGuid(storeGuid);
            var store = BackendUtility.GetRegularsCardStoreData(cardGroupId);
            return Json(new { result = (int)ApiResultCode.Success, store = store });
        }

        [HttpPost]
        public JsonResult ImageUpload()
        {
            var result = new ApiResult();
            
            int GroupId,UserId,ImageSequence;
            string ImageData;
            PcpImageType imgType;

            try
            {
                int.TryParse(Request.Form["GroupId"], out GroupId);

                var member = mp.MemberGet(User.Identity.Name);
                UserId = member.UniqueId;

                int.TryParse(Request.Form["ImageSequence"], out ImageSequence);
                ImageData = Request.Form["ImageData"];
                int tmp;
                int.TryParse(Request.Form["Type"], out tmp);
                imgType = (PcpImageType)tmp;
            }
            catch (Exception)
            {
                result.Code = ApiResultCode.InputError;
                return Json(result);
            }


            byte[] imageData = null;
            try
            {
                imageData = System.Convert.FromBase64String(ImageData);
            }
            catch (System.ArgumentNullException)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "Base 64 string is null.";
                return Json(result);
                //return result;
            }
            catch (System.FormatException)
            {
                result.Code = ApiResultCode.InputError;
                result.Message = "Base 64 string length is not 4 or is not an even multiple of 4.";
                //return result;
                return Json(result);
            }

            var success = false;

            using (MemoryStream ms = new MemoryStream(imageData))
            {
                using (Image img = Image.FromStream(ms))
                {
                    success = BackendUtility.UploadPcpImage(GroupId, UserId, img, ImageSequence, imgType);
                }
            }

            if (success)
            {
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.SaveFail;
            }

            return Json(result);
        }

        [Authorize,HttpPost]
        public JsonResult VendorDetailDataImgSet(string mode="")
        {
            //string vbsAccount = GetVbsAccountId();
            var result = new ApiResult() { Code = ApiResultCode.Success };

            if (string.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                result.Code = ApiResultCode.UserNoSignIn;
                return Json(result);
            }

            int groupId;
            int.TryParse(Request.Form["GroupId"], out groupId);

            if (groupId == 0)
            {
                result.Code = ApiResultCode.UserNoSignIn;
                result.Message = "資料有誤，請與技術部聯絡。";
                return Json(result);
            }

            int imageType;
            int.TryParse(Request.Form["ImageType"], out imageType);

            if (!Enum.IsDefined(typeof(PcpImageType), imageType))
            {
                result.Code = ApiResultCode.UserNoSignIn;
                result.Message = "資料有誤，請與技術部聯絡。";
                return Json(result);
            }

            int seq;
            int.TryParse(Request.Form["Sequence"], out seq);

            var member = mp.MemberGet(User.Identity.Name);

            var d = new VendorDetailData
            {
                Files = Request.Files,
                ModifyUserId = member.UniqueId,
                GroupId = groupId,
                ImageType = imageType,
                Sequence = seq,
            };

            switch (mode)
            {
                case"":
                    if (!BackendUtility.SetVendorDetailDataImg(d))
                    {
                        result.Code = ApiResultCode.Error;
                    }
                    break;
                case"del":
                    if (!BackendUtility.SetDelVendorDetailDataImg(d))
                    {
                        result.Code = ApiResultCode.Error;
                    }
                    break;
            }
            return Json(result);
        }

        [Authorize, HttpPost]
        public JsonResult VendorDetailSellerIntroSet()
        {
            var result = new ApiResult();

            string sellerUserIdstr = GetVbsAccountId(returnAccountId: false);
            int sellerUserId, cardGroupId;
            if (!int.TryParse(sellerUserIdstr, out sellerUserId))
            {
                result.Code = ApiResultCode.InputError;
                return Json(result);
            }

            if (!int.TryParse(Request.Form["cardGroupId"], out cardGroupId))
            {
                result.Code = ApiResultCode.InputError;
                return Json(result);
            }

            var d = new VendorDetailData
            {
                SellerIntro = new JsonSerializer().Deserialize<string[]>(Request.Form["SellerIntro"])
            };

            if (BackendUtility.SellerIntroSet(cardGroupId, sellerUserId, d.SellerIntro.Where(x => !string.IsNullOrWhiteSpace(x)).Select(x=>x).ToArray()))
            {
                result.Code = ApiResultCode.Success;
            }
            else
            {
                result.Code = ApiResultCode.Error;
            }

            return Json(result);
        }

        /// <summary>
        /// 熟客卡查詢-儲存變更
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardSave()
        {
            try
            {
                var original = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PCPRegularsCard>>(Request.Form["original"]);
                var changed = Newtonsoft.Json.JsonConvert.DeserializeObject<List<PCPRegularsCard>>(Request.Form["changed"]);
                if (!changed.Any()) { throw new Exception(); }

                if (BackendUtility.SetRegularsCardData(User.Identity.Name, original, changed))
                {
                    return Json(new { result = 1, data = "更新成功" });
                }
            }
            catch
            {
                return Json(new { result = 0, data = "格式錯誤" });
            }

            return Json(new { result = 0, data = "格式錯誤" });
        }

        /// <summary>
        /// 熟客卡查詢-編輯歷程
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardHistory()
        {

            int cardId;
            int.TryParse(Request.Form["cardId"], out cardId);

            if (cardId == 0)
            {
                return Json(new { result = 0, data = "資料有誤，請與技術聯絡" });
            }

            var data = BackendUtility.GetRegularsCardHistory(cardId);

            return Json(new { result = 0, data = data });
        }

        /// <summary>
        /// 熟客卡查詢-會員名單
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardUserMember()
        {

            int cardId;
            int.TryParse(Request.Form["cardId"], out cardId);

            if (cardId == 0)
            {
                return Json(new { result = 0, data = "資料有誤，請與技術聯絡" });
            }

            var data = BackendUtility.GetRegularsCardUserMember(cardId);

            return Json(new { result = 0, data = data });
        }

        /// <summary>
        /// 熟客卡查詢-會員卡狀態
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardUpdateEnable()
        {

            int cardId;
            int.TryParse(Request.Form["cardId"], out cardId);

            if (cardId == 0)
            {
                return Json(new { result = 0, data = "資料有誤，請與技術聯絡" });
            }

            var data = BackendUtility.SetRegularsCardStatus(User.Identity.Name, cardId);

            return Json(new { result = 1, data = "更新成功。" });
        }

        /// <summary>
        /// 熟客卡作廢
        /// </summary>
        /// <param name="cardId"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsCardUpdateStatus(int cardId)
        {
            if (cardId == 0)
            {
                return Json(new { Success = false, Message = "資料有誤，請與技術聯絡" });
            }
            else
            {
                var result = BackendUtility.SetMemberShipCardStatus(cardId);
                return Json(result ? new { Success = true, Message = "作廢成功。" } : new { Success = false, Message = "作廢失敗。" });
            }
            
        }
        
        /// <summary>
        /// 熟客券查詢（使用於熟客券查詢）
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult RegularsTicketQuery()
        {
            string account = GetVbsAccountId();
            
            var data = BackendUtility.GetRegularsTicketData(account);

            return Json(new { result = (int)ApiResultCode.Success, data = data });
        }

        /// <summary>
        /// 熟客券＆公關券紀錄（使用於帳戶明細）
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult TicketUsageGet(int t)
        {
            string account = GetVbsAccountId();

            if (!Enum.IsDefined(typeof(DiscountCampaignType), t)) { return Json(new { result = (int)ApiResultCode.Error, msg = "錯誤型態" }); }

            var data = BackendUtility.GetTicketUsageData(account, (DiscountCampaignType)t);

            return Json(new { result = (int)ApiResultCode.Success, data = data });
        }
        
        /// <summary>
        /// Vendor Account Management - 1. fuzzy search  2. multiple account selection
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult VendorAccountQuery()
        {
            int type;
            int.TryParse(Request.Form["type"], out type);
            string account = Request.Form["condition"];
            VbsMembershipCollection members = new VbsMembershipCollection();

            switch (type)
            {
                //依熟客卡卡號搜尋
                case (int)ManageVendorSearchOption.RegularCardNo:
                    var cardGroupId = pcp.MembershipCardGet(int.Parse(account)).CardGroupId;
                    var membershipCardGroup = pcp.MembershipCardGroupGet(cardGroupId);
                    var resourceGuids = new List<Guid> { membershipCardGroup.SellerGuid };
                    var resourceAclList = mp.ResourceAclGetListByResourceGuidList(resourceGuids);
                    foreach (var resourceAcl in resourceAclList)
                    {
                        members.AddRange(VbsMemberUtility.GetManageVender((int)ManageVendorSearchOption.AccountId, resourceAcl.AccountId));
                    }
                    break;
                //依照商家帳號或帳號名稱搜尋
                case (int)ManageVendorSearchOption.AccountId:
                case (int)ManageVendorSearchOption.AccountName:
                    members = VbsMemberUtility.GetManageVender(type, account);
                    break;
                default:
                    return Json(new
                    {
                        result = (int)ApiResultCode.Error,
                        message = "請聯絡技術部"
                    });
            }

            var notFind = members.FirstOrDefault();

            if (notFind == null)
            {
                return Json(new
                {
                    result = (int)ApiResultCode.DataNotFound,
                    message = "沒有符合的條件。"
                });
            }

            return Json(new
            {
                result = (int)ApiResultCode.Success,
                count = members.Count(),
                member = members.Select(x => new VendorAccount { AccountId = x.AccountId, AccountName = x.Name }).ToList()
            });
          
        }

        /// <summary>
        /// 依帳號查詢賣家資訊
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public JsonResult VendorSellerInfoQuery(string accountId)
        {
            var resourceAclList = mp.ResourceAclGetListByAccountId(accountId).Where(x => x.ResourceType == (int)ResourceType.Seller || x.ResourceType == (int)ResourceType.Store);
            List<Seller> sellerListInfo = new List<Seller>();

            foreach (var resourceAcl in resourceAclList)
            {
                sellerListInfo.AddRange(sp.GetSellerByGuid(resourceAcl.ResourceGuid));  
            }

            List <SellerInfoModel> sellerInfoModelList = new List<SellerInfoModel>();
            foreach (var sellerInfo in sellerListInfo.Distinct())
            {
                SellerInfoModel sellerInfoModel = new SellerInfoModel
                {
                    Guid = sellerInfo.Guid,
                    SellerBossName = sellerInfo.SellerBossName,
                    SellerEmail = sellerInfo.SellerEmail,
                    SellerId = sellerInfo.SellerId,
                    SellerName = sellerInfo.SellerName,
                    SellerTel = sellerInfo.SellerTel,
                    SellerMobile = sellerInfo.SellerMobile,
                    SellerAddress = sellerInfo.SellerAddress,
                    IsServiceState = pcp.MembershipCardGroupGetBySellerGuid(sellerInfo.Guid).IsLoaded
                };
                sellerInfoModelList.Add(sellerInfoModel);
            }
            return Json(sellerInfoModelList);
        }

        [Authorize]
        [HttpPost]
        public JsonResult GetVendorDetailData()
        {
            Guid sellerGuid;

            if (Guid.TryParse(Request.Form["sellerGuid"], out sellerGuid))
            {
                var data = BackendUtility.VbsCompanyDetailGet(sellerGuid);
                return Json(new { result = (int)ApiResultCode.Success, data });
            }

            return Json(new { result = (int)ApiResultCode.Error });
        }
        
        [Authorize]
        [HttpPost]
        public JsonResult VendorDetailImgDelete()
        {
            int sellerUserId = Convert.ToInt32(GetVbsAccountId(false));
            int imageId;
            PcpImageType imageType;
            try
            {
                int.TryParse(Request.Form["imageId"], out imageId);
                int imgType;
                int.TryParse(Request.Form["imageType"], out imgType);
                                                
                if (Enum.IsDefined(typeof(PcpImageType), imgType))
                {
                    imageType = (PcpImageType)Convert.ToInt16(imgType);
                }
                else
                {
                    imageType = PcpImageType.None;
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            var result = new ApiResult();

            if (VBSFacade.DeleteSellerImage(sellerUserId, User.Identity.Name, imageId, imageType)) { result.Code = ApiResultCode.Success; }
            else {
                result.Code = ApiResultCode.SaveFail;
            }

            return Json(result);
        }
        [Authorize]
        [HttpPost]
        public JsonResult VendorDetailSet()
        {
            //string vbsAccount = GetVbsAccountId(false);            
            int userId;
            if (!int.TryParse(GetVbsAccountId(false), out userId))
            {
                return Json(new { Result = (int)ApiResultCode.Error, Message = "帳號有誤。" }); 
            }

            var data = new VendorDetailData();

            try
            {
                data.SellerGuid = Guid.Parse(Request.Form["SellerGuid"]);
                data.Email = Request.Form["Email"];
                data.Mobile = Request.Form["Mobile"];
                data.InvoiceTitle = Request.Form["InvoiceTitle"];
                data.InvoiceComId = Request.Form["InvoiceComId"];
                data.InvoiceName = Request.Form["InvoiceName"];
                data.InvoiceAddress = Request.Form["InvoiceAddress"];
                data.AccountBankCode = Request.Form["AccountBankCode"];
                data.AccountBranchCode = Request.Form["AccountBranchCode"];
                data.AccountName = Request.Form["AccountName"];
                data.AccountId = Request.Form["AccountId"];
                data.AccountNo = Request.Form["AccountNo"];
                data.UserId = userId.ToString();
                data.SellerName = Request.Form["SellerName"];
                data.InContractWith = Request.Form["InContractWith"];
                data.CategoryJson = Request.Form["CategoryList"];
                data.SellerEmail = Request.Form["SellerEmail"];
                data.SellerMobile = Request.Form["SellerMobile"];
                data.ContractValidDate = string.Format("{0} 23:59:59.997", Request.Form["ContractValidDate"]);
                data.SellerCardType = Convert.ToInt32(Request.Form["CardType"]);
                data.IsPromo = Convert.ToBoolean(Request.Form["IsPromo"]);
                data.IsMemberShipCard = Convert.ToBoolean(Request.Form["IsMemberShipCard"]);
                data.IsCupDeposit = Convert.ToBoolean(Request.Form["IsCupDeposit"]);
                data.IsPointDeposit = Convert.ToBoolean(Request.Form["IsPointDeposit"]);
                data.IsPos = Convert.ToBoolean(Request.Form["IsPos"]);
            }
            catch (Exception)
            {
                return Json(new { Result = (int)ApiResultCode.Error, Message = "資料有誤。" }); 
            }

            bool result = BackendUtility.SetVendorDetail(data, User.Identity.Name);

            if (result)
            {
                return Json(new { Result = (int)ApiResultCode.Success, Message = "儲存成功" });
            }
            else
            {
                return Json(new { Result = (int)ApiResultCode.Error, Message = "儲存失敗" });
            }

            
        }

        [Authorize]
        [HttpPost]
        public JsonResult SuperBonusGet()
        {
            string r = GetVbsAccountId(returnAccountId: false);
            int userId;
            int.TryParse(r, out userId);

            var data = BackendUtility.GetSuperBonusData(userId);

            return Json(new { result = 1, data = data });
        }

        [Authorize]
        [HttpPost]
        public JsonResult FavorPointDataGet()
        {
            string r = GetVbsAccountId(returnAccountId: false);
            int userId;
            int.TryParse(r, out userId);

            var data = BackendUtility.GetPcpPointData(userId, PcpPointType.FavorPoint);
            return Json(new { result = 1, data = data });
        }

        [Authorize]
        [HttpPost]
        public JsonResult RegularsPointDataGet()
        {
            string r = GetVbsAccountId(returnAccountId: false);
            int userId;
            int.TryParse(r, out userId);

            var data = BackendUtility.GetPcpPointData(userId, PcpPointType.RegularsPoint);
            return Json(new { result = 1, data = data });
        }

        private string GetVbsAccountId(bool returnAccountId = true)
        {
            int type;
            int.TryParse(Request.Form["type"], out type);
            string condition = Request.Form["condition"];

            string result = "";
            if (Enum.IsDefined(typeof(ManageVendorSearchOption), type))
            {
                VbsMembershipCollection members = VbsMemberUtility.GetManageVender(type, condition);

                if (members.FirstOrDefault() != null && members.Count() == 1)
                {
                    if (returnAccountId)
                    {
                        result = members.FirstOrDefault().AccountId;
                    }
                    else
                    {
                        result = string.Format("{0}", members.FirstOrDefault().UserId);
                    }
                }
            }
            return result;
        }

        #endregion

        /// <summary>
        /// 異動商家帳號
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public JsonResult ModifyVendorAccount(ManageVendorModel model)
        {
            if (!string.IsNullOrWhiteSpace(model.AccountId))
            {
                string failReason = string.Empty;
                VbsMembership member = mp.VbsMembershipGetByAccountId(model.AccountId);
                if (member != null && member.IsLoaded)
                {
                    if (model.ResetPassword)
                    {
                        failReason +=
                            VbsMemberUtility.AdministrativeChangePassword(model.AccountId, model.Password)
                                ? string.Empty
                                : "密碼變更失敗. ";
                    }

                    if (model.ResetInspectionCode)
                    {
                        failReason +=
                            VbsMemberUtility.AdministrativeChangeInspectionCode(model.AccountId, model.InspectionCode)
                                ? string.Empty
                                : "查帳碼變更失敗. ";
                    }

                    if (!string.Equals(member.Name, model.AccountName)
                        || member.HasInspectionCode != model.HasInspectionCode
                        || member.ViewBsRight != model.ViewBsRight
                        || member.ViewVerifyRight != model.ViewVerifyRight
                        || member.ViewShipRight != model.ViewShipRight
                        || member.ViewBookingRight != model.ViewBookingRight
                        || member.ViewReserveLockRight != model.ViewReserveLockRight
                        || member.ViewUserEvaluateRight != model.ViewUserEvaluateRight
                        || member.ViewProposalRight != model.ViewProposalRight
                        || member.ViewProposalProductRight != model.ViewProposalProductRight
                        || member.ViewShoppingCartRight != model.ViewShoppingCartManageRight
                        || member.ViewAccountManageRight != model.ViewAccountManageRight
                        || member.ViewCustomerServiceRight != model.ViewCustomerServiceRight
                        || member.ViewWmsRight != model.ViewWmsRight
                        || member.ViewToShop != model.ViewToShop
                        || member.ViewToHouse != model.ViewToHouse
                        )
                    {
                        member.Name = model.AccountName;
                        member.HasInspectionCode = model.HasInspectionCode;
                        member.ViewBsRight = model.ViewBsRight;
                        member.ViewVerifyRight = model.ViewVerifyRight;
                        member.ViewShipRight = model.ViewShipRight;
                        member.ViewBookingRight = model.ViewBookingRight;
                        member.ViewReserveLockRight = model.ViewReserveLockRight;
                        member.ViewUserEvaluateRight = model.ViewUserEvaluateRight;
                        member.ViewProposalRight = model.ViewProposalRight;
                        member.ViewProposalProductRight = model.ViewProposalProductRight;
                        member.ViewShoppingCartRight = model.ViewShoppingCartManageRight;
                        member.ViewAccountManageRight = model.ViewAccountManageRight;
                        member.ViewCustomerServiceRight = model.ViewCustomerServiceRight;
                        member.ViewWmsRight = model.ViewWmsRight;
                        member.ViewToShop = model.ViewToShop;
                        member.ViewToHouse = model.ViewToHouse;
                        mp.VbsMembershipSet(member);
                    }

                    #region 熟客系統角色與權限
                    var scopes = model.PermissionScopes == string.Empty ? new List<int>() : model.PermissionScopes.Split(',').Select(int.Parse).ToList();

                    //角色
                    failReason = EditVendorRole(model.VendorRole.ToString(), model.AccountId, member.UserId.Value);
                    //權限
                    SetMembershipPermission(member, scopes);
                    #endregion
                    
                    //帳號綁定身分/權限 清除
                    List<string> aclIdLists = model.ClearAclIds.Split(',').ToList();
                    var aclIds = new List<int>();
                    foreach (var id in aclIdLists)
                    {
                        int aclId;
                        if (int.TryParse(id, out aclId))
                        {
                            aclIds.Add(aclId);
                        }
                    }
                    if (aclIds.Any())
                    { 
                        var acls = mp.ResourceAclGetListByIds(aclIds);
                        SellerFacade.ResourceAclDelete(acls, "身分權限清除", User.Identity.Name);
                    }

                    var result = new
                                     {
                                         AccountId = model.AccountId,
                                         IsSuccess = string.Equals(string.Empty, failReason),
                                         Message = failReason
                                     };

                    return Json(result);
                }
            }

            return Json(new
                            {
                                AccountId = model.AccountId,
                                IsSuccess = false,
                                Message = "找不到帳號."
                            });
        }

        private string EditVendorRole(string vendorRole, string accountId, int? userIdN = null)
        {
            try
            {
                var userId = userIdN ?? mp.VbsMembershipGetByAccountId(accountId).UserId.Value;

                mp.VbsRoleMemberDelete(userId, Enum.GetNames(typeof(VendorRole)).ToArray<string>());

                int enumValue;
                if (int.TryParse(vendorRole, out enumValue))
                {
                    if (Enum.IsDefined(typeof(VendorRole), enumValue))
                    {
                        VbsRole vbsRole = mp.VbsRoleGet(((VendorRole)enumValue).ToString("g"));
                        if (vbsRole.IsLoaded)
                        {
                            mp.VbsRoleMemberSet(new VbsRoleMember
                            {
                                RoleGuid = vbsRole.Guid,
                                UserId = userId
                            });
                        }
                    }
                }

                return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Format("設定熟客系統角色時發生錯誤：{0}", ex.Message);
            }
        }

        private void SetMembershipPermission(VbsMembership vbsMembership, List<int> permissionScopeList) {
            vbs.VbsMembershipPermissionDelAll(vbsMembership.Id);
            if (permissionScopeList.Any())
            {
                VbsMembershipPermissionCollection permissionCol = new VbsMembershipPermissionCollection();
                foreach (var scope in permissionScopeList)
                {
                    permissionCol.Add(new VbsMembershipPermission { VbsMembershipId = vbsMembership.Id, Scope = scope, CreateTime = DateTime.Now, UserId = vbsMembership.UserId ?? 0 });
                }

                vbs.VbsMembershipPermissionSet(permissionCol);
            }
        }
        #endregion 商家帳號管理

        #region 商家權限管理

        /// <summary>
        /// 商家帳號可新增的權限
        /// </summary>
        /// <param name="acct">query string standing for AccountId</param>
        /// <param name="sc">query string standing for SearchCondition</param>
        /// <param name="se">query string standing for SearchExpression</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ManageVendorAcl(string acct, PermissionSearchCondition sc = PermissionSearchCondition.Unknown, string se = null)
        {
            ManageVendorAclModel model = new ManageVendorAclModel();

            model.AccountId = acct;
            model.SearchCondition = sc;
            model.SearchExpression = se;

            VbsMembership mem = mp.VbsMembershipGetByAccountId(acct);
            if (mem != null && mem.IsLoaded)
            {
                model.IsAccountExist = mem.IsLoaded;
                model.AccountName = mem.Name;
            }

            if (model.IsAccountExist && sc != PermissionSearchCondition.Unknown && !string.IsNullOrWhiteSpace(se))
            {
                VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(model.AccountId);
                model.Permissions = aclModel.FindAllowableAclHierarchy(sc, se.Trim());
            }
            else
            {
                model.Permissions = new List<VbsVendorAce>();
            }

            return View(model);
        }

        [HttpPost]
        public JsonResult ManageVendorAcl(PermissionRequestModel model)
        {
            ResourceAclCollection modifications = new ResourceAclCollection();
            List<string> allows = model.Allows.Split(',').ToList();
            List<string> denies = model.Denies.Split(',').ToList();
            List<string> noDenies = model.NoDenies.Split(',').ToList();

            foreach (string allow in allows)
            {
                string[] permission = allow.Split('|');
                int resourceType;
                Guid resourceGuid;
                if (int.TryParse(permission[0], out resourceType) && Guid.TryParse(permission[1], out resourceGuid))
                {
                    ResourceAcl allowResource;
                    if (TryGetResourceAcl(model.AccountId, resourceGuid, resourceType, ModificationResourceMode.Allow, out allowResource))
                    {
                        if (!modifications.Any(x => x.ResourceGuid == allowResource.ResourceGuid && x.ResourceType == allowResource.ResourceType
                                                 && x.PermissionType == allowResource.PermissionType && x.PermissionSetting == allowResource.PermissionSetting))
                        { 
                            modifications.Add(allowResource);
                        }
                    }
                }
            }

            foreach (string deny in denies)
            {
                string[] permission = deny.Split('|');
                int resourceType;
                Guid resourceGuid;
                if (int.TryParse(permission[0], out resourceType) && Guid.TryParse(permission[1], out resourceGuid))
                {
                    ResourceAcl denyResource;
                    if (TryGetResourceAcl(model.AccountId, resourceGuid, resourceType, ModificationResourceMode.Deny, out denyResource))
                    {
                        if (!modifications.Any(x => x.ResourceGuid == denyResource.ResourceGuid && x.ResourceType == denyResource.ResourceType
                                                 && x.PermissionType == denyResource.PermissionType && x.PermissionSetting == denyResource.PermissionSetting))
                        {
                            modifications.Add(denyResource);
                        }
                    }
                }
            }

            foreach (string noDeny in noDenies)
            {
                string[] permission = noDeny.Split('|');
                int resourceType;
                Guid resourceGuid;
                if (int.TryParse(permission[0], out resourceType) && Guid.TryParse(permission[1], out resourceGuid))
                {
                    ResourceAcl dummy;
                    TryGetResourceAcl(model.AccountId, resourceGuid, resourceType, ModificationResourceMode.NoDeny, out dummy);
                }
            }

            SellerFacade.ResourceAclSetList(modifications, "權限異動", User.Identity.Name);
            return Json(new { IsFinished = true });
        }

        private bool TryGetResourceAcl(string accountId, Guid resourceGuid, int resourceType, ModificationResourceMode mode, out ResourceAcl result)
        {
            ResourceAcl resource = mp.ResourceAclGet(accountId, ResourceAclAccountType.VendorBillingSystem, resourceGuid, (ResourceType)resourceType);
            if (resource.IsLoaded)
            {
                //return false if no modification is needed
                switch (mode)
                {
                    case ModificationResourceMode.Allow:
                        if (((PermissionType)resource.PermissionType).HasFlag(PermissionType.Read)
                            && ((PermissionType)resource.PermissionSetting).HasFlag(PermissionType.Read))
                        {
                            result = null;
                            return false;
                        }
                        break;

                    case ModificationResourceMode.Deny:
                        if (((PermissionType)resource.PermissionType).HasFlag(PermissionType.Read)
                            && !((PermissionType)resource.PermissionSetting).HasFlag(PermissionType.Read))
                        {
                            result = null;
                            return false;
                        }
                        break;

                    case ModificationResourceMode.NoDeny:
                        if (((PermissionType)resource.PermissionType).HasFlag(PermissionType.Read)
                            && ((PermissionType)resource.PermissionSetting).HasFlag(PermissionType.Read))
                        {
                            //resource is allow: no change needed
                            result = null;
                            return false;
                        }

                        if (!((PermissionType)resource.PermissionType).HasFlag(PermissionType.Read))
                        {
                            //no permission set on this resource: no change needed
                            //無權限改為直接清除 並加註異動紀錄
                            SellerFacade.ResourceAclDelete(new ResourceAclCollection { resource }, "權限清除", User.Identity.Name);
                            result = null;
                            return false;
                        }
                        break;
                }

                switch (mode)
                {
                    case ModificationResourceMode.Allow:
                        resource.PermissionType = (int)(((PermissionType)resource.PermissionType) | PermissionType.Read);
                        resource.PermissionSetting = (int)(((PermissionType)resource.PermissionSetting) | PermissionType.Read);
                        break;

                    case ModificationResourceMode.Deny:
                        resource.PermissionType = (int)(((PermissionType)resource.PermissionType) | PermissionType.Read);
                        resource.PermissionSetting = (int)(((PermissionType)resource.PermissionSetting) & ~PermissionType.Read);
                        break;

                    case ModificationResourceMode.NoDeny:
                        if (((PermissionType)resource.PermissionType).HasFlag(PermissionType.Read)
                            && !((PermissionType)resource.PermissionSetting).HasFlag(PermissionType.Read))
                        {
                            //resource is deny -> remove this permission
                            //無權限改為直接清除 並加註異動紀錄
                            SellerFacade.ResourceAclDelete(new ResourceAclCollection { resource }, "權限清除",User.Identity.Name);
                            result = null;
                            return false;
                        }
                        break;
                }
            }
            else
            {
                switch (mode)
                {
                    case ModificationResourceMode.Allow:
                        resource.PermissionSetting = (int)PermissionType.Read;
                        break;

                    case ModificationResourceMode.Deny:
                        resource.PermissionSetting = 0;
                        break;

                    case ModificationResourceMode.NoDeny:
                        result = null;
                        return false;
                }
                resource.AccountId = accountId;
                resource.AccountType = (int)ResourceAclAccountType.VendorBillingSystem;
                resource.ResourceGuid = resourceGuid;
                resource.ResourceType = resourceType;
                resource.PermissionType = (int)PermissionType.Read;
            }

            result = resource;
            return true;
        }

        private IEnumerable<AccountIdentity> GetAccountIdentity(string accountId)
        {
            var acls = mp.ResourceAclGetListByAccountId(accountId).OrderBy(x => x.ResourceType);
            var sellerGuids = acls.Where(x => x.ResourceType == (int)ObjectResourceType.Seller || x.ResourceType == (int)ObjectResourceType.Store)
                                  .Select(x => x.ResourceGuid);
            var storeGuids = acls.Where(x => x.ResourceType == (int)ObjectResourceType.Store)
                                .Select(x => x.ResourceGuid).ToList();
            var dealGuids = acls.Where(x => x.ResourceType == (int)ObjectResourceType.HiDealProduct || x.ResourceType == (int)ObjectResourceType.BusinessHour)
                                .Select(x => x.ResourceGuid);
            var resourceGuids = acls.Where(x => x.ResourceType == (int)ObjectResourceType.PponStore || x.ResourceType == (int)ObjectResourceType.HiDealProductStore)
                                  .Select(x => x.ResourceGuid).ToList();
            var sellerInfo = sp.SellerGetList(sellerGuids)
                                .ToDictionary(x => x.Guid, x => string.Format("{0} {1}", x.SellerId, x.SellerName));
            var storeInfo = sp.StoreGetListByStoreGuidList(storeGuids)                                
                                .ToDictionary(x => x.Guid, x => x.StoreName);
            var dealInfo = pp.ViewVbsToHouseDealGetListByProductGuid(dealGuids)
                                .GroupBy(x => x.ProductGuid)
                                .ToDictionary(x => x.Key, x => string.Format("[{0}] {1}", x.First().ProductId, x.First().ProductName));
            var dealStoreInfo = mp.ResourceAclGetDealStorePermissionInfoByAccountId(accountId).AsEnumerable()
                                    .Where(x => resourceGuids.Contains(x.Field<Guid>("resource_guid")))
                                    .GroupBy(x => x.Field<Guid>("resource_guid"))
                                    .ToDictionary(x => x.Key, x => string.Format("[{0}] {1} - {2}", x.First().Field<int>("product_id"), x.First().Field<string>("product_name"), x.First().Field<string>("store_name")));
            return acls.Select(x => 
                    {
                        var identityDesc = string.Empty;
                        var resourceTypeDesc = string.Empty;

                        switch ((ObjectResourceType)x.ResourceType)
                        {
                            case ObjectResourceType.Seller:
                                resourceTypeDesc = "賣家";
                                if (sellerInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = sellerInfo[x.ResourceGuid];
                                }
                                break;
                            case ObjectResourceType.Store:
                                resourceTypeDesc = "分店";
                                if (sellerInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = sellerInfo[x.ResourceGuid];
                                }
                                else if (storeInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = storeInfo[x.ResourceGuid];
                                }
                                break;
                            case ObjectResourceType.HiDealProduct:
                                resourceTypeDesc = "檔次(品生活)";
                                if (dealInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = dealInfo[x.ResourceGuid];
                                }
                                break;
                            case ObjectResourceType.BusinessHour:
                                resourceTypeDesc = "檔次(好康)";
                                if (dealInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = dealInfo[x.ResourceGuid];
                                }
                                break;
                            case ObjectResourceType.HiDealProductStore:
                                resourceTypeDesc = "檔次分店(品生活)";
                                if (dealStoreInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = dealStoreInfo[x.ResourceGuid];
                                } 
                                break;
                            case ObjectResourceType.PponStore:
                                resourceTypeDesc = "檔次分店(P好康)";
                                if (dealStoreInfo.ContainsKey(x.ResourceGuid))
                                {
                                    identityDesc = dealStoreInfo[x.ResourceGuid];
                                }
                                break;
                            default:
                                break;
                        }
                        return new AccountIdentity
                        {
                            ResourceAclId = x.Id,
                            IdentityDesc = identityDesc,
                            ResourceTypeDesc = resourceTypeDesc,
                            IsAllow = ((PermissionType)x.PermissionSetting).HasFlag(PermissionType.Read) &&
                                      ((PermissionType)x.PermissionType).HasFlag(PermissionType.Read)
                        };
                    });

        }

        private enum ModificationResourceMode
        {
            Unknown,
            Allow,
            Deny,
            NoDeny
        }

        

        #endregion 商家權限管理

        #region 物流商管理

        public JsonResult ShipCompanySearch(string term)
        {
            ShipCompanyCollection scc = op.ShipCompanyGetList(null);
            string[] arr = ((System.Collections.IEnumerable)scc).Cast<ShipCompany>().Select(x => x.ShipCompanyName).ToArray();
            return this.Json(arr.Where(t => t.Contains(term)), JsonRequestBehavior.AllowGet);
        }

        [RolePage]
        public ActionResult ShipCompanyManage([Bind(Prefix = "search")] string searchValue, int page = 1)
        {
            ViewBag.ShipCompanyList = ShipCompanyFacade.GetViewShipCompanyList(ShipCompany.Columns.ShipCompanyName, searchValue, true, page);
            return View();
        }

        [RolePage]
        [HttpGet]
        public ActionResult ShipCompanyNew(int? id)
        {

            if (id == null)
            {
                ViewData.Model = new ShipCompany();
                return View();
            }
            else
            {
                ShipCompanyCollection scc = op.ShipCompanyGetList(id.Value);
                if (scc.Count <= 0)
                {
                    return this.Content("查無符合條件的資料。");
                }
                return View(scc.FirstOrDefault());
            }
        }

        [HttpPost]
        public ActionResult ShipCompanyNew(ShipCompany sc)
        {
            if (string.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                return this.Content("尚未登入!");
            }
            string userName = this.HttpContext.User.Identity.Name;

            op.ShipCompanySet(new ShipCompany()
            {
                Id = sc.Id,
                Sequence = sc.Sequence,
                ShipCompanyName = sc.ShipCompanyName,
                ShipWebsite = sc.ShipWebsite,
                ServiceTel = sc.ServiceTel,
                Status = sc.Status,
                Creator = userName
            });
            return RedirectToAction("ShipCompanyManage");
        }

        #endregion        
    }
}
﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using log4net;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class VendorBillingSystemController : BaseController
    {
        #region Property

        private static IPponProvider _pp;
        private static IVerificationProvider _vp;
        private static IMemberProvider _mp;
        private static ISellerProvider _sp;
        private static IHiDealProvider _hp;
        private static IAccountingProvider _ap;
        private static ISysConfProvider config;
        private static IOrderProvider _op;
        private static IBookingSystemProvider _bp;
        private static IHumanProvider _hmp;
        private static Core.Interface.IVbsProvider _vbs;
        private static VbsMembershipProvider membership;
        private static Core.IServiceProvider _service;
        private static IWmsProvider _wp;
        private static ILog logger = LogManager.GetLogger("VendorBillingSystemController");

        static VendorBillingSystemController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _hmp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            _vbs = ProviderFactory.Instance().GetProvider<Core.Interface.IVbsProvider>();
            _service = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            _wp = ProviderFactory.Instance().GetProvider<Core.IWmsProvider>();
            membership = System.Web.Security.Membership.Providers["VbsMembershipProvider"] as VbsMembershipProvider;
            if (membership == null)
            {
                membership = new VbsMembershipProvider();
            }
        }

        #endregion Property
               
        #region Action

        #region 商家系統Layout
        [ChildActionOnly]
        public ActionResult VbsTop()
        {
            string[] menu = new string[3];
            string deliveryType = string.Empty;
            string paramD = Request.Params["d"];

            if (paramD == null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), Session[VbsSession.DeliveryType.ToString()] != null ? int.Parse(Session[VbsSession.DeliveryType.ToString()].ToString()) : 0);
            }
            else if (paramD != ((int)DeliveryType.ToShop).ToString() && paramD != ((int)DeliveryType.ToHouse).ToString() && Session[VbsSession.DeliveryType.ToString()] == null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), 0);
            }
            else if (paramD != ((int)DeliveryType.ToShop).ToString() && paramD != ((int)DeliveryType.ToHouse).ToString() && Session[VbsSession.DeliveryType.ToString()] != null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), int.Parse(Session[VbsSession.DeliveryType.ToString()].ToString()));
            }
            else
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), int.Parse(paramD));


            List<SelectListItem> deliveryTypeList = new List<SelectListItem>();
            SelectListItem selectedItem = new SelectListItem();
            if (VbsCurrent.ViewVbsRight.ToShop || VbsCurrent.ViewVbsRight.ToHouse)
            {
                //add
                if (VbsCurrent.ViewVbsRight.ToShop)
                {
                    deliveryTypeList.Add(new SelectListItem() { Text = "在地憑證", Value = "1", Selected = false });
                }
                if (VbsCurrent.ViewVbsRight.ToHouse && Session[VbsSession.VbsAccountType.ToString()].EqualsNone(VbsMembershipAccountType.DemoUser))
                {
                    deliveryTypeList.Add(new SelectListItem() { Text = "宅配商品", Value = "2", Selected = false });
                }
                //selected
                if (deliveryTypeList.Count == 1)
                {
                    if (VbsCurrent.ViewVbsRight.ToShop)
                    {
                        deliveryType = "1";
                    }
                    if (VbsCurrent.ViewVbsRight.ToHouse && Session[VbsSession.VbsAccountType.ToString()].EqualsNone(VbsMembershipAccountType.DemoUser))
                    {
                        deliveryType = "2";
                    }
                }
                else
                {
                    deliveryType = menu[0];
                }
                Session[VbsSession.DeliveryType.ToString()] = deliveryType;
                selectedItem = deliveryTypeList.Where(x => x.Value == deliveryType).First();
                selectedItem.Selected = true;
            }
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.VbsRight = Session[VbsSession.ViewVbsRight.ToString()];
            ViewBag.DeliveryType = deliveryType;
            ViewBag.deliveryTypeList = deliveryTypeList;
            
            

            return PartialView("_VbsTop");
        }

        public ActionResult VbsMenu()
        {
            string[] menu = new string[3];
            string deliveryType = string.Empty;
            string paramD = Request.Params["d"];

            if (paramD == null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), Session[VbsSession.DeliveryType.ToString()] != null ? int.Parse(Session[VbsSession.DeliveryType.ToString()].ToString()) : 0);
            }
            else if (paramD != ((int)DeliveryType.ToShop).ToString() && paramD != ((int)DeliveryType.ToHouse).ToString() && Session[VbsSession.DeliveryType.ToString()] == null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), 0);
            }
            else if (paramD != ((int)DeliveryType.ToShop).ToString() && paramD != ((int)DeliveryType.ToHouse).ToString() && Session[VbsSession.DeliveryType.ToString()] != null)
            {
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), int.Parse(Session[VbsSession.DeliveryType.ToString()].ToString()));
            }
            else
                menu = VBSFacade.GetVbsMenuByPageName(GetVbsFileName(), int.Parse(paramD));

            if (Session[VbsSession.DeliveryType.ToString()] != null)
            {
                if (string.Compare(menu[0], Session[VbsSession.DeliveryType.ToString()].ToString(), true) == 1)
                {
                    //menu[0]>session
                    deliveryType = Session[VbsSession.DeliveryType.ToString()].ToString();
                }
                else
                {
                    deliveryType = menu[0];
                }
            }
            else
            {
                deliveryType = menu[0];
            }
            //商家Guid
            int Count = 0;
            var vbsSellers = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId);
            if (vbsSellers.Count() > 0)
            {
                string sellerGuids = string.Join("','", vbsSellers
                .Select(x => Convert.ToString(x.ResourceGuid)).Distinct().ToList());
                sellerGuids = "'" + sellerGuids + "'";

                Count = _service.GetCustomerServiceInsideLogCount(sellerGuids);
            }
            else
            {
                Count = 0;
            }


            #region 7-11週期不一致申請
            string sid = "";
            var sids = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId).Where(x => x.ResourceType == (int)ObjectResourceType.Seller).Select(x => x.ResourceGuid).ToList();

            Seller seller = new Seller();
            ViewVbsInstorePickup vvipFamily = new ViewVbsInstorePickup();
            ViewVbsInstorePickup vvipSeven = new ViewVbsInstorePickup();
            foreach (var s in sids)
            {
                var root_guid = SellerFacade.GetRootSellerGuid(s);
                if (!vvipFamily.IsLoaded)
                {
                    vvipFamily = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.FamilyMart) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);
                    if (!vvipFamily.IsLoaded)
                        vvipFamily = new ViewVbsInstorePickup();
                }

                if (!vvipSeven.IsLoaded)
                {
                    vvipSeven = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.SevenEleven) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.SevenEleven);
                    if (!vvipSeven.IsLoaded)
                        vvipSeven = new ViewVbsInstorePickup();
                }

            }
            bool IsCycle = true;
            if (vvipFamily.ReturnCycle == (int)RrturnCycle.Mon)
            {
                IsCycle = false;
            }
            else if (vvipFamily.ReturnCycle == (int)RrturnCycle.Thu && !(vvipFamily.ReturnType == (int)RetrunShip.Kerrytj || vvipFamily.ReturnType == (int)RetrunShip.Hct))
            {
                IsCycle = false;
            }
            else if (vvipFamily.ReturnCycle == (int)RrturnCycle.Wed && !(vvipFamily.ReturnType == (int)RetrunShip.Other || vvipFamily.ReturnType == (int)RetrunShip.SelfPicking))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Tue && vvipFamily.ReturnType != (int)RetrunShip.Uni))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Fri && vvipFamily.ReturnType != (int)RetrunShip.Pelican))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Daily && vvipFamily.ReturnType == (int)RetrunShip.Family))
            {
                IsCycle = false;
            }

            ViewBag.ShowApplyIsp = vvipFamily.IsLoaded && !vvipSeven.IsLoaded && !IsCycle && config.ApplyPopout;
            #endregion

            #region 退換貨
            if (deliveryType == ((int)DeliveryType.ToHouse).ToString())
            {
                int exchangeCount = 0;
                int returnCount = 0;

                if (config.EnabledExchangeProcess)
                {
                    exchangeCount = _op.ViewVbsExchangeOrderListCollectionGet(
                                    ViewVbsExchangeOrderList.Columns.VendorProgressStatus + " <= " + ((int)ExchangeVendorProgressStatus.UnExchangeable).ToString(),
                                    ViewVbsExchangeOrderList.Columns.ProgressStatus + string.Format(" in ({0})", string.Join(",", new int[] { (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.SendToSeller })),
                                    ViewVbsExchangeOrderList.Columns.AccountId + " = " + VbsCurrent.AccountId
                                    ).Count;
                }


                returnCount = _op.ViewVbsReturnOrderListCollectionGet(null, null,
                                                                      new List<int>() { (int)ProgressStatus.Processing },
                                                                      new List<int>() { (int)VendorProgressStatus.Processing, (int)VendorProgressStatus.Retrieving, (int)VendorProgressStatus.Unreturnable },
                                                                      VbsCurrent.AccountId, "", "")
                                                                      .Count;

                ViewBag.ExchangeCount = exchangeCount;
                ViewBag.ReturnCount = returnCount;
            }
            #endregion


            ViewBag.DeliveryType = deliveryType;
            ViewBag.MenuNav = menu[1];
            ViewBag.SubMenuNav = menu[2];
            ViewBag.Count = Count;


            return PartialView("_VbsMenu");
        }

        private string[] GetVbsFileName()
        {
            string[] fileName = new string[2];
            for (var i = Request.Url.Segments.Length - 1; i > 0; i--)
            {
                if (Request.Url.Segments[i].Replace("/", "") == "vbs")
                {
                    fileName[0] = Request.Url.Segments[i + 1].Replace("/", "").ToLower();
                    if (Request.Url.Segments.Length > 3)
                    {
                        fileName[1] = Request.Url.Segments[i + 2].Replace("/", "").ToLower();
                    }
                    else
                    {
                        fileName[1] = null;
                    }
                    break;
                }
            }
            return fileName;
        }
        #endregion

        #region 登入/登出

        [HttpGet]
        public ActionResult Login()
        {
            bool isLogin = CheckCookieOrSessionExists();


            //若在登入狀態輸入網址Login頁面 需導入登入後首頁
            if (isLogin.Equals(true))
            {
                if (Session[VbsSession.Is17LifeEmployee.ToString()] != null)
                {
                    return RedirectToAction("RedirectToActionAfterLogin");
                }
            }
            
            ViewBag.IsEnableVbsBindAccount = config.IsEnableVbsBindAccount;
            ViewBag.UserName = "";
            ViewBag.Is17LifeEmployee = "";
            ViewBag.IsAuth = true;
            return View();
        }

        public ActionResult Logout()
        {
            HttpCookie cookie = CookieManager.NewCookie(".ASPXAUTH", "");
            cookie.Path = VbsMemberUtility.GetVbsAuthCookiePath();
            cookie.Expires = DateTime.Now;
            Response.Cookies.Add(cookie);
            Session.Remove(VbsSession.UserId.ToString());
            Session.Remove(VbsSession.UniqueId.ToString());
            Session.Remove(VbsSession.Is17LifeEmployee.ToString());
            Session.Remove(VbsSession.InspectionRight.ToString());
            Session.Remove(VbsSession.DealCount.ToString());
            Session.Remove(VbsSession.StoreCount.ToString());
            Session.Remove(VbsSession.IsTemporaryLogin.ToString());
            Session.Remove(VbsSession.TemporaryLoginDate.ToString());
            Session.Remove(VbsSession.DeliveryType.ToString());
            Session.Remove(VbsSession.IsInstorePickup.ToString());
            Session.Remove(VbsSession.VbsShipDealInfos.ToString());
            Session.Remove(VbsSession.LoginBy17LifeSimulate.ToString());
            Session.Remove(VbsSession.LoginBy17LifeSimulateUserName.ToString());

            Session.Remove("OrderInfos");
            Session.Remove("DealId");

            #region App熟客卡相關
            Session.Remove(VbsSession.SellerAppApiToken.ToString());
            Session.Remove(VbsSession.MembershipCardGroupId.ToString());
            Session.Remove("CardSellerGuid");
            Session.Remove(VbsSession.VbsMembershipPermission.ToString());
            #endregion
            Session.Remove(VbsSession.VbsRole.ToString());

            return RedirectToAction("Login");
        }

        #region 忘記密碼
        [HttpGet]
        public ActionResult ForgetPassword()
        {
            ViewBag.SiteUrl = config.SiteUrl;
            ViewBag.SessionId = LkSiteSession.CaptchaForgetPassword.ToString();
            return View();
        }

        [HttpPost]
        public ActionResult SendForgetMail(string accountId, string encryEmail)
        {
            bool flag = false;
            string message = "";
            try
            {
                string decryptEmail = Helper.Decrypt(encryEmail);
                if (CheckVbsContactEmail(decryptEmail) && !string.IsNullOrEmpty(accountId))
                {
                    //檢查密碼是否屬於該商家的聯絡人
                    Dictionary<string, string> contacts = GetVbsContacts(accountId);
                    if (!contacts.ContainsKey(encryEmail))
                    {
                        throw new Exception("email不存在");
                    }

                    //產生新密碼
                    string[] authPair = MemberFacade.GenEmailAuthCode(9, 9);
                    string applyPassword = authPair[0];
                    VbsMembershipPasswordFormat passwordFormat;
                    string encryApplyPassword = membership.GetFormattedPassword(applyPassword, out passwordFormat);

                    Guid pwdGuid = Guid.NewGuid();

                    //回寫密碼資料
                    VbsMembership mem = _mp.VbsMembershipGetByAccountId(accountId);
                    if (mem.IsLoaded)
                    {
                        mem.TemporaryPassword = encryApplyPassword;
                        mem.TemporaryPasswordDate = DateTime.Now;
                        _mp.VbsMembershipSet(mem);

                        //紀錄Log
                        VbsAccountApplyPasswordLog log = new VbsAccountApplyPasswordLog()
                        {
                            Guid = pwdGuid,
                            AccountId = accountId,
                            ApplyPassword = encryApplyPassword,
                            ApplyTime = DateTime.Now,
                            SourceIp = Helper.GetClientIP(),
                            Host = System.Net.Dns.GetHostName(),
                            Memo = decryptEmail
                        };
                        _mp.VbsAccountApplyPasswordLogSet(log);

                        //發送信件
                        List<string> mails = SplitEmail(decryptEmail);
                        foreach (string mail in mails)
                        {
                            SendForgetPasswordMail(mail, accountId, applyPassword);
                        }
                    }
                }
                flag = true;
                message = "";
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }

            return Json(new
            {
                IsSuccess = flag,
                Message = message,
                AccountId = accountId,
                Email = encryEmail
            });
        }

        [HttpPost]
        public ActionResult CheckCaptcha(string accountId, string captchaResponse)
        {
            Dictionary<string, string> mailList = new Dictionary<string, string>();
            int code = (int)ForgetPasswordErrorType.None;
            bool isVbsAccount = IsVbsAccount(accountId);
            string encryEmail = "";

            string sessionId = LkSiteSession.CaptchaForgetPassword.ToString();
            if (Session[sessionId] == null ||
                Session[sessionId].ToString().Equals(captchaResponse, StringComparison.OrdinalIgnoreCase) == false)
            {
                code = (int)ForgetPasswordErrorType.CaptchaResponseInputError;
                return Json(new
                {
                    Code = code
                });
            }
            else
            {
                if (string.IsNullOrEmpty(accountId))
                {
                    code = (int)ForgetPasswordErrorType.AccountInputError;
                    return Json(new
                    {
                        Code = code
                    });
                }
                else
                {
                    if (isVbsAccount)
                    {
                        VbsMembership mem = _mp.VbsMembershipGetByAccountId(accountId);
                        if (!mem.IsLoaded)
                        {
                            code = (int)ForgetPasswordErrorType.AccountInputError;
                            return Json(new
                            {
                                Code = code
                            });
                        }

                        if (code == (int)ForgetPasswordErrorType.None)
                        {
                            mailList = GetVbsContacts(accountId);
                        }
                        if (mailList.Count == 0)
                        {
                            code = (int)ForgetPasswordErrorType.NoContactPerson;
                        }
                    }
                    else
                    {
                        Member mem = MemberFacade.GetMember(accountId);
                        if (!mem.IsLoaded)
                        {
                            code = (int)ForgetPasswordErrorType.AccountInputError;
                            return Json(new
                            {
                                Code = code
                            });
                        }
                        else
                        {
                            IList<SingleSignOnSource> ssoSourceList = GetSingleSignOnSoureceList(mem.UniqueId);
                            if (ssoSourceList.Count == 0)
                            {
                                code = (int)ForgetPasswordErrorType.AccountInputError;
                                return Json(new
                                {
                                    Code = code
                                });
                            }
                            var vbsMsBa = VBSFacade.VbsMembershipBindAccountGet(accountId);
                            if (vbsMsBa.IsLoaded)
                            {
                                if (RegExRules.CheckEmail(accountId))
                                {
                                    encryEmail = System.Web.HttpUtility.UrlEncode(Helper.Encrypt(mem.UserName));
                                }
                                else if (RegExRules.CheckMobile(accountId))
                                {
                                    encryEmail = System.Web.HttpUtility.UrlEncode(Helper.Encrypt(mem.Mobile));
                                }
                                else
                                {
                                    code = (int)ForgetPasswordErrorType.AccountInputError;
                                    return Json(new
                                    {
                                        Code = code
                                    });
                                }
                            }
                            else
                            {
                                //此17Life帳號沒綁定過商家帳號
                                code = (int)ForgetPasswordErrorType.AccountInputError;
                                return Json(new
                                {
                                    Code = code
                                });
                            }
                        }
                    }
                }
            }

            return Json(new
            {
                Code = code,
                IsVbsAccount = isVbsAccount,
                EncryEmail = encryEmail,
                MailList = mailList
            });
        }

        [VbsAuthorize]
        public ActionResult TemporaryLoginPassBy()
        {
            ViewBag.VbsTemporaryPasswordHourLimit = config.VbsTemporaryPasswordHourLimit / 60;
            return View();
        }

        private Dictionary<string, string> GetVbsContacts(string accountId)
        {
            Dictionary<string, string> mailList = new Dictionary<string, string>();
            VbsVendorAclMgmtModel aclModel = new VbsVendorAclMgmtModel(accountId);
            var data = aclModel.GetAllowedDealStores().ToList();
            List<Guid> sid = new List<Guid>();
            sid = data.Distinct().ToList();
            if (sid.Count == 0)
            {
                sid = _mp.ResourceAclGetListByAccountId(accountId)
                                       .Where(x => x.ResourceType == (int)ResourceType.Seller)
                                       .Select(x => x.ResourceGuid).Distinct().ToList();
            }
            if (sid.Count > 0)
            {
                #region 取得聯絡人
                foreach (Guid si in sid)
                {
                    Seller seller = _sp.SellerGet(si);
                    List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                    if (contacts == null)
                    {
                        //一般聯絡人
                        if (!string.IsNullOrEmpty(seller.SellerContactPerson))
                        {
                            if (!string.IsNullOrEmpty(seller.SellerEmail) && RegExRules.CheckEmail(seller.SellerEmail))
                            {
                                if (!mailList.ContainsKey(Helper.Encrypt(seller.SellerEmail)))
                                {
                                    mailList.Add(
                                        Helper.Encrypt(seller.SellerEmail),
                                        GetForgetPasswordEmailString(seller.SellerName, seller.SellerContactPerson, seller.SellerEmail)
                                        );
                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (Seller.MultiContracts contact in contacts)
                        {
                            if (contact.Type == ((int)ProposalContact.Normal).ToString())
                            {
                                if (!string.IsNullOrEmpty(contact.ContactPersonEmail) && CheckVbsContactEmail(contact.ContactPersonEmail))
                                {
                                    if (!mailList.ContainsKey(Helper.Encrypt(contact.ContactPersonEmail)))
                                    {
                                        mailList.Add(
                                               Helper.Encrypt(contact.ContactPersonEmail),
                                               GetForgetPasswordEmailString(seller.SellerName, contact.ContactPersonName, contact.ContactPersonEmail)
                                               );
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion 取得聯絡人
            }
            return mailList;
        }

        private bool CheckVbsContactEmail(string email)
        {
            bool flag = false;
            List<string> mails = SplitEmail(email);
            foreach (string mail in mails)
            {
                if (!string.IsNullOrEmpty(mail))
                {
                    flag = RegExRules.CheckEmail(mail);
                    if (flag == false)
                    {
                        break;
                    }
                }
            }

            return flag;
        }

        private List<string> SplitEmail(string email)
        {
            List<string> mails = new List<string>();
            string[] splitWords = { ";", ",", "/", " " };
            string splitKey = string.Empty;

            foreach (string w in splitWords)
            {
                string[] mailList = email.Split(w);
                if (mailList.Length > 1)
                {
                    splitKey = w;
                }
            }
            if (!string.IsNullOrEmpty(splitKey))
            {
                string[] mailList = email.Split(splitKey);
                mails.AddRange(mailList);
            }
            else
            {
                mails.Add(email);
            }

            return mails;
        }

        private string GetForgetPasswordEmailString(string sellerName, string contactName, string email)
        {
            string encryName = "";

            if (!string.IsNullOrEmpty(contactName))
            {
                for (int i = 0; i < contactName.Length; i++)
                {
                    if ((i == 0) || (i == contactName.Length - 1))
                    {
                        encryName += contactName[i];
                    }
                    else
                    {
                        encryName += "*";
                    }
                }
            }

            string encryEmail = "";
            if (!string.IsNullOrEmpty(email) && CheckVbsContactEmail(email))
            {
                List<string> mails = SplitEmail(email);
                int idx = 0;
                foreach (string mail in mails)
                {
                    if (string.IsNullOrEmpty(mail))
                    {
                        continue;
                    }
                    idx++;

                    string[] m = mail.Split("@");
                    string prefix = m[0];
                    string suffix = m[1];
                    encryEmail += string.Format("{0}{1}{2}{3}{4}",
                                prefix.Substring(0, 1),
                                "".PadLeft(prefix.Length - 2, '*'),
                                prefix.Substring(prefix.Length - 1),
                                "@",
                                suffix);
                    if (idx < mails.Count)
                    {
                        encryEmail += "/";
                    }
                }
            }
            return string.Format("{0} / {1} / {2}", sellerName, encryName, encryEmail);
        }

        private IList<SingleSignOnSource> GetSingleSignOnSoureceList(int userId)
        {
            IList<SingleSignOnSource> ssoSourceList = new List<SingleSignOnSource>();
            MemberLinkCollection mlCol = _mp.MemberLinkGetList(userId);

            foreach (MemberLink ml in mlCol)
            {
                if (Enum.IsDefined(typeof(SingleSignOnSource), ml.ExternalOrg))
                {
                    ssoSourceList.Add((SingleSignOnSource)ml.ExternalOrg);
                }
            }

            return ssoSourceList;
        }

        private static void SendForgetPasswordMail(string memMail, string accountId, string applyPassword)
        {
            Core.Component.Template.ForgetPasswordVbsMail template = TemplateFactory.Instance().GetTemplate<Core.Component.Template.ForgetPasswordVbsMail>();
            template.SiteUrl = config.SiteUrl;
            template.AccountId = accountId;
            template.ApplyPassword = applyPassword;
            template.HourLimit = (config.VbsTemporaryPasswordHourLimit / 60).ToString();

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress(config.ServiceEmail, config.ServiceName);
            msg.To.Add(memMail);
            msg.Subject = "商家系統申請密碼通知";
            msg.Body = template.ToString();
            msg.IsBodyHtml = true;
            PostMan.Instance().Send(msg, SendPriorityType.Immediate);
        }
        #endregion 忘記密碼

        #region Step By Step註冊會員
        public ActionResult StepByStepRegister(string bind)
        {
            if (string.IsNullOrEmpty(bind))
            {
                return RedirectToAction("Others");
            }
            ViewBag.VbsAccount = Session[VbsSession.UserId.ToString()];
            ViewBag.BindAccount = bind;
            return View();
        }
        [VbsAuthorize]
        public ActionResult RedirectToRegister()
        {
            TempData["IsRedirectByVbs"] = true;
            return RedirectToAction("Login", "Mobile");
        }

        [VbsAuthorize]
        public ActionResult GetMemberByMail(string list)
        {
            List<string> rtnList = new List<string>();

            List<string> memData = new JsonSerializer().Deserialize<List<string>>(list);

            foreach (string data in memData)
            {
                if (RegExRules.CheckMobile(data))
                {
                    Member mem = MemberFacade.GetMemberByMobile(data);
                    if (mem.IsLoaded)
                    {
                        IList<SingleSignOnSource> ssoSourceList = GetSingleSignOnSoureceList(mem.UniqueId);
                        if (ssoSourceList.Count > 0)
                        {
                            rtnList.Add(mem.Mobile);
                        }
                    }
                }
                else if (RegExRules.CheckEmail(data))
                {
                    Member mem = MemberFacade.GetMember(data);
                    if (mem.IsLoaded)
                    {
                        IList<SingleSignOnSource> ssoSourceList = GetSingleSignOnSoureceList(mem.UniqueId);
                        if (ssoSourceList.Count > 0)
                        {
                            rtnList.Add(mem.UserEmail);
                        }
                    }
                }
            }

            return Json(rtnList);
        }

        [ActionName("Login")]
        [HttpPost]
        public ActionResult AuthenticateLogin(LoginModel inputModel)
        {
            string userName = inputModel.UserName.Trim();
            inputModel.UserName = userName;

            bool isVbsAccout = IsVbsAccount(userName);

            Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()] = !isVbsAccout;     //是否以17Life綁定帳號登入
            Session[VbsSession.RealLoginAccount.ToString()] = inputModel.UserName;  //實際登入的帳號(可能是商家帳號，或17Life綁定的帳號)

            if (isVbsAccout)
            {
                #region 商家帳號登入

                #region Demo User登入
                //判斷是否為Demo User登入
                if (VbsMemberUtility.DemoUserSignIn(inputModel.UserName, inputModel.Password))
                {
                    Session[VbsSession.LoginSessionCheck.ToString()] = new object();
                    Session[VbsSession.VbsAccountType.ToString()] = VbsMembershipAccountType.DemoUser;
                    //第一次登入或變更Demo user帳號 登入狀態及密碼回復為初始狀態
                    if (Session[VbsSession.FirstLogin.ToString()] == null || Convert.ToBoolean(Session[VbsSession.FirstLogin.ToString()]) == true)
                    {
                        Session[VbsSession.DemoUser.ToString()] = new DemoUser();
                        Session[VbsSession.DemoData.ToString()] = new DemoData();
                        Session[VbsSession.FirstLogin.ToString()] = true;
                        VbsCurrent.DemoUser.Password = "123456";

                        if (string.Compare(inputModel.UserName, "DEMONO0002", true) == 0)
                        {
                            Session[VbsSession.UseInspectionCode.ToString()] = true;
                            Session[VbsSession.IsInspectionCodeNeverChange.ToString()] = true;
                            VbsCurrent.DemoUser.InspectionCode = "123456";
                        }
                        else
                        {
                            Session[VbsSession.UseInspectionCode.ToString()] = false;
                            Session[VbsSession.IsInspectionCodeNeverChange.ToString()] = false;
                        }
                    }

                    //設定帳號檢視工具列權限
                    Session[VbsSession.ViewVbsRight.ToString()] = new ViewVbsRight();
                    VbsCurrent.ViewVbsRight.ToShop = true;
                    VbsCurrent.ViewVbsRight.ToHouse = false;
                    VbsCurrent.ViewVbsRight.Verify = true;
                    VbsCurrent.ViewVbsRight.BalanceSheet = true;
                    VbsCurrent.ViewVbsRight.Ship = false;

                    Session[VbsSession.UserId.ToString()] = inputModel.UserName.ToUpper();
                    Session[VbsSession.UserName.ToString()] = "17Life生活館";
                    Session[VbsSession.IsPwdPassTimeLimit.ToString()] = false;
                    Session[VbsSession.Is17LifeEmployee.ToString()] = false;

                    if (Session[VbsSession.FirstLogin.ToString()].Equals(true))
                    {
                        return RedirectToAction("SetPassword");
                    }
                    if (Session[VbsSession.IsInspectionCodeNeverChange.ToString()].Equals(true))
                    {
                        return RedirectToAction("SetInspectionCode");
                    }

                    return RedirectToAction("Verify");
                }

                #endregion Demo User登入

                #region 一般User登入
                VbsMembership memOriginal = _mp.VbsMembershipGetByAccountId(inputModel.UserName);
                //要先把member取出, 因為 UserSignIn 後會更新 member 資料, 永遠判斷不出來是否第一次登入

                bool loginSuccess = VbsMemberUtility.UserSignIn(inputModel.UserName, inputModel.Password, this.HttpContext);
                if (loginSuccess)
                {
                    VbsMembership mem = _mp.VbsMembershipGetByAccountId(inputModel.UserName);

                    _mp.VbsAccountAuditSet(mem.Id,
                        mem.AccountId,
                        Helper.GetClientIP(),
                        VbsAccountAuditAction.Login,
                        true,
                        mem.IsTemporaryLogin);

                    SetSession(mem, memOriginal.IsFirstLogin);

                    if (mem.IsFirstLogin || mem.IsPasswordResetNeeded)
                    {
                        return RedirectToAction("SetPassword");
                    }
                    if (mem.HasInspectionCode && !mem.LastInspectionCodeChangedDate.HasValue)
                    {
                        return RedirectToAction("SetInspectionCode");
                    }

                    return RedirectToAction("RedirectToActionAfterLogin");
                }
                else
                {
                    int? id = null;
                    if (memOriginal != null && memOriginal.IsLoaded)
                    {
                        id = memOriginal.Id;
                    }

                    _mp.VbsAccountAuditSet(id,
                        inputModel.UserName,
                        Helper.GetClientIP(),
                        VbsAccountAuditAction.Login,
                        false,
                        false);
                }

                #endregion 一般User登入
                #endregion 商家帳號登入
            }
            else
            {
                #region 17Life會員帳號登入
                MemberLinkCollection mlCol = null;
                Member mem;
                SingleSignOnSource source;
                SignInReply reply = Component.MemberActions.LoginProviders.Login(inputModel.UserName, inputModel.Password,
                    false, ref mlCol, out userName, out source, out mem);
                if (reply == SignInReply.Success)
                {
                    #region 驗證是否帳號已綁定
                    var vbsMsBa = VBSFacade.VbsMembershipBindAccountGet(inputModel.UserName);
                    if (vbsMsBa.IsLoaded)
                    {
                        VbsMembership memOriginal = _mp.VbsMembershipGetByAccountId(vbsMsBa.VbsAccountId);
                        //要先把member取出, 因為 UserSignIn 後會更新 member 資料, 永遠判斷不出來是否第一次登入
                        bool loginSuccess = VbsMemberUtility.UserSignInByEncryptPassword(vbsMsBa.VbsAccountId, memOriginal.Password, this.HttpContext);
                        if (loginSuccess)
                        {
                            Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()] = true;     //是否以17Life綁定帳號登入


                            VbsMembership vbsMem = _mp.VbsMembershipGetByAccountId(vbsMsBa.VbsAccountId);

                            vbsMem.IsTemporaryLogin = false;
                            _mp.VbsMembershipSet(vbsMem);

                            _mp.VbsAccountAuditSet(vbsMem.Id,
                                vbsMem.AccountId,
                                Helper.GetClientIP(),
                                VbsAccountAuditAction.Login,
                                true,
                                false);

                            SetSession(vbsMem, memOriginal.IsFirstLogin);

                            if (vbsMem.IsFirstLogin || vbsMem.IsPasswordResetNeeded)
                            {
                                return RedirectToAction("SetPassword");
                            }
                            if (vbsMem.HasInspectionCode && !vbsMem.LastInspectionCodeChangedDate.HasValue)
                            {
                                return RedirectToAction("SetInspectionCode");
                            }

                            return RedirectToAction("RedirectToActionAfterLogin");
                        }
                    }
                    else
                    {
                        ViewBag.IsFoundTheVbsAccount = "NotFound";
                    }
                    #endregion 驗證是否帳號已綁定
                }
                #endregion 17Life會員帳號登入
            }


            ViewBag.IsAuth = false;
            ViewBag.IsEnableVbsBindAccount = config.IsEnableVbsBindAccount;
            return View("Login");
        }

        private bool IsVbsAccount(string userName)
        {
            if (userName.Contains("@") || RegExRules.CheckMobile(userName))
            {
                //17life會員帳號
                if (!config.IsEnableVbsBindAccount)
                {
                    //如果設定沒開，預設只走商家帳號登入
                    return true;
                }
            }
            else
            {
                //商家帳號
                return true;
            }
            return false;
        }

        #region 後臺模擬商家帳號登入
        public ActionResult VendorAccountForceSignIn(string id, string simulateUserName, bool privilege)
        {
            VbsMembership mem = _mp.VbsMembershipGetByAccountId(id);
            VbsMemberUtility.VendorAccountForceSignIn(mem, this.HttpContext, simulateUserName);
            Session[VbsSession.RealLoginAccount.ToString()] = id;  //實際登入的帳號(可能是商家帳號，或17Life綁定的帳號)
            Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()] = false;
            Session[VbsSession.IsTemporaryLogin.ToString()] = false;
            Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] = simulateUserName;
            if (config.SimulateReadOnlyEnabled)
            {
                if (privilege)
                    Session.Remove(VbsSession.LoginBy17LifeSimulate.ToString());
                else
                    Session[VbsSession.LoginBy17LifeSimulate.ToString()] = true;
            }

            SetSession(mem, mem.IsFirstLogin);

            return RedirectToAction("RedirectToActionAfterLogin");
        }

        #endregion 後臺模擬商家帳號登入

        #endregion 登入/登出

        #endregion 登入/登出

        #region 綁定17life帳號

        [HttpPost]
        [VbsAuthorize]
        public ActionResult BindAccount(string bindAccount, bool isBind)
        {
            
            VbsBindAccountErrorType errorType = VbsBindAccountErrorType.NONE;
            //商家帳號
            var UserId = Session[VbsSession.UserId.ToString()].ToString();
            VBSFacade.BindAccount(bindAccount, isBind, UserId, out errorType);

            return Json(new
            {
                IsSuccess = errorType == VbsBindAccountErrorType.NONE,
                ErrorType = errorType,
            });
            
        }

        /// <summary>
        /// 判斷目前登入的會員是否為綁定的帳號
        /// </summary>
        /// <returns></returns>
        //public ActionResult IsBind17LifeMemberAccount()
        //{
        //    var isBind17LifeMemberAccount = Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()];
        //    return Json(new
        //    {
        //        IsBindAccount = isBind17LifeMemberAccount//若為此商家帳號綁定的會員帳號, 則一併登出
        //    });
        //}
        #endregion

        #region 個資提醒/條款

        public ActionResult ToInfoProtectPage(bool isShowConfirmButton)
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.FirstLogin = Session[VbsSession.FirstLogin.ToString()];
            ViewBag.IsShowConfirmButton = isShowConfirmButton;
            return View("PersonalDataRemind");
        }

        [VbsAuthorize]
        public ActionResult PersonalDataRemind()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.FirstLogin = Session[VbsSession.FirstLogin.ToString()];

            VbsMembership mem = _mp.VbsMembershipGetByAccountId(ViewBag.UserId);
            //寫入 "個資提醒的讀取時間"
            mem.LastInfoProtectReadDate = DateTime.Now;
            _mp.VbsMembershipSet(mem);
            Session[VbsSession.IsReadInfoProtectNeeded.ToString()] = false;

            return RedirectToAction("RedirectToActionAfterLogin");
        }

        [VbsAuthorize]
        public ActionResult PersonalInfoClause(bool isShowConfirmButton)
        {
            ViewBag.IsShowConfirmButton = isShowConfirmButton;
            return View("PersonalInfoClause");
        }

        [VbsAuthorize]
        public ActionResult SetPerosnalInfoClauseAgreeDate()
        {
            var userId = Session[VbsSession.UserId.ToString()].ToString();
            var mem = _mp.VbsMembershipGetByAccountId(userId);
            //寫入 "個資條款同意時間"
            mem.PersonalInfoClauseAgreeDate = DateTime.Now;
            _mp.VbsMembershipSet(mem);
            Session[VbsSession.IsAgreePersonalInfoClause.ToString()] = true;

            return RedirectToAction("RedirectToActionAfterLogin");
        }

        #endregion 個資提醒/條款

        #region 公告與通知

        [VbsAuthorize]
        public ActionResult BulletinBoardList(int? postPageNumber, int? returnPageNumber, int? bsPageNumber, int? exchangePageNumber)
        {
            var model = new BulletinBoardListModel();

            //if (IsTemporaryTimeout())
            //{
            //    return RedirectToAction("Logout");
            //}

            #region 變數

            #region ViewBag

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.ViewVbsRightShip = VbsCurrent.ViewVbsRight.Ship;
            ViewBag.ViewBalanceSheet = VbsCurrent.ViewVbsRight.BalanceSheet;

            #endregion ViewBag

            #region 公告

            int postPageSize = 10; //一頁有幾筆
            int markRedDay = 2; //距今 N日內的訊息要標示為紅色. 如今天是 9/13, 而 N=2, 則9/11公布的訊息也包含在內
            postPageNumber = postPageNumber ?? 1;

            #endregion 公告

            #region 退貨申請

            int returnPageSize = 5; //一頁有幾筆
            returnPageNumber = returnPageNumber ?? 1;

            #endregion 退貨申請

            #region 對帳單通知

            int bsPageSize = 5; //一頁有幾筆
            bsPageNumber = bsPageNumber ?? 1;

            #endregion 對帳單通知

            #region 換貨申請

            int exchangePageSize = 5; //一頁有幾筆
            exchangePageNumber = exchangePageNumber ?? 1;

            #endregion 換貨申請

            #endregion 變數

            #region 公告訊息

            IEnumerable<VbsBulletinBoard> dbPosts = VerificationFacade.BulletinBoardGetListByAnnounce(postPageNumber.Value, 0, false, false, string.Empty).ToList();

            if (config.IsEnableVbsNewUi)
            {
                if (Request.Params["d"] == ((int)DeliveryType.ToShop).ToString() && VbsCurrent.ViewVbsRight.Verify)
                {
                    //僅憑證權限
                    dbPosts = dbPosts.Where(x => Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToShop));
                }
                else if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString() && VbsCurrent.ViewVbsRight.Ship)
                {
                    //僅宅配權限
                    dbPosts = dbPosts.Where(x => Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToHouse));
                }
                else
                {
                    //都沒權限
                    dbPosts = dbPosts.Where(x => x.PublishSellerType == (int)VbsBulletinBoardPublishSellerType.None);
                }
            }
            else
            {
                if (VbsCurrent.ViewVbsRight.Verify && VbsCurrent.ViewVbsRight.Ship)
                {
                    //有憑證和宅配權限
                    dbPosts = dbPosts.Where(x => Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToShop)
                        || Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToHouse));
                }
                else if (VbsCurrent.ViewVbsRight.Verify)
                {
                    //僅憑證權限
                    dbPosts = dbPosts.Where(x => Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToShop));
                }
                else if (VbsCurrent.ViewVbsRight.Ship)
                {
                    //僅宅配權限
                    dbPosts = dbPosts.Where(x => Helper.IsFlagSet(x.PublishSellerType, (int)VbsBulletinBoardPublishSellerType.ToHouse));
                }
                else
                {
                    //都沒權限
                    dbPosts = dbPosts.Where(x => x.PublishSellerType == (int)VbsBulletinBoardPublishSellerType.None);
                }
            }

            List<VbsBulletinBoardPostInfo> posts = new List<VbsBulletinBoardPostInfo>();
            foreach (VbsBulletinBoard p in dbPosts)
            {
                VbsBulletinBoardPostInfo info = new VbsBulletinBoardPostInfo();
                info.Id = p.Id;
                info.IsSticky = p.IsSticky;
                info.PublishContent = p.PublishContent;
                info.PublishSellerType = (VbsBulletinBoardPublishSellerType)p.PublishSellerType;
                info.PublishStatus = (VbsBulletinBoardPublishStatus)p.PublishStatus;
                info.PublishTime = p.PublishTime;
                info.CreateTime = p.CreateTime;
                info.IsMarkRed = p.PublishTime.Date >= DateTime.Now.AddDays(-markRedDay).Date;
                info.LinkTitle = p.LinkTitle;
                info.LinkUrl = p.LinkUrl;
                posts.Add(info);
            }
            posts = posts.OrderByDescending(x => x.PublishTime).ToList();
            model.Posts = GetPagerBulletinBoardInfo(posts, postPageSize, postPageNumber.Value - 1, string.Empty, false);

            #endregion

            #region 對帳單產出提醒

            if (VbsCurrent.ViewVbsRight.BalanceSheet)
            {
                var balanceSheets = new List<VbsBulletinBoardBalanceSheetInfo>();
                var tempBsDealInfos = new List<VbsBalanceDealInfo>();
                var allowedDeals = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.ViewBalanceSheet, false)
                                        .GroupBy(x => x.MerchandiseGuid.Value)
                                        .ToDictionary(x => x.Key, x => x.First());
                //撈取有權限之檔次
                var dealGuids = allowedDeals.Select(x => x.Key).ToList();
                //撈取宅配檔次母檔資訊
                var mainDealGuids = allowedDeals.Values.Where(x => x.IsHomeDelivery.GetValueOrDefault(false))
                                        .Select(x => x.MerchandiseGuid.Value);
                var storeGuids = allowedDeals.Values.Select(x => x.StoreGuid).Distinct();
                //撈取 宅配多檔次母檔對應之子檔 資訊 
                var subDeals = GetComboSubDealsInfo(mainDealGuids);
                if (subDeals.Any())
                {
                    dealGuids.AddRange(subDeals.Select(x => x.Key));
                }
                var tempBsInfos = _ap.ViewBalanceSheetListGetListByIsNotConfirmed(dealGuids, Session[VbsSession.UserId.ToString()].ToString())
                    .Where(x =>
                    {
                        if (x.DeliveryType == (int)DeliveryType.ToShop &&
                            !storeGuids.Contains(x.StoreGuid))
                        {
                            return false;
                        }
                        return true;
                    });
                var bsInfos = tempBsInfos
                    .GroupBy(x => new { x.ProductGuid, x.UniqueId, x.Name })
                    .Select(x => new
                    {
                        ProductGuid = x.Key.ProductGuid,
                        UniqueId = x.Key.UniqueId,
                        Name = x.Key.Name,
                        StoreGuid = x.OrderByDescending(b => b.Id).First().StoreGuid,
                        Id = x.Max(b => b.Id),
                        DeliveryType = x.Max(b => b.DeliveryType),
                        GenerationFrequency = x.OrderByDescending(b => b.Id).First().GenerationFrequency,
                        IntervalEnd = x.Max(b => b.IntervalEnd),
                        CreateTime = x.Max(b => b.CreateTime),
                        RemittanceType = x.OrderByDescending(b => b.Id).First().RemittanceType
                    });

                tempBsDealInfos = tempBsInfos.Where(x => x.DeliveryType == (int)DeliveryType.ToShop)
                                    .GroupBy(x => new { x.ProductGuid, x.StoreGuid })
                                    .Select(x => new VbsBalanceDealInfo
                                    {
                                        DealId = x.Key.ProductGuid,
                                        DealType = x.First().ProductType == (int)BusinessModel.Ppon
                                                            ? VbsDealType.Ppon
                                                            : VbsDealType.PiinLife,
                                        DealName = x.First().Name,
                                        StoreGuid = x.Key.StoreGuid,
                                        StoreName = x.First().StoreName,
                                        DealUniqueId = x.First().UniqueId,
                                        ExchangePeriodEndTime = x.First().UseEndTime.Value,
                                        GenerationFrequency = x.First().GenerationFrequency,
                                        DeliveryType = x.First().DeliveryType.Value,
                                        RemittanceType = x.First().RemittanceType
                                    })
                                            .OrderByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId)
                                            .ToList();

                if (bsInfos.Any())
                {
                    var mailDealBsInfo = (from rd in bsInfos
                                          join deal in subDeals on rd.ProductGuid equals deal.Key
                                          group rd by deal.Value
                                              into md
                                          select new
                                          {
                                              MainDealGuid = md.Key,
                                              StoreGuid = md.Max(x => x.StoreGuid),
                                              BalanceSheetId = md.Max(x => x.Id),
                                              DeliveryType = md.Max(x => x.DeliveryType),
                                              GenerationFrequency = md.Max(x => x.GenerationFrequency),
                                              IntervalEnd = md.Max(x => x.IntervalEnd),
                                              GenerateTime = md.Max(x => x.CreateTime),
                                              RemittanceType = md.Max(x => x.RemittanceType)
                                          })
                                        .ToLookup(x => x.MainDealGuid);
                    foreach (var bs in bsInfos)
                    {
                        //個別抓取子檔之對帳單 需轉換為母檔顯示 
                        var mainDealGuid = subDeals.ContainsKey(bs.ProductGuid)
                                               ? subDeals[bs.ProductGuid]
                                               : bs.ProductGuid;
                        var deal = allowedDeals.ContainsKey(mainDealGuid)
                                    ? allowedDeals[mainDealGuid]
                                    : null;
                        var mainBsInfos = mailDealBsInfo.Contains(mainDealGuid)
                                            ? mailDealBsInfo[mainDealGuid].First()
                                            : null;
                        if (mainBsInfos != null)
                        {
                            //母檔未壓記對帳單開立日 代表尚未開放對帳單明細請商家請款
                            var da = _pp.DealAccountingGet(mainBsInfos.MainDealGuid);
                            if (da != null && da.IsLoaded)
                            {
                                if ((da.RemittanceType == (int)RemittanceType.ManualPartially ||
                                     da.RemittanceType == (int)RemittanceType.Others) &&
                                    (!da.BalanceSheetCreateDate.HasValue ||
                                     DateTime.Compare(da.BalanceSheetCreateDate.Value, DateTime.Today) > 0))
                                {
                                    continue;
                                }
                            }
                        }

                        var bsInfo = new VbsBulletinBoardBalanceSheetInfo
                        {
                            DealGuid = deal != null
                                        ? deal.MerchandiseGuid.Value
                                        : bs.ProductGuid,
                            DealId = deal != null
                                        ? deal.MerchandiseId.Value
                                        : bs.UniqueId,
                            DealName = deal != null
                                        ? !string.IsNullOrEmpty(deal.AppTitle) ? deal.AppTitle : deal.MerchandiseName
                                        : bs.Name,
                            StoreGuid = mainBsInfos != null
                                            ? mainBsInfos.StoreGuid
                                            : bs.StoreGuid,
                            BalanceSheetId = mainBsInfos != null
                                            ? mainBsInfos.BalanceSheetId
                                            : bs.Id,
                            DeliveryType = (DeliveryType)(mainBsInfos != null
                                            ? mainBsInfos.DeliveryType
                                            : bs.DeliveryType),
                            GenerationFrequency = (BalanceSheetGenerationFrequency)(mainBsInfos != null
                                            ? mainBsInfos.GenerationFrequency
                                            : bs.GenerationFrequency),
                            IntervalEnd = string.Format("{0:yyyy/MM/dd}"
                                            , mainBsInfos != null
                                                ? mainBsInfos.IntervalEnd
                                                : bs.IntervalEnd),
                            GenerateTime = string.Format("{0:yyyy/MM/dd HH:mm}"
                                            , mainBsInfos != null
                                                ? mainBsInfos.GenerateTime
                                                : bs.CreateTime),
                            RemittanceType = (RemittanceType)(mainBsInfos != null
                                            ? mainBsInfos.RemittanceType
                                            : bs.RemittanceType),
                        };

                        if (!balanceSheets.Select(x => x.DealGuid).Contains(bsInfo.DealGuid))
                        {
                            balanceSheets.Add(bsInfo);
                        }
                        if (bs.DeliveryType == (int)DeliveryType.ToHouse && !tempBsDealInfos.Select(x => x.DealId).Contains(bs.ProductGuid))
                        {
                            tempBsDealInfos.Add(new VbsBalanceDealInfo
                            {
                                DealId = bs.ProductGuid,
                                DealType = VbsDealType.Ppon,
                                DealName = bs.Name,
                                StoreGuid = bs.StoreGuid,
                                DealUniqueId = bs.UniqueId,
                                ExchangePeriodEndTime = bs.IntervalEnd,
                                GenerationFrequency = bs.GenerationFrequency,
                                DeliveryType = bs.DeliveryType.Value,
                                RemittanceType = bs.RemittanceType
                            });
                        }
                    }
                    if (config.IsEnableVbsNewUi)
                    {
                        if (Request.Params["d"] == ((int)DeliveryType.ToShop).ToString())
                        {
                            tempBsDealInfos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToShop);
                            balanceSheets.RemoveAll(x => x.DeliveryType != DeliveryType.ToShop);
                        }
                        else if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString())
                        {
                            tempBsDealInfos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToHouse);
                            balanceSheets.RemoveAll(x => x.DeliveryType != DeliveryType.ToHouse);
                        }
                    }

                    balanceSheets = balanceSheets.OrderByDescending(x => x.GenerateTime).ToList();
                    Session[VbsSession.BalanceDealInfos.ToString()] = tempBsDealInfos;
                }

                model.BalanceSheets = GetPagerBulletinBoardBalanceSheetInfo(balanceSheets, bsPageSize, bsPageNumber.Value - 1, string.Empty, false);
            }

            #endregion 對帳單產出提醒


            #region PChome退貨功能跳窗
            bool readPchomeReturnRule2k19 = false;
            WmsContractLogCollection pclc = _sp.GetWmsContractLogByAccountid(VbsCurrent.AccountId);
            if (pclc.Count > 0)
            {
                VbsConfirmNoticeLogCollection vcnlc = _sp.VbsConfirmNoticeLogGet(VbsCurrent.AccountId);
                var vcnl = vcnlc.Where(x => x.Type == (int)VbsConfirmNoticeType.PchomeReturnRule2k19).FirstOrDefault();
                if (vcnl != null)
                {
                    readPchomeReturnRule2k19 = true;
                }
            }
            else
            {
                //沒簽約，所以不用跳窗，當成已讀
                readPchomeReturnRule2k19 = true;
            }
            ViewBag.ReadPchomeReturnRule2k19 = readPchomeReturnRule2k19;
            #endregion

            #region 提案相關功能關閉跳窗
            bool readDeliveryClose = false;
            if (config.DeliveryClosePop)
            {
                if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString())
                {
                    VbsConfirmNoticeLogCollection vcnlc2 = _sp.VbsConfirmNoticeLogGet(VbsCurrent.AccountId);
                    var vcnl2 = vcnlc2.Where(x => x.Type == (int)VbsConfirmNoticeType.DeliveryClose).FirstOrDefault();
                    if (vcnl2 != null)
                    {
                        readDeliveryClose = true;
                    }
                }
                else
                {
                    //憑證商家不用顯示
                    readDeliveryClose = true;
                }
            }
            else
            {
                readDeliveryClose = true;
            }
            
            
            ViewBag.ReadDeliveryClose = readDeliveryClose;
            #endregion


            return View("BulletinBoardList", model);
        }

        #endregion

        #region 線上核銷

        [VbsAuthorize]
        public ActionResult Verify()
        {
            //if (IsTemporaryTimeout())
            //{
            //    return RedirectToAction("Logout");
            //}
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.ParentActionName.ToString()] = "Verify";
            return View();
        }

        /// <summary>
        /// 檢核是否能核銷
        /// </summary>
        /// <param name="inputModel"></param>
        /// <returns></returns>
        [ActionName("Verify")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult VerifyCouponCheck(VerifyModel inputModel)
        {
            #region 宣告

            bool isCouponAvailable = false;
            bool isDuInTime = false;
            string errMsg;
            string trustId = string.Empty;
            string dealName = string.Empty;
            string couponNumber = string.Empty;
            string payerName = string.Empty;
            string useStart = string.Empty;
            string useEnd = string.Empty;
            string dealType = string.Empty;
            string couponSequence = inputModel.txtCouponP1;
            string couponCode = inputModel.txtCouponP2;
            string couponPreSequence = inputModel.txtPrefixCoupon;
            string userId = VbsCurrent.AccountId;
            #endregion

            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                errMsg = "模擬身分不允許執行此功能";
            }
            else
            {
                var couponInfos = new List<VbsVerifyCouponInfo>();

                //Demo user 登入直接替換為 Demo版核銷資料
                if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
                {
                    if (VbsCurrent.DemoData != null)
                    {
                        couponInfos = VbsCurrent.DemoData.VerifyCouponInfos.Where(x => x.CouponId.Substring(5, 4) == couponSequence && x.TrustId == couponCode).ToList();
                        if (couponInfos.Any()) dealType = couponInfos.First().DealType.ToString();
                    }
                }
                else
                {
                    VbsMembership member = _mp.VbsMembershipGetByAccountId(userId);

                    couponInfos = VerificationFacade.GetAccessibleCoupon(member, couponSequence, couponCode, Guid.Empty)
                                            .Select(info => new VbsVerifyCouponInfo
                                            {
                                                DealName = info.ItemName,
                                                DealId = info.DealId,
                                                DealType = info.DealType,
                                                CouponId = info.CouponSequence,
                                                CouponCode = info.CouponCode,
                                                ExchangePeriodStartTime = info.ExchangePeriodStartTime,
                                                ExchangePeriodEndTime = info.ExchangePeriodEndTime,
                                                VerifyState = info.VerifyState,
                                                ModifyTime = info.ModifyTime,
                                                PayerName = VerificationFacade.SetPayerNameHidden(info.BuyerName),
                                                TrustId = info.TrustId.ToString(),
                                                Message = string.Empty
                                            })
                                            .ToList();
                    if (!string.IsNullOrEmpty(couponPreSequence))
                    {
                        couponInfos = couponInfos.Where(couponinfo => string.Equals(couponinfo.CouponId, couponPreSequence + "-" + couponSequence))
                                                .ToList();
                    }
                }

                if (couponInfos.Any())
                {
                    //檢查憑證核銷狀態，並回傳檢查憑證狀態結果
                    var accessibleCoupon = inputModel.isVerify
                            ? VerificationFacade.GetVbsAccessibleCoupon(couponInfos).ToList()
                            : VerificationFacade.GetVbsAccessibleUnverifyCoupon(couponInfos).ToList();

                    if (accessibleCoupon.Count > 1)
                    {
                        ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                        ViewBag.UserId = userId;
                        ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

                        var vdlModel = new VerifyModel
                        {
                            isVerify = inputModel.isVerify,
                            CouponInfos = accessibleCoupon
                        };

                        return View("VerifyRepeatCouponList", vdlModel);
                    }

                    VbsVerifyCouponInfo couponInfo = accessibleCoupon.First();
                    errMsg = couponInfo.Message;
                    isCouponAvailable = couponInfo.IsCouponAvailable;
                    trustId = couponInfo.TrustId;
                    dealName = couponInfo.DealName;
                    couponNumber = couponInfo.CouponId;
                    isDuInTime = couponInfo.IsDuInTime;
                    payerName = couponInfo.PayerName;
                    useStart = couponInfo.ExchangePeriodStartTime.ToString("yyyy/MM/dd");
                    useEnd = couponInfo.ExchangePeriodEndTime.ToString("yyyy/MM/dd");
                    dealType = ((int)couponInfo.DealType).ToString();
                }
                else
                {
                    errMsg = "查無此憑證。" + (inputModel.isVerify ? "可能為無效憑證，\n或非本店所能核銷的憑證。" : "無法取消核銷。");
                }

            }
            
            Guid trustGuid;
            if (Guid.TryParse(trustId, out trustGuid))
            {
                new AntiFraud(trustGuid).VerifyCouponCheck(ref isCouponAvailable, ref errMsg, ref trustId);
            }

            return Json(new
            {
                IsCouponAvailable = isCouponAvailable,
                Message = errMsg,
                TrustId = trustId,
                DealName = dealName,
                PayerName = payerName,
                CouponNumber = couponNumber,
                UserId = userId,
                IsDuInTime = isDuInTime,
                CouponCode = couponCode,
                UseStart = useStart,
                UseEnd = useEnd,
                dealType
            });
        }

        [VbsAuthorize]
        public ActionResult VerifyRepeatCouponList()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            return View();
        }

        /// <summary>
        /// 確定核銷
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult VerifyCoupon(string id, string type)
        {
            type = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            VerificationStatus status = VerificationStatus.CouponError;
            //Demo User登入 核銷結果轉換為 Demo版核銷結果內容
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                if (VbsCurrent.DemoData.VerifyResultMsg.Any(x => x.Key == id))
                {
                    status = VbsCurrent.DemoData.VerifyResultMsg.First(x => x.Key == id).Value;
                }
            }
            else
            {
                status = OrderFacade.VerifyCoupon(new Guid(id), type);
            }

            VbsVerifyResult couponInfo = VerificationFacade.GetVbsVerifyResult(status, id);

            return Json(new { IsVerifySuccess = couponInfo.IsVerifySuccess, ResultMsg = couponInfo.Message });
            
            
        }

        /// <summary>
        /// 取消核銷
        /// </summary>
        /// <param name="id"></param>
        /// <param name="createId"></param>
        /// <param name="dealType"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult UnverifyCoupon(string id, string createId, string dealType)
        {
            createId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            switch (Convert.ToInt32(dealType))
            {
                case (int)BusinessModel.PiinLife:
                    if (OrderFacade.UndoVerifiedStatus(new Guid(id), createId, OrderClassification.HiDeal))
                    {
                        return Json(new { IsUnverifySuccess = true, ResultMsg = "取消核銷成功" });
                    }
                    else
                    {
                        return Json(new { IsUnverifySuccess = false, ResultMsg = "取消核銷失敗" });
                    }
                case (int)BusinessModel.Ppon:
                    if (OrderFacade.UndoVerifiedStatus(new Guid(id), createId, OrderClassification.LkSite))
                    {
                        return Json(new { IsUnverifySuccess = true, ResultMsg = "取消核銷成功" });
                    }
                    else
                    {
                        return Json(new { IsUnverifySuccess = false, ResultMsg = "取消核銷失敗" });
                    }
                default:
                    return Json(new { IsUnverifySuccess = false, ResultMsg = "未預期的狀況，請重新再試" });
            }
            
            

        }

        #endregion 線上核銷

        #region 核銷查詢

        [VbsAuthorize]
        public ActionResult VerificationDealList()
        {
            if (VbsCurrent.Is17LifeEmployee)
            {
                //TODO 改用 auth 之類的 attribute 來驗證權限?
                throw new Exception("這是商家才能操作的功能");
            }

            VerificationDealListModel vdlModel = new VerificationDealListModel();
            List<VbsVerificationDealInfo> infos = new List<VbsVerificationDealInfo>();
            vdlModel.DealInfos = infos;

            //Demo user登入 則替換Demo版核銷查詢檔次列表資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                infos = VbsCurrent.DemoData.DealInfos;
            }
            else
            {
                //取得有權限且符合條件檢視的檔次資訊
                var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.VerifyShop);

                List<VbsVerificationDealInfo> pponInfos = GetVerificationDealInfo(allowedDealUnits, BusinessModel.Ppon);
                List<VbsVerificationDealInfo> piinInfos = GetVerificationDealInfo(allowedDealUnits, BusinessModel.PiinLife);

                infos.AddRange(pponInfos);
                infos.AddRange(piinInfos);
            }

            vdlModel.DealInfos = GetVerificationDealVerificationPercent(infos.OrderByDescending(x => x.ExchangePeriodEndTime)
                                                                             .ToList());

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.DealCount.ToString()] = vdlModel.DealInfos.Count();
            if (vdlModel.DealInfos.Count() > 1)
            {
                return View("VerificationDealList", vdlModel);
            }
            else if (vdlModel.DealInfos.Count() == 1)
            {
                if (vdlModel.DealInfos.First().StoreCount > 0)
                {
                    return RedirectToAction("VerificationStoreList", new { id = vdlModel.DealInfos.First().DealId, type = vdlModel.DealInfos.First().DealType });
                }
                else
                {
                    return RedirectToAction("VerificationCouponList", new { id = vdlModel.DealInfos.First().DealId, type = vdlModel.DealInfos.First().DealType, detailId = "null" });
                }
            }
            else
            {
                Session[VbsSession.ParentActionName.ToString()] = "VerificationDealList";
                return RedirectToAction("NoRecord");
            }
        }

        [VbsAuthorize]
        public ActionResult EmployeeVerificationDealList(string selQueryOption, string queryKeyword,
            DateTime? queryStartTime, DateTime? queryEndTime)
        {
            if (VbsCurrent.Is17LifeEmployee == false)
            {
                //TODO 改用 auth 之類的 attribute 來驗證權限?
                throw new Exception("這是17Life才能操作的功能");
            }
            VerificationDealListModel vdlModel = new VerificationDealListModel();

            //TODO 整個搬到 Facade 如何?
            List<VbsVerificationDealInfo> infos = new List<VbsVerificationDealInfo>();

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.DealCount.ToString()] = 0;
            if (string.Equals("GET", HttpContext.Request.HttpMethod))
            {
                vdlModel.DealInfos = infos;
                return View(vdlModel);
            }

            DateTime now = DateTime.Now;
            queryKeyword = string.IsNullOrEmpty(queryKeyword) ? queryKeyword : queryKeyword.Trim();

            List<ViewVbsToShopDeal> deals = _pp.ViewVbsToShopDealGetList(
                selQueryOption, queryKeyword, queryStartTime, queryEndTime)
                .Where(x =>
                {
                    if (!x.DealStartTime.HasValue || !x.DealEndTime.HasValue ||
                        !x.UseStartTime.HasValue || !x.UseEndTime.HasValue) //不撈取未設定販賣時間及兌換時間之異常檔次
                    {
                        return false;
                    }

                    //開賣時間還沒開始，不顯示
                    if (DateTime.Compare(now, x.DealStartTime.Value) < 0)
                    {
                        return false;
                    }

                    if (x.DealEstablished.HasValue)
                    {
                        return int.Equals(1, x.DealEstablished); //過門檻
                    }

                    if (x.DealType.Equals((int)VbsDealType.PiinLife))
                    {
                        return true;
                    }

                    return DateTime.Compare(now, x.DealEndTime.Value) < 0; //目前時間未超過結檔時間   
                })
                .ToList();

            if (deals.Count > 0)
            {
                foreach (var deal in deals)
                {
                    VbsVerificationState VerifiedState = VbsVerificationState.NotYetStart;
                    if (now.Date >= deal.UseStartTime.Value.Date && now.Date <= deal.UseEndTime.Value.Date)  //兌換中
                    {
                        VerifiedState = VbsVerificationState.Verifying;
                    }
                    else if (now.Date > deal.UseEndTime.Value.Date)  //活動結束
                    {
                        VerifiedState = VbsVerificationState.Finished;
                    }
                    VbsVerificationDealInfo info = new VbsVerificationDealInfo
                    {
                        DealName = deal.ProductName,
                        DealId = deal.ProductGuid.ToString(),
                        DealType = (VbsDealType)deal.DealType,
                        DealUniqueId = deal.ProductId,
                        ExchangePeriodStartTime = deal.UseStartTime.Value,
                        ExchangePeriodEndTime = deal.UseEndTime.Value,
                        StoreCount = deal.StoreCount.Value,
                        VerificationState = VerifiedState
                    };

                    infos.Add(info);
                }
            }

            vdlModel.DealInfos = GetVerificationDealVerificationPercent(infos.OrderByDescending(x => x.ExchangePeriodEndTime)
                                                                             .ToList());

            Session[VbsSession.DealCount.ToString()] = vdlModel.DealInfos.Count();
            Session[VbsSession.ParentActionName.ToString()] = "EmployeeVerificationDealList";

            if (int.Equals(0, Session[VbsSession.DealCount.ToString()]))
            {
                vdlModel.IsResultEmpty = true;
            }
            return View(vdlModel);
        }

        [VbsAuthorize]
        public ActionResult VerificationStoreList(string id, VbsDealType type)
        {
            VerificationStoreListModel vdlModel = new VerificationStoreListModel();
            //Demo user登入 則替換Demo版核銷查詢檔次列表資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                VbsVerificationDealInfo dealInfo = VbsCurrent.DemoData.DealInfos.Where(x => x.DealId == id).FirstOrDefault();
                List<VbsVerificationStoreInfo> infos = VbsCurrent.DemoData.StoreInfos.Where(x => x.StoreId.Substring(0, 8) == id.Substring(0, 8)).ToList();

                vdlModel.DealId = id;
                vdlModel.DealType = type;
                vdlModel.DealName = dealInfo.DealName;
                vdlModel.DealUniqueId = dealInfo.DealUniqueId;
                vdlModel.StoreInfos = infos;
            }
            else
            {
                vdlModel.DealId = id;
                vdlModel.DealType = type;

                if (type == VbsDealType.PiinLife)
                {
                    Guid productGuid;
                    if (Guid.TryParse(id, out productGuid))
                    {
                        HiDealProduct prod = _hp.HiDealProductGet(productGuid);
                        if (prod != null && prod.IsLoaded)
                        {
                            HiDealDeal deal = _hp.HiDealDealGet(prod.DealId);
                            vdlModel.DealName = string.Format("{0}　{1}", deal.Name, prod.Name);
                            vdlModel.DealUniqueId = prod.Id;
                            vdlModel.NoRestrictedStore = false;
                        }
                    }
                }
                else if (type == VbsDealType.Ppon)
                {
                    Guid bid;
                    if (Guid.TryParse(id, out bid))
                    {
                        ViewPponDeal deal = _pp.ViewPponDealGetByBusinessHourGuid(bid);
                        if (deal != null && deal.IsLoaded)
                        {
                            vdlModel.DealName = deal.CouponUsage;
                            vdlModel.DealUniqueId = deal.UniqueId ?? 0;
                            vdlModel.NoRestrictedStore = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore);
                        }
                    }
                }

                if (VbsCurrent.Is17LifeEmployee)
                {
                    vdlModel.StoreInfos = GetVerificationStoreInfo(id, type);
                }
                else
                {
                    vdlModel.StoreInfos = GetAllowedVerificationStoreInfo(id, type);
                }
            }
            var storeCount = vdlModel.StoreInfos.Count();
            vdlModel.StoreInfos = storeCount > 1
                                    ? GetAllStoreSalesSummery(Guid.Parse(id), vdlModel.StoreInfos.ToList(), vdlModel.NoRestrictedStore)
                                    : vdlModel.StoreInfos;

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
            Session[VbsSession.StoreCount.ToString()] = storeCount;
            ViewBag.StoreCount = Session[VbsSession.StoreCount.ToString()];
            if (storeCount > 1)
            {
                return View("VerificationStoreList", vdlModel);
            }
            else if (storeCount == 1)
            {
                return RedirectToAction("VerificationCouponList", new { id = vdlModel.DealId, type = vdlModel.DealType, detailId = vdlModel.StoreInfos.First().StoreId });
            }
            else if (storeCount == 0)
            {
                return RedirectToAction("VerificationCouponList", new { id = vdlModel.DealId, type = vdlModel.DealType, detailId = "null" });
            }
            else
            {
                Session[VbsSession.ParentActionName.ToString()] = "VerificationStoreList";
                return RedirectToAction("NoRecord");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="detailId"></param>
        /// <param name="pageNumber"></param>
        /// <param name="sortCol"></param>
        /// <param name="sortDesc"></param>
        /// <param name="couponNumber"></param>
        /// <param name="dateFrom"></param>
        /// <param name="dateTo"></param>
        /// <param name="excel">將查詢的結果匯出excel</param>
        /// <param name="verifyState"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult VerificationCouponList(string id, VbsDealType type, string detailId,
            int? pageNumber, string sortCol, bool? sortDesc, string couponNumber, DateTime? dateFrom, DateTime? dateTo,
            int? excel, VbsVerificationShowType verifyState = VbsVerificationShowType.All)
        {
            pageNumber = pageNumber ?? 1;
            sortDesc = sortDesc ?? false;
            sortCol = sortCol ?? "CouponId";
            var sellerName = string.Empty;
            var model = new VerificationCouponListModel();
            var infos = new List<VbsVerificationCouponInfo>();
            var countSummary = new VerificationCountSummary();
            var items = new ViewVbsCashTrustLogStatusLogInfoCollection();
            var dt = new DataTable();
            var dealIntroduction = string.Empty;

            #region Demo data set up

            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                VbsVerificationDealInfo dealInfo = VbsCurrent.DemoData.DealInfos.Where(x => x.DealId == id).FirstOrDefault();
                model.DealId = id;
                model.DealName = dealInfo.DealName;
                model.DealType = type;
                model.DealUniqueId = dealInfo.DealUniqueId;
                model.ExchangePeriodStartTime = dealInfo.ExchangePeriodStartTime;
                model.ExchangePeriodEndTime = dealInfo.ExchangePeriodEndTime;
                model.DealOrderStartTime = dealInfo.ExchangePeriodStartTime;
                model.DealOrderEndTime = dealInfo.ExchangePeriodEndTime;
                model.StoreId = detailId;
                if (detailId == "all")
                {
                    model.StoreName = "所有分店";
                }
                else if (detailId == "null")
                {
                    model.StoreName = null;
                }
                else
                {
                    model.StoreName = VbsCurrent.DemoData.StoreInfos.Where(x => x.StoreId == detailId).FirstOrDefault().StoreName;
                }

                List<VbsVerificationCouponInfo> couponInfos = VbsCurrent.DemoData.VerificationCouponInfos;

                if (!string.IsNullOrEmpty(couponNumber))
                {
                    couponInfos = couponInfos.Where(x => x.CouponId == couponNumber).ToList();
                }

                foreach (var item in couponInfos)
                {
                    item.ProductName = dealInfo.DealName;
                    switch (item.VerifyState)
                    {
                        case (VbsCouponVerifyState.UnUsed):
                            countSummary.PlusUnused();
                            break;
                        case (VbsCouponVerifyState.Verified):
                            countSummary.PlusVerified();
                            break;
                        case (VbsCouponVerifyState.Return):
                            countSummary.PlusReturn();
                            break;
                        case (VbsCouponVerifyState.Returning):
                            countSummary.PlusReturning();
                            break;
                        case (VbsCouponVerifyState.ExpiredVerified):
                            countSummary.PlusExpiredVerified();
                            break;
                    }
                }
                model.CountSummary = countSummary;

                infos = couponInfos;

                if (excel == (int)ExportType.SellerList)
                {
                    SellerDealInfo sellerDeal = VbsCurrent.DemoData.SellerDealInfo.Where(x => x.DealUniqueId == dealInfo.DealUniqueId).First();
                    sellerName = sellerDeal.SellerName;
                    dealIntroduction = sellerDeal.DealIntroduction;
                    dt = VbsCurrent.DemoData.SellerVerificationCouponInfos;
                }
            }

            #endregion Demo data set up

            #region Verification coupon data set up

            else
            {
                Guid? storeGuid = null;
                if (!detailId.Equals("null") && !detailId.Equals("all"))
                {
                    storeGuid = Guid.Parse(detailId);
                }

                //檢查商家帳號是否有檢視該檔次分店權限
                bool isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee
                                            ? true
                                            : CheckIsAllowedDealOrStore(VbsCurrent.AccountId, new Guid(id), storeGuid, VbsRightFlag.VerifyShop);

                if (!isAllowedDealOrStore)
                {
                    return RedirectToAction("RedirectToActionAfterLogin");
                }

                model.DealId = id;
                model.DealType = type;
                model.StoreId = detailId;

                #region Ppon

                if (type == VbsDealType.Ppon)
                {
                    ViewPponDealStore pponInfo;
                    int storeCount = Session[VbsSession.StoreCount.ToString()] != null ? int.Parse(Session[VbsSession.StoreCount.ToString()].ToString()) : 1;
                    var isSellerPemission = VbsCurrent.Is17LifeEmployee ? true : new VbsVendorAclMgmtModel(VbsCurrent.AccountId).GetAllowedDeals(new Guid(id), DeliveryType.ToShop).Any(x => x.IsSellerPermissionAllow);

                    if (detailId == "all")
                    {
                        if (VbsCurrent.Is17LifeEmployee)
                        {
                            pponInfo = _pp.ViewPponDealStoreGetToShopDealList(
                                string.Empty, ViewPponDealStore.Columns.ProductGuid + " = " + id).First();
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                pponInfo.ProductGuid, OrderClassification.LkSite, couponNumber, VbsStoreFilter.All, null);
                        }
                        else
                        {
                            var allowedStoreGuid = GetAllowedStores(new Guid(id));
                            pponInfo = _pp.ViewPponDealStoreGetToShopDealList(
                                string.Join(",", allowedStoreGuid), ViewPponDealStore.Columns.ProductGuid + " = " + id).First();
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                pponInfo.ProductGuid, OrderClassification.LkSite, couponNumber, VbsStoreFilter.BranchStoreAndHeaderOffice, string.Join(",", allowedStoreGuid));
                        }
                        model.StoreName = "所有分店";
                    }
                    else if (detailId == "null")
                    {
                        pponInfo = _pp.ViewPponDealStoreGetToShopDealList(string.Empty, ViewPponDealStore.Columns.ProductGuid + " = " + id).First();
                        items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                            pponInfo.ProductGuid, OrderClassification.LkSite, couponNumber, VbsStoreFilter.HeaderOffice, null);
                    }
                    else
                    {
                        pponInfo = _pp.ViewPponDealStoreGetToShopDealList(
                            detailId, ViewPponDealStore.Columns.ProductGuid + " = " + id).First();

                        if (Helper.IsFlagSet(pponInfo.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) && isSellerPemission && storeCount == 1)
                        {
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                pponInfo.ProductGuid, OrderClassification.LkSite, couponNumber, VbsStoreFilter.All, null);
                        }
                        else
                        {
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                pponInfo.ProductGuid, OrderClassification.LkSite, couponNumber, VbsStoreFilter.BranchStore, pponInfo.StoreGuid.Value.ToString());
                        }

                        model.StoreName = pponInfo.StoreName;
                    }

                    model.DealUniqueId = pponInfo.UniqueId;
                    model.DealName = pponInfo.ProductName;
                    model.ExchangePeriodStartTime = pponInfo.UseStartTime.Value;
                    model.ExchangePeriodEndTime = pponInfo.UseEndTime.Value;
                    model.DealOrderEndTime = pponInfo.DealEndTime;
                    model.DealOrderStartTime = pponInfo.DealStartTime;
                    model.HideSalesInfo = Helper.IsFlagSet(pponInfo.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) && (detailId != "all" && (!isSellerPemission || storeCount > 1));

                    if (excel == (int)ExportType.SellerList)
                    {
                        sellerName = pponInfo.SellerName;

                        #region 依使用者的選擇, 篩選掉不要的分店憑證, 只留需要的

                        DataTable dt0 = _pp.GetSellerCouponList(new Guid(model.DealId));
                        IEnumerable<DataRow> rows = dt0.AsEnumerable();

                        if (model.StoreId != "null" && model.StoreId != "all")
                        {
                            rows = rows.Where(x => x.Field<Guid?>("分店代碼") == Guid.Parse(model.StoreId))
                                        .ToList();
                        }
                        dt = dt0.Clone();
                        foreach (DataRow dr in rows)
                        {
                            dt.ImportRow(dr);
                        }

                        dealIntroduction = _pp.CouponEventContentGet(CouponEventContent.Columns.BusinessHourGuid, new Guid(model.DealId)).Introduction;

                        #endregion
                    }
                }

                #endregion Ppon

                #region PiinLife

                else if (type == VbsDealType.PiinLife)
                {
                    ViewHiDealProductSellerStore hiDealCouponLists;

                    if (detailId == "all")
                    {
                        if (VbsCurrent.Is17LifeEmployee)
                        {
                            hiDealCouponLists = _hp.ViewHiDealProductSellerStoreGetToShopDealList(string.Empty, ViewHiDealProductSellerStore.Columns.ProductGuid + " = " + id).First();
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                hiDealCouponLists.ProductGuid, OrderClassification.HiDeal, couponNumber, VbsStoreFilter.All, null);
                        }
                        else
                        {
                            var allowedStoreGuid = GetAllowedStores(new Guid(id));
                            hiDealCouponLists = _hp.ViewHiDealProductSellerStoreGetToShopDealList(string.Join(",", allowedStoreGuid), ViewHiDealProductSellerStore.Columns.ProductGuid + " = " + id).First();
                            items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                                hiDealCouponLists.ProductGuid, OrderClassification.HiDeal, couponNumber, VbsStoreFilter.BranchStoreAndHeaderOffice, string.Join(",", allowedStoreGuid));
                        }
                        model.StoreName = "所有分店";
                    }
                    else if (detailId == "null")
                    {
                        hiDealCouponLists = _hp.ViewHiDealProductSellerStoreGetToShopDealList(string.Empty, ViewHiDealProductSellerStore.Columns.ProductGuid + " = " + id).First();

                        items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                            hiDealCouponLists.ProductGuid, OrderClassification.HiDeal, couponNumber, VbsStoreFilter.HeaderOffice, null);
                    }
                    else
                    {
                        hiDealCouponLists = _hp.ViewHiDealProductSellerStoreGetToShopDealList(string.Empty, ViewHiDealProductSellerStore.Columns.ProductGuid + " = " + id, ViewHiDealProductSellerStore.Columns.StoreGuid + " = " + detailId).First();
                        items = _vp.GetCashTrustLogGetListByProductGuidAndStore(
                            hiDealCouponLists.ProductGuid, OrderClassification.HiDeal, couponNumber, VbsStoreFilter.BranchStore, hiDealCouponLists.StoreGuid.Value.ToString());
                        model.StoreName = hiDealCouponLists.StoreName;
                    }
                    model.DealName = hiDealCouponLists.ProductName;
                    model.DealUniqueId = hiDealCouponLists.ProductId;
                    model.ExchangePeriodStartTime = hiDealCouponLists.UseStartTime.Value;
                    model.ExchangePeriodEndTime = hiDealCouponLists.UseEndTime.Value;
                    model.DealOrderEndTime = hiDealCouponLists.DealEndTime.Value;
                    model.DealOrderStartTime = hiDealCouponLists.DealStartTime.Value;
                    model.HideSalesInfo = false;

                    if (excel == (int)ExportType.SellerList)
                    {
                        sellerName = hiDealCouponLists.SellerName;
                        dt = _hp.GetHiDealCouponListForExport(new Guid(model.DealId), model.StoreId != "null" && model.StoreId != "all" ? new Guid(model.StoreId) : Guid.Empty);
                        dealIntroduction = _hp.HiDealContentGetAllByDealId(hiDealCouponLists.DealId).Where(x => x.Seq == 5).FirstOrDefault().Context;
                    }
                }

                #endregion PiinLife

                if (items.Count > 0)
                {
                    var vpcc = _pp.ViewPponCouponGetIsReservationLockCouponList(Guid.Parse(id))
                                    .AsEnumerable()
                                    .ToDictionary(x => x.Field<string>("sequence_number"), x => x.Field<bool?>("is_reservation_lock"));
                    var vbscrc = _bp.ViewBookingSystemCouponReservationGetByOrderKey(items.Select(x => x.OrderGuid.ToString()))
                                    .AsEnumerable()
                                    .ToDictionary(x => x.Field<string>("coupon_sequence_number"), x => x.Field<bool>("is_cancel"));
                    foreach (var item in items)
                    {
                        var createId = item.VerifyCreateId;
                        //憑證核銷狀態 及 統計 核銷/退貨/未使用 憑證數量
                        var verifiedState = VbsCouponVerifyState.Null;
                        switch (item.Status)
                        {
                            case ((int)TrustStatus.Initial):
                            case ((int)TrustStatus.Trusted):
                                bool isReturning = false;
                                if (item.OrderGuid != null)
                                {
                                    IList<BizLogic.Model.Refund.ReturnFormEntity> returnForms = BizLogic.Model.Refund.ReturnFormRepository.FindAllByOrder(item.OrderGuid.Value)
                                                                            .Where(x => x.ProgressStatus == ProgressStatus.Processing || x.ProgressStatus == ProgressStatus.AtmQueueing).ToList();
                                    var returningForms = returnForms.Where(x => x.ProgressStatus == ProgressStatus.Processing || x.ProgressStatus == ProgressStatus.AtmQueueing);
                                    foreach (var returnForm in returningForms)
                                    {
                                        List<Guid> trustId = returnForm.ReturnFormRefunds
                                            .Select(x => x.TrustId).ToList();
                                        if (trustId.Contains(item.TrustId ?? Guid.Empty))
                                        {
                                            isReturning = true;
                                        }
                                    }
                                }
                                if (isReturning)
                                {
                                    verifiedState = VbsCouponVerifyState.Returning;
                                    countSummary.PlusReturning();
                                }
                                else
                                {
                                    verifiedState = VbsCouponVerifyState.UnUsed;
                                    countSummary.PlusUnused();
                                }

                                break;

                            case ((int)TrustStatus.Verified):
                                createId = string.IsNullOrEmpty(createId) ? "未設定" : createId;
                                if (createId.Contains("seller:") || createId.Contains("store guid:"))
                                {
                                    createId = "舊核銷系統核銷";
                                }
                                if (Helper.IsFlagSet((item.Status ?? 0), TrustStatus.Verified)
                                    && Helper.IsFlagSet((item.SpecialStatus ?? 0), TrustSpecialStatus.VerificationForced)
                                    && item.VerifyCreateId.Equals(config.ForcedVerificationAccount))
                                {
                                    verifiedState = VbsCouponVerifyState.ExpiredVerified;
                                    countSummary.PlusExpiredVerified();
                                }
                                else
                                {
                                    verifiedState = VbsCouponVerifyState.Verified;
                                    countSummary.PlusVerified();
                                }
                                break;

                            case ((int)TrustStatus.Refunded):
                            case ((int)TrustStatus.Returned):
                                verifiedState = VbsCouponVerifyState.Return;
                                countSummary.PlusReturn();
                                //加總結檔前退貨之憑證
                                if (item.ReturnTime.HasValue && item.ReturnTime.Value < model.DealOrderEndTime)
                                {
                                    countSummary.PlusBeforeDealEndReturn();
                                }
                                break;

                            case ((int)TrustStatus.ATM):
                                break;
                        }

                        //核銷狀態未給值視為無效憑證
                        if (verifiedState != VbsCouponVerifyState.Null)
                        {
                            //強制退貨 須列入已核銷憑證 故狀態會顯示核銷 並顯示當初核銷時間及帳號
                            int specialStatus = item.SpecialStatus.Value;
                            if (Helper.IsFlagSet(specialStatus, TrustSpecialStatus.ReturnForced))
                            {
                                verifiedState = VbsCouponVerifyState.Verified;
                                createId = string.IsNullOrEmpty(createId) ? "未設定" : createId;
                                if (createId.Contains("seller:") || createId.Contains("store guid:"))
                                {
                                    createId = "舊核銷系統核銷";
                                }
                                countSummary.PlusForceReturn();
                            }
                            var info = new VbsVerificationCouponInfo
                            {
                                CouponId = item.CouponSequenceNumber,
                                ProductName = (item.ItemName != null) ? item.ItemName.Replace("&nbsp", " ") : model.DealName,
                                StoreName = item.StoreName,
                                VerifyState = verifiedState,
                                ReturnDateTime = item.ReturnTime,
                                VerifiedDateTime = item.VerifyTime,
                                UserName = createId,
                                IsBooked = vbscrc.ContainsKey(item.CouponSequenceNumber) ? (bool?)(!vbscrc[item.CouponSequenceNumber]) : null,
                                IsReservationLock = vpcc.ContainsKey(item.CouponSequenceNumber) ? vpcc[item.CouponSequenceNumber] : null
                            };
                            infos.Add(info);
                        }
                    }
                }

                model.CountSummary = countSummary;
            }

            #endregion Verification coupon data set up

            //判斷頁面上輸入之查詢條件[核銷狀態]value
            model.VerifyState = verifyState;

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
            ViewBag.StoreCount = Session[VbsSession.StoreCount.ToString()];

            switch (excel)
            {
                //憑證清冊
                case (int)ExportType.CouponList:
                    model.CouponInfos = VerificationFacade.GetPagerVbsVerificationCouponInfos(
                        infos, verifyState, dateFrom, dateTo, int.MaxValue, 0, sortCol, sortDesc.Value, VbsCurrent.AccountId);
                    MemoryStream outputStream = VerificationFacade.GetVbsVerificationCouponInfosExcel(model.CouponInfos);
                    return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = HttpUtility.UrlEncode("VerificationCouponListReport.xls") };
                //商家清冊
                case (int)ExportType.SellerList:
                    model.CouponInfos = VerificationFacade.GetPagerVbsVerificationCouponInfos(
                        infos, verifyState, dateFrom, dateTo, 20, 0, sortCol, sortDesc.Value, VbsCurrent.AccountId);

                    OrderFacade.ExportCouponList(new Guid(model.DealId), model.DealName, dealIntroduction, model.DealOrderStartTime,
                        model.DealOrderEndTime, model.ExchangePeriodStartTime, model.ExchangePeriodEndTime, dt,
                        model.DealUniqueId, sellerName, model.StoreName, countSummary.AllCount);
                    break;
                default:
                    model.CouponInfos = VerificationFacade.GetPagerVbsVerificationCouponInfos(
                        infos, verifyState, dateFrom, dateTo, 20, pageNumber.Value - 1, sortCol, sortDesc.Value, VbsCurrent.AccountId);
                    break;
            }

            return View("VerificationCouponList", model);
        }

        [VbsAuthorize]
        public ActionResult VerificationCouponListByStore(string storeId, int? day, int? pageNumber, string only, int? excel, string mailTo)
        {
            bool isPostBack = day != null || pageNumber != null;
            pageNumber = pageNumber == null ? 0 : pageNumber - 1;
            day = day ?? 1;
            excel = excel ?? 0;
            const int pageSize = 10;

            var now = DateTime.Now;
            var model = new VerificationCouponListByStoreModel();
            var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.VerifyShop, true);
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            var deals = allowedDealUnits.Where(x => now.Date >= x.DealUseStartTime && now.Date <= x.DealUseEndTime.Value.AddDays(day.Value));

            if (deals.Count() > 0)
            {
                deals.GroupBy(x => new { x.StoreGuid, x.StoreName })
                     .Select(s => new { StoreGuid = s.Key.StoreGuid.Value, StoreName = s.Key.StoreName })
                     .ForEach(x => model.StoreList.Add(x.StoreGuid.ToString(), x.StoreName.Length > 9 ? x.StoreName.Substring(9, x.StoreName.Length - 9) : x.StoreName));

                model.VerifyTime = new Dictionary<string, string>
                    {
                        {"1","24小時以內" },
                        {"3","72小時以內" },
                        {"7","一星期以內" },
                        {"30","一個月以內" },
                        {"90","三個月以內" },
                        {"0","所有可兌換商品" },
                    };

                var isSeller = deals.Any(x => x.IsSellerPermissionAllow);
                Guid? storeGuid = null;
                if (isSeller)
                {
                    model.StoreList.Add("all", "所有分店");
                    model.IsSellerPermission = true;
                }

                if (!string.IsNullOrEmpty(storeId) && storeId != "all")
                {
                    storeGuid = isSeller ? Guid.Parse(storeId) : storeGuid;
                    model.StoreId = storeGuid.ToString();
                }
                storeGuid = !isSeller || storeId == "all" ? null : storeGuid;
                only = !isPostBack && !isSeller ? "on" : only;
                model.IsOnlyAccount = !string.IsNullOrEmpty(only);
                var bids = deals.GroupBy(x => new { x.MerchandiseId, x.MerchandiseGuid }).Select(s => new { MerchandiseId = s.Key.MerchandiseId, MerchandiseGuid = s.Key.MerchandiseGuid }).ToDictionary(d => d.MerchandiseId, d => d.MerchandiseGuid);
                List<VbsVerificationCoupon> list = new List<VbsVerificationCoupon>();
                foreach (var bid in bids)
                {
                    var items = _vp.GetCashTrustLogGetListByProductGuid(bid.Value.Value, OrderClassification.LkSite).Where(x => x.Status == (int)TrustStatus.Verified && (storeGuid != null ? x.StoreGuid == storeGuid : (isSeller ? true : model.StoreList.Any(y => y.Key == x.StoreGuid.ToString()))) && (only == "on" ? x.VerifyCreateId == VbsCurrent.AccountId : true));
                    items.ForEach(x => list.Add(new VbsVerificationCoupon
                    {
                        DealName = string.Format(@"[{0}]{1}", bid.Key, x.ItemName),
                        VerifyCount = 1,
                        CouponId = x.CouponSequenceNumber,
                        VerifiedDateTime = x.VerifyTime,
                        VerifyAccount = x.VerifyCreateId,
                        VerifiedStore = x.StoreName,
                    }));
                }

                DateTime? newVerifyTime;
                List<VbsVerificationCoupon> data = new List<VbsVerificationCoupon>();
                if (list.Count > 0)
                {
                    if (!isSeller)
                    {
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true) && x.VerifyAccount == VbsCurrent.AccountId).OrderByDescending(x => x.VerifiedDateTime).ForEach(x => data.Add(x));
                        newVerifyTime = data.Count > 0 ? data.Max(m => m.VerifiedDateTime) : null;
                        if (data.Count > 0)
                        {
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true) && x.VerifyAccount != VbsCurrent.AccountId).OrderByDescending(x => x.VerifiedDateTime).ForEach(x => data.Add(x));
                        if (newVerifyTime == null)
                        {
                            newVerifyTime = data.Max(m => m.VerifiedDateTime);
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                    }
                    else
                    {
                        newVerifyTime = list.Max(m => m.VerifiedDateTime);
                        list.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        data = list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)).OrderByDescending(x => x.VerifiedDateTime).ToList();
                    }
                }

                if (excel == (int)ExportType.CouponList)
                {
                    //憑證清冊
                    MemoryStream outputStream = VerificationFacade.GetVbsVerificationCouponInfosExcelByStore(data,
                                        string.IsNullOrEmpty(mailTo) ? mailTo : string.Format("{0},{1},{2}", VbsCurrent.AccountId, Session[VbsSession.UserName.ToString()], mailTo));
                    if (outputStream != null)
                    {
                        return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = HttpUtility.UrlEncode("VerificationCouponListReport.xls") };
                    }
                }
                model.Coupons = new PagerList<VbsVerificationCoupon>(data.Skip(pageNumber.Value * pageSize).Take(pageSize),
                    new PagerOption(pageSize, pageNumber.Value * pageSize, VbsVerificationCoupon.Columns.VerifiedDateTime, false),
                    pageNumber.Value, data.Count);

                return View("VerificationCouponListByStore", model);
            }
            Session[VbsSession.ParentActionName.ToString()] = "VerificationCouponListByStore";
            return RedirectToAction("NoRecord");
        }

        #endregion 核銷查詢

        #region 對帳查詢

        [VbsAuthorize]
        public ActionResult EmployeeBalanceDealList(EmployeeBalanceDealListModel model)
        {
            bool unReceiptReceived;
            List<Guid> unReceiptReceivedDealGuid;
            List<VbsBalanceDealInfo> infos = new List<VbsBalanceDealInfo>();

            if (config.IsEnableVbsNewUi)
            {
                if (Request.Params["d"] == ((int)DeliveryType.ToShop).ToString())
                {
                    infos = TransformToDealInfos(model.SearchCondition, model.SearchExpression, out unReceiptReceived, out unReceiptReceivedDealGuid, (int)DeliveryType.ToShop);
                }
                else if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString())
                {
                    infos = TransformToDealInfos(model.SearchCondition, model.SearchExpression, out unReceiptReceived, out unReceiptReceivedDealGuid, (int)DeliveryType.ToHouse);
                }
                else
                {
                    infos = TransformToDealInfos(model.SearchCondition, model.SearchExpression, out unReceiptReceived, out unReceiptReceivedDealGuid);
                }
            }
            else
            {
                infos = TransformToDealInfos(model.SearchCondition, model.SearchExpression, out unReceiptReceived, out unReceiptReceivedDealGuid);
            }

            model.ShowBalanceSheetPack = unReceiptReceived;
            model.PackDealGuids = unReceiptReceivedDealGuid;
            model.DealInfos = infos;

            if (string.Equals("POST", HttpContext.Request.HttpMethod) && int.Equals(0, infos.Count))
            {
                model.IsResultEmpty = true;
            }

            Session[VbsSession.DealCount.ToString()] = model.DealInfos.Count();
            Session[VbsSession.ParentActionName.ToString()] = "EmployeeBalanceDealList";
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];

            return View(model);
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceDealList(VbsBalanceState? state)
        {
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }
            //if (IsTemporaryTimeout())
            //{
            //    return RedirectToAction("Logout");
            //}

            state = state ?? VbsBalanceState.Balancing;
            var vdlModel = new BalanceDealListModel();
            var infos = new List<VbsBalanceDealInfo>();
            var wmsInfos = new List<VbsBalanceWmsInfo>();
            bool isShowContract = false;
            var unReceiptReceived = false;
            var wmsUnReceiptReceived = false;

            #region Demo data set up

            //Demo user登入 則替換Demo版對帳查詢檔次列表資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                infos = VbsCurrent.DemoData.BalanceSheetDealInfos
                        .Where(x => x.BalanceState == state.Value)
                        .ToList();
            }

            #endregion Demo data set up

            #region balance deal data set up

            else
            {
                //取得有權限檢視之檔次
                var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.ViewBalanceSheet, false);

                var dealLists = GetAllowedViewBalanceSheetListCollection(allowedDealUnits);
                
                if (dealLists.Any())
                {
                    if (config.IsEnableVbsNewUi)
                    {
                        if (Request.Params["d"] == ((int)DeliveryType.ToShop).ToString())
                        {
                            infos = GetBalanceDealInfo(dealLists, state.Value, out unReceiptReceived, (int)DeliveryType.ToShop);
                        }
                        else if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString())
                        {
                            infos = GetBalanceDealInfo(dealLists, state.Value, out unReceiptReceived, (int)DeliveryType.ToHouse);
                        }
                        else
                        {
                            infos = GetBalanceDealInfo(dealLists, state.Value, out unReceiptReceived);
                        }
                    }
                    else
                    {
                        infos = GetBalanceDealInfo(dealLists, state.Value, out unReceiptReceived);
                    }
                }

                


                //檢查是否有換約需要商家確認
                if (Request.Params["d"] == ((int)DeliveryType.ToShop).ToString())
                {
                    //在地合約
                    if (!string.IsNullOrEmpty(config.SellerContractVersionPpon))
                    {
                        var sellers = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true);
                        foreach (var g in sellers.Keys)
                        {
                            if (_sp.SellerGet(g).ContractVersionPpon != config.SellerContractVersionPpon)
                            {
                                isShowContract = true;
                                continue;
                            }
                        }
                    }
                }
                else if (Request.Params["d"] == ((int)DeliveryType.ToHouse).ToString())
                {
                    //宅配合約
                    if (!string.IsNullOrEmpty(config.SellerContractVersionHouse))
                    {
                        var sellers = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true);
                        foreach (var g in sellers.Keys)
                        {
                            if (_sp.SellerGet(g).ContractVersionHouse != config.SellerContractVersionHouse)
                            {
                                isShowContract = true;
                                continue;
                            }
                        }
                    }
                }

                ViewBag.ShowContract = isShowContract;
            }

            #endregion balance deal data set up

            #region balance wms data set up
            var wmsSellers = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true);
            Dictionary<int, int> seq = new Dictionary<int, int>();

            foreach (KeyValuePair<Guid, string> seller in wmsSellers)
            {
                BalanceSheetWmCollection bswc = _ap.BalanceSheetWmsGetBySeller(seller.Key, state.ToString());

                foreach (BalanceSheetWm bsw in bswc)
                {
                    if (!seq.ContainsKey(bsw.Year + bsw.Month))
                        seq.Add(bsw.Year + bsw.Month, 1);
                    else
                        seq[bsw.Year + bsw.Month]++;

                    wmsInfos.Add(new VbsBalanceWmsInfo
                    {
                        BalanceSheetWmsID = bsw.Id,
                        Name = bsw.IntervalStart.ToString("yyyy年MM月") + " 倉儲費用對帳0" + seq[bsw.Year + bsw.Month],
                        IntervalStartTime = bsw.IntervalStart,
                        IntervalEndTime = bsw.IntervalEnd,
                        ConfirmedTime = bsw.ConfirmedTime ?? null,
                        IsReceiptReceived = bsw.IsReceiptReceived,
                        BalanceState = (bsw.ConfirmedUserName != null) ? VbsBalanceState.Finished : VbsBalanceState.Balancing
                    });

                }

            } 
            #endregion

            wmsUnReceiptReceived = wmsInfos.Any(x =>
            {
                return !x.IsReceiptReceived;
            });

            vdlModel.ShowBalanceSheetPack = unReceiptReceived || wmsUnReceiptReceived;
            vdlModel.DealInfos = infos;
            vdlModel.WmsInfos = wmsInfos;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.DealCount.ToString()] = vdlModel.DealInfos.Count();
            Session[VbsSession.ParentActionName.ToString()] = "BalanceDealList";

            if (infos.Any())
            {
                Session[VbsSession.StoreCount.ToString()] = vdlModel.DealInfos.First().StoreCount;
            }

            return View("BalanceDealList", vdlModel);
        }

        /// <summary>
        /// PCHOME倉儲費匯出
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult BalanceSheetWmsFeeExport(int balanceSheetWmsId)
        {
            BalanceSheetWm bsw = _ap.BalanceSheetWmsGet(balanceSheetWmsId);
            MemoryStream outputStream = null;
            
            outputStream = BalanceSheetWmsFeeExportToExcel(balanceSheetWmsId, bsw);
            return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = bsw.IntervalStart.ToString("yyyy年MM月") + "倉儲費用" + ".xls" };
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceStoreList(Guid id, VbsDealType type)
        {
            var vdlModel = new BalanceStoreListModel();

            #region Demo data set up

            //Demo user登入 則替換Demo版對帳查詢分店列表資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                var dealInfo = VbsCurrent.DemoData.BalanceSheetDealInfos.First(x => x.DealId == id);
                var infos = VbsCurrent.DemoData.BalanceSheetStoreInfos.Where(x => x.StoreId.Substring(0, 8) == id.ToString().Substring(0, 8)).ToList();

                vdlModel.DealId = id;
                vdlModel.DealType = type;
                vdlModel.DealName = dealInfo.DealName;
                vdlModel.DealUniqueId = dealInfo.DealUniqueId;
                vdlModel.RemittanceType = (int)RemittanceType.Monthly;
                vdlModel.StoreInfos = infos;
            }

            #endregion Demo data set up

            #region balance store data set up

            else
            {
                var infos = new List<VbsBalanceStoreInfo>();
                var dealSalesCounts = new List<VendorDealSalesCount>();
                var bzModel = type == VbsDealType.Ppon
                                ? BusinessModel.Ppon
                                : BusinessModel.PiinLife;
                VbsBalanceState balanceState;
                vdlModel.DealId = id;
                vdlModel.DealType = type;

                var storeLists = GetBalanceStoreList(VbsCurrent.Is17LifeEmployee, bzModel, id);

                #region Ppon

                if (type == VbsDealType.Ppon)
                {
                    //取得各核銷狀態數量
                    dealSalesCounts = _vp.GetPponVerificationSummaryGroupByStore(storeLists.Select(x => x.ProductGuid).Distinct());
                }

                #endregion Ppon

                #region PiinLife

                else if (type == VbsDealType.PiinLife)
                {
                    //取得各核銷狀態數量
                    dealSalesCounts = _vp.GetHiDealVerificationSummaryGroupByStore(storeLists.Select(x => x.UniqueId).Distinct());
                }

                #endregion PiinLife

                //不放在下方 group by 計算(不分月/週對帳單)，需另外統計 避免未產生月對帳單之檔次 被誤判為對帳結束
                Dictionary<Guid, int> dicBalancingCount = storeLists.Where(x => x.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                                                                                x.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet)
                                                                    .GroupBy(x => x.StoreGuid)
                                                                    .ToDictionary(x => x.Key.HasValue
                                                                                        ? x.Key.Value
                                                                                        : Guid.Empty,
                                                                                  x => x.Count(b => !b.IsConfirmedReadyToPay));
                var balancingList = storeLists.GroupBy(x => new
                {
                    x.ProductGuid,
                    x.Name,
                    x.UniqueId,
                    x.StoreGuid,
                    x.StoreName
                })
                                              .Select(x => new
                                              {
                                                  x.Key.ProductGuid,
                                                  x.Key.Name,
                                                  x.Key.UniqueId,
                                                  x.Key.StoreGuid,
                                                  x.Key.StoreName,
                                                  x.OrderByDescending(t => t.IntervalEnd).First().GenerationFrequency,
                                                  IntervalEnd = x.Max(t => t.IntervalEnd),
                                                  x.First().UseStartTime
                                              });

                if (balancingList.Any())
                {
                    foreach (var storeList in balancingList)
                    {
                        balanceState = VbsBalanceState.Balancing;
                        Guid storeGuid = storeList.StoreGuid.HasValue
                                             ? storeList.StoreGuid.Value
                                             : Guid.Empty;
                        if (dicBalancingCount.ContainsKey(storeGuid))
                        {
                            //無論週月結 依照該檔次產出最後一筆之月 or 人工對帳單資料來判斷對帳單確認狀態
                            //月 or 人工對帳單之interval_end於兌換期間內皆顯示對帳中  於兌換期間外 若對帳單已確認 則顯示對帳結束
                            var verificationPolicy = new VerificationPolicy();
                            if (!verificationPolicy.IsInVerifyTimeRange(BusinessModel.Ppon, storeList.ProductGuid, storeList.StoreGuid, storeList.IntervalEnd, true) &&
                                dicBalancingCount[storeGuid].Equals(0))
                            {
                                balanceState = VbsBalanceState.Finished;
                            }
                        }
                        //取得各核銷狀態數量
                        var dealSaleCount = dealSalesCounts.FirstOrDefault(x => x.MerchandiseGuid == storeList.ProductGuid && x.StoreGuid == storeList.StoreGuid);
                        int verifiedCount = 0, returnCount = 0, unVerifyCount = 0;
                        if (dealSaleCount != null)
                        {
                            verifiedCount = dealSaleCount.VerifiedCount;
                            unVerifyCount = dealSaleCount.UnverifiedCount;
                            returnCount = dealSaleCount.ReturnedCount;
                        }

                        var info1 = new VbsBalanceStoreInfo
                        {
                            StoreName = storeList.StoreName,
                            StoreId = (string.IsNullOrEmpty(storeList.StoreGuid.ToString())) ? "null" : storeList.StoreGuid.ToString(),
                            SaleCount = verifiedCount + unVerifyCount + returnCount,
                            ReturnCount = returnCount,
                            VerifiedCount = verifiedCount,
                            UnUsedCount = unVerifyCount,
                            BalanceState = balanceState,
                            GenerationFrequency = storeList.GenerationFrequency,
                            UseStartTime = storeList.UseStartTime.Value
                        };
                        infos.Add(info1);
                    }
                    vdlModel.DealName = storeLists.First().Name;
                    vdlModel.DealUniqueId = storeLists.First().UniqueId;
                    vdlModel.RemittanceType = _pp.DealAccountingGet(id).RemittanceType;
                }

                vdlModel.StoreInfos = infos;
            }

            #endregion balance store data set up

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            Session[VbsSession.StoreCount.ToString()] = vdlModel.StoreInfos.Count();

            if (vdlModel.StoreInfos.Count() > 1)
            {
                return View("BalanceStoreList", vdlModel);
            }
            if (vdlModel.StoreInfos.Count() == 1)
            {
                var generationFrequency = vdlModel.StoreInfos.First().GenerationFrequency;
                if (generationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                    vdlModel.RemittanceType == (int)RemittanceType.Flexible ||
                    vdlModel.RemittanceType == (int)RemittanceType.Weekly)
                {
                    if (config.IsEnableVbsNewUi)
                    {
                        return RedirectToAction("BalanceSheetsMonth", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId, d = Request.Params["d"] });
                    }
                    else
                    {
                        return RedirectToAction("BalanceSheetsMonth", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId });
                    }
                }
                else if (generationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                {
                    if (config.IsEnableVbsNewUi)
                    {
                        return RedirectToAction("BalanceSheetsWeek", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId, d = Request.Params["d"] });
                    }
                    else
                    {
                        return RedirectToAction("BalanceSheetsWeek", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId });
                    }
                }
                else if (generationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
                {
                    if (config.IsEnableVbsNewUi)
                    {
                        return RedirectToAction("BalanceSheetsManual", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId, d = Request.Params["d"] });
                    }
                    else
                    {
                        return RedirectToAction("BalanceSheetsManual", new { id = id, type = type, detailId = vdlModel.StoreInfos.First().StoreId });
                    }
                }
            }

            Session[VbsSession.ParentActionName.ToString()] = "BalanceStoreList";
            return RedirectToAction("NoRecord");
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetsWeek(Guid id, VbsDealType type, string detailId, int? balanceSheetId)
        {
            var vdlModel = new BalanceSheetsWeekModel();
            var infos = new List<VbsBalanceCouponInfo>();
            var weekInfos = new List<VbsBalanceSheetsWeekInfo>();

            #region Demo data set up

            //Demo user登入 則替換Demo版週對帳單資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                var dealInfo = VbsCurrent.DemoData.BalanceSheetDealInfos.First(x => x.DealId == id);
                if (!balanceSheetId.HasValue)
                {
                    balanceSheetId = VbsCurrent.DemoData.BalanceSheetWeekInfos.OrderByDescending(x => x.BalanceSheetId).First().BalanceSheetId;
                }
                var bsWeekInfo = VbsCurrent.DemoData.BalanceSheetWeekInfos.First(x => x.BalanceSheetId == balanceSheetId);
                weekInfos = VbsCurrent.DemoData.BalanceSheetWeekInfos.OrderByDescending(x => x.BalanceSheetId).ToList();
                vdlModel.DealId = id;
                vdlModel.DealName = dealInfo.DealName;
                vdlModel.DealType = type;
                vdlModel.DealUniqueId = dealInfo.DealUniqueId;
                vdlModel.ExchangePeriodStartTime = dealInfo.ExchangePeriodStartTime;
                vdlModel.ExchangePeriodEndTime = dealInfo.ExchangePeriodEndTime;
                vdlModel.StoreId = detailId;

                switch (detailId)
                {
                    case "all":
                        vdlModel.StoreName = "所有分店";
                        break;
                    case "null":
                        vdlModel.StoreName = null;
                        break;
                    default:
                        vdlModel.StoreName = VbsCurrent.DemoData.BalanceSheetStoreInfos.First(x => x.StoreId == detailId).StoreName;
                        break;
                }

                vdlModel.ReturnCount = 9;
                vdlModel.VerifiedCount = 7;
                vdlModel.UnDoCount = 1;
                vdlModel.PaymentPeriodStartTime = bsWeekInfo.WeekIntervalStartTime;
                vdlModel.PaymentPeriodEndTime = bsWeekInfo.WeekIntervalEndTime;
                vdlModel.PredictPaymentDate = bsWeekInfo.WeekIntervalEndTime.AddDays(5).ToString("yyyy/MM/dd");
                vdlModel.FundTransferInfo = new FundTransferInfo(BalanceSheetType.WeeklyPayWeekBalanceSheet);
                foreach (var item in VbsCurrent.DemoData.BalanceSheetCouponInfos)
                {
                    item.StoreName = vdlModel.StoreName;
                    item.UserName = Session[VbsSession.UserId.ToString()].ToString();
                    switch (item.UnDoState)
                    {
                        case BalanceSheetDetailStatus.Normal:
                        case BalanceSheetDetailStatus.Undo:
                            item.VerifiedDateTime = vdlModel.PaymentPeriodStartTime.AddDays(1);
                            break;

                        case BalanceSheetDetailStatus.Deduction:
                            item.VerifiedDateTime = vdlModel.PaymentPeriodStartTime.AddDays(2);
                            item.UnDoDateTime = vdlModel.PaymentPeriodStartTime.AddDays(3);
                            break;
                        case BalanceSheetDetailStatus.NoPay:
                        case BalanceSheetDetailStatus.UndoNoPay:
                            item.VerifiedDateTime = vdlModel.PaymentPeriodStartTime.AddDays(1);
                            break;
                        default:
                            break;
                    }
                    infos.Add(item);
                }
            }

            #endregion Demo data set up

            #region Weekly balance sheet data set up

            else
            {
                Guid? storeGuid = null;
                if (!detailId.Equals("null"))
                {
                    storeGuid = Guid.Parse(detailId);
                }

                //檢查商家帳號是否有檢視該檔次分店權限
                var isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, storeGuid, VbsRightFlag.ViewBalanceSheet);

                if (!isAllowedDealOrStore)
                {
                    return RedirectToAction("RedirectToActionAfterLogin");
                }

                var bzModel = type == VbsDealType.Ppon
                                  ? BusinessModel.Ppon
                                  : BusinessModel.PiinLife;

                var balanceSheetLists = GetBalanceSheets(id, storeGuid, bzModel);
                int bsManualCount = balanceSheetLists.Count(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet);

                //取得週對帳單別起訖日期
                var weeklyLists = balanceSheetLists.Where(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                                                   .OrderByDescending(x => x.IntervalStart);
                if (weeklyLists.Any())
                {
                    foreach (var balanceSheetWeekList in weeklyLists)
                    {
                        //頁面輸入對帳單Id預設值為最近之對帳單Id
                        if (!balanceSheetId.HasValue)
                        {
                            balanceSheetId = balanceSheetWeekList.Id;
                        }

                        var weekInfo1 = new VbsBalanceSheetsWeekInfo
                        {
                            BalanceSheetId = balanceSheetWeekList.Id,
                            WeekIntervalStartTime = balanceSheetWeekList.IntervalStart,
                            WeekIntervalEndTime = balanceSheetWeekList.IntervalEnd.AddDays(-1)
                        };
                        weekInfos.Add(weekInfo1);
                    }
                    //根據頁面輸入之對帳單Id取得對帳單內容 預設為取得最新對帳單內容
                    var balanceSheetList = balanceSheetLists.First(x => x.Id == balanceSheetId.Value);

                    //統計該對帳單核銷、反核銷及退貨數量
                    var dicCountStatus = GetQuantityInfoByBalanceSheetId(balanceSheetList.Id, balanceSheetList.IntervalStart, balanceSheetList.GenerationFrequency, balanceSheetList.ProductGuid);

                    var bsModel = new BalanceSheetModel(balanceSheetList.Id);

                    vdlModel.BalanceSheetsWeekInfos = new List<VbsBalanceSheetsWeekInfo>();
                    vdlModel.DealId = id;
                    vdlModel.DealName = balanceSheetList.Name;
                    vdlModel.DealType = type;
                    vdlModel.DealUniqueId = balanceSheetList.UniqueId;
                    vdlModel.ExchangePeriodStartTime = balanceSheetList.UseStartTime.Value;
                    vdlModel.ExchangePeriodEndTime = balanceSheetList.UseEndTime.Value;
                    vdlModel.RemittanceType = balanceSheetList.RemittanceType;
                    vdlModel.StoreId = detailId;
                    vdlModel.StoreName = balanceSheetList.StoreName;
                    vdlModel.ReturnCount = dicCountStatus["return"];
                    vdlModel.VerifiedCount = dicCountStatus["verified"];
                    vdlModel.UnDoCount = dicCountStatus["undo"];
                    vdlModel.VerifiedTotalCount = dicCountStatus["verifiedTotal"];
                    vdlModel.SlottingFeeQuantity = dicCountStatus["slottingFeeQuantity"];
                    vdlModel.PaymentPeriodStartTime = balanceSheetList.IntervalStart;
                    vdlModel.PaymentPeriodEndTime = balanceSheetList.IntervalEnd.AddDays(-1);
                    vdlModel.BalanceSheetManualCount = bsManualCount;
                    vdlModel.FundTransferInfo = bsModel.TransferInfo;
                    vdlModel.PredictPaymentDate = bsModel.TransferInfo.AccountsPayable <= 0
                                                    ? "無須付款"
                                                    : balanceSheetList.IntervalEnd.AddDays(4).ToString("yyyy/MM/dd");
                    //抓取憑證核銷紀錄
                    infos = GetBalanceSheetDetailList(balanceSheetId.Value);
                    vdlModel.CrossMonthPaymentInfos = GetCrossMonthPaymentInfos(balanceSheetList, infos);
                }
            }

            #endregion Weekly balance sheet data set up

            vdlModel.BalanceSheetsWeekInfos = weekInfos;
            vdlModel.CouponInfos = infos;
            vdlModel.BalanceDealInfos = Session[VbsSession.BalanceDealInfos.ToString()] as IEnumerable<VbsBalanceDealInfo>;

            

            if (vdlModel.BalanceSheetsWeekInfos.Any())
            {
                ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                ViewBag.UserId = Session[VbsSession.UserId.ToString()];
                ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
                ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
                ViewBag.StoreCount = Session[VbsSession.StoreCount.ToString()];
                ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];

                return View("BalanceSheetsWeek", vdlModel);
            }

            Session[VbsSession.ParentActionName.ToString()] = "BalanceSheetsWeek";
            return RedirectToAction("NoRecord");
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetsDeliver(Guid id)
        {
            //檢查商家帳號是否有檢視該檔次分店權限
            var isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, null, VbsRightFlag.ViewBalanceSheet);

            if (!isAllowedDealOrStore)
            {
                return RedirectToAction("RedirectToActionAfterLogin");
            }

            var deliverModel = new BalanceSheetsDeliverModel();

            try
            {
                deliverModel.ModelInit(id);

                

                if (deliverModel.DealInfos.Any())
                {
                    ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                    ViewBag.UserId = Session[VbsSession.UserId.ToString()];
                    ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
                    ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
                    ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
                    ViewBag.BalanceDealInfos = Session[VbsSession.BalanceDealInfos.ToString()] as IEnumerable<VbsBalanceDealInfo>;

                    return View("BalanceSheetsDeliver", deliverModel);
                }

                Session[VbsSession.ParentActionName.ToString()] = VbsSession.ParentActionName.ToString();
                return RedirectToAction("NoRecord");
            }
            catch (Exception ex)
            {
                throw new Exception("資料未設定完整:" + ex.Message);
            }
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetsMonth(Guid id, VbsDealType type, string detailId, int? balanceSheetId)
        {
            var vdlModel = new BalanceSheetsMonthModel();
            var infos = new List<VbsBalanceSheetsMonthInfo>();

            #region Demo data set up

            //Demo user登入 則替換Demo版月對帳單資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                BalanceSheetsMonthModel demoModel;
                List<VbsBalanceSheetsMonthInfo> demoInfos;
                SetupDemoBalanceSheetsMonth(id, type, detailId, balanceSheetId, out demoModel, out demoInfos);
                vdlModel = demoModel;
                infos = demoInfos;
            }

            #endregion Demo data set up

            #region Monthly balance sheet data set up

            else
            {
                var balanceSheetLists = new ViewBalanceSheetListCollection();
                Guid? storeGuid = null;
                if (!detailId.Equals("null"))
                {
                    storeGuid = Guid.Parse(detailId);
                }

                //檢查商家帳號是否有檢視該檔次分店權限
                bool isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, storeGuid, VbsRightFlag.ViewBalanceSheet);

                if (!isAllowedDealOrStore)
                {
                    return RedirectToAction("RedirectToActionAfterLogin");
                }

                #region Ppon

                if (type == VbsDealType.Ppon)
                {
                    balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.Ppon);
                    //發票應稅或免稅設定
                    vdlModel.IsTax = _pp.DealAccountingGet(id).IsInputTaxRequired;
                }

                #endregion Ppon

                #region PiinLife

                else if (type == VbsDealType.PiinLife)
                {
                    balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.PiinLife);
                    //發票應稅或免稅設定
                    vdlModel.IsTax = !_hp.HiDealProductGet(vdlModel.DealUniqueId).IsInputTaxFree;
                }

                #endregion PiinLife

                var bsManualCount = balanceSheetLists.Count(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet);

                vdlModel.DealId = id;
                vdlModel.DealType = type;
                vdlModel.StoreId = detailId;
                if (balanceSheetLists.Any(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                               x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                                               x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet))
                {
                    //取得月對帳單別起訖日期
                    foreach (var balanceSheetMonthList in balanceSheetLists.Where(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                                                                       x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                                                                                       x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet)
                                                                           .OrderByDescending(x => x.IntervalEnd))
                    {
                        //頁面輸入對帳單Id預設值為最近之對帳單Id
                        if (!balanceSheetId.HasValue)
                        {
                            balanceSheetId = balanceSheetMonthList.Id;
                        }
                        var bsDesc = string.Empty;
                        switch (balanceSheetMonthList.GenerationFrequency)
                        {
                            case (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet:
                                bsDesc = string.Format("{0:yyyy/MM/dd}", balanceSheetMonthList.IntervalEnd);
                                break;
                            case (int)BalanceSheetGenerationFrequency.MonthBalanceSheet:
                                bsDesc = string.Format("{0}/{1}", balanceSheetMonthList.Year, balanceSheetMonthList.Month);
                                break;
                            case (int)BalanceSheetGenerationFrequency.WeekBalanceSheet:
                                bsDesc = string.Format("{0:yyyy/MM/dd}", balanceSheetMonthList.IntervalEnd.AddDays(-1));
                                break;
                            default:
                                break;
                        }

                        var monthInfo = new VbsBalanceSheetsMonthInfo
                        {
                            BalanceSheetId = balanceSheetMonthList.Id,
                            BalanceSheetMonth = bsDesc,
                            IsConfirmedReadyToPay = balanceSheetMonthList.IsConfirmedReadyToPay
                        };
                        infos.Add(monthInfo);
                    }
                    //根據頁面輸入之對帳單Id取得對帳單內容 預設為取得最新對帳單內容
                    ViewBalanceSheetList balanceSheetList = balanceSheetLists.First(x => x.Id == balanceSheetId.Value);

                    var bsModel = new BalanceSheetModel(balanceSheetList.Id);
                    //統計該對帳單核銷、反核銷及退貨數量
                    var dicCountStatus = GetQuantityInfoByBalanceSheetId(balanceSheetList.Id, balanceSheetList.IntervalStart, balanceSheetList.GenerationFrequency, balanceSheetList.ProductGuid);

                    //週結月對帳單
                    var verifiedCount = 0;
                    var undoCount = 0;
                    if (balanceSheetList.RemittanceType == (int)RemittanceType.ManualWeekly || balanceSheetList.RemittanceType == (int)RemittanceType.AchWeekly)
                    {
                        var dicWBCountStatus = GetQuantityInfoByBalanceSheetIds(balanceSheetLists.Where(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet &&
                                                                                                             x.Year == balanceSheetList.Year &&
                                                                                                             x.Month == balanceSheetList.Month)
                                                                                                  .Select(x => x.Id).Distinct().ToList());

                        //各週對帳單數量、金額加總為該月份對帳單之數量及金額
                        verifiedCount = dicWBCountStatus["verified"];
                        undoCount = dicWBCountStatus["undo"];
                    }
                    //月結 or other remittance type
                    else
                    {
                        verifiedCount = dicCountStatus["verified"];
                        undoCount = dicCountStatus["undo"];
                    }

                    vdlModel.DealName = balanceSheetList.Name;
                    vdlModel.DealUniqueId = balanceSheetList.UniqueId;
                    vdlModel.ExchangePeriodStartTime = balanceSheetList.UseStartTime.Value;
                    vdlModel.ExchangePeriodEndTime = balanceSheetList.UseEndTime.Value;
                    vdlModel.StoreName = balanceSheetList.StoreName;
                    vdlModel.VendorReceiptType = balanceSheetList.VendorReceiptType;
                    vdlModel.ReturnCount = dicCountStatus["return"];
                    vdlModel.VerifiedCount = verifiedCount;
                    vdlModel.UnDoCount = undoCount;
                    vdlModel.PaymentPeriodStartTime = balanceSheetList.IntervalStart;
                    vdlModel.PaymentPeriodEndTime = DateTime.Compare(balanceSheetList.IntervalEnd, balanceSheetList.IntervalStart) > 0 ? balanceSheetList.IntervalEnd.AddDays(-1) : balanceSheetList.IntervalEnd;
                    vdlModel.BalanceSheetManualCount = bsManualCount;
                    vdlModel.RemittanceType = _pp.DealAccountingGet(id).RemittanceType;
                    vdlModel.BalanceSheetRemittanceType = balanceSheetList.RemittanceType;//憑證對帳單結帳方式
                    vdlModel.FundTransferInfo = bsModel.TransferInfo;
                    vdlModel.VerifiedTotalCount = dicCountStatus["verifiedTotal"];
                    vdlModel.SlottingFeeQuantity = dicCountStatus["slottingFeeQuantity"];
                    if (balanceSheetList.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet)
                    {
                        vdlModel.CrossMonthPaymentInfos = GetCrossMonthPaymentInfos(balanceSheetList, GetBalanceSheetDetailList(balanceSheetId.Value));
                    }
                }
                else
                {
                    var dealInfo = _ap.GetFlexiblePayToShopDealWithOutBalanceSheet(new List<Guid> { id }).FirstOrDefault();
                    if (dealInfo != null)
                    {
                        vdlModel.DealName = dealInfo.Name;
                        vdlModel.DealUniqueId = dealInfo.UniqueId;
                        vdlModel.StoreName = dealInfo.StoreName;
                        vdlModel.RemittanceType = dealInfo.RemittanceType;
                        vdlModel.ExchangePeriodStartTime = dealInfo.UseStartTime.Value;
                        vdlModel.ExchangePeriodEndTime = dealInfo.UseEndTime.Value;
                    }
                }
            }

            #endregion Monthly balance sheet data set up

            

            vdlModel.BalanceSheetsMonthInfos = infos;
            vdlModel.BalanceDealInfos = Session[VbsSession.BalanceDealInfos.ToString()] as IEnumerable<VbsBalanceDealInfo>;

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
            ViewBag.StoreCount = Session[VbsSession.StoreCount.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];

            return View("BalanceSheetsMonth", vdlModel);
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetsManual(Guid id, VbsDealType type, string detailId, int? balanceSheetId)
        {
            ViewBalanceSheetListCollection balanceSheetLists = new ViewBalanceSheetListCollection();
            ViewBalanceSheetBillList balanceSheetBillList = new ViewBalanceSheetBillList();
            BalanceSheetsManualModel vdlModel = new BalanceSheetsManualModel();
            List<VbsBalanceCouponInfo> infos = new List<VbsBalanceCouponInfo>();
            List<VbsBalanceSheetsWeekInfo> weekInfos = new List<VbsBalanceSheetsWeekInfo>();
            DataTable balanceSheetDetailLists = new DataTable();
            Guid? storeGuid = null;
            if (!detailId.Equals("null"))
            {
                storeGuid = Guid.Parse(detailId);
            }

            //檢查商家帳號是否有檢視該檔次分店權限
            bool isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee
                                        ? true
                                        : CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, storeGuid, VbsRightFlag.ViewBalanceSheet);

            if (!isAllowedDealOrStore)
            {
                return RedirectToAction("RedirectToActionAfterLogin");
            }

            #region Ppon

            if (type == VbsDealType.Ppon)
            {
                balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.Ppon);
                vdlModel.IsTax = _pp.DealAccountingGet(id).IsInputTaxRequired;
            }

            #endregion Ppon

            #region PiinLife

            else if (type == VbsDealType.PiinLife)
            {
                balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.PiinLife);
                vdlModel.IsTax = !_hp.HiDealProductGet(vdlModel.DealUniqueId).IsInputTaxFree;
            }

            #endregion PiinLife

            //取得週對帳單別起訖日期
            int i = 0;
            if (balanceSheetLists.Any(x => x.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)))
            {
                foreach (var balanceSheetMaunalList in balanceSheetLists
                                                    .Where(x => x.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet))
                                                    .OrderByDescending(x => x.IntervalStart))
                {
                    //頁面輸入對帳單Id預設值為最近之對帳單Id
                    if (!balanceSheetId.HasValue)
                    {
                        balanceSheetId = balanceSheetMaunalList.Id;
                    }

                    var weekInfo1 = new VbsBalanceSheetsWeekInfo
                    {
                        BalanceSheetId = balanceSheetMaunalList.Id,
                        WeekIntervalStartTime = balanceSheetMaunalList.IntervalStart,
                        WeekIntervalEndTime = balanceSheetMaunalList.IntervalEnd
                    };
                    weekInfos.Add(weekInfo1);
                    i++;
                }
                //根據頁面輸入之對帳單Id取得對帳單內容 預設為取得最新對帳單內容
                var balanceSheetList = balanceSheetLists.First(x => x.Id == balanceSheetId.Value);
                var bsModel = new BalanceSheetModel(balanceSheetList.Id);
                //抓取該檔次家匯款帳號資料
                balanceSheetBillList = _ap.ViewBalanceSheetBillListGetList(ViewBalanceSheetBillList.Columns.Id + " = " + balanceSheetList.Id).First();
                //統計該對帳單核銷、反核銷及退貨數量
                var dicCountStatus = GetQuantityInfoByBalanceSheetId(balanceSheetList.Id, balanceSheetList.IntervalStart, balanceSheetList.GenerationFrequency, balanceSheetList.ProductGuid);

                vdlModel.BalanceSheetsWeekInfos = new List<VbsBalanceSheetsWeekInfo>();
                vdlModel.DealId = id;
                vdlModel.DealName = balanceSheetList.Name;
                vdlModel.DealType = type;
                vdlModel.DealUniqueId = balanceSheetBillList.UniqueId;
                vdlModel.ExchangePeriodStartTime = balanceSheetList.UseStartTime.Value;
                vdlModel.ExchangePeriodEndTime = balanceSheetList.UseEndTime.Value;
                vdlModel.StoreId = detailId;
                vdlModel.StoreName = balanceSheetList.StoreName;
                vdlModel.ReturnCount = dicCountStatus["return"];
                vdlModel.VerifiedCount = dicCountStatus["verified"];
                vdlModel.UnDoCount = dicCountStatus["undo"];
                vdlModel.VerifiedTotalCount = dicCountStatus["verifiedTotal"];
                vdlModel.SlottingFeeQuantity = dicCountStatus["slottingFeeQuantity"];
                vdlModel.PaymentPeriodStartTime = balanceSheetList.IntervalStart;
                vdlModel.PaymentPeriodEndTime = balanceSheetList.IntervalEnd;
                vdlModel.VendorReceiptType = balanceSheetList.VendorReceiptType;
                vdlModel.RemittanceType = balanceSheetList.RemittanceType;
                vdlModel.FundTransferInfo = bsModel.TransferInfo;
            }

            vdlModel.BalanceSheetsWeekInfos = weekInfos;
            vdlModel.BalanceDealInfos = Session[VbsSession.BalanceDealInfos.ToString()] as IEnumerable<VbsBalanceDealInfo>;

            

            if (vdlModel.BalanceSheetsWeekInfos.Count() > 0)
            {
                ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                ViewBag.UserId = Session[VbsSession.UserId.ToString()];
                ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
                ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
                ViewBag.StoreCount = Session[VbsSession.StoreCount.ToString()];
                ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];

                return View("BalanceSheetsManual", vdlModel);
            }
            else
            {
                Session[VbsSession.ParentActionName.ToString()] = "BalanceSheetsManual";
                return RedirectToAction("NoRecord");
            }
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult MonthlyVerificationList(Guid id, VbsDealType type, string detailId, int balanceSheetId, bool? excel)
        {
            MonthlyVerificationListModel vdlModel = new MonthlyVerificationListModel();
            List<VbsWeeklyVerificationInfo> infos = new List<VbsWeeklyVerificationInfo>();
            List<VbsBalanceCouponInfo> couponInfos = new List<VbsBalanceCouponInfo>();
            excel = excel ?? false;

            #region Demo data set up

            //Demo user登入 則替換Demo版月核銷清單資料
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                VbsBalanceDealInfo dealInfo = VbsCurrent.DemoData.BalanceSheetDealInfos.Where(x => x.DealId == id).FirstOrDefault();
                string storeName = "";
                if (detailId != "null")
                {
                    storeName = VbsCurrent.DemoData.BalanceSheetStoreInfos.Where(x => x.StoreId == detailId).FirstOrDefault().StoreName;
                }
                VbsBalanceSheetsMonthInfo bsInfo = VbsCurrent.DemoData.BalanceSheetMonthInfos.Where(x => x.BalanceSheetId == balanceSheetId).FirstOrDefault();

                vdlModel.DealId = id;
                vdlModel.DealType = type;
                vdlModel.DealName = dealInfo.DealName;
                vdlModel.StoreId = detailId;
                vdlModel.StoreName = storeName;
                vdlModel.ExchangePeriodStartTime = dealInfo.ExchangePeriodStartTime;
                vdlModel.ExchangePeriodEndTime = dealInfo.ExchangePeriodEndTime;
                vdlModel.MonthIntervalStartTime = new DateTime(int.Parse(bsInfo.BalanceSheetMonth.Substring(0, 4)), int.Parse(bsInfo.BalanceSheetMonth.Substring(5, 2)), 1);
                vdlModel.MonthIntervalEndTime = new DateTime(int.Parse(bsInfo.BalanceSheetMonth.Substring(0, 4)), int.Parse(bsInfo.BalanceSheetMonth.Substring(5, 2)), 31); ;
                vdlModel.DealUniqueId = dealInfo.DealUniqueId;
                vdlModel.BalanceSheetMonth = bsInfo.BalanceSheetMonth;
                vdlModel.GenerationFrequency = (dealInfo.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet)
                                                ? (int)RemittanceType.AchMonthly
                                                : (int)RemittanceType.AchWeekly;
                foreach (var item in VbsCurrent.DemoData.BalanceSheetWeekInfos.Where(x => x.WeekIntervalStartTime.Month == vdlModel.MonthIntervalStartTime.Month))
                {
                    VbsWeeklyVerificationInfo info = new VbsWeeklyVerificationInfo
                    {
                        BalanceSheetId = item.BalanceSheetId,
                        WeekIntervalStartTime = item.WeekIntervalStartTime,
                        WeekIntervalEndTime = item.WeekIntervalEndTime,
                        PaymentTotal = 600,
                        PaymentState = "已付款"
                    };
                    infos.Add(info);

                    foreach (var couponItem in VbsCurrent.DemoData.BalanceSheetCouponInfos)
                    {
                        VbsBalanceCouponInfo couponInfo = new VbsBalanceCouponInfo
                        {
                            BalanceSheetId = item.BalanceSheetId,
                            CouponId = couponItem.CouponId,
                            ItemName = vdlModel.DealName,
                            VerifiedDateTime = item.WeekIntervalStartTime.AddDays(1),
                            StoreName = storeName,
                            UserName = Session[VbsSession.UserId.ToString()].ToString(),
                            UnDoState = couponItem.UnDoState,
                            UnDoDateTime = item.WeekIntervalStartTime.AddDays(2)
                        };
                        couponInfos.Add(couponInfo);
                    }
                }
                vdlModel.WeeklyVerificationInfos = infos;
                vdlModel.BalanceCouponInfos = couponInfos;
                vdlModel.VerifiedTotal = couponInfos.Where(x => x.UnDoState == BalanceSheetDetailStatus.Normal || x.UnDoState == BalanceSheetDetailStatus.Undo).Count() * 100;
                vdlModel.UnDoTotal = couponInfos.Where(x => x.UnDoState == BalanceSheetDetailStatus.Deduction).Count() * 100;
                vdlModel.PaymentTotal = vdlModel.VerifiedTotal - vdlModel.UnDoTotal;
            }

            #endregion Demo data set up

            #region balance sheet verification detail data set up

            else
            {
                int paymentPrice = 0, paymentTotal = 0;
                string paymentState = string.Empty;
                ViewBalanceSheetListCollection balanceSheetLists = new ViewBalanceSheetListCollection();
                Guid? storeGuid = null;
                if (!detailId.Equals("null"))
                {
                    storeGuid = Guid.Parse(detailId);
                }

                //檢查商家帳號是否有檢視該檔次分店權限
                bool isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, storeGuid, VbsRightFlag.ViewBalanceSheet);

                if (!isAllowedDealOrStore)
                {
                    return RedirectToAction("RedirectToActionAfterLogin");
                }

                #region Ppon

                if (type == VbsDealType.Ppon)
                {
                    balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.Ppon);
                }

                #endregion Ppon

                #region PiinLife

                else if (type == VbsDealType.PiinLife)
                {
                    balanceSheetLists = GetBalanceSheets(id, storeGuid, BusinessModel.PiinLife);
                }

                #endregion PiinLife

                //取得檔次為週結或月結資料
                var balanceSheetList = balanceSheetLists.Where(x => x.Id == balanceSheetId).First();

                //取得月對帳單應付金額
                var bsModel = new BalanceSheetModel(balanceSheetList.Id);
                paymentTotal = (int)bsModel.GetAccountsPayable().TotalAmount;

                if ((RemittanceType)balanceSheetList.RemittanceType == RemittanceType.ManualMonthly
                    || (RemittanceType)balanceSheetList.RemittanceType == RemittanceType.AchWeekly
                    || ((RemittanceType)balanceSheetList.RemittanceType == RemittanceType.ManualWeekly))
                {
                    vdlModel.FundTransferInfo = bsModel.TransferInfo;
                }

                //週結月對帳單
                if (balanceSheetList.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.MonthBalanceSheet) &&
                    (balanceSheetList.RemittanceType == (int)RemittanceType.ManualWeekly ||
                     balanceSheetList.RemittanceType == (int)RemittanceType.AchWeekly))
                {
                    //週結檔次 需撈取該月份各週對帳單detail
                    foreach (var bslist in balanceSheetLists.Where(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet && x.Year == balanceSheetList.Year && x.Month == balanceSheetList.Month).OrderBy(x => x.IntervalStart))
                    {
                        //取得各週對帳單應付/實付金額
                        var bsWModel = new BalanceSheetModel(bslist.Id);

                        paymentPrice = (int)(bsWModel.TransferInfo.AccountsPaid.HasValue ? bsWModel.TransferInfo.AccountsPaid.Value : 0);
                        paymentState = bsWModel.TransferInfo.TransferCompletionDescription;

                        if (!(paymentPrice > 0))
                        {
                            paymentState = "無須付款";
                        }

                        VbsWeeklyVerificationInfo info = new VbsWeeklyVerificationInfo
                        {
                            BalanceSheetId = bslist.Id,
                            WeekIntervalStartTime = bslist.IntervalStart,
                            WeekIntervalEndTime = bslist.IntervalEnd.AddDays(-1),
                            PaymentTotal = paymentPrice,
                            PaymentState = paymentState
                        };
                        infos.Add(info);

                        //抓取各週憑證核銷紀錄
                        couponInfos.AddRange(GetBalanceSheetDetailList(bslist.Id));
                    }

                    vdlModel.WeeklyVerificationInfos = infos;
                }
                //人工對帳單 或 月結月對帳單 or other
                else
                {
                    if (balanceSheetList.RemittanceType.Equals((int)RemittanceType.ManualMonthly))
                    {
                        paymentState = bsModel.TransferInfo.TransferCompletionDescription;
                    }
                    else
                    {
                        //抓取週對帳單付款狀態及付款時間
                        var balanceSheetBillList = _ap.ViewBalanceSheetBillListGetList(ViewBalanceSheetBillList.Columns.Id + " = " + balanceSheetId).First();

                        //對帳單金額大於0才需判斷匯款狀態 否則顯示 "無須付款"
                        if (paymentTotal > 0)
                        {
                            //根據匯款方式 抓取付款狀態及日期
                            //顯示:實際付款日期 or 付款狀態
                            //weekly_pay_report result紀錄為00:匯款成功
                            if (balanceSheetBillList.ResponseTime.HasValue && balanceSheetBillList.IsTransferComplete.GetValueOrDefault(false))
                            {
                                paymentState = balanceSheetBillList.ResponseTime.Value.ToString("yyyy/MM/dd");
                            }

                            if (paymentState == "")
                            {
                                paymentState = "尚未付款";
                            }
                        }
                        else
                        {
                            paymentState = "無須付款";
                        }
                    }
                    //抓取憑證核銷紀錄
                    couponInfos = GetBalanceSheetDetailList(balanceSheetId);
                    vdlModel.PaymentState = paymentState;
                }

                vdlModel.BalanceCouponInfos = couponInfos;

                vdlModel.DealId = id;
                vdlModel.DealType = type;
                vdlModel.DealName = balanceSheetList.Name;
                vdlModel.StoreId = detailId;
                if (detailId != "null")
                {
                    vdlModel.StoreName = balanceSheetList.StoreName;
                }
                vdlModel.ExchangePeriodStartTime = balanceSheetList.UseStartTime.Value;
                vdlModel.ExchangePeriodEndTime = balanceSheetList.UseEndTime.Value;
                vdlModel.MonthIntervalStartTime = balanceSheetList.IntervalStart;
                if (balanceSheetList.GenerationFrequency.Equals((int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet))
                {
                    vdlModel.MonthIntervalEndTime = balanceSheetList.IntervalEnd;
                    vdlModel.BalanceSheetMonth = balanceSheetList.IntervalStart.ToString("yyyy/MM/dd");
                }
                else
                {
                    var bsDesc = string.Empty;
                    switch (balanceSheetList.GenerationFrequency)
                    {
                        case (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet:
                            bsDesc = string.Format("【{0:yyyy/MM/dd}】彈性對帳單", balanceSheetList.IntervalEnd);
                            vdlModel.MonthIntervalEndTime = balanceSheetList.IntervalEnd;
                            break;
                        case (int)BalanceSheetGenerationFrequency.MonthBalanceSheet:
                            bsDesc = string.Format("【{0}/{1}】月對帳單", balanceSheetList.Year, balanceSheetList.Month);
                            vdlModel.MonthIntervalEndTime = balanceSheetList.IntervalEnd.AddDays(-1);
                            break;
                        case (int)BalanceSheetGenerationFrequency.WeekBalanceSheet:
                            bsDesc = string.Format("【{0:yyyy/MM/dd}】週對帳單", balanceSheetList.IntervalEnd.AddDays(-1));
                            vdlModel.MonthIntervalEndTime = balanceSheetList.IntervalEnd.AddDays(-1);
                            break;
                        default:
                            break;
                    }
                    vdlModel.MonthIntervalEndTime = balanceSheetList.IntervalEnd.AddDays(-1);
                    vdlModel.BalanceSheetMonth = bsDesc;
                }
                vdlModel.VerifiedTotal = (int)bsModel.GetAccountsPayable().PayAmount;
                vdlModel.UnDoTotal = (int)bsModel.GetAccountsPayable().DeductAmount;
                vdlModel.PaymentTotal = paymentTotal;
                vdlModel.DealUniqueId = balanceSheetList.UniqueId;
                vdlModel.RemittanceType = balanceSheetList.RemittanceType;
                vdlModel.GenerationFrequency = balanceSheetList.GenerationFrequency;
                vdlModel.SlottingFeeQuantity = GetSlottingFeeQuantity(balanceSheetList.ProductGuid);
            }

            #endregion  balance sheet verification detail data set up

            if (excel.Value)
            {
                MemoryStream outputStream = MonthlyVerificationListExport(vdlModel, vdlModel.RemittanceType);
                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = HttpUtility.UrlEncode("MonthlyVerificationList.xls") };
            }

            return View("MonthlyVerificationList", vdlModel);
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetPack(BalanceSheetPackModel model)
        {
            List<ViewBalanceSheetList> dealLists;
            if (VbsCurrent.Is17LifeEmployee)
            {
                #region 憑證對帳單

                dealLists = _ap.ViewBalanceSheetListGetList(model.UnReceiptReceivedDealGuids)
                                .Where(x => (x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet ||
                                             x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet ||
                                             x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet) &&
                                            !x.IsReceiptReceived &&
                                            x.EstAmount != 0 && 
                                            x.DeliveryType == (int)DeliveryType.ToShop)
                                .ToList();
                var dealIdList = dealLists.Select(x => x.ProductGuid).ToList();

                #endregion 憑證對帳單

                #region 商品對帳單

                dealLists.AddRange(_ap.PartialPaymentToBalanceSheetList(model.UnReceiptReceivedDealGuids)
                                        .Where(x => !dealIdList.Contains(x.ProductGuid) &&
                                                    !x.IsReceiptReceived &&
                                                    x.EstAmount != 0 &&
                                                    x.DeliveryType == (int)DeliveryType.ToHouse));

                #endregion 商品對帳單

                dealLists = new List<ViewBalanceSheetList>(dealLists.OrderBy(x => x.DeliveryType));
            }
            else
            {
                //取得有權限檢視之檔次
                var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.ViewBalanceSheet, false);

                dealLists = GetAllowedViewBalanceSheetListCollection(allowedDealUnits)
                                .Where(x => (x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FortnightlyBalanceSheet ||
                                             x.RemittanceType == (int)RemittanceType.Weekly) &&
                                            !x.IsReceiptReceived)
                                .OrderBy(x => x.DeliveryType)
                                .ToList();
            }

            model = GetBalanceSheetPack(model, dealLists);

            Session[VbsSession.ParentActionName.ToString()] = "BalanceSheetPack";
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            ViewBag.IsEnableVbsNewShipFile = config.IsEnableVbsNewShipFile;
            return View("BalanceSheetPack", model);
        }


        /// <summary>
        /// 總明細匯出對帳Excel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult BalanceSheetPackExport(BalanceSheetPackModel model)
        {
            List<ViewBalanceSheetList> dealLists;
            if (VbsCurrent.Is17LifeEmployee)
            {
                #region 憑證對帳單

                dealLists = _ap.ViewBalanceSheetListGetList(model.UnReceiptReceivedDealGuids)
                                .Where(x => (x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet ||
                                             x.BalanceSheetType == (int)BalanceSheetType.FlexiblePayBalanceSheet ||
                                             x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayBalanceSheet) &&
                                            !x.IsReceiptReceived &&
                                            x.EstAmount != 0)
                                .ToList();
                var dealIdList = dealLists.Select(x => x.ProductGuid).ToList();

                #endregion 憑證對帳單

                #region 商品對帳單

                dealLists.AddRange(_ap.PartialPaymentToBalanceSheetList(model.UnReceiptReceivedDealGuids)
                                        .Where(x => !dealIdList.Contains(x.ProductGuid) &&
                                                    !x.IsReceiptReceived &&
                                                    x.EstAmount != 0));

                #endregion 商品對帳單

                dealLists = new List<ViewBalanceSheetList>(dealLists.OrderBy(x => x.DeliveryType));
            }
            else
            {
                //取得有權限檢視之檔次
                var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, null, VbsRightFlag.ViewBalanceSheet, false);

                dealLists = GetAllowedViewBalanceSheetListCollection(allowedDealUnits)
                                .Where(x => (x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet ||
                                             x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.FortnightlyBalanceSheet ||
                                             x.RemittanceType == (int)RemittanceType.Weekly) &&
                                            !x.IsReceiptReceived &&
                                            x.EstAmount != 0)
                                .OrderBy(x => x.DeliveryType)
                                .ToList();
            }

            model = GetBalanceSheetPack(model, dealLists);

            MemoryStream outputStream = null;
            if (model.DeliveryType == DeliveryType.ToShop)
            {
                outputStream = BalanceSheetPackExportToShopToExcel(model);
            }
            else
            {
                outputStream = BalanceSheetPackExportToHouseToExcel(model);
            }

            return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = "總明細對帳單_新出帳方式.xls" };
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult BalanceSheetsToHouse(Guid id, int? balanceSheetId)
        {
            //檢查商家帳號是否有檢視該檔次分店權限
            var isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, null, VbsRightFlag.ViewBalanceSheet);

            if (!isAllowedDealOrStore)
            {
                return RedirectToAction("RedirectToActionAfterLogin");
            }

            var vdlModel = new BalanceSheetToHouseModel();

            try
            {
                vdlModel.Initial(id, balanceSheetId);

                

                ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                ViewBag.UserId = Session[VbsSession.UserId.ToString()];
                ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
                ViewBag.DealCount = Session[VbsSession.DealCount.ToString()];
                ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
                ViewBag.BalanceDealInfos = Session[VbsSession.BalanceDealInfos.ToString()] as IEnumerable<VbsBalanceDealInfo>;

                return View("BalanceSheetsToHouse", vdlModel);

            }
            catch (Exception ex)
            {
                throw new Exception("資料未設定完整:" + ex.Message);
            }
        }

        /// <summary>
        /// 彈性產對帳單
        /// </summary>
        /// <param name="id">檔次bid</param>
        /// <param name="storeGuid"></param>
        /// <param name="genDate">對帳單結算時間</param>
        /// <param name="generate">是否實際產對帳單，false為預算</param>
        /// <returns></returns>
        [VbsAuthorize(RequiresInspectionCode = true)]
        public JsonResult GenerateFlexibleBalanceSheet(Guid id, string storeGuid, DateTime? genDate, bool generate)
        {
            try
            {
                if (!genDate.HasValue)
                {
                    return Json(new { Success = false, FailReason = "沒有設定結算時間" });
                }
                string[] storeGuidarray = (storeGuid != null && !storeGuid.Equals("null") ? storeGuid : "").Split(",");

                if (storeGuidarray != null && storeGuidarray.Length > 0)
                {
                    if (storeGuidarray.Length == 1)
                    {
                        Guid? storeGuids = null;
                        if (storeGuidarray[0].ToString() != "")
                        {
                            storeGuids = Guid.Parse(storeGuidarray[0].ToString());
                        }

                        if (!generate)
                        {
                            //預算金額,尚未實際產對帳單
                            var bsInfo = new FlexibleBalanceSheetModel(id, storeGuids, genDate.Value).CaculateCollectAmount();
                            if (bsInfo != null)
                            {
                                return Json(new { Success = true, DealName = bsInfo.DealName, RemittanceTypeDesc = bsInfo.RemittanceTypeDesc, EstAmount = bsInfo.EstAmount });
                            }

                            return Json(new { Success = false, FailReason = "對帳單試算有問題!請洽廠商客服窗口。" });
                        }

                        var isSuccess = false;
                        var failReason = new StringBuilder();
                        BalanceSheetSpec spec;
                        var subDeals = new List<Guid>();
                        var deal = _pp.DealPropertyGet(id);
                        if (deal.DeliveryType == (int)DeliveryType.ToHouse)
                        {
                            //宅配檔次為母檔對帳 須產出該母檔下所有子檔之對帳單 
                            subDeals = _pp.GetComboDealByBid(id, false).Select(x => x.BusinessHourGuid).ToList();
                        }
                        //檢查是否為多檔次
                        if (!subDeals.Any())
                        {
                            subDeals = new List<Guid> { id };
                        }
                        foreach (var dealGuid in subDeals)
                        {
                            var dp = _pp.DealPropertyGet(dealGuid);
                            if (dp != null && dp.IsLoaded)
                            {
                                spec = new BalanceSheetSpec
                                {
                                    SheetCategory = BalanceSheetGenerationFrequency.FlexibleBalanceSheet,
                                    BizModel = BusinessModel.Ppon,
                                    BusinessHourGuid = dp.BusinessHourGuid,
                                    ManualTriggeredStoreGuid = storeGuids,
                                    TriggerUser = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId
                                };
                            }
                            else
                            {
                                return Json(new { Success = false, FailReason = "檔次資料有問題!請洽廠商客服窗口。" });
                            }

                            var dealId = dp.UniqueId;
                            var genResults = BalanceSheetService.SimulateCreate(spec, genDate.Value);
                            if (genResults != null && genResults.Count == 1)
                            {
                                var result = genResults.First();
                                if (result.IsSuccess)
                                {
                                    isSuccess = true;
                                }
                                switch (result.Status)
                                {
                                    case BalanceSheetGenerationResultStatus.Duplication:
                                        failReason.Append(string.Format("檔號[{0}]此結算日期已產過對帳單了。\n", dealId));
                                        break;
                                    case BalanceSheetGenerationResultStatus.ItemsCheckFailed:
                                        failReason.Append(string.Format("檔號[{0}]沒有資料可產對帳單。\n", dealId));
                                        break;
                                    case BalanceSheetGenerationResultStatus.Success:
                                        isSuccess = true;
                                        break;
                                    default:
                                        failReason.Append(string.Format("檔號[{0}]產出對帳單發生錯誤 ({1})\n", dealId, result.FailureMessage));
                                        break;
                                }
                            }
                        }

                        if (isSuccess)
                        {
                            BalanceSheetManager.ProductBalanceSheetCreateMailNotify(new List<Guid> { id });
                        }
                        return Json(new { Success = isSuccess, FailReason = failReason.ToString() });
                    }
                    else
                    {
                        int cntSuccessStore = 0, cntFailStoreDuplication = 0, cntFailStore = 0;
                        int cntSectionSuccess = 0, cntSectionFailDuplication = 0, cntSectionFail = 0;
                        var isSuccess = false;
                        var failReason = new StringBuilder();
                        foreach (var sGuid in storeGuidarray)
                        {
                            if (sGuid.Length > 0)
                            {
                                Guid? storeGuids = null;
                                if (!sGuid.Equals("null") && sGuid.Length > 0)
                                {
                                    storeGuids = Guid.Parse(sGuid);
                                }
                                BalanceSheetSpec spec;
                                var subDeals = new List<Guid>();
                                var deal = _pp.DealPropertyGet(id);
                                if (deal.DeliveryType == (int)DeliveryType.ToHouse)
                                {
                                    //宅配檔次為母檔對帳 須產出該母檔下所有子檔之對帳單 
                                    subDeals = _pp.GetComboDealByBid(id, false).Select(x => x.BusinessHourGuid).ToList();
                                }
                                //檢查是否為多檔次
                                if (!subDeals.Any())
                                {
                                    subDeals = new List<Guid> { id };
                                }
                                foreach (var dealGuid in subDeals)
                                {
                                    var dp = _pp.DealPropertyGet(dealGuid);
                                    if (dp != null && dp.IsLoaded)
                                    {
                                        spec = new BalanceSheetSpec
                                        {
                                            SheetCategory = BalanceSheetGenerationFrequency.FlexibleBalanceSheet,
                                            BizModel = BusinessModel.Ppon,
                                            BusinessHourGuid = dp.BusinessHourGuid,
                                            ManualTriggeredStoreGuid = storeGuids,
                                            TriggerUser = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId
                                        };
                                    }
                                    else
                                    {
                                        return Json(new { Success = false, FailReason = "檔次資料有問題!請洽廠商客服窗口。" });
                                    }

                                    var dealId = dp.UniqueId;
                                    var genResults = BalanceSheetService.SimulateCreate(spec, genDate.Value);
                                    if (genResults != null && genResults.Count == 1)
                                    {
                                        var result = genResults.First();
                                        if (result.IsSuccess)
                                        {
                                            isSuccess = true;
                                        }
                                        switch (result.Status)
                                        {
                                            case BalanceSheetGenerationResultStatus.Duplication:
                                                failReason.Append(string.Format("檔號[{0}]此結算日期已產過對帳單了。\n", dealId));
                                                cntSectionFailDuplication++;
                                                break;
                                            case BalanceSheetGenerationResultStatus.ItemsCheckFailed:
                                                failReason.Append(string.Format("檔號[{0}]沒有資料可產對帳單。\n", dealId));
                                                cntSectionFailDuplication++;
                                                break;
                                            case BalanceSheetGenerationResultStatus.Success:
                                                isSuccess = true;
                                                cntSectionSuccess++;
                                                break;
                                            default:
                                                failReason.Append(string.Format("檔號[{0}]產出對帳單發生錯誤 ({1})\n", dealId, result.FailureMessage));
                                                cntSectionFail++;
                                                break;
                                        }
                                    }
                                }

                                if (isSuccess)
                                {
                                    BalanceSheetManager.ProductBalanceSheetCreateMailNotify(new List<Guid> { id });
                                }

                                if (cntSectionSuccess > 0)
                                {
                                    cntSuccessStore++;
                                }
                                else if (cntSectionFail > 0)
                                {
                                    cntFailStore++;
                                }
                                else if (cntSectionFailDuplication > 0)
                                {
                                    cntFailStoreDuplication++;
                                }
                            }
                        }
                        return Json(new
                        {
                            Success = isSuccess,
                            CntSuccessStore = cntSuccessStore.ToString(),
                            CntFailStore = cntFailStore.ToString(),
                            CntFailStoreDuplication = cntFailStoreDuplication.ToString(),
                            FailReason = failReason.ToString()
                        });
                    }

                }
                return Json(new { Success = false, FailReason = "對帳單試算有問題!請洽廠商客服窗口。" });
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message, ex);
                return Json(new { Success = false, FailReason = ex.Message });
            }
            
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult ToHousePayList(Guid id, int balanceSheetId)
        {
            //檢查商家帳號是否有檢視該檔次分店權限
            var isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, null, VbsRightFlag.ViewBalanceSheet);

            if (!isAllowedDealOrStore)
            {
                return RedirectToAction("RedirectToActionAfterLogin");
            }

            var vdlModel = new ToHousePayListModel();

            try
            {
                vdlModel.Initial(id, balanceSheetId);

                ViewBag.ProductId = id;
                ViewBag.BalanceSheetId = balanceSheetId;
                ViewBag.UserName = Session[VbsSession.UserName.ToString()];
                ViewBag.UserId = Session[VbsSession.UserId.ToString()];
                ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
                ViewBag.IsEnableVbsNewShipFile = config.IsEnableVbsNewShipFile;
                return View("ToHousePayList", vdlModel);

            }
            catch (Exception ex)
            {
                throw new Exception("資料未設定完整:" + ex.Message);
            }
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public ActionResult ToHousePayListExport(Guid id, int balanceSheetId)
        {
            //檢查商家帳號是否有檢視該檔次分店權限
            var isAllowedDealOrStore = VbsCurrent.Is17LifeEmployee || CheckIsAllowedDealOrStore(VbsCurrent.AccountId, id, null, VbsRightFlag.ViewBalanceSheet);

            if (!isAllowedDealOrStore)
            {
                return RedirectToAction("RedirectToActionAfterLogin");
            }

            var bsthModel = new BalanceSheetToHouseModel();
            var thplModel = new ToHousePayListModel();

            try
            {
                bsthModel.Initial(id, balanceSheetId);
                thplModel.Initial(id, balanceSheetId);
                var outputStream = PayInfoExportToExcel(bsthModel, thplModel);
                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("{0}_{1}_對帳明細.xls", bsthModel.DealUniqueId, bsthModel.DealName)) };
            }
            catch (Exception ex)
            {
                throw new Exception("資料未設定完整:" + ex.Message);
            }
        }

        [VbsAuthorize(RequiresInspectionCode = true)]
        public JsonResult GoToBalanceSheetPage(Guid id, VbsDealType type, string detailId, int deliveryType, int generationFrequency, int remittanceType)
        {

            if (deliveryType == (int)DeliveryType.ToShop)
            {
                if (generationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet ||
                    remittanceType == (int)RemittanceType.Flexible ||
                    remittanceType == (int)RemittanceType.Weekly)
                {
                    return Json(new { Url = Url.Action("BalanceSheetsMonth", new { id = id, type = type, detailId = detailId }) });
                }
                if (generationFrequency == (int)BalanceSheetGenerationFrequency.WeekBalanceSheet)
                {
                    return Json(new { Url = Url.Action("BalanceSheetsWeek", new { id = id, type = type, detailId = detailId }) });
                }
                if (generationFrequency == (int)BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
                {
                    return Json(new { Url = Url.Action("BalanceSheetsManual", new { id = id, type = type, detailId = detailId }) });
                }
            }
            else if (deliveryType == (int)DeliveryType.ToHouse)
            {
                if (generationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet)
                {
                    return Json(new { Url = Url.Action("BalanceSheetsDeliver", new { id = id }) });
                }
                return Json(new { Url = Url.Action("BalanceSheetsToHouse", new { id = id }) });
            }

            return Json(new { Url = Url.Action(VbsCurrent.Is17LifeEmployee ? "EmployeeBalanceDealList" : "BalanceDealList") });
        }


        public ActionResult SearchBalanceSheet(string strUniqueId)
        {
            int uniqueId = 0;
            int.TryParse(strUniqueId, out uniqueId);
            if (uniqueId!=0)
            {
                ViewPponDeal vpd = _pp.ViewPponDealGetByUniqueId(uniqueId);
                if (vpd.IsLoaded)
                {
                    ComboDeal cd = _pp.GetComboDeal(vpd.BusinessHourGuid);
                    
                    if (cd.IsLoaded)
                    {
                        return Json(new { isSuccess = true, Message = cd.MainBusinessHourGuid.ToString() });

                    }
                    else
                    {
                        return Json(new { isSuccess = true, Message = vpd.BusinessHourGuid.ToString() });

                    }
                }
                else
                {
                    return Json(new { isSuccess = false, Message = "查無檔次資料" });
                }
                
            }
            else
            {
                return Json(new { isSuccess = false, Message = "輸入錯誤" });
            }
        }
        #endregion 對帳查詢

        #region 文件管理
        [VbsAuthorize]
        public ActionResult VbsDocumentDownloadSys(string t)
        {
            GetDocumentFile(t);
            ViewBag.Title = Helper.GetDescription(DocPageType.System);
            ViewBag.SubTitle = "系統的相關功能操作，可以在這裡下載相關文件使用";
            return View("~/Views/VendorBillingSystem/VbsDocumentDownload.cshtml");
        }

        [VbsAuthorize]
        public ActionResult VbsDocumentDownloadOther(string t)
        {
            GetDocumentFile(t);
            ViewBag.Title = Helper.GetDescription(DocPageType.Other);
            ViewBag.SubTitle = "需要經由人工審核或是合約異更等之文件書面資料皆可在此下載使用";
            return View("~/Views/VendorBillingSystem/VbsDocumentDownload.cshtml");
        }

        private void GetDocumentFile(string t)
        {
            int t1 = (int)DocPageType.System;
            int.TryParse(t, out t1);


            string deliveryType = Session[VbsSession.DeliveryType.ToString()] == null ? "0" : Session[VbsSession.DeliveryType.ToString()].ToString();

            List<VbsDocumentFile> vdfs = _vbs.VbsDocumentFileGetList()
                                            .Where(x => x.Status != -1 && x.PageType == t1).OrderBy(s => s.Sort).ToList();

            if (deliveryType == ((int)DeliveryType.ToShop).ToString())
            {
                vdfs = vdfs.Where(x => x.IsShop == true).ToList();
            }
            else if (deliveryType == ((int)DeliveryType.ToHouse).ToString())
            {
                vdfs = vdfs.Where(x => x.IsHouse == true).ToList();
            }
            else
            {
                vdfs = vdfs.Where(x => x.IsShop == false && x.IsHouse == false).ToList();
            }

            List<VbsDocumentCategory> vdcs = _vbs.VbsDocumentCategoryGetList()
                                            .Where(x => x.Status != -1).OrderBy(s => s.Sort).ToList();

            ViewBag.DocumentCategory = vdcs;
            ViewBag.DocumentFile = vdfs;
        }

        [Authorize]
        public ActionResult DownLoadVbsFile(int id)
        {
            VbsDocumentFile vdf = _vbs.VbsDocumentFileGet(id);
            if (vdf.IsLoaded)
            {
                string ftpFolder = "VbsDocument";
                string FileName = vdf.FileName;

                if (config.SellerContractIsFtp)
                {
                    //Use FTP
                    SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + "/" + ftpFolder + "/" + FileName));
                    cli.SimpleDownload(FileName, ftpFolder + "/" + FileName);
                }
                else
                {
                    //Use Folder
                    string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                    string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ftpFolder);

                    System.Net.WebClient wc = new System.Net.WebClient();
                    byte[] byteFiles = null;
                    string FilePath = System.IO.Path.Combine(Dir, vdf.FileName);
                    byteFiles = wc.DownloadData(FilePath);
                    string fileName = System.IO.Path.GetFileName(FilePath);
                    Response.AddHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(fileName));
                    Response.ContentType = "application/octet-stream";
                    Response.BinaryWrite(byteFiles);
                    Response.End();
                }
            }
            return null;
        }
        #endregion 文件管理

        #region 其他功能
        [VbsAuthorize]
        public ActionResult Others()
        {
            OthersModel vdlModel = new OthersModel();
            List<ManualCategoryInfo> categoryInfos = new List<ManualCategoryInfo>();
            List<ManualFileInfo> infos = new List<ManualFileInfo>();
            VbsUserManualCategoryCollection manualCategoryInfos = _sp.VbsUserManualCategoryGetEnabledList();
            Dictionary<int, List<VbsUserManualFile>> manualFileInfos = _sp.VbsUserManualFileGetEnabledList()
                                                                        .GroupBy(x => x.CategoryId)
                                                                        .ToDictionary(x => x.Key, x => x.ToList());

            VbsContactInfo contactInfo = _vbs.VbsContactInfoGet(1);
            if (contactInfo.IsLoaded)
            {
                ViewBag.ContactInfo = Server.HtmlDecode(contactInfo.ContactInfo);
            }
            else
            {
                ViewBag.ContactInfo = string.Empty;
            }
            

            string downloadPath = WebUtility.GetSiteRoot() + config.VendorBillingSystemManual;

            foreach (var category in manualCategoryInfos)
            {
                infos = new List<ManualFileInfo>();
                foreach (var file in manualFileInfos[category.Id])
                {
                    ManualFileInfo info = new ManualFileInfo
                    {
                        FileName = file.Name,
                        FileDescription = file.Description,
                        FilePath = string.Format("{0}/{1}/{2}", downloadPath, category.Reference, file.Reference)
                    };
                    infos.Add(info);
                }
                ManualCategoryInfo categoryInfo = new ManualCategoryInfo
                {
                    CategoryName = category.Name,
                    CategoryDescription = category.Description,
                    FileInfos = infos
                };
                categoryInfos.Add(categoryInfo);
            }

            vdlModel.ManualCategoryInfos = categoryInfos;

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.IsUseInspectionCode = Session[VbsSession.UseInspectionCode.ToString()];
            ViewBag.BusinessServiceToHouseTel = config.BusinessServiceToHouseTel;
            ViewBag.BusinessServiceToHouseAccountTel = config.BusinessServiceToHouseAccountTel;
            ViewBag.BusinessServicePponAccountTel = config.BusinessServicePponAccountTel;
            ViewBag.BusinessServicePponTel = config.BusinessServicePponTel;
            ViewBag.BusinessServiceTravelPponAccountTel = config.BusinessServiceTravelPponAccountTel;
            Session[VbsSession.ParentActionName.ToString()] = "Others";

            string bindAccount = string.Empty;
            string mobileNumber = string.Empty;
            VBSFacade.GetVbsBindAccountEmailOrMobileByVbsAccount(ViewBag.UserId, out bindAccount, out mobileNumber);
            vdlModel.BindAccount = bindAccount;
            vdlModel.MobileNumber = mobileNumber;

            ViewBag.IsEnableVbsBindAccount = config.IsEnableVbsBindAccount;
            ViewBag.IsLoginBy17LifeMember = Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()];
            return View("Others", vdlModel);
        }

        [VbsAuthorize]
        public ActionResult VerifyInfo()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.IsUseInspectionCode = Session[VbsSession.UseInspectionCode.ToString()];
            return View("VerifyInfo");
        }

        #region 登入密碼

        [HttpGet]
        [VbsAuthorize]
        public ActionResult SetPassword()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.FirstLogin = Session[VbsSession.FirstLogin.ToString()];
            ViewBag.IsPwdPassTimeLimit = Session[VbsSession.IsPwdPassTimeLimit.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            ViewBag.IsEnableVbsBindAccount = config.IsEnableVbsBindAccount;

            ViewBag.RealLoginAccount = Session[VbsSession.RealLoginAccount.ToString()].ToString();
            return View();
        }

        [ActionName("SetPassword")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult AuthenticatePassword(SetPasswordModel inputModel)
        {
            try
            {
                bool isLoginBy17LifeMemberAccount = false;
                bool.TryParse(Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()].ToString(), out isLoginBy17LifeMemberAccount);

                if (IsNewPasswodAndOriginalPasswordTheSame(inputModel.NewPassword, inputModel.Password))
                {
                    return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.NewPasswordCanNotBeEqualToOldPassword);
                }

                if (!IsNewPasswodAndRetypedNewPasswordTheSame(inputModel.NewPassword, inputModel.CheckNewPassword))
                {
                    return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordInconsistent);
                }

                if (!IsPasswordFormatCorrect(inputModel.NewPassword))
                {
                    return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordFormatRule);
                }

                if (isLoginBy17LifeMemberAccount)
                {
                    //17Life帳號綁定，修改17Life會員密碼
                    if (Session[VbsSession.RealLoginAccount.ToString()] == null)
                    {
                        return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordChangeFailure);
                    }
                    string loginAccount = Session[VbsSession.RealLoginAccount.ToString()].ToString();
                    Member m = new Member();
                    if (RegExRules.CheckEmail(loginAccount))
                    {
                        m = _mp.MemberGet(loginAccount);
                    }
                    else if (RegExRules.CheckMobile(loginAccount))
                    {
                        m = _mp.MemberGetByMobile(loginAccount);
                    }
                    else
                    {
                        return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordChangeFailure);
                    }
                    if (m != null && m.IsLoaded)
                    {
                        bool isValid = MemberFacade.ResetMemberPassword(Session[VbsSession.RealLoginAccount.ToString()].ToString(),
                            inputModel.Password,
                            inputModel.NewPassword,
                            ResetPasswordReason.MemberChange);
                        if (isValid)
                        {
                            return GetChangePasswordResult(isSuccess: true, errorMessage: string.Empty);
                        }
                        else
                        {
                            return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordChangeFailure);
                        }
                    }
                    return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordChangeFailure);
                }
                else
                {
                    //商家帳號修改密碼，修改商家帳號密碼

                    if (!IsOriginalPasswordCorrect(this.UserName, inputModel.Password))
                    {
                        return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordVerificationFailure);
                    }

                    if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
                    {
                        VbsCurrent.DemoUser.Password = inputModel.NewPassword;
                        Session[VbsSession.FirstLogin.ToString()] = false;
                    }
                    else
                    {
                        if (!VbsMemberUtility.ChangePassword(this.UserName, inputModel.Password, inputModel.NewPassword))
                        {
                            return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.PasswordChangeFailure);
                        }
                    }
                    Session[VbsSession.FirstLogin.ToString()] = false;
                    Session[VbsSession.IsPwdPassTimeLimit.ToString()] = false;
                    return GetChangePasswordResult(isSuccess: true, errorMessage: string.Empty);
                }
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        private JsonResult GetChangePasswordResult(bool isSuccess, string errorMessage)
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.FirstLogin = Session[VbsSession.FirstLogin.ToString()];
            var isBindAccount = _mp.VbsMembershipBindAccountGetByVbsAccountId(Session[VbsSession.UserId.ToString()].ToString()).IsLoaded;
            return Json(new { IsFirstLogin = (bool)Session[VbsSession.FirstLogin.ToString()], IsSuccess = isSuccess, Message = errorMessage, IsBindAccount = isBindAccount });
        }

        #endregion 登入密碼

        #region 查帳碼

        [HttpGet]
        [VbsAuthorize]
        public ActionResult SetInspectionCode()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.IsInspectionCodeNeverChange = Session[VbsSession.IsInspectionCodeNeverChange.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            return View();
        }

        [ActionName("SetInspectionCode")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult AuthenticateInspectionCode(SetInspectionCodeModel inputModel)
        {
            if (!IsInspectionCodeCorrect(this.UserName, inputModel.InspectionCode))
            {
                return GetChangePasswordResult(isSuccess: false, errorMessage: I18N.Message.InspectionCodeVerificationFailure);
            }

            if (!IsNewAndRetypedInspectionCodeTheSame(inputModel.NewInspectionCode, inputModel.CheckNewInspectionCode))
            {
                return GetChangeInspectionCodeResult(isSuccess: false, errorMessage: I18N.Message.PasswordInconsistent);
            }

            if (!IsInspectionCodeFormatCorrect(inputModel.NewInspectionCode))
            {
                return GetChangeInspectionCodeResult(isSuccess: false, errorMessage: I18N.Message.PasswordFormatRule);
            }

            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                VbsCurrent.DemoUser.InspectionCode = inputModel.NewInspectionCode;
            }
            else
            {
                if (!VbsMemberUtility.ChangeInspectionCode(this.UserName, inputModel.InspectionCode, inputModel.NewInspectionCode))
                {
                    return GetChangeInspectionCodeResult(isSuccess: false, errorMessage: I18N.Message.InspectionCodeChangeFailure);
                }
            }

            Session[VbsSession.IsInspectionCodeNeverChange.ToString()] = false;
            return GetChangeInspectionCodeResult(isSuccess: true, errorMessage: string.Empty);
        }

        private ActionResult GetChangeInspectionCodeResult(bool isSuccess, string errorMessage)
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.FirstLogin = Session[VbsSession.FirstLogin.ToString()];
            return Json(new { IsFirstLogin = (bool)Session[VbsSession.FirstLogin.ToString()], IsSuccess = isSuccess, Message = errorMessage });
        }

        [HttpGet]
        [VbsAuthorize]
        public ActionResult InspectionCodeLogin()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            return View();
        }

        [ActionName("InspectionCodeLogin")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult AuthenticateInspectionCodeLogin(InspectionCodeLoginModel inputModel)
        {
            if (IsInspectionCodeCorrect(this.UserName, inputModel.InspectionCode))
            {
                Session.Add(VbsSession.InspectionRight.ToString(), new object());

                //save登入查帳碼時間
                VbsMembership mem = _mp.VbsMembershipGetByAccountId(this.UserName);
                mem.LastInspectionDate = DateTime.Now;
                _mp.VbsMembershipSet(mem);

                return Json(new { IsSuccess = true, Message = string.Empty, ParentActionName = Session[VbsSession.ParentActionName.ToString()] });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = I18N.Message.InspectionCodeRestriction, ParentActionName = Session[VbsSession.ParentActionName.ToString()] });
            }
        }

        #endregion 查帳碼

        #region 同意換約

        [HttpPost]
        [VbsAuthorize]
        public void IsArgeeContract(bool argee, int deliveryType)
        {
            string accountId = Session[VbsSession.UserId.ToString()].ToString();
            VBSFacade.UpdateSellerContractVersion(accountId, argee, deliveryType);
        }

        #endregion

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ApplyIsp(int applyReturnCycle, int applyReturnType, string applyReturnOther)
        {
            string sid = "";
            var sids = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId).Where(x => x.ResourceType == (int)ObjectResourceType.Seller).Select(x => x.ResourceGuid).ToList();

            Seller seller = new Seller();
            ViewVbsInstorePickup vvipFamily = new ViewVbsInstorePickup();
            foreach (var s in sids)
            {
                var root_guid = SellerFacade.GetRootSellerGuid(s);
                if (!vvipFamily.IsLoaded)
                {
                    vvipFamily = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.FamilyMart) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);
                    if (!vvipFamily.IsLoaded)
                        vvipFamily = new ViewVbsInstorePickup();
                }

            }

            string sellerGuid = vvipFamily.SellerGuid.ToString();
            var acls = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId).Any(x => x.ResourceGuid.ToString() == sellerGuid);

            if (acls)
            {
                var root_guid = SellerFacade.GetRootSellerGuid(new Guid(sellerGuid));


                
                VbsInstorePickupCollection vipc = new VbsInstorePickupCollection();

                var vipFamily = _sp.VbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);

                if (vipFamily.IsLoaded)
                {
                    //申請Seven
                    var vipSeven = _sp.VbsInstorePickupGet(root_guid, (int)ServiceChannel.SevenEleven);

                    if (vipSeven.IsLoaded)
                    {
                        
                    }
                    else
                    {
                        vipSeven = new VbsInstorePickup()
                        {
                            SellerGuid = root_guid,
                            Status = (int)ISPStatus.Verifying,
                            CreateId = VbsCurrent.AccountId,
                            CreateTime = DateTime.Now,
                            ReturnCycle = applyReturnCycle,
                            ReturnType = applyReturnType,
                            ReturnOther = applyReturnType == (int)RetrunShip.Other ? applyReturnOther : "",
                            ServiceChannel = (int)ServiceChannel.SevenEleven,
                            SellerAddress = vipFamily.SellerAddress,
                            Contacts = vipFamily.Contacts,
                        };
                    }

                    vipc.Add(vipSeven);

                    _sp.VbsInstorePickupSetList(vipc);
                }

            }

            return Json(new { Result = true });
        }

        #endregion 其他功能

        #region 商家提案單
        [VbsAuthorize]
        public ActionResult ProposalContent(int pid)
        {
            Dictionary<int, string> dealType = new Dictionary<int, string>();
            Dictionary<int, string> deliveryMethod = new Dictionary<int, string>();
            Dictionary<int, string> selerSales = new Dictionary<int, string>();

            SellerProposal sProposal = new SellerProposal();

            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            //商家須先簽訂同意書
            VbsMembership member = _mp.VbsMembershipGetByAccountId(UserId);
            if (member != null && member.IsLoaded)
            {
                if (!member.ProposalConsent)
                {
                    return RedirectToAction("ProposalConsent");
                }
            }

            List<ShoppingCartFreight> scfs = new List<ShoppingCartFreight>();

            List<Guid> sellers = ProposalFacade.GetVbsSeller(UserId).Select(x => x.Key).Distinct().ToList();
            List<ProposalSellerFile> files = new List<ProposalSellerFile>();
            ProposalMultiDealCollection multiDeals = new ProposalMultiDealCollection();
            Proposal pro = _sp.ProposalGet(pid);
            sProposal.Pid = pid;
            sProposal.sellerGuid = pro.SellerGuid;
            if (pro.IsLoaded)
            {
                //該提案單需要是該賣家才能看到
                if (!sellers.Contains(pro.SellerGuid))
                {
                    return RedirectToAction("ProposalList");
                }
                List<int> logTypes = new List<int>()
                {
                    (int)ProposalLogType.Seller,
                    (int)ProposalLogType.DenySales
                };

                ViewBag.logs = _sp.ProposalLogGetList(pro.Id, logTypes).OrderByDescending(x => x.CreateTime);
                Seller seller = _sp.SellerGet(pro.SellerGuid);
                //StoreCollection stores = _sp.StoreGetListBySellerGuid(pro.SellerGuid);
                sProposal.UniqueId = "NA";
                if (pro.BusinessHourGuid != null)
                {
                    sProposal.BusinessCreated = true;
                    sProposal.BusinessHourId = pro.BusinessHourGuid.ToString();     //Bid                    
                    ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                    if (vpd != null && vpd.IsLoaded)
                    {
                        sProposal.UniqueId = vpd.UniqueId.ToString();     //檔號   
                        if (vpd.BusinessHourOrderTimeS != null && vpd.BusinessHourOrderTimeE != null
                            && vpd.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year && vpd.BusinessHourOrderTimeE.Year != DateTime.MaxValue.Year)
                        {
                            sProposal.BusinessHourOrderTime = vpd.BusinessHourOrderTimeS + "~" + vpd.BusinessHourOrderTimeE;
                        }
                        else
                        {
                            sProposal.UniqueId = "NA";
                            sProposal.BusinessHourId = "NA";
                            sProposal.BusinessHourOrderTime = "尚未排檔";
                        }
                    }
                    else
                    {
                        sProposal.BusinessHourOrderTime = "尚未排檔";
                    }
                }
                else
                {
                    sProposal.BusinessHourId = "NA";
                    sProposal.BusinessHourOrderTime = "尚未排檔";
                }

                if (pro.BusinessHourGuid != null)
                {
                    ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                    sProposal.BusinessHourOrderTimeS = vpd.BusinessHourOrderTimeS.ToString("yyyy/MM/dd");
                    sProposal.BusinessHourOrderTimeE = vpd.BusinessHourOrderTimeE.ToString("yyyy/MM/dd");
                }


                int SaleId = 0;
                int.TryParse(pro.OperationSalesId.ToString(), out SaleId);
                if (SaleId != 0)
                {
                    ViewEmployee opSalesEmp = _hmp.ViewEmployeeGet(Employee.Columns.UserId, pro.OperationSalesId);
                    if (opSalesEmp != null && opSalesEmp.IsLoaded)
                    {
                        sProposal.OpSalesId = opSalesEmp.UserId;                    //負責業務
                        sProposal.OpSalesName = opSalesEmp.EmpName;
                    }
                }

                Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);

                sProposal.Pid = pro.Id;         //提案單號
                foreach (var item in Enum.GetValues(typeof(SellerProposalFlag)))
                {
                    if (Helper.IsFlagSet(pro.SellerProposalFlag, (SellerProposalFlag)item))
                    {
                        sProposal.Status = ((int)((SellerProposalFlag)item)).ToString();
                        sProposal.StatusName = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerProposalFlag)item);    //提案狀態
                    }
                }

                sProposal.DeliveryType = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (DeliveryType)pro.DeliveryType); ;      //消費方式(好康/宅配)
                sProposal.BrandName = pro.BrandName;                      //品牌名稱
                sProposal.DealType = pro.DealType;       //商品類型
                sProposal.DealSubType = pro.DealSubType;       //商品類型
                sProposal.VendorReceiptType = pro.VendorReceiptType ?? (int)VendorReceiptType.Other;               //店家單據開立方式
                if (pro.ShipType == (int)DealShipType.Normal)
                {
                    sProposal.ShippingdateType = pro.ShippingdateType ?? (int)DealShippingDateType.Normal;
                    if (pro.ShippingdateType == (int)DealShippingDateType.Special)
                    {
                        sProposal.ShipType = pro.ShipType.ToString() + "-2";              //出貨類型
                        sProposal.txtShipText1 = pro.ShipText1;
                        sProposal.txtShipText2 = pro.ShipText2;

                    }
                    else
                    {
                        sProposal.ShipType = pro.ShipType.ToString() + "-1";              //出貨類型
                        sProposal.txtShipText3 = pro.ShipText3;
                    }
                }
                else if (pro.ShipType == (int)DealShipType.Ship72Hrs)
                {
                    sProposal.ShipType = pro.ShipType.ToString();              //出貨類型
                }
                else if (pro.ShipType == (int)DealShipType.Other)
                {
                    sProposal.ShipType = pro.ShipType.ToString();              //出貨類型
                    sProposal.ShipTypeOther = pro.ShipOther;
                }

                sProposal.DeliveryMethod = pro.DeliveryMethod ?? (int)ProposalDeliveryMethod.Normal;        //配送方式
                sProposal.IsParallelImportation = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation);   //平行輸入
                sProposal.Photographers = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Photographers);      //圖片來源(需拍攝)
                sProposal.SellerPhoto = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.SellerPhoto);      //圖片來源(店家提供照片)
                sProposal.FTPPhoto = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FTPPhoto);      //圖片來源(舊照片)
                sProposal.IsDealStar = (!Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.DealStar)).ToString();      //本檔主打星
                sProposal.ProductSpec = pro.ProductSpec;       //規格功能/主要成份
                sProposal.IsBeauty = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.Beauty);         //醫療 / 妝廣字號

                sProposal.FileDone = pro.FilesDone;
                if (specialText != null)
                {
                    if (specialText.ContainsKey(ProposalSpecialFlag.Beauty))
                    {
                        sProposal.Beauty = specialText[ProposalSpecialFlag.Beauty];            //醫療 / 妝廣字號
                    }
                    if (specialText.ContainsKey(ProposalSpecialFlag.InspectRule))
                    {
                        sProposal.InspectRule = specialText[ProposalSpecialFlag.InspectRule];       //檢驗規定
                    }
                    if (specialText.ContainsKey(ProposalSpecialFlag.ExpiringProduct))
                    {
                        sProposal.ExpiringProduct = specialText[ProposalSpecialFlag.ExpiringProduct];       //即期品
                    }
                    if (specialText.ContainsKey(ProposalSpecialFlag.DealStar))
                    {
                        sProposal.DealStar = specialText[ProposalSpecialFlag.DealStar];       //即期品
                    }
                    if (specialText.ContainsKey(ProposalSpecialFlag.StoreStory))
                    {
                        sProposal.StoreStory = specialText[ProposalSpecialFlag.StoreStory];       //店家/品牌故事
                    }
                }
                else
                {
                    sProposal.Beauty = "";            //醫療 / 妝廣字號
                    sProposal.InspectRule = "";       //檢驗規定
                    sProposal.ExpiringProduct = "";       //即期品
                }
                sProposal.IsExpiringProduct = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct);    //即期品
                sProposal.IsInspectRule = Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.InspectRule); ;    //檢驗規定
                sProposal.ContractMemo = pro.ContractMemo;  //備註說明
                sProposal.AccountingMessage = pro.Othermessage;

                //取得優惠內容
                multiDeals = _sp.ProposalMultiDealGetByPid(pro.Id);
                if (multiDeals.Count() > 0)
                {
                    sProposal.MultiDeals = new JsonSerializer().Serialize(multiDeals);
                }
                else
                {
                    ProposalMultiDealCollection newDeal = new ProposalMultiDealCollection();
                    sProposal.MultiDeals = new JsonSerializer().Serialize(newDeal);
                }

                sProposal.IsMediaReportFlag = pro.MediaReportFlag == (int)ProposalMediaReport.None ? false : true;
                sProposal.IsMediaReportPic = pro.MediaReportFlag == (int)ProposalMediaReport.Pic ? true : false;
                List<ProposalMediaReportLink> mediareportText = new JsonSerializer().Deserialize<List<ProposalMediaReportLink>>(pro.MediaReportFlagText);
                if (mediareportText == null)
                    sProposal.MediaTextCount = 2;
                else if (mediareportText.Count == 0)
                    sProposal.MediaTextCount = 2;
                else
                {
                    sProposal.MediaTextCount = (mediareportText.Count() + 1);
                    if (mediareportText.Count >= 1)
                    {
                        sProposal.txtMediaReport1 = mediareportText[0].Link;
                        sProposal.rdbMediaReport1 = mediareportText[0].Type;
                    }
                    if (mediareportText.Count >= 2)
                    {
                        sProposal.txtMediaReport2 = mediareportText[1].Link;
                        sProposal.rdbMediaReport2 = mediareportText[1].Type;
                    }
                    if (mediareportText.Count >= 3)
                    {
                        sProposal.txtMediaReport3 = mediareportText[2].Link;
                        sProposal.rdbMediaReport3 = mediareportText[2].Type;
                    }
                    if (mediareportText.Count >= 4)
                    {
                        sProposal.txtMediaReport4 = mediareportText[3].Link;
                        sProposal.rdbMediaReport4 = mediareportText[3].Type;
                    }
                    if (mediareportText.Count >= 5)
                    {
                        sProposal.txtMediaReport5 = mediareportText[4].Link;
                        sProposal.rdbMediaReport5 = mediareportText[4].Type;
                    }
                }


                // 提案類型
                foreach (var item in Enum.GetValues(typeof(ProposalDealType)))
                {
                    dealType[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)item);
                }

                var dealSubType = ProposalFacade.GetDealSubType(sProposal.DealType.ToString());
                ViewBag.DealSubType = dealSubType;

                //配送方式
                foreach (var item in Enum.GetValues(typeof(ProposalDeliveryMethod)))
                {
                    deliveryMethod[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDeliveryMethod)item);
                }

                //檔次特色
                if (!string.IsNullOrEmpty(pro.DealCharacterFlagText))
                {
                    Dictionary<ProposalDealCharacter, string> dealcharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(pro.DealCharacterFlagText);
                    foreach (var item in Enum.GetValues(typeof(ProposalDealCharacter)))
                    {
                        if (Helper.IsFlagSet(pro.DealCharacterFlag, (ProposalDealCharacter)item))
                        {
                            switch ((ProposalDealCharacter)item)
                            {
                                case ProposalDealCharacter.ToHouse1:
                                    if (dealcharacterText.Keys.Contains((ProposalDealCharacter)item))
                                    {
                                        sProposal.DealCharacterToHouse1 = dealcharacterText[(ProposalDealCharacter)item];
                                    }
                                    break;
                                case ProposalDealCharacter.ToHouse2:
                                    if (dealcharacterText.Keys.Contains((ProposalDealCharacter)item))
                                    {
                                        sProposal.DealCharacterToHouse2 = dealcharacterText[(ProposalDealCharacter)item];
                                    }
                                    break;
                                case ProposalDealCharacter.ToHouse3:
                                    if (dealcharacterText.Keys.Contains((ProposalDealCharacter)item))
                                    {
                                        sProposal.DealCharacterToHouse3 = dealcharacterText[(ProposalDealCharacter)item];
                                    }
                                    break;
                                case ProposalDealCharacter.ToHouse4:
                                    if (dealcharacterText.Keys.Contains((ProposalDealCharacter)item))
                                    {
                                        sProposal.DealCharacterToHouse4 = dealcharacterText[(ProposalDealCharacter)item];
                                    }
                                    break;
                            }
                        }
                    }
                }

                if (string.IsNullOrEmpty(sProposal.DealCharacterToHouse1) && string.IsNullOrEmpty(sProposal.DealCharacterToHouse2)
                     && string.IsNullOrEmpty(sProposal.DealCharacterToHouse3) && string.IsNullOrEmpty(sProposal.DealCharacterToHouse4))
                {
                    sProposal.IsDealCharacterToHouse = true.ToString();
                }

                files = _sp.ProposalSellerFileListGet(pid).ToList();

                //賣家所屬購物車
                scfs = _sp.ShoppingCartFreightsGetBySellerGuid(
                                        new List<Guid>() { pro.SellerGuid }
                                        ).Where(x => x.FreightsStatus == (int)ShoppingCartFreightStatus.Approve).ToList();

                // 該商家業務2
                List<MultipleSalesModel> model = SellerFacade.SellerSaleGetBySellerGuid(pro.SellerGuid);
                foreach (var item in model)
                {
                    if (!selerSales.ContainsKey(item.OperationSalesId) && item.OperationSalesDeptId == EmployeeChildDept.S010.ToString())
                        selerSales[item.OperationSalesId] = item.OperationSalesEmpName;
                }


            }
            else
            {
                return RedirectToAction("ProposalList");
            }

            ViewBag.ShoppingCartFreights = scfs;
            ViewBag.Sellers = ProposalFacade.GetVbsSeller(UserId, true);
            ViewBag.DeliveryMethod = deliveryMethod;
            ViewBag.DealType = dealType;
            ViewBag.Files = files;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.ShoppingCartV2Enabled = config.ShoppingCartV2Enabled;
            ViewBag.IsConsignment = config.IsConsignment ? pro.Consignment : false;
            ViewBag.IsEnableVbsNewShipFile = config.IsEnableVbsNewShipFile;
            ViewBag.IsAgreeNewContractSeller = SellerFacade.IsAgreeNewContractSeller(pro.SellerGuid, (int)DeliveryType.ToHouse);

            return View(sProposal);
        }



        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProposalDealsUpload(int pid)
        {
            Core.ModelPartial.AjaxSellerContractResult result = new Core.ModelPartial.AjaxSellerContractResult();

            try
            {
                StringBuilder sb = new StringBuilder();
                Proposal pro = _sp.ProposalGet(pid);
                if (pro.IsLoaded && pro != null)
                {
                    ProposalMultiDealCollection multi = new ProposalMultiDealCollection();
                    StringBuilder errBuilder = new StringBuilder();
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        var file = Request.Files[i];

                        HSSFWorkbook hssfworkbook = new HSSFWorkbook(file.InputStream);
                        Sheet sheet = hssfworkbook.GetSheetAt(0);
                        Row row;

                        for (int k = 1; k <= sheet.LastRowNum; k++)
                        {
                            row = sheet.GetRow(k);
                            if (row != null)
                            {
                                //check
                                if (IsNullOrEmpty(row.GetCell(0)) == ""
                                    || IsNullOrEmpty(row.GetCell(1)) == ""
                                    || IsNullOrEmpty(row.GetCell(2)) == "")
                                {
                                    continue;
                                }
                                if (IsNullOrEmpty(row.GetCell(0)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫方案內容" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(0)).Length > 400)
                                {
                                    errBuilder.AppendLine("方案內容不得超過400字" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(1)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫原價" + Environment.NewLine);
                                }
                                else
                                {
                                    if (IsNullOrEmpty(row.GetCell(1)).ToString().IndexOf(".") >= 0)
                                    {
                                        errBuilder.AppendLine("原價不可有小數點" + Environment.NewLine);
                                    }
                                }
                                if (IsNullOrEmpty(row.GetCell(2)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫售價" + Environment.NewLine);
                                }
                                else
                                {
                                    if (IsNullOrEmpty(row.GetCell(2)).ToString().IndexOf(".") >= 0)
                                    {
                                        errBuilder.AppendLine("售價不可有小數點" + Environment.NewLine);
                                    }
                                }
                                if (row.GetCell(3) != null)
                                {
                                    if (row.GetCell(3).CellType == CellType.FORMULA)
                                    {
                                        //公式
                                        var data = row.GetCell(3).NumericCellValue;
                                        if (IsNullOrEmpty(data) == "")
                                        {
                                            errBuilder.AppendLine("必須填寫進貨價" + Environment.NewLine);
                                        }
                                        else
                                        {
                                            if (data.ToString().IndexOf(".") >= 0)
                                            {
                                                errBuilder.AppendLine("進貨價不可有小數點" + Environment.NewLine);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //非公式
                                        if (IsNullOrEmpty(row.GetCell(3)) == "")
                                        {
                                            errBuilder.AppendLine("必須填寫進貨價" + Environment.NewLine);
                                        }
                                        else
                                        {
                                            if (IsNullOrEmpty(row.GetCell(3)).ToString().IndexOf(".") >= 0)
                                            {
                                                errBuilder.AppendLine("進貨價不可有小數點" + Environment.NewLine);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    errBuilder.AppendLine("必須填寫進貨價" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(4)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫最大購買數" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(5)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫每人限購" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(6)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫運費(元/次)" + Environment.NewLine);
                                }
                                if (IsNullOrEmpty(row.GetCell(7)) == "")
                                {
                                    errBuilder.AppendLine("必須填寫免運份數" + Environment.NewLine);
                                }
                                if (errBuilder.Length == 0)
                                {
                                }
                                else
                                {
                                    throw new Exception(errBuilder.ToString());
                                }

                                string ItemName = row.GetCell(0).ToString();    //優惠內容
                                decimal OriPrice = 0;
                                decimal.TryParse(row.GetCell(1).ToString(), out OriPrice);    //原價
                                decimal ItemPrice = 0;
                                decimal.TryParse(row.GetCell(2).ToString(), out ItemPrice);    //售價
                                decimal Cost = 0;
                                if (row.GetCell(3) != null)
                                {
                                    if (row.GetCell(3).CellType == CellType.FORMULA)
                                    {
                                        //公式
                                        decimal.TryParse(row.GetCell(3).NumericCellValue.ToString(), out Cost);    //進貨價
                                    }
                                    else
                                    {
                                        //非公式
                                        decimal.TryParse(row.GetCell(3).ToString(), out Cost);    //進貨價
                                    }
                                }
                                int OrderTotalLimit = 0;
                                int.TryParse(row.GetCell(4).ToString(), out OrderTotalLimit);    //最大購買數
                                if (OrderTotalLimit == 0)
                                {
                                    OrderTotalLimit = 99999;
                                }
                                int OrderMaxPersonal = 0;
                                int.TryParse(row.GetCell(5).ToString(), out OrderMaxPersonal);    //每人限購
                                if (OrderMaxPersonal == 0)
                                {
                                    OrderMaxPersonal = 20;
                                }

                                int AtmMaximum = 0;
                                int.TryParse((OrderTotalLimit / 5).ToString(), out AtmMaximum);    //ATM保留
                                if (AtmMaximum == 0)
                                {
                                    AtmMaximum = Convert.ToInt32(OrderTotalLimit / 4);
                                }
                                decimal Freight = 0;
                                decimal.TryParse(row.GetCell(6).ToString(), out Freight);    //運費(元/次)
                                int NoFreightLimit = 0;
                                int.TryParse(row.GetCell(7).ToString(), out NoFreightLimit);    //免運份數
                                string SellerOptions = IsNullOrEmpty(row.GetCell(8));   //選購項目
                                decimal GrossMargin = Math.Floor((ItemPrice == 0 ? 0 : (ItemPrice - Cost) / ItemPrice) * 10000) / 100;    //無條件捨去

                                int FreightsId = 0;
                                if (config.ShoppingCartV2Enabled)
                                {
                                    List<ShoppingCartFreight> scgs = _sp.ShoppingCartFreightsGetBySellerGuid(new List<Guid>() { pro.SellerGuid })
                                                .Where(x => x.Freights == 0 && x.FreightsStatus == (int)ShoppingCartFreightStatus.Approve).ToList();
                                    if (scgs.Count > 0)
                                    {
                                        FreightsId = scgs.FirstOrDefault().Id;
                                    }
                                }

                                multi.Add(new ProposalMultiDeal
                                {
                                    Pid = pro.Id,
                                    ItemName = ItemName,
                                    OrigPrice = OriPrice,
                                    ItemPrice = ItemPrice,
                                    Cost = Cost,
                                    OrderTotalLimit = OrderTotalLimit,
                                    OrderMaxPersonal = OrderMaxPersonal,
                                    AtmMaximum = AtmMaximum,
                                    Freights = Freight,
                                    NoFreightLimit = NoFreightLimit,
                                    SellerOptions = SellerOptions,
                                    FreightsId = FreightsId == 0 ? (int?)null : FreightsId,
                                    Sort = k,
                                    CreateId = UserName,
                                    CreateTime = DateTime.Now
                                });

                                sb.AppendFormat("優惠內容:{0},原價:{1},售價:{2},進貨價:{3},最大購買數:{4},每人限購:{5},運費(元/次):{6},免運份數:{7},選購項目:{8}",
                                    ItemName, OriPrice, ItemPrice, Cost, OrderTotalLimit, OrderMaxPersonal, Freight, NoFreightLimit, SellerOptions);
                            }

                        }

                        result.IsSuccess = true;
                    }

                    if (errBuilder.Length == 0)
                    {
                        _sp.ProposalMultiDealDeleteByPid(pro.Id);
                        _sp.ProposalMultiDealSetList(multi);

                        ProposalFacade.ProposalLog(pro.Id, "[上傳]" + sb.ToString(), Session[VbsSession.UserId.ToString()].ToString(), ProposalLogType.Seller);
                    }
                    else
                    {
                        throw new Exception(errBuilder.ToString());
                    }

                }
            }
            catch (Exception ex)
            {
                result.IsSuccess = false;
                result.Message = ex.Message;
            }
            return Json(result);
        }


        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProposalSellerFilesUpload(int pid)
        {
            List<ProposalSellerFile> data = new List<ProposalSellerFile>();

            List<string> ZipExtension = new List<string>
            {
                ".zip",".rar",".7z",".tar"
            };

            List<string> DenyFiles = new List<string>
            {
                ".exe"
            };

            string ProposalId = pid.ToString().PadLeft(7, '0');

            try
            {
                StringBuilder sb = new StringBuilder();
                Proposal pro = _sp.ProposalGet(pid);
                if (pro != null && pro.IsLoaded)
                {
                    Seller seller = _sp.SellerGet(pro.SellerGuid);

                    string FolderFile = ""; //單號-商家名稱-品牌名稱
                    if (pro.IsLoaded && pro != null)
                    {
                        for (int i = 0; i < Request.Files.Count; i++)
                        {
                            HttpPostedFileBase file = Request.Files[i];
                            string oriFileName = Request.Files[i].FileName;
                            string _Extension = oriFileName.Substring(oriFileName.LastIndexOf("."));
                            string realFileName = DateTime.Now.ToString("yyyyMMddHHmmss_") + oriFileName;


                            if (config.SellerContractIsFtp)
                            {
                                //FTP
                                string ftpFolder = "Seller_Files/" + ProposalId;
                                SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                                FolderFile = ftpFolder + "/" + realFileName;
                                cli.MakeDirectory(ftpFolder);
                                cli.SimpleUpload(file, ftpFolder, realFileName);
                            }
                            else
                            {
                                //UNC
                                string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                                string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, ProposalId);

                                if (!System.IO.Directory.Exists(Dir))
                                {
                                    System.IO.Directory.CreateDirectory(Dir);
                                }
                                string fname = System.IO.Path.Combine(Dir, realFileName);

                                if (DenyFiles.Contains(_Extension))
                                {
                                    continue;
                                }
                                file.SaveAs(fname);
                                //if (ZipExtension.Contains(_Extension))
                                //{
                                //    FolderFile = Dir + "/" + fileName;
                                //    file.SaveAs(fname);
                                //    UnZip(FolderFile, Dir);
                                //}
                                //else
                                //{
                                //    FolderFile = Dir.Replace(Server.MapPath("~\\"), "") + "/" + fileName;
                                //    file.SaveAs(fname);
                                //}
                            }

                            //寫入資料庫
                            ProposalSellerFile sellerFile = new ProposalSellerFile()
                            {
                                ProposalId = pro.Id,
                                OriFileName = oriFileName,
                                RealFileName = realFileName,
                                CreateId = UserName,
                                CreateTime = DateTime.Now,
                                FileStatus = (int)ProposalFileStatus.Nomal,
                                FileSize = unchecked((int)file.InputStream.Length)
                            };
                            _sp.ProposalSellerFileSet(sellerFile);

                            //寫入log
                            ProposalFacade.ProposalLog(pro.Id, "[新增檔案]：" + oriFileName + "", UserName, ProposalLogType.Seller);
                        }

                        //發送mail
                        if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                        {
                            ProposalFacade.ProposalFileChangeMail(pro);
                        }
                    }
                }
            }
            catch
            {

            }
            finally
            {
                data = _sp.ProposalSellerFileListGet(pid).ToList();
            }
            return Json(data);
        }

        [VbsAuthorize]
        public ActionResult ProposalSellerFilesDownload(int id)
        {
            ProposalSellerFile files = _sp.ProposalSellerFileGet(id);
            string ftpFolder = "Seller_Files/" + files.ProposalId.ToString().PadLeft(7, '0') + "/" + files.RealFileName;
            string FileName = files.OriFileName;

            if (config.SellerContractIsFtp)
            {
                //Use FTP
                SimpleFtpClient cli = new SimpleFtpClient(new Uri(config.SellerContractFtpUri + ftpFolder));
                cli.SimpleDownload(FileName, ftpFolder);
            }
            else
            {
                //Use Folder
                string BaseDir = string.IsNullOrEmpty(config.CkFinderBaseDir) ? System.Web.HttpContext.Current.Server.MapPath("~/Images/") : config.CkFinderBaseDir;
                string Dir = BaseDir == "" ? "" : string.Format("{0}imagesU/{1}/", BaseDir, files.ProposalId.ToString().PadLeft(7, '0'));

                System.Net.WebClient wc = new System.Net.WebClient();
                byte[] byteFiles = null;
                string FilePath = System.IO.Path.Combine(Dir, files.RealFileName);
                byteFiles = wc.DownloadData(FilePath);
                string fileName = System.IO.Path.GetFileName(FilePath);
                Response.AddHeader("content-disposition", "attachment;filename=" + Server.UrlEncode(fileName));
                Response.ContentType = "application/octet-stream";
                Response.BinaryWrite(byteFiles);
                Response.End();
            }
            return null;
        }

        [VbsAuthorize]
        public ActionResult ProposalSellerFilesDelete(int id)
        {
            List<ProposalSellerFile> fileList = ProposalFacade.ProposalSellerFileDelete(id, UserName, ProposalLogType.Seller);

            return Json(fileList);
        }

        private void UnZip(string source, string destination)
        {
            string zPath = @"C:\Program Files\7-Zip\7zG.exe";
            try
            {
                ProcessStartInfo pro = new ProcessStartInfo();
                pro.WindowStyle = ProcessWindowStyle.Hidden;
                pro.FileName = zPath;
                pro.Arguments = "x \"" + source + "\" -o" + destination + " -y";
                Process x = Process.Start(pro);
                x.WaitForExit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private string IsNullOrEmpty(object obj)
        {
            return (obj ?? "").ToString();
        }

        /// <summary>
        /// 取得優惠方案
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public string PorposalMultiDealsGet(string pid)
        {
            int id = 0;
            int.TryParse(pid, out id);
            ProposalMultiDealCollection multiDeals = _sp.ProposalMultiDealGetByPid(id);
            return new JsonSerializer().Serialize(multiDeals);
        }

        [VbsAuthorize]
        public ActionResult GetEmployeeNameArray(string empName)
        {
            empName = empName.Replace("'", "").Replace("<", "").Replace(">", "").Replace("\\", "");

            //宅配部門
            List<Department> dept = new List<Department>();
            dept.Add(HumanFacade.GetDepartmentByDeptId(EmployeeChildDept.S010.ToString()));

            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(empName) && empName.Length >= 1 && empName.Length < 6)
            {
                var emps = SellerFacade.GetEmployeeNameArrayByDept(empName, dept);
                foreach (var e in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = e.Key.ToString(),
                        Label = e.Value
                    });
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [VbsAuthorize]
        public ActionResult GetEmployeeName(string empName)
        {
            if (!string.IsNullOrEmpty(empName))
            {
                var emp = SellerFacade.GetToHouseSalesmanName(empName).FirstOrDefault();
                return Json(emp, JsonRequestBehavior.AllowGet);
            }
            return Json(string.Empty, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 刪除提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult DeleteSellerProposal(int pid)
        {
            Proposal pro = _sp.ProposalGet(pid);
            bool flag = false;

            if (pro != null && pro.IsLoaded)
            {
                if (pro.SellerProposalFlag <= ((int)SellerProposalFlag.Apply | (int)SellerProposalFlag.Saved))
                {
                    _sp.ProposalDelete(pid);
                    flag = true;
                }
            }

            return Json(flag, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 複製提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult CloneSellerProposal(int pid)
        {
            string UserName = Session[VbsSession.UserId.ToString()].ToString();
            int proId = PponFacade.CreateProposalByCloneSellerPro(pid, ProposalCopyType.Seller, UserName, true, false);

            return Json(proId, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 送交提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SendSellerProposal(int pid)
        {
            Proposal pro = _sp.ProposalGet(pid);
            ProposalCouponEventContent pcec = _pp.ProposalCouponEventContentGetByPid(pid);
            dynamic obj = new
            {
                IsSuccess = false,
                Message = ""
            };

            if (pro != null && pro.IsLoaded)
            {
                string message = "";
                message = ProposalContentCheckSave(pro);

                if (!string.IsNullOrEmpty(message))
                {
                    message += "請確認您已先儲存資料後再送出。\r\n";
                    return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = message
                    }, JsonRequestBehavior.AllowGet);
                }

                pro.PassSeller = 1;
                pro.SendSeller = 1;
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                pro.ModifyTime = DateTime.Now;
                pro.ModifyId = UserName;
                _sp.ProposalSet(pro);

                //紀錄Log
                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Send);
                ProposalFacade.ProposalLog(pro.Id, "[" + title + "] ", Session[VbsSession.UserId.ToString()].ToString(), ProposalLogType.Seller);


                //商家提案通知信
                ViewEmployee opSalesEmp = _hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId);
                string SaleName = "";
                string SaleMail = "";
                if (opSalesEmp.IsLoaded && opSalesEmp != null)
                {
                    SaleName = opSalesEmp.EmpName;
                    SaleMail = opSalesEmp.Email;

                    string Memo = @"• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔";

                    ProposalFacade.ProposalLog(pro.Id, "[商家提案通知信件]" + SaleMail, Session[VbsSession.UserId.ToString()].ToString(), ProposalLogType.Initial);

                    ProposalFacade.SendEmail(new List<string> { SaleMail }, "【商家提案通知】 NO." + pro.Id + "  " + (!string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName),
                        ProposalFacade.SendSellerProposalMailContent(pro, SaleName, config.SiteUrl + "/sal/ProposalContent.aspx?pid=",
                        Memo));
                }


                return Json(
                    new
                    {
                        IsSuccess = true,
                        Message = ""
                    }, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = ""
                    }, JsonRequestBehavior.AllowGet);
        }



        /// <summary>
        /// 編輯提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult EditSellerProposal(int pid)
        {
            Proposal pro = _sp.ProposalGet(pid);
            bool flag = false;

            if (pro != null && pro.IsLoaded)
            {
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                pro.ModifyId = UserName;
                pro.ModifyTime = DateTime.Now;
                _sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SellerEditor);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Seller);
                flag = true;
            }

            return Json(flag, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 覆核確認
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ConfirmSellerProposal(int pid)
        {
            Proposal pro = _sp.ProposalGet(pid);
            bool flag = false;

            if (pro != null && pro.IsLoaded)
            {
                string message = "";
                message = ProposalContentCheckSave(pro);

                if (!string.IsNullOrEmpty(message))
                {
                    message += "請確認您已先儲存資料後再送出。\r\n";
                    return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = message
                    }, JsonRequestBehavior.AllowGet);
                }

                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Check));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                if (pro.Status >= (int)ProposalStatus.Apply)
                {
                    //已過到業務的提案單，不須再更新狀態，否則會影響建檔的按鈕
                }
                else
                {
                    pro.Status = (int)ProposalStatus.SellerCreate;
                }
                pro.ModifyTime = DateTime.Now;
                pro.ModifyId = UserName;
                pro.DeliveryMethod = pro.DeliveryMethod ?? (int)ProposalDeliveryMethod.Normal;
                pro.PassSeller = 1;
                _sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Check);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Seller);


                MailToConfirmList(pro);

                flag = true;
            }

            //return Json(flag, JsonRequestBehavior.AllowGet);
            return Json(
                    new
                    {
                        IsSuccess = flag,
                        Message = ""
                    }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 提案覆核通知信
        /// </summary>
        /// <param name="pro">提案單</param>
        private void MailToConfirmList(Proposal pro)
        {
            #region Mail(頁確聯絡人;宅配業助;業務)
            ////取得宅配特助
            //string orgName = "SalesAssistant";
            List<string> users = new List<string>();
            List<string> MailUsers = new List<string>();
            ProposalCouponEventContent pcec = _pp.ProposalCouponEventContentGetByPid(pro.Id);
            //List<Organization> childList = MemberFacade.OrganizationGetChildList(orgName);
            //List<string> DeptS010 = _hmp.ViewEmployeeCollectionGetByFilter(ViewEmployee.Columns.DeptId, EmployeeChildDept.S010, false).Select(x => x.Email).Distinct().ToList();
            //foreach (Organization org in childList)
            //{
            //    users = users.Union(System.Web.Security.Roles.GetUsersInRole(org.Name)).ToList();
            //}
            //foreach (string u in users)
            //{
            //    if (DeptS010.Contains(u))
            //    {
            //        MailUsers.Add(u);
            //    }
            //}
            ////頁確聯絡人(賣家一般聯絡人)
            //Seller seller = _sp.SellerGet(pro.SellerGuid);
            //List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
            //if (contacts != null)
            //{
            //    //一般聯絡人
            //    foreach (Seller.MultiContracts c in contacts)
            //    {
            //        if (c.Type == ((int)ProposalContact.Normal).ToString())
            //        {
            //            MailUsers.Add(c.ContactPersonEmail);
            //        }
            //    }
            //}
            //業務
            ViewEmployee opSalesEmp = _hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId);
            string SaleName = "";
            string SaleMail = "";
            if (opSalesEmp.IsLoaded && opSalesEmp != null)
            {
                SaleName = opSalesEmp.EmpName;
                SaleMail = opSalesEmp.Email;
                MailUsers.Add(SaleMail);

                string Memo = @"• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔";
                ProposalFacade.SendEmail(MailUsers, "【提案覆核通知】 NO." + pro.Id + "  " + (!string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName),
                    ProposalFacade.SendSellerProposalMailContent(pro, SaleName, config.SiteUrl + "/sal/ProposalContent.aspx?pid=",
                    Memo));
            }

            #endregion
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetDeSalesId(Guid sellerGuid, int opSalesId)
        {
            var deSalesId = SellerFacade.SellerSaleGetBySellerGuid(sellerGuid).Where(x => x.OperationSalesId == opSalesId).Select(x => x.DevelopeSalesId).First();

            return Json(deSalesId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProposalContentSave(SellerProposal model)
        {

            try
            {
                string UserName = Session[VbsSession.UserId.ToString()].ToString();


                Proposal pro = _sp.ProposalGet(model.Pid);
                ProposalMultiDealCollection multiDeals = _sp.ProposalMultiDealGetByPid(model.Pid);

                if (pro != null && pro.IsLoaded)
                {
                    Proposal oriPro = pro.Clone();
                    ProposalMultiDealCollection oriMultiDeal = multiDeals.Clone();
                    GetProposalData(model, pro);
                    ProposalFacade.GetProposalMultiDeal(pro.Id, model.MultiDeals, multiDeals, UserName);



                    pro.ModifyId = UserName;
                    pro.ModifyTime = DateTime.Now;

                    _sp.ProposalSet(pro);
                    _sp.ProposalMultiDealSetList(multiDeals);

                    //提案單異動紀錄
                    ProposalFacade.CompareProposal(pro, oriPro, UserName, ProposalLogType.Seller);

                    if (oriMultiDeal == null)
                    {
                        oriMultiDeal = new ProposalMultiDealCollection();
                    }
                    //比較優惠內容修改Log
                    ProposalFacade.CompareMultiDeals(pro.Id, multiDeals, oriMultiDeal, UserName, ProposalSourceType.Original, ProposalLogType.Seller);


                    TempData["Success"] = "OK!";
                }


            }
            catch (Exception ex)
            {
                ProposalFacade.ProposalPerformanceLogSet("ProposalContentSave", ex.Message, UserName);
                TempData["ErrorMessage"] = "ERROR!";

            }

            return RedirectToAction("ProposalContent", new { pid = model.Pid });
        }

        private string ProposalContentCheckSave(Proposal pro)
        {
            string message = "";
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);

            /*
            * Check Data
            */

            if (pro.OperationSalesId == 0)
            {
                message += "請輸入負責業務。\r\n";
            }
            if (string.IsNullOrEmpty(pro.BrandName))
            {
                message += "請輸入商品名稱。\r\n";
            }
            if (pro.DealSubType == 0)
            {
                message += "請選擇商品類型。\r\n";
            }
            if (!Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Photographers) &&
                !Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto) &&
                !Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto)
                )
            {
                message += "請選擇照片來源。\r\n";
            }
            if (pro.VendorReceiptType == (int)Core.VendorReceiptType.Other)
            {
                if (string.IsNullOrEmpty(pro.Othermessage))
                {
                    message += "店家單據開立方式為「其他」，請輸入說明\r\n";
                }
            }

            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.DealStar))
            {
                if (specialText.ContainsKey(ProposalSpecialFlag.DealStar))
                {
                    if (string.IsNullOrEmpty(specialText[ProposalSpecialFlag.DealStar]))
                    {
                        message += "「本檔主打星」未勾選，請確認後再行送交提案。\r\n";
                    }
                }
                else
                {
                    message += "「本檔主打星」未勾選，請確認後再行送交提案。\r\n";
                }
            }

            if (string.IsNullOrEmpty(pro.ProductSpec))
            {
                message += "請輸入規格功能/主要成份。\r\n";
            }
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.Beauty))
            {
                if (string.IsNullOrEmpty(specialText[ProposalSpecialFlag.Beauty]))
                {
                    message += "「醫療 / 妝廣字號」欄位勾選後無值填入，請確認後再行送交提案。\r\n";
                }
            }
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.ExpiringProduct))
            {
                if (string.IsNullOrEmpty(specialText[ProposalSpecialFlag.ExpiringProduct]))
                {
                    message += "「即期品」欄位勾選後無值填入，請確認後再行送交提案。\r\n";
                }
            }
            if (Helper.IsFlagSet(pro.SpecialFlag, (ProposalSpecialFlag)ProposalSpecialFlag.InspectRule))
            {
                if (string.IsNullOrEmpty(specialText[ProposalSpecialFlag.InspectRule]))
                {
                    message += "「檢驗規定」欄位勾選後無值填入，請確認後再行送交提案。\r\n";
                }
            }
            //檔次特色
            if (!string.IsNullOrEmpty(pro.DealCharacterFlagText))
            {
                if (Helper.IsFlagSet(pro.DealCharacterFlag, ProposalDealCharacter.ToHouse1))
                {
                    Dictionary<ProposalDealCharacter, string> dealcharacterText = new JsonSerializer().Deserialize<Dictionary<ProposalDealCharacter, string>>(pro.DealCharacterFlagText);
                    int idxCharacter = 0;
                    if (dealcharacterText.ContainsKey(ProposalDealCharacter.ToHouse1))
                    {
                        if (!string.IsNullOrEmpty(dealcharacterText[ProposalDealCharacter.ToHouse1]))
                        {
                            idxCharacter++;
                        }
                    }
                    if (dealcharacterText.ContainsKey(ProposalDealCharacter.ToHouse2))
                    {
                        if (!string.IsNullOrEmpty(dealcharacterText[ProposalDealCharacter.ToHouse2]))
                        {
                            idxCharacter++;
                        }
                    }
                    if (dealcharacterText.ContainsKey(ProposalDealCharacter.ToHouse3))
                    {
                        if (!string.IsNullOrEmpty(dealcharacterText[ProposalDealCharacter.ToHouse3]))
                        {
                            idxCharacter++;
                        }
                    }
                    if (idxCharacter < 3)
                    {
                        message += "「檔次特色」請輸入至少三項，若無資料請輸入「-」。\r\n";
                    }
                }
            }
            return message;
        }

        private Proposal GetProposalData(SellerProposal model, Proposal pro)
        {
            Dictionary<ProposalSpecialFlag, string> specialText = new JsonSerializer().Deserialize<Dictionary<ProposalSpecialFlag, string>>(pro.SpecialFlagText);
            if (specialText == null)
            {
                specialText = new Dictionary<ProposalSpecialFlag, string>();
            }

            pro.OperationSalesId = model.OpSalesId;
            if (pro.DevelopeSalesId == 0)
                pro.DevelopeSalesId = model.OpSalesId;
            if (!string.IsNullOrEmpty(model.BrandName))
            {
                pro.BrandName = model.BrandName.Trim();
            }
            else
            {
                pro.BrandName = model.BrandName;
            }
            if (string.IsNullOrEmpty(pro.PicAlt))
            {
                pro.PicAlt = model.BrandName;
            }

            pro.DealType = model.DealType;
            pro.DealSubType = model.DealSubType;
            pro.VendorReceiptType = model.VendorReceiptType;
            pro.DeliveryMethod = model.DeliveryMethod;
            pro.ContractMemo = model.ContractMemo;
            pro.Othermessage = model.AccountingMessage;
            pro.ProductSpec = ProposalFacade.RemoveSpecialCharacter(model.ProductSpec);
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.VendorReceiptType == (int)VendorReceiptType.NoTaxInvoice, pro.SpecialFlag, ProposalSpecialFlag.FreeTax));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.IsParallelImportation, pro.SpecialFlag, ProposalSpecialFlag.ParallelImportation));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.Photographers, pro.SpecialFlag, ProposalSpecialFlag.Photographers));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.SellerPhoto, pro.SpecialFlag, ProposalSpecialFlag.SellerPhoto));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.FTPPhoto, pro.SpecialFlag, ProposalSpecialFlag.FTPPhoto));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.IsBeauty, pro.SpecialFlag, ProposalSpecialFlag.Beauty));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.IsExpiringProduct, pro.SpecialFlag, ProposalSpecialFlag.ExpiringProduct));
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(model.IsInspectRule, pro.SpecialFlag, ProposalSpecialFlag.InspectRule));
            bool IsDealStar = false;
            bool.TryParse(model.IsDealStar, out IsDealStar);
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(!IsDealStar, pro.SpecialFlag, ProposalSpecialFlag.DealStar));//本檔主打星
            pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(!string.IsNullOrEmpty(model.StoreStory), pro.SpecialFlag, ProposalSpecialFlag.StoreStory));//店家/品牌故事
            bool fileFlag = false;
            bool.TryParse(model.FileDone.ToString(), out fileFlag);
            pro.FilesDone = fileFlag;
            if (!specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.ParallelImportation))
            {
                specialText.Add(ProposalSpecialFlag.ParallelImportation, string.Empty);
            }
            if (!specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.Photographers))
            {
                specialText.Add(ProposalSpecialFlag.Photographers, "|");
            }
            if (!specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.SellerPhoto))
            {
                specialText.Add(ProposalSpecialFlag.SellerPhoto, string.Empty);
            }
            if (!specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.FTPPhoto))
            {
                specialText.Add(ProposalSpecialFlag.FTPPhoto, string.Empty);
            }
            if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.Beauty))
            {
                specialText.Remove(ProposalSpecialFlag.Beauty);
            }
            specialText.Add(ProposalSpecialFlag.Beauty, ProposalFacade.RemoveSpecialCharacter(model.Beauty.Trim()));
            if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.ExpiringProduct))
            {
                specialText.Remove(ProposalSpecialFlag.ExpiringProduct);
            }
            specialText.Add(ProposalSpecialFlag.ExpiringProduct, ProposalFacade.RemoveSpecialCharacter(model.ExpiringProduct.Trim()));
            if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.InspectRule))
            {
                specialText.Remove(ProposalSpecialFlag.InspectRule);
            }
            specialText.Add(ProposalSpecialFlag.InspectRule, ProposalFacade.RemoveSpecialCharacter(model.InspectRule.Trim()));
            if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.StoreStory))
            {
                specialText.Remove(ProposalSpecialFlag.StoreStory);
            }
            specialText.Add(ProposalSpecialFlag.StoreStory, model.StoreStory);

            //本檔主打星
            if (!IsDealStar)
            {
                if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.DealStar))
                {
                    specialText.Remove(ProposalSpecialFlag.DealStar);
                }
                specialText.Add(ProposalSpecialFlag.DealStar, model.DealStar);
            }
            else
            {
                if (specialText.ContainsKey((ProposalSpecialFlag)ProposalSpecialFlag.DealStar))
                {
                    specialText.Remove(ProposalSpecialFlag.DealStar);
                }
                specialText.Add(ProposalSpecialFlag.DealStar, "");
            }

            pro.SpecialFlagText = new JsonSerializer().Serialize(specialText);

            Dictionary<ProposalDealCharacter, string> dealcharacterText = new Dictionary<ProposalDealCharacter, string>();
            bool IsDealCharacterToHouse = false;
            bool.TryParse(model.IsDealCharacterToHouse, out IsDealCharacterToHouse);
            if (!IsDealCharacterToHouse)
            {
                dealcharacterText.Add(ProposalDealCharacter.ToHouse1, model.DealCharacterToHouse1);
                dealcharacterText.Add(ProposalDealCharacter.ToHouse2, model.DealCharacterToHouse2);
                dealcharacterText.Add(ProposalDealCharacter.ToHouse3, model.DealCharacterToHouse3);
                dealcharacterText.Add(ProposalDealCharacter.ToHouse4, model.DealCharacterToHouse4);
            }

            pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(string.IsNullOrEmpty(model.DealCharacterToHouse1) ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse1));//檔次特色
            pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(string.IsNullOrEmpty(model.DealCharacterToHouse2) ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse2));
            pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(string.IsNullOrEmpty(model.DealCharacterToHouse3) ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse3));
            pro.DealCharacterFlag = Convert.ToInt32(Helper.SetFlag(string.IsNullOrEmpty(model.DealCharacterToHouse4) ? false : true, pro.DealCharacterFlag, ProposalDealCharacter.ToHouse4));

            //出貨方式

            if (model.ShipType == ((int)DealShipType.Ship72Hrs).ToString())
            {
                pro.ShipType = (int)DealShipType.Ship72Hrs;
                pro.ShipText2 = "";
                pro.ShipText3 = "";
                pro.ShipOther = "";
            }
            else if (model.ShipType == (((int)DealShipType.Normal)).ToString() + "-1")
            {
                pro.ShipType = (int)DealShipType.Normal;
                pro.ShipText1 = "";
                pro.ShipText2 = "";
                pro.ShipText3 = model.txtShipText3;
                pro.ShipOther = "";
                pro.ShippingdateType = (int)DealShippingDateType.Normal;
            }
            else if (model.ShipType == (((int)DealShipType.Normal)).ToString() + "-2")
            {
                pro.ShipType = (int)DealShipType.Normal;
                pro.ShipText1 = model.txtShipText1;
                pro.ShipText2 = model.txtShipText2;
                pro.ShipText3 = "";
                pro.ShipOther = "";
                pro.ShippingdateType = (int)DealShippingDateType.Special;
            }
            else if (model.ShipType == ((int)DealShipType.Other).ToString())
            {
                pro.ShipType = (int)DealShipType.Other;
                pro.ShipText3 = "";
                pro.ShipText2 = "";
                pro.ShipText3 = "";
                pro.ShipOther = model.ShipTypeOther;
            }

            //媒體/報導
            pro.MediaReportFlag = model.IsMediaReportFlag ? model.IsMediaReportPic ? (int)ProposalMediaReport.Pic : (int)ProposalMediaReport.Link : (int)ProposalMediaReport.None;

            ProposalMediaReportLink MediaReportLink = null;
            List<ProposalMediaReportLink> ProposalMediaReportLinkList = new List<ProposalMediaReportLink>();
            if (!model.IsMediaReportPic)
            {
                for (int i = 1; i <= 5; i++)
                {
                    MediaReportLink = new ProposalMediaReportLink();
                    switch (i)
                    {
                        case 1:
                            if (model.txtMediaReport1 != "")
                            {
                                MediaReportLink.Type = model.rdbMediaReport1;
                                MediaReportLink.Link = model.txtMediaReport1;
                                ProposalMediaReportLinkList.Add(MediaReportLink);
                            }
                            break;
                        case 2:
                            if (model.txtMediaReport2 != "")
                            {
                                MediaReportLink.Type = model.rdbMediaReport2;
                                MediaReportLink.Link = model.txtMediaReport2;
                                ProposalMediaReportLinkList.Add(MediaReportLink);
                            }
                            break;
                        case 3:
                            if (model.txtMediaReport3 != "")
                            {
                                MediaReportLink.Type = model.rdbMediaReport3;
                                MediaReportLink.Link = model.txtMediaReport3;
                                ProposalMediaReportLinkList.Add(MediaReportLink);
                            }
                            break;
                        case 4:
                            if (model.txtMediaReport4 != "")
                            {
                                MediaReportLink.Type = model.rdbMediaReport4;
                                MediaReportLink.Link = model.txtMediaReport4;
                                ProposalMediaReportLinkList.Add(MediaReportLink);
                            }
                            break;
                        case 5:
                            if (model.txtMediaReport5 != "")
                            {
                                MediaReportLink.Type = model.rdbMediaReport5;
                                MediaReportLink.Link = model.txtMediaReport5;
                                ProposalMediaReportLinkList.Add(MediaReportLink);
                            }
                            break;

                    }
                }
            }
            pro.MediaReportFlagText = new JsonSerializer().Serialize(ProposalMediaReportLinkList);


            pro.DealCharacterFlagText = new JsonSerializer().Serialize(dealcharacterText);
            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Saved));

            if (string.IsNullOrEmpty(pro.SaleMarketAnalysis))
            {
                var SaleMarketAnalysis = new ProposalMarketAnalysis
                {
                    GomajiPrice = "",
                    GomajiLink = "",
                    KuobrothersPrice = "",
                    KuobrothersLink = "",
                    CrazymikePrice = "",
                    CrazymikeLink = "",
                    EzPricePrice1 = "",
                    EzPriceLink1 = "",
                    EzPricePrice2 = "",
                    EzPriceLink2 = "",
                    EzPricePrice3 = "",
                    EzPriceLink3 = "",
                    PinglePrice1 = "",
                    PingleLink1 = "",
                    PinglePrice2 = "",
                    PingleLink2 = "",
                    PinglePrice3 = "",
                    PingleLink3 = "",
                    SimilarPrice = "",
                    SimilarLink = "",
                    OtherShop1 = "",
                    OtherPrice1 = "",
                    OtherLink1 = "",
                    OtherShop2 = "",
                    OtherPrice2 = "",
                    OtherLink2 = "",
                    OtherShop3 = "",
                    OtherPrice3 = "",
                    OtherLink3 = ""
                };
                pro.SaleMarketAnalysis = new JsonSerializer().Serialize(SaleMarketAnalysis);
            }

            return pro;
        }

        


        

        [VbsAuthorize]
        public ActionResult ProposalConsent()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            //商家須先簽訂同意書
            VbsMembership member = _mp.VbsMembershipGetByAccountId(UserId);
            if (member != null && member.IsLoaded)
            {
                if (member.ProposalConsent)
                {
                    //商家已簽署過，無須再簽，避免時間被重壓
                    return RedirectToAction("ProposalList");
                }
            }

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            return View();
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProposalConsentConfirm()
        {
            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            //商家須先簽訂同意書
            VbsMembership member = _mp.VbsMembershipGetByAccountId(UserId);
            if (member != null && member.IsLoaded)
            {
                if (!member.ProposalConsent)
                {
                    member.ProposalConsent = true;
                    member.ProposalConsentDate = DateTime.Now;
                    _mp.VbsMembershipSet(member);
                }
                return RedirectToAction("ProposalList");
            }

            return RedirectToAction("ProposalAgreeProvision");
        }

        [VbsAuthorize]
        public ActionResult ProposalList()
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                ViewBag.LoginBy17LifeSimulate = true;
            }

            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            Dictionary<int, string> statusFlag = sellerProposalStatusGet();

            Dictionary<Guid, string> sellers = ProposalFacade.GetVbsSeller(UserId, true);

            bool isShowContract = false;

            //商家須先簽訂同意書
            VbsMembership member = _mp.VbsMembershipGetByAccountId(UserId);
            if (member != null && member.IsLoaded)
            {
                if (!member.ProposalConsent)
                {
                    return RedirectToAction("ProposalConsent");
                }
            }
            List<Seller> newSellers = new List<Seller>();
            foreach (var da in sellers)
            {
                Seller s = _sp.SellerGet(da.Key);
                if (s != null && s.IsLoaded)
                {
                    newSellers.Add(s);
                }

                //檢查是否有換約需要商家確認-只有宅配合約
                if (!string.IsNullOrEmpty(config.SellerContractVersionHouse) && s.ContractVersionHouse != config.SellerContractVersionHouse && !isShowContract)
                {
                    isShowContract = true;
                }
            }

            ViewBag.Sellers = newSellers;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.StatusFlag = statusFlag;
            ViewBag.ShowContract = isShowContract;
            ViewBag.IsConfirmWms = WmsFacade.isConfirmWms(null, VbsCurrent.AccountId);

            return View();
        }

        [VbsAuthorize]
        public ActionResult ContractChangeNotice(bool visible = false)
        {
            if (visible)
            {
                return View();
            }
            return RedirectToAction("RedirectToActionAfterLogin");
        }

        private Dictionary<int, string> sellerProposalStatusGet()
        {
            Dictionary<int, string> statusFlag = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(SellerProposalFlag)))
            {
                if ((int)item == (int)SellerProposalFlag.Initial || (int)item == (int)SellerProposalFlag.SellerEditor || (int)item == (int)SellerProposalFlag.Saved
                        || (int)item == (int)SellerProposalFlag.SaleEditor)
                {
                    continue;
                }
                statusFlag[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SellerProposalFlag)item);
            }
            return statusFlag;
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProposalListGet(string ProposalId, string BrandName, string Deals, string OrderTimeS, string OrderTimeE,
            string SaleName, string UniqueId, int Status, string isWms,
            int PerDataCnt, int CurrentPage)
        {
            int TotalCount = 0; //總筆數
            int TotalPages = 0; //總頁數

            string UserId = Session[VbsSession.UserId.ToString()].ToString();

            List<Guid> SellerGuids = ProposalFacade.GetVbsSeller(UserId).Select(x => x.Key).Distinct().ToList();

            Guid bid = Guid.Empty;
            if (!string.IsNullOrEmpty(UniqueId))
            {
                int _UniqueId = 0;
                int.TryParse(UniqueId, out _UniqueId);
                ViewPponDeal vpd = _pp.ViewPponDealGetByUniqueId(_UniqueId);
                if (vpd != null && vpd.IsLoaded)
                {
                    bid = vpd.BusinessHourGuid;
                }
            }

            int pid = 0;
            int.TryParse(ProposalId, out pid);

            List<Proposal> Proposals = _sp.SellerProposalSellerGetList(SellerGuids, pid, bid, BrandName, Deals, OrderTimeS, OrderTimeE, SaleName, isWms)
                .Where(x => Helper.IsFlagSet(x.SellerProposalFlag, SellerProposalFlag.Apply)).OrderByDescending(x => x.Id).ToList();

            if (!string.IsNullOrEmpty(SaleName))
            {
                List<int> empIds = _hmp.ViewEmployeeByLikeUserName(SaleName).Select(x => x.UserId).Distinct().ToList();
                Proposals = Proposals.Where(x => empIds.Contains(x.OperationSalesId ?? 0)).ToList();
            }


            if (Status != -1)
            {
                switch (Status)
                {
                    case 997:
                        //已排檔
                        Proposals = Proposals.Where(x => x.OrderTimeS != null && x.OrderTimeS.Value.Year != DateTime.MaxValue.Year && x.OrderTimeS.Value.Year != 1900).ToList();
                        break;
                    case 998:
                    //ON檔中
                    case 999:
                        //已結檔
                        List<Guid> bids = Proposals.Where(x => x.BusinessHourGuid != null).Select(x => x.BusinessHourGuid.Value).ToList();
                        ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(bids);

                        List<Proposal> tmpProposals = new List<Proposal>();
                        foreach (Proposal pro in Proposals)
                        {
                            var vpd = vpds.Where(x => x.BusinessHourGuid == pro.BusinessHourGuid).FirstOrDefault();
                            if (vpd != null)
                            {
                                if (Status == 998)
                                {
                                    //ON檔中
                                    if (vpd.BusinessHourOrderTimeS <= DateTime.Now && vpd.BusinessHourOrderTimeE >= DateTime.Now && vpd.Slug == null)
                                    {
                                        tmpProposals.Add(pro);
                                    }
                                }
                                else if (Status == 999)
                                {
                                    //已結檔
                                    if (vpd.BusinessHourOrderTimeE <= DateTime.Now || vpd.Slug != null)
                                    {
                                        tmpProposals.Add(pro);
                                    }
                                }

                            }
                        }
                        Proposals = tmpProposals;
                        break;
                    default:
                        //Proposals = Proposals.Where(x => x.SellerProposalStatus == Status).ToList();
                        Proposals = Proposals.Where(x => Helper.IsFlagSet(x.SellerProposalFlag, (SellerProposalFlag)Status)).ToList();
                        List<Proposal> tmpDefProposals = new List<Proposal>();

                        foreach (Proposal pro in Proposals)
                        {
                            int tmpStatus = 0;
                            foreach (var item in Enum.GetValues(typeof(SellerProposalFlag)))
                            {
                                if (Helper.IsFlagSet(pro.SellerProposalFlag, (SellerProposalFlag)item))
                                {
                                    if ((SellerProposalFlag)item == SellerProposalFlag.Saved
                                        || (SellerProposalFlag)item == SellerProposalFlag.SaleEditor
                                        || (SellerProposalFlag)item == SellerProposalFlag.SellerEditor)
                                    {
                                        continue;
                                    }
                                    tmpStatus = (int)item;    //提案狀態
                                }
                            }
                            if (tmpStatus == Status)
                            {
                                tmpDefProposals.Add(pro);
                            }
                        }


                        Proposals = tmpDefProposals;
                        break;
                }
            }
            //List<Proposal> pros = _sp.ProposalGetListBySellerGuid(s.Guid)
            //    .Where(x => Helper.IsFlagSet(x.SellerProposalFlag, SellerProposalFlag.Apply)).ToList();

            //目前先只查詢宅配檔次
            Proposals = Proposals.Where(x => x.DeliveryType == (int)DeliveryType.ToHouse
                                    && (x.ProposalCreatedType == (int)ProposalCreatedType.Seller || (x.ProposalCreatedType == (int)ProposalCreatedType.Sales
                                    &&  (!string.IsNullOrEmpty(x.BrandName) || !string.IsNullOrEmpty(x.DealName)) 
                                    ))
                ).ToList();

            TotalCount = Proposals.Count;
            TotalPages = TotalCount / PerDataCnt;
            if ((TotalCount > PerDataCnt) && (TotalCount % PerDataCnt != 0))
            {
                TotalPages += 1;
            }
            Proposals = Proposals.Skip((CurrentPage - 1) * PerDataCnt).Take(PerDataCnt).ToList();
            List<SellerProposalList> spls = new List<SellerProposalList>();

            ViewEmployeeCollection emps = _hmp.ViewEmployeeCollectionGetAll();

            foreach (Proposal pro in Proposals)
            {
                IViewPponDeal vpd = null;
                if (pro.BusinessHourGuid != null)
                {
                    //vpd = _pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                    vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(pro.BusinessHourGuid.Value);
                }
                ViewEmployee emp = emps.Where(x => x.UserId == pro.OperationSalesId).FirstOrDefault();
                //ViewEmployee emp = _hmp.ViewEmployeeGet(Employee.Columns.UserId, pro.SalesId);

                SellerProposalList spl = new SellerProposalList();
                spl.Pid = pro.Id;
                spl.IsWms = pro.IsWms;
                spl.ProposalSourceType = pro.ProposalSourceType;
                spl.DealType = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealType)pro.DealType);
                if (pro.DealSubType != (int)ProposalDealSubType.None)
                {
                    spl.DealSubType = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (ProposalDealSubType)pro.DealSubType);
                }

                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                if (multiDeals != null && multiDeals.Count() > 0)
                {
                    spl.MultiDeals = multiDeals.FirstOrDefault().BrandName + multiDeals.FirstOrDefault().ItemName;
                }
                if (vpd != null)
                {
                    if (vpd.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year && vpd.BusinessHourOrderTimeE.Year != DateTime.MaxValue.Year 
                        && vpd.BusinessHourOrderTimeS.Year != new DateTime(1900, 1, 1).Year && vpd.BusinessHourOrderTimeE.Year != new DateTime(1900, 1, 1).Year
                        )
                    {
                        spl.BusinessHourOrderTime = vpd.BusinessHourOrderTimeS + "~" + vpd.BusinessHourOrderTimeE;
                    }
                    else
                    {
                        spl.BusinessHourOrderTime = "";
                    }

                    spl.BusinessHourGuid = vpd.BusinessHourGuid.ToString();
                    if (vpd.UniqueId != null)
                    {
                        if (vpd.BusinessHourOrderTimeS != null && vpd.BusinessHourOrderTimeE != null
                            && vpd.BusinessHourOrderTimeS.Year != DateTime.MaxValue.Year && vpd.BusinessHourOrderTimeE.Year != DateTime.MaxValue.Year
                            && vpd.BusinessHourOrderTimeS.Year != new DateTime(1900, 1, 1).Year && vpd.BusinessHourOrderTimeE.Year != new DateTime(1900, 1, 1).Year)
                        {
                            //上檔後才出現
                            spl.UniqueId = vpd.UniqueId.Value.ToString();
                        }
                    }

                }
                if (emp != null)
                {
                    spl.SaleName = emp.EmpName;
                }


                foreach (var item in Enum.GetValues(typeof(SellerProposalFlag)))
                {
                    if (Helper.IsFlagSet(pro.SellerProposalFlag, (SellerProposalFlag)item))
                    {
                        if ((SellerProposalFlag)item == SellerProposalFlag.Saved
                            || (SellerProposalFlag)item == SellerProposalFlag.SaleEditor
                            || (SellerProposalFlag)item == SellerProposalFlag.SellerEditor)
                        {
                            continue;
                        }
                        spl.Status = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (SellerProposalFlag)item);    //提案狀態
                    }
                }


                spls.Add(spl);
            }
            dynamic obj = new
            {
                data = spls,
                totalCount = TotalCount,
                totalPages = TotalPages
            };

            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [VbsAuthorize]
        public int CreateProposal(string sid, bool IsTravelProposal, bool IsPBeautyProposal, int ProposalDeliveryType, bool IsWms)
        {
            string UserId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;

            Guid SellerGuid = Guid.Empty;
            Guid.TryParse(sid, out SellerGuid);

            if (SellerGuid != Guid.Empty)
            {
                if (ProposalFacade.IsVbsProposalNewVersion())
                {
                    return ProposalFacade.CreateProposalByHouse(UserId, "", SellerGuid, UserId, true, IsWms, ProposalCreatedType.Seller);
                }
                else
                {
                    return ProposalFacade.CreateProposal(UserId, "", SellerGuid, UserId, ProposalDeliveryType, IsTravelProposal, IsPBeautyProposal, true, false, ProposalCreatedType.Seller);
                }                
            }


            return 0;// 
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult IsAgreeNewContractSeller(Guid sid)
        {
            bool isAgree = false;
            isAgree = SellerFacade.IsAgreeNewContractSeller(sid, (int)DeliveryType.ToHouse);
            return Json(new { IsSuccess = isAgree });
        }
        #endregion

        #region 客服處理
        [VbsAuthorize]
        public ActionResult ServiceProcess(ServiceProcessModel model = null)
        {            
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            //優先權項目
            Dictionary<int, string> casePriority = new Dictionary<int, string>();
            foreach (priorityConvert pc in Enum.GetValues(typeof(priorityConvert)))
            {
                casePriority.Add((int)pc, Helper.GetEnumDescription(pc));
            }

            //問題分類項目
            Dictionary<int, string> problemType = _service.GetCustomerServiceCategory()
                .Where(x => x.IsNeedOrderId)
                .Select(x => new { x.CategoryId, x.CategoryName })
                .ToDictionary(x => x.CategoryId, x => x.CategoryName );

            //分頁長度
            int pageLength = 10;

            //查詢參數
            string[] queryParams = GetServiceProcessFilter(model);

            //商家Guid
            string sellerGuids = string.Join("','", _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId)
                .Select(x => Convert.ToString(x.ResourceGuid)).Distinct().ToList());
            sellerGuids = "'" + sellerGuids + "'";

            if (model == null)
            {
                model = new ServiceProcessModel();
            }

            model.ServiceList = new List<ServiceListData>(); 
            PageData pageData = new PageData();

            var spList = _service.GetViewCustomerServiceListByVbs(model.CurrentPage, pageLength, ViewCustomerServiceList.Columns.ServiceNo, sellerGuids, queryParams);

            if (spList != null)
            {
                foreach (var row in spList)
                {
                    int notRead = _service.CustomerServiceInsideLogByServiceNoAndStatus(row.ServiceNo, (int)statusConvert.transfer, null);

                    model.ServiceList.Add(new ServiceListData()
                    {
                        ServiceNo = row.ServiceNo,
                        CaseStatusDescription = Helper.GetEnumDescription((statusConvert)row.CustomerServiceStatus),
                        Priority = row.CasePriority,
                        PriorityDescription = Helper.GetEnumDescription((priorityConvert)row.CasePriority),
                        OrderId = row.OrderId,
                        OrderGuid = row.OrderGuid ?? Guid.Empty,
                        UniqueId = row.UniqueId ?? 0,
                        BusinessHourGuid = row.BusinessHourGuid ?? Guid.Empty,
                        ItemName = row.ItemName,
                        SellerGuid = row.SellerGuid ?? Guid.Empty,
                        SellerName = row.SellerName,
                        ProblemDescription = row.MainCategoryName,
                        SubProblemDescription = row.SubCategoryName,
                        CreateTime = row.CreateTime,
                        LastModifyTime = row.ModifyTime,
                        ServicePeople = row.ServicePeopleId == null ? "一線客服" : MemberFacade.GetFullName((int)row.ServicePeopleId) + "<br>一線客服",
                        notRead = notRead,
                        Recipient = row.MemberName,
                        RecipientMobile = row.MobileNumber
                    });
                }

                pageData.CurrentPage = model.CurrentPage;
                pageData.TotalCount = _service.GetViewCustomerServiceListByVbsCount(sellerGuids, queryParams);
                pageData.TotalPages = Convert.ToInt32(Math.Ceiling((Convert.ToDecimal(pageData.TotalCount) / Convert.ToDecimal(pageLength))));
            }
             
            ViewBag.ProblemType = problemType;
            ViewBag.CasePriority = casePriority;
            ViewBag.PageData = pageData;
            return View(model);
        }

        [VbsAuthorize]
        public ActionResult ServiceProcessDetail(string serviceNo)
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            if (string.IsNullOrEmpty(serviceNo))
            {
                return RedirectToAction("ServiceProcess");
            }

            //優先權項目
            Dictionary<int, string> casePriority = new Dictionary<int, string>();
            foreach (priorityConvert pc in Enum.GetValues(typeof(priorityConvert)))
            {
                casePriority.Add((int)pc, Helper.GetEnumDescription(pc));
            }

            //商家Guid
            string sellerGuids = string.Join("','", _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId)
                .Select(x => Convert.ToString(x.ResourceGuid)).Distinct().ToList());
            sellerGuids = "'" + sellerGuids + "'";

            //案件資訊
            var vcsl = _service.GetViewCustomerServiceMessageByServiceNo(serviceNo);

            if (!vcsl.IsLoaded ||
                !sellerGuids.Contains(vcsl.SellerGuid.ToString()) || 
                (vcsl.CustomerServiceStatus != (int)statusConvert.process &&
                 vcsl.CustomerServiceStatus != (int)statusConvert.transfer &&
                 vcsl.CustomerServiceStatus != (int)statusConvert.transferback &&
                 vcsl.CustomerServiceStatus != (int)statusConvert.complete))
            {
                return RedirectToAction("ServiceProcess");
            }

            ServiceListData model = new ServiceListData() {
                ServiceNo = vcsl.ServiceNo,
                CaseStatus = vcsl.CustomerServiceStatus,
                CaseStatusDescription = Helper.GetEnumDescription((statusConvert)vcsl.CustomerServiceStatus),
                OrderGuid = vcsl.OrderGuid ?? Guid.Empty,                
                OrderId = vcsl.OrderId,
                Recipient = vcsl.MemberName,
                RecipientMobile = vcsl.MobileNumber,
                RecipientAddress = vcsl.DeliveryAddress,
                UniqueId = vcsl.UniqueId ?? 0,
                BusinessHourGuid = vcsl.BusinessHourGuid ?? Guid.Empty,
                ItemName = vcsl.ItemName,
                SellerGuid = vcsl.SellerGuid ?? Guid.Empty,
                SellerName = vcsl.SellerName,
                ServicePeople = vcsl.ServicePeopleName,
                PriorityDescription = Helper.GetEnumDescription((priorityConvert)vcsl.CasePriority),
                ProblemDescription = vcsl.MainCategoryName,
                SubProblemDescription = vcsl.SubCategoryName,
                SecServicePeople = vcsl.SecServicePeopleName
            };

            //取得記錄訊息
            List<CustomerServiceInsideLog> serviceLog = _service.CustomerServiceInsideLogGet(serviceNo)
                .Where(x => x.Status == (int)statusConvert.transfer
                         || x.Status == (int)statusConvert.transferback
                         || x.Status == (int)statusConvert.complete).ToList();

            //更新為已讀資訊
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                
            }
            else
            {
                foreach (var item in serviceLog.Where(p => p.Status == (int)statusConvert.transfer && p.IsRead == 0))
                {
                    item.IsRead = 1;
                    item.ReadTime = DateTime.Now;
                    _service.CustomerServiceInsideLogSet(item);
                }
            }
            

            var empIds = serviceLog.Select(x => x.CreateId).ToList();

            //員工資料
            Dictionary<int, string> empData = _hmp.ViewEmployeeCollectionGetAll().Where(x => empIds.Contains(x.UserId))
                .Select(x => new { x.UserId, x.Name ,x.DeptName}) //客服姓名一律使用化名 (Member Display Name)
                .ToDictionary(x => x.UserId, x => x.Name + "("+ x.DeptName + ")");

            Guid oid = Guid.Empty;
            bool isChangeOrder = false;
            bool isReturnOrder = false;

            if (vcsl.OrderGuid != null)
            {
                oid = vcsl.OrderGuid.Value;
            }
            #region 退貨
            if (oid != null)
            {
                IList<BizLogic.Model.Refund.ReturnFormEntity> returnForms = BizLogic.Model.Refund.ReturnFormRepository.FindAllByOrder(oid);
                if (returnForms.Count > 0)
                {
                    isReturnOrder = true;
                }
            }
            #endregion 退貨

            #region 換貨
            if(oid != null)
            {
                Models.Service.ChangeOrder d = new Models.Service.ChangeOrder();
                var cols = _op.OrderReturnListGetListByType(oid, (int)OrderReturnType.Exchange)
                           .OrderByDescending(x => x.CreateTime)
                           .ToList();
                if (cols.Count > 0)
                {
                    isChangeOrder = true;
                }
            }
            #endregion 換貨

            #region 出貨
            Models.Service.ShippingOrderMethod som = new Models.Service.ShippingOrderMethod();
            Models.Service.ShippingOrder vso = new Models.Service.ShippingOrder();
            var orderShip = _op.OrderShipGetListByOrderGuid(oid);
            var orderProductDelivery = _op.OrderProductDeliveryGetByOrderGuid(oid);
            if (orderShip.Count() > 0)
            {
                som.Count = orderShip.Count();
                foreach (var item in orderShip)
                {
                    vso = new Models.Service.ShippingOrder();

                    if (item.ShipTime.HasValue)
                        vso.ShippingDate = item.ShipTime.Value.ToString("yyyy/MM/dd");
                    vso.ShippingNo = item.ShipNo;
                    if (!string.IsNullOrEmpty(_op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipWebsite))
                    {
                        //vso.LogisticsCompany = "<a href='" + _op.ShipCompanyGetList(ship.ShipCompanyId).FirstOrDefault().ShipWebsite + "' target='_blank'>" + _op.ShipCompanyGetList(ship.ShipCompanyId).FirstOrDefault().ShipCompanyName + "</a>";
                        vso.LogisticsCompany = string.Format("<p><a target='_blank' style='cursor:pointer' onclick='gotoShipUrl(this)'>{0}</a><span id='shipUrl' style='display:none'>{1}</span><span id='shipNo' style='display:none'>{2}</span></p>",
                            _op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipCompanyName,
                            _op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipWebsite,
                            item.ShipNo
                            );
                    }
                    else
                    {
                        vso.LogisticsCompany = _op.ShipCompanyGetList(item.ShipCompanyId).FirstOrDefault().ShipCompanyName;
                    }

                    if (orderProductDelivery.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                    {
                        vso.ShippingRemark = "-";
                        vso.LastModifyDate = "-";
                        vso.LastModifyAccount = "-";
                    }
                    else
                    {
                        vso.ShippingRemark = item.ShipMemo;
                        vso.LastModifyDate = item.ModifyTime.ToString("yyyy/MM/dd  HH:mm");
                        vso.LastModifyAccount = item.CreateId;
                    }

                    som.ShippingOrder.Add(vso);
                }
            }
            else
            {
                som.Success = false;
                som.Count = orderShip.Count();
            }
            #endregion 出貨

            Guid mainBusinessHourGuid = model.BusinessHourGuid;
            ViewComboDealCollection comboDeals = _pp.GetViewComboDealAllByBid(model.BusinessHourGuid);
            if(comboDeals != null && comboDeals.Count() > 0)
            {
                var comboDeal = comboDeals.Where(x => x.MainBusinessHourGuid == model.BusinessHourGuid).FirstOrDefault();
                if(comboDeal == null)
                {
                    mainBusinessHourGuid = comboDeals.FirstOrDefault().MainBusinessHourGuid ?? Guid.Empty;
                }
            }

            ViewBag.ShippingOrderMethod = som;
            ViewBag.MainBusinessHourGuid = mainBusinessHourGuid;//母檔Bid
            ViewBag.IsReturnOrder = isReturnOrder;  //退貨
            ViewBag.IsChangeOrder = isChangeOrder;  //換貨
            ViewBag.ServiceLog = serviceLog;
            ViewBag.EmpData = empData;
            ViewBag.CasePriority = casePriority;
            ViewBag.ImagePath = config.InSideUrl;
            return View(model);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult SentServiceProcessReply(ReplyParamsModel model)
        {            
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                TempData["message"] = "模擬身分不允許執行此功能";
                return RedirectToAction("ServiceProcessDetail", new { serviceNo = model.ServiceNo });
            }

            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            if (model == null || string.IsNullOrEmpty(model.ServiceNo) || (string.IsNullOrEmpty(model.Content) && model.CustomerServicePic == null))
            {
                throw new Exception("參數有誤");
            }

            int modifyId = -1;
            if (!string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName))
            {
                modifyId = _mp.MemberGetByUserName(VbsCurrent.LoginBy17LifeSimulateUserName).UniqueId;
            }
            else
            {
                modifyId = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId).UserId ?? -1;
            }
            
            //inside log
            CustomerServiceInsideLog insideLog = new CustomerServiceInsideLog();
            insideLog.ServiceNo = model.ServiceNo;
            insideLog.Content = Server.HtmlEncode(model.Content);
            insideLog.Status = (int)statusConvert.transferback;
            insideLog.CreateId = modifyId;
            insideLog.CreateTime = DateTime.Now;

            //upload pic
            if (model.CustomerServicePic != null && !string.IsNullOrEmpty(model.CustomerServicePic.FileName))
            {
                insideLog.File = insideLog.CreateTime.ToString("ddhhmmss") + model.CustomerServicePic.FileName;
                ImageUtility.UploadFileWithResizeForFileBase(model.CustomerServicePic, Server.MapPath(config.InSideUrl), insideLog.ServiceNo, insideLog.File);
            }

            int csiId = _service.CustomerServiceInsideLogSet(insideLog);

            //update case
            var csm = _service.CustomerServiceMessageGet(model.ServiceNo);
            csm.ModifyId = modifyId;
            csm.ModifyTime = DateTime.Now;
            csm.Status = (int)statusConvert.transferback;

            _service.CustomerServiceMessageSet(csm);

            EmailFacade.SendTransferMailToCs(csiId);

            return RedirectToAction("ServiceProcessDetail", new { serviceNo = model.ServiceNo });
        }
        #endregion

        #region 超商取貨

        [VbsAuthorize]
        public ActionResult InStorePickup(string sid = "")
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                TempData["message"] = "模擬身分不允許執行此功能";
                ViewBag.LoginBy17LifeSimulate = true;
            }

            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            var accountId = VbsCurrent.AccountId;

            InStorePickupModel modelFamily = new InStorePickupModel();
            InStorePickupModel modelSeven = new InStorePickupModel();
            Dictionary<string, string> sellers = new Dictionary<string, string>();

            var sids = _mp.ResourceAclGetListByAccountId(accountId).Where(x => x.ResourceType == (int)ObjectResourceType.Seller).Select(x => x.ResourceGuid).ToList();

            Seller seller = new Seller();
            ViewVbsInstorePickup vvipFamily = new ViewVbsInstorePickup();
            ViewVbsInstorePickup vvipSeven = new ViewVbsInstorePickup();
            foreach (var s in sids)
            {
                var root_guid = SellerFacade.GetRootSellerGuid(s);
                seller = _sp.SellerGet(root_guid);
                if (!sellers.Keys.Any(x => x == root_guid.ToString()))
                {
                    sellers.Add(root_guid.ToString(), string.Format("{0} {1}", seller.SellerId, seller.SellerName));
                }

                if (!vvipFamily.IsLoaded)
                {
                    vvipFamily = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid),(int)ServiceChannel.FamilyMart) : _sp.ViewVbsInstorePickupGet(root_guid,(int)ServiceChannel.FamilyMart);
                    if (!vvipFamily.IsLoaded)
                        vvipFamily = new ViewVbsInstorePickup();
                }
                if (config.EnableSevenIsp)
                {
                    if (!vvipSeven.IsLoaded)
                    {
                        vvipSeven = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.SevenEleven) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.SevenEleven);
                        if (!vvipSeven.IsLoaded)
                            vvipSeven = new ViewVbsInstorePickup();
                    }
                }
                
            }

            seller = !string.IsNullOrEmpty(sid) && sids.Any(x => x.ToString() == sid) ? _sp.SellerGet(Guid.Parse(sid)) : seller;


            List<InStorePickupModel> modelList = new List<InStorePickupModel>();
            if (config.EnableSevenIsp)
            {
                var contacts = new ReturnContracts();
                if (vvipFamily.IsLoaded && !string.IsNullOrEmpty(vvipFamily.Contacts))
                {
                    contacts = new JsonSerializer().Deserialize<ReturnContracts>(vvipFamily.Contacts);
                }
                else if (vvipSeven.IsLoaded && !string.IsNullOrEmpty(vvipSeven.Contacts))
                {
                    contacts = new JsonSerializer().Deserialize<ReturnContracts>(vvipSeven.Contacts);
                }


                if (vvipFamily.IsLoaded)
                {
                    modelFamily = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = vvipFamily.SellerName,
                        SellerId = vvipFamily.SellerId,
                        SellerGuid = vvipFamily.SellerGuid.ToString(),
                        ServiceChannel = vvipFamily.ServiceChannel,
                        ContactName = contacts.ContactPersonName,
                        ContactEmail = contacts.ContactPersonEmail,
                        ContactMobile = contacts.SellerMobile,
                        ContactTel = contacts.SellerTel,
                        StoreSubCode = vvipFamily.StoreSubCode,
                        VerifyMemo = vvipFamily.VerifyMemo,
                        ReturnAddress = vvipFamily.SellerAddress,
                        Status = vvipFamily.Status,
                        ApplyDate = vvipFamily.CreateTime.ToString("yyyy/MM/dd"),
                        ReturnCycle = vvipFamily.ReturnCycle ?? -1,
                        ReturnType = vvipFamily.ReturnType ?? 0,
                        ReturnOther = vvipFamily.ReturnOther
                    };
                }
                else
                {
                    var township = CityManager.TownShips.Where(x => x.Id == seller.CityId).FirstOrDefault();
                    var city = township != null ? CityManager.Citys.Where(x => x.Id == township.ParentId).FirstOrDefault().CityName + township : string.Empty;
                    modelFamily = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = seller.SellerName,
                        SellerId = seller.SellerId,
                        ServiceChannel = (int)ServiceChannel.FamilyMart,
                        ContactTel = "-#",
                        Status = (int)ISPStatus.Initial,
                        ApplyDate = DateTime.Now.ToString("yyyy/MM/dd"),
                        ReturnCycle = -1,
                    };
                }
                modelList.Add(modelFamily);


                if (vvipSeven.IsLoaded)
                {
                    modelSeven = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = vvipSeven.SellerName,
                        SellerId = vvipSeven.SellerId,
                        SellerGuid = vvipSeven.SellerGuid.ToString(),
                        ServiceChannel = vvipSeven.ServiceChannel,
                        ContactName = contacts.ContactPersonName,
                        ContactEmail = contacts.ContactPersonEmail,
                        ContactMobile = contacts.SellerMobile,
                        ContactTel = contacts.SellerTel,
                        StoreSubCode = vvipSeven.StoreSubCode,
                        VerifyMemo = vvipSeven.VerifyMemo,
                        ReturnAddress = vvipSeven.SellerAddress,
                        Status = vvipSeven.Status,
                        ApplyDate = vvipSeven.CreateTime.ToString("yyyy/MM/dd"),
                        ReturnCycle = vvipSeven.ReturnCycle ?? -1,
                        ReturnType = vvipSeven.ReturnType ?? 0,
                        ReturnOther = vvipSeven.ReturnOther
                    };
                }
                else
                {
                    var township = CityManager.TownShips.Where(x => x.Id == seller.CityId).FirstOrDefault();
                    var city = township != null ? CityManager.Citys.Where(x => x.Id == township.ParentId).FirstOrDefault().CityName + township : string.Empty;
                    modelSeven = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = seller.SellerName,
                        SellerId = seller.SellerId,
                        ServiceChannel = (int)ServiceChannel.SevenEleven,
                        ContactTel = "-#",
                        Status = (int)ISPStatus.Initial,
                        ApplyDate = DateTime.Now.ToString("yyyy/MM/dd"),
                        ReturnCycle = -1,
                    };
                }

                modelList.Add(modelSeven);
            }
            else
            {
                var contacts = new Seller.MultiContracts();
                if (vvipFamily.IsLoaded && !string.IsNullOrEmpty(vvipFamily.Contacts))
                {
                    contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(vvipFamily.Contacts).Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).FirstOrDefault();
                }
                else if (!string.IsNullOrEmpty(seller.Contacts))
                {
                    contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts).Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).FirstOrDefault();
                }

                if (vvipFamily.IsLoaded)
                {
                    modelFamily = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = vvipFamily.SellerName,
                        SellerId = vvipFamily.SellerId,
                        SellerGuid = vvipFamily.SellerGuid.ToString(),
                        ContactName = contacts.ContactPersonName.Split(',')[0].Split(';')[0],
                        ContactEmail = contacts.ContactPersonEmail.Split(',')[0].Split(';')[0],
                        ContactMobile = contacts.SellerMobile.Split(',')[0].Split(';')[0],
                        ContactTel = contacts.SellerTel.Split(',')[0].Split(';')[0],
                        StoreSubCode = vvipFamily.StoreSubCode,
                        VerifyMemo = vvipFamily.VerifyMemo,
                        ReturnAddress = vvipFamily.SellerAddress,
                        Fax = contacts.SellerFax,
                        Status = vvipFamily.Status,
                        ApplyDate = vvipFamily.CreateTime.ToString("yyyy/MM/dd"),
                        ReturnCycle = vvipFamily.ReturnCycle ?? -1,
                        ReturnType = vvipFamily.ReturnType ?? 0,
                        ReturnOther = vvipFamily.ReturnOther
                    };
                }
                else
                {
                    var township = CityManager.TownShips.Where(x => x.Id == seller.CityId).FirstOrDefault();
                    var city = township != null ? CityManager.Citys.Where(x => x.Id == township.ParentId).FirstOrDefault().CityName + township : string.Empty;
                    modelFamily = new InStorePickupModel
                    {
                        Sellers = sellers,
                        SellerName = seller.SellerName,
                        SellerId = seller.SellerId,
                        SellerGuid = seller.Guid.ToString(),
                        ContactName = contacts.ContactPersonName.Split(',')[0].Split(';')[0],
                        ContactEmail = contacts.ContactPersonEmail.Split(',')[0].Split(';')[0],
                        ContactMobile = contacts.SellerMobile.Split(',')[0].Split(';')[0],
                        ContactTel = contacts.SellerTel.Split(',')[0].Split(';')[0],
                        ReturnAddress = city + seller.SellerAddress,
                        Fax = contacts.SellerFax,
                        Status = (int)ISPStatus.Initial,
                        ApplyDate = DateTime.Now.ToString("yyyy/MM/dd"),
                        ReturnCycle = vvipFamily.ReturnCycle ?? -1,
                    };
                }
                modelList.Add(modelFamily);
            }

            #region 7-11週期不一致申請
            //string sid = "";
            //var sids = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId).Where(x => x.ResourceType == (int)ObjectResourceType.Seller).Select(x => x.ResourceGuid).ToList();

            //Seller seller = new Seller();
            //ViewVbsInstorePickup vvipFamily = new ViewVbsInstorePickup();
            //ViewVbsInstorePickup vvipSeven = new ViewVbsInstorePickup();
            //foreach (var s in sids)
            //{
            //    var root_guid = SellerFacade.GetRootSellerGuid(s);
            //    if (!vvipFamily.IsLoaded)
            //    {
            //        vvipFamily = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.FamilyMart) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);
            //        if (!vvipFamily.IsLoaded)
            //            vvipFamily = new ViewVbsInstorePickup();
            //    }

            //    if (!vvipSeven.IsLoaded)
            //    {
            //        vvipSeven = !string.IsNullOrEmpty(sid) ? _sp.ViewVbsInstorePickupGet(new Guid(sid), (int)ServiceChannel.SevenEleven) : _sp.ViewVbsInstorePickupGet(root_guid, (int)ServiceChannel.SevenEleven);
            //        if (!vvipSeven.IsLoaded)
            //            vvipSeven = new ViewVbsInstorePickup();
            //    }

            //}
            bool IsCycle = true;
            if (vvipFamily.ReturnCycle == (int)RrturnCycle.Mon)
            {
                IsCycle = false;
            }
            else if (vvipFamily.ReturnCycle == (int)RrturnCycle.Thu && !(vvipFamily.ReturnType == (int)RetrunShip.Kerrytj || vvipFamily.ReturnType == (int)RetrunShip.Hct))
            {
                IsCycle = false;
            }
            else if (vvipFamily.ReturnCycle == (int)RrturnCycle.Wed && !(vvipFamily.ReturnType == (int)RetrunShip.Other || vvipFamily.ReturnType == (int)RetrunShip.SelfPicking))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Tue && vvipFamily.ReturnType != (int)RetrunShip.Uni))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Fri && vvipFamily.ReturnType != (int)RetrunShip.Pelican))
            {
                IsCycle = false;
            }
            else if ((vvipFamily.ReturnCycle == (int)RrturnCycle.Daily && vvipFamily.ReturnType == (int)RetrunShip.Family))
            {
                IsCycle = false;
            }

            ViewBag.ShowApplyIsp = vvipFamily.IsLoaded && !vvipSeven.IsLoaded && !IsCycle && config.ApplyPopout;
            #endregion



            ViewBag.instorepickup = modelList;

            return View(modelList);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult StoreChange(string sellerGuid)
        {
            return RedirectToAction("InStorePickup", new { sid = sellerGuid });
        }

        /// <summary>
        /// 商家申請超取
        /// </summary>
        /// <param name="returnCycle"></param>
        /// <param name="returnType"></param>
        /// <param name="returnOther"></param>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        [ActionName("InStorePickup")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult Apply(int? returnCycleFamily, int? returnTypeFamily, string returnOtherFamily, int? returnCycleSeven, int? returnTypeSeven, 
                                  string returnOtherSeven, string sellerGuid, string returnAddress, string contactName, string contactTel1, string contactTel2, string contactTel3 , string contactEmail,string contactMobile)
        {
            var acls = _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId).Any(x => x.ResourceGuid.ToString() == sellerGuid);
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;

            if (acls)
            {
                var root_guid = SellerFacade.GetRootSellerGuid(new Guid(sellerGuid));
                

                if (config.EnableSevenIsp)
                {
                    VbsInstorePickupCollection vipc = new VbsInstorePickupCollection();

                    //申請全家
                    if (returnCycleFamily != null)//null可能為單一超商申請,另一家已申請
                    {
                        var vipFamily = _sp.VbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);

                        if (vipFamily.IsLoaded)
                        {
                            if (vipFamily.Status == (int)ISPStatus.Initial || vipFamily.Status == (int)ISPStatus.Reject)
                            {
                                vipFamily.Status = (int)ISPStatus.Verifying;
                                vipFamily.ReturnCycle = returnCycleFamily;
                                vipFamily.ReturnType = returnTypeFamily;
                                vipFamily.ReturnOther = returnTypeFamily == (int)RetrunShip.Other ? returnOtherFamily : "";
                                vipFamily.SellerAddress = returnAddress;
                                vipFamily.Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contactName, SellerTel = contactTel1 + '-' + contactTel2 + (string.IsNullOrEmpty(contactTel3) ? "" : "#" + contactTel3), SellerMobile = contactMobile, ContactPersonEmail = contactEmail });
                                vipFamily.ModifyId = modifyId;
                                vipFamily.ModifyTime = DateTime.Now;
                            }
                        }
                        else
                        {
                            vipFamily = new VbsInstorePickup()
                            {
                                SellerGuid = root_guid,
                                Status = (int)ISPStatus.Verifying,
                                CreateId = modifyId,
                                CreateTime = DateTime.Now,
                                ReturnCycle = returnCycleFamily,
                                ReturnType = returnTypeFamily,
                                ReturnOther = returnTypeFamily == (int)RetrunShip.Other ? returnOtherFamily : "",
                                ServiceChannel = (int)ServiceChannel.FamilyMart,
                                SellerAddress = returnAddress,
                                Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contactName, SellerTel = contactTel1 + '-' + contactTel2 + (string.IsNullOrEmpty(contactTel3) ? "" : "#" + contactTel3), SellerMobile = contactMobile, ContactPersonEmail = contactEmail }),
                            };
                        }

                        vipc.Add(vipFamily);
                    }


                    //申請Seven
                    if (returnCycleSeven != null)
                    {
                        var vipSeven = _sp.VbsInstorePickupGet(root_guid, (int)ServiceChannel.SevenEleven);

                        if (vipSeven.IsLoaded)
                        {
                            if (vipSeven.Status == (int)ISPStatus.Initial || vipSeven.Status == (int)ISPStatus.Reject)
                            {
                                vipSeven.Status = (int)ISPStatus.Verifying;
                                vipSeven.ReturnCycle = returnCycleSeven;
                                vipSeven.ReturnType = returnTypeSeven;
                                vipSeven.ReturnOther = returnTypeSeven == (int)RetrunShip.Other ? returnOtherSeven : "";
                                vipSeven.SellerAddress = returnAddress;
                                vipSeven.Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contactName, SellerTel = contactTel1 + '-' + contactTel2 + (string.IsNullOrEmpty(contactTel3) ? "" : "#" + contactTel3), SellerMobile = contactMobile, ContactPersonEmail = contactEmail });
                                vipSeven.ModifyId = modifyId;
                                vipSeven.ModifyTime = DateTime.Now;
                            }
                        }
                        else
                        {
                            vipSeven = new VbsInstorePickup()
                            {
                                SellerGuid = root_guid,
                                Status = (int)ISPStatus.Verifying,
                                CreateId = modifyId,
                                CreateTime = DateTime.Now,
                                ReturnCycle = returnCycleSeven,
                                ReturnType = returnTypeSeven,
                                ReturnOther = returnTypeSeven == (int)RetrunShip.Other ? returnOtherSeven : "",
                                ServiceChannel = (int)ServiceChannel.SevenEleven,
                                SellerAddress = returnAddress,
                                Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contactName, SellerTel = contactTel1 + '-' + contactTel2 + (string.IsNullOrEmpty(contactTel3) ? "" : "#" + contactTel3), SellerMobile = contactMobile, ContactPersonEmail = contactEmail }),
                            };
                        }

                        vipc.Add(vipSeven);
                    }
                   
                    _sp.VbsInstorePickupSetList(vipc);
                }
                else
                {
                    var vip = _sp.VbsInstorePickupGet(root_guid, (int)ServiceChannel.FamilyMart);

                    if (vip.IsLoaded)
                    {
                        if (vip.Status == (int)ISPStatus.Initial || vip.Status == (int)ISPStatus.Reject)
                        {
                            vip.Status = (int)ISPStatus.Verifying;
                            vip.ReturnCycle = returnCycleFamily;
                            vip.ReturnType = returnTypeFamily;
                            vip.ReturnOther = returnTypeFamily == (int)RetrunShip.Other ? returnOtherFamily : "";
                        }
                    }
                    else
                    {
                        vip = new VbsInstorePickup()
                        {
                            SellerGuid = root_guid,
                            Status = (int)ISPStatus.Verifying,
                            CreateId = modifyId,
                            CreateTime = DateTime.Now,
                            ReturnCycle = returnCycleFamily,
                            ReturnType = returnTypeFamily,
                            ReturnOther = returnTypeFamily == (int)RetrunShip.Other ? returnOtherFamily : "",
                            ServiceChannel = (int)ServiceChannel.FamilyMart,
                        };
                    }

                    _sp.VbsInstorePickupSet(vip);
                }
            }
            
            return RedirectToAction("InStorePickup");
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetTestTag(string key, int channel)
        {
            var data = new {
                storeKey = key,
                serviceChannelCode = channel
            };

            var result = ISPFacade.ISPApiRequest(Newtonsoft.Json.JsonConvert.SerializeObject(data), config.ISPApiUrl + "GetTestTag");
            return Json(new { Data = result.Data });
        }

        #endregion 超商取貨

        [VbsAuthorize]
        public ActionResult NoDeal()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            return View();
        }

        [VbsAuthorize]
        public ActionResult NoRecord()
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.ParentActionName = Session[VbsSession.ParentActionName.ToString()];
            return View();
        }

        [VbsAuthorize]
        public ActionResult KeepSessionAlive()
        {
            return new EmptyResult();
        }

        [VbsAuthorize]
        public ActionResult RedirectToActionAfterLogin()
        {
            //商家帳號登入導頁檢查  
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.VendorAccount))
            {
                //有出貨管理權限： 若未點選 "個資提醒" 按鈕，則導到該頁
                if (VbsCurrent.ViewVbsRight.Ship && Session[VbsSession.IsReadInfoProtectNeeded.ToString()].Equals(true))
                {
                    return RedirectToAction("ToInfoProtectPage", new { isShowConfirmButton = true });
                }
                //有預約管理權限 : 若未"同意個資條款" 則導到該頁
                if (VbsCurrent.ViewVbsRight.Booking && Session[VbsSession.IsAgreePersonalInfoClause.ToString()].Equals(false))
                {
                    return RedirectToAction("PersonalInfoClause", new { isShowConfirmButton = true });
                }
            }

            //若是17Life員工，依照舊規則導引；否則一律導到公告頁  ; 補加預約系統導頁設定
            if (VbsCurrent.Is17LifeEmployee)
            {
                if (VbsCurrent.ViewVbsRight.Verify)
                {
                    return RedirectToAction("Verify");
                }
                if (VbsCurrent.ViewVbsRight.Ship)
                {
                    return RedirectToAction("EmployeeShipDealList", "VendorBillingSystemShip");
                }
                if (VbsCurrent.ViewVbsRight.Booking)
                {
                    return RedirectToAction("BookingSystemAuthorize", "BookingSystem");
                }
                if (VbsCurrent.ViewVbsRight.ReserveLock)
                {
                    return RedirectToAction("ReserveCouponLock", "ReserveLock");
                }
                if (VbsCurrent.ViewVbsRight.UserEvaluate)
                {
                    return RedirectToAction("Evaluate", "UserEvaluation");
                }
                if (VbsCurrent.ViewVbsRight.BalanceSheet)
                {
                    return RedirectToAction("EmployeeBalanceDealList");
                }
                if (VbsCurrent.ViewVbsRight.Proposal)
                {
                    return RedirectToAction("ProposalList");
                }
            }

            VbsMembership mem = _mp.VbsMembershipGetByAccountId(Session[VbsSession.UserId.ToString()].ToString());
            if (mem.IsTemporaryLogin)
            {
                return RedirectToAction("TemporaryLoginPassBy");
            }

            if (config.IsEnableVbsNewUi)
            {
                if (VbsCurrent.ViewVbsRight.ToShop && !VbsCurrent.ViewVbsRight.ToHouse)
                {
                    return RedirectToAction("BulletinBoardList", new { d=1});
                }
                else if((VbsCurrent.ViewVbsRight.ToShop && VbsCurrent.ViewVbsRight.ToHouse) || (!VbsCurrent.ViewVbsRight.ToShop && VbsCurrent.ViewVbsRight.ToHouse))
                {
                    return RedirectToAction("BulletinBoardList", new { d=2});
                }
                else
                {
                    return RedirectToAction("BulletinBoardList");
                }
            }
            else
            {
                return RedirectToAction("BulletinBoardList");
            }
        }

        #region 帳戶管理與維護
        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerBasicInfo()
        {
            
            Dictionary<Guid, string> vbsSellers = ProposalFacade.GetVbsSeller(UserName);
            Seller seller = new Seller();
            if (vbsSellers.Count > 0)
            {
                seller = _sp.SellerGet(vbsSellers.FirstOrDefault().Key);
            }
            IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
            City township = CityManager.TownShipGetById(seller.CityId);

            ViewBag.VbsSellers = vbsSellers;
            ViewBag.CompanyCityId = township.ParentId.ToString();
            ViewBag.TownshipId = township.Id.ToString();

            ViewBag.Citys = citys;
            ViewBag.SiteUrl = config.SSLSiteUrl;
            return View(seller);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerBasicInfo(string sellerGuid)
        {
            Guid sid = Guid.Empty;
            Guid.TryParse(sellerGuid, out sid);
            Seller seller = new Seller();
            if (sid != Guid.Empty)
            {
                seller = _sp.SellerGet(sid);
            }
            City township = CityManager.TownShipGetById(seller.CityId);
            dynamic obj = new {
                SellerName = seller.SellerName,
                CompanyID = seller.CompanyID,
                CompanyCityId = township.ParentId.ToString(),
                TownshipId = township.Id.ToString(),
                SellerAddress = seller.SellerAddress,
                CompanyBossName = seller.CompanyBossName,
                IsCloseDown = seller.IsCloseDown,
                ModifyId = seller.ModifyId,
                ModifyTime = (seller.ModifyTime ?? DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss")
            };
            return Json(obj);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveSellerBasicInfo(
            string sellerGuid,
            string sellerName,
            string townshipId,
            string sellerAddress)
        {
            

            try
            {
                Guid sid = Guid.Empty;
                Guid.TryParse(sellerGuid, out sid);
                Seller seller = new Seller();
                string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                if (sid != Guid.Empty)
                {
                    seller = _sp.SellerGet(sid);
                    Seller oriSeller = seller.Clone();
                    if (seller.IsLoaded)
                    {
                        int cityId = seller.CityId;
                        int.TryParse(townshipId, out cityId);
                        seller.SellerName = sellerName;
                        seller.CityId = cityId;
                        seller.SellerAddress = sellerAddress;
                        seller.ModifyId = modifyId;
                        seller.ModifyTime = DateTime.Now;
                        _sp.SellerSet(seller);

                        #region 變更紀錄
                        List<string> changeList = new List<string>();
                        foreach (var item in PponFacade.ColumnValueComparison(oriSeller, seller))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
                        }
                        if (changeList.Count > 0)
                        {
                            SellerFacade.SellerLogSet(sid, string.Join("|", changeList), modifyId);
                        }
                        #endregion 變更紀錄
                    }
                }

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerContact()
        {
            Dictionary<int, string> contactList = new Dictionary<int, string>();
            foreach (ProposalContact item in Enum.GetValues(typeof(ProposalContact)))
            {
                if(item == ProposalContact.Other)
                {
                    continue;
                }
                contactList[(int)item] = Helper.GetDescription(item);
            }
            contactList[90] = "帳務聯絡人";

            Dictionary<Guid, string> vbsSellers = ProposalFacade.GetVbsSeller(UserName);
            Guid sellerGuid = Guid.Empty;
            if (vbsSellers.Count > 0)
            {
                sellerGuid = vbsSellers.FirstOrDefault().Key;
            }

            ViewBag.VbsSellers = vbsSellers;
            ViewBag.ContactList = contactList;
            return View();
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerContact(string sid)
        {
            Guid sellerGuid = Guid.Empty;
            Guid.TryParse(sid, out sellerGuid);
            List<Seller.MultiContracts> contacts = new List<Seller.MultiContracts>();
            if (sellerGuid != Guid.Empty)
            {
                Seller seller = _sp.SellerGet(sellerGuid);
                contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);

                //財務聯絡人
                if (contacts != null)
                {
                    contacts.Add(new Seller.MultiContracts
                    {
                        Type = "90",
                        ContactPersonName = seller.AccountantName,
                        SellerTel = seller.AccountantTel,
                        ContactPersonEmail = seller.CompanyEmail
                    });
                }
                    
            }
            
            return Json(contacts);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveSellerContact(List<Seller.MultiContracts> contracts)
        {
            try
            {
                string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                string message = string.Empty;

                if (contracts.Count == 0)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "無資料更新。"
                    });
                }

                List<Seller.MultiContracts> contacts = new List<Seller.MultiContracts>();

                Guid sid = Guid.Empty;
                Guid.TryParse(contracts.FirstOrDefault().Others, out sid);
                Seller seller = _sp.SellerGet(sid);
                Seller oriSeller = seller.Clone();

                string accountantName = string.Empty;
                string accountantTel = string.Empty;
                string companyEmail = string.Empty;
                List<string> sellerPorpertyList = new List<string>();

                if (!string.IsNullOrEmpty(seller.SellerPorperty))
                {
                    sellerPorpertyList.AddRange(seller.SellerPorperty.Split(",").ToList());
                }

                if (contracts.Where(x => x.Type == ((int)ProposalContact.Normal).ToString()).FirstOrDefault() == null)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "至少一個一般(頁確)聯絡人。"
                    });
                }

                if (sellerPorpertyList.Count > 0)
                {
                    if (sellerPorpertyList.Contains(((int)ProposalSellerPorperty.HomeDelivery).ToString()))
                    {
                        //宅配賣家
                        if (contracts.Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).FirstOrDefault() == null)
                        {
                            return Json(new
                            {
                                Success = false,
                                Message = "宅配商家至少一個退換貨聯絡人。"
                            });
                        }
                    }
                }

                if (contracts.Where(x => x.Type == "90").FirstOrDefault() == null)
                {
                    return Json(new
                    {
                        Success = false,
                        Message = "至少一個帳務聯絡人。"
                    });
                }
                else
                {
                    if (contracts.Where(x => x.Type == "90").ToList().Count() > 1)
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = "限一個帳務聯絡人。"
                        });
                    }
                }

                foreach (Seller.MultiContracts contract in contracts)
                {
                    string contactType = contract.Type;
                    string contactName = contract.ContactPersonName;
                    string contactTel = contract.SellerTel;
                    string contactMobile = contract.SellerMobile;
                    string contactFax = contract.SellerFax;
                    string contactEmail = contract.ContactPersonEmail;

                    if (string.IsNullOrEmpty(contactType))
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(contactName))
                    {
                        continue;
                    }

                    if (!string.IsNullOrEmpty(contactEmail) && !RegExRules.CheckEmail(contactEmail))
                    {
                        return Json(new
                        {
                            Success = false,
                            Message = contactEmail + "-Email格式不正確"
                        });
                    }

                    if (contactType == ((int)ProposalContact.Normal).ToString()
                        || contactType == ((int)ProposalContact.ReturnPerson).ToString())
                    {
                        contacts.Add(new Seller.MultiContracts
                        {
                            Type = contactType,
                            ContactPersonName = contactName,
                            SellerTel = contactTel,
                            SellerMobile = contactMobile,
                            SellerFax = contactFax,
                            ContactPersonEmail = contactEmail
                        });
                    }
                    if (contactType == "90")
                    {
                        accountantName = contactName;
                        accountantTel = contactTel;
                        companyEmail = contactEmail;
                    }

                }

                seller.Contacts = new JsonSerializer().Serialize(contacts);


                #region 額外的聯絡人資訊
                if (!string.IsNullOrEmpty(seller.Contacts))
                {
                    List<Seller.MultiContracts> contacts2 = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                    if (contacts2 != null)
                    {
                        List<string> listContact = new List<string>();
                        foreach (Seller.MultiContracts c in contacts2)
                        {
                            if (int.Parse(c.Type) == (int)ProposalContact.Normal)
                            {
                                //一般
                                if (!listContact.Contains(ProposalContact.Normal.ToString()))
                                {
                                    seller.SellerContactPerson = c.ContactPersonName;
                                    seller.SellerTel = c.SellerTel;
                                    seller.SellerMobile = c.SellerMobile;
                                    seller.SellerFax = c.SellerFax;
                                    seller.SellerEmail = c.ContactPersonEmail;

                                    listContact.Add(ProposalContact.Normal.ToString());
                                }
                            }
                            if (int.Parse(c.Type) == (int)ProposalContact.ReturnPerson)
                            {
                                //退貨
                                if (!listContact.Contains(ProposalContact.ReturnPerson.ToString()))
                                {
                                    seller.ReturnedPersonName = c.ContactPersonName;
                                    seller.ReturnedPersonTel = c.SellerTel;
                                    seller.ReturnedPersonEmail = c.ContactPersonEmail;

                                    listContact.Add(ProposalContact.ReturnPerson.ToString());
                                }
                            }
                        }

                        //檢查是否資料有刪除
                        if (!listContact.Contains(ProposalContact.Normal.ToString()))
                        {
                            //沒有一般
                            seller.SellerContactPerson = "";
                            seller.SellerTel = "";
                            seller.SellerMobile = "";
                            seller.SellerFax = "";
                            seller.SellerEmail = "";
                        }
                        if (!listContact.Contains(ProposalContact.ReturnPerson.ToString()))
                        {
                            //沒有退貨
                            seller.ReturnedPersonName = "";
                            seller.ReturnedPersonTel = "";
                            seller.ReturnedPersonEmail = "";
                        }
                    }
                }
                else
                {
                    //一般
                    seller.SellerContactPerson = "";
                    seller.SellerTel = "";
                    seller.SellerMobile = "";
                    seller.SellerFax = "";
                    seller.SellerEmail = "";
                    seller.SellerDescription = "";
                    //退貨
                    seller.ReturnedPersonName = "";
                    seller.ReturnedPersonTel = "";
                    seller.ReturnedPersonEmail = "";
                } 
                #endregion

                if (!string.IsNullOrEmpty(accountantName))
                {
                    seller.AccountantName = accountantName;
                    seller.AccountantTel = accountantTel;
                    seller.CompanyEmail = companyEmail;
                }

                seller.ModifyId = modifyId;
                seller.ModifyTime = DateTime.Now;
                _sp.SellerSet(seller);

                #region 變更紀錄
                List<string> changeList = new List<string>();
                foreach (var item in PponFacade.ColumnValueComparison(oriSeller, seller))
                {
                    changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
                }
                if (changeList.Count > 0)
                {
                    SellerFacade.SellerLogSet(sid, string.Join("|", changeList), modifyId);
                }
                #endregion 變更紀錄

                return Json(new
                {
                    Success = true,
                    Message = message
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    Success = false,
                    Message = ex.Message
                });
            }
        }

        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerFinance()
        {
            Dictionary<Guid, string> vbsSellers = ProposalFacade.GetVbsSeller(UserName);

            ViewBag.VbsSellers = vbsSellers;
            return View();
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerFinance(string sid)
        {
            Guid sellerGuid = Guid.Empty;
            Guid.TryParse(sid, out sellerGuid);

            Seller seller = new Seller();
            if (sellerGuid != Guid.Empty)
            {
                seller = _sp.SellerGet(sellerGuid);
            }
            BankInfo bank = _op.BankInfoGet(seller.CompanyBankCode);
            string bankName = string.Empty;
            string branchName = string.Empty;
            if (bank.IsLoaded)
            {
                bankName = bank.BankName;
            }
            BankInfo branch = _op.BankInfoGetByBranch(seller.CompanyBankCode, seller.CompanyBranchCode);
            if (branch.IsLoaded)
            {
                branchName = branch.BranchName;
            }
            string companyID = string.Empty;
            if (!string.IsNullOrEmpty(seller.CompanyID))
            {
                companyID = seller.CompanyID.Substring(0, 1) + "******" + seller.CompanyID.Substring(seller.CompanyID.Length - 1, 1);
            }
            string companyAccount = string.Empty;
            if (!string.IsNullOrEmpty(seller.CompanyAccount))
            {
                companyAccount = seller.CompanyAccount.Substring(0, 1) + "******" + seller.CompanyAccount.Substring(seller.CompanyAccount.Length - 1, 1);
            }
            dynamic obj = new
            {
                CompanyName = seller.CompanyName,
                CompanyBossName = seller.CompanyBossName,
                CompanyAccount = companyAccount,
                CompanyBankCode = string.Format("{0}-{1}", seller.CompanyBankCode, bankName),
                CompanyBranchCode = string.Format("{0}-{1}", seller.CompanyBranchCode, branchName),
                CompanyID = companyID,
                CompanyAccountName = seller.CompanyAccountName,
                VendorReceiptType = seller.VendorReceiptType != null ? Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, ((VendorReceiptType)seller.VendorReceiptType)) : string.Empty
            };
            return Json(obj);
        }
        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerBindAccount()
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                ViewBag.LoginBy17LifeSimulate = true;
            }
            string bindAccount = string.Empty;
            string mobileNumber = string.Empty;
            VBSFacade.GetVbsBindAccountEmailOrMobileByVbsAccount(UserName, out bindAccount, out mobileNumber);
            ViewBag.BindAccount = bindAccount;
            ViewBag.IsLoginBy17LifeMember = Session[VbsSession.IsLoginBy17LifeMemberAccount.ToString()];
            return View();
        }
        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerLocation()
        {
            //公休時間
            Dictionary<int, string> storeCloseDate = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(StoreCloseDate)))
            {
                storeCloseDate[(int)item] = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (StoreCloseDate)item);
            }
            ViewBag.StoreCloseDate = storeCloseDate;

            Dictionary<Guid, string> vbsSellers = ProposalFacade.GetVbsSeller(UserName);
            ViewBag.VbsSellers = vbsSellers;

            IEnumerable<City> citys = CityManager.Citys.Where(x => x.Code != "SYS");
            ViewBag.Citys = citys;

            ViewBag.SiteUrl = config.SSLSiteUrl;
            return View();
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerLocation(string sid)
        {
            Guid sellerGuid = Guid.Empty;
            Guid.TryParse(sid, out sellerGuid);

            Seller seller = new Seller();
            if (sellerGuid != Guid.Empty)
            {
                seller = _sp.SellerGet(sellerGuid);
            }
            string locationCity = string.Empty;
            string locationTownshipId = string.Empty;
            if (seller.StoreTownshipId != null)
            {
                City locationtownship = CityManager.TownShipGetById((int)seller.StoreTownshipId);
                if (locationtownship != null)
                {
                    locationCity = locationtownship.ParentId.ToString();
                    locationTownshipId = locationtownship.Id.ToString();
                }
            }
            bool isPpon = false;
            List<string> sellerPorpertyList = new List<string>();
            if (!string.IsNullOrEmpty(seller.SellerPorperty))
            {
                sellerPorpertyList.AddRange(seller.SellerPorperty.Split(",").ToList());
            }

            if (sellerPorpertyList.Count > 0)
            {
                if (sellerPorpertyList.Contains(((int)ProposalSellerPorperty.Ppon).ToString()))
                {
                    isPpon = true;
                }
            }

            dynamic obj = new
            {
                LockLocation = (isPpon == false),
                NoLocation = string.IsNullOrEmpty(seller.StoreTel),
                StoreTel = seller.StoreTel,
                LocationCity = locationCity,
                LocationTownshipId = locationTownshipId,
                StoreAddress = seller.StoreAddress,
                Mrt = seller.Mrt,
                Car = seller.Car,
                Bus = seller.Bus,
                OtherVehicles = seller.OtherVehicles,
                WebUrl = seller.WebUrl,
                FacebookUrl = seller.FacebookUrl,
                BlogUrl = seller.BlogUrl,
                OtherUrl = seller.OtherUrl,
                OpenTime = seller.OpenTime,
                CloseDate = seller.CloseDate
            };
            return Json(obj);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetWeeklyName(string frenquency, string weeklys, string beginTime, string endTime, bool allDay)
        {
            var data = SellerFacade.GetWeeklyNames(frenquency, weeklys, beginTime, endTime, allDay);
            return Json(data);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveSellerLocation(
            string sellerGuid,
            string noLocation,
            string storeTel,
            string storeTownshipId,
            string storeAddress,
            string mrt,
            string car,
            string bus,
            string otherVehicles,
            string webUrl,
            string facebookUrl,
            string blogUrl,
            string otherUrl,
            string openTime,
            string closeTime
            )
        {
            

            try
            {
                string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                Guid sid = Guid.Empty;
                Guid.TryParse(sellerGuid, out sid);
                Seller seller = new Seller();
                if (sid != Guid.Empty)
                {
                    seller = _sp.SellerGet(sid);
                    Seller oriSeller = seller.Clone();
                    if (seller.IsLoaded)
                    {
                        bool isNoLocation = false;
                        bool.TryParse(noLocation, out isNoLocation);
                        if (!isNoLocation)
                        {
                            int townshipId = seller.StoreTownshipId.GetValueOrDefault(0);
                            int.TryParse(storeTownshipId, out townshipId);
                            seller.StoreTel = storeTel;
                            seller.StoreTownshipId = townshipId;
                            seller.StoreAddress = storeAddress;
                            seller.Mrt = mrt;
                            seller.Car = car;
                            seller.Bus = bus;
                            seller.OtherVehicles = otherVehicles;
                            seller.WebUrl = webUrl;
                            seller.FacebookUrl = facebookUrl;
                            seller.BlogUrl = blogUrl;
                            seller.OtherUrl = otherUrl;
                            seller.OpenTime = openTime;
                            seller.CloseDate = closeTime;
                            seller.ModifyId = modifyId;
                            seller.ModifyTime = DateTime.Now;
                            _sp.SellerSet(seller);
                        }
                        else
                        {
                            seller.StoreTel = null;
                            seller.StoreTownshipId = null;
                            seller.StoreAddress = null;
                            seller.Mrt = null;
                            seller.Car = null;
                            seller.Bus = null;
                            seller.OtherVehicles = null;
                            seller.WebUrl = null;
                            seller.FacebookUrl = null;
                            seller.BlogUrl = null;
                            seller.OtherUrl = null;
                            seller.OpenTime = null;
                            seller.CloseDate = null;
                            seller.ModifyId = modifyId;
                            seller.ModifyTime = DateTime.Now;
                            _sp.SellerSet(seller);
                        }


                        #region 變更紀錄
                        List<string> changeList = new List<string>();
                        foreach (var item in PponFacade.ColumnValueComparison(oriSeller, seller))
                        {
                            changeList.Add(string.Format("變更 {0} 為 {1}", item.Key, item.Value));
                        }
                        if (changeList.Count > 0)
                        {
                            SellerFacade.SellerLogSet(sid, string.Join("|", changeList), modifyId);
                        }
                        #endregion 變更紀錄

                        #region Mail
                        int salesId = seller.SalesId.GetValueOrDefault(0);
                        SellerSale ssales = _sp.SellerSaleGetBySellerGuid(seller.Guid, 1, 1);
                        if(ssales != null && ssales.IsLoaded)
                        {
                            salesId = ssales.SellerSalesId;
                        }
                        ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(salesId);
                        string subject = string.Format("【商家資料變更通知】{0}", seller.SellerName);
                        string content = string.Empty;
                        content += string.Format(@"商家編號：{0}<br/>" +
                                                    "店家名稱：{1}<br/>" +
                                                    "負責業務：{2}<br/>" +
                                                    "資料變更者：{3}<br/>" +
                                                    "資料變更項目：{4}<br/>" +
                                                    "<p><p/>" +
                                                    "協請相關營運/業務同仁協助確認，是否需要因營業資訊變動而做必要的處理或通知。" +
                                                    "此信件為系統自動發出，如有任何問題，請直接洽詢商家，請勿直接回信",
                                                    seller.SellerId,
                                                    seller.SellerName,
                                                    deSalesEmp.EmpName,
                                                    modifyId,
                                                    string.Join(",", changeList)
                                    );
                        List<string> mailToUser = new List<string>();
                        //mailToUser.Add(ProposalFacade.PponAssistantEmailGet(deSalesEmp.DeptId));//業務助理
                        mailToUser.Add(config.CsdEmail);//客服中心
                        mailToUser.Add(deSalesEmp.Email);//負責業務
                        if (!string.IsNullOrEmpty(config.VbsShortageMail))
                        {
                            //Elin
                            string[] ss = config.VbsShortageMail.Split(",");
                            foreach (string s in ss)
                            {
                                mailToUser.Add(s);
                            }
                        }
                        foreach (string email in mailToUser)
                        {
                            if (RegExRules.CheckEmail(email))
                            {
                                MailMessage msg = new MailMessage();
                                msg.From = new MailAddress(config.SystemEmail);
                                msg.To.Add(email);
                                msg.Subject = subject;
                                msg.Body = HttpUtility.HtmlDecode(content);
                                msg.IsBodyHtml = true;
                                PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                            }
                        }
                        #endregion Mail
                    }
                }

                return Json(new { Success = true });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }
        [HttpGet]
        [VbsAuthorize]
        public ActionResult SellerChangeLog()
        {
            Dictionary<Guid, string> vbsSellers = ProposalFacade.GetVbsSeller(UserName);
            ViewBag.VbsSellers = vbsSellers;
            ViewBag.SiteUrl = config.SSLSiteUrl;
            return View();
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerChangeLogs(int pageStart, int pageLength, Guid sid)
        {
            SellerChangeLogCollection scc = _sp.SellerChangeLogGetList(sid);
            int totalCount = scc.Count();
            int totalPage = (totalCount / pageLength);
            if ((totalCount % pageLength) > 0)
            {
                totalPage += 1;
            }
            if (totalCount == 0)
            {
                totalPage = 1;
            }

            var data = scc.OrderByDescending(t => t.CreateTime).Skip((pageStart - 1) * pageLength).Take(pageLength).ToList().Select(y => new
            {
                CreateId = y.CreateId,
                ChangeLog = y.ChangeLog,
                CreateTime = y.CreateTime.ToString("yyyy/MM/dd HH:mm")
            });

            return Json(new {
                Data = data,
                TotalCount = totalCount,
                TotalPage = totalPage,
                CurrentPage = pageStart
            });
        }
        #endregion 帳戶管理與維護

        #endregion Action

        #region method

        #region 登入/變更密碼

        private bool IsNewPasswodAndOriginalPasswordTheSame(string newPassword, string password)
        {
            return string.Equals(newPassword, password, StringComparison.OrdinalIgnoreCase);
        }

        private bool IsOriginalPasswordCorrect(string accountId, string password)
        {
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                bool isOriginalPasswordCorrect = false;
                if (string.Compare(password, VbsCurrent.DemoUser.Password, false) == 0)
                {
                    isOriginalPasswordCorrect = true;
                }

                return isOriginalPasswordCorrect;
            }
            else
            {
                return VbsMemberUtility.VerifyPassword(accountId, password);
            }
        }

        private bool IsNewPasswodAndRetypedNewPasswordTheSame(string newPassword, string retypedNewPassword)
        {
            return string.Equals(newPassword, retypedNewPassword);
        }

        private bool IsPasswordFormatCorrect(string password)
        {
            return VbsMemberUtility.CheckPassword(password);
        }

        private bool IsInspectionCodeCorrect(string accountId, string inspectionCode)
        {
            if (Session[VbsSession.VbsAccountType.ToString()].Equals(VbsMembershipAccountType.DemoUser))
            {
                bool isOriginalCodeCorrect = false;
                if (string.Compare(inspectionCode, VbsCurrent.DemoUser.InspectionCode, false) == 0)
                {
                    isOriginalCodeCorrect = true;
                }

                return isOriginalCodeCorrect;
            }
            else
            {
                return VbsMemberUtility.VerifyInspectionCode(accountId, inspectionCode);
            }
        }

        private bool IsNewAndRetypedInspectionCodeTheSame(string newInspectionCode, string retypedInspectionCode)
        {
            return string.Equals(newInspectionCode, retypedInspectionCode);
        }

        private bool IsInspectionCodeFormatCorrect(string inspectionCode)
        {
            return VbsMemberUtility.CheckInspectionCode(inspectionCode);
        }

        private bool CheckCookieOrSessionExists()
        {
            var vbsAuthorize = new VbsAuthorizeAttribute();
            if (!vbsAuthorize.AuthCookieExists(HttpContext) || !vbsAuthorize.SessionDataExists(HttpContext) || vbsAuthorize.SessionDataCheckError(HttpContext))
            {
                return false;
            }
            return true;
        }

        private void SetSession(VbsMembership mem, bool isFirstLogin)
        {
            Session[VbsSession.LoginSessionCheck.ToString()] = new object();
            Session[VbsSession.VbsAccountType.ToString()] = mem.VbsMembershipAccountType;

            //模擬跳過第一次登入
            if (Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()].ToString() != "")
            {
                Session[VbsSession.FirstLogin.ToString()] = false;
            }
            else
            {
                Session[VbsSession.FirstLogin.ToString()] = isFirstLogin;
            }
            Session[VbsSession.UseInspectionCode.ToString()] = mem.HasInspectionCode;

            //模擬跳過密碼過期檢查
            if (Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()].ToString() != "")
            {
                Session[VbsSession.IsPwdPassTimeLimit.ToString()] = false;
            }
            else
            {
                Session[VbsSession.IsPwdPassTimeLimit.ToString()] = mem.IsPasswordResetNeeded;
            }
            Session[VbsSession.UserName.ToString()] = mem.Name + (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True" ? "(內部人員模擬)" : "");
            Session[VbsSession.UserId.ToString()] = mem.AccountId.ToUpper();
            Session[VbsSession.Is17LifeEmployee.ToString()] = mem.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount;
            Session[VbsSession.UniqueId.ToString()] = mem.UserId;
            Session[VbsSession.IsInspectionCodeNeverChange.ToString()] = (!mem.LastInspectionCodeChangedDate.HasValue);

            //模擬跳過宅配個資檢查
            if (Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()].ToString() != "")
            {
                Session[VbsSession.IsReadInfoProtectNeeded.ToString()] = false;
            }
            else
            {
                Session[VbsSession.IsReadInfoProtectNeeded.ToString()] = mem.IsReadInfoProtectNeeded;
            }
            //模擬跳過密碼憑證個資條款
            if (Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulateUserName.ToString()].ToString() != "")
            {
                Session[VbsSession.IsAgreePersonalInfoClause.ToString()] = true;
            }
            else
            {
                Session[VbsSession.IsAgreePersonalInfoClause.ToString()] = mem.IsAgreePersonalInfoClause;
            }

            Session[VbsSession.IsTemporaryLogin.ToString()] = mem.IsTemporaryLogin;
            Session[VbsSession.TemporaryLoginDate.ToString()] = mem.TemporaryPasswordDate;


            if (Session[VbsSession.ParentActionName.ToString()] != null)
            {
                Session[VbsSession.ParentActionName.ToString()] = "";
            }


            //超取
            List<Guid> vbsSeller = ProposalFacade.GetVbsSeller(mem.AccountId)
                                                    .Select(x => x.Key).ToList();

            Session[VbsSession.IsInstorePickup.ToString()] = false;
            foreach (Guid sid in vbsSeller)
            {
                ViewVbsInstorePickupCollection ipc = _sp.ViewVbsInstorePickupCollectionGet(sid);
                var data = ipc.Where(x => x.IsEnabled == true).FirstOrDefault();
                if (data != null)
                {
                    Session[VbsSession.IsInstorePickup.ToString()] = true;
                    break;
                }
            }

            //設定帳號檢視工具列權限
            Session[VbsSession.ViewVbsRight.ToString()] = new ViewVbsRight();
            VbsCurrent.ViewVbsRight.ToShop = mem.ViewToShop;
            VbsCurrent.ViewVbsRight.ToHouse = mem.ViewToHouse;
            VbsCurrent.ViewVbsRight.Verify = mem.ViewVerifyRight;
            VbsCurrent.ViewVbsRight.BalanceSheet = mem.ViewBsRight;
            VbsCurrent.ViewVbsRight.Ship = mem.ViewShipRight;
            VbsCurrent.ViewVbsRight.Booking = mem.ViewBookingRight;
            VbsCurrent.ViewVbsRight.ReserveLock = mem.ViewReserveLockRight;
            VbsCurrent.ViewVbsRight.UserEvaluate = mem.ViewUserEvaluateRight;
            VbsCurrent.ViewVbsRight.Proposal = mem.ViewProposalRight;
            VbsCurrent.ViewVbsRight.ProposalProduct = mem.ViewProposalProductRight;
            VbsCurrent.ViewVbsRight.ShoppingCartManage = mem.ViewShoppingCartRight;
            VbsCurrent.ViewVbsRight.AccountManage = mem.ViewAccountManageRight;
            VbsCurrent.ViewVbsRight.CustomerServiceManage = mem.ViewCustomerServiceRight;

            bool isInstorePickup = false;
            if (Session[VbsSession.IsInstorePickup.ToString()] != null)
            {
                bool.TryParse(Session[VbsSession.IsInstorePickup.ToString()].ToString(), out isInstorePickup);
            }
            VbsCurrent.ViewVbsRight.IsInstorePickup = isInstorePickup;
            VbsCurrent.ViewVbsRight.Wms = mem.ViewWmsRight;
        }

        #endregion 登入/變更密碼

        #region 核銷查詢

        private List<Guid> GetAllowedStores(Guid merchandiseGuid)
        {
            List<Guid> result = (new VbsVendorAclMgmtModel(VbsCurrent.AccountId)).GetAllowedDealStores(merchandiseGuid);
            return result;
        }

        private List<VbsVerificationDealInfo> GetVerificationDealInfo(
            List<IVbsVendorAce> allowedDealUnits,
            BusinessModel bizModel)
        {
            List<VbsVerificationDealInfo> result = new List<VbsVerificationDealInfo>();

            Dictionary<Guid, List<IVbsVendorAce>> groupedDeals =
                allowedDealUnits.GroupBy(deal => deal.MerchandiseGuid.Value).ToDictionary(g => g.Key, g => g.ToList());
            DateTime now = DateTime.Now;

            foreach (KeyValuePair<Guid, List<IVbsVendorAce>> deal in groupedDeals)
            {
                var theDeal = deal.Value.First();

                if (bizModel == BusinessModel.PiinLife && !theDeal.IsPiinDeal.Value)
                {
                    continue;
                }
                else if (bizModel == BusinessModel.Ppon && !theDeal.IsPponDeal.Value)
                {
                    continue;
                }

                bool dealHasNoStore = false;

                VbsVerificationState verificationState = VbsVerificationState.NotYetStart;
                if (now.Date >= theDeal.DealUseStartTime.Value.Date && now.Date <= theDeal.DealUseEndTime.Value.Date)
                {
                    verificationState = VbsVerificationState.Verifying;
                }
                else if (now.Date > theDeal.DealUseEndTime.Value.Date)
                {
                    verificationState = VbsVerificationState.Finished;
                }

                VbsVerificationDealInfo info = new VbsVerificationDealInfo
                {
                    DealName = theDeal.MerchandiseName,
                    DealId = deal.Key.ToString(),
                    DealType = bizModel == BusinessModel.Ppon ? VbsDealType.Ppon : VbsDealType.PiinLife,
                    DealUniqueId = theDeal.MerchandiseId.Value,
                    ExchangePeriodStartTime = theDeal.DealUseStartTime.Value,
                    ExchangePeriodEndTime = theDeal.DealUseEndTime.Value,
                    StoreCount = dealHasNoStore ? 0 : deal.Value.Count,
                    VerificationState = verificationState
                };
                result.Add(info);
            }

            return result;
        }

        private List<VbsVerificationDealInfo> GetVerificationDealVerificationPercent(List<VbsVerificationDealInfo> dealInfos)
        {
            List<Guid> bids = dealInfos.Where(x => x.DealType.Equals(VbsDealType.Ppon))
                                       .Select(x => Guid.Parse(x.DealId))
                                       .ToList();
            List<int> pids = dealInfos.Where(x => x.DealType.Equals(VbsDealType.PiinLife))
                                      .Select(x => x.DealUniqueId)
                                      .ToList();

            //verification summary (verified, unverified, returned)  
            //Dictionary<Guid, VendorDealSalesCount> pponVerificationSummary =
            //    bids.Count > 0
            //    ? _vp.GetPponVerificationSummary(bids)
            //        .ToDictionary(x => x.MerchandiseGuid, x => x)
            //    : null;
            Dictionary<Guid, VendorDealSalesCount> pponVerificationSummary =
                bids.Count > 0
                    ? VerificationFacade.GetPponVerificationSummary(bids)
                        .ToDictionary(x => x.MerchandiseGuid, x => x)
                    : null;
            Dictionary<Guid, VendorDealSalesCount> piinVerificationSummary =
                pids.Count > 0
                ? _vp.GetHiDealVerificationSummary(pids)
                    .ToDictionary(x => x.MerchandiseGuid, x => x)
                : null;

            foreach (var deal in dealInfos)
            {
                //取得單一檔次之銷售、已出貨、未出貨及退貨數量
                VendorDealSalesCount dealSaleSummary = new VendorDealSalesCount();
                Guid dealGuid = Guid.Parse(deal.DealId);
                int totalSaleCount = 0;
                int verifyCount = 0;
                if (deal.VerificationPercent.HasValue)
                {
                    continue;
                }
                if (deal.DealType.Equals(VbsDealType.Ppon))
                {
                    dealSaleSummary = pponVerificationSummary.ContainsKey(dealGuid)
                                        ? pponVerificationSummary[dealGuid]
                                        : null;
                }
                else if (deal.DealType.Equals(VbsDealType.PiinLife))
                {
                    dealSaleSummary = piinVerificationSummary.ContainsKey(dealGuid)
                                        ? piinVerificationSummary[dealGuid]
                                        : null;
                }

                if (dealSaleSummary != null)
                {
                    verifyCount = dealSaleSummary.VerifiedCount + dealSaleSummary.ForceVerifyCount + dealSaleSummary.ForceReturnCount;
                    totalSaleCount = verifyCount + dealSaleSummary.UnverifiedCount;
                }

                if (totalSaleCount > 0)
                {
                    deal.VerificationPercent = (int)(Math.Round((decimal)verifyCount / (decimal)totalSaleCount, 2) * 100); //取百分比
                }
                else
                {
                    deal.VerificationPercent = 0;
                }
            }

            return dealInfos.OrderByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId).ToList();
        }

        private List<VbsVerificationStoreInfo> GetVerificationStoreInfo(string id, VbsDealType type)
        {
            List<VbsVerificationStoreInfo> infos = new List<VbsVerificationStoreInfo>();
            VbsVerificationState VerifiedState;
            DateTime now = DateTime.Now;
            int verifiedCount = 0;
            int unVerifyCount = 0;
            int returnCount = 0;
            int totalSaleCount = 0;

            #region Ppon

            if (type == VbsDealType.Ppon)
            {
                var storeLists = _pp.ViewPponDealStoreGetToShopDealList(string.Empty, ViewPponDealStore.Columns.ProductGuid + " = " + id)
                                    .Where(x => x.VbsRight.HasValue &&
                                                ((!Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) &&
                                                  Helper.IsFlagSet(x.VbsRight.Value, VbsRightFlag.VerifyShop)) ||
                                                 (Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.NoRestrictedStore) &&
                                                  Helper.IsFlagSet(x.VbsRight.Value, VbsRightFlag.Verify))));

                if (storeLists.Count() > 0)
                {
                    //統計各核銷狀態數量
                    List<VendorDealSalesCount> dealSalesCounts = _vp.GetPponVerificationSummaryGroupByStore(new List<Guid>() { Guid.Parse(id) });
                    foreach (ViewPponDealStore storeList in storeLists)
                    {
                        verifiedCount = unVerifyCount = returnCount = 0;
                        VerifiedState = VbsVerificationState.NotYetStart;
                        if (now.Date < storeList.UseStartTime.Value.Date)   //尚未開始
                        {
                            VerifiedState = VbsVerificationState.NotYetStart;
                        }
                        else if (now.Date >= storeList.UseStartTime.Value.Date && now.Date <= storeList.UseEndTime.Value.Date)  //兌換中
                        {
                            VerifiedState = VbsVerificationState.Verifying;
                        }
                        else if (now.Date > storeList.UseEndTime.Value.Date)  //活動結束
                        {
                            VerifiedState = VbsVerificationState.Finished;
                        }

                        var row = dealSalesCounts.Where(x => x.StoreGuid == storeList.StoreGuid).FirstOrDefault();
                        if (row != null)
                        {
                            verifiedCount = row.VerifiedCount;
                            unVerifyCount = row.UnverifiedCount;
                            returnCount = row.ReturnedCount;
                        }
                        totalSaleCount = verifiedCount + unVerifyCount + returnCount;
                        VbsVerificationStoreInfo info1 = new VbsVerificationStoreInfo
                        {
                            StoreName = storeList.StoreName,
                            StoreId = (string.IsNullOrEmpty(storeList.StoreGuid.ToString())) ? "null" : storeList.StoreGuid.ToString(),
                            SaleCount = totalSaleCount,
                            ReturnCount = returnCount,
                            VerifiedCount = verifiedCount,
                            UnUsedCount = unVerifyCount,
                            VerificationState = VerifiedState,
                            VerificationPercent = (verifiedCount + unVerifyCount > 0) ? (int)(Math.Round((decimal)verifiedCount / (decimal)(verifiedCount + unVerifyCount), 2) * 100) : 0
                        };
                        infos.Add(info1);
                    }
                }
            }

            #endregion Ppon

            #region PiinLife

            else if (type == VbsDealType.PiinLife)
            {
                ViewHiDealProductSellerStoreCollection hiDealStoreLists = _hp.ViewHiDealProductSellerStoreGetToShopDealList(string.Empty, ViewHiDealProductSellerStore.Columns.ProductGuid + " = " + id);
                if (hiDealStoreLists.Count > 0)
                {
                    List<VendorDealSalesCount> hiDealStoreCountStatusList =
                        _vp.GetHiDealVerificationSummaryGroupByStore(new List<int>() { hiDealStoreLists.First().ProductId });
                    foreach (var hiDealStoreList in hiDealStoreLists)
                    {
                        verifiedCount = unVerifyCount = returnCount = 0;
                        VerifiedState = VbsVerificationState.NotYetStart;
                        if (now.Date < hiDealStoreList.UseStartTime.Value.Date)   //尚未開始
                        {
                            VerifiedState = VbsVerificationState.NotYetStart;
                        }
                        else if (now.Date >= hiDealStoreList.UseStartTime.Value.Date && now.Date <= hiDealStoreList.UseEndTime.Value.Date)  //兌換中
                        {
                            VerifiedState = VbsVerificationState.Verifying;
                        }
                        else if (now.Date > hiDealStoreList.UseEndTime.Value.Date)  //活動結束
                        {
                            VerifiedState = VbsVerificationState.Finished;
                        }

                        var row = hiDealStoreCountStatusList.Where(x => x.StoreGuid == hiDealStoreList.StoreGuid).FirstOrDefault();
                        if (row != null)
                        {
                            verifiedCount = row.VerifiedCount;
                            unVerifyCount = row.UnverifiedCount;
                            returnCount = row.ReturnedCount;
                        }
                        totalSaleCount = verifiedCount + unVerifyCount + returnCount;
                        VbsVerificationStoreInfo info2 = new VbsVerificationStoreInfo
                        {
                            StoreName = hiDealStoreList.StoreName,
                            StoreId = (string.IsNullOrEmpty(hiDealStoreList.StoreGuid.ToString())) ? "null" : hiDealStoreList.StoreGuid.ToString(),
                            SaleCount = totalSaleCount,
                            ReturnCount = returnCount,
                            VerifiedCount = verifiedCount,
                            UnUsedCount = unVerifyCount,
                            VerificationState = VerifiedState,
                            VerificationPercent = (verifiedCount + unVerifyCount > 0) ? (int)(Math.Round((decimal)verifiedCount / (decimal)(verifiedCount + unVerifyCount), 2) * 100) : 0
                        };
                        infos.Add(info2);
                    }
                }
            }

            #endregion PiinLife

            return infos;
        }

        private List<VbsVerificationStoreInfo> GetAllowedVerificationStoreInfo(string dealGuid, VbsDealType type)
        {
            List<VbsVerificationStoreInfo> result = new List<VbsVerificationStoreInfo>();

            Guid merchandiseGuid;
            if (!Guid.TryParse(dealGuid, out merchandiseGuid))
            {
                return result;
            }

            //取得有權限檢視之檔次分店資料
            var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, merchandiseGuid, VbsRightFlag.VerifyShop);

            //verification summary (verified, unverified, returned) by store
            List<VendorDealSalesCount> verificationSummary;
            if (type == VbsDealType.Ppon)
            {
                List<Guid> bids = allowedDealUnits.Where(x => x.IsPponDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();
                if (int.Equals(0, bids.Count))
                {
                    return result;
                }
                verificationSummary = _vp.GetPponVerificationSummaryGroupByStore(bids);
            }
            else
            {
                List<int> pids = allowedDealUnits.Where(x => x.IsPiinDeal.Value).Select(x => x.MerchandiseId.Value).Distinct().ToList();
                if (int.Equals(0, pids.Count))
                {
                    return result;
                }
                verificationSummary = _vp.GetHiDealVerificationSummaryGroupByStore(pids);
            }

            DateTime now = DateTime.Now;
            foreach (var deal in allowedDealUnits)
            {
                decimal verified = 0;
                decimal unverified = 0;
                decimal returned = 0;

                if (deal.IsHomeDelivery.GetValueOrDefault(false))
                {
                    VendorDealSalesCount dealSalesCount = verificationSummary.FirstOrDefault(x => x.MerchandiseGuid == deal.MerchandiseGuid);
                    if (dealSalesCount != null)
                    {
                        verified += dealSalesCount.VerifiedCount;
                        unverified += dealSalesCount.UnverifiedCount;
                        returned += dealSalesCount.ReturnedCount;
                    }
                }
                else if (deal.IsInStore.GetValueOrDefault(false))
                {
                    VendorDealSalesCount storeSalesCount = verificationSummary.FirstOrDefault(x => x.MerchandiseGuid == deal.MerchandiseGuid &&
                                                                                                    x.StoreGuid == deal.StoreGuid);
                    if (storeSalesCount != null)
                    {
                        verified += storeSalesCount.VerifiedCount;
                        unverified += storeSalesCount.UnverifiedCount;
                        returned += storeSalesCount.ReturnedCount;
                    }
                }

                int verificationPercent = 0;
                if ((verified + unverified) > 0)
                {
                    verificationPercent = (int)(Math.Round(verified / (verified + unverified), 2) * 100);
                }
                VbsVerificationState verificationState;

                if (now.Date >= deal.DealUseStartTime.Value.Date && now.Date <= deal.DealUseEndTime.Value.Date)
                {
                    verificationState = VbsVerificationState.Verifying;
                }
                else if (now.Date > deal.DealUseEndTime.Value.Date)
                {
                    verificationState = VbsVerificationState.Finished;
                }
                else
                {
                    verificationState = VbsVerificationState.NotYetStart;
                }

                var info = new VbsVerificationStoreInfo
                {
                    StoreName = deal.StoreName.Substring(9, deal.StoreName.Length - 9),
                    StoreId = deal.StoreGuid.HasValue ? deal.StoreGuid.ToString() : "null",
                    SaleCount = (int)(verified + unverified + returned),
                    ReturnCount = (int)returned,
                    VerifiedCount = (int)verified,
                    UnUsedCount = (int)unverified,
                    VerificationState = verificationState,
                    VerificationPercent = verificationPercent
                };

                result.Add(info);
            }

            return result;
        }

        private List<IVbsVendorAce> GetAllowedDealUnits(string accountId, Guid? merchandiseGuid, VbsRightFlag vbsRight, bool isInStore = true)
        {
            //deal unit: 
            //Ppon:  ppon_store or business_hour, depending on whether ppon_store is present
            //HiDeal: hi_deal_product_store or hi_deal_product
            var deliveryType = isInStore ? DeliveryType.ToShop : (DeliveryType?)null;
            var allowedDealUnits = (new VbsVendorAclMgmtModel(accountId)).GetAllowedDealUnits(merchandiseGuid, deliveryType, vbsRight).ToList();

            var result = new List<IVbsVendorAce>();

            if (vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                var storeList = deliveryType.HasValue
                                ? (new VbsVendorAclMgmtModel(accountId)).GetAllowedDealUnits(merchandiseGuid, deliveryType, VbsRightFlag.VerifyShop)
                                            .ToLookup(x => x.MerchandiseStoreGuid, x => x)
                                : null;
                foreach (var ace in allowedDealUnits)
                {
                    if (ace.IsSellerPermissionAllow)
                    {
                        if (!allowedDealUnits.Any(x => x.MerchandiseGuid == ace.MerchandiseGuid && x.SellerGuid == x.StoreGuid && Helper.IsFlagSet(x.VbsRight.Value, VbsRightFlag.ViewBalanceSheet)))
                        {
                            continue;
                        }
                        if (ace.VbsRight.HasValue && !Helper.IsFlagSet(ace.VbsRight.Value, VbsRightFlag.BalanceSheetHideFromDealSeller))
                        {
                            if (!deliveryType.HasValue ||
                                (storeList.Contains(ace.MerchandiseStoreGuid) &&
                                 storeList[ace.MerchandiseStoreGuid].Any(x => x.MerchandiseGuid == ace.MerchandiseGuid)))
                            {
                                result.Add(ace);
                            }
                            continue;
                        }
                    }
                    else if (ace.VbsRight.HasValue && Helper.IsFlagSet(ace.VbsRight.Value, VbsRightFlag.ViewBalanceSheet))
                    {
                        if (!deliveryType.HasValue ||
                            (storeList.Contains(ace.MerchandiseStoreGuid) &&
                             storeList[ace.MerchandiseStoreGuid].Any(x => x.MerchandiseGuid == ace.MerchandiseGuid)))
                        {
                            result.Add(ace);
                        }
                        continue;
                    }
                }
            }
            else
            {
                result = allowedDealUnits;
            }

            return result;
        }

        private List<VbsVerificationStoreInfo> GetAllStoreSalesSummery(Guid productGuid, List<VbsVerificationStoreInfo> stores, bool noRestrictedStore)
        {
            var returnStores = new List<VbsVerificationStoreInfo>();

            //通用券檔次之所有分店銷售統計 非依各分店銷售統計加總計算
            //需判斷該帳號權限 賣家權限:抓取該檔次所有銷售數(不論分店) 其餘權限:抓取有權限分店之各分店銷售統計加總
            var isSellerPemission = VbsCurrent.Is17LifeEmployee || new VbsVendorAclMgmtModel(VbsCurrent.AccountId).GetAllowedDeals(productGuid, DeliveryType.ToShop).Any(x => x.IsSellerPermissionAllow);
            if (!noRestrictedStore || !isSellerPemission)
            {
                var total = new VbsVerificationStoreInfo
                {
                    StoreName = "所有分店",
                    StoreId = "all",
                    SaleCount = stores.Sum(x => x.SaleCount),
                    ReturnCount = stores.Sum(x => x.ReturnCount),
                    VerifiedCount = stores.Sum(x => x.VerifiedCount),
                    UnUsedCount = stores.Sum(x => x.UnUsedCount),
                    VerificationState = stores.First().VerificationState
                };
                total.VerificationPercent = total.SaleCount - total.ReturnCount > 0 ? (int)(Math.Round((decimal)total.VerifiedCount.Value / (decimal)(total.SaleCount.Value - total.ReturnCount.Value), 2) * 100) : 0;

                returnStores.Add(total);
            }
            else
            {
                var verificationSummary = _vp.GetPponVerificationSummaryGroupByStore(new List<Guid> { productGuid });

                var total = new VbsVerificationStoreInfo
                {
                    StoreName = "所有分店",
                    StoreId = "all",
                    ReturnCount = verificationSummary.Sum(x => x.ReturnedCount),
                    VerifiedCount = verificationSummary.Sum(x => x.VerifiedCount),
                    UnUsedCount = verificationSummary.Sum(x => x.UnverifiedCount),
                    VerificationState = stores.First().VerificationState
                };
                total.SaleCount = total.VerifiedCount + total.UnUsedCount + total.ReturnCount;
                total.VerificationPercent = total.SaleCount - total.ReturnCount > 0 ? (int)(Math.Round((decimal)total.VerifiedCount.Value / (decimal)(total.SaleCount.Value - total.ReturnCount.Value), 2) * 100) : 0;

                returnStores.Add(total);
            }

            returnStores.AddRange(stores);

            return returnStores;
        }

        #endregion  核銷查詢

        #region 對帳查詢

        //依對帳單Id抓取核銷、反核銷及退貨數量
        private Dictionary<string, int> GetQuantityInfoByBalanceSheetId(int balanceSheetId, DateTime bsIntervalStart, int generationFrequency, Guid productGuid)
        {
            var balanceSheetDetails = _ap.BalanceSheetDetailGetListByBalanceSheetId(balanceSheetId);
            var dicCountStatus = new Dictionary<string, int>
                                {
                                    {"verified", 0},    //已核銷
                                    {"undo", 0},        //反核銷
                                    {"return", 0},      //退貨
                                    {"verifiedTotal", 0}, //累積核銷
                                    {"slottingFeeQuantity", 0}, //上架費份數
                                    {"exceedRemittanceTimeLimit", 0} //超出匯款限制次數
                                };
            var lastReturnCount = 0;
            //統計核銷及反核銷數量
            dicCountStatus["verified"] = balanceSheetDetails.Count(x => x.Status == (int)BalanceSheetDetailStatus.Normal || x.Status == (int)BalanceSheetDetailStatus.Undo || x.Status == (int)BalanceSheetDetailStatus.NoPay || x.Status == (int)BalanceSheetDetailStatus.UndoNoPay);
            dicCountStatus["undo"] = balanceSheetDetails.Count(x => x.Status == (int)BalanceSheetDetailStatus.Deduction);
            //統計退貨數
            var verificationStatisticsLog = _pp.VerificationStatisticsLogGetListByBalanceSheetId(balanceSheetId);
            //截至本次對帳單統計之累計退貨數
            var returnCount = verificationStatisticsLog.Returned ?? 0;
            //截至本次對帳單統計之累計核銷數
            dicCountStatus["verifiedTotal"] = verificationStatisticsLog.Verified ?? 0;
            //上架費份數
            dicCountStatus["slottingFeeQuantity"] = GetSlottingFeeQuantity(productGuid);

            //根據product_guid、對帳單區間及對帳單型態抓取上一張對帳單id
            var lastBalanceSheetId = _ap.BalanceSheetGetList(BalanceSheet.Columns.IntervalStart, BalanceSheet.Columns.ProductGuid + " = " + verificationStatisticsLog.BusinessHourGuid
                                                            , BalanceSheet.Columns.IntervalStart + " < " + bsIntervalStart
                                                            , BalanceSheet.Columns.GenerationFrequency + " = " + generationFrequency)
                                                            .Select(x => x.Id)
                                                            .LastOrDefault();
            if (lastBalanceSheetId > 0)
            {
                //截至前次對帳單統計之累計退貨數
                verificationStatisticsLog = _pp.VerificationStatisticsLogGetListByBalanceSheetId(lastBalanceSheetId);
                lastReturnCount = verificationStatisticsLog.Returned ?? 0;
            }
            dicCountStatus["return"] = Math.Abs(returnCount - lastReturnCount);
            return dicCountStatus;
        }

        //依對帳單Id抓取核銷、反核銷數量
        private Dictionary<string, int> GetQuantityInfoByBalanceSheetIds(List<int> balanceSheetIds)
        {
            var balanceSheetDetails = _ap.BalanceSheetDetailGetListByBalanceSheetIds(balanceSheetIds);
            var dicCountStatus = new Dictionary<string, int>
                                {
                                    {"verified", 0},    //已核銷
                                    {"undo", 0},        //反核銷
                                };
            //統計核銷及反核銷數量
            dicCountStatus["verified"] = balanceSheetDetails.Count(x => x.Status == (int)BalanceSheetDetailStatus.Normal || x.Status == (int)BalanceSheetDetailStatus.Undo || x.Status == (int)BalanceSheetDetailStatus.NoPay || x.Status == (int)BalanceSheetDetailStatus.UndoNoPay);
            dicCountStatus["undo"] = balanceSheetDetails.Count(x => x.Status == (int)BalanceSheetDetailStatus.Deduction);
            return dicCountStatus;
        }

        private List<VbsBalanceDealInfo> TransformToDealInfos(EmployeeVbsSearchCondition searchCondiction, string searchExpression, out bool unReceiptReceived,
                                                                out List<Guid> unReceiptReceivedDealGuid, int? deliveryType = null)
        {
            var result = new List<VbsBalanceDealInfo>();
            unReceiptReceived = false;
            unReceiptReceivedDealGuid = new List<Guid>();
            if (searchExpression == null)
            {
                return result;
            }
            List<ViewBalancingDeal> intermidiateResult;
            var condition = string.Empty;
            var rpSearchExpression = searchExpression.Replace("'", "''");

            switch (searchCondiction)
            {
                case EmployeeVbsSearchCondition.DealId:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    int dealId;
                    if (int.TryParse(searchExpression, out dealId))
                    {
                        intermidiateResult = _ap.ViewBalancingDealGetByDealId(dealId, true, true).ToList();
                        condition = string.Format(" and dp.unique_id = {0}", dealId);
                    }
                    break;

                case EmployeeVbsSearchCondition.DealName:
                    intermidiateResult = _ap.ViewBalancingDealGetListLikeDealName(searchExpression, true, false).ToList();
                    condition = string.Format(" and i.item_name like N'%{0}%'", rpSearchExpression);
                    break;

                case EmployeeVbsSearchCondition.SellerName:
                    intermidiateResult = _ap.ViewBalancingDealGetListLikeSellerName(searchExpression, true, false).ToList();
                    condition = string.Format(" and s.seller_name like N'%{0}%'", rpSearchExpression);
                    break;

                case EmployeeVbsSearchCondition.SellerId:
                    intermidiateResult = _ap.ViewBalancingDealGetListLikeSellerId(searchExpression, true, false).ToList();
                    condition = string.Format(" and s.seller_id like '%{0}%'", rpSearchExpression);
                    break;

                default:
                    intermidiateResult = new List<ViewBalancingDeal>();
                    break;
            }

            var unReceiptReceivedInfo = new List<Guid>();

            #region 憑證對帳單

            intermidiateResult.AddRange(_ap.GetFlexiblePayToShopDealWithOutBalanceSheet(condition));
            if (intermidiateResult.Any())
            {
                var toShopDealBsInfo = _ap.ViewBalanceSheetListGetList(intermidiateResult.Select(x => x.MerchandiseGuid).Distinct())
                                            .Where(x => x.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                                                        x.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet)
                                            .ToList();
                unReceiptReceivedInfo = toShopDealBsInfo
                                        .Where(x => !x.IsReceiptReceived && x.EstAmount > 0)
                                        .GroupBy(x => x.ProductGuid)
                                        .Select(x => x.Key)
                                        .ToList();
                var dicBalancingCount = toShopDealBsInfo
                                            .GroupBy(x => x.ProductGuid)
                                            .ToDictionary(x => x.Key, x => x.Count(b => !b.IsConfirmedReadyToPay));

                result = intermidiateResult
                    .Where(dbresult =>
                    {
                        if (dbresult.GenerationFrequency != null)
                        {
                            if (DateTime.Now > dbresult.DealEndTime)  //已過結檔時間判斷好康檔次是否過門檻
                            {
                                if (dbresult.BusinessModel.Equals((int)BusinessModel.Ppon))
                                {
                                    if (dbresult.DealEstablished.HasValue)
                                    {
                                        return int.Equals(1, dbresult.DealEstablished); //過門檻
                                    }

                                    return false;
                                }

                                return true;
                            }
                            //未結檔, 但隨買即用(避免撈出未結檔的無效檔次)
                            if (DateTime.Compare(dbresult.DealUseStartTime.Value.Date, dbresult.DealEndTime.Value.Date) <= 0 &&
                                DateTime.Compare(dbresult.DealUseStartTime.Value.Date, dbresult.DealStartTime.Value.Date) >= 0)
                            {
                                //結檔日或兌換開始日後(判斷兌換開始日與結檔日哪個先) 才可顯示於檔次列表中
                                if (DateTime.Compare(dbresult.DealUseStartTime.Value.Date, dbresult.DealEndTime.Value.Date) < 0)
                                {
                                    return DateTime.Compare(DateTime.Now, dbresult.DealUseStartTime.Value) > 0;
                                }

                                return DateTime.Compare(DateTime.Now, dbresult.DealEndTime.Value) > 0;
                            }

                            return false;
                        }

                        return false;
                    })
                    .GroupBy(x => new { x.MerchandiseGuid, x.DealId, x.DealUseStartTime, x.DealUseEndTime, x.DealName, x.BusinessModel, x.GenerationFrequency, x.DeliveryType, x.StoreCount })
                    //although the SsBll methods currently used already filtered nulls, but just in case...
                    .Select(dbresult =>
                    {
                        DateTime useStartTime = dbresult.Key.DealUseStartTime.HasValue
                                                    ? dbresult.Key.DealUseStartTime.Value
                                                    : DateTime.MinValue;
                        DateTime useEndTime = dbresult.Key.DealUseEndTime.HasValue
                                                  ? dbresult.Key.DealUseEndTime.Value
                                                  : DateTime.MinValue;
                        DateTime now = DateTime.Now;

                        #region determine balance state

                        //TODO:  retrieve max_use_end_date in one query, and modify VerificationPolicy to return value without querying database

                        var verificationPolicy = new VerificationPolicy();

                        var balanceState =
                            dicBalancingCount.ContainsKey(dbresult.Key.MerchandiseGuid) &&
                            dicBalancingCount[dbresult.Key.MerchandiseGuid].Equals(0) &&
                            (!verificationPolicy.IsInVerifyTimeRange(BusinessModel.Ppon, dbresult.Key.MerchandiseGuid, null, dbresult.Max(b => b.IntervalEnd.Value), true))
                                ? VbsBalanceState.Finished
                                : VbsBalanceState.Balancing;

                        #endregion determine balance state

                        return new VbsBalanceDealInfo
                        {
                            DealName = dbresult.Key.DealName,
                            DealId = dbresult.Key.MerchandiseGuid,
                            DealType = ((BusinessModel)dbresult.Key.BusinessModel) == BusinessModel.Ppon
                                           ? VbsDealType.Ppon
                                           : VbsDealType.PiinLife,
                            ExchangePeriodStartTime = useStartTime,
                            ExchangePeriodEndTime = useEndTime,
                            StoreCount = dbresult.Key.StoreCount,
                            BalanceState = balanceState,
                            VerificationState =
                                (now.Date < useStartTime.Date
                                     ? VbsVerificationState.NotYetStart
                                     : now.Date > useEndTime.Date
                                           ? VbsVerificationState.Finished
                                           : VbsVerificationState.Verifying),
                            DealUniqueId = dbresult.Key.DealId,
                            GenerationFrequency = dbresult.Key.GenerationFrequency.Value,
                            DeliveryType = dbresult.Key.DeliveryType.Value
                        };
                    }).ToList();
            }

            var tempDealInfos = _ap.ViewBalanceSheetListGetList(result.Select(x => x.DealId).Distinct());
            tempDealInfos.AddRange(_ap.GetFlexiblePayToShopDealWithOutBalanceSheet(result.Select(x => x.DealId).Distinct()));
            var dealStoreInfos = tempDealInfos
                                    .GroupBy(x => new { x.ProductGuid, x.StoreGuid })
                                    .Select(x => new VbsBalanceDealInfo
                                    {
                                        DealId = x.Key.ProductGuid,
                                        DealType = x.First().ProductType == (int)BusinessModel.Ppon
                                                   ? VbsDealType.Ppon
                                                   : VbsDealType.PiinLife,
                                        DealName = x.First().Name,
                                        StoreGuid = x.Key.StoreGuid,
                                        StoreName = x.First().StoreName,
                                        DealUniqueId = x.First().UniqueId,
                                        ExchangePeriodEndTime = x.First().UseEndTime.Value,
                                        GenerationFrequency = x.OrderByDescending(g => g.IntervalEnd).Max(g => g.GenerationFrequency),
                                        DeliveryType = x.First().DeliveryType.Value,
                                        RemittanceType = x.First().RemittanceType
                                    })
                                    .ToList();

            #endregion 憑證對帳單

            #region 宅配對帳單

            var dealIdList = result.Select(x => x.DealId).ToList();
            var balancingDealInfo = _ap.PartialPaymentToBalanceingDeal(condition)
                                        .AsEnumerable()
                                        .Where(x => !dealIdList.Contains(x.Field<Guid>("merchandise_guid")));
            unReceiptReceivedInfo.AddRange(balancingDealInfo.Where(x => x.Field<int>("is_receipt_received") == 0 && x.Field<int?>("est_amount") != 0)
                                                            .Select(x => x.Field<Guid>("merchandise_guid")));

            var addin = from x in balancingDealInfo
                        select new VbsBalanceDealInfo
                        {
                            DealName = x.Field<string>("deal_name"),
                            DealId = x.Field<Guid>("merchandise_guid"),
                            DealType = VbsDealType.Ppon,
                            ExchangePeriodStartTime = x.Field<DateTime>("deal_use_start_time"),
                            ExchangePeriodEndTime = x.Field<DateTime>("deal_use_end_time"),
                            StoreCount = 0,
                            BalanceState = x.Field<int>("is_confirmed_ready_to_pay") > 0 &&
                                           !BalanceSheetManager.IsInBalanceTimeRange(x.Field<Guid>("merchandise_guid"), x.Field<DateTime>("interval_end"), true)
                                            ? VbsBalanceState.Finished
                                            : VbsBalanceState.Balancing,
                            VerificationState = x.Field<DateTime?>("final_balance_sheet_date").HasValue
                                                ? VbsVerificationState.ShipFinished
                                                : VbsVerificationState.ReturnAndChanging,
                            DealUniqueId = x.Field<int>("deal_id"),
                            GenerationFrequency = x.Field<int>("generation_frequency"),
                            RemittanceType = x.Field<int>("remittance_type"),
                            DeliveryType = x.Field<int>("delivery_type")
                        };
            result.AddRange(addin);

            unReceiptReceived = result.Any(x => unReceiptReceivedInfo.Contains(x.DealId));
            unReceiptReceivedDealGuid.AddRange(result.Where(x => unReceiptReceivedInfo.Contains(x.DealId)).Select(x => x.DealId));

            dealStoreInfos.AddRange(addin.Select(x => new VbsBalanceDealInfo
            {
                DealId = x.DealId,
                DealType = x.DealType,
                DealName = x.DealName,
                StoreGuid = (Guid?)null,
                StoreName = string.Empty,
                DealUniqueId = x.DealUniqueId,
                ExchangePeriodEndTime = x.ExchangePeriodEndTime,
                GenerationFrequency = x.GenerationFrequency,
                DeliveryType = x.DeliveryType,
                RemittanceType = x.RemittanceType
            }));

            #endregion 宅配對帳單

            List<VbsBalanceDealInfo> data = new List<VbsBalanceDealInfo>();
            if (config.IsEnableVbsNewUi)
            {
                if (deliveryType == (int)DeliveryType.ToShop)
                {
                    dealStoreInfos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToShop);
                    result.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToShop);
                }
                else if (deliveryType == (int)DeliveryType.ToHouse)
                {
                    dealStoreInfos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToHouse);
                    result.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToHouse);
                }
            }

            Session[VbsSession.BalanceDealInfos.ToString()] = dealStoreInfos.OrderByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId).ToList();
            data = result.OrderBy(x => x.BalanceState).ThenByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId).ToList();

            return data;
        }

        private ViewBalanceSheetListCollection GetBalanceSheets(Guid productGuid, Guid? storeGuid, BusinessModel productType)
        {
            var bsList = _ap.ViewBalanceSheetListGetList(productType, productGuid, storeGuid);
            if (!storeGuid.HasValue)
            {
                bsList.AddRange(_ap.GetFlexiblePayToShopDealWithOutBalanceSheet(new List<Guid> { productGuid }));
            }

            return bsList;
        }

        private ViewBalanceSheetListCollection GetAllowedViewBalanceSheetListCollection(List<IVbsVendorAce> allowedDealUnits)
        {
            var result = new ViewBalanceSheetListCollection();

            var bids = allowedDealUnits.Select(x => x.MerchandiseGuid.Value).Distinct().ToList();

            if (!bids.Any())
            {
                return result;
            }

            var tempDealList = _ap.ViewBalanceSheetListGetList(bids)
                                    .Where(x => x.DeliveryType == (int)DeliveryType.ToShop)
                                    .ToList();
            tempDealList.AddRange(_ap.GetFlexiblePayToShopDealWithOutBalanceSheet(bids));
            tempDealList.AddRange(_ap.PartialPaymentToBalanceSheetList(bids));

            var indexer = tempDealList.GroupBy(x => x.ProductGuid)
                                      .ToDictionary(x => x.Key, x => x.ToList());

            var indexer2 = tempDealList.GroupBy(x => new { x.ProductGuid, x.StoreGuid })
                                       .ToDictionary(x => string.Format("{0}::{1}", x.Key.ProductGuid, x.Key.StoreGuid), x => x.ToList());

            foreach (var dealUnit in allowedDealUnits)
            {
                if (dealUnit.IsHomeDelivery.GetValueOrDefault(false))
                {
                    if (dealUnit.MerchandiseGuid != null && indexer.ContainsKey(dealUnit.MerchandiseGuid.Value))
                    {
                        result.AddRange(indexer[dealUnit.MerchandiseGuid.Value]);
                    }
                }
                else if (dealUnit.IsInStore.GetValueOrDefault(false))
                {
                    if (dealUnit.MerchandiseGuid != null && dealUnit.StoreGuid != null)
                    {
                        string key = string.Format("{0}::{1}", dealUnit.MerchandiseGuid.Value, dealUnit.StoreGuid.Value);
                        if (indexer2.ContainsKey(key))
                        {
                            result.AddRange(indexer2[key]);
                        }
                    }
                }
            }

            return result;
        }

        private List<VbsBalanceDealInfo> GetBalanceDealInfo(ViewBalanceSheetListCollection dealLists,
            VbsBalanceState state, out bool unReceiptReceived, int? deliveryType = null)
        {
            var infos = new List<VbsBalanceDealInfo>();
            var now = DateTime.Now;
            VbsVerificationState verifiedState;
            VbsBalanceState balanceState;
            //憑證對帳單判斷對帳狀態使用：不放在下方 group by 計算(不分月/週對帳單)，需另外統計 避免未產生月對帳單之檔次 被誤判為對帳結束
            var dicBalancingCount = dealLists.Where(
                x => x.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                     x.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet)
                .GroupBy(x => x.ProductGuid)
                .ToDictionary(x => x.Key, x => x.Count(b => !b.IsConfirmedReadyToPay));
            var balanceDeals = dealLists.GroupBy(x => x.ProductGuid)
                .Select(x => new
                {
                    ProductGuid = x.Key,
                    x.First().ProductType,
                    x.First().Name,
                    x.First().AppTitle,
                    x.First().UseStartTime,
                    x.First().UseEndTime,
                    x.First().UniqueId,
                    x.First().DeliveryType,
                    x.First().RemittanceType,
                    x.OrderByDescending(t => t.IntervalEnd).First().GenerationFrequency,
                    IntervalEnd = x.Max(t => t.IntervalEnd),
                    StoreCount = x.Where(t => t.StoreGuid != null).Select(t => t.StoreGuid).Distinct().Count()
                });

            //宅配對帳單判斷退換貨狀態：檢查是否已壓退換貨完成日
            var toHouseDealGuids = dealLists.Where(x => x.DeliveryType == (int)DeliveryType.ToHouse)
                .Select(x => x.ProductGuid);
            var dealAccountings = _pp.DealAccountingGet(toHouseDealGuids)
                .GroupBy(x => x.BusinessHourGuid)
                .ToDictionary(x => x.Key, x => x.First());
            //判斷是否有需要顯示[總對帳單]連結使用：檢查 憑證 or 宅配(非多檔次) or 宅配(多檔次)對帳單 是否有單據未回
            unReceiptReceived = dealLists.Any(x =>
            {
                if (x.DeliveryType == (int)DeliveryType.ToShop &&
                    (x.BalanceSheetType == (int)BalanceSheetType.WeeklyPayWeekBalanceSheet ||
                     x.BalanceSheetType == (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet))
                {
                    return false;

                }
                return !x.IsReceiptReceived;
            });

            foreach (var dealList in balanceDeals)
            {
                verifiedState = VbsVerificationState.NotYetStart;
                balanceState = VbsBalanceState.Balancing;

                //新增判斷好康宅配檔對帳單
                if (dealList.ProductType.Equals((int)BusinessModel.Ppon) &&
                    dealList.DeliveryType.Equals((int)DeliveryType.ToHouse))
                {
                    if (dicBalancingCount.ContainsKey(dealList.ProductGuid))
                    {
                        if (!BalanceSheetManager.IsInBalanceTimeRange(dealList.ProductGuid, dealList.IntervalEnd, true) &&
                            dicBalancingCount[dealList.ProductGuid].Equals(0))
                        {
                            balanceState = VbsBalanceState.Finished;
                        }
                    }

                    if (state != balanceState)
                    {
                        continue;
                    }

                    var finalBsDate = dealAccountings.ContainsKey(dealList.ProductGuid)
                        ? dealAccountings[dealList.ProductGuid].FinalBalanceSheetDate
                        : null;
                    verifiedState = finalBsDate.HasValue
                        ? VbsVerificationState.ShipFinished
                        : VbsVerificationState.ReturnAndChanging;
                }
                else
                {
                    //無論週月結 依照該檔次產出最後之月對帳單資料來判斷對帳單確認狀態(因分店的關係 可能為一筆或多筆)
                    //月or人工對帳單之interval_end於 兌換期間內皆顯示對帳中  於兌換期間外 則需檢查是否月/人工對帳單皆為已確認
                    if (dicBalancingCount.ContainsKey(dealList.ProductGuid))
                    {
                        //若最後產出的月or人工對帳單為多筆(多家分店) 則需檢查是否月對帳單皆為已確認
                        if (
                            !(new VerificationPolicy()).IsInVerifyTimeRange((BusinessModel)dealList.ProductType,
                                dealList.ProductGuid, null, dealList.IntervalEnd, true) &&
                            dicBalancingCount[dealList.ProductGuid].Equals(0))
                        {
                            balanceState = VbsBalanceState.Finished;
                        }
                    }

                    if (state != balanceState)
                    {
                        continue;
                    }

                    if (now.Date < dealList.UseStartTime.Value.Date) //尚未開始
                    {
                        verifiedState = VbsVerificationState.NotYetStart;
                    }
                    else if (now.Date >= dealList.UseStartTime.Value.Date && now.Date <= dealList.UseEndTime.Value.Date)
                    //兌換中
                    {
                        verifiedState = VbsVerificationState.Verifying;
                    }
                    else if (now.Date > dealList.UseEndTime.Value.Date) //活動結束
                    {
                        verifiedState = VbsVerificationState.Finished;
                    }
                }

                var info1 = new VbsBalanceDealInfo
                {
                    DealName = !string.IsNullOrEmpty(dealList.AppTitle) ? dealList.AppTitle : dealList.Name,
                    DealId = dealList.ProductGuid,
                    DealType = (dealList.ProductType.Equals((int)BusinessModel.Ppon) ? VbsDealType.Ppon : VbsDealType.PiinLife),
                    DealUniqueId = dealList.UniqueId,
                    ExchangePeriodStartTime = dealList.UseStartTime.Value,
                    ExchangePeriodEndTime = dealList.UseEndTime.Value,
                    StoreCount = dealList.StoreCount,
                    VerificationState = verifiedState,
                    BalanceState = balanceState,
                    GenerationFrequency = dealList.GenerationFrequency,
                    DeliveryType = dealList.DeliveryType.Value,
                    RemittanceType = dealList.RemittanceType
                };
                infos.Add(info1);
            }

            List<VbsBalanceDealInfo> data = new List<VbsBalanceDealInfo>();
            List<VbsBalanceDealInfo> tempdealLists = new List<VbsBalanceDealInfo>();

            tempdealLists = dealLists.Where(x => infos.Select(d => d.DealId).Contains(x.ProductGuid))
                                                                       .GroupBy(x => new { x.ProductGuid, x.StoreGuid })
                                                                       .Select(x => new VbsBalanceDealInfo
                                                                       {
                                                                           DealId = x.Key.ProductGuid,
                                                                           DealType = x.First().ProductType == (int)BusinessModel.Ppon
                                                                                        ? VbsDealType.Ppon
                                                                                        : VbsDealType.PiinLife,
                                                                           DealName = !string.IsNullOrEmpty(x.First().AppTitle) ? x.First().AppTitle : x.First().Name,
                                                                           StoreGuid = x.Key.StoreGuid,
                                                                           StoreName = x.First().StoreName,
                                                                           DealUniqueId = x.First().UniqueId,
                                                                           ExchangePeriodEndTime = x.First().UseEndTime.Value,
                                                                           GenerationFrequency = x.First().GenerationFrequency,
                                                                           DeliveryType = x.First().DeliveryType.Value,
                                                                           RemittanceType = x.First().RemittanceType
                                                                       })
                                                                       .OrderByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId)
                                                                       .ToList();

            if (config.IsEnableVbsNewUi)
            {
                if (deliveryType == (int)DeliveryType.ToShop)
                {
                    tempdealLists.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToShop);
                    infos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToShop);
                }
                else if (deliveryType == (int)DeliveryType.ToHouse)
                {
                    tempdealLists.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToHouse);
                    infos.RemoveAll(x => x.DeliveryType != (int)DeliveryType.ToHouse);
                }
            }

            Session[VbsSession.BalanceDealInfos.ToString()] = tempdealLists;
            data = infos.OrderBy(x => x.BalanceState).ThenByDescending(x => x.ExchangePeriodEndTime).ThenBy(x => x.DealUniqueId).ToList();

            return data;
        }

        private ViewBalanceSheetListCollection GetBalanceStoreList(bool is17LifeEmployee, BusinessModel productType, Guid productGuid)
        {
            var storeLists = new ViewBalanceSheetListCollection();

            if (is17LifeEmployee)
            {
                storeLists = GetBalanceSheets(productGuid, null, productType);
            }
            else
            {
                var allowedDealUnits = GetAllowedDealUnits(VbsCurrent.AccountId, productGuid, VbsRightFlag.ViewBalanceSheet, false);
                storeLists = GetAllowedViewBalanceSheetListCollection(allowedDealUnits);
            }

            return storeLists;
        }

        private void SetupDemoBalanceSheetsMonth(Guid id, VbsDealType type, string detailId, int? balanceSheetId, out BalanceSheetsMonthModel vdlModel, out List<VbsBalanceSheetsMonthInfo> infos)
        {
            BalanceSheetsMonthModel resultModel = new BalanceSheetsMonthModel();

            VbsBalanceDealInfo dealInfo = VbsCurrent.DemoData.BalanceSheetDealInfos.First(x => x.DealId == id);
            if (!balanceSheetId.HasValue)
            {
                balanceSheetId = VbsCurrent.DemoData.BalanceSheetMonthInfos.OrderByDescending(x => x.BalanceSheetId).First().BalanceSheetId;
            }

            string bsMonthInfo = VbsCurrent.DemoData.BalanceSheetMonthInfos.First(x => x.BalanceSheetId == balanceSheetId).BalanceSheetMonth;

            resultModel.DealId = id;
            resultModel.DealName = dealInfo.DealName;
            resultModel.DealType = type;
            resultModel.DealUniqueId = dealInfo.DealUniqueId;
            resultModel.ExchangePeriodStartTime = dealInfo.ExchangePeriodStartTime;
            resultModel.ExchangePeriodEndTime = dealInfo.ExchangePeriodEndTime;
            resultModel.StoreId = detailId;

            if (detailId == "all")
            {
                resultModel.StoreName = "所有分店";
            }
            else if (detailId == "null")
            {
                resultModel.StoreName = null;
            }
            else
            {
                resultModel.StoreName = VbsCurrent.DemoData.BalanceSheetStoreInfos.First(x => x.StoreId == detailId).StoreName;
            }

            resultModel.VendorReceiptType = (int)VendorReceiptType.Invoice;
            resultModel.ReturnCount = 9;
            resultModel.VerifiedCount = 7;
            resultModel.UnDoCount = 1;
            resultModel.PaymentPeriodStartTime = new DateTime(int.Parse(bsMonthInfo.Substring(0, 4)), int.Parse(bsMonthInfo.Substring(5, 2)), 1);
            resultModel.PaymentPeriodEndTime = new DateTime(int.Parse(bsMonthInfo.Substring(0, 4)), int.Parse(bsMonthInfo.Substring(5, 2)), 31);
            resultModel.IsTax = true;
            resultModel.RemittanceType = (dealInfo.GenerationFrequency == (int)BalanceSheetGenerationFrequency.MonthBalanceSheet)
                                        ? (int)RemittanceType.AchMonthly
                                        : (int)RemittanceType.AchWeekly;
            resultModel.FundTransferInfo = new FundTransferInfo(BalanceSheetType.WeeklyPayMonthBalanceSheet);

            vdlModel = resultModel;
            infos = VbsCurrent.DemoData.BalanceSheetMonthInfos.OrderByDescending(x => x.BalanceSheetId).ToList();
        }

        private static MemoryStream MonthlyVerificationListExport(MonthlyVerificationListModel vdlModel, int achType)
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("本月核銷清單");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue(vdlModel.BalanceSheetMonth + "月對帳");
            if (vdlModel.StoreName != null)
            {
                cols.CreateCell(1).SetCellValue("[" + vdlModel.DealUniqueId + "] " + vdlModel.DealName + " (" + vdlModel.StoreName + ")");
            }
            else
            {
                cols.CreateCell(1).SetCellValue("[" + vdlModel.DealUniqueId + "] " + vdlModel.DealName);
            }
            cols = sheet.CreateRow(1);
            cols.CreateCell(0).SetCellValue("結算時間：");
            cols.CreateCell(1).SetCellValue(vdlModel.MonthIntervalStartTime.ToString("yyyy/MM/dd") + " ～ " + vdlModel.MonthIntervalEndTime.ToString("yyyy/MM/dd"));
            cols = sheet.CreateRow(2);
            cols.CreateCell(0).SetCellValue("核銷金額：");
            cols.CreateCell(1).SetCellValue(string.Format("{0:$#,0}", vdlModel.VerifiedTotal));
            cols = sheet.CreateRow(3);
            cols.CreateCell(0).SetCellValue("取消核銷金額：");
            cols.CreateCell(1).SetCellValue(string.Format("{0:$#,0}", vdlModel.UnDoTotal));
            cols = sheet.CreateRow(4);
            cols.CreateCell(0).SetCellValue("本月對帳金額：");
            cols.CreateCell(1).SetCellValue(string.Format("{0:$#,0}", vdlModel.PaymentTotal));
            cols = sheet.CreateRow(5);
            cols.CreateCell(0).SetCellValue("（核銷金額 - 取消核銷金額 = 本月對帳金額）");
            cols = sheet.CreateRow(6);
            if (achType == (int)RemittanceType.ManualMonthly || achType == (int)RemittanceType.AchMonthly)
            {
                cols.CreateCell(0).SetCellValue("付款日期：");
                cols.CreateCell(1).SetCellValue(vdlModel.PaymentState);
            }
            cols = sheet.CreateRow(7);
            cols.CreateCell(0).SetCellValue("憑證編號");
            cols.CreateCell(1).SetCellValue("核銷時間");
            cols.CreateCell(2).SetCellValue("取消核銷時間");
            cols.CreateCell(3).SetCellValue("商品名稱");
            cols.CreateCell(4).SetCellValue("分店");
            if (achType == (int)RemittanceType.ManualWeekly || achType == (int)RemittanceType.AchWeekly)
            {
                cols.CreateCell(5).SetCellValue("付款週次");
            }

            int rowIdx = 8;
            foreach (var item in vdlModel.BalanceCouponInfos)
            {
                Row row = sheet.CreateRow(rowIdx);
                Font font = workbook.CreateFont();
                CellStyle cellStyle = workbook.CreateCellStyle();
                //若UnDoState為-反核銷 需以刪除線標註
                if (item.UnDoState == BalanceSheetDetailStatus.Undo)
                {
                    font.IsStrikeout = true;
                }
                else
                {
                    font.IsStrikeout = false;
                }

                cellStyle.SetFont(font);
                row.CreateCell(0).SetCellValue(item.CouponId);
                row.GetCell(0).CellStyle = cellStyle;
                row.CreateCell(1).SetCellValue(item.VerifiedDateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                row.GetCell(1).CellStyle = cellStyle;
                if (item.UnDoDateTime.HasValue)
                {
                    row.CreateCell(2).SetCellValue(item.UnDoDateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"));
                }
                else
                {
                    row.CreateCell(2).SetCellValue("");
                }
                row.GetCell(2).CellStyle = cellStyle;
                row.CreateCell(3).SetCellValue(item.ItemName);
                row.GetCell(3).CellStyle = cellStyle;
                row.CreateCell(4).SetCellValue(item.StoreName);
                row.GetCell(4).CellStyle = cellStyle;
                if (achType == (int)RemittanceType.ManualWeekly || achType == (int)RemittanceType.AchWeekly)
                {
                    var m = vdlModel.WeeklyVerificationInfos.Where(x => x.BalanceSheetId == item.BalanceSheetId).First();
                    row.CreateCell(5).SetCellValue(m.PaymentState + " " + string.Format("{0:$#,0}", m.PaymentTotal) + " （核銷區間：" + m.WeekIntervalStartTime.ToString("yyyy/MM/dd") + " ~ " + m.WeekIntervalEndTime.ToString("yyyy/MM/dd") + ")");
                    row.GetCell(5).CellStyle = cellStyle;
                }
                rowIdx++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            sheet.AutoSizeColumn(4);
            if (achType == (int)RemittanceType.ManualWeekly || achType == (int)RemittanceType.AchWeekly)
            {
                sheet.AutoSizeColumn(5);
            }
            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private static List<VbsBalanceCouponInfo> GetBalanceSheetDetailList(int bsId)
        {
            DataTable balanceSheetDetailLists = new DataTable();
            BalanceSheetDetailStatus unDoStatus;
            DateTime? unDoDateTime;
            DateTime tempDateTime, verifyDateTime;
            List<VbsBalanceCouponInfo> couponInfos = new List<VbsBalanceCouponInfo>();

            balanceSheetDetailLists = _ap.CashTrustLogGetListByBalanceSheetId(bsId);
            foreach (DataRow row in balanceSheetDetailLists.Rows)
            {
                unDoStatus = BalanceSheetDetailStatus.Normal;
                unDoDateTime = null;
                switch (Convert.ToInt32(row["unverify_status"].ToString()))
                {
                    case (int)BalanceSheetDetailStatus.Normal:
                        unDoStatus = BalanceSheetDetailStatus.Normal;
                        break;
                    //反核銷未扣款 仍列入核銷清單中 但須有刪除註記
                    case (int)BalanceSheetDetailStatus.Undo:
                        unDoStatus = BalanceSheetDetailStatus.Undo;
                        break;
                    case (int)BalanceSheetDetailStatus.NoPay:
                        unDoStatus = BalanceSheetDetailStatus.NoPay;
                        break;
                    case (int)BalanceSheetDetailStatus.UndoNoPay:
                        unDoStatus = BalanceSheetDetailStatus.UndoNoPay;
                        break;
                    //反核銷已扣款 需紀錄undo time
                    case (int)BalanceSheetDetailStatus.Deduction:
                        unDoStatus = BalanceSheetDetailStatus.Deduction;
                        unDoDateTime = DateTime.TryParse(row["undo_time"].ToString(), out tempDateTime) ? DateTime.Parse(row["undo_time"].ToString()) : unDoDateTime;
                        break;
                    default:
                        break;
                }
                verifyDateTime = DateTime.Parse(row["modify_time"].ToString());
                VbsBalanceCouponInfo couponInfo = new VbsBalanceCouponInfo
                {
                    BalanceSheetId = bsId,
                    CouponId = row["coupon_sequence_number"].ToString(),
                    ItemName = row["item_name"].ToString(),
                    VerifiedDateTime = verifyDateTime,
                    StoreName = row["store_name"].ToString(),
                    UserName = row["create_id"].ToString(),
                    UnDoState = unDoStatus,
                    UnDoDateTime = (unDoDateTime.HasValue) ? unDoDateTime.Value : unDoDateTime
                };
                couponInfos.Add(couponInfo);
            }

            return couponInfos;
        }

        private bool CheckIsAllowedDealOrStore(string accountId, Guid merchandiseGuid, Guid? storeGuid, VbsRightFlag vbsRight)
        {
            bool isAllowedDealOrStore = false;

            if (storeGuid.HasValue)
            {
                isAllowedDealOrStore = GetAllowedStores(merchandiseGuid).Contains(storeGuid.Value);
            }
            else
            {
                isAllowedDealOrStore = GetAllowedDealUnits(accountId, merchandiseGuid, vbsRight, false).Count > 0;
            }

            return isAllowedDealOrStore;
        }

        private static BalanceSheetPackModel GetBalanceSheetPack(BalanceSheetPackModel model, List<ViewBalanceSheetList> dealLists)
        {
            model.SellerInfos = dealLists
                                .Where(x => x.SellerGuid != Guid.Empty)
                                .GroupBy(x => new { x.SellerGuid, x.SellerName })
                                .ToDictionary(x => x.Key.SellerGuid, x => x.Key.SellerName);
            //用string格式做client side兩層篩選
            //sellerGuid1,ToShop:憑證,ToHouse:宅配|sellerGuid2,ToShop:憑證,ToHouse:宅配|...
            model.DeliveryTypeInfos = string.Join("|", dealLists
                                        .Where(x => x.DeliveryType.HasValue)
                                        .GroupBy(x => x.SellerGuid)
                                        .Select(x => string.Format("{0},{1}",
                                                        x.Key,
                                                        string.Join(",", x.GroupBy(t => t.DeliveryType.Value)
                                                            .OrderBy(t => t.Key)
                                                            .Select(t => string.Format("{0}:{1}",
                                                                           (DeliveryType)t.Key,
                                                                           t.Key == (int)DeliveryType.ToShop
                                                                              ? I18N.Phrase.ToShop
                                                                              : I18N.Phrase.ToHouse))))));
            model.BalanceSheetsMonthInfos = dealLists.Where(x => x.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                                                                 x.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet &&
                                                                 x.Year.HasValue &&
                                                                 x.Month.HasValue)
                                                       .GroupBy(x => x.Year.Value * 100 + x.Month.Value)
                                                       .OrderBy(x => x.Key)
                                                       .ToDictionary(x => x.Key, x => x.Select(b => string.Format("{0}/{1}月", b.Year, b.Month)).First());

            if (!model.SellerGuid.Equals(Guid.Empty) && !model.DeliveryType.Equals(0))
            {
                var infos = dealLists.Where(x => x.SellerGuid == model.SellerGuid &&
                                                 x.DeliveryType == (int)model.DeliveryType);
                if (model.DeliveryType.Equals(DeliveryType.ToShop) && !model.BsYearMonth.Equals(0))
                {
                    infos = infos.Where(x => x.Year * 100 + x.Month == model.BsYearMonth);
                }
                var subDeals = GetComboSubDealsInfo(infos.Where(x => x.DeliveryType == (int)DeliveryType.ToHouse).Select(x => x.ProductGuid));
                var bsIds = infos.Select(x => x.Id);
                var allowanceAmountInfos = _ap.VendorPaymentChangeGetList(subDeals.Keys)
                                                .Where(x => x.BalanceSheetId.HasValue && bsIds.Contains(x.BalanceSheetId.Value))
                                                .GroupBy(x => x.BusinessHourGuid)
                                                .ToDictionary(x => x.Key, x => x.Sum(a => a.AllowanceAmount == 0 ? a.Amount : 0));
                var mainDealAllowanceInfo = (from ai in allowanceAmountInfos
                                             join deal in subDeals on ai.Key equals deal.Key
                                             group ai by deal.Value
                                                 into ad
                                             select new
                                             {
                                                 MainDealGuid = ad.Key,
                                                 AddInAmount = ad.Sum(x => x.Value)
                                             })
                                            .ToDictionary(x => x.MainDealGuid, x => x.AddInAmount);

                //所有要請款的對帳單
                List<Guid> pordictGuidList = subDeals.Select(x => x.Key).ToList();
                pordictGuidList = pordictGuidList.Union(infos.Select(x => x.ProductGuid)).ToList();//舊版單檔也要抓

                
                var bsc = _ap.BalanceSheetGetListByProductGuidStoreGuid(pordictGuidList).Where(x => !x.IsConfirmedReadyToPay && !x.IsReceiptReceived);
                model.BalanceSheetPackInfos = infos.Select(x =>
                {
                    var addInAmount = 0;
                    if (x.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        addInAmount = mainDealAllowanceInfo.ContainsKey(x.ProductGuid)
                                        ? mainDealAllowanceInfo[x.ProductGuid]
                                        : allowanceAmountInfos.ContainsKey(x.ProductGuid)
                                            ? allowanceAmountInfos[x.ProductGuid]
                                            : 0;
                    }


                    //宅配才需要貨款等明細
                    int sumPayAmount = 0;
                    int sumPayFreightAmount = 0;
                    int sumDeductAmount = 0;
                    int sumPaymentChangeAmount = 0;
                    int sumPositivePaymentOverdueAmount = 0;
                    int sumNegativePaymentOverdueAmount = 0;
                    int sumISPFamilyOrderAmount = 0;
                    int sumISPSevenOrderAmount = 0;
                    int sumWmsOrderAmount = 0;
                    List<Guid> dealList = new List<Guid>();

                    if (x.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        ComboDealCollection cdc = _pp.GetComboDealByBid(x.ProductGuid, false);
                        if (cdc.Count() > 0)
                        {
                            foreach (var cs in cdc)
                            {
                                dealList.Add(cs.BusinessHourGuid);
                            }

                        }
                        else
                        {
                            dealList.Add(x.ProductGuid);
                        }


                        //撈出各母子檔的金額明細
                        foreach (var deal in dealList)
                        {
                            var dealBss = bsc.Where(y => y.ProductGuid == deal);

                            foreach (var dealBs in dealBss)
                            {
                                var bsModel = new BalanceSheetModel(dealBs.Id);
                                var deliveryPaymentInfo = bsModel.GetDeliveryPaymentInfo();
                                var vendorPaymentAmount = bsModel.GetVendorPaymentAmount();

                                sumPayAmount += Convert.ToInt32(deliveryPaymentInfo.PayAmount);
                                sumPayFreightAmount += deliveryPaymentInfo.PayFreightAmount;
                                sumDeductAmount += (Convert.ToInt32(deliveryPaymentInfo.DeductAmount) + deliveryPaymentInfo.DeductFreightAmount);
                                sumPaymentChangeAmount += vendorPaymentAmount != null ? vendorPaymentAmount.Amount : 0;
                                sumPositivePaymentOverdueAmount += bsModel.GetVendorPaymentOverdueAmount(true);
                                sumNegativePaymentOverdueAmount += bsModel.GetVendorPaymentOverdueAmount(false);
                                sumISPFamilyOrderAmount += dealBs.IspFamilyAmount;
                                sumISPSevenOrderAmount += dealBs.IspSevenAmount;
                                sumWmsOrderAmount += dealBs.WmsOrderAmount;
                            }
                        }
                    }
                    
                    


                    //是否含稅
                    var da = _pp.DealAccountingGet(x.ProductGuid);

                    if (x.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        var info = new BalanceSheetPackInfo
                        {
                            DealName = !string.IsNullOrEmpty(x.AppTitle) ? x.AppTitle : x.Name,
                            DealId = x.ProductGuid,
                            DealType = x.ProductType.Equals((int)BusinessModel.Ppon)
                                    ? VbsDealType.Ppon
                                    : VbsDealType.PiinLife,
                            DealUniqueId = x.UniqueId,
                            GenerationFrequency = x.GenerationFrequency,
                            DeliveryType = x.DeliveryType,
                            VendorReceiptType = (VendorReceiptType)x.VendorReceiptType,
                            BalanceSheetId = x.Id,
                            PayAmount = sumPayAmount,
                            PayFreightAmount = sumPayFreightAmount,
                            DeductAmount = sumDeductAmount,
                            PaymentChangeAmount = sumPaymentChangeAmount,
                            PositivePaymentOverdueAmount = sumPositivePaymentOverdueAmount,
                            NegativePaymentOverdueAmount = sumNegativePaymentOverdueAmount,
                            ISPFamilyOrderAmount = sumISPFamilyOrderAmount,
                            ISPSevenOrderAmount = sumISPSevenOrderAmount,
                            EstAmount = (int)(x.TransferAmount ?? (x.EstAmount.GetValueOrDefault(0) - addInAmount)),
                            IspFamilyAmount = x.IspFamilyAmount,
                            IspSevenAmount = x.IspSevenAmount,
                            VendorPaymentOverdue = x.OverdueAmount,
                            WmsOrderAmount = x.WmsOrderAmount,
                            RemittanceType = x.RemittanceType,
                            IsTax = da.IsInputTaxRequired,
                        };
                        return info;
                    }
                    else
                    {
                        var info = new BalanceSheetPackInfo
                        {
                            DealName = x.Name,
                            DealId = x.ProductGuid,
                            DealType = x.ProductType.Equals((int)BusinessModel.Ppon)
                                    ? VbsDealType.Ppon
                                    : VbsDealType.PiinLife,
                            DealUniqueId = x.UniqueId,
                            StoreGuid = x.StoreGuid,
                            StoreName = x.StoreName,
                            GenerationFrequency = x.GenerationFrequency,
                            DeliveryType = x.DeliveryType,
                            VendorReceiptType = (VendorReceiptType)x.VendorReceiptType,
                            BalanceSheetId = x.Id,
                            EstAmount = (int)(x.TransferAmount ?? (x.EstAmount.GetValueOrDefault(0) - addInAmount)),
                            IspFamilyAmount = x.IspFamilyAmount,
                            IspSevenAmount = x.IspSevenAmount,
                            VendorPaymentOverdue = x.OverdueAmount,
                            WmsOrderAmount = x.WmsOrderAmount,
                            RemittanceType = x.RemittanceType,
                            IsTax = da.IsInputTaxRequired,
                        };
                        return info;
                    }
                    
                });

                model.IsResultEmpty = !model.BalanceSheetPackInfos.Any();
            }

            var wmsSellers = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true);
            var wmsInfos = new List<BalanceSheetWmsPackInfo>();
            Dictionary<int, int> seq = new Dictionary<int, int>();
            foreach (KeyValuePair<Guid, string> seller in wmsSellers)
            {
                BalanceSheetWmCollection bswc = _ap.BalanceSheetWmsGetBySeller(seller.Key, VbsBalanceState.Balancing.ToString());

                foreach (BalanceSheetWm bsw in bswc)
                {
                    if (!seq.ContainsKey(bsw.Year + bsw.Month))
                        seq.Add(bsw.Year + bsw.Month, 1);
                    else
                        seq[bsw.Year + bsw.Month]++;

                    wmsInfos.Add(new BalanceSheetWmsPackInfo
                    {
                        Name = bsw.IntervalStart.ToString("yyyy年MM月") + " 倉儲費用對帳0" + seq[bsw.Year + bsw.Month],
                        BalanceSheetWmsId = bsw.Id,
                        EstAmount = bsw.EstAmount,
                    });



                    //當只有倉儲費時的處理
                    if (!model.SellerInfos.ContainsKey(seller.Key))
                    {
                        model.SellerInfos.Add(seller.Key, seller.Value);
                        model.IsResultEmpty = false;
                    }
                    if (string.IsNullOrEmpty(model.DeliveryTypeInfos))
                    {
                        model.DeliveryTypeInfos = seller.Key.ToString() + ",ToHouse: 宅配";
                    }
                }

            }
            model.BalanceSheetWmsPackInfos = wmsInfos;

            return model;
        }

        private int GetSlottingFeeQuantity(Guid productGuid)
        {
            var dcc = _pp.DealCostGetList(productGuid);
            var quantity = dcc.Count > 1 ? dcc.Min(x => x.CumulativeQuantity).Value : 0;
            return quantity;
        }

        private IEnumerable<CrossMonthPaymentInfo> GetCrossMonthPaymentInfos(ViewBalanceSheetList bs, IEnumerable<VbsBalanceCouponInfo> details)
        {
            //憑證週結對帳單才有跨月結算問題
            if (bs.BalanceSheetType != (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet &&
                bs.BalanceSheetType != (int)BalanceSheetType.WeeklyPayBalanceSheet &&
                bs.BalanceSheetType != (int)BalanceSheetType.WeeklyPayWeekBalanceSheet)
            {
                return null;
            }
            //當週對帳區間非跨月對帳單 無須抓取跨月結算資訊
            if (bs.IntervalStart.Month == bs.IntervalEnd.AddDays(-1).Month)
            {
                return null;
            }
            var cost = (int)(bs.Cost ?? 0);
            var paymentInfos = new List<CrossMonthPaymentInfo>
            {
                //截至當週上月月底計算之核銷金額
                new CrossMonthPaymentInfo
                {
                    PaymentPeriodStartTime = bs.IntervalStart,
                    PaymentPeriodEndTime = bs.IntervalStart.GetLastDayOfMonth(),
                    PayableAmount = GetPayableAmount(details, bs.IntervalStart, bs.IntervalEnd.GetFirstDayOfMonth(), cost)
                },
                //截至當週計算之核銷金額
                new CrossMonthPaymentInfo
                {
                    PaymentPeriodStartTime = bs.IntervalEnd.GetFirstDayOfMonth(),
                    PaymentPeriodEndTime = bs.IntervalEnd.AddDays(-1),
                    PayableAmount = GetPayableAmount(details, bs.IntervalEnd.GetFirstDayOfMonth(), bs.IntervalEnd, cost)
                }
            };

            return paymentInfos;
        }

        private int GetPayableAmount(IEnumerable<VbsBalanceCouponInfo> details, DateTime intervalStart, DateTime intervalEnd, int cost)
        {
            var detailInfos = details as IList<VbsBalanceCouponInfo> ?? details.ToList();
            //只需抓取需付款份數 不考慮上架費狀態
            var verifiedCount = detailInfos.Count(x => (x.UnDoState == BalanceSheetDetailStatus.Normal || x.UnDoState == BalanceSheetDetailStatus.Undo)
                                                    && x.VerifiedDateTime > intervalStart
                                                    && x.VerifiedDateTime < intervalEnd);
            var deductCount = detailInfos.Count(x => x.UnDoState == BalanceSheetDetailStatus.Deduction
                                                  && x.UnDoDateTime > intervalStart
                                                  && x.UnDoDateTime < intervalEnd);
            var payAmount = (verifiedCount - deductCount) * cost;

            return payAmount;
        }


        #endregion 對帳查詢

        #region 商家公布欄

        /// <summary>
        /// 將公告物件轉換成PagerList以實現分頁
        /// </summary>
        private static PagerList<VbsBulletinBoardPostInfo> GetPagerBulletinBoardInfo(List<VbsBulletinBoardPostInfo> infos, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in infos select t);
            var paging = new PagerList<VbsBulletinBoardPostInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
            pageIndex,
            query.ToList().Count);

            return paging;
        }

        /// <summary>
        /// 將退貨申請轉換成PagerList以實現分頁
        /// </summary>
        private static PagerList<VbsBulletinBoardReturnInfo> GetPagerBulletinBoardReturnInfo(List<VbsBulletinBoardReturnInfo> obj, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in obj select t);
            var paging = new PagerList<VbsBulletinBoardReturnInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
            pageIndex,
            query.ToList().Count);

            return paging;
        }

        /// <summary>
        /// 將對帳單明細轉換成PagerList以實現分頁
        /// </summary>
        private static PagerList<VbsBulletinBoardBalanceSheetInfo> GetPagerBulletinBoardBalanceSheetInfo(List<VbsBulletinBoardBalanceSheetInfo> obj, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in obj select t);
            var paging = new PagerList<VbsBulletinBoardBalanceSheetInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
            pageIndex,
            query.ToList().Count);

            return paging;
        }

        /// <summary>
        /// 將換貨申請轉換成PagerList以實現分頁
        /// </summary>
        private static PagerList<VbsBulletinBoardExchangeInfo> GetPagerBulletinBoardExchangeInfo(List<VbsBulletinBoardExchangeInfo> obj, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in obj select t);
            var paging = new PagerList<VbsBulletinBoardExchangeInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
            pageIndex,
            query.ToList().Count);

            return paging;
        }

        #endregion


        private static Dictionary<Guid, Guid> GetComboSubDealsInfo(IEnumerable<Guid> dealGuids)
        {
            return _pp.GetComboDealByBid(dealGuids)
                        .Where(x => x.BusinessHourGuid != x.MainBusinessHourGuid)
                        .GroupBy(x => new { x.BusinessHourGuid, x.MainBusinessHourGuid })
                        .ToDictionary(x => x.Key.BusinessHourGuid, x => x.Key.MainBusinessHourGuid.Value);
        }

        private bool IsTemporaryTimeout()
        {
            bool flag = false;
            bool isTemporaryLogin = false;
            DateTime TemporaryLoginDate = DateTime.MinValue;

            if (Session[VbsSession.IsTemporaryLogin.ToString()] != null)
            {
                bool.TryParse(Session[VbsSession.IsTemporaryLogin.ToString()].ToString(), out isTemporaryLogin);
            }
            if (Session[VbsSession.TemporaryLoginDate.ToString()] != null)
            {
                DateTime.TryParse(Session[VbsSession.TemporaryLoginDate.ToString()].ToString(), out TemporaryLoginDate);
            }
            TimeSpan ts = DateTime.Now - TemporaryLoginDate;
            if (isTemporaryLogin)
            {
                if (ts.TotalMinutes > config.VbsTemporaryPasswordHourLimit)
                {
                    flag = true;
                }
            }

            return flag;
        }

        /// <summary>
        /// 商家系統 - 匯出對帳明細
        /// </summary>
        /// <param name="bsthModel">對帳明細Model</param>
        /// <param name="thplModel">請款明細Model</param>
        /// <returns></returns>
        private MemoryStream PayInfoExportToExcel(BalanceSheetToHouseModel bsthModel, ToHousePayListModel thplModel)
        {
            //頁簽
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("對帳明細");

            #region style
            //字形
            NPOI.SS.UserModel.Font fontForB = workbook.CreateFont();
            fontForB.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            fontForB.FontName = "微軟正黑體";
            fontForB.FontHeightInPoints = (short)10;

            NPOI.SS.UserModel.Font font = workbook.CreateFont();
            font.FontName = "微軟正黑體";
            font.FontHeightInPoints = (short)10;

            //cell style
            HSSFCellStyle headerStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            headerStyle.SetFont(fontForB);
            headerStyle.BorderBottom = CellBorderType.THIN;
            headerStyle.BorderLeft = CellBorderType.THIN;
            headerStyle.BorderRight = CellBorderType.THIN;
            headerStyle.BorderTop = CellBorderType.THIN;
            headerStyle.BackgroundColor(HSSFColor.TAN.index);
            headerStyle.VerticalAlignment = VerticalAlignment.CENTER;
            headerStyle.Alignment = HorizontalAlignment.LEFT;

            HSSFCellStyle contentStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            contentStyle.SetFont(font);
            contentStyle.BorderBottom = CellBorderType.THIN;
            contentStyle.BorderLeft = CellBorderType.THIN;
            contentStyle.BorderRight = CellBorderType.THIN;
            contentStyle.BorderTop = CellBorderType.THIN;
            contentStyle.VerticalAlignment = VerticalAlignment.CENTER;
            contentStyle.Alignment = HorizontalAlignment.LEFT;
            contentStyle.WrapText = true;

            HSSFCellStyle moneyStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            moneyStyle.SetFont(font);
            moneyStyle.BorderBottom = CellBorderType.THIN;
            moneyStyle.BorderLeft = CellBorderType.THIN;
            moneyStyle.BorderRight = CellBorderType.THIN;
            moneyStyle.BorderTop = CellBorderType.THIN;
            moneyStyle.VerticalAlignment = VerticalAlignment.CENTER;
            moneyStyle.Alignment = HorizontalAlignment.RIGHT;
            moneyStyle.WrapText = true;

            HSSFCellStyle moneyColorStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            moneyColorStyle.SetFont(font);
            moneyColorStyle.BorderBottom = CellBorderType.THIN;
            moneyColorStyle.BorderLeft = CellBorderType.THIN;
            moneyColorStyle.BorderRight = CellBorderType.THIN;
            moneyColorStyle.BorderTop = CellBorderType.THIN;
            moneyColorStyle.BackgroundColor(HSSFColor.TAN.index);
            moneyColorStyle.VerticalAlignment = VerticalAlignment.CENTER;
            moneyColorStyle.Alignment = HorizontalAlignment.RIGHT;
            moneyColorStyle.WrapText = true;

            HSSFCellStyle contentStyleNoBorder = (HSSFCellStyle)workbook.CreateCellStyle();
            contentStyleNoBorder.SetFont(font);
            contentStyleNoBorder.VerticalAlignment = VerticalAlignment.CENTER;
            contentStyleNoBorder.Alignment = HorizontalAlignment.LEFT;
            contentStyle.WrapText = true;

            HSSFCellStyle subTotalStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            subTotalStyle.SetFont(fontForB);
            subTotalStyle.BorderBottom = CellBorderType.THIN;
            subTotalStyle.BorderLeft = CellBorderType.THIN;
            subTotalStyle.BorderRight = CellBorderType.THIN;
            subTotalStyle.BorderTop = CellBorderType.DOUBLE;
            subTotalStyle.VerticalAlignment = VerticalAlignment.CENTER;
            subTotalStyle.Alignment = HorizontalAlignment.LEFT;

            HSSFCellStyle totalStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            totalStyle.SetFont(fontForB);
            totalStyle.BorderBottom = CellBorderType.DOUBLE;
            totalStyle.BorderLeft = CellBorderType.THIN;
            totalStyle.BorderRight = CellBorderType.THIN;
            totalStyle.BorderTop = CellBorderType.DOUBLE;
            totalStyle.BackgroundColor(HSSFColor.TAN.index);
            totalStyle.VerticalAlignment = VerticalAlignment.CENTER;
            totalStyle.Alignment = HorizontalAlignment.LEFT;
            #endregion

            int idx = 0;
            int intervalIdx = 2; //間格距離

            #region 對帳明細區塊

            #region header
            Row colsHeader = sheet.CreateRow(idx);
            colsHeader.CreateCell(0).SetCellValue("對帳總表");
            SetCellStyle(colsHeader, 2, headerStyle, true);
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 2));
            idx += 1;

            Row colsHeaderColumns = sheet.CreateRow(idx);
            colsHeaderColumns.CreateCell(0).SetCellValue("方案");
            colsHeaderColumns.CreateCell(1).SetCellValue("項目");
            colsHeaderColumns.CreateCell(2).SetCellValue("金額");
            SetCellStyle(colsHeaderColumns, 2, contentStyle);
            
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in bsthModel.DealInfos)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(string.Format("[{0}]{1}", deal.DealUniqueId, deal.DealName));
                row.CreateCell(1).SetCellValue(string.Empty);
                row.CreateCell(2).SetCellValue(string.Empty);
                SetCellStyle(row, 2, contentStyle);
                idx += 1;

                Row payRow = sheet.CreateRow(idx);
                payRow.CreateCell(0).SetCellValue(string.Empty);
                payRow.CreateCell(1).SetCellValue("應付貨款，份數：" + deal.PayCount + " 份");
                payRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", deal.PayAmount));
                SetCellStyle(payRow, 2, contentStyle);
                payRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                Row FreightRow = sheet.CreateRow(idx);
                FreightRow.CreateCell(0).SetCellValue(string.Empty);
                FreightRow.CreateCell(1).SetCellValue("應付運費，份數：" + deal.PayFreightCount + " 份");
                FreightRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", deal.PayFreightAmount));
                SetCellStyle(FreightRow, 2, contentStyle);
                FreightRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                Row deductRow = sheet.CreateRow(idx);
                deductRow.CreateCell(0).SetCellValue(string.Empty);
                deductRow.CreateCell(1).SetCellValue("退貨(含運費)");
                deductRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", deal.DeductAmount + deal.DeductFreightAmount));
                SetCellStyle(deductRow, 2, contentStyle);
                deductRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                

                if (deal.ISPFamilyOrderBeforeCount.Any())
                {
                    List<int> ispFamilyBeforeFreightsList = config.ISPFamilyBeforeFreights.Split(",").ToList().Select(freight => Int32.Parse(freight)).ToList();
                    for (int i = 0; i < deal.ISPFamilyOrderBeforeCount.Count; i++)
                    {
                        Row familyBeforeRow = sheet.CreateRow(idx);
                        familyBeforeRow.CreateCell(0).SetCellValue(string.Empty);
                        familyBeforeRow.CreateCell(1).SetCellValue("物流處理費(全家)-" + ispFamilyBeforeFreightsList[i] + "元/份，份數：" + deal.ISPFamilyOrderBeforeCount[i] + "份");
                        familyBeforeRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", deal.ISPFamilyOrderBeforeCount[i] * ispFamilyBeforeFreightsList[i]));
                        SetCellStyle(familyBeforeRow, 2, contentStyle);
                        familyBeforeRow.GetCell(2).CellStyle = moneyStyle;

                        idx += 1;
                    }
                }

                Row familyNowRow = sheet.CreateRow(idx);
                familyNowRow.CreateCell(0).SetCellValue(string.Empty);
                familyNowRow.CreateCell(1).SetCellValue("物流處理費(全家)-" + config.ISPFamilyNowFreights + "元/份，份數：" + deal.ISPFamilyOrderNowCount + "份");
                familyNowRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", config.ISPFamilyNowFreights * deal.ISPFamilyOrderNowCount));
                SetCellStyle(familyNowRow, 2, contentStyle);
                familyNowRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                if (deal.ISPSevenOrderBeforeCount.Any())
                {
                    List<int> ispSevenBeforeFreightsList = config.ISPSevenBeforeFreights.Split(",").ToList().Select(freight => Int32.Parse(freight)).ToList();
                    for (int i = 0; i < deal.ISPSevenOrderBeforeCount.Count; i++)
                    {
                        Row sevenBeforeRow = sheet.CreateRow(idx);
                        sevenBeforeRow.CreateCell(0).SetCellValue(string.Empty);
                        sevenBeforeRow.CreateCell(1).SetCellValue("物流處理費(7-11)-" + ispSevenBeforeFreightsList[i] + "元/份，份數：" + deal.ISPSevenOrderBeforeCount[i] + "份");
                        sevenBeforeRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", deal.ISPSevenOrderBeforeCount[i] * ispSevenBeforeFreightsList[i]));
                        SetCellStyle(sevenBeforeRow, 2, contentStyle);
                        sevenBeforeRow.GetCell(2).CellStyle = moneyStyle;

                        idx += 1;
                    }
                }

                Row sevenNowRow = sheet.CreateRow(idx);
                sevenNowRow.CreateCell(0).SetCellValue(string.Empty);
                sevenNowRow.CreateCell(1).SetCellValue("物流處理費(7-11)-" + config.ISPSevenNowFreights + "元/份，份數：" + deal.ISPSevenOrderNowCount + "份");
                sevenNowRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", config.ISPSevenNowFreights * deal.ISPSevenOrderNowCount));
                SetCellStyle(sevenNowRow, 2, contentStyle);
                sevenNowRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                Row wmsOrderRow = sheet.CreateRow(idx);
                wmsOrderRow.CreateCell(0).SetCellValue(string.Empty);
                wmsOrderRow.CreateCell(1).SetCellValue("物流處理費(PCHOME)");
                wmsOrderRow.CreateCell(2).SetCellValue(string.Format("{0:(-#,###0)}", (deal.WmsOrderAmount)));
                SetCellStyle(wmsOrderRow, 2, contentStyle);
                wmsOrderRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;

                Row positivePaymentOverdueRow = sheet.CreateRow(idx);
                positivePaymentOverdueRow.CreateCell(0).SetCellValue(string.Empty);
                positivePaymentOverdueRow.CreateCell(1).SetCellValue("逾期出貨補款");
                positivePaymentOverdueRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", (deal.PositivePaymentOverdueAmount)));
                SetCellStyle(positivePaymentOverdueRow, 2, contentStyle);
                positivePaymentOverdueRow.GetCell(2).CellStyle = moneyStyle;
                idx += 1;

                Row negativePaymentOverdueRow = sheet.CreateRow(idx);
                negativePaymentOverdueRow.CreateCell(0).SetCellValue(string.Empty);
                negativePaymentOverdueRow.CreateCell(1).SetCellValue("逾期出貨扣款");
                negativePaymentOverdueRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", (deal.NegativePaymentOverdueAmount)));
                SetCellStyle(negativePaymentOverdueRow, 2, contentStyle);
                negativePaymentOverdueRow.GetCell(2).CellStyle = moneyStyle;
                idx += 1;

                Row changeRow = sheet.CreateRow(idx);
                changeRow.CreateCell(0).SetCellValue(string.Empty);
                changeRow.CreateCell(1).SetCellValue("異動調整金額");
                if (deal.PaymentChangeAmount < 0)
                    changeRow.CreateCell(2).SetCellValue((deal.PaymentChangeAmount.ToString("#,###0")));
                else
                    changeRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", deal.PaymentChangeAmount));

                SetCellStyle(changeRow, 2, contentStyle);
                changeRow.GetCell(2).CellStyle = moneyStyle;

                idx += 1;


                //Row subTotalRow = sheet.CreateRow(idx);
                //subTotalRow.CreateCell(0).SetCellValue(string.Empty);
                //subTotalRow.CreateCell(1).SetCellValue("小計");
                //subTotalRow.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", deal.EstAmount));
                //SetCellStyle(subTotalRow, 2, subTotalStyle);
                //subTotalRow.GetCell(2).CellStyle = moneyStyle;

                //idx += 1;

            }
            #endregion

            #region footer
            Row colsTotal = sheet.CreateRow(idx);
            colsTotal.CreateCell(0).SetCellValue("本期實際付款金額");
            colsTotal.CreateCell(1).SetCellValue(string.Empty);
            colsTotal.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", bsthModel.EstAmount));

            SetCellStyle(colsTotal, 2, totalStyle);
            colsTotal.GetCell(2).CellStyle = moneyColorStyle;
            idx += 1;

            Row colsEnvoiceTotal = sheet.CreateRow(idx);
            colsEnvoiceTotal.CreateCell(0).SetCellValue("本期發票開立金額總計");
            colsEnvoiceTotal.CreateCell(1).SetCellValue(string.Empty);
            if (bsthModel.EstAmount >= 0 || bsthModel.ReceiptAmount >= 0 || bsthModel.PositivePaymentOverdueAmount > 0)
            {
                if (!bsthModel.IsTax && (!bsthModel.FreightAmount.Equals(0) || bsthModel.PositivePaymentOverdueAmount > 0))
                {
                    colsEnvoiceTotal.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", bsthModel.ReceiptAmount + bsthModel.FreightAmount + bsthModel.PositivePaymentOverdueAmount));
                }
                else
                {
                    colsEnvoiceTotal.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", bsthModel.ReceiptAmount));
                }
            }
            else
            {
                colsEnvoiceTotal.CreateCell(2).SetCellValue(string.Format("{0:#,###0}", 0));
            }
            SetCellStyle(colsEnvoiceTotal, 2, totalStyle);
            colsEnvoiceTotal.GetCell(2).CellStyle = moneyColorStyle;
            idx += 1;
            #endregion

            #endregion

            idx += intervalIdx;

            #region 請款明細區塊

            #region 總資訊
            Row info1 = sheet.CreateRow(idx);
            info1.CreateCell(0).SetCellValue(thplModel.BalanceSheetDesc);
            SetCellStyle(info1, 0, headerStyle);
            idx += 1;

            Row info2 = sheet.CreateRow(idx);
            info2.CreateCell(0).SetCellValue(string.Format("最後結算時間：{0}", thplModel.IntervalEnd.ToString("yyyy/MM/dd")));
            SetCellStyle(info2, 0, contentStyleNoBorder);
            idx += 1;

            Row info3 = sheet.CreateRow(idx);
            info3.CreateCell(0).SetCellValue(String.Format("退貨金額：{0}", thplModel.DeductAmount));
            SetCellStyle(info3, 0, contentStyleNoBorder);
            idx += 1;

            Row info4 = sheet.CreateRow(idx);
            info4.CreateCell(0).SetCellValue(String.Format("異動金額：{0}", thplModel.PaymentChangeAmount));
            SetCellStyle(info4, 0, contentStyleNoBorder);
            idx += 1;

            if (thplModel.IsTransferComplete)
            {
                Row info5 = sheet.CreateRow(idx);
                info5.CreateCell(0).SetCellValue(String.Format("本月實付金額：{0}", thplModel.PaidAmount));
                SetCellStyle(info5, 0, contentStyleNoBorder);
                idx += 1;
            }
            else
            {
                string paymentDesc = (thplModel.EstAmount < 0) ? "溢付" : "應付";

                Row info6 = sheet.CreateRow(idx);
                info6.CreateCell(0).SetCellValue(string.Format("本次{0}金額：{1}", paymentDesc, thplModel.EstAmount));

                SetCellStyle(info6, 0, contentStyleNoBorder);
                idx += 1;
            }
            #endregion

            idx += intervalIdx;

            #region 請款明細
            #region header
            Row payHeader = sheet.CreateRow(idx);
            payHeader.CreateCell(0).SetCellValue("請款明細");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 11));
            SetCellStyle(payHeader, 11, headerStyle, true);
            idx += 1;

            Row payHeaderColumns = sheet.CreateRow(idx);
            payHeaderColumns.CreateCell(0).SetCellValue("購買日期");
            payHeaderColumns.CreateCell(1).SetCellValue("檔號");
            payHeaderColumns.CreateCell(2).SetCellValue("訂單編號");
            payHeaderColumns.CreateCell(3).SetCellValue("方案名稱");
            payHeaderColumns.CreateCell(4).SetCellValue("方案組數");
            payHeaderColumns.CreateCell(5).SetCellValue("出貨日期");
            payHeaderColumns.CreateCell(6).SetCellValue("稅前進價");
            payHeaderColumns.CreateCell(7).SetCellValue("稅額");
            payHeaderColumns.CreateCell(8).SetCellValue("稅後進價");
            payHeaderColumns.CreateCell(9).SetCellValue("運費(含稅)");
            payHeaderColumns.CreateCell(10).SetCellValue("品項規格");
            payHeaderColumns.CreateCell(11).SetCellValue("貨號");
            SetCellStyle(payHeaderColumns, 11, contentStyle);
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in thplModel.DealInfos)
            {
                string orderId = string.Empty;
                foreach (var info in deal.PayInfos.OrderBy(x => x.OrderCreateTime).ThenBy(x => x.OrderId))
                {
                    //寫一筆方案資訊
                    if (orderId != info.OrderId)
                    {
                        Row mainRow = sheet.CreateRow(idx);
                        mainRow.CreateCell(0).SetCellValue(info.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                        mainRow.CreateCell(1).SetCellValue(deal.DealUniqueId);
                        mainRow.CreateCell(2).SetCellValue(info.OrderId);
                        mainRow.CreateCell(3).SetCellValue(deal.DealName);
                        mainRow.CreateCell(4).SetCellValue(info.Count);
                        mainRow.CreateCell(5).SetCellValue(info.ShipTime.ToString("yyyy/MM/dd"));
                        mainRow.CreateCell(6).SetCellValue(Convert.ToInt64(deal.ItemUnTexCost));
                        mainRow.CreateCell(7).SetCellValue(Convert.ToInt64(deal.ItemCostTex));
                        mainRow.CreateCell(8).SetCellValue(Convert.ToInt64(deal.ItemCost));
                        mainRow.CreateCell(9).SetCellValue(deal.FreightAmount);
                        mainRow.CreateCell(10).SetCellValue(string.Empty);
                        mainRow.CreateCell(11).SetCellValue(string.Empty);
                        SetCellStyle(mainRow, 11, contentStyle);

                        mainRow.GetCell(6).CellStyle = moneyStyle;
                        mainRow.GetCell(7).CellStyle = moneyStyle;
                        mainRow.GetCell(8).CellStyle = moneyStyle;
                        mainRow.GetCell(9).CellStyle = moneyStyle;
                        
                        orderId = info.OrderId;
                        idx += 1;
                    }

                    //有規格才寫
                    if (!string.IsNullOrEmpty(info.ItemOption) || !string.IsNullOrEmpty(info.ItemNo))
                    {
                        Row row = sheet.CreateRow(idx);
                        row.CreateCell(0).SetCellValue(info.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                        row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                        row.CreateCell(2).SetCellValue(info.OrderId);
                        row.CreateCell(3).SetCellValue(deal.DealName);
                        row.CreateCell(4).SetCellValue(string.Empty);
                        row.CreateCell(5).SetCellValue(string.Empty);
                        row.CreateCell(6).SetCellValue(string.Empty);
                        row.CreateCell(7).SetCellValue(string.Empty);
                        row.CreateCell(8).SetCellValue(string.Empty);
                        row.CreateCell(9).SetCellValue(string.Empty);
                        row.CreateCell(10).SetCellValue(info.ItemOption);
                        row.CreateCell(11).SetCellValue(info.ItemNo);
                        SetCellStyle(row, 11, contentStyle);
                        idx += 1;
                    }
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 退貨清單
            #region header
            Row rtnHeader = sheet.CreateRow(idx);
            rtnHeader.CreateCell(0).SetCellValue("退貨清單");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
            SetCellStyle(rtnHeader, 4, headerStyle, true);
            idx += 1;

            Row rtnHeaderColumns = sheet.CreateRow(idx);
            rtnHeaderColumns.CreateCell(0).SetCellValue("檔號");
            rtnHeaderColumns.CreateCell(1).SetCellValue("訂單編號");
            rtnHeaderColumns.CreateCell(2).SetCellValue("方案名稱");
            rtnHeaderColumns.CreateCell(3).SetCellValue("退貨數");
            rtnHeaderColumns.CreateCell(4).SetCellValue("運費");
            SetCellStyle(rtnHeaderColumns, 4, contentStyle);
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.DeductInfos)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(deal.DealUniqueId);
                    row.CreateCell(1).SetCellValue(info.OrderId);
                    row.CreateCell(2).SetCellValue(deal.DealName);
                    row.CreateCell(3).SetCellValue(info.Count);
                    row.CreateCell(4).SetCellValue(deal.FreightAmount);
                    SetCellStyle(row, 4, contentStyle);

                    row.GetCell(4).CellStyle = moneyStyle;
                    idx += 1;
                }
            }
            #endregion

            #endregion

            idx += intervalIdx;

            #region 物流處理明細 (全家)
            
            if (thplModel.DealInfos.Sum(x => x.ISPFamilyBeforeInfo.Count()) > 0)
            {
                List<DateTime> ispFamilyFreightsChangeDateList = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.FamilyPickup);
                List<int> ispFamilyBeforeFreightsList = config.ISPFamilyBeforeFreights.Split(",").ToList().Select(freights => Int32.Parse(freights)).ToList();

                for (int i = 1; i < ispFamilyFreightsChangeDateList.Count; i++)
                {
                    #region header
                    Row familyBeforeHeader = sheet.CreateRow(idx);
                    familyBeforeHeader.CreateCell(0).SetCellValue("舊物流處理明細 (全家) " + ispFamilyBeforeFreightsList[i - 1] + "元/筆");
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
                    SetCellStyle(familyBeforeHeader, 4, headerStyle, true);
                    idx += 1;

                    Row familyBeforeHeaderColumns = sheet.CreateRow(idx);
                    familyBeforeHeaderColumns.CreateCell(0).SetCellValue("序號");
                    familyBeforeHeaderColumns.CreateCell(1).SetCellValue("檔號");
                    familyBeforeHeaderColumns.CreateCell(2).SetCellValue("訂單編號");
                    familyBeforeHeaderColumns.CreateCell(3).SetCellValue("方案名稱");
                    familyBeforeHeaderColumns.CreateCell(4).SetCellValue("實際入倉日");
                    SetCellStyle(familyBeforeHeaderColumns, 4, contentStyle);
                    idx += 1;

                    #endregion

                    #region detail
                    int familyBeforeCount = 1;
                    foreach (var deal in thplModel.DealInfos)
                    {

                        foreach (var info in deal.ISPFamilyBeforeInfo.Where(x => x.ShipToStockTime >= ispFamilyFreightsChangeDateList[i - 1] && x.ShipToStockTime < ispFamilyFreightsChangeDateList[i]))
                        {
                            Row row = sheet.CreateRow(idx);
                            row.CreateCell(0).SetCellValue(familyBeforeCount);
                            row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                            row.CreateCell(2).SetCellValue(info.OrderId);
                            row.CreateCell(3).SetCellValue(deal.DealName);
                            row.CreateCell(4).SetCellValue(info.ShipToStockTime.ToString("yyyy/MM/dd"));
                            SetCellStyle(row, 4, contentStyle);
                            idx += 1;
                            familyBeforeCount++;
                        }
                    }

                    #endregion
                }
            }



            #endregion

            idx += intervalIdx;

            #region 物流處理明細 (全家)
            #region header
            Row familyNowHeader = sheet.CreateRow(idx);
            familyNowHeader.CreateCell(0).SetCellValue("物流處理明細 (全家) " + config.ISPFamilyNowFreights + "元/筆");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
            SetCellStyle(familyNowHeader, 4, headerStyle, true);
            idx += 1;

            Row familyNowHeaderColumns = sheet.CreateRow(idx);
            familyNowHeaderColumns.CreateCell(0).SetCellValue("序號");
            familyNowHeaderColumns.CreateCell(1).SetCellValue("檔號");
            familyNowHeaderColumns.CreateCell(2).SetCellValue("訂單編號");
            familyNowHeaderColumns.CreateCell(3).SetCellValue("方案名稱");
            familyNowHeaderColumns.CreateCell(4).SetCellValue("實際入倉日");
            SetCellStyle(familyNowHeaderColumns, 4, contentStyle);
            idx += 1;
            #endregion

            #region detail

            int familyNowCount = 1;
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.ISPFamilyNowInfo)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(familyNowCount);
                    row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                    row.CreateCell(2).SetCellValue(info.OrderId);
                    row.CreateCell(3).SetCellValue(deal.DealName);
                    row.CreateCell(4).SetCellValue(info.ShipToStockTime.ToString("yyyy/MM/dd"));
                    SetCellStyle(row, 4, contentStyle);
                    idx += 1;
                    familyNowCount++;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 物流處理明細 (7-11)


            if (thplModel.DealInfos.Sum(x => x.ISPSevenBeforeInfo.Count()) > 0)
            {
                List<DateTime> ispSevenFreightsChangeDateList = BalanceSheetService.GetIspFreightsChangeDateList(ProductDeliveryType.SevenPickup);
                List<int> ispSevenBeforeFreightsList = config.ISPSevenBeforeFreights.Split(",").ToList().Select(freights => Int32.Parse(freights)).ToList();

                for (int i = 1; i < ispSevenFreightsChangeDateList.Count; i++)
                {
                    #region header
                    Row SevenBeforeHeader = sheet.CreateRow(idx);
                    SevenBeforeHeader.CreateCell(0).SetCellValue("舊物流處理明細 (7-11) " + ispSevenBeforeFreightsList[i - 1] + "元/筆");
                    sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
                    SetCellStyle(SevenBeforeHeader, 4, headerStyle, true);
                    idx += 1;

                    Row Before = sheet.CreateRow(idx);
                    Before.CreateCell(0).SetCellValue("序號");
                    Before.CreateCell(1).SetCellValue("檔號");
                    Before.CreateCell(2).SetCellValue("訂單編號");
                    Before.CreateCell(3).SetCellValue("方案名稱");
                    Before.CreateCell(4).SetCellValue("實際入倉日");
                    SetCellStyle(Before, 4, contentStyle);
                    idx += 1;

                    #endregion

                    #region detail
                    int SevenBeforeCount = 1;
                    foreach (var deal in thplModel.DealInfos)
                    {

                        foreach (var info in deal.ISPSevenBeforeInfo.Where(x => x.ShipToStockTime >= ispSevenFreightsChangeDateList[i - 1] && x.ShipToStockTime < ispSevenFreightsChangeDateList[i]))
                        {
                            Row row = sheet.CreateRow(idx);
                            row.CreateCell(0).SetCellValue(SevenBeforeCount);
                            row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                            row.CreateCell(2).SetCellValue(info.OrderId);
                            row.CreateCell(3).SetCellValue(deal.DealName);
                            row.CreateCell(4).SetCellValue(info.ShipToStockTime.ToString("yyyy/MM/dd"));
                            SetCellStyle(row, 4, contentStyle);
                            idx += 1;
                            SevenBeforeCount++;
                        }
                    }

                    #endregion
                }
            }



            #endregion

            idx += intervalIdx;

            #region 物流處理明細 (7-11)
            #region header
            Row sevenNowHeader = sheet.CreateRow(idx);
            sevenNowHeader.CreateCell(0).SetCellValue("物流處理明細 (7-11) " + config.ISPSevenNowFreights + "元/筆");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
            SetCellStyle(sevenNowHeader, 4, headerStyle, true);
            idx += 1;

            Row sevenNowHeaderColumns = sheet.CreateRow(idx);
            sevenNowHeaderColumns.CreateCell(0).SetCellValue("序號");
            sevenNowHeaderColumns.CreateCell(1).SetCellValue("檔號");
            sevenNowHeaderColumns.CreateCell(2).SetCellValue("訂單編號");
            sevenNowHeaderColumns.CreateCell(3).SetCellValue("方案名稱");
            sevenNowHeaderColumns.CreateCell(4).SetCellValue("實際入倉日");
            SetCellStyle(sevenNowHeaderColumns, 4, contentStyle);
            idx += 1;
            #endregion

            #region detail

            int sevenNowCount = 1;
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.ISPSevenNowInfo)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(sevenNowCount);
                    row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                    row.CreateCell(2).SetCellValue(info.OrderId);
                    row.CreateCell(3).SetCellValue(deal.DealName);
                    row.CreateCell(4).SetCellValue(info.ShipToStockTime.ToString("yyyy/MM/dd"));
                    SetCellStyle(row, 4, contentStyle);
                    idx += 1;
                    sevenNowCount++;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 物流處理明細(PCHOME)
            #region header
            Row wmsOrderHeader = sheet.CreateRow(idx);
            wmsOrderHeader.CreateCell(0).SetCellValue("物流處理明細(PCHOME)");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 9));
            SetCellStyle(wmsOrderHeader, 9, headerStyle, true);
            idx += 1;

            Row wmsOrderHeaderColumns = sheet.CreateRow(idx);
            wmsOrderHeaderColumns.CreateCell(0).SetCellValue("序號");
            wmsOrderHeaderColumns.CreateCell(1).SetCellValue("檔號");
            wmsOrderHeaderColumns.CreateCell(2).SetCellValue("訂單編號");
            wmsOrderHeaderColumns.CreateCell(3).SetCellValue("方案名稱");
            wmsOrderHeaderColumns.CreateCell(4).SetCellValue("實際出貨日");
            wmsOrderHeaderColumns.CreateCell(5).SetCellValue("包材費");
            wmsOrderHeaderColumns.CreateCell(6).SetCellValue("運費");
            wmsOrderHeaderColumns.CreateCell(7).SetCellValue("退貨運費");
            wmsOrderHeaderColumns.CreateCell(8).SetCellValue("出貨作業處理費");
            wmsOrderHeaderColumns.CreateCell(9).SetCellValue("總計");
            SetCellStyle(wmsOrderHeaderColumns, 9, contentStyle);
            idx += 1;
            #endregion

            #region detail
            int wmsOrderCount = 1;
            foreach (var deal in thplModel.DealInfos)
            {
                for (int i = 0; i < deal.WmsOrderInfo.Rows.Count; i++)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(wmsOrderCount);
                    row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                    row.CreateCell(2).SetCellValue(deal.WmsOrderInfo.Rows[i]["OrderId"].ToString());
                    row.CreateCell(3).SetCellValue(deal.DealName);
                    row.CreateCell(4).SetCellValue(deal.WmsOrderInfo.Rows[i]["ShipTime"].ToString());
                    row.CreateCell(5).SetCellValue(deal.WmsOrderInfo.Rows[i]["PackFee"].ToString());
                    row.CreateCell(6).SetCellValue(deal.WmsOrderInfo.Rows[i]["ShipFee"].ToString());
                    row.CreateCell(7).SetCellValue(deal.WmsOrderInfo.Rows[i]["RefundShipFee"].ToString());
                    row.CreateCell(8).SetCellValue(deal.WmsOrderInfo.Rows[i]["OrderServFee"].ToString());
                    row.CreateCell(9).SetCellValue(deal.WmsOrderInfo.Rows[i]["TotalFee"].ToString());
                    SetCellStyle(row, 9, contentStyle);
                    idx += 1;
                    wmsOrderCount++;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 逾期出貨補款明細
            #region header
            Row positiveOverdueHeader = sheet.CreateRow(idx);
            positiveOverdueHeader.CreateCell(0).SetCellValue("逾期出貨補款明細");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 6));
            SetCellStyle(positiveOverdueHeader, 6, headerStyle, true);
            idx += 1;

            Row positiveOverdueHeaderColumns = sheet.CreateRow(idx);
            positiveOverdueHeaderColumns.CreateCell(0).SetCellValue("訂單編號");
            positiveOverdueHeaderColumns.CreateCell(1).SetCellValue("訂單類型");
            positiveOverdueHeaderColumns.CreateCell(2).SetCellValue("預計出貨日");
            positiveOverdueHeaderColumns.CreateCell(3).SetCellValue("上傳出貨日");
            positiveOverdueHeaderColumns.CreateCell(4).SetCellValue("售價");
            positiveOverdueHeaderColumns.CreateCell(5).SetCellValue("罰款/異動金額");
            positiveOverdueHeaderColumns.CreateCell(6).SetCellValue("異動原因");
            SetCellStyle(positiveOverdueHeaderColumns, 6, contentStyle);
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.PositiveOverdueInfos)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(info.Orderid);
                    row.CreateCell(1).SetCellValue(info.ShipType);
                    row.CreateCell(2).SetCellValue(info.LashShipDate == null ? "" : info.LashShipDate.Value.ToString("yyyy/MM/dd"));
                    row.CreateCell(3).SetCellValue(info.CreateTime == null ? "" : info.CreateTime.Value.ToString("yyyy/MM/dd"));
                    row.CreateCell(4).SetCellValue(info.ItemPrice);
                    row.CreateCell(5).SetCellValue(info.Amount);
                    row.CreateCell(6).SetCellValue(info.Reason);
                    SetCellStyle(row, 6, contentStyle);
                    idx += 1;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 逾期出貨罰款明細
            #region header
            Row negativeOverdueHeader = sheet.CreateRow(idx);
            negativeOverdueHeader.CreateCell(0).SetCellValue("逾期出貨罰款明細");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 6));
            SetCellStyle(negativeOverdueHeader, 6, headerStyle, true);
            idx += 1;

            Row negativeOverdueHeaderColumns = sheet.CreateRow(idx);
            negativeOverdueHeaderColumns.CreateCell(0).SetCellValue("訂單編號");
            negativeOverdueHeaderColumns.CreateCell(1).SetCellValue("訂單類型");
            negativeOverdueHeaderColumns.CreateCell(2).SetCellValue("預計出貨日");
            negativeOverdueHeaderColumns.CreateCell(3).SetCellValue("上傳出貨日");
            negativeOverdueHeaderColumns.CreateCell(4).SetCellValue("售價");
            negativeOverdueHeaderColumns.CreateCell(5).SetCellValue("罰款/異動金額");
            negativeOverdueHeaderColumns.CreateCell(6).SetCellValue("異動原因");
            SetCellStyle(negativeOverdueHeaderColumns, 6, contentStyle);
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.NegativeOverdueInfos)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(info.Orderid);
                    row.CreateCell(1).SetCellValue(info.ShipType);
                    row.CreateCell(2).SetCellValue(info.LashShipDate == null ? "" : info.LashShipDate.Value.ToString("yyyy/MM/dd"));
                    row.CreateCell(3).SetCellValue(info.CreateTime == null ? "" : info.CreateTime.Value.ToString("yyyy/MM/dd"));
                    row.CreateCell(4).SetCellValue(info.ItemPrice);
                    row.CreateCell(5).SetCellValue(info.Amount);
                    row.CreateCell(6).SetCellValue(info.Reason);
                    SetCellStyle(row, 6, contentStyle);
                    idx += 1;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            #region 異動金額
            #region header
            Row changHeader = sheet.CreateRow(idx);
            changHeader.CreateCell(0).SetCellValue("異動金額");
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 4));
            SetCellStyle(changHeader, 4, headerStyle, true);
            idx += 1;

            Row changHeaderColumns = sheet.CreateRow(idx);
            changHeaderColumns.CreateCell(0).SetCellValue("異動日期");
            changHeaderColumns.CreateCell(1).SetCellValue("檔號");
            changHeaderColumns.CreateCell(2).SetCellValue("方案名稱");
            changHeaderColumns.CreateCell(3).SetCellValue("異動金額");
            changHeaderColumns.CreateCell(4).SetCellValue("內容");
            SetCellStyle(changHeaderColumns, 4, contentStyle);
            idx += 1;
            #endregion

            #region detail
            foreach (var deal in thplModel.DealInfos)
            {
                foreach (var info in deal.ChangeInfos)
                {
                    Row row = sheet.CreateRow(idx);
                    row.CreateCell(0).SetCellValue(info.CreateTime.ToString("yyyy/MM/dd"));
                    row.CreateCell(1).SetCellValue(deal.DealUniqueId);
                    row.CreateCell(2).SetCellValue(deal.DealName);
                    row.CreateCell(3).SetCellValue(info.Amount);
                    row.CreateCell(4).SetCellValue(info.Reason);
                    SetCellStyle(row, 4, contentStyle);

                    row.GetCell(3).CellStyle = moneyStyle;
                    idx += 1;
                }
            }
            #endregion
            #endregion

            idx += intervalIdx;

            
            #endregion

            //設置寬度為autosize
            for (int i = 0; i <= 11; i++)
            {
                sheet.AutoSizeColumn(i, true);
            }

            //設置一下高度讓wordwrap會自動延展
            sheet.GetRow(1).HeightInPoints = sheet.DefaultRowHeightInPoints * 1;

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 商家系統 - 匯出總明細(憑證)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private MemoryStream BalanceSheetPackExportToShopToExcel(BalanceSheetPackModel model)
        {
            var groupByDeal = !model.GroupByStore;
            var models = groupByDeal
                    ? model.BalanceSheetPackInfos.Where(x => x.RemittanceType == (int)RemittanceType.Weekly ||
                                                                    x.RemittanceType == (int)RemittanceType.Monthly ||
                                                                    x.RemittanceType == (int)RemittanceType.Flexible)
                                                .GroupBy(x => x.DealId)
                                                .ToDictionary(x => x.Key, x => x.ToList())
                    : model.BalanceSheetPackInfos.Where(x => x.RemittanceType == (int)RemittanceType.Weekly ||
                                                                    x.RemittanceType == (int)RemittanceType.Monthly ||
                                                                    x.RemittanceType == (int)RemittanceType.Flexible)
                                                .Where(x => x.StoreGuid.HasValue)
                                                .GroupBy(x => x.StoreGuid.Value)
                                                .ToDictionary(x => x.Key, x => x.ToList());


            //頁簽
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("總明細");

            NPOI.SS.UserModel.Font fontForB = workbook.CreateFont();
            fontForB.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            HSSFCellStyle fontBoldStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            fontBoldStyle.SetFont(fontForB);

            HSSFCellStyle fontBoldStyleForNum = (HSSFCellStyle)workbook.CreateCellStyle();
            fontBoldStyleForNum.Alignment = HorizontalAlignment.RIGHT;
            fontBoldStyleForNum.SetFont(fontForB);

            HSSFCellStyle flooterStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            flooterStyle.VerticalAlignment = VerticalAlignment.CENTER;
            flooterStyle.WrapText = true;

            HSSFCellStyle numStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            numStyle.Alignment = HorizontalAlignment.RIGHT;

            //header
            int idx = 0;
            Row colsHeader = sheet.CreateRow(idx);
            colsHeader.CreateCell(0).SetCellValue(groupByDeal ? "檔次" : "分店");
            colsHeader.CreateCell(1).SetCellValue(groupByDeal ? "分店" : "檔次");
            colsHeader.CreateCell(2).SetCellValue("對帳單金額");
            idx += 1;

            var invoiceAmount = 0;
            var receiptAmount = 0;
            var invoiceAmountTaxFree = 0;

            #region Detail
            foreach (var dealId in models.Keys)
            {
                var seq = 1;
                var bsCount = models[dealId].Count;
                var totalAmount = 0;
                foreach (var m in models[dealId])
                {
                    Row row = sheet.CreateRow(idx);

                    if (seq == 1)
                    {
                        row.CreateCell(0).SetCellValue(groupByDeal ? string.Format("[{0}]{1}", m.DealUniqueId, m.DealName) : m.StoreName);
                        row.GetCell(0).CellStyle = flooterStyle;
                    }

                    row.CreateCell(1).SetCellValue(groupByDeal ? m.StoreName : string.Format("[{0}]{1}", m.DealUniqueId, m.DealName));
                    row.CreateCell(2).SetCellValue(string.Format("{0:$#,0}", m.EstAmount));
                    row.GetCell(2).CellStyle = numStyle;

                    if (m.EstAmount < 0)
                    {
                        row.GetCell(1).CellStyle = fontBoldStyle;
                        row.GetCell(2).CellStyle = fontBoldStyleForNum;
                    }

                    totalAmount += m.EstAmount;
                    switch (m.VendorReceiptType)
                    {
                        case VendorReceiptType.Other:
                        case VendorReceiptType.Invoice:
                            if (m.IsTax)
                            {
                                invoiceAmount += m.EstAmount;
                            }
                            else
                            {
                                invoiceAmountTaxFree += m.EstAmount;
                            }

                            break;
                        case VendorReceiptType.Receipt:
                            receiptAmount += m.EstAmount;
                            break;
                    }
                    idx += 1;

                    if (seq == bsCount)
                    {
                        Row subTotal = sheet.CreateRow(idx);
                        subTotal.CreateCell(0).SetCellValue(string.Empty);
                        subTotal.CreateCell(1).SetCellValue("合計");
                        subTotal.CreateCell(2).SetCellValue(string.Format("{0:$#,0}", totalAmount));
                        subTotal.GetCell(0).CellStyle = flooterStyle;
                        subTotal.GetCell(2).CellStyle = numStyle;
                        sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx - bsCount, idx, 0, 0));
                        idx += 1;
                    }
                }
            }

            #endregion

            //footer
            StringBuilder sbFooter = new StringBuilder();
            sbFooter.AppendLine("抬頭：康太數位整合股份有限公司");
            sbFooter.AppendLine("統編：24317014");
            sbFooter.AppendLine("單據發票寄件地址：10441台北市中山區中山北路一段11號13樓 財務部 收");
            sbFooter.AppendLine();
            sbFooter.AppendLine(string.Format("含稅發票總金額：{0:$#,0}", invoiceAmount));
            sbFooter.AppendLine(string.Format("免稅發票總金額：{0:$#,0}", invoiceAmountTaxFree));
            sbFooter.Append(string.Format("收據總金額：{0:$#,0}", receiptAmount));

            Row footer = sheet.CreateRow(idx);
            footer.CreateCell(0).SetCellValue(sbFooter.ToString());
            footer.CreateCell(1).SetCellValue(string.Empty);
            footer.CreateCell(2).SetCellValue(string.Empty);
            footer.GetCell(0).CellStyle = flooterStyle;
            footer.GetCell(1).CellStyle = flooterStyle;
            footer.GetCell(2).CellStyle = flooterStyle;
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 2));
            footer.HeightInPoints = 80.25f;

            //設置寬度為autosize
            for (int i = 0; i <= 2; i++)
            {
                sheet.AutoSizeColumn(i, true);
            }

            sheet.SetColumnWidth(0, 18000);
            sheet.SetColumnWidth(1, 18000);
            sheet.SetColumnWidth(2, 3300);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 商家系統 - 匯出總明細(宅配)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private MemoryStream BalanceSheetPackExportToHouseToExcel(BalanceSheetPackModel model)
        {
            //頁簽
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("總明細");

            //style
            NPOI.SS.UserModel.Font fontForB = workbook.CreateFont();
            fontForB.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;

            HSSFCellStyle HeaderLifeStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HeaderLifeStyle.BackgroundColor(HSSFColor.LIGHT_TURQUOISE.index);

            HSSFCellStyle HeaderVendorStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HeaderVendorStyle.BackgroundColor(HSSFColor.ROSE.index);

            HSSFCellStyle HeaderPaymentStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HeaderPaymentStyle.BackgroundColor(HSSFColor.LIGHT_GREEN.index);

            HSSFCellStyle fontBoldStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            fontBoldStyle.SetFont(fontForB);

            HSSFCellStyle fontBoldStyleForNum = (HSSFCellStyle)workbook.CreateCellStyle();
            fontBoldStyleForNum.Alignment = HorizontalAlignment.RIGHT;
            fontBoldStyleForNum.SetFont(fontForB);

            HSSFCellStyle flooterStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            flooterStyle.VerticalAlignment = VerticalAlignment.CENTER;
            flooterStyle.WrapText = true;

            HSSFCellStyle numStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            numStyle.Alignment = HorizontalAlignment.RIGHT;

            int idx = 0;

            //header
            Row colsHeader = sheet.CreateRow(idx);
            colsHeader.CreateCell(0).SetCellValue("");
            colsHeader.CreateCell(1).SetCellValue("A.廠商發票開立項目與金額明細");
            colsHeader.CreateCell(8).SetCellValue("B.17Life開立代扣發票項目與金額明細");
            colsHeader.CreateCell(14).SetCellValue("C.匯款金額");
            colsHeader.GetCell(1).CellStyle = HeaderLifeStyle;
            colsHeader.GetCell(8).CellStyle = HeaderVendorStyle;
            colsHeader.GetCell(14).CellStyle = HeaderPaymentStyle;
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 1, 7));
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 8, 13));
            idx += 1;

            Row colsHeaderSub = sheet.CreateRow(idx);
            colsHeaderSub.CreateCell(0).SetCellValue("檔次");
            colsHeaderSub.CreateCell(1).SetCellValue("應付貨款");
            colsHeaderSub.CreateCell(2).SetCellValue("應付運費");
            colsHeaderSub.CreateCell(3).SetCellValue("退貨(含運費)");
            colsHeaderSub.CreateCell(4).SetCellValue("異動調整金額");
            colsHeaderSub.CreateCell(5).SetCellValue("貨款小計(A1)");
            colsHeaderSub.CreateCell(6).SetCellValue("逾期出貨補款小計(A2)");
            colsHeaderSub.CreateCell(7).SetCellValue("發票開立總計(A)");
            colsHeaderSub.CreateCell(8).SetCellValue("物流處理費(全家)");
            colsHeaderSub.CreateCell(9).SetCellValue("物流處理費(7-11)");
            colsHeaderSub.CreateCell(10).SetCellValue("逾期出貨");
            colsHeaderSub.CreateCell(11).SetCellValue("物流處理費(PChome)");
            colsHeaderSub.CreateCell(12).SetCellValue("倉儲費用(PChome)");
            colsHeaderSub.CreateCell(13).SetCellValue("代扣發票金額小計(B)");
            colsHeaderSub.CreateCell(14).SetCellValue("匯款應付(A - B)");
            colsHeaderSub.GetCell(1).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(2).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(3).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(4).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(5).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(6).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(7).CellStyle = HeaderLifeStyle;
            colsHeaderSub.GetCell(8).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(9).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(10).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(11).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(12).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(12).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(13).CellStyle = HeaderVendorStyle;
            colsHeaderSub.GetCell(14).CellStyle = HeaderPaymentStyle;
            idx += 1;

            var totalPayAmount = 0;
            var totalPayFreightAmount = 0;
            var totalDeductAmount = 0;
            var totalPaymentChangeAmount = 0;
            var totalPositivePaymentOverdueAmount = 0;
            var totalNegativePaymentOverdueAmount = 0;
            var totalISPFamilyOrderAmount = 0;
            var totalISPSevenOrderAmount = 0;
            var totalWmsOrderAmount = 0;
            var totalWmsAmount = 0;
            var totalAmount = 0;
            var invoiceAmount = 0;
            var receiptAmount = 0;
            var invoiceAmountTaxFree = 0;

            #region Detail
            foreach (var m in model.BalanceSheetPackInfos.Where(x => x.RemittanceType == (int)RemittanceType.Weekly ||
                                                                        x.RemittanceType == (int)RemittanceType.Monthly ||
                                                                        x.RemittanceType == (int)RemittanceType.Flexible ||
                                                                        x.RemittanceType == (int)RemittanceType.Fortnightly))
            {
                Row row = sheet.CreateRow(idx);

                row.CreateCell(0).SetCellValue(string.Format("[{0}]{1}", m.DealUniqueId, m.DealName));
                row.CreateCell(1).SetCellValue(string.Format("{0:$#,0}", m.PayAmount));
                row.GetCell(1).CellStyle = numStyle;

                row.CreateCell(2).SetCellValue(string.Format("{0:$#,0}", m.PayFreightAmount));
                row.GetCell(2).CellStyle = numStyle;

                row.CreateCell(3).SetCellValue(string.Format("{0:-$(#,0)}", m.DeductAmount));
                row.GetCell(3).CellStyle = numStyle;

                row.CreateCell(4).SetCellValue(string.Format("{0:$#,0}", m.PaymentChangeAmount));
                row.GetCell(4).CellStyle = numStyle;

                row.CreateCell(5).SetCellValue(string.Format("{0:$#,0}", m.PayAmount + m.PayFreightAmount - m.DeductAmount + m.PaymentChangeAmount));
                row.GetCell(5).CellStyle = numStyle;

                row.CreateCell(6).SetCellValue(string.Format("{0:$#,0}", m.PositivePaymentOverdueAmount));
                row.GetCell(6).CellStyle = numStyle;

                row.CreateCell(7).SetCellValue("-");
                row.GetCell(7).CellStyle = HeaderLifeStyle;

                row.CreateCell(8).SetCellValue(string.Format("{0:-$(#,0)}", m.ISPFamilyOrderAmount));
                row.GetCell(8).CellStyle = numStyle;

                row.CreateCell(9).SetCellValue(string.Format("{0:-$(#,0)}", m.ISPSevenOrderAmount));
                row.GetCell(9).CellStyle = numStyle;

                row.CreateCell(10).SetCellValue(string.Format("{0:$#,0}", m.NegativePaymentOverdueAmount));
                row.GetCell(10).CellStyle = numStyle;

                row.CreateCell(11).SetCellValue(string.Format("{0:$#,0}", m.WmsOrderAmount));
                row.GetCell(11).CellStyle = numStyle;

                row.CreateCell(12).SetCellValue("-");

                row.CreateCell(13).SetCellValue("-");
                row.GetCell(13).CellStyle = HeaderVendorStyle;

                row.CreateCell(14).SetCellValue("-");
                row.GetCell(14).CellStyle = HeaderPaymentStyle;

                if (m.EstAmount < 0)
                {
                    row.GetCell(0).CellStyle = fontBoldStyle;
                    row.GetCell(1).CellStyle = fontBoldStyleForNum;
                    row.GetCell(2).CellStyle = fontBoldStyleForNum;
                    row.GetCell(3).CellStyle = fontBoldStyleForNum;
                    row.GetCell(4).CellStyle = fontBoldStyleForNum;
                    row.GetCell(5).CellStyle = fontBoldStyleForNum;
                    row.GetCell(6).CellStyle = fontBoldStyleForNum;
                    row.GetCell(8).CellStyle = fontBoldStyleForNum;
                    row.GetCell(9).CellStyle = fontBoldStyleForNum;
                    row.GetCell(10).CellStyle = fontBoldStyleForNum;
                }


                totalPayAmount += m.PayAmount;
                totalPayFreightAmount += m.PayFreightAmount;
                totalDeductAmount += m.DeductAmount;
                totalPaymentChangeAmount += m.PaymentChangeAmount;
                totalPositivePaymentOverdueAmount += m.PositivePaymentOverdueAmount;
                totalNegativePaymentOverdueAmount += m.NegativePaymentOverdueAmount;
                totalISPFamilyOrderAmount += m.ISPFamilyOrderAmount;
                totalISPSevenOrderAmount += m.ISPSevenOrderAmount;
                totalWmsOrderAmount += m.WmsOrderAmount;
                totalAmount += m.EstAmount;

                switch (m.VendorReceiptType)
                {
                    case VendorReceiptType.Other:
                    case VendorReceiptType.Invoice:
                        if (m.IsTax)
                        {
                            invoiceAmount += m.EstAmount + m.IspFamilyAmount + m.IspSevenAmount + Math.Abs(m.NegativePaymentOverdueAmount) + m.WmsOrderAmount;
                        }
                        else
                        {
                            invoiceAmount += m.PayFreightAmount + Math.Abs(m.PositivePaymentOverdueAmount);
                            invoiceAmountTaxFree += m.EstAmount + m.IspFamilyAmount + m.IspSevenAmount + Math.Abs(m.NegativePaymentOverdueAmount) + m.WmsOrderAmount - Math.Abs(m.PositivePaymentOverdueAmount) - m.PayFreightAmount ;
                        }

                        break;
                    case VendorReceiptType.Receipt:
                        receiptAmount += m.EstAmount + m.IspFamilyAmount + m.IspSevenAmount + Math.Abs(m.NegativePaymentOverdueAmount) + m.WmsOrderAmount;
                        break;
                }

                idx += 1;
            }

            #endregion

            #region PCHOME Detail
            foreach (var m in model.BalanceSheetWmsPackInfos)
            {
                Row row = sheet.CreateRow(idx);

                row.CreateCell(0).SetCellValue(m.Name);
                row.CreateCell(1).SetCellValue("-");
                row.CreateCell(2).SetCellValue("-");
                row.CreateCell(3).SetCellValue("-");
                row.CreateCell(4).SetCellValue("-");
                row.CreateCell(5).SetCellValue("-");
                row.CreateCell(6).SetCellValue("-");
                row.GetCell(6).CellStyle = numStyle;
                row.CreateCell(7).SetCellValue("-");
                row.GetCell(7).CellStyle = HeaderLifeStyle;
                row.CreateCell(8).SetCellValue("-");
                row.GetCell(8).CellStyle = numStyle;
                row.CreateCell(9).SetCellValue("-");
                row.GetCell(9).CellStyle = numStyle;
                row.CreateCell(10).SetCellValue("-");
                row.GetCell(10).CellStyle = numStyle;
                row.CreateCell(11).SetCellValue("-");
                row.GetCell(11).CellStyle = numStyle;
                row.CreateCell(12).SetCellValue(string.Format("{0:$#,0}", m.EstAmount));
                row.GetCell(12).CellStyle = numStyle;
                row.CreateCell(13).SetCellValue("-");
                row.GetCell(13).CellStyle = HeaderVendorStyle;
                row.CreateCell(14).SetCellValue("-");
                row.GetCell(14).CellStyle = HeaderPaymentStyle;

                if (m.EstAmount < 0)
                {
                    row.GetCell(0).CellStyle = fontBoldStyle;
                    row.GetCell(1).CellStyle = fontBoldStyleForNum;
                    row.GetCell(2).CellStyle = fontBoldStyleForNum;
                    row.GetCell(3).CellStyle = fontBoldStyleForNum;
                    row.GetCell(4).CellStyle = fontBoldStyleForNum;
                    row.GetCell(5).CellStyle = fontBoldStyleForNum;
                    row.GetCell(6).CellStyle = fontBoldStyleForNum;
                    row.GetCell(8).CellStyle = fontBoldStyleForNum;
                    row.GetCell(9).CellStyle = fontBoldStyleForNum;
                    row.GetCell(10).CellStyle = fontBoldStyleForNum;
                    row.GetCell(11).CellStyle = fontBoldStyleForNum;
                }

                totalWmsAmount += m.EstAmount;
                totalAmount += m.EstAmount;


                idx += 1;
            } 
            #endregion

            #region Total
            Row totalRow = sheet.CreateRow(idx);

            
            totalRow.CreateCell(0).SetCellValue("合併請款對帳項目金額總計");
            totalRow.CreateCell(1).SetCellValue(string.Format("{0:$#,0}", totalPayAmount));
            totalRow.GetCell(1).CellStyle = numStyle;
            totalRow.CreateCell(2).SetCellValue(string.Format("{0:$#,0}", totalPayFreightAmount));
            totalRow.GetCell(2).CellStyle = numStyle;
            totalRow.CreateCell(3).SetCellValue(string.Format("{0:-$(#,0)}", totalDeductAmount));
            totalRow.GetCell(3).CellStyle = numStyle;
            totalRow.CreateCell(4).SetCellValue(string.Format("{0:$#,0}", totalPaymentChangeAmount));
            totalRow.GetCell(4).CellStyle = numStyle;
            totalRow.CreateCell(5).SetCellValue(string.Format("{0:$#,0}", totalPayAmount + totalPayFreightAmount - totalDeductAmount + totalPaymentChangeAmount));
            totalRow.GetCell(5).CellStyle = numStyle;
            totalRow.CreateCell(6).SetCellValue(string.Format("{0:$#,0}", totalPositivePaymentOverdueAmount));
            totalRow.GetCell(6).CellStyle = numStyle;
            totalRow.CreateCell(7).SetCellValue(string.Format("{0:$#,0}", totalPayAmount + totalPayFreightAmount - totalDeductAmount + totalPaymentChangeAmount + totalPositivePaymentOverdueAmount));
            totalRow.GetCell(7).CellStyle = HeaderLifeStyle;
            totalRow.CreateCell(8).SetCellValue(string.Format("{0:-$(#,0)}", totalISPFamilyOrderAmount));
            totalRow.GetCell(8).CellStyle = numStyle;
            totalRow.CreateCell(9).SetCellValue(string.Format("{0:-$(#,0)}", totalISPSevenOrderAmount));
            totalRow.GetCell(9).CellStyle = numStyle;
            totalRow.CreateCell(10).SetCellValue(string.Format("{0:$#,0}", totalNegativePaymentOverdueAmount));
            totalRow.GetCell(10).CellStyle = numStyle;
            totalRow.CreateCell(11).SetCellValue(string.Format("{0:$#,0}", totalWmsOrderAmount));
            totalRow.GetCell(11).CellStyle = numStyle;
            totalRow.CreateCell(12).SetCellValue(string.Format("{0:$#,0}", totalWmsAmount));
            totalRow.GetCell(12).CellStyle = numStyle;
            totalRow.CreateCell(13).SetCellValue(string.Format("{0:$#,0}", -totalISPFamilyOrderAmount - totalISPSevenOrderAmount + totalNegativePaymentOverdueAmount - totalWmsOrderAmount + totalWmsAmount));
            totalRow.GetCell(13).CellStyle = HeaderVendorStyle;
            totalRow.CreateCell(14).SetCellValue(string.Format("{0:$#,0}", totalAmount >= 0 ? totalAmount : 0));
            totalRow.GetCell(14).CellStyle = HeaderPaymentStyle;
            idx += 1;
            #endregion

            //footer
            StringBuilder sbFooter = new StringBuilder();
            sbFooter.AppendLine("抬頭：康太數位整合股份有限公司");
            sbFooter.AppendLine("統編：24317014");
            sbFooter.AppendLine("品名：商品一批");
            sbFooter.AppendLine("單據發票寄件地址：10441台北市中山區中山北路一段11號13樓 帳務服務組 收");
            sbFooter.AppendLine();
            sbFooter.AppendLine("※開立發票時，請使用以下金額開立。另A區中的逾期出貨金額請於同一張應稅發票中，開立另外名目。（建議：其他收入）。");
            sbFooter.AppendLine(string.Format("含稅發票總金額：{0:$#,0}", totalAmount >= 0 ? invoiceAmount : 0));
            sbFooter.AppendLine(string.Format("免稅發票總金額：{0:$#,0}", totalAmount >= 0 ? invoiceAmountTaxFree : 0));
            sbFooter.Append(string.Format("收據總金額：{0:$#,0}", totalAmount >= 0 ? receiptAmount : 0));
            sbFooter.AppendLine();
            sbFooter.Append(string.Format("應付金額總計(A-B)：{0:$#,0}", totalAmount >= 0 ? totalAmount : 0));

            Row footer = sheet.CreateRow(idx);
            footer.CreateCell(0).SetCellValue(sbFooter.ToString());
            footer.CreateCell(1).SetCellValue(string.Empty);
            footer.GetCell(0).CellStyle = flooterStyle;
            footer.GetCell(1).CellStyle = flooterStyle;
            sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(idx, idx, 0, 12));
            footer.HeightInPoints = 115f;

            //設置寬度為autosize
            for (int i = 0; i <= 1; i++)
            {
                sheet.AutoSizeColumn(i, true);
            }

            sheet.SetColumnWidth(0, 18000);
            sheet.SetColumnWidth(1, 3300);
            sheet.SetColumnWidth(2, 3300);
            sheet.SetColumnWidth(3, 3400);
            sheet.SetColumnWidth(4, 3600);
            sheet.SetColumnWidth(5, 4200);
            sheet.SetColumnWidth(6, 5200);
            sheet.SetColumnWidth(7, 4000);
            sheet.SetColumnWidth(8, 4000);
            sheet.SetColumnWidth(9, 4000);
            sheet.SetColumnWidth(10, 4200);
            sheet.SetColumnWidth(11, 5000);
            sheet.SetColumnWidth(12, 4200);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 商家系統 - 匯出總明細(宅配)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private MemoryStream BalanceSheetWmsFeeExportToExcel(int balanceSheetWmsId, BalanceSheetWm bsw)
        {
            
            WmsPurchaseServFeeCollection wpcoc = _wp.GetWmsPurchaseServFeeByBalanceSheetWmsId(balanceSheetWmsId);
            WmsStockFeeCollection wsfc = _wp.GetWmsStockFeeByBalanceSheetWmsId(balanceSheetWmsId);
            WmsReturnServFeeCollection wrsefc = _wp.GetWmsReturnServFeeByBalanceSheetWmsId(balanceSheetWmsId);
            WmsReturnPackFeeCollection wrpfc = _wp.GetWmsReturnPackFeeByBalanceSheetWmsId(balanceSheetWmsId);
            WmsReturnShipFeeCollection wrshfc = _wp.GetWmsReturnShipFeeByBalanceSheetWmsId(balanceSheetWmsId);
            
            //頁簽
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("總明細");

            //style
            NPOI.SS.UserModel.Font fontForB = workbook.CreateFont();
            fontForB.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.BOLD;
            fontForB.Color = NPOI.HSSF.Util.HSSFColor.WHITE.index;

            HSSFCellStyle HeaderSubStyle = (HSSFCellStyle)workbook.CreateCellStyle();
            HeaderSubStyle.BackgroundColor(HSSFColor.RED.index);
            HeaderSubStyle.SetFont(fontForB);


            int idx = 0;

            //header
            Row colsHeader = sheet.CreateRow(idx);
            colsHeader.CreateCell(0).SetCellValue("對帳單名稱：" + bsw.IntervalStart.ToString("yyyy年MM月"));
            idx += 1;

            Row colsHeader2 = sheet.CreateRow(idx);
            colsHeader2.CreateCell(0).SetCellValue("結算日期：" + bsw.IntervalStart.ToString("yyyy/MM/dd") + "~" + bsw.IntervalEnd.ToString("yyyy/MM/dd"));
            idx += 1;

            #region 進倉作業處理費
            Row colsHeaderPurchaseServFee = sheet.CreateRow(idx);
            colsHeaderPurchaseServFee.CreateCell(0).SetCellValue("進倉作業處理費");
            colsHeaderPurchaseServFee.GetCell(0).CellStyle = HeaderSubStyle;
            idx += 1;

            Row colsHeaderSubPurchaseServFee = sheet.CreateRow(idx);
            colsHeaderSubPurchaseServFee.CreateCell(0).SetCellValue("進倉單編號");
            colsHeaderSubPurchaseServFee.CreateCell(1).SetCellValue("驗收批號");
            colsHeaderSubPurchaseServFee.CreateCell(2).SetCellValue("進倉作業處理費");
            colsHeaderSubPurchaseServFee.CreateCell(3).SetCellValue("驗收日期(立帳日)");
            colsHeaderSubPurchaseServFee.CreateCell(4).SetCellValue("商品ID(貨主)");
            colsHeaderSubPurchaseServFee.CreateCell(5).SetCellValue("商品ID");
            colsHeaderSubPurchaseServFee.CreateCell(6).SetCellValue("商品名稱");
            colsHeaderSubPurchaseServFee.CreateCell(7).SetCellValue("商品規格");
            colsHeaderSubPurchaseServFee.CreateCell(8).SetCellValue("處理費單價");
            colsHeaderSubPurchaseServFee.CreateCell(9).SetCellValue("數量");

            idx += 1;
            foreach (WmsPurchaseServFee wpco in wpcoc)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(wpco.PurchaseOrderId);
                row.CreateCell(1).SetCellValue(wpco.AcceptId);
                row.CreateCell(2).SetCellValue(wpco.Fee.ToString());
                row.CreateCell(3).SetCellValue(wpco.AccountingDate.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(4).SetCellValue(wpco.VendorPid);
                row.CreateCell(5).SetCellValue(wpco.ProdId);
                row.CreateCell(6).SetCellValue(wpco.ProdName);
                row.CreateCell(7).SetCellValue(wpco.ProdSpec);
                row.CreateCell(8).SetCellValue(wpco.UnitFee.ToString());
                row.CreateCell(9).SetCellValue(wpco.Qty.ToString());
                idx += 1;

            }
            #endregion

            #region 倉儲費
            Row colsHeaderStockFee = sheet.CreateRow(idx);
            colsHeaderStockFee.CreateCell(0).SetCellValue("倉儲費");
            colsHeaderStockFee.GetCell(0).CellStyle = HeaderSubStyle;
            idx += 1;

            Row colsHeaderSubStockFee = sheet.CreateRow(idx);
            colsHeaderSubStockFee.CreateCell(0).SetCellValue("立帳日");
            colsHeaderSubStockFee.CreateCell(1).SetCellValue("商品ID(貨主)");
            colsHeaderSubStockFee.CreateCell(2).SetCellValue("商品ID");
            colsHeaderSubStockFee.CreateCell(3).SetCellValue("倉儲費");
            colsHeaderSubStockFee.CreateCell(4).SetCellValue("商品名稱");
            colsHeaderSubStockFee.CreateCell(5).SetCellValue("商品規格");
            colsHeaderSubStockFee.CreateCell(6).SetCellValue("材積");
            colsHeaderSubStockFee.CreateCell(7).SetCellValue("倉儲費單價");
            colsHeaderSubStockFee.CreateCell(8).SetCellValue("數量");

            idx += 1;
            foreach (WmsStockFee wsf in wsfc)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(wsf.AccountingDate.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(wsf.VendorPid);
                row.CreateCell(2).SetCellValue(wsf.ProdId);
                row.CreateCell(3).SetCellValue(wsf.Fee.ToString());
                row.CreateCell(4).SetCellValue(wsf.ProdName);
                row.CreateCell(5).SetCellValue(wsf.ProdSpec);
                row.CreateCell(6).SetCellValue(wsf.Volume.ToString());
                row.CreateCell(7).SetCellValue(wsf.UnitFee.ToString());
                row.CreateCell(8).SetCellValue(wsf.Qty.ToString());
                idx += 1;

            }
            #endregion

            #region 還貨作業費
            Row colsHeaderReturnServFee = sheet.CreateRow(idx);
            colsHeaderReturnServFee.CreateCell(0).SetCellValue("還貨作業費");
            colsHeaderReturnServFee.GetCell(0).CellStyle = HeaderSubStyle;
            idx += 1;

            Row colsHeaderSubReturnServFee = sheet.CreateRow(idx);
            colsHeaderSubReturnServFee.CreateCell(0).SetCellValue("還貨單日期");
            colsHeaderSubReturnServFee.CreateCell(1).SetCellValue("還貨單編號-序號");
            colsHeaderSubReturnServFee.CreateCell(2).SetCellValue("還貨作業處理費");
            colsHeaderSubReturnServFee.CreateCell(3).SetCellValue("出貨日期(立帳日)");
            colsHeaderSubReturnServFee.CreateCell(4).SetCellValue("商品ID(貨主)");
            colsHeaderSubReturnServFee.CreateCell(5).SetCellValue("商品ID");
            colsHeaderSubReturnServFee.CreateCell(6).SetCellValue("商品名稱");
            colsHeaderSubReturnServFee.CreateCell(7).SetCellValue("商品規格");
            colsHeaderSubReturnServFee.CreateCell(8).SetCellValue("處理費單價");
            colsHeaderSubReturnServFee.CreateCell(9).SetCellValue("數量");

            idx += 1;
            foreach (WmsReturnServFee wrsef in wrsefc)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(wrsef.ReturnDate.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(wrsef.ReturnId);
                row.CreateCell(2).SetCellValue(wrsef.Fee.ToString());
                row.CreateCell(3).SetCellValue(wrsef.AccountingDate.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(4).SetCellValue(wrsef.VendorPid.ToString());
                row.CreateCell(5).SetCellValue(wrsef.ProdId);
                row.CreateCell(6).SetCellValue(wrsef.ProdName);
                row.CreateCell(7).SetCellValue(wrsef.ProdSpec);
                row.CreateCell(8).SetCellValue(wrsef.UnitFee.ToString());
                row.CreateCell(9).SetCellValue(wrsef.Qty.ToString());
                idx += 1;

            }
            #endregion

            #region 還貨包材費
            Row colsHeaderReturnPackFee = sheet.CreateRow(idx);
            colsHeaderReturnPackFee.CreateCell(0).SetCellValue("還貨包材費");
            colsHeaderReturnPackFee.GetCell(0).CellStyle = HeaderSubStyle;
            idx += 1;

            Row colsHeaderSubReturnPackFee = sheet.CreateRow(idx);
            colsHeaderSubReturnPackFee.CreateCell(0).SetCellValue("還貨單日期");
            colsHeaderSubReturnPackFee.CreateCell(1).SetCellValue("還貨單編號-序號");
            colsHeaderSubReturnPackFee.CreateCell(2).SetCellValue("還貨包材費");
            colsHeaderSubReturnPackFee.CreateCell(3).SetCellValue("出貨日期(立帳日)");
            colsHeaderSubReturnPackFee.CreateCell(4).SetCellValue("商品ID(貨主)");
            colsHeaderSubReturnPackFee.CreateCell(5).SetCellValue("商品ID");
            colsHeaderSubReturnPackFee.CreateCell(6).SetCellValue("商品名稱");
            colsHeaderSubReturnPackFee.CreateCell(7).SetCellValue("商品規格");
            colsHeaderSubReturnPackFee.CreateCell(8).SetCellValue("數量");

            idx += 1;
            foreach (WmsReturnPackFee wrpf in wrpfc)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(wrpf.ReturnDate.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(wrpf.ReturnId);
                row.CreateCell(2).SetCellValue(wrpf.Fee.ToString());
                row.CreateCell(3).SetCellValue(wrpf.AccountingDate.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(4).SetCellValue(wrpf.VendorPid.ToString());
                row.CreateCell(5).SetCellValue(wrpf.ProdId);
                row.CreateCell(6).SetCellValue(wrpf.ProdName);
                row.CreateCell(7).SetCellValue(wrpf.ProdSpec);
                row.CreateCell(8).SetCellValue(wrpf.Qty.ToString());
                idx += 1;

            }
            #endregion

            #region 還貨運費
            Row colsHeaderReturnShipFee = sheet.CreateRow(idx);
            colsHeaderReturnShipFee.CreateCell(0).SetCellValue("還貨運費");
            colsHeaderReturnShipFee.GetCell(0).CellStyle = HeaderSubStyle;
            idx += 1;

            Row colsHeaderSubWmsReturnShipFee = sheet.CreateRow(idx);
            colsHeaderSubWmsReturnShipFee.CreateCell(0).SetCellValue("還貨單日期");
            colsHeaderSubWmsReturnShipFee.CreateCell(1).SetCellValue("還貨單編號-序號");
            colsHeaderSubWmsReturnShipFee.CreateCell(2).SetCellValue("還貨運費");
            colsHeaderSubWmsReturnShipFee.CreateCell(3).SetCellValue("出貨日期(立帳日)");
            colsHeaderSubWmsReturnShipFee.CreateCell(4).SetCellValue("商品ID(貨主)");
            colsHeaderSubWmsReturnShipFee.CreateCell(5).SetCellValue("商品ID");
            colsHeaderSubWmsReturnShipFee.CreateCell(6).SetCellValue("商品名稱");
            colsHeaderSubWmsReturnShipFee.CreateCell(7).SetCellValue("商品規格");
            colsHeaderSubWmsReturnShipFee.CreateCell(8).SetCellValue("數量");
            colsHeaderSubWmsReturnShipFee.CreateCell(9).SetCellValue("物流單號");

            idx += 1;
            foreach (WmsReturnShipFee wrshf in wrshfc)
            {
                Row row = sheet.CreateRow(idx);
                row.CreateCell(0).SetCellValue(wrshf.ReturnDate.ToString("yyyy/MM/dd"));
                row.CreateCell(1).SetCellValue(wrshf.ReturnId);
                row.CreateCell(2).SetCellValue(wrshf.Fee.ToString());
                row.CreateCell(3).SetCellValue(wrshf.AccountingDate.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(4).SetCellValue(wrshf.VendorPid.ToString());
                row.CreateCell(5).SetCellValue(wrshf.ProdId);
                row.CreateCell(6).SetCellValue(wrshf.ProdName);
                row.CreateCell(7).SetCellValue(wrshf.ProdSpec);
                row.CreateCell(8).SetCellValue(wrshf.Qty.ToString());
                row.CreateCell(9).SetCellValue(wrshf.ShipId);
                idx += 1;

            }
            #endregion

            //設置寬度為autosize
            for (int i = 0; i <= 1; i++)
            {
                sheet.AutoSizeColumn(i, true);
            }

            sheet.SetColumnWidth(0, 9000);
            sheet.SetColumnWidth(1, 5000);
            sheet.SetColumnWidth(2, 5000);
            sheet.SetColumnWidth(3, 5000);
            sheet.SetColumnWidth(4, 5000);
            sheet.SetColumnWidth(5, 5000);
            sheet.SetColumnWidth(6, 5000);
            sheet.SetColumnWidth(7, 5000);
            sheet.SetColumnWidth(8, 5000);
            sheet.SetColumnWidth(9, 5000);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private void SetCellStyle(Row row, int endIdx, HSSFCellStyle style, bool isMerged = false)
        {
            for (int i = 0; i <= endIdx; i++)
            {
                if (isMerged && i != 0)
                {
                    row.CreateCell(i).SetCellValue(string.Empty);
                }
                row.GetCell(i).CellStyle = style;
            }
        }

        private static string DownloadFileNameFormat(string fileName)
        {
            //檔名含全形雙引號 會導致excel無法正常開啟(空白) 故置換為全形單引號
            fileName = fileName.Replace("“", "‘").Replace("”", "’");
            List<char> formatStr = new List<char> { '/', '\\' };

            return formatStr.Aggregate(fileName, (current, fs) => current.Replace(fs, '_'));
        }

        private string DownloadFileNameEncode(string fileName)
        {
            fileName = DownloadFileNameFormat(fileName);
            string browser = HttpContext.Request.UserAgent.ToUpper();
            if (browser.Contains("IE"))
            {
                return HttpUtility.UrlEncode(fileName);
            }

            return fileName;
        }

        #region 客服處理
        protected string[] GetServiceProcessFilter(ServiceProcessModel model)
        {
            if (model == null)
            {
                return null;
            }

            System.Collections.ArrayList filterList = new System.Collections.ArrayList();

            if (!string.IsNullOrEmpty(model.ServiceNo))
            {
                filterList.Add(ViewCustomerServiceList.Columns.ServiceNo + " = " + model.ServiceNo);
            }

            if (!string.IsNullOrEmpty(model.OrderId))
            {
                filterList.Add(ViewCustomerServiceList.Columns.OrderId + " = " + model.OrderId);
            }

            if ((model.Priority??-1) != -1)
            {
                filterList.Add(ViewCustomerServiceList.Columns.CasePriority + " = " + model.Priority);
            }

            if ((model.ProblemType ?? -1) != -1)
            {
                filterList.Add(ViewCustomerServiceList.Columns.Category + " = " + model.ProblemType);
            }

            if (model.CreateTimeS != null && model.CreateTimeE != null)
            {
                filterList.Add(ViewCustomerServiceList.Columns.CreateTime + " between " + ((DateTime)model.CreateTimeS).ToString("yyyy/MM/dd") + " 00:00:00 " + " and " + ((DateTime)model.CreateTimeE).ToString("yyyy/MM/dd") + " 23:59:59 ");
            }
            else if (model.CreateTimeS == null && model.CreateTimeE != null)
            {
                filterList.Add(ViewCustomerServiceList.Columns.CreateTime + " <= " + ((DateTime)model.CreateTimeE).ToString("yyyy/MM/dd") + " 23:59:59 ");
            }
            else if (model.CreateTimeS != null && model.CreateTimeE == null)
            {
                filterList.Add(ViewCustomerServiceList.Columns.CreateTime + " >= " + ((DateTime)model.CreateTimeS).ToString("yyyy/MM/dd") + " 00:00:00 ");
            }

            if (!string.IsNullOrEmpty(model.UniqueId))
            {
                filterList.Add(ViewCustomerServiceList.Columns.UniqueId + " = " + model.UniqueId);
            }
            if (!string.IsNullOrEmpty(model.ItemName))
            {
                filterList.Add(ViewCustomerServiceList.Columns.ItemName + " like %" + model.ItemName +"%");
            }
            if (!string.IsNullOrEmpty(model.MemberName))
            {
                filterList.Add(ViewCustomerServiceList.Columns.MemberName + " like %" + model.MemberName + "%");
            }
            if (!string.IsNullOrEmpty(model.MobileNumber))
            {
                filterList.Add(ViewCustomerServiceList.Columns.MobileNumber + " like %" + model.MobileNumber + "%");
            }

            string status = string.Empty;
            if (model.IsProcess)
            {
                status += (int)statusConvert.process + ",";
            }

            if (model.IsTransfer)
            {
                status += (int)statusConvert.transfer + ",";
            }

            if (model.IsTransferBack)
            {
                status += (int)statusConvert.transferback + ",";
            }

            if (model.IsComplete)
            {
                status += (int)statusConvert.complete + ",";
            }

            if (!string.IsNullOrEmpty(status))
            {
                filterList.Add(ViewCustomerServiceList.Columns.CustomerServiceStatus + " in " + status.TrimEnd(','));
            }

            if (filterList.Count > 0)
            {
                string[] rtn = new string[filterList.Count];
                for (int i = 0; i < filterList.Count; i++)
                {
                    rtn[i] = (string)filterList[i];
                }
                return rtn;
            }

            return null;
        }
        #endregion
        #endregion method
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.BookingSystem;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.BookingSystem;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class ReserveLockController : BaseController
    {
        private static IMemberProvider _mp;
        private static IBookingSystemProvider _bp;
        private static ISellerProvider _sp;
        private static ISysConfProvider _conf;
        private static IPponProvider _pp;
        private static IHiDealProvider _hp;

        private const int LOCK_COUPON_PAGE_NUMBER = 20;

        static ReserveLockController()
        {
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _bp = ProviderFactory.Instance().GetProvider<IBookingSystemProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
        }

        #region Action

        #region ReserveCouponLock

        [VbsAuthorize]
        public ActionResult ReserveCouponLock(ReserveCouponLockViewModel model)
        {
            ReserveLockCommonInfo("ReserveCouponLock");

            return View(model);
        }

        [ActionName("ReserveCouponLock")]
        [VbsAuthorize, HttpPost]
        public ActionResult ReserveCouponLockSearch(ReserveCouponLockViewModel model)
        {
            ReserveLockCommonInfo("ReserveCouponLock");

            VbsMembership member = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
            bool is17LifeEmployee = member.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount;

            var allowedDealUnits = (new VbsVendorAclMgmtModel(member.AccountId)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();

            model.ReserveCouponInfos = BookingSystemFacade.GetAccessibleReserveCoupon(is17LifeEmployee, allowedDealUnits, model.CouponSequenceP1, model.CouponSequenceP2);

            return View(model);
        }


        #endregion ReserveCouponLock

        #region ReserveCouponLockSearch

        [VbsAuthorize]
        public ActionResult CouponLockSearch(CouponLockSearchViewModel vModel, int? pageNumber)
        {
            ReserveLockCommonInfo("CouponLockSearch");

            if (pageNumber != null && TempData["ReserveCouponInfos"] != null)
            {
                List<ReserveCouponInfo> reserveCouponInfos = TempData["ReserveCouponInfos"] as List<ReserveCouponInfo>;
                vModel.ReserveCouponLockSearchType = (TempData["ReserveCouponLockSearchType"] == null) ? default(int) : (int)TempData["ReserveCouponLockSearchType"];
                vModel.SearchStartDate = (TempData["SearchStartDate"] == null) ? string.Empty : TempData["SearchStartDate"] as string;
                vModel.SearchEndDate = (TempData["SearchEndDate"] == null) ? string.Empty : TempData["SearchEndDate"] as string;

                if (reserveCouponInfos != null)
                {
                    int pageSize = LOCK_COUPON_PAGE_NUMBER; 
                    int pageIndex = pageNumber.Value - 1;

                    var query = (from t in reserveCouponInfos select t);
                    var paging = new PagerList<ReserveCouponInfo>(
                        query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                        new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
                        pageIndex,
                        query.ToList().Count);

                    vModel.ReserveCouponInfos = paging;

                    TempData["ReserveCouponInfos"] = reserveCouponInfos;
                    TempData["SearchStartDate"] = vModel.SearchStartDate;
                    TempData["SearchEndDate"] = vModel.SearchEndDate;
                    TempData["ReserveCouponLockSearchType"] = vModel.ReserveCouponLockSearchType;
                }
            }

            return View(vModel);
        }

        [VbsAuthorize]
        [HttpPost]
        public ActionResult CouponLockListSearch(CouponLockSearchViewModel vModel)
        {
            ReserveLockCommonInfo("CouponLockSearch");

            DateTime searchStartDate;
            DateTime searchEndDate;
            if (DateTime.TryParse(vModel.SearchStartDate,out searchStartDate) && DateTime.TryParse(vModel.SearchEndDate,out searchEndDate))
            {
            VbsMembership member = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
            bool is17LifeEmployee = member.VbsMembershipAccountType == VbsMembershipAccountType.VerificationAccount;

            var allowedDealUnits = (new VbsVendorAclMgmtModel(member.AccountId)).GetAllowedDeals(VbsRightFlag.VerifyShop, DeliveryType.ToShop).ToList();

            List<ReserveCouponInfo> reserveCouponInfos = BookingSystemFacade.GetAccessibleReserveLockCoupon(is17LifeEmployee, (int)vModel.ReserveCouponLockSearchType, allowedDealUnits, searchStartDate, searchEndDate)
                                                                            .Where(x => x.ReserveCouponStatus != ReserveCouponStatus.Returning &&
                                                                                        x.ReserveCouponStatus != ReserveCouponStatus.Returned)
                                                                            .ToList();

            int pageSize = LOCK_COUPON_PAGE_NUMBER;
            int pageNumber = 1;
            int pageIndex = pageNumber - 1;

            var query = (from t in reserveCouponInfos select t);
            var paging = new PagerList<ReserveCouponInfo>(
            query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
            new PagerOption(pageSize, pageIndex * pageSize, string.Empty, false),
            pageIndex,
            query.ToList().Count);

            TempData["ReserveCouponInfos"] = reserveCouponInfos;
            TempData["SearchStartDate"] = vModel.SearchStartDate;
            TempData["SearchEndDate"] = vModel.SearchEndDate;
            TempData["ReserveCouponLockSearchType"] = vModel.ReserveCouponLockSearchType;
            
            vModel.ReserveCouponInfos = paging;
            }

            return View("CouponLockSearch", vModel);
        }

        #endregion ReserveCouponLockSearch

        #endregion Action

        [VbsAuthorize, HttpPost]
        public ActionResult CouponLock(int dealType, int couopnId, int couponStatus, string couponReservationDate)
        {
            DateTime reservationDate;

            if (!DateTime.TryParse(couponReservationDate, out reservationDate))
            {
                return Json(new { ReservationCouponLockSuccess = false, errorMsg = "日期格式錯誤" });
            }

            CashTrustLog ctl = _mp.CashTrustLogGetByCouponId(couopnId);
            if (ctl.Status == (int)TrustStatus.Refunded || ctl.Status == (int)TrustStatus.Returned)
            {
                return Json(new { ReservationCouponLockSuccess = false, errorMsg = "該憑證已退貨" });
            }
            else if (ctl.Status == (int)TrustStatus.Trusted || ctl.Status == (int)TrustStatus.Initial)
            {
                var returningCtlog = _mp.PponRefundingCashTrustLogGetListByOrderGuid(ctl.OrderGuid).FirstOrDefault(x => x.CouponId == couopnId);

                if (returningCtlog != null)
                {
                    return Json(new { ReservationCouponLockSuccess = false, errorMsg = "該憑證退貨中" });
                }
            }


            switch (dealType)
            {
                case (int)BusinessModel.Ppon:
                    var coupon = _pp.CouponGet(couopnId);
                    coupon.IsReservationLock = !coupon.IsReservationLock;
                    _pp.CouponSet(coupon);
                    SetReserveCouponLockLog(BusinessModel.Ppon, couopnId, VbsCurrent.AccountId, couponStatus, reservationDate);

                    break;
                case (int)BusinessModel.PiinLife:
                    var hiDealCoupon = _hp.HiDealCouponGet(couopnId);
                    hiDealCoupon.IsReservationLock = !hiDealCoupon.IsReservationLock;
                    _hp.HiDealCouponSet(hiDealCoupon);
                    SetReserveCouponLockLog(BusinessModel.PiinLife, couopnId, VbsCurrent.AccountId, couponStatus, reservationDate);
                    break;
            }

            return Json(new { ReservationCouponLockSuccess = true });

        }

        [VbsAuthorize, HttpPost]
        public ActionResult CouponUnLock(int dealType, int couopnId, int couponStatus)
        {
            switch (dealType)
            {
                case (int)BusinessModel.Ppon:
                    var coupon = _pp.CouponGet(couopnId);
                    coupon.IsReservationLock = !coupon.IsReservationLock;
                    _pp.CouponSet(coupon);
                    SetReserveCouponLockLog(BusinessModel.Ppon, couopnId, VbsCurrent.AccountId, couponStatus, null);

                    break;
                case (int)BusinessModel.PiinLife:
                    var hiDealCoupon = _hp.HiDealCouponGet(couopnId);
                    hiDealCoupon.IsReservationLock = !hiDealCoupon.IsReservationLock;
                    _hp.HiDealCouponSet(hiDealCoupon);
                    SetReserveCouponLockLog(BusinessModel.PiinLife, couopnId, VbsCurrent.AccountId, couponStatus, null);
                    break;
            }

            return Json(new { ReservationCouponLockSuccess = true });

        }

        private void SetReserveCouponLockLog(BusinessModel businessModel, int couponId, string accountId, int couponStatus, DateTime? couponReservationDate)
        {
            var reserveLocklog = new BookingSystemReserveLockStatusLog();
            reserveLocklog.DealType = (int)businessModel;
            reserveLocklog.CouponId = couponId;
            reserveLocklog.AccountId = accountId;
            reserveLocklog.ModifyTime = DateTime.Now;

            if (couponStatus == (int)ReserveCouponStatus.UnLock || couponStatus == (int)ReserveCouponStatus.Valid)
            {
                reserveLocklog.ReserveLockStatus = (int)ReserveCouponStatus.Lock;
                reserveLocklog.LogInfo = ((couponStatus == (int)ReserveCouponStatus.UnLock) ? ReserveCouponStatus.UnLock.ToString() : ReserveCouponStatus.Valid.ToString()) + "-->" + ReserveCouponStatus.Lock.ToString();
                if (couponReservationDate.HasValue)
                {
                    reserveLocklog.CouponReservationDate = couponReservationDate.Value.Date;
                }
            }
            else if (couponStatus == (int)ReserveCouponStatus.Lock)
            {
                reserveLocklog.ReserveLockStatus = (int)ReserveCouponStatus.UnLock;
                reserveLocklog.LogInfo = ReserveCouponStatus.Lock.ToString() + "-->" + ReserveCouponStatus.UnLock.ToString();
            }
            else
            {
                return;
            }

            _bp.BookingSystemReserveLockStatusLogSet(reserveLocklog);
        }

        private void ReserveLockCommonInfo(string parentActionName)
        {
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.Title = parentActionName;
            Session[VbsSession.ParentActionName.ToString()] = parentActionName;
        }
    }
}

﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.BizLogic.Models.Wallet;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Component.RocketMQEntity;
using LunchKingSite.BizLogic.Component.RocketMQTrans;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models;
using LunchKingSite.Core.Models.Entities;

using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.SkmDeal;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.ComponentModel;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.WebLib.Controllers
{
    public class SKMDealController : PponControllerBase
    {
        private static IChannelEventProvider _cep;
        private static IPponProvider _pp;
        private static ISellerProvider _sp;
        private static ISystemProvider _sysp;
        private static ISysConfProvider _config;
        private static IMemberProvider _mp;
        private static ISkmProvider _skm;
        private static IChannelProvider _cp;
        private static IExhibitionProvider _ep;
        private static ISkmEfProvider _skmef;
        private static string _defaultSeller;
        private static int _skmActionEventInfoId;
        private static int _skmChannelId;
        private static ILog logger = LogManager.GetLogger("Skm");
        private const string SkmPublicImageSysName = "SkmPublicImage";
        private const string SkmPrizeImgFolder = "PrizeDraw";
        private string fileUploadDirPath = System.Web.HttpContext.Current.Server.MapPath("~/media/Files/");
        private string imgUploadDirPath = System.Web.HttpContext.Current.Server.MapPath("~/media/Event/");
        private const string AliOnMarket = "AliOnMarket";
        private const string AliResult = "AliResult";
        private const string AliMessage = "AliMessage";

        static SKMDealController()
        {
            _skmef = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
            _cep = ProviderFactory.Instance().GetProvider<IChannelEventProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            _cp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            _ep = ProviderFactory.Instance().GetProvider<IExhibitionProvider>();
            _defaultSeller = _sp.SkmShoppeCollectionGetByShopCode("121").FirstOrDefault().SellerGuid.ToString();
        }

        /// <summary>
        /// 新光 ChannelId
        /// </summary>
        private static int SkmChannelId
        {
            get
            {
                if (_skmChannelId == 0)
                {
                    _skmChannelId = _cp.ChannelOdmCustomerGet(_config.SkmRootSellerGuid).Id;
                }
                return _skmChannelId;
            }
        }

        private static int SkmActionEventInfoId
        {
            get
            {
                if (_skmActionEventInfoId == 0)
                {
                    var skmAei = _mp.ActionEventInfoListGet().FirstOrDefault(x => x.ActionName.Equals("SkmBeaconEvent"));
                    _skmActionEventInfoId = skmAei == null ? 0 : skmAei.Id;
                }
                return _skmActionEventInfoId;
            }
            set { _skmActionEventInfoId = value; }
        }

        private static string DefaultSeller
        {
            get
            {
                if (string.IsNullOrEmpty(_defaultSeller))
                {
                    _defaultSeller = _sp.SkmShoppeCollectionGetByShopCode("121").FirstOrDefault().SellerGuid.ToString();
                }
                return _defaultSeller;
            }
            set
            {
                _defaultSeller = value;
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("SKMList");
        }

        public ActionResult SetPassword()
        {
            return Redirect("../User/UserAccount.aspx");
        }

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            string resultMsg = "";
            string firstStore = "";
            string firstShopCode = "";
            bool flag = true;

            List<dynamic> fileItems = new List<dynamic>();

            for (int i = 0; i < Request.Files.Count; i++)
            {
                file = Request.Files[i];

                string fileExt = Helper.GetExtensionByContentType(file.ContentType);
                string fileId = string.Format("{0}.{1}", Guid.NewGuid(), fileExt);
                string tempFileName = GetTempImagePath(fileId);
                file.SaveAs(tempFileName);

                fileItems.Add(new { FileId = fileId, Name = file.FileName, Size = file.ContentLength });
            }
            Response.ContentType = "application/json";
            string jsonStr = ProviderFactory.Instance().GetSerializer().Serialize(fileItems);
            Response.Write(jsonStr);
            Response.End();

            string back = string.Format("?filter=0&seller={0}&shop={1}&page=1", firstStore, firstShopCode);
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        private string GetTempImagePath(string fileId)
        {

            string imageTempFolder = Path.Combine(
                ProviderFactory.Instance().GetConfig().WebTempPath, WebStorage._IMAGE_TEMP);
            if (Directory.Exists(imageTempFolder) == false)
            {
                Directory.CreateDirectory(imageTempFolder);
            }
            return Path.Combine(imageTempFolder, fileId);
        }

        private void UploadPhoto(HttpPostedFileBase uploadedFile)
        {
            if (uploadedFile != null && uploadedFile.ContentLength > 0)//使用者有選擇照片檔案
            {
                //新的檔案名稱
                string strFileName = Guid.NewGuid().ToString() + Path.GetExtension(uploadedFile.FileName);
                //存放檔案路徑
                string strFilePath = Server.MapPath("~/media/" + strFileName);
                //檔案存放在Server
                uploadedFile.SaveAs(strFilePath);
                //ViewModel的PhotoFileNames累加一張圖片名稱
                ViewBag.PhotoFileNames.Add(strFileName);

            }
        }

        [HttpPost]
        public ActionResult ShopGetBySellerGuid(string sellerGuid)
        {
            Guid guid = new Guid(sellerGuid);

            int userId = Convert.ToInt32(User.Identity.GetPropertyValue("Id"));
            if (userId == 0) userId = MemberFacade.GetUniqueId(User.Identity.Name);

            var roleShoppeGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, _config.SkmRootSellerGuid).ToList();

            var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(guid, false).ToList()
                .Where(x => roleShoppeGuids.Contains(x.StoreGuid ?? Guid.Empty))
                .GroupBy(x => new { x.ShopCode, x.ShopName },
                    (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName });

            var shops = sellerGuid == DefaultSeller ? skmShoppeShopCol.OrderBy(s => int.Parse(Regex.Match(s.shopCode, @"\d+").Value))
               .Select(s => new { key = s.shopCode, value = s.shopName }).ToList() :
               skmShoppeShopCol.Select(s => new { key = s.shopCode, value = s.shopName }).ToList();

            return Json(new { Result = shops.Any(), Shops = shops });
        }

        [HttpGet]
        public ActionResult SkmCenterLogin()
        {
            //debug為除錯方便先用90秒，上到正式要先改為10秒
            SKMCenterLoginUtility sKMCenterLoginUtility = new SKMCenterLoginUtility(90);

            Dictionary<string, string> data = new Dictionary<string, string>();
            
            NameValueCollection myquery = Request.QueryString;

            foreach (string key in myquery.Keys)
            {
                data.Add(key, myquery[key]);
            }

            //先驗證時間
            string time = (Request.QueryString["time"] == null) ? "0" : Request.QueryString["time"];
            long timeStamp = 0;

            try
            {
                timeStamp = Convert.ToInt64(time);
            }
            catch (Exception ex)
            {
                data.Add("error","true");
                data.Add("errorMessage", ex.ToString());
            }

            if (timeStamp > sKMCenterLoginUtility.CheckStartTimeStamp && timeStamp < sKMCenterLoginUtility.CheckEndTimeStamp)
            {

                //時間驗證成功，再來驗證網址
                string pageUrl = string.Format("{0}{1}", Request.Url.Host, Request.Path);

                string sign = sKMCenterLoginUtility.SignInfo(pageUrl, Request.QueryString);

                //debug用，顯示簽章讓串接無誤,上到正式時需移除
                data.Add("signinfo", sign);

                bool verifyUrl = sKMCenterLoginUtility.CheckSign(pageUrl, Request.QueryString);

                if (verifyUrl)
                {
                    //取得商品代碼
                    string itemId = (Request.QueryString["itemId"] == null) ? "0" : Request.QueryString["itemId"];
                    string accountTemp = (Request.QueryString["account"] == null) ? string.Empty : Request.QueryString["account"];

                    if (itemId.Equals("0"))
                    {
                        data.Add("error", "true");
                        data.Add("errorMessage", "商品代碼不正確");
                    }
                    else
                    {
                        string account = accountTemp;

                        if (accountTemp.IndexOf('@') <= 0)
                        {
                            account = string.Format("{0}@skm.com.tw", accountTemp);
                        }

                        //取得身份資料
                        MemberLinkCollection mlCol = null;
                        Member mem;
                        string userName;
                        SingleSignOnSource source;

                        LunchKingSite.WebLib.Component.MemberActions.SKMCenterLoginProvider sKMCenterLoginProvider = new Component.MemberActions.SKMCenterLoginProvider();

                        SignInReply reply = sKMCenterLoginProvider.Login(account, "", true, ref mlCol, out userName, out source, out mem);

                        string replyMessage = reply.GetAttributeOfType<DescriptionAttribute>().Description;
                        if (reply == SignInReply.Success)
                        {
                            PasswordErrorTimesSession.Current.Reset();

                            //取得導入網址
                            string path = (Request.QueryString["Path"] == null) ? string.Empty : Request.QueryString["Path"];

                            if (path.Equals("SKMList") || path.Equals("SKMListForAlibaba") )
                            {
                                //導入其它頁另外再寫
                                return RedirectToAction("SKMListForAlibaba", new { itemId = itemId });
                            }

                            return RedirectToAction("SKMPayDealInfoForAlibaba", new { itemId = itemId });
                        }
                        else
                        {
                            string errorMessage = sKMCenterLoginUtility.GetDescription(reply);

                            data.Add("error", "true");
                            data.Add("errorMessage", errorMessage);
                        }
                    }
                }
                else
                {
                    data.Add("error", "true");
                    data.Add("errorMessage", "簽章驗證失敗");
                }
            }
            else 
            {
                data.Add("error", "true");
                data.Add("errorMessage", "時間驗證錯誤，網址已失效");
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestCreateComplete(long itemId = 110000000000000)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            string getResult = "Done";

            SKMCenterRocketMQTrans.SKMItemCreateComplete(itemId);

            data.Add("message", getResult);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestCategoryModify(long categoryId = 5)
        {
            SKMCenterRocketMQCategoryEntity sKMCenterRocketMQCategoryEntity = new SKMCenterRocketMQCategoryEntity();

            sKMCenterRocketMQCategoryEntity.id = categoryId;

            SKMCenterRocketMQTrans.SKMCenterCategoryModify(sKMCenterRocketMQCategoryEntity);

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("message", "success!!");

            return Json(data, JsonRequestBehavior.AllowGet);
        }
        public ActionResult TestSKMCenterCategory(long itemId = 10)
        {
            Dictionary<string, string> data = new Dictionary<string, string>();

            SKMCenterCategoryUtility sKMCenterCategoryUtility = new SKMCenterCategoryUtility();

            string getResult = sKMCenterCategoryUtility.CreateOrModifyCategory(itemId);
            //string getResult = sKMCenterCategoryUtility.DeleteCategory(itemId);

            data.Add("message", getResult);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public ActionResult testUpdateCategory (long itemId = 110000000000000, string categoryId = "2" , bool isAdd = false) {
            SKMCenterCategoryUtility sKMCenterCategoryUtility = new SKMCenterCategoryUtility();

            bool data = sKMCenterCategoryUtility.setExternalDealCategory(itemId, categoryId, isAdd);
            
            return Json(data , JsonRequestBehavior.AllowGet);
        }

        public ActionResult TestApiInfo(string pampasCall = "item.info", long itemId = 2001021055, string account = "test_skm@skm.com")
        {
            //取得skm openapi資料
            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();
            string returnTransData = sKMOpenAPIMain.GetItemInfo(itemId);

            switch (pampasCall)
            {
                case "item.info":
                    returnTransData = sKMOpenAPIMain.GetItemInfo(itemId);
                    break;
                case "full.spu.info":
                    returnTransData = sKMOpenAPIMain.GetFullSpuInfo(itemId);
                    break;
                case "skuTemplate.info":
                    returnTransData = sKMOpenAPIMain.GetSkuTemplateInfo(itemId);
                    break;
                case "channel.category.info":
                    returnTransData = sKMOpenAPIMain.GetChannelCategoryInfo(itemId);
                    break;
                case "channel.category.binding.info":
                    returnTransData = sKMOpenAPIMain.GetChannelCategoryBindingInfo(itemId);
                    break;
                case "user.auth":
                    returnTransData = sKMOpenAPIMain.GetUserAuth(account);
                    break;
                default:
                    returnTransData = sKMOpenAPIMain.GetItemInfo(itemId);
                    break;
            }


            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("returnMessage", returnTransData);

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult RocketMQ(string order_number = "12345", string warehouseCode = "W001", string skuId = "110000000000999")
        {
            //測試庫存api傳送資料
            InventoryChangeEntity inventory = new InventoryChangeEntity();
            inventory.entityId = skuId;
            inventory.bizSrcId = order_number;
            inventory.quantity = 5;
            inventory.warehouseCode = warehouseCode;

            List<InventoryChangeEntity> inventoryChangeEntities = new List<InventoryChangeEntity>();

            inventoryChangeEntities.Add(inventory);

            SKMCenterRocketMQTrans.InventoryOccupy(inventoryChangeEntities);

            Dictionary<string, string> data = new Dictionary<string, string>();

            data.Add("test","data");

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 取得店/館/櫃位資料
        /// ViewBag.SKMAccount
        /// ViewBag.SKMStores
        /// ViewBag.SKMShoppes 
        /// ViewBag.SKMCategory
        /// ViewBag.SKMDefaultStore
        /// </summary>
        /// <param name="hasRoot"></param>
        private void SetPageDefault(bool hasRoot = false, bool isContainsNotAvailableShoppe = false, bool isConnectSKM = false)
        {

            int userId = Convert.ToInt32(User.Identity.GetPropertyValue("Id"));
            if (userId == 0) userId = MemberFacade.GetUniqueId(User.Identity.Name);

            bool administrator = false;

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, _config.SkmRootSellerGuid, ref administrator).ToList();
            var mainStoreRoles = ChannelFacade.GetOwnOneDownSeller(sellerRoleGuids, _config.SkmRootSellerGuid);

            //all
            var tempSellerCol = _sp.SellerGetSkmParentList().ToList().Where(x => mainStoreRoles.Contains(x.Guid));

            Dictionary<string, bool> allStoreChecked = new Dictionary<string, bool>();

            Dictionary<string, string> allStore = tempSellerCol.OrderByDescending(
                x => string.Format("{0},{1}", LocationFacade.GetGeographyByCoordinate(x.Coordinate).Lat.ToString()
                , LocationFacade.GetGeographyByCoordinate(x.Coordinate).Long.ToString()))
                .ToDictionary(x => x.Guid.ToString(), x => x.SellerName);

            if (hasRoot)
            {
                allStore.Add(_config.SkmRootSellerGuid.ToString(), "總公司");
            }

            IEnumerable<SkmShoppe> tempShoppeCol;

            //管理者有多達1萬多的點，比對上太花時間
            if (!administrator)
            {
                tempShoppeCol = _sp.SkmShoppeCollectionGetAll(false, isContainsNotAvailableShoppe).Where(x => sellerRoleGuids.Contains(x.StoreGuid ?? Guid.Empty));
            }
            else
            {
                tempShoppeCol = _sp.SkmShoppeCollectionGetAll(false, isContainsNotAvailableShoppe);
            }

            var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll();

            List<Shoppes> allShoppe = tempShoppeCol.OrderBy(x => x.ShopCode).ThenBy(x => x.BrandCounterCode).Select(x => new Shoppes
            {
                Name = string.Format("{0}（{1}）", x.BrandCounterName, x.BrandCounterCode),
                StoreGuid = x.StoreGuid.ToString(),
                ShopName = x.BrandCounterName,
                ShopCode = x.ShopCode,
                SellerGuid = x.SellerGuid.ToString(),
                IsShoppe = x.IsShoppe,
                IsShow = false,
                IsExchange = x.IsExchange,
                MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == x.ShopCode && y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == x.ShopCode && y.StoreName.Contains("超市")).CostCenterCode : string.Empty,
                SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == x.ShopCode && !y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == x.ShopCode && !y.StoreName.Contains("超市")).CostCenterCode : string.Empty,
                ParentSellerGuid = x.ParentSellerGuid
            }).ToList();


            if (hasRoot)
            {
                allShoppe.Insert(0, new Shoppes()
                {
                    Name = "總公司",
                    StoreGuid = _config.SkmRootSellerGuid.ToString(),
                    ShopName = "總公司",
                    ShopCode = "001",
                    SellerGuid = _config.SkmRootSellerGuid.ToString(),
                    IsShoppe = false,
                    IsShow = false,
                    IsExchange = false,
                    MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001") != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001").CostCenterCode : string.Empty,
                    SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001") != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001").CostCenterCode : string.Empty,
                    ParentSellerGuid = _config.SkmRootSellerGuid
                });

                allShoppe.Insert(1, new Shoppes()
                {
                    Name = "總公司",
                    StoreGuid = _config.SkmRootSellerGuid.ToString(),
                    ShopName = "總公司",
                    ShopCode = "001",
                    SellerGuid = _config.SkmRootSellerGuid.ToString(),
                    IsShoppe = true,
                    IsShow = false,
                    IsExchange = false,
                    MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001") != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001").CostCenterCode : string.Empty,
                    SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001") != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == "001").CostCenterCode : string.Empty,
                    ParentSellerGuid = _config.SkmRootSellerGuid
                });
            }
            Dictionary<int, string> allCategory = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealCategory)
                .OrderBy(x => x.Seq).ToDictionary(x => x.CategoryId, x => x.CategoryName);

            bool setStoreChecked = false;

            if (isConnectSKM)
            {
                //門市權限需要從Alibaba來的時候使用
                SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();

                string skmCenterAccount = User.Identity.Name;

                if (skmCenterAccount.IndexOf('@') > 0 && !skmCenterAccount.Equals("test_skm@skm.com"))
                {
                    skmCenterAccount = User.Identity.Name.Substring(0, User.Identity.Name.IndexOf('@'));
                }

                string getReturn = sKMOpenAPIMain.GetUserAuth(skmCenterAccount);

                //if(  )

                SKMCenterUserAuthMain getUserAuth = null;
                SKMCenterUserAuthMainError getUserAuthError = null;

                try
                {
                    getUserAuth = JsonConvert.DeserializeObject<SKMCenterUserAuthMain>(getReturn);
                }
                catch (Exception ex)
                {
                    getUserAuthError = JsonConvert.DeserializeObject<SKMCenterUserAuthMainError>(getReturn);

                    setStoreChecked = false;

                    //無帳號時館全部唯讀
                    foreach (KeyValuePair<string, string> eachStore in allStore)
                    {  
                        allStoreChecked.Add(eachStore.Key, false);
                    }
                }

                //非alibaba管理者時，需鎖使用者權限
                if ( getUserAuth != null && !getUserAuth.result.primary)
                {
                    //var checkData = allShoppe.Select(a => a.SellerGuid)
                    //foreach (Shoppes shoppes in allShoppe)
                    //{ 
                    //    shoppes.
                    //}
                    Dictionary<string, bool> keyValuePairs = new Dictionary<string, bool>();
                    foreach (string SkmCenterShopCode in getUserAuth.result.storeList)
                    {
                        keyValuePairs.Add(SkmCenterShopCode, true);
                    }

                    //驗證是否有資料
                    Dictionary<string, string> addControl = new Dictionary<string, string>();

                    List<Shoppes> realAllShoppes = new List<Shoppes>();

                    foreach (Shoppes shoppes in allShoppe)
                    {
                        if (keyValuePairs.ContainsKey(shoppes.ShopCode) && !addControl.ContainsKey(shoppes.ParentSellerGuid.ToString()))
                        {
                            addControl.Add(shoppes.ParentSellerGuid.ToString(), shoppes.ShopCode);
                        }

                        //只留有權限的門店
                        if (keyValuePairs.ContainsKey(shoppes.ShopCode))
                        {
                            realAllShoppes.Add(shoppes);
                        }
                    }

                    allShoppe = realAllShoppes;

                    //把館別設定是否可編輯
                    foreach (KeyValuePair<string, string> eachStore in allStore)
                    {
                        if (addControl.ContainsKey(eachStore.Key))
                        {
                            allStoreChecked.Add(eachStore.Key, true);
                        }
                        else
                        {
                            allStoreChecked.Add(eachStore.Key, false);
                        }
                    }

                }
                else if(getUserAuth != null)
                {
                    setStoreChecked = true;
                }
            }
            else
            {
                setStoreChecked = true;
            }

            if (setStoreChecked)
            {
                foreach (KeyValuePair<string, string> eachStore in allStore)
                {
                    allStoreChecked.Add(eachStore.Key, true);
                }
            }

            KeyValuePair<string, bool>[] sellerCanEditArray = new KeyValuePair<string, bool>[allStoreChecked.Count];

            int index = 0;
            foreach (KeyValuePair<string, bool> eachStore in allStoreChecked)
            {
                sellerCanEditArray[index] = eachStore;
                index++;
            }

            ViewBag.SKMAccount = SkmFacade.CheckSkmAccount(User.Identity.Name);
            ViewBag.SKMStores = allStore;
            ViewBag.SKMShoppes = allShoppe;
            ViewBag.SKMCategory = allCategory;
            ViewBag.SKMDefaultStore = DefaultSeller;
            ViewBag.SKMLockChose = sellerCanEditArray;
        }

        [HttpGet]
        public ContentResult GetShopData(string sg)
        {
            SetPageDefault();
            string myDefaultSeller = DefaultSeller; //信義新天地

            //有帶分店default就改成帶進來的店
            Guid tempSg;
            if (sg != string.Empty && Guid.TryParse(sg, out tempSg))
            {
                myDefaultSeller = sg;
            }

            //有權限的分店中沒有傳入的分店，就從有限權分店取第一筆
            var storeKeys = ((Dictionary<string, string>)ViewBag.SKMStores).Keys;
            if (storeKeys.Any() && !storeKeys.Contains(myDefaultSeller))
            {
                myDefaultSeller = storeKeys.First();
            }

            var seller = myDefaultSeller;

            Guid sellerGuid = new Guid(seller);

            var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
            var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(sellerGuid, false).ToList()
                .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
                .GroupBy(x => new { x.ShopCode, x.ShopName },
                    (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
                .OrderBy(s => s.shopCode);

            Dictionary<string, string> shops = skmShoppeShopCol.ToDictionary(x => x.shopCode, x => x.shopName);

            var storeData = new StoreData();

            foreach (var t in (Dictionary<string, string>)ViewBag.SKMStores)
            {
                var isDefault = t.Key == myDefaultSeller;
                var store = new SelectData { Key = t.Key, Value = t.Value, IsDefault = isDefault };
                storeData.Store.Add(store);
            }

            foreach (var t in shops)
            {
                storeData.Shop.Add(new SelectData { Key = t.Key, Value = t.Value });
            }

            return Content(JsonConvert.SerializeObject(storeData), "application/json");
        }

        private static object GetPropertyValue(object obj, string property)
        {
            System.Reflection.PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
            return propertyInfo.GetValue(obj, null);
        }

        private static object GetPropertyValue(object obj, string property, bool IsUpper) //isupper: end_date -> EndDate(拿掉底線)
        {
            if (property.IndexOf("_") != -1 && IsUpper)
            {
                property = (property.Split("_")[0][0].ToString().ToUpper() + property.Split("_")[0].Substring(1) +
                            property.Split("_")[1][0].ToString().ToUpper() + property.Split("_")[1].Substring(1));
            }
            //else
            //{
            //    property = property.Substring(0, 1).ToUpper() + property.Substring(1);
            //}

            System.Reflection.PropertyInfo propertyInfo = obj.GetType().GetProperty(property);
            return propertyInfo.GetValue(obj, null);
        }

        private static DateTime? ConvertDateAndTime(string date, string time)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(string.Format("{0} {1}", date, time), out dt))
            {
                return dt;
            }
            return null;
        }

        /// <summary>
        /// 取得新光賣家
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> SellersGet()
        {
            return _sp.SellerGetSkmParentList()
                .OrderByDescending(x => string.Format("{0},{1}", LocationFacade.GetGeographyByCoordinate(x.Coordinate).Lat.ToString(), LocationFacade.GetGeographyByCoordinate(x.Coordinate).Long.ToString()))
                .ToDictionary(x => x.Guid.ToString(),
                                x => x.SellerName
                                );
        }

        /// <summary>
        /// 依登入帳號取得有權限的分店
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> SellersGetByUserId()
        {
            //取得所有分店
            Dictionary<string, string> allSeller = SellersGet();
            //取得有權限的seller
            int userId = Convert.ToInt32(User.Identity.GetPropertyValue("Id"));
            if (userId == 0) userId = MemberFacade.GetUniqueId(User.Identity.Name);

            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, _config.SkmRootSellerGuid).ToList();
            var mainStoreRoles = ChannelFacade.GetOwnOneDownSeller(sellerRoleGuids, _config.SkmRootSellerGuid);
            return allSeller.Where(item => mainStoreRoles.Contains(new Guid(item.Key))).ToDictionary(item => item.Key, item => item.Value);
        }

        /// <summary>
        /// 取得新光分類
        /// </summary>
        /// <returns></returns>
        private Dictionary<string, string> CategoryTypeGet()
        {
            List<ViewCategoryDependency> dependencyCol = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryStatus == (int)CategoryStatus.Enabled).ToList();

            Dictionary<string, string> cats = new Dictionary<string, string>();
            foreach (var vcdc in dependencyCol.OrderBy(x => x.Seq))
            {
                if (vcdc.CategoryType == (int)CategoryType.DealCategory)
                {
                    cats.Add(vcdc.CategoryId.ToString(), vcdc.CategoryName.ToString());
                }
            }
            return cats;
        }

        private dynamic getFloor(Guid sellerGuid, string shopCode)
        {
            List<ActionEventDeviceInfo> beacons = new List<ActionEventDeviceInfo>();
            if (SkmActionEventInfoId != 0)
            {
                beacons = _mp.ActionEventDeviceInfoListGet(SkmActionEventInfoId).Where(x => x.ShopCode == shopCode && x.Floor != null).ToList();
            }

            return beacons.Select(x => new { x.Floor })
                .Distinct().OrderBy(x => x.Floor);
        }

        [HttpPost]
        public ActionResult GetProductCode(string code, bool bForAli)
        {
            logger.Info("GetProductCode(" + code + ").Into");
            string flag;
            string result;
            string data = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(bForAli ? "http://api.skm.com.tw:8090/PlusInfo.aspx" : "https://api.skm.com.tw/PlusInfo.aspx");

            request.Method = "POST";
            request.ContentType = "text/json";
            try
            {
                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = "{\"PLU_NO\":\"" + code + "\"}";
                    streamWriter.Write(json);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                var httpResponse = (HttpWebResponse)request.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }
                JObject jb = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(result);
                if (jb.Property("STATUS_NO").Value.ToString() == ((int)SkmItemNoApiResult.Success).ToString())
                {
                    data = result;
                    flag = jb.Property("STATUS_NO").Value.ToString();
                }
                else
                {
                    data = jb.Property("MESSAGE_TX").Value.ToString();
                    flag = jb.Property("STATUS_NO").Value.ToString();
                }

                _skm.SkmLogSet(new SkmLog()
                {
                    SkmToken = "GetProductCode",
                    UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                    LogType = (int)SkmLogType.GetProductCode,
                    InputValue = code,
                    OutputValue = result
                });

            }
            catch (Exception ex)
            {
                _skm.SkmLogSet(new SkmLog()
                {
                    SkmToken = "GetProductCode",
                    UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                    LogType = (int)SkmLogType.GetProductCode,
                    InputValue = code,
                    OutputValue = ex.ToString()
                });
                logger.ErrorFormat("{0}->{1}", ex.Message, ex.StackTrace);
                flag = "";
            }

            return Json(new { Result = flag, Message = data });
        }

        private List<int> SkmHqCategoriesConvertToList()
        {
            var cateCol = _config.SkmHQCategories.Split(',');
            var hqCategoryCol = cateCol.Any() ? cateCol.Select(int.Parse).ToList() : new List<int>();
            return hqCategoryCol;
        }

        #region 首頁活動Banner管理 SkmAppBanner
        [HttpGet]
        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public ActionResult SkmAppBanner(string qSeller, int? page, string order, bool? isOrderASC)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            Dictionary<string, string> sellers = SellersGetByUserId();

            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }
            if (order == null)
            {
                order = MerchantEventPromo.Columns.StartDate;
            }

            List<MerchantEventPromo> promos = null;
            ViewBag.SkmSellers = sellers;
            ViewData["qSeller"] = qSeller;

            #region 分頁
            int pageCnt = 30;
            var data = _pp.MerchantEventPromoListGet(MerchantEventType.Skm).Where(x => x.SellerGuid == Guid.Parse(qSeller));
            int totalLogCount = data.Count();

            ViewData["Order"] = order ?? MerchantEventPromo.Columns.Sort;
            ViewData["IsOrderASC"] = isOrderASC ?? false;
            if (isOrderASC ?? false)
            {
                promos = (from q in data
                          orderby GetPropertyValue(q, order)
                          select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }
            else
            {
                promos = (from q in data
                          orderby GetPropertyValue(q, order) descending
                          select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }

            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / pageCnt;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalLogCount % pageCnt > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            #endregion

            return View(promos);
        }
        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public ActionResult SkmAppBannerSort(string qSeller)
        {
            Dictionary<string, string> sellers = SellersGetByUserId();
            if (qSeller == null)
            {
                qSeller = DefaultSeller;
            }
            ViewBag.SkmSellers = sellers;

            ViewData["qSeller"] = qSeller;

            List<MerchantEventPromo> promos = _pp.MerchantEventPromoListGet(MerchantEventType.Skm)
                .Where(x => x.Status == (int)SkmAppStyleStatus.Normal && x.SellerGuid == Guid.Parse(qSeller) && x.StartDate <= DateTime.Now && x.EndDate >= DateTime.Now)
                .OrderBy(x => x.Sort)
                .Take(20).ToList();
            return View(promos);
        }

        [HttpPost]
        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public JsonResult SkmAppBannerSortUpdate(string input)
        {
            bool flag = true;
            try
            {
                List<SkmAppBannerSort> data = new JsonSerializer().Deserialize<List<SkmAppBannerSort>>(input);
                foreach (SkmAppBannerSort sbs in data)
                {
                    MerchantEventPromo promo = _pp.MerchantEventPromoGet(sbs.Id);
                    promo.Sort = sbs.Sort;
                    _pp.MerchantEventPromoSet(promo);
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = "" });
        }

        [HttpPost]
        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public JsonResult SkmAppBannerStatusUpdate(Guid guid)
        {
            bool flag = true;
            int newStatus = (int)SkmAppStyleStatus.Hide;
            try
            {
                MerchantEventPromoCollection promos = _pp.MerchantEventPromoGet(guid);
                if (promos.Count > 0)
                {
                    if (promos.FirstOrDefault().Status == (int)SkmAppStyleStatus.Hide)
                    {
                        newStatus = (int)SkmAppStyleStatus.Normal;
                    }
                    foreach (var promo in promos)
                    {
                        promo.Status = newStatus;
                        _pp.MerchantEventPromoSet(promo);
                    }
                }

            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = newStatus });
        }

        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public ActionResult SkmAppBannerEdit(Guid? guid, Guid? qSeller)
        {
            MerchantEventPromo promo = new MerchantEventPromo();
            Dictionary<string, string> sellers = SellersGetByUserId();

            if (guid != null)
            {
                promo = _pp.MerchantEventPromoGet(guid ?? Guid.Empty).FirstOrDefault();
            }
            ViewBag.SkmSellers = sellers;

            if (!string.IsNullOrEmpty(promo.MainPic))
            {
                promo.MainPic = SkmFacade.SkmAppStyleImgPath(promo.MainPic);
            }
            ViewData["qSeller"] = qSeller.ToString();
            return View(promo);
        }

        [HttpPost]
        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        public ActionResult SkmAppBannerEdit(
            Guid? guid,
            string Title,
            string Url,
            string EventBeginDate,
            string EventBeginHour,
            string EventBeginMinute,
            string EventEndDate,
            string EventEndHour,
            string EventEndMinute,
            string Location,
            string MainPic,
            Guid qSeller,
            string IsMustLogin
            )
        {
            Guid _Guid = Guid.Empty;
            MerchantEventPromoCollection promos = _pp.MerchantEventPromoGet(guid ?? Guid.Empty);
            if (promos.Count > 0)
            {
                _Guid = promos.FirstOrDefault().Guid ?? Guid.Empty;
            }
            else
            {
                _Guid = Guid.NewGuid();
            }
            DateTime StartDate = DateTime.Now;
            DateTime.TryParse(EventBeginDate + " " + EventBeginHour + ":" + EventBeginMinute, out StartDate);
            DateTime EndDate = DateTime.Now;
            DateTime.TryParse(EventEndDate + " " + EventEndHour + ":" + EventEndMinute, out EndDate);

            string fileName = string.Empty;
            string oriFileName = string.Empty;
            bool checkFile = false;
            if (_config.EnableCustomizedBoardList)
            {
                var imgFileResult = SkmFacade.CheckImgType(1125, 750, "SkmEventPromoAppBanner", _Guid);

                if (imgFileResult.SuccessLoadFile)
                {
                    if (!imgFileResult.ImgFiles[0].IsExtensionPass)
                    {
                        ModelState.AddModelError("MainPic", "只可上傳jpg或png格式的圖檔");
                    }
                    if (!imgFileResult.ImgFiles[0].IsSizePass)
                    {
                        ModelState.AddModelError("MainPic", "上傳圖片尺寸限制為1125*750");
                    }
                    if (Request.Files[0].ContentLength > (1 * 1024 * 1024))
                    {
                        ModelState.AddModelError("MainPic", "檔案大小不得超過1MB");
                    }

                    if (imgFileResult.ImgFiles[0].IsExtensionPass
                        && imgFileResult.ImgFiles[0].IsSizePass
                        && Request.Files[0].ContentLength < (1 * 1024 * 1024)) //img驗證成功
                    {
                        fileName = imgFileResult.ImgFiles.First().SaveFileName;
                        oriFileName = imgFileResult.ImgFiles.First().OriFileName;
                    }
                }
                else if (string.IsNullOrEmpty(MainPic))
                {
                    ModelState.AddModelError("MainPic", "此為必填欄位！");
                }

                if (!ModelState.IsValid) // model驗證不合法則return View
                {
                    ViewBag.SkmSellers = SellersGetByUserId();
                    ViewData["qSeller"] = qSeller.ToString();
                    TempData["validateResult"] = new JsonSerializer().Serialize(ModelState.Where(item => item.Value.Errors.Any()).Select(item => { return new { Key = item.Key, Message = string.Join("<br/>", item.Value.Errors.Select(error => { return error.ErrorMessage; })) }; }));
                    return View(new MerchantEventPromo()
                    {
                        Guid = guid,
                        Title = Title,
                        Url = Url,
                        StartDate = StartDate,
                        EndDate = EndDate,
                        Location = Location,
                        MainPic = MainPic,
                        IsMustLogin = IsMustLogin == "on",
                    });
                }
            }
            else
            {
                var imgFileResult = SkmFacade.CheckImgType(640, 344, "SkmEventPromoAppBanner", _Guid);
                if ((imgFileResult.SuccessLoadFile && imgFileResult.ImgFiles.Any()))
                {
                    checkFile = true;
                    fileName = imgFileResult.ImgFiles.First().SaveFileName;
                    oriFileName = imgFileResult.ImgFiles.First().OriFileName;
                }
                else if (!string.IsNullOrEmpty(MainPic))
                {
                    checkFile = true;
                }

                if (!checkFile)
                {
                    return Content("<script>alert('僅能上傳圖片檔案，大小為 640 x 344');history.go(-1);</script>");
                }
            }


            string[] _Locations = Location.Split(",");
            if (promos.Count > 0)
            {
                //update
                foreach (var promo in promos)
                {
                    //刪除多餘的資料
                    if (!_Locations.Contains(promo.SellerGuid.ToString()))
                    {
                        _pp.MerchantEventPromoDelete(promo.Id);
                    }
                }
                foreach (string _lo in _Locations)
                {
                    Guid _nGuid = Guid.Empty;
                    if (!Guid.TryParse(_lo, out _nGuid))
                    {
                        continue;
                    }
                    var promo = promos.Where(x => x.SellerGuid == _nGuid).FirstOrDefault();
                    if (promo != null)
                    {
                        promo.Title = Title;
                        promo.Url = Url;
                        promo.StartDate = StartDate;
                        promo.EndDate = EndDate;
                        promo.Location = Location;
                        promo.SellerGuid = _nGuid;
                        promo.IsMustLogin = IsMustLogin == "on" && Url.ToLower().Contains("17life.com");
                        if (fileName != "")
                        {
                            promo.MainPic = fileName;
                        }
                        promo.ModifyDate = DateTime.Now;
                        promo.ModifyUser = UserName;

                        _pp.MerchantEventPromoSet(promo);
                    }
                    else
                    {
                        MerchantEventPromo npromo = new MerchantEventPromo();
                        npromo.Title = Title;
                        npromo.Url = Url;
                        npromo.StartDate = StartDate;
                        npromo.EndDate = EndDate;
                        npromo.Location = Location;
                        npromo.SellerGuid = Guid.Parse(_lo);
                        if (fileName != "")
                        {
                            npromo.MainPic = fileName;
                        }
                        npromo.Type = (int)MerchantEventType.Skm;
                        npromo.Status = (int)SkmAppStyleStatus.Normal;
                        npromo.Sort = _pp.MerchantEventPromoListGet(MerchantEventType.Skm).Where(x => x.Location == _lo).ToList().Count + 1;
                        npromo.CreateDate = DateTime.Now;
                        npromo.CreateUser = UserName;
                        npromo.ModifyDate = DateTime.Now;
                        npromo.ModifyUser = UserName;
                        npromo.Guid = _Guid;
                        npromo.IsMustLogin = IsMustLogin == "on" && Url.ToLower().Contains("17life.com");
                        _pp.MerchantEventPromoSet(npromo);
                    }
                }
            }
            else
            {
                //insert
                foreach (string _lo in _Locations)
                {
                    Guid _nGuid = Guid.Empty;
                    if (!Guid.TryParse(_lo, out _nGuid))
                    {
                        continue;
                    }
                    MerchantEventPromo npromo = new MerchantEventPromo();
                    npromo.Title = Title;
                    npromo.Url = Url;
                    npromo.StartDate = StartDate;
                    npromo.EndDate = EndDate;
                    npromo.Location = Location;
                    npromo.SellerGuid = _nGuid;
                    if (fileName != "")
                    {
                        npromo.MainPic = fileName;
                    }
                    npromo.Type = (int)MerchantEventType.Skm;
                    npromo.Status = (int)SkmAppStyleStatus.Normal;
                    npromo.Sort = _pp.MerchantEventPromoListGet(MerchantEventType.Skm).Where(x => x.SellerGuid == _nGuid).ToList().Count + 1;
                    npromo.CreateDate = DateTime.Now;
                    npromo.CreateUser = UserName;
                    npromo.ModifyDate = DateTime.Now;
                    npromo.ModifyUser = UserName;
                    npromo.Guid = _Guid;
                    npromo.IsMustLogin = IsMustLogin == "on" && Url.ToLower().Contains("17life.com");
                    _pp.MerchantEventPromoSet(npromo);

                }

            }

            if (fileName != string.Empty && oriFileName != string.Empty)
            {
                HttpPostedFileBase hpf = Request.Files[oriFileName] as HttpPostedFileBase;
                hpf.SaveAs(imgUploadDirPath + fileName);
            }

            return RedirectToAction("SkmAppBanner", new { qSeller = qSeller });
        }

        private List<ViewSKMAppDefaultStyle> ViewSKMAppDefaultStyleListGet(string typeName, string cid, Guid gid)
        {
            List<ViewSKMAppDefaultStyle> returnList = new List<ViewSKMAppDefaultStyle>();

            if (typeName.Contains("活動"))
            {
                var data = SkmFacade.SkmLatestActivityGetBySellerGuid(gid).ToList();
                data = data.Where(x => x.CategoryId == Convert.ToInt32(cid)).ToList();
                foreach (var item in data)
                {
                    ViewSKMAppDefaultStyle ViewSKMAppDefault = new ViewSKMAppDefaultStyle();
                    ViewSKMAppDefault.SellrtGuid = item.SellerGuid;
                    ViewSKMAppDefault.Guid = item.Guid;
                    ViewSKMAppDefault.EDate = item.EDate;
                    ViewSKMAppDefault.SDate = item.SDate;
                    ViewSKMAppDefault.status = item.Status;
                    ViewSKMAppDefault.Title = item.ActivityTitle;
                    ViewSKMAppDefault.Sort = item.Sort;
                    returnList.Add(ViewSKMAppDefault);
                }
            }
            else
            {
                var data = SkmFacade.SkmLatestSellerDealGetBySellerGuid(gid).ToList();
                data = data.Where(x => x.CategoryId == Convert.ToInt32(cid)).ToList();
                foreach (var item in data)
                {
                    ViewSKMAppDefaultStyle ViewSKMAppDefault = new ViewSKMAppDefaultStyle();
                    ViewSKMAppDefault.SellrtGuid = item.SellerGuid;
                    ViewSKMAppDefault.Guid = item.Guid;
                    ViewSKMAppDefault.EDate = item.EDate;
                    ViewSKMAppDefault.SDate = item.SDate;
                    ViewSKMAppDefault.status = item.Status;
                    ViewSKMAppDefault.Title = item.DealTitle;
                    returnList.Add(ViewSKMAppDefault);
                }
            }
            return returnList;

        }
        #endregion

        #region 首頁版型設定 SkmAppDefaultStyle
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDefaultStyle(string qSeller, int? Sequence, int? page, string order, bool? isOrderASC, string category)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            Dictionary<string, string> sellers = SellersGetByUserId();

            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }
            if (order == null)
            {
                order = SkmLatestActivity.Columns.SDate;
            }
            if (isOrderASC == null)
            {
                isOrderASC = false;
            }

            Dictionary<string, string> cats = CategoryTypeGet();
            Guid seller_guid = Guid.Empty;
            Guid.TryParse(qSeller, out seller_guid);

            if (category == null)
            {
                category = cats.FirstOrDefault().Key;
            }

            var data = ViewSKMAppDefaultStyleListGet(cats[category], category, seller_guid);

            #region 分頁
            int pageCnt = 10;
            List<ViewSKMAppDefaultStyle> datas;
            int totalLogCount = data.Count();

            ViewData["Order"] = order ?? SkmLatestActivity.Columns.SDate;
            ViewData["IsOrderASC"] = isOrderASC ?? false;
            if (isOrderASC ?? true)
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true)
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }
            else
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true) descending
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }

            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / pageCnt;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalLogCount % pageCnt > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            #endregion

            bool isSkmHQOwner = false;
            //判斷是否總公司可編輯的類別
            ViewBag.EditPower = SkmFacade.CheckSkmHqCategoriesPower(userId, category, ref isSkmHQOwner);
            //判斷是否總公司
            ViewBag.SkmHqPower = isSkmHQOwner;
            ViewBag.SkmSellers = sellers;
            ViewBag.SkmCategory = cats;
            ViewBag.Sequence = Sequence;
            ViewData["qSeller"] = qSeller;
            ViewData["qCategory"] = category;
            ViewData["catsName"] = cats[category].Contains("活動") ? "活動" : "商品";
            if (string.IsNullOrEmpty(_config.SkmAppHomeDisableCategory))
            {
                ViewBag.DisableCategory = new List<string>();
            }
            else
            {
                ViewBag.DisableCategory = _config.SkmAppHomeDisableCategory.Split(',').ToList();
            }

            return View(datas);
        }

        /// <summary>
        /// 首頁活動設定
        /// </summary>
        /// <param name="qSeller"></param>
        /// <param name="Sequence"></param>
        /// <param name="page"></param>
        /// <param name="order"></param>
        /// <param name="isOrderASC"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppActivityStyle(string qSeller, int? Sequence, int? page, string order, bool? isOrderASC, string category)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            Dictionary<string, string> sellers = SellersGetByUserId();

            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }
            if (order == null)
            {
                order = SkmLatestActivity.Columns.SDate;
            }
            if (isOrderASC == null)
            {
                isOrderASC = false;
            }

            Dictionary<string, string> cats = CategoryTypeGet().Where(x => x.Key == _config.SkmDealDisableCategory.ToString()).ToDictionary(x => x.Key, x => x.Value);  //最新活動
            Guid seller_guid = Guid.Empty;
            Guid.TryParse(qSeller, out seller_guid);

            if (category == null)
            {
                category = cats.FirstOrDefault().Key;
            }

            var data = ViewSKMAppDefaultStyleListGet(cats[category], category, seller_guid);



            #region 分頁
            int pageCnt = 10;
            List<ViewSKMAppDefaultStyle> datas;
            int totalLogCount = data.Count();

            ViewData["Order"] = order ?? SkmLatestActivity.Columns.SDate;
            ViewData["IsOrderASC"] = isOrderASC ?? false;
            if (isOrderASC ?? true)
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true)
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }
            else
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true) descending
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }

            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / pageCnt;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalLogCount % pageCnt > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            #endregion

            bool isSkmHQOwner = false;
            //判斷是否總公司可編輯的類別
            ViewBag.EditPower = SkmFacade.CheckSkmHqCategoriesPower(userId, category, ref isSkmHQOwner);
            //判斷是否總公司
            ViewBag.SkmHqPower = false;  //停用業種形象圖設定
            ViewBag.SkmSellers = sellers;
            ViewBag.SkmCategory = cats;
            ViewBag.Sequence = Sequence;
            ViewData["qSeller"] = qSeller;
            ViewData["qCategory"] = _config.SkmDealDisableCategory.ToString(); //最新活動
            if (string.IsNullOrEmpty(_config.SkmAppHomeDisableCategory))
            {
                ViewBag.DisableCategory = new List<string>();
            }
            else
            {
                ViewBag.DisableCategory = _config.SkmAppHomeDisableCategory.Split(',').ToList();
            }

            return View(datas);
        }

        /// <summary>
        /// 精選優惠設定
        /// </summary>
        /// <param name="qSeller"></param>
        /// <param name="Sequence"></param>
        /// <param name="page"></param>
        /// <param name="order"></param>
        /// <param name="isOrderASC"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDealStyle(string qSeller, int? Sequence, int? page, string order, bool? isOrderASC, string category)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            Dictionary<string, string> sellers = SellersGetByUserId();

            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }
            if (order == null)
            {
                order = SkmLatestActivity.Columns.SDate;
            }
            if (isOrderASC == null)
            {
                isOrderASC = false;
            }

            Dictionary<string, string> cats = CategoryTypeGet().Where(x => x.Key != _config.SkmDealDisableCategory.ToString()).ToDictionary(x => x.Key, x => x.Value); ; //刪去最新活動;
            Guid seller_guid = Guid.Empty;
            Guid.TryParse(qSeller, out seller_guid);

            if (category == null)
            {
                category = cats.FirstOrDefault().Key;
            }

            var data = ViewSKMAppDefaultStyleListGet(cats[category], category, seller_guid);



            #region 分頁
            int pageCnt = 10;
            List<ViewSKMAppDefaultStyle> datas;
            int totalLogCount = data.Count();

            ViewData["Order"] = order ?? SkmLatestActivity.Columns.SDate;
            ViewData["IsOrderASC"] = isOrderASC ?? false;
            if (isOrderASC ?? true)
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true)
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }
            else
            {
                datas = (from q in data
                         orderby GetPropertyValue(q, order, true) descending
                         select q).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();
            }

            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / pageCnt;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalLogCount % pageCnt > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            #endregion

            bool isSkmHQOwner = false;
            //判斷是否總公司可編輯的類別
            ViewBag.EditPower = SkmFacade.CheckSkmHqCategoriesPower(userId, category, ref isSkmHQOwner);
            //判斷是否總公司
            ViewBag.SkmHqPower = isSkmHQOwner;
            ViewBag.SkmSellers = sellers;
            ViewBag.SkmCategory = cats;
            ViewBag.Sequence = Sequence;
            ViewData["qSeller"] = qSeller;
            ViewData["qCategory"] = category;
            if (string.IsNullOrEmpty(_config.SkmAppHomeDisableCategory))
            {
                ViewBag.DisableCategory = new List<string>();
            }
            else
            {
                ViewBag.DisableCategory = _config.SkmAppHomeDisableCategory.Split(',').ToList();
            }

            return View(datas);
        }

        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppActivityStyleSort(string qSeller)
        {
            Dictionary<string, string> sellers = SellersGetByUserId();
            if (qSeller == null)
            {
                qSeller = DefaultSeller;
            }
            ViewBag.SkmSellers = sellers;

            ViewData["qSeller"] = qSeller;
            Dictionary<string, string> cats = CategoryTypeGet().Where(x => x.Key == _config.SkmDealDisableCategory.ToString()).ToDictionary(x => x.Key, x => x.Value);  //最新活動
            ViewData["qCategory"] = cats.FirstOrDefault().Key;

            Guid seller_guid = Guid.Empty;
            Guid.TryParse(qSeller, out seller_guid);
            List<ViewSKMAppDefaultStyle> data = ViewSKMAppDefaultStyleListGet(cats[cats.FirstOrDefault().Key], cats.FirstOrDefault().Key, seller_guid);

            //判斷是否總公司可編輯的類別 
            bool isSkmHQOwner = false;
            ViewBag.EditPower = SkmFacade.CheckSkmHqCategoriesPower(MemberFacade.GetUniqueId(User.Identity.Name), cats.FirstOrDefault().Key, ref isSkmHQOwner);

            //上檔中邏輯如同頁面StatusGet()
            data = data.Where(p => p.status != 2 && DateTime.Now <= p.EDate && DateTime.Now >= p.SDate).OrderBy(p => p.Sort).ToList();
            return View(data);
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SKMAppActivityStyleSortUpdate(string input)
        {
            bool flag = true;
            try
            {
                List<ViewSKMAppDefaultStyle> data = new JsonSerializer().Deserialize<List<ViewSKMAppDefaultStyle>>(input);
                foreach (ViewSKMAppDefaultStyle d in data)
                {
                    SkmLatestActivity sla = _skm.SkmLatestActivityGet(d.Guid.Value);
                    sla.Sort = d.Sort;
                    _skm.SkmLatestActivitySet(sla);
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = "" });
        }

        public ActionResult SkmAppDefaultStylePublicImag()
        {

            Dictionary<string, string> cats = CategoryTypeGet();
            ViewBag.SkmCategory = cats;

            SystemData systemData = _sysp.SystemDataGet(SkmPublicImageSysName);
            ViewBag.SkmPublicImage = new JsonSerializer().Deserialize<Dictionary<string, string>>(systemData.Data).ToDictionary(kvp => kvp.Key, kvp => ImageFacade.GetMediaPathsFromRawData("Event," + kvp.Value, MediaType.PponDealPhoto).FirstOrDefault());
            return View();
        }

        [HttpPost]
        public ActionResult SkmAppDefaultStylePublicImagUpdate(string category, string publicText, string imagePath)
        {
            string errorMsg = string.Empty;
            //修改img
            if (!string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(imagePath) && !imagePath.Contains("http"))
            {
                try
                {
                    var tempImagePath = Path.Combine(_config.WebTempPath, WebStorage._IMAGE_TEMP,
                        imagePath);
                    ImageUtility.UploadFile(new PhysicalPostedFileAdapter(tempImagePath), UploadFileType.PponEvent, "Event", Path.GetFileNameWithoutExtension(tempImagePath), false);
                    //string imgPath = ImageFacade.GenerateMediaPath("Event", Path.GetFileName(imagePath));
                    SystemData systemData = _sysp.SystemDataGet(SkmPublicImageSysName);
                    Dictionary<string, string> tempData = new JsonSerializer().Deserialize<Dictionary<string, string>>(systemData.Data);
                    tempData[category] = Path.GetFileName(imagePath);
                    systemData.Data = new JsonSerializer().Serialize(tempData);
                    systemData.ModifyId = User.Identity.Name;
                    systemData.ModifyTime = DateTime.Now;
                    _sysp.SystemDataSet(systemData);
                }
                catch (Exception e)
                {
                    errorMsg += "修改圖片失敗 原因:" + e.Message;
                }
            }
            if (!string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(publicText))//修改text
            {
                try
                {
                    Category oldCategory = _sp.CategoryGet(Convert.ToInt32(category));
                    oldCategory.Name = publicText;
                    oldCategory.ModifyId = User.Identity.Name;
                    oldCategory.ModifyTime = DateTime.Now;
                    _sp.CategorySet(oldCategory);
                }
                catch (Exception e)
                {
                    if (!string.IsNullOrEmpty(errorMsg))
                        errorMsg += " & ";
                    errorMsg = "修改名稱失敗 原因:" + e.Message;
                }
            }

            TempData["selectedCategory"] = category;
            TempData["errorMsg"] = string.IsNullOrEmpty(errorMsg) ? "修改成功" : errorMsg;

            return RedirectToAction("SkmAppDefaultStylePublicImag");
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SKMAppDefaultStyleStatusUpdate(Guid guid, Guid qSeller)
        {
            bool flag = true;
            int newStatus = (int)SkmAppStyleStatus.Hide;
            try
            {
                var acts = SkmFacade.SkmLatestActivityGetBySellerGuid(qSeller).Where(x => x.Guid == guid).ToList();
                var deals = SkmFacade.SkmLatestSellerDealGetBySellerGuid(qSeller).Where(x => x.Guid == guid).ToList();
                if (acts.Count > 0)
                {
                    if (acts.FirstOrDefault().Status == (int)SkmAppStyleStatus.Hide)
                    {
                        newStatus = (int)SkmAppStyleStatus.Normal;
                    }
                    foreach (var act in acts)
                    {
                        act.Status = newStatus;
                        SkmFacade.SkmLatestActivitySet(act);
                    }
                }

                if (deals.Count > 0)
                {
                    if (deals.FirstOrDefault().Status == (int)SkmAppStyleStatus.Hide)
                    {
                        newStatus = (int)SkmAppStyleStatus.Normal;
                    }
                    foreach (var deal in deals)
                    {
                        deal.Status = newStatus;
                        SkmFacade.SkmLatestSellerDealSet(deal);
                    }
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = newStatus });
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppActivityStyleEdit(
            string Title,
            string Url,
            string EventBeginDate,
            string EventBeginHour,
            string EventBeginMinute,
            string EventEndDate,
            string EventEndHour,
            string EventEndMinute,
            string Description,
            string StroeName,
            string StroeFloor,
            string ActDate,
            string MainPic,
            string qCategory,
            Guid QSeller,
            Guid guid,
            string IsMustLogin
            )
        {
            Guid? _Guid = Guid.Empty;
            var data = SkmFacade.SkmLatestActivityGetBySellerGuid(QSeller).FirstOrDefault(x => x.Guid == guid);
            if (data != null)
            {
                _Guid = data.Guid;
            }
            else
            {
                _Guid = Guid.NewGuid();
            }
            DateTime StartDate = DateTime.Now;
            DateTime.TryParse(EventBeginDate + " " + EventBeginHour + ":" + EventBeginMinute, out StartDate);
            DateTime EndDate = DateTime.Now;
            DateTime.TryParse(EventEndDate + " " + EventEndHour + ":" + EventEndMinute, out EndDate);

            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/Event/");
            string fileName = "";
            bool checkFile = true;
            if (Request.Files.Count > 0)
            {
                foreach (string file in Request.Files)
                {
                    System.Web.HttpPostedFileBase hpf = Request.Files[file] as System.Web.HttpPostedFileBase;
                    if (hpf.FileName != "")
                    {
                        string Extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf("."),
                            hpf.FileName.Length - hpf.FileName.LastIndexOf("."));
                        //檢查圖片格式
                        if (!SkmFacade.CheckImageFormat(Extension.ToLower()))
                        {
                            checkFile = false;
                            break;
                        }

                        System.Drawing.Image img = System.Drawing.Image.FromStream(hpf.InputStream);
                        double imgW = img.Width;
                        double imgH = img.Height;
                        //檢查圖片大小
                        if (!_config.EnableNewSKMAppDealStyle)
                        {
                            if (imgW < 200 || imgH < 200 || imgW != imgH)
                            {
                                checkFile = false;
                            }
                        }
                        else
                        {
                            if ((imgW < 770 || imgH < 330 || imgW / imgH != 770 / 330.0) && (imgW < 200 || imgH < 200 || imgW != imgH))
                            {
                                checkFile = false;
                            }
                        }

                        fileName = string.Format("{0}_SkmEventSkmAppDefaultStyleBanner_{1}{2}", _Guid.ToString() == "" ? "{0}" : _Guid.ToString(), DateTime.Now.ToString("yyyyMMddHHmmss"), Extension);
                        break;
                    }

                }
            }

            if (!checkFile)
            {
                if (!_config.EnableSKMActivityStyleNewImgSize)
                {
                    return Content("<script>alert('僅能上傳圖片檔案，大小為 200 x 200，寬與高需等比，上傳檔案尺寸不符，儲存失敗！');history.go(-1);</script>");
                }
                else
                {
                    return Content("<script>alert('僅能上傳圖片檔案，大小為 770 x 330，寬與高需等比，上傳檔案尺寸不符，儲存失敗！');history.go(-1);</script>");
                }
            }

            if (data != null)
            {
                //update
                DateTime actDate = DateTime.Now;
                DateTime.TryParse(ActDate, out actDate);
                data.ActivityTitle = Title;
                data.LinkUrl = Url;
                data.SDate = StartDate;
                data.EDate = EndDate;
                data.StoreFloor = StroeFloor;
                data.StoreName = StroeName;
                data.Guid = _Guid;
                data.SellerGuid = QSeller;
                data.ActDate = actDate;
                data.ActivityDescription = Description;
                data.CategoryId = Convert.ToInt32(qCategory);
                data.Status = 1;
                data.IsMustLogin = IsMustLogin == "on" && Url.ToLower().Contains("17life.com");
                if (fileName != "")
                {
                    data.ImageUrl = fileName;
                }
                SkmFacade.SkmLatestActivitySet(data);
            }
            else
            {
                //insert
                DateTime actDate = DateTime.Now;
                DateTime.TryParse(ActDate, out actDate);
                SkmLatestActivity act = new SkmLatestActivity();
                act.ActivityTitle = Title;
                act.LinkUrl = Url;
                act.SDate = StartDate;
                act.EDate = EndDate;
                act.StoreFloor = StroeFloor;
                act.StoreName = StroeName;
                act.Guid = _Guid;
                act.SellerGuid = QSeller;
                act.ActDate = actDate;
                act.ActivityDescription = Description;
                act.CategoryId = Convert.ToInt32(qCategory);
                act.Status = 1;
                act.IsMustLogin = IsMustLogin == "on" && Url.ToLower().Contains("17life.com");
                if (fileName != "")
                {
                    act.ImageUrl = fileName;
                }
                SkmFacade.SkmLatestActivitySet(act);
            }

            if (fileName != "")
            {
                foreach (string file in Request.Files)
                {
                    System.Web.HttpPostedFileBase hpf = Request.Files[file] as System.Web.HttpPostedFileBase;
                    hpf.SaveAs(baseDirectoryPath + fileName);
                    break;
                }
            }
            if (_config.EnableNewSKMAppDealStyle)
            {
                return RedirectToAction("SKMAppActivityStyle", new { qSeller = QSeller, Category = qCategory });
            }
            else
            {
                return RedirectToAction("SKMAppDefaultStyle", new { qSeller = QSeller, Category = qCategory });
            }
        }

        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDefaultStyleApply(string sid, string gid)
        {
            Guid sgid = Guid.Empty;
            Guid.TryParse(sid, out sgid);
            Guid guid = Guid.Empty;
            Guid.TryParse(gid, out guid);
            SkmAppStyle sas = _skm.SkmAppStyleGet(sgid);
            if (sas == null)
            {
                return RedirectToAction("SKMAppDefaultStyle");
            }

            Dictionary<string, string> sellers = SellersGetByUserId();
            Dictionary<string, string> cats = CategoryTypeGet();
            Dictionary<string, string> deals = null;
            SkmAppStyleSetup sass = null;
            string BeginDate = DateTime.Today.ToString("yyyy/MM/dd");
            string EndDate = DateTime.Today.AddDays(7).ToString("yyyy/MM/dd");

            if (guid != Guid.Empty)
            {
                sass = _skm.SkmAppStyleSetupGet(guid);
            }

            if (sass != null)
            {
                //將檔案轉換成Http://xxx顯示在前端
                List<SkmAppStyleImages> imgs = new JsonSerializer().Deserialize<List<SkmAppStyleImages>>(sass.DealImage);
                foreach (SkmAppStyleImages img in imgs)
                {
                    if (!string.IsNullOrEmpty(img.FileName))
                    {
                        img.FileUrl = SkmFacade.SkmAppStyleImgPath(img.FileName);
                    }
                    else
                    {
                        img.FileUrl = "";
                    }
                }
                sass.DealImage = new JsonSerializer().Serialize(imgs);

                BeginDate = sass.BeginDate + " " + sass.BeginTime;
                EndDate = sass.EndDate + " " + sass.EndTime;
            }
            deals = AppStyleDealsGet(sas.SellerGuid ?? Guid.Empty, Convert.ToInt32(sas.CategoryId), DateTime.Parse(BeginDate), DateTime.Parse(EndDate)).Deals;


            ViewBag.SellerName = sellers.Where(x => x.Key == sas.SellerGuid.ToString()).Select(x => x.Value).FirstOrDefault();
            ViewBag.Sequence = sas.Sequence;
            ViewBag.CategoryName = cats.Where(x => x.Key == sas.CategoryId.ToString()).Select(x => x.Value).FirstOrDefault();
            ViewBag.SkmAppStyleSetup = sass;
            ViewBag.SkmDeals = deals;

            ViewData["qSeller"] = sas.SellerGuid.ToString();
            ViewData["qCategoryType"] = sas.CategoryId.ToString();
            ViewData["sid"] = sid;
            ViewData["gid"] = gid;

            return View();
        }

        /// <summary>
        /// 取得App首頁版型設定Deals
        /// </summary>
        /// <param name="seller_guid">新光賣家</param>
        /// <param name="CategoryId">類別</param>
        /// <param name="dateStart">起始日</param>
        /// <param name="dateEnd">結束日</param>
        /// <returns></returns>
        private SkmDealsGetModel AppStyleDealsGet(Guid seller_guid, int CategoryId, DateTime? dateStart, DateTime? dateEnd)
        {
            Dictionary<string, string> deals = new Dictionary<string, string>();
            List<SkmDealInfo> dealsInfo = new List<SkmDealInfo>();

            List<SkmDealTimeSlot> dtsc = _skm.SkmDealTimeSlotGetList(dateStart ?? DateTime.Now, dateEnd ?? DateTime.Today.AddDays(30), _config.SkmCityId, new List<Guid> { seller_guid })
                .Where(x => x.Status == (int)DealTimeSlotStatus.Default).ToList();

            var vpds = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(dtsc.Select(x => x.BusinessHourGuid).Distinct().ToList()).ToList();
            vpds = vpds.Where(x => x.SellerGuid == seller_guid).OrderBy(x => x.BusinessHourOrderTimeS).ToList();

            CategoryDealCollection cdc = _pp.CategoryDealsGetList(dtsc.Select(x => x.BusinessHourGuid).Distinct().ToList());


            foreach (ViewPponDeal vpd in vpds)
            {
                var v = cdc.Where(x => x.Bid == vpd.BusinessHourGuid && x.Cid == CategoryId).FirstOrDefault();
                if (v == null)
                {
                    //該檔次不屬於該類別
                    continue;
                }
                if (!deals.ContainsKey(vpd.BusinessHourGuid.ToString()))
                {
                    deals.Add(vpd.BusinessHourGuid.ToString(), vpd.AppTitle);
                    dealsInfo.Add(new SkmDealInfo
                    {
                        Bid = vpd.BusinessHourGuid,
                        OrderDateStart = vpd.BusinessHourOrderTimeS,
                        OrderDateEnd = vpd.BusinessHourOrderTimeE
                    });
                }
            }
            return new SkmDealsGetModel
            {
                Deals = deals,
                DealsInfo = dealsInfo
            };
        }

        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppActivityStyleEdit(Guid? gid, Guid qSeller, string category)
        {
            Dictionary<string, string> sellers = SellersGetByUserId();
            var Activitys = SkmFacade.SkmLatestActivityGetBySellerGuid(qSeller).ToList();
            var viewData = Activitys.Where(x => x.Guid == gid).FirstOrDefault();

            ViewBag.SkmSellers = sellers;

            if (viewData != null)
            {
                viewData.ImageUrl = SkmFacade.SkmAppStyleImgPath(viewData.ImageUrl);
            }

            Dictionary<string, string> cats = CategoryTypeGet();

            if (category == null)
            {
                category = cats.FirstOrDefault().Key;
            }

            ViewData["qSeller"] = qSeller.ToString();
            ViewData["qCategory"] = category;
            ViewData["catsName"] = cats[category];
            return View(viewData);
        }

        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDealStyleEdit(Guid? gid, Guid qSeller, string category)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            if (!SkmFacade.CheckSkmHqCategoriesPower(userId, category))
            {
                return RedirectToAction("SkmAppDefaultStyle");
            }

            Dictionary<string, string> sellers = SellersGetByUserId();
            var Deals = SkmFacade.SkmLatestSellerDealGetBySellerGuid(qSeller).ToList();
            var viewData = Deals.Where(x => x.Guid == gid).FirstOrDefault();

            ViewBag.SkmSellers = sellers;

            if (viewData != null)
            {
                viewData.ImageUrl = SkmFacade.SkmAppStyleImgPath(viewData.ImageUrl);
            }

            Dictionary<string, string> cats = CategoryTypeGet();

            if (category == null)
            {
                category = cats.FirstOrDefault().Key;
            }
            //檔次上下架時間
            if (viewData != null)
            {
                SkmDealTimeSlotCollection sdts = _skm.SkmDealTimeSlotGetByBid(viewData.LinkDealGuid).OrderByDesc("EffectiveStart");
                ViewData["dealSDate"] = sdts.Select(x => x.EffectiveStart).FirstOrDefault().ToString("yyyy/MM/dd HH:mm:ss");
                ViewData["dealEDate"] = sdts.Select(x => x.EffectiveStart).Last().ToString("yyyy/MM/dd HH:mm:ss");
            }

            ViewData["qSeller"] = qSeller.ToString();
            ViewData["qCategory"] = category;
            ViewData["catsName"] = cats[category];

            return View(viewData);
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDealStyleEdit(
           string Title,
           string EventBeginDate,
           string EventBeginHour,
           string EventBeginMinute,
           string EventEndDate,
           string EventEndHour,
           string EventEndMinute,
           string Description,
           string MainPic,
           string qCategory,
           Guid QSeller,
           Guid guid,
           Guid LinkDeal
           )
        {
            Guid? _Guid = Guid.Empty;
            var data = SkmFacade.SkmLatestSellerDealGetBySellerGuid(QSeller).FirstOrDefault(x => x.Guid == guid);
            if (data != null)
            {
                _Guid = data.Guid;
            }
            else
            {
                _Guid = Guid.NewGuid();
            }

            //刊登起迄時間
            DateTime StartDate = DateTime.Now;
            DateTime.TryParse(EventBeginDate + " " + EventBeginHour + ":" + EventBeginMinute, out StartDate);
            DateTime EndDate = DateTime.Now;
            DateTime.TryParse(EventEndDate + " " + EventEndHour + ":" + EventEndMinute, out EndDate);

            //產品上下架時間
            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(LinkDeal);
            if (vpd.IsLoaded && EndDate > vpd.BusinessHourOrderTimeE)
            {
                EndDate = vpd.BusinessHourOrderTimeE;
            }
            if (vpd.IsLoaded && StartDate < vpd.BusinessHourOrderTimeS)// 如果刊登起始時間小於產品上架時間, 則將刊登起始時間修改成產品上架時間
            {
                StartDate = vpd.BusinessHourOrderTimeS;
            }

            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/Event/");
            string fileName = "";
            bool checkFile = true;
            if (Request.Files.Count > 0)
            {
                foreach (string file in Request.Files)
                {
                    System.Web.HttpPostedFileBase hpf = Request.Files[file] as System.Web.HttpPostedFileBase;
                    if (hpf.FileName != "")
                    {
                        string Extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf("."), hpf.FileName.Length - hpf.FileName.LastIndexOf("."));
                        //檢查圖片格式
                        if (!SkmFacade.CheckImageFormat(Extension.ToLower()))
                        {
                            checkFile = false;
                            break;
                        }

                        System.Drawing.Image img = System.Drawing.Image.FromStream(hpf.InputStream);
                        int imgW = img.Width;
                        int imgH = img.Height;
                        //檢查圖片大小
                        if (imgW < 220 || imgH < 220 || imgW != imgH)
                        {
                            checkFile = false;
                        }
                        fileName = string.Format("{0}_SkmEventSKMAppDealStyleBanner{1}_{2}", _Guid.ToString() == "" ? "{0}" : _Guid.ToString(), DateTime.Now.ToString("yyyyMMddHHmmss"), Extension);
                        break;
                    }

                }
            }

            if (!checkFile)
            {
                return Content("<script>alert('上傳檔案尺寸不符,儲存失敗!僅能上傳圖片尺寸為220x220, 寬與高須等比');history.go(-1);</script>");
            }

            if (data != null)
            {
                //update

                data.DealTitle = Title;
                data.LinkDealGuid = LinkDeal;
                data.SDate = StartDate;
                data.EDate = EndDate;
                data.Guid = _Guid;
                data.SellerGuid = QSeller;
                data.DealDescription = Description;
                data.CategoryId = Convert.ToInt32(qCategory);
                data.Status = 1;

                if (fileName != "")
                {
                    data.ImageUrl = fileName;
                }
                SkmFacade.SkmLatestSellerDealSet(data);
            }
            else
            {
                //insert
                DateTime actDate = DateTime.Now;
                SkmLatestSellerDeal sdeal = new SkmLatestSellerDeal();
                sdeal.DealTitle = Title;
                sdeal.LinkDealGuid = LinkDeal;
                sdeal.SDate = StartDate;
                sdeal.EDate = EndDate;
                sdeal.Guid = _Guid;
                sdeal.SellerGuid = QSeller;
                sdeal.DealDescription = Description;
                sdeal.CategoryId = Convert.ToInt32(qCategory);
                sdeal.Status = 1;
                if (fileName != "")
                {
                    sdeal.ImageUrl = fileName;
                }
                SkmFacade.SkmLatestSellerDealSet(sdeal);
            }

            if (fileName != "")
            {
                foreach (string file in Request.Files)
                {
                    System.Web.HttpPostedFileBase hpf = Request.Files[file] as System.Web.HttpPostedFileBase;
                    hpf.SaveAs(baseDirectoryPath + fileName);
                    break;
                }
            }

            if (_config.EnableNewSKMAppDealStyle)
            {
                return RedirectToAction("SKMAppDealStyle", new { qSeller = QSeller, Category = qCategory });
            }
            else
            {
                return RedirectToAction("SKMAppDefaultStyle", new { qSeller = QSeller, Category = qCategory });
            }
        }



        public JsonResult SKMAppDefaultStyleSave()
        {
            bool flag = true;
            string Message = "";



            return Json(new { Success = flag, Message = Message });
        }



        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public ActionResult SKMAppDefaultStyleApplySave(FormCollection form, IEnumerable<System.Web.HttpPostedFileBase> files)
        {
            #region SaveData
            Guid StyleGuid = Guid.Empty;
            string sid = form["sid"].ToString();
            Guid.TryParse(sid, out StyleGuid);

            Guid guid = Guid.Empty;
            Guid.TryParse(form["gid"].ToString(), out guid);

            List<SkmAppStyleImages> sasis = new List<SkmAppStyleImages>();
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/Event/");

            Dictionary<int, string> fileList = new Dictionary<int, string>();
            #region File
            int idx = 0;
            foreach (System.Web.HttpPostedFileBase hpf in files)
            {
                if (hpf != null)
                {
                    string Extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf("."), hpf.FileName.Length - hpf.FileName.LastIndexOf("."));
                    fileList.Add(idx, "skm_app_style_" + idx + "_" + sid + Extension);
                    hpf.SaveAs(baseDirectoryPath + "skm_app_style_" + idx + "_" + sid + Extension);
                }
                idx++;
            }
            #endregion File

            bool checkFile = true;
            string errMessage = "";
            switch (form["chkType"].ToString())
            {
                case "1":
                    //版型1
                    string f0 = fileList.ContainsKey(0) ? fileList.Where(x => x.Key == 0).FirstOrDefault().Value : form["hid_type1_file_1"].ToString();
                    string f1 = fileList.ContainsKey(1) ? fileList.Where(x => x.Key == 1).FirstOrDefault().Value : form["hid_type1_file_2"].ToString();
                    string f2 = fileList.ContainsKey(2) ? fileList.Where(x => x.Key == 2).FirstOrDefault().Value : form["hid_type1_file_3"].ToString();

                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type1_deal_1"].ToString(),
                        FileName = f0
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type1_deal_2"].ToString(),
                        FileName = f1
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type1_deal_3"].ToString(),
                        FileName = f2
                    });

                    #region 檢查檔案格式
                    string fe0 = f0.Substring(f0.LastIndexOf("."), f0.Length - f0.LastIndexOf("."));
                    string fe1 = f1.Substring(f1.LastIndexOf("."), f1.Length - f1.LastIndexOf("."));
                    string fe2 = f2.Substring(f2.LastIndexOf("."), f2.Length - f2.LastIndexOf("."));

                    if (!SkmFacade.CheckImageFormat(fe0))
                    {
                        checkFile = false;
                        errMessage += "圖檔1格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe1))
                    {
                        checkFile = false;
                        errMessage += "圖檔2格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe2))
                    {
                        checkFile = false;
                        errMessage += "圖檔3格式不正確";
                    }
                    #endregion

                    #region 檢查檔案大小
                    if (checkFile)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[0] != null)
                        {
                            System.Drawing.Image img0 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[0].InputStream);
                            int img0W = img0.Width;
                            int img0H = img0.Height;

                            if (img0W != 320 || img0H != 324)
                            {
                                checkFile = false;
                                errMessage += "圖檔1應為320*324，您上傳的為" + img0W.ToString() + "*" + img0H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[1] != null)
                        {
                            System.Drawing.Image img1 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[1].InputStream);
                            int img1W = img1.Width;
                            int img1H = img1.Height;

                            if (img1W != 320 || img1H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔2應為320*162，您上傳的為" + img1W.ToString() + "*" + img1H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[2] != null)
                        {
                            System.Drawing.Image img2 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[2].InputStream);
                            int img2W = img2.Width;
                            int img2H = img2.Height;

                            if (img2W != 320 || img2H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔3應為320*162，您上傳的為" + img2W.ToString() + "*" + img2H.ToString();
                            }
                        }
                    }
                    #endregion

                    break;
                case "2":
                    //版型2
                    string f3 = fileList.ContainsKey(3) ? fileList.Where(x => x.Key == 3).FirstOrDefault().Value : form["hid_type2_file_1"].ToString();
                    string f4 = fileList.ContainsKey(4) ? fileList.Where(x => x.Key == 4).FirstOrDefault().Value : form["hid_type2_file_2"].ToString();
                    string f5 = fileList.ContainsKey(5) ? fileList.Where(x => x.Key == 5).FirstOrDefault().Value : form["hid_type2_file_3"].ToString();
                    string f6 = fileList.ContainsKey(6) ? fileList.Where(x => x.Key == 6).FirstOrDefault().Value : form["hid_type2_file_4"].ToString();
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type2_deal_1"].ToString(),
                        FileName = f3
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type2_deal_2"].ToString(),
                        FileName = f4
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type2_deal_3"].ToString(),
                        FileName = f5
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type2_deal_4"].ToString(),
                        FileName = f6
                    });

                    #region 檢查檔案格式
                    string fe3 = f3.Substring(f3.LastIndexOf("."), f3.Length - f3.LastIndexOf("."));
                    string fe4 = f4.Substring(f4.LastIndexOf("."), f4.Length - f4.LastIndexOf("."));
                    string fe5 = f5.Substring(f5.LastIndexOf("."), f5.Length - f5.LastIndexOf("."));
                    string fe6 = f6.Substring(f6.LastIndexOf("."), f6.Length - f6.LastIndexOf("."));

                    if (!SkmFacade.CheckImageFormat(fe3))
                    {
                        checkFile = false;
                        errMessage += "圖檔1格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe4))
                    {
                        checkFile = false;
                        errMessage += "圖檔2格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe5))
                    {
                        checkFile = false;
                        errMessage += "圖檔3格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe6))
                    {
                        checkFile = false;
                        errMessage += "圖檔4格式不正確";
                    }
                    #endregion

                    #region 檢查檔案大小
                    if (checkFile)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[3] != null)
                        {
                            System.Drawing.Image img3 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[3].InputStream);
                            int img3W = img3.Width;
                            int img3H = img3.Height;

                            if (img3W != 320 || img3H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔1應為320*162，您上傳的為" + img3W.ToString() + "*" + img3H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[4] != null)
                        {
                            System.Drawing.Image img4 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[4].InputStream);
                            int img4W = img4.Width;
                            int img4H = img4.Height;

                            if (img4W != 320 || img4H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔2應為320*162，您上傳的為" + img4W.ToString() + "*" + img4H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[5] != null)
                        {
                            System.Drawing.Image img5 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[5].InputStream);
                            int img5W = img5.Width;
                            int img5H = img5.Height;

                            if (img5W != 320 || img5H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔3應為320*162，您上傳的為" + img5W.ToString() + "*" + img5H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[6] != null)
                        {
                            System.Drawing.Image img6 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[6].InputStream);
                            int img6W = img6.Width;
                            int img6H = img6.Height;

                            if (img6W != 320 || img6H != 162)
                            {
                                checkFile = false;
                                errMessage += "圖檔4應為320*162，您上傳的為" + img6W.ToString() + "*" + img6H.ToString();
                            }
                        }
                    }
                    #endregion
                    break;
                case "3":
                    //版型3
                    string f7 = fileList.ContainsKey(7) ? fileList.Where(x => x.Key == 7).FirstOrDefault().Value : form["hid_type3_file_1"].ToString();
                    string f8 = fileList.ContainsKey(8) ? fileList.Where(x => x.Key == 8).FirstOrDefault().Value : form["hid_type3_file_2"].ToString();
                    string f9 = fileList.ContainsKey(9) ? fileList.Where(x => x.Key == 9).FirstOrDefault().Value : form["hid_type3_file_3"].ToString();

                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type3_deal_1"].ToString(),
                        FileName = f7
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type3_deal_2"].ToString(),
                        FileName = f8
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type3_deal_3"].ToString(),
                        FileName = f9
                    });

                    #region 檢查檔案格式
                    string fe7 = f7.Substring(f7.LastIndexOf("."), f7.Length - f7.LastIndexOf("."));
                    string fe8 = f8.Substring(f8.LastIndexOf("."), f8.Length - f8.LastIndexOf("."));
                    string fe9 = f9.Substring(f9.LastIndexOf("."), f9.Length - f9.LastIndexOf("."));

                    if (!SkmFacade.CheckImageFormat(fe7))
                    {
                        checkFile = false;
                        errMessage += "圖檔1格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe8))
                    {
                        checkFile = false;
                        errMessage += "圖檔2格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe9))
                    {
                        checkFile = false;
                        errMessage += "圖檔3格式不正確";
                    }
                    #endregion


                    #region 檢查檔案大小
                    if (checkFile)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[7] != null)
                        {
                            System.Drawing.Image img7 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[7].InputStream);
                            int img7W = img7.Width;
                            int img7H = img7.Height;

                            if (img7W != 212 || img7H != 324)
                            {
                                checkFile = false;
                                errMessage += "圖檔1應為212*324，您上傳的為" + img7W.ToString() + "*" + img7H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[8] != null)
                        {
                            System.Drawing.Image img8 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[8].InputStream);
                            int img8W = img8.Width;
                            int img8H = img8.Height;

                            if (img8W != 212 || img8H != 324)
                            {
                                checkFile = false;
                                errMessage += "圖檔2應為212*324，您上傳的為" + img8W.ToString() + "*" + img8H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[9] != null)
                        {
                            System.Drawing.Image img9 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[9].InputStream);
                            int img9W = img9.Width;
                            int img9H = img9.Height;

                            if (img9W != 212 || img9H != 324)
                            {
                                checkFile = false;
                                errMessage += "圖檔3應為212*324，您上傳的為" + img9W.ToString() + "*" + img9H.ToString();
                            }
                        }
                    }
                    #endregion
                    break;
                case "4":
                    //版型4
                    string f10 = fileList.ContainsKey(10) ? fileList.Where(x => x.Key == 10).FirstOrDefault().Value : form["hid_type4_file_1"].ToString();
                    string f11 = fileList.ContainsKey(11) ? fileList.Where(x => x.Key == 11).FirstOrDefault().Value : form["hid_type4_file_2"].ToString();
                    string f12 = fileList.ContainsKey(12) ? fileList.Where(x => x.Key == 12).FirstOrDefault().Value : form["hid_type4_file_3"].ToString();
                    string f13 = fileList.ContainsKey(13) ? fileList.Where(x => x.Key == 13).FirstOrDefault().Value : form["hid_type4_file_4"].ToString();

                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type4_deal_1"].ToString(),
                        FileName = f10
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type4_deal_2"].ToString(),
                        FileName = f11
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type4_deal_3"].ToString(),
                        FileName = f12
                    });
                    sasis.Add(new SkmAppStyleImages
                    {
                        BusinessHourGuid = form["type4_deal_4"].ToString(),
                        FileName = f13
                    });

                    #region 檢查檔案格式
                    string fe10 = f10.Substring(f10.LastIndexOf("."), f10.Length - f10.LastIndexOf("."));
                    string fe11 = f11.Substring(f11.LastIndexOf("."), f11.Length - f11.LastIndexOf("."));
                    string fe12 = f12.Substring(f12.LastIndexOf("."), f12.Length - f12.LastIndexOf("."));
                    string fe13 = f13.Substring(f13.LastIndexOf("."), f13.Length - f13.LastIndexOf("."));

                    if (!SkmFacade.CheckImageFormat(fe10))
                    {
                        checkFile = false;
                        errMessage += "圖檔1格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe11))
                    {
                        checkFile = false;
                        errMessage += "圖檔2格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe12))
                    {
                        checkFile = false;
                        errMessage += "圖檔3格式不正確";
                    }
                    if (!SkmFacade.CheckImageFormat(fe13))
                    {
                        checkFile = false;
                        errMessage += "圖檔4格式不正確";
                    }
                    #endregion

                    #region 檢查檔案大小
                    if (checkFile)
                    {
                        if (((System.Web.HttpPostedFileBase[])(files))[10] != null)
                        {
                            System.Drawing.Image img10 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[10].InputStream);
                            int img10W = img10.Width;
                            int img10H = img10.Height;

                            if (img10W != 320 || img10H != 324)
                            {
                                checkFile = false;
                                errMessage += "圖檔1應為320*324，您上傳的為" + img10W.ToString() + "*" + img10H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[11] != null)
                        {
                            System.Drawing.Image img11 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[11].InputStream);
                            int img11W = img11.Width;
                            int img11H = img11.Height;

                            if (img11W != 320 || img11H != 108)
                            {
                                checkFile = false;
                                errMessage += "圖檔2應為320*108，您上傳的為" + img11W.ToString() + "*" + img11H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[12] != null)
                        {
                            System.Drawing.Image img12 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[12].InputStream);
                            int img12W = img12.Width;
                            int img12H = img12.Height;

                            if (img12W != 320 || img12H != 108)
                            {
                                checkFile = false;
                                errMessage += "圖檔3應為320*108，您上傳的為" + img12W.ToString() + "*" + img12H.ToString();
                            }
                        }
                        if (((System.Web.HttpPostedFileBase[])(files))[13] != null)
                        {
                            System.Drawing.Image img13 = System.Drawing.Image.FromStream(((System.Web.HttpPostedFileBase[])(files))[13].InputStream);
                            int img13W = img13.Width;
                            int img13H = img13.Height;

                            if (img13W != 320 || img13H != 108)
                            {
                                checkFile = false;
                                errMessage += "圖檔4應為320*108，您上傳的為" + img13W.ToString() + "*" + img13H.ToString();
                            }
                        }
                    }
                    #endregion
                    break;
            }

            if (!checkFile)
            {
                return Content("<script>alert('圖片檔案大小不正確，" + errMessage + "');history.go(-1);</script>");
            }

            if (guid != Guid.Empty)
            {
                //修改
                SkmAppStyleSetup sass = _skm.SkmAppStyleSetupGet(guid);

                sass.StyleGuid = StyleGuid;
                sass.BeginDate = form["BeginDate"].ToString();
                sass.BeginTime = form["BeginTime"].ToString();
                sass.EndDate = form["EndDate"].ToString();
                sass.EndTime = form["EndTime"].ToString();
                sass.Style = Convert.ToInt32(form["chkType"].ToString());
                sass.DealImage = new JsonSerializer().Serialize(sasis);
                sass.ModifyTime = DateTime.Now;
                sass.ModifyUser = UserName;
                sass.Status = (int)SkmAppStyleStatus.Normal;

                _skm.SkmAppStyleSetupSet(sass);
            }
            else
            {
                //新增
                SkmAppStyleSetup sass = new SkmAppStyleSetup();
                sass.Guid = Guid.NewGuid();
                sass.StyleGuid = StyleGuid;
                sass.BeginDate = form["BeginDate"].ToString();
                sass.BeginTime = form["BeginTime"].ToString();
                sass.EndDate = form["EndDate"].ToString();
                sass.EndTime = form["EndTime"].ToString();
                sass.Style = Convert.ToInt32(form["chkType"].ToString());
                sass.DealImage = new JsonSerializer().Serialize(sasis);
                sass.CreateTime = DateTime.Now;
                sass.CreateUser = UserName;
                sass.ModifyTime = DateTime.Now;
                sass.ModifyUser = UserName;
                sass.Status = (int)SkmAppStyleStatus.Normal;

                _skm.SkmAppStyleSetupSet(sass);
            }
            #endregion SaveData



            return RedirectToAction("SkmAppDefaultStyle", new { qSeller = form["qSeller"].ToString() });
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SkmAppStyleUpdate(string SellerGuid, string CategoryId)
        {
            bool flag = true;
            string Message = "";
            try
            {
                Guid seller_guid = Guid.Empty;
                Guid.TryParse(SellerGuid, out seller_guid);

                SkmAppStyleCollection sasc = _skm.SkmAppStyleGetBySeller(seller_guid);
                int newSequence = 1;
                if (sasc.Count > 0)
                {
                    newSequence = sasc.Select(x => x.Sequence).Max() + 1;
                }

                SkmAppStyle sas = new SkmAppStyle();
                sas.SellerGuid = seller_guid;
                sas.Sequence = newSequence;
                sas.CategoryId = CategoryId;
                sas.Status = (int)SkmAppStyleStatus.Normal;
                sas.Guid = Guid.NewGuid();
                sas.CreateTime = DateTime.Now;
                sas.CreateUser = this.UserName;
                sas.ModifyTime = DateTime.Now;
                sas.ModifyUser = this.UserName;
                _skm.SkmAppStyleSet(sas);
                Message = newSequence.ToString();
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = Message });
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult checkStyleDate(string sid, string gid, string begindate, string enddate)
        {
            bool flag = true;
            try
            {
                Guid sguid = Guid.Empty;
                Guid.TryParse(sid, out sguid);

                Guid _gid = Guid.Empty;
                Guid.TryParse(gid, out _gid);

                List<SkmAppStyleSetup> sasc = _skm.SkmAppStyleSetupGetBySid(sguid).ToList();
                if (sasc.Count > 0)
                {
                    if (_gid != Guid.Empty)
                    {
                        sasc = sasc.Where(x => x.Guid != _gid).ToList();
                    }
                    foreach (SkmAppStyleSetup sas in sasc)
                    {
                        if (DateTime.Parse(sas.BeginDate) <= DateTime.Parse(begindate) && DateTime.Parse(sas.EndDate) <= DateTime.Parse(begindate))
                        {

                        }
                        else if (DateTime.Parse(sas.BeginDate) >= DateTime.Parse(enddate) && DateTime.Parse(sas.EndDate) >= DateTime.Parse(enddate))
                        {

                        }
                        else
                        {
                            flag = false;
                            break;
                        }
                    }
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = "" });
        }


        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SkmAppStyleStatusUpdate(string guid)
        {
            bool flag = true;
            string message = "";
            int newStatus = (int)SkmAppStyleStatus.Hide;
            try
            {
                Guid _guid = Guid.Empty;
                Guid.TryParse(guid, out _guid);

                SkmAppStyleSetup sass = _skm.SkmAppStyleSetupGet(_guid);
                if (sass != null)
                {
                    if (sass.Status == (int)SkmAppStyleStatus.Hide)
                    {
                        newStatus = (int)SkmAppStyleStatus.Normal;
                    }
                    sass.Status = newStatus;
                    _skm.SkmAppStyleSetupSet(sass);
                    message = (newStatus).ToString();
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = message });
        }


        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SkmDealsGet(string SellerGuid, int CategoryId, DateTime? dateStart, DateTime? dateEnd)
        {
            try
            {
                SkmDealsGetModel result = new SkmDealsGetModel();
                Guid seller_guid = Guid.Empty;
                Guid.TryParse(SellerGuid, out seller_guid);
                var data = AppStyleDealsGet(seller_guid, CategoryId, dateStart, dateEnd);
                result.Deals = data.Deals;
                result.DealsInfo = data.DealsInfo;
                return Json(result);
            }
            catch
            {
                return Json("");
            }
        }

        [HttpPost]
        public JsonResult SkmAppStyleDelete(string guid)
        {
            bool flag = true;
            try
            {
                Guid gid = Guid.Empty;
                Guid.TryParse(guid, out gid);

                SkmAppStyle sas = _skm.SkmAppStyleGet(gid);
                if (sas != null)
                {
                    //更新狀態為false
                    sas.Status = (int)SkmAppStyleStatus.Delete;
                    _skm.SkmAppStyleSet(sas);

                    //將排程一併刪除
                    List<SkmAppStyleSetup> sassc = _skm.SkmAppStyleSetupGetBySid(sas.Guid ?? Guid.Empty).ToList();
                    foreach (SkmAppStyleSetup sass in sassc)
                    {
                        sass.Status = (int)SkmAppStyleStatus.Delete;
                        _skm.SkmAppStyleSetupSet(sass);
                    }

                    //將其他筆的順序重排
                    List<SkmAppStyle> sasc = _skm.SkmAppStyleGetBySeller(sas.SellerGuid ?? Guid.Empty).OrderBy(x => x.Sequence).ToList();

                    int idx = 1;
                    foreach (SkmAppStyle sa in sasc)
                    {
                        sa.Sequence = idx;
                        _skm.SkmAppStyleSet(sa);
                        idx++;
                    }
                }
                else
                {
                    flag = false;
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = "" });
        }

        [HttpPost]
        [Authorize(Roles = "SkmDefaultStyle, Administrator, ME2O, Production")]
        public JsonResult SkmAppStyleGeyBySeq(string SellerGuid, int Sequence)
        {
            try
            {
                Guid seller_guid = Guid.Empty;
                Guid.TryParse(SellerGuid, out seller_guid);

                SkmAppStyleCollection sasc = _skm.SkmAppStyleGetBySeller(seller_guid);
                SkmAppStyle sas = sasc.Where(x => x.Sequence == Sequence).FirstOrDefault();
                Dictionary<string, string> cats = CategoryTypeGet();

                if (sas != null)
                {
                    List<SkmAppStyleSetup> sassc = _skm.SkmAppStyleSetupGetBySid(sas.Guid ?? Guid.Empty).ToList();

                    List<SkmAppStyleSetupModel> data = sassc.Select(x => new SkmAppStyleSetupModel
                    {
                        Gid = x.Guid,
                        BeginDate = x.BeginDate,
                        EndDate = x.EndDate,
                        Status = x.Status,
                        DealStatus = StatusGet(x.Status.ToString(), x.BeginDate, x.EndDate)
                    }).ToList();

                    dynamic obj = new
                    {
                        Id = sas.Id,
                        CategoryId = sas.CategoryId,
                        Sid = sas.Guid,
                        Name = cats.Where(x => x.Key == sas.CategoryId).Select(x => x.Value).FirstOrDefault(),
                        Styles = data.OrderByDescending(x => x.BeginDate)
                    };
                    return Json(obj);
                }
                return Json(new
                {
                    Id = 0,
                    CategoryId = "",
                    Sid = "",
                    Name = "",
                    Styles = ""
                });
            }
            catch
            {
                return Json(new
                {
                    Id = 0,
                    CategoryId = "",
                    Sid = "",
                    Name = "",
                    Styles = ""
                });
            }
        }

        private string StatusGet(string status, string d1, string d2)
        {
            string rtn = "";
            if (status == ((int)SkmAppStyleStatus.Hide).ToString())
            {
                rtn = "隱藏中";
            }
            else
            {
                if (DateTime.Now > DateTime.Parse(d2))
                {
                    rtn = "已過期";
                }
                else if (DateTime.Now < DateTime.Parse(d1))
                {
                    rtn = "待上檔";
                }
                else
                {
                    rtn = "上檔中";
                }
            }
            return rtn;
        }

        #endregion SkmAppDefaultStyle

        #region 自訂版位設定設定 skm_customized_board
        public ActionResult CustomizedBoardList(string store)
        {
            SetPageDefault();//為了取得ViewBag.SKMStores，但執行的有點多東西
            Guid StoreGuid = Guid.NewGuid();
            if (!Guid.TryParse(store, out StoreGuid))
            {
                var skmStores = ViewBag.SKMStores as Dictionary<string, string>;
                if (skmStores.Count == 0)
                {
                    return Redirect(_config.SSLSiteUrl);
                }
                StoreGuid = skmStores.ContainsKey(DefaultSeller) ? Guid.Parse(DefaultSeller) : Guid.Parse(skmStores.FirstOrDefault().Key);
            }
            IEnumerable<CustomizedBoardBaseModel> response = SkmFacade.GetCustomizedBoardList(StoreGuid).Select(item => new CustomizedBoardBaseModel()
            {
                Id = item.Id,
                SellerGuid = item.SellerGuid,
                ContentType = Helper.GetEnumDescription((SkmCustomizedBoardUrlTypeEnum)item.ContentType),
                LayoutType = Helper.GetEnumDescription((SkmCustomizedBoardLayoutTypeEnum)item.LayoutType),
                UpdateDate = item.LastModifyDate.ToString("yyyy/MM/dd HH:mm:ss"),
            });
            return View(response);
        }

        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        [HttpGet]
        public ActionResult EditCustomizedBoard(int id)
        {
            SkmCustomizedBoard skmCustomizedBoard = SkmFacade.GetCustomizedBoardById(id);
            EditCustomizedBoardModel response = new EditCustomizedBoardModel()
            {
                Id = skmCustomizedBoard.Id,
                ContentType = (SkmCustomizedBoardUrlTypeEnum)skmCustomizedBoard.ContentType,
                LayoutType = Helper.GetEnumDescription((SkmCustomizedBoardLayoutTypeEnum)skmCustomizedBoard.LayoutType),
                SellerGuid = skmCustomizedBoard.SellerGuid,
                ImageUrl = skmCustomizedBoard.ImageUrl,
                LinkUrl = skmCustomizedBoard.LinkUrl,
                ImageLimiteWidth = skmCustomizedBoard.ImageLimiteWidth,
                ImageLimiteHeight = skmCustomizedBoard.ImageLimiteHeight,
            };

            return View(response);
        }

        [Authorize(Roles = "SkmAppBanner, Administrator, ME2O, Production")]
        [HttpPost]
        public ActionResult EditCustomizedBoard(EditCustomizedBoardModel request)
        {
            #region Set Default Data
            switch (request.ContentType)
            {
                case SkmCustomizedBoardUrlTypeEnum.DefaultGoodEat:
                    request.LinkUrl = _config.SkmRsvUrl;
                    break;
                case SkmCustomizedBoardUrlTypeEnum.DefaultExhibitionList:
                    request.LinkUrl = string.Format(SkmDeepLinkFormat.ExhibitionList, request.SellerGuid, _sp.SellerGet(request.SellerGuid).SellerName);
                    break;
                case SkmCustomizedBoardUrlTypeEnum.DefaultBeautyStage:
                    request.LinkUrl = _config.SkmBeautyStageUrl;
                    break;
                default:
                    break;
            }
            #endregion

            #region validate
            SkmCustomizedBoard skmCustomizedBoard = SkmFacade.GetCustomizedBoardById(request.Id);
            if (skmCustomizedBoard == null || skmCustomizedBoard.Id <= 0)
            {
                TempData["message"] = "查無此筆資料";
                return RedirectToAction("CustomizedBoardList");
            }
            string imageFileName = string.Empty;
            if (ModelState.IsValid)
            {
                if (request.UploadImageFile != null && !string.IsNullOrEmpty(request.UploadImageFile.FileName))
                {
                    string extension = Path.GetExtension(request.UploadImageFile.FileName).ToLower();
                    if (extension != ".jpg" && extension != ".png") ModelState.AddModelError("UploadImageFile", "只可上傳jpg或png格式的圖檔");
                    if (request.UploadImageFile.ContentLength > 300 * 1024) ModelState.AddModelError("UploadImageFile", "檔案大小不得超過300kb");
                    ImgData imgUploadData = SkmFacade.CheckImgType(request.UploadImageFile, skmCustomizedBoard.ImageLimiteWidth, skmCustomizedBoard.ImageLimiteHeight, "SkmCustomizedBoard", Guid.NewGuid().ToString());
                    if (!imgUploadData.IsSizePass)
                        ModelState.AddModelError("UploadImageFile", string.Format("上傳圖片尺寸限制為{0}*{1}px", skmCustomizedBoard.ImageLimiteWidth, skmCustomizedBoard.ImageLimiteHeight));
                    else
                        imageFileName = imgUploadData.SaveFileName;
                }
                else if (string.IsNullOrEmpty(request.ImageUrl))
                {
                    ModelState.AddModelError("UploadImageFile", "請選擇圖檔");
                }

                var urlLink = request.LinkUrl.ToLower().Trim();

                switch (request.ContentType)
                {
                    case SkmCustomizedBoardUrlTypeEnum.CustomizedExhibitionList:
                        if (urlLink.IndexOf(SkmDeepLinkFormat.ExhibitionList.Split('?')[0], StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "此連結格式與類型不符，請確認您輸入的連結！");
                        break;
                    case SkmCustomizedBoardUrlTypeEnum.ExhibitionEvent:
                        if (urlLink.IndexOf(SkmDeepLinkFormat.ExhibitionEvent.Split('?')[0], StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "此連結格式與類型不符，請確認您輸入的連結！");
                        break;
                    case SkmCustomizedBoardUrlTypeEnum.ExternalDeal:
                        if (urlLink.IndexOf(SkmDeepLinkFormat.ExternalDeal.Split('?')[0], StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "此連結格式與類型不符，請確認您輸入的連結！");
                        break;
                    case SkmCustomizedBoardUrlTypeEnum.Url:
                        if (urlLink.IndexOf("http", StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "此連結格式與類型不符，請確認您輸入的連結！");
                        break;
                    case SkmCustomizedBoardUrlTypeEnum.PrizeDrawEvent:
                        if (urlLink.IndexOf(SkmDeepLinkFormat.PrizeDrawEvent.Split('&')[0], StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "連結內容輸入錯誤，只可輸入抽紅包網址！");
                        break;
                    case SkmCustomizedBoardUrlTypeEnum.WeeklyDrawEvent:
                        if (urlLink.IndexOf(SkmDeepLinkFormat.WeeklyDrawEvent.Split("&")[0], StringComparison.InvariantCultureIgnoreCase) != 0)
                            ModelState.AddModelError("LinkUrl", "連結內容輸入錯誤，只可輸入週週抽獎網址！");
                        break;
                }
            }
            if (!ModelState.IsValid)
            {
                request.ImageLimiteHeight = skmCustomizedBoard.ImageLimiteHeight;
                request.ImageLimiteWidth = skmCustomizedBoard.ImageLimiteWidth;
                request.ImageUrl = skmCustomizedBoard.ImageUrl;
                request.LayoutType = Helper.GetEnumDescription((SkmCustomizedBoardLayoutTypeEnum)skmCustomizedBoard.LayoutType);
                TempData["validateResult"] = new JsonSerializer().Serialize(ModelState.Where(item => item.Value.Errors.Any()).Select(item => { return new { Key = item.Key, Message = string.Join("<br/>", item.Value.Errors.Select(error => { return error.ErrorMessage; })) }; }));
                return View(request);
            }
            #endregion

            if (request.UploadImageFile != null && !string.IsNullOrEmpty(request.UploadImageFile.FileName))
            {
                request.UploadImageFile.SaveAs(imgUploadDirPath + imageFileName);
                skmCustomizedBoard.ImageUrl = new Uri(new Uri(_config.SSLSiteUrl), "/media/Event/" + imageFileName).ToString();
            }
            skmCustomizedBoard.LinkUrl = request.LinkUrl;
            skmCustomizedBoard.ContentType = (int)request.ContentType;
            skmCustomizedBoard.LastModifyDate = DateTime.Now;
            skmCustomizedBoard.LastModifyUser = UserId;

            if (SkmFacade.SetCustomizedBoard(skmCustomizedBoard))
            {
                TempData["message"] = "修改成功";
                return RedirectToAction("CustomizedBoardList", new { store = request.SellerGuid });
            }
            else
            {
                TempData["message"] = "修改失敗";
                return View(request);
            }
        }
        #endregion

        #region APP檔次排序 SkmAppDealSort
        [Authorize(Roles = "SkmAppDealSort, Administrator, ME2O, Production")]
        public ActionResult SkmAppDealSort(string qSeller, string Category, string DealType,
            string BeginDate)
        {
            Dictionary<string, string> sellers = SellersGetByUserId();
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }


            Dictionary<string, string> cats = CategoryTypeGet();
            if (string.IsNullOrEmpty(Category))
            {
                Category = cats.FirstOrDefault().Key;
            }


            Dictionary<string, string> dealType = new Dictionary<string, string>();
            foreach (var item in Enum.GetValues(typeof(SkmDealTypes)))
            {
                dealType.Add(((int)item).ToString(), Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SkmDealTypes)item));
            }
            if (DealType == null)
            {
                DealType = dealType.FirstOrDefault().Key;
            }
            //預設為當週檔次時程
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            #region 找出相關檔次
            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));
            List<SkmDealSort> SkmDeals = new List<SkmDealSort>();
            //Seller seller = _sp.SellerGet("seller_id", qSeller);

            //找出這段期間上檔的檔次

            List<Guid> sellerGuids = new List<Guid> { Guid.Parse(qSeller), _config.SkmRootSellerGuid };
            List<Guid> bids = _skm.SkmDealTimeSlotGetList(begindate, begindate.AddDays(7), _config.SkmCityId, sellerGuids)
                .Where(x => x.CategoryType == Convert.ToInt32(Category) && x.DealType == Convert.ToInt32(DealType))
                .Select(x => x.BusinessHourGuid).Distinct().ToList();
            //補全館的資料
            List<IViewPponDeal> vpds = ViewPponDealManager.DefaultManager.ViewPponDealGetListByGuidList(bids)
                .Where(x => x.SellerGuid == Guid.Parse(qSeller) || x.SellerGuid == _config.SkmRootSellerGuid).ToList();


            //找出排序
            for (int w = 0; w < 7; w++)
            {
                //List<ViewPponDeal> pds = vpds.Where(x=>x.BusinessHourOrderTimeS<=begindate.AddDays(w) && x.BusinessHourOrderTimeE >= begindate.AddDays(w)).ToList();
                SkmDealTimeSlotCollection sdtsc = _skm.SkmDealTimeSlotGetAllByDate(begindate.AddDays(w));

                List<SeqDeals> sdeals = new List<SeqDeals>();
                SkmDealSort deal = new SkmDealSort();
                List<Guid> bidList = new List<Guid>();

                foreach (SkmDealTimeSlot slot in sdtsc)
                {
                    IViewPponDeal vpd = vpds.Where(x => x.BusinessHourGuid == slot.BusinessHourGuid).FirstOrDefault();
                    if (vpd == null)
                    {
                        continue;
                    }
                    /*
                     * 檢核
                     * */
                    if (bidList.Contains(slot.BusinessHourGuid))
                    {
                        continue;
                    }
                    if (string.IsNullOrEmpty(vpd.AppTitle))
                    {
                        continue;
                    }
                    if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
                    {
                        continue;
                    }

                    ViewExternalDeal extDeal = _pp.ViewExternalDealGetByBid(vpd.BusinessHourGuid);
                    if (!extDeal.IsLoaded)
                    {
                        continue;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(extDeal.TagList))
                        {
                            continue;
                        }
                        if (string.IsNullOrEmpty(extDeal.CategoryList))
                        {
                            continue;
                        }
                        string[] tags = new JsonSerializer().Deserialize<string[]>(extDeal.TagList);
                        if (!tags.Contains(DealType))
                        {
                            continue;
                        }
                        string[] categorys = new JsonSerializer().Deserialize<string[]>(extDeal.CategoryList);
                        if (!categorys.Contains(Category))
                        {
                            continue;
                        }
                    }
                    SkmDealTimeSlot sdts = sdtsc.Where(x => x.BusinessHourGuid == vpd.BusinessHourGuid && x.CategoryType == int.Parse(Category) && x.DealType == int.Parse(DealType)).FirstOrDefault();
                    int Sequence = 99999;
                    int Status = (int)DealTimeSlotStatus.Default;
                    string Link = "";
                    if (sdts != null)
                    {
                        Sequence = sdts.Sequence;
                        Status = sdts.Status;
                    }
                    if (extDeal != null)
                    {
                        Link = _config.SiteUrl + "/SKMDeal/SkmDealPreview?guid=" + extDeal.Guid.ToString();
                    }
                    SeqDeals sdeal = new SeqDeals();
                    sdeal.BusinessHourGuid = vpd.BusinessHourGuid;
                    sdeal.ItemName = vpd.AppTitle;
                    sdeal.DealDate = begindate.AddDays(w).ToString("yyyy/MM/dd HH:mm:ss");
                    sdeal.Sequence = Sequence;
                    sdeal.CityId = _config.SkmCityId;
                    sdeal.Status = Status;
                    sdeal.Id = sdts.Id;
                    sdeal.Url = Link;
                    sdeals.Add(sdeal);
                    bidList.Add(vpd.BusinessHourGuid);
                }
                deal.DealDate = begindate.AddDays(w);
                deal.Deals = sdeals.OrderBy(x => x.Sequence).ThenByDescending(x => x.Id).ToList();
                SkmDeals.Add(deal);
            }

            var skmHqCategoriesPower = SkmFacade.CheckSkmHqCategoriesPower(userId, Category);

            #endregion
            ViewBag.SkmSellers = sellers;
            ViewBag.SkmCategory = cats.Where(x => x.Key != _config.SkmDealDisableCategory.ToString()).ToDictionary(x => x.Key, x => x.Value); //刪去最新活動;
            ViewBag.SkmDealType = dealType;
            ViewBag.SkmDeals = SkmDeals;

            ViewData["qSeller"] = qSeller;
            ViewData["Category"] = Category;
            ViewData["DealType"] = DealType;
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");
            ViewData["EndDate"] = begindate.AddDays(7).ToString("yyyy/MM/dd");
            ViewData["SkmHqCategoriesPower"] = skmHqCategoriesPower;
            ViewData["SkmHqCategories"] = SkmHqCategoriesConvertToList();

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "SkmAppDealSort, Administrator, ME2O, Production")]
        public JsonResult SkmDealSortUpdate(string data, int CategoryType, int DealType)
        {
            bool flag = true;
            string message = "";
            try
            {
                List<SeqDeals> sdsus = new JsonSerializer().Deserialize<List<SeqDeals>>(data);

                foreach (SeqDeals sdsu in sdsus)
                {
                    SkmDealTimeSlot sdts = _skm.SkmDealTimeSlotGetByBidAndDate(sdsu.BusinessHourGuid, DateTime.Parse(DateTime.Parse(sdsu.DealDate).ToString("yyyy/MM/dd")),
                        CategoryType.ToString(), DealType.ToString());
                    if (sdts != null)
                    {
                        sdts.Sequence = sdsu.Sequence;
                        sdts.Status = sdsu.Status;
                        _skm.SkmDealTimeSlotSet(sdts);
                    }
                    else
                    {
                        SkmDealTimeSlot dts = new SkmDealTimeSlot();
                        dts.BusinessHourGuid = sdsu.BusinessHourGuid;
                        dts.CityId = _config.SkmCityId;
                        dts.Sequence = sdsu.Sequence;
                        dts.EffectiveStart = DateTime.Parse(DateTime.Parse(sdsu.DealDate).ToString("yyyy/MM/dd"));
                        dts.Status = sdsu.Status;
                        dts.CategoryType = CategoryType;
                        dts.DealType = DealType;
                        _skm.SkmDealTimeSlotSet(dts);
                    }

                    /*
                     * 同步更新 skm_beacon_message_time_slot Beacon訊息
                     * */
                    string[] filter = new string[2];
                    filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;
                    filter[1] = "effective_start=" + DateTime.Parse(DateTime.Parse(sdsu.DealDate).ToString("yyyy/MM/dd"));
                    SkmBeaconMessageTimeSlot sbmts = _skm.SkmBeaconMessageTimeSlotGetList("sequence", filter).FirstOrDefault();
                    if (sbmts != null)
                    {
                        sbmts.Status = sdsu.Status;
                        _skm.SkmBeaconMessageTimeSlotSet(sbmts);
                    }

                }
            }
            catch (Exception ex)
            {
                flag = false;
                message = ex.Message;
            }

            return Json(new { Success = flag, Message = message });
        }

        private string[] DealTimeSlotfilterGet(string f, DateTime begindate)
        {
            string[] filter = new string[3];
            filter[0] = "" + f + " >= " + begindate.ToString("yyyy/MM/dd") + "";
            filter[1] = "" + f + " <= " + begindate.AddDays(7).ToString("yyyy/MM/dd") + "";
            filter[2] = "city_id = " + _config.SkmCityId;

            return filter;
        }
        #endregion SkmAppDealSort

        #region Beacon訊息管理 SkmAppBeacon

        [HttpPost]
        public ActionResult BeaconFileUpload()
        {
            bool flag = true;
            if (Request.Form["Id"] != null)
            {
                int Id = int.Parse(Request.Form["Id"].ToString());

                SkmBeaconMessage message = _skm.SkmBeaconMessageGet(Id);
                if (message != null && message.IsLoaded)
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        SkmBeaconGroupCollection groups = new SkmBeaconGroupCollection();

                        var file = Request.Files[i];
                        int idx = 0;
                        StreamReader reader = new StreamReader(file.InputStream, System.Text.Encoding.Default);
                        while (!reader.EndOfStream)
                        {
                            var line = reader.ReadLine();
                            if (idx == 0)
                            {
                                idx++;
                                continue;
                            }
                            var data = line.Split(",");
                            groups.Add(new SkmBeaconGroup
                            {
                                Guid = message.Guid.Value,
                                MemberId = data[0],
                                BsLabel = data[1]
                            });
                            idx++;
                        }

                        _skm.SkmBeaconGroupDelete(message.Guid.Value);
                        _skm.SkmBeaconGroupSet(groups);
                    }
                }
            }
            return Json(flag);
        }

        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeacon(string BeginDate)
        {
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));

            string[] fit = new string[3];
            fit[0] = "effective_start >= " + begindate.ToString("yyyy/MM/dd") + "";
            fit[1] = "effective_start <= " + begindate.AddDays(7).ToString("yyyy/MM/dd") + "";
            fit[2] = "beacon_type=" + (int)SkmBeaconType.Top;

            var beaconIds = _skm.SkmBeaconMessageTimeSlotGetList("sequence", fit)
                                                .Select(x => new
                                                {
                                                    BeaconId = x.BeaconId.ToString()
                                                }).Distinct();

            List<string> beaids = beaconIds.Where(x => x.BeaconId != null).Select(x => x.BeaconId).ToList();
            List<BeaconList> beaconLists = new List<BeaconList>();
            foreach (var beaconId in beaids)
            {
                BeaconList beaconList = new BeaconList();
                var major = string.Empty;
                var minor = string.Empty;
                major = beaconId.Substring(0, 4);
                minor = beaconId.Substring(4);
                var aei = _mp.ActionEventInfoListGet().FirstOrDefault(x => x.ActionName.Equals("SkmBeaconEvent"));
                if (aei != null)
                {
                    var aedi = _mp.ActionEventDeviceInfoByMajorMinorGet(aei.Id, Convert.ToInt32(major), Convert.ToInt32(minor));
                    var skmDeviceName = string.Empty;
                    if (aedi.IsLoaded)
                    {
                        skmDeviceName = aedi.DeviceName;
                    }
                    beaconList.BeaconId = beaconId;
                    beaconList.DeviceName = skmDeviceName;
                    beaconLists.Add(beaconList);
                }
            }

            List<SkmDealSort> _SkmDealSort = new List<DataOrm.SkmDealSort>();
            if (beaconLists.Count() > 0)//beaconLists因為是foreach最外層, 有值才需要運算_SkmDealSort
            {
                for (int w = 0; w < 7; w++)
                {
                    List<SeqDeals> SkmDeals = new List<SeqDeals>();

                    string[] filter = new string[3];
                    filter[0] = "effective_start <= " + begindate.AddDays(w).ToString("yyyy/MM/dd") + " 23:59:59";
                    filter[1] = "effective_end >= " + begindate.AddDays(w).ToString("yyyy/MM/dd") + " 00:00:00";
                    filter[2] = "beacon_type=" + (int)SkmBeaconType.Top;

                    List<SkmBeaconMessageTimeSlot> smcs = _skm.SkmBeaconMessageTimeSlotGetList("sequence", filter).ToList();

                    List<Guid> bids = smcs.Select(x => x.BusinessHourGuid ?? Guid.Empty).ToList();
                    // ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(bids);
                    foreach (SkmBeaconMessageTimeSlot smc in smcs)
                    {
                        string[] filterBeacon = new string[2];
                        filterBeacon[0] = "";
                        var aepm = _skm.SkmBeaconMessageGetList("", "Guid=" + smc.BusinessHourGuid).FirstOrDefault();
                        if (aepm != null)
                        {
                            SkmDeals.Add(new SeqDeals
                            {
                                Id = aepm.Id,
                                BusinessHourGuid = smc.BusinessHourGuid ?? Guid.Empty,
                                ItemName = aepm.Subject,
                                DealDate = begindate.AddDays(w).ToString("yyyy/MM/dd HH:mm:ss"),
                                Sequence = smc.Sequence ?? 99999,
                                Status = smc.Status ?? (int)DealTimeSlotStatus.NotShowInPponDefault,
                                BeaconId = smc.BeaconId
                            });
                        }
                    }
                    _SkmDealSort.Add(new SkmDealSort
                    {
                        DealDate = begindate.AddDays(w),
                        Deals = SkmDeals
                    });
                }
            }

            ViewBag.SkmDealSort = _SkmDealSort;
            ViewBag.BeaconList = beaconLists;
            ViewData["BeaconType"] = getBeaconTypes();
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");

            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeacon2(string beacon, string date)
        {
            DateTime bd = DateTime.Now;
            DateTime.TryParse(date, out bd);
            string[] filter = new string[4];
            filter[0] = "effective_start <= " + bd.ToString("yyyy/MM/dd") + " 23:59:59";
            filter[1] = "effective_end >= " + bd.ToString("yyyy/MM/dd") + " 00:00:00";
            filter[2] = "beacon_type=" + (int)SkmBeaconType.Top;
            filter[3] = "beacon_id=" + beacon;

            SkmBeaconMessageTimeSlotCollection slots = _skm.SkmBeaconMessageTimeSlotGetList("sequence", filter);

            List<SkmDealSort> SkmDealSort = new List<DataOrm.SkmDealSort>();

            foreach (SkmBeaconMessageTimeSlot slot in slots)
            {
                List<SeqDeals> SkmDeals = new List<SeqDeals>();

                string[] filterBeacon = new string[2];
                filterBeacon[0] = "";
                var aepm = _skm.SkmBeaconMessageGetList("", "Guid=" + slot.BusinessHourGuid).FirstOrDefault();
                if (aepm != null)
                {
                    SkmDeals.Add(new SeqDeals
                    {
                        Id = aepm.Id,
                        BusinessHourGuid = aepm.Guid ?? Guid.Empty,
                        ItemName = aepm.Subject,
                        DealDate = bd.ToString("yyyy/MM/dd HH:mm:ss"),
                        Sequence = slot.Sequence ?? 99999,
                        Status = slot.Status ?? (int)DealTimeSlotStatus.NotShowInPponDefault,
                        BeaconId = aepm.Beacon,
                        Type = aepm.BeaconType
                    });
                }

                SkmDealSort.Add(new SkmDealSort
                {
                    DealDate = bd,
                    Deals = SkmDeals
                });
            }

            ViewBag.SkmDealSort = SkmDealSort;
            ViewBag.beacon = beacon;
            ViewBag.date = date;
            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconDeal(string qSeller, string shopCode, string ddlFloor,
           string BeginDate)
        {
            SetPageDefault();
            Dictionary<string, string> beaconType = new Dictionary<string, string>();
            foreach (var item in Enum.GetValues(typeof(SkmBeaconType)))
            {
                beaconType.Add(((int)item).ToString(), Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SkmBeaconType)item));
            }
            //Dictionary<string, string> sellers = SellersGetByUserId();
            if (string.IsNullOrEmpty(qSeller))
            {
                qSeller = DefaultSeller;
            }


            if (string.IsNullOrEmpty(shopCode))
            {
                Guid guid = new Guid(qSeller);

                var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
                var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(guid, false).ToList()
                    .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
                    .GroupBy(x => new { x.ShopCode, x.ShopName },
                        (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
                    .OrderBy(s => s.shopCode);

                var shops = qSeller == DefaultSeller ? skmShoppeShopCol.OrderBy(s => int.Parse(Regex.Match(s.shopCode, @"\d+").Value))
                   .Select(s => new { key = s.shopCode, value = s.shopName }) : skmShoppeShopCol.Select(s => new { key = s.shopCode, value = s.shopName });

                if (shops.Any())
                {
                    shopCode = shops.FirstOrDefault().key;
                }
            }

            if (string.IsNullOrEmpty(ddlFloor))
            {
                var data = getFloor(Guid.Parse(qSeller), shopCode);
                foreach (var d in data)
                {
                    ddlFloor = d.Floor;
                    break;
                }
            }

            ViewBag.shopCode = shopCode;
            ViewBag.SkmSellers = ViewBag.SKMStores;
            ViewBag.Floor = ddlFloor;
            //ViewBag.SkmSellers = sellers;


            //預設為當週檔次時程
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));


            //var shops = _sp.SkmShoppeCollectionGetBySeller(Guid.Parse(qSeller), false);

            /*
             * 取得每天的資料
             * */
            List<SkmDealSort> SkmDealSort = GetBeaconDataListV1(begindate, (int)SkmBeaconType.Deal, shopCode, ddlFloor);

            ViewBag.SkmDealSort = SkmDealSort;
            ViewData["qSeller"] = qSeller;
            ViewData["BeaconType"] = beaconType;
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");

            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconProject(string BeginDate)
        {
            //預設為當週檔次時程
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));

            List<SkmDealSort> SkmDealSort = GetBeaconDataListV1(begindate, (int)SkmBeaconType.Project);

            ViewBag.SkmDealSort = SkmDealSort;
            ViewData["BeaconType"] = getBeaconTypes();
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");

            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconEventManual(string BeginDate)
        {
            //預設為當週檔次時程
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));

            List<SkmDealSort> SkmDealSort = GetBeaconDataListV1(begindate, (int)SkmBeaconType.Event_Manual);

            ViewBag.SkmDealSort = SkmDealSort;
            ViewData["BeaconType"] = getBeaconTypes();
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");

            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconEventCount(string BeginDate)
        {
            //預設為當週檔次時程
            DateTime bd = DateTime.Now;
            if (BeginDate != null)
            {
                DateTime.TryParse(BeginDate, out bd);
            }

            DateTime begindate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd"));

            List<SkmDealSort> SkmDealSort = GetBeaconDataListV1(begindate, (int)SkmBeaconType.Event_Count);

            ViewBag.SkmDealSort = SkmDealSort;
            ViewData["BeaconType"] = getBeaconTypes();
            ViewData["BeginDate"] = begindate.ToString("yyyy/MM/dd");

            return View();
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconEdit(int? id, int beconType, string shopCode)
        {
            Guid SellerGuid = Guid.Empty;

            //if (id == null)
            //{
            //    return View();
            //}
            var sbm = _skm.SkmBeaconMessageGet(id ?? 0);
            switch (beconType)
            {
                case (int)SkmBeaconType.Deal:
                    var vpd = _pp.ViewPponDealGetByBusinessHourGuid(sbm.BusinessHourGuid);
                    if (vpd == null)
                    {
                        return View();
                    }
                    else
                    {
                        SellerGuid = vpd.SellerGuid;
                    }
                    break;
                default:
                    //先預設信義新天地
                    SellerGuid = Guid.Parse(DefaultSeller);
                    break;
            }


            List<ActionEventDeviceInfo> beacons = new List<ActionEventDeviceInfo>();
            if (sbm.Id != 0)
            {
                if (SkmActionEventInfoId != 0)
                {
                    beacons = _mp.ActionEventDeviceInfoListGet(SkmActionEventInfoId).Where(x => x.ShopCode == sbm.ShopCode && x.Floor == sbm.Floor).ToList();
                }
                Dictionary<string, string> dicData = new Dictionary<string, string>();
                ViewExternalDealCollection exterDealList = _pp.ViewExternalDealGetListByBid(_skm.SkmBeaconMessageGet(sbm.Id).BusinessHourGuid);
                foreach (var i in exterDealList.GroupBy(x => new { x.ShopCode, x.Floor }))
                {
                    if (!dicData.Keys.Contains(i.Key.ShopCode))
                    {
                        string tempFloor = i.Key.Floor;
                        if (!string.IsNullOrEmpty(i.Key.Floor))
                        {
                            if (!i.Key.Floor.ToLower().Contains("F"))
                            {
                                tempFloor += "F";
                                tempFloor = tempFloor.PadLeft(3, '0');
                            }
                            dicData.Add(i.Key.ShopCode, tempFloor);
                        }
                    }
                }
                //Dictionary<string, string> dicData = exterDealList.GroupBy(x => new { x.ShopCode, x.Floor }).ToDictionary(p => p.Key.ShopCode, p => p.Key.Floor); 
                ViewBag.targetShopList = dicData.Count() > 0 ? ProviderFactory.Instance().GetSerializer().Serialize(dicData) : null;
            }

            int BeaconMemberCount = 0;
            if (sbm.IsLoaded)
            {
                BeaconMemberCount = _skm.SkmBeaconGroupGetCountByGuid(sbm.Guid.Value);
            }

            ViewBag.msgId = (id == null ? 0 : id);
            ViewBag.BeconType = beconType;
            ViewBag.IsRoot = SkmFacade.CheckSkmHqPower(Convert.ToInt32(User.Identity.GetPropertyValue("Id")));
            ViewBag.SKMStores = SellersGetByUserId();
            ViewBag.shopCode = shopCode;
            ViewBag.SellerGuid = SellerGuid.ToString();
            ViewBag.skmBeaconMessage = sbm;
            ViewBag.beacons = beacons;
            ViewBag.BeaconMemberCount = BeaconMemberCount;

            return View();
        }
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconEdit(int Id, string Subject, string BeginDate, string EndDate, string TimeStart, string TimeEnd, bool IsTop, int Status,
            string ShopCode, string Floor, string BeaconId, int beconType, string selectedBeacons)
        {
            int flag = 0;
            Guid guid = Guid.NewGuid();
            try
            {
                /*
                 * skm_beacon_message
                 * */
                SkmBeaconMessage sbm = _skm.SkmBeaconMessageGet(Id);
                if (sbm == null || sbm.Id == 0)
                {
                    sbm = new SkmBeaconMessage();
                }
                else
                {
                    guid = sbm.Guid ?? Guid.Empty;
                }

                string[] Bids = new JsonSerializer().Deserialize<string[]>(selectedBeacons);

                sbm.Subject = Subject;
                sbm.BusinessHourGuid = guid;
                sbm.EffectiveStart = DateTime.Parse(BeginDate);
                sbm.EffectiveEnd = DateTime.Parse(EndDate);
                sbm.TimeStart = TimeStart;
                sbm.TimeEnd = TimeEnd;
                sbm.IsTop = IsTop;
                sbm.Status = Status;
                sbm.ShopCode = ShopCode;
                sbm.Floor = Floor;
                sbm.Beacon = selectedBeacons;
                sbm.Guid = guid;
                sbm.BeaconType = beconType;
                _skm.SkmBeaconMessageSet(sbm);

                //Delete & Save Skm_Beacon_Message_Device_Info_Link 
                _skm.SkmBeaconMessageDeviceInfoLinkDeleteBySkmBeaconMessageId(sbm.Id);

                foreach (string beacon in Bids)
                {
                    var major = Convert.ToInt32(beacon.Substring(0, 4));
                    var minor = Convert.ToInt32(beacon.Substring(4));

                    ActionEventDeviceInfo aed = _mp.ActionEventDeviceInfoByMajorMinorGet(SkmActionEventInfoId, major, minor);

                    if (aed.IsLoaded)
                    {
                        var skmBeaconMessageDeviceInfoLink = new SkmBeaconMessageDeviceInfoLink
                        {
                            ActionEventDeviceInfoId = aed.Id,
                            SkmBeaconMessageId = sbm.Id,
                            CreateTime = DateTime.Now
                        };

                        _skm.SkmBeaconMessageDeviceInfoLinkSet(skmBeaconMessageDeviceInfoLink);
                    }
                }

                //刪除後重新Insert
                _skm.SkmBeaconMessageInfoDelete(guid);

                foreach (string beacon in Bids)
                {
                    SkmBeaconMessageInfo mi = new SkmBeaconMessageInfo();
                    mi.Guid = guid;
                    mi.BeaconId = beacon;
                    _skm.SkmBeaconMessageInfoSet(mi);
                }


                if (Id == 0)
                {
                    Id = _skm.SkmBeaconMessageGetList("", "Guid=" + guid).FirstOrDefault().Id;
                }


                /*
                * skm_beacon_message_time_slot
                * */

                SkmFacade.SkmBeaconMessageTimeSlotInsert(sbm, (IsTop ? (int)SkmBeaconType.Top : beconType), IsTop);

                if (beconType == (int)SkmBeaconType.Deal)
                {
                    //商品檔次
                    Guid gid = _pp.ViewExternalDealGetByBid(sbm.BusinessHourGuid).Guid;
                    ViewExternalDealCollection veds = _pp.ViewExternalDealGetListByDealrGuid(gid);
                    foreach (ViewExternalDeal ved in veds)
                    {
                        if (ved.Bid != null)
                        {
                            if (ved.Floor != null)
                            {
                                SkmBeaconMessage beaconMessage = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, ved.Floor);
                                if (beaconMessage != null)
                                {
                                    beaconMessage.Subject = Subject;
                                    beaconMessage.EffectiveStart = DateTime.Parse(BeginDate);
                                    beaconMessage.EffectiveEnd = DateTime.Parse(EndDate);
                                    beaconMessage.BusinessHourGuid = ved.Bid ?? Guid.Empty;
                                    beaconMessage.TimeStart = TimeStart;
                                    beaconMessage.TimeEnd = TimeEnd;
                                    beaconMessage.IsTop = IsTop;
                                    beaconMessage.Status = Status;
                                    beaconMessage.BeaconType = beconType;
                                    beaconMessage.ModifyTime = DateTime.Now;
                                    beaconMessage.ModifyUser = this.UserName;
                                    _skm.SkmBeaconMessageSet(beaconMessage);

                                    //刪除後重新Insert
                                    _skm.SkmBeaconMessageInfoDelete(ved.Bid ?? Guid.Empty);

                                    foreach (string beacon in Bids)
                                    {
                                        SkmBeaconMessageInfo mi = new SkmBeaconMessageInfo();
                                        mi.Guid = ved.Bid ?? Guid.Empty;
                                        mi.BeaconId = beacon;
                                        _skm.SkmBeaconMessageInfoSet(mi);
                                    }

                                    SkmFacade.SkmBeaconMessageTimeSlotInsert(beaconMessage, (IsTop ? (int)SkmBeaconType.Top : beconType), IsTop);
                                }
                            }
                            else
                            {
                                SkmBeaconMessage beaconMessage = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode);
                                if (beaconMessage != null)
                                {
                                    beaconMessage.Subject = Subject;
                                    beaconMessage.EffectiveStart = DateTime.Parse(BeginDate);
                                    beaconMessage.EffectiveEnd = DateTime.Parse(EndDate);
                                    beaconMessage.BusinessHourGuid = ved.Bid ?? Guid.Empty;
                                    beaconMessage.TimeStart = TimeStart;
                                    beaconMessage.TimeEnd = TimeEnd;
                                    beaconMessage.IsTop = IsTop;
                                    beaconMessage.Status = Status;
                                    beaconMessage.BeaconType = beconType;
                                    beaconMessage.ModifyTime = DateTime.Now;
                                    beaconMessage.ModifyUser = this.UserName;
                                    _skm.SkmBeaconMessageSet(beaconMessage);

                                    //刪除後重新Insert
                                    _skm.SkmBeaconMessageInfoDelete(ved.Bid ?? Guid.Empty);

                                    foreach (string beacon in Bids)
                                    {
                                        SkmBeaconMessageInfo mi = new SkmBeaconMessageInfo();
                                        mi.Guid = ved.Bid ?? Guid.Empty;
                                        mi.BeaconId = beacon;
                                        _skm.SkmBeaconMessageInfoSet(mi);
                                    }

                                    SkmFacade.SkmBeaconMessageTimeSlotInsert(beaconMessage, (IsTop ? (int)SkmBeaconType.Top : beconType), IsTop);
                                }
                            }
                        }
                    }
                }


                flag = Id;
            }
            catch
            {

            }

            return Json(flag);
        }
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult ShoppGet(Guid seller_guid)
        {
            var data = _skm.SkmShoppeGetAllShoppe(seller_guid)
                .Where(x => !string.IsNullOrEmpty(x.ShopName))
                .Select(x => new { x.ShopCode, x.ShopName })
                .Distinct();
            return Json(data
                .ToDictionary(x => x.ShopCode, x => x.ShopName));
        }
        [HttpPost]
        public JsonResult ShoppFloorGet(Guid sellerGuid, string shopCode)
        {
            var data = getFloor(sellerGuid, shopCode);
            return Json(data);
        }
        [HttpPost]
        public JsonResult ShoppMapGet(Guid sellerGuid, string shopCode, string floor)
        {
            var shopCodeCol = _skm.SkmShoppeGetShopCodeBySellerGuid(sellerGuid);
            if (!shopCodeCol.Any()) return Json(string.Empty);
            if (!shopCodeCol.Contains(shopCode)) return Json(string.Empty);

            string[] filter = new string[3];
            filter[0] = "action_event_info_id=" + SkmActionEventInfoId;
            filter[1] = "shop_code=" + shopCode;
            filter[2] = "floor=" + floor;

            ActionEventDeviceInfoCollection aedic = _mp.ActionEventDeviceInfoGetList(1, 999, "minor", true, filter);
            return Json(aedic);
        }
        [HttpPost]
        public JsonResult SkmMapImagesGet(int shopCode, string floor)
        {
            floor = floor.ToLower().Replace("f", string.Empty);
            if (floor.Substring(0, 1) == "0" && floor.Length > 1) floor = floor.Substring(1, floor.Length - 1);
            return Json(string.Format("{0}/{1}.jpg", shopCode, floor));
        }
        [HttpPost]
        public JsonResult BindBeacon(Guid sellerGuid, string shopCode, string floor)
        {
            List<ActionEventDeviceInfo> beacons = new List<ActionEventDeviceInfo>();
            if (SkmActionEventInfoId != 0)
            {
                beacons = _mp.ActionEventDeviceInfoListGet(SkmActionEventInfoId).Where(x => x.ShopCode == shopCode && x.Floor == floor).ToList();
            }
            return Json(beacons);
        }
        [HttpPost]
        public JsonResult BindBeaconData(string beacons)
        {
            string[] beaconsArray = new JsonSerializer().Deserialize<string[]>(beacons);
            List<string> ret = new List<string>();

            var allShop = _sp.SkmShoppeGetShopCodeNameList();
            if (!string.IsNullOrEmpty(beacons))
            {
                foreach (string beacon in beaconsArray)
                {
                    string[] filter = new string[3];
                    filter[0] = "major=" + beacon.Substring(0, 4);
                    filter[1] = "minor=" + beacon.Substring(4);
                    filter[2] = "action_event_info_id=" + SkmActionEventInfoId;
                    ActionEventDeviceInfo aed = _mp.ActionEventDeviceInfoGetList(1, 50, "", true, filter).FirstOrDefault() ?? new ActionEventDeviceInfo();
                    var shoppe = allShop.FirstOrDefault(x => x.Key == aed.ShopCode);
                    if (aed.IsLoaded && !string.IsNullOrEmpty(shoppe.Value))
                    {
                        ret.Add(shoppe.Value + "_" + aed.Floor + "_" + beacon.Substring(4));
                    }
                }
            }

            return Json(ret);
        }
        [HttpPost]
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public JsonResult SkmBeaconMessageSortUpdate(string data, bool isHorizontal, bool isVertical, string shoppe, string floor, bool isDeal)
        {
            bool flag = true;
            string message = "";
            try
            {
                List<SeqDeals> sdsus = new JsonSerializer().Deserialize<List<SeqDeals>>(data);

                foreach (SeqDeals sdsu in sdsus)
                {
                    /*
                     * 水平
                     * */
                    if (isHorizontal)
                    {
                        string[] filter = new string[1];
                        filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;

                        SkmBeaconMessageTimeSlotCollection sdts = _skm.SkmBeaconMessageTimeSlotGetListByDate("", sdsu.DealDate, sdsu.DealDate, filter);
                        foreach (var sdt in sdts)
                        {
                            if (sdt.BusinessHourGuid != Guid.Empty)
                            {
                                sdt.Sequence = sdsu.Sequence;
                                sdt.Status = sdsu.Status;
                                _skm.SkmBeaconMessageTimeSlotSet(sdt);
                            }
                        }
                    }
                    /*
                     * 垂直
                     * */
                    if (isVertical)
                    {
                        string[] filter = new string[3];
                        filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;
                        filter[1] = "beacon_id=" + sdsu.BeaconId;

                        SkmBeaconMessageTimeSlotCollection sdts = _skm.SkmBeaconMessageTimeSlotGetList("", filter);
                        foreach (var sdt in sdts)
                        {
                            if (sdt.BusinessHourGuid != Guid.Empty)
                            {
                                sdt.Sequence = sdsu.Sequence;
                                sdt.Status = sdsu.Status;
                                _skm.SkmBeaconMessageTimeSlotSet(sdt);
                            }
                        }
                    }
                    if (!isHorizontal && !isVertical)
                    {
                        //單筆
                        if (sdsu.BusinessHourGuid != Guid.Empty)
                        {
                            if (sdsu.BeaconId == null)
                            {
                                //其他
                                if (isDeal)
                                {
                                    string[] filter = new string[3];
                                    filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;
                                    filter[1] = "shop_code=" + shoppe;
                                    filter[2] = "floor=" + floor;
                                    var sdts = _skm.SkmBeaconMessageTimeSlotGetListByDate("", sdsu.DealDate, sdsu.DealDate, filter).FirstOrDefault();
                                    if (sdts != null)
                                    {
                                        sdts.Sequence = sdsu.Sequence;
                                        sdts.Status = sdsu.Status;
                                        _skm.SkmBeaconMessageTimeSlotSet(sdts);
                                    }
                                }
                                else
                                {
                                    string[] filter = new string[1];
                                    filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;
                                    SkmBeaconMessageTimeSlot sdts = _skm.SkmBeaconMessageTimeSlotGetListByDate("", sdsu.DealDate, sdsu.DealDate, filter).FirstOrDefault();
                                    if (sdts != null)
                                    {
                                        sdts.Sequence = sdsu.Sequence;
                                        sdts.Status = sdsu.Status;
                                        _skm.SkmBeaconMessageTimeSlotSet(sdts);
                                    }
                                }
                            }
                            else
                            {
                                //專案
                                string[] filter = new string[2];
                                filter[0] = "business_hour_guid=" + sdsu.BusinessHourGuid;
                                filter[1] = "beacon_id=" + sdsu.BeaconId;
                                SkmBeaconMessageTimeSlot sdts = _skm.SkmBeaconMessageTimeSlotGetListByDate("", sdsu.DealDate, sdsu.DealDate, filter).FirstOrDefault();
                                if (sdts != null)
                                {
                                    sdts.Sequence = sdsu.Sequence;
                                    sdts.Status = sdsu.Status;
                                    _skm.SkmBeaconMessageTimeSlotSet(sdts);
                                }
                            }

                        }
                    }

                }
            }
            catch (Exception ex)
            {
                flag = false;
                message = ex.Message;
            }

            return Json(new { Success = flag, Message = message });
        }

        private Dictionary<string, string> getBeaconTypes()
        {
            Dictionary<string, string> beaconType = new Dictionary<string, string>();
            foreach (var item in Enum.GetValues(typeof(SkmBeaconType)))
            {
                beaconType.Add(((int)item).ToString(), Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SkmBeaconType)item));
            }
            return beaconType;
        }

        /// <summary>
        /// 改良版
        /// </summary>
        /// <param name="bd"></param>
        /// <param name="skmBeaconType"></param>
        /// <param name="shopCode"></param>
        /// <param name="floor"></param>
        /// <returns></returns>
        private List<SkmDealSort> GetBeaconDataListV1(DateTime bd, int skmBeaconType, string shopCode = "0", string floor = "")
        {
            List<SkmDealSort> SkmDealSort = new List<DataOrm.SkmDealSort>();
            string[] filter = new string[3];
            filter[0] = "beacon_type=" + skmBeaconType;
            if (shopCode != "0")
            {
                filter[1] = "shop_code=" + shopCode;
            }
            else
            {
                filter[1] = "beacon_type=" + skmBeaconType;
            }
            if (string.IsNullOrEmpty(floor))
            {
                filter[2] = "beacon_type=" + skmBeaconType;
            }
            else
            {
                filter[2] = string.Format("floor in ({0})", string.Join(",", SkmFacade.getFloor(floor)));
            }

            List<SkmBeaconMessageTimeSlot> smcs = _skm.SkmBeaconMessageTimeSlotGetListByDateV1("sequence", bd.AddDays(0).ToString("yyyy/MM/dd"), bd.AddDays(7).ToString("yyyy/MM/dd"), filter).ToList();
            SkmBeaconMessageInfoCollection beaconCollection = _skm.SkmBeaconMessageInfoAll();
            SkmBeaconMessageCollection aepmList = _skm.SkmBeaconMessageGetList("", "");

            for (int w = 0; w < 7; w++)
            {
                List<SeqDeals> SkmDeals = new List<SeqDeals>();
                // List<Guid> bids = smcs.Select(x => x.BusinessHourGuid ?? Guid.Empty).ToList();
                // ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(bids);
                //foreach (SkmBeaconMessageTimeSlot smc in smcs)
                foreach (SkmBeaconMessageTimeSlot smc in smcs.Where(p => p.EffectiveStart.Value.Date == bd.AddDays(w).Date || p.EffectiveEnd.Value.Date == bd.AddDays(w).Date))
                {
                    string[] filterBeacon = new string[2];
                    filterBeacon[0] = "";
                    var aepm = aepmList.Where(p => p.Guid.Equals(smc.BusinessHourGuid)).FirstOrDefault();
                    if (aepm != null)
                    {
                        SkmDeals.Add(new SeqDeals
                        {
                            Id = aepm.Id,
                            BusinessHourGuid = smc.BusinessHourGuid ?? Guid.Empty,
                            ItemName = aepm.Subject,
                            DealDate = bd.AddDays(w).ToString("yyyy/MM/dd HH:mm:ss"),
                            Sequence = smc.Sequence ?? 99999,
                            Status = smc.Status ?? (int)DealTimeSlotStatus.NotShowInPponDefault,
                            BeaconId = smc.BeaconId,
                            BeaconCount = beaconCollection.Where(p => p.Guid.Equals(smc.BusinessHourGuid)).Count()
                        });
                    }
                }
                SkmDealSort.Add(new SkmDealSort
                {
                    DealDate = bd.AddDays(w),
                    Deals = SkmDeals
                });
            }
            return SkmDealSort;
        }

        private List<SkmDealSort> GetBeaconDataList(DateTime bd, int skmBeaconType, string shopCode = "0", string floor = "")
        {
            List<SkmDealSort> SkmDealSort = new List<DataOrm.SkmDealSort>();
            for (int w = 0; w < 7; w++)
            {
                List<SeqDeals> SkmDeals = new List<SeqDeals>();

                string[] filter = new string[3];
                filter[0] = "beacon_type=" + skmBeaconType;
                if (shopCode != "0")
                {
                    filter[1] = "shop_code=" + shopCode;
                }
                else
                {
                    filter[1] = "beacon_type=" + skmBeaconType;
                }
                if (string.IsNullOrEmpty(floor))
                {
                    filter[2] = "beacon_type=" + skmBeaconType;
                }
                else
                {
                    filter[2] = string.Format("floor in ({0})", string.Join(",", SkmFacade.getFloor(floor)));
                }

                List<SkmBeaconMessageTimeSlot> smcs = _skm.SkmBeaconMessageTimeSlotGetListByDate("sequence", bd.AddDays(w).ToString("yyyy/MM/dd"), bd.AddDays(w).ToString("yyyy/MM/dd"), filter).ToList();

                List<Guid> bids = smcs.Select(x => x.BusinessHourGuid ?? Guid.Empty).ToList();
                // ViewPponDealCollection vpds = _pp.ViewPponDealGetByBusinessHourGuidList(bids);
                foreach (SkmBeaconMessageTimeSlot smc in smcs)
                {
                    string[] filterBeacon = new string[2];
                    filterBeacon[0] = "";
                    var aepm = _skm.SkmBeaconMessageGetList("", "Guid=" + smc.BusinessHourGuid).FirstOrDefault();
                    if (aepm != null)
                    {
                        SkmBeaconMessageInfoCollection beaconCollection = _skm.SkmBeaconMessageInfoGet(smc.BusinessHourGuid ?? Guid.Empty);
                        SkmDeals.Add(new SeqDeals
                        {
                            Id = aepm.Id,
                            BusinessHourGuid = smc.BusinessHourGuid ?? Guid.Empty,
                            ItemName = aepm.Subject,
                            DealDate = bd.AddDays(w).ToString("yyyy/MM/dd HH:mm:ss"),
                            Sequence = smc.Sequence ?? 99999,
                            Status = smc.Status ?? (int)DealTimeSlotStatus.NotShowInPponDefault,
                            BeaconId = smc.BeaconId,
                            BeaconCount = beaconCollection.Count()
                        });
                    }
                }
                SkmDealSort.Add(new SkmDealSort
                {
                    DealDate = bd.AddDays(w),
                    Deals = SkmDeals
                });
            }
            return SkmDealSort;
        }


        #region Beacon電量管理

        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconPowerList(int? page, string seller, int? shop, int? floor, string filter)
        {
            ViewBag.SKMStores = SellersGetByUserId();

            SkmDealListModel model = new SkmDealListModel();

            //seller = seller ?? stores.FirstOrDefault().Key;
            //先預設信義新天地
            seller = seller ?? DefaultSeller;

            //Guid sellerGuid = new Guid(seller);

            //var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
            //var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(sellerGuid, false).ToList()
            //    .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
            //    .GroupBy(x => new { x.ShopCode, x.ShopName },
            //        (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
            //    .OrderBy(s => s.shopCode);

            //Dictionary<string, string> shops = seller == DefaultSeller ? skmShoppeShopCol.OrderBy(s => Int32.Parse(Regex.Match(s.shopCode, @"\d+").Value))
            //   .ToDictionary(x => x.shopCode, x => x.shopName) : skmShoppeShopCol.ToDictionary(x => x.shopCode, x => x.shopName);

            //if (shops.Any())
            //{
            //    shop = shop ?? int.Parse(shops.FirstOrDefault().Key);
            //}

            //model.Shop = shop ?? 0;

            //ddlelectricpower
            Dictionary<string, string> electricLevels = new Dictionary<string, string>();
            electricLevels.Add("2", "充足電量");
            electricLevels.Add("1", "一般電量");
            electricLevels.Add("0", "低電量");

            int pageCount = 10;
            //int lastPage = newlist.Count > 0 ? newlist.Count / pageCount + (newlist.Count % pageCount > 0 ? 1 : 0) : 1;
            page = page ?? 1;
            //page = page <= lastPage ? page : 1;
            int skip = page > 1 ? (page.Value - 1) * pageCount : 0;
            filter = filter ?? string.Empty;
            int flag = 0;


            model.Store = seller;
            model.Page = page.Value;
            model.LastPage = 1;
            model.Filter = flag;
            model.IsSkmUser = User.IsInRole("SkmProducter");

            //set filter
            //ViewBag.SKMShops = shops;
            //ViewBag.SKMFloors = shops;
            ViewBag.SKMElectricLevels = electricLevels;
            ViewBag.SelectedectricLevel = 2;
            return View(model);
        }
        [HttpPost]
        public JsonResult BeaconDeviceInfoGet(Guid sellerGuid, string shopCode, string floor, int electricPower, string orderBy, bool isDesc)
        {
            List<ActionEventDeviceInfo> beaconDeviceInfos = new List<ActionEventDeviceInfo>();

            if (SkmActionEventInfoId != 0)
            {
                beaconDeviceInfos = _mp.ActionEventDeviceInfoListGet(SkmActionEventInfoId).Where(x => x.ShopCode == shopCode).ToList();

                if (!string.IsNullOrEmpty(floor))
                {
                    beaconDeviceInfos = beaconDeviceInfos.Where(x => x.Floor == floor).OrderBy(x => x.ElectricPower).ToList();
                }

                if (electricPower.Equals((int)BeaconPowerLevel.Low))
                {
                    beaconDeviceInfos = beaconDeviceInfos.Where(x => x.ElectricPower <= 2.3).ToList();
                }
                else if (electricPower.Equals((int)BeaconPowerLevel.Medium))
                {
                    beaconDeviceInfos = beaconDeviceInfos.Where(x => 2.3 < x.ElectricPower && x.ElectricPower < 2.6).ToList();
                }
                else //if (electricPower.Equals((int) BeaconPowerLevel.Full)
                {
                    beaconDeviceInfos = beaconDeviceInfos.Where(x => x.ElectricPower >= 2.6).ToList();
                }
            }
            List<SkmBeaconDeviceInfo> skmBeaconDeviceInfos = new List<SkmBeaconDeviceInfo>();
            foreach (var beaconDeviceInfo in beaconDeviceInfos)
            {
                skmBeaconDeviceInfos.Add(new SkmBeaconDeviceInfo
                {
                    Major = beaconDeviceInfo.Major,
                    Minor = beaconDeviceInfo.Minor,
                    ElectricPower = beaconDeviceInfo.ElectricPower.ToString(),
                    DeviceName = beaconDeviceInfo.DeviceName,
                    Floor = beaconDeviceInfo.Floor,
                    LastUpdateTime = (beaconDeviceInfo.LastUpdateTime.HasValue) ? beaconDeviceInfo.LastUpdateTime.Value.ToString("yyyy/MM/dd HH:mm:ss") : "----"
                });
            }

            switch (orderBy)
            {
                case "LastUpdateTime":
                    beaconDeviceInfos = isDesc ? beaconDeviceInfos.OrderByDescending(x => x.LastUpdateTime).ToList() : beaconDeviceInfos.OrderBy(x => x.LastUpdateTime).ToList();
                    break;
                case "Floor":
                    beaconDeviceInfos = isDesc ? beaconDeviceInfos.OrderByDescending(x => x.Floor).ToList() : beaconDeviceInfos.OrderBy(x => x.Floor).ToList();
                    break;
                default:
                    beaconDeviceInfos = isDesc ? beaconDeviceInfos.OrderByDescending(x => x.DeviceName).ToList() : beaconDeviceInfos.OrderBy(x => x.DeviceName).ToList();
                    break;
            }

            return Json(beaconDeviceInfos);
        }
        [Authorize(Roles = "BeaconManager, Administrator, ME2O, Production")]
        public ActionResult SkmAppBeaconNotification()
        {
            //ddlelectricpower
            Guid sellerGuid = Guid.Parse(Request.Form["sellerGuid"]);
            var shopCodeList = _skm.SkmShoppeGetShopCodeBySellerGuid(sellerGuid);

            Dictionary<string, string> electricLevels = new Dictionary<string, string>();
            electricLevels.Add("0", "低電量");

            JsonSerializer json = new JsonSerializer();
            string systemDataName = string.Format("{0}{1}", SkmFacade.BeaconPowerNoticeSystemData, shopCodeList.First());
            var systemData = _sysp.SystemDataGet(systemDataName);
            if (!systemData.IsLoaded)
            {
                systemData = new SystemData();
                systemData.Name = systemDataName;
                Dictionary<string, string> settingDictionary = new Dictionary<string, string>();
                settingDictionary.Add("beaconPowerLevel", "0");
                settingDictionary.Add("notificationEmails", "");
                settingDictionary.Add("eletricPowerNotUpdateWaitDay", "30");
                systemData.Data = json.Serialize(settingDictionary);
                systemData.CreateId = "sys@17life.com";
                systemData.CreateTime = DateTime.Now;
            }
            var setting = json.Deserialize<Dictionary<string, string>>(systemData.Data);

            ViewBag.BeaconPowerLevel = setting["beaconPowerLevel"];
            ViewBag.NotificationEmails = setting["notificationEmails"];
            ViewBag.EletricPowerNotUpdateWaitDay = setting["eletricPowerNotUpdateWaitDay"];
            ViewBag.ShopCode = string.Join(",", shopCodeList);
            ViewBag.SKMElectricLevels = electricLevels;

            return View();
        }
        [HttpPost]
        public JsonResult SkmAppBeaconNotificationSetting(int beaconPowerLevel, string notificationEmails, int eletricPowerNotUpdateWaitDay, string shopCode)
        {
            bool updateflag = true;
            foreach (var sc in shopCode.Split(','))
            {
                JsonSerializer json = new JsonSerializer();
                string systemDataName = string.Format("{0}{1}", SkmFacade.BeaconPowerNoticeSystemData, sc);
                var systemData = _sysp.SystemDataGet(systemDataName);

                if (!systemData.IsLoaded)
                {
                    systemData = new SystemData();
                    systemData.Name = systemDataName;
                    Dictionary<string, string> settingDictionary = new Dictionary<string, string>();
                    settingDictionary.Add("beaconPowerLevel", "0");
                    settingDictionary.Add("notificationEmails", "");
                    settingDictionary.Add("eletricPowerNotUpdateWaitDay", "30");
                    systemData.Data = json.Serialize(settingDictionary);
                    systemData.CreateId = "sys@17life.com";
                    systemData.CreateTime = DateTime.Now;
                }
                systemData.ModifyId = this.HttpContext.User.Identity.Name;
                systemData.ModifyTime = DateTime.Now;
                try
                {
                    var setting = json.Deserialize<Dictionary<string, string>>(systemData.Data);

                    setting["beaconPowerLevel"] = beaconPowerLevel.ToString();
                    setting["notificationEmails"] = notificationEmails;
                    setting["eletricPowerNotUpdateWaitDay"] = eletricPowerNotUpdateWaitDay.ToString();
                    systemData.Data = json.Serialize(setting);

                    _sysp.SystemDataSet(systemData);
                }
                catch
                {
                    updateflag = false;
                }
            }
            return Json(updateflag);
        }

        #endregion Beacon電量管理

        #endregion

        #region SkmBeaconSetup
        [Authorize(Roles = "BeaconMapManager, Administrator, ME2O, Production")]
        public ActionResult SkmBeaconSetup(string qSeller, string shopCode, string ddlFloor)
        {
            SetPageDefault();

            //與SetPageDefault 重複做了綁定權限seller的事
            //Dictionary<string, string> sellers = SellersGetByUserId();
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }


            Guid guid = new Guid(qSeller);

            var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
            var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(guid, false).ToList()
                .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
                .GroupBy(x => new { x.ShopCode, x.ShopName },
                    (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
                .OrderBy(s => s.shopCode);

            var shops = qSeller == DefaultSeller ? skmShoppeShopCol.OrderBy(s => int.Parse(Regex.Match(s.shopCode, @"\d+").Value))
               .Select(s => new { key = s.shopCode, value = s.shopName }) : skmShoppeShopCol.Select(s => new { key = s.shopCode, value = s.shopName });

            if (shopCode == null && shops.Any())
            {
                shopCode = shops.FirstOrDefault().key;
            }

            if (string.IsNullOrEmpty(ddlFloor))
            {
                var data = getFloor(Guid.Parse(qSeller), shopCode);
                foreach (var d in data)
                {
                    ddlFloor = d.Floor;
                    break;
                }
            }

            ViewBag.qSeller = qSeller;
            ViewBag.shopCode = shopCode;
            //ViewBag.SkmSellers = sellers;
            ViewBag.SkmSellers = ViewBag.SKMStores;
            ViewBag.Floor = ddlFloor;

            return View();
        }

        [HttpPost]
        public JsonResult SkmBeaconLocSave(Guid sellerGuid, string shopCode, string floor, string data)
        {
            bool flag = false;
            try
            {
                List<ActionEventDeviceInfo> beacons = new List<ActionEventDeviceInfo>();
                if (SkmActionEventInfoId != 0)
                {
                    beacons = _mp.ActionEventDeviceInfoListGet(SkmActionEventInfoId).Where(x => x.ShopCode == shopCode && x.Floor == floor).ToList();
                }
                List<ActionEventDeviceInfo> info = new JsonSerializer().Deserialize<List<ActionEventDeviceInfo>>(data);

                foreach (var fo in info)
                {
                    ActionEventDeviceInfo aedi = beacons.Where(x => x.Id == fo.Id).FirstOrDefault();
                    if (aedi != null)
                    {
                        aedi.DeviceName = fo.DeviceName;
                        aedi.Xtop = fo.Xtop;
                        aedi.Xleft = fo.Xleft;
                        aedi.LastUpdateTime = DateTime.Now;
                        _mp.ActionEventDeviceInfoSet(aedi);
                    }
                }
                flag = true;
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            return Json(flag);
        }

        #endregion

        #region SkmAppBeaconPowerMaps
        public ActionResult SkmAppBeaconPowerMaps(string qSeller, string shopCode, string ddlFloor)
        {
            SetPageDefault();
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            if (qSeller == null)
            {
                qSeller = SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString();
            }


            Guid guid = new Guid(qSeller);

            var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
            var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(guid, false).ToList()
                .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
                .GroupBy(x => new { x.ShopCode, x.ShopName },
                    (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
                .OrderBy(s => s.shopCode);

            var shops = qSeller == DefaultSeller ? skmShoppeShopCol.OrderBy(s => int.Parse(Regex.Match(s.shopCode, @"\d+").Value))
               .Select(s => new { key = s.shopCode, value = s.shopName }) : skmShoppeShopCol.Select(s => new { key = s.shopCode, value = s.shopName });

            if (shopCode == null)
            {
                shopCode = shops.FirstOrDefault().key;
            }

            if (string.IsNullOrEmpty(ddlFloor))
            {
                var data = getFloor(Guid.Parse(qSeller), shopCode);
                foreach (var d in data)
                {
                    ddlFloor = d.Floor;
                    break;
                }
            }

            ViewBag.qSeller = qSeller;
            ViewBag.shopCode = shopCode;
            //ViewBag.SkmSellers = sellers;
            ViewBag.SkmSellers = ViewBag.SKMStores;
            ViewBag.Floor = ddlFloor;

            return View();
        }
        #endregion SkmAppBeaconPowerMaps

        #region SkmPayBanner

        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SkmPayBannerList(int? page)
        {
            var bannerData = _skmef.GetSkmPayBannerList();
            var model = new List<SkmPayBannerListResponse>();
            #region 分頁

            int pageCnt = 30;
            int totalLogCount = bannerData.Count();
            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / pageCnt;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            ViewData["Order"] = string.Empty;
            ViewData["IsOrderASC"] = false;
            if (totalLogCount % pageCnt > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }

            model = (from q in bannerData
                     orderby GetPropertyValue(q, "Id") descending
                     select new SkmPayBannerListResponse(q)).Skip(pageCnt * ((page ?? 1) - 1)).Take(pageCnt).ToList();

            #endregion

            return View(model);
        }

        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SkmPayBannerPreview(int id)
        {
            ViewBag.Oauth = _config.SkmBackendOauth;
            ViewBag.Site = _config.SSLSiteUrl;
            var bannerContent = _skmef.GetSkmPayBanner(id);
            ViewBag.BannerUrl = bannerContent.Id != 0 ? SkmFacade.SkmAppStyleImgPath(bannerContent.BannerPath) : string.Empty;
            ViewBag.PreviewContent = bannerContent.Id != 0 ? bannerContent.ContentHtml : string.Empty;
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public JsonResult SkmPayBannerStatusUpdate(int id)
        {
            bool success = true;

            try
            {
                var payBanner = _skmef.GetSkmPayBanner(id);
                if (payBanner.Id > 0)
                {
                    payBanner.IsHidden = !payBanner.IsHidden;
                    _skmef.SetSkmPayBanner(payBanner, User.Identity.Name);
                }

            }
            catch
            {
                success = false;
            }

            return Json(new { Success = success });
        }

        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SkmPayBannerEdit(int? id)
        {
            SkmPayBanner payBanner = id == null || id == 0 ? new SkmPayBanner() : _skmef.GetSkmPayBanner((int)id);
            return View(payBanner);
        }

        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SkmPayBannerSave(FormCollection data)
        {
            string startTimeStr = string.Format("{0} {1}:{2}:00", data["bannerStartDate"].ToString(),
                data["bannerStartHour"].ToString(), data["bannerStartMinute"].ToString());

            string endTimeStr = string.Format("{0} {1}:{2}:00", data["bannerEndDate"].ToString(),
                data["bannerEndHour"].ToString(), data["bannerEndMinute"].ToString());

            string fileName = string.Empty;
            string oriFileName = string.Empty;

            var imgFileResult = SkmFacade.CheckImgType(702, 160, "SkmPayBanner", Guid.NewGuid());
            if (imgFileResult.SuccessLoadFile && imgFileResult.ImgFiles.Any())
            {
                fileName = imgFileResult.ImgFiles.First().SaveFileName;
                oriFileName = imgFileResult.ImgFiles.First().OriFileName;
            }

            if (imgFileResult.SuccessLoadFile && imgFileResult.ImgFiles.Any()
                                              && (!imgFileResult.ImgFiles.First().IsExtensionPass
                || !imgFileResult.ImgFiles.First().IsSizePass))
            {
                return Content("<script>alert('僅能上傳圖片檔案，大小為 702 x 160');history.go(-1);</script>");
            }

            if (fileName != string.Empty && oriFileName != string.Empty)
            {
                HttpPostedFileBase hpf = Request.Files[oriFileName] as HttpPostedFileBase;
                hpf.SaveAs(imgUploadDirPath + fileName);
            }
            else
            {
                fileName = data["oriImgPath"].ToString();
            }

            var payBanner = new SkmPayBanner
            {
                Id = data["bannerId"].ToString() == "0" ? 0 : int.Parse(data["bannerId"].ToString()),
                Name = data["bannerName"].ToString(),
                StartTime = DateTime.Parse(startTimeStr),
                EndTime = DateTime.Parse(endTimeStr),
                ContentHtml = Server.HtmlDecode(data["bannerHtml"].ToString()),
                BannerPath = fileName
            };

            _skmef.SetSkmPayBanner(payBanner, User.Identity.Name);

            return RedirectToAction("SkmPayBannerList");
        }

        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SkmPayBannerSort()
        {
            var data = _skmef.GetSkmPayBannerSortData();
            return View(data);
        }

        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public JsonResult SkmPayBannerSortUpdate(string input)
        {
            bool flag = true;
            try
            {
                List<SkmAppBannerSort> data = new JsonSerializer().Deserialize<List<SkmAppBannerSort>>(input);
                foreach (SkmAppBannerSort sbs in data)
                {
                    SkmPayBanner banner = _skmef.GetSkmPayBanner(sbs.Id);
                    banner.Seq = sbs.Sort;
                    _skmef.SetSkmPayBanner(banner, User.Identity.Name);
                }
            }
            catch
            {
                flag = false;
            }

            return Json(new { Success = flag, Message = "" });
        }

        #endregion SkmPayBanner

        #region PrizeEvent

        /// <summary>
        /// 活動隱藏
        /// </summary>
        /// <param name="prizeDrawEventId"></param>
        /// <param name="oldStatus"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult PrizeDrawEventHide(int prizeDrawEventId, int oldStatus)
        {
            bool flag = true;
            int newStatus = (oldStatus == (int)SkmPrizeDrawEventStatus.Hide ? (int)SkmPrizeDrawEventStatus.Normal : (int)SkmPrizeDrawEventStatus.Hide);
            try
            {
                flag = _cep.UpdatePrizeDrawEventStatus(prizeDrawEventId, newStatus);
            }
            catch
            {
                flag = false;
            }
            return Json(new { Success = flag });
        }

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult RefreshBidData(string bid, int itemId)
        {
            ViewPrizeExternalDeal bidData = new ViewPrizeExternalDeal();
            int drawOutCount = 0;
            try
            {
                bidData = _cep.GetExternalDealRemainingQuantity(new Guid(bid));
                if (itemId != 0)
                {
                    var drwOutList = _cep.GetPrizeDrawRecordByItemId(itemId, PrizeDrawRecordStatus.PrizeDrawCompleted);
                    drawOutCount = drwOutList.Count;
                }
            }
            catch (Exception e)
            {

            }
            return Json(new
            {
                drawOutCount,
                count = bidData.RemainingQuantity,
                sd = bidData.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm"),
                ed = bidData.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm")
            });
        }


        /// <summary>
        /// 活動獎項商品下拉選單
        /// </summary>
        /// <param name="sd"></param>
        /// <param name="st"></param>
        /// <param name="ed"></param>
        /// <param name="et"></param>
        /// <param name="joinFee"></param>
        /// <param name="pdei"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult PrizeBidList(string sd, string st, string ed, string et, int joinFee, int pdei)
        {
            DateTime std = Convert.ToDateTime(sd);
            DateTime stt = Convert.ToDateTime(st);
            DateTime etd = Convert.ToDateTime(ed);
            DateTime ett = Convert.ToDateTime(et);

            var ddlList = _cep.GetPrizeDrawEventDDLList(std.AddHours(stt.Hour).AddMinutes(stt.Minute), etd.AddHours(ett.Hour).AddMinutes(ett.Minute), joinFee);
            return Json(ddlList);
        }

        /// <summary>
        /// 判斷滿版圖是否只存在一個檔期內
        /// </summary>
        /// <param name="sd"></param>
        /// <param name="st"></param>
        /// <param name="ed"></param>
        /// <param name="et"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult CheckIsOtherEventOnline(string sd, string st, string ed, string et, int id)
        {
            DateTime std = Convert.ToDateTime(sd);
            DateTime stt = Convert.ToDateTime(st);
            DateTime etd = Convert.ToDateTime(ed);
            DateTime ett = Convert.ToDateTime(et);

            var isOverlapping = _cep.CheckIsOtherEventOnline(std.AddHours(stt.Hour).AddMinutes(stt.Minute), etd.AddHours(ett.Hour).AddMinutes(ett.Minute), id);
            return Json(isOverlapping);
        }


        /// <summary>
        /// 活動列表
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult PrizeDrawEventList(PrizeDrawEventListModel model)
        {
            //get and order
            model.PrizeDrawEventList = _cep.GetPrizeDrawEventList(model.OrderField, model.Sort);
            if (model.Sort == 0)
            {
                model.Sort = 1;
            }
            else
            {
                model.Sort = 0;
            }
            //pager
            model.TotalCount = model.PrizeDrawEventList.Count();
            model.PrizeDrawEventList = model.PrizeDrawEventList.Skip(model.Skip).Take(model.Take).ToList();

            return View(model);
        }

        /// <summary>
        /// 活動新增/編輯
        /// </summary>
        /// <param name="prizeDrawEventId"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult PrizeDrawEventAdd(int prizeDrawEventId = 0)
        {
            var model = new PrizeDrawEventModel();
            if (prizeDrawEventId != 0)//編輯edit
            {
                model = SkmFacade.GetGetPrizeDrawEvent(prizeDrawEventId);
            }

            model.SellerGuidDDL = GetSellerGuidDDL();
            //因為增加總店的選項，所以這邊把第一個選項變成總店
            model.SellerGuidDDL.Insert(1, new SelectListItem() { Text = "總店", Value = _config.SkmRootSellerGuid.ToString() });
            return View(model);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public JsonResult SetPrizeItemUseOrNot(SetPrizeItemUseOrNotModel model)
        {
            PrizeDrawItem prizeDrawItem = SkmFacade.SetPrizeItemUseOrNot(model.Id);
            if (prizeDrawItem.Id > 0)
            {
                return Json(new { IsSuccess = true, IsUse = prizeDrawItem.IsUse });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "查無獎品資料" });
            }
        }

        private List<SelectListItem> GetSellerGuidDDL()
        {
            List<Guid> list = _sp.SellerGetSkmParentList().Select(s => s.Guid).ToList();
            var shops = _sp.SellerGetList(list);
            List<SelectListItem> data = new List<SelectListItem>();
            data.Add(new SelectListItem()
            {
                Value = Guid.Empty.ToString(),
                Text = "抽獎點數轉拋店"
            });

            foreach (Seller item in shops)
            {
                SelectListItem info = new SelectListItem();
                info.Text = item.SellerName;
                info.Value = item.Guid.ToString();
                data.Add(info);
            }
            return data;
        }
        /// <summary>
        /// 活動create/update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, Administrator, ME2O, Production")]
        public ActionResult PrizeDrawEventSave(PrizeDrawEventModel model)
        {
            SystemFacade.SetApiLog("PrizeDrawEventSave.Start", User.Identity.Name, model, null, Helper.GetClientIP());
            var changePrizeDrawItems = JsonConvert.DeserializeObject<List<PrizeDrawAddItemModel>>(model.PrizeDrawItems);
            _skm.SkmLogSet(new SkmLog()
            {
                SkmToken = "PrizeDrawEventSave",
                UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                LogType = (int)SkmLogType.PrizeDrawEventSave,
                InputValue = JsonConvert.SerializeObject(changePrizeDrawItems),
                OutputValue = "changePrizeDrawItems"
            });
            DateTime tsd = Convert.ToDateTime(string.Format("{0} {1}", model.StartDate, model.StartTime));
            DateTime ted = Convert.ToDateTime(string.Format("{0} {1}", model.EndDate, model.EndTime));
            string errorMsg = string.Empty;

            _skm.SkmLogSet(new SkmLog()
            {
                SkmToken = "PrizeDrawEventSave",
                UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                LogType = (int)SkmLogType.PrizeDrawEventSave,
                InputValue = string.Format("tsd={0}, ted={1}", tsd.ToString("yyyy/MM/dd HH:mm:ss"), ted.ToString("yyyy/MM/dd HH:mm:ss")),
                OutputValue = "tsd,ted"
            });

            #region 驗證
            //活動時間&燒點點數
            List<PrizeDrawEventDDLModel> prizeDrawEventDDLModels = _cep.GetPrizeDrawEventDDLList(tsd, ted, model.JoinFee);
            _skm.SkmLogSet(new SkmLog()
            {
                SkmToken = "PrizeDrawEventSave",
                UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                LogType = (int)SkmLogType.PrizeDrawEventSave,
                InputValue = JsonConvert.SerializeObject(prizeDrawEventDDLModels),
                OutputValue = "prizeDrawEventDDLModels"
            });
            changePrizeDrawItems.ForEach((item) =>
            {
                Guid bid = new Guid(item.bid);
                _skm.SkmLogSet(new SkmLog()
                {
                    SkmToken = "PrizeDrawEventSave",
                    UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                    LogType = (int)SkmLogType.PrizeDrawEventSave,
                    InputValue = string.Format("bid={0}", bid.ToString()),
                    OutputValue = "changePrizeDrawItems foreach bid"
                });
                if (!prizeDrawEventDDLModels.Any(deal => deal.Bid == bid))
                {
                    errorMsg += "商品名稱:" + item.name + "\\n";
                }
            });
            if (!string.IsNullOrEmpty(errorMsg))
            {
                errorMsg = "以下獎品與活動條件不符合:" + "\\n" + errorMsg;
            };
            //機率，暫時相信前端驗證
            if (!string.IsNullOrEmpty(errorMsg))
            {
                model.SellerGuidDDL = GetSellerGuidDDL();
                model.PrizeDrawItemList = ReSetDDlList(changePrizeDrawItems);
                model.EventPrize = model.PrizeDrawItemList.Sum(p => p.Rate);
                TempData["error"] = errorMsg;
                SystemFacade.SetApiLog("PrizeDrawEventSave.Error", User.Identity.Name, model, errorMsg, Helper.GetClientIP());
                return View("PrizeDrawEventAdd", model);
            }
            #endregion

            if (model.Id == 0)
            {
                #region create

                var newdata = new PrizeDrawEvent()//活動新增
                {
                    Token = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 25),
                    ChannelOdmId = SkmChannelId,
                    StartDate = Convert.ToDateTime(model.StartDate).AddHours(tsd.Hour).AddMinutes(tsd.Minute),
                    EndDate = Convert.ToDateTime(model.EndDate).AddHours(ted.Hour).AddMinutes(ted.Minute),
                    Subject = model.Subject,
                    JoinFee = model.JoinFee,
                    EventContent = HttpUtility.UrlDecode(model.EventContent.Trim()).Replace("\n", ""),
                    FullImg = (model.FullImgFile == null) ? string.Empty : SaveImg(model.FullImgFile),
                    SmallImg = (model.SmallImgFile == null) ? string.Empty : SaveImg(model.SmallImgFile),
                    PrizeImg = (model.PrizeImgFile == null) ? model.PrizeImg : SaveImg(model.PrizeImgFile),
                    WinningImg = (model.WinningSmallImgFile == null) ? model.WinningSmallImg : SaveImg(model.WinningSmallImgFile),
                    WinningBackgroundImg = (model.WinningBackgroundImgFile == null) ? model.WinningBackgroundImg : SaveImg(model.WinningBackgroundImgFile),
                    DailyLimit = model.DailyLimit,
                    TotalLimit = model.TotalLimit,
                    Temple = (int)SkmPrizeDrawEventOrderField.Temple,
                    CreateTime = DateTime.Now,
                    ModifyTime = DateTime.Now,
                    ModifyId = User.Identity.Name,
                    SellerGuid = string.IsNullOrEmpty(model.SellerGuid) ? Guid.Empty : new Guid(model.SellerGuid),
                    NoticeDate = Convert.ToDateTime(model.NoticeDate),
                    PeriodHour = model.PeriodHour,
                    Notice = model.Notice
                };

                try
                {
                    int prizeDrawEventId = _cep.InsertPrizeDrawEvent(newdata);
                    if (prizeDrawEventId != 0)//獎項新增
                    {
                        List<PrizeDrawItem> prizeDrawItems = ReSetList(prizeDrawEventId, changePrizeDrawItems);
                        if (_cep.InsertPrizeDrawItemList(prizeDrawItems))
                        {
                            SkmFacade.SetPrizeDrawItemPeriod(newdata, prizeDrawItems);
                            return RedirectToAction("PrizeDrawEventList");
                        }
                    }
                }
                catch (Exception e)
                {
                    errorMsg = "新增失敗" + e.Message;
                }

                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = "新增失敗";
                #endregion
            }
            else
            {
                #region udpate

                //圖片處理
                if (model.FullImgFile != null)
                {
                    model.FullImg = SaveImg(model.FullImgFile);
                }
                if (model.SmallImgFile != null)
                {
                    model.SmallImg = SaveImg(model.SmallImgFile);
                }
                if (model.PrizeImgFile != null)
                {
                    model.PrizeImg = SaveImg(model.PrizeImgFile);
                }
                if (model.WinningSmallImgFile != null)
                {
                    model.WinningSmallImg = SaveImg(model.WinningSmallImgFile);
                }
                if (model.WinningBackgroundImgFile != null)
                {
                    model.WinningBackgroundImg = SaveImg(model.WinningBackgroundImgFile);
                }

                var updateData = new PrizeDrawEvent()//活動新增
                {
                    Id = model.Id,
                    CreateTime = model.CreateTime,
                    Token = model.Token,
                    ChannelOdmId = model.ChannelOdmId,
                    PeriodHour = model.PeriodHour,
                    StartDate = Convert.ToDateTime(model.StartDate).AddHours(tsd.Hour).AddMinutes(tsd.Minute),
                    EndDate = Convert.ToDateTime(model.EndDate).AddHours(ted.Hour).AddMinutes(ted.Minute),
                    Subject = model.Subject,
                    JoinFee = model.JoinFee,
                    EventContent = model.EventContent,
                    PrizeImg = model.PrizeImg,
                    FullImg = string.IsNullOrEmpty(model.FullImg) ? string.Empty : model.FullImg,
                    SmallImg = string.IsNullOrEmpty(model.SmallImg) ? string.Empty : model.SmallImg,
                    WinningImg = model.WinningSmallImg,
                    WinningBackgroundImg = model.WinningBackgroundImg,
                    DailyLimit = model.DailyLimit,
                    TotalLimit = model.TotalLimit,
                    ModifyTime = DateTime.Now,
                    ModifyId = User.Identity.Name,
                    SellerGuid = string.IsNullOrEmpty(model.SellerGuid) ? Guid.Empty : new Guid(model.SellerGuid),
                    NoticeDate = Convert.ToDateTime(model.NoticeDate),
                    Notice = model.Notice
                };

                try
                {
                    //儲存event
                    if (_cep.UpdatePrizeDrawEvent(updateData))
                    {
                        var updateItemList = new List<PrizeDrawItem>();
                        var hideItemList = new List<PrizeDrawItem>();
                        var oldItemList = _cep.GetPrizeDrawItemsWithNoLock(updateData.Id, false);
                        var changeItemList = ReSetList(updateData.Id, changePrizeDrawItems);
                        foreach (var oldItem in oldItemList)
                        {
                            var changeItem = changeItemList.FirstOrDefault(p => p.Id == oldItem.Id);
                            if (changeItem != null) //新項目如果存在db就update
                            {
                                updateItemList.Add(changeItem);
                            }
                            else
                            {
                                oldItem.IsRemove = true;
                                hideItemList.Add(oldItem);
                            }
                        }

                        if (updateItemList.Any()) //修改
                            _cep.UpdatePrizeDrawItemListWithoutIsUse(updateItemList);
                        if (hideItemList.Any())//刪除設隱藏
                            _cep.UpdatePrizeDrawItemListWithoutIsUse(hideItemList);
                        var newItemList = changeItemList.Where(p => p.Id == 0).ToList();
                        if (newItemList.Any())
                            _cep.InsertPrizeDrawItemList(newItemList); //兩者差 新增

                        SkmFacade.SetPrizeDrawItemPeriod(updateData, changeItemList);

                        return RedirectToAction("PrizeDrawEventList");
                    }
                    errorMsg = "活動修改失敗";

                }
                catch (Exception e)
                {
                    errorMsg = "獎項修改失敗" + e.Message;
                }

                if (string.IsNullOrEmpty(errorMsg))
                    errorMsg = "修改失敗";

                #endregion
            }

            model.SellerGuidDDL = GetSellerGuidDDL();
            model.PrizeDrawItemList = ReSetDDlList(changePrizeDrawItems);
            model.EventPrize = model.PrizeDrawItemList.Sum(p => p.Rate);
            TempData["error"] = errorMsg;
            SystemFacade.SetApiLog("PrizeDrawEventSave.End", User.Identity.Name, model, errorMsg, Helper.GetClientIP());
            return View("PrizeDrawEventAdd", model);

        }

        private List<PrizeDrawEditItemModel> ReSetDDlList(List<PrizeDrawAddItemModel> addPrizeDrawItems)
        {
            List<PrizeDrawEditItemModel> itemList = new List<PrizeDrawEditItemModel>();
            foreach (var i in addPrizeDrawItems)
            {
                var pdi = new PrizeDrawEditItemModel
                {
                    Name = i.name,
                    Bid = new Guid(i.bid),
                    Rate = !string.IsNullOrEmpty(i.rate) ? Convert.ToDecimal(i.rate.Replace("%", "")) : 0,
                    Count = Convert.ToInt32(i.count.Replace(" ", "").Split("/").LastOrDefault()),
                    IsThanksFlag = i.isthanks
                };

                if (!string.IsNullOrEmpty(i.effectiveEndDate.Replace(" ", "")))
                {
                    pdi.ED = Convert.ToDateTime(i.effectiveEndDate);
                }
                if (!string.IsNullOrEmpty(i.effectiveStartDate.Replace(" ", "")))
                {
                    pdi.SD = Convert.ToDateTime(i.effectiveStartDate);
                }
                itemList.Add(pdi);
            }
            return itemList;

        }

        private List<PrizeDrawItem> ReSetList(int prizeDrawEventId, List<PrizeDrawAddItemModel> addPrizeDrawItems)
        {
            List<PrizeDrawItem> itemList = new List<PrizeDrawItem>();
            foreach (var i in addPrizeDrawItems)
            {
                var pdi = new PrizeDrawItem()
                {
                    Id = i.id,
                    PrizeType = (int)SkmPrizeDrawItemType.Bid, //預設
                    PrizeDrawEventId = prizeDrawEventId,
                    ExternalDealId = new Guid(i.bid),//存bid沒錯 是名字取錯了
                    WinningRate = !string.IsNullOrEmpty(i.rate) ? Convert.ToDecimal(i.rate.Replace("%", "")) : 0,
                    IsThanks = i.isthanks,
                    IsUse = true,
                    Qty = Convert.ToInt32(i.count.Replace(" ", "").Split("/").LastOrDefault()),
                };
                if (!string.IsNullOrEmpty(i.effectiveEndDate.Replace(" ", "")))
                {
                    pdi.EffectiveEndDate = Convert.ToDateTime(i.effectiveEndDate);
                }
                if (!string.IsNullOrEmpty(i.effectiveStartDate.Replace(" ", "")))
                {
                    pdi.EffectiveStartDate = Convert.ToDateTime(i.effectiveStartDate);
                }
                itemList.Add(pdi);
            }
            return itemList;
        }


        /// <summary>
        /// 圖片儲存
        /// </summary>
        /// <param name="hpf"></param>
        /// <param name="imgWLimit"></param>
        /// <param name="imgHLimit"></param>
        /// <returns></returns>
        private string SaveImg(HttpPostedFileBase hpf, int imgWLimit = 200, int imgHLimit = 500)
        {
            string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/" + SkmPrizeImgFolder + "/");
            string fileName = "";
            bool checkFile = true;

            if (hpf.FileName != "")
            {
                string Extension = hpf.FileName.Substring(hpf.FileName.LastIndexOf("."),
                    hpf.FileName.Length - hpf.FileName.LastIndexOf("."));
                //檢查圖片格式
                if (!SkmFacade.CheckImageFormat(Extension.ToLower()))
                {
                    checkFile = false;
                }

                if (checkFile)
                {
                    //System.Drawing.Image img = System.Drawing.Image.FromStream(hpf.InputStream);
                    //double imgW = img.Width;
                    //double imgH = img.Height;
                    ////檢查圖片大小
                    //if (imgW < imgWLimit || imgH < imgHLimit)
                    //{
                    //    checkFile = false;
                    //}
                    Guid _Guid = Guid.NewGuid();
                    fileName = string.Format("{0}{1}", _Guid.ToString() == "" ? "{0}" : _Guid.ToString(), Extension);
                    //save
                    hpf.SaveAs(baseDirectoryPath + fileName);
                }
            }
            return fileName;
        }

        #endregion

        #region 檔次列表

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMList(int? page, string seller, int? shop, string filter, int? category, int? tag, string dealType, string searchTitle)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SkmDealListModel model = new SkmDealListModel();
            model.PrizeList = !string.IsNullOrEmpty(dealType) && dealType.ToLower() == "prizedeals" && SkmFacade.CheckSkmHqPower(userId);
            SetPageDefault();
            string myDefaultSeller = DefaultSeller;
            //先預設信義新天地
            if (!((Dictionary<string, string>)ViewBag.SKMStores).Keys.Contains(DefaultSeller))
            {
                myDefaultSeller = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.First();
            }

            seller = model.PrizeList ? _config.SkmRootSellerGuid.ToString() : seller ?? myDefaultSeller;

            Guid sellerGuid = new Guid(seller);

            var roleShoppeGuids = ((List<Shoppes>)ViewBag.SKMShoppes).Select(g => g.StoreGuid);
            var skmShoppeShopCol = _sp.SkmShoppeCollectionGetBySeller(sellerGuid, false).ToList()
                                    .Where(x => roleShoppeGuids.Contains(x.StoreGuid.ToString()))
                                    .GroupBy(x => new { x.ShopCode, x.ShopName },
                                        (key, value) => new { shopCode = key.ShopCode, shopName = key.ShopName })
                                    .OrderBy(s => s.shopCode);

            Dictionary<string, string> shops = skmShoppeShopCol.ToDictionary(x => x.shopCode, x => x.shopName);

            if (!model.PrizeList)
            {
                shop = shop ?? int.Parse(shops.FirstOrDefault().Key);
            }

            Dictionary<string, string> categories = new Dictionary<string, string>();
            categories.Add("", "全部");
            _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryType == (int)CategoryType.DealCategory && x.CategoryStatus == (int)CategoryStatus.Enabled)
                    .ForEach(x =>
                        categories.Add(x.CategoryId.ToString(), x.CategoryName)
                );

            Dictionary<string, string> tags = new Dictionary<string, string>();
            tags.Add("", "全部");
            Enum.GetNames(typeof(SkmDealTypes)).ForEach(x =>
                tags.Add(((int)Enum.Parse(typeof(SkmDealTypes), x)).ToString(), Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (SkmDealTypes)Enum.Parse(typeof(SkmDealTypes), x)))
            );

            List<ViewExternalDeal> list;
            if (model.PrizeList)
            {
                list = _pp.ViewExternalDealGetListByPrizeDeal(searchTitle).ToList();
            }
            else
            {
                list = _pp.ViewExternalDealGetListBySellerGuid(sellerGuid, shop, searchTitle)
                    .Where(x => ((category > -1 && x.CategoryList != null && x.CategoryList.Contains(category.Value.ToString()))
                                 || category == null)
                                && ((tag > -1 && x.TagList != null && x.TagList.Contains(tag.Value.ToString()))
                                    || tag == null)).ToList();

                //補全館的資料
                list = list.Concat(_pp.ViewExternalDealGetListBySellerGuid(_config.SkmRootSellerGuid, shop, searchTitle)
                    .Where(x => ((category > -1 && x.CategoryList != null && x.CategoryList.Contains(category.Value.ToString()))
                                 || category == null)
                                && ((tag > -1 && x.TagList != null && x.TagList.Contains(tag.Value.ToString()))
                                    || tag == null))).ToList();
            }

            //取得現行與最新版本
            var newCurrentDealVersion = new List<ExternalDealVersion>();
            foreach (ExternalDealVersion version in Enum.GetValues(typeof(ExternalDealVersion)))
            {
                if ((int)version >= _config.SkmDealCurrentVerion)
                {
                    newCurrentDealVersion.Add(version);
                }
            }

            ViewExternalDealCollection dealList = new ViewExternalDealCollection();
            foreach (ViewExternalDeal deal in list)
            {
                if (!model.PrizeList && deal.IsPrizeDeal)
                {
                    continue;
                }

                //判斷新光帳號的檔次版本實際列表與頁數
                bool setHideDeal = ViewBag.SkmAccount && !_config.EnableSkmPay
                    ? deal.DealVersion != _config.SkmDealCurrentVerion
                    : !newCurrentDealVersion.Contains((ExternalDealVersion)deal.DealVersion);

                if (!dealList.Any(x => x.Guid == deal.Guid) && deal.CreateTime > _config.SkmOldDeal && !setHideDeal)
                {
                    dealList.Add(deal);
                }
            }

            int pageCount = 10;
            int lastPage = dealList.Count > 0 ? dealList.Count / pageCount + (dealList.Count % pageCount > 0 ? 1 : 0) : 1;
            page = page ?? 1;
            page = page <= lastPage ? page : 1;
            int skip = page > 1 ? (page.Value - 1) * pageCount : 0;
            filter = filter ?? string.Empty;
            int flag = 0;
            bool isOrderbyDesc = int.TryParse(filter, out flag) && flag > 0 ? Helper.IsFlagSet(flag, PageFilterField.Desc) : true;
            filter = flag > 0 ? Helper.GetEnumDescription(isOrderbyDesc ? (PageFilterField)flag - 1 : (PageFilterField)flag) : string.Empty;
            filter = ViewExternalDeal.Schema.Columns.Any(x => x.PropertyName == filter) ? filter : ViewExternalDeal.Schema.GetColumn(ViewExternalDeal.Columns.ModifyTime).PropertyName;
            var prop = typeof(ViewExternalDeal).GetProperty(filter);
            model.List = isOrderbyDesc
                ? dealList.OrderByDescending(x => prop.GetValue(x, null)).Skip(skip).Take(pageCount)
                : dealList.OrderBy(x => prop.GetValue(x, null)).Skip(skip).Take(pageCount);

            model.Store = seller;
            model.Shop = shop ?? 0;
            model.Page = page.Value;
            model.LastPage = lastPage;
            model.DealCount = dealList.Count;
            model.Category = category ?? -1;
            model.Tag = tag ?? -1;
            model.Filter = flag;
            model.IsSkmUser = User.IsInRole("SkmProducter");
            model.SkmHqCategories = SkmHqCategoriesConvertToList();
            model.SkmHqPower = SkmFacade.CheckSkmHqPower(userId);
            model.SearchTitle = searchTitle;

            ViewBag.SKMShops = shops;
            ViewBag.SKMCategory = categories.Where(x => x.Key != _config.SkmDealDisableCategory.ToString()).ToDictionary(x => x.Key, x => x.Value); //刪去最新活動
            ViewBag.SKMTags = tags;
            ViewBag.SiteUrl = _config.SiteUrl;

            return View(model);
        }

        #region 建檔 Coupon Verion

        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMDealInfo(string guid, string seller)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault();
            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.NewGuid() : new Guid(guid);
            var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();
            var isDel = relationStore.Select(x => x.IsDel).FirstOrDefault();
            var stores = new List<string>();
            foreach (var item in relationStore)
            {
                var isShoppe = _skm.SkmShoppeGet(item.StoreGuid).IsShoppe;
                //判斷是否全館
                stores.AddRange(isShoppe ? relationStore.Select(x => x.ParentSellerGuid.ToString()) : relationStore.Select(x => x.SellerGuid.ToString()));
            }

            List<string> shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();

            SkmDealModel deal = new SkmDealModel();
            bool isNew = !_pp.ExternalDealGet(dealGuid).IsLoaded;
            if (!isNew)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                ExternalDeal ed = _pp.ExternalDealGet(dealGuid);
                //買幾送幾
                if (ed.ActiveType == (int)ActiveType.BuyGetFree)
                {
                    ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                    if (relationItemList.Any())
                    {
                        deal.ExternalDealRelationItemList = relationItemList;
                    }
                    deal.Buy = ed.Buy;
                    deal.Free = ed.Free;
                }

                deal.Guid = ed.Guid.ToString();
                deal.ProductCode = ed.ProductCode;
                deal.SpecialItemNo = ed.SpecialItemNo;
                deal.ItemNo = ed.ItemNo;
                deal.Title = ed.Title;
                deal.ActiveType = (ActiveType)ed.ActiveType;
                deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
                deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
                deal.DiscountType = ed.DiscountType;
                deal.Discount = ed.Discount;
                deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
                deal.Remark = HttpUtility.UrlEncode(ed.Remark);
                deal.Description = HttpUtility.UrlEncode(ed.Description);
                deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
                deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
                deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
                deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BeaconType = ed.BeaconType;
                deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
                deal.Status = ed.Status;
                deal.IsEqualTime = ed.BusinessHourDeliverTimeS == ed.BusinessHourOrderTimeS && ed.BusinessHourDeliverTimeE == ed.BusinessHourOrderTimeE;

                //顯示主圖及列表方圖
                deal.ImagePath = ed.ImagePath;
                deal.AppDealPic = ed.AppDealPic;
            }
            else
            {
                if (!User.IsInRole("SkmProducter"))
                    return RedirectToAction("SKMList");
                deal.Status = -1; //New
                deal.Category = new int[] { };
                deal.Tags = new int[] { (int)SkmDealTypes.SkmPromotional };
                deal.ActiveType = ActiveType.Product; // default
                deal.BeaconType = (int)BeaconType.Default;
                deal.Guid = dealGuid.ToString();
                deal.SourceSeller = seller;
            }

            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.SkmDealDisableCategory = _config.SkmDealDisableCategory;
            deal.UserId = userId;
            deal.Stores = stores.ToArray();
            deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.WaitingConfirm || isDel || Convert.ToDateTime(deal.BusinessHourOrderDateE + " " + deal.BusinessHourOrderTimeE) < DateTime.Now;
            deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;
            deal.Shoppes = shoppes.ToArray();
            deal.BuyGetFreeIsShow = _config.SkmBuyGetFreeIsShow;
            deal.NoInvoicesIsShow = _config.SkmNoInvoicesIsShow;

            foreach (var item in (List<Shoppes>)ViewBag.SKMShoppes)
            {
                if (stores.Contains(item.ParentSellerGuid.ToString()) || stores.Contains(item.SellerGuid))
                {
                    item.IsShow = true;
                }
            }

            //新版-最新活動與商品建檔分開
            Dictionary<int, string> allCategory = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealCategory)
                .OrderBy(x => x.Seq).ToDictionary(x => x.CategoryId, x => x.CategoryName);
            ViewBag.SKMCategory = allCategory.Where(x => x.Key != _config.SkmDealDisableCategory).ToDictionary(x => x.Key, x => x.Value); //刪去最新活動

            return View(deal);
        }

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SkmDealPreview(string guid)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault();
            SkmDealModel deal = new SkmDealModel();

            if (TempData["entity"] != null)
            {
                deal = (SkmDealModel)TempData["entity"];
                ExternalDeal existDeal = _pp.ExternalDealGet(new Guid(deal.Guid));
                deal.Status = existDeal.IsLoaded ? existDeal.Status : (int)SKMDealStatus.Draft;
                deal.SkmHqCategories = SkmHqCategoriesConvertToList();
                deal.BeaconMessage = existDeal.IsLoaded ? existDeal.BeaconMessage : deal.BeaconMessage;
                deal.UserId = userId;
                return View(deal);
            }

            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.Empty : new Guid(guid);
            var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();

            List<string> stores = relationStore.GroupBy(x => x.ParentSellerGuid).Select(x => x.Key.ToString()).ToList();
            var isAllShoppe = relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid);
            if (isAllShoppe.Any())
            {
                //有全館
                stores.AddRange(relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid)
                                        .GroupBy(x => x.SellerGuid)
                                        .Select(x => x.Key.ToString())
                                        .ToList());
            }
            //分店群組
            stores = stores.GroupBy(x => x).Select(x => x.Key.ToString()).ToList();

            List<string> shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            ExternalDeal ed = _pp.ExternalDealGet(dealGuid);

            if (!ed.IsLoaded)
                return RedirectToAction("SKMList");

            deal.Guid = ed.Guid.ToString();
            deal.Title = ed.Title;
            deal.ActiveType = (ActiveType)ed.ActiveType;
            deal.ProductCode = ed.ProductCode;
            deal.SpecialItemNo = ed.SpecialItemNo;
            deal.ItemNo = ed.ItemNo;
            deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
            deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
            deal.DiscountType = ed.DiscountType;
            deal.Discount = ed.Discount;
            deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
            deal.Remark = ed.Remark;
            deal.Description = ed.Description;/*HttpUtility.UrlEncode(ed.Description.Replace("<br />", "\n"));*/
            deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
            deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
            deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
            deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("tt hh:mm") : "";
            deal.BeaconType = ed.BeaconType;
            deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
            deal.Status = ed.Status;
            deal.Stores = stores.ToArray();
            deal.Shoppes = shoppes.ToArray();
            deal.AppDealPic = ed.AppDealPic;
            deal.ImagePath = ed.ImagePath;
            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.UserId = userId;

            if (deal.ActiveType == ActiveType.BuyGetFree)
            {
                ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                if (relationItemList.Any())
                {
                    deal.ExternalDealRelationItemList = relationItemList;
                }
                deal.Buy = ed.Buy;
                deal.Free = ed.Free;
            }

            deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.Draft;

            return View(deal);
        }

        #endregion 建檔 Coupon Verion

        #region 建檔 Burning Verion (燒點&抽獎)

        /// <summary>
        /// 建檔(目前只給抽獎用 新增&編輯)
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="seller"></param>
        /// <param name="dealType"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMBurningDealInfo(string guid, string seller, string dealType)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault(true);
            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.NewGuid() : new Guid(guid);
            var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();
            var isDel = relationStore.Select(x => x.IsDel).FirstOrDefault();

            var stores = new List<string>();
            SkmShoppeCollection allShoppe = _sp.SkmShoppeCollectionGetAll(false);
            foreach (var item in relationStore)
            {
                var isShoppe = allShoppe.Where(p => p.StoreGuid == item.StoreGuid).FirstOrDefault().IsShoppe;
                //判斷是否全館
                string storeGuid = isShoppe ? item.ParentSellerGuid.ToString() : item.SellerGuid.ToString();
                if (!stores.Contains(storeGuid))
                {
                    stores.Add(storeGuid);
                }
            }
            List<string> shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();


            SkmDealModel deal = new SkmDealModel();
            bool isNew = !_pp.ExternalDealGet(dealGuid).IsLoaded;
            if (!isNew)
            {
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                ExternalDeal ed = _pp.ExternalDealGet(dealGuid);
                //買幾送幾
                if (ed.ActiveType == (int)ActiveType.BuyGetFree)
                {
                    ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                    if (relationItemList.Any())
                    {
                        deal.ExternalDealRelationItemList = relationItemList;
                    }
                    deal.Buy = ed.Buy;
                    deal.Free = ed.Free;
                }

                deal.Guid = ed.Guid.ToString();
                deal.ProductCode = ed.ProductCode;
                deal.SpecialItemNo = ed.SpecialItemNo;
                deal.ItemNo = ed.ItemNo;
                deal.Title = ed.Title;
                deal.ActiveType = (ActiveType)ed.ActiveType;
                deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
                deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
                deal.DiscountType = ed.DiscountType;
                deal.Discount = ed.Discount;
                deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
                deal.Remark = HttpUtility.UrlEncode(ed.Remark);
                deal.Description = HttpUtility.UrlEncode(ed.Description);
                deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
                deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
                deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
                deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BeaconType = ed.BeaconType;
                deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
                deal.Status = ed.Status;
                deal.IsEqualTime = ed.BusinessHourDeliverTimeS == ed.BusinessHourOrderTimeS && ed.BusinessHourDeliverTimeE == ed.BusinessHourOrderTimeE;
                deal.IsPrize = ed.IsPrizeDeal;
                deal.IsHqDeal = ed.IsHqDeal;
                deal.IsCrmDeal = ed.IsCrmDeal;
                if (deal.IsCrmDeal)
                {
                    if (_ep.GetExhibitionDeal(ed.Guid).Any())
                    {
                        deal.AllowEditCrm = false;
                    }
                }

                //顯示主圖及列表方圖
                deal.ImagePath = ed.ImagePath;
                deal.AppDealPic = ed.AppDealPic;
                deal.DealIconId = ed.DealIconId;
            }
            else
            {
                var isPrize = !string.IsNullOrEmpty(dealType) && dealType.ToLower() == "prizedeals";
                if (!User.IsInRole("SkmProducter"))
                {
                    if (isPrize)
                    {
                        return RedirectToRoute("SKMPrizeList", new { dealType = "PrizeDeals" });
                    }
                    else
                    {
                        return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
                    }
                }

                deal.Status = -1; //New
                deal.Category = new int[] { };
                deal.Tags = new int[] { (int)SkmDealTypes.SkmPromotional };
                deal.ActiveType = ActiveType.Product; // default
                deal.BeaconType = (int)BeaconType.Default;
                deal.Guid = dealGuid.ToString();
                deal.SourceSeller = seller;
                deal.IsPrize = isPrize;
                //deal.IsHqDeal = deal.IsPrize; //獎項Deal必為總公司Deal
            }

            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.SkmDealDisableCategory = _config.SkmDealDisableCategory;
            deal.UserId = userId;
            deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.WaitingConfirm || isDel || Convert.ToDateTime(deal.BusinessHourOrderDateE + " " + deal.BusinessHourOrderTimeE) < DateTime.Now;
            deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;

            #region 客製抽獎為總公司抽獎檔次

            if (deal.IsHqDeal && deal.IsPrize)
            {
                //所有分店
                var allStores = _sp.SellerGetSkmParentList();
                var sellerGuids = allStores.Select(p => p.Guid).ToList();

                //所有贈品櫃位
                var allExchangeShops = allShoppe.Where(p => p.IsShoppe && p.IsExchange && sellerGuids.Contains(p.SellerGuid.Value));
                stores = new List<string>() { _config.SkmRootSellerGuid.ToString() };
                shoppes = new List<string>() { _config.SkmRootSellerGuid.ToString() };
            }

            deal.Stores = stores.ToArray();
            deal.Shoppes = shoppes.ToArray();

            #endregion

            deal.BuyGetFreeIsShow = _config.SkmBuyGetFreeIsShow;
            deal.NoInvoicesIsShow = _config.SkmNoInvoicesIsShow;

            //3.7版燒點新增
            var skmBurningEvent = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), string.Empty, false);
            deal.IsBurningEvent = skmBurningEvent.IsLoaded;
            deal.ActivityStatus = deal.IsBurningEvent && skmBurningEvent.CloseProjectStatus;
            deal.BurningPoint = deal.IsBurningEvent ? skmBurningEvent.BurningPoint : 0;
            deal.ExchangeNo = deal.IsBurningEvent ? skmBurningEvent.ExchangeItemId : string.Empty;
            deal.GiftPoint = deal.IsBurningEvent ? skmBurningEvent.GiftPoint : 0;
            deal.GiftWalletId = deal.IsBurningEvent ? skmBurningEvent.GiftWalletId : string.Empty;
            var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEvent.Id).ToList();
            var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
            var costCenters = new List<CostCenter>();
            foreach (var smBurningCostCenter in smBurningCostCenters)
            {
                var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                var costCenter = new CostCenter();
                costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                costCenter.MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.OrderNo = smBurningCostCenter.OrderCode;
                int sc;
                if (int.TryParse(smBurningCostCenter.StoreCode, out sc))
                {
                    costCenter.MainStoreCode = Convert.ToString(sc / 10 * 10);
                }
                costCenters.Add(costCenter);
            }

            deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();

            if (deal.IsPrize)
            {
                deal.IsBurningEvent = true;
            }

            foreach (var item in (List<Shoppes>)ViewBag.SKMShoppes)
            {
                if (stores.Contains(item.ParentSellerGuid.ToString()) || stores.Contains(item.SellerGuid))
                {
                    item.IsShow = true;
                }

                //檢查是否為贈品櫃
                if (shoppes.Contains(item.StoreGuid) && item.IsExchange)
                {
                    deal.IsExchange = true;
                }

                int sc;
                if (int.TryParse(item.ShopCode, out sc))
                {
                    item.MainShopCode = Convert.ToString(sc / 10 * 10);
                }
            }

            if (deal.IsHqDeal && deal.IsPrize)//為指定櫃位優惠
            {
                deal.IsExchange = false;
            }



            //新版-最新活動與商品建檔分開
            ViewBag.SkmDefaultCategoryId = SkmFacade.SkmDefaultCategoryId; //取得精選優惠ID
            Dictionary<int, string> allCategory = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                .Where(x => x.CategoryStatus == 1 && x.CategoryType == (int)CategoryType.DealCategory)
                .OrderBy(x => x.Seq).ToDictionary(x => x.CategoryId, x => x.CategoryName);
            var category = allCategory.Where(x => x.Key == SkmFacade.SkmDefaultCategoryId).ToDictionary(x => x.Key, x => x.Value); //只留下精選商品
            ViewBag.SKMCategory = category;

            //判斷是否總公司
            bool isSkmHQOwner = false;
            //判斷是否總公司可編輯的類別
            ViewBag.EditPower = SkmFacade.CheckSkmHqCategoriesPower(userId, category.FirstOrDefault().Key.ToString(), ref isSkmHQOwner);
            ViewBag.SkmHqPower = isSkmHQOwner;
            ViewBag.DealIconList = SkmFacade.GetDealIconModelList();
            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(dealGuid).Select(x => new DealExhibitionCode(x)).ToList());
            ViewBag.ExhibitionCodes = dealExhibitionCodes;

            if (!isSkmHQOwner)
            {
                return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
            }

            return View(deal);
        }

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SkmBurningDealPreview(string guid)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault();
            SkmDealModel deal = new SkmDealModel();

            if (TempData["entity"] != null)
            {
                deal = (SkmDealModel)TempData["entity"];
                ExternalDeal existDeal = _pp.ExternalDealGet(new Guid(deal.Guid));
                deal.Status = existDeal.IsLoaded ? existDeal.Status : (int)SKMDealStatus.Draft;
                deal.SkmHqCategories = SkmHqCategoriesConvertToList();
                deal.BeaconMessage = existDeal.IsLoaded ? existDeal.BeaconMessage : deal.BeaconMessage;
                deal.UserId = userId;
                deal.IsPrize = existDeal.IsPrizeDeal;
                //3.7版燒點新增
                var tempBe = _skm.SkmBurningEventGet(existDeal.Guid, string.Empty);
                if (tempBe.IsLoaded)
                {
                    deal.BurningEventId = tempBe.SkmEventId;
                    deal.BurningPoint = tempBe.BurningPoint;
                    deal.GiftPoint = tempBe.GiftPoint;
                    deal.GiftWalletId = tempBe.GiftWalletId;
                }

                List<DealExhibitionCode> tempExhibitionCodes = new List<DealExhibitionCode>();
                tempExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(existDeal.Guid)
                    .Select(x => new DealExhibitionCode(x)).ToList());
                deal.ExhibitionData = new JsonSerializer().Serialize(tempExhibitionCodes);
                List<CostCenter> costCenterTemp = new JsonSerializer().Deserialize<List<CostCenter>>(deal.CostCenterTemp);
                deal.CostCenter = deal.IsBurningEvent ? costCenterTemp : new List<CostCenter>();
                return View(deal);
            }

            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.Empty : new Guid(guid);
            var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();

            List<string> stores = relationStore.GroupBy(x => x.ParentSellerGuid).Select(x => x.Key.ToString()).ToList();
            var isAllShoppe = relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid);
            if (isAllShoppe.Any())
            {
                //有全館
                stores.AddRange(relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid)
                                        .GroupBy(x => x.SellerGuid)
                                        .Select(x => x.Key.ToString())
                                        .ToList());
            }
            //分店群組
            stores = stores.GroupBy(x => x).Select(x => x.Key.ToString()).ToList();

            List<string> shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            ExternalDeal ed = _pp.ExternalDealGet(dealGuid);

            if (!ed.IsLoaded)
            {
                if (ed.IsPrizeDeal)
                {
                    return RedirectToRoute("SKMPrizeList", new { dealType = "PrizeDeals" });
                }
                else
                {
                    return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
                }
            }

            deal.Guid = ed.Guid.ToString();
            deal.Title = ed.Title;
            deal.ActiveType = (ActiveType)ed.ActiveType;
            deal.ProductCode = ed.ProductCode;
            deal.SpecialItemNo = ed.SpecialItemNo;
            deal.ItemNo = ed.ItemNo;
            deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
            deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
            deal.DiscountType = ed.DiscountType;
            deal.Discount = ed.Discount;
            deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
            deal.Remark = ed.Remark;
            deal.Description = ed.Description;/*HttpUtility.UrlEncode(ed.Description.Replace("<br />", "\n"));*/
            deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
            deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
            deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
            deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("tt hh:mm") : null;
            deal.BeaconType = ed.BeaconType;
            deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
            deal.Status = ed.Status;
            deal.Stores = stores.ToArray();
            deal.Shoppes = shoppes.ToArray();
            deal.AppDealPic = ed.AppDealPic;
            deal.ImagePath = ed.ImagePath;
            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.UserId = userId;

            //3.7版燒點新增
            var skmBurningEvent = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), string.Empty, false);
            if (skmBurningEvent.IsLoaded)
            {
                deal.IsBurningEvent = skmBurningEvent.IsLoaded;
                deal.ActivityStatus = skmBurningEvent.CloseProjectStatus;
                deal.BurningPoint = skmBurningEvent.BurningPoint;
                deal.ExchangeNo = skmBurningEvent.ExchangeItemId;
                deal.BurningEventId = skmBurningEvent.SkmEventId;
                deal.GiftWalletId = skmBurningEvent.GiftWalletId;
                deal.GiftPoint = skmBurningEvent.GiftPoint;
            }

            var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEvent.Id).ToList();
            var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
            var costCenters = new List<CostCenter>();
            foreach (var smBurningCostCenter in smBurningCostCenters)
            {
                var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                var costCenter = new CostCenter();
                costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                costCenter.MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.OrderNo = smBurningCostCenter.OrderCode;
                costCenters.Add(costCenter);
            }
            deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();


            if (deal.ActiveType == ActiveType.BuyGetFree)
            {
                ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                if (relationItemList.Any())
                {
                    deal.ExternalDealRelationItemList = relationItemList;
                }
                deal.Buy = ed.Buy;
                deal.Free = ed.Free;
            }
            deal.IsPrize = ed.IsPrizeDeal;
            deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.Draft;

            #region Load 策展代號

            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(dealGuid)
                .Select(x => new DealExhibitionCode(x)).ToList());
            deal.ExhibitionData = new JsonSerializer().Serialize(dealExhibitionCodes);

            #endregion Load 策展代號

            return View(deal);
        }

        #endregion 建檔 Burning Verion

        #region 建檔 SkmPay Verion

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMPayDealInfo(string guid, string seller, string dealType)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.NewGuid() : new Guid(guid);
            SkmPayDealInfoViewModel deal = new SkmPayDealInfoViewModel();
            bool isDel = false;
            var stores = new List<string>();
            var shoppes = new List<string>();

            bool isNew = string.IsNullOrEmpty(guid);
            if (isNew)//add
            {
                var isPrize = !string.IsNullOrEmpty(dealType) && dealType.ToLower() == "prizedeals";
                if (!User.IsInRole("SkmProducter"))//沒權限則導向列表
                {
                    if (isPrize)
                    {
                        return RedirectToRoute("SKMPrizeList", new { dealType = "PrizeDeals" });
                    }
                    else
                    {
                        return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
                    }
                }
                deal.Guid = dealGuid.ToString();
                deal.Status = -1; //New
                deal.Category = new int[] { };
                deal.Tags = new int[] { (int)SkmDealTypes.SkmPromotional };
                deal.ActiveType = ActiveType.Product; // default
                deal.BeaconType = (int)BeaconType.Default;
                deal.SourceSeller = seller;
                deal.IsPrize = isPrize;
                deal.IsHqDeal = deal.IsPrize; //獎項Deal必為總公司Deal
                deal.IsCostCenterHq = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name));
            }
            else //edit
            {

                #region 撈現有館櫃資訊

                var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();
                isDel = relationStore.Select(x => x.IsDel).FirstOrDefault();

                foreach (var item in relationStore)
                {
                    var isShoppe = _skm.SkmShoppeGet(item.StoreGuid, true).IsShoppe;
                    //判斷是否全館
                    stores.AddRange(isShoppe ? relationStore.Select(x => x.ParentSellerGuid.ToString()) : relationStore.Select(x => x.SellerGuid.ToString()));
                }
                shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();

                #endregion

                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                ExternalDeal ed = _pp.ExternalDealGet(dealGuid);
                //買幾送幾
                if (ed.ActiveType == (int)ActiveType.BuyGetFree)
                {
                    ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                    if (relationItemList.Any())
                    {
                        deal.ExternalDealRelationItemList = relationItemList;
                    }
                    deal.Buy = ed.Buy;
                    deal.Free = ed.Free;
                }

                deal.Guid = ed.Guid.ToString();
                deal.ProductCode = ed.ProductCode;
                deal.SpecialItemNo = ed.SpecialItemNo;
                deal.ItemNo = ed.ItemNo;
                deal.Title = ed.Title;
                deal.ActiveType = (ActiveType)ed.ActiveType;
                deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
                deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
                deal.DiscountType = ed.DiscountType;
                deal.Discount = ed.Discount;
                deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
                deal.Remark = HttpUtility.UrlEncode(ed.Remark);
                deal.Description = HttpUtility.UrlEncode(ed.Description);
                deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
                deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
                deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
                deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BeaconType = ed.BeaconType;
                deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
                deal.Status = ed.Status;
                deal.IsEqualTime = ed.BusinessHourDeliverTimeS == ed.BusinessHourOrderTimeS && ed.BusinessHourDeliverTimeE == ed.BusinessHourOrderTimeE;
                deal.IsPrize = ed.IsPrizeDeal;
                deal.IsHqDeal = ed.IsHqDeal;
                deal.IsCrmDeal = ed.IsCrmDeal;
                deal.TaxRate = ed.TaxRate;
                deal.IsRepeatPurchase = ed.IsRepeatPurchase;
                deal.RepeatPurchaseCount = ed.RepeatPurchaseCount;
                deal.RepeatPurchaseType = (SkmDealRepeatPurchaseType)Enum.Parse(typeof(SkmDealRepeatPurchaseType), ed.RepeatPurchaseType.ToString());
                deal.IsSkmPay = ed.IsSkmPay;
                deal.ExcludingTaxPrice = ed.ExcludingTaxPrice;
                deal.BurningPoint = ed.SkmPayBurningPoint;
                deal.ExchangeType = (SkmDealExchangeType)Enum.Parse(typeof(SkmDealExchangeType), ed.ExchangeType.ToString());
                deal.StoreDisplayNames = SkmFacade.GetViewExternalDealStoreDisplayNameBydealGuid(dealGuid);
                deal.EnableInstall = ed.EnableInstall;
                if (deal.IsCrmDeal)
                {
                    if (_ep.GetExhibitionDeal(ed.Guid).Any())
                    {
                        deal.AllowEditCrm = false;
                    }
                }

                //顯示主圖及列表方圖
                deal.ImagePath = ed.ImagePath;
                deal.AppDealPic = ed.AppDealPic;
                deal.DealIconId = ed.DealIconId;
                var combodeals = _skmef.GetExternalDealCombo(dealGuid).Where(x => !x.IsDel).ToList();
                deal.ComboDeal = new List<SkmComboDeal>(combodeals.Select(x => new SkmComboDeal(x))).OrderByDescending(x => x.Order).OrderBy(x => x.ShopCode).ToList();

                //燒點相關
                var skmBurningEventList = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), false);
                deal.IsBurningEvent = skmBurningEventList.Any();
                var defaultBurningEvent = skmBurningEventList.FirstOrDefault();//同一筆dealGuid下的BurningEvent設定值都一樣
                if (deal.IsBurningEvent)
                {
                    deal.IsCostCenterHq = defaultBurningEvent.IsCostCenterHq;
                    deal.ActivityStatus = defaultBurningEvent.CloseProjectStatus;
                    deal.ExchangeNo = defaultBurningEvent.ExchangeItemId;
                    deal.GiftPoint = defaultBurningEvent.GiftPoint;
                    deal.GiftWalletId = defaultBurningEvent.GiftWalletId;
                    deal.BurningPoint = defaultBurningEvent.BurningPoint;
                }
                var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEventList.Select(x => x.Id).ToList()).ToList();
                var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
                var costCenters = new List<CostCenter>();
                foreach (var smBurningCostCenter in smBurningCostCenters)
                {
                    var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                    var costCenter = new CostCenter();
                    costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                    costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                    costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                    costCenter.OrderNo = smBurningCostCenter.OrderCode;

                    int sc;
                    if (int.TryParse(smBurningCostCenter.StoreCode, out sc))
                    {
                        costCenter.MainStoreCode = Convert.ToString(sc / 10 * 10);
                    }

                    var marketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                        y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市"));
                    if (marketCostCenterCode != null)
                    {
                        costCenter.MarketCostCenterCode = marketCostCenterCode != null ? marketCostCenterCode.CostCenterCode : string.Empty;
                    }

                    var sellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                        y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市") == false);
                    if (sellCostCenterCode != null)
                    {
                        costCenter.SellCostCenterCode = sellCostCenterCode != null ? sellCostCenterCode.CostCenterCode : string.Empty;
                    }

                    costCenters.Add(costCenter);
                }
                deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();
            }

            if (isDel)
            {
                deal.IsAllowEdit = false;
            }
            else
            {
                deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.WaitingConfirm || Convert.ToDateTime(deal.BusinessHourOrderDateE + " " + deal.BusinessHourOrderTimeE) < DateTime.Now;
            }

            if (deal.IsPrize) deal.IsBurningEvent = true;

            deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;

            if (deal.IsAllowStoreEdit)
            {
                SetPageDefault();
            }
            else
            {
                SetPageDefault(false, true);
            }

            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.SkmDealDisableCategory = _config.SkmDealDisableCategory;
            deal.UserId = userId;
            deal.Stores = stores.Distinct().ToArray(); //Distinct
            deal.Shoppes = shoppes.ToArray();
            deal.BuyGetFreeIsShow = _config.SkmBuyGetFreeIsShow;
            deal.NoInvoicesIsShow = _config.SkmNoInvoicesIsShow;

            foreach (var item in (List<Shoppes>)ViewBag.SKMShoppes)
            {
                if (stores.Contains(item.ParentSellerGuid.ToString()) || stores.Contains(item.SellerGuid))
                {
                    item.IsShow = true;
                }

                //檢查是否為贈品櫃
                if (shoppes.Contains(item.StoreGuid) && item.IsExchange)
                {
                    deal.IsExchange = true;
                }

                int sc;
                if (int.TryParse(item.ShopCode, out sc))
                {
                    item.MainShopCode = Convert.ToString(sc / 10 * 10);
                }
            }

            #region 商品主分類

            //新版-最新活動與商品建檔分開
            ViewBag.SkmDefaultCategoryId = SkmFacade.SkmDefaultCategoryId; //取得精選優惠ID

            //所有分類都建構於_config.SkmCategordId(新光三越頻道 TYPE=4)底下
            var dependencyCol = _sp.CategoryDependencyByRegionGet(_config.SkmCategordId);
            if (dependencyCol.Any())
            {
                var mCategoryCol = SkmFacade.GetBelongSkmCategoryList();

                if (isNew)//新增模式不顯示下架
                {
                    mCategoryCol = mCategoryCol.Where(p => p.IsShowFrontEnd).ToList();
                }
                //        <main-category, sub-categorylist>
                Dictionary<Category, List<Category>> list = new Dictionary<Category, List<Category>>();
                foreach (var mc in mCategoryCol.OrderBy(p => p.Rank))
                {
                    var dp = _sp.CategoryDependencyByRegionGet(mc.Id);
                    List<Category> dpList = new List<Category>();
                    if (dp.Any())
                    {
                        List<string> dpFilters = new List<string>();
                        dpFilters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dp.Select(p => p.CategoryId).ToList())));
                        dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                        if (isNew)//新增模式不顯示下架
                        {
                            dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowFrontEnd, true));
                        }
                        dpList = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), dpFilters.ToArray()).ToList();
                    }
                    list.Add(mc, dpList);
                }
                ViewBag.SKMCategory = list;
            }

            #endregion

            //判斷是否總公司
            ViewBag.SkmHqPower = SkmFacade.CheckSkmHqPower((int)User.Identity.GetPropertyValue("Id"));
            ViewBag.DealIconList = SkmFacade.GetDealIconModelList();
            ViewBag.StoreDisplayNameList = SkmFacade.GetSkmStoreDisplayNameList();
            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(dealGuid).Select(x => new DealExhibitionCode(x)).ToList());
            ViewBag.ExhibitionCodes = dealExhibitionCodes;
            if (deal.IsPrize && !(bool)ViewBag.SkmHqPower)
                return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
            return View(deal);
        }

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMPayDealInfoV2(string guid, string seller, string dealType, long itemId = 2001021055)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            //取得skm openapi資料
            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();

            string returnTransData = sKMOpenAPIMain.GetItemInfo(itemId);

            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.NewGuid() : new Guid(guid);
            SkmPayDealInfoViewModel deal = new SkmPayDealInfoViewModel();
            bool isDel = false;
            var stores = new List<string>();
            var shoppes = new List<string>();

            bool isNew = string.IsNullOrEmpty(guid);
            if (isNew)//add
            {
                var isPrize = !string.IsNullOrEmpty(dealType) && dealType.ToLower() == "prizedeals";
                if (!User.IsInRole("SkmProducter"))//沒權限則導向列表
                {
                    if (isPrize)
                    {
                        return RedirectToRoute("SKMPrizeList", new { dealType = "PrizeDeals" });
                    }
                    else
                    {
                        return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
                    }
                }
                deal.Guid = dealGuid.ToString();
                deal.Status = -1; //New
                deal.Category = new int[] { };
                deal.Tags = new int[] { (int)SkmDealTypes.SkmPromotional };
                deal.ActiveType = ActiveType.Product; // default
                deal.BeaconType = (int)BeaconType.Default;
                deal.SourceSeller = seller;
                deal.IsPrize = isPrize;
                deal.IsHqDeal = deal.IsPrize; //獎項Deal必為總公司Deal
                deal.IsCostCenterHq = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name));
            }
            else //edit
            {

                #region 撈現有館櫃資訊

                var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();
                isDel = relationStore.Select(x => x.IsDel).FirstOrDefault();

                foreach (var item in relationStore)
                {
                    var isShoppe = _skm.SkmShoppeGet(item.StoreGuid, true).IsShoppe;
                    //判斷是否全館
                    stores.AddRange(isShoppe ? relationStore.Select(x => x.ParentSellerGuid.ToString()) : relationStore.Select(x => x.SellerGuid.ToString()));
                }
                shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();

                #endregion

                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                ExternalDeal ed = _pp.ExternalDealGet(dealGuid);
                //買幾送幾
                if (ed.ActiveType == (int)ActiveType.BuyGetFree)
                {
                    ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                    if (relationItemList.Any())
                    {
                        deal.ExternalDealRelationItemList = relationItemList;
                    }
                    deal.Buy = ed.Buy;
                    deal.Free = ed.Free;
                }

                deal.Guid = ed.Guid.ToString();
                deal.ProductCode = ed.ProductCode;
                deal.SpecialItemNo = ed.SpecialItemNo;
                deal.ItemNo = ed.ItemNo;
                deal.Title = ed.Title;
                deal.ActiveType = (ActiveType)ed.ActiveType;
                deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
                deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
                deal.DiscountType = ed.DiscountType;
                deal.Discount = ed.Discount;
                deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
                deal.Remark = HttpUtility.UrlEncode(ed.Remark);
                deal.Description = HttpUtility.UrlEncode(ed.Description);
                deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
                deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
                deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
                deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
                deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
                deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
                deal.BeaconType = ed.BeaconType;
                deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
                deal.Status = ed.Status;
                deal.IsEqualTime = ed.BusinessHourDeliverTimeS == ed.BusinessHourOrderTimeS && ed.BusinessHourDeliverTimeE == ed.BusinessHourOrderTimeE;
                deal.IsPrize = ed.IsPrizeDeal;
                deal.IsHqDeal = ed.IsHqDeal;
                deal.IsCrmDeal = ed.IsCrmDeal;
                deal.TaxRate = ed.TaxRate;
                deal.IsRepeatPurchase = ed.IsRepeatPurchase;
                deal.RepeatPurchaseCount = ed.RepeatPurchaseCount;
                deal.RepeatPurchaseType = (SkmDealRepeatPurchaseType)Enum.Parse(typeof(SkmDealRepeatPurchaseType), ed.RepeatPurchaseType.ToString());
                deal.IsSkmPay = ed.IsSkmPay;
                deal.ExcludingTaxPrice = ed.ExcludingTaxPrice;
                deal.BurningPoint = ed.SkmPayBurningPoint;
                deal.ExchangeType = (SkmDealExchangeType)Enum.Parse(typeof(SkmDealExchangeType), ed.ExchangeType.ToString());
                deal.StoreDisplayNames = SkmFacade.GetViewExternalDealStoreDisplayNameBydealGuid(dealGuid);
                deal.EnableInstall = ed.EnableInstall;
                if (deal.IsCrmDeal)
                {
                    if (_ep.GetExhibitionDeal(ed.Guid).Any())
                    {
                        deal.AllowEditCrm = false;
                    }
                }

                //顯示主圖及列表方圖
                deal.ImagePath = ed.ImagePath;
                deal.AppDealPic = ed.AppDealPic;
                deal.DealIconId = ed.DealIconId;
                var combodeals = _skmef.GetExternalDealCombo(dealGuid).Where(x => !x.IsDel).ToList();
                deal.ComboDeal = new List<SkmComboDeal>(combodeals.Select(x => new SkmComboDeal(x))).OrderByDescending(x => x.Order).OrderBy(x => x.ShopCode).ToList();

                //燒點相關
                var skmBurningEventList = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), false);
                deal.IsBurningEvent = skmBurningEventList.Any();
                var defaultBurningEvent = skmBurningEventList.FirstOrDefault();//同一筆dealGuid下的BurningEvent設定值都一樣
                if (deal.IsBurningEvent)
                {
                    deal.IsCostCenterHq = defaultBurningEvent.IsCostCenterHq;
                    deal.ActivityStatus = defaultBurningEvent.CloseProjectStatus;
                    deal.ExchangeNo = defaultBurningEvent.ExchangeItemId;
                    deal.GiftPoint = defaultBurningEvent.GiftPoint;
                    deal.GiftWalletId = defaultBurningEvent.GiftWalletId;
                    deal.BurningPoint = defaultBurningEvent.BurningPoint;
                }
                var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEventList.Select(x => x.Id).ToList()).ToList();
                var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
                var costCenters = new List<CostCenter>();
                foreach (var smBurningCostCenter in smBurningCostCenters)
                {
                    var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                    var costCenter = new CostCenter();
                    costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                    costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                    costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                    costCenter.OrderNo = smBurningCostCenter.OrderCode;

                    int sc;
                    if (int.TryParse(smBurningCostCenter.StoreCode, out sc))
                    {
                        costCenter.MainStoreCode = Convert.ToString(sc / 10 * 10);
                    }

                    var marketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                        y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市"));
                    if (marketCostCenterCode != null)
                    {
                        costCenter.MarketCostCenterCode = marketCostCenterCode != null ? marketCostCenterCode.CostCenterCode : string.Empty;
                    }

                    var sellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                        y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市") == false);
                    if (sellCostCenterCode != null)
                    {
                        costCenter.SellCostCenterCode = sellCostCenterCode != null ? sellCostCenterCode.CostCenterCode : string.Empty;
                    }

                    costCenters.Add(costCenter);
                }
                deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();
            }

            if (isDel)
            {
                deal.IsAllowEdit = false;
            }
            else
            {
                deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.WaitingConfirm || Convert.ToDateTime(deal.BusinessHourOrderDateE + " " + deal.BusinessHourOrderTimeE) < DateTime.Now;
            }

            if (deal.IsPrize) deal.IsBurningEvent = true;

            deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;

            if (deal.IsAllowStoreEdit)
            {
                SetPageDefault();
            }
            else
            {
                SetPageDefault(false, true);
            }

            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.SkmDealDisableCategory = _config.SkmDealDisableCategory;
            deal.UserId = userId;
            deal.Stores = stores.Distinct().ToArray(); //Distinct
            deal.Shoppes = shoppes.ToArray();
            deal.BuyGetFreeIsShow = _config.SkmBuyGetFreeIsShow;
            deal.NoInvoicesIsShow = _config.SkmNoInvoicesIsShow;

            foreach (var item in (List<Shoppes>)ViewBag.SKMShoppes)
            {
                if (stores.Contains(item.ParentSellerGuid.ToString()) || stores.Contains(item.SellerGuid))
                {
                    item.IsShow = true;
                }

                //檢查是否為贈品櫃
                if (shoppes.Contains(item.StoreGuid) && item.IsExchange)
                {
                    deal.IsExchange = true;
                }

                int sc;
                if (int.TryParse(item.ShopCode, out sc))
                {
                    item.MainShopCode = Convert.ToString(sc / 10 * 10);
                }
            }

            #region 商品主分類

            //新版-最新活動與商品建檔分開
            ViewBag.SkmDefaultCategoryId = SkmFacade.SkmDefaultCategoryId; //取得精選優惠ID

            //所有分類都建構於_config.SkmCategordId(新光三越頻道 TYPE=4)底下
            var dependencyCol = _sp.CategoryDependencyByRegionGet(_config.SkmCategordId);
            if (dependencyCol.Any())
            {
                var mCategoryCol = SkmFacade.GetBelongSkmCategoryList();

                if (isNew)//新增模式不顯示下架
                {
                    mCategoryCol = mCategoryCol.Where(p => p.IsShowFrontEnd).ToList();
                }
                //        <main-category, sub-categorylist>
                Dictionary<Category, List<Category>> list = new Dictionary<Category, List<Category>>();
                foreach (var mc in mCategoryCol.OrderBy(p => p.Rank))
                {
                    var dp = _sp.CategoryDependencyByRegionGet(mc.Id);
                    List<Category> dpList = new List<Category>();
                    if (dp.Any())
                    {
                        List<string> dpFilters = new List<string>();
                        dpFilters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dp.Select(p => p.CategoryId).ToList())));
                        dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                        if (isNew)//新增模式不顯示下架
                        {
                            dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowFrontEnd, true));
                        }
                        dpList = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), dpFilters.ToArray()).ToList();
                    }
                    list.Add(mc, dpList);
                }
                ViewBag.SKMCategory = list;
            }

            #endregion

            //判斷是否總公司
            ViewBag.SkmHqPower = SkmFacade.CheckSkmHqPower((int)User.Identity.GetPropertyValue("Id"));
            ViewBag.DealIconList = SkmFacade.GetDealIconModelList();
            ViewBag.StoreDisplayNameList = SkmFacade.GetSkmStoreDisplayNameList();
            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(dealGuid).Select(x => new DealExhibitionCode(x)).ToList());
            ViewBag.ExhibitionCodes = dealExhibitionCodes;
            if (deal.IsPrize && !(bool)ViewBag.SkmHqPower)
                return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));

            ViewBag.TransData = returnTransData;
            return View(deal);
        }


        /// <summary>
        /// 阿里巴巴商品頁
        /// </summary>
        /// <param name="itemCode">商品條碼</param> 
        /// <returns></returns>
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMPayDealInfoForAlibaba(long? itemId)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            SkmPayDealInfoViewModel deal = new SkmPayDealInfoViewModel();
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            bool isDel = false;
            var stores = new List<string>();
            var shoppes = new List<string>();
            bool isNew = true;
            int errcount = 0;

            string returnTransData = "";
            AlibabaItemResult alibabaRtn=new AlibabaItemResult();
            ExternalDeal ed = new ExternalDeal();
            /////TODO 接MAX的FUN 
            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();
            if (itemId == null)
            {
                if (TempData["entity"] != null)
                {
                    deal = (SkmPayDealInfoViewModel)TempData["entity"];
                }
                else {
                    return View(deal);
                }
            }
            else {
                while (errcount < 3 && !alibabaRtn.success )
                {
                    try
                    {
                        returnTransData = sKMOpenAPIMain.GetItemInfo((long)itemId);
                        alibabaRtn = JsonConvert.DeserializeObject<AlibabaItemResult>(returnTransData);
                    }
                    catch (Exception)
                    {
                        errcount += 1;

                    }
                }
                try
                {
               
                    if (alibabaRtn.success)
                    {
                        ed = _pp.ExternalDealGet(alibabaRtn.result.itemInfo.skuIdList?[0].ToString());
                         if (ed == null || ed.Guid.Equals(Guid.Empty))
                        {
                            deal.Guid = Guid.NewGuid().ToString();
                            deal.Status = -1; //New
                            deal.Tags = new int[] { (int)SkmDealTypes.SkmPromotional };
                            deal.ActiveType = ActiveType.Product; // default
                            deal.BeaconType = (int)BeaconType.Default;
                            deal.IsHqDeal = deal.IsPrize; //獎項Deal必為總公司Deal
                            deal.IsCostCenterHq = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name));

                        }
                        else //edit
                        {

                            //買幾送幾
                            if (ed.ActiveType == (int)ActiveType.BuyGetFree)
                            {
                                ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(ed.Guid);
                                if (relationItemList.Any())
                                {
                                    deal.ExternalDealRelationItemList = relationItemList;
                                }
                                deal.Buy = ed.Buy;
                                deal.Free = ed.Free;
                            }

                            deal.Guid = ed.Guid.ToString();

                            deal.SpecialItemNo = ed.SpecialItemNo;


                            deal.ActiveType = (ActiveType)ed.ActiveType;

                            deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
                            deal.DiscountType = ed.DiscountType;
                            deal.Discount = ed.Discount;
                            deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
                            deal.Remark = HttpUtility.UrlEncode(ed.Remark);

                            deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));

                            deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };

                            deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
                            deal.BeaconType = ed.BeaconType;
                            deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
                            deal.Status = ed.Status;
                            deal.IsEqualTime = ed.BusinessHourDeliverTimeS == ed.BusinessHourOrderTimeS && ed.BusinessHourDeliverTimeE == ed.BusinessHourOrderTimeE;
                            deal.IsPrize = ed.IsPrizeDeal;
                            deal.IsHqDeal = ed.IsHqDeal;
                            deal.IsCrmDeal = ed.IsCrmDeal;
                            deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                            deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
                            deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
                            deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
                            deal.IsRepeatPurchase = ed.IsRepeatPurchase;
                            deal.RepeatPurchaseCount = ed.RepeatPurchaseCount;
                            deal.RepeatPurchaseType = (SkmDealRepeatPurchaseType)Enum.Parse(typeof(SkmDealRepeatPurchaseType), ed.RepeatPurchaseType.ToString());
                            deal.IsSkmPay = ed.IsSkmPay;
                            deal.ExcludingTaxPrice = ed.ExcludingTaxPrice;
                            deal.BurningPoint = ed.SkmPayBurningPoint;
                            deal.ExchangeType = (SkmDealExchangeType)Enum.Parse(typeof(SkmDealExchangeType), ed.ExchangeType.ToString());
                            deal.StoreDisplayNames = SkmFacade.GetViewExternalDealStoreDisplayNameBydealGuid(ed.Guid);
                            deal.EnableInstall = ed.EnableInstall;
                            if (deal.IsCrmDeal)
                            {
                                if (_ep.GetExhibitionDeal(ed.Guid).Any())
                                {
                                    deal.AllowEditCrm = false;
                                }
                            }

                            //顯示主圖及列表方圖
                            deal.ImagePath = ed.ImagePath;

                            deal.DealIconId = ed.DealIconId;
                            var combodeals = _skmef.GetExternalDealCombo(ed.Guid).Where(x => !x.IsDel).ToList();
                            deal.ComboDeal = new List<SkmComboDeal>(combodeals.Select(x => new SkmComboDeal(x))).OrderByDescending(x => x.Order).OrderBy(x => x.ShopCode).ToList();

                            //燒點相關
                            var skmBurningEventList = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), false);
                            deal.IsBurningEvent = skmBurningEventList.Any();
                            var defaultBurningEvent = skmBurningEventList.FirstOrDefault();//同一筆dealGuid下的BurningEvent設定值都一樣
                            if (deal.IsBurningEvent)
                            {
                                deal.IsCostCenterHq = defaultBurningEvent.IsCostCenterHq;
                                deal.ActivityStatus = defaultBurningEvent.CloseProjectStatus;
                                deal.ExchangeNo = defaultBurningEvent.ExchangeItemId;
                                deal.GiftPoint = defaultBurningEvent.GiftPoint;
                                deal.GiftWalletId = defaultBurningEvent.GiftWalletId;
                                deal.BurningPoint = defaultBurningEvent.BurningPoint;
                            }
                            var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEventList.Select(x => x.Id).ToList()).ToList();
                            var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
                            var costCenters = new List<CostCenter>();
                            foreach (var smBurningCostCenter in smBurningCostCenters)
                            {
                                var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                                var costCenter = new CostCenter();
                                costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                                costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                                costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                                costCenter.OrderNo = smBurningCostCenter.OrderCode;

                                int sc;
                                if (int.TryParse(smBurningCostCenter.StoreCode, out sc))
                                {
                                    costCenter.MainStoreCode = Convert.ToString(sc / 10 * 10);
                                }

                                var marketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                                    y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市"));
                                if (marketCostCenterCode != null)
                                {
                                    costCenter.MarketCostCenterCode = marketCostCenterCode != null ? marketCostCenterCode.CostCenterCode : string.Empty;
                                }

                                var sellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y =>
                                    y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市") == false);
                                if (sellCostCenterCode != null)
                                {
                                    costCenter.SellCostCenterCode = sellCostCenterCode != null ? sellCostCenterCode.CostCenterCode : string.Empty;
                                }

                                costCenters.Add(costCenter);
                            }
                            deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();

                            //商品已愈上架時間時且狀態不為新增時，改為編輯新增
                            if (ed.Status>0 && ed.BusinessHourDeliverTimeE < DateTime.Now)
                            {
                                deal.Guid = Guid.NewGuid().ToString();
                                deal.Status = -1; //New
                            }

                            //稅務資訊 ?? 
                            deal.TaxRate = ed.TaxRate;
                        }

                        ////
                        // deal.Guid = Guid.NewGuid().ToString();
                        // deal.Status = -1;
                        //  isDel = false;
                        //  deal.skuid = "123456789";
                        ////

                        #region 由阿里巴巴資料填入
                        // var ShoppeCodeList = alibabaRtn.result.itemInfo.extra.salesStores.Split(',').ToList() ?? new List<string>();
                        //var strShoppeCodeList= alibabaRtn.result.skuInfoList.Where(o => JsonConvert.DeserializeObject<List<SalesStores>>(o.extra.salesStores).Count > 0).FirstOrDefault().extra.salesStores;
                        var tmp = alibabaRtn.result.skuInfoList.Where(o => (string.IsNullOrEmpty(o.extra.salesStores)) ? false : JsonConvert.DeserializeObject<List<SalesStores>>(o.extra.salesStores).Count > 0).FirstOrDefault();
                        var strShoppeCodeList = (tmp==null)?"": tmp.extra.salesStores;
                        // var ShoppeCodeList = JsonConvert.DeserializeObject<List<SalesStores>>(strShoppeCodeList).Select(y=>y.storeId).ToList() ?? new List<string>();
                        var ShoppeCodeList =(strShoppeCodeList=="")? new List<string[]>(): JsonConvert.DeserializeObject<List<SalesStores>>(strShoppeCodeList).Select(y =>new string[] { y.storeId,y.counterId }).ToList() ?? new List<string[]>();
                        if (ShoppeCodeList.Count() > 0) {
                            SkmShoppeCollection allShoppe = _sp.SkmShoppeCollectionGetAll(false);
                            var ShoppesList = allShoppe.Where(o => ShoppeCodeList.Where(y=>y[0]==o.ShopCode && y[1]==o.BrandCounterCode).Any()).ToList();
                          
                            stores.AddRange(ShoppesList.Select(o => _skm.SkmShoppeGet(o.StoreGuid ?? Guid.Empty, true).IsShoppe ?
                                        o.ParentSellerGuid.ToString() : o.SellerGuid.ToString()).ToList().Distinct());
                            shoppes.AddRange(ShoppesList.Select(o => o.StoreGuid.ToString()).ToList());
                        }


                        //deal.Status = alibabaRtn.result.itemInfo.status; //為免引發不可知的錯誤，商品狀態由資料庫來
                        //適用分店 salesStores 
                        deal.Stores = stores.ToArray();
                        deal.Shoppes = shoppes.ToArray();

                        //商品分類 categoryList
                        deal.Category = alibabaRtn.result.itemInfo.categoryIds ?? new int[] { };
                        //商品條碼 barcode 
                        deal.ProductCode = alibabaRtn.result.skuInfoList?[0].barcode;
                        //商品貨號 skuCode
                        deal.ItemNo = alibabaRtn.result.skuInfoList?[0].skuCode;
                        //商品名稱 name
                        deal.Title = alibabaRtn.result.itemInfo.name;
                        //商品原價 originalPrice 
                        deal.ItemOrigPrice = Convert.ToInt32(alibabaRtn.result.skuInfoList?[0].originalPrice/100);//要除100
                        //商品圖 mainImage 
                       deal.AppDealPic = alibabaRtn.result.skuInfoList.Select(o => (!string.IsNullOrWhiteSpace(o.extra.imageList))?JsonConvert.DeserializeObject<List<string>>(o.extra.imageList):new List<string>())
                            .Where(o => o.Count > 0).FirstOrDefault()?[0];
                        //商品介紹 pcDetail
                        deal.Description = HttpUtility.UrlEncode(alibabaRtn.result.itemDetailInfo.pcDetail?[0].content);

                        //商品上架時間 offShelfTime
                        DateTime BusinessHourOrderDateStart = new DateTime(1970,1,1).AddMilliseconds(alibabaRtn.result.itemInfo.extra.onShelfTime).AddHours(8);
                        DateTime BusinessHourOrderDateEnd = new DateTime(1970, 1, 1).AddMilliseconds(alibabaRtn.result.itemInfo.extra.offShelfTime).AddHours(8);
                        deal.BusinessHourOrderDateS = BusinessHourOrderDateStart.ToString("ddd MMM dd yyyy", culture);
                        deal.BusinessHourOrderDateE = BusinessHourOrderDateEnd.ToString("ddd MMM dd yyyy", culture);
                        deal.BusinessHourOrderTimeS = BusinessHourOrderDateStart.ToString("h:mm tt", culture);
                        deal.BusinessHourOrderTimeE = BusinessHourOrderDateEnd.ToString("h:mm tt", culture);
                        deal.skuid = alibabaRtn.result.itemInfo.skuIdList?[0].ToString();
                        deal.spuid = alibabaRtn.result.itemInfo.spuId;
                        deal.itemid = alibabaRtn.result.itemDetailInfo.itemId;
                        /* deal.attrVal = alibabaRtn.result.itemInfo.otherAttributes.Select(o => o.otherAttributes.Where(y => y.attrKey == "應免稅").FirstOrDefault().attrVal).FirstOrDefault();
                         if (!new string[] { "應稅", "免稅" }.Contains(deal.attrVal)) {
                             throw new ApplicationException("attrVal 參數有誤");
                         }*/
                        switch (alibabaRtn.result.itemInfo.extra.isTaxable) {
                            case 1:
                                deal.attrVal = "應稅";
                                break;
                            case 0:
                                deal.attrVal = "免稅";
                                break;
                            default:
                                throw new ApplicationException("attrVal 參數有誤");
                                break;
                        }

                        #endregion

                    }
                    else
                    {
                        throw new ApplicationException(returnTransData);
                    }

                     deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.Draft;
                    deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;
                }
                catch (Exception e)
                {
                    logger.Error("get Alibaba ItemInfo Error  return message=>" + returnTransData +"  errmsg=>" + e.Message);
                    return View(deal);
                }

            }



            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.SkmDealDisableCategory = _config.SkmDealDisableCategory;
            deal.UserId = userId;
         
            deal.BuyGetFreeIsShow = _config.SkmBuyGetFreeIsShow;
            deal.NoInvoicesIsShow = _config.SkmNoInvoicesIsShow;

            #region 商品主分類

            //新版-最新活動與商品建檔分開
            ViewBag.SkmDefaultCategoryId = SkmFacade.SkmDefaultCategoryId; //取得精選優惠ID

            //所有分類都建構於_config.SkmCategordId(新光三越頻道 TYPE=4)底下
            var dependencyCol = _sp.CategoryDependencyByRegionGet(_config.SkmCategordId);
            if (dependencyCol.Any())
            {
                var mCategoryCol = SkmFacade.GetBelongSkmCategoryList();

                if (isNew)//新增模式不顯示下架
                {
                    mCategoryCol = mCategoryCol.Where(p => p.IsShowFrontEnd).ToList();
                    deal.IsAllowStoreEdit = deal.Status < (int)SKMDealStatus.WaitingConfirm;
                    SetPageDefault(false, false, true);

                }
                else
                {
                    SetPageDefault(false, true, true);
                }

                //        <main-category, sub-categorylist>
                Dictionary<Category, List<Category>> list = new Dictionary<Category, List<Category>>();
                foreach (var mc in mCategoryCol.OrderBy(p => p.Rank))
                {
                    var dp = _sp.CategoryDependencyByRegionGet(mc.Id);
                    List<Category> dpList = new List<Category>();
                    if (dp.Any())
                    {
                        List<string> dpFilters = new List<string>();
                        dpFilters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dp.Select(p => p.CategoryId).ToList())));
                        dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                        if (isNew)
                        {
                            dpFilters.Add(string.Format("{0} = {1}", Category.Columns.IsShowFrontEnd, true));
                        }
                        dpList = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), dpFilters.ToArray()).ToList();
                    }
                    list.Add(mc, dpList);
                }
                ViewBag.SKMCategory = list;
            }
            else
            {
                if (deal.IsAllowStoreEdit)
                {
                    SetPageDefault(false, false, true);
                }
                else
                {
                    SetPageDefault(false, true, true);
                }
            }

            foreach (var item in (List<Shoppes>)ViewBag.SKMShoppes)
            {
                if (stores.Contains(item.ParentSellerGuid.ToString()) || stores.Contains(item.SellerGuid))
                {
                    item.IsShow = true;
                }

                //檢查是否為贈品櫃
                if (shoppes.Contains(item.StoreGuid) && item.IsExchange)
                {
                    deal.IsExchange = true;
                }

                int sc;
                if (int.TryParse(item.ShopCode, out sc))
                {
                    item.MainShopCode = Convert.ToString(sc / 10 * 10);
                }
            }

            #endregion

            //判斷是否總公司
            ViewBag.SkmHqPower = SkmFacade.CheckSkmHqPower((int)User.Identity.GetPropertyValue("Id"));
            ViewBag.DealIconList = SkmFacade.GetDealIconModelList();
            ViewBag.StoreDisplayNameList = SkmFacade.GetSkmStoreDisplayNameList();
            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(Guid.Parse(deal.Guid)).Select(x => new DealExhibitionCode(x)).ToList());
            ViewBag.ExhibitionCodes = dealExhibitionCodes;

            return View(deal);
        }

        /// <summary>
        /// 阿里巴巴商品建檔頁
        /// </summary>
        /// <param name="page"></param>
        /// <param name="seller"></param>
        /// <param name="shop"></param>
        /// <param name="filter"></param>
        /// <param name="category"></param>
        /// <param name="tag"></param>
        /// <param name="dealType"></param>
        /// <param name="searchTitle"></param>
        /// <param name="itemId"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SKMListForAlibaba (int? page , string seller , int? shop , string filter , int? category , int? tag , string dealType , string searchTitle, long? itemId)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SkmDealListModel model = new SkmDealListModel();
            model.PrizeList = !string.IsNullOrEmpty(dealType) && dealType.ToLower() == "prizedeals" && SkmFacade.CheckSkmHqPower(userId);
            SetPageDefault();

            SKMOpenAPIMain sKMOpenAPIMain = new SKMOpenAPIMain();
            string returnTransData = sKMOpenAPIMain.GetItemInfo((long)itemId);
            var alibabaRtn = JsonConvert.DeserializeObject<AlibabaItemResult>(returnTransData);
            var skuid = alibabaRtn.result.itemInfo.skuIdList? [0].ToString();
            var deals = _pp.ViewExternalDealGetListBySkuId(skuid);


            List<ViewExternalDeal> list;
            if (model.PrizeList)
            {
                list = _pp.ViewExternalDealGetListByPrizeDeal(searchTitle).ToList();
            } else
            {
                list = deals.ToList();
            }

            //取得現行與最新版本
            var newCurrentDealVersion = new List<ExternalDealVersion>();
            foreach (ExternalDealVersion version in Enum.GetValues(typeof(ExternalDealVersion)))
            {
                if ((int)version >= _config.SkmDealCurrentVerion)
                {
                    newCurrentDealVersion.Add(version);
                }
            }

            ViewExternalDealCollection dealList = new ViewExternalDealCollection();
            foreach (ViewExternalDeal deal in list)
            {
                if (!model.PrizeList && deal.IsPrizeDeal)
                {
                    continue;
                }

                //判斷新光帳號的檔次版本實際列表與頁數
                bool setHideDeal = ViewBag.SkmAccount && !_config.EnableSkmPay
                    ? deal.DealVersion != _config.SkmDealCurrentVerion
                    : !newCurrentDealVersion.Contains((ExternalDealVersion)deal.DealVersion);

                if (!dealList.Any(x => x.Guid == deal.Guid) && deal.CreateTime > _config.SkmOldDeal && !setHideDeal)
                {
                    dealList.Add(deal);
                }
            }

            int pageCount = 10;
            int lastPage = dealList.Count > 0 ? dealList.Count / pageCount + (dealList.Count % pageCount > 0 ? 1 : 0) : 1;
            page = page ?? 1;
            page = page <= lastPage ? page : 1;
            int skip = page > 1 ? (page.Value - 1) * pageCount : 0;
            filter = filter ?? string.Empty;
            int flag = 0;
            bool isOrderbyDesc = int.TryParse(filter , out flag) && flag > 0 ? Helper.IsFlagSet(flag , PageFilterField.Desc) : true;
            filter = flag > 0 ? Helper.GetEnumDescription(isOrderbyDesc ? (PageFilterField)flag - 1 : (PageFilterField)flag) : string.Empty;
            filter = ViewExternalDeal.Schema.Columns.Any(x => x.PropertyName == filter) ? filter : ViewExternalDeal.Schema.GetColumn(ViewExternalDeal.Columns.ModifyTime).PropertyName;
            var prop = typeof(ViewExternalDeal).GetProperty(filter);

            model.List = isOrderbyDesc ? dealList.OrderByDescending(x => prop.GetValue(x , null)).Skip(skip).Take(pageCount)
                : dealList.OrderBy(x => prop.GetValue(x , null)).Skip(skip).Take(pageCount);

            model.Page = page.Value;
            model.LastPage = lastPage;
            model.DealCount = dealList.Count;
            model.Filter = flag;
            model.IsSkmUser = User.IsInRole("SkmProducter");
            model.SkmHqCategories = SkmHqCategoriesConvertToList();
            model.SkmHqPower = SkmFacade.CheckSkmHqPower(userId);
            model.SearchTitle = searchTitle;

            ViewBag.SiteUrl = _config.SiteUrl;

            return View(model);
        }

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public void AlibabaItemCreateComplete(long itemId)
        {
          
            SKMCenterRocketMQTrans.SKMItemCreateComplete(itemId);

          
        }

        /// <summary>
        /// 顯示主從關係的分類名字 組成->主分類1(子分類1、子分類2)、主分類2
        /// </summary>
        /// <returns></returns>
        private string GetShowCategoryInfo(Dictionary<int, string> allCategoryList, int[] haveCategorys)
        {
            List<string> tempMain = new List<string>();
            var defaultCid = SkmFacade.SkmDefaultCategoryId;
            foreach (var cid in haveCategorys.Where(p => p != defaultCid))
            {
                var dplist = _sp.CategoryDependencyByRegionGet(cid);
                if (dplist.Any()) //是為主分類, 可接著找其子分類
                {
                    List<string> tempSub = new List<string>();
                    var intersectArray = haveCategorys.Intersect(dplist.Select(p => p.CategoryId));
                    foreach (var c in intersectArray)
                    {
                        tempSub.Add(allCategoryList[c]);
                    }

                    if (intersectArray.Any())
                    {
                        tempMain.Add(allCategoryList[cid] + "[" + string.Join("、", tempSub) + "]");
                    }
                    else
                    {
                        tempMain.Add(allCategoryList[cid]);
                    }
                }
                else if (_sp.CategoryGet(cid).IsFinal) //子分類不必往下
                {
                    tempMain.Add(allCategoryList[cid]);
                }
            }
            return string.Join("、", tempMain);
        }

        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SkmPayDealPreviewForAlibaba()
        {
            SkmPayDealInfoViewModel deal = new SkmPayDealInfoViewModel();
            if (TempData["entity"] != null) {
                deal = (SkmPayDealInfoViewModel)TempData["entity"];
                if (!string.IsNullOrEmpty(deal.ComboDealTemp))
                {
                    deal.ComboDeal = new JsonSerializer().Deserialize<List<SkmComboDeal>>(deal.ComboDealTemp);
                    deal.CostCenter = new JsonSerializer().Deserialize<List<CostCenter>>(deal.CostCenterTemp);
                }
                else
                {
                    deal.ComboDeal = new List<SkmComboDeal>();
                    deal.CostCenter = new List<CostCenter>();
                }
            }
            if (deal.IsAllowStoreEdit)
            {
                SetPageDefault();
            }
            else
            {
                SetPageDefault(false, true);
            }
         
            return View(deal);
        }


        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SkmPayDealPreview(string guid)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault(false, true);
            SkmPayDealInfoViewModel deal = new SkmPayDealInfoViewModel();
            if (TempData["entity"] != null)
            {
                deal = (SkmPayDealInfoViewModel)TempData["entity"];
                ExternalDeal existDeal = _pp.ExternalDealGet(new Guid(deal.Guid));
                deal.Status = existDeal.IsLoaded ? existDeal.Status : (int)SKMDealStatus.Draft;
                deal.SkmHqCategories = SkmHqCategoriesConvertToList();
                deal.BeaconMessage = existDeal.IsLoaded ? existDeal.BeaconMessage : deal.BeaconMessage;
                deal.UserId = userId;
                deal.IsPrize = existDeal.IsPrizeDeal;
                //3.7版燒點新增
                var tempBe = _skm.SkmBurningEventGet(existDeal.Guid, string.Empty);
                if (tempBe.IsLoaded)
                {
                    deal.BurningEventId = tempBe.SkmEventId;
                    deal.BurningPoint = tempBe.BurningPoint;
                    deal.GiftPoint = tempBe.GiftPoint;
                    deal.GiftWalletId = tempBe.GiftWalletId;
                }

                deal.BurningPoint = existDeal.IsSkmPay ? existDeal.SkmPayBurningPoint : deal.BurningPoint;
                List<DealExhibitionCode> tempExhibitionCodes = new List<DealExhibitionCode>();
                tempExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(existDeal.Guid)
                    .Select(x => new DealExhibitionCode(x)).ToList());
                deal.ExhibitionData = new JsonSerializer().Serialize(tempExhibitionCodes);
                List<CostCenter> costCenterTemp = new JsonSerializer().Deserialize<List<CostCenter>>(deal.CostCenterTemp);
                deal.CostCenter = deal.IsBurningEvent ? costCenterTemp : new List<CostCenter>();
                var tempCoboDeal = _skmef.GetExternalDealCombo(existDeal.Guid).Where(x => !x.IsDel).ToList();
                deal.ComboDeal = new List<SkmComboDeal>(tempCoboDeal.Select(x => new SkmComboDeal(x))).OrderByDescending(x => x.Order).OrderBy(x => x.ShopCode).ToList();
                deal.ExcludingTaxPrice = existDeal.ExcludingTaxPrice;
                deal.IsRepeatPurchase = existDeal.IsRepeatPurchase;

                if (deal.Category != null && deal.Category.Any())
                {
                    //顯示分類名稱
                    var allCategoryList = ViewBag.SKMCategory as Dictionary<int, string>;
                    deal.ShowCategory = GetShowCategoryInfo(allCategoryList, deal.Category);
                }

                return View(deal);
            }

            Guid dealGuid = string.IsNullOrEmpty(guid) ? Guid.Empty : new Guid(guid);

            ExternalDeal ed = _pp.ExternalDealGet(dealGuid);
            if (!ed.IsLoaded)
            {
                if (ed.IsPrizeDeal)
                {
                    return RedirectToRoute("SKMPrizeList", new { dealType = "PrizeDeals" });
                }
                else
                {
                    return Redirect(String.Format("{0}/SKMDeal/SKMList", _config.SSLSiteUrl));
                }
            }

            var relationStore = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid).ToList();
            List<string> stores = relationStore.GroupBy(x => x.ParentSellerGuid).Select(x => x.Key.ToString()).ToList();
            var isAllShoppe = relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid);
            if (isAllShoppe.Any())
            {
                //有全館
                stores.AddRange(relationStore.Where(x => x.ParentSellerGuid == _config.SkmRootSellerGuid)
                                        .GroupBy(x => x.SellerGuid)
                                        .Select(x => x.Key.ToString())
                                        .ToList());
            }
            //分店群組
            stores = stores.GroupBy(x => x).Select(x => x.Key.ToString()).ToList();
            List<string> shoppes = relationStore.Select(x => x.StoreGuid.ToString()).ToList();
            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
            deal.Guid = ed.Guid.ToString();
            deal.Title = ed.Title;
            deal.ActiveType = (ActiveType)ed.ActiveType;
            deal.ProductCode = ed.ProductCode;
            deal.SpecialItemNo = ed.SpecialItemNo;
            deal.ItemNo = ed.ItemNo;
            deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
            deal.SkmOrigPrice = Convert.ToInt32(ed.SkmOrigPrice);
            deal.DiscountType = ed.DiscountType;
            deal.Discount = ed.Discount;
            deal.OrderTotalLimit = int.Parse(ed.OrderTotalLimit.ToString());
            deal.Remark = HttpUtility.UrlEncode(ed.Remark);
            deal.Description = ed.Description;/*HttpUtility.UrlEncode(ed.Description.Replace("<br />", "\n"));*/
            deal.Introduction = HttpUtility.UrlEncode(ed.Introduction.Replace("<br />", "\n"));
            deal.Category = !string.IsNullOrEmpty(ed.CategoryList) ? ed.CategoryList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            if (deal.Category != null && deal.Category.Any())
            {
                //顯示分類的階層關係 組成->主分類1(子分類1、子分類2)、主分類2
                var allCategoryList = ViewBag.SKMCategory as Dictionary<int, string>;
                deal.ShowCategory = GetShowCategoryInfo(allCategoryList, deal.Category);
            }
            deal.Tags = !string.IsNullOrEmpty(ed.TagList) ? ed.TagList.Trim('[', ']').Split(",").Select(int.Parse).ToArray() : new int[] { };
            deal.BusinessHourOrderDateS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderDateE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy", culture);
            deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("h:mm tt", culture);
            deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("h:mm tt", culture);
            deal.BusinessHourDeliverDateS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverDateE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("ddd MMM dd yyyy", culture) : string.Empty;
            deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS != null ? ed.BusinessHourDeliverTimeS.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE != null ? ed.BusinessHourDeliverTimeE.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("tt hh:mm") : deal.SettlementTime = ed.SettlementTime != null ? ed.SettlementTime.Value.ToString("h:mm tt", culture) : string.Empty;
            deal.BeaconType = ed.BeaconType;
            deal.BeaconMessage = ed.BeaconMessage.Replace("<br />", "\n");
            deal.Status = ed.Status;
            deal.Stores = stores.ToArray();
            deal.Shoppes = shoppes.ToArray();
            deal.AppDealPic = ed.AppDealPic;
            deal.ImagePath = ed.ImagePath;
            deal.SkmHqCategories = SkmHqCategoriesConvertToList();
            deal.UserId = userId;
            deal.StoreDisplayNames = SkmFacade.GetViewExternalDealStoreDisplayNameBydealGuid(dealGuid);
            //3.7版燒點新增
            //當burningEvent多筆的時候 活動代號(skmeventid)跟成本中心會不一樣 只取第一筆寫法要注意!
            var skmBurningEvent = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), string.Empty, false);
            deal.IsBurningEvent = skmBurningEvent.IsLoaded;
            deal.IsCostCenterHq = skmBurningEvent.IsCostCenterHq;
            deal.ActivityStatus = deal.IsBurningEvent && skmBurningEvent.CloseProjectStatus;
            deal.BurningPoint = ed.IsSkmPay && ed.SkmPayBurningPoint > 0 ? ed.SkmPayBurningPoint : deal.IsBurningEvent ? skmBurningEvent.BurningPoint : 0;
            deal.ExchangeNo = deal.IsBurningEvent ? skmBurningEvent.ExchangeItemId : string.Empty;
            deal.BurningEventId = deal.IsBurningEvent ? skmBurningEvent.SkmEventId : string.Empty;
            deal.GiftWalletId = deal.IsBurningEvent ? skmBurningEvent.GiftWalletId : string.Empty;
            deal.GiftPoint = deal.IsBurningEvent ? skmBurningEvent.GiftPoint : 0;
            deal.IsCrmDeal = ed.IsCrmDeal;

            //撈成本中心不能只以一筆burningEvent撈, 因為一筆external_deal會對應多筆成本中心
            var skmBurningEventIds = _skm.SkmBurningEventGet(Guid.Parse(deal.Guid), false).Select(p => p.Id).ToList();
            var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEventIds).ToList();
            var skmCostCenterInfo = _skm.SkmCostCenterInfoGetAll().ToList();
            var costCenters = new List<CostCenter>();
            foreach (var smBurningCostCenter in smBurningCostCenters)
            {
                var sci = skmCostCenterInfo.FirstOrDefault(x => x.StoreCode == smBurningCostCenter.StoreCode.PadLeft(3, '0'));
                var costCenter = new CostCenter();
                costCenter.StoreCode = smBurningCostCenter.StoreCode.PadLeft(4, '0');
                costCenter.StoreName = sci == null ? string.Empty : sci.StoreName;
                costCenter.SelectCenterCode = smBurningCostCenter.CostCenterCode;
                costCenter.MarketCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.SellCostCenterCode = skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")) != null ? skmCostCenterInfo.FirstOrDefault(y => y.StoreCode == smBurningCostCenter.StoreCode && !y.StoreName.Contains("超市")).CostCenterCode : string.Empty;
                costCenter.OrderNo = smBurningCostCenter.OrderCode;
                costCenters.Add(costCenter);
            }
            deal.CostCenter = deal.IsBurningEvent ? costCenters : new List<CostCenter>();
            var combodeals = _skmef.GetExternalDealCombo(ed.Guid).Where(x => !x.IsDel).ToList();
            deal.ComboDeal = new List<SkmComboDeal>(combodeals.Select(x => new SkmComboDeal(x))).OrderByDescending(x => x.Order).OrderBy(x => x.ShopCode).ToList();

            if (deal.ActiveType == ActiveType.BuyGetFree)
            {
                ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                if (relationItemList.Any())
                {
                    deal.ExternalDealRelationItemList = relationItemList;
                }
                deal.Buy = ed.Buy;
                deal.Free = ed.Free;
            }
            deal.IsPrize = ed.IsPrizeDeal;
            deal.IsAllowEdit = deal.Status <= (int)SKMDealStatus.Draft;
            deal.IsSkmPay = ed.IsSkmPay;
            deal.ExcludingTaxPrice = ed.ExcludingTaxPrice;
            deal.IsRepeatPurchase = ed.IsRepeatPurchase;
            deal.RepeatPurchaseType = (SkmDealRepeatPurchaseType)ed.RepeatPurchaseType;
            deal.RepeatPurchaseCount = ed.RepeatPurchaseCount;
            deal.EnableInstall = ed.EnableInstall;
            deal.skuid = ed.Skuid;
            #region Load 策展代號

            List<DealExhibitionCode> dealExhibitionCodes = new List<DealExhibitionCode>();
            dealExhibitionCodes.AddRange(_cep.GetViewExternalDealExhibitionCode(dealGuid)
                .Select(x => new DealExhibitionCode(x)).ToList());
            deal.ExhibitionData = new JsonSerializer().Serialize(dealExhibitionCodes);

            #endregion Load 策展代號

            return View(deal);
        }

       
        #endregion 建檔 SkmPay Verion

        #region 檔次儲存與建檔

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SaveTempData(SkmPayDealInfoViewModel model)
        {
            ExternalDeal deal = _pp.ExternalDealGet(new Guid(model.Guid));

            model.IsAllowEdit = deal.IsLoaded ? deal.Status <= (int)SKMDealStatus.Draft : true;

            if (deal.ActiveType == (int)ActiveType.BuyGetFree)
            {
                ExternalDealRelationItemCollection relationItemList = _pp.ExternalDealRelationItemGetListByDealGuid(deal.Guid);
                if (relationItemList.Any())
                {
                    model.ExternalDealRelationItemList = relationItemList;
                }
            }

            TempData["entity"] = model;
            return Json(new { Result = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 建檔確認頁
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SkmAppPreview(string bid)
        {
            Guid bGuid;

            if (!Guid.TryParse(bid, out bGuid))
            {
                return Json(new { Result = "Error", Message = "Bid格式錯誤，請檢查!" });
            }

            ViewBag.Oauth = _config.SkmBackendOauth;
            ViewBag.ImageSize = (int)ApiImageSize.XS;
            ViewBag.Site = _config.SSLSiteUrl;

            return View();
        }

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SaveDraft(SkmPayDealInfoViewModel model)
        {

            Guid dealGuid = string.IsNullOrEmpty(model.Guid) ? Guid.NewGuid() : Guid.Parse(model.Guid);
            bool isNew = !_pp.ExternalDealGet(dealGuid).IsLoaded;
            bool flag = false;
            DateTime now = DateTime.Now;

            ExternalDeal deal = isNew ? new ExternalDeal() : _pp.ExternalDealGet(dealGuid);
            deal.Guid = isNew ? dealGuid : deal.Guid;

            //驗證圖片大小
            if (_config.EnableCheckDealImageLength && !string.IsNullOrEmpty(model.TempAppDealPic)
                && (isNew || deal.CreateTime > Convert.ToDateTime(_config.EnableCheckDealImageLengthDate))
               )
            {
                //data:image/jpeg;base64,/9j/4.....
                var index = model.TempAppDealPic.IndexOf(',');
                var image = index > -1 ? model.TempAppDealPic.Substring(index + 1, model.TempAppDealPic.Length - (index + 1)) : model.TempAppDealPic;
                if (Convert.FromBase64String(image).Length > 500 * 1024)
                {
                    return Json(new { Result = false, Message = "商品圖片不可超過500KB" });
                }
            }

            //共用 IsAvailable shop
            SkmShoppeCollection allShoppe = _sp.SkmShoppeCollectionGetAll(false);

            //代表選擇總公司, 則預設所有分店下的所有贈品處, 採取載產qrcode時對於使用者選擇的館位做篩選, 此時預設所有都可
            if (model.Stores.Length == 1 && model.Stores[0] == _config.SkmRootSellerGuid.ToString())
            {
                //model.Stores = 所有分店
                //model.Shoppes = 所有分店>所有館位>所有贈品處
                var stores = _sp.SellerGetSkmParentList(); //seller
                model.Stores = stores.Select(p => p.Guid.ToString()).ToArray();

                var seller_guids = stores.Select(p => p.Guid).ToList();
                var exchangeShop =
                    allShoppe.Where(p => p.IsShoppe && p.IsExchange && seller_guids.Contains(p.SellerGuid.Value));
                //可兌換地點
                model.Shoppes = exchangeShop.Select(p => p.StoreGuid.ToString()).ToArray(); //shopps
                model.IsHqDeal = true;
            }
            else
            {
                model.IsHqDeal = false;
            }

            var myDefaultShoppe = model.Shoppes[0];
            if (model.SourceSeller.Trim() != "null" && model.SourceSeller.Trim() != string.Empty)//新建檔才需要判斷
            {
                var myDefaultSeller = model.SourceSeller;
                //返回列表的參數

                foreach (var shoppe in model.Shoppes.ToList())
                {
                    var shoppes = allShoppe.Where(p => p.StoreGuid == Guid.Parse(shoppe)).FirstOrDefault();
                    if (shoppes.ParentSellerGuid == Guid.Parse(myDefaultSeller))
                    {
                        myDefaultShoppe = shoppe;
                        break;
                    }
                }
            }

            var firstSkmShoppe = allShoppe.Where(p => p.StoreGuid == Guid.Parse(myDefaultShoppe)).FirstOrDefault();

            //判斷是否為全館
            string firstStore = firstSkmShoppe.ParentSellerGuid == _config.SkmRootSellerGuid ? firstSkmShoppe.SellerGuid.ToString() : firstSkmShoppe.ParentSellerGuid.ToString();
            string firstShopCode = firstSkmShoppe.ShopCode;
            ExternalDealRelationStoreCollection removeShoppes = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid); //before all shoppes
            //判斷是否更新清算時間
            var isDel = removeShoppes.Select(x => x.IsDel).FirstOrDefault();
            //檢查每分店至少有一櫃位
            //model.IsPrize = true = 抽獎預設所有分店的所有館下所有贈品櫃,故不必檢查
            //IsAllStore    = true = skm pay 全選 預設選擇全部適用分店下的全館, 故也不用檢查
            if (!model.IsPrize && !model.IsAllStore)
            {
                foreach (var store in model.Stores.ToList())
                {
                    Guid parentGuid;
                    Guid.TryParse(store, out parentGuid);
                    var checkCount = 0;
                    foreach (var shoppe in model.Shoppes.ToList())
                    {
                        Guid shoppeGuid;
                        Guid.TryParse(shoppe, out shoppeGuid);
                        var skmShoppe = allShoppe.Where(p => p.StoreGuid == shoppeGuid).FirstOrDefault();

                        if ((skmShoppe.ParentSellerGuid != parentGuid &&
                             skmShoppe.ParentSellerGuid != _config.SkmRootSellerGuid) ||
                            (skmShoppe.ParentSellerGuid == _config.SkmRootSellerGuid &&
                             skmShoppe.SellerGuid.Value != parentGuid)) continue;
                        checkCount++;
                        break;
                    }
                    if (checkCount != 0) continue;
                    flag = false;
                    return Json(new { Result = flag, Message = "每個分店店鋪至少要勾選一個!" });
                }

                //條碼為 990000+7x 時，檢核條碼 7x 是否與勾選的櫃號相同，不同不給存
                if ((model.ExchangeType == SkmDealExchangeType.Normal ||
                        model.ExchangeType == SkmDealExchangeType.Cache ||
                        (model.IsSkmPay && model.ExchangeType == SkmDealExchangeType.Exchange))
                    && model.ProductCode.Contains("9900007"))
                {
                    var brandCounterCode = model.ProductCode.Replace("990000", string.Empty);
                    if (allShoppe.Where(x => model.Shoppes.Contains(x.StoreGuid.ToString()) && x.BrandCounterCode != brandCounterCode).Any())
                    {
                        return Json(new { Result = false, Message = "條碼與7x櫃號不符" });
                    }
                }

                //檢查顯示櫃位
                if (model.StoreDisplayNames != null && model.StoreDisplayNames.Count > 0)
                {
                    //先將有選擇分店的各館資料取出
                    IEnumerable<SkmShoppe> sellerShoppes = allShoppe.Where(item => !item.IsShoppe && item.SellerGuid.HasValue && model.Stores.Any(seller => item.SellerGuid.Value.ToString() == seller));
                    //轉換為有選顯示櫃名的館shop code
                    IEnumerable<string> displayNmaeShopCodes = model.StoreDisplayNames.Select(item => item.ShopCode).Distinct();
                    foreach (var sellerGroup in sellerShoppes.GroupBy(item => item.SellerGuid))
                    {
                        if (!sellerGroup.Any(item => displayNmaeShopCodes.Any(shopCode => item.ShopCode == shopCode)))
                        {
                            return Json(new { Result = false, Message = "每個分店顯示至少要勾選一個!" });
                        }
                    }
                }
            }
            if (model.Status <= (int)SKMDealStatus.WaitingConfirm) //todo:非已確認狀態都可更新
            {
                deal.ActiveType = (int)model.ActiveType;
                deal.Title = model.Title;
                deal.ProductCode = model.ProductCode.Trim();
                deal.SpecialItemNo = model.SpecialItemNo.Trim();
                deal.ItemNo = model.ItemNo.Trim();
                deal.ItemOrigPrice = model.ItemOrigPrice;
                deal.SkmOrigPrice = model.SkmOrigPrice;
                deal.DiscountType = model.ActiveType == ActiveType.Product ? (int)DiscountType.DiscountPrice : model.DiscountType;
                deal.Discount = model.Discount;
                deal.OrderTotalLimit = model.OrderTotalLimit;
                deal.Remark = HttpUtility.UrlDecode(model.Remark.Trim()).Replace("\n", "").Replace("\t", "");
                deal.Description = HttpUtility.UrlDecode(model.Description.Trim()).Replace("\n", "").Replace("\t", "");
                deal.Introduction = HttpUtility.UrlDecode(model.Introduction.Trim()).Replace("\n", "<br />");
                deal.BusinessHourOrderTimeS = ConvertDateAndTime(model.BusinessHourOrderDateS, model.BusinessHourOrderTimeS).Value;
                deal.BusinessHourOrderTimeE = ConvertDateAndTime(model.BusinessHourOrderDateE, model.BusinessHourOrderTimeE).Value;
                deal.BusinessHourDeliverTimeS = model.ActiveType != ActiveType.Experience ? ConvertDateAndTime(model.BusinessHourDeliverDateS, model.BusinessHourDeliverTimeS) : deal.BusinessHourOrderTimeS;
                deal.BusinessHourDeliverTimeE = model.ActiveType != ActiveType.Experience ? ConvertDateAndTime(model.BusinessHourDeliverDateE, model.BusinessHourDeliverTimeE) : deal.BusinessHourOrderTimeE;
                deal.SettlementTime = string.IsNullOrEmpty(model.SettlementTime) ? null : ConvertDateAndTime(model.BusinessHourDeliverDateS, model.SettlementTime);
                deal.BeaconType = model.BeaconType;
                deal.BeaconMessage = (model.BeaconType == (int)BeaconType.Default) ? string.Format(_config.SkmBeaconDefaultMsg, model.Title) : model.BeaconMessage.Trim().Replace("\n", "<br />");
                deal.ImagePath = string.IsNullOrEmpty(deal.ImagePath) ? string.Empty : deal.ImagePath;
                deal.AppDealPic = string.IsNullOrEmpty(deal.AppDealPic) ? string.Empty : deal.AppDealPic;
                deal.IsSkmPay = model.IsSkmPay;
                deal.EnableInstall = model.EnableInstall;
                logger.Info(string.Format("model.Status={0},model.TaxRate={1}", model.Status, model.TaxRate));
                deal.TaxRate = model.TaxRate / 100; //model.TaxRate是利用商品條碼跟skm查詢得知
                deal.ExcludingTaxPrice = model.IsSkmPay ? SkmFacade.GetSkmPayExcludingTaxPrice(model.Discount, deal.TaxRate) : 0;
                logger.Info(string.Format("deal.TaxRate={0},deal.ExcludingTaxPrice={1}", deal.TaxRate, deal.ExcludingTaxPrice));
                deal.SkmPayBurningPoint = model.IsSkmPay ? model.BurningPoint : 0;
                deal.IsRepeatPurchase = model.IsRepeatPurchase;
                deal.ExchangeType = (int)model.ExchangeType;
                deal.RepeatPurchaseType = (int)model.RepeatPurchaseType;
                deal.RepeatPurchaseCount = model.RepeatPurchaseCount;
                deal.Skuid = model.skuid;
                if (!isNew)
                {
                    deal.ModifyId = User.Identity.Name;
                    deal.ModifyTime = now;
                }
                else
                {
                    deal.Status = (int)SKMDealStatus.Draft;
                    deal.ComeFromType = (int)OrderClassification.Skm;
                    deal.CreateId = User.Identity.Name;
                    deal.CreateTime = now;
                    deal.ModifyId = User.Identity.Name;
                    deal.ModifyTime = now;
                }


                ExternalDealRelationStoreCollection newShoppes = new ExternalDealRelationStoreCollection();
                ExternalDealRelationStoreCollection newSellers = new ExternalDealRelationStoreCollection(); //no shoppe and except already exists in store

                //1.非注目商品僅能在同一賣家選取多分店
                //2.注目商品不受限
                //4.避免QR code太大增加選取櫃位上限
                bool is_save = model.Shoppes.Count() <= 100;
                //first except now shoopes
                if (is_save || model.IsPrize)//抽獎會超過100, 因兌換地點改在前台抽獎後選擇, 故產生qrcode時再做過濾
                {
                    foreach (var shoppe in model.Shoppes)
                    {
                        if (removeShoppes.Any(x => x.StoreGuid.ToString() == shoppe))
                        {
                            newShoppes.Add(removeShoppes.FirstOrDefault(x => x.StoreGuid.ToString() == shoppe));
                            removeShoppes.Remove(removeShoppes.FirstOrDefault(x => x.StoreGuid.ToString() == shoppe));
                        }
                        else
                        {
                            var bid = removeShoppes.Select(x => x.Bid).FirstOrDefault();
                            var skmShoppe = allShoppe.FirstOrDefault(x => x.StoreGuid.ToString() == shoppe);
                            if (skmShoppe != null)
                            {
                                newShoppes.Add(new ExternalDealRelationStore
                                {
                                    DealGuid = dealGuid,
                                    StoreGuid = Guid.Parse(shoppe),
                                    SellerGuid = skmShoppe.SellerGuid,
                                    ModifyTime = now,
                                    Bid = bid,
                                    ParentSellerGuid = skmShoppe.ParentSellerGuid,
                                });
                            }
                            else
                            {
                                flag = false;
                                return Json(new { Result = flag, Message = "櫃位資料有誤，請檢查!" });
                            }
                        }
                    }
                }
                else
                {
                    flag = false;
                    return Json(new { Result = flag, Message = "櫃位數量超過上限!" });
                }


                List<string> newSellersList = model.Stores.ToList();

                //except seller in shoppes (分店)
                newShoppes.Select(x => x.ParentSellerGuid.ToString()).ForEach(x => { newSellersList.Remove(x); });
                //排除全館且非新增的分店
                foreach (var shoppe in newShoppes.Where(shoppe => shoppe.ParentSellerGuid == _config.SkmRootSellerGuid && newSellersList.Contains(shoppe.SellerGuid.ToString())))
                {
                    newSellersList.Remove(shoppe.SellerGuid.ToString());
                }

                //then, except already exists sellers(not in shoppe)
                foreach (string store in newSellersList)
                {
                    if (removeShoppes.Any(x => x.ParentSellerGuid.ToString() == store))
                    {
                        newSellers.Add(removeShoppes.FirstOrDefault(x => x.ParentSellerGuid.ToString() == store));
                        removeShoppes.Remove(removeShoppes.FirstOrDefault(x => x.ParentSellerGuid.ToString() == store));
                    }
                    else
                    {
                        var skmShoppe = allShoppe.FirstOrDefault(x => x.ParentSellerGuid.ToString() == store && x.IsShoppe == false);
                        if (skmShoppe != null)
                        {
                            newSellers.Add(new ExternalDealRelationStore
                            {
                                DealGuid = dealGuid,
                                StoreGuid = skmShoppe.StoreGuid.Value,
                                SellerGuid = skmShoppe.SellerGuid,
                                ModifyTime = now,
                                ParentSellerGuid = skmShoppe.ParentSellerGuid,
                            });
                        }
                    }
                }
                //mergin
                newShoppes.AddRange(newSellers);

                _pp.ExternalDealRelationStoreDeleteByStores(removeShoppes);
                _pp.ExternalDealRelationStoreCollectionSet(newShoppes);

                //更新external_deal_relation_store_display_name
                _cep.DeleteExternalDealRelationStoreDisplayNameByDealGuid(dealGuid);
                _cep.InsertExternalDealRelationStoreDisplayNameByDealGuid(model.StoreDisplayNames.Select(item =>
                {
                    return new ExternalDealRelationStoreDisplayName()
                    {
                        ExternalDealGuid = dealGuid,
                        SkmStoreDisplayNameGuid = item.Guid,
                        CreateDate = DateTime.Now
                    };
                }));

                #region externalDealRelationItem 買幾送幾
                if (model.ActiveType == ActiveType.BuyGetFree)
                {
                    var RelationItemCol = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RelationItem>>(model.RelationItemJsonList);
                    deal.Buy = model.Buy;
                    deal.Free = model.Free;
                    ExternalDealRelationItemCollection removeItems = _pp.ExternalDealRelationItemGetListByDealGuid(dealGuid);
                    ExternalDealRelationItemCollection newItems = new ExternalDealRelationItemCollection();
                    //購買
                    foreach (var item in RelationItemCol.Where(x => x.IsFree == false).ToList())
                    {
                        if (removeItems.Any(x => x.ProductCode == item.ProductCode))
                        {
                            newItems.Add(removeItems.FirstOrDefault(x => x.ProductCode == item.ProductCode));
                            removeItems.Remove(removeItems.FirstOrDefault(x => x.ProductCode == item.ProductCode));
                        }
                        else
                        {
                            newItems.Add(new ExternalDealRelationItem
                            {
                                ExternalGuid = dealGuid,
                                ProductCode = item.ProductCode.Trim(),
                                ItemNo = item.ItemNo,
                                ItemName = item.ItemName,
                                ModifyId = User.Identity.Name,
                                ModifyTime = now,
                                IsFree = false
                            });
                        }
                    }
                    //贈品
                    foreach (var item in RelationItemCol.Where(x => x.IsFree).ToList())
                    {
                        if (removeItems.Any(x => x.ProductCode == item.ProductCode))
                        {
                            newItems.Add(removeItems.FirstOrDefault(x => x.ProductCode == item.ProductCode));
                            removeItems.Remove(removeItems.FirstOrDefault(x => x.ProductCode == item.ProductCode));
                        }
                        else
                        {
                            newItems.Add(new ExternalDealRelationItem
                            {
                                ExternalGuid = dealGuid,
                                ProductCode = item.ProductCode.Trim(),
                                ItemNo = item.ItemNo,
                                ItemName = item.ItemName,
                                ModifyId = User.Identity.Name,
                                ModifyTime = now,
                                IsFree = true
                            });

                        }
                    }
                    _pp.ExternalDealRelationItemDeleteByItems(removeItems);
                    _pp.ExternalDealRelationItemCollectionSet(newItems);
                }

                #endregion

                ExternalDealFacade.ClearViewExternalDealCaches(removeShoppes.Select(t => t.Bid.GetValueOrDefault()));
                ExternalDealFacade.ClearViewExternalDealCaches(newShoppes.Select(t => t.Bid.GetValueOrDefault()));

                #region 檔次拆分館別子檔

                if (model.DealVersion >= (int)ExternalDealVersion.SkmPay)
                {
                    if (!string.IsNullOrEmpty(model.ComboDealTemp))
                    {
                        model.ComboDeal = new JsonSerializer().Deserialize<List<SkmComboDeal>>(model.ComboDealTemp);
                    }
                    else
                    {
                        model.ComboDeal = new List<SkmComboDeal>();
                    }

                    var comboDeals = _skmef.GetExternalDealCombo(dealGuid);
                    var newComboDeals = new List<ExternalDealCombo>();
                    comboDeals.ForEach(x => x.IsDel = true);
                    foreach (var d in model.ComboDeal)
                    {
                        var cd = comboDeals.FirstOrDefault(x => x.ShopCode == d.ShopCode);
                        if (cd != null)
                        {
                            cd.Qty = int.Parse(d.Qty);
                            cd.ShopName = d.ShopName;
                            cd.IsDel = false;
                        }
                        else
                        {
                            var shop = allShoppe.FirstOrDefault(x => x.ShopCode.ToString() == d.ShopCode.Trim() && x.IsShoppe == false);
                            if (shop == null)
                            {
                                continue;
                            }

                            newComboDeals.Add(new ExternalDealCombo
                            {
                                ExternalDealGuid = dealGuid,
                                IsDel = false,
                                Qty = int.Parse(d.Qty),
                                ShopCode = d.ShopCode,
                                ShopName = d.ShopName,
                                ShopGuid = (Guid)shop.StoreGuid,
                                SellerGuid = (Guid)shop.SellerGuid
                            });
                        }
                    }

                    _skmef.SetExternalDealCombo(comboDeals, true);
                    _skmef.SetExternalDealCombo(newComboDeals, false);
                }

                #endregion 檔次拆分館別子檔

                #region 上傳主圖及列表方圖

                if (model.Status == -1 || //新增時，此值是-1，且不在enum裡面
                    model.Status == (int)SKMDealStatus.Draft ||
                    model.Status == (int)SKMDealStatus.WaitingConfirm)
                {
                    var sellerId = _sp.SellerGet((Guid)newShoppes.Select(x => x.SellerGuid).FirstOrDefault()).SellerId;
                    if (!string.IsNullOrEmpty(model.AppDealPic))
                    {
                        if (!model.AppDealPic.Contains("http")) 
                        {
                            var tempAppDealPic = Path.Combine(_config.WebTempPath, WebStorage._IMAGE_TEMP,
                                model.AppDealPic);
                            ImageUtility.UploadFile(new PhysicalPostedFileAdapter(tempAppDealPic), UploadFileType.PponEvent,
                                sellerId,
                                Path.GetFileNameWithoutExtension(tempAppDealPic), false);
                            string appDealPic = ImageFacade.GenerateMediaPath(sellerId, Path.GetFileName(model.AppDealPic));
                            deal.AppDealPic = appDealPic;
                        }
                        else
                        {
                            //阿里中台圖檔由中台上傳, 我們只記錄網址
                            deal.AppDealPic = model.AppDealPic + "?x-oss-process=image/resize,m_fixed,h_640,w_640";
                        }
                    }
                    if (!string.IsNullOrEmpty(model.ImagePath) && !model.ImagePath.Contains("http"))
                    {

                        var tempImagePath = Path.Combine(_config.WebTempPath, WebStorage._IMAGE_TEMP,
                            model.ImagePath);
                        ImageUtility.UploadFile(new PhysicalPostedFileAdapter(tempImagePath), UploadFileType.PponEvent,
                            sellerId,
                            Path.GetFileNameWithoutExtension(tempImagePath), false);
                        string imagePath = ImageFacade.GenerateMediaPath(sellerId, Path.GetFileName(model.ImagePath));
                        deal.ImagePath = imagePath;
                    }
                }


                #endregion
            }

            deal.Remark = HttpUtility.UrlDecode(model.Remark.Trim()).Replace("\n", "").Replace("\t", "");
            deal.Description = HttpUtility.UrlDecode(model.Description.Trim()).Replace("\n", "").Replace("\t", "");
            deal.Introduction = HttpUtility.UrlDecode(model.Introduction.Trim()).Replace("\n", "<br />");
            deal.CategoryList = model.Category != null && model.Category.Count() > 0 ? string.Format("[{0}]", string.Join(",", model.Category.Select(x => x.ToString()))) : null;
            deal.TagList = model.Tags != null && model.Tags.Count() > 0 ? string.Format("[{0}]", string.Join(",", model.Tags.Select(x => x.ToString()))) : null;
            deal.DealVersion = model.DealVersion;
            deal.IsHqDeal = model.IsHqDeal;
            deal.IsPrizeDeal = model.IsPrize;
            deal.IsCrmDeal = model.IsCrmDeal;
            deal.DealIconId = model.DealIconId;


            var exhibitionData = ProviderFactory.Instance().GetSerializer().Deserialize<List<DealExhibitionCode>>(model.ExhibitionData);

            //刪除策展代號，避免若沒有輸入任何資料時不會刪除(排除若只有一筆且將他刪除的話不會刪除的問題)
            _cep.DeleteExternalDealExhibitionCode(deal.Guid);
            if (exhibitionData.Any())
            {
                _cep.AddExternalDealExhibitionCodeList(deal.Guid, exhibitionData.Select(p => p.ExhibitionCodeId).ToList());
            }

            //save external
            _pp.ExternalDealSet(deal);
            //update category_deal
            SkmFacade.UpdateSkmDealCategory(deal);
            //update (external_deal_realation_stroe, business_hour...etc)
            SkmFacade.UpdateDeal(deal);
            /*
            * 同步更新skm_deal_time_slot
            * */
            ViewExternalDealCollection vedc = _pp.ViewExternalDealGetListByDealrGuid(dealGuid);
            if (vedc.Count > 0)
            {
                var external = vedc.FirstOrDefault();
                if (external != null)
                {
                    if (external.Status >= (int)SKMDealStatus.WaitingConfirm)
                    {
                        var skmBurningEvent = _skm.SkmBurningEventGet(external.Guid, string.Empty, false);
                        //只要external_deal有資料 => 代表上下架時間已固定 => 代表燒點上下架時間已在umall新增過
                        //skmpay or 純金(非燒點) 才允許更改活動時間
                        if (external == null || external.IsSkmPay || !skmBurningEvent.IsLoaded)
                        {
                            var bids = vedc.Select(x => x.Bid).Distinct().ToList();
                            foreach (var bid in bids)
                            {
                                PponDeal vpd = _pp.PponDealGet(bid ?? Guid.Empty);
                                PponFacade.SkmDealTimeSlotSet(vpd);//update
                            }
                        }

                        /*
                        * 分店變更時，把多餘的Bid刪除(同分店不同櫃位不刪除)
                        * */
                        List<int> delIds = new List<int>();
                        foreach (ExternalDealRelationStore rs in removeShoppes)
                        {
                            var sameParentSellerList = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid)
                                .Where(x => x.ParentSellerGuid == rs.ParentSellerGuid);

                            if (rs.Bid != null && !sameParentSellerList.Any())
                            {
                                delIds.AddRange(_skm.SkmDealTimeSlotGetByBid(rs.Bid ?? Guid.Empty).Select(p => p.Id).ToList());
                            }
                        }
                        if (delIds.Any())
                        {
                            _skm.SkmDealTimeSlotDeleteAll(delIds);
                        }

                        #region 存檔時，重新產生skm_beacon_message_time_slot

                        if (!_config.EnableSKMNewBeacon)
                        {
                            //SkmBeaconMessage
                            foreach (ViewExternalDeal ved in vedc)
                            {
                                //所有櫃位 ved.Floor = null / 指定櫃位 ved.Floor != null
                                if (string.IsNullOrEmpty(ved.Floor))
                                {
                                    //全館，提品只會產生一筆資料，所以要自己產生
                                    var data = getFloor(ved.SellerGuid, ved.ShopCode);
                                    foreach (var d in data)
                                    {
                                        SkmBeaconMessage qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, d.Floor);
                                        if (qsbm != null)
                                        {
                                            qsbm.Subject = deal.BeaconMessage;
                                            _skm.SkmBeaconMessageSet(qsbm);

                                            if (ved.BeaconType != (int)BeaconType.None)
                                            {
                                                //SkmBeaconMessageTimeSlot
                                                SkmFacade.SkmBeaconMessageTimeSlotInsert(qsbm, (int)SkmBeaconType.Deal, false);
                                            }
                                            else
                                            {
                                                //不需BeaconMessage
                                                string[] filter = new string[1];
                                                filter[0] = "business_hour_guid=" + (ved.Bid ?? Guid.Empty).ToString();
                                                SkmBeaconMessageTimeSlotCollection delData = _skm.SkmBeaconMessageTimeSlotGetList("", filter);
                                                foreach (SkmBeaconMessageTimeSlot sdts in delData)
                                                {
                                                    _skm.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    //有選擇館別，提品會產生多份資料
                                    SkmBeaconMessage qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, ved.Floor);
                                    if (qsbm != null)
                                    {
                                        qsbm.Subject = deal.BeaconMessage;
                                        _skm.SkmBeaconMessageSet(qsbm);

                                        if (ved.BeaconType != (int)BeaconType.None)
                                        {
                                            //SkmBeaconMessageTimeSlot
                                            SkmFacade.SkmBeaconMessageTimeSlotInsert(qsbm, (int)SkmBeaconType.Deal, false);
                                        }
                                        else
                                        {
                                            //不需BeaconMessage
                                            string[] filter = new string[1];
                                            filter[0] = "business_hour_guid=" + (ved.Bid ?? Guid.Empty).ToString();
                                            SkmBeaconMessageTimeSlotCollection delData = _skm.SkmBeaconMessageTimeSlotGetList("", filter);
                                            foreach (SkmBeaconMessageTimeSlot sdts in delData)
                                            {
                                                _skm.SkmBeaconMessageTimeSlotDelete(sdts.Id);
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        #endregion 存檔時，重新產生skm_beacon_message_time_slot
                    }
                }
            }


            flag = true;
            string back = string.Format("?filter=0&seller={0}&shop={1}&page=1", firstStore, firstShopCode);
            logger.Info("SaveDraft");
            return Json(new { Result = flag, Message = "", BackUrl = back });
        }
       

        #endregion 檔次儲存與建檔

        #region Page using api

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public JsonResult SaveDraftWithBurningEvent(SkmBurningEventModel skmBurningEventModel)
        {
           
            logger.Info("skmBurningEventModel=" + JsonConvert.SerializeObject(skmBurningEventModel));
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, type:{1}, name:{2}"
                , skmBurningEventModel.ExternalGuid, skmBurningEventModel.ActiveType, skmBurningEventModel.EventName);
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, start:{1}, end:{2}"
                , skmBurningEventModel.ExternalGuid, skmBurningEventModel.DealStartTime, skmBurningEventModel.DeliveryTimeE);

            string message = string.Empty;
            var flag = false;
            if (skmBurningEventModel.DealVersion == (int)ExternalDealVersion.SkmPay)
            {
                flag = SkmFacade.CreateSkmBurningEventCombo(skmBurningEventModel, out message);
            }
            else
            {
                flag = SkmFacade.CreateSkmBurningEvent(skmBurningEventModel, out message);
            }

            logger.InfoFormat("Bu.SaveEventEnd->guid:{0}, message:{1}, flag:{2}"
                , skmBurningEventModel.ExternalGuid, message, flag);

            return Json(new { Result = flag, Message = message });
        }

        /// <summary>
        /// 阿里巴巴燒點
        /// </summary>
        /// <param name="skmBurningEventModel"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public JsonResult SaveDraftWithBurningEventForAlibaba(SkmPayDealInfoViewModel deal)
        {
            SkmBurningEventModel skmBurningEventModel = new SkmBurningEventModel();
            if (deal!=null)
            {
                skmBurningEventModel.ActiveType = (int)deal.ActiveType;               
                skmBurningEventModel.ExternalGuid = Guid.Parse(deal.Guid);
                skmBurningEventModel.EventName = deal.Title;
                skmBurningEventModel.ExchangeQty = deal.OrderTotalLimit;
                skmBurningEventModel.DeliveryTimeS = DateTime.Parse(deal.BusinessHourDeliverDateS + " " + deal.BusinessHourDeliverTimeS);
                skmBurningEventModel.DeliveryTimeE = DateTime.Parse(deal.BusinessHourDeliverDateE + " " + deal.BusinessHourDeliverTimeE);
                skmBurningEventModel.DealStartTime = DateTime.Parse(deal.BusinessHourOrderDateS + " " + deal.BusinessHourOrderTimeS);
                skmBurningEventModel.DealEndTime = DateTime.Parse(deal.BusinessHourOrderDateE + " " + deal.BusinessHourOrderTimeE);
                skmBurningEventModel.BurningPoint = deal.BurningPoint;
                skmBurningEventModel.ExchangeItemId = deal.ExchangeNo;
                skmBurningEventModel.CostCenter = deal.CostCenter.Select(o => new SkmBurningEventCostCenter()
                {
                    StoreCode = int.Parse(o.StoreCode).ToString(),
                    CostCenterCode = o.SelectCenterCode,
                    OrderCode = o.OrderNo,
                    ExChangeQty = int.Parse(deal.ComboDeal.Where(y => y.ShopCode == int.Parse(o.StoreCode).ToString()).FirstOrDefault().Qty??"0")
                }).ToList();
                skmBurningEventModel.CloseProjectStatus = deal.ActivityStatus;
                skmBurningEventModel.GiftWalletId = deal.GiftWalletId;
                skmBurningEventModel.GiftPoint = deal.GiftPoint;
                skmBurningEventModel.PrizeDeal = deal.IsPrize;
                skmBurningEventModel.DealVersion = 2;
                skmBurningEventModel.IsCostCenterHq = deal.IsCostCenterHq;
            }

            logger.Info("skmBurningEventModel=" + JsonConvert.SerializeObject(skmBurningEventModel));
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, type:{1}, name:{2}"
                , skmBurningEventModel.ExternalGuid, skmBurningEventModel.ActiveType, skmBurningEventModel.EventName);
            logger.InfoFormat("Bu.SaveEvent->guid:{0}, start:{1}, end:{2}"
                , skmBurningEventModel.ExternalGuid, skmBurningEventModel.DealStartTime, skmBurningEventModel.DeliveryTimeE);

            string message = string.Empty;
            var flag = false;
            if (skmBurningEventModel.DealVersion == (int)ExternalDealVersion.SkmPay)
            {
                flag = SkmFacade.CreateSkmBurningEventCombo(skmBurningEventModel, out message,true);
            }
            else
            {
                flag = SkmFacade.CreateSkmBurningEvent(skmBurningEventModel, out message,true);
            }

            logger.InfoFormat("Bu.SaveEventEnd->guid:{0}, message:{1}, flag:{2}"
                , skmBurningEventModel.ExternalGuid, message, flag);

            return Json(new { Result = flag, Message = message });
        }

        /// <summary>
        /// 設定失效
        /// </summary>
        /// <param name="externalGuid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult SetInvalid(Guid externalGuid)
        {
            var ed = _pp.ExternalDealGet(externalGuid);
            if (ed.Status != (int)SKMDealStatus.Draft)
            {
                return Json(new { Result = false, Message = "只有草稿階段的商品可設定失效。" });
            }

            ed.Status = (int)SKMDealStatus.Invalid;
            _pp.ExternalDealSet(ed);
            return Json(new { Result = true, Message = string.Empty });
        }

        //測試用上架API
        [FilterIP]
        public ActionResult AlibabaTestOnMarket(string guid)
        {
            GoNext(guid, true);
            GoNext(guid, true);
            GoNext(guid, true);
            return Json(new { Result = true, Message = "上好架了~ 應該吧..." });
        }


        /// <summary>
        /// for 阿里巴巴 上架功能
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        [HttpPost]
        [FilterIP]
        //[Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult AlibabaOnMarket(long itemId)
        {
            StockCenterDeal stockCenterDeal = SkmFacade.GetStockCenterDealGuidByAliItemId(itemId);
            if (stockCenterDeal.skuId == 0)
            {
                return Json(new { Result = false, Message = "There is no skuId from Alibaba." });
            }

            var ed = _pp.ExternalDealGet(stockCenterDeal.deal.Guid);
           
            if (ed.Guid.Equals(Guid.Empty))
            {
                return Json(new { Result = false, Message = "Can't find this item." });
            }
            else {
                var skmBurningEventList = _skm.SkmBurningEventGet(ed.Guid, false);
              
                var isBurningEvent = skmBurningEventList.Any();
                var smBurningCostCenters = _skm.SkmBurningCostCenterGet(skmBurningEventList.Select(x => x.Id).ToList()).ToList();
                if (isBurningEvent)
                {
                    var combodeals = _skmef.GetExternalDealCombo(ed.Guid).Where(x => !x.IsDel).ToList();
                    string message = "";
                    SkmBurningEventModel skmBurningEventModel = new SkmBurningEventModel();
                    skmBurningEventModel.ActiveType = ed.ActiveType;
                    skmBurningEventModel.ExternalGuid = ed.Guid;
                    skmBurningEventModel.EventName = ed.Title;
                    skmBurningEventModel.ExchangeQty = ed.OrderTotalLimit;
                    skmBurningEventModel.DeliveryTimeS = (DateTime)ed.BusinessHourDeliverTimeS;
                    skmBurningEventModel.DeliveryTimeE = (DateTime)ed.BusinessHourDeliverTimeE;
                    skmBurningEventModel.DealStartTime = ed.BusinessHourOrderTimeS;
                    skmBurningEventModel.DealEndTime = ed.BusinessHourOrderTimeE;
                    skmBurningEventModel.BurningPoint = skmBurningEventList[0].BurningPoint;
                    skmBurningEventModel.ExchangeItemId = skmBurningEventList[0].ExchangeItemId;
                    skmBurningEventModel.CostCenter =
                        smBurningCostCenters.Select(o => new SkmBurningEventCostCenter()
                    {
                        StoreCode = int.Parse(o.StoreCode).ToString(),
                        CostCenterCode = o.CostCenterCode,
                        OrderCode = o.OrderCode,
                        ExChangeQty = combodeals.Where(y => y.ShopCode == o.StoreCode).FirstOrDefault().Qty
                    }).ToList();
                    skmBurningEventModel.CloseProjectStatus = skmBurningEventList[0].CloseProjectStatus;
                    skmBurningEventModel.GiftWalletId = skmBurningEventList[0].GiftWalletId;
                    skmBurningEventModel.GiftPoint = skmBurningEventList[0].GiftPoint;
                    skmBurningEventModel.PrizeDeal = ed.IsPrizeDeal;
                    skmBurningEventModel.DealVersion = 2;
                    skmBurningEventModel.IsCostCenterHq = skmBurningEventList[0].IsCostCenterHq;
                    if (skmBurningEventModel.DealVersion == (int)ExternalDealVersion.SkmPay)
                    {
                        if (!SkmFacade.CreateSkmBurningEventCombo(skmBurningEventModel, out message)) {
                            return Json(new { Result = false, Message = "SkmBurningEvent failed ! " + message });
                        }
                       
                    }
                    else
                    {
                        if (!SkmFacade.CreateSkmBurningEvent(skmBurningEventModel, out message)){
                            return Json(new { Result = false, Message = "SkmBurningEvent failed ! " + message });
                        }
                    }
                }


              
            }


            GoNext(stockCenterDeal.deal.Guid.ToString(), true);
            if ((bool)TempData[AliResult])
            {
                GoNext(stockCenterDeal.deal.Guid.ToString(), true);
                if ((bool)TempData[AliResult])
                {
                    GoNext(stockCenterDeal.deal.Guid.ToString(), true);
                    if ((bool)TempData[AliResult])
                    {

                        var socketEnityList = new List<InventoryChangeEntity>();

                        List<ViewSkmDealGuidQuantity> viewSkmDealGuidQuantityInfos = _skmef.GetExternalQuantityByGuid(stockCenterDeal.deal.Guid);

                        foreach (var item in viewSkmDealGuidQuantityInfos.OrderByDescending(p => p.ShopCode))
                        {
                            socketEnityList.Add(new InventoryChangeEntity()
                            {
                                entityId = stockCenterDeal.skuId.ToString(),
                                entityType = 1,
                                code = stockCenterDeal.deal.ProductCode,
                                warehouseType = 100,
                                warehouseCode = item.ShopCode,
                                quantity = (int)item.OrderTotalLimit - item.OrderedQuantity + item.CanceledQuantity + item.ReturnedQuantity,
                                bizSrcType = 200

                            });
                        }
                        //alibaba 庫存增量
                        SKMCenterRocketMQTrans.InventoryAddAdjust(socketEnityList);
                    }
                }
            }
            SKMChannelOnSaleMessageEntity msgEntity = new SKMChannelOnSaleMessageEntity();
            msgEntity.itemId = itemId;
            msgEntity.success = (bool)TempData[AliResult];
            msgEntity.message = (string)TempData[AliMessage];
            SKMCenterRocketMQTrans.SKMItemOnSelfComplete(msgEntity);
            return Json(new { Result = (bool)TempData[AliResult], Message = (string)TempData[AliMessage] });
        }


        /// <summary>
        /// for 阿里巴巴 下架功能
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        [HttpPost]
        [FilterIP]
        //[Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult AlibabaOffMarket(long itemId)
        {
            StockCenterDeal stockCenterDeal = SkmFacade.GetStockCenterDealGuidByAliItemId(itemId);
            if (stockCenterDeal.skuId == 0)
            {
                return Json(new { Result = false, Message = "There is no skuId from Alibaba." });
            }

            OffShelf(stockCenterDeal.deal.Guid.ToString());
            //if ((bool)TempData[AliResult])
            //{

            //    List<ViewSkmDealGuidQuantity> viewSkmDealGuidQuantityInfos = _skmef.GetExternalQuantityByGuid(stockCenterDeal.deal.Guid);
            //    var socketEnityList = new List<InventoryChangeEntity>();

            //    foreach (var item in viewSkmDealGuidQuantityInfos.OrderByDescending(p => p.ShopCode))
            //    {
            //        socketEnityList.Add(new InventoryChangeEntity()
            //        {
            //            entityId = stockCenterDeal.skuId.ToString(),
            //            entityType = 1,
            //            code = stockCenterDeal.deal.ProductCode,
            //            warehouseType = 100,
            //            warehouseCode = item.ShopCode,
            //            quantity = (-1) * ((int)item.OrderTotalLimit - item.OrderedQuantity + item.CanceledQuantity + item.ReturnedQuantity),
            //            bizSrcType = 200

            //        });
            //    }
            //    //alibaba 庫存增量, quantity為負數
            //    SKMCenterRocketMQTrans.InventoryAddAdjust(socketEnityList);
            //}
            SKMChannelOnSaleMessageEntity msgEntity = new SKMChannelOnSaleMessageEntity();
            msgEntity.itemId = itemId;
            msgEntity.success = (bool)TempData[AliResult];
            msgEntity.message = (string)TempData[AliMessage];
            SKMCenterRocketMQTrans.SKMItemOffSelfComplete(itemId);
            return Json(new { Result = (bool)TempData[AliResult], Message = (string)TempData[AliMessage] });
        }


        /// <summary>
        /// 檔次關卡階段更新
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult GoNext(string guid) {
            return GoNext(guid, false);
        }

        /// <summary>
        /// 檔次關卡階段更新
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private ActionResult GoNext(string guid, bool bAlibabaGo)
        {
            string resultMsg = "";
            string username = User.Identity.Name;
            string firstStore = "";
            string firstShopCode = "";
            Guid dealGuid;
           
            TempData[AliResult] = true;
            TempData[AliMessage] = "";
            if (!Guid.TryParse(guid, out dealGuid) && (TempData["entity"] == null && !bAlibabaGo))
            {
                resultMsg = "傳入參數有誤";
                TempData[AliResult] = false;
                TempData[AliMessage] = resultMsg;
                return Json(new { Result = false, Message = resultMsg });
            }
            ViewExternalDealCollection deals = _pp.ViewExternalDealGetListByDealrGuid(dealGuid);
            bool flag = deals.Count > 0 || TempData["entity"] != null || bAlibabaGo;
            if (flag)
            {
                ExternalDeal deal = _pp.ExternalDealGet(dealGuid);
                SkmBurningEvent be = _skm.SkmBurningEventGet(dealGuid, string.Empty, true);
                var isBurningEvent = be.IsLoaded && be.BurningPoint > 0;
                ExternalDealRelationStoreCollection rsc = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid);
                Dictionary<Guid, Guid> bids = new Dictionary<Guid, Guid>(); //bid,store_guid
                DateTime now = DateTime.Now;
                int status = (int)SKMDealStatus.Draft;
                status = deal.IsLoaded ? deal.Status : status;
                bool isSkmPayVer = deal.DealVersion == (int)ExternalDealVersion.SkmPay;
                //檢查權限、檢查是否進入下一關
                logger.Info("GoNext++ status=" + status);

                switch (status)
                {
                  
                    case (int)SKMDealStatus.Draft:
                        if (TempData["entity"] != null)
                        {
                            SkmPayDealInfoViewModel model = (SkmPayDealInfoViewModel)TempData["entity"];
                            SaveDraft(model);
                            status += 1;
                            dealGuid = new Guid(model.Guid);
                            deal = _pp.ExternalDealGet(dealGuid);
                            SkmFacade.DealApplyNotify(deal);

                            SkmShoppe firstShoppe = model.Shoppes.Count() > 0 ? _sp.SkmShoppeGet(new Guid(model.Shoppes.FirstOrDefault())) : new SkmShoppe();
                            firstStore = firstShoppe.IsLoaded ? firstShoppe.SellerGuid.ToString() : string.Empty;
                            firstShopCode = firstShoppe.IsLoaded ? firstShoppe.ShopCode.ToString() : string.Empty;

                            
                        }
                        else if (bAlibabaGo) 
                        {
                            status += 1;
                            //dealGuid = new Guid(model.Guid);
                            //deal = _pp.ExternalDealGet(dealGuid);
                        } 
                        else
                        {
                            resultMsg = "送出失敗";
                            flag = false;
                        }
                        break;
                    case (int)SKMDealStatus.WaitingMake://make order
                        List<ExternalDealCombo> skmComboDeal = isSkmPayVer ? _skmef.GetExternalDealCombo(deal.Guid).Where(item => !item.IsDel).ToList() : new List<ExternalDealCombo>();
                        resultMsg = isSkmPayVer ? SkmFacade.OnSaveBusinessHourForSkmPay(deals, skmComboDeal, username, out bids)
                            : SkmFacade.OnSaveBusinessHour(deals, username, isBurningEvent, out bids);

                        if (bids.Count > 0)
                        {
                            status += isSkmPayVer ? 2 : 1;
                        }
                        else
                        {
                            resultMsg = "檔次建置未成功";
                            flag = false;
                        }

                        break;
                    case (int)SKMDealStatus.OnTheMake:
                        //17Life頁確
                        status += 1;

                        break;
                    case (int)SKMDealStatus.WaitingConfirm:
                        //新光頁確
                        status += 1;

                        #region 送出頁確時，更新SkmDealTimeSlot狀態 (此時最後一個步驟才會把skm_deal_time_slot.status改成正常顯示0)

                        var relationStoreBids = rsc.Where(p => p.Bid.HasValue).Select(x => x.Bid).Distinct().ToList();
                        //SkmDealTimeSlot
                        _skm.UpdateSkmDealTimeSlotSetStatus((int)DealTimeSlotStatus.Default, relationStoreBids);
                        //SkmBeaconMessageTimeSlot
                        _skm.UpdateSkmBeaconMessageTimeSlotStatus((int)DealTimeSlotStatus.Default, relationStoreBids,
                            (int)SkmBeaconType.Deal);
                        foreach (Guid bid in relationStoreBids)
                        {
                            if (bid == Guid.Empty) continue;
                            var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
                            if (!vpd.IsLoaded) continue;

                            PponDealStage pponDealStage = vpd.GetDealStage();
                            if (pponDealStage != PponDealStage.Running &&
                                pponDealStage != PponDealStage.RunningAndOn)
                            {
                                _pp.PponDealExtendAfterClose(bid);
                            }
                        }
                        #endregion 送出頁確時，更新SkmDealTimeSlot狀態
                        break;
                    case (int)SKMDealStatus.OnProduct:
                        status += 1;
                        break;
                }

                if (deal.IsHqDeal)
                {
                    foreach (var bid in bids)
                    {
                        rsc.ForEach(rs =>
                        {
                            rs.Bid = bid.Key;
                            rs.ModifyTime = now;
                        });
                    }
                }
                else
                {
                    foreach (var bid in bids)
                    {
                        var AllShoppe = rsc.Where(x => x.SellerGuid == bid.Value);
                        var Shoppe = rsc.Where(x => x.ParentSellerGuid == bid.Value);
                        if (AllShoppe.Any())
                        {
                            //全館
                            rsc.Where(x => x.SellerGuid == bid.Value).ForEach(
                                rs =>
                                {
                                    rs.Bid = bid.Key;
                                    rs.ModifyTime = now;
                                });
                        }
                        if (Shoppe.Any())
                        {
                            //非全館
                            rsc.Where(x => x.ParentSellerGuid == bid.Value).ForEach(
                                rs =>
                                {
                                    rs.Bid = bid.Key;
                                    rs.ModifyTime = now;
                                });
                        }
                    }
                }

                deal.Status = status;
                deal.ModifyId = username;
                deal.ModifyTime = now;
                _pp.ExternalDealSet(deal);
                _pp.ExternalDealRelationStoreCollectionSet(rsc);
                ExternalDealFacade.ClearViewExternalDealCaches(rsc.Select(t => t.Bid.GetValueOrDefault()));

                //預覽頁面按下確定時執行 
                if (status == (int)SKMDealStatus.WaitingConfirm)
                {
                    var ebids = rsc.Select(x => x.Bid).Distinct().ToList();

                    #region 送出頁確時，產生SkmDealTimeSlot
                    foreach (Guid ebid in ebids)
                    {
                        if (ebid != Guid.Empty)
                        {
                            PponDeal pdeal = _pp.PponDealGet(ebid);
                            //add (此時剛有bid, 草稿狀態還沒有bid  [草稿-> 預覽頁確定])
                            PponFacade.SkmDealTimeSlotSet(pdeal); //add
                        }
                    }
                    #endregion 送出頁確時，產生SkmDealTimeSlot

                    #region 送出頁確時，產生SkmBeaconMessage(商品訊息)
                    foreach (Guid ebid in ebids)
                    {
                        if (ebid != Guid.Empty)
                        {
                            PponDeal pdeal = _pp.PponDealGet(ebid);

                            //SkmBeaconMessage
                            int sid = 0; //TODO是否沒用到?
                            ViewExternalDealCollection veds = _pp.ViewExternalDealGetListByBid(ebid);
                            foreach (ViewExternalDeal ved in veds.Where(v => v.BeaconType != (int)BeaconType.None))
                            {
                                if (string.IsNullOrEmpty(ved.Floor))
                                {
                                    //全館，提品只會產生一筆資料，所以要自己產生
                                    var data = getFloor(ved.SellerGuid, ved.ShopCode);

                                    foreach (var d in data)
                                    {
                                        SkmBeaconMessage qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, d.Floor);
                                        if (qsbm == null || qsbm.Id == 0)
                                        {
                                            SkmBeaconMessage sbm = new SkmBeaconMessage();
                                            sbm.Subject = ved.BeaconMessage;
                                            sbm.BusinessHourGuid = ebid;
                                            sbm.EffectiveStart = pdeal.Deal.BusinessHourOrderTimeS;
                                            sbm.EffectiveEnd = pdeal.Deal.BusinessHourOrderTimeE;
                                            sbm.TimeStart = "00:00";
                                            sbm.TimeEnd = "23:59";
                                            sbm.IsTop = false;
                                            sbm.Status = (int)SkmBeaconStatus.Deal;
                                            sbm.Guid = ebid;
                                            sbm.BeaconType = (int)SkmBeaconType.Deal;
                                            sbm.ShopCode = ved.ShopCode;
                                            sbm.Floor = d.Floor;
                                            _skm.SkmBeaconMessageSet(sbm);

                                            qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, d.Floor);
                                            if (qsbm != null)
                                            {
                                                sid = qsbm.Id;
                                            }
                                            SkmFacade.SkmBeaconMessageTimeSlotInsert(qsbm, (int)SkmBeaconType.Deal, false);
                                        }
                                    }
                                }
                                else
                                {
                                    //有選擇館別，提品會產生多份資料
                                    SkmBeaconMessage qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, ved.Floor);
                                    if (qsbm == null || qsbm.Id == 0)
                                    {
                                        SkmBeaconMessage sbm = new SkmBeaconMessage();
                                        sbm.Subject = ved.BeaconMessage;
                                        sbm.BusinessHourGuid = ebid;
                                        sbm.EffectiveStart = pdeal.Deal.BusinessHourOrderTimeS;
                                        sbm.EffectiveEnd = pdeal.Deal.BusinessHourOrderTimeE;
                                        sbm.TimeStart = "00:00";
                                        sbm.TimeEnd = "23:59";
                                        sbm.IsTop = false;
                                        sbm.Status = (int)SkmBeaconStatus.Deal;
                                        sbm.Guid = ebid;
                                        sbm.BeaconType = (int)SkmBeaconType.Deal;
                                        sbm.ShopCode = ved.ShopCode;
                                        sbm.Floor = ved.Floor;
                                        _skm.SkmBeaconMessageSet(sbm);

                                        qsbm = _skm.SkmBeaconMessageGetByBid(ved.Bid ?? Guid.Empty, ved.ShopCode, ved.Floor);
                                        if (qsbm != null)
                                        {
                                            sid = qsbm.Id;
                                        }

                                        //SkmBeaconMessageTimeSlot
                                        SkmFacade.SkmBeaconMessageTimeSlotInsert(qsbm, (int)SkmBeaconType.Deal, false);
                                    }
                                }
                            }
                        }
                    }

                    #endregion 送出頁確時，產生SkmBeaconMessage
                }else if(status == (int)SKMDealStatus.Confirmed)
                {
                    List<ExternalDealCombo> skmComboDeals = isSkmPayVer ? _skmef.GetExternalDealCombo(deal.Guid).Where(item => !item.IsDel).ToList() : new List<ExternalDealCombo>();
                    foreach (var comboDeal in skmComboDeals)
                    {
                        logger.InfoFormat("skmComboDeal={0},{1}", comboDeal.ShopCode, comboDeal.Qty);
                    }
                    
                }
                logger.Info("GoNext-- status=" + status);
            }
            
            string back = string.Format("?filter=0&seller={0}&shop={1}&page=1", firstStore, firstShopCode);
            TempData[AliResult] = flag;
            TempData[AliMessage] = resultMsg;
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        /// <summary>
        /// 重新上架
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult OnShelf(string guid)
        {
            string resultMsg = "";
            string firstStore = "";
            string firstShopCode = "";
            Guid dealGuid;
            if (!Guid.TryParse(guid, out dealGuid) && TempData["entity"] == null)
            {
                return Json(new { Result = "Error", Message = "Bid格式錯誤，請檢查!" });
            }

            ViewExternalDealCollection deals = _pp.ViewExternalDealGetListByDealrGuid(dealGuid);
            bool flag = deals.Count > 0;
            if (flag)
            {
                ExternalDeal deal = _pp.ExternalDealGet(dealGuid);

                ExternalDealRelationStoreCollection rsc = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid);
                DateTime now = DateTime.Now;
                deal.ModifyTime = now;
                var baseDateTime = Convert.ToDateTime(string.Format("{0} {1}", Convert.ToDateTime(deal.BusinessHourDeliverTimeS).Date.ToString("yyyy/MM/dd"), Convert.ToDateTime(deal.SettlementTime).ToString("HH:mm:ss")));
                deal.SettlementTime = Convert.ToDateTime(baseDateTime > now ? string.Format("{0} {1}", DateTime.Today.Date.ToString("yyyy/MM/dd"), Convert.ToDateTime(deal.SettlementTime).ToString("HH:mm:ss")) : string.Format("{0} {1}", DateTime.Today.AddDays(1).Date.ToString("yyyy/MM/dd"), Convert.ToDateTime(deal.SettlementTime).ToString("HH:mm:ss")));
                _pp.ExternalDealSet(deal);

                /*
                * 重新上架時，把Bid加入SkmDealTimeSlot,DealTimeSlot
                */
                Guid bid = Guid.Empty;

                foreach (ExternalDealRelationStore edrs in rsc)
                {
                    edrs.IsDel = false;
                    bid = edrs.Bid ?? Guid.Empty;

                    BusinessHour bh = _pp.BusinessHourGet((Guid)edrs.Bid);
                    bh.BusinessHourDeliverTimeE = deal.BusinessHourDeliverTimeE;
                    bh.BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE;
                    bh.ModifyTime = deal.ModifyTime;
                    _pp.BusinessHourSet(bh);
                    _pp.ExternalDealRelationStoreCollectionSet(rsc);

                    PponDeal vpd = _pp.PponDealGet(bid);
                    PponFacade.SkmDealTimeSlotSet(vpd);
                    //SkmDealTimeSlot Copy 到DealTimeSlot
                    SkmDealTimeSlotCollection sdts = _skm.SkmDealTimeSlotGetByBid(bid);

                    var cityId = sdts.Select(x => x.CityId).First();
                    var sequence = sdts.Select(x => x.Sequence).First();
                    var daySpan = new TimeSpan(deal.BusinessHourOrderTimeE.Ticks - DateTime.Today.Ticks).Days;


                    for (var i = 0; i < daySpan; i++)
                    {
                        var j = i + 1;
                        if (!_pp.DealTimeSlotGet(bid, cityId, DateTime.Today.AddDays(i).AddHours(12)).IsLoaded)
                        {
                            DealTimeSlot dt = new DealTimeSlot
                            {
                                BusinessHourGuid = bid,
                                CityId = cityId,
                                Sequence = sequence,
                                EffectiveStart = DateTime.Today.AddDays(i).Date.AddHours(12),
                                EffectiveEnd = i == daySpan - 1 ? deal.BusinessHourOrderTimeE : DateTime.Today.AddDays(j).AddHours(12),
                                Status = (int)DealTimeSlotStatus.Default
                            };
                            _pp.DealTimeSlotSet(dt);
                        }
                    }
                    foreach (SkmDealTimeSlot sdt in sdts)
                    {
                        if (sdt.EffectiveStart >= DateTime.Today)
                        {
                            sdt.Status = (int)DealTimeSlotStatus.Default;
                            _skm.SkmDealTimeSlotSet(sdt);
                        }
                    }
                    //如果結檔後延長 "結檔日期" , 自動執行 "復原結檔"
                    _pp.PponDealExtendAfterClose(bid);
                }

            }

            string back = string.Format("?filter=0&seller={0}&shop={1}&page=1", firstStore, firstShopCode);
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        /// <summary>
        /// 下架
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public ActionResult OffShelf(string guid)
        {
            string resultMsg = "";
            string firstStore = "";
            string firstShopCode = "";
            Guid dealGuid;
            TempData[AliResult] = true;
            TempData[AliMessage] = "";
            if (!Guid.TryParse(guid, out dealGuid) && TempData["entity"] == null)
            {
                TempData[AliResult] = false;
                TempData[AliMessage] = "Bid格式錯誤，請檢查!";
                return Json(new { Result = "Error", Message = "Bid格式錯誤，請檢查!" });
            }

            ViewExternalDealCollection deals = _pp.ViewExternalDealGetListByDealrGuid(dealGuid);
            bool flag = deals.Count > 0;
            if (flag)
            {
                ExternalDeal deal = _pp.ExternalDealGet(dealGuid);

                ExternalDealRelationStoreCollection rsc = _pp.ExternalDealRelationStoreGetListByDeal(dealGuid);
                DateTime updateDay = DateTime.Today.AddDays(-1);
                DateTime now = DateTime.Now;
                updateDay = updateDay.AddHours(12);
                deal.BusinessHourDeliverTimeE = updateDay;
                deal.BusinessHourOrderTimeE = updateDay;
                deal.ModifyTime = now;
                _pp.ExternalDealSet(deal);

                /*
                * 下架時，把多餘的Bid刪除從SkmDealTimeSlot,DealTimeSlot
                */
                foreach (var item in rsc)
                {
                    if (item.Bid != null)
                    {
                        item.IsDel = true;
                        item.ModifyTime = now;
                        var dels = _skm.SkmDealTimeSlotGetByBid(item.Bid ?? Guid.Empty).Where(x => x.EffectiveStart >= DateTime.Today).ToList();
                        foreach (var del in dels)
                        {
                            _skm.SkmDealTimeSlotDelete(del.Id);
                        }
                        _pp.ExternalDealRelationStoreCollectionSet(rsc);
                        _pp.DealTimeSlotDeleteByToday((Guid)item.Bid);
                        BusinessHour bh = _pp.BusinessHourGet((Guid)item.Bid);
                        bh.BusinessHourDeliverTimeE = updateDay;
                        bh.BusinessHourOrderTimeE = updateDay;
                        bh.ModifyTime = now;
                        _pp.BusinessHourSet(bh);
                    }
                }
            }

            string back = string.Format("?filter=0&seller={0}&shop={1}&page=1", firstStore, firstShopCode);
            TempData[AliResult] = flag;
            TempData[AliMessage] = resultMsg;
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        /// <summary>
        /// 取得指定檔次編號的數量資訊(實際訂購數量、兌換數量、庫存數，取消憑證數量)
        /// </summary>
        /// <param name="guid">檔次編號</param>
        /// <returns>檔次數量資訊</returns>
        [HttpPost]
        public ActionResult GetExternalQuantity(string guid)
        {
            Guid dealGuid;
            if (!Guid.TryParse(guid, out dealGuid))
            {
                return Json(new { Result = false, Message = "Bid格式錯誤，請檢查!" });
            }
            List<ViewSkmDealGuidQuantity> viewSkmDealGuidQuantityInfos = _skmef.GetExternalQuantityByGuid(dealGuid);
            List<SKMListExternalQuantityViewModel> sKMListExternalQuantityViewModelinfos = new List<SKMListExternalQuantityViewModel>();
            foreach (var item in viewSkmDealGuidQuantityInfos.OrderByDescending(p => p.ShopCode))
            {
                sKMListExternalQuantityViewModelinfos.Add(new SKMListExternalQuantityViewModel()
                {
                    ShopName = item.ShopName,
                    ShopCode = item.ShopCode,
                    //分店Guid
                    SellerGuid = item.SellerGuid,
                    //上架數量(建檔憑證數量)
                    OrderTotalLimit = item.OrderTotalLimit,
                    ExternalDealGuid = item.ExternalDealGuid,
                    //訂購數量(實際購買)
                    OrderedQuantity = item.OrderedQuantity,
                    //已取貨數量(已核銷)
                    VerifiedQuantity = item.VerifiedQuantity,
                    //未取貨數量(未核銷)
                    NotVerifiedQuantity = item.OrderedQuantity - item.VerifiedQuantity - (item.CanceledQuantity + item.ReturnedQuantity),
                    //取消數量(未核銷退貨)
                    CanceledNotVerifiedQuantity = item.CanceledQuantity,
                    //退貨數量(已核銷退貨)
                    CanceledVerifiedQuantity = item.ReturnedQuantity,
                    //剩餘數量
                    RemainingQuantity = (int)item.OrderTotalLimit - item.OrderedQuantity + item.CanceledQuantity + item.ReturnedQuantity

                });
            }
            return Json(new { Result = true, Message = "", Data = new JsonSerializer().Serialize(sKMListExternalQuantityViewModelinfos) });
        }

        #endregion Page using api

        #endregion 檔次列表

        #region Exhibition 策展

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public JsonResult QueryExhibitionCode(string code)
        {
            var result = _ep.GetExhibitionCode(code);
            if (result == null)
            {
                return Json(new { Success = false, Code = string.Empty, Name = string.Empty });
            }
            return Json(new { Success = true, Code = result.Id, Name = result.ExhibitionName });
        }

        [HttpPost]
        [Authorize(Roles = "SkmMarketing, SkmProducter, Administrator, ME2O, Production")]
        public JsonResult AddExhibitionDealCode(string eg, string code)
        {
            Guid dealGuid;
            long exhCode;

            if (!Guid.TryParse(eg, out dealGuid) || !long.TryParse(code, out exhCode))
            {
                return Json(new { Success = false, Id = 0, ExhibitionCodeId = 0, ExhibitionName = string.Empty });
            }

            var result = _cep.GetViewExternalDealExhibitionCode(dealGuid, exhCode);
            if (result.Any())
            {
                return Json(new { Success = false, Id = 0, ExhibitionCodeId = 0, ExhibitionName = string.Empty });
            }

            if (_cep.SetExternalDealExhibitionCode(dealGuid, exhCode))
            {
                result = _cep.GetViewExternalDealExhibitionCode(dealGuid, exhCode);
                if (result.Any())
                {
                    return Json(new { Success = true, Id = result.First().Id, ExhibitionCodeId = result.First().ExhibitionCodeId, ExhibitionName = result.First().ExhibitionName });
                }
            }

            return Json(new { Success = false, Id = 0, ExhibitionCodeId = 0, ExhibitionName = string.Empty });
        }

        #endregion Exhibition 策展

        #region 檔次商品分類

        /// <summary>
        /// 檔次排序
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DealCategoryReOrder(string data)
        {
            List<int> idList = JsonConvert.DeserializeObject<List<int>>(data);
            var categoryList = _sp.CategoryGetList(idList);
            foreach (var c in categoryList)
            {
                c.Rank = Array.IndexOf(idList.ToArray(), c.Id) + 1;
            }

            bool isSuccess = false;
            var successCount = _sp.CategorySetList(categoryList);
            if (successCount == idList.Count)
            {
                isSuccess = true;
            }
            return Json(new { isSuccess });
        }

        /// <summary>
        /// 檔次排序
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DealSubCategoryReOrder(string data, int parentId)
        {
            List<int> idList = JsonConvert.DeserializeObject<List<int>>(data);
            var categoryDependencyList = _sp.CategoryDependencyByRegionGet(parentId);
            foreach (var cdp in categoryDependencyList)
            {
                cdp.Seq = Array.IndexOf(idList.ToArray(), cdp.CategoryId) + 1;
            }

            bool isSuccess = false;
            var successCount = _sp.CategoryDependencySetList(categoryDependencyList);
            if (successCount == idList.Count)
            {
                isSuccess = true;
            }
            return Json(new { isSuccess });
        }
        #region  主分類

        /// <summary>
        /// 商品分類列表
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        public ActionResult DealCategoryList(int page = 1)
        {
            var mCategoryCol = SkmFacade.GetBelongSkmCategoryList().OrderBy(p => p.Rank).ThenBy(p => p.Id).ToList();
            return View(mCategoryCol);
        }

        /// <summary>
        /// 商品分類新增
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        public ActionResult DealCategoryAdd(int id = 0)
        {
            DealCategoryModel data = new DealCategoryModel();
            if (id == 0)
            {
                data.MainCategory = new Category();
            }
            else
            {
                data.MainCategory = _sp.CategoryGet(id);
                var dependencyCol = _sp.CategoryDependencyByRegionGet(id);

                List<string> filters = new List<string>();
                filters.Add(string.Format("{0} = {1}", Category.Columns.Status, (int)CategoryStatus.Enabled));
                filters.Add(string.Format("{0} = {1}", Category.Columns.Type, (int)CategoryType.DealCategory));

                //手動將舊有分類隱藏_is_show_back_end=false
                filters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                if (dependencyCol.Any())
                {
                    filters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dependencyCol.Select(p => p.CategoryId).ToList())));
                    var subCategoryCol = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), filters.ToArray());
                    data.SubCategoryList = subCategoryCol.ToList();
                }
                else
                {
                    data.SubCategoryList = new List<Category>() { };
                }
            }
            return View(data);
        }

        /// <summary>
        /// 商品分類新增 post
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        [HttpPost]
        public ActionResult DealCategoryAdd(Category data)
        {
            if (data.Id == 0) //create
            {
                data.CreateId = User.Identity.Name;
                data.IsShowBackEnd = true;
                data.IsShowFrontEnd = false;//預設下架
                data.CreateTime = DateTime.Now;
                data.IsFinal = false;
                data.Status = (int)CategoryStatus.Enabled;
                data.Type = (int)CategoryType.DealCategory;

                //修改為所有category的max，而不是同type的max
                data.Code = _sp.GetCategoryMaxCode() + 1;

                //category 的 id並非  identity insert, 所以得自己取max(id)
                var maxId = _sp.GetCategoryMaxId();
                data.Id = maxId + 1;

                //計算rank / isfinal=子分類
                data.Rank = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                                .Where(p => p.IsFinal == false && p.CategoryType == (int)CategoryType.DealCategory
                                          && p.IsShowBackEnd == true).ToList().Count + 1;

                var categorySetSuccess = _sp.CategorySet(data);
                var depSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                {
                    ParentId = _config.SkmCategordId,
                    CategoryId = data.Id,
                });

                if (categorySetSuccess > 0 && depSetSuccess > 0)
                {
                    return RedirectToAction("DealCategoryList");
                }
                else
                {
                    return View(data);
                }
            }
            else //update
            {
                var category = _sp.CategoryGet(data.Id);
                category.Name = data.Name;
                category.ModifyId = User.Identity.Name;
                category.ModifyTime = DateTime.Now;

                if (_sp.CategorySet(category) > 0)
                {
                    return RedirectToAction("DealCategoryList");
                }
                else
                {
                    return View("DealCategoryAdd", data);
                }
            }
        }

        /// <summary>
        /// 上/下架主分類
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DealCategoryHide(int id)
        {
            var category = _sp.CategoryGet(id);
            if (category.IsShowFrontEnd)
            {
                category.IsShowFrontEnd = false;
            }
            else
            {
                category.IsShowFrontEnd = true;
            }
            category.ModifyId = User.Identity.Name;
            category.ModifyTime = DateTime.Now;

            if (_sp.CategorySet(category) > 0)
            {
                //修改子分類讓其下架
                var dependencyCol = _sp.CategoryDependencyByRegionGet(id);
                if (dependencyCol.Any())
                {
                    List<string> filters = new List<string>();
                    filters.Add(string.Format("{0} = {1}", Category.Columns.Status, (int)CategoryStatus.Enabled));
                    filters.Add(string.Format("{0} = {1}", Category.Columns.Type, (int)CategoryType.DealCategory));
                    //手動將舊有分類隱藏_is_show_back_end=false
                    filters.Add(string.Format("{0} = {1}", Category.Columns.IsShowBackEnd, true));
                    filters.Add(string.Format("{0} in ({1})", Category.Columns.Id, string.Join(",", dependencyCol.Select(p => p.CategoryId).ToList())));
                    var subCategoryCol = _sp.CategoryGetList(0, 0, Category.Columns.Rank.ToString(), filters.ToArray());
                    foreach (var sc in subCategoryCol)
                    {
                        if (sc.IsShowFrontEnd) //上下架
                        {
                            sc.IsShowFrontEnd = false;
                        }
                        else
                        {
                            sc.IsShowFrontEnd = true;
                        }
                        sc.ModifyId = User.Identity.Name;
                        sc.ModifyTime = DateTime.Now;
                    }
                    var successCount = _sp.CategorySetList(subCategoryCol);
                }
            }
            return RedirectToAction("DealCategoryList");

        }

        /// <summary>
        /// 商品分類新增 API(阿里
        /// </summary>
        /// <returns></returns>
        public void CreateDealCategotyApi (Category data) {
            if (data.Id == 0) {
                data.CreateId = User.Identity.Name;
                data.IsShowBackEnd = true;
                data.IsShowFrontEnd = false;//預設下架
                data.CreateTime = DateTime.Now;
                data.IsFinal = false;
                data.Status = (int)CategoryStatus.Enabled;
                data.Type = (int)CategoryType.DealCategory;

                //修改為所有category的max，而不是同type的max
                data.Code = _sp.GetCategoryMaxCode() + 1;

                //category 的 id並非  identity insert, 所以得自己取max(id)
                var maxId = _sp.GetCategoryMaxId();
                data.Id = maxId + 1;

                //計算rank / isfinal=子分類
                data.Rank = _sp.ViewCategoryDependencyGetByParentId(_config.SkmCategordId)
                                .Where(p => p.IsFinal == false && p.CategoryType == (int)CategoryType.DealCategory && p.IsShowBackEnd == true)
                                .ToList().Count + 1;

                var categorySetSuccess = _sp.CategorySet(data);
                var depSetSuccess = _sp.CategoryDependencySet(new CategoryDependency() {
                    ParentId = _config.SkmCategordId ,
                    CategoryId = data.Id ,
                });

                if (categorySetSuccess > 0 && depSetSuccess > 0) {

                }
            } else {
                var category = _sp.CategoryGet(data.Id);
                category.Name = data.Name;
                category.ModifyId = User.Identity.Name;
                category.ModifyTime = DateTime.Now;
                if (_sp.CategorySet(category) > 0) {

                } else {

                }
            }
        }

        /// <summary>
        /// 上/下架主分類 API(阿里
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public void DealCategoryHideApi (int id) {
            var category = _sp.CategoryGet(id);
            if (category.IsShowFrontEnd) {
                category.IsShowFrontEnd = false;
            } else {
                category.IsShowFrontEnd = true;
            }
            category.ModifyId = User.Identity.Name;
            category.ModifyTime = DateTime.Now;

            if (_sp.CategorySet(category) > 0) {
                //修改子分類讓其下架
                var dependencyCol = _sp.CategoryDependencyByRegionGet(id);
                if (dependencyCol.Any()) {
                    List<string> filters = new List<string>();
                    filters.Add(string.Format("{0} = {1}" , Category.Columns.Status , (int)CategoryStatus.Enabled));
                    filters.Add(string.Format("{0} = {1}" , Category.Columns.Type , (int)CategoryType.DealCategory));
                    //手動將舊有分類隱藏_is_show_back_end=false
                    filters.Add(string.Format("{0} = {1}" , Category.Columns.IsShowBackEnd , true));
                    filters.Add(string.Format("{0} in ({1})" , Category.Columns.Id , string.Join("," , dependencyCol.Select(p => p.CategoryId).ToList())));
                    var subCategoryCol = _sp.CategoryGetList(0 , 0 , Category.Columns.Rank.ToString() , filters.ToArray());
                    foreach (var sc in subCategoryCol) {
                        if (sc.IsShowFrontEnd) //上下架
                        {
                            sc.IsShowFrontEnd = false;
                        } else {
                            sc.IsShowFrontEnd = true;
                        }
                        sc.ModifyId = User.Identity.Name;
                        sc.ModifyTime = DateTime.Now;
                    }
                    var successCount = _sp.CategorySetList(subCategoryCol);
                }
            }
        }

        #endregion

        #region 子分類

        /// <summary>
        /// 商品子分類新增
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        public ActionResult DealSubCategoryAdd(int parentId, int id = 0)
        {
            DealCategoryModel data = new DealCategoryModel();
            data.MainCategory = _sp.CategoryGet(parentId);

            if (id == 0)
            {
                data.SubCategory = new Category();
            }
            else
            {
                data.SubCategory = _sp.CategoryGet(id);
            }

            return View(data);
        }

        /// <summary>
        /// 商品分類新增 post
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmProducter, Administrator, ME2O, Production")]
        [HttpPost]
        public ActionResult DealSubCategoryAdd(DealCategoryModel data)
        {
            if (data.SubCategory.Id == 0) //create
            {
                data.SubCategory.CreateId = User.Identity.Name;
                data.SubCategory.IsShowBackEnd = true;
                data.SubCategory.IsShowFrontEnd = false;//預設下架
                data.SubCategory.CreateTime = DateTime.Now;
                data.SubCategory.IsFinal = true;
                data.SubCategory.Status = (int)CategoryStatus.Enabled;
                data.SubCategory.Type = (int)CategoryType.DealCategory;
                //修改為所有category的max，而不是同type的max
                data.SubCategory.Code = _sp.GetCategoryMaxCode() + 1;

                //category 的 id並非  identity insert, 所以得自己取max(id)
                var maxId = _sp.GetCategoryMaxId();
                data.SubCategory.Id = maxId + 1;

                data.SubCategory.Rank = _sp.CategoryDependencyByRegionGet(data.MainCategory.Id).Count + 1;

                // 1. save 本身 category
                var mainSetSuccess = _sp.CategorySet(data.SubCategory);
                // 1. 依附_config.SkmCategordId
                var depSkmSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                {
                    ParentId = _config.SkmCategordId,
                    CategoryId = data.SubCategory.Id,
                });
                // 2. 依附 parent_id
                var depSetSuccess = _sp.CategoryDependencySet(new CategoryDependency()
                {
                    ParentId = data.MainCategory.Id,
                    CategoryId = data.SubCategory.Id,
                });

                if (mainSetSuccess > 0 && depSetSuccess > 0 && depSkmSetSuccess > 0)
                {
                    return RedirectToAction("DealCategoryAdd", new { id = data.MainCategory.Id });
                }
                else
                {
                    return View(data);
                }
            }
            else //update
            {
                var category = _sp.CategoryGet(data.SubCategory.Id);
                category.Name = data.SubCategory.Name;
                category.ModifyId = User.Identity.Name;
                category.ModifyTime = DateTime.Now;

                if (_sp.CategorySet(category) > 0)
                {
                    return RedirectToAction("DealCategoryAdd", new { id = data.MainCategory.Id });
                }
                else
                {
                    return View("DealCategoryAdd", data);
                }
            }
        }

        /// <summary>
        /// 上/下架子分類
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult DealSubCategoryHide(int parentId, int id)
        {
            var category = _sp.CategoryGet(id);
            if (category.IsShowFrontEnd)
            {
                category.IsShowFrontEnd = false;
            }
            else
            {
                category.IsShowFrontEnd = true;
            }
            category.ModifyId = User.Identity.Name;
            category.ModifyTime = DateTime.Now;
            _sp.CategorySet(category);
            return RedirectToAction("DealCategoryAdd", new { id = parentId });

        }

        #endregion

        #endregion

        #region 策展標籤

        /// <summary>
        /// 策展標籤管理
        /// </summary>
        /// <param name="nowPage">目前頁數</param>
        /// <returns>策展標籤管理回應資訊</returns>
        [Authorize(Roles = "SkmDealTag, Administrator, ME2O, Production")]
        public ActionResult ManageTagType(int nowPage = 1)
        {
            var extermalDealTags = _cep.GetExternalDealIcons();

            PageInformation pageInfo = new PageInformation()
            {
                NowPage = nowPage,
                pageSize = 10,
                TotalPageCount = ((extermalDealTags.Count() - 1) / 10) + 1,//-1因為第十筆的時候不會增加一頁
                TotalDataCount = extermalDealTags.Count()
            };
            if (pageInfo.NowPage <= 1)
            {
                pageInfo.NowPage = 1;
            }
            if (pageInfo.NowPage >= pageInfo.TotalPageCount)
            {
                pageInfo.NowPage = pageInfo.TotalPageCount;
            }
            ManageTagTypeResponse response = new ManageTagTypeResponse()
            {
                ExternalDealTags = extermalDealTags.Skip((pageInfo.NowPage - 1) * pageInfo.pageSize).Take(pageInfo.pageSize).ToList(),
                IconStyles = _sysp.SystemDataGet("ExternalDealIconStyle").Data,
                PageInfo = pageInfo
            };
            return View(response);
        }

        [HttpPost]
        public ActionResult AddTag(AddTagRequest request)
        {
            string validateMessage = string.Empty;
            if (string.IsNullOrEmpty(request.TagName))
            {
                validateMessage += "請輸入標籤名稱" + Environment.NewLine;
            }
            if (request.StyleID <= 0)
            {
                validateMessage += "請選擇標籤樣式" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(validateMessage))
            {
                TempData["ReturnMessage"] = validateMessage;
                return RedirectToAction("ManageTagType");
            }

            ExternalDealIcon externalDealIcon = new ExternalDealIcon()
            {
                CreateTime = DateTime.Now,
                StyleId = request.StyleID,
                IconName = request.TagName
            };
            if (!_cep.InsertExternalDealIcon(externalDealIcon))
            {
                TempData["ReturnMessage"] = "新增標籤資料失敗";
            }
            return RedirectToAction("ManageTagType");
        }

        [HttpPost]
        public ActionResult EditTag(EditTagRequest request)
        {
            string validateMessage = string.Empty;
            if (string.IsNullOrEmpty(request.TagName))
            {
                validateMessage += "請輸入標籤名稱" + Environment.NewLine;
            }
            if (request.StyleID <= 0)
            {
                validateMessage += "請選擇標籤樣式" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(validateMessage))
            {
                TempData["ReturnMessage"] = validateMessage;
                return RedirectToAction("ManageTagType");
            }

            ExternalDealIcon externalDealIcon = new ExternalDealIcon()
            {
                Id = request.TagID,
                StyleId = request.StyleID,
                IconName = request.TagName,
                CreateTime = DateTime.Now
            };
            if (!_cep.SetExternalDealIcon(externalDealIcon))
            {
                TempData["ReturnMessage"] = "編輯標籤資料失敗";
            }
            return RedirectToAction("ManageTagType");
        }

        [HttpPost]
        public ActionResult DeleteTag(int tagID)
        {
            string validateMessage = string.Empty;
            if (tagID <= 0)
            {
                validateMessage += "請選擇要刪除的標籤資料" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(validateMessage))
            {
                TempData["ReturnMessage"] = validateMessage;
                return RedirectToAction("ManageTagType");
            }

            ExternalDealIcon externalDealIcon = new ExternalDealIcon()
            {
                Id = tagID,
            };
            if (!_cep.DeleteExternalDealIcon(externalDealIcon))
            {
                TempData["ReturnMessage"] = "刪除標籤資料失敗";
            }
            return RedirectToAction("ManageTagType");
        }
        #endregion

        #region 策展代號

        /// <summary>
        /// 策展代號管理
        /// </summary>
        /// <param name="nowPage">目前頁數</param>
        /// <returns>策展代號管理回應資訊</returns>
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionCode(int nowPage = 1)
        {
            SetPageDefault();
            List<ExhibitionCode> exhibitionCodeInfos = new List<ExhibitionCode>();
            if (SkmFacade.CheckSkmHqPower((int)User.Identity.GetPropertyValue("Id")))
            {
                List<string> selllerStoreGuids = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.ToList();
                selllerStoreGuids.Insert(0, _config.SkmRootSellerGuid.ToString());//將總公司加進去
                exhibitionCodeInfos = _ep.GetExhibitionCodeList(selllerStoreGuids);
            }
            else
            {
                string firstSelllerStoreGuid = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.First();
                exhibitionCodeInfos = _ep.GetExhibitionCodeList(new List<string>() { _config.SkmRootSellerGuid.ToString(), firstSelllerStoreGuid });
            }

            PageInformation pageInfo = new PageInformation()
            {
                NowPage = nowPage,
                pageSize = 10,
                TotalPageCount = ((exhibitionCodeInfos.Count() - 1) / 10) + 1,//-1因為第十筆的時候不會增加一頁
                TotalDataCount = exhibitionCodeInfos.Count()
            };
            if (pageInfo.NowPage <= 1)
            {
                pageInfo.NowPage = 1;
            }
            if (pageInfo.NowPage >= pageInfo.TotalPageCount)
            {
                pageInfo.NowPage = pageInfo.TotalPageCount;
            }

            return View(new ExhibitionCodeResponse()
            {
                ExhibitionCodeInfos = exhibitionCodeInfos.Skip((pageInfo.NowPage - 1) * pageInfo.pageSize).Take(pageInfo.pageSize).ToList(),
                PageInfo = pageInfo
            });
        }

        /// <summary>
        /// 新增策展代號
        /// </summary>
        /// <param name="request">策展代號資訊</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult AddExhibitionCode(ExhibitionCode request)
        {
            //驗證
            string validateMessage = string.Empty;
            if (string.IsNullOrEmpty(request.ExhibitionName))
            {
                validateMessage += "請輸入策展名稱" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(validateMessage))
            {
                TempData["ReturnMessage"] = validateMessage;
                return RedirectToAction("ExhibitionCode");
            }
            //取得seller資訊供新增資料
            string sellerGuid = string.Empty;
            if (SkmFacade.CheckSkmHqPower((int)User.Identity.GetPropertyValue("Id")))
            {
                sellerGuid = _config.SkmRootSellerGuid.ToString();
            }
            else
            {
                SetPageDefault();//為了取得ViewBag.SKMStores，但執行的有點多東西
                sellerGuid = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.First();
            }
            Seller seller = _sp.SellerGet(Guid.Parse(sellerGuid));
            request.SellerGuid = seller.Guid;
            request.SellerName = seller.SellerName;
            //新增策展代號
            if (!_ep.InsertExhibitionCode(request))
            {
                TempData["ReturnMessage"] = "新增策展代號資料失敗";
            }
            return RedirectToAction("ExhibitionCode");
        }

        /// <summary>
        /// 新增策展代號
        /// </summary>
        /// <param name="request">策展代號資訊</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult EditExhibitionCode(ExhibitionCode request)
        {
            string validateMessage = string.Empty;
            if (string.IsNullOrEmpty(request.SellerName))
            {
                validateMessage += "請輸入策展名稱" + Environment.NewLine;
            }
            if (!string.IsNullOrEmpty(validateMessage))
            {
                TempData["ReturnMessage"] = validateMessage;
                return RedirectToAction("ExhibitionCode");
            }
            request.CreateTime = DateTime.Now;
            if (!_ep.SetExhibitionCode(request))
            {
                TempData["ReturnMessage"] = "編輯策展代號資料失敗";
            }
            return RedirectToAction("ExhibitionCode");
        }

        /// <summary>
        /// 新增策展代號
        /// </summary>
        /// <param name="request">策展代號資訊</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult DeleteExhibitionCode(long id)
        {
            string validateMessage = string.Empty;
            if (id <= 0)
            {
                TempData["ReturnMessage"] = "請選擇要刪除的策展資料";
                return RedirectToAction("ExhibitionCode");
            }
            if (_cep.ChechExhibitionCodeIsUsed(id))
            {
                TempData["ReturnMessage"] = "策展代號使用中，無法刪除";
                return RedirectToAction("ExhibitionCode");
            }

            if (!_ep.DeleteExhibitionCode(new ExhibitionCode() { Id = id }))
            {
                TempData["ReturnMessage"] = "新增策展代號資料失敗";
            }
            return RedirectToAction("ExhibitionCode");
        }
        #endregion

        #region 策展建檔

        /// <summary>
        /// 策展管理
        /// </summary>
        /// <param name="request">策展清單請求資訊</param>
        /// <returns></returns>
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionEvent(ExhibitionEventRequest request)
        {
            if (string.IsNullOrEmpty(request.SellerGuid))
            {
                if (TempData["ExhibitionEvent_SellerGuid"] != null)
                {
                    request.SellerGuid = TempData["ExhibitionEvent_SellerGuid"].ToString();
                    //再把目前選擇的項目放回去TempData
                    TempData["ExhibitionEvent_SellerGuid"] = request.SellerGuid;
                }
            }
            else
            {
                TempData["ExhibitionEvent_SellerGuid"] = request.SellerGuid;
            }
            Guid sellerGuid;
            SetPageDefault();//為了取得ViewBag.SKMStores，但執行的有點多東西
            var SKMStores = (Dictionary<string, string>)ViewBag.SKMStores;
            if (string.IsNullOrEmpty(request.SellerGuid))
            {
                if (SKMStores.Keys.Contains(DefaultSeller))
                {
                    sellerGuid = Guid.Parse(DefaultSeller);
                }
                else
                {
                    sellerGuid = Guid.Parse(SKMStores.First().Key);
                }
            }
            else
            {
                //若查詢沒有權限的館代號，則顯示第一個館的館代號
                if (!SKMStores.Keys.Contains(request.SellerGuid))
                {
                    sellerGuid = Guid.Parse(SKMStores.First().Key);
                }
                else
                {
                    sellerGuid = Guid.Parse(request.SellerGuid);
                }
            }

            List<ExhibitionEvent> exhibitionEvents = _ep.GetExhibitionEventsBySellerGuid(sellerGuid).OrderByDescending(p => p.EndDate).ToList();

            PageInformation pageInfo = new PageInformation()
            {
                NowPage = request.NowPage,
                pageSize = 10,
                TotalPageCount = ((exhibitionEvents.Count() - 1) / 10) + 1,//-1因為第十筆的時候不會增加一頁
                TotalDataCount = exhibitionEvents.Count()
            };

            //分頁
            exhibitionEvents = exhibitionEvents.Skip((pageInfo.NowPage - 1) * pageInfo.pageSize).Take(pageInfo.pageSize)
                .ToList();

            //取得有分群的策展
            var haveUserGroupExhibitionEventIds = _ep.GetHaveUserGroupExhibitionEventIds(exhibitionEvents.Select(p => p.Id).ToList());

            if (pageInfo.NowPage <= 1)
            {
                pageInfo.NowPage = 1;
            }
            if (pageInfo.NowPage >= pageInfo.TotalPageCount)
            {
                pageInfo.NowPage = pageInfo.TotalPageCount;
            }

            List<SelectListItem> selectList = SKMStores.Select(item => new SelectListItem()
            {
                Text = item.Value,
                Value = item.Key,
                Selected = item.Key == sellerGuid.ToString()
            }
            ).ToList();

            return View(new ExhibitionEventResponse()
            {
                ExhibitionEventInfos = exhibitionEvents,
                HaveUserGroupExhibitionEventIds = haveUserGroupExhibitionEventIds,
                Sellers = selectList,
                PageInfo = pageInfo,
            });
        }

        /// <summary>
        /// 策展Event排序
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult SortExhibitionEvent(string storeGuid = null)
        {
            Guid sellerGuid;
            SetPageDefault();//為了取得ViewBag.SKMStores，但執行的有點多東西
            var SKMStores = (Dictionary<string, string>)ViewBag.SKMStores;
            if (string.IsNullOrEmpty(storeGuid))
            {
                if (SKMStores.Keys.Contains(DefaultSeller))
                {
                    sellerGuid = Guid.Parse(DefaultSeller);
                }
                else
                {
                    sellerGuid = Guid.Parse(SKMStores.First().Key);
                }
            }
            else
            {
                //若查詢沒有權限的館代號，則顯示第一個館的館代號
                if (!SKMStores.Keys.Contains(storeGuid))
                {
                    sellerGuid = Guid.Parse(SKMStores.First().Key);
                }
                else
                {
                    sellerGuid = Guid.Parse(storeGuid);
                }
            }

            IEnumerable<ExhibitionEvent> exhibitionEvents = _ep.GetPublishExhibitionEventsBySellerGuid(sellerGuid).OrderBy(item => item.Seq);
            ViewBag.SelectStore = sellerGuid.ToString();
            return View(exhibitionEvents);
        }

        /// <summary>
        /// 調整策展排序
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult SortExhibitionEvent(List<SortExhibitionEventRequest> request, string storeGuid)
        {
            _ep.SetExhibitionEventStatus(request.ToDictionary(item => item.Id, item => item.Sort));
            return Json(new { ResultCode = 0001 });
        }

        /// <summary>
        /// 更改策展狀態(上架)
        /// </summary>
        /// <param name="id">策展編號</param>
        /// <returns>更改結果</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult PublishExternalEvent(int id)
        {
            ExhibitionEvent exhibitionEvent = _ep.GetExhibitionEvent(id);
            if (exhibitionEvent.Id != id)
            {
                return Json(new { ResultCode = 1003, ResultMessage = "無此策展" });
            }
            if (exhibitionEvent.Status != (byte)SkmExhibitionEventStatus.Draft &&
                exhibitionEvent.Status != (byte)SkmExhibitionEventStatus.Unpublished)
            {
                return Json(new { ResultCode = 1004, ResultMessage = "此策展無法更改狀態" });
            }
            exhibitionEvent.Status = (byte)SkmExhibitionEventStatus.Published;
            if (_ep.SetExhibitionEvent(exhibitionEvent))
                return Json(new { ResultCode = 0001 });
            else
                return Json(new { ResultCode = 1005, ResultMessage = "更改策展狀態失敗" });
        }

        /// <summary>
        /// 更改策展狀態(下架)
        /// </summary>
        /// <param name="id">策展編號</param>
        /// <returns>更改結果</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult UnpublishExternalEvent(int id)
        {
            ExhibitionEvent exhibitionEvent = _ep.GetExhibitionEvent(id);
            if (exhibitionEvent.Id != id)
            {
                return Json(new { ResultCode = 1003, ResultMessage = "無此策展" });
            }
            if (exhibitionEvent.Status != (byte)SkmExhibitionEventStatus.Published)
            {
                return Json(new { ResultCode = 1004, ResultMessage = "此策展無法更改狀態" });
            }
            exhibitionEvent.Status = (byte)SkmExhibitionEventStatus.Unpublished;
            if (_ep.SetExhibitionEvent(exhibitionEvent))
                return Json(new { ResultCode = 0001 });
            else
                return Json(new { ResultCode = 1005, ResultMessage = "更改策展狀態失敗" });
        }

        /// <summary>
        /// 新增策展
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult AddExhibitionEvent()
        {
            SetPageDefault();
            List<string> selllerStoreGuids = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.ToList();
            selllerStoreGuids.Insert(0, _config.SkmRootSellerGuid.ToString());//將總公司加進去
            ViewBag.ExhibitionCodes = _ep.GetExhibitionCodeList(selllerStoreGuids);
            return View(
                new SetExhibitionEvent()
                {
                    Store = new Guid[] { },
                    RecommendSkmCardLevels = new List<SkmCardLevelType>().ToArray(),
                    IsSkmHqPower = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name))
                });
        }

        /// <summary>
        /// 新增策展
        /// </summary>
        /// <param name="request">設定策展資訊</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult AddExhibitionEvent(SetExhibitionEvent request)
        {
            //為了抓為什麼沒有辦法新增策展
            try
            {
                #region 驗證
                string preFixName = "SkmExhibitionEvent";
                ImgUploadData imgUploadData = SkmFacade.CheckImgType(750, 470, "SkmExhibitionEvent", Guid.NewGuid());
                List<string> errorMessage = ValidateExhibitionEventSetInfo(request, preFixName);

                if (errorMessage.Count() > 0)
                {
                    return Json(new { ResultCode = 1002, ResultMessage = String.Join(Environment.NewLine, errorMessage) });
                }
                #endregion

                SaveExhibitionEvent(request);
                return Json(new { ResultCode = 0001 });
            }
            catch (Exception ex)
            {
                logger.Error("AddExhibitionEvent", ex);
                throw ex;
            }
        }

        /// <summary>
        /// 編輯策展
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult EditExhibitionEvent(int id)
        {
            SetPageDefault();
            List<string> selllerStoreGuids = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.ToList();
            selllerStoreGuids.Insert(0, _config.SkmRootSellerGuid.ToString());//將總公司加進去
            ViewBag.ExhibitionCodes = _ep.GetExhibitionCodeList(selllerStoreGuids);
            ExhibitionEvent exhibitionEvent = _ep.GetExhibitionEvent(id);
            if (exhibitionEvent.Id == 0)
            {
                TempData["ReturnMessage"] = "無此權限編輯此策展";//故意使用同一個錯誤訊息
                return RedirectToAction("ExhibitionEvent");
            }
            List<ExhibitionStore> exhibitionStores = _ep.GetExhibitionStore(id);
            if (!((Dictionary<string, string>)ViewBag.SKMStores).Any(item => exhibitionStores.Any(store => store.StoreGuid.ToString() == item.Key)))
            {
                TempData["ReturnMessage"] = "無此權限編輯此策展";
                return RedirectToAction("ExhibitionEvent");
            }
            if (exhibitionEvent.Status == (byte)SkmExhibitionEventStatus.Published)
            {
                TempData["ReturnMessage"] = "無此權限編輯此策展";
                return RedirectToAction("ExhibitionEvent");
            }
            List<ExhibitionCardType> exhibitionCardTypes = _ep.GetExhibitionCardTypesByEventId(id);
            List<ExhibitionCategory> exhibitionCategoryList = _ep.GetExhibitionCategoryList(id);
            List<ExhibitionDeal> exhibitionDeals = _ep.GetExhibitionDealListByEventId(id);
            List<ExhibitionActivity> exhibitionActivitiex = _ep.GetExhibitionActivities(id);
            List<ExhibitionEventExternalDeal> externalDeals = new List<ExhibitionEventExternalDeal>();
            bool isCRM = exhibitionEvent.EventType == (byte)SkmExhibitionEventType.DealExhibition
                && !exhibitionCardTypes.Any(item => item.CardType == (byte)SkmCardLevelType.All);

            IEnumerable<ViewExternalDealRelationStoreExhibitionCode> externalDealRelationStoreExhibitionCodes =
            _ep.GetExternalDealByExhibitionCode(exhibitionEvent.ExhibitionCodeId, exhibitionEvent.StartDate, exhibitionEvent.EndDate, exhibitionStores.Select(item => { return item.StoreGuid.ToString(); }).ToList());
            if (exhibitionEvent.EventType == (byte)SkmExhibitionEventType.DealExhibition)
            {
                externalDealRelationStoreExhibitionCodes = externalDealRelationStoreExhibitionCodes.Where(item => item.IsCrmDeal == isCRM);
            }

            externalDealRelationStoreExhibitionCodes.Select(item =>
            {
                return new ExhibitionEventExternalDeal()
                {
                    EventGuid = item.EventGuid.ToString(),
                    EventName = item.EventName,
                    StartDate = item.StartDate.HasValue ? item.StartDate.Value.ToString("yyyy/MM/dd HH:mm") : "",
                    EndDate = item.EndDate.HasValue ? item.EndDate.Value.ToString("yyyy/MM/dd HH:mm") : ""
                };
            })
            //為了排除重複的檔次編號(因Store會好多筆)
            .ForEach(item =>
            {
                if (!externalDeals.Any(resultItem => resultItem.EventGuid == item.EventGuid))
                {
                    externalDeals.Add(item);
                }
            });

            SetExhibitionEvent result = new SetExhibitionEvent()
            {
                IsSkmHqPower = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name)),
                Id = exhibitionEvent.Id,
                EventContent = HttpUtility.HtmlEncode(exhibitionEvent.EventContent),
                EventRule = HttpUtility.HtmlEncode(exhibitionEvent.EventRule),
                ExhibitionCodeId = exhibitionEvent.ExhibitionCodeId,
                Subject = exhibitionEvent.Subject,
                Type = (SkmExhibitionEventType)Enum.Parse(typeof(SkmExhibitionEventType), exhibitionEvent.EventType.ToString()),
                StartDate = exhibitionEvent.StartDate.ToString("yyyy/MM/dd"),
                StartTime = exhibitionEvent.StartDate.ToString("HH:mm:ss"),
                EndDate = exhibitionEvent.EndDate.ToString("yyyy/MM/dd"),
                EndTime = exhibitionEvent.EndDate.ToString("HH:mm:ss"),
                Store = exhibitionStores.Select(item => item.StoreGuid).ToArray(),
                BannerLink = exhibitionEvent.BannerLink,
                BannerPicFileName = exhibitionEvent.BannerPic,
                RecommendSkmCardLevels = exhibitionCardTypes.Select(item => { return (SkmCardLevelType)Enum.Parse(typeof(SkmCardLevelType), item.CardType.ToString()); }).ToArray(),
                Categorys = exhibitionCategoryList.Select(category =>
                {
                    return new ExhibitionEventCategory()
                    {
                        Name = category.Name,
                        //排除不存在的檔次資料
                        ExternalDeals = exhibitionDeals.Join(externalDeals, ExhibitionDeals => ExhibitionDeals.ExternalDealGuid.ToString(), ExternalDeals => ExternalDeals.EventGuid, (ExhibitionDeals, ExternalDeals) => new { ExhibitionDeals, ExternalDeals })
                                                       .Where(deal => deal.ExhibitionDeals.CategoryId == category.Id)
                                                       .Select(deal =>
                                                       {
                                                           return new ExhibitionEventCategoryExternalDeal()
                                                           {
                                                               ExternalDealGuid = deal.ExhibitionDeals.ExternalDealGuid,
                                                               IsMainDeal = deal.ExhibitionDeals.IsMainDeal
                                                           };
                                                       }).ToArray()
                    };
                }).ToArray(),
                ExistEvents = exhibitionActivitiex.Select(item =>
                {
                    return new ExistEvent()
                    {
                        Seq = item.Seq,
                        EventImageName = item.BannerPic,
                        EventUrl = item.EventLink,
                    };
                }).ToArray(),
            };

            ViewBag.MediaBaseUrl = _config.MediaBaseUrl;
            ViewBag.ExternalDeal = HttpUtility.JavaScriptStringEncode(new JsonSerializer().Serialize(externalDeals));
            return View(result);
        }

        /// <summary>
        /// 編輯策展
        /// </summary>
        /// <param name="request">設定策展資訊</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult EditExhibitionEvent(SetExhibitionEvent request)
        {
            #region 驗證
            SetPageDefault();
            string preFixName = "SkmExhibitionEvent";
            List<string> errorMessage = ValidateExhibitionEventSetInfo(request, preFixName);

            if (errorMessage.Count() > 0)
            {
                return Json(new { ResultCode = 1002, ResultMessage = String.Join(Environment.NewLine, errorMessage) });
            }

            ExhibitionEvent exhibitionEvent = _ep.GetExhibitionEvent(request.Id);
            if (exhibitionEvent.Id == 0)
            {
                return Json(new { ResultCode = 1006, ResultMessage = "無此權限編輯此策展" });
            }
            List<ExhibitionStore> exhibitionStores = _ep.GetExhibitionStore(request.Id);
            if (!((Dictionary<string, string>)ViewBag.SKMStores).Any(item => exhibitionStores.Any(store => store.StoreGuid.ToString() == item.Key)))
            {
                return Json(new { ResultCode = 1006, ResultMessage = "無此權限編輯此策展" });
            }
            if (exhibitionEvent.Status == (byte)SkmExhibitionEventStatus.Published)
            {
                return Json(new { ResultCode = 1006, ResultMessage = "無此權限編輯此策展" });
            }
            #endregion

            SaveExhibitionEvent(request);
            return Json(new { ResultCode = 0001 });
        }

        /// <summary>
        /// 驗證設定策展資訊
        /// </summary>
        /// <param name="setExhibitionEvent">設定策展資訊</param>
        /// <param name="preFixName">策展上傳圖檔資訊</param>
        /// <returns>驗證錯誤訊息</returns>
        private static List<string> ValidateExhibitionEventSetInfo(SetExhibitionEvent setExhibitionEvent, string preFixName)
        {
            List<string> result = new List<string>();
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            if (setExhibitionEvent.ExhibitionCodeId <= 0) result.Add("請選擇策展代號");
            if (string.IsNullOrEmpty(setExhibitionEvent.StartDate) || string.IsNullOrEmpty(setExhibitionEvent.StartTime) ||
                !DateTime.TryParse(setExhibitionEvent.StartDate + " " + setExhibitionEvent.StartTime, out startDate))
                result.Add("請選擇檔次上架時間");
            if (string.IsNullOrEmpty(setExhibitionEvent.EndDate) || string.IsNullOrEmpty(setExhibitionEvent.EndTime) ||
                !DateTime.TryParse(setExhibitionEvent.EndDate + " " + setExhibitionEvent.EndTime, out endDate))
                result.Add("請選擇檔次下架時間");
            if (setExhibitionEvent.BannerPic == null)
            {
                if (string.IsNullOrEmpty(setExhibitionEvent.BannerPicFileName))
                {
                    result.Add("請選擇網頁圖檔");
                }
            }
            else
            {
                ImgData imgUploadData = SkmFacade.CheckImgType(setExhibitionEvent.BannerPic, 750, 470, preFixName, Guid.NewGuid().ToString());
                if (!imgUploadData.IsExtensionPass || !imgUploadData.IsSizePass)
                {
                    result.Add("僅能上傳圖片檔案，大小為 750 x 470");
                }
                else
                {
                    setExhibitionEvent.BannerPicFileName = imgUploadData.SaveFileName;
                }
            }
            if (setExhibitionEvent.RecommendSkmCardLevels.Count() <= 0) result.Add("請選擇推薦卡別");
            if (setExhibitionEvent.Store.Count() <= 0) result.Add("請選擇適用分店");
            if (setExhibitionEvent.Type == SkmExhibitionEventType.DealExhibition || setExhibitionEvent.Type == SkmExhibitionEventType.MessageExhibition)
            {
                if (string.IsNullOrEmpty(setExhibitionEvent.EventRule))
                    result.Add("請編輯注意事項");
            }
            if (setExhibitionEvent.Type == SkmExhibitionEventType.DealExhibition)
            {
                if (string.IsNullOrEmpty(setExhibitionEvent.EventContent))
                    result.Add("請編輯策展內容");
                if (setExhibitionEvent.Categorys == null)
                    result.Add("請填寫商品分類");
                else
                {
                    setExhibitionEvent.Categorys = setExhibitionEvent.Categorys
                        .Where(
                        item => !string.IsNullOrEmpty(item.Name) &&
                        item.ExternalDeals != null &&
                        item.ExternalDeals.Any(subItem => subItem.ExternalDealGuid != new Guid()))
                        .Select(item =>
                        {
                            return new ExhibitionEventCategory()
                            {
                                Name = item.Name,
                                ExternalDeals = item.ExternalDeals.Where(subItem => subItem.ExternalDealGuid != new Guid()).ToArray()
                            };
                        }).ToArray();
                    if (!setExhibitionEvent.Categorys.Any())
                        result.Add("請填寫商品分類");
                }
            }
            if (setExhibitionEvent.Type == SkmExhibitionEventType.SingleEvent)
            {
                //去網頁尾巴空白
                setExhibitionEvent.BannerLink = setExhibitionEvent.BannerLink.TrimEnd();
                if (string.IsNullOrEmpty(setExhibitionEvent.BannerLink))
                    result.Add("請填寫活動網址");

                else if (!setExhibitionEvent.BannerLink.ToLower().StartsWith("https://") &&
                         !setExhibitionEvent.BannerLink.ToLower().StartsWith("http://"))
                    result.Add("活動網址請填寫正確的URL格式");
            }
            if (setExhibitionEvent.Type == SkmExhibitionEventType.MultiEvent)
            {
                ImgData imgUploadData;
                if (setExhibitionEvent.ExistEvents == null)
                {
                    result.Add("請設定多重活動");
                }
                else
                {
                    setExhibitionEvent.ExistEvents.ForEach(item =>
                    {
                        //去網頁尾巴空白
                        item.EventUrl = item.EventUrl.TrimEnd();
                        if (string.IsNullOrEmpty(item.EventUrl))
                        {
                            result.Add("請選擇順序[" + item.Seq + "]的活動網址");
                        }
                        else if (!Helper.CheckUrlValid(item.EventUrl))
                        {
                            result.Add("順序[" + item.Seq + "]的活動網址請填寫正確的URL格式");
                        }
                        if (item.EventImage == null)
                        {
                            if (string.IsNullOrEmpty(item.EventImageName))
                            {
                                result.Add("請選擇順序[" + item.Seq + "]的活動圖檔");
                            }
                        }
                        else
                        {
                            imgUploadData = SkmFacade.CheckImgType(item.EventImage, 750, 316, preFixName, Guid.NewGuid().ToString());
                            if (!imgUploadData.IsExtensionPass || !imgUploadData.IsSizePass)
                            {
                                result.Add("順序[" + item.Seq + "]的活動圖檔僅能上傳圖片檔案，大小為 750 x 316");
                            }
                            else
                            {
                                item.EventImageName = imgUploadData.SaveFileName;
                            }
                        }
                    });
                }
            }
            return result;
        }

        /// <summary>
        /// 儲存策展資訊
        /// </summary>
        /// <param name="setExhibitionEvent">設定策展資訊</param>
        private void SaveExhibitionEvent(SetExhibitionEvent setExhibitionEvent)
        {
            if (_config.IsEnableCRMUserGroup)
            {
                bool delUserGroupIsSuccess = _ep.DelExhibitionUserGroupMapping(setExhibitionEvent.Id);
            }

            bool delStoreIsSuccess = _ep.DelExhibitionStoreByEventId(setExhibitionEvent.Id);
            bool delCategoryIsSuccess = _ep.DelExhibitionCategoryByEventID(setExhibitionEvent.Id);
            bool delDealIsSuccess = _ep.DelExhibitionDealByEventId(setExhibitionEvent.Id);

            //一個館就一筆資料 (編輯只會帶一筆Store, 新增才會多筆Store)
            foreach (Guid store in setExhibitionEvent.Store)
            {
                ExhibitionEvent exhibitionEvent = new ExhibitionEvent()
                {
                    Id = setExhibitionEvent.Id,
                    ExhibitionCodeId = setExhibitionEvent.ExhibitionCodeId,
                    BannerPic = setExhibitionEvent.BannerPicFileName,
                    EventContent = HttpUtility.UrlDecode(setExhibitionEvent.EventContent.Trim()).Replace("\n", ""),
                    EventRule = HttpUtility.UrlDecode(setExhibitionEvent.EventRule.Trim()).Replace("\n", ""),
                    Subject = setExhibitionEvent.Subject,
                    StartDate = DateTime.Parse(setExhibitionEvent.StartDate + " " + setExhibitionEvent.StartTime),
                    EndDate = DateTime.Parse(setExhibitionEvent.EndDate + " " + setExhibitionEvent.EndTime),
                    Creater = Convert.ToInt32(User.Identity.GetPropertyValue("Id")),
                    EventType = (byte)setExhibitionEvent.Type,
                    Status = (byte)SkmExhibitionEventStatus.Draft,
                    CreateTime = DateTime.Now,
                    Seq = 0,
                    IsHqEvent = false,
                    BannerLink = setExhibitionEvent.BannerLink,
                };
                bool setEventIsSuccess = _ep.SetExhibitionEvent(exhibitionEvent);
                if (setEventIsSuccess)
                {
                    #region 圖檔處理
                    if (setExhibitionEvent.BannerPic != null)
                    {
                        setExhibitionEvent.BannerPic.SaveAs(imgUploadDirPath + setExhibitionEvent.BannerPicFileName);
                    }
                    #endregion

                    ExhibitionStore exhibitionStore = new ExhibitionStore()
                    {
                        EventId = exhibitionEvent.Id,
                        StoreGuid = store
                    };

                    //儲存資料暫時不理會失敗
                    if (_config.IsEnableCRMUserGroup && setExhibitionEvent.UserGroup != null)
                    {
                        bool setUserGroupIsSuccess = _ep.AddExhibitionUserGroupMapping(exhibitionEvent.Id,
                            setExhibitionEvent.UserGroup.ToList());
                    }
                    bool setStoreIsSuccess = _ep.SetExhibitionStore(exhibitionEvent.Id, new List<ExhibitionStore>() { exhibitionStore });
                    bool setCardTypeIsSuccess = _ep.SetExhibitionCardType(exhibitionEvent.Id, setExhibitionEvent.RecommendSkmCardLevels);

                    if (setExhibitionEvent.Type == SkmExhibitionEventType.DealExhibition)
                    {
                        foreach (ExhibitionEventCategory category in setExhibitionEvent.Categorys)
                        {
                            ExhibitionCategory exhibitionCategory = new ExhibitionCategory()
                            {
                                EventId = exhibitionEvent.Id,
                                Name = category.Name,
                                CreateTime = DateTime.Now,
                                Seq = 0
                            };
                            bool setCategoryIsSuccess = _ep.SetExhibitionCategory(exhibitionCategory);
                            category.ExternalDeals.ForEach(item =>
                            {
                                bool setDealIsSuccess = _ep.SetExhibitionDeal(
                                new ExhibitionDeal()
                                {
                                    CategoryId = exhibitionCategory.Id,
                                    EventId = exhibitionEvent.Id,
                                    IsMainDeal = item.IsMainDeal,
                                    ExternalDealGuid = item.ExternalDealGuid,
                                    CreateTime = DateTime.Now
                                });
                                SkmFacade.SetExhibitionDealTimeSlot(store, exhibitionEvent.Id, exhibitionCategory.Id, item.ExternalDealGuid, exhibitionEvent.StartDate, exhibitionEvent.EndDate);
                            });
                        }
                    }
                    if (setExhibitionEvent.Type == SkmExhibitionEventType.MultiEvent)
                    {
                        bool setActivityIsSuccess = _ep.SetExhibitionActivity(setExhibitionEvent.ExistEvents.Select(item =>
                        {
                            return new ExhibitionActivity()
                            {
                                ExhibitionEventId = exhibitionEvent.Id,
                                BannerPic = item.EventImageName,
                                EventLink = item.EventUrl,
                                Seq = item.Seq,
                            };
                        }));
                        foreach (ExistEvent existEvent in setExhibitionEvent.ExistEvents)
                        {
                            if (existEvent.EventImage != null)
                            {
                                existEvent.EventImage.SaveAs(imgUploadDirPath + existEvent.EventImageName);
                            }
                        }
                    }
                }
                //刪除ExhibitionDealTimeSlot內該策展編號(ExhibitionEvent.Id)下已不存在的分類(ExhibitionCategory)資料
                _ep.DelExhibitionDealTimeSlotUnreferencedCategor(exhibitionEvent.Id);
            }
        }

        /// <summary>
        /// 依條件取得策展檔次資訊清單
        /// </summary>
        /// <param name="request">依條件取得策展檔次資訊清單請求資訊</param>
        /// <returns>策展檔次資訊清單</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult GetExternalDeals(GetExternalDealsRequest request)
        {
            SetPageDefault();
            if (request.SelectedExhibitionCode <= 0)
            {
                return Json(new { ResultCode = 1001, ResultMessage = "請先選擇策展代號" });
            }
            if (request.RecommendSkmCardLevels.Count() <= 0)
            {
                return Json(new { ResultCode = 1001, ResultMessage = "請先選擇信用卡別" });
            }
            if (request.Stores == null || request.Stores.Count() == 0)
            {
                request.Stores = ((Dictionary<string, string>)ViewBag.SKMStores).Keys.ToList();
            }
            bool isCRM = request.Type == SkmExhibitionEventType.DealExhibition && !request.RecommendSkmCardLevels.Any(item => item == SkmCardLevelType.All);

            List<ExhibitionEventExternalDeal> result = new List<ExhibitionEventExternalDeal>();
            if (request.EndDate == DateTime.MinValue) request.EndDate = DateTime.MaxValue;
            IEnumerable<ViewExternalDealRelationStoreExhibitionCode> externalDealRelationStoreExhibitionCodes =
            _ep.GetExternalDealByExhibitionCode(request.SelectedExhibitionCode, request.StartDate, request.EndDate, request.Stores);
            //預防前端回來的不是商品策展，基本上都會是啦
            if (request.Type == SkmExhibitionEventType.DealExhibition)
            {
                externalDealRelationStoreExhibitionCodes = externalDealRelationStoreExhibitionCodes.Where(item => item.IsCrmDeal == isCRM);
            }

            externalDealRelationStoreExhibitionCodes.Select(item =>
            {
                return new ExhibitionEventExternalDeal()
                {
                    EventGuid = item.EventGuid.ToString(),
                    EventName = item.EventName,
                    StartDate = item.StartDate.HasValue ? item.StartDate.Value.ToString("yyyy/MM/dd HH:mm") : "",
                    EndDate = item.EndDate.HasValue ? item.EndDate.Value.ToString("yyyy/MM/dd HH:mm") : ""
                };
            })
            //為了排除重複的檔次編號(因Store會好多筆)
            .ForEach(item =>
            {
                if (!result.Any(resultItem => resultItem.EventGuid == item.EventGuid))
                {
                    result.Add(item);
                }
            });
            return Json(new { ResultCode = 0001, Data = result });
        }

        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult GetEventStoreList(string guid)
        {
            string result = string.Empty;
            Guid eventGuid = new Guid();
            if (!Guid.TryParse(guid, out eventGuid))
            {
                //若是亂入就回傳沒有資料的東西
                return Json(new { ResultCode = 0001, Data = result });
            }

            IEnumerable<Guid> eventStoreGuids = _pp.ExternalDealRelationStoreGetListByDeal(eventGuid).Select(item => item.ParentSellerGuid).Distinct();
            result = string.Join("|", _sp.SellerGetSkmParentList()
                .Where(item => eventStoreGuids.Contains(item.Guid))
                .Select(item => { return item.SellerName; }));

            return Json(new { ResultCode = 0001, Data = result });
        }

        #endregion

        #region 策展排序

        private bool GetExhibitionEventAndCategory(Guid sellerGuid, int extId, out Dictionary<int, string> exhibitionData, out Dictionary<int, string> categoryData)
        {
            exhibitionData = new Dictionary<int, string>();
            categoryData = new Dictionary<int, string>();
            try
            {
                var exhibitionEvents = _ep.GetExhibitionEventsBySellerGuid(sellerGuid);
                if (exhibitionEvents.Any())
                {
                    exhibitionEvents = exhibitionEvents.Where(x => x.EndDate > DateTime.Now
                                                                   && x.EventType == (byte)SkmExhibitionEventType.DealExhibition)
                        .OrderByDescending(x => x.Id).ToList();
                }

                if (exhibitionEvents.Any())
                {
                    exhibitionData = exhibitionEvents.ToDictionary(x => x.Id, y => y.Subject);

                    var temp = extId == 0 ? exhibitionData.First() :
                        exhibitionData.Any(x => x.Key == extId) ? exhibitionData.First(x => x.Key == extId) :
                        exhibitionData.First();
                    categoryData = _ep.GetExhibitionCategoryList(temp.Key).ToDictionary(x => x.Id, y => y.Name);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult QueryExhibition(string sellerGuid, int? exhId)
        {
            Guid sg = Guid.TryParse(sellerGuid, out sg) ? sg : Guid.Empty;
            var exhibitionData = new Dictionary<int, string>();
            var categoryData = new Dictionary<int, string>();
            var result = GetExhibitionEventAndCategory(sg, exhId ?? 0, out exhibitionData, out categoryData);
            return Json(
                new
                {
                    success = result,
                    exhId,
                    exhData = exhibitionData.Select(item => new { Key = item.Key, Value = item.Value }),
                    catData = categoryData.Select(item => new { Key = item.Key, Value = item.Value })
                },
                JsonRequestBehavior.AllowGet
            );
        }

        /// <summary>
        /// string qSeller, int? Exid, int? Cat, string BeginDate
        /// </summary>
        /// <param name="qSeller"></param>
        /// <param name="exid"></param>
        /// <param name="cat"></param>
        /// <param name="beginDate"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionDealSort(string qSeller, int? exid, int? cat, string beginDate)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            qSeller = qSeller == null ? SkmFacade.GetDefaultSellerGuid(userId, Guid.Parse(DefaultSeller)).ToString() : qSeller;

            DateTime bd = DateTime.Now;
            if (!string.IsNullOrEmpty(beginDate))
            {
                DateTime.TryParse(beginDate, out bd);
            }

            var response = new ExhibitionSortResponse
            {
                QuerySeller = qSeller,
                QueryExhibitionId = exid ?? 0,
                QueryCategoryId = cat ?? 0,
                BeginDate = DateTime.Parse(bd.AddDays(1 - (int)bd.DayOfWeek).ToString("yyyy/MM/dd")),
                Sellers = SellersGetByUserId(),
                ExhibitionCol = new Dictionary<int, string>(),
                CategoryCol = new Dictionary<int, string>(),
                DealTimeSlotData = new List<ExhibitionSortData>()
            };

            var exhibitionData = new Dictionary<int, string>();
            var categoryData = new Dictionary<int, string>();

            if (GetExhibitionEventAndCategory(Guid.Parse(response.QuerySeller), response.QueryExhibitionId,
                out exhibitionData, out categoryData))
            {
                response.ExhibitionCol = exhibitionData;
                response.CategoryCol = categoryData;
            }

            if (response.QueryExhibitionId == 0 && exhibitionData.Any())
            {
                response.QueryExhibitionId = exhibitionData.First().Key;
            }

            if (response.QueryCategoryId == 0 && categoryData.Any())
            {
                response.QueryCategoryId = categoryData.First().Key;
            }

            response.SelectSellerName = response.Sellers.First(x => x.Key == qSeller).Value;
            response.SelectExhibitionName = response.QueryExhibitionId == 0 ? string.Empty : response.ExhibitionCol.First(x => x.Key == response.QueryExhibitionId).Value;
            response.SelectCategoryName = response.QueryCategoryId == 0 ? string.Empty : response.CategoryCol.First(x => x.Key == response.QueryCategoryId).Value;

            for (var i = 0; i < 7; i++)
            {
                var data = _ep.GetExhibitionDealTimeSlot(response.BeginDate.AddDays(i), Guid.Parse(response.QuerySeller),
                    response.QueryExhibitionId, response.QueryCategoryId);

                List<ExhDealTimeSlot> timeSlot = new List<ExhDealTimeSlot>();

                foreach (var d in data)
                {
                    ViewExternalDeal ved = _pp.ViewExternalDealGetListByDealrGuid(d.ExternalGuid)
                        .FirstOrDefault(x => x.ParentSellerGuid == Guid.Parse(response.QuerySeller));

                    #region 檢核

                    if (ved == null || !ved.IsLoaded)
                    {
                        continue;
                    }

                    if (ved.Bid == null || ved.Bid == Guid.Empty)
                    {
                        continue;
                    }

                    var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid((Guid)ved.Bid);

                    if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.SKMDeal))
                    {
                        continue;
                    }

                    #endregion 檢核

                    timeSlot.Add(new ExhDealTimeSlot
                    {
                        TimeSlotId = d.Id,
                        ExternalGuid = ved.Guid,
                        ItemName = vpd.AppTitle,
                        DealDate = response.BeginDate.AddDays(i),
                        Sequence = d.Sequence,
                        IsHidden = d.IsHidden,
                        DealUrl = string.Format("{0}/SKMDeal/SkmDealPreview?guid={1}", _config.SiteUrl, ved.Guid)
                    });
                }

                response.DealTimeSlotData.Add(new ExhibitionSortData
                {
                    DealDate = response.BeginDate.AddDays(i),
                    Deals = timeSlot
                });
            }

            return View(response);
        }

        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult ExhibitionDealSortUpdate(string data)
        {
            bool result = true;
            string message = string.Empty;
            try
            {
                List<ExhDealTimeSlot> edtsCol = new JsonSerializer().Deserialize<List<ExhDealTimeSlot>>(data);
                List<ExhibitionDealTimeSlot> updateList = new List<ExhibitionDealTimeSlot>();
                foreach (var edts in edtsCol)
                {
                    ExhibitionDealTimeSlot slot = _ep.GetExhibitionDealTimeSlotById(edts.TimeSlotId);
                    if (slot != null && slot.Id > 0)
                    {
                        slot.Sequence = edts.Sequence;
                        slot.IsHidden = edts.IsHidden;
                        updateList.Add(slot);
                    }
                }

                if (updateList.Any())
                {
                    _ep.UpdateExhibitionDealTimeSlot(updateList);
                }
            }
            catch (Exception ex)
            {
                result = false;
                message = ex.Message;
            }

            return Json(new { Success = result, Message = message });
        }

        #endregion 策展排序

        #region 策展客群

        /// <summary>
        /// 依條件取得策展客群 (api)
        /// </summary>
        /// <param name="request">依條件取得策展客群</param>
        /// <returns>策展客群清單</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult GetUserGroupList(GetUserGroupListRequest request)
        {
            List<ExhibitionUserGroup> result = new List<ExhibitionUserGroup>();

            var list = _ep.GetExhibitionUserGroupList(request.EventId, request.Stores, request.StartDate, request.EndDate);
            var mapping = _ep.GetExhibitionUserGroupMappings();

            list.ForEach(p => p.Selected = mapping.Any(m => m.ExhibitionEventId == request.EventId
                                                            && m.ExhibitionUserGroupId.ToString() == p.Value));

            return Json(new { ResultCode = 0001, Data = list });
        }

        /// <summary>
        /// 策展客群告列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupList(string qSeller, int page = 1)
        {
            Dictionary<string, string> sellers = SellersGetByUserId();

            bool isHq = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name));

            //是否為總公司權限
            if (isHq)
            {
                Dictionary<string, string> newSellers = new Dictionary<string, string>();
                newSellers.Add(_config.SkmRootSellerGuid.ToString(), "總公司 (全部)");
                sellers = newSellers.Concat(sellers).ToDictionary(x => x.Key, x => x.Value);

            }

            //總公司可以看其他分店資訊, 若有選擇分店 qSeller不等於null
            if (string.IsNullOrEmpty(qSeller) && isHq)
            {
                qSeller = _config.SkmRootSellerGuid.ToString();
            }
            else if (string.IsNullOrEmpty(qSeller))
            {
                qSeller = DefaultSeller;
            }


            int dataCnt = 0;
            int dataPageSize = 10;
            var list = _ep.GetExhibitionUserGroupList(qSeller, out dataCnt, page, dataPageSize);

            ViewBag.SkmSellers = sellers;
            ViewBag.qSeller = qSeller;
            ViewBag.isHq = isHq;
            ViewBag.dataCnt = dataCnt;
            ViewBag.dataPage = page;
            ViewBag.dataPageSize = dataPageSize;

            return View(list);
        }

        /// <summary>
        /// 策展客群Add
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupAdd(Guid sellerGuid, string sellerName)
        {
            UserGroupViewModel model = new UserGroupViewModel();
            model.SellerName = sellerName;
            model.SellerGuid = sellerGuid;
            model.StartDate = DateTime.Now;
            model.EndDate = DateTime.Now.AddMonths(1);
            return View(model);
        }

        /// <summary>
        /// 時間處理
        /// </summary>
        /// <param name="model"></param>
        private void ReSetDate(UserGroupViewModel model)
        {
            if (!string.IsNullOrEmpty(model.StartTimeHour) && !model.StartTimeHour.Equals("00"))
            {
                model.StartDate = model.StartDate.AddHours(Convert.ToInt16(model.StartTimeHour));
            }
            if (!string.IsNullOrEmpty(model.EndTimeHour) && !model.EndTimeHour.Equals("00"))
            {
                model.EndDate = model.EndDate.AddHours(Convert.ToInt16(model.EndTimeHour));
            }
            if (!string.IsNullOrEmpty(model.StartTimeMinute) && !model.StartTimeMinute.Equals("00"))
            {
                model.StartDate = model.StartDate.AddMinutes(Convert.ToInt16(model.StartTimeMinute));
            }
            if (!string.IsNullOrEmpty(model.EndTimeMinute) && !model.EndTimeMinute.Equals("00"))
            {
                model.EndDate = model.EndDate.AddMinutes(Convert.ToInt16(model.EndTimeMinute));
            }
        }

        /// <summary>
        /// 客群csv檔案處理
        /// </summary>
        /// <param name="tokenList"></param>
        /// <param name="fileBase"></param>
        /// <returns></returns>
        private List<string> ParseCsvFile(List<string> tokenList, HttpPostedFileBase fileBase)
        {
            if (!fileBase.FileName.Split('.').LastOrDefault().ToLower().Equals("csv"))
            {
                ModelState.AddModelError("File", "客群名單必須是.csv檔");
            }
            else
            {
                StreamReader reader = new StreamReader(fileBase.InputStream, System.Text.Encoding.Default);
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    if (line == null)
                    {
                        continue;
                    }

                    string[] data = line.Split(",");
                    if (data.Length < 1 || string.IsNullOrEmpty(data[0]))
                    {
                        continue;
                    }
                    tokenList.Add(data[0]);
                }

                if (!tokenList.Any())
                {
                    ModelState.AddModelError("File", "客群名單至少有一筆");
                }
            }

            return tokenList;
        }
        /// <summary>
        /// 策展客群Add post
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupAdd(UserGroupViewModel model)
        {
            string fileName = Guid.NewGuid() + ".csv";
            List<string> tokenList = new List<string>();
            if (model.File == null)
            {
                ModelState.AddModelError("File", "必須匯入客群名單");
                return View("ExhibitionUserGroupAdd", model);
            }

            //csv
            tokenList = ParseCsvFile(tokenList, model.File);

            if (!ModelState.IsValid)
            {
                return View("ExhibitionUserGroupAdd", model);
            }
            else
            {
                //1. 有正確skm_member_token才存檔
                model.File.SaveAs(fileUploadDirPath + fileName);

                int userId = MemberFacade.GetUniqueId(User.Identity.Name);

                ReSetDate(model);

                bool isHq = SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name));
                //2. 新增客群
                var userGroup = new ExhibitionUserGroup
                {
                    Id = model.Id,
                    Name = model.Name,
                    StartDate = model.StartDate,
                    EndDate = model.EndDate,
                    FileName = fileName,
                    FileUserCount = tokenList.Count(),
                    SellerGuid = model.SellerGuid,
                    CreateDate = DateTime.Now,
                    CreateUser = userId,
                    //是否為總公司替其他分店建的客群
                    IsHqCreated = (isHq && model.SellerGuid != _config.SkmRootSellerGuid),
                    IsHq = (model.SellerGuid == _config.SkmRootSellerGuid)
                };

                //Save ExhibitionUserGroup
                if (!_ep.AddExhibitionUserGroup(userGroup))
                {
                    ModelState.AddModelError("ErrorMsg", "存檔有誤，請洽IT");
                    return View("ExhibitionUserGroupAdd", model);
                }
                else
                {
                    List<ExhibitionUserGroupMemberToken> groupList = new List<ExhibitionUserGroupMemberToken>();
                    //2. 新增客群名單 
                    foreach (var token in tokenList)
                    {
                        groupList.Add(new ExhibitionUserGroupMemberToken()
                        {
                            ExhibitionUserGroupId = userGroup.Id,
                            SkmMemberToken = token
                        });
                    }

                    //BulkInsert Tokens
                    if (!_ep.BulkInsertExhibitionUserGroupMemberToken(groupList))
                    {
                        ModelState.AddModelError("ErrorMsg", "新增客群名單失敗");
                        return View("ExhibitionUserGroupAdd", model);
                    }

                    return RedirectToAction("ExhibitionUserGroupList", new { qSeller = model.SellerGuid });
                }
            }
        }

        /// <summary>
        /// 策展客群Edit
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupEdit(int id, string sellerName, int page = 1)
        {
            var d = _ep.GetExhibitionUserGroup(id);
            UserGroupViewModel model = new UserGroupViewModel()
            {
                Id = d.Id,
                Name = d.Name,
                StartDate = d.StartDate,
                EndDate = d.EndDate,
                SellerName = sellerName,
                SellerGuid = d.SellerGuid,
                FileName = d.FileName,
                Page = page
            };
            return View("ExhibitionUserGroupAdd", model);
        }


        /// <summary>
        /// 策展客群Edit post
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupEdit(UserGroupViewModel model)
        {
            List<string> tokenList = new List<string>();
            if (model.File != null)
            {
                //csv
                tokenList = ParseCsvFile(tokenList, model.File);
            }

            if (!ModelState.IsValid)
            {
                return View("ExhibitionUserGroupAdd", model);
            }
            else
            {
                if (model.File != null)
                {
                    model.FileName = Guid.NewGuid() + ".csv";
                    model.File.SaveAs(fileUploadDirPath + model.FileName);

                    //先刪除token mapping
                    _ep.DelExhibitionUserGroupMemberToken(model.Id);

                    //新增客群名單token mapping + BulkInsert Tokens
                    List<ExhibitionUserGroupMemberToken> groupList = new List<ExhibitionUserGroupMemberToken>();
                    foreach (var token in tokenList)
                    {
                        groupList.Add(new ExhibitionUserGroupMemberToken()
                        {
                            ExhibitionUserGroupId = model.Id,
                            SkmMemberToken = token
                        });
                    }

                    if (!_ep.BulkInsertExhibitionUserGroupMemberToken(groupList))
                    {
                        ModelState.AddModelError("ErrorMsg", "重新上傳客群名單失敗");
                        return View("ExhibitionUserGroupAdd", model);
                    }
                }

                ReSetDate(model);

                var oldGroup = _ep.GetExhibitionUserGroup(model.Id);
                var oldGroupLog = JsonConvert.SerializeObject(oldGroup);

                oldGroup.Name = model.Name;
                oldGroup.StartDate = model.StartDate;
                oldGroup.EndDate = model.EndDate;
                oldGroup.FileUserCount = (model.File != null) ? tokenList.Count : oldGroup.FileUserCount;
                oldGroup.FileName = (model.File != null) ? model.FileName : oldGroup.FileName;

                oldGroup.ModifyDate = DateTime.Now;
                oldGroup.ModifyUser = MemberFacade.GetUniqueId(User.Identity.Name);

                if (!_ep.UpdateExhibitionUserGroup(oldGroup))
                {
                    ModelState.AddModelError("ErrorMsg", "修改客群有誤");
                    return View("ExhibitionUserGroupAdd", model);
                }

                _skm.SkmLogSet(new SkmLog()
                {
                    SkmToken = SkmLogType.ManageBackgroundOperations.ToString(),
                    UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                    LogType = (int)SkmLogType.ManageBackgroundOperations,
                    InputValue = "/ExhibitionUserGroupEdit?OldData=" + oldGroupLog,
                    OutputValue = "Success & NewData =" + JsonConvert.SerializeObject(oldGroup)
                });

                //success
                return RedirectToAction("ExhibitionUserGroupList", new { qSeller = model.SellerGuid, page = model.Page });
            }
        }

        /// <summary>
        /// 策展客群Delete
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult ExhibitionUserGroupDelete(int id, string qSeller)
        {
            var oldGroupLog = JsonConvert.SerializeObject(_ep.GetExhibitionUserGroup(id));
            _ep.DelExhibitionUserGroup(id);
            _ep.DelExhibitionUserGroupMapping(id);
            _ep.DelExhibitionUserGroupMemberToken(id);

            _skm.SkmLogSet(new SkmLog()
            {
                SkmToken = SkmLogType.ManageBackgroundOperations.ToString(),
                UserId = MemberFacade.GetUniqueId(User.Identity.Name),
                LogType = (int)SkmLogType.ManageBackgroundOperations,
                InputValue = "/ExhibitionUserGroupDelete?oldData=" + oldGroupLog,
                OutputValue = "Success & IsHq=" + SkmFacade.CheckSkmHqPower(MemberFacade.GetUniqueId(User.Identity.Name))
            });
            return RedirectToAction("ExhibitionUserGroupList", new { qSeller });
        }


        #endregion

        #region skm pay 退貨

        /// <summary>
        /// 退貨頁
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmEcRefund, Administrator, ME2O, Production")]
        public ActionResult Return()
        {
            return View();
        }

        /// <summary>
        /// 管理後台 查詢skm pay訂單
        /// </summary>
        /// <param name="orderNo"></param>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        [Authorize(Roles = "SkmEcRefund, Administrator, ME2O, Production")]
        public JsonResult GetSkmPayOrderByOrderNo(string orderNo, string shopCode, string tradeNo)
        {
            var skmPayOrder = SkmFacade.GetBackendSkmPayOrder(orderNo, shopCode, tradeNo);
            var skmPayGift = skmPayOrder != null ? SkmFacade.GetUmallGift(skmPayOrder.OrderNo) : null;

            var result = new BackSkmPayOrderAndGift { Order = skmPayOrder, Gift = skmPayGift };

            SystemFacade.SetApiLog("GetSkmPayOrderByOrderNo", "", new { orderNo, shopCode }, result);
            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        /// <summary>
        /// 管理後台 退skm pay訂單
        /// </summary>
        /// <param name="orderNo"></param>
        /// <returns></returns>
        public JsonResult RefundSkmPayOrderByOrderNo(string orderNo)
        {
            var result = SkmFacade.RefundSkmPayOrderByOrderNo(orderNo, "Backend");

            SystemFacade.SetApiLog("RefundSkmPayOrderByOrderNo", "", orderNo, result);
            if (result.Code == WalletReturnCode.Success) {
                try
                {
                    var skmEp = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();
                    var skmPayOrder = skmEp.GetSkmPayOrderByOrderNo(orderNo);
                    SKMCenterRocketMQTrans.AlibabaMQTrans(skmPayOrder.OrderGuid, SKMCenterRocketMQTrans.ordeSatus.cancel);
                }
                catch (Exception e) {
                    logger.Error("call AlibabaMQTrans has Error =>" + e.Message);
                }
            }

            return new JsonResult { Data = result, JsonRequestBehavior = JsonRequestBehavior.AllowGet };
        }

        #endregion

        #region 蓋板廣告 Ads

        /// <summary>
        /// 蓋板廣告列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult AdList()
        {
            return View();
        }

        /// <summary>
        /// 取得蓋板廣告列表(實作分頁功能)
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult GetAdList(AdListModel model)
        {
            var data = _skmef.GetSkmAdBoardList();
            model.SkmAdBoardList = data.OrderByDescending(p => p.EndDate)
                .Skip(model.Skip).Take(model.Take)
                .Select(item =>
                {
                    return new SkmAdBoardViewModel()
                    {
                        Id = item.Id,
                        StartDate = item.StartDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        EndDate = item.EndDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        Name = item.Name,
                        IsEnable = item.IsEnable,
                        Status = item.IsEnable ? "顯示" : "隱藏",
                        SetIsEnableText = item.IsEnable ? "隱藏" : "顯示"
                    };
                }).ToList();
            model.TotalCount = data.Count;
            return Json(
                new
                {
                    Code = ApiResultCode.Success,
                    Data = model
                });
        }

        /// <summary>
        /// 新增蓋板廣告
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult AddAdBoard()
        {
            SetPageDefault();
            return View(new AddAdBoardRequest() { IsEnable = true });
        }

        /// <summary>
        /// 新增蓋板廣告
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult AddAdBoard(AddAdBoardRequest input)
        {
            #region Validation
            List<string> validationResult = ValidateAdBoardBaseRequest(input, 0);
            ImgData imgUploadData = new ImgData();
            if (input.UploadImageUrl != null && !string.IsNullOrEmpty(input.UploadImageUrl.FileName))
            {
                imgUploadData = SkmFacade.CheckImgType(input.UploadImageUrl, 620, 820, "SKMAdBoard", Guid.NewGuid().ToString());
                if (!imgUploadData.IsSizePass) validationResult.Add("僅能上傳圖片檔案，檔案大小建議700KB以內，尺寸為 620 x 820");
                else input.UploadImageUrl.SaveAs(imgUploadDirPath + imgUploadData.SaveFileName);
            }
            else validationResult.Add("請選擇活動圖檔");
            if (validationResult.Count > 0)
            {
                SetPageDefault();
                TempData["errorMessage"] = String.Join("\\n", validationResult);
                return View(input);
            }
            #endregion

            SkmAdBoard skmAdBoard = new SkmAdBoard()
            {
                CreateDate = DateTime.Now,
                CreateUser = UserId,
                Name = input.Name,
                OpenUrl = input.OpenUrl,
                Sort = 0,
                IsEnable = input.IsEnable,
                StartDate = DateTime.Parse(input.StartDate),
                EndDate = DateTime.Parse(input.EndDate),
                ImageUrl = SkmFacade.SkmAppStyleImgPath(imgUploadData.SaveFileName, false),
                LinkType = (int)input.LinkType,
                NeedLogin = input.NeedLogin,
            };
            if (!string.IsNullOrEmpty(input.LinkStoreGuid))
                skmAdBoard.LinkStoreGuid = new Guid(input.LinkStoreGuid);
            if (!_skmef.InsertAdBoard(skmAdBoard))
            {
                SetPageDefault();
                TempData["errorMessage"] = "新增活動失敗。";
                return View(input);
            }
            else
            {
                return RedirectToAction("AdList");
            }
        }

        [HttpGet]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult EditAdBoard(int id)
        {
            SetPageDefault();
            SkmAdBoard skmAdBoard = _skmef.GetSkmAdBoardById(id);
            if (skmAdBoard == null || skmAdBoard.Id <= 0)
            {
                return RedirectToAction("AdList");
            }
            EditAdBoardResponse editAdBoardResponse = new EditAdBoardResponse()
            {
                Id = skmAdBoard.Id,
                StartDate = skmAdBoard.StartDate.ToString("yyyy/MM/dd"),
                StartHour = skmAdBoard.StartDate.Hour,
                StartMinute = skmAdBoard.StartDate.Minute,
                EndDate = skmAdBoard.EndDate.ToString("yyyy/MM/dd"),
                EndHour = skmAdBoard.EndDate.Hour,
                EndMinute = skmAdBoard.EndDate.Minute,
                ImageUrl = skmAdBoard.ImageUrl,
                FullImageUrl = skmAdBoard.ImageUrl,
                Name = skmAdBoard.Name,
                OpenUrl = skmAdBoard.OpenUrl,
                LinkType = (int)skmAdBoard.LinkType,
                LinkStoreGuid = skmAdBoard.LinkStoreGuid,
                NeedLogin = skmAdBoard.NeedLogin,
                IsEnable = skmAdBoard.IsEnable
            };
            return View(editAdBoardResponse);
        }

        /// <summary>
        /// 新增蓋板廣告
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public ActionResult EditAdBoard(EditAdBoardRequest input)
        {
            #region Validation
            List<string> validationResult = ValidateAdBoardBaseRequest(input, input.Id);
            if (input.UploadImageUrl != null && !string.IsNullOrEmpty(input.UploadImageUrl.FileName))
            {
                ImgData imgUploadData = SkmFacade.CheckImgType(input.UploadImageUrl, 620, 820, "SKMAdBoard", Guid.NewGuid().ToString());
                if (!imgUploadData.IsSizePass) validationResult.Add("僅能上傳圖片檔案，檔案大小建議700KB以內，尺寸為 620 x 820");
                else
                {
                    input.UploadImageUrl.SaveAs(imgUploadDirPath + imgUploadData.SaveFileName);
                    input.ImageUrl = SkmFacade.SkmAppStyleImgPath(imgUploadData.SaveFileName, false);
                }
            }
            if (string.IsNullOrEmpty(input.ImageUrl)) validationResult.Add("請選擇活動圖檔");
            if (validationResult.Count > 0)
            {
                SetPageDefault();
                TempData["ErrorMessage"] = String.Join("\\n", validationResult);
                return Redirect("/skmdeal/EditAdBoard?id=" + input.Id);
            }
            #endregion

            SkmAdBoard skmAdBoard = new SkmAdBoard()
            {
                Id = input.Id,
                Name = input.Name,
                OpenUrl = input.OpenUrl,
                Sort = 0,
                IsEnable = input.IsEnable,
                StartDate = DateTime.Parse(input.StartDate),
                EndDate = DateTime.Parse(input.EndDate),
                ImageUrl = input.ImageUrl,
                LinkType = (int)input.LinkType,
                NeedLogin = input.NeedLogin,
                LastUpdateDate = DateTime.Now,
                LastUpdateUser = UserId,
            };
            if (!string.IsNullOrEmpty(input.LinkStoreGuid))
                skmAdBoard.LinkStoreGuid = new Guid(input.LinkStoreGuid);
            if (!_skmef.UpdatAdBoard(skmAdBoard))
            {
                SetPageDefault();
                TempData["ErrorMessage"] = "編輯活動失敗。";
                return Redirect("/skmdeal/EditAdBoard?id=" + input.Id);
            }
            else
            {
                return RedirectToAction("AdList");
            }
        }

        /// <summary>
        /// 刪除蓋板廣告
        /// </summary>
        /// <param name="id">要刪除的廣告看版編號</param>
        /// <returns>是否刪除成功</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult DeleteAdBoard(int id)
        {
            if (_skmef.DeleteAdBoards(id))
            {
                return Json(new { IsSuccess = true });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "刪除失敗" });
            }
        }

        /// <summary>
        /// 更新廣告看版狀態(顯示/隱藏)
        /// </summary>
        /// <param name="id">廣告看版編號</param>
        /// <param name="isEnable">是否顯示</param>
        /// <returns>是否成功</returns>
        [HttpPost]
        [Authorize(Roles = "SkmExhibition, Administrator, ME2O, Production")]
        public JsonResult SetAdBoardIsEnable(int id, bool isEnable)
        {
            SkmAdBoard skmAdBoard = _skmef.GetSkmAdBoardById(id);
            if (skmAdBoard == null || skmAdBoard.Id <= 0)
            {
                return Json(new { IsSuccess = false, Message = "更新狀態失敗。" });
            }
            if (_skmef.CheckAdBoardIsExistByDate(skmAdBoard.StartDate, skmAdBoard.EndDate, skmAdBoard.Id))
            {
                return Json(new { IsSuccess = false, Message = "活動區間重複" });
            }
            SkmAdBoard adBoard = _skmef.UpdateAdBoardIsEnable(id, isEnable);
            if (adBoard != null)
            {
                return Json(new { IsSuccess = true, IsEnable = adBoard.IsEnable, Status = adBoard.IsEnable ? "顯示" : "隱藏", SetIsEnableText = adBoard.IsEnable ? "隱藏" : "顯示" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "更新狀態失敗" });
            }
        }

        /// <summary>
        /// 驗證廣告看板資訊是否正確
        /// </summary>
        /// <param name="input">廣告看板請求資訊</param>
        /// <param name="nowAdBoardId">目前的廣告看板編號(若是新增填0)</param>
        /// <returns>驗證結果(沒有表示驗證成功)</returns>
        private List<string> ValidateAdBoardBaseRequest(AdBoardBaseRequest input, int nowAdBoardId)
        {
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            List<string> validationResult = new List<string>();
            if (!DateTime.TryParse(input.StartDate, out startDate)) validationResult.Add("請輸入格式正確的活動開始時間");
            if (!DateTime.TryParse(input.EndDate, out endDate)) validationResult.Add("請輸入格式正確的活動結束時間");
            if (startDate > endDate) validationResult.Add("活動開始時間必須大於活動結束時間");
            if (_skmef.CheckAdBoardIsExistByDate(startDate, endDate, nowAdBoardId))
            {
                validationResult.Add("活動區間重複");
            }
            return validationResult;
        }

        #endregion

        #region 信用卡卡樣
        /// <summary>
        /// 信用卡卡樣頁面
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult CreditCardCategoryList(string bankNo = null)
        {
            List<BankInfo> bankInfos = VourcherFacade.BankInfoGetMainList().GetList();

            return View(bankInfos.Select(item => { return new SelectListItem() { Text = item.BankName, Value = item.BankNo, Selected = item.BankNo == bankNo }; }));
        }

        /// <summary>
        /// 依查詢條件取得信用卡卡樣資料清單
        /// </summary>
        /// <param name="input">查詢條件</param>
        /// <returns>信用卡卡樣資料清單</returns>
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        [HttpPost]
        public JsonResult GetCreditCardCategoryList(WalletGetCreditCardCategoriesRequest input)
        {
            WalletServerResponse<WalletGetCreditCardCategoryResponse> response = SkmWalletServerUtility.GetCreditCardCategories(input);
            if (response.Code != WalletReturnCode.Success)
            {
                return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = "取得實體卡面資料失敗",
                    });
            }
            GetCreditCardCategoryListResponse result = new GetCreditCardCategoryListResponse()
            {
                CardDatas = response.Data.CreditCardCategories.Select(item =>
                {
                    return new CreditCardCategoryModel()
                    {
                        Id = item.Id,
                        CardName = item.CardName,
                        CardCategory = item.CardCategory,
                        Bank = item.Bank,
                        BankName = item.BankName,
                        CardImageUrl = item.ImageUrl,
                        IsHidden = item.IsHidden,
                        ModifyDate = item.ModifyDate.ToString("yyyy/MM/dd HH:mm:ss"),
                    };
                }).ToList(),
                PageIndex = response.Data.PageIndex,
                TotalCount = response.Data.TotalCount,
                PageSize = input.PageSize,
            };
            return Json(
                new
                {
                    IsSuccess = true,
                    Data = result,
                });
        }

        /// <summary>
        /// 依信用卡卡樣編號刪除信用卡卡樣資料
        /// </summary>
        /// <param name="input">信用卡卡樣編號</param>
        /// <returns>是否刪除成功</returns>
        public JsonResult DeleteCreditCardCategory(WalletDeleteCreditCardCategoryRequest input)
        {
            WalletServerResponse<int> response = SkmWalletServerUtility.DeleteCreditCardCategory(input);
            if (response.Code != WalletReturnCode.Success)
            {
                return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = "刪除實體卡面資料失敗," + response.Message,
                    });
            }
            else
            {
                return Json(new { IsSuccess = true });
            }
        }

        /// <summary>
        /// 新增信用卡卡樣資料
        /// </summary>
        /// <returns>空的卡樣資料</returns>
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        [HttpGet]
        public ActionResult AddCreditCardCategory()
        {
            AddCreditCardCategoryModel returnValue = new AddCreditCardCategoryModel();
            return View(returnValue);
        }

        /// <summary>
        /// 新增信用卡卡樣資料
        /// </summary>
        /// <param name="request">要新增的卡樣資料</param>
        /// <returns></returns>
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        [HttpPost]
        public ActionResult AddCreditCardCategory(AddCreditCardCategoryModel request)
        {
            #region validate
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(request.UploadImageUrl.FileName))
                {
                    string extension = Path.GetExtension(request.UploadImageUrl.FileName).ToLower();
                    if (extension != ".jpg" && extension != ".png") ModelState.AddModelError("UploadImageUrl", "只可上傳jpg或png格式的圖檔");
                    if (request.UploadImageUrl.ContentLength > 300 * 1024) ModelState.AddModelError("UploadImageUrl", "檔案大小不得超過300kb");
                    ImgData imgUploadData = SkmFacade.CheckImgType(request.UploadImageUrl, 345, 216, "SKMCreditCardCategory", Guid.NewGuid().ToString());
                    if (!imgUploadData.IsSizePass)
                        ModelState.AddModelError("UploadImageUrl", "上傳圖片尺寸限制為345*216px");
                }
                else ModelState.AddModelError("UploadImageUrl", "請選擇卡樣圖檔");
            }
            if (!ModelState.IsValid)
            {

                TempData["validateResult"] = new JsonSerializer().Serialize(ModelState.Where(item => item.Value.Errors.Any()).Select(item => { return new { Key = item.Key, Message = string.Join("<br/>", item.Value.Errors.Select(error => { return error.ErrorMessage; })) }; }));
                return View(request);
            }
            #endregion
            request.UploadImageUrl.InputStream.SeekToBegin();
            WalletCreditCardCategoryRequest input = new WalletCreditCardCategoryRequest()
            {
                Id = 0,
                Bank = request.Bank,
                CardCategory = request.CategoryNo,
                CardName = request.Name,
                UploadImageStream = request.UploadImageUrl.InputStream.ReadAllBytes(),
                FileName = request.UploadImageUrl.FileName,
            };
            WalletServerResponse<WalletCreditCardCategory> response = SkmWalletServerUtility.AddCreditCardCategory(input);
            if (response.Code != WalletReturnCode.Success)
            {
                //TempData["errorMessage"] = result.Message;
                TempData["validateResult"] = "[{\"Key\":\"CategoryNo\",\"Message\":\"卡片識別碼重複，請重新輸入\"}]";
                return View(request);
            }
            else
            {
                return RedirectToAction("CreditCardCategoryList", new { bankNo = input.Bank });
            }
        }

        /// <summary>
        /// 依銀行代號取得銀行資訊
        /// </summary>
        /// <param name="bankNo">銀行代號</param>
        /// <returns>銀行資訊</returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public JsonResult GetBankInfoByBankNo(string bankNo)
        {
            int tryParseInt = 0;
            if (string.IsNullOrEmpty(bankNo) || bankNo.Length != 3 || !int.TryParse(bankNo, out tryParseInt))
                return Json(new { isFind = false, bankNo = bankNo, bankName = "" });
            BankInfo bankInfo = SkmFacade.GetBankInfoByBankNo(bankNo);
            if (bankInfo == null) return Json(new { isFind = false, bankNo = bankNo, bankName = "" });
            else return Json(new { isFind = bankInfo.IsLoaded, bankNo = bankInfo.BankNo, bankName = bankInfo.BankName });
        }

        /// <summary>
        /// 依卡樣代號取得信用卡卡樣資訊
        /// </summary>
        /// <param name="id">銀行代號</param>
        /// <returns>信用卡卡樣資訊</returns>
        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult EditCreditCardCategory(int id)
        {
            WalletGetCreditCardCategoryByIdRequest request = new WalletGetCreditCardCategoryByIdRequest()
            {
                Id = id
            };
            WalletServerResponse<WalletGetCreditCardCategory> result = SkmWalletServerUtility.GetCreditCardCategoryById(request);
            if (result.Code != WalletReturnCode.Success || result.Data == null || result.Data.Id != id)
            {
                return RedirectToAction("CreditCardCategoryList");
            }
            EditCreditCardCategoryModel editCreditCardCategory = new EditCreditCardCategoryModel()
            {
                Id = result.Data.Id,
                Bank = result.Data.Bank,
                CategoryNo = result.Data.CardCategory,
                Name = result.Data.CardName,
                CardImageUrl = result.Data.ImageUrl,
            };
            return View(editCreditCardCategory);
        }

        /// <summary>
        /// 依卡樣代號取得信用卡卡樣資訊
        /// </summary>
        /// <param name="id">銀行代號</param>
        /// <returns>信用卡卡樣資訊</returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult EditCreditCardCategory(EditCreditCardCategoryModel request)
        {
            #region validate
            if (ModelState.IsValid)
            {
                if (request.UploadImageUrl != null && !string.IsNullOrEmpty(request.UploadImageUrl.FileName))
                {
                    string extension = Path.GetExtension(request.UploadImageUrl.FileName).ToLower();
                    if (extension != ".jpg" && extension != ".png") ModelState.AddModelError("UploadImageUrl", "只可上傳jpg或png格式的圖檔");
                    if (request.UploadImageUrl.ContentLength > 300 * 1024) ModelState.AddModelError("UploadImageUrl", "檔案大小不得超過300kb");
                    ImgData imgUploadData = SkmFacade.CheckImgType(request.UploadImageUrl, 345, 216, "SKMCreditCardCategory", Guid.NewGuid().ToString());
                    if (!imgUploadData.IsSizePass)
                        ModelState.AddModelError("UploadImageUrl", "上傳圖片尺寸限制為345*216px");
                }
                else if (string.IsNullOrEmpty(request.CardImageUrl)) ModelState.AddModelError("UploadImageUrl", "請選擇卡樣圖檔");
            }
            if (!ModelState.IsValid)
            {
                TempData["validateResult"] = new JsonSerializer().Serialize(ModelState.Where(item => item.Value.Errors.Any()).Select(item => { return new { Key = item.Key, Message = string.Join("<br/>", item.Value.Errors.Select(error => { return error.ErrorMessage; })) }; }));
                return View(request);
            }
            #endregion
            WalletCreditCardCategoryRequest input = new WalletCreditCardCategoryRequest()
            {
                Id = request.Id,
                Bank = request.Bank,
                CardName = request.Name,
                ImageUrl = request.CardImageUrl,
                CardCategory = request.CategoryNo,
            };
            if (request.UploadImageUrl != null && !string.IsNullOrEmpty(request.UploadImageUrl.FileName))
            {
                request.UploadImageUrl.InputStream.SeekToBegin();
                input.UploadImageStream = request.UploadImageUrl.InputStream.ReadAllBytes();
                input.FileName = request.UploadImageUrl.FileName;
            }
            WalletServerResponse<WalletCreditCardCategory> result = SkmWalletServerUtility.EditCreditCardCategory(input);
            if (result.Code != WalletReturnCode.Success)
            {
                //TempData["errorMessage"] = result.Message;
                TempData["validateResult"] = "[{\"Key\":\"CategoryNo\",\"Message\":\"卡片識別碼重複，請重新輸入\"}]";
                return View(request);
            }
            else
            {
                return RedirectToAction("CreditCardCategoryList", new { bankNo = input.Bank });
            }
        }
        #endregion

        #region SKMAPP分享連結

        public ActionResult SKMShareDeal(string bid)
        {
            Guid gbid;
            Guid.TryParse(bid, out gbid);
            var deal = SkmFacade.GetDealByBid(gbid, ApiImageSize.XS);

            return View(deal);
        }

        [HttpGet]
        public ActionResult GetShareDeal()
        {
            return View();
        }

        #endregion

        [HttpGet]
        public ActionResult SkmInstallmentInfo(int price = 0)
        {
            var model = SkmFacade.GetInstallmentInfoByPrice(price);

            return View(model);
        }               

        #region 信用卡分期管理

        /// <summary>
        /// 分期活動列表頁面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult InstallEventList()
        {
            return View();
        }
        /// <summary>
        /// 取得分期活動列表
        /// </summary>
        /// <param name="model">搜尋條件模組</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult GetInstallEventList(GetInstallEventListModel model)
        {
            var eventList = _skmef.GetViewSkmInstallEventListByEventTitle(model.SearchTitle);

            if (eventList.Any())
            {
                model.SkmInstallEvents = eventList.OrderBy(model.SortBy, model.SortType == "desc").Skip((model.PageIndex - 1) * model.PageSize)
                    .Take(model.PageSize).Select(x => new SkmInstallEventViewModel(x)).ToList();
                model.TotalCount = eventList.Count();
            }

            return Content(JsonConvert.SerializeObject(model), "application/json");
        }

        /// <summary>
        /// 切換分期活動狀態(上架/下架)
        /// </summary>
        /// <param name="id">活動編號</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult ToggleInstallEventStatus(int id)
        {
            var installEvent = _skmef.GetSkmInstallEventById(id);

            if (installEvent != null)
            {
                installEvent.IsAvailable = !installEvent.IsAvailable;
                installEvent.ModifyId = User.Identity.Name;
                installEvent.ModifyTime = DateTime.Now;
                _skmef.AddOrUpdateSkmInstallEvent(installEvent);
                SkmFacade.RefreshInstallmentCache();
            }

            return Content(JsonConvert.SerializeObject(installEvent), "application/json");
        }

        /// <summary>
        /// 分期活動資訊(新增/編輯)頁面
        /// </summary>
        /// /// <param name="id">活動編號</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult InstallEventInfo(int id = 0)
        {
            var installEvent = _skmef.GetSkmInstallEventById(id) ?? new SkmInstallEvent();

            var now = DateTime.Now; //上架時禁止編輯
            if (installEvent.IsAvailable && now >= installEvent.StartDate && now <= installEvent.EndDate)
            {
                return RedirectToAction("InstallEventList");
            }

            var model = new SaveInstallEventInfoModel(installEvent);

            return View(model);
        }

        /// <summary>
        /// 新增/編輯 分期活動
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SaveInstallEventInfo(SaveInstallEventInfoModel model)
        {
            var apiResult = new ApiResult();

            if (!ModelState.IsValid)
            {
                apiResult.Code = ApiResultCode.InputError;
                apiResult.Message = "資料驗證錯誤";
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            var sd = ConvertDateAndTime(model.StartDate, model.StartTime).Value;
            var ed = ConvertDateAndTime(model.EndDate, model.EndTime).Value;

            var checkDateRange = _skmef.GetSkmInstallEventList().Any(x => x.Id != model.Id && (
                                    (x.StartDate <= sd && x.EndDate > sd) ||
                                    (x.StartDate < ed && x.EndDate >= ed) ||
                                    (x.StartDate >= sd && x.EndDate <= ed)));
            if (checkDateRange)
            {
                apiResult.Code = ApiResultCode.Error;
                apiResult.Message = "該時段已設定分期資料";
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            var installEvent = _skmef.GetSkmInstallEventById(model.Id) ?? new SkmInstallEvent();

            installEvent.Title = model.Title;
            installEvent.StartDate = sd;
            installEvent.EndDate = ed;
            if (installEvent.Id == 0)
            {
                installEvent.CreateId = User.Identity.Name;
                installEvent.CreateTime = DateTime.Now;
                installEvent.IsAvailable = true;
            }
            else
            {
                installEvent.ModifyId = User.Identity.Name;
                installEvent.ModifyTime = DateTime.Now;
            }

            var isSucceed = _skmef.AddOrUpdateSkmInstallEvent(installEvent);

            apiResult = new ApiResult()
            {
                Code = isSucceed ? ApiResultCode.Success : ApiResultCode.SaveFail,
                Data = installEvent,
                Message = isSucceed ? "" : "儲存失敗"
            };

            if (isSucceed)
            {
                SkmFacade.RefreshInstallmentCache();
            }

            return Content(JsonConvert.SerializeObject(apiResult), "application/json");
        }

        /// <summary>
        /// 取得某活動底下的銀行列表
        /// </summary>
        /// <param name="id">活動編號</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult GetInstallEventBankList(int id)
        {
            var viewInstallBank = _skmef.GetViewSkmInstallEventBankByEventId(id).GroupBy(x => x.EventBankId, x => x);

            var bankInfosDic = VourcherFacade.BankInfoGetMainList().GetList()//先GroupBy防止Key重複
                .GroupBy(x => x.BankNo, x => x.BankName).ToDictionary(x=>x.Key,x=>x.FirstOrDefault());

            var model = viewInstallBank.Select(x =>
                new EventBankListViewModel()
                {
                    Id = x.Key ?? 0,
                    BankNo = x.Select(y => y.BankNo).FirstOrDefault(),
                    BankName = bankInfosDic[x.Select(y => y.BankNo).FirstOrDefault()],
                    StartDate = x.Select(y => ((DateTime)y.BankStartDate).ToString("yyyy/MM/dd HH:mm")).FirstOrDefault(),
                    EndDate = x.Select(y => ((DateTime)y.BankEndDate).ToString("yyyy/MM/dd HH:mm")).FirstOrDefault(),
                    Periods = string.Join("、", x.Where(y => y.PeriodIsAvailable ?? false).Select(y => y.InstallPeriod ?? 0).ToList()),
                    IsCoBranded = x.Select(y => y.IsCoBranded ?? false).FirstOrDefault(),
                    ModifyTime = x.Select(y => y.BankModifyTime != null
                        ? ((DateTime)y.BankModifyTime).ToString("yyyy/MM/dd HH:mm")
                        : ((DateTime)y.BankCreateTime).ToString("yyyy/MM/dd HH:mm")).FirstOrDefault()
                }).OrderByDescending(x => x.ModifyTime).ToList();

            return Content(JsonConvert.SerializeObject(model), "application/json");
        }

        /// <summary>
        /// 刪除銀行(變更isDel)
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult DeleteEventBank(int id)
        {
            var apiResult = new ApiResult()
            {
                Code = ApiResultCode.Success,
            };

            var bank = _skmef.GetSkmInstallEventBankById(id);

            if (bank == null)
            {
                apiResult.Code = ApiResultCode.DataNotFound;
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            bank.IsDel = true;

            if (!_skmef.AddOrUpdateSkmInstallEventBank(bank))
            {
                apiResult.Code = ApiResultCode.SaveFail;
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            apiResult.Data = bank;
            SkmFacade.RefreshInstallmentCache();

            return Content(JsonConvert.SerializeObject(apiResult), "application/json");
        }

        /// <summary>
        /// 銀行分期資訊頁面
        /// </summary>
        /// <param name="id">銀行Id</param>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult InstallBankPeriodInfo(int eventId = 0, int id = 0)
        {
            ViewBag.BankInfos = VourcherFacade.BankInfoGetMainList().GetList();

            var bankPeriodInfo = _skmef.GetViewSkmInstallEventBankByEventBankId(id);

            var now = DateTime.Now; //上架時禁止編輯
            var firstInfo = bankPeriodInfo.FirstOrDefault();
            if (firstInfo != null && firstInfo.EventIsAvailable && now >= firstInfo.EventStartDate && now <= firstInfo.EventEndDate)
            {
                return RedirectToAction("InstallEventList");
            }

            SaveInstallBankPeriodInfoModel model;
            if (bankPeriodInfo.Any())
            {
                model = new SaveInstallBankPeriodInfoModel(bankPeriodInfo);

            }
            else
            {
                var installEvent = _skmef.GetSkmInstallEventById(eventId);
                if (installEvent == null)
                {
                    return RedirectToAction("InstallEventList");
                }
                model = new SaveInstallBankPeriodInfoModel(installEvent);
            }

            return View(model);
        }

        /// <summary>
        /// 新增/修改 銀行分期資訊
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "SkmPay, Administrator, ME2O, Production")]
        public ActionResult SaveInstallBankPeriodInfo(SaveInstallBankPeriodInfoModel model)
        {
            var apiResult = new ApiResult();

            if (model.EventId == 0)
            {
                apiResult.Code = ApiResultCode.InputError;
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            var eventBank = _skmef.GetSkmInstallEventBankById(model.Id) ?? new SkmInstallEventBank();

            eventBank.BankNo = model.BankNo;
            eventBank.EventId = model.EventId;
            eventBank.StartDate = ConvertDateAndTime(model.StartDate, model.StartTime).Value;
            eventBank.EndDate = ConvertDateAndTime(model.EndDate, model.EndTime).Value;
            eventBank.IsCoBranded = model.IsCoBranded;
            eventBank.Seq = 99999;

            if (eventBank.Id == 0)
            {
                eventBank.CreateId = User.Identity.Name;
                eventBank.CreateTime = DateTime.Now;
            }
            else
            {
                eventBank.ModifyId = User.Identity.Name;
                eventBank.ModifyTime = DateTime.Now;
            }

            if (!_skmef.AddOrUpdateSkmInstallEventBank(eventBank))
            {
                apiResult.Code = ApiResultCode.SaveFail;
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            if (!_skmef.DeleteSkmInstallEventPeriodsByEventBankId(model.Id))
            {
                apiResult.Code = ApiResultCode.SaveFail;
                return Content(JsonConvert.SerializeObject(apiResult), "application/json");
            }

            var preInsertList = new List<SkmInstallEventPeriod>();

            foreach (var item in model.InstallBankPeriodList)
            {
                preInsertList.Add(new SkmInstallEventPeriod()
                {
                    Amount = item.Amount,
                    EventBankId = eventBank.Id,
                    InstallPeriod = item.InstallPeriod,
                    InterestRate = item.InterestRate,
                    PayFee = item.PayFee,
                    IsAvailable = item.IsAvailable
                });
            }

            var isSucceed = _skmef.BulkInsertSkmInstallEventPeriods(preInsertList);

            apiResult = new ApiResult()
            {
                Code = isSucceed ? ApiResultCode.Success : ApiResultCode.SaveFail,
                Data = preInsertList
            };

            if (isSucceed)
            {
                SkmFacade.RefreshInstallmentCache();
            }

            return Content(JsonConvert.SerializeObject(apiResult), "application/json");
        }

        #endregion

    }

    public class DealCategoryModel
    {
        public Category MainCategory { get; set; }
        public Category SubCategory { get; set; }

        public List<Category> SubCategoryList { get; set; }
    }

    #region 客群Object

    /// <summary> 客群資訊 </summary>
    public class UserGroupViewModel
    {
        public int Id { get; set; }
        /// <summary> 適用分店Guid </summary>
        public Guid SellerGuid { get; set; }
        /// <summary> 適用分店名稱 </summary>
        public string SellerName { get; set; }
        /// <summary> 客群名稱 </summary>
        public string Name { get; set; }
        /// <summary> 上架日期 </summary>
        public DateTime StartDate { get; set; }
        /// <summary> 下架日期 </summary>
        public DateTime EndDate { get; set; }
        public string StartTimeHour { get; set; }
        public string EndTimeHour { get; set; }
        public string StartTimeMinute { get; set; }
        public string EndTimeMinute { get; set; }
        /// <summary> 匯入客群檔案名稱 </summary>
        public string FileName { get; set; }
        /// <summary> 匯入客群檔案 </summary>
        public HttpPostedFileBase File { get; set; }
        public int Page { get; set; }
    }


    #endregion

    #region 策展標籤 Object

    /// <summary> 策展標籤管理回應資訊 </summary>
    [Serializable]
    public class ManageTagTypeResponse
    {
        /// <summary> 標籤資訊清單 </summary>
        public List<ExternalDealIcon> ExternalDealTags { get; set; }
        /// <summary> 基本標籤樣式清單(Json內容) </summary>
        public string IconStyles { get; set; }
        /// <summary> 分頁相關資訊 </summary>
        public PageInformation PageInfo { get; set; }
    }

    /// <summary> 新增策展標籤請求資訊 </summary>
    public class AddTagRequest
    {
        /// <summary> 標籤名稱 </summary>
        public string TagName { get; set; }
        /// <summary> 標籤樣式編號 </summary>
        public int StyleID { get; set; }
    }

    /// <summary> 新增策展標籤請求資訊 </summary>
    public class EditTagRequest
    {
        /// <summary> 標籤編號 </summary>
        public int TagID { get; set; }
        /// <summary> 標籤名稱 </summary>
        public string TagName { get; set; }
        /// <summary> 標籤樣式編號 </summary>
        public int StyleID { get; set; }
    }

    /// <summary> 分頁相關資訊 </summary>
    public class PageInformation
    {
        /// <summary> 目前頁數 </summary>
        public int NowPage { get; set; }
        /// <summary> 每頁筆數 </summary>
        public int pageSize { get; set; }
        /// <summary> 總頁數 </summary>
        public int TotalPageCount { get; set; }
        /// <summary> 總筆數 </summary>
        public int TotalDataCount { get; set; }
    }
    #endregion

    #region 策展代號 Object

    /// <summary> 策展代號管理回應資訊 </summary>
    [Serializable]
    public class ExhibitionCodeResponse
    {
        /// <summary> 策展代號資訊清單 </summary>
        public List<ExhibitionCode> ExhibitionCodeInfos { get; set; }
        /// <summary> 分頁相關資訊 </summary>
        public PageInformation PageInfo { get; set; }
    }
    #endregion

    #region 策展排序

    public class ExhibitionSortResponse
    {
        public string QuerySeller { get; set; }
        public int QueryExhibitionId { get; set; }
        public int QueryCategoryId { get; set; }
        public DateTime BeginDate { get; set; }
        public string SelectSellerName { get; set; }
        public string SelectExhibitionName { get; set; }
        public string SelectCategoryName { get; set; }
        /// <summary>
        /// 分店ID(seller_guid), 分店名稱
        /// </summary>
        public Dictionary<string, string> Sellers { get; set; }
        /// <summary>
        /// 策展ID, 策展名稱
        /// </summary>
        public Dictionary<int, string> ExhibitionCol { get; set; }
        /// <summary>
        /// 分類ID, 分類名稱
        /// </summary>
        public Dictionary<int, string> CategoryCol { get; set; }

        public List<ExhibitionSortData> DealTimeSlotData { get; set; }
    }

    public class ExhibitionSortData
    {
        public DateTime DealDate { get; set; }
        public List<ExhDealTimeSlot> Deals { get; set; }
    }

    public class ExhDealTimeSlot
    {
        [JsonProperty("timeSlotId")]
        public int TimeSlotId { get; set; }
        [JsonProperty("externalGuid")]
        public Guid ExternalGuid { get; set; }
        [JsonProperty("itemName")]
        public string ItemName { get; set; }
        [JsonProperty("dealDate")]
        public DateTime DealDate { get; set; }
        [JsonProperty("sequence")]
        public int Sequence { get; set; }
        [JsonProperty("isHidden")]
        public bool IsHidden { get; set; }
        [JsonProperty("dealUrl")]
        public string DealUrl { get; set; }
    }

    #endregion 策展排序

    #region 策展 Object
    /// <summary> 策展活動排序請求資訊 </summary>
    public class SortExhibitionEventRequest
    {
        /// <summary> 策展活動編號 </summary>
        public int Id { get; set; }
        /// <summary> 排序 </summary>
        public int Sort { get; set; }
    }

    /// <summary> 策展清單請求資訊 </summary>
    public class ExhibitionEventRequest
    {
        private string _sellerGuid = null;
        private int _nowPage = 1;

        /// <summary> 館代號 </summary>
        public string SellerGuid
        {
            get { return _sellerGuid; }
            set { _sellerGuid = value; }
        }

        /// <summary> 目前頁數 </summary>
        public int NowPage
        {
            get { return _nowPage; }
            set { _nowPage = value; }
        }
    }

    /// <summary> 策展清單回應資訊 </summary>
    public class ExhibitionEventResponse
    {
        /// <summary> 策展資訊清單 </summary>
        public List<ExhibitionEvent> ExhibitionEventInfos { get; set; }
        /// <summary> 符合客群的策展Ids </summary>
        public List<int> HaveUserGroupExhibitionEventIds { get; set; }
        /// <summary> 各館代號清單 </summary>
        public List<SelectListItem> Sellers { get; set; }
        /// <summary> 分頁相關資訊 </summary>
        public PageInformation PageInfo { get; set; }

    }

    /// <summary> 設定策展資訊 </summary>
    [Serializable]
    public class SetExhibitionEvent
    {
        /// <summary> 策展編號 </summary>
        public int Id { get; set; }
        /// <summary> 策展代號 </summary>
        public long ExhibitionCodeId { get; set; }
        /// <summary> 策展名稱 </summary>
        public string Subject { get; set; }
        /// <summary> 策展類型 </summary>
        public SkmExhibitionEventType Type { get; set; }
        /// <summary> 信用卡別 </summary>
        public SkmCardLevelType[] RecommendSkmCardLevels { get; set; }
        /// <summary> 適用分店編號 </summary>
        public Guid[] Store { get; set; }
        /// <summary>
        /// 客群
        /// </summary>
        public string[] UserGroup { get; set; }
        /// <summary> 策展內容 </summary>
        public string EventContent { get; set; }
        /// <summary> 注意事項 </summary>
        public string EventRule { get; set; }
        /// <summary> 上架日期 </summary>
        public string StartDate { get; set; }
        /// <summary> 上架時間 </summary>
        public string StartTime { get; set; }
        /// <summary> 下架日期 </summary>
        public string EndDate { get; set; }
        /// <summary> 下架時間 </summary>
        public string EndTime { get; set; }
        /// <summary> 商品分類資訊清單 </summary>
        public ExhibitionEventCategory[] Categorys { get; set; }
        /// <summary> Banner 活動連結 </summary>
        public string BannerLink { get; set; }
        /// <summary> Banner 活動圖片 </summary>
        public HttpPostedFileBase BannerPic { get; set; }
        /// <summary> Banner 活動圖片檔名 </summary>
        public string BannerPicFileName { get; set; }
        /// <summary> 既有活動資料清單 </summary>
        public ExistEvent[] ExistEvents { get; set; }
        /// <summary>
        /// 是否為總公司權限
        /// </summary>
        public bool IsSkmHqPower { get; set; }
    }

    /// <summary> 商品分類資訊 </summary>
    [Serializable]
    public class ExhibitionEventCategory
    {
        /// <summary> 分類名稱 </summary>
        public string Name { get; set; }
        /// <summary> 商品分類檔次資訊清單 </summary>
        public ExhibitionEventCategoryExternalDeal[] ExternalDeals { get; set; }
    }

    /// <summary> 商品分類檔次資訊 </summary>
    [Serializable]
    public class ExhibitionEventCategoryExternalDeal
    {
        /// <summary> 檔次編號 </summary>
        public Guid ExternalDealGuid { get; set; }
        /// <summary> 是否為主打商品 </summary>
        public bool IsMainDeal { get; set; }
    }

    /// <summary> 既有活動資料 </summary>
    public class ExistEvent
    {
        /// <summary> 活動網址 </summary>
        public string EventUrl { get; set; }
        /// <summary> 活動圖檔 </summary>
        public HttpPostedFileBase EventImage { get; set; }
        /// <summary> 排序 </summary>
        public int Seq { get; set; }
        /// <summary> 圖檔名稱 </summary>
        public string EventImageName { get; set; }
    }

    /// <summary> 依條件取得策展檔次資訊清單請求資訊 </summary>
    public class GetExternalDealsRequest
    {
        /// <summary> 策展代號 </summary>
        [Required]
        public long SelectedExhibitionCode { get; set; }
        /// <summary> 上架日期 </summary>
        public DateTime StartDate { get; set; }
        /// <summary> 下架日期 </summary>
        public DateTime EndDate { get; set; }
        /// <summary> 適用分店 </summary>
        public List<string> Stores { get; set; }
        /// <summary> 策展類型 </summary>
        public SkmExhibitionEventType Type { get; set; }
        /// <summary> 信用卡別 </summary>
        public SkmCardLevelType[] RecommendSkmCardLevels { get; set; }
    }

    public class GetUserGroupListRequest
    {
        public int EventId { get; set; }
        /// <summary> 上架日期 </summary>
        public DateTime StartDate { get; set; }
        /// <summary> 下架日期 </summary>
        public DateTime EndDate { get; set; }
        /// <summary> 適用分店 </summary>
        public List<string> Stores { get; set; }
    }

    /// <summary> 策展檔次資訊 </summary>
    public class ExhibitionEventExternalDeal
    {
        /// <summary> 檔次名稱 </summary>
        public string EventName { get; set; }
        /// <summary> 檔次編號 </summary>
        public string EventGuid { get; set; }
        /// <summary> 有效起始日期 </summary>
        public string StartDate { get; set; }
        /// <summary> 有效結束日期 </summary>
        public string EndDate { get; set; }
    }
    #endregion

    #region 其他 Object

    [Serializable]
    public class StoreData
    {
        public StoreData()
        {
            Store = new List<SelectData>();
            Shop = new List<SelectData>();
        }

        public List<SelectData> Store { get; set; }
        public List<SelectData> Shop { get; set; }
    }

    public class SelectData
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool IsDefault { get; set; }
    }

    public class BeaconList
    {
        public string BeaconId { get; set; }
        public string DeviceName { get; set; }
    }

    public class SkmDealListModel
    {
        public IEnumerable<ViewExternalDeal> List { get; set; }
        public string Store { get; set; }
        public int Shop { get; set; }
        public int Page { get; set; }
        public int LastPage { get; set; }
        public int DealCount { get; set; }
        public int Filter { get; set; }
        public int Category { get; set; }
        public int Tag { get; set; }
        public bool IsSkmUser { get; set; }
        public List<int> SkmHqCategories { get; set; }
        public bool SkmHqPower { get; set; }
        public bool PrizeList { get; set; }
        public string SearchTitle { get; set; }
    }

    public class ViewSKMAppDefaultStyle
    {
        public Guid? SellrtGuid { get; set; }
        public string Title { get; set; }
        public DateTime SDate { get; set; }
        public DateTime EDate { get; set; }
        public Guid? Guid { get; set; }
        public int? status { get; set; }
        public int Sort { get; set; }
    }

    [Serializable()]
    public class SkmPayDealInfoViewModel : SkmDealModel
    {
        public SkmPayDealInfoViewModel() : base()
        {
            StoreDisplayNames = new List<SkmStoreDisplayNameInfoModel>();
        }
        public List<SkmStoreDisplayNameInfoModel> StoreDisplayNames { get; set; }
    }

    [Serializable()]
    public class SkmDealModel
    {
        public string Guid { get; set; }
        public string SellerGuid { get; set; }
        public ActiveType ActiveType { get; set; }
        public string Title { get; set; }
        public string ProductCode { get; set; }
        public string ItemNo { get; set; }
        public string SpecialItemNo { get; set; }
        public int ItemOrigPrice { get; set; }
        public int SkmOrigPrice { get; set; }
        public int DiscountType { get; set; }
        public int Discount { get; set; }
        public int OrderTotalLimit { get; set; }
        public string Remark { get; set; }
        public string Description { get; set; }
        public string Introduction { get; set; }
        public int[] Category { get; set; }
        public string ShowCategory { get; set; }
        public int[] Tags { get; set; }
        public string BusinessHourOrderDateS { get; set; }
        public string BusinessHourOrderDateE { get; set; }
        public string BusinessHourOrderTimeS { get; set; }
        public string BusinessHourOrderTimeE { get; set; }
        public string BusinessHourDeliverDateS { get; set; }
        public string BusinessHourDeliverDateE { get; set; }
        public string BusinessHourDeliverTimeS { get; set; }
        public string BusinessHourDeliverTimeE { get; set; }
        public string SettlementTime { get; set; }
        public string[] Stores { get; set; } //seller 
        public string[] Shoppes { get; set; } //store
        public int BeaconType { get; set; }
        public string BeaconMessage { get; set; }
        public int Status { get; set; }
        public string BackUrl { get; set; }
        public bool IsAllowEdit { get; set; }
        public bool IsEqualTime { get; set; }
        public string ImagePath { get; set; }
        public string AppDealPic { get; set; }
        public string TempImagePath { get; set; }
        public string TempAppDealPic { get; set; }
        public bool IsAllowStoreEdit { get; set; }
        public int? Buy { get; set; }
        public int? Free { get; set; }
        public ExternalDealRelationItemCollection ExternalDealRelationItemList { get; set; }
        public string RelationItemJsonList { get; set; }
        public bool BuyGetFreeIsShow { get; set; }
        public bool NoInvoicesIsShow { get; set; }
        public List<int> SkmHqCategories { get; set; }
        public int SkmDealDisableCategory { get; set; }
        public int UserId { get; set; }
        public string SourceSeller { get; set; }
        public decimal TaxRate { get; set; }
        public bool IsCostCenterHq { get; set; }
        /// <summary>
        /// 適用分店全選
        /// </summary>
        public bool IsAllStore { get; set; }
        public SkmDealModel()
        {
            this.Stores = new string[] { };
            this.Shoppes = new string[] { };
            this.Category = new int[] { };
            this.Tags = new int[] { };
            IsRepeatPurchase = IsHqDeal = IsPrize = IsSkmPay = false;
            AllowEditCrm = true;
            ComboDeal = new List<SkmComboDeal>();
            CostCenter = new List<CostCenter>();
        }
        //3.7版燒點新增
        public int DealVersion { get; set; }
        public bool IsBurningEvent { get; set; }
        public int BurningPoint { get; set; }
        public string ExchangeNo { get; set; }
        public string CostCenterTemp { get; set; }
        public List<CostCenter> CostCenter { get; set; }
        public bool ActivityStatus { get; set; }
        public bool IsExchange { get; set; }
        public string BurningEventId { get; set; }
        public bool IsPrize { get; set; }
        public bool IsHqDeal { get; set; }
        public string GiftWalletId { get; set; }
        public int GiftPoint { get; set; }
        public int DealIconId { get; set; }
        public string ExhibitionData { get; set; }
        public bool IsCrmDeal { get; set; }
        public bool AllowEditCrm { get; set; }
        public bool IsSkmPay { get; set; }
        public string ComboDealTemp { get; set; }
        public List<SkmComboDeal> ComboDeal { get; set; }
        public int ExcludingTaxPrice { get; set; }
        public bool IsRepeatPurchase { get; set; }
        /// <summary> 檔次重複兌換類型 </summary>
        public SkmDealRepeatPurchaseType RepeatPurchaseType { get; set; }
        /// <summary> 檔次重複兌換數量 </summary>
        public int RepeatPurchaseCount { get; set; }
        /// <summary> 兌換地點類型 </summary>
        public SkmDealExchangeType ExchangeType { get; set; }
        /// <summary>
        /// 是否允許分期
        /// </summary>
        public bool EnableInstall { get; set; }
        //阿里巴巴用
        public long itemid { get; set; }
        //阿里巴巴用
        public string spuid { get; set; }
        //阿里巴巴貨號
        public string skuid { get; set; }
        //阿里巴巴應稅免稅
        public string attrVal { get; set; }
    }

    [Serializable]
    public class SkmComboDeal
    {
        public SkmComboDeal()
        {
        }

        public SkmComboDeal(ExternalDealCombo deal)
        {
            ShopCode = deal.ShopCode;
            ShopName = deal.ShopName;
            Qty = deal.Qty.ToString();
            var s = SellerFacade.SellerGet(deal.SellerGuid);
            Order = string.Format("{0},{1}", LocationFacade.GetGeographyByCoordinate(s.Coordinate).Lat.ToString()
                , LocationFacade.GetGeographyByCoordinate(s.Coordinate).Long.ToString());
        }

        public string ShopCode { get; set; }
        public string ShopName { get; set; }
        public string Qty { get; set; }
        public string Order { get; set; }
    }

    [Serializable]
    public class CostCenter
    {
        public string StoreCode { get; set; }
        public string StoreName { get; set; }
        public string SelectCenterCode { get; set; }
        public string MarketCostCenterCode { get; set; }
        public string SellCostCenterCode { get; set; }
        public string OrderNo { get; set; }
        public string MainStoreCode { get; set; }
    }

    public class RelationItem
    {
        public string ProductCode { get; set; }
        public string ItemNo { get; set; }
        public string ItemName { get; set; }
        public bool IsFree { get; set; }
    }

    public class Shoppes
    {
        public string Name { get; set; }
        public string StoreGuid { get; set; }
        public string ShopName { get; set; }
        public string ShopCode { get; set; }
        public string SellerGuid { get; set; }
        public bool IsShoppe { get; set; }
        public bool IsShow { get; set; }
        public bool IsExchange { get; set; }
        public string MarketCostCenterCode { get; set; }
        public string SellCostCenterCode { get; set; }
        public Guid ParentSellerGuid { get; set; }
        public string MainShopCode { get; set; }
    }

    public class SkmDealsGetModel
    {
        public Dictionary<string, string> Deals { get; set; }
        public List<SkmDealInfo> DealsInfo { get; set; }
    }

    public class SkmDealInfo
    {
        public Guid Bid { get; set; }
        public DateTime OrderDateStart { get; set; }
        public DateTime OrderDateEnd { get; set; }
    }

    public class SkmPayBannerListResponse
    {
        public SkmPayBannerListResponse() { }

        public SkmPayBannerListResponse(SkmPayBanner ba)
        {
            Id = ba.Id;
            Name = ba.Name;
            StartTime = ba.StartTime.ToString("yyyy/MM/dd HH:mm:ss");
            EndTime = ba.EndTime.ToString("yyyy/MM/dd HH:mm:ss");
            Seq = ba.Seq;
            IsHidde = ba.IsHidden;
            if (ba.IsHidden)
            {
                Status = "隱藏中";
            }
            else if (DateTime.Now.IsBetween(ba.StartTime, ba.EndTime))
            {
                Status = "已上檔";
            }
            else if (DateTime.Now < ba.StartTime)
            {
                Status = "待上檔";
            }
            else if (DateTime.Now > ba.EndTime)
            {
                Status = "已下檔";
            }
            else
            {
                Status = string.Empty;
            }
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("start")]
        public string StartTime { get; set; }
        [JsonProperty("end")]
        public string EndTime { get; set; }
        [JsonProperty("seq")]
        public int Seq { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("isHidde")]
        public bool IsHidde { get; set; }
    }

    #endregion

    #region 阿里巴巴
    public class AlibabaItemResult {
        public string code { get; set; }
        public bool success { get; set; }
        public AlibabaItemInfo result { get; set; }
    }
    public class AlibabaItemInfo {
        public ItemInfo itemInfo { get; set; }
        public List<SkuInfo> skuInfoList { get; set; }
        public ItemDetailInfo itemDetailInfo { get; set; }
    }
    public class ItemInfo
    {
        public int tenantId { get; set; }
        public string tenantIdLong { get; set; }
        public ItemInfo_Extra extra { get; set; }
        public string name { get; set; }
        public int[] categoryIds { get; set; }
        public string spuId { get; set; }
        public string mainImage { get; set; }
        public int businessType { get; set; }
        public List<long> skuIdList { get; set; }
        public int status { get; set; }
        public List<ItemInfo_OtherAttributes> otherAttributes { get; set; }

    }
    public class SkuInfo
    {
        public int tenantId { get; set; }
        public string tenantIdLong { get; set; }
        public string barcode { get; set; }
        public string skuCode { get; set; }
        public SkuInfo_Extra extra { get; set; }
        public long originalPrice { get; set; }
        public string image { get; set; }
    }
    public class ItemDetailInfo
    {
        public List<itemDetailInfo_Detail> pcDetail { get; set; }
        public long itemId { get; set; }
    }

    public class ItemInfo_Extra
    {
        public string categoryList { get; set; }
        public string salesStores { get; set; }
        public long onShelfTime { get; set; }
        public long offShelfTime { get; set; }
        public int isTaxable { get; set; }
        
    }

    public class SkuInfo_Extra
    {
        //public List<SalesStores> salesStores { get; set; }
        public string salesStores { get; set; }
        public string imageList { get; set; }
    }

    public class SalesStores {
        public string storeId { get; set; }
        public string counterId { get; set; }

    }

    public class ItemInfo_OtherAttributes
    {
        public List<ItemInfo_OtherAttributes_otherAttributes> otherAttributes { get; set; }
       
    }
    public class ItemInfo_OtherAttributes_otherAttributes
    {
        public string attrKey { get; set; }
        public string attrVal { get; set; }

    }
    public class itemDetailInfo_Detail {
        public string title { get; set; }
        public string content { get; set; }
    }
    #endregion

}


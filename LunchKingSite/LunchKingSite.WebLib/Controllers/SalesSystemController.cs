﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;
using SubSonic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using LunchKingSite.BizLogic.Jobs;
using System.Web;
using System.Drawing;
using System.Collections.Specialized;
using System.Net;
using System.Drawing.Imaging;
using LunchKingSite.BizLogic.Models.PchomeChannel;
using System.Web.Hosting;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json.Linq;
using System.Threading;
using System.Transactions;

namespace LunchKingSite.WebLib.Controllers
{

    public class SalesSystemController : BaseController
    {
        private ILog Logger = LogManager.GetLogger("RemittancetypeConvert");
        private static ISellerProvider _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static IPponProvider _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private static IAccountingProvider _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private static IWmsProvider _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        private static IMemberProvider _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static ISysConfProvider _config = ProviderFactory.Instance().GetConfig();
        private static IChannelProvider _chp = ProviderFactory.Instance().GetProvider<IChannelProvider>();
        private static ISystemProvider _sysp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        private static object thisLock = new object();

        public SalesSystemController()
        {

        }

        [RolePage]
        public ActionResult SellerTreeView([Bind(Prefix = "sid")] Guid sellerGuid, string type)
        {
            var model = new LunchKingSite.BizLogic.Models.SellerModels.SellerTreeModel();
            model.Initial(sellerGuid);
            if (type == null)
            {
                ViewBag.type = "";
            }
            else
            {
                ViewBag.type = type;
            }

            return View(model);
        }

        #region PartialView
        [RolePage]
        public ActionResult ProposalReturnDetailDialog(string codeGroup, IList<int> detailIds)
        {
            if (detailIds == null)
            {
                detailIds = new List<int>();
            }
            SystemCodeCollection sytemCodes = SystemCodeManager.GetSystemCodeListByGroup(codeGroup);
            ViewBag.CheckedItems = detailIds;
            return PartialView("_ProposalReturnDetail", sytemCodes);
        }
        public ActionResult SellerSalesReferral(Guid sellerGuid)
        {
            List<MultipleSalesModel> sellerSalesReferral = SellerFacade.SellerSaleGetBySellerGuid(sellerGuid);
            ViewBag.SellerGuid = sellerGuid;
            ViewBag.UserName = UserName;
            return PartialView("_SellerSalesReferral", sellerSalesReferral);
        }
        public ActionResult SelectSellerSales(Guid sellerGuid, string btn)
        {
            List<MultipleSalesModel> sellerSalesReferral = SellerFacade.SellerSaleGetBySellerGuid(sellerGuid);
            ViewBag.SellerGuid = sellerGuid;
            ViewBag.UserName = UserName;
            ViewBag.Btn = btn;
            return PartialView("_SelectSellerSales", sellerSalesReferral);
        }
        #endregion


        #region ProposalMultiDeals轉檔bk,已不再使用
        //public ActionResult ProposalMultiDealsConvert()
        //{

        //    DateTime start = DateTime.Now;

        //    string sql = "select id,business_hour_guid,multi_deals from proposal where Datalength(multi_deals) != 0";
        //    QueryCommand qc = new QueryCommand(sql, ViewProposalSeller.Schema.Provider.Name);

        //    DataTable dt = new DataTable();
        //    IDataReader reader = DataService.GetReader(qc);
        //    using (reader)
        //    {
        //        dt.Load(reader);
        //    }

        //    int count;
        //    Dictionary<int, string> error;
        //    ConvertMethod(dt, out count, out error);

        //    DateTime end = DateTime.Now;

        //    return RedirectToAction("ProposalMultiDeals", new { msg = "轉檔成功 提案單共" + dt.Rows.Count + "筆  子檔共" + count.ToString() + "  錯誤提案單共" + error.Count + "筆" + " 執行時間" + start.ToString() + " 結束時間" + end.ToString() });
        //}
        //public ActionResult ProposalMultiDealsSingleConvert(int pid)
        //{
        //    string deSql = "delete from proposal_multi_deals where pid=" + pid.ToString();
        //    QueryCommand deQc = new QueryCommand(deSql, CtAtmRefund.Schema.Provider.Name);
        //    DataService.ExecuteScalar(deQc);


        //    DateTime start = DateTime.Now;

        //    string sql = "select id,business_hour_guid,multi_deals from proposal where Datalength(multi_deals) != 0 and id=" + pid.ToString();
        //    QueryCommand qc = new QueryCommand(sql, ViewProposalSeller.Schema.Provider.Name);

        //    DataTable dt = new DataTable();
        //    IDataReader reader = DataService.GetReader(qc);
        //    using (reader)
        //    {
        //        dt.Load(reader);
        //    }

        //    int count;
        //    Dictionary<int, string> error;
        //    ConvertMethod(dt, out count, out error);

        //    DateTime end = DateTime.Now;

        //    return RedirectToAction("ProposalMultiDeals", new { msg = "轉檔成功 提案單共" + dt.Rows.Count + "筆  子檔共" + count.ToString() + "  錯誤提案單共" + error.Count + "筆" + " 執行時間" + start.ToString() + " 結束時間" + end.ToString() });
        //}
        //private void ConvertMethod(DataTable dt, out int count, out Dictionary<int, string> error)
        //{
        //    ISellerProvider sp = ProviderFactory.Instance().GetDefaultProvider<ISellerProvider>();

        //    count = 0;
        //    error = new Dictionary<int, string>();
        //    decimal a = 1.2M;
        //    int b = 2;
        //    var c = a * b;
        //    foreach (DataRow dr in dt.Rows)
        //    {
        //        List<ProposalMultiDealModel> model = new JsonSerializer().Deserialize<List<ProposalMultiDealModel>>(dr["multi_deals"].ToString());
        //        foreach (var m in model)
        //        {
        //            try
        //            {
        //                ProposalMultiDeal pmd = new ProposalMultiDeal();
        //                pmd.Pid = Convert.ToInt32(dr["id"]);

        //                //舊的對不回去了
        //                //if (!string.IsNullOrEmpty(dr["business_hour_guid"].ToString()))
        //                //{
        //                //    pmd.Bid = Guid.Parse(dr["business_hour_guid"].ToString());
        //                //}

        //                pmd.BrandName = "";
        //                pmd.ItemName = m.ItemName;
        //                pmd.ItemNameTravel = m.ItemNameTravel;
        //                pmd.Content = "";
        //                pmd.OrigPrice = m.OrigPrice;
        //                pmd.ItemPrice = m.ItemPrice;
        //                pmd.Cost = m.Cost;
        //                pmd.GroupCouponBuy = m.CPCBuy;
        //                pmd.GroupCouponPresent = m.CPCPresent;
        //                pmd.SlottingFeeQuantity = m.SlottingFeeQuantity;
        //                int max = 0;
        //                pmd.OrderMaxPersonal = int.TryParse(m.OrderMaxPersonal, out max) ? max : 0;
        //                pmd.OrderTotalLimit = Convert.ToInt32(m.OrderTotalLimit);
        //                pmd.AtmMaximum = m.AtmMaximum;
        //                pmd.Freights = m.Freights;
        //                pmd.NoFreightLimit = Convert.ToInt32(m.NoFreightLimit);
        //                pmd.Options = m.Options;
        //                pmd.SellerOptions = m.SellerOptions;
        //                pmd.Sort = m.Sort;
        //                if (!string.IsNullOrEmpty(m.FreightsId) && m.FreightsId != "undefined")
        //                    pmd.FreightsId = Convert.ToInt32(m.FreightsId);
        //                pmd.CreateId = config.SystemEmail;
        //                pmd.CreateTime = DateTime.Now;
        //                sp.ProposalMultiDealsSet(pmd);
        //                count++;
        //            }
        //            catch (Exception ex)
        //            {
        //                if (!error.ContainsKey(Convert.ToInt32(dr["id"])))
        //                    error.Add(Convert.ToInt32(dr["id"]), ex.Message);
        //                Logger.Info("單號" + Convert.ToInt32(dr["id"]) + "錯誤訊息" + ex.Message);
        //            }


        //        }
        //    }
        //}
        //public ActionResult ProposalMultiDealsDelete()
        //{
        //    string sql = "truncate table proposal_multi_deals";
        //    QueryCommand qc = new QueryCommand(sql, CtAtmRefund.Schema.Provider.Name);
        //    DataService.ExecuteScalar(qc);

        //    return RedirectToAction("ProposalMultiDeals", new { msg = "清除成功" });

        //}
        //[RolePage]
        //public ActionResult ProposalMultiDeals(string msg)
        //{

        //    ViewBag.result = msg;
        //    return View();
        //} 
        #endregion


        [RolePage]
        public ActionResult RemittancetypeConvert()
        {


            //string baseDirectoryPath = System.Web.HttpContext.Current.Server.MapPath("~/media/");
            //string path = @"D:\pg\Cognac\Main\Source\LunchKingSite\LunchKingSite.Web\Media\A0-00-320\636981870483670643RemoveBG - 複製.png";
            //System.Drawing.Image image = System.Drawing.Image.FromFile(path);
            //Image rtnImage = image.GetThumbnailImage(120, 120, null, IntPtr.Zero); // 取得縮略圖
            //image.Dispose();
            //ImageCodecInfo jgpEncoder = ImageFacade.GetImageCodecInfo("image/png");
            //EncoderParameter encoderParameter = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 100L); // 調整圖片品質
            //EncoderParameters encoderParameters = new EncoderParameters(1);
            //encoderParameters.Param[0] = encoderParameter;
            //rtnImage.Save(path, jgpEncoder, encoderParameters);


            return View();
        }

        public ActionResult PponRemittancetypeConvert()
        {
            DateTime start = DateTime.Now;


            //查詢憑證合約已通過之商家
            string sellersql = @"SELECT *
                            FROM   seller
                            WHERE  contract_version_ppon IS NOT NULL
                                   AND contract_ckeck_time_ppon IS NOT NULL 
                                   and remittance_type=7";

            QueryCommand sellerqc = new QueryCommand(sellersql, Seller.Schema.Provider.Name);

            DataTable sellerdt = new DataTable();
            IDataReader sellerreader = DataService.GetReader(sellerqc);
            using (sellerreader)
            {
                sellerdt.Load(sellerreader);
            }

            int sellertotalCount, sellersuccessCount;
            Dictionary<string, string> sellererror;
            ConvertSellerMethod(sellerdt, out sellertotalCount, out sellersuccessCount, out sellererror);


            DateTime sellerend = DateTime.Now;

            TempData["message"] = "商家轉檔成功<br /> 檔次共" + sellertotalCount + "筆<br />成功檔次共" + sellersuccessCount + "筆<br />錯誤檔次共" + sellererror.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + sellerend.ToString() + "<br /><br />" + string.Join("<br/>", sellererror.Values) + "<br />";



            //查詢憑證合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_ppon,
                           s.contract_version_ppon,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_ppon IS NOT NULL
                                                  AND contract_ckeck_time_ppon IS NOT NULL)
                           AND business_hour_order_time_s <= '2017/08/01'
                           AND business_hour_order_time_e >= '2017/08/01'
                           AND business_hour_order_time_e < '9999/01/01'
                           AND business_hour_status & 2048 = 0
                           AND (delivery_type = 1 or (delivery_type = 2 and a.dept_id='S008'))
                           AND a.remittance_type = 7 ";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] += "憑證轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult PponNewRemittancetypeConvert()
        {
            DateTime start = DateTime.Now;


            //查詢憑證合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_ppon,
                           s.contract_version_ppon,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_ppon IS NOT NULL
                                                  AND contract_ckeck_time_ppon IS NOT NULL)
                           AND business_hour_order_time_s >= '2017/08/01'
                           AND business_hour_status & 2048 = 0
                           AND (delivery_type = 1 or (delivery_type = 2 and a.dept_id='S008'))
                           AND a.remittance_type = 7 ";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] += "憑證轉檔(8/1才上的或尚未壓檔期)成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult HouseRemittancetypeConvert()
        {
            DateTime start = DateTime.Now;

            //查詢宅配合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_ppon,
                           s.contract_version_ppon,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_house IS NOT NULL
                                                  AND contract_ckeck_time_house IS NOT NULL)
                           AND business_hour_order_time_s <= '2017/08/01'
                           AND business_hour_order_time_e >= '2017/08/01'
                           AND business_hour_order_time_e < '9999/01/01'
                           AND business_hour_status & 2048 = 0
                           AND delivery_type = 2 AND a.dept_id!='S008'
                           AND a.remittance_type in (6,7,8)  ";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] = "宅配轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult HouseNewRemittancetypeConvert()
        {
            DateTime start = DateTime.Now;

            //查詢宅配合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_ppon,
                           s.contract_version_ppon,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_house IS NOT NULL
                                                  AND contract_ckeck_time_house IS NOT NULL)
                           AND business_hour_order_time_s >= '2017/08/01'
                           AND business_hour_status & 2048 = 0
                           AND delivery_type = 2 AND a.dept_id!='S008'
                           AND a.remittance_type in (6,7,8)  ";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] = "宅配轉檔(8/1才上的或尚未壓檔期)成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult HouseProposalRemittancetypeConvert()
        {
            DateTime start = DateTime.Now;

            //查詢宅配合約已通過之檔次
            string sql = @"select id,ship_type,remittance_type from proposal where proposal_source_type=1 and business_create_flag=0
                           and seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_house IS NOT NULL
                                                  AND contract_ckeck_time_house IS NOT NULL)";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertProposalMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] = "宅配提案單轉檔(8/1才上的或尚未壓檔期)成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }


        public ActionResult SingleRemittancetypeConvert(string bid)
        {
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            DateTime start = DateTime.Now;


            //查詢該檔次
            string sql = @"SELECT v.business_hour_guid,
                           main_bid,
                           v.business_hour_status,
                           v.seller_GUID,
                           v.business_hour_order_time_s,
                           v.business_hour_order_time_e,
                           v.delivery_type,
                           v.ship_type,
                           da.remittance_type,
                           da.dept_id
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting da
                                   ON v.business_hour_guid = da.business_hour_guid
                    WHERE  v.business_hour_guid = '" + bid + "'";





            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;

            if (dt.Rows.Count > 0)
            {
                //子檔不可單獨轉
                if (Helper.IsFlagSet(Convert.ToInt32(dt.Rows[0]["business_hour_status"]), BusinessHourStatus.ComboDealSub))
                {
                    TempData["message"] = "子檔不可轉";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else if (Convert.ToDateTime(dt.Rows[0]["business_hour_order_time_e"]) < Convert.ToDateTime("2017/08/01"))
                {
                    TempData["message"] = "已下檔不能轉";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else if (Convert.ToInt32(dt.Rows[0]["delivery_type"]) == (int)DeliveryType.ToShop || (Convert.ToInt32(dt.Rows[0]["delivery_type"]) == (int)DeliveryType.ToHouse && dt.Rows[0]["dept_id"].ToString() == EmployeeChildDept.S008.ToString()))
                {
                    Guid s = Guid.Empty;
                    Guid.TryParse(dt.Rows[0]["seller_guid"].ToString(), out s);
                    Seller seller = sp.SellerGet(s);
                    if (seller.IsLoaded)
                    {
                        if (seller.ContractCkeckTimePpon != null && seller.ContractVersionPpon != null)
                        {

                            ConvertMethod(dt, out totalCount, out successCount, out error);
                        }
                        else
                        {
                            TempData["message"] = "該檔次之商家尚未同意憑證換約";
                            return RedirectToAction("RemittancetypeConvert");
                        }
                    }
                    else
                    {
                        TempData["message"] = "找不到商家";
                        return RedirectToAction("RemittancetypeConvert");

                    }
                }
                else
                {
                    Guid s = Guid.Empty;
                    Guid.TryParse(dt.Rows[0]["seller_guid"].ToString(), out s);
                    Seller seller = sp.SellerGet(s);
                    if (seller.IsLoaded)
                    {
                        if (seller.ContractCkeckTimeHouse != null && seller.ContractCkeckTimeHouse != null)
                        {

                            ConvertMethod(dt, out totalCount, out successCount, out error);
                        }
                        else
                        {
                            TempData["message"] = "該檔次之商家尚未同意宅配換約";
                            return RedirectToAction("RemittancetypeConvert");
                        }
                    }
                    else
                    {
                        TempData["message"] = "找不到商家";
                        return RedirectToAction("RemittancetypeConvert");

                    }
                }
            }
            else
            {
                TempData["message"] = "查無資料或bid錯誤";
                return RedirectToAction("RemittancetypeConvert");
            }


            DateTime end = DateTime.Now;

            TempData["message"] = "轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult VbsRemittancetypeConvert(string accountid, bool? ppon, bool? house)
        {
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            DateTime start = DateTime.Now;

            if (ppon != null || house != null)
            {
                var sellers = ProposalFacade.GetVbsSeller(accountid, true);
                if (sellers.Count > 0)
                {
                    foreach (var g in sellers.Keys)
                    {
                        Seller s = sp.SellerGet(g);
                        if (ppon != null)
                        {
                            if (Convert.ToBoolean(ppon))
                            {
                                s.ContractCkeckTimePpon = DateTime.Now;
                                s.ContractVersionPpon = config.SellerContractVersionPpon;
                            }

                        }
                        if (house != null)
                        {
                            if (Convert.ToBoolean(house))
                            {
                                s.ContractCkeckTimeHouse = DateTime.Now;
                                s.ContractVersionHouse = config.SellerContractVersionHouse;
                            }
                        }

                        sp.SellerSet(s);
                    }


                    if (ppon != null)
                    {
                        if (Convert.ToBoolean(ppon))
                        {
                            //查詢該檔次
                            string sql = @"SELECT s.guid as seller_guid,
                            main_bid,
                            s.contract_ckeck_time_ppon,
                            s.contract_version_ppon,
                            v.business_hour_guid,
                            business_hour_order_time_s,
                            business_hour_order_time_e,
                            delivery_type,
                            seller_guid,
                            item_name,
                            a.remittance_type,
                            a.dept_id,
                            a.sales_id,
                            v.ship_type
                    FROM   view_ppon_deal v
                    INNER JOIN deal_accounting a ON v.business_hour_guid = a.business_hour_guid
                    INNER JOIN seller s  ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN ('" + string.Join("','", sellers.Keys) + "'" + @") 
                    AND business_hour_order_time_s <= '2017/08/01'
                                               AND business_hour_order_time_e >= '2017/08/01'
                                               AND business_hour_order_time_e < '9999/01/01'
                                               AND business_hour_status & 2048 = 0
                                               AND (delivery_type = 1 or (delivery_type = 2 and a.dept_id='S008'))
                                               AND a.remittance_type = 7 

                    union
                    SELECT s.guid as seller_guid,
                            main_bid,
                            s.contract_ckeck_time_ppon,
                            s.contract_version_ppon,
                            v.business_hour_guid,
                            business_hour_order_time_s,
                            business_hour_order_time_e,
                            delivery_type,
                            seller_guid,
                            item_name,
                            a.remittance_type,
                            a.dept_id,
                            a.sales_id,
                            v.ship_type
                    FROM   view_ppon_deal v
                    INNER JOIN deal_accounting a ON v.business_hour_guid = a.business_hour_guid
                    INNER JOIN seller s  ON s.guid = v.seller_GUID
                    WHERE  v.business_hour_guid IN (SELECT business_hour_guid
                                                  FROM   ppon_store
                                                  WHERE  store_guid IN  ('" + string.Join("','", sellers.Keys) + "'" + @"))
                    AND business_hour_order_time_s <= '2017/08/01'
                                               AND business_hour_order_time_e >= '2017/08/01'
                                               AND business_hour_order_time_e < '9999/01/01'
                                               AND business_hour_status & 2048 = 0
                                               AND (delivery_type = 1 or (delivery_type = 2 and a.dept_id='S008'))
                                               AND a.remittance_type = 7 ";



                            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

                            DataTable dt = new DataTable();
                            IDataReader reader = DataService.GetReader(qc);
                            using (reader)
                            {
                                dt.Load(reader);
                            }

                            int totalCount, successCount;
                            Dictionary<string, string> error;

                            if (dt.Rows.Count > 0)
                            {
                                ConvertMethod(dt, out totalCount, out successCount, out error);

                                DateTime end = DateTime.Now;

                                TempData["message"] = "憑證轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values) + "<br/>";
                            }
                            else
                            {
                                TempData["message"] = "無憑證檔次資料可以轉<br />";
                            }
                        }

                    }

                    if (house != null)
                    {
                        if (Convert.ToBoolean(house))
                        {
                            //查詢該檔次
                            string sql = @"SELECT s.guid as seller_guid,
                                            main_bid,
                                            s.contract_ckeck_time_ppon,
                                            s.contract_version_ppon,
                                            v.business_hour_guid,
                                            business_hour_order_time_s,
                                            business_hour_order_time_e,
                                            delivery_type,
                                            seller_guid,
                                            item_name,
                                            a.remittance_type,
                                            a.dept_id,
                                            a.sales_id,
                                            v.ship_type
                                    FROM   view_ppon_deal v
                                    INNER JOIN deal_accounting a ON v.business_hour_guid = a.business_hour_guid
                                    INNER JOIN seller s  ON s.guid = v.seller_GUID
                                    WHERE  seller_guid IN ('" + string.Join("','", sellers.Keys) + "'" + @") 
                                    AND business_hour_order_time_s <= '2017/08/01'
                                                               AND business_hour_order_time_e >= '2017/08/01'
                                                               AND business_hour_order_time_e < '9999/01/01'
                                                               AND business_hour_status & 2048 = 0
                                                               AND delivery_type = 2 AND a.dept_id!='S008'
                                                               AND a.remittance_type in (6,7,8)

                                    union
                                    SELECT s.guid as seller_guid,
                                            main_bid,
                                            s.contract_ckeck_time_ppon,
                                            s.contract_version_ppon,
                                            v.business_hour_guid,
                                            business_hour_order_time_s,
                                            business_hour_order_time_e,
                                            delivery_type,
                                            seller_guid,
                                            item_name,
                                            a.remittance_type,
                                            a.dept_id,
                                            a.sales_id,
                                            v.ship_type
                                    FROM   view_ppon_deal v
                                    INNER JOIN deal_accounting a ON v.business_hour_guid = a.business_hour_guid
                                    INNER JOIN seller s  ON s.guid = v.seller_GUID
                                    WHERE  v.business_hour_guid IN (SELECT business_hour_guid
                                                                  FROM   ppon_store
                                                                  WHERE  store_guid IN ('" + string.Join("','", sellers.Keys) + "'" + @"))
                                    AND business_hour_order_time_s <= '2017/08/01'
                                                               AND business_hour_order_time_e >= '2017/08/01'
                                                               AND business_hour_order_time_e < '9999/01/01'
                                                               AND business_hour_status & 2048 = 0
                                                               AND delivery_type = 2  AND a.dept_id!='S008'
                                                               AND a.remittance_type in (6,7,8)";



                            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

                            DataTable dt = new DataTable();
                            IDataReader reader = DataService.GetReader(qc);
                            using (reader)
                            {
                                dt.Load(reader);
                            }

                            int totalCount, successCount;
                            Dictionary<string, string> error;

                            if (dt.Rows.Count > 0)
                            {
                                ConvertMethod(dt, out totalCount, out successCount, out error);

                                DateTime end = DateTime.Now;

                                TempData["message"] = "宅配轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values) + "<br/>";
                            }
                            else
                            {
                                TempData["message"] += "無宅配檔次資料可以轉<br />";


                            }

                            if (TempData["message"] != null)
                            {
                                if (string.IsNullOrEmpty(TempData["message"].ToString()))
                                    return RedirectToAction("RemittancetypeConvert");
                            }

                        }
                    }
                }
                else
                {
                    TempData["message"] = "無相關商家資料可以轉";
                    return RedirectToAction("RemittancetypeConvert");
                }

            }
            else
            {
                TempData["message"] = "請勾選要轉檔的類型";
                return RedirectToAction("RemittancetypeConvert");
            }




            return RedirectToAction("RemittancetypeConvert");
        }


        private void ConvertSellerMethod(DataTable dt, out int totalCount, out int successCount, out Dictionary<string, string> error)
        {
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            totalCount = 0;
            successCount = 0;
            error = new Dictionary<string, string>();

            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    totalCount++;
                    Guid g = Guid.Empty;
                    Guid.TryParse(dr["guid"].ToString(), out g);

                    Seller s = sp.SellerGet(g);
                    if (s.IsLoaded)
                    {
                        string oriRemittance = string.Empty;
                        if (s.RemittanceType == null)
                            oriRemittance = "沒選擇";
                        else
                            oriRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)s.RemittanceType);
                        s.RemittanceType = (int)RemittanceType.Flexible;
                        sp.SellerSet(s);

                        string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)s.RemittanceType);
                        Logger.Info("商家" + s.Guid.ToString() + " " + s.SellerName + "由" + oriRemittance + "改為" + newRemittance);
                        successCount++;
                    }
                    else
                    {
                        if (!error.ContainsKey(dr["guid"].ToString()))
                            error.Add(dr["guid"].ToString(), "找不到商家" + dr["guid"].ToString());

                        Logger.Info("找不到商家" + dr["guid"].ToString());
                    }



                }
                catch (Exception ex)
                {
                    if (!error.ContainsKey(dr["guid"].ToString()))
                        error.Add(dr["guid"].ToString(), "不明錯誤" + ex.Message);

                    Logger.Info(dr["guid"].ToString() + "不明錯誤" + ex.Message);
                }
            }
        }

        private void ConvertProposalMethod(DataTable dt, out int totalCount, out int successCount, out Dictionary<string, string> error)
        {
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            totalCount = 0;
            successCount = 0;
            error = new Dictionary<string, string>();

            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    Proposal p = sp.ProposalGet(Convert.ToInt32(dr["id"].ToString()));
                    if (p.IsLoaded)
                    {
                        if (p.ShipType == (int)DealShipType.Ship72Hrs)
                        {
                            //24hr轉成雙周
                            if (p.RemittanceType == (int)RemittanceType.Flexible || p.RemittanceType == (int)RemittanceType.Weekly || p.RemittanceType == (int)RemittanceType.Monthly)
                            {
                                string oriRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)p.RemittanceType);
                                p.RemittanceType = (int)RemittanceType.Fortnightly;
                                sp.ProposalSet(p);

                                string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)p.RemittanceType);
                                Logger.Info("提案單" + p.Id.ToString() + "由" + oriRemittance + "改為" + newRemittance);
                                successCount++;
                            }
                            else if (p.RemittanceType == (int)RemittanceType.Fortnightly)
                            {
                                if (!error.ContainsKey(p.Id.ToString()))
                                    error.Add(p.Id.ToString(), "提案單" + p.Id.ToString() + "該宅配檔次為24且為雙周，不用轉檔");

                                Logger.Info("提案單" + p.Id.ToString() + "該宅配檔次為24且為雙周，不用轉檔");
                            }
                            else
                            {
                                if (!error.ContainsKey(p.Id.ToString()))
                                    error.Add(p.Id.ToString(), "提案單" + p.Id.ToString() + "該宅配檔次非新出帳，不用轉檔");

                                Logger.Info("提案單" + p.Id.ToString() + "該宅配檔次非新出帳，不用轉檔");
                            }

                        }
                        else
                        {
                            //非24轉成月結
                            if (p.RemittanceType == (int)RemittanceType.Flexible || p.RemittanceType == (int)RemittanceType.Weekly)
                            {
                                string oriRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)p.RemittanceType);
                                p.RemittanceType = (int)RemittanceType.Monthly;
                                sp.ProposalSet(p);

                                string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)p.RemittanceType);
                                Logger.Info("提案單" + p.Id.ToString() + "由" + oriRemittance + "改為" + newRemittance);
                                successCount++;
                            }
                            else if (p.RemittanceType == (int)RemittanceType.Monthly)
                            {
                                if (!error.ContainsKey(p.Id.ToString()))
                                    error.Add(p.Id.ToString(), "提案單" + p.Id.ToString() + "該宅配檔次非24且為每月結，不用轉檔");

                                Logger.Info("提案單" + p.Id.ToString() + "該宅配檔次非24且為每月結，不用轉檔");
                            }
                            else
                            {
                                if (!error.ContainsKey(p.Id.ToString()))
                                    error.Add(p.Id.ToString(), "提案單" + p.Id.ToString() + "該宅配檔次非新出帳方式，不用轉檔");

                                Logger.Info("提案單" + p.Id.ToString() + "該宅配檔次非新出帳方式，不用轉檔");
                            }
                        }
                    }
                    else
                    {
                        if (!error.ContainsKey(dr["id"].ToString()))
                            error.Add(dr["id"].ToString(), "找不到提案單");

                        Logger.Info(dr["id"].ToString() + "找不到提案單");
                    }
                }
                catch (Exception ex)
                {
                    if (!error.ContainsKey(dr["id"].ToString()))
                        error.Add(dr["id"].ToString(), "不明錯誤" + ex.Message);

                    Logger.Info(dr["id"].ToString() + "不明錯誤" + ex.Message);
                }
            }
        }
        private void ConvertMethod(DataTable dt, out int totalCount, out int successCount, out Dictionary<string, string> error)
        {
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            totalCount = 0;
            successCount = 0;
            error = new Dictionary<string, string>();


            foreach (DataRow dr in dt.Rows)
            {
                try
                {
                    List<Guid> list = new List<Guid>();
                    if (string.IsNullOrEmpty(dr["main_bid"].ToString()))
                    {
                        //單檔
                        Guid g = Guid.Empty;
                        Guid.TryParse(dr["business_hour_guid"].ToString(), out g);
                        DealAccounting da = pp.DealAccountingGet(g);
                        if (da.IsLoaded)
                        {
                            list.Add(da.BusinessHourGuid);
                        }
                        else
                        {
                            if (!error.ContainsKey(dr["business_hour_guid"].ToString()))
                                error.Add(dr["business_hour_guid"].ToString(), "DealAccounting找不到單檔" + dr["business_hour_guid"].ToString());

                            Logger.Info("DealAccounting找不到單檔" + dr["business_hour_guid"].ToString());
                        }

                    }
                    else
                    {
                        //多檔
                        Guid main = Guid.Empty;
                        ViewComboDealCollection cdc = pp.GetViewComboDealAllByBid(Guid.TryParse(dr["main_bid"].ToString(), out main) ? main : Guid.Empty);
                        if (cdc.Count > 0)
                        {
                            foreach (ViewComboDeal cd in cdc)
                            {
                                DealAccounting da = pp.DealAccountingGet(cd.BusinessHourGuid);
                                if (da.IsLoaded)
                                {
                                    list.Add(da.BusinessHourGuid);
                                }
                                else
                                {
                                    if (!error.ContainsKey(cd.BusinessHourGuid.ToString()))
                                        error.Add(cd.BusinessHourGuid.ToString(), "DealAccounting找不到母檔為" + dr["main_bid"].ToString() + "子檔為" + cd.BusinessHourGuid.ToString());
                                    Logger.Info("DealAccounting找不到母檔為" + dr["main_bid"].ToString() + "子檔為" + cd.BusinessHourGuid.ToString());
                                }
                            }
                        }
                    }

                    if (list.Count > 0)
                    {
                        foreach (var bid in list)
                        {
                            totalCount++;
                            DealAccounting da = pp.DealAccountingGet(bid);
                            BusinessHour bh = pp.BusinessHourGet(bid);
                            DealProperty dp = pp.DealPropertyGet(bid);
                            Item it = pp.ItemGetByBid(bid);
                            Proposal p = sp.ProposalGet(bid);
                            string message = "";
                            if (string.IsNullOrEmpty(dr["main_bid"].ToString()))
                                message = "單檔：" + bh.Guid.ToString() + " " + it.ItemName;
                            else
                                message = "母檔：" + dr["main_bid"].ToString() + "子檔：" + bh.Guid.ToString() + " " + it.ItemName;

                            string oriRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)da.RemittanceType);

                            if (dp.DeliveryType == (int)DeliveryType.ToShop || (dp.DeliveryType == (int)DeliveryType.ToHouse && da.DeptId == EmployeeChildDept.S008.ToString()))
                            {
                                if (da.RemittanceType == (int)RemittanceType.Weekly)
                                {
                                    da.RemittanceType = (int)RemittanceType.Flexible;
                                    pp.DealAccountingSet(da);

                                    string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)da.RemittanceType);
                                    Logger.Info(message + "由" + oriRemittance + "改為" + newRemittance);
                                    successCount++;
                                }
                                else
                                {
                                    if (!error.ContainsKey(bh.Guid.ToString()))
                                        error.Add(bh.Guid.ToString(), message + "該憑證或旅遊宅配檔次非每周，不用轉檔");

                                    Logger.Info(message + "該憑證或旅遊宅配檔次非每周，不用轉檔");
                                }
                            }
                            else
                            {
                                if (dp.ShipType == (int)DealShipType.Ship72Hrs)
                                {
                                    //24hr轉成雙周
                                    if (da.RemittanceType == (int)RemittanceType.Flexible || da.RemittanceType == (int)RemittanceType.Weekly || da.RemittanceType == (int)RemittanceType.Monthly)
                                    {
                                        da.RemittanceType = (int)RemittanceType.Fortnightly;
                                        pp.DealAccountingSet(da);

                                        string pro = string.Empty;
                                        if (p.IsLoaded)
                                        {
                                            if (p.ProposalSourceType == (int)ProposalSourceType.House && p.RemittanceType != (int)RemittanceType.Others)
                                            {
                                                p.RemittanceType = (int)RemittanceType.Fortnightly;
                                                sp.ProposalSet(p);
                                                pro = "提案單" + p.Id.ToString();
                                            }
                                        }


                                        string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)da.RemittanceType);
                                        Logger.Info(message + " " + pro + "由" + oriRemittance + "改為" + newRemittance);
                                        successCount++;
                                    }
                                    else if (da.RemittanceType == (int)RemittanceType.Fortnightly)
                                    {
                                        if (!error.ContainsKey(bh.Guid.ToString()))
                                            error.Add(bh.Guid.ToString(), message + "該宅配檔次為24且為雙周，不用轉檔");

                                        Logger.Info(message + "該宅配檔次為24且為雙周，不用轉檔");
                                    }
                                    else
                                    {
                                        if (!error.ContainsKey(bh.Guid.ToString()))
                                            error.Add(bh.Guid.ToString(), message + "該宅配檔次非新出帳，不用轉檔");

                                        Logger.Info(message + "該宅配檔次非新出帳，不用轉檔");
                                    }

                                }
                                else
                                {
                                    //非24轉成月結
                                    if (da.RemittanceType == (int)RemittanceType.Flexible || da.RemittanceType == (int)RemittanceType.Weekly)
                                    {
                                        da.RemittanceType = (int)RemittanceType.Monthly;
                                        pp.DealAccountingSet(da);

                                        string pro = string.Empty;
                                        if (p.IsLoaded)
                                        {
                                            if (p.ProposalSourceType == (int)ProposalSourceType.House && p.RemittanceType != (int)RemittanceType.Others)
                                            {
                                                p.RemittanceType = (int)RemittanceType.Monthly;
                                                sp.ProposalSet(p);
                                                pro = "提案單" + p.Id.ToString();
                                            }
                                        }

                                        string newRemittance = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (RemittanceType)da.RemittanceType);
                                        Logger.Info(message + " " + pro + "由" + oriRemittance + "改為" + newRemittance);
                                        successCount++;
                                    }
                                    else if (da.RemittanceType == (int)RemittanceType.Monthly)
                                    {
                                        if (!error.ContainsKey(bh.Guid.ToString()))
                                            error.Add(bh.Guid.ToString(), message + "該宅配檔次非24且為每月結，不用轉檔");

                                        Logger.Info(message + "該宅配檔次非24且為每月結，不用轉檔");
                                    }
                                    else
                                    {
                                        if (!error.ContainsKey(bh.Guid.ToString()))
                                            error.Add(bh.Guid.ToString(), message + "該宅配檔次非新出帳方式，不用轉檔");

                                        Logger.Info(message + "該宅配檔次非新出帳方式，不用轉檔");
                                    }
                                }
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    if (!error.ContainsKey(dr["business_hour_guid"].ToString()))
                        error.Add(dr["business_hour_guid"].ToString(), "不明錯誤" + ex.Message);

                    Logger.Info(dr["business_hour_guid"].ToString() + "不明錯誤" + ex.Message);
                }

            }
        }


        public ActionResult DealRemittancetypeConvert(string pid, string bid, int remittancetype)
        {
            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();


            if (!string.IsNullOrEmpty(bid))
            {
                //改檔次及提案單
                Guid main = Guid.Empty;
                ViewComboDealCollection cdc = pp.GetViewComboDealAllByBid(Guid.TryParse(bid, out main) ? main : Guid.Empty);
                if (cdc.Count > 0)
                {
                    foreach (ViewComboDeal cd in cdc)
                    {
                        DealAccounting da = pp.DealAccountingGet(cd.BusinessHourGuid);
                        if (da.IsLoaded)
                        {
                            da.RemittanceType = remittancetype;
                            pp.DealAccountingSet(da);

                            Proposal pro = sp.ProposalGet(da.BusinessHourGuid);
                            if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                            {
                                pro.RemittanceType = remittancetype;
                                sp.ProposalSet(pro);
                                ProposalFacade.ProposalLog(pro.Id, "變更 出帳方式 為 " + Helper.GetLocalizedEnum((RemittanceType)remittancetype), User.Identity.Name);
                            }
                            string auditMsg = "變更出帳日:{0};變更匯款資料:{1}";
                            auditMsg = string.Format(auditMsg, (RemittanceType)remittancetype, ((da.Paytocompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店"));
                            CommonFacade.AddAudit(da.BusinessHourGuid.ToString(), AuditType.DealAccounting, auditMsg, User.Identity.Name, true);
                        }
                        else
                        {
                            TempData["message"] = "找不到檔次";
                        }
                    }
                }
                else
                {
                    Guid g = Guid.Empty;
                    Guid.TryParse(bid, out g);
                    DealAccounting da = pp.DealAccountingGet(g);

                    if (da.IsLoaded)
                    {
                        da.RemittanceType = remittancetype;
                        pp.DealAccountingSet(da);

                        Proposal pro = sp.ProposalGet(da.BusinessHourGuid);
                        if (pro.ProposalSourceType == (int)ProposalSourceType.House)
                        {
                            pro.RemittanceType = remittancetype;
                            sp.ProposalSet(pro);
                            ProposalFacade.ProposalLog(pro.Id, "變更 出帳方式 為 " + Helper.GetLocalizedEnum((RemittanceType)remittancetype), User.Identity.Name);
                        }

                        string auditMsg = "變更出帳日:{0};變更匯款資料:{1}";
                        auditMsg = string.Format(auditMsg, (RemittanceType)remittancetype, ((da.Paytocompany == 1) ? "統一匯至賣家匯款資料" : "匯至各分店"));
                        CommonFacade.AddAudit(da.BusinessHourGuid.ToString(), AuditType.DealAccounting, auditMsg, User.Identity.Name, true);

                    }
                    else
                    {
                        TempData["message"] = "找不到檔次";
                    }
                }
            }
            else if (!string.IsNullOrEmpty(pid))
            {
                //還沒建檔只改提案單
                int proposalId = 0;
                int.TryParse(pid, out proposalId);
                Proposal p = sp.ProposalGet(proposalId);
                if (p.IsLoaded && p.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    p.RemittanceType = remittancetype;
                    sp.ProposalSet(p);
                    ProposalFacade.ProposalLog(proposalId, "變更 出帳方式 為 " + Helper.GetLocalizedEnum((RemittanceType)remittancetype), User.Identity.Name);
                }
                else
                {
                    TempData["message"] = "找不到提案單";
                }
            }



            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult SingleHouseRemittancetypeConvert(string sellerId)
        {
            DateTime start = DateTime.Now;

            //查詢宅配合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_house,
                           s.contract_version_house,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_house IS NOT NULL
                                                  AND contract_ckeck_time_house IS NOT NULL
                                                  AND seller_id in (" + sellerId + @"))
                           AND business_hour_order_time_e >= '2017/08/01'
                           AND business_hour_status & 2048 = 0
                           AND delivery_type = 2 AND a.dept_id!='S008'
                           AND a.remittance_type in (6,7,8)";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] = "宅配轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult SinglePponRemittancetypeConvert(string sellerId)
        {
            DateTime start = DateTime.Now;

            //查詢憑證合約已通過之商家
            string sellersql = @"SELECT *
                            FROM   seller
                            WHERE  contract_version_ppon IS NOT NULL
                                   AND contract_ckeck_time_ppon IS NOT NULL 
                                   AND (remittance_type=7 or remittance_type is null)
                                   AND seller_id in (" + sellerId + @")";

            QueryCommand sellerqc = new QueryCommand(sellersql, Seller.Schema.Provider.Name);

            DataTable sellerdt = new DataTable();
            IDataReader sellerreader = DataService.GetReader(sellerqc);
            using (sellerreader)
            {
                sellerdt.Load(sellerreader);
            }

            int sellertotalCount, sellersuccessCount;
            Dictionary<string, string> sellererror;
            ConvertSellerMethod(sellerdt, out sellertotalCount, out sellersuccessCount, out sellererror);


            DateTime sellerend = DateTime.Now;

            TempData["message"] = "商家轉檔成功<br /> 檔次共" + sellertotalCount + "筆<br />成功檔次共" + sellersuccessCount + "筆<br />錯誤檔次共" + sellererror.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + sellerend.ToString() + "<br /><br />" + string.Join("<br/>", sellererror.Values) + "<br />";


            //查詢憑證合約已通過之檔次
            string sql = @"SELECT s.guid as seller_guid,
                           main_bid,
                           s.contract_ckeck_time_ppon,
                           s.contract_version_ppon,
                           v.business_hour_guid,
                           business_hour_order_time_s,
                           business_hour_order_time_e,
                           delivery_type,
                           seller_guid,
                           item_name,
                           a.remittance_type,
                           a.dept_id,
                           a.sales_id,
                           v.ship_type
                    FROM   view_ppon_deal v
                           INNER JOIN deal_accounting a
                                   ON v.business_hour_guid = a.business_hour_guid
                           INNER JOIN seller s
                                   ON s.guid = v.seller_GUID
                    WHERE  seller_guid IN (SELECT guid
                                           FROM   seller
                                           WHERE  contract_version_ppon IS NOT NULL
                                                  AND contract_ckeck_time_ppon IS NOT NULL
                                                  AND seller_id in (" + sellerId + @"))
                           AND business_hour_order_time_e >= CONVERT(varchar(10),GETDATE(),111)
                           AND business_hour_status & 2048 = 0
                           AND (delivery_type = 1 or (delivery_type = 2 and a.dept_id='S008'))
                           AND a.remittance_type = 7";

            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }

            int totalCount, successCount;
            Dictionary<string, string> error;
            ConvertMethod(dt, out totalCount, out successCount, out error);

            DateTime end = DateTime.Now;

            TempData["message"] += "憑證轉檔成功<br /> 檔次共" + totalCount + "筆<br />成功檔次共" + successCount + "筆<br />錯誤檔次共" + error.Count + "筆" + "<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br /><br />" + string.Join("<br/>", error.Values);
            return RedirectToAction("RemittancetypeConvert");
        }


        public ActionResult SellerAgree(string sellerId)
        {
            List<string> seller = new List<string>();
            seller = sellerId.Split(",").ToList();

            int successCount = 0;
            foreach (string id in seller)
            {
                Seller s = _sp.SellerGet(Seller.Columns.SellerId, id);
                s.ContractVersionPpon = "VA1.2";
                s.ContractCkeckTimePpon = DateTime.Now;
                _sp.SellerSet(s);
                successCount++;
            }


            TempData["message"] += "憑證轉檔成功<br /> 檔次共" + seller.Count + "筆<br />成功檔次共" + successCount + "筆";
            return RedirectToAction("RemittancetypeConvert");

        }

        public ActionResult SellerAgreeHouse(string sellerId)
        {
            List<string> seller = new List<string>();
            seller = sellerId.Split(",").ToList();

            int successCount = 0;
            foreach (string id in seller)
            {
                Seller s = _sp.SellerGet(Seller.Columns.SellerId, id);
                s.ContractVersionHouse = "VA1.1";
                s.ContractCkeckTimeHouse = DateTime.Now;
                _sp.SellerSet(s);
                successCount++;
            }


            TempData["message"] += "宅配轉檔成功<br /> 檔次共" + seller.Count + "筆<br />成功檔次共" + successCount + "筆";
            return RedirectToAction("RemittancetypeConvert");

        }


        public ActionResult ISPInstore()
        {
            int successCount = 0;
            try
            {
                DateTime start = DateTime.Now;

                List<string> filter = new List<string>();
                filter.Add(ViewVbsInstorePickup.Columns.ServiceChannel + " = " + (int)ServiceChannel.FamilyMart);
                ViewVbsInstorePickupCollection vvipc = _sp.ViewVbsInstorePickupCollectionGet(1, 9999, filter.ToArray());


                foreach (ViewVbsInstorePickup vvip in vvipc)
                {
                    ConvertInstore(vvip, false);
                    successCount++;
                }


                DateTime end = DateTime.Now;

                TempData["message"] = "轉檔成功<br /> 檔次共" + vvipc.Count.ToString() + "筆<br />成功檔次共" + successCount.ToString() + "筆<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br />";
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = "第" + (successCount + 1).ToString() + "筆轉檔失敗" + ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult ISPInstoreSingle(string sellerId)
        {
            int successCount = 0;
            try
            {
                DateTime start = DateTime.Now;
                List<string> sellerList = new List<string>();
                sellerList = sellerId.Split(",").ToList();

                foreach (string seller in sellerList)
                {
                    Seller s = _sp.SellerGet(Seller.Columns.SellerId, seller);
                    ViewVbsInstorePickup vvip = _sp.ViewVbsInstorePickupGet(s.Guid, (int)ServiceChannel.FamilyMart);
                    if (vvip.IsLoaded)
                    {
                        ConvertInstore(vvip, true);
                        successCount++;
                    }

                }

                DateTime end = DateTime.Now;

                TempData["message"] = "轉檔成功<br /> 檔次共" + sellerList.Count.ToString() + "筆<br />成功檔次共" + successCount.ToString() + "筆<br />執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br />";
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = "第" + (successCount + 1).ToString() + "筆轉檔失敗" + ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        private static void ConvertInstore(ViewVbsInstorePickup vvip, bool isCover)
        {
            VbsInstorePickup vip = _sp.VbsInstorePickupGet(vvip.SellerGuid, vvip.ServiceChannel);

            var contacts = new Seller.MultiContracts();
            contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(vvip.ContactsOld).Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).FirstOrDefault();

            string tel = "";
            if (!string.IsNullOrEmpty(contacts.SellerTel))
            {
                var firstTell = contacts.SellerTel.Split(',')[0].Split(';')[0].Trim();
                var firstTellNum = decimal.Parse(Regex.Replace(firstTell, "[^0-9]+", string.Empty));
                var extension = firstTell.IndexOf('#') > 0 ? firstTell.Split('#')[1] : "";

                string _format = string.Format("0#-#######{0}-{1}", "".PadLeft(firstTellNum.ToString().Length - (!string.IsNullOrEmpty(extension) ? extension.Length : 0) - 8, '#'), "".PadRight(extension.Length, '#'));
                tel = string.Format("{0:" + _format + "}", firstTellNum);
            }
            if (string.IsNullOrEmpty(vip.Contacts) || isCover)
                vip.Contacts = new JsonSerializer().Serialize(new { ContactPersonName = contacts.ContactPersonName.Split(',')[0].Split(';')[0], SellerTel = tel, SellerMobile = contacts.SellerMobile, ContactPersonEmail = contacts.ContactPersonEmail.Split(',')[0].Split(';')[0] });
            else
            {

            }

            if (string.IsNullOrEmpty(vip.SellerAddress) || isCover)
                vip.SellerAddress = vvip.SellerAddressOld;
            else
            {

            }


            _sp.VbsInstorePickupSet(vip);
        }


        public ActionResult ISPSevenApply()
        {
            int successCount = 0;
            Dictionary<Guid, string> error = new Dictionary<Guid, string>();
            try
            {
                DateTime start = DateTime.Now;
                List<string> filter = new List<string>();
                filter.Add(ViewVbsInstorePickup.Columns.ServiceChannel + " = " + (int)ServiceChannel.FamilyMart);

                ViewVbsInstorePickupCollection vvipc = _sp.ViewVbsInstorePickupCollectionGet(1, 9999, filter.ToArray());
                VbsInstorePickupCollection vipc = new VbsInstorePickupCollection();
                foreach (ViewVbsInstorePickup vvip in vvipc)
                {
                    var vipFamily = _sp.VbsInstorePickupGet(vvip.SellerGuid, (int)ServiceChannel.FamilyMart);
                    var vipSeven = _sp.VbsInstorePickupGet(vvip.SellerGuid, (int)ServiceChannel.SevenEleven);

                    if (vipFamily.IsLoaded && !vipSeven.IsLoaded)//有全家無7-11
                    {
                        if (vipFamily.ReturnCycle == (int)RrturnCycle.Mon)
                        {
                            error.Add(vipFamily.SellerGuid, "周一");
                        }
                        else if (vipFamily.ReturnCycle == (int)RrturnCycle.Thu && !(vipFamily.ReturnType == (int)RetrunShip.Kerrytj || vipFamily.ReturnType == (int)RetrunShip.Hct))
                        {
                            error.Add(vipFamily.SellerGuid, "週二僅大榮、新竹");
                        }
                        else if (vipFamily.ReturnCycle == (int)RrturnCycle.Wed && !(vipFamily.ReturnType == (int)RetrunShip.Other || vipFamily.ReturnType == (int)RetrunShip.SelfPicking))
                        {
                            error.Add(vipFamily.SellerGuid, "週三僅其他、自取");
                        }
                        else if ((vipFamily.ReturnCycle == (int)RrturnCycle.Tue && vipFamily.ReturnType != (int)RetrunShip.Uni))
                        {
                            error.Add(vipFamily.SellerGuid, "週四僅統一黑貓");
                        }
                        else if ((vipFamily.ReturnCycle == (int)RrturnCycle.Fri && vipFamily.ReturnType != (int)RetrunShip.Pelican))
                        {
                            error.Add(vipFamily.SellerGuid, "週五僅宅配通");
                        }
                        else if ((vipFamily.ReturnCycle == (int)RrturnCycle.Daily && vipFamily.ReturnType == (int)RetrunShip.Family))
                        {
                            error.Add(vipFamily.SellerGuid, "日退不能選全家");
                        }
                        else
                        {

                            vipSeven = new VbsInstorePickup()
                            {
                                SellerGuid = vipFamily.SellerGuid,
                                Status = (int)ISPStatus.Verifying,
                                CreateId = vipFamily.CreateId,
                                CreateTime = DateTime.Now,
                                ReturnCycle = Convert.ToInt32(vipFamily.ReturnCycle),
                                ReturnType = Convert.ToInt32(vipFamily.ReturnType),
                                ReturnOther = Convert.ToInt32(vipFamily.ReturnType) == (int)RetrunShip.Other ? vipFamily.ReturnOther : "",
                                ServiceChannel = (int)ServiceChannel.SevenEleven,
                                SellerAddress = vipFamily.SellerAddress,
                                Contacts = vipFamily.Contacts,
                            };

                            vipc.Add(vipSeven);
                            successCount++;

                        }
                    }

                }

                if (vipc.Count > 0)
                    _sp.VbsInstorePickupSetList(vipc);

                DateTime end = DateTime.Now;
                TempData["message"] = "7-11申請成功<br /> 檔次共" + vvipc.Count.ToString() + "筆<br />成功檔次共" + successCount.ToString() + "筆<br />失敗比數共" + error.Count.ToString() + "執行時間" + start.ToString() + " 結束時間" + end.ToString() + "<br />" + string.Join("<br/>", error.Keys);
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }


        public ActionResult ISPSevenApplySingle(string sellerId, string returnCycle, string returnType, string returnOther)
        {
            try
            {
                List<string> sellerList = new List<string>();
                sellerList = sellerId.Split(",").ToList();
                VbsInstorePickupCollection vipc = new VbsInstorePickupCollection();
                foreach (string id in sellerList)
                {
                    Seller s = _sp.SellerGet(Seller.Columns.SellerId, id);
                    var vipSeven = _sp.VbsInstorePickupGet(s.Guid, (int)ServiceChannel.SevenEleven);
                    var vipFamily = _sp.VbsInstorePickupGet(s.Guid, (int)ServiceChannel.FamilyMart);

                    if (vipFamily.IsLoaded && !vipSeven.IsLoaded)//有全家無7-11
                    {
                        vipSeven = new VbsInstorePickup()
                        {
                            SellerGuid = s.Guid,
                            Status = (int)ISPStatus.Verifying,
                            CreateId = vipFamily.CreateId,
                            CreateTime = DateTime.Now,
                            ReturnCycle = Convert.ToInt32(returnCycle),
                            ReturnType = Convert.ToInt32(returnType),
                            ReturnOther = Convert.ToInt32(returnType) == (int)RetrunShip.Other ? returnOther : "",
                            ServiceChannel = (int)ServiceChannel.SevenEleven,
                            SellerAddress = vipFamily.SellerAddress,
                            Contacts = vipFamily.Contacts,
                        };

                        vipc.Add(vipSeven);
                    }

                }

                if (vipc.Count > 0)
                    _sp.VbsInstorePickupSetList(vipc);

                TempData["message"] = "已申請7-11";
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult InventoryImportTemp(string dir)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(dir);

                string file = "";

                if (!Directory.Exists(dir))
                {
                    TempData["message"] = "該資料夾不存在";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else if (di.GetFiles().Count() == 0 && di.GetDirectories().Count() == 0)
                {
                    TempData["message"] = "無檔案";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else
                {
                    foreach (var getfi in di.GetFiles())
                    {
                        file += getfi.Name + "<br />";
                    }
                    foreach (var getdir in di.GetDirectories())
                    {
                        file += getdir.Name + "<br />";
                    }
                    TempData["message"] = file;
                    return RedirectToAction("RemittancetypeConvert");
                }

            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult SearchDir(string dir)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(dir);

                string file = "";

                if (!Directory.Exists(dir))
                {
                    TempData["message"] = "該資料夾不存在";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else
                {
                    foreach (var getdir in di.GetDirectories())
                    {
                        foreach (var getdir2 in getdir.GetDirectories("InventoryImportTemp"))
                        {
                            foreach (var getfi in getdir2.GetFiles())
                            {
                                file += getfi.FullName + "<br />";
                            }
                        }
                    }
                    TempData["message"] = file;
                    return RedirectToAction("RemittancetypeConvert");
                }

            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }


        public ActionResult Overdue(string orderList, string amount, string reason)
        {
            //效能太差囉
            try
            {
                List<string> oList = orderList.Split(",").ToList();
                VendorPaymentOverdueCollection apoc = new VendorPaymentOverdueCollection();
                foreach (string orderId in oList)
                {
                    Order o = _op.OrderGet(Order.Columns.OrderId, orderId);
                    if (o.IsLoaded)
                    {
                        ViewPponOrder vporder = _pp.ViewPponOrderGet(o.Guid);
                        Guid bid = vporder.BusinessHourGuid;

                        VendorPaymentOverdue vpo = new VendorPaymentOverdue();
                        vpo.BusinessHourGuid = bid;
                        vpo.OrderGuid = o.Guid;
                        vpo.Reason = reason;
                        vpo.PromotionAmount = 0;
                        vpo.Amount = Convert.ToInt32(amount);
                        vpo.BalanceSheetId = null;
                        vpo.CreateTime = DateTime.Now;
                        vpo.CreateId = HttpContext.User.Identity.Name;
                        apoc.Add(vpo);

                        string msg = "扣款/異動金額：" + amount + @"元 <br /> 扣款 / 異動原因：" + reason + @"<br />  紅利金摘要：  <br />  須給消費者紅利金：0元";
                        CommonFacade.AddAudit(o.Guid, AuditType.Order, msg, HttpContext.User.Identity.Name, false);
                    }

                }

                if (apoc.Any())
                {
                    _ap.VendorPaymentOverdueSetList(apoc);
                }

                TempData["message"] = "補款成功";
                return RedirectToAction("RemittancetypeConvert");


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult ContactsConvert(string sellerList)
        {
            try
            {
                List<string> sList = sellerList.Split(",").ToList();
                string sql = @"select  guid,contacts,seller_name,seller_porperty,seller_contact_person,seller_tel,seller_mobile,seller_fax,seller_email,
                               returned_person_name,returned_person_tel,returned_person_email from seller
                               where contacts is not null and contacts!=''";


                sql += " and guid in (@s0";

                for (int i = 1; i < sList.Count(); i++)
                {
                    sql += ",@s" + i.ToString();
                }
                sql += ")";


                QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
                qc.AddParameter("@s0", sList[0], DbType.String);
                for (int i = 1; i < sList.Count(); i++)
                {
                    qc.AddParameter("@s" + i.ToString(), sList[i], DbType.String);
                }


                DataTable dt = new DataTable();
                IDataReader reader = DataService.GetReader(qc);
                using (reader)
                {
                    dt.Load(reader);
                }


                foreach (DataRow dr in dt.Rows)
                {
                    try
                    {

                        Guid sellerGuid = Guid.Empty;
                        Guid.TryParse(dr["guid"].ToString(), out sellerGuid);
                        string s = dr["contacts"].ToString();
                        Seller seller = _sp.SellerGet(sellerGuid);

                        List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s);
                        if (contacts != null)
                        {
                            List<string> listContact = new List<string>();
                            foreach (Seller.MultiContracts c in contacts)
                            {
                                if (int.Parse(c.Type) == (int)ProposalContact.Normal)
                                {
                                    //一般
                                    if (!listContact.Contains(ProposalContact.Normal.ToString()))
                                    {
                                        seller.SellerContactPerson = c.ContactPersonName;
                                        seller.SellerTel = c.SellerTel;
                                        seller.SellerMobile = c.SellerMobile;
                                        seller.SellerFax = c.SellerFax;
                                        seller.SellerEmail = c.ContactPersonEmail;

                                        listContact.Add(ProposalContact.Normal.ToString());
                                    }
                                }
                                if (int.Parse(c.Type) == (int)ProposalContact.ReturnPerson)
                                {
                                    //退貨
                                    if (!listContact.Contains(ProposalContact.ReturnPerson.ToString()))
                                    {
                                        seller.ReturnedPersonName = c.ContactPersonName;
                                        seller.ReturnedPersonTel = c.SellerTel;
                                        seller.ReturnedPersonEmail = c.ContactPersonEmail;

                                        listContact.Add(ProposalContact.ReturnPerson.ToString());
                                    }
                                }
                            }
                            _sp.SellerSet(seller);
                            if (listContact.Count() > 0)
                            {
                                Logger.Info("sid=" + seller.Guid.ToString());
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        TempData["message"] = ex.Message;
                        return RedirectToAction("RemittancetypeConvert");
                    }

                }


                TempData["message"] = "轉檔成功";
                return RedirectToAction("RemittancetypeConvert");


            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }

        public ActionResult OTPDelete(string userid)
        {
            try
            {
                if (!string.IsNullOrEmpty(userid))
                {
                    string sql = @"delete  from creditcard_otp where user_id=@userid";

                    QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
                    qc.AddParameter("@userid", userid, System.Data.DbType.String);

                    DataService.ExecuteScalar(qc);


                    TempData["message"] = "轉檔成功";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else
                {
                    TempData["message"] = "請輸入userid";
                    return RedirectToAction("RemittancetypeConvert");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }

        public ActionResult OTPWhiteListAdd(string userid)
        {
            try
            {
                if (!string.IsNullOrEmpty(userid))
                {
                    string sql = @"insert into creditcard_otp_whitelist
                                   select @userid,1,getdate(),null";

                    QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
                    qc.AddParameter("@userid", userid, System.Data.DbType.String);

                    DataService.ExecuteScalar(qc);


                    TempData["message"] = "加入成功";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else
                {
                    TempData["message"] = "請輸入userid";
                    return RedirectToAction("RemittancetypeConvert");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }

        public ActionResult OTPWhiteListDelete(string userid)
        {
            try
            {
                if (!string.IsNullOrEmpty(userid))
                {
                    string sql = @"delete  from creditcard_otp_whitelist where user_id=@userid";

                    QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
                    qc.AddParameter("@userid", userid, System.Data.DbType.String);

                    DataService.ExecuteScalar(qc);


                    TempData["message"] = "刪除成功";
                    return RedirectToAction("RemittancetypeConvert");
                }
                else
                {
                    TempData["message"] = "請輸入userid";
                    return RedirectToAction("RemittancetypeConvert");
                }
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }

        public ActionResult OTPWhiteListGet()
        {
            try
            {

                UpdateCreditcardFraudDataJob.UpdateCreditcarOTPWhitelist();
                TempData["message"] = "執行成功";
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }

        public ActionResult DownloadWmsFee(string strTime, string type)
        {
            try
            {
                bool isHasData = false;
                string message = WmsFacade.DownloadWmsFeeFile(Convert.ToDateTime(strTime), type, HttpContext.User.Identity.Name, out isHasData);
                TempData["message"] = message;
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult UpdateWmsPurchaseOrder(string pchomePurchaseOrderId)
        {
            try
            {
                string sql = "";
                if (string.IsNullOrEmpty(pchomePurchaseOrderId))
                {
                    sql = @"select * from wms_purchase_order where status in (4,20) and apply_date is null ";
                }
                else
                {
                    sql = @"select * from wms_purchase_order where status in (4,20) and apply_date is null and pchome_purchase_order_id='" + pchomePurchaseOrderId + "'";
                }
                QueryCommand qc = new QueryCommand(sql, WmsPurchaseOrder.Schema.Provider.Name);
                WmsPurchaseOrderCollection list = new WmsPurchaseOrderCollection();
                list.LoadAndCloseReader(DataService.GetReader(qc));

                int count = 0;
                foreach (WmsPurchaseOrder wpo in list)
                {
                    LunchKingSite.BizLogic.Models.Wms.WmsPurchaseOrder apiOrder = PchomeWmsAPI.GetPurchaseOrder(wpo.PchomePurchaseOrderId);

                    if (!string.IsNullOrEmpty(apiOrder.ExpireDate))
                    {
                        wpo.ExpireDate = Convert.ToDateTime(apiOrder.ExpireDate);
                    }
                    if (!string.IsNullOrEmpty(apiOrder.ArrivalDate))
                    {
                        wpo.ArrivalDate = Convert.ToDateTime(apiOrder.ArrivalDate);
                    }
                    if (!string.IsNullOrEmpty(apiOrder.EntryDate))
                    {
                        wpo.EntryDate = Convert.ToDateTime(apiOrder.EntryDate);
                    }
                    if (!string.IsNullOrEmpty(apiOrder.ApplyDate))
                    {
                        wpo.ApplyDate = Convert.ToDateTime(apiOrder.ApplyDate);
                    }

                    _wp.WmsPurchaseOrderSet(wpo);
                    count++;
                }



                TempData["message"] = "更新了" + count.ToString();
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }

        }

        public ActionResult Encrypt(string en, string de1, string de2)
        {
            try
            {
                string result = string.Empty;
                result = "EncryptAES1=" + PayeasyFacade.EncryptAES1(en) + "<br>";
                result += "EncryptAES2=" + PayeasyFacade.EncryptAES2(en) + "<br>";

                if (de1 != "")
                    result += "DecryptASE1=" + PayeasyFacade.DecryptASE1(de1) + "<br>";
                if (de2 != "")
                    result += "DecryptASE2=" + PayeasyFacade.DecryptASE2(de2) + "<br>";


                TempData["message"] = result;
                return RedirectToAction("RemittancetypeConvert");
            }
            catch (Exception ex)
            {
                TempData["message"] = ex.Message;
                return RedirectToAction("RemittancetypeConvert");
            }
        }


        public ActionResult PchomeEncrypt(string str)
        {
            string en = ChannelFacade.PChomeChannelEncryptStr(str);

            TempData["message"] = en;
            return RedirectToAction("RemittancetypeConvert");

        }

        public ActionResult PchomeDecrypt(string str)
        {
            string de = ChannelFacade.PChomeChannelDecryptStr(str);

            TempData["message"] = de;
            return RedirectToAction("RemittancetypeConvert");

        }

        public ActionResult PchomeSha1(string str)
        {
            string sha1 = Security.GetSHA1Hash(str);
            TempData["message"] = sha1;
            return RedirectToAction("RemittancetypeConvert");

        }

        public ActionResult PChomeUploadFile(string pic1, string pic2)
        {

            //check width
            //Uri mUri = new Uri(originalImage);
            //HttpWebRequest mRequest = (HttpWebRequest)WebRequest.Create(mUri);
            //mRequest.Method = "GET";
            //mRequest.Timeout = 200;
            //mRequest.ContentType = "text/html;charset=utf-8";
            //HttpWebResponse mResponse = (HttpWebResponse)mRequest.GetResponse();
            //Stream mStream = mResponse.GetResponseStream();
            //string aSize = (mResponse.ContentLength / 1024).ToString() + "KB";
            //Image mImage = Image.FromStream(mStream);
            //string aLength = mImage.Width.ToString() + "x" + mImage.Height.ToString();
            //logger.Info("width:" + mImage.Width.ToString());
            //if (mImage.Width>800)
            //{
            //    logger.Info("大於800:" + originalImage);
            //}
            //mStream.Close();

            //////////////////////////////


            //bool uploadResult = false;
            //List<IntroDetail> list = new List<IntroDetail>();
            //List<UpdateFile> fileIntro = new List<UpdateFile>();
            //CouponEventContent content = _pp.CouponEventContentGetByBid(Guid.Parse("99eacc75-4a35-4bf2-8ffe-5d7eac72822f"));
            //string des = "";
            //des = content.Description + "<br />";
            ////intro += "<span style=\"color: blue; \">權益說明</span>" + "<br />";
            ////intro += content.Restrictions;

            //string test = "1234567";
            //int inm = test.IndexOf("4");


            //List<string> imgTags = new List<string>();
            //List<string> imgHrefs = new List<string>();
            //string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            //MatchCollection matchesImgSrc = Regex.Matches(des, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            //foreach (Match m in matchesImgSrc)
            //{
            //    string tag = m.Groups[0].Value;
            //    string href = m.Groups[1].Value;
            //    imgTags.Add(tag);
            //    imgHrefs.Add(href);
            //}

            //if (imgHrefs.Count > 0)
            //{
            //    for (int count = 0; count < imgHrefs.Count; count++)
            //    {
            //        string image = imgHrefs[count];
            //        string originalImage = image;
            //        image = System.Web.HttpUtility.UrlDecode(image);

            //        int index = image.ToLower().IndexOf("images");
            //        image = @"~/" + image.Substring(index);
            //        if (image.Contains("?"))
            //        {
            //            image = image.Substring(0, image.IndexOf("?"));
            //        }

            //        fileIntro.Add(new UpdateFile
            //        {
            //            E7OriginalUrl = originalImage,
            //            E7PhysicalPath = HostingEnvironment.MapPath(image),
            //            E7FileName = Path.GetFileName(HostingEnvironment.MapPath(image)),
            //            PCPath = "",
            //            PCUrl = ""
            //        });
            //    }

            //    uploadResult = PChomeChannelAPI.UpdateFile(fileIntro);
            //}

            //for (int i = 0, l = imgTags.Count; i < l; i++)
            //{
            //    string imgTag = imgTags[i];
            //    int index = des.IndexOf(imgTag);
            //    if (index > 0)
            //    {
            //        //圖前面有文字
            //        string intro = des.Substring(0, index);
            //        list.Add(new IntroDetail
            //        {
            //            Intro = intro
            //        });
            //        list.Add(new IntroDetail
            //        {
            //            Pstn = "M",
            //            Pic = fileIntro[i].PCPath
            //        });
            //        des = des.Substring(index + imgTag.Length);
            //    }
            //    else if (index == 0)
            //    {
            //        //前面沒有圖
            //        list.Add(new IntroDetail
            //        {
            //            Pstn = "M",
            //            Pic = fileIntro[i].PCPath
            //        });
            //        des = des.Substring(imgTag.Length);
            //    }
            //}
            //if (!string.IsNullOrEmpty(des))
            //{
            //    //剩下的文字
            //    list.Add(new IntroDetail
            //    {
            //        Intro = des
            //    });
            //}

            ////在家權益說明
            //ProdEditIntro edit = new ProdEditIntro
            //{
            //    GroupId="11",
            //    Info = list
            //};
            //PChomeChannelAPI.EditIntro(edit);

            ////////////////////


            //if (!string.IsNullOrEmpty(pic2))
            //{
            //    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(Guid.Parse(pic1));
            //    IViewPponDeal vpd2 = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(Guid.Parse(pic2));


            //    UpdateFile file = new UpdateFile();
            //    string[] imagesArray = ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToArray();
            //    string DealImage = imagesArray.FirstOrDefault();
            //    int index = DealImage.ToLower().IndexOf("media");
            //    DealImage = @"~/" + DealImage.Substring(index);
            //    if (DealImage.Contains("?"))
            //    {
            //        DealImage = DealImage.Substring(0, DealImage.IndexOf("?"));
            //    }


            //    string[] imagesArray2 = ImageFacade.GetMediaPathsFromRawData(vpd2.AppDealPic, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToArray();
            //    string DealImage2 = imagesArray2.FirstOrDefault();
            //    int index2 = DealImage2.ToLower().IndexOf("media");
            //    DealImage2 = @"~/" + DealImage2.Substring(index2);
            //    if (DealImage2.Contains("?"))
            //    {
            //        DealImage2 = DealImage2.Substring(0, DealImage2.IndexOf("?"));
            //    }


            //    file.E7PhysicalPath = new string[] { HostingEnvironment.MapPath(DealImage), HostingEnvironment.MapPath(DealImage2) };
            //    file.E7FileName = new string[] { Path.GetFileName(file.E7PhysicalPath[0]), Path.GetFileName(file.E7PhysicalPath[1]) };
            //    file.PCPath = new string[] { "", "" };

            //    NameValueCollection nvc = new NameValueCollection();
            //    nvc.Add("files[0]", file.E7PhysicalPath[0] + ";filename=" + Path.GetFileName(file.E7PhysicalPath[0]));
            //    nvc.Add("files[1]", file.E7PhysicalPath[1] + ";filename=" + Path.GetFileName(file.E7PhysicalPath[1]));
            //    file.nvc = nvc;

            //    //uploadResult = PChomeChannelAPI.UpdateFile(file);
            //}
            //else
            //{
            //    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(Guid.Parse(pic1));


            //    UpdateFile file = new UpdateFile();
            //    string[] imagesArray = ImageFacade.GetMediaPathsFromRawData(vpd.AppDealPic, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToArray();
            //    string DealImage = imagesArray.FirstOrDefault();
            //    int index = DealImage.ToLower().IndexOf("media");
            //    DealImage = @"~/" + DealImage.Substring(index);
            //    if (DealImage.Contains("?"))
            //    {
            //        DealImage = DealImage.Substring(0, DealImage.IndexOf("?"));
            //    }

            //    file.E7PhysicalPath = new string[] { HostingEnvironment.MapPath(DealImage)};
            //    file.E7FileName = new string[] { Path.GetFileName(file.E7PhysicalPath[0])};
            //    file.PCPath = new string[] { ""};

            //    NameValueCollection nvc = new NameValueCollection();
            //    nvc.Add("files[0]", file.E7PhysicalPath[0] + ";filename=" + Path.GetFileName(file.E7PhysicalPath[0]));
            //    file.nvc = nvc;

            //    //uploadResult = PChomeChannelAPI.UpdateFile(file);
            //}

            TempData["message"] = "";
            return RedirectToAction("RemittancetypeConvert");

        }

        public ActionResult ImportScash(int userId, int amount, string title)
        {
            bool result = MemberFacade.ImportScash(userId, amount, title);
            if (result)
            {
                TempData["message"] = "匯入成功";
            }
            else
            {
                TempData["message"] = "匯入失敗";
            }
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult PChomeSync(string bids, bool? price, bool? qty, bool? desc, bool? intro, bool? pic, bool? prod)
        {
            List<string> strList = bids.Split(";").ToList();
            List<Guid> list = strList.ConvertAll<Guid>(i => Guid.Parse(i));
            bool priceResult = true;
            bool qtyResult = true;
            bool descResult = true;
            bool prodResult = true;
            bool introResult = true;
            bool picResult = true;
            string errMessage = "";

            string result = "";
            foreach (Guid bid in list)
            {
                PchomeChannelProd pchomeProd = _chp.PChomeChannelProdGetByBid(Guid.Parse(bid.ToString()));
                ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(Guid.Parse(bid.ToString()));
                Guid mainBid = vpd.MainBid == null ? Guid.Parse(bid.ToString()) : Guid.Parse(vpd.MainBid.ToString());
                CouponEventContent content = _pp.CouponEventContentGetByBid(mainBid);


                if (price != null && price.Value)
                {
                    priceResult = ChannelFacade.SyncPChomePrice(pchomeProd, vpd.ItemPrice, vpd.ItemOrigPrice, out errMessage);
                }

                if (qty != null && qty.Value)
                {
                    //同步庫存
                    int qauantity = (Convert.ToInt32(vpd.OrderedQuantity) > Convert.ToInt32(vpd.OrderTotalLimit)) ? 0 : Convert.ToInt32(vpd.OrderTotalLimit) - Convert.ToInt32(vpd.OrderedQuantity);
                    if (qauantity > 1200)
                        qauantity = 1200;
                    qtyResult = ChannelFacade.SyncPChomeQty(pchomeProd, qauantity, out errMessage);
                }

                if (desc != null && desc.Value)
                {
                    descResult = ChannelFacade.SyncPChomeEG(pchomeProd, content.Remark, vpd.BusinessHourDeliverTimeS.Value, (vpd.ChangedExpireDate.HasValue ? vpd.ChangedExpireDate.Value : vpd.BusinessHourDeliverTimeE.Value), out errMessage);
                }

                if (prod != null && prod.Value)
                {
                    prodResult = ChannelFacade.SyncPChomeProd(pchomeProd, vpd.ItemName, out errMessage);
                }

                if (intro != null && intro.Value)
                {
                    introResult = ChannelFacade.SyncDesc(pchomeProd, content.Description, content.Restrictions, content.Availability, out errMessage);
                }

                if (pic != null && pic.Value)
                {
                    picResult = ChannelFacade.SyncPChomePic(pchomeProd, content.AppDealPic, out errMessage);
                }

                result += "bid:" + bid.ToString() + ",price=" + priceResult.ToString() + ",qty=" + qtyResult.ToString() + ",desc=" + descResult.ToString() + ",prod=" + prodResult.ToString() + ",intro=" + introResult.ToString() + ",pic=" + picResult.ToString() + "<br>";
            }

            TempData["message"] = result;
            return RedirectToAction("RemittancetypeConvert");
        }

        public ActionResult MultiThread()
        {

            Thread oThreadA = new Thread(new ThreadStart(MultiThreadLock));
            oThreadA.Name = "A Thread";

            //建立一個執行緒，並且傳入一個委派物件 ThreadStart，並且指向 PrintNumber 方法。
            //ThreadStart myRunB = new ThreadStart(PrintNumber);
            //Thread oThreadB = new Thread(myRunB);
            Thread oThreadB = new Thread(new ThreadStart(MultiThreadLock));
            oThreadB.Name = "B Thread";

            Thread oThreadC = new Thread(new ThreadStart(MultiThreadLock));
            oThreadC.Name = "C Thread";

            //啟動執行緒物件
            oThreadA.Start();
            oThreadB.Start();
            oThreadC.Start();



            return RedirectToAction("RemittancetypeConvert");
        }

        private void MultiThreadLock()
        {
            Logger.Info("MultiThreadLock out " + Thread.CurrentThread.Name);
            lock (thisLock)
            {
                Thread.Sleep(5000);
                Logger.Info("MultiThreadLock int " + Thread.CurrentThread.Name);
            }
        }
    }
}

﻿using System;
using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model.API;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.WebLib.Controllers.ControlRoom;
using LunchKingSite.WebLib.Models.Event;
using LunchKingSite.WebLib.ActionFilter;

namespace LunchKingSite.WebLib.Controllers
{
    public class EventController : ControllerExt
    {
        private static ISysConfProvider _config;
        private static IPCPProvider _pcpProvider;
        private static IPponProvider _pp;
        private static IOrderProvider _op;

        static EventController()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _pcpProvider = ProviderFactory.Instance().GetProvider<IPCPProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = Request.UserAgent;
                if (!string.IsNullOrEmpty(userAgent) &&
                    ((userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("blackberry")
                        || (userAgent.ToLower().Contains("bb10")
                        || (userAgent.ToLower().Contains("windows phone")
                        || (userAgent.ToLower().Contains("iemobile"))))))))))
                {
                    return true;
                }
                return false;
            }
        }

        public ActionResult GetDiscountCode(int brandId, Guid? bid)
        {
            //按照傳入的策展ID回傳該策展折價券的ID列表、策展的活動圖片、策展的活動說明
            Brand brand = _pp.GetBrand(brandId);

            var dcList = PromotionFacade.DiscountCampaignGetList(brandId).OrderBy(x => x.Amount);

            var discountStatusList = new List<KeyValuePair<DiscountCampaign, KeyValuePair<bool, bool>>>();

            var userId = 0;

            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                userId = MemberFacade.GetUniqueId(User.Identity.Name);
            }

            foreach (var dc in dcList)
            {
                bool isExceed = false;
                bool isUserGet = false;

                if (userId != 0)
                {
                    int currentTotal = _op.DiscountCodeGetCount(DiscountCode.Columns.CampaignId + "=" + dc.Id);
                    if (currentTotal >= dc.Qty)
                    {
                        isExceed = true;
                    }

                    int sendToUser = _op.DiscountCodeGetListByOwner(dc.Id, userId).Count;
                    if (sendToUser > 0)
                    {
                        isUserGet = true;
                    }
                }

                KeyValuePair<bool, bool> Status = new KeyValuePair<bool, bool>(isExceed, isUserGet);
                KeyValuePair<DiscountCampaign, KeyValuePair<bool, bool>> discountStatus = new KeyValuePair<DiscountCampaign, KeyValuePair<bool, bool>>(dc, Status);

                discountStatusList.Add(discountStatus);
            }

            Guid tmpGuid;
            JArray ja = new JArray();
            if (Guid.TryParse(bid.ToString(), out tmpGuid))
            {
                var brandList = _pp.GetViewBrandItemListByBid(tmpGuid).Select(x => x.MainId).ToList();

                foreach (var tmpId in brandList)
                {
                    Brand tmpBrand = _pp.GetBrand(tmpId);
                    if ((!string.IsNullOrEmpty(tmpBrand.DiscountList)) && tmpBrand.Status && tmpBrand.StartTime < DateTime.Now && tmpBrand.EndTime > DateTime.Now)
                    {
                        var itemCount = _pp.GetBrandItemList(tmpId).Count;

                        //過渡期程式，之後需移除
                        string tempMPic = ImageFacade.GetHtmlFirstSrc(tmpBrand.MobileMainPic);
                        if (tempMPic == string.Empty) tempMPic = tmpBrand.MobileMainPic;
                        tempMPic = ImageFacade.GetMediaPath(tempMPic, MediaType.DealPromoImage);

                        JObject jo = new JObject
                        {
                            {"Url", tmpBrand.Url},
                            {"Pic", tempMPic}, //{"Pic", tmpBrand.MobileMainPic}
                            {"Count", itemCount}
                        };
                        ja.Add(jo);
                    }
                }
                ViewBag.BrandList = ja;
            }

            ViewBag.CampaignIdList = discountStatusList;

            //過渡期程式，之後需移除
            string tempMainPic = ImageFacade.GetHtmlFirstSrc(brand.MainPic);
            if (tempMainPic == string.Empty) tempMainPic = brand.MainPic;
            ViewBag.WebMainPic = ImageFacade.GetMediaPath(tempMainPic, MediaType.DealPromoImage);
            //ViewBag.WebMainPic = brand.MainPic;

            //過渡期程式，之後需移除
            string tempMobileMainPic = ImageFacade.GetHtmlFirstSrc(brand.MobileMainPic);
            if (tempMobileMainPic == string.Empty) tempMobileMainPic = brand.MobileMainPic;
            tempMobileMainPic = ImageFacade.GetMediaPath(tempMobileMainPic, MediaType.DealPromoImage);
            ViewBag.MobileMainPic = tempMobileMainPic;
            //ViewBag.MobileMainPic = brand.MobileMainPic;

            ViewBag.EventContent = brand.Description;
            ViewBag.Url = brand.Url;
            ViewBag.UserId = userId;
            return View();
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetBrandId(Guid bid)
        {
            JArray ja = new JArray();

            if (PponFacade.IsBlackDeal(bid))
            {
                return Content(JsonConvert.SerializeObject(ja), "application/json");
            }

            //回傳該檔有綁折價券的策展ID，跟策展的活動圖片
            var brandList = _pp.GetViewBrandItemListByBid(bid).Select(x => x.MainId).ToList();
            if (brandList.Any())
            {
                foreach (var brandId in brandList)
                {
                    Brand brand = _pp.GetBrand(brandId);
                    if ((!string.IsNullOrEmpty(brand.DiscountList)) && brand.Status && brand.StartTime < DateTime.Now && brand.EndTime > DateTime.Now)
                    {
                        JObject jo = new JObject();
                        jo.Add("BrandId", brandId);
                        jo.Add("WebPic", brand.WebRelayImage);
                        jo.Add("MobilePic", brand.MobileRelayImage);
                        ja.Add(jo);
                    }
                }
            }
            return Content(JsonConvert.SerializeObject(ja), "application/json");
        }

        public ActionResult ThemeCurationChannel()
        {
            if (IsMobileBroswer)
            {
                return RedirectToAction("ThemeCurationChannelMobile");
            }

            ThemeCurationMain theme = _pp.GetNowThemeCurationMainCollection().FirstOrDefault(x => x.ThemeType == (int)ThemeType.Channel);
            if (theme != null)
            {
                CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(theme.Id, true);
                if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
                {
                    ViewBag.ThemeList = lists;
                    ViewBag.Theme = theme;
                }
            }

            ViewBag.PageTitle = string.Format("主題活動 - {0}", _config.Title);
            ViewBag.MetaDescription = "各種你感興趣的主題活動，通通都在17life！領取使用主題專屬的超殺折價券，保證最划算，現在就來逛逛吧！";
            return View();
        }

        public ActionResult ThemeCurationChannelMobile()
        {
            if (!IsMobileBroswer)
            {
                return RedirectToAction("ThemeCurationChannel");
            }

            var channelList = _pp.GetNowThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Channel);
            if (channelList.Count() > 0)
            {
                var channelId = channelList.First().Id;
                CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(channelId, true);
                if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
                {
                    ThemeCurationMain theme = _pp.GetThemeCurationMainById(channelId);
                    ViewBag.ThemeList = lists;
                    ViewBag.Theme = theme;
                }
            }

            ViewBag.PageTitle = string.Format("主題活動 - {0}", _config.Title);
            ViewBag.MetaDescription = "各種你感興趣的主題活動，通通都在17life！領取使用主題專屬的超殺折價券，保證最划算，現在就來逛逛吧！";
            return View();
        }

        public ActionResult ExhibitionList(int id = 0, string rsrc = "", string p = "")
        {
            return Redirect("/event/exhibitionlist.aspx?id=" + id + ((p != "") ? "&p=" + p : string.Empty));
            if (IsMobileBroswer)
            {
                return RedirectToAction("ExhibitionListMobile", new { id = id });
            }

            if (id == 0)
            {
                id = _pp.GetNowThemeCurationMainCollection().OrderByDescending(x => x.StartDate).First().Id;
            }

            CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(id);

            if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
            {
                ViewBag.ThemeList = lists;
                ThemeCurationMain theme = _pp.GetThemeCurationMainById(id);
                ViewBag.Theme = theme;
                if ((theme.StartDate <= DateTime.Now && DateTime.Now <= theme.EndTime && theme.ShowInWeb) || p == "show_me_the_preview")
                {
                    ViewBag.IsThemeOnline = true;
                }
                else
                {
                    ViewBag.IsThemeOnline = false;
                }
            }
            else
            {
                ViewBag.Theme = null;
                ViewBag.IsThemeOnline = false;
                ViewBag.ThemeList = null;
            }

            return View();
        }

        public ActionResult ExhibitionListMobile(int id = 0, string rsrc = "", string p = "")
        {
            return Redirect("/event/ExhibitionListMobile.aspx?id=" + id + ((p != "") ? "&p=" + p : string.Empty));
            if (!IsMobileBroswer)
            {
                return RedirectToAction("ExhibitionList", new { id = id });
            }

            if (id == 0)
            {
                id = _pp.GetNowThemeCurationMainCollection().OrderByDescending(x => x.StartDate).First().Id;
            }

            CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(id);

            if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
            {
                ViewBag.ThemeList = lists;
                ThemeCurationMain theme = _pp.GetThemeCurationMainById(id);
                ViewBag.Theme = theme;
                if ((theme.StartDate <= DateTime.Now && DateTime.Now <= theme.EndTime && theme.ShowInWeb) || p == "show_me_the_preview")
                {
                    ViewBag.IsThemeOnline = true;
                }
                else
                {
                    ViewBag.IsThemeOnline = false;
                }
            }
            else
            {
                ViewBag.Theme = null;
                ViewBag.IsThemeOnline = false;
                ViewBag.ThemeList = null;
            }
            ViewBag.IsApp = false;
            return View();
        }


        public ActionResult ExhibitionListApp(int id = 0)
        {
            if (!IsMobileBroswer)
            {
                return RedirectToAction("ExhibitionList", new { id = id });
            }

            if (id == 0)
            {
                id = _pp.GetNowThemeCurationMainCollection().OrderByDescending(x => x.StartDate).First().Id;
            }

            CurationsByTheme lists = PromotionFacade.GetCurationsByTheme(id);

            if (lists != null && !(string.IsNullOrEmpty(lists.MainImageUrl)))
            {
                ViewBag.ThemeList = lists;
                ThemeCurationMain theme = _pp.GetThemeCurationMainById(id);
                ViewBag.Theme = theme;
                if ((theme.StartDate <= DateTime.Now && DateTime.Now <= theme.EndTime && theme.ShowInWeb))
                {
                    ViewBag.IsThemeOnline = true;
                }
                else
                {
                    ViewBag.IsThemeOnline = false;
                }
            }
            else
            {
                ViewBag.Theme = null;
                ViewBag.IsThemeOnline = false;
                ViewBag.ThemeList = null;
            }
            ViewBag.IsApp = true;
            return View();
        }

        public ActionResult BrandEvent(string u, string p = "")
        {
            if (IsMobileBroswer)
            {
                if (ControllerContext.RouteData.Values["u"] != null)
                {
                    Response.Redirect(string.Format("/event/brandmobile/{0}{1}", u, Request.Url.Query));
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    Response.Redirect("brandmobile.aspx" + Request.Url.Query);
                }
            }

            bool IsBrandOnline = false;

            Brand brand = _pp.GetBrand(u);
            //主題活動存在，且時間符合展示時間，可顯示於WEB，當有Preview參數時，為預覽模式，不考慮時間與顯示於WEB設定
            if (brand.IsLoaded)
            {
                if (((brand.StartTime <= DateTime.Now && DateTime.Now <= brand.EndTime && brand.ShowInWeb) || p == "show_me_the_preview"))
                {
                    //策展商品清單
                    var vepctmp = _pp.GetViewBrandCategoryByBrandId(brand.Id).Where(x => x.ItemStatus).OrderBy(x => x.Seq).OrderBy(x => x.BrandItemCategoryId).ToList();
                    ViewBag.BrandItemList = vepctmp;
                    IsBrandOnline = true;
                    string tempMobileMainPic = ImageFacade.GetHtmlFirstSrc(brand.MainPic);
                    if (tempMobileMainPic == string.Empty) { tempMobileMainPic = brand.MainPic; }
                    brand.MainPic = ImageFacade.GetMediaPath(tempMobileMainPic, MediaType.DealPromoImage);
                }
                else
                {
                    IsBrandOnline = false;
                }
            }
            else
            {
                IsBrandOnline = false;
            }

            ViewBag.Brand = brand;
            ViewBag.IsBrandOnline = IsBrandOnline;
            ViewBag.PreView = p;

            return View();
        }

        public ActionResult BrandMobile(string u, string p = "")
        {
            if (!IsMobileBroswer)
            {
                if (ControllerContext.RouteData.Values["u"] != null)
                {
                    Response.Redirect(string.Format("/event/brandevent/{0}{1}", u, Request.Url.Query));
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["u"]))
                {
                    Response.Redirect("brandevent.aspx" + Request.Url.Query);
                }
            }

            bool IsBrandOnline = false;

            Brand brand = _pp.GetBrand(u);
            //主題活動存在，且時間符合展示時間，可顯示於WEB，當有Preview參數時，為預覽模式，不考慮時間與顯示於WEB設定
            if (brand.IsLoaded)
            {
                if (((brand.StartTime <= DateTime.Now && DateTime.Now <= brand.EndTime && brand.ShowInWeb) || p == "show_me_the_preview"))
                {
                    //策展商品清單
                    var vepctmp = _pp.GetViewBrandCategoryByBrandId(brand.Id).Where(x => x.ItemStatus).OrderBy(x => x.Seq).OrderBy(x => x.BrandItemCategoryId).ToList();
                    ViewBag.BrandItemList = vepctmp;
                    IsBrandOnline = true;
                }
                else
                {
                    IsBrandOnline = false;
                }
            }
            else
            {
                IsBrandOnline = false;
            }

            ViewBag.Brand = brand;
            ViewBag.IsBrandOnline = IsBrandOnline;
            ViewBag.PreView = p;

            return View();
        }

        public ActionResult EdmLog(int id)
        {
            //if (!string.IsNullOrEmpty(email))


            var basePath = Server.MapPath("~/Themes/PCweb/images/");
            var realPath = Path.Combine(basePath, "EDMLOGO.png");
            return File(realPath, "image/png");
        }

        public ActionResult OnlineSoloEDM(int eid)
        {
            SoloEdmMain tmp = EmailFacade.GetSoloEdmMainById(eid);
            var tags = PromotionFacade.GetPromoSearchKeys().Items;
            ViewBag.HotTags1 = tags.Take(3);
            ViewBag.HotTags2 = tags.Skip(3).Take(3);
            ViewBag.EdmData = tmp;
            ViewBag.BrandType = true;
            ViewBag.PponType = true;
            ViewBag.Title = tmp.Subject;
            if (string.IsNullOrEmpty(tmp.Cpa) == false)
            {
                ViewBag.Cpa = tmp.Cpa;
            }

            var brandData = EmailFacade.GetSoloEdmBrandById(eid);
            if (brandData.Count > 0)
            {
                var brandList = new List<EdmController.BrandList>();
                foreach (var d in brandData)
                {
                    Brand brand = _pp.GetBrand(d.BrandId ?? 0);
                    var t = new EdmController.BrandList();
                    t.BrandId = d.BrandId ?? 0;
                    t.BrandName = brand.BrandName;
                    t.GroupId = d.GroupId ?? 0;
                    t.GroupName = d.GroupName;
                    t.Url = d.Url;
                    t.ImgUrl = ImageFacade.GetMediaPath(brand.EventListImage, MediaType.DealPromoImage);

                    brandList.Add(t);
                }

                ViewBag.BrandList = brandList.GroupBy(x => x.GroupId);
            }
            else
            {
                ViewBag.BrandType = false;
            }

            var pponData = EmailFacade.GetSoloEdmPponById(eid);
            if (pponData.Count > 0)
            {

                var PponList = new List<EdmController.PponList>();
                foreach (var d in pponData)
                {
                    var deal = EmailFacade.ViewPponDealGetByBusinessHourGuid(d.Bid);
                    var t = new EdmController.PponList();
                    t.Bid = d.Bid;
                    t.BlackTitle = d.BlackTitle;
                    t.GroupId = d.GroupId;
                    t.GroupName = d.GroupName;
                    t.OrangeTitle = d.OrangeTitle;
                    t.Url = d.Url;
                    t.ImgUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).First();

                    PponList.Add(t);
                }

                ViewBag.PponList = PponList.GroupBy(x => x.GroupId);
            }
            else
            {
                ViewBag.PponType = false;
            }


            return View();
        }


        /// <summary>
        /// 2017年1111的活動
        /// </summary>
        /// <returns></returns>
        public ActionResult Event1111(string test)
        {
            string baseDir = Helper.MapPath("/Images/Events/D11/");
            string closedMessage = "1111活動已結束，感謝大家的參與。";

            DateTime now;
            if (DateTime.TryParse(test, out now) == false)
            {
                now = DateTime.Now;
            }
            return EventAction(baseDir, now, closedMessage);
        }

        /// <summary>
        /// 2017年1111的活動
        /// </summary>
        /// <returns></returns>
        public ActionResult Event1212(string test)
        {
            string baseDir = Helper.MapPath("/Images/Events/D12/");
            string closedMessage = "1212活動已結束，感謝大家的參與。";

            DateTime now;
            if (DateTime.TryParse(test, out now) == false)
            {
                now = DateTime.Now;
            }
            return EventAction(baseDir, now, closedMessage);
        }

        public ActionResult Event20190102(string test)
        {
            string baseDir = Helper.MapPath("/Images/Events/20190102/");
            string closedMessage = "活動已結束，感謝大家的參與。";
            //https://www.17life.com/Images/Events/20190102/index.html
            DateTime now;
            if (DateTime.TryParse(test, out now) == false)
            {
                now = DateTime.Now;
            }
            return EventAction(baseDir, now, closedMessage);
        }

        public ActionResult Event20190130(string test)
        {
            string baseDir = Helper.MapPath("/Images/Events/20190130/");
            string closedMessage = "活動已結束，感謝大家的參與。";
            //https://www.17life.com/Images/Events/20190102/index.html
            DateTime now;
            if (DateTime.TryParse(test, out now) == false)
            {
                now = DateTime.Now;
            }
            return EventAction(baseDir, now, closedMessage);
        }

        [Route("join/{*pathInfo}")]
        public ActionResult JoinEvent(string pathInfo, string test)
        {
            string baseDir = Helper.MapPath(string.Format("/Images/Events/{0}/", pathInfo));
            string closedMessage = "活動準備中。";

            DateTime now;
            if (DateTime.TryParse(test, out now) == false)
            {
                now = DateTime.Now;
            }
            return EventAction(baseDir, now, closedMessage);
        }

        private ActionResult EventAction(string baseDir, DateTime now, string closedMessage)
        {
            var event11s = Event11.GetDataSource(baseDir);
            if (event11s == null || event11s.Count == 0)
            {
                return Content(closedMessage);
            }
            Event11 event11 = event11s.FirstOrDefault(t => now >= t.StartTime && now < t.EndTime);
            if (event11 == null)
            {
                if (DateTime.Now < event11s.Select(t => t.StartTime).Min())
                {
                    return Content("活動尚未開始，敬請期待。");
                }
                else if (DateTime.Now > event11s.Select(t => t.EndTime).Max())
                {
                    return Content("活動已經結束。");
                }
            }
            string filePath;
            string baseHref;
            if (CommonFacade.IsMobileVersion() && string.IsNullOrEmpty(event11.MobileFileName) == false)
            {
                filePath = Helper.MapPath(event11.MobileFileName);
                baseHref = event11.MobileFileName;
            }
            else
            {
                filePath = Helper.MapPath(event11.WebFileName);
                baseHref = event11.WebFileName;
            }
            if (System.IO.File.Exists(filePath))
            {
                string html = System.IO.File.ReadAllText(filePath, Encoding.UTF8);

                html = html.Replace("<head>",
                    string.Format("<head>\r\n    <base href='{0}'>", baseHref));
                if (string.IsNullOrEmpty(event11.DataSourceFileName) == false)
                {
                    html = html.Replace("data.js", event11.DataSourceFileName);
                }
                ViewBag.HtmlContent = html;
            }
            else
            {
                ViewBag.HtmlContent = "活動檔次準備中，請稍後。";
            }
            ViewBag.StartTime = DateTime.Now.ToString("s");
            ViewBag.EndTime = event11.EndTime.ToString("s");
            return View("Event1111");
        }

        #region 搶購專區 limitedTimeSelection
        [NoCache]
        public ActionResult RushBuyLoader()
        {
            //limitedTimeSelectionMain.TheDate值2018-07-03 00:00:00，指有效範圍為 2018-07-03 12:00:00 ~ 2018-07-04 12:00:00
            string todayString = DateTime.Today.AddDays(-1).ToString("yyyyMMdd");
            if (DateTime.Now >= DateTime.Today.AddHours(12))
            {
                todayString = DateTime.Today.ToString("yyyyMMdd");
            }
            return RedirectToRoute("rushbuy", new { id = todayString });
        }

        public ActionResult RushBuy(string id, string code)
        {
            DateTime limitedTimeSelectionDay;
            if (!DateTime.TryParseExact(id, "yyyyMMdd", CultureInfo.InvariantCulture, DateTimeStyles.None,
                out limitedTimeSelectionDay))
            {
                return Redirect(_config.SiteUrl);
            }

            LimitedTimeSelection limitedTimeSelection = EventFacade.GetEffectiveLimitedTimeSelections(limitedTimeSelectionDay, code);
            if (limitedTimeSelection.Id == 0)
            {
                return Redirect(_config.SiteUrl);
            }

            ViewBag.BannerImage = string.Format("{0}?{1}", "https://www.17life.com/images/rushbuy/bn_web.png", limitedTimeSelection.ModifyTime.Ticks);
            return View(new LimitedTimeSelectionModel(limitedTimeSelection));
        }

        #endregion
    }
}

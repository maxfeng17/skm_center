﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component.MGM;
using LunchKingSite.WebLib.Models.Mobile;
using NPOI.HSSF.Util;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Models.MGM;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.WebLib.Models.Service;
using log4net;

namespace LunchKingSite.WebLib.Controllers
{
    public class UserController : PponControllerBase
    {
        private ISysConfProvider config;
        private IMGMProvider mgm = ProviderFactory.Instance().GetProvider<IMGMProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private Core.IServiceProvider svp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();

        private ILog logger = LogManager.GetLogger(typeof(UserController));

        //客服紀錄客戶資料的Session name
        private const string SESSION_CUSINFO = "ServiceCustomerInfo";

        public UserController()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        private bool IsMobileBroswer
        {
            get
            {
                var userAgent = System.Web.HttpContext.Current.Request.UserAgent;
                if ((!string.IsNullOrEmpty(userAgent)) &&
                    ((userAgent.ToLower().Contains("iphone")
                        || (userAgent.ToLower().Contains("ipod")
                        || (userAgent.ToLower().Contains("android")
                        || (userAgent.ToLower().Contains("Blackberry")))))))
                {
                    return true;
                }
                return false;
            }
        }
        
        #region MGM

        #region 送禮流程
        public ActionResult SendGift()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            var dataCount = config.MGMGiftCount;
            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fSendGift");
            }

            var userId = MemberFacade.GetUniqueId(UserName);
            var data = MGMFacade.GetGiftOrderListByUser(userId).Where(x => x.RemainCount > 0);

            var hotDeals = ViewPponDealManager.DefaultManager.ViewPponDealFoolGetByTopSalesDeal(10);
            ViewBag.HotDeal1 = hotDeals.Take(5);
            ViewBag.HotDeal2 = hotDeals.Skip(5);
            ViewBag.UserId = UserId;
            ViewBag.UserEmail = UserName;
            ViewBag.OrderList = data;
            ViewBag.PageCount = data.Count() / dataCount;
            if (data.Count() % dataCount > 0)
            {
                ViewBag.PageCount++;
            }
            ViewBag.DataCount = dataCount;
            return View();
        }

        public JsonResult GetUserContacts(string userStr)
        {
            userStr = userStr ?? string.Empty;
            string[] userInfos = userStr.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            List<string> items = new List<string>();
            foreach(string userInfo in userInfos)
            {
                string trimmedUserInfo = userInfo.Trim();
                if (RegExRules.CheckMobile(trimmedUserInfo) || RegExRules.CheckEmail(trimmedUserInfo))
                {
                    items.Add(trimmedUserInfo);
                }
                else
                {
                    int userId;
                    if (int.TryParse(trimmedUserInfo, out userId) && userId > 0)
                    {
                        MobileMember mm = MemberFacade.GetMobileMember(userId);
                        if (mm.IsLoaded)
                        {
                            items.Add(mm.MobileNumber);
                        }
                        else
                        {
                            Member mem = MemberFacade.GetMember(userId);
                            if (mem.IsLoaded)
                            {
                                items.Add(mem.UserEmail);
                            }
                        }
                    }
                }
            }

            return Json(new
                { items =  items}
            );
        }

        [HttpPost]
        public ActionResult ChooesFriend(string oid, string count, string dealName)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (UserId == 0 || string.IsNullOrEmpty(oid))
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fSendGift");
            }

            var userId = MemberFacade.GetUniqueId(UserName);
            var orderList = MGMFacade.GetGiftOrderListByUser(userId).Where(x => x.RemainCount > 0).ToList();
            ViewBag.OrderList = orderList;
            ViewBag.FacebookScope = FacebookUser.DefaultScope;
            ViewBag.FacebookAppId = config.FacebookApplicationId;
            ViewBag.GoogleApiKey = config.GoogleApiKey;
            ViewBag.GoogleApiSecret = config.GoogleApiSecret;
            ViewBag.Amount = count;
            ViewBag.Oid = oid;
            ViewBag.DealName = dealName;

            return View();
        }

        public ActionResult ChooesFriend()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            return RedirectToAction("SendGift", new { });
        }

        public ActionResult GiftCardSet()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fSendGift");
            }

            ViewBag.SenderName = UserFullName;
            ViewBag.FacebookScope = FacebookUser.DefaultScope;
            ViewBag.FacebookAppId = config.FacebookApplicationId;
            ViewBag.FacebookObjectId = config.FacebookGiftObjectId;
            ViewBag.CardStyle = GiftCardUtility.CardStyleGet;
            ViewBag.MessagePattern = GiftCardUtility.MessagePatternGet;
            return View();
        }

        [HttpPost]
        public ActionResult GiftCardSet([FromJson]GiftCard giftCard)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (UserId == 0 || giftCard == null)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fSendGift");
            }

            //卡片
            giftCard.SenderId = UserId;
            giftCard.Message = giftCard.Message ?? "收禮愉快";
            //giftCard.SenderName = UserFullName;
            giftCard.MatchMemberGroup = giftCard.MatchMembers    //計算群組，同一個人多筆禮物發送
                .GroupBy(x => new { x.Value, x.Type, x.Name })
                .Select(g => new MatchMemberGroup() { Value = g.Key.Value, Type = g.Key.Type, Name = g.Key.Name, Count = g.Count() })
                .ToList();

            string errMsg = string.Empty;
            if (MGMFacade.CreateGift(ref giftCard, ref errMsg))
            {
                foreach (var matchMemeber in giftCard.MatchMemberGroup)
                {
                    GiftCardUtility.SendGift(matchMemeber, giftCard);

                    //加入通訊錄
                    MemberContact mc = new MemberContact()
                    {
                        ContactValue = matchMemeber.Value,
                        ContactType = matchMemeber.Type,
                        UserId = UserId,
                        Guid = Guid.NewGuid(),
                        ContactName = matchMemeber.Name
                    };
                    MemberFacade.MemberContactSet(mc);
                }

                return RedirectToAction("GiftSendSuccess", new { });
            }

            ViewBag.CardStyle = GiftCardUtility.CardStyleGet;
            ViewBag.MessagePattern = GiftCardUtility.MessagePatternGet;
            TempData["error"] = "發送禮物失敗," + errMsg;

            return View();
        }

        public ActionResult FbGiftCanvas()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (string.IsNullOrEmpty(Request.QueryString["request_ids"]))
            {
                return RedirectToAction("GiftList", new { });
            }

            //取得禮物內容
            string[] requestIds = Request.QueryString["request_ids"].Split(new char[] { ',' });
            var gifts = new List<ViewMgmGift>();
            ViewBag.Gifts = gifts;

            foreach (var id in requestIds)
            {
                var gift = MGMFacade.GiftGetByFbRequestId(id);
                if (gift != null && gift.IsLoaded)
                {
                    gift.ImagePath = ImageFacade.GetMediaPathsFromRawData(gift.ImagePath, MediaType.PponDealPhoto)
                        .DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg")
                        .First();
                    gifts.Add(gift);
                }
            }

            if (!gifts.Any())
            {
                return View();
            }

            ViewBag.FacebookScope = FacebookUser.DefaultScope;
            ViewBag.FacebookAppId = config.FacebookApplicationId;
            ViewBag.ReturnUrl = config.SiteUrl + "/user/fbcanvashandle"; //fb redirect_uri 一律小寫，否則會掛

            return View();
        }

        [HttpPost]
        [AjaxCall]
        public JsonResult GiftCardSetByFb(GiftCard giftCard, string fbUserId)
        {
            var result = new ApiResult();
            if (UserId == 0)
            {
                result.Code = ApiResultCode.UserNoSignIn;
                return Json(result);
            }

            if (giftCard == null)
            {
                result.Code = ApiResultCode.InputError;
                return Json(result);
            }

            //卡片
            giftCard.SenderId = UserId;
            giftCard.MatchMemberGroup = giftCard.MatchMembers    //計算群組，同一個人多筆禮物發送
                .GroupBy(x => new { x.Value, x.Type, x.Name })
                .Select(g => new MatchMemberGroup() { Value = g.Key.Value, Type = g.Key.Type, Name = g.Key.Name, Count = g.Count() })
                .ToList();

            string errMsg = string.Empty;
            if (MGMFacade.CreateGift(ref giftCard, ref errMsg))
            {
                var matchMember = giftCard.MatchMemberGroup.First();  //FB目前是限制一人
                string code = matchMember.AccessCode;
                string accessLink = Url.Action("GiftSendSuccess") + "?accessLink=" + code;

                GiftCardUtility.SendGift(matchMember, giftCard);
                //加入通訊錄
                MemberContact mc = new MemberContact()
                {
                    ContactValue = matchMember.Value,
                    ContactType = matchMember.Type,
                    UserId = UserId,
                    Guid = Guid.NewGuid(),
                    ContactName = matchMember.Name
                };
                MemberFacade.MemberContactSet(mc);

                result.Code = ApiResultCode.Success;
                result.Message = accessLink;
                return Json(result);
            }

            result.Code = ApiResultCode.Error;
            result.Message = errMsg;
            return Json(result);
        }

        public ActionResult GiftSendSuccess(string accessLink = null)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (accessLink != null)
            {
                ViewBag.AccessLink = accessLink;
            }

            var hotDeals = ViewPponDealManager.DefaultManager.ViewPponDealFoolGetByTopSalesDeal(9);
            ViewBag.HotDeals = hotDeals;
            return View();
        }

        #endregion

        #region 收禮流程
        public ActionResult GiftGet(string accessCode)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            string message = "";

            int giftId = 0;

            if (UserId == 0)
            {
                return accessCode != null ? Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftGet%3faccessCode%3d" + accessCode) : Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftGet");
            }
            if (accessCode != null)
            {
                var tmpId = MGMFacade.GiftUserIdSet(accessCode, UserId);
                if (tmpId != 0)
                {
                    giftId = tmpId;
                    MGMFacade.GiftMessageCountSet(UserId, (int)GiftMessageType.Send);
                }

            }
            else
            {
                var asCodeList = MGMFacade.GiftAccessGetByUserId(UserId);
                foreach (var code in asCodeList)
                {
                    var tmpId = MGMFacade.GiftUserIdSet(code, UserId);
                    if (tmpId != 0)
                    {
                        giftId = tmpId;
                        MGMFacade.GiftMessageCountSet(UserId, (int)GiftMessageType.Send);
                    }

                }
            }

            if (giftId == 0)
            {
                message = "禮物已被領取，無法再次領取";
            }

            return RedirectToAction("GiftList", new { msg = message, giftId = giftId });
        }

        public ActionResult GiftList(int? giftId, int? giftType, string msg)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}
            var id = giftId ?? 0;
            var dataCount = config.MGMGiftCount;

            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftList");
            }

            var hotDeals = ViewPponDealManager.DefaultManager.ViewPponDealFoolGetByTopSalesDeal(10);
            ViewBag.HotDeal1 = hotDeals.Take(5);
            ViewBag.HotDeal2 = hotDeals.Skip(5);

            ViewBag.GiftId = id;
            ViewBag.UserId = UserId;
            ViewBag.UserEmail = UserName;
            var data = MGMFacade.GiftGetByReceiverId(UserId);

            ViewBag.NewGiftList = data.Where(x => x.SendTime.AddDays(config.MGMGiftLockDay) > DateTime.Now && x.Accept == 0).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            ViewBag.NotUsedGift = data.Where(x => x.Accept == 1 && x.IsUsed == false).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            ViewBag.UsedGift = data.Where(x => x.Accept == 1 && x.IsUsed == true).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            ViewBag.RedeemExpired = data.Where(x => x.Accept == 1 && (x.GiftData.OrderTimeE < DateTime.Now)).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            ViewBag.ReturnGift = data.Where(x => x.Accept == 2).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            ViewBag.ConfirmExpired = data.Where(x => x.SendTime.AddDays(config.MGMGiftLockDay) < DateTime.Now && x.Accept == 0).ToLookup(x => x.AccessCode).ToDictionary(x => x.First(), x => x.Count());

            if (id != 0)
            {
                var tmp = data.Where(x => x.GiftId == id);
                if (tmp.Any())
                {
                    ViewBag.OpenAsCode = tmp.First().AccessCode;
                    ViewBag.OpenMsgId = tmp.First().SendMsgId;
                    ViewBag.OpenPicUrl = tmp.First().GiftData.PicUrl;
                    ViewBag.OpenItemName = tmp.First().GiftData.ItemName;
                    ViewBag.OpenName = tmp.First().GiftData.Name;
                    ViewBag.OpenBid = tmp.First().GiftData.Bid;
                }

            }

            ViewBag.DataCount = dataCount;

            ViewBag.msg = msg;
            ViewBag.GiftType = giftType ?? 0;
            return View();
        }

        public ActionResult GiftItem(string asCode)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            var data = MGMFacade.GiftViweGetByAsCode(asCode, UserId);
            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftItem%3fasCode%3d" + asCode);
            }

            ViewBag.UserId = UserId;
            ViewBag.UserEmail = UserName;
            ViewBag.DataList = data;
            if (data.Any())
            {
                ViewBag.ReplyMessageId = data.First().ReplyMessageId;
                ViewBag.ImagePath =
                    ImageFacade.GetMediaPathsFromRawData(data.First().ImagePath, MediaType.PponDealPhoto)
                        .DefaultIfEmpty(@"https://www.17life.com/Themes/default/images/17Life/G2/ppon-M1_pic.jpg")
                        .First();
                ViewBag.Id = data.First().Id;
                ViewBag.SendMessageId = data.First().SendMessageId;
                ViewBag.CouponName = data.First().Name;

                var receiveTime = data.First().ReceiveTime;

                if (receiveTime != null)
                {
                    ViewBag.ReceiveTime = receiveTime.Value.ToString("yyyy/MM/dd");
                }

                ViewBag.ItemName = data.First().ItemName;
                ViewBag.SendUserName = data.First().LastName + data.First().FirstName;
                ViewBag.CouponBid = data.First().Bid;
                var businessHourDeliverTimeE = data.First().BusinessHourOrderTimeE;

                if (businessHourDeliverTimeE != null)
                {
                    ViewBag.ExchangeDate = businessHourDeliverTimeE.ToString("yyyy/MM/dd");
                }

            }

            return View();
        }

        public ActionResult CardList()
        {
            if (true)
            {
                return Redirect("/");
            }

            //if (UserId == 0)
            //{
            //    return Redirect(FormsAuthentication.LoginUrl+"?ReturnUrl=%2fUser%2fCardList");
            //}

            //var dataList = MGMFacade.CardListGet(UserId);

            //ViewBag.SendCardList = dataList.Where(x => x.MessageType == (int)GiftMessageType.Send);

            //ViewBag.ReplyCardList = dataList.Where(x => x.MessageType == (int)GiftMessageType.Reply);

            //return View();
        }


        public ActionResult CardItem(int msgId, int type)
        {
            return Redirect("/");

            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            //if (UserId == 0)
            //{
            //    return Redirect(FormsAuthentication.LoginUrl+"?ReturnUrl=%2fUser%2fCardItem%3fmsgId%3d"+ msgId + "%26type%3d" + type);
            //}

            //var dataList = MGMFacade.CardItemGet(msgId,type);

            //return View();
        }


        public ActionResult GiftCardReply(int msgId, string name)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            ViewBag.MsgId = msgId;
            ViewBag.Name = name;
            var m = MemberFacade.GetMember(User.Identity.Name);
            ViewBag.UserName = m.LastName + m.FirstName;
            ViewBag.CardImage = GiftCardUtility.CardStyleGet.First().ImagePic;
            ViewBag.CardStyle = GiftCardUtility.CardStyleGet;
            ViewBag.MessagePattern = GiftCardUtility.MessagePatternGet;
            return View();
        }
        [HttpPost]
        public ActionResult CardReplySet(int msgId, string msg, string sendName, int cardStyleId)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}
            if (MGMFacade.CardReply(msgId, msg, sendName, cardStyleId, UserId))
            {
                return RedirectToAction("GiftList");
            }
            return RedirectToAction("GiftList", new { msg = "操作錯誤，卡片無法寄出" });
        }

        public ActionResult GiftWhere()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftWhere");
            }

            var dataList = MGMFacade.GiftWhereGetBySenderId(UserId);

            ViewBag.DataList = dataList;

            return View();
        }

        [HttpPost]
        public ActionResult GiftWhere(string searchStr)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}

            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fGiftWhere");
            }

            var dataList = MGMFacade.GiftWhereGetBySenderId(UserId).Where(x => x.GiftData.ItemName.Contains(searchStr));

            ViewBag.DataList = dataList;

            return View();
        }

        public ActionResult GiftWhereItem()
        {
            return RedirectToAction("GiftWhere");
        }


        [HttpPost]
        public ActionResult GiftWhereItem(Guid oid, string sendTime)
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}
            var tmpdata = Convert.ToDateTime(sendTime);
            var data = MGMFacade.GiftWhereItemGetByOid(oid, tmpdata);
            ViewBag.Data = data;
            return View();
        }


        public ActionResult GiftInFo(string accessCode)
        {
            var gift = mgm.GiftViewGetByAccessCode(accessCode);
            var isMobileBroswer = this.IsMobileBroswer;
            IViewPponDeal deal = null;
            ViewComboDealCollection mainDeal = null;
            ViewBag.IsReverseVerify = false;

            //找不到沒有禮物
            if (!gift.Any())
            {
                gift = null;
                ViewBag.Accept = (int)MgmAcceptGiftType.None;
            }
            else
            {
                //取得禮物內容，open17life:// 為手機app開啟schema url
                var msg = mgm.GiftCardGetById(gift.First().SendMessageId);
                deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(gift.First().Bid);
                mainDeal = pp.GetViewComboDealAllByBid(gift.First().Bid);
                ViewBag.DealItem = mainDeal.Any() ? mainDeal.First().CouponUsage : deal.ItemName ?? "";
                ViewBag.SenderName = msg.SenderName;
                ViewBag.AsCodeLink = "open17life://" + config.SSLSiteUrl.Replace("http://", "").Replace("https://", "")
                    + "/User/GiftGet?accessCode=" + accessCode;
                ViewBag.Url = config.SSLSiteUrl + Url.Action("GiftInFo", "user", new { accessCode = accessCode });
                ViewBag.Accept = gift.First().Accept;
                ViewBag.ExpiredTime = gift.First().SendTime.AddDays(config.MGMGiftLockDay).ToString("yyyy/MM/dd HH:00");
                ViewBag.MatchType = mgm.GiftMatchGetById(gift.FirstOrDefault().MgmMatchId).MatchType;
                ViewBag.AccessCode = accessCode;
                ViewBag.IsReverseVerify = deal.VerifyActionType == (int)VerifyActionType.Reverse;
            }

            ViewBag.IsMobileBroswer = isMobileBroswer;

            return View();
        }

        #endregion

        #region 通訊錄

        public ActionResult Contact()
        {
            //if (!config.MGMEnabled)
            //{
            //    return Redirect("/");
            //}
            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fContact");
            }

            var mcData = MemberFacade.GetMemberContactsDataById(UserId);

            return View(mcData);
        }

        public ActionResult ContactDetails(Guid id)
        {
            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fContact");
            }

            var mc = MemberFacade.MemberContactGetByGuid(id);
            if (!mc.IsLoaded) //找不到朋友
            {
                throw new HttpException(404, "");
            }
            else
            {
                if (UserId != mc.UserId) //找到朋友，確認關係
                    throw new HttpException(404, "");
            }

            return View(mc);
        }

        [HttpPost]
        [AjaxCall]
        public JsonResult ContactSet(Contact ct)
        {
            var result = new ApiResult();
            //if (UserId == 0)
            //{
            //    result.Code = ApiResultCode.UserNoSignIn;
            //    return Json(result);
            //}

            //test
            MemberContact mc = new MemberContact
            {
                ContactName = ct.Name,
                ContactNickName = ct.NickName,
                ContactValue = ct.Value,
                ContactBirthday = ct.Birthday

            };

            //TODO: 先判斷通訊錄好友是否有加過


            if (!MemberFacade.MemberContactSet(mc))
            {
                return Json((int)ContactStatus.Fail);
            }

            return Json((int)ContactStatus.Success);
        }

        [HttpPost]
        public ActionResult GroupSet(MemberGroup mg)
        {
            var result = new ApiResult();
            if (UserId == 0)
            {
                return Redirect(FormsAuthentication.LoginUrl + "?ReturnUrl=%2fUser%2fContact");
            }

            if (!UploadUtility.UploadFile(Request, "groupImage", "~/Images/MGM/member/"))
            {
                return Content("<script>alert('上傳圖片檔案失敗');history.go(-1);</script>");
            }

            //TODO:塞資料庫


            return RedirectToAction("Contact", new { upl = "tabGroup" });
        }


        #endregion

        #region 推薦禮物

        public ActionResult GiftRecommend()
        {
            //DateTime now = DateTime.Now;

            //ViewCmsRandomCollection dataCms = CmsRandomFacade.GetOrAddData(EventPromoEventType.Curation.ToString(), RandomCmsType.PponMasterPage);
            //var filterDatas = dataCms
            //  .Where(x => (x.CityId == CityId || x.CityId < 0) && now <= x.EndTime && now >= x.StartTime)
            //  .ToList();

            var hotDeals = ViewPponDealManager.DefaultManager.ViewPponDealFoolGetByTopSalesDeal(10);
            ViewBag.HotDeal1 = hotDeals.Take(5);
            ViewBag.HotDeal2 = hotDeals.Skip(5);

            return View();
        }

        #endregion

        #region AjaxCall 

        [HttpPost]
        [AjaxCall]
        public ActionResult GetMenuGiftList(string pageType)
        {
            var userId = MemberFacade.GetUniqueId(UserName);

            var returnData = new List<BannerGiftData>();
            if (pageType == "1")
            {
                return Json(MGMFacade.BannerSendGiftByUser(userId));
            }
            if (pageType == "2")
            {
                return Json(MGMFacade.BannerGiftListByUser(UserId));
            }
            if (pageType == "3")
            {
                return Json(MGMFacade.BannerGiftCardByUser(UserId));
            }
            return Json(returnData);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult UpDateGift(string asCode, int type)
        {
            int error = 0;
            MGMFacade.UpDateGift(asCode, type, ref error);
            return Json("OK");
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GiftCardGet(int msgId, int isRead)
        {
            string errMsg = string.Empty;
            var data = MGMFacade.GiftCardGet(UserId, msgId, isRead, ref errMsg);
            return Json(data);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetBarCode(string accept, string info, string asCode)
        {
            List<int?> couponIdList = new List<int?>();
            //先抓取AsCode有多少CouponId
            couponIdList = MGMFacade.GetCouponIdByAsCode(asCode);
            ApiBarCode result = new ApiBarCode();
            List<ApiBarCode> data = new List<ApiBarCode>();

            foreach (var item in couponIdList)
            {

                //先檢查此couponId是否已被領取
                if (accept == "0")
                {
                    //回寫狀態和寫入收件人
                    MGMFacade.QuickReceive(asCode, item, info);
                }

                result = new ApiBarCode();
                result = MGMFacade.GetBarcode(item);

                if (result != null)
                {
                    data.Add(result);

                }
            }

            var jsonResult = Json(data);
            jsonResult.MaxJsonLength = int.MaxValue;
            return jsonResult;
        }

        #endregion

        #endregion

        #region 客服
        /// <summary>
        /// 一般問題列表
        /// </summary>
        /// <returns></returns>
        public ActionResult ServiceList()
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            //會員資訊
            string fisrtName = string.Empty;
            string lastName = string.Empty;
            string email = string.Empty;
            string mobile = string.Empty;

            if (!string.IsNullOrEmpty(this.UserName))
            {
                Member mem = mp.MemberGet(this.UserName);
                if (mem.IsLoaded)
                {
                    fisrtName = mem.FirstName;
                    lastName = mem.LastName;
                    email = mem.UserEmail;
                    mobile = mem.Mobile;
                }
            }

            //列表資訊
            DataList model = CustomerServiceFacade.GetDataList(this.UserId, (int)ServiceIssueType.GeneralIssue);

            foreach (var row in model.IssueList)
            {
                row.ModifyTime = ConvertApiDateTimeToSystemDateTime(row.ModifyTime);
            }

            ViewBag.FisrtName = fisrtName;
            ViewBag.LastName = lastName;
            ViewBag.Email = email;
            ViewBag.Mobile = mobile;
            ViewBag.Categorys = CustomerServiceFacade.GetCategoryList(); //問題列表
            return View(model);
        }

        /// <summary>
        /// By no 查詢對話記錄
        /// </summary>
        /// <param name="issueType">1:一般問題 2:訂單問題</param>
        /// <param name="no">未填則當作是問新問題</param>
        /// <param name="isSuccess"></param>
        /// <returns></returns>
        public ActionResult ServiceConversation(int issueType = (int)ServiceIssueType.GeneralIssue, string no = null, bool isSuccess = false)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            if (!string.IsNullOrEmpty(this.UserName))
            {
                Member mem = mp.MemberGet(this.UserName);
                if (mem.IsLoaded)
                {
                    ViewBag.FirstName = mem.FirstName;
                    ViewBag.LastName = mem.LastName;
                    ViewBag.Email = mem.UserEmail;
                    ViewBag.Mobile = mem.Mobile;
                }
            }

            CommunicateData data = new CommunicateData();
            var categorys = new List<ParentCategory>();
           
            //問新問題
            if (!string.IsNullOrEmpty(no))
            {
                if (issueType == (int) ServiceIssueType.OrderIssue)
                {
                    #region 訂單

                    Guid orderGuid;
                    if (!Guid.TryParse(no, out orderGuid))
                    {
                        return RedirectToAction("ServiceList");
                    }

                    //對話記錄
                    data = CustomerServiceFacade.GetCommunicateListByOrderGuid(UserId, no, true);

                    #endregion
                }
                else
                {
                    #region 一般 

                    //對話記錄
                    data = CustomerServiceFacade.GetCommunicateListByServiceNo(this.UserId, no, true);

                    #endregion
                }

                //轉換日期
                data.ModifyTime = ConvertApiDateTimeToSystemDateTime(data.ModifyTime);
                foreach (var row in data.Communicate)
                {
                    row.CreateTime = ConvertApiDateTimeToSystemDateTime(row.CreateTime);
                }
            }

            //已結案 || 新問題
            if (data.Status == (int)statusConvert.complete || data.CategoryId == 0)
            {
                categorys = issueType == (int)ServiceIssueType.OrderIssue
                    ? CustomerServiceFacade.GetCategoryList(1).Category
                    : CustomerServiceFacade.GetCategoryList().Category;
            }

            ViewBag.CaseNo = no;
            ViewBag.IssueType = issueType;
            ViewBag.Categorys = categorys;
            ViewBag.IsSuccess = isSuccess;

            return View(data);
        }

        /// <summary>
        /// 儲存顧客提問訊息
        /// </summary>
        /// <param name="model">提問訊息Model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveServiceMassage(ServiceMassageModel model)
        {
            string message = string.Empty;
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            if (string.IsNullOrEmpty(model.QuestionContent) && string.IsNullOrEmpty(model.ImageString))
            {
                throw new Exception("參數錯誤");
            }
            
            Member mem = mp.MemberGet(this.UserName);

            ApiSendData sendData = new ApiSendData();
            sendData.UserId = this.UserId.ToString();
            sendData.Mobile = model.IssueType == (int)ServiceIssueType.OrderIssue ? mem.Mobile : model.Mobile;
            sendData.Email = model.IssueType == (int)ServiceIssueType.OrderIssue ? mem.UserEmail : model.Email;
            sendData.Name = model.IssueType == (int)ServiceIssueType.OrderIssue 
                ? DisplayName(mem.LastName, mem.FirstName) 
                : DisplayName(model.LastName, model.FirstName);
            sendData.Content = model.QuestionContent;
            //來源為Web
            sendData.Source = (int)Source.web;

            int preStrIndex = model.ImageString.IndexOf(",");
            if (preStrIndex != -1)
            {
                sendData.Image = model.ImageString.Substring(preStrIndex + 1, model.ImageString.Length - (preStrIndex + 1));
            }

            if (model.IssueType == (int)ServiceIssueType.OrderIssue)
            {
                #region 訂單問題
                Guid orderGuid;
                Guid.TryParse(model.CaseNo, out orderGuid);
                if (orderGuid == Guid.Empty)
                {
                    throw new Exception("參數錯誤");
                }

                sendData.OrderGuid = model.CaseNo;

                if (model.CategoryId == null)
                {
                    throw new Exception("參數錯誤");
                }

                sendData.CategoryId = (int)model.CategoryId;

                string serviceNo = "";
                if (!CustomerServiceFacade.CreateOrderIssue(UserName, this.UserId, sendData, false, out serviceNo))
                {
                    throw new Exception("create order issue fail.");
                }
                #endregion

                return RedirectToAction("ServiceConversation", new { issueType = (int)ServiceIssueType.OrderIssue, no = model.CaseNo, isSuccess = true });
            }
            else
            {
                #region 一般問題
                sendData.ServiceNo = model.CaseNo;
                if (model.CategoryId == null)
                {
                    throw new Exception("參數錯誤");
                }

                sendData.CategoryId = (int)model.CategoryId;
                ServiceData serviceData;

                //更新聯絡人資訊(有登入時沒有相關資訊才會更新基本資料)(New case)
                #region update member info
                bool hasChange = false;

                if (string.IsNullOrEmpty(model.CaseNo) && !string.IsNullOrEmpty(this.UserName))
                {
                    if (mem.IsLoaded)
                    {
                        if (mem.UserEmail != model.Email && !string.IsNullOrEmpty(model.Email))
                        {
                            mem.UserEmail = model.Email;
                            hasChange = true;
                        }

                        if (mem.LastName != model.LastName && !string.IsNullOrEmpty(model.LastName))
                        {
                            mem.LastName = model.LastName;
                            hasChange = true;
                        }

                        if (mem.FirstName != model.FirstName && !string.IsNullOrEmpty(model.FirstName))
                        {
                            mem.FirstName = model.FirstName;
                            hasChange = true;
                        }

                        if (mem.Mobile != model.Mobile)
                        {
                            mem.Mobile = model.Mobile;
                            hasChange = true;
                        }

                        if (hasChange)
                        {
                            mp.MemberSet(mem);
                        }
                    }
                }
                #endregion

                if (!CustomerServiceFacade.CreateIssue(UserName, this.UserId, sendData, out serviceData, out message))
                {
                    logger.Warn("SaveServiceMassage fail, " + message);
                    throw new Exception("create issue fail.");
                }
                #endregion

                return RedirectToAction("ServiceConversation", new { issueType = (int)ServiceIssueType.GeneralIssue, no = serviceData.ServiceNo, isSuccess = true });           
            }
        }

        /// <summary>
        /// 一般問題的中繼頁
        /// </summary>
        /// <param name="model">提問訊息Model</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult StartConversation(ServiceMassageModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
            }

            if (string.IsNullOrEmpty(model.Email) ||
                string.IsNullOrEmpty(model.LastName) ||
                string.IsNullOrEmpty(model.FirstName) ||
                model.CategoryId == null)
            {
                return RedirectToAction("ServiceList");
            }

            Session[SESSION_CUSINFO] = model;

            return RedirectToAction("ServiceConversation");
        }

        /// <summary>
        /// 轉換app專用日期為正常日期
        /// </summary>
        /// <param name="apiDateTime">api日期</param>
        /// <returns></returns>
        private string ConvertApiDateTimeToSystemDateTime(string apiDateTime)
        {
            DateTime dt;
            ApiSystemManager.TryDateTimeStringToDateTime(apiDateTime, out dt);
            return dt.ToString("yyyy-MM-dd HH:mm");
        }

        /// <summary>
        /// 顯示姓名
        /// </summary>
        private string DisplayName(string lastName, string firstName)
        { 
            if (string.IsNullOrEmpty(lastName) == false && string.IsNullOrEmpty(firstName) == false)
            {
                if (Helper.IsChinese(firstName.ToCharArray()[0]) == false || Helper.IsChinese(lastName.ToCharArray()[0]) == false)
                {
                    return string.Format("{0} {1}", firstName, lastName);
                }
                //中文姓名
                return string.Format("{0}{1}", lastName, firstName);
            }
            if (string.IsNullOrEmpty(lastName) == false)
            {
                return lastName;
            }
            if (string.IsNullOrEmpty(firstName) == false)
            {
                return firstName;
            }
            return string.Empty;
        }
        #endregion

        #region 客服Ajax
        /// <summary>
        /// 取得問題清單
        /// </summary>
        /// <param name="issueType">問題類型 1:一般 2:訂單</param>
        /// <returns></returns>
        [HttpPost]
        [AjaxCall]        
        public JsonResult GetIssueList(int issueType)
        {
            //列表資訊
            DataList dataList = CustomerServiceFacade.GetDataList(this.UserId, issueType);

            foreach (var row in dataList.IssueList)
            {
                row.ModifyTime = ConvertApiDateTimeToSystemDateTime(row.ModifyTime);
            }

            return Json(new { Data = dataList.IssueList });
        }

        /// <summary>
        /// 取得問題分類名稱()
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxCall]
        public JsonResult GetParentName(int categoryId)
        {
            var category = CustomerServiceFacade.GetCategoryList().Category.FirstOrDefault(p => p.ParentId == categoryId);
            if (category != null)
            {
                return Json(new { Result = category.ParentName });
            }

            return Json(new { Result = string.Empty });
        }

        /// <summary>
        /// 檢查客戶資訊
        /// </summary>
        /// <param name="lastName"></param>
        /// <param name="firstName"></param>
        /// <param name="email">電子信箱</param>
        /// <param name="mobile">手機號碼</param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxCall]
        public JsonResult CheckCustomerInfo(string lastName, string firstName, string email, string mobile, string categoryId)
        {
            string errmsg = string.Empty;
            bool result = true;
            int outCategoryId;

            if (!Int32.TryParse(categoryId, out outCategoryId))
            {
                errmsg = "請選擇問題類別";
            }

            if (string.IsNullOrEmpty(lastName) || string.IsNullOrEmpty(firstName))
            {
                errmsg = "請輸入姓名";
            }

            if (!string.IsNullOrEmpty(mobile) && !RegExRules.CheckMobile(mobile))
            {
                errmsg = "請輸入正確的手機號碼";
            }

            if (!RegExRules.CheckEmail(email))
            {
                errmsg = "請輸入正確的電子信箱";
            }

            if (!string.IsNullOrEmpty(errmsg))
            {
                result = false;
            }

            return Json(new { Result = result, Errmsg = errmsg });
        }

        /// <summary>
        /// 取得驗證圖片的代碼
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AjaxCall]
        public JsonResult GetCaptchaCode()
        {
            return Json(new { Code = Convert.ToString(Session["CaptchaService"]).ToLower() });
        }
        #endregion
    }
}
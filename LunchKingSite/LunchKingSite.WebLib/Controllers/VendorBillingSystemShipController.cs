﻿using System.Globalization;
using Ionic.Zip;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Model.VBS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using NPOI.HSSF.UserModel;
using NPOI.HSSF.Util;
using NPOI.SS.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Text;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.BizLogic.Models.Wms.ViewModels;
using LunchKingSite.WebLib.Models.VendorBillingSystem.Ship;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class VendorBillingSystemShipController : BaseController
    {
        #region property

        private static IPponProvider _pp;
        private static IHiDealProvider _hp;
        private static IOrderProvider _op;
        private static ISellerProvider _sp;
        private static IMemberProvider _mp;
        private static IVerificationProvider _vp;
        private static ISysConfProvider _conf;
        private static ILog logger = LogManager.GetLogger("ShipProductInventory");
        private static Core.IServiceProvider _service;
        private static IItemProvider _ip;
        private static IWmsProvider _wp;

        static VendorBillingSystemShipController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _hp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
            _service = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
            _ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
            _wp = ProviderFactory.Instance().GetProvider<IWmsProvider>();
        }

        #endregion property

        #region Action

        #region 宅配檔次列表

        [VbsAuthorize]
        public ActionResult ShipDealList(VbsShipState? state)
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            state = state ?? VbsShipState.Processing;

            var vdlModel = new ShipDealListModel();
            var infos = GetAllowedShipDealList(state.Value, false);
            
            Session[VbsSession.VbsShipDealInfos.ToString()] = infos.OrderBy(x => x.ShipState)
                                                              .ThenBy(x => x.ShipPeriodStartTime)
                                                              .ThenBy(x => x.ShipPeriodEndTime).ToList();
            vdlModel.DealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.IsEnableVbsNewShipFile = _conf.IsEnableVbsNewShipFile;
            Session[VbsSession.ParentActionName.ToString()] = "ShipDealList";

            if(state == VbsShipState.ShippingOverdue)
            {
                List<VbsShipDealInfo> vsdis = new List<VbsShipDealInfo>();
                foreach(VbsShipDealInfo vdi in vdlModel.DealInfos)
                {
                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vdi.DealId);
                    if (!string.IsNullOrWhiteSpace(vpd.LabelIconList))
                    {
                        string[] ss = vpd.LabelIconList.Split(',');
                        if (!ss.Contains(((int)DealLabelSystemCode.TwentyFourHoursArrival).ToString()))
                        {
                            vsdis.Add(vdi);
                        }
                    }
                }
                vdlModel.DealInfos = vsdis;
            }

            if (VBSFacade.IsEnabledVbsShipRule2k18())
            {
                List<VbsShipDealInfo> vsdis = vdlModel.DealInfos.OrderByDescending(x => x.UnShipCount).ToList();
                vdlModel.DealInfos = vsdis;
            }

            return View("ShipDealList", vdlModel);
        }

        [VbsAuthorize]
        public ActionResult EmployeeShipDealList(string selQueryOption, string queryKeyword,
            DateTime? queryStartTime, DateTime? queryEndTime)
        {
            if (VbsCurrent.Is17LifeEmployee == false)
            {
                throw new Exception("這是17Life才能操作的功能");
            }
            var vdlModel = new ShipDealListModel();
            var infos = new List<VbsShipDealInfo>();
            var now = DateTime.Now;
            queryKeyword = string.IsNullOrEmpty(queryKeyword) ? queryKeyword : queryKeyword.Trim();

            List<ViewVbsToHouseDeal> deals = _pp.ViewVbsToHouseDealGetList(
                selQueryOption, queryKeyword, queryStartTime, queryEndTime)
                .Where(x =>
                {
                    if (!x.DealStartTime.HasValue || !x.DealEndTime.HasValue ||
                        !x.UseStartTime.HasValue || !x.UseEndTime.HasValue) //不撈取未設定販賣時間及兌換時間之異常檔次
                    {
                        return false;
                    }

                    if (x.BusinessHourStatus.HasValue)
                    {
                        if (Helper.IsFlagSet(x.BusinessHourStatus.Value, BusinessHourStatus.ComboDealSub)) //不撈取多檔次子檔
                        {
                            return false;
                        }
                    }

                    //開賣時間還沒開始，不顯示
                    if (DateTime.Compare(now, x.DealStartTime.Value) < 0)
                    {
                        return false;
                    }

                    if (x.DealEstablished.HasValue)
                    {
                        return int.Equals(1, x.DealEstablished); //過門檻
                    }

                    if (x.DealType.Equals((int)VbsDealType.PiinLife))
                    {
                        return true;
                    }

                    return DateTime.Compare(now, x.DealEndTime.Value) < 0; //目前時間未超過結檔時間   
                })
                .ToList();

            if (deals.Count > 0)
            {
                foreach (var deal in deals)
                {
                    var shipState = deal.FinalBalanceSheetDate.HasValue
                                    ? VbsShipState.Finished
                                    : deal.ShippedDate.HasValue
                                        ? VbsShipState.ReturnAndChanging
                                        : DateTime.Compare(deal.UseEndTime.Value.Date, now.Date) < 0
                                            ? VbsShipState.ShippingOverdue
                                            : DateTime.Compare(deal.UseStartTime.Value.Date, now.Date) > 0
                                                ? VbsShipState.NotYetShip
                                                : VbsShipState.Shipping;

                    var info = new VbsShipDealInfo
                    {
                        //抓取24H / 72H到貨 標籤
                        DealName = string.Format("{0}{1}", GetLableDesc(deal.LabelIconList), deal.ProductName),
                        DealId = deal.ProductGuid,
                        DealType = (VbsDealType)deal.DealType,
                        DealUniqueId = deal.ProductId,
                        ShipPeriodStartTime = deal.UseStartTime.Value,
                        ShipPeriodEndTime = deal.UseEndTime.Value,
                        ShipState = shipState
                    };

                    infos.Add(info);
                }
            }
            Session[VbsSession.VbsShipDealInfos.ToString()] = infos.OrderBy(x => x.ShipState)
                                                                   .ThenBy(x => x.ShipPeriodStartTime)
                                                                   .ThenBy(x => x.ShipPeriodEndTime).ToList(); 
            vdlModel.DealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;

            if (string.Equals("POST", HttpContext.Request.HttpMethod) && int.Equals(0, vdlModel.DealInfos.Count()))
            {
                vdlModel.IsResultEmpty = true;
            }
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.ParentActionName.ToString()] = "EmployeeShipDealList";

            return View("EmployeeShipDealList", vdlModel);
        }

        #endregion 宅配檔次列表

        #region 出貨管理

        [VbsAuthorize]
        public ActionResult ShipOrderList(Guid id, VbsDealType type,
            int? pageNumber, string selQueryOption, string queryKeyWord)
        {
            bool sortDesc = false;
            string sortCol = "OrderId";
            pageNumber = pageNumber ?? 1;
            queryKeyWord = string.IsNullOrEmpty(queryKeyWord) ? queryKeyWord : queryKeyWord.Trim();
            var vdlModel = new ShipOrderListModel();
            var infos = new List<VbsOrderListInfo>();
            var returnInfos = new List<VbsReturnOrderListInfo>();

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }

            //檢查是否小倉檔次
            bool isConsignment = false;
            if (_conf.IsConsignment)
            {
                isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
            }

            var dealInfos = GetDealSaleInfo(id);
            vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);
            var dealGuids = dealInfos.Select(x => x.DealId);
            //取得照會完成數量
            int[] orderUserMemoList = GetOrderUserMemoListCount(dealInfos, type);
            int orderUserMemoListCount = orderUserMemoList[0];
            int orderUserMemoListCountAll = orderUserMemoList[1];

            if (!string.IsNullOrEmpty(queryKeyWord))
            {
                //撈取物流公司資料
                vdlModel.ShipCompanyInfos = _op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();

                //檔次出貨狀態為已完成 隱藏消費者個資
                bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);

                infos = GetOrderShipList(dealInfos, isHiddenPersonalInfo, selQueryOption, queryKeyWord);

                vdlModel.IsResultEmpty = !infos.Any();
                vdlModel.IsComboDeal = dealInfos.Select(x => x.DealUniqueId).Distinct().Count() > 1;

                //取得退貨訂單
                List<Guid> orderGuids = infos.Select(x => x.OrderGuid).ToList();
                List<ViewOrderReturnFormList> returnOrderList = _op.ViewOrderReturnFormListGetListByOrderGuid(orderGuids)
                                                                       .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                                                                   x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                                                                   x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                                                                   x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                                                                   x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                                                                   x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                                                       .ToList();
                returnInfos = GetReturnOrderList(returnOrderList);
            }

            vdlModel.OrderInfos = GetPagerVbsOrderShipInfos(infos, null, 1, pageNumber.Value - 1, sortCol, sortDesc, null, null, false);
            vdlModel.ReturnOrderInfos = vdlModel.OrderInfos.FirstOrDefault() != null
                                            ? returnInfos.Where(x => x.OrderGuid.Equals(vdlModel.OrderInfos.First().OrderGuid))
                                                         .OrderByDescending(x => x.ReturnApplicationTime)
                                                         .ToList()
                                            : new List<VbsReturnOrderListInfo>();
            vdlModel.OrderUserMemoListCount = orderUserMemoListCount;
            vdlModel.OrderUserMemoListCountAll = orderUserMemoListCountAll;
            vdlModel.DealId = id;
            ViewBag.DealType = type;
            ViewBag.IsConsignment = isConsignment;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            Session[VbsSession.ParentActionName.ToString()] = "ShipOrderList";

            Dictionary<string, int> checkmin = new Dictionary<string, int>();
            var shipCompany= _op.ShipCompanyGetList(null).Where(x => x.Status == true);
            foreach (var item in shipCompany)
            {
                checkmin.Add(item.Id.ToString(), item.Min);
            }
            ViewBag.CheckMin = checkmin;

            return View("ShipOrderList", vdlModel);
        }

        /// <summary>
        /// 單一訂單出貨
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        [ActionName("ShipOrderList")]
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveOrderShip(ProductOrderShip pos)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            try
            {
                string errMessage = "";
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
                }
                if (!pos.ShipCompanyId.HasValue || string.IsNullOrEmpty(pos.ShipNo) || !pos.ShipTime.HasValue)
                {
                    errMessage = "資料輸入不完整";
                    CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0};{1}", "單筆出貨失敗", errMessage, false), modifyId, true);
                    return Json(new { IsSuccess = false, Message = errMessage });
                }

                if (pos.ShipMemo.Length > 60)
                {
                    errMessage = "出貨備註只限最多60個字";
                    CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0};{1}", "單筆出貨失敗", errMessage, false), modifyId, true);
                    return Json(new { IsSuccess = false, Message = errMessage });
                }

                //檢查輸入之貨運單號是否在該檔次已存在
                if (OrderShipUtility.CheckDealShipNoExist(Guid.Parse(pos.DealId), (OrderClassification)pos.OrderClassification, pos.OrderGuid, pos.ShipNo))
                {
                    errMessage = "貨運單號重複，請填入正確的貨運單號";
                    CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0};{1}", "單筆出貨失敗", errMessage, false), modifyId, true);
                    return Json(new { IsSuccess = false, Message = errMessage });
                }

                if (CheckEverExchange(pos.OrderGuid))
                {
                    errMessage = "已有換貨紀錄，不允許異動原始出貨資訊";
                    CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0};{1}", errMessage, false), modifyId, true);
                    return Json(new { IsSuccess = false, Message = errMessage });
                }

                bool result = false;
                string message = string.Empty;

                OrderShip os = new OrderShip
                {
                    OrderGuid = pos.OrderGuid,
                    OrderClassification = pos.OrderClassification,
                    ShipCompanyId = pos.ShipCompanyId,
                    ShipNo = pos.ShipNo,
                    ShipTime = pos.ShipTime,
                    Type = (int)OrderShipType.Normal
                };

                //若出貨備註內容同輸入提示內容 不須儲存備註內容
                if (!pos.ShipMemo.Trim().Equals(pos.ShipMemoDefault))
                {
                    os.ShipMemo = pos.ShipMemo;
                }

                os.ModifyId = modifyId;
                os.ModifyTime = DateTime.Now;

                Session[VbsSession.ParentActionName.ToString()] = "SaveOrderShip";

                //若該筆訂單未有出貨紀錄(剔除換貨及結案) 使用新增 否則為修改
                if (pos.OrderShipId.HasValue)
                {
                    os.Id = pos.OrderShipId.Value;

                    result = OrderShipUtility.OrderShipUpdate(os);
                }
                else
                {
                    //檢查出貨資訊是否已存在(避免重複按下儲存按鈕)
                    if (OrderShipUtility.CheckShipDataExist(pos.OrderGuid, (OrderClassification)pos.OrderClassification))
                    {
                        result = true;
                        message = "出貨資訊已存在，請勿重複點選新增";
                    }
                    else
                    {
                        os.CreateId = os.ModifyId;
                        os.CreateTime = os.ModifyTime;
                        result = OrderShipUtility.OrderShipSet(os);

                        //宅配已配送出貨要發推播給使用者(僅只有新增的第一次需要push)
                        var order = _op.OrderGet(pos.OrderGuid);
                        if (!os.SentArrivalNoticePush)
                        {
                            var msg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", order.OrderId);
                            NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, msg, pos.OrderGuid);
                            logger.Info("宅配已出貨 VendorBillingSystemShip.SaveOrderShip: " +
                                       string.Format("UserId={0}, pmsg={1}, Guid={2}", order.UserId, msg, order.OrderId));
                            os.SentArrivalNoticePush = true;
                        }
                    }
                }

                if (!result)
                {
                    CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0}", "單筆出貨失敗", false), modifyId, true);
                    message = "儲存失敗，請稍後再試";
                }
                else
                {
                    if (pos.OrderShipId.HasValue)
                        CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0}", "單筆出貨修改成功", false), modifyId, true);
                    else
                        CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0}", "單筆出貨成功", false), modifyId, true);
                    message = "儲存成功";
                }

                return Json(new { IsSuccess = result, Message = message, ShipTime = os.ShipTime.Value.ToString("yyyy/MM/dd"), ShipCompanyId = os.ShipCompanyId, ShipNo = os.ShipNo });
            }
            catch (Exception ex)
            {
                CommonFacade.AddAudit(pos.OrderGuid.ToString(), AuditType.Order, string.Format("{0};{1}", "單筆出貨失敗", ex.Message, false), modifyId, true);
                return Json(new { IsSuccess = false, Message = ex.Message});
            }
            
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult AuditLog(string orderGuid,string message)
        {
            CommonFacade.AddAudit(orderGuid, AuditType.Order, string.Format("{0};{1}", "單筆出貨失敗", message, false), VbsCurrent.AccountId, true);
            return Json(new { IsSuccess = true });
        }

        #endregion 出貨管理

        #region 超取

        [VbsAuthorize]
        public ActionResult IspShipOrderList()
        {
            return View();
        }

        /// <summary>
        /// 超取出貨查詢
        /// </summary>
        /// <param name="pageStart">頁數</param>
        /// <param name="orderId">訂單編號</param>
        /// <param name="dealUniqueId">檔號</param>
        /// <param name="dateStart">訂購日(起)</param>
        /// <param name="dateEnd">訂購日(迄)</param>
        /// <param name="shipStatusPrint">列印狀態</param>
        /// <param name="shipStatusReport">出貨狀態</param>
        /// <param name="d">step</param>
        /// <param name="serviceChannel">超商類型</param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult IspShipOrderList(
            int? pageStart, 
            string orderId, 
            string dealUniqueId, 
            string dateStart,
            string dateEnd,
            int shipStatusPrint,
            int shipStatusReport,
            string d,
            int serviceChannel
            )
        {
            int perDataCnt = 30;

            var infos = GetAllowedShipDealList(VbsShipState.Processing);




            //只處理未回填出貨回覆日之檔次
            //step2,3任何狀態皆可查(因為可查已列印或已出貨)
            if (d.Equals("1"))
            {
                infos = infos.Where(x => x.ShipState == VbsShipState.Shipping || x.ShipState == VbsShipState.ShippingOverdue);
            }
            
            var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryListByIsp(infos.Select(x => x.DealId), false, true), null, int.MaxValue, 0, "OrderCreateTime", false, null, null, true)
                                    .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);

            
            List<Guid> ispOGuids = new List<Guid>();
            if (_conf.EnableSevenIsp)
            {
                //判斷撈7-11或全家
                ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                    .Where(y => y.ProductDeliveryType == serviceChannel)
                                                    .Select(y => y.OrderGuid).ToList();
            }
            else
            {
                //指撈取全家超取的訂單
                ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                    .Where(y => y.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                                                    .Select(y => y.OrderGuid).ToList();
            }
            

            List<VbsOrderListInfo> resultData = dealOrderInfos
                .Where(x => ispOGuids.Contains(x.OrderGuid))
                .ToList();

            FilterIspShipData(ref resultData, orderId, dealUniqueId, dateStart, dateEnd, shipStatusPrint, shipStatusReport, d);

            resultData = resultData.OrderBy(x => x.OrderCreateTime).ToList();

            int totalCount = 0;
            int totalPages = 0;
            totalCount = resultData.Count();
            resultData = resultData.Skip((pageStart.GetValueOrDefault(1) - 1) * perDataCnt).Take(perDataCnt).ToList();


            totalPages = totalCount / perDataCnt;
            if ((totalCount > perDataCnt) && (totalCount % perDataCnt != 0))
            {
                totalPages += 1;
            }
            if (totalPages == 0)
            {
                totalPages = 1;
            }

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPages,
                CurrentPage = pageStart,
                Data = resultData.OrderBy(x => x.OrderCreateTime).ToList()
            };


            ProposalFacade.ProposalPerformanceLogSet("IspShipOrderList", new JsonSerializer().Serialize(new
            {
                pageStart = pageStart.ToString(),
                orderId = orderId.ToString(),
                dealUniqueId = dealUniqueId.ToString(),
                dateStart = dateStart.ToString(),
                dateEnd = dateEnd.ToString(),
                shipStatusPrint = shipStatusPrint.ToString(),
                shipStatusReport = shipStatusReport.ToString(),
                d = d.ToString(),
                serviceChannel = serviceChannel.ToString(),
                TotalCount = totalCount.ToString(),
                TotalPage = totalPages.ToString(),
                CurrentPage = pageStart.ToString(),
                Data = string.Join("、", resultData.Select(x => x.OrderId))
            }), UserName);
            return Json(obj);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetShipNormalCount()
        {
            int cnt = 0;
            if (VBSFacade.IsEnabledVbsShipRule2k18())
            {
                var dealOrderInfos = GetAllowedShipDealList(VbsShipState.Processing);

                cnt = dealOrderInfos.Sum(x => x.UnShipCount);
            }

            return Json(cnt);
        }

        /// <summary>
        /// 超取筆數查詢
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult IspShipOrderCount()
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 1");
            bool isInstorePickup = false;
            if (Session[VbsSession.IsInstorePickup.ToString()] != null)
            {
                isInstorePickup = (bool)Session[VbsSession.IsInstorePickup.ToString()];
            }
            List<VbsOrderListInfo> resultData = new List<VbsOrderListInfo>();
            List<VbsOrderListInfo> resultDataNotShip = new List<VbsOrderListInfo>();
            List<Guid> oidList = new List<Guid>();
            int familyCount = 0;
            int sevenCount = 0;

            if (isInstorePickup)
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 1-1");
                var infos = GetAllowedShipDealList(VbsShipState.Processing);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 2");
                //只處理未回填出貨回覆日之檔次
                infos = infos.Where(x => x.ShipState == VbsShipState.Shipping || x.ShipState == VbsShipState.ShippingOverdue);
                //var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryList(infos.Select(x => x.DealId), false, false), VbsOrderShipState.UnShip, int.MaxValue, 0, "OrderCreateTime", false, null, null)
                //                        .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 3");

                //var infoIsp = GetOrderShipInventoryListByIsp(infos.Select(x => x.DealId), false, true);
                var saleInfos = GetDealSaleInfo(infos.Select(x => x.DealId)); ;
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 3-1");
                var infoIsp = OrderShipInventoryListGet(saleInfos, false, true);

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 3-2");
                var dealOrderInfos = GetPagerVbsOrderShipInfos(infoIsp, null, int.MaxValue, 0, "OrderCreateTime", false, null, null, true);

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 4");

                //指撈取全家+7-11超取的訂單
                List<Guid> ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                        .Where(y => y.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup || y.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                                                        .Select(y => y.OrderGuid).ToList();
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 5");
                resultData = dealOrderInfos
                    .Where(x => ispOGuids.Contains(x.OrderGuid))
                    .ToList();

                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 6");
                //先把原有的複製起來
                resultDataNotShip = resultData.ToList();
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 7");
                //篩選待出貨
                FilterIspShipData(ref resultData, string.Empty, string.Empty, string.Empty, string.Empty, 1, 1, "1");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 8");
                familyCount = resultData.Where(x => x.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup).Count();
                sevenCount = resultData.Where(x => x.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup).Count();
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 9");
                //篩選待回報出貨
                FilterIspShipData(ref resultDataNotShip, string.Empty, string.Empty, string.Empty, string.Empty, 1, 1, "4");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 10");
            }


            List<dynamic> list = new List<dynamic>();

            //if (VBSFacade.IsEnabledVbsShipRule2k18())
            //{

            //    var dealOrderInfos = GetAllowedShipDealList(VbsShipState.Processing);

            //    shinCount = dealOrderInfos.Sum(x=>x.UnShipCount);

            //    list.Add(new
            //    {
            //        Name = "待出貨訂單-宅配",
            //        Value = shinCount,
            //        Url = "/vbs/ship/ShipDealList"
            //    });

            //}
        


            list.Add(new {
                Name = "待出貨訂單-全家",
                Value = familyCount,
                Url = "/vbs/ship/ispshiporderlist?d=1"
            });


            list.Add(new
            {
                Name = "待出貨訂單-7-11",
                Value = sevenCount,
                Url = "/vbs/ship/ispshiporderlist?d=1&t=2"
            });


            list.Add(new
            {
                Name = "待回報出貨完成訂單-超商",
                Value = resultDataNotShip.Count(),
                Url = "/vbs/ship/ispshiporderlist?d=3"
            });

            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(UserName, true).Select(x => x.Key).ToList();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 11");
            if (sellerGuids.Count > 0)
            {
                List<OrderProductDelivery> opdsRetuen = _op.OrderProductDeliveryGetByReturn(sellerGuids).ToList();
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 12");
                List<VbsOrderListInfo> resultDataRetuen = OrderShipUtility.GetIspReturnOrder(opdsRetuen, 1, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, 0);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 13");
                list.Add(new
                {
                    Name = "待處理刷退與驗退訂單-超商",
                    Value = resultDataRetuen.Count(),
                    Url = "/vbs/ship/IspShipOrderRefund"
                });
            }
            

            dynamic obj = new {
                IsInstorePickup = isInstorePickup,
                Data = list
            };
            ProposalFacade.ProposalPerformanceLogSet("IspShipOrderCount", builder.ToString() + " " + string.Join("、", list.Select(x => x.Value).ToList()), UserName);
            return Json(obj);
        }


        /// <summary>
        /// 確認pchome合約
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ConfirmPchomeReturnNotice2k19()
        {
            bool IsSucces = false;

            try
            {
                string createId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;

                VbsConfirmNoticeLog log = new VbsConfirmNoticeLog
                {
                    AccountId = VbsCurrent.AccountId,
                    Type = (int)Core.Enumeration.VbsConfirmNoticeType.PchomeReturnRule2k19,
                    CreateId = createId,
                    CreateTime = DateTime.Now
                };
                _sp.VbsConfirmNoticeLogSet(log);
                IsSucces = true;
            }
            catch { }

            return Json(IsSucces);
        }

        /// <summary>
        /// 確認各種跳窗
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ConfirmBpopup(int type)
        {
            bool IsSucces = false;

            try
            {
                string createId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;

                VbsConfirmNoticeLog log = new VbsConfirmNoticeLog
                {
                    AccountId = VbsCurrent.AccountId,
                    Type = type,
                    CreateId = createId,
                    CreateTime = DateTime.Now
                };
                _sp.VbsConfirmNoticeLogSet(log);
                IsSucces = true;
            }
            catch { }

            return Json(IsSucces);
        }


        /// <summary>
        /// 產生物流編號
        /// </summary>
        /// <param name="orderList"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetApiTrackingNumber(string orderList)
        {
            try
            {
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
                }


                List<Guid> oList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

                OrderCollection orders = _op.OrderGet(oList);
                List<Core.ModelPartial.ShipmentOrderModel> orderModel = new List<Core.ModelPartial.ShipmentOrderModel>();

                OrderProductDeliveryCollection opds = _op.OrderProductDeliveryGetListByOrderGuid(oList);

                #region 判斷尚未產生MakeOrder之訂單，呼叫api產生到大平台

                List<OrderProductDelivery> makeOrders = opds.Where(x => x.IsApplicationServerConnected != (int)IspIsApplicationServerConnectedStatus.SuccessConnected).ToList();
                if (makeOrders.Any())
                    ISPFacade.IspMakeOrder(makeOrders.Select(p => p.OrderGuid).ToList());

                #endregion 判斷尚未產生MakeOrder之訂單，呼叫api產生到大平台

                var infos = GetAllowedShipDealList(VbsShipState.Processing);


                infos = infos.Where(x => x.ShipState == VbsShipState.Shipping || x.ShipState == VbsShipState.ShippingOverdue);
                var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryListByIsp(infos.Select(x => x.DealId), false, true), null, int.MaxValue, 0, "OrderCreateTime", false, null, null, true)
                                    .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);


                List<Guid> ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                        .Where(y => y.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                                                        .Select(y => y.OrderGuid).ToList();

                List<VbsOrderListInfo> resultData = dealOrderInfos
                    .Where(x => ispOGuids.Contains(x.OrderGuid))
                    .ToList();

                var dealGuids = resultData.Select(x => x.DealInfo.DealId);

                var returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                   .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                               x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                               x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                               x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                               x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                               x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                   .ToLookup(x => x.OrderGuid);


                foreach (Order o in orders)
                {
                    if (returnLists.Contains(o.Guid))
                    {
                        continue;
                    }

                    var oriData = orderModel.Where(x => x.OrderType == IspOrderType.B2C && x.StoreKey == o.SellerGuid).FirstOrDefault();
                    if (oriData == null)
                    {
                        IspOrderType orderType = IspOrderType.B2C;
                        OrderProductDelivery opd = opds.Where(x => x.OrderGuid == o.Guid).FirstOrDefault();
                        if(opd != null)
                        {
                            orderType = (IspOrderType)opd.IspOrderType;
                        }
                        Core.ModelPartial.ShipmentOrderModel om = new Core.ModelPartial.ShipmentOrderModel();
                        om.OrderType = IspOrderType.B2C;
                        if (_conf.EnableSevenIsp)
                        {
                            om.channelType = (ServiceChannel)opd.ProductDeliveryType;
                            om.isReTag = false;//預設false
                        }
                        om.StoreKey = o.SellerGuid;
                        om.OrderList = new List<string>()
                        {
                            o.OrderId
                        };

                        
                        orderModel.Add(om);
                    }
                    else
                    {
                        if (oriData.OrderList != null)
                        {
                            if (!oriData.OrderList.Contains(o.OrderId))
                            {
                                oriData.OrderList.Add(o.OrderId);
                            }                            
                        }
                    }
                }

                //呼叫配編api產生到大平台
                dynamic rtnObj = ISPFacade.GetApiTrackingNumber(orderModel);

                List<Core.ModelPartial.IspShipmentNumberRespModel> rtnData = new JsonSerializer().Deserialize<List<Core.ModelPartial.IspShipmentNumberRespModel>>(rtnObj);
                int successCount = 0;
                foreach (Core.ModelPartial.IspShipmentNumberRespModel obj in rtnData)
                {
                    foreach (Core.ModelPartial.IspShipmentNumberModel isp in obj.ShipmentNumberItemList)
                    {
                        Order o = orders.Where(x => x.OrderId == isp.OrderKey).FirstOrDefault();
                        if (o == null)
                        {
                            o = _op.OrderGet(Order.Columns.OrderId, isp.OrderKey);
                        }

                        if (o != null && o.IsLoaded)
                        {
                            OrderProductDelivery opd = _op.OrderProductDeliveryGetByOrderGuid(o.Guid);
                            opd.PreShipNo = isp.ShipmentNo;
                            _op.OrderProductDeliverySet(opd);
                        }
                        successCount++;
                    }
                }

                return Json(new { IsSuccess = true, Count = successCount });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new { IsSuccess = false, Message = "產生物流編號失敗" });
            }            
        }


        /// <summary>
        /// 列印出貨標籤
        /// </summary>
        /// <param name="orderList"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult PrintShipmentPDF(string orderList)
        {

            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
            }


            List<Guid> oList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

            OrderCollection orders = _op.OrderGet(oList);
            //Dictionary<Guid, List<string>> dicOrders = new Dictionary<Guid, List<string>>();
            List<Core.ModelPartial.ShipmentOrderModel> orderModel = new List<Core.ModelPartial.ShipmentOrderModel>();

            OrderProductDeliveryCollection opds = _op.OrderProductDeliveryGetListByOrderGuid(oList);

            foreach (Order o in orders)
            {
                var oriData = orderModel.Where(x => x.OrderType == IspOrderType.B2C && x.StoreKey == o.SellerGuid).FirstOrDefault();
                if (oriData == null)
                {
                    IspOrderType orderType = IspOrderType.B2C;
                    OrderProductDelivery opd = opds.Where(x => x.OrderGuid == o.Guid).FirstOrDefault();
                    if (opd != null)
                    {
                        orderType = (IspOrderType)opd.IspOrderType;
                    }
                    Core.ModelPartial.ShipmentOrderModel om = new Core.ModelPartial.ShipmentOrderModel();
                    if (_conf.EnableSevenIsp)
                    {
                        om.OrderType = IspOrderType.B2C;
                        om.channelType = (ServiceChannel)opd.ProductDeliveryType;
                        om.isReTag = false;//預設false
                    }
                    om.StoreKey = o.SellerGuid;
                    om.OrderList = new List<string>()
                    {
                        o.OrderId
                    };


                    orderModel.Add(om);
                }
                else
                {
                    if (oriData.OrderList != null)
                    {
                        if (!oriData.OrderList.Contains(o.OrderId))
                        {
                            oriData.OrderList.Add(o.OrderId);
                        }
                    }
                }
                //    if (!dicOrders.ContainsKey(o.SellerGuid))
                //    {
                //        dicOrders.Add(o.SellerGuid,
                //            new List<string> { o.OrderId }
                //            );
                //    }
                //    else
                //    {
                //        var keyData = dicOrders.Where(x => x.Key == o.SellerGuid).FirstOrDefault();
                //        List<string> valueData = keyData.Value as List<string>;
                //        if (!valueData.Contains(o.OrderId))
                //        {
                //            valueData.Add(o.OrderId);
                //        }
                //    }
            }

            string dlUrl = string.Empty;
            int success = 0;
            int fail = 0;
            string failOrderKey = string.Empty;
            string message = string.Empty;
            try
            {
                dynamic rtnObj = ISPFacade.GetApiShipmentPDF(orderModel);
                Core.ModelPartial.IspShipmentPdfRespModel rtnData = new JsonSerializer().Deserialize<Core.ModelPartial.IspShipmentPdfRespModel>(rtnObj);

                
                foreach (Core.ModelPartial.IspShipmentPdfListRespModel models in rtnData.SuccessData)
                {
                    foreach (Core.ModelPartial.IspShipmentNumberModel model in models.ShipmentNumberItemList)
                    {
                        Order o = orders.Where(x => x.OrderId == model.OrderKey).FirstOrDefault();
                        if (o != null)
                        {
                            OrderProductDelivery opd = _op.OrderProductDeliveryGetByOrderGuid(o.Guid);
                            if (opd.IsLoaded)
                            {
                                opd.IsShipmentPdf = true;
                                _op.OrderProductDeliverySet(opd);
                                success++;
                            }
                        }
                    }
                }

                if (rtnData.FailData != null)
                {
                    foreach (Core.ModelPartial.IspShipmentPdfFailModel model in rtnData.FailData)
                    {
                        failOrderKey += model.OrderKey + "(" + model.Message + ") <br>";
                        fail++;
                    }
                }
                

                if (success > 0)
                {
                    message = "成功共" + success.ToString() + "筆 <br>";
                    dlUrl = rtnData.pdfDownloadUrl;
                }
                if (fail > 0)
                {
                    message += "失敗共" + fail.ToString() + "筆 <br>";
                    message += "失敗清單：" + failOrderKey;

                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new { IsSuccess = false, Message = "列印標籤失敗" });
            }
            
            return Json(new { IsSuccess = true, Url = dlUrl, Message = message });
        }

        /// <summary>
        /// 回報確認出貨完成
        /// </summary>
        /// <param name="posList"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveIspOrderShip(string posData)
        {

            try
            {
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
                }

                string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                List<ProductOrderShip> posList = new JsonSerializer().Deserialize<List<ProductOrderShip>>(posData);

                ShipCompanyCollection shipCompany = _op.ShipCompanyGetList(null);

                bool result = false;
                string message = string.Empty;
                List<string> errMsg = new List<string>();

                OrderCollection orders = _op.OrderGet(posList.Select(x => x.OrderGuid).ToList());
                OrderProductDeliveryCollection opdList = _op.OrderProductDeliveryGetListByOrderGuid(posList.Select(x => x.OrderGuid).ToList());

                List<Core.ModelPartial.ShipmentOrderModel> orderModel = new List<Core.ModelPartial.ShipmentOrderModel>();

                foreach (Order o in orders)
                {
                    var oriData = orderModel.Where(x => x.OrderType == IspOrderType.B2C && x.StoreKey == o.SellerGuid).FirstOrDefault();
                    if (oriData == null)
                    {
                        IspOrderType orderType = IspOrderType.B2C;
                        OrderProductDelivery opd = opdList.Where(x => x.OrderGuid == o.Guid).FirstOrDefault();
                        if (opd != null)
                        {
                            orderType = (IspOrderType)opd.IspOrderType;
                        }
                        Core.ModelPartial.ShipmentOrderModel om = new Core.ModelPartial.ShipmentOrderModel();
                        om.StoreKey = o.SellerGuid;
                        om.OrderType = IspOrderType.B2C;
                        om.OrderList = new List<string>()
                        {
                            o.OrderId
                        };
                        orderModel.Add(om);
                    }
                    else
                    {
                        if (oriData.OrderList != null)
                        {
                            if (!oriData.OrderList.Contains(o.OrderId))
                            {
                                oriData.OrderList.Add(o.OrderId);
                            }
                        }
                    }
                }

                dynamic rtnApiObj = ISPFacade.ShippingDelivery(orderModel);

                List<Core.ModelPartial.ShippingDeliveryRtnModel> rtnData = new JsonSerializer().Deserialize<List<Core.ModelPartial.ShippingDeliveryRtnModel>>(rtnApiObj);

                foreach (ProductOrderShip pos in posList)
                {
                    Order order = orders.Where(x => x.Guid == pos.OrderGuid).FirstOrDefault();
                    OrderProductDelivery opd = opdList.Where(x => x.OrderGuid == pos.OrderGuid).FirstOrDefault();

                    var rtn = rtnData.Where(x => x.OrderKey == order.OrderId).FirstOrDefault();
                    if (!rtn.IsSuccess)
                    {
                        continue;
                    }

                    if (string.IsNullOrEmpty(pos.ShipNo))
                    {
                        errMsg.Add(string.Format("{0}-超商物流編號不完整", order.OrderId));
                    }

                    //檢查輸入之貨運單號是否在該檔次已存在
                    if (OrderShipUtility.CheckDealShipNoExist(Guid.Parse(pos.DealId), (OrderClassification)pos.OrderClassification, pos.OrderGuid, pos.ShipNo))
                    {
                        //return Json(new { IsSuccess = false, Message = "貨運單號重複，請填入正確的貨運單號" });
                    }

                    if (CheckEverExchange(pos.OrderGuid))
                    {
                        errMsg.Add(string.Format("{0}-已有換貨紀錄，不允許異動原始出貨資訊", order.OrderId));
                        //return Json(new { IsSuccess = false, Message = "已有換貨紀錄，不允許異動原始出貨資訊" });
                    }

                    int shipCompanyId = default(int);
                    if (opd.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                    {
                        ShipCompany company = shipCompany.Where(x => x.ShipCompanyName == "全家").FirstOrDefault();
                        if (company != null)
                        {
                            shipCompanyId = company.Id;
                        }
                    }
                    else if (opd.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                    {
                        ShipCompany company = shipCompany.Where(x => x.ShipCompanyName == "7-11").FirstOrDefault();
                        if (company != null)
                        {
                            shipCompanyId = company.Id;
                        }
                    }

                    OrderShip os = new OrderShip
                    {
                        OrderGuid = pos.OrderGuid,
                        OrderClassification = (int)OrderClassification.LkSite,
                        ShipCompanyId = shipCompanyId,
                        ShipNo = pos.ShipNo,
                        ShipTime = DateTime.Today,
                        Type = (int)OrderShipType.Normal,
                        ShipMemo = "超商取貨"
                    };

                    //若出貨備註內容同輸入提示內容 不須儲存備註內容
                    if (pos.ShipMemo != null && !pos.ShipMemo.Equals(pos.ShipMemoDefault))
                    {
                        os.ShipMemo = pos.ShipMemo;
                    }

                    os.ModifyId = modifyId;
                    os.ModifyTime = DateTime.Now;

                    //若該筆訂單未有出貨紀錄(剔除換貨及結案) 使用新增 否則為修改
                    if (pos.OrderShipId.HasValue)
                    {
                        os.Id = pos.OrderShipId.Value;

                        result = OrderShipUtility.OrderShipUpdate(os);
                        if (result)
                        {
                            OrderShipUtility.OrderShipLogSet(os);
                        }
                    }
                    else
                    {
                        //檢查出貨資訊是否已存在(避免重複按下儲存按鈕)
                        if (OrderShipUtility.CheckShipDataExist(pos.OrderGuid, (OrderClassification)pos.OrderClassification))
                        {
                            //result = true;
                            //message = "出貨資訊已存在，請勿重複點選新增";
                            errMsg.Add(string.Format("{0}-出貨資訊已存在，請勿重複點選新增", order.OrderId));
                        }
                        else
                        {
                            os.CreateId = os.ModifyId;
                            os.CreateTime = os.ModifyTime;

                            if (!os.DcReceiveNoticePush)
                            {
                                //丟至mq直至上班時間做推播通知
                                var dcReceiveSuccessMsg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", order.OrderId);
                                NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, dcReceiveSuccessMsg, order.Guid);
                                logger.Info("AppPushMQ: 超取已出貨 RefreshOrder.PushMemberOrderMessageOnWorkTime: " +
                                            string.Format("UserId={0}, pmsg={1}, Guid={2}", order.UserId, dcReceiveSuccessMsg, order.OrderId));
                                os.DcReceiveNoticePush = true;
                            }

                            result = OrderShipUtility.OrderShipSet(os);
                            if (result)
                            {
                                OrderShipUtility.OrderShipLogSet(os);

                            }
                            else
                            {
                                CommonFacade.AddAudit(os.OrderGuid.ToString(), AuditType.Order, string.Format("{0}", "超取出貨失敗", false), modifyId, true);
                            }
                        }
                    }
                }

                if (errMsg.Count() > 0)
                {
                    result = false;
                    foreach (string msg in errMsg)
                    {
                        string orderId = msg.Split("-")[0];
                        string m = msg.Split("-")[1];
                        Order o = _op.OrderGet(Order.Columns.OrderId, orderId);
                        CommonFacade.AddAudit(o.Guid.ToString(), AuditType.Order, string.Format("{0};{1}", "超取出貨失敗", m, false), modifyId, true);
                    }

                }

                dynamic rtnObj = new
                {
                    IsSuccess = result,
                    Message = string.Join(",", errMsg)
                };
                return Json(rtnObj);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new { IsSuccess = false, Message = "出貨失敗" });
            }
            
        }


        /// <summary>
        /// 超取刷驗退顯示
        /// </summary>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult IspShipOrderRefund()
        {
            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(UserName, true).Select(x => x.Key).ToList();

            string returnCycle = string.Empty;
            string returnType = string.Empty;
            if (sellerGuids.Count() > 0)
            {
                ViewVbsInstorePickupCollection ipsList = new ViewVbsInstorePickupCollection();
                foreach (Guid sid in sellerGuids)
                {
                    ipsList.AddRange(_sp.ViewVbsInstorePickupCollectionGet(sid));
                }
                foreach (ViewVbsInstorePickup ips in ipsList)
                {
                    returnCycle += "【" + Helper.GetDescription((ServiceChannel)ips.ServiceChannel) + "】" + Helper.GetDescription((RrturnCycle)ips.ReturnCycle.GetValueOrDefault(0));
                    returnType += "【" + Helper.GetDescription((ServiceChannel)ips.ServiceChannel) + "】" + Helper.GetDescription((RetrunShip)ips.ReturnType.GetValueOrDefault(0));
                }
            }

            ViewBag.ReturnCycle = returnCycle;
            ViewBag.ReturnType = returnType;

            return View();
        }

        /// <summary>
        /// 超取刷驗退查詢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="dateType"></param>
        /// <param name="orderId"></param>
        /// <param name="dealUniqueId"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="csv"></param>
        /// <param name="abnormal"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetIspShipOrderRefund(
            int? pageStart,
            string dateType,
            string orderId,
            string dealUniqueId,
            string dateStart,
            string dateEnd,
            int csv,
            int abnormal
            )
        {
            int perDataCnt = 30;
            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(UserName, true).Select(x => x.Key).ToList();

            List<OrderProductDelivery> opds = _op.OrderProductDeliveryGetByReturn(sellerGuids).ToList();
            List<VbsOrderListInfo> resultData = OrderShipUtility.GetIspReturnOrder(opds, pageStart, orderId, dealUniqueId, dateType, dateStart, dateEnd, csv, abnormal);



            int totalCount = 0;
            int totalPages = 0;

            totalCount = resultData.Count();

            resultData = resultData.Skip((pageStart.GetValueOrDefault(1) - 1) * perDataCnt).Take(perDataCnt).ToList();

            totalPages = totalCount / perDataCnt;
            if ((totalCount > perDataCnt) && (totalCount % perDataCnt != 0))
            {
                totalPages += 1;
            }
            if (totalPages == 0)
            {
                totalPages = 1;
            }

            dynamic obj = new
            {
                TotalCount = totalCount,
                TotalPage = totalPages,
                CurrentPage = pageStart,
                Data = resultData.OrderByDescending(x => x.OrderCreateTime)
            };

            return Json(obj);
        }


        /// <summary>
        /// 超取刷驗退匯出清冊
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="dateType"></param>
        /// <param name="orderId"></param>
        /// <param name="dealUniqueId"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="csv"></param>
        /// <param name="abnormal"></param>
        [VbsAuthorize]
        public void GetIspShipOrderRefundForExcel(
        int? pageStart,
        string dateType,
        string orderId,
        string dealUniqueId,
        string dateStart,
        string dateEnd,
        int csv,
        int abnormal
        )
        {
            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(UserName, true).Select(x => x.Key).ToList();

            List<OrderProductDelivery> opds = _op.OrderProductDeliveryGetByReturn(sellerGuids).ToList();
            List<VbsOrderListInfo> resultData = OrderShipUtility.GetIspReturnOrder(opds, pageStart, orderId, dealUniqueId, dateType, dateStart, dateEnd, csv, abnormal);

            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("超商刷驗退清冊");
            /*列表寬度設定*/
            sheet.SetColumnWidth(0, 20 * 256);
            sheet.SetColumnWidth(1, 10 * 256);
            sheet.SetColumnWidth(2, 34 * 256);
            sheet.SetColumnWidth(3, 14 * 256);
            sheet.SetColumnWidth(4, 14 * 256);
            sheet.SetColumnWidth(5, 20 * 256);
            sheet.SetColumnWidth(6, 10 * 256);
            sheet.SetColumnWidth(7, 18 * 256);
            sheet.SetColumnWidth(8, 20 * 256);
            sheet.SetColumnWidth(9, 20 * 256);
            sheet.SetColumnWidth(10, 60 * 256);
            sheet.SetColumnWidth(11, 40 * 256);
            sheet.SetColumnWidth(12, 6 * 256);
            sheet.SetColumnWidth(13, 10 * 256);
            sheet.SetColumnWidth(14, 10 * 256);
            sheet.SetColumnWidth(15, 14 * 256);
            sheet.SetColumnWidth(16, 14 * 256);

            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("商家收到退貨包裹");
            cols.CreateCell(1).SetCellValue("問題類別");
            cols.CreateCell(2).SetCellValue("問題敘述");
            cols.CreateCell(3).SetCellValue("超商驗退日");
            cols.CreateCell(4).SetCellValue("檔號");
            cols.CreateCell(5).SetCellValue("訂單編號");
            cols.CreateCell(6).SetCellValue("超商類型");
            cols.CreateCell(7).SetCellValue("取貨門市");
            cols.CreateCell(8).SetCellValue("超商物流編號");
            cols.CreateCell(9).SetCellValue("收件人");
            cols.CreateCell(10).SetCellValue("檔次(商品)名稱");
            cols.CreateCell(11).SetCellValue("規格");
            cols.CreateCell(12).SetCellValue("數量");
            cols.CreateCell(13).SetCellValue("進貨價");
            cols.CreateCell(14).SetCellValue("售價");
            cols.CreateCell(15).SetCellValue("訂購日");
            cols.CreateCell(16).SetCellValue("出貨完成日");

            int rowIdx = 1;
            foreach (var item in resultData)
            {
                Row row = sheet.CreateRow(rowIdx);
                if (item.CheckButton == 1)
                {
                    row.CreateCell(0).SetCellValue("未確認");
                }
                else if (item.CheckButton == 2)
                {
                    row.CreateCell(0).SetCellValue("請儘速出貨");
                }
                else
                {
                    row.CreateCell(0).SetCellValue("");
                }
                row.CreateCell(1).SetCellValue(item.ReturnType);
                row.CreateCell(2).SetCellValue(item.DcReturnReason);
                row.CreateCell(3).SetCellValue(item.DcReturnTime == null ? "" : item.DcReturnTime.Value.ToString("yyyy/MM/dd"));
                row.CreateCell(4).SetCellValue(item.DealInfo.DealUniqueId);
                row.CreateCell(5).SetCellValue(item.OrderId);
                if (item.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                {
                    row.CreateCell(6).SetCellValue("全家");
                }
                else if (item.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                {
                    row.CreateCell(6).SetCellValue("統一");
                }
                else
                {
                    row.CreateCell(6).SetCellValue("其他");
                }
                row.CreateCell(7).SetCellValue(item.IspStoreName);
                row.CreateCell(8).SetCellValue(item.ShipNo);
                row.CreateCell(9).SetCellValue(item.RecipientName);
                row.CreateCell(10).SetCellValue(item.DealInfo.DealName);
                row.CreateCell(11).SetCellValue(item.ItemDescription);
                var rowHeight = row.GetCell(11).StringCellValue.Split("\n");
                int defaultHeight = 16;
                row.HeightInPoints = rowHeight.Count() * defaultHeight;
                row.GetCell(11).CellStyle.WrapText = true;
                row.CreateCell(12).SetCellValue(item.ItemCount);
                row.CreateCell(13).SetCellValue(item.ItemCost.ToString());
                row.CreateCell(14).SetCellValue(item.ItemPrice.ToString());
                if (item.ShipTime == null)
                {
                    row.CreateCell(15).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd"));
                    row.CreateCell(16).SetCellValue("");
                }
                else
                {
                    row.CreateCell(15).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd"));
                    row.CreateCell(16).SetCellValue(item.ShipTime.Value.ToString("yyyy/MM/dd"));
                }

                rowIdx++;
            }


            Response.AddHeader("Content-Disposition", string.Format("attachment; filename={0}_超商刷驗退清冊.xls", DateTime.Now.ToString("yyyy-MM-dd")));
            workbook.Write(Response.OutputStream);

        }


        /// <summary>
        /// 確認刷驗退
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ConfirmDCReturn(Guid oid)
        {
            try
            {
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
                }

                OrderProductDelivery opd = _op.OrderProductDeliveryGetByOrderGuid(oid);
                if (opd != null && opd.IsLoaded)
                {
                    if (opd.DcReceiveFail != 1 && opd.DcReturn != 1)
                    {
                        return Json(false);
                    }
                    if (opd.DcReceiveFail == 1)
                    {
                        //驗退
                        opd.DcReceiveFail = 2;
                    }
                    if (opd.DcReturn == 1)
                    {
                        //刷退
                        opd.DcReturn = 2;
                    }
                    opd.ReturnConfirmTime = DateTime.Now;
                    opd.ReturnConfirmId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId; ;
                    _op.OrderProductDeliverySet(opd);
                }

                return Json(new { IsSuccess = true });
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Json(new { IsSuccess = false, Message = "刷驗退失敗" });
            }
            
        }

        /// <summary>
        /// 超取下載出貨清冊
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="dealUniqueId"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="shipStatusPrint"></param>
        /// <param name="shipStatusReport"></param>
        /// <param name="d"></param>
        /// <param name="serviceChannel"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult DownloadShipExcel(
            string orderId,
            string dealUniqueId,
            string dateStart,
            string dateEnd,
            int shipStatusPrint,
            int shipStatusReport,
            string d,
            int serviceChannel)
        {
            var infos = GetAllowedShipDealList(VbsShipState.Processing);


            infos = infos.Where(x => x.ShipState == VbsShipState.Shipping || x.ShipState == VbsShipState.ShippingOverdue);
            //var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryList(infos.Select(x => x.DealId), false, false), VbsOrderShipState.UnShip, int.MaxValue, 0, "OrderCreateTime", false, null, null)
            //                        .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);
            var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryListByIsp(infos.Select(x => x.DealId), false, true), null, int.MaxValue, 0, "OrderCreateTime", false, null, null, true)
                                    .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);

            List<Guid> ispOGuids = new List<Guid>();
            if (_conf.EnableSevenIsp)
            {
                ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                   .Where(y => y.ProductDeliveryType == serviceChannel)
                                                   .Select(y => y.OrderGuid).ToList();
            }
            else
            {
                ispOGuids = _op.OrderProductDeliveryGetListByOrderGuid(dealOrderInfos.Select(x => x.OrderGuid).ToList())
                                                   .Where(y => y.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                                                   .Select(y => y.OrderGuid).ToList();
            }
               

            List<VbsOrderListInfo> resultData = dealOrderInfos
                .Where(x => ispOGuids.Contains(x.OrderGuid))
                .ToList();

            FilterIspShipData(ref resultData, orderId, dealUniqueId, dateStart, dateEnd, shipStatusPrint, shipStatusReport, d);

            OrderProductDeliveryCollection opdList = _op.OrderProductDeliveryGetListByOrderGuid(resultData.Select(x=>x.OrderGuid).ToList());

            foreach (var res in resultData)
            {
                try
                {
                    if (!string.IsNullOrEmpty(res.ItemNo))
                    {
                        continue;
                    }
                    OrderProductDelivery opd = opdList.Where(x => x.OrderGuid == res.OrderGuid).FirstOrDefault();
                    if (opd != null)
                    {
                        if (opd.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup 
                            || opd.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                        {
                            continue;
                        }
                    }

                    CashTrustLogCollection ctls = _mp.CashTrustLogGetListByOrderGuid(res.OrderGuid);
                    if (ctls.Count() > 0)
                    {
                        Guid bid = ctls.FirstOrDefault().BusinessHourGuid ?? Guid.Empty;
                        if (bid != Guid.Empty)
                        {
                            ViewComboDealCollection vcds = _pp.GetViewComboDealAllByBid(bid);
                            Guid mainBid = bid;
                            if (vcds.Count > 0)
                            {
                                if (vcds.FirstOrDefault().MainBusinessHourGuid.Value != bid)
                                {
                                    bid = vcds.FirstOrDefault().MainBusinessHourGuid.Value;
                                }
                            }
                            ProposalMultiOptionSpecCollection optSpecs = _sp.ProposalMultiOptionSpecGetByBid(bid);
                            if (optSpecs.Count() > 0)
                            {
                                ProductItem proItem = _pp.ProductItemGet(optSpecs.FirstOrDefault().ItemGuid);
                                if (proItem.IsLoaded)
                                {
                                    ProductInfo product = _pp.ProductInfoGet(proItem.InfoGuid);
                                    if (product.IsLoaded)
                                    {
                                        //if (product.IsMulti == true)
                                        //{
                                            if (!string.IsNullOrEmpty(proItem.ProductCode))
                                            {
                                                res.ItemNo = proItem.ProductCode;
                                            }
                                        //}
                                    }

                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                }
            }

            InventoryType inventoryType = InventoryType.UnShip;
            MemoryStream outputStream = IspShipProductInventoryExport(resultData, inventoryType);

            return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("出貨清冊{0}.xls", DateTime.Today.ToString("yyyyMMdd"))) };
        }
        #endregion 超商出貨管理

        #region 線上/匯出清冊

        [VbsAuthorize]
        public ActionResult ShipProductInventory(Guid id, VbsDealType type,
            int? pageNumber, string sortCol, bool? sortDesc, bool? excel, VbsOrderShipState? viewOrderShipState, bool? hasOrderShip, ViewVendorProgressState? viewVendorReturnProgressState)
        {
            pageNumber = pageNumber ?? 1;
            sortDesc = sortDesc ?? false;
            sortCol = sortCol ?? "OrderId";
            excel = excel ?? false;
            ShipOrderListModel vdlModel = new ShipOrderListModel();
            List<VbsOrderListInfo> infos = new List<VbsOrderListInfo>();

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }

            vdlModel.DealSaleInfo = GetDealSaleInfo(id).First(x => x.DealId == id);

            //檔次出貨狀態為已完成 隱藏消費者個資
            bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
            infos = GetOrderShipInventoryList(vdlModel.DealSaleInfo.DealId, isHiddenPersonalInfo, false);

            //統計該檔次 已回報及未回報出貨資訊(檢查出貨日期是否為null) 之訂單總數(不包含退貨訂單)
            List<VbsOrderListInfo> infosWithoutReturn = infos.Where(x => x.OrderShipState.EqualsNone(VbsOrderShipState.Return)).ToList();
            vdlModel.NoOrderShipCount = infosWithoutReturn.Count(x => x.HasOrderShip.Equals(false));
            vdlModel.ReturnOrderProcessCount = infosWithoutReturn.Count(x => x.VendorReturnProgressState.Equals(ViewVendorProgressState.Process));

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            Session[VbsSession.ParentActionName.ToString()] = "ShipProductInventory";

            if (excel.Value)
            {
                vdlModel.OrderInfos = GetPagerVbsOrderShipInfos(infos, viewOrderShipState, int.MaxValue, 0, sortCol, sortDesc.Value, hasOrderShip, viewVendorReturnProgressState, false);
                MemoryStream outputStream = ShipProductInventoryExport(vdlModel.OrderInfos, InventoryType.AllShip);

                //使用非同步寫入該檔次清冊匯出紀錄
                List<Guid> orderGuids = infos.Select(x => x.OrderGuid).Distinct().ToList();
                SetOrderProductInventoryExportLog(orderGuids, VbsCurrent.AccountId, InventoryType.AllShip);

                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("{0}_{1}_出貨清冊.xls", vdlModel.DealSaleInfo.DealUniqueId, vdlModel.DealSaleInfo.DealName)) };
            }
            else
            {
                vdlModel.OrderInfos = GetPagerVbsOrderShipInfos(infos, viewOrderShipState, 20, pageNumber.Value - 1, sortCol, sortDesc.Value, hasOrderShip, viewVendorReturnProgressState, false);
            }

            return View("ShipProductInventory", vdlModel);
        }

        /// <summary>
        /// 單一檔次匯出出貨清冊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="type"></param>
        /// <param name="inventoryType"></param>
        /// <param name="newVersion"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult InventoryExport(Guid id, VbsDealType type, InventoryType inventoryType, bool newVersion = false)
        {
            var vdlModel = new ShipOrderListModel();
            var shipFacade = new VBSShipFacade();

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }

            var dealInfos = GetDealSaleInfo(id);
            vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);
            var dealIds = dealInfos.Select(x => x.DealUniqueId);
            var newFileTips = newVersion ? "_新版" : string.Empty;

            VbsOrderShipState? orderShipState = null;
            if (inventoryType == InventoryType.UnShip)
            {
                orderShipState = VbsOrderShipState.UnShip;
            }

            switch (inventoryType)
            {
                #region 品項統計表

                case InventoryType.OptionStatistic:
                    {
                        List<ViewOrderProductOptionList> orderProductOptionlists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds)
                                                                                        .Where(x => !x.IsReturned)
                                                                                        .ToList();
                        bool isMergeCount = true;
                        if (type == VbsDealType.Ppon)
                        {
                            isMergeCount = _pp.DealPropertyGet(id).IsMergeCount;
                        }

                        vdlModel.OrderProductStatistics = GetOrderProductStatistics(isMergeCount, orderProductOptionlists);
                        if (vdlModel.OrderProductStatistics == null)
                        {
                            return null;
                        }
                        MemoryStream outputStream = ProductOptionStatisticsExport(vdlModel.DealSaleInfo, vdlModel.OrderProductStatistics);

                        return new FileStreamResult(outputStream, "application/vnd.ms-excel")
                        {
                            FileDownloadName =
                                DownloadFileNameEncode(string.Format("{0}_{1}_品項統計.xls",
                                    vdlModel.DealSaleInfo.DealUniqueId, vdlModel.DealSaleInfo.DealName))
                        };
                    }

                #endregion 品項統計表

                #region 待出貨品項統計

                case InventoryType.UnShipOptionStatistic:
                    {
                        bool isMergeCount = true;
                        if (type == VbsDealType.Ppon)
                        {
                            isMergeCount = _pp.DealPropertyGet(id).IsMergeCount;
                        }
                        vdlModel.OrderProductStatistics =
                            shipFacade.GetMergeProductStatisticsNullShip(isMergeCount ? 1 : 0, dealIds)
                                .Select(item => new OrderProductStatistics
                                {
                                    ProductOption = item.Key,
                                    Count = item.Value
                                }).ToList();

                        MemoryStream outputStream = ProductOptionStatisticsExport(vdlModel.DealSaleInfo, vdlModel.OrderProductStatistics);

                        return new FileStreamResult(outputStream, "application/vnd.ms-excel")
                        {
                            FileDownloadName =
                                DownloadFileNameEncode(string.Format("{0}_{1}_待出貨品項統計.xls",
                                    vdlModel.DealSaleInfo.DealUniqueId, vdlModel.DealSaleInfo.DealName))
                        };
                    }

                #endregion 待出貨統計

                #region 出貨清冊 / 待出貨清冊

                case InventoryType.AllShip:
                case InventoryType.UnShip:
                    {
                        //檔次出貨狀態為已完成 隱藏消費者個資
                        bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
                        List<VbsOrderListInfo> infos = GetOrderShipInventoryList(vdlModel.DealSaleInfo.DealId, isHiddenPersonalInfo, newVersion);

                        PagerList<VbsOrderListInfo> orderInfos = GetPagerVbsOrderShipInfos(infos, orderShipState, int.MaxValue, 0, "OrderCreateTime", false, null, null, false);
                        vdlModel.OrderInfos = orderInfos;
                        MemoryStream outputStream;
                        if (newVersion)
                        {
                            outputStream = NewShipProductInventoryExport(vdlModel.OrderInfos, inventoryType);
                        }
                        else
                        {
                            outputStream = ShipProductInventoryExport(vdlModel.OrderInfos, inventoryType);
                        }

                        return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("{0}_{1}_出貨清冊{2}.xls", vdlModel.DealSaleInfo.DealUniqueId, vdlModel.DealSaleInfo.DealName, newFileTips)) };
                    }

                #endregion 出貨清冊 / 待出貨清冊

                #region 各別出貨清冊

                case InventoryType.ComboDeals:
                    {
                        var workStream = new MemoryStream();
                        var mainDeal = dealInfos.First(x => x.DealId == id);
                        string mainDealName = mainDeal.DealName ?? "ComboDeals";
                        bool isHiddenPersonalInfo = mainDeal.ShipState.Equals(VbsShipState.Finished);
                        var comboDealInfos = dealInfos.Where(x => x.DealId != id);
                        using (var zip = new ZipFile(System.Text.Encoding.Default))
                        {
                            foreach (var dealInfo in comboDealInfos)
                            {
                                //檔次出貨狀態為已完成 隱藏消費者個資
                                var infos = GetOrderShipInventoryList(dealInfo.DealId, isHiddenPersonalInfo, newVersion);
                                var orderInfos = GetPagerVbsOrderShipInfos(infos, orderShipState, int.MaxValue, 0, "OrderCreateTime", false, null, null, false);
                                MemoryStream outputStream;
                                if (newVersion)
                                {
                                    outputStream = NewShipProductInventoryExport(orderInfos, inventoryType);
                                }
                                else
                                {
                                    outputStream = ShipProductInventoryExport(orderInfos, inventoryType);
                                }
                                var bytes = new byte[outputStream.Length];
                                outputStream.Read(bytes, 0, bytes.Length);
                                zip.AddEntry(DownloadFileNameFormat(string.Format("{0}_{1}_出貨清冊{2}.xls", dealInfo.DealUniqueId, dealInfo.DealName, newFileTips)), bytes);
                            }

                            zip.Save(workStream);
                        }
                        workStream.Position = 0;

                        var fileResult = new FileStreamResult(workStream, System.Net.Mime.MediaTypeNames.Application.Zip)
                        {
                            FileDownloadName = DownloadFileNameEncode(string.Format("{0}.zip", mainDealName))
                        };

                        return fileResult;
                    }

                #endregion 各別出貨清冊

                default:
                    break;
            }

            return null;
        }

        /// <summary>
        /// 全檔次批次匯出待出貨清冊
        /// </summary>
        /// <param name="newVersion"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult BatchInventoryExport(bool newVersion = false)
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            #region 一次匯出出貨狀態為:處理中的所有未回填出貨單之訂單清冊

            var infos = Session[VbsSession.VbsShipDealInfos.ToString()] != null &&
                        (Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>).Any(x => x.ShipState == VbsShipState.Shipping || 
                                                                                                                   x.ShipState == VbsShipState.ShippingOverdue)
                        ? Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>
                        : GetAllowedShipDealList(VbsShipState.Processing);

            ProposalFacade.ProposalPerformanceLogSet("BatchInventoryExport" + newVersion.ToString(), string.Join("\n", infos.Select(x => x.DealId + " " + x.ShipState)), "sys");
            //只處理未回填出貨回覆日之檔次
            var inventoryType = InventoryType.BatchUnShip;
            infos = infos.Where(x => x.ShipState == VbsShipState.Shipping || x.ShipState == VbsShipState.ShippingOverdue);
            var dealOrderInfos = GetPagerVbsOrderShipInfos(GetOrderShipInventoryList(
                infos.Select(x => x.DealId), false, newVersion), VbsOrderShipState.UnShip, int.MaxValue, 0, "OrderCreateTime", false, null, null, false
                , containPchomeWmsOrder: false)
                                    .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);
            var orderCountLimit = 1000000;
            var now = DateTime.Now;
            var newFileTips = newVersion ? "_新版" : string.Empty;

            //超過excel行數限制則拆成兩個excel檔匯出並壓縮成壓縮檔
            if (dealOrderInfos.Count() > orderCountLimit)
            {
                var workStream = new MemoryStream();

                using (var zip = new ZipFile(System.Text.Encoding.Default))
                {
                    int idx = 0;
                    int index = 1;
                    while (idx <= dealOrderInfos.Count() - 1)
                    {
                        MemoryStream outputStream;

                        if (newVersion)
                        {
                            outputStream = NewShipProductInventoryExport(dealOrderInfos.Skip(idx).Take(orderCountLimit), inventoryType);
                        }
                        else
                        {
                            outputStream = ShipProductInventoryExport(dealOrderInfos.Skip(idx).Take(orderCountLimit), inventoryType);
                        }

                        var bytes = new byte[outputStream.Length];
                        outputStream.Read(bytes, 0, bytes.Length);
                        zip.AddEntry(DownloadFileNameFormat(string.Format("{0:yyyyMMddHHmmss}_一次撈單待出貨清冊{1}_Part{2}.xls", now, newFileTips, index)), bytes);
                        idx += orderCountLimit;
                        index++;
                    }

                    zip.Save(workStream);
                }
                workStream.Position = 0;

                var fileResult = new FileStreamResult(workStream, System.Net.Mime.MediaTypeNames.Application.Zip)
                {
                    FileDownloadName = DownloadFileNameEncode(string.Format("{0:yyyyMMddHHmmss}_一次撈單待出貨清冊{1}.zip", now, newFileTips))
                };

                return fileResult;
            }
            else
            {
                MemoryStream outputStream;

                if (newVersion)
                {
                    outputStream = NewShipProductInventoryExport(dealOrderInfos, inventoryType);
                }
                else
                {
                    outputStream = ShipProductInventoryExport(dealOrderInfos, inventoryType);
                }

                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("{0:yyyyMMddHHmmss}_一次撈單待出貨清冊{1}.xls", now, newFileTips)) };
            }

            #endregion 一次匯出出貨狀態為:處理中的所有未回填出貨單之訂單清冊
        }

        #endregion 線上清冊

        #region 批次出貨管理/小倉批次

        [VbsAuthorize]
        public ActionResult ShipBatchImport(Guid id, VbsDealType type)
        {
            ShipOrderListModel vdlModel = new ShipOrderListModel();
            List<VbsOrderListInfo> infos = new List<VbsOrderListInfo>();
            List<CashTrustLog> cashTrustLogs = new List<CashTrustLog>();

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }

            //檢查是否小倉檔次
            bool isConsignment = false;
            if (_conf.IsConsignment)
            {
                isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
            }

            var dealSaleInfos = GetDealSaleInfo(id);
            vdlModel.DealSaleInfo = dealSaleInfos.First(x => x.DealId == id);
            var dealGuids = dealSaleInfos.Select(x => x.DealId);
            //取得照會完成數量
            int[] orderUserMemoList = GetOrderUserMemoListCount(dealSaleInfos, type);
            int orderUserMemoListCount = orderUserMemoList[0];
            int orderUserMemoListCountAll = orderUserMemoList[1];

            //撈取該檔次訂單之cash_trust_log(抓取trust_id 以抓取cash_trust_status_log)
            //不包含退貨(全退)訂單
            if (type == VbsDealType.Ppon)
            {
                cashTrustLogs = _mp.CashTrustLogGetListByBid(dealGuids)
                            .Where(x => x.Status == (int)TrustStatus.Initial ||
                                        x.Status == (int)TrustStatus.Trusted ||
                                        x.Status == (int)TrustStatus.Verified)
                            .ToList();
            }
            else if (type == VbsDealType.PiinLife)
            {
                cashTrustLogs = _mp.CashTrustLogGetAllListByHiDealProductId(vdlModel.DealSaleInfo.DealUniqueId)
                                .Where(x => x.Status == (int)TrustStatus.Initial ||
                                            x.Status == (int)TrustStatus.Trusted ||
                                            x.Status == (int)TrustStatus.Verified)
                                .ToList();
            }

            infos = _op.ViewOrderShipListGetListByProductGuid(dealGuids)
                        .Where(o => cashTrustLogs.Select(x => x.OrderGuid).Contains(o.OrderGuid))
                        .Select(order => new VbsOrderListInfo
                        {
                            OrderGuid = order.OrderGuid,
                            OrderId = order.OrderId,
                            OrderShipId = order.OrderShipId,
                            HasOrderShip = order.ShipTime.HasValue,
                        }).ToList();

            //統計該檔次 已回報及未回報出貨資訊(檢查出貨日期是否為null) 之訂單總數(不包含退貨訂單)
            vdlModel.NoOrderShipCount = infos.Count(x => x.HasOrderShip.Equals(false));
            vdlModel.HasOrderShipCount = infos.Count - vdlModel.NoOrderShipCount;
            vdlModel.ImportResult = TempData["importResult"] as ImportResult;

            //尋找匯入失敗檔案
            string path = string.Format("{0}/{1}", VbsCurrent.AccountId, vdlModel.DealSaleInfo.DealUniqueId);
            vdlModel.FailFileInfos = GetImportFailFileInfo(path);

            vdlModel.OrderUserMemoListCount = orderUserMemoListCount;
            vdlModel.OrderUserMemoListCountAll = orderUserMemoListCountAll;
            vdlModel.DealId = id;
            ViewBag.DealType = type;
            ViewBag.IsConsignment = isConsignment;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            ViewBag.IsEnableVbsNewShipFile = _conf.IsEnableVbsNewShipFile;
            Session[VbsSession.ParentActionName.ToString()] = "ShipBatchImport";

            return View(vdlModel);
        }

        [VbsAuthorize]
        public ActionResult BatchInventoryImport()
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }                        

            var vdlModel = new BatchInventoryImportModel();
            vdlModel.ImportResult = TempData["importResult"] as ImportResult;
            //尋找匯入失敗檔案
            string path = string.Format("{0}/{1}", VbsCurrent.AccountId, "BatchInventoryImport");
            vdlModel.FailFileInfos = GetImportFailFileInfo(path);

            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            ViewBag.IsEnableVbsNewShipFile = _conf.IsEnableVbsNewShipFile;
            Session[VbsSession.ParentActionName.ToString()] = "BatchInventoryImport";

            return View(vdlModel);
        }

        /// <summary>
        /// 單一檔次批次匯入出貨
        /// </summary>
        /// <param name="vdlModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ImportProcess(ShipBatchImportModel vdlModel)
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                ImportResult importResult = new ImportResult(); ;
                importResult.OtherMessage = "模擬身分不允許執行此功能";
                TempData["importResult"] = importResult;
            }
            else
            {
                var dealInfos = GetDealSaleInfo(Guid.Parse(vdlModel.DealId));
                var importResult = ProcessImportOrderShip(dealInfos, vdlModel);

                TempData["importResult"] = importResult;
            }

            
            return RedirectToAction("ShipBatchImport", new { id = vdlModel.DealId, type = vdlModel.DealType });
        }

        /// <summary>
        /// 全檔次批次匯入出貨
        /// </summary>
        /// <param name="vdlModel"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult BatchImportProcess(ShipBatchImportModel vdlModel)
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                ImportResult importResult = new ImportResult(); ;
                importResult.OtherMessage = "模擬身分不允許執行此功能";
                TempData["importResult"] = importResult;
            }
            else
            {
                var infos = Session[VbsSession.VbsShipDealInfos.ToString()] != null &&
                    (Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>).Any(x => x.ShipState == VbsShipState.Shipping ||
                                                                                                                x.ShipState == VbsShipState.ShippingOverdue)
                    ? Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>
                    : GetAllowedShipDealList(VbsShipState.Processing);

                var dealInfos = GetDealSaleInfo(infos.Select(x => x.DealId));
                var importResult = ProcessImportOrderShip(dealInfos, vdlModel);

                TempData["importResult"] = importResult;
            }
            
            return RedirectToAction("BatchInventoryImport");
        }

        #endregion 批次出貨管理/小倉批次

        #region 退貨管理

        /// <summary>
        /// 新版退貨管理
        /// </summary>
        /// <param name="queryData"></param>
        /// <param name="dealId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult ReturnOrderProcessList(VbsShipOrderQueryData queryData, string dealId, string orderId)
        {
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            int defaultCount = 20;
            int pageSize = queryData.maxPage ?? defaultCount;
            queryData.pageIndex = queryData.pageIndex ?? 1;
            queryData.maxPage = pageSize;
            int pageIndex = queryData.pageIndex.Value;
            DateTime beginDate; 
            DateTime endDate;

            //用於信件直接連結
            if (!string.IsNullOrEmpty(dealId))
            {
                queryData.queryOptionValue = dealId;
                queryData.queryOption = VbsShipQueryOption.DealId;
            }
                
            if (!string.IsNullOrEmpty(orderId))
            {
                queryData.queryOptionValue = orderId;
                queryData.queryOption = VbsShipQueryOption.OrderId;
            }

            OrderShipListSharedModel model = new OrderShipListSharedModel();
            model.Query = queryData;

            var returnOrderList = new List<ViewVbsReturnOrderList>();
            var endReturnOrderList = new List<ViewVbsReturnOrderList>();
            int tmp;
            string filter = "";
            switch (queryData.queryOption)
            {
                case VbsShipQueryOption.DealId:
                    if (int.TryParse(queryData.queryOptionValue, out tmp))
                    {
                        filter = ViewVbsReturnOrderList.Columns.UniqueId;
                    }
                    break;
                case VbsShipQueryOption.OrderId:
                    filter = ViewVbsReturnOrderList.Columns.OrderId;
                    break;
                case VbsShipQueryOption.Name:
                    filter = ViewVbsReturnOrderList.Columns.MemberName;
                    break;
                case VbsShipQueryOption.Mobile:
                    filter = ViewVbsReturnOrderList.Columns.MobileNumber;
                    break;
                default:
                    queryData.queryOptionValue = "";
                    break;
            }

           
            //未處理&進行中

            DateTime.TryParse(queryData.queryBeginDate, out beginDate);
            DateTime.TryParse(queryData.queryEndDate, out endDate);

            returnOrderList = _op.ViewVbsReturnOrderListCollectionGet(beginDate, endDate,
                                                                      new List<int>() { (int)ProgressStatus.Processing, (int)ProgressStatus.Retrieving, (int)ProgressStatus.RetrieveToPC, (int)ProgressStatus.RetrieveToCustomer, (int)ProgressStatus.ConfirmedForVendor, (int)ProgressStatus.ConfirmedForUnArrival }, 
                                                                      new List<int>() { (int)VendorProgressStatus.Processing, (int)VendorProgressStatus.Retrieving, (int)VendorProgressStatus.Unreturnable, (int)VendorProgressStatus.ConfirmedForVendor, (int)VendorProgressStatus.ConfirmedForUnArrival }, 
                                                                      VbsCurrent.AccountId, filter, queryData.queryOptionValue)
                                                                      .OrderByDescending(x => x.CreateTime).ToList();

            model.UnProcessCount = returnOrderList.Where(x => x.VendorProgressStatus <= (int)VendorProgressStatus.Processing).Count();
            model.ProcessingCount = returnOrderList.Count - model.UnProcessCount;
            queryData.queryProcessStep = queryData.queryProcessStep ?? 1;
            bool isReadOnly = queryData.queryProcessStep == 3;
            model.IsReadOnly = isReadOnly;


            //已結案

            endReturnOrderList = _op.ViewVbsReturnOrderListCollectionGet(beginDate, endDate,
                                                                      new List<int>() { (int)ProgressStatus.Processing, (int)ProgressStatus.Completed, (int)ProgressStatus.Unreturnable, (int)ProgressStatus.Canceled, (int)ProgressStatus.AtmQueueing, (int)ProgressStatus.AtmQueueSucceeded, (int)ProgressStatus.CompletedWithCreditCardQueued, (int)ProgressStatus.AtmFailed },
                                                                      new List<int>() { (int)VendorProgressStatus.Processing, (int)VendorProgressStatus.Retrieving, (int)VendorProgressStatus.Unreturnable, (int)VendorProgressStatus.CompletedAndRetrievied, (int)VendorProgressStatus.CompletedAndNoRetrieving, (int)VendorProgressStatus.CompletedAndUnShip, (int)VendorProgressStatus.UnreturnableProcessed, (int)VendorProgressStatus.CompletedByCustomerService, (int)VendorProgressStatus.Automatic, (int)VendorProgressStatus.ConfirmedForVendor, (int)VendorProgressStatus.ConfirmedForCS, (int)VendorProgressStatus.ConfirmedForUnArrival },
                                                                      VbsCurrent.AccountId, filter, queryData.queryOptionValue)
                                                                      .OrderByDescending(x => x.CreateTime).ToList();


            endReturnOrderList = endReturnOrderList.Where(x => (x.VendorProgressStatus > (int)VendorProgressStatus.Unreturnable && x.VendorProgressStatus < (int)VendorProgressStatus.ConfirmedForVendor) || (x.ProgressStatus == (int)ProgressStatus.Canceled)).ToList();
            model.EndCount = endReturnOrderList.Count();

            if (isReadOnly)
            {
                returnOrderList = endReturnOrderList;
            }


            returnOrderList = returnOrderList.Where(x => queryData.queryProcessStep == 1 ? x.VendorProgressStatus <= (int)VendorProgressStatus.Processing :
            queryData.queryProcessStep == 2 ? x.VendorProgressStatus > (int)VendorProgressStatus.Processing : x.VendorProgressStatus > (int)VendorProgressStatus.Unreturnable || (x.ProgressStatus == (int)ProgressStatus.Canceled)).ToList();


            List<OrderProductOptionInfo> returnOptionList = _op.GetReturnOrderProductOptionInfo(returnOrderList.Select(x => x.ReturnFormId));
            List<OrderProductOptionInfo> returndOptionList = isReadOnly ? _op.GetReturnOrderProductOptionInfo(returnOrderList.Select(x => x.ReturnFormId), true) : new List<OrderProductOptionInfo>();
            Dictionary<int, string> returnItemLists = GetReturnOrderProductItemDescription(returnOptionList);

            if (returnOrderList.Count > 0)
            {
                int serNo = 1;

                foreach (var item in returnOrderList.Skip((pageIndex - 1) * pageSize).Take(pageSize))
                {
                    OrderListSharedInfos info = new OrderListSharedInfos();

                    #region 隱藏個資
                    bool unRecoverable = item.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                                     item.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued) ||
                                                     item.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                                     item.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                                     item.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                                     item.ProgressStatus.Equals((int)ProgressStatus.Canceled);

                    var vdlModel = new ShipOrderListModel();
                    var dealInfos = GetDealSaleInfo((Guid)item.MainBid);
                    var dealGuids = dealInfos.Select(x => x.DealId);
                    vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == (Guid)item.MainBid);
                    //檔次出貨狀態為完成 隱藏消費者個資
                    bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);

                    //退貨完成 或 ATM退款訂單(廠商無法進行退貨復原) 或 檔次出貨狀態為完成 隱藏消費者個資
                    if (unRecoverable || isHiddenPersonalInfo)
                    {
                        item.MemberName = "***";
                        item.ReceiverName = "***";
                        item.MobileNumber = "******";
                        item.ReceiverAddress = "******";
                        item.DeliveryAddress = "******";
                    }
                    #endregion

                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.Bid);

                    info.SerNo = (((pageIndex - 1) * pageSize) + serNo).ToString();
                    info.OrderReturnId = item.ReturnFormId;
                    info.OrderId = (vpd.IsWms ? "[24H到貨]<br/>" : string.Empty) + item.OrderId;
                    info.ProgressStatus = item.ProgressStatus;
                    info.VendorProgressStatus = item.VendorProgressStatus ?? 0;
                    info.ProcessMemo = item.VendorMemo;
                    info.ComboPackCount = item.ComboPackCount;
                    info.CreateTime = item.CreateTime.ToString("yyyy/MM/dd HH:mm");
                    info.Reason = item.ReturnReason;
                    info.RecipientName = string.IsNullOrEmpty(item.ReceiverName) ? item.MemberName : item.ReceiverName;
                    info.RecipientAddress = string.IsNullOrEmpty(item.ReceiverAddress) ? item.DeliveryAddress : item.ReceiverAddress;
                    info.RecipientMobile = item.MobileNumber;
                    info.DealId = item.UniqueId;
                    info.DealName = item.ItemName;
                    info.ReturnItemInfo = isReadOnly ? returndOptionList.Where(x => x.ReturnFormId.Equals(item.ReturnFormId)).ToList() : returnOptionList.Where(x => x.ReturnFormId.Equals(item.ReturnFormId)).ToList();
                    info.ReturnItemDescription = returnItemLists.ContainsKey(item.ReturnFormId) ? returnItemLists[item.ReturnFormId]
                                                : string.Empty;
                    info.VendorProcessLog = GetReturnStatusLog(item.ReturnFormId);
                    info.IsShip = item.ShipTime != null ? true : false;
                    info.IsWms = vpd.IsWms;
                    info.IsWmsRefund = _wp.WmsRefundOrderGetByReturnFormId(item.ReturnFormId).IsLoaded;
                    model.Infos.Add(info);

                    serNo++;
                }
                int totalCount = returnOrderList.Count;
                model.TotalCount = totalCount;
                model.PageCount = totalCount % pageSize > 0 ? (totalCount / pageSize) + 1 : totalCount / pageSize;

                var shipList = _op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();

                ViewBag.ShipCompanyInfos = shipList;
                ViewBag.WmsRefundEnable = _conf.WmsRefundEnable && (string.IsNullOrEmpty(_conf.WmsRefundVbsAccount) || _conf.WmsRefundVbsAccount.Contains(VbsCurrent.AccountId));
            }

            return View(model);
        }

        /// <summary>
        /// 退貨儲存
        /// </summary>
        /// <param name="returnProducts"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ReturnProcessShipped(IEnumerable<VendorReturnOrderProcess> returnProducts)
        {
            bool result = false;
            string message = string.Empty;
            bool? isReturnFinish = null;
            int? returnFormId = null;
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;


            foreach (var returnProduct in returnProducts)
            {
                List<int> orderProductIds = new List<int>();
                int unReturnCount = 0;
                if (returnProduct.ReturnProdcutId != null)
                {
                    for (var i = 0; i < returnProduct.ReturnProdcutId.Count(); i++)
                    {
                        int count = int.Parse(returnProduct.ReturnQuantity.ElementAt(i));
                        IEnumerable<string> prods = returnProduct.ReturnProdcutId.ElementAt(i).Split(',');

                        prods
                            .Take(count)
                            .ForEach(id => orderProductIds.Add(int.Parse(id)));

                        unReturnCount += prods.Count() - count;
                    }
                }

                //不退貨數須以個數或組數(成套販售)計算            
                unReturnCount = unReturnCount / returnProduct.ComboPackCount;

                //處理備註若和輸入提示內容相同 則改為null
                if (returnProduct.ProcessMemo.Trim().Equals(returnProduct.ProcessMemoDefault))
                {
                    returnProduct.ProcessMemo = null;
                }

                ReturnFormEntity rfEntity = ReturnFormRepository.FindById(returnProduct.ReturnFormId);
                result = rfEntity.VendorReturnProcessShipped((VendorProgressStatus)returnProduct.ProgressStatus, orderProductIds, unReturnCount, returnProduct.ProcessMemo, modifyId);


                if (result)
                {
                    ReturnFormRepository.Save(rfEntity);
                }
                else
                {
                    message = "儲存失敗，訂單狀態已更新；\n請重整畫面後重試";
                    isReturnFinish = ValidateVendorProgressState(rfEntity.ProgressStatus, rfEntity.VendorProgressStatus);
                }
                returnFormId = returnProduct.ReturnFormId;
            }

            return Json(new { IsSuccess = result, Message = message, ReturnFormId = returnFormId, IsReturnFinish = isReturnFinish });
        }


        #region 舊版已不再使用,留下參考用
        //        /// <summary>
        //        /// 舊版退貨管理
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="type"></param>
        //        /// <param name="pageNumber"></param>
        //        /// <param name="sortCol"></param>
        //        /// <param name="sortDesc"></param>
        //        /// <param name="viewVendorProgressState"></param>
        //        /// <param name="selQueryOption"></param>
        //        /// <param name="queryKeyWord"></param>
        //        /// <returns></returns>
        //        [VbsAuthorize]
        //        public ActionResult ReturnOrderList(Guid id, VbsDealType type,
        //            int? pageNumber, string sortCol, bool? sortDesc, ViewVendorProgressState? viewVendorProgressState, string selQueryOption, string queryKeyWord)
        //        {
        //            //檢查商家帳號是否有檢視該檔次分店權限
        //            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
        //            {
        //                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
        //            }
        //            //檢查是否小倉檔次
        //            bool isConsignment = false;
        //            if (_conf.IsConsignment)
        //            {
        //                isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
        //            }

        //            pageNumber = pageNumber ?? 1;
        //            sortDesc = sortDesc ?? false;
        //            sortCol = sortCol ?? "ReturnApplicationTime";
        //            queryKeyWord = string.IsNullOrEmpty(queryKeyWord) ? queryKeyWord : queryKeyWord.Trim();
        //            viewVendorProgressState = viewVendorProgressState ?? ViewVendorProgressState.Process;

        //            var vdlModel = new ShipOrderListModel();
        //            var infos = new List<VbsReturnOrderListInfo>();
        //            var dealInfos = GetDealSaleInfo(id);
        //            var dealGuids = dealInfos.Select(x => x.DealId);
        //            vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);
        //            //檔次出貨狀態為完成 隱藏消費者個資
        //            bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
        //            //取得照會完成數量
        //            int[] orderUserMemoList = GetOrderUserMemoListCount(dealInfos, type);
        //            int orderUserMemoListCount = orderUserMemoList[0];
        //            int orderUserMemoListCountAll = orderUserMemoList[1];

        //            //取得退款狀態為處理中及退貨完成之退貨訂單
        //            List<ViewOrderReturnFormList> returnList = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
        //                                                                .Where(x => (x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued) ||
        //                                                                            x.ProgressStatus.Equals((int)ProgressStatus.Canceled)) &&
        //((x.ProductDeliveryType ?? (int)ProductDeliveryType.Normal) == (int)ProductDeliveryType.Normal || ((x.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup || x.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup) && (x.IsReceipt ?? false))))
        //                                                                .ToList();
        //            if (returnList.Count > 0)
        //            {
        //                //取得return_form_id
        //                List<int> returnFormIds = returnList.Select(x => x.ReturnFormId).Distinct().ToList();
        //                List<Guid> orderGuids = returnList.Select(x => x.OrderGuid).Distinct().ToList();
        //                //取得商品品項資訊
        //                Dictionary<int, string> orderItemLists = _op.GetReturnOrderProductOptionDesc(returnFormIds);
        //                Dictionary<Guid, List<OrderProduct>> orderProductLists = _op.OrderProductGetList(orderGuids)
        //                                                                            .GroupBy(x => x.OrderGuid)
        //                                                                            .ToDictionary(x => x.Key, x => x.ToList());
        //                foreach (ViewOrderReturnFormList order in returnList)
        //                {
        //                    bool unRecoverable = order.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
        //                                         order.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued) ||
        //                                         order.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
        //                                         order.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
        //                                         order.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
        //                                         order.ProgressStatus.Equals((int)ProgressStatus.Canceled);

        //                    //排除配送開始日前全部退貨之訂單
        //                    if (order.ModifyTime.HasValue &&
        //                        DateTime.Compare(order.ModifyTime.Value.Date, vdlModel.DealSaleInfo.ShipPeriodStartTime.Date) < 0 &&
        //                        (order.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
        //                         order.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued)))
        //                    {
        //                        if (orderProductLists.ContainsKey(order.OrderGuid))
        //                        {
        //                            if (!orderProductLists[order.OrderGuid].Any(x => x.IsCurrent && !x.IsReturned))
        //                            {
        //                                continue;
        //                            }
        //                        }
        //                    }

        //                    //退貨完成 或 ATM退款訂單(廠商無法進行退貨復原) 或 檔次出貨狀態為完成 隱藏消費者個資
        //                    if (unRecoverable || isHiddenPersonalInfo)
        //                    {
        //                        order.MemberName = "***";
        //                        order.PhoneNumber = "******";
        //                    }

        //                    var info = new VbsReturnOrderListInfo
        //                    {
        //                        OrderId = order.OrderId,
        //                        OrderGuid = order.OrderGuid,
        //                        ReturnApplicationTime = order.ReturnApplicationTime,
        //                        ReturnItemDescription = orderItemLists.ContainsKey(order.ReturnFormId)
        //                                                 ? orderItemLists[order.ReturnFormId]
        //                                                 : string.Empty,
        //                        RecipientName = string.IsNullOrEmpty(order.ReceiverName) ? order.MemberName : order.ReceiverName,
        //                        RecipientTel = order.PhoneNumber,
        //                        OrderShipId = order.OrderShipId,
        //                        ShipTime = order.ShipTime,
        //                        ReturnFormId = order.ReturnFormId,
        //                        ReturnReason = order.ReturnReason,
        //                        VendorProgressStatus = order.VendorProgressStatus.HasValue
        //                                                ? (VendorProgressStatus)order.VendorProgressStatus.Value
        //                                                : VendorProgressStatus.Processing,
        //                        ProgressStatus = (ProgressStatus)order.ProgressStatus
        //                    };

        //                    infos.Add(info);
        //                }
        //            }

        //            vdlModel.ReturnOrderPagerInfos = GetPagerVbsReturnOrderInfos(infos, viewVendorProgressState, selQueryOption, queryKeyWord, 20, pageNumber.Value - 1, sortCol, sortDesc.Value);
        //            vdlModel.IsResultEmpty = !infos.Any();
        //            vdlModel.OrderUserMemoListCount = orderUserMemoListCount;
        //            vdlModel.OrderUserMemoListCountAll = orderUserMemoListCountAll;
        //            vdlModel.DealId = id;
        //            ViewBag.DealType = type;
        //            ViewBag.IsConsignment = isConsignment;
        //            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
        //            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
        //            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
        //            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
        //            Session[VbsSession.ParentActionName.ToString()] = "ReturnOrderList";

        //            return View("ReturnOrderList", vdlModel);
        //        }

        //        /// <summary>
        //        /// 舊版退貨管理內頁
        //        /// </summary>
        //        /// <param name="id"></param>
        //        /// <param name="type"></param>
        //        /// <param name="orderId"></param>
        //        /// <returns></returns>
        //        [VbsAuthorize]
        //        public ActionResult ReturnOrderProcess(Guid id, VbsDealType type, string orderId)
        //        {
        //            //檢查商家帳號是否有檢視該檔次分店權限
        //            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
        //            {
        //                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
        //            }
        //            //檢查是否小倉檔次
        //            bool isConsignment = false;
        //            if (_conf.IsConsignment)
        //            {
        //                isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
        //            }
        //            ShipOrderListModel vdlModel = new ShipOrderListModel();
        //            List<VbsReturnOrderListInfo> infos = new List<VbsReturnOrderListInfo>();
        //            List<string> customerServiceList = new List<string>();

        //            vdlModel.DealSaleInfo = GetDealSaleInfo(id).First(x => x.DealId == id);
        //            //檔次出貨狀態為完成 隱藏消費者個資
        //            bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
        //            var dealInfos = GetDealSaleInfo(id);
        //            var dealIds = dealInfos.Select(x => x.DealId).ToList();

        //            vdlModel.IsComboDeal = dealIds.Count > 1;

        //            //取得退貨訂單
        //            var returnList = _op.ViewOrderReturnFormListGetListByDealGuid(dealIds)
        //                            .Where(x =>
        //                            {
        //                                //只需處理退款狀態為處理中的退貨單
        //                                if (!x.ProgressStatus.Equals((int)ProgressStatus.Processing))
        //                                {
        //                                    return false;
        //                                }
        //                                //只需處理廠商退貨進度為處理中之退貨單
        //                                if (x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndNoRetrieving ||
        //                                    x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied ||
        //                                    x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip ||
        //                                    x.VendorProgressStatus == (int)VendorProgressStatus.CompletedByCustomerService ||
        //                                    x.VendorProgressStatus == (int)VendorProgressStatus.Automatic)
        //                                {
        //                                    return false;
        //                                }
        //                                //只需處理已出貨的退貨單
        //                                if (!x.ShipTime.HasValue || x.ShipTime.Value.Date > DateTime.Now.Date)
        //                                {
        //                                    return false;
        //                                }

        //                                return true;
        //                            })
        //                            .ToList();
        //            if (!string.IsNullOrEmpty(orderId))
        //            {
        //                returnList = returnList.Where(x => x.OrderId.Equals(orderId))
        //                                       .ToList();
        //            }
        //            if (returnList.Count > 0)
        //            {
        //                //取得訂購商品品項資訊
        //                PponOrder odr = new PponOrder(returnList.First().OrderGuid);
        //                IEnumerable<SummarizedProductSpec> source = odr.GetToHouseProductSummary();
        //                string orderItemDesc = string.Join("\n", source.Select(x => x.Spec + " " + x.TotalCount));
        //                //取得return_form_id
        //                List<int> returnFormIds = returnList.Select(x => x.ReturnFormId).Distinct().ToList();
        //                //取得退貨商品品項資訊
        //                List<OrderProductOptionInfo> returnOptionList = _op.GetReturnOrderProductOptionInfo(returnFormIds);
        //                Dictionary<int, string> returnItemLists = GetReturnOrderProductItemDescription(returnOptionList);
        //                foreach (ViewOrderReturnFormList order in returnList)
        //                {
        //                    var dealInfo = dealInfos.FirstOrDefault(x => x.DealId == order.ProductGuid);
        //                    if (dealInfo == null)
        //                    {
        //                        continue;
        //                    }

        //                    if (isHiddenPersonalInfo)
        //                    {
        //                        order.MemberName = "***";
        //                        order.PhoneNumber = "******";
        //                        order.DeliveryAddress = "*********";
        //                    }

        //                    VbsReturnOrderListInfo info = new VbsReturnOrderListInfo
        //                    {
        //                        DealInfo = dealInfo,
        //                        OrderId = order.OrderId,
        //                        OrderGuid = order.OrderGuid,
        //                        OrderItemDescription = orderItemDesc,
        //                        RecipientName = string.IsNullOrEmpty(order.ReceiverName) ? order.MemberName : order.ReceiverName,
        //                        RecipientTel = order.PhoneNumber,
        //                        RecipientAddress = string.IsNullOrEmpty(order.ReceiverAddress) ? order.DeliveryAddress : order.ReceiverAddress,
        //                        ReturnApplicationTime = order.ReturnApplicationTime,
        //                        ReturnItemDescription = returnItemLists.ContainsKey(order.ReturnFormId)
        //                                                ? returnItemLists[order.ReturnFormId]
        //                                                : string.Empty,
        //                        ReturnReason = order.ReturnReason,
        //                        VendorProgressStatus = order.VendorProgressStatus.HasValue
        //                                                ? (VendorProgressStatus)order.VendorProgressStatus.Value
        //                                                : VendorProgressStatus.Processing,
        //                        VendorMemo = order.VendorMemo,
        //                        ReturnFormId = order.ReturnFormId,
        //                        ReturnItemInfo = returnOptionList.Where(x => x.ReturnFormId.Equals(order.ReturnFormId)).ToList()
        //                    };

        //                    infos.Add(info);
        //                }
        //                #region 客服案件
        //                ViewCustomerServiceListCollection viewCSList = _service.GetViewCustomerServiceMessageByOrderGuid(returnList.FirstOrDefault().OrderGuid);
        //                customerServiceList.AddRange(
        //                    viewCSList
        //                    .Where(x => x.CustomerServiceStatus >= (int)Core.Enumeration.statusConvert.transfer)
        //                    .Select(y => y.ServiceNo).ToList()
        //                    );
        //                #endregion 客服案件
        //            }


        //            ViewBag.CustomerServiceList = customerServiceList;
        //            vdlModel.ReturnOrderInfos = infos.OrderByDescending(x => x.ReturnApplicationTime)
        //                                             .ToList();
        //            ViewBag.IsConsignment = isConsignment;
        //            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
        //            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
        //            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
        //            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
        //            Session[VbsSession.ParentActionName.ToString()] = "ReturnOrderProcess";
        //            return View("ReturnOrderProcess", vdlModel);
        //        }

        //        /// <summary>
        //        /// 未出貨直接退貨
        //        /// </summary>
        //        /// <param name="returnFormId"></param>
        //        /// <returns></returns>
        //        [HttpPost]
        //        [VbsAuthorize]
        //        public ActionResult ReturnProcessUnShip(int returnFormId)
        //        {
        //            bool result = false;
        //            string message = string.Empty;
        //            string shipTime = string.Empty;

        //            ReturnFormEntity rfEntity = ReturnFormRepository.FindById(returnFormId);
        //            result = rfEntity.VendorReturnProcessUnShip(VbsCurrent.AccountId);
        //            bool? isReturnFinish = null;

        //            if (result)
        //            {
        //                ReturnFormRepository.Save(rfEntity);
        //                message = "儲存成功";

        //                ViewOrderReturnFormList orderReturnForm = _op.ViewOrderReturnFormListGet(returnFormId);
        //                if (orderReturnForm != null && orderReturnForm.IsLoaded)
        //                {
        //                    shipTime = orderReturnForm.ShipTime.HasValue ? orderReturnForm.ShipTime.Value.ToString("yyyy/MM/dd") : "未出貨";
        //                }
        //            }
        //            else
        //            {
        //                message = "儲存失敗，訂單狀態已更新；\n請重整畫面後重試";
        //                isReturnFinish = ValidateVendorProgressState(rfEntity.ProgressStatus,
        //                                                             rfEntity.VendorProgressStatus);
        //            }

        //            return Json(new { IsSuccess = result, Message = message, ReturnFormId = returnFormId, ShipTime = shipTime, IsReturnFinish = isReturnFinish });
        //        }

        //        /// <summary>
        //        /// 復原退貨
        //        /// </summary>
        //        /// <param name="returnFormId"></param>
        //        /// <returns></returns>
        //        [HttpPost]
        //        [VbsAuthorize]
        //        public ActionResult ReturnProcessRecover(int returnFormId)
        //        {
        //            bool result = false;
        //            string message = string.Empty;

        //            ReturnFormEntity rfEntity = ReturnFormRepository.FindById(returnFormId);
        //            result = rfEntity.VendorReturnProcessRecover(VbsCurrent.AccountId);
        //            bool? isReturnFinish = null;

        //            if (result)
        //            {
        //                ReturnFormRepository.Save(rfEntity);
        //                message = "復原成功";
        //            }
        //            else
        //            {
        //                message = "復原失敗，訂單狀態已更新；\n請重整畫面後重試";
        //                isReturnFinish = ValidateVendorProgressState(rfEntity.ProgressStatus,
        //                                                             rfEntity.VendorProgressStatus);
        //            }

        //            return Json(new { IsSuccess = result, Message = message, ReturnFormId = returnFormId, IsReturnFinish = isReturnFinish });
        //        } 
        #endregion


        #endregion 退貨管理

        #region 照會

        [VbsAuthorize]
        public ActionResult ShipRefer(Guid id, VbsDealType type,
            int? pageNumber, string sortCol, bool? sortDesc)
        {
            sortDesc = false;
            sortCol = "OrderId";
            pageNumber = pageNumber ?? 1;
            var vdlModel = new ShipOrderListModel();
            var infos = new List<VbsOrderListInfo>();
            var returnInfos = new List<VbsReturnOrderListInfo>();

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }
            //檢查是否小倉檔次
            bool isConsignment = false;
            if (_conf.IsConsignment)
            {
                isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
            }
            var dealInfos = GetDealSaleInfo(id);
            var dealGuids = dealInfos.Select(x => x.DealId);
            vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);

            //檔次出貨狀態為已完成 隱藏消費者個資
            bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);

            infos = GetOrderShipList(dealInfos, isHiddenPersonalInfo, string.Empty, "dummy");

            infos = infos.Where(x => x.OrderUserMemoInfos.Any(i => i.UserMemo.Contains("**照會成功，請正常出貨**"))).ToList();

            vdlModel.OrderInfos = GetPagerVbsOrderShipInfos(infos, null, 10, pageNumber.Value - 1, sortCol, sortDesc.Value, null, null, false);

            //取得照會完成數量
            vdlModel.OrderUserMemoListCount = infos.Count(x => !x.HasOrderShip);
            vdlModel.OrderUserMemoListCountAll = infos.Count();
            vdlModel.DealId = id;
            ViewBag.DealType = type;
            ViewBag.IsConsignment = isConsignment;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            Session[VbsSession.ParentActionName.ToString()] = "ShipRefer";

            return View("ShipRefer", vdlModel);
        }

        #endregion 照會

        #region 備料統計

        [VbsAuthorize]
        public ActionResult ShipSalesStatistics(Guid id, VbsDealType type)
        {
            var vdlModel = new ShipOrderListModel();
            int[] orderUserMemoList = null;
            int orderUserMemoListCount = 0;
            int orderUserMemoListCountAll = 0;

            //檢查商家帳號是否有檢視該檔次分店權限
            if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
            {
                return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
            }

            var dealProperty = _pp.DealPropertyGet(id);
            //檢查是否小倉檔次
            bool isConsignment = false;
            if (_conf.IsConsignment) 
            {
                isConsignment = (bool)dealProperty.Consignment;
            }

            List<DealSaleInfo> dealInfos = GetDealSaleInfo(id).ToList();
            vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);
            bool isMergeCount = true;
            var dealIds = dealInfos.Select(x => x.DealUniqueId);

            var dealSalesSummarys = new Dictionary<Guid, VendorDealSalesCount>();
            var dealSalesStatistics = new List<DealSalesStatistics>();
            var now = DateTime.Now;
            vdlModel.IsComboDeal = dealInfos.Count() > 1;

            dealSalesStatistics = GetDealSalesStatistics(id, dealInfos, dealSalesSummarys, type, vdlModel.IsComboDeal,
                vdlModel.DealSaleInfo.DealUniqueId, 
                ref  isMergeCount, 
                ref orderUserMemoList, 
                ref orderUserMemoListCount, 
                ref orderUserMemoListCountAll
                );


            vdlModel.DealSalesStatistics = dealSalesStatistics.OrderBy(x => x.DealUniqueId);

            List<ViewOrderProductOptionList> orderProductOptionlists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds)
                                                                            .Where(x => !x.IsReturned)
                                                                            .ToList();
            vdlModel.OrderProductStatistics = GetOrderProductStatistics(isMergeCount, orderProductOptionlists);
            vdlModel.OrderUserMemoListCount = orderUserMemoListCount;
            vdlModel.OrderUserMemoListCountAll = orderUserMemoListCountAll;
            vdlModel.DealId = id;
            ViewBag.DealType = type;
            ViewBag.IsConsignment = isConsignment;
            ViewBag.UserName = Session[VbsSession.UserName.ToString()];
            ViewBag.UserId = Session[VbsSession.UserId.ToString()];
            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
            ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
            ViewBag.IsEnableVbsNewShipFile = _conf.IsEnableVbsNewShipFile;
            ViewBag.IsWms = dealProperty.IsWms;
            Session[VbsSession.ParentActionName.ToString()] = "ShipSalesStatistics";
            return View("ShipSalesStatistics", vdlModel);
        }

        private List<DealSalesStatistics> GetDealSalesStatistics(
            Guid id,
            List<DealSaleInfo> dealInfos,
            Dictionary<Guid, VendorDealSalesCount> dealSalesSummarys,
            VbsDealType type,
            bool isComboDeal,
            int dealUniqueId,
            ref bool isMergeCount,
            ref int[] orderUserMemoList, 
            ref int orderUserMemoListCount,
            ref int orderUserMemoListCountAll
            )
        {
            List<DealSalesStatistics> dealSalesStatistics = new List<DealSalesStatistics>();
            
            var dealGuids = dealInfos.Select(x => x.DealId);

            if (type.Equals(VbsDealType.Ppon))
            {
                dealSalesSummarys = _vp.GetPponToHouseDealSummary(dealGuids)
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);
                isMergeCount = _pp.DealPropertyGet(id).IsMergeCount;
            }
            else if (type.Equals(VbsDealType.PiinLife))
            {
                dealSalesSummarys = _vp.GetHidealToHouseDealSummary(new List<int> { dealUniqueId })
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);
            }

            var returnOrderLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids);
            //撈取退款中或退貨完成之order_guid 其中購物金轉退現金失敗 須視為退貨完成
            var returningOrders = returnOrderLists.Where(x => x.ProgressStatus != (int)ProgressStatus.Canceled &&
                                                              (x.ProgressStatus != (int)ProgressStatus.Unreturnable ||
                                                               (x.RefundType == (int)RefundType.ScashToAtm ||
                                                                x.RefundType == (int)RefundType.ScashToCash ||
                                                                x.RefundType == (int)RefundType.ScashToTcash)))
                                                  .Select(x => x.OrderGuid);
            var unShipOrderCounts = _op.ViewOrderShipListGetListByProductGuid(dealGuids)
                                            .Where(x => !returningOrders.Contains(x.OrderGuid) && !Helper.IsFlagSet(x.OrderStatus, OrderStatus.Cancel))
                                            .GroupBy(x => x.ProductGuid)
                                            .ToDictionary(x => x.Key,
                                                          x => x.Count(s => !s.ShipTime.HasValue ||
                                                                            DateTime.Compare(s.ShipTime.Value, DateTime.Now) > 0));
            var returningOrderCounts = returnOrderLists
                                        .Where(x => (x.ProductDeliveryType ?? (int)ProductDeliveryType.Normal) == (int)ProductDeliveryType.Normal || ((x.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup || x.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup) && (x.IsReceipt ?? false)))
                                        .GroupBy(x => x.ProductGuid)
                                        .ToDictionary(x => x.Key,
                                                      x => x.Count(r => r.ProgressStatus == (int)ProgressStatus.Processing &&
                                                                       (r.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                        r.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                        r.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                        r.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed)));

            //依廠商的觀點，認定已退貨狀態
            var verderCompletedStatus = new int[] {
                    (int)VendorProgressStatus.CompletedAndRetrievied,
                    (int)VendorProgressStatus.CompletedAndNoRetrieving,
                    (int)VendorProgressStatus.CompletedAndUnShip,
                    (int)VendorProgressStatus.CompletedByCustomerService,
                    (int)VendorProgressStatus.Automatic};

            var vendorReturnFinishId = returnOrderLists.GroupBy(x => x.ProductGuid)
                .ToDictionary(x => x.Key,
                              x => x.Where(r => verderCompletedStatus.Contains(r.VendorProgressStatus.GetValueOrDefault()) &&
                                                r.ProgressStatus != (int)ProgressStatus.Canceled)
                                    .Select(jj => jj.ReturnFormId).ToList());

            var returnedInfo = _op.ReturnFormRefundGetList(vendorReturnFinishId.SelectMany(x => x.Value))
                                    .GroupBy(x => x.ReturnFormId)
                                    .ToDictionary(x => x.Key,
                                                  x => x.Count(r => !r.IsFreight && !r.VendorNoRefund));

            var vendorReturned = vendorReturnFinishId
                                    .ToDictionary(item => item.Key,
                                                  item => returnedInfo.Where(x => item.Value.Contains(x.Key))
                                                                      .Sum(x => x.Value));
            //取得照會完成數量
            orderUserMemoList = GetOrderUserMemoListCount(dealInfos, type);
            orderUserMemoListCount = orderUserMemoList[0];
            orderUserMemoListCountAll = orderUserMemoList[1];

            foreach (var deal in dealInfos)
            {
                if (isComboDeal && deal.DealId == id)
                {
                    continue;
                }
                var saleSummary = dealSalesSummarys.ContainsKey(deal.DealId) ? dealSalesSummarys[deal.DealId] : null;

                var salesCount = saleSummary == null
                                    ? 0
                                    : saleSummary.VerifiedCount + saleSummary.UnverifiedCount + saleSummary.ReturnedCount;
                //var dealEndSalesCount = deal.DealOrderEndTime > now
                //                            ? "未結檔"
                //                            : deal.DealEndSalesCount.HasValue
                //                                ? string.Format("{0:#,0}", deal.DealEndSalesCount.Value)
                //                                : saleSummary == null
                //                                    ? "0"
                //                                    : string.Format("{0:#,0}", salesCount - saleSummary.BeforeDealEndReturnedCount);
                var unShipOrderCount = unShipOrderCounts.ContainsKey(deal.DealId)
                                            ? unShipOrderCounts[deal.DealId]
                                            : 0;
                var returningOrderCount = returningOrderCounts.ContainsKey(deal.DealId)
                                            ? returningOrderCounts[deal.DealId]
                                            : 0;

                var returnedCount = vendorReturned.ContainsKey(deal.DealId) ? vendorReturned[deal.DealId] : 0;

                var salesStatistic = new DealSalesStatistics
                {
                    DealUniqueId = deal.DealUniqueId,
                    DealName = deal.DealName,
                    UnShipOrderCount = unShipOrderCount,
                    ReturningOrderCount = returningOrderCount,
                    SalesCount = salesCount,
                    ReturnedCount = returnedCount,
                };

                dealSalesStatistics.Add(salesStatistic);
            }
            
            return dealSalesStatistics;
        }

        #endregion 備料統計

        #region 換貨管理

        /// <summary>
        /// 新版換貨管理
        /// </summary>
        /// <param name="queryData"></param>
        /// <param name="dealId"></param>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult ExchangeOrderProcessList(VbsShipOrderQueryData queryData, string dealId, string orderId)
        {
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            int defaultCount = 20;
            int pageSize = queryData.maxPage ?? defaultCount;
            queryData.pageIndex = queryData.pageIndex ?? 1;
            queryData.maxPage = pageSize;
            int pageIndex = queryData.pageIndex.Value;

            DateTime beginDate;
            DateTime endDate;

            //用於信件直接連結
            if (!string.IsNullOrEmpty(dealId))
            {
                queryData.queryOptionValue = dealId;
                queryData.queryOption = VbsShipQueryOption.DealId;
            }

            if (!string.IsNullOrEmpty(orderId))
            {
                queryData.queryOptionValue = orderId;
                queryData.queryOption = VbsShipQueryOption.OrderId;
            }


            OrderShipListSharedModel model = new OrderShipListSharedModel();
            model.Query = queryData;

            var exchangeOrderList = new List<ViewVbsExchangeOrderList>();
            var endExchangeOrderList = new List<ViewVbsExchangeOrderList>();
            int tmp;
            string filter = "";
            switch (queryData.queryOption)
            {
                case VbsShipQueryOption.DealId:
                    if (int.TryParse(queryData.queryOptionValue, out tmp))
                    {
                        filter = ViewVbsExchangeOrderList.Columns.UniqueId;
                    }
                    break;
                case VbsShipQueryOption.OrderId:
                    filter = ViewVbsExchangeOrderList.Columns.OrderId;
                    break;
                case VbsShipQueryOption.Name:
                    filter = ViewVbsExchangeOrderList.Columns.MemberName;
                    break;
                case VbsShipQueryOption.Mobile:
                    filter = ViewVbsExchangeOrderList.Columns.MobileNumber;
                    break;
                default:
                    queryData.queryOptionValue = "";
                    break;
            }

            string queryExchagneStatus = "";
            string queryStatus = "";

            queryExchagneStatus = " <= " + (int)ExchangeVendorProgressStatus.UnExchangeable;
            queryStatus = string.Join(",", new int[] { (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.SendToSeller });
            queryStatus = string.Format(" in ({0}) ", queryStatus);

            exchangeOrderList = _op.ViewVbsExchangeOrderListCollectionGet(
                DateTime.TryParse(queryData.queryBeginDate, out beginDate) ?
                ViewVbsExchangeOrderList.Columns.CreateTime + " >= " + beginDate : "",
                DateTime.TryParse(queryData.queryEndDate, out endDate) ?
                ViewVbsExchangeOrderList.Columns.CreateTime + " < " + endDate.AddDays(1) : "",
                !string.IsNullOrEmpty(queryData.queryOptionValue) && !string.IsNullOrEmpty(filter) ? filter + " = " + queryData.queryOptionValue : "",
                !string.IsNullOrEmpty(queryExchagneStatus) ?
                ViewVbsExchangeOrderList.Columns.VendorProgressStatus + queryExchagneStatus : "",
                ViewVbsExchangeOrderList.Columns.ProgressStatus + queryStatus,
                ViewVbsExchangeOrderList.Columns.AccountId + " = " + VbsCurrent.AccountId
                ).OrderByDescending(x => x.ProgressStatus).ThenByDescending(x => x.CreateTime).ToList();

            model.UnProcessCount = exchangeOrderList.Where(x => x.VendorProgressStatus <= (int)ExchangeVendorProgressStatus.Processing).Count();
            model.ProcessingCount = exchangeOrderList.Count - model.UnProcessCount;
            queryData.queryProcessStep = queryData.queryProcessStep ?? 1;
            bool isReadOnly = queryData.queryProcessStep == 3;
            model.IsReadOnly = isReadOnly;


            queryExchagneStatus = " > " + (int)ExchangeVendorProgressStatus.ExchangeProceed;
            queryStatus = string.Join(",", new int[] { (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.ExchangeSuccess, (int)OrderReturnStatus.ExchangeFailure, (int)OrderReturnStatus.ExchangeCancel });
            queryStatus = string.Format(" in ({0}) ", queryStatus);

            endExchangeOrderList = _op.ViewVbsExchangeOrderListCollectionGet(
            DateTime.TryParse(queryData.queryBeginDate, out beginDate) ?
            ViewVbsExchangeOrderList.Columns.CreateTime + " >= " + beginDate : "",
            DateTime.TryParse(queryData.queryEndDate, out endDate) ?
            ViewVbsExchangeOrderList.Columns.CreateTime + " < " + endDate.AddDays(1) : "",
            !string.IsNullOrEmpty(queryData.queryOptionValue) && !string.IsNullOrEmpty(filter) ? filter + " = " + queryData.queryOptionValue : "",
            !string.IsNullOrEmpty(queryExchagneStatus) ?
            ViewVbsExchangeOrderList.Columns.VendorProgressStatus + queryExchagneStatus : "",
            ViewVbsExchangeOrderList.Columns.ProgressStatus + queryStatus,
            ViewVbsExchangeOrderList.Columns.AccountId + " = " + VbsCurrent.AccountId
            ).OrderByDescending(x => x.ProgressStatus).ThenByDescending(x => x.CreateTime).ToList();

            model.EndCount = endExchangeOrderList.Where(x => x.VendorProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted ||
            (x.VendorProgressStatus == (int)ExchangeVendorProgressStatus.UnExchangeable && x.ProgressStatus != (int)OrderReturnStatus.ExchangeProcessing)).Count();

            if (isReadOnly)
            {
                exchangeOrderList = endExchangeOrderList;
            }

            exchangeOrderList = exchangeOrderList.Where(x => queryData.queryProcessStep == 1 ? x.VendorProgressStatus <= (int)ExchangeVendorProgressStatus.Processing :
            queryData.queryProcessStep == 2 ? x.VendorProgressStatus > (int)ExchangeVendorProgressStatus.Processing :
            x.VendorProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted ||
            (x.VendorProgressStatus == (int)ExchangeVendorProgressStatus.UnExchangeable && x.ProgressStatus != (int)OrderReturnStatus.ExchangeProcessing)).ToList();

            if (exchangeOrderList.Count > 0)
            {
                int serNo = 1;

                foreach (var item in exchangeOrderList.Skip((pageIndex - 1) * pageSize).Take(pageSize))
                {
                    OrderListSharedInfos info = new OrderListSharedInfos();

                    #region 隱藏個資

                    bool unRecoverable = item.ProgressStatus.Equals((int)OrderReturnStatus.ExchangeCancel) ||
                                             item.ProgressStatus.Equals((int)OrderReturnStatus.ExchangeSuccess) ||
                                             item.ProgressStatus.Equals((int)OrderReturnStatus.ExchangeFailure);

                    var vdlModel = new ShipOrderListModel();
                    var dealInfos = GetDealSaleInfo((Guid)item.MainBid);
                    var dealGuids = dealInfos.Select(x => x.DealId);
                    vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == (Guid)item.MainBid);
                    //檔次出貨狀態為完成 隱藏消費者個資
                    bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);

                    //退貨完成 或 ATM退款訂單(廠商無法進行退貨復原) 或 檔次出貨狀態為完成 隱藏消費者個資
                    if (unRecoverable || isHiddenPersonalInfo)
                    {
                        item.MemberName = "***";
                        item.ReceiverName = "***";
                        item.MobileNumber = "******";
                        item.ReceiverAddress = "******";
                        item.DeliveryAddress = "******";
                    }
                    #endregion

                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.Bid);

                    info.SerNo = (((pageIndex - 1) * pageSize) + serNo).ToString();
                    info.OrderReturnId = item.Id;
                    info.OrderId = (vpd.IsWms ? "[24H到貨]<br/>" : string.Empty) + item.OrderId;
                    info.ProgressStatus = item.ProgressStatus;
                    info.VendorProgressStatus = item.VendorProgressStatus ?? 0;
                    info.ShipCompanyId = item.ShipCompanyId ?? 0;
                    info.ShipNo = item.ShipNo;
                    info.ProcessMemo = item.VendorMemo;
                    info.CreateTime = item.CreateTime.ToString("yyyy/MM/dd HH:mm");
                    info.Reason = item.Reason ?? string.Empty;
                    info.RecipientName = string.IsNullOrEmpty(item.ReceiverName) ? item.MemberName : item.ReceiverName;
                    info.RecipientAddress = string.IsNullOrEmpty(item.ReceiverAddress) ? item.DeliveryAddress : item.ReceiverAddress;
                    info.RecipientMobile = item.MobileNumber;
                    info.DealId = item.UniqueId;
                    info.DealName = item.ItemName;
                    info.VendorProcessLog = GetExchangeStatusLog(item.Id);
                    model.Infos.Add(info);

                    serNo++;
                }
                int totalCount = exchangeOrderList.Count;
                model.TotalCount = totalCount;
                model.PageCount = totalCount % pageSize > 0 ? (totalCount / pageSize) + 1 : totalCount / pageSize;

                var shipList = _op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();

                ViewBag.ShipCompanyInfos = shipList;
            }

            return View(model);
        }

        /// <summary>
        /// 換貨儲存
        /// </summary>
        /// <param name="exchangeInfo"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ExchangeProcess(IEnumerable<VendorExchangeOrderProcess> exchangeInfos)
        {
            bool result = true;
            string message = string.Empty;
            var now = DateTime.Now;
            bool? isFinish = false;
            int? exchangeLogId = null;
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            if (exchangeInfos != null)
            {
                foreach (var exchangeInfo in exchangeInfos)
                {
                    //處理備註若和輸入提示內容相同 則改為null
                    if (exchangeInfo.ShipMemo != null && exchangeInfo.ShipMemo.Trim().Equals(exchangeInfo.ShipMemoDefault))
                    {
                        exchangeInfo.ShipMemo = null;
                    }
                    if (exchangeInfo.ProcessMemo != null && exchangeInfo.ProcessMemo.Trim().Equals(exchangeInfo.ProcessMemoDefault))
                    {
                        exchangeInfo.ProcessMemo = null;
                    }

                    #region 換貨處理進度

                    var exchangeLog = _op.OrderReturnListGet(exchangeInfo.ExchangeLogId);

                    if (exchangeLog.Status == (int)OrderReturnStatus.ExchangeCancel ||
                        exchangeLog.Status == (int)OrderReturnStatus.ExchangeSuccess ||
                        exchangeLog.Status == (int)OrderReturnStatus.ExchangeFailure)
                    {
                        return Json(new { IsSuccess = false, Message = "17Life已確認換貨狀態並結案，請返回換貨列表繼續進行換貨處理", ExchangeLogId = exchangeInfo.ExchangeLogId, IsFinish = isFinish });
                    }

                    if (exchangeLog.VendorMemo != exchangeInfo.ProcessMemo ||
                        exchangeLog.VendorProgressStatus != exchangeInfo.ProgressStatus)
                    {
                        exchangeLog.Status = (int)OrderReturnStatus.ExchangeProcessing;
                        exchangeLog.VendorProgressStatus = exchangeInfo.ProgressStatus == (int)ExchangeVendorProgressStatus.Unknown ? (int)ExchangeVendorProgressStatus.Processing : (int)exchangeInfo.ProgressStatus;
                        exchangeLog.VendorMemo = exchangeInfo.ProcessMemo;
                        exchangeLog.VendorProcessTime = now;
                        exchangeLog.MessageUpdate = false;
                        exchangeLog.ModifyId = modifyId;
                        exchangeLog.ModifyTime = now;

                        var modifyMessage = string.Format("商家 {0} 更新換貨進度->{1}, 處理備註->{2}", modifyId,
                            Helper.GetLocalizedEnum((ExchangeVendorProgressStatus)exchangeLog.VendorProgressStatus),
                            exchangeLog.VendorMemo);

                        if (!OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
                        {
                            result = false;
                            message = "儲存失敗，訂單狀態已更新；\n請重整畫面後重試";
                            isFinish = ValidateVendorProgressState((OrderReturnStatus)exchangeLog.Status,
                                                                   (ExchangeVendorProgressStatus)exchangeLog.VendorProgressStatus);
                        }
                    }

                    #endregion 換貨處理進度

                    #region 出貨資訊

                    var os = new OrderShip();
                    if (exchangeLog.OrderShipId.HasValue)
                    {
                        os = _op.GetOrderShipById(exchangeLog.OrderShipId.Value);
                        if (!os.IsLoaded)
                        {
                            result = false;
                            message = "儲存出貨資訊出現異常狀況，查無出貨紀錄";
                        }
                    }
                    else
                    {
                        os.Type = (int)OrderShipType.Exchange;
                        os.OrderGuid = exchangeLog.OrderGuid;
                        os.OrderClassification = (int)OrderClassification.LkSite;
                        os.CreateId = modifyId;
                        os.CreateTime = now;
                    }


                    if (exchangeInfo.ProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted && exchangeInfo.ShipTime == null)
                    {
                        exchangeInfo.ShipTime = DateTime.Now.Date;
                    }
                    if ((!string.IsNullOrEmpty(exchangeInfo.ShipNo) &&
                         exchangeInfo.ShipNo != os.ShipNo) ||
                        exchangeInfo.ShipTime != os.ShipTime ||
                        exchangeInfo.ShipCompanyId != os.ShipCompanyId ||
                        exchangeInfo.ShipMemo != os.ShipMemo)
                    {
                        os.ShipNo = exchangeInfo.ShipNo;
                        os.ShipTime = exchangeInfo.ShipTime;
                        os.ShipCompanyId = exchangeInfo.ShipCompanyId;
                        os.ShipMemo = exchangeInfo.ShipMemo;
                        os.ModifyId = modifyId;
                        os.ModifyTime = now;

                        if (!OrderShipUtility.ExchangeOrderShipSet(os, exchangeLog))
                        {
                            result = false;
                            message = "儲存出貨資訊出現錯誤狀況";
                        }
                    }
                    exchangeLogId = exchangeInfo.ExchangeLogId;
                    #endregion 出貨資訊


                }
            }



            return Json(new { IsSuccess = result, Message = message, ExchangeLogId = exchangeLogId, IsFinish = isFinish });
        }


        #region 舊版已不再使用,留下參考用
        ///// <summary>
        ///// 舊版換貨管理
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="type"></param>
        ///// <param name="pageNumber"></param>
        ///// <param name="sortCol"></param>
        ///// <param name="sortDesc"></param>
        ///// <param name="viewVendorProgressState"></param>
        ///// <param name="selQueryOption"></param>
        ///// <param name="queryKeyWord"></param>
        ///// <returns></returns>
        //[VbsAuthorize]
        //public ActionResult ExchangeOrderList(Guid id, VbsDealType type,
        //    int? pageNumber, string sortCol, bool? sortDesc, ViewVendorProgressState? viewVendorProgressState, string selQueryOption, string queryKeyWord)
        //{
        //    //檢查商家帳號是否有檢視該檔次分店權限
        //    if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
        //    {
        //        return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
        //    }
        //    //檢查是否小倉檔次
        //    bool isConsignment = false;
        //    if (_conf.IsConsignment)
        //    {
        //        isConsignment = (bool)_pp.DealPropertyGet(id).Consignment;
        //    }

        //    pageNumber = pageNumber ?? 1;
        //    sortDesc = sortDesc ?? false;
        //    sortCol = sortCol ?? "ExchangeApplicationTime";
        //    queryKeyWord = string.IsNullOrEmpty(queryKeyWord) ? queryKeyWord : queryKeyWord.Trim();
        //    viewVendorProgressState = viewVendorProgressState ?? ViewVendorProgressState.Process;

        //    var vdlModel = new ShipOrderListModel();
        //    var infos = new List<VbsExchangeOrderListInfo>();
        //    var dealInfos = GetDealSaleInfo(id);
        //    var dealGuids = dealInfos.Select(x => x.DealId);
        //    vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == id);

        //    if (!isConsignment)
        //    {
        //        //檔次出貨狀態為完成 隱藏消費者個資
        //        bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
        //        //取得照會完成數量
        //        int[] orderUserMemoList = GetOrderUserMemoListCount(dealInfos, type);
        //        int orderUserMemoListCount = orderUserMemoList[0];
        //        int orderUserMemoListCountAll = orderUserMemoList[1];

        //        //取得換貨狀態為處理中及換貨完成之換貨訂單
        //        var exchangeStatusList = new List<int> { (int)OrderReturnStatus.ExchangeCancel, (int)OrderReturnStatus.ExchangeProcessing, (int)OrderReturnStatus.ExchangeSuccess,
        //                                                 (int)OrderReturnStatus.SendToSeller, (int)OrderReturnStatus.ExchangeFailure};
        //        var exchangeList = _op.ViewPponOrderReturnListGetList(dealGuids, OrderReturnType.Exchange, exchangeStatusList, new List<int>()).ToList();
        //        if (exchangeList.Count > 0)
        //        {
        //            //取得return_form_id
        //            //List<int> exchangeIds = exchangeList.Select(x => x.ReturnId).Distinct().ToList();
        //            //List<Guid> orderGuids = exchangeList.Select(x => x.OrderGuid).Distinct().ToList();

        //            foreach (var order in exchangeList)
        //            {
        //                bool unRecoverable = order.ReturnStatus.Equals((int)OrderReturnStatus.ExchangeCancel) ||
        //                                     order.ReturnStatus.Equals((int)OrderReturnStatus.ExchangeSuccess) ||
        //                                     order.ReturnStatus.Equals((int)OrderReturnStatus.ExchangeFailure);

        //                //換貨完成(廠商無法進行退貨復原) 或 檔次退換貨狀態為完成 隱藏消費者個資
        //                if (unRecoverable || isHiddenPersonalInfo)
        //                {
        //                    order.MemberName = "***";
        //                    order.PhoneNumber = "******";
        //                }

        //                var message = string.Empty;
        //                var messageList = GetExchangeStatusLog(order.ReturnId, true).OrderByDescending(x => x.CreateTime);
        //                if (messageList.Any())
        //                {
        //                    message = messageList.First().Message;
        //                }

        //                //舊版換貨單進度為完成換貨 因無廠商處理進度資料 則統一於前台顯示 客服已處理
        //                //             而處理中的換貨單 則可自行於商家系統中進行換貨管理
        //                var info = new VbsExchangeOrderListInfo
        //                {
        //                    OrderId = order.OrderId,
        //                    OrderGuid = order.OrderGuid,
        //                    ExchangeApplicationTime = order.ReturnCreateTime,
        //                    ExchangeReason = order.Reason,
        //                    ExchangeMessage = message,
        //                    RecipientName = string.IsNullOrEmpty(order.ReceiverName) ? order.MemberName : order.ReceiverName,
        //                    RecipientTel = order.PhoneNumber,
        //                    OrderShipId = order.OrderShipId,
        //                    ShipTime = order.OrignalShipTime,
        //                    ExchangeLogId = order.ReturnId,
        //                    VendorProgressStatus = order.VendorProgressStatus.HasValue
        //                                            ? (ExchangeVendorProgressStatus)order.VendorProgressStatus.Value
        //                                            : unRecoverable 
        //                                                ? ExchangeVendorProgressStatus.Unknown
        //                                                : ExchangeVendorProgressStatus.Processing,
        //                    ExchangeStatus = (OrderReturnStatus)order.ReturnStatus
        //                };

        //                infos.Add(info);
        //            }
        //        }
        //        vdlModel.OrderUserMemoListCount = orderUserMemoListCount;
        //        vdlModel.OrderUserMemoListCountAll = orderUserMemoListCountAll;
        //    }

        //    vdlModel.ExchangeOrderPagerInfos = GetPagerVbsExchangeOrderInfos(infos, viewVendorProgressState, selQueryOption, queryKeyWord, 20, pageNumber.Value - 1, sortCol, sortDesc.Value);
        //    vdlModel.IsResultEmpty = !infos.Any();
        //    vdlModel.DealId = id;
        //    ViewBag.DealType = type;
        //    ViewBag.IsConsignment = isConsignment;
        //    ViewBag.UserName = Session[VbsSession.UserName.ToString()];
        //    ViewBag.UserId = Session[VbsSession.UserId.ToString()];
        //    ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
        //    ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
        //    Session[VbsSession.ParentActionName.ToString()] = "ExchangeOrderList";

        //    return View("ExchangeOrderList", vdlModel);
        //}

        ///// <summary>
        ///// 舊版換貨管理內頁
        ///// </summary>
        ///// <param name="id"></param>
        ///// <param name="type"></param>
        ///// <param name="orderId"></param>
        ///// <returns></returns>
        //[VbsAuthorize]
        //public ActionResult ExchangeOrderProcess(Guid id, VbsDealType type, string orderId)
        //{
        //    //檢查商家帳號是否有檢視該檔次分店權限
        //    if (!CheckIsAllowedDeal(VbsCurrent.Is17LifeEmployee, VbsCurrent.AccountId, id))
        //    {
        //        return RedirectToAction("RedirectToActionAfterLogin", "VendorBillingSystem");
        //    }

        //    var vdlModel = new ShipOrderListModel();
        //    var infos = new List<VbsExchangeOrderListInfo>();
        //    List<string> customerServiceList = new List<string>();

        //    vdlModel.DealSaleInfo = GetDealSaleInfo(id).First(x => x.DealId == id);
        //    //檔次出貨狀態為完成 隱藏消費者個資
        //    bool isHiddenPersonalInfo = vdlModel.DealSaleInfo.ShipState.Equals(VbsShipState.Finished);
        //    var dealInfos = GetDealSaleInfo(id);
        //    var dealIds = dealInfos.Select(x => x.DealId).ToList();

        //    vdlModel.IsComboDeal = dealIds.Count > 1;

        //    //取得換貨訂單
        //    var exchangeList = _op.ViewPponOrderReturnListGetList(dealIds, OrderReturnType.Exchange)
        //                        .Where(x =>
        //                        {
        //                            //只需處理換貨狀態為處理中的換貨單
        //                            if (!x.ReturnStatus.Equals((int)OrderReturnStatus.ExchangeProcessing) &&
        //                                !x.ReturnStatus.Equals((int)OrderReturnStatus.SendToSeller))
        //                            {
        //                                return false;
        //                            }
        //                            //只需處理廠商處理進度為處理中之換貨單
        //                            if (x.VendorProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted)
        //                            {
        //                                return false;
        //                            }
        //                            //只需處理已出貨的退貨單
        //                            if (!x.OrignalShipTime.HasValue || x.OrignalShipTime.Value.Date > DateTime.Now.Date)
        //                            {
        //                                return false;
        //                            }

        //                            return true;
        //                        })
        //                        .ToList();
        //    if (!string.IsNullOrEmpty(orderId))
        //    {
        //        exchangeList = exchangeList.Where(x => x.OrderId.Equals(orderId))
        //                               .ToList();
        //    }
        //    if (exchangeList.Count > 0)
        //    {
        //        //取得訂購商品品項資訊
        //        PponOrder odr = new PponOrder(exchangeList.First().OrderGuid);
        //        IEnumerable<SummarizedProductSpec> source = odr.GetToHouseProductSummary();
        //        string orderItemDesc = string.Join("\n", source.Select(x => x.Spec + " " + x.TotalCount));
        //        //取得exchange log id
        //        //List<int> exchangeLogId = exchangeList.Select(x => x.ReturnId).Distinct().ToList();
        //        foreach (var order in exchangeList)
        //        {
        //            var dealInfo = dealInfos.FirstOrDefault(x => x.DealId == order.BusinessHourGuid);
        //            if (dealInfo == null)
        //            {
        //                continue;
        //            }

        //            if (isHiddenPersonalInfo)
        //            {
        //                order.MemberName = "***";
        //                order.PhoneNumber = "******";
        //                order.DeliveryAddress = "*********";
        //            }

        //            var info = new VbsExchangeOrderListInfo
        //            {
        //                DealInfo = dealInfo,
        //                OrderId = order.OrderId,
        //                OrderGuid = order.OrderGuid,
        //                OrderItemDescription = orderItemDesc,
        //                RecipientName = string.IsNullOrEmpty(order.ReceiverName) ? order.MemberName : order.ReceiverName,
        //                RecipientTel = order.PhoneNumber,
        //                RecipientAddress = string.IsNullOrEmpty(order.ReceiverAddress) ? order.DeliveryAddress : order.ReceiverAddress,
        //                ExchangeApplicationTime = order.ReturnCreateTime,
        //                ExchangeReason = string.IsNullOrEmpty(order.Reason) 
        //                                 ? string.Empty 
        //                                 : order.Reason.Replace("\n", "<br/>"),
        //                VendorProgressStatus = order.VendorProgressStatus.HasValue
        //                                        ? (ExchangeVendorProgressStatus)order.VendorProgressStatus.Value
        //                                        : ExchangeVendorProgressStatus.Processing,
        //                VendorMemo = order.VendorMemo,
        //                ShipTime = order.OrignalShipTime,
        //                ExchangeLogId = order.ReturnId,
        //                VendorProcessLog = GetExchangeStatusLog(order.ReturnId)
        //            };

        //            info.ExchangeOrderShipInfo = GetExchangeOrderShipInfo(order.OrderShipId);

        //            infos.Add(info);
        //        }

        //        #region 客服案件
        //        ViewCustomerServiceListCollection viewCSList = _service.GetViewCustomerServiceMessageByOrderGuid(exchangeList.FirstOrDefault().OrderGuid);
        //        customerServiceList.AddRange(
        //            viewCSList
        //            .Where(x => x.CustomerServiceStatus >= (int)Core.Enumeration.statusConvert.transfer)
        //            .Select(y => y.ServiceNo).ToList()
        //            );                
        //        #endregion 客服案件

        //    }

        //    ViewBag.CustomerServiceList = customerServiceList;

        //    vdlModel.ExchangeOrderInfos = infos.OrderByDescending(x => x.ExchangeApplicationTime)
        //                                     .ToList();
        //    //撈取物流公司資料
        //    vdlModel.ShipCompanyInfos = _op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();
        //    ViewBag.UserName = Session[VbsSession.UserName.ToString()];
        //    ViewBag.UserId = Session[VbsSession.UserId.ToString()];
        //    ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];
        //    ViewBag.VbsShipDealInfos = Session[VbsSession.VbsShipDealInfos.ToString()] as IEnumerable<VbsShipDealInfo>;
        //    Session[VbsSession.ParentActionName.ToString()] = "ExchangeOrderProcess";
        //    return View("ExchangeOrderProcess", vdlModel);
        //}


        ///// <summary>
        ///// 復原換貨
        ///// </summary>
        ///// <param name="exchangeLogId"></param>
        ///// <returns></returns>
        //[HttpPost]
        //[VbsAuthorize]
        //public ActionResult ExchangeProcessRecover(int exchangeLogId)
        //{
        //    bool result = false;
        //    string message = string.Empty;
        //    var now = DateTime.Now;

        //    var exchangeLog = _op.OrderReturnListGet(exchangeLogId);

        //    if (exchangeLog.Status == (int)OrderReturnStatus.ExchangeCancel ||
        //        exchangeLog.Status == (int)OrderReturnStatus.ExchangeSuccess ||
        //        exchangeLog.Status == (int)OrderReturnStatus.ExchangeFailure)
        //    {
        //        return Json(new { IsSuccess = result, Message = "17Life已確認換貨狀態並結案，無法使用復原功能", ExchangeLogId = exchangeLogId, IsFinish = false });
        //    }

        //    var modifyMessage = string.Format("商家 {0} 進行換貨狀態復原作業", VbsCurrent.AccountId);
        //    exchangeLog.Status = (int)OrderReturnStatus.ExchangeProcessing;
        //    exchangeLog.VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing;
        //    exchangeLog.VendorProcessTime = now;
        //    exchangeLog.VendorMemo = null;
        //    exchangeLog.ModifyTime = now;
        //    exchangeLog.ModifyId = VbsCurrent.AccountId;

        //    bool? isFinish = null;

        //    if (OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
        //    {
        //        message = "復原成功";
        //    }
        //    else
        //    {
        //        message = "復原失敗，訂單狀態已更新；\n請重整畫面後重試";
        //        isFinish = ValidateVendorProgressState((OrderReturnStatus)exchangeLog.Status,
        //                                               (ExchangeVendorProgressStatus)exchangeLog.VendorProgressStatus);
        //    }

        //    return Json(new { IsSuccess = result, Message = message, ExchangeLogId = exchangeLogId, IsFinish = isFinish });
        //} 
        #endregion

        #endregion 換貨管理

        #region Wms

        #region 合約確認
        [VbsAuthorize]
        public ActionResult WmsContract()
        {
            VbsMembership vbsMembership = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
            ViewBag.IsContractConfirm = vbsMembership.WmsContract == _conf.WmsContractVersion;
            return View();
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetWmsContractLog()
        {
            WmsContractLogCollection pclc = _sp.GetWmsContractLogByAccountid(VbsCurrent.AccountId);

            var logs = pclc.Select(x => new
            {
                CreateTime = x.CreateTime.Value.ToString("yyyyMMdd HH:mm:ss")
            });

            return Json(logs);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult WmsContractConfirm()
        {
            try
            {
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    return Json(new { IsSuccess = false, Message = "模擬身分不允許執行此功能" });
                }
                bool result = false;

                //log
                WmsContractLog pcl = new WmsContractLog();
                pcl.AccountId = VbsCurrent.AccountId;
                pcl.Version = _conf.WmsContractVersion;
                pcl.CreateId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                pcl.CreateTime = DateTime.Now;
                _sp.WmsContractLogSet(pcl);

                //開啟寄倉權限
                VbsMembership vbsMembership = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
                vbsMembership.ViewWmsRight = true;
                vbsMembership.WmsContract = _conf.WmsContractVersion;
                _mp.VbsMembershipSet(vbsMembership);

                result = true;
                return Json(new { IsSuccess = result, Message = "寄倉權限已開啟，請重新登入" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }

        }
        #endregion

        #region 聯絡人資訊

        [VbsAuthorize]
        public ActionResult WmsContact()
        {
            VbsMembership vbsMembership = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);

            WmsContact contact = WmsFacade.GetWmsContact(VbsCurrent.AccountId);

            WmsContactModel model = WmsContactModel.Create(contact, VbsCurrent.AccountId);

            return View(model);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SaveWmsContact(WmsContactModel model)
        {
            try
            {
                string message;
                bool result = WmsFacade.SaveWmsContact(model, VbsCurrent.AccountId, UserName, out message);
                return Json(new
                {
                    IsSuccess = result,
                    Message = message
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    IsSuccess = false,
                    ex.Message
                });
            }
        }

        #endregion 聯絡人資訊

        #region 新增/查詢進倉單

        /// <summary>
        /// 新增進倉單搜尋
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="bid"></param>
        /// <param name="uniqueId"></param>
        /// <param name="productBrandName"></param>
        /// <param name="productName"></param>
        /// <param name="productNo"></param>
        /// <param name="specs"></param>
        /// <param name="productCode"></param>
        /// <param name="pchomeProdId"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult CreateWmsPurchaseOrderList(int pageStart, int pageLength, int? pid, string strBid, int? uniqueId, string productBrandName, string productName, int? productNo, string specs, string productCode, string pchomeProdId)
        {
            Guid bid = Guid.Empty;
            Guid? resultBid = null;
            if (!string.IsNullOrEmpty(strBid))
            {
                bool tryP = Guid.TryParse(strBid, out bid);
                resultBid = bid;
            }
            else
            {
                resultBid = null;
            }

            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true).Select(x => x.Key).ToList();
            var vpiList = PponFacade.GetProductOptionListForVbs(sellerGuids, productNo, productBrandName, productName, "", productCode, specs, null, null, resultBid, uniqueId, pid, 1);
            var result = PponFacade.GetBaseProductOption(vpiList, pageStart, pageLength);


            return Json(result);
        }

        /// <summary>
        /// 申請進倉單審核
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult WmsPurchaseOrderConfirm(string item)
        {
            try
            {
                List<dynamic> items = new JsonSerializer().Deserialize<List<dynamic>>(item);
                DataOrm.WmsPurchaseOrderCollection orders = new WmsPurchaseOrderCollection();
                foreach (var order in items)
                {
                    string itemguid = order["itemGuid"];
                    int qty = Convert.ToInt32(order["qty"]);
                    int salesId = Convert.ToInt32(order["salesId"]);

                    DataOrm.WmsPurchaseOrder newOrder = new DataOrm.WmsPurchaseOrder();
                    newOrder.Guid = Guid.NewGuid();
                    newOrder.ItemGuid = Guid.Parse(itemguid);
                    newOrder.Qty = qty;
                    newOrder.SalesId = salesId;
                    newOrder.Status = (int)PurchaseOrderStatus.Apply;
                    newOrder.CreateId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                    newOrder.CreateTime = DateTime.Now;
                    orders.Add(newOrder);
                }
                
                _wp.WmsPurchaseOrderSet(orders);

                if(orders.Count() > 0)
                {
                    WmsFacade.SaveWmsPurchaseOrderLog(orders.Select(x => x.Guid).ToList(), orders.First().Status
                    ,  orders.First().CreateId);
                }

                ProposalFacade.SendWmsPurchaseOrderMail(orders.Select(x => x.Guid).ToList(), true, null);
                return Json(new { IsSuccess = true, Message = "送審後，不要忘了要常常到＜列印進倉單麥頭與二聯單＞頁籤看看狀況喔" });

            }
            catch (Exception ex)
            {
                logger.Error("WmsPurchaseOrderConfirm:" + item, ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }

        }

        /// <summary>
        /// 搜尋進倉單
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="bid"></param>
        /// <param name="uniqueId"></param>
        /// <param name="productBrandName"></param>
        /// <param name="productName"></param>
        /// <param name="productNo"></param>
        /// <param name="specs"></param>
        /// <param name="productCode"></param>
        /// <param name="pchomePurchaseOrderId"></param>
        /// <param name="purchaseOrderStatus"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult WmsPurchaseOrderList(int pageStart, int pageLength, int? pid, string strBid, int? uniqueId, string productBrandName
            , string productName, int? productNo, string specs, string productCode, string pchomePurchaseOrderId, int? purchaseOrderStatus, bool? dontDisplayInvalidation)
        {
            Guid bid = Guid.Empty;
            Guid? resultBid = null;
            if (!string.IsNullOrEmpty(strBid))
            {
                bool tryP = Guid.TryParse(strBid, out bid);
                resultBid = bid;
            }
            else
            {
                resultBid = null;
            }

            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true).Select(x => x.Key).ToList();
            var vpiList = PponFacade.GetWmsProchaseOrder(sellerGuids, pid, resultBid, uniqueId, productBrandName, productName, productNo, specs, productCode
                , pchomePurchaseOrderId, purchaseOrderStatus, "", null, null, 1, string.Empty, dontDisplayInvalidation, string.Empty);
            var result = PponFacade.GetWmsPurchaseOrder(vpiList, pageStart, pageLength);


            return Json(result);
        }

        [VbsAuthorize]
        public ActionResult WmsPrint()
        {
            return View();
        }

        /// <summary>
        /// 呼叫API新增進倉單
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult CreatePurchaseOrder(string item)
        {
            try
            {
                //新增進倉單->pchome
                WmsContact contact = _wp.WmsContactGet(VbsCurrent.AccountId);
                VbsMembership vbsMembership = _mp.VbsMembershipGetByAccountId(VbsCurrent.AccountId);
                List<string> purchaseOrderGuidList = new List<string>();
                List<Guid> purchaseOrderList = new List<Guid>();
                Dictionary<string, string> purchaseOrderGuidErrorList = new Dictionary<string, string>();
                List<dynamic> items = new JsonSerializer().Deserialize<List<dynamic>>(item);
                foreach (var order in items)
                {
                    string AddProdMessage = string.Empty;
                    
                    Guid itemGuid = Guid.Empty;
                    string iGuid = order["itemGuid"];
                    Guid.TryParse(iGuid, out itemGuid);

                    string pchomeProdId = _pp.ProductItemGet(itemGuid).PchomeProdId;

                    if (string.IsNullOrEmpty(pchomeProdId))
                    {
                        //取得商品ID
                        WmsFacade.RequestWmsAPIToAddProd(itemGuid, VbsCurrent.AccountId, out AddProdMessage, out pchomeProdId);
                    }
                    string purchaseOrderGuid = order["purchaseOrderGuid"];
                    int qty = Convert.ToInt32(order["qty"]);
                    int price = Convert.ToInt32(order["price"]);

                    if (string.IsNullOrEmpty(AddProdMessage))
                    {
                        if (!string.IsNullOrEmpty(pchomeProdId) && contact.Id != 0)
                        {
                            var po = new BizLogic.Models.Wms.WmsPurchaseOrder
                            {
                                Prod = new BizLogic.Models.Wms.WmsPurchaseOrder.PurchaseOrderProd
                                {
                                    Id = pchomeProdId,
                                    Price = price,
                                    Qty = qty,
                                },
                                SupplyId = VbsCurrent.AccountId,
                                SupplyName = vbsMembership.Name,
                                //ApplyDate = DateTime.Now.ToString("yyyy/MMddHHmm"),
                                PurchaseContact = new BizLogic.Models.Wms.WmsPerson
                                {
                                    Name = contact.WarehousingName,
                                    Tel = contact.WarehousingTel,
                                    Mobile = contact.WarehousingMobile
                                },
                                ReturnContact = new BizLogic.Models.Wms.WmsPerson
                                {
                                    Name = contact.ReturnerName,
                                    Tel = contact.ReturnerTel,
                                    Mobile = contact.ReturnerMobile,
                                    Email = contact.ReturnerEmail,
                                    Zip = contact.ReturnerZip,
                                    Address = contact.ReturnerAddress
                                }
                            };
                            bool purchaseOrderAdded = PchomeWmsAPI.AddPurchaseOrder(po);
                            if (purchaseOrderAdded)
                            {
                                //取得進倉ID
                                WmsPurchaseOrder wpo = _wp.WmsPurchaseOrderGet(Guid.Parse(purchaseOrderGuid));
                                wpo.PchomePurchaseOrderId = po.Id;
                                wpo.Status = (int)PurchaseOrderStatus.Submit;
                                wpo.ModifyId = VbsCurrent.AccountId;
                                wpo.ModifyTime = DateTime.Now;
                                
                                _wp.WmsPurchaseOrderSet(wpo);
                                WmsFacade.SaveWmsPurchaseOrderLog(new List<Guid> {wpo.Guid}, (int)PurchaseOrderStatus.Submit, VbsCurrent.AccountId);

                                purchaseOrderGuidList.Add(po.Id);
                            }
                            else
                            {
                                purchaseOrderGuidErrorList.Add(purchaseOrderGuid,"建立進倉單失敗");
                                //抓取進倉單
                                WmsPurchaseOrder wpo = _wp.WmsPurchaseOrderGet(Guid.Parse(purchaseOrderGuid));
                                wpo.Status = (int)PurchaseOrderStatus.WmsFail;
                                wpo.Invalidation = (int)WmsInvalidation.Yes;
                                wpo.ModifyId = VbsCurrent.AccountId;
                                wpo.ModifyTime = DateTime.Now;
                                _wp.WmsPurchaseOrderSet(wpo);
                                WmsFacade.SaveWmsPurchaseOrderLog(new List<Guid> { wpo.Guid }, (int)PurchaseOrderStatus.WmsFail, VbsCurrent.AccountId);


                                //發送信件
                                purchaseOrderList = new List<Guid>();
                                purchaseOrderList.Add(Guid.Parse(purchaseOrderGuid));
                                ProposalFacade.SendWmsPurchaseOrderMail(purchaseOrderList, false, true);

                            }
                        }
                        else
                        {
                            purchaseOrderGuidErrorList.Add(purchaseOrderGuid,"聯絡人或商品ID為空");
                        }
                    }
                    else
                    {
                        purchaseOrderGuidErrorList.Add(purchaseOrderGuid, AddProdMessage);
                    }
                    
                    
                }


                //列印
                string message = string.Empty;
                string url = string.Empty;
                if (purchaseOrderGuidList.Any())
                {
                    url = PchomeWmsAPI.GetPurchaseOrderMarkWithReceiptUrl(purchaseOrderGuidList.ToArray());
                    url = Helper.CombineUrl(_conf.SiteUrl, "Service/PrintMarkWithReceipt.ashx?url=") + Uri.EscapeDataString(url);
                }
                else
                {
                    message = "下載錯誤";
                }
                if (purchaseOrderGuidErrorList.Any())
                {
                    message = "下載錯誤清單：" + string.Join(",", purchaseOrderGuidErrorList);
                }


                if (!string.IsNullOrEmpty(message) || !purchaseOrderGuidList.Any())
                {
                    logger.Error("CreatePurchaseOrder Error:" + message);
                    return Json(new { IsSuccess = false, Message = message });
                }
                else
                    return Json(new { IsSuccess = true, Url = url });

            }
            catch (Exception ex)
            {
                logger.Error("CreatePurchaseOrder  Error:" + item, ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }


        }

        [VbsAuthorize]
        public ActionResult WmsProgressStatus()
        {
            return View();
        }

        /// <summary>
        /// 呼叫API列印麥頭
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult PrintMarkWithReceipt(string orderId)
        {
            try
            {
                List<string> orderIdList = new JsonSerializer().Deserialize<List<string>>(orderId);
                string url = PchomeWmsAPI.GetPurchaseOrderMarkWithReceiptUrl(orderIdList.ToArray());

                url = Helper.CombineUrl(_conf.SiteUrl, "Service/PrintMarkWithReceipt.ashx?url=") + Uri.EscapeDataString(url);
                logger.Info("PrintMarkWithReceipt url=" + url);

                return Json(new { IsSuccess = true, Url = url });

            }
            catch (Exception ex)
            {
                logger.Error("PrintMarkWithReceipt:" + orderId, ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }


        }

        [VbsAuthorize]
        public ActionResult WmsPurchaseOrder()
        {
            WmsContact contact = _wp.WmsContactGet(VbsCurrent.AccountId);
            ViewBag.ContactSuccess = (contact == null) ? false : true;
            return View();
        } 

        /// <summary>
        /// 進倉單作廢
        /// </summary>
        /// <param name="purchaseOrderGuid"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public ActionResult ChangeWmsPurchaseOrderInvalidationStatus(string purchaseOrderGuid)
        {
            try
            {
                bool IsSuccess = PponFacade.ChangeWmsPurchaseOrderInvalidationStatus(purchaseOrderGuid, UserName);

                return Json(new { IsSuccess = IsSuccess, Message = "處理成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }



        #endregion

        #region 還貨單

        [VbsAuthorize]
        public ActionResult WmsReturnOrder()
        {
            WmsContact contact = _wp.WmsContactGet(VbsCurrent.AccountId);
            ViewBag.ContactSuccess = (contact == null) ? false : true;
            return View();
        }
        [VbsAuthorize]
        public ActionResult WmsReturnOrderQuery()
        {
            List<dynamic> returnStatus = new List<dynamic>();
            foreach (var item in Enum.GetValues(typeof(WmsReturnOrderStatus)))
            {
                if ((WmsReturnOrderStatus)item == WmsReturnOrderStatus.Initial)
                {
                    continue;
                }
                returnStatus.Add(
                    new
                    {
                        Key = (int)item,
                        Value = Helper.GetDescription((WmsReturnOrderStatus)item)
                    });
            }
            ViewBag.ReturnStatus = returnStatus;
            return View();
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SearchWmsReturnOrderList(
            int pageStart,
            int pageLength,
            string productNo,
            string productBrandName,
            string productName,
            string pchomeProdId,
            string specs,
            string productCode
            )
        {
            Guid bid = Guid.Empty;

            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true).Select(x => x.Key).ToList();
            List<ViewProductItem> vpiList = _pp.ViewProductItemListGet(
                        pageStart, pageLength, sellerGuids,
                        productNo, productBrandName, productName,
                        pchomeProdId, specs, productCode
                ).ToList();

            //var vpiList = PponFacade.GetProductOptionListForVbs(sellerGuids, productNo, productBrandName, productName, "", productCode, specs, null, null, resultBid, uniqueId, pid, 1);
            var result = PponFacade.GetBaseProductOption(vpiList, pageStart, pageLength);


            return Json(result);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ConfirmWmsReturnOrderList(string param)
        {
            try
            {
                List<dynamic> items = new JsonSerializer().Deserialize<List<dynamic>>(param);
                DataOrm.WmsReturnOrderCollection retOrders = new WmsReturnOrderCollection();
                foreach (var item in items)
                {
                    string itemGuid = item["itemGuid"];
                    string itemName = item["productName"];
                    string returnQty = item["returnQty"];
                    string salesId = item["salesId"];
                    string remark = item["remark"];

                    Guid itGuid = Guid.Empty;
                    int qty = default(int);
                    int uniqueId = default(int);

                    #region Check
                    Guid.TryParse(itemGuid, out itGuid);
                    if (itGuid == Guid.Empty)
                    {
                        return Json(
                            new
                            {
                                IsSuccess = false,
                                Message = string.Format("{0}-{1}:[{2}]", itemName, "商品Guid異常", itGuid)
                            });
                    }
                    int.TryParse(returnQty, out qty);
                    if (qty <= 0)
                    {
                        return Json(
                            new
                            {
                                IsSuccess = false,
                                Message = string.Format("{0}-{1}:[{2}]", itemName, "申請還貨數異常", returnQty)
                            });
                    }
                    int.TryParse(salesId, out uniqueId);
                    if (uniqueId == 0)
                    {
                        return Json(
                            new
                            {
                                IsSuccess = false,
                                Message = string.Format("{0}-{1}:[{2}]", itemName, "負責業務異常", uniqueId)
                            });
                    }
                    Member mem = _mp.MemberGet(uniqueId);
                    if (mem == null || mem.IsLoaded == false)
                    {
                        return Json(
                            new
                            {
                                IsSuccess = false,
                                Message = string.Format("{0}-{1}:[{2}]", itemName, "找不到該名業務", uniqueId)
                            });
                    }
                    ProductItem pi = _pp.ProductItemGet(itGuid);
                    if (pi != null && pi.IsLoaded)
                    {
                        if (pi.Stock < qty)
                        {
                            return Json(
                            new
                            {
                                IsSuccess = false,
                                Message = string.Format("{0}-{1}:庫存：[{2}]/還貨數：{3}", itemName, "申請還貨數不可大於庫存數", pi.Stock, qty)
                            });
                        }
                    }
                    #endregion Check

                    DataOrm.WmsReturnOrder retOrder = new DataOrm.WmsReturnOrder();
                    retOrder.Guid = Guid.NewGuid();
                    retOrder.AccountId = VbsCurrent.AccountId;
                    retOrder.ItemGuid = itGuid;
                    retOrder.Qty = qty;
                    retOrder.SalesId = uniqueId;
                    retOrder.Status = (int)WmsReturnOrderStatus.Apply;
                    retOrder.CreateId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                    retOrder.CreateTime = DateTime.Now;
                    retOrder.ModifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                    retOrder.ModifyTime = DateTime.Now;
                    retOrder.Remark = remark;
                    retOrder.Source = (int)WmsReturnSource.E7Life;
                    retOrders.Add(retOrder);
                }

                _wp.WmsReturnOrderSet(retOrders);


                if (retOrders.Count() > 0)
                {
                    WmsFacade.SaveWmsReturnOrderLog(retOrders.Select(x => x.Guid).ToList(), retOrders.First().Status
                    , retOrders.First().CreateId);
                }

                ProposalFacade.SendWmsReturnOrderMail(retOrders.Select(x => x.Guid).ToList(), true, null);
                return Json(new { IsSuccess = true, Message = "送審後，可以到＜查詢還貨單＞找到對應的審核結果和狀態喔" });

            }
            catch (Exception ex)
            {
                logger.Error("ConfirmWmsReturnOrderList:" + param, ex);
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult SearchWmsReturnOrderStatusList(
            int pageStart,
            int pageLength,
            string productNo,
            string productBrandName,
            string productName,
            string pchomeProdId,
            string specs,
            string productCode,
            string sDate,
            string eDate,
            string rtnStatus,
            string source,
            bool invalidation
            )
        {
            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(VbsCurrent.AccountId, true).Select(x => x.Key).ToList();
            List<ViewWmsReturnOrder> vpiList = _wp.ViewWmsReturnOrdermListGetBySellerGuidList(
                pageStart, pageLength, sellerGuids,
                        productNo, productBrandName, productName,
                        pchomeProdId, specs, productCode,
                        sDate, eDate, rtnStatus, source, invalidation
                        ).ToList();

            var result = PponFacade.GetViewWmsReturnOrderModel(vpiList, pageStart, pageLength);


            return Json(result);
        }
        [Authorize]
        [HttpPost]
        public ActionResult ChangeWmsReturnOrderInvalidationStatus(
            string returnOrderGuid)
        {
            try
            {
                bool newInvalidationStatus = PponFacade.ChangeWmsReturnOrderInvalidationStatus(returnOrderGuid, UserName);

                if (newInvalidationStatus)
                {
                    return Json(new
                    {
                        IsSuccess = true,
                        Message = "處理成功"
                    });
                }
                else
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        Message = "無法變更"
                    });
                }

            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        #endregion

        #region 查詢訂單配送與物流

        [VbsAuthorize]
        public ActionResult WmsOrderDelivery(WmsOrderDeliveryModel model = null)
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }

            ViewBag.Is17LifeEmployee = Session[VbsSession.Is17LifeEmployee.ToString()];

            Guid bid;
            if (!string.IsNullOrEmpty(model.BID) && !Guid.TryParse(model.BID, out bid))
            {
                return View(model);
            }

            model = GetWmsOrderDelivery(model);

            return View(model);
        }

        /// <summary>
        /// 24查詢訂單配送與物流下載
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [VbsAuthorize]
        public ActionResult DownloadWmsOrderDeliveryExcel(WmsOrderDeliveryModel model = null)
        {

            Guid bid;
            if (!string.IsNullOrEmpty(model.BID) && !Guid.TryParse(model.BID, out bid))
            {
                return View(model);
            }

            model = GetWmsOrderDelivery(model);

            MemoryStream outputStream = WmsOrderDeliveryExport(model.WmsOrderDeliveryList);

            return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("WmsOrderDelivery_{0}.xls", DateTime.Today.ToString("yyyyMMdd"))) };
        }

        #endregion

        #endregion

        #endregion Action

        #region method

        #region 宅配訂單列表
        [VbsAuthorize]
        public ActionResult ShipOrderStatusList(VbsShipOrderQueryData queryData)
        {
            //17Life內部帳號登入
            if (VbsCurrent.Is17LifeEmployee)
            {
                throw new Exception("這是商家才能操作的功能");
            }
            
            int pageSize = 20;
            queryData.pageIndex = queryData.pageIndex ?? 1;
            int pageIndex = queryData.pageIndex.Value;
            var beginDate = !string.IsNullOrEmpty(queryData.queryBeginDate) ? DateTime.Parse(queryData.queryBeginDate) : DateTime.Now.Date;
            var endDate = !string.IsNullOrEmpty(queryData.queryBeginDate) ? DateTime.Parse(queryData.queryEndDate) : DateTime.Now;
            
            ShipOrderStatusModel model = new ShipOrderStatusModel();
            model.PageSize = pageSize;
            model.Query = queryData;
            //取得訂單列表
            var shipOrderList = new ViewShipOrderStatusCollection();
            if (beginDate.AddMonths(1).Ticks >= endDate.Ticks)
            {
                shipOrderList = _op.ViewShipOrderStatusCollectionGet(
                    ViewShipOrderStatus.Columns.CreateTime + " >= " + beginDate,
                    ViewShipOrderStatus.Columns.CreateTime + " < " + endDate.AddDays(1),
                    !string.IsNullOrEmpty(queryData.queryOrderId) ? ViewShipOrderStatus.Columns.OrderId + " = " + queryData.queryOrderId : "",
                    !string.IsNullOrEmpty(queryData.queryDealId) ? ViewShipOrderStatus.Columns.UniqueId + " = " + queryData.queryDealId : "",
                    ViewShipOrderStatus.Columns.AccountId + " = " + VbsCurrent.AccountId
                    );
            }

            if (shipOrderList.Count > 0)
            {
                var data = new List<ShipOrderInfo>();
                int i = 1;

                foreach (var item in shipOrderList.Skip((pageIndex - 1) * pageSize).Take(pageSize))
                {
                    ShipOrderDetail detail = new ShipOrderDetail();
                    detail.OrderId = item.OrderId;
                    detail.OrderDate = item.CreateTime.ToString("yyyy/MM/dd");
                    detail.MainDealId = (item.MainUniqueId ?? item.UniqueId).ToString();
                    detail.SubDealId = item.UniqueId.ToString();
                    detail.ItemName = item.ItemName.Replace(",", "<br/>");
                    detail.ComboPack = item.ComboPackCount.ToString();
                    detail.OrderSpec = item.OrderSpec;
                    detail.OrderPrice = item.ItemPrice.ToString("C0");
                    detail.Receiver = item.MemberName;
                    detail.ReveiverAddress = item.DeliveryAddress;
                    detail.ReceiverMobile = item.MobileNumber;
                    detail.Log = new List<ShipOrderLog>();
                    var log_list = _op.ViewVendorShipLogCollectionGetByOrderGuid(item.OrderGuid);
                    foreach(var log in log_list)
                    {
                        detail.Log.Add(new ShipOrderLog { 
                            ProcessTime = log.VendorProcessTime.HasValue ? log.VendorProcessTime.Value.ToString("yyyy/MM/dd") : "",
                            ProcessStatus = log.VendorProgressStatus,
                            Memo = log.VendorMemo,
                            Type = log.ShipType,
                            ModifyId = log.ModifyId,
                            ShipCompanyName = log.ShipCompanyName,
                            ShipNo = log.ShipNo,
                        });
                    }

                    data.Add(new ShipOrderInfo
                    {
                        SerNo = i.ToString(),
                        OrderId = item.OrderId,
                        SubDealId = item.MainUniqueId == null ? "--" : item.UniqueId.ToString(),
                        MainDealId = item.MainUniqueId == null ? item.UniqueId.ToString() : item.MainUniqueId.ToString(),
                        OrderDate = item.CreateTime.ToString("yyyy/MM/dd"),
                        ItemName = item.ItemName.Replace(",", "<br/>"),
                        IsShipped = item.IsShipped,
                        ExchangeStatus = item.ExchangeStatus,
                        ReturnStatus = item.VendorProgressStatus,
                        CoustmerOrderId = item.CsmOrderId,
                        DealId = item.BusinessHourGuid,
                        DealType = VbsDealType.Ppon,
                        Detail = detail
                    });
                    i++;
                }

                model.TotalCount = data.Count > 0 ? shipOrderList.Count : 0;
                model.PageCount = shipOrderList.Count % pageSize > 0 ? (shipOrderList.Count / pageSize) + 1 : shipOrderList.Count / pageSize;
                model.ShipOrderInfos = data;

            }

            return View(model);
        }

        [VbsAuthorize]
        public ActionResult ShipOrderDetail(ShipOrderDetail model)
        {
            return PartialView(model);
        }

        
        #endregion

        #region 宅配檔次列表

        private List<VbsShipDealInfo> GetDealListShipPercent(List<VbsShipDealInfo> dealInfos)
        {
            List<Guid> bids = dealInfos.Where(x => x.DealType.Equals(VbsDealType.Ppon))
                                       .Select(x => x.DealId)
                                       .ToList();
            List<int> pids = dealInfos.Where(x => x.DealType.Equals(VbsDealType.PiinLife))
                                      .Select(x => x.DealUniqueId)
                                      .ToList();

            //取得所有檔次之銷售、已出貨、未出貨及退貨數量
            Dictionary<Guid, VendorDealSalesCount> pponDealSaleSummary = null;
            Dictionary<Guid, VendorDealSalesCount> hidealDealSaleSummary = null;
            if (bids.Count > 0)
            {
                pponDealSaleSummary = _vp.GetPponToHouseDealSummary(bids)
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);
            }

            if (pids.Count > 0)
            {
                hidealDealSaleSummary = _vp.GetHidealToHouseDealSummary(pids)
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);
            }

            foreach (var deal in dealInfos)
            {
                //取得單一檔次之銷售、已出貨、未出貨及退貨數量
                VendorDealSalesCount dealSaleSummary = new VendorDealSalesCount();
                int totalSaleCount = 0;
                int shipCount = 0;

                if (deal.DealType.Equals(VbsDealType.Ppon))
                {
                    dealSaleSummary = pponDealSaleSummary.ContainsKey(deal.DealId)
                                        ? pponDealSaleSummary[deal.DealId]
                                        : null;
                }
                else if (deal.DealType.Equals(VbsDealType.PiinLife))
                {
                    dealSaleSummary = hidealDealSaleSummary.ContainsKey(deal.DealId)
                                        ? hidealDealSaleSummary[deal.DealId]
                                        : null;
                }

                if (dealSaleSummary != null)
                {
                    shipCount = dealSaleSummary.VerifiedCount;
                    totalSaleCount = shipCount + dealSaleSummary.UnverifiedCount;
                }

                if (totalSaleCount > 0)
                {
                    deal.ShipPercent = (int)(Math.Round((decimal)shipCount / (decimal)totalSaleCount, 2) * 100); //取百分比
                }
                else
                {
                    deal.ShipPercent = 0;
                }
            }

            return dealInfos;
        }

        #endregion 宅配檔次列表

        #region 檔次資訊(共用)

        /// <summary>
        /// 取得相關檔次
        /// </summary>
        /// <param name="state"></param>
        /// <returns></returns>
        private IEnumerable<VbsShipDealInfo> GetAllowedShipDealList(VbsShipState state, bool containPchome24HShippingOverdue = true)
        {
            var infos = new List<VbsShipDealInfo>();
            var now = DateTime.Today;
            var nowTime = DateTime.Now;
            var allowedDealUnits = new List<IVbsVendorAce>();
            //取得有權限檢視之檔次
            allowedDealUnits = GetAllowedDeals(VbsCurrent.AccountId, null);
            List<Guid> bids = allowedDealUnits.Where(x => x.IsPponDeal.Value).Select(x => x.MerchandiseGuid.Value).Distinct().ToList();
            List<int> pids = allowedDealUnits.Where(x => x.IsPiinDeal.Value).Select(x => x.MerchandiseId.Value).Distinct().ToList();

            //取得所有檔次的回填之出貨回覆日期 判斷檔次為出貨中或出貨結束狀態
            Dictionary<Guid, DealAccounting> pponShippedDateList = null;
            Dictionary<Guid, DealProductDelivery> pponEnableISPList = null;
            Dictionary<Guid, HiDealProduct> hidealShippedDateList = null;
            if (bids.Count > 0)
            {
                pponShippedDateList = _pp.DealAccountingGet(bids)
                                        .ToDictionary(x => x.BusinessHourGuid, x => x);
            }

            if (bids.Count > 0)
            {
                pponEnableISPList = _pp.DealProductDeliveryGetByBid(bids)
                                        .ToDictionary(x => x.BusinessHourGuid, x => x);
            }

            if (pids.Count > 0)
            {
                hidealShippedDateList = _hp.HiDealProductGetList(pids)
                                        .ToDictionary(x => x.Guid, x => x);
            }
            allowedDealUnits = allowedDealUnits.Where(x =>
            {
                DateTime? shippedDate = null;
                DateTime? finalBalanceSheetDate = null;
                
                if (x.IsPponDeal.GetValueOrDefault(false))
                {
                    if (pponShippedDateList.ContainsKey(x.MerchandiseGuid.Value))
                    {
                        shippedDate = pponShippedDateList[x.MerchandiseGuid.Value].ShippedDate;
                        finalBalanceSheetDate = pponShippedDateList[x.MerchandiseGuid.Value].FinalBalanceSheetDate;
                    }
                }
                else if (x.IsPiinDeal.GetValueOrDefault(false))
                {
                    if (hidealShippedDateList.ContainsKey(x.MerchandiseGuid.Value))
                    {
                        shippedDate = hidealShippedDateList[x.MerchandiseGuid.Value].ShippedDate;
                    }
                }

                switch (state)
                {
                    case VbsShipState.ShippingOverdue:
                        return !finalBalanceSheetDate.HasValue &&
                               !shippedDate.HasValue &&
                               DateTime.Compare(x.DealUseEndTime.Value, now) < 0;

                    case VbsShipState.Shipping:
                        return !finalBalanceSheetDate.HasValue &&
                               !shippedDate.HasValue &&
                               DateTime.Compare(x.DealUseEndTime.Value, now) >= 0 &&
                               DateTime.Compare(x.DealUseStartTime.Value, now) <= 0;

                    case VbsShipState.ReturnAndChanging:
                        return shippedDate.HasValue && !finalBalanceSheetDate.HasValue;

                    case VbsShipState.Finished:
                        return finalBalanceSheetDate.HasValue;

                    case VbsShipState.NotYetShip:
                        return !finalBalanceSheetDate.HasValue &&
                               DateTime.Compare(x.DealUseStartTime.Value, now) > 0;

                    case VbsShipState.Processing:
                        return !finalBalanceSheetDate.HasValue &&
                               DateTime.Compare(x.DealUseStartTime.Value, nowTime) <= 0;//先改這個試試
                }

                return false;
            }).ToList();

            if (allowedDealUnits.Any())
            {
                foreach (var deal in allowedDealUnits)
                {
                    //檔次出貨狀態由出貨回覆日是否填寫判定:已填出貨回覆日視為出貨結束 未填則為出貨中
                    VbsShipState shipState = state;
                    bool enabelISP = false;
                    if (state == VbsShipState.Processing)
                    {
                        DateTime? shippedDate = null;
                        DateTime? finalBalanceSheetDate = null;

                        if (deal.IsPponDeal.GetValueOrDefault(false))
                        {
                            if (pponShippedDateList.ContainsKey(deal.MerchandiseGuid.Value))
                            {
                                shippedDate = pponShippedDateList[deal.MerchandiseGuid.Value].ShippedDate;
                                finalBalanceSheetDate = pponShippedDateList[deal.MerchandiseGuid.Value].FinalBalanceSheetDate;
                            }
                        }
                        else if (deal.IsPiinDeal.GetValueOrDefault(false))
                        {
                            if (hidealShippedDateList.ContainsKey(deal.MerchandiseGuid.Value))
                            {
                                shippedDate = hidealShippedDateList[deal.MerchandiseGuid.Value].ShippedDate;
                            }
                        }

                        if (!finalBalanceSheetDate.HasValue)
                        {
                            shipState = shippedDate.HasValue
                                        ? VbsShipState.ReturnAndChanging
                                        : DateTime.Compare(deal.DealUseEndTime.Value, now) < 0
                                            ? VbsShipState.ShippingOverdue
                                            : DateTime.Compare(deal.DealUseStartTime.Value, now) > 0
                                                ? VbsShipState.NotYetShip
                                                : VbsShipState.Shipping;
                        }

                        if (pponEnableISPList.ContainsKey(deal.MerchandiseGuid.Value))
                        {
                            enabelISP = pponEnableISPList[deal.MerchandiseGuid.Value].EnableIsp ?? false;
                        }
                    }
                    int unShipCount = 0;
                    if (VBSFacade.IsEnabledVbsShipRule2k18())
                    {
                        //ViewComboDealCollection vcds = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(deal.MerchandiseGuid.Value);
                        var vdlModel = new ShipOrderListModel();
                        int[] orderUserMemoList = null;
                        int orderUserMemoListCount = 0;
                        int orderUserMemoListCountAll = 0;
                        List<DealSaleInfo> dealInfos = GetDealSaleInfo(deal.MerchandiseGuid.Value).ToList();
                        vdlModel.DealSaleInfo = dealInfos.First(x => x.DealId == deal.MerchandiseGuid.Value);
                        bool isMergeCount = true;
                        var dealIds = dealInfos.Select(x => x.DealUniqueId);

                        var dealSalesSummarys = new Dictionary<Guid, VendorDealSalesCount>();
                        var dealSalesStatistics = new List<DealSalesStatistics>();
                        vdlModel.IsComboDeal = dealInfos.Count() > 1;
                        dealSalesStatistics = GetDealSalesStatistics(
                            deal.MerchandiseGuid.Value,
                            dealInfos,
                            dealSalesSummarys,
                            VbsDealType.Ppon,
                            vdlModel.IsComboDeal,
                            vdlModel.DealSaleInfo.DealUniqueId,
                            ref isMergeCount,
                            ref orderUserMemoList,
                            ref orderUserMemoListCount,
                            ref orderUserMemoListCountAll
                            );
                        unShipCount = dealSalesStatistics.Sum(x => x.UnShipOrderCount);
                    }

                    //24H到貨有逾期出貨中的狀態，就不顯示
                    if(!containPchome24HShippingOverdue && deal.IsWms && shipState == VbsShipState.ShippingOverdue)
                    {
                        continue;
                    }

                    string labelFor24hr = deal.ShipType == (int)DealShipType.Ship72Hrs ? string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(0)) : string.Empty;
                    if (string.IsNullOrEmpty(labelFor24hr))
                    {
                        labelFor24hr = deal.IsWms ? "[24H到貨]" : string.Empty;
                    }
                    var info = new VbsShipDealInfo
                    {

                        DealType = deal.IsPponDeal.GetValueOrDefault(false)
                                    ? VbsDealType.Ppon
                                    : VbsDealType.PiinLife,
                        //抓取24H / 72H到貨 標籤
                        DealName = string.Format("{0}{1}", labelFor24hr, !string.IsNullOrEmpty(deal.AppTitle) ? deal.AppTitle : deal.MerchandiseName),
                        DealId = deal.MerchandiseGuid.Value,
                        DealUniqueId = deal.MerchandiseId.Value,
                        ShipPeriodStartTime = deal.DealUseStartTime.Value,
                        ShipPeriodEndTime = deal.DealUseEndTime.Value,
                        ShipState = shipState,
                        EnableISP = enabelISP,
                        UnShipCount = unShipCount
                    };
                    if (!infos.Select(x => x.DealId).Contains(info.DealId))
                    {
                        infos.Add(info);
                    }
                }
            }
            return infos;
        }

        private IEnumerable<DealSaleInfo> GetDealSaleInfo(Guid dealGuid)
        {
            var dealGuidList = new List<Guid> { dealGuid };
            var subDealIds = GetComboSubDealGuids(dealGuid);
            if (subDealIds != null)
            {
                dealGuidList.AddRange(subDealIds.Keys);
            }

            return DealSaleInfoGet(dealGuidList);
        }

        private IEnumerable<DealSaleInfo> GetDealSaleInfo(IEnumerable<Guid> dealGuids)
        {
            var dealGuidList = new List<Guid>();
            foreach (var dealGuid in dealGuids)
            {
                //有啟用小倉功能，才取得是否小倉檔次
                if (_conf.IsConsignment)
                {
                    bool isConsignment = (bool)_pp.DealPropertyGet(dealGuid).Consignment;
                    //非小倉檔次才add list
                    if (!isConsignment)
                    {
                        dealGuidList.Add(dealGuid);
                        var subDealIds = GetComboSubDealGuids(dealGuid);
                        if (subDealIds != null)
                        {
                            dealGuidList.AddRange(subDealIds.Keys);
                        }
                    }
                }
                else 
                {
                    dealGuidList.Add(dealGuid);
                    var subDealIds = GetComboSubDealGuids(dealGuid);
                    if (subDealIds != null)
                    {
                        dealGuidList.AddRange(subDealIds.Keys);
                    }
                }
            }

            return DealSaleInfoGet(dealGuidList);
        }

        private IEnumerable<DealSaleInfo> DealSaleInfoGet(IEnumerable<Guid> dealGuids)
        {
            var dealSaleInfos = new List<DealSaleInfo>();
            var tempDateTime = DateTime.MinValue;
            var now = DateTime.Today;

            //過濾未達門檻子檔
            var deals = _pp.ViewVbsToHouseDealGetListByProductGuid(dealGuids);
                            //.Where(x =>
                            //    {
                            //        if (!x.DealStartTime.HasValue || !x.DealEndTime.HasValue ||
                            //            !x.UseStartTime.HasValue || !x.UseEndTime.HasValue) //不撈取未設定販賣時間或兌換時間之異常檔次
                            //        {
                            //            return false;
                            //        }
                            //        if (x.DealEstablished.HasValue)
                            //        {
                            //            return int.Equals(1, x.DealEstablished); //過門檻
                            //        }

                            //        return true;
                            //    });
                            //前面就篩選過了應該不用再跑一次
                        
            foreach (var deal in deals)
            {
                string labelFor24hr = deal.ShipType == (int)DealShipType.Ship72Hrs ? string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(0)) : string.Empty;
                var dealSaleInfo = new DealSaleInfo
                {
                    DealId = deal.ProductGuid,
                    DealType = (VbsDealType)deal.DealType,
                    //抓取24H / 72H到貨 標籤
                    DealName = string.Format("{0}{1}", labelFor24hr, !string.IsNullOrEmpty(deal.AppTitle) ? deal.AppTitle : deal.ProductName),
                    DealUniqueId = deal.ProductId,
                    DealOrderStartTime = deal.DealStartTime ?? tempDateTime,
                    DealOrderEndTime = deal.DealEndTime ?? tempDateTime,
                    ShipPeriodStartTime = deal.UseStartTime ?? tempDateTime,
                    ShipPeriodEndTime = deal.UseEndTime ?? tempDateTime,
                    //成套販售倍數(若 >1 視為成套販售檔次)
                    ComboPackCount = deal.ComboPackCount ?? 1,
                    DealEndSalesCount = deal.Slug,
                    DealLabelDesc = labelFor24hr.Replace("[", "").Replace("]", ""),                   
                };

                VbsShipState shipState;
                if (deal.ShippedDate.HasValue)
                {
                    shipState = deal.FinalBalanceSheetDate.HasValue
                                    ? VbsShipState.Finished
                                    : VbsShipState.ReturnAndChanging;
                }
                else
                {
                    shipState = DateTime.Compare(deal.UseEndTime.Value, now) < 0
                                    ? VbsShipState.ShippingOverdue
                                    : DateTime.Compare(deal.UseStartTime.Value, now) > 0
                                        ? VbsShipState.NotYetShip
                                        : VbsShipState.Shipping;
                }
                dealSaleInfo.ShipState = shipState;

                dealSaleInfos.Add(dealSaleInfo);
            }

            return dealSaleInfos;
        }

        private Dictionary<Guid, int> GetComboSubDealGuids(Guid id)
        {
            //撈取多檔次之子檔bid, unique_id
            //var subDeals = _pp.ViewPponDealGetListByMainBid(id);
            var subDeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(id);
            if (subDeals.Any())
            {
                //return subDeals.Where(x => x.UniqueId.HasValue)
                //               .ToDictionary(x => x.BusinessHourGuid,
                //                             x => x.UniqueId.Value);
                return subDeals.ToDictionary(x => x.BusinessHourGuid,
                                             x => x.UniqueId);
            }

            return null;
        }

        #endregion 檔次資訊(共用)

        #region 訂單資訊(共用)

        private static PagerList<VbsOrderListInfo> GetPagerVbsOrderShipInfos(
           IEnumerable<VbsOrderListInfo> infos, VbsOrderShipState? orderShipState, int pageSize,
           int pageIndex, string sortCol, bool sortDesc, bool? hasOrderShip,
           ViewVendorProgressState? viewVendorReturnProgressState,
           bool isIspOrder, bool containPchomeWmsOrder = true)
        {
            var query = (from t in infos select t);

            if (orderShipState == VbsOrderShipState.Shipped)
            {
                query = query.Where(t => t.OrderShipState == VbsOrderShipState.Shipped ||
                                         t.OrderShipState == VbsOrderShipState.ShippedAndPartialCancel).ToList();
            }
            else if (orderShipState == VbsOrderShipState.UnShip)
            {
                query = query.Where(t => t.OrderShipState == VbsOrderShipState.UnShip ||
                                         t.OrderShipState == VbsOrderShipState.UnShipAndPartialCancel).ToList();
            }
            else if (orderShipState == VbsOrderShipState.Return)
            {
                query = query.Where(t => t.OrderShipState == VbsOrderShipState.Return ||
                                         t.OrderShipState == VbsOrderShipState.ReturnAfterDealClose ||
                                         t.OrderShipState == VbsOrderShipState.ReturnBeforeDealClose).ToList();
            }
            else if (orderShipState.HasValue)
            {
                query = query.Where(t => t.OrderShipState == orderShipState.Value).ToList();
            }

            if (hasOrderShip.HasValue)
            {
                query = query.Where(t => t.HasOrderShip.Equals(hasOrderShip.Value)).ToList();
            }

            if (viewVendorReturnProgressState.HasValue)
            {
                query = query.Where(t => t.VendorReturnProgressState == viewVendorReturnProgressState.Value).ToList();
            }

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case VbsOrderListInfo.Columns.OrderId:
                        query = query.OrderBy(t => t.OrderId);
                        break;

                    case VbsOrderListInfo.Columns.ItemDescription:
                        query = query.OrderBy(t => t.ItemDescription);
                        break;

                    case VbsOrderListInfo.Columns.RecipientName:
                        query = query.OrderBy(t => t.RecipientName);
                        break;

                    case VbsOrderListInfo.Columns.RecipientTel:
                        query = query.OrderBy(t => t.RecipientTel);
                        break;

                    case VbsOrderListInfo.Columns.RecipientAddress:
                        query = query.OrderBy(t => t.RecipientAddress);
                        break;

                    case VbsOrderListInfo.Columns.OrderShipState:
                        query = query.OrderBy(t => t.OrderShipState);
                        break;

                    case VbsOrderListInfo.Columns.OrderCreateTime:
                        query = query.OrderBy(t => t.OrderCreateTime);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case VbsOrderListInfo.Columns.OrderId:
                        query = query.OrderByDescending(t => t.OrderId);
                        break;

                    case VbsOrderListInfo.Columns.ItemDescription:
                        query = query.OrderByDescending(t => t.ItemDescription);
                        break;

                    case VbsOrderListInfo.Columns.RecipientName:
                        query = query.OrderByDescending(t => t.RecipientName);
                        break;

                    case VbsOrderListInfo.Columns.RecipientTel:
                        query = query.OrderByDescending(t => t.RecipientTel);
                        break;

                    case VbsOrderListInfo.Columns.RecipientAddress:
                        query = query.OrderByDescending(t => t.RecipientAddress);
                        break;

                    case VbsOrderListInfo.Columns.OrderShipState:
                        query = query.OrderByDescending(t => t.OrderShipState);
                        break;

                    default:
                        break;
                }
            }

            List<Guid> opdGuids = _op.OrderProductDeliveryGetListByOrderGuid(query.Select(x => x.OrderGuid).ToList())
                                                        .Where(y => (y.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup) || (y.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup))
                                                        .Select(t => t.OrderGuid).ToList();

            //var ispOids = query.Where(x => x.OrderStatus != -1 && !opdGuids.Contains(x.OrderGuid)).Select(y=>y.OrderGuid).Distinct().ToList();
            if (isIspOrder)
            {
                query = query.Where(x => opdGuids.Contains(x.OrderGuid)).ToList();
            }
            else
            {
                query = query.Where(x => !opdGuids.Contains(x.OrderGuid)).ToList();
            }

            List<Guid> pchomeWmsOrderGuids = _op.OrderProductDeliveryGetListByOrderGuid(query.Select(x => x.OrderGuid).ToList())
                                                        .Where(y => (y.ProductDeliveryType == (int)ProductDeliveryType.Wms))
                                                        .Select(t => t.OrderGuid).ToList();

            if (!containPchomeWmsOrder)
            {
                query = query.Where(x => !pchomeWmsOrderGuids.Contains(x.OrderGuid)).ToList();
            }

            var paging = new PagerList<VbsOrderListInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.ToList().Count);

            return paging;
        }


        private List<VbsOrderListInfo> GetOrderShipList(IEnumerable<DealSaleInfo> dealInfos, bool isHiddenPersonalInfo, string selQueryOption, string queryKeyWord)
        {
            var dealIds = dealInfos.Select(x => x.DealUniqueId);
            var dealGuids = dealInfos.Select(x => x.DealId);
            var type = dealInfos.Select(x => x.DealType).First();
            //抓取訂單商品規格
            List<ViewOrderProductOptionList> orderProductOptionlists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds);
            Dictionary<Guid, string> orderItemLists = _op.GetOrderProductOptionInfo(orderProductOptionlists);
            var orderShipList = new List<ViewOrderShipList>();
            //撈取該檔次訂單之cash_trust_log(抓取trust_id 以抓取cash_trust_status_log)
            ILookup<Guid, CashTrustLog> cashTrustLogs = null;

            if (type == VbsDealType.Ppon)
            {
                cashTrustLogs = _mp.CashTrustLogGetListByBid(dealGuids)
                                    .ToLookup(x => x.OrderGuid);
            }
            else if (type == VbsDealType.PiinLife)
            {
                var productId = dealIds.First();
                cashTrustLogs = _mp.CashTrustLogGetAllListByHiDealProductId(productId)
                                    .ToLookup(x => x.OrderGuid);
            }

            if (!string.IsNullOrEmpty(queryKeyWord))
            {
                orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids).ToList();
                switch (selQueryOption)
                {
                    case "orderId":
                        orderShipList = orderShipList.Where(x => x.OrderId == queryKeyWord)
                                                     .OrderBy(x => x.OrderId)
                                                     .ToList();
                        break;
                    case "recipientName":
                        orderShipList = orderShipList.Where(x => x.MemberName.Contains(queryKeyWord))
                                                     .OrderBy(x => x.OrderId)
                                                     .ToList();
                        break;
                    case "recipientTel":
                        orderShipList = orderShipList.Where(x => x.PhoneNumber.Contains(queryKeyWord))
                                                     .OrderBy(x => x.OrderId)
                                                     .ToList();
                        break;
                    default:
                        break;
                }
            }

            //抓取訂單備註異動紀錄list
            var orderUserMemoLists = _op.OrderUserMemoListGetList(orderShipList.Select(x => x.OrderGuid).Distinct().ToList())
                                        .GroupBy(x => x.OrderGuid)
                                        .ToDictionary(x => x.Key, x => x.ToList());
            //抓取換貨記錄
            var exchangeLogInfos = GetHasExchangeLogOrderInfos(dealGuids);

            var infos = new List<VbsOrderListInfo>();
            if (orderShipList.Count > 0)
            {
                var orderShipState = VbsOrderShipState.UnShip;
                foreach (var order in orderShipList)
                {
                    var orderUserMemoInfos = new List<OrderUserMemoInfo>();
                    var dealInfo = dealInfos.FirstOrDefault(x => x.DealId == order.ProductGuid);
                    if (dealInfo == null)
                    {
                        continue;
                    }

                    //若對應不到cash_trust_log資料 則不顯示該筆訂單
                    if (!cashTrustLogs.Contains(order.OrderGuid))
                    {
                        continue;
                    }

                    if (cashTrustLogs[order.OrderGuid].Any(x => x.Status == (int)TrustStatus.Initial ||
                                                                x.Status == (int)TrustStatus.Trusted))
                    {
                        orderShipState = VbsOrderShipState.UnShip;
                    }
                    else if (cashTrustLogs[order.OrderGuid].Any(x => x.Status == (int)TrustStatus.Verified))
                    {
                        orderShipState = VbsOrderShipState.Shipped;
                    }
                    else if (cashTrustLogs[order.OrderGuid].Any(x => x.Status == (int)TrustStatus.Refunded ||
                                                                     x.Status == (int)TrustStatus.Returned))
                    {
                        //出貨管理顯示的訂單不包含退貨
                        continue;
                    }

                    if (orderUserMemoLists.ContainsKey(order.OrderGuid))
                    {
                        orderUserMemoInfos.AddRange(orderUserMemoLists[order.OrderGuid].Select(userMemo =>
                                                                new OrderUserMemoInfo
                                                                {
                                                                    CreateTime = userMemo.CreateTime,
                                                                    UserMemo = userMemo.UserMemo
                                                                }));
                    }
                    //檔次出貨狀態為已完成 隱藏消費者個資
                    if (isHiddenPersonalInfo)
                    {
                        order.MemberName = "***";
                        order.PhoneNumber = "******";
                        order.DeliveryAddress = "*********";
                    }

                    var info = new VbsOrderListInfo
                    {
                        DealInfo = dealInfo,
                        OrderGuid = order.OrderGuid,
                        OrderId = order.OrderId,
                        ItemDescription = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid] : "",
                        RecipientName = order.MemberName,
                        RecipientTel = order.PhoneNumber,
                        RecipientAddress = order.DeliveryAddress,
                        RecipientRemark = order.UserMemo,
                        OrderShipId = order.OrderShipId,
                        ShipCompanyId = order.ShipCompanyId,
                        ShipCompanyName = order.ShipCompanyName,
                        ShipTime = order.ShipTime,
                        ShipNo = order.ShipNo,
                        ShipMemo = order.ShipMemo,
                        OrderShipState = orderShipState,
                        HasOrderShip = order.ShipTime.HasValue,
                        OrderUserMemoInfos = orderUserMemoInfos.OrderByDescending(t => t.CreateTime)
                                                               .ToList(),
                        HasExchangeLog = exchangeLogInfos.Contains(order.OrderGuid)

                    };

                    infos.Add(info);
                }
            }

            return infos;
        }

        #endregion 訂單資訊(共用)

        #region 共用method

        /// <summary>
        /// 快速出貨條件說明
        /// </summary>
        /// <param name="labelIcons"></param>
        /// <returns></returns>
        private string GetLableDesc(string labelIcons)
        {
            string labelDesc = string.Empty;
            if (!string.IsNullOrEmpty(labelIcons))
            {
                List<int> labelIconList = new List<int>(labelIcons.Split(',').Select(int.Parse));
                List<int> arriveList = new List<int> { (int)DealLabelSystemCode.ArriveIn24Hrs, (int)DealLabelSystemCode.ArriveIn72Hrs };

                foreach (int label in labelIconList.Where(i => arriveList.Contains(i)))
                {
                    labelDesc += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(label));
                }
            }

            return labelDesc;
        }

        /// <summary>
        /// 一般出貨條件說明
        /// </summary>
        /// <returns></returns>
        private static string GetNormalLableDesc(DateTime dateS, DateTime dateE, int startDay, int? endDay, VacationCollection recentHolidays)
        {
            string labelDesc = string.Empty;
            string labelDescRange = "請於{0} - {1}出貨完畢";
            string labelDescSingle = "請於{0}前出貨完畢";

            DateTime startDate = PponExpandEndTime.IncrDaysExceptVocations(dateS, startDay, recentHolidays);

            if (endDay != null)
            {
                DateTime endDate = PponExpandEndTime.IncrDaysExceptVocations(dateE, (int)endDay, recentHolidays);
                labelDesc = string.Format(labelDescRange, startDate.Date.ToString("yyyy/MM/dd"), endDate.Date.ToString("yyyy/MM/dd"));
            }
            else
            {
                labelDesc = string.Format(labelDescSingle, startDate.Date.ToString("yyyy/MM/dd"));
            }

            return labelDesc;
        }

        #endregion 訂單資訊(共用)

        #region 出貨管理 / 線上清冊 / 備料統計

        /// <summary>
        /// 舊版匯出清冊格式
        /// </summary>
        /// <param name="orderInfos"></param>
        /// <param name="inventoryType"></param>
        /// <returns></returns>
        private static MemoryStream ShipProductInventoryExport(IEnumerable<VbsOrderListInfo> orderInfos, InventoryType inventoryType)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            DataFormat formatDate = workbook.CreateDataFormat();
            cellStyle = SetCellBackgroudColor(cellStyle, HSSFColor.LIGHT_YELLOW.index);
            cellStyle.DataFormat = formatDate.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle = SetCellBackgroudColor(cellYStyle, HSSFColor.LIGHT_YELLOW.index);

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle = SetCellBackgroudColor(cellCStyle, HSSFColor.LIGHT_YELLOW.index);
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellMStyle = workbook.CreateCellStyle();
            cellMStyle = SetCellBackgroudColor(cellMStyle, HSSFColor.LIGHT_TURQUOISE.index);

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font fontRB = workbook.CreateFont();
            fontRB.Color = HSSFColor.RED.index;
            fontRB.Boldweight = (short)FontBoldWeight.BOLD;
            cellBStyle.SetFont(fontRB);
            cellBStyle.WrapText = true;

            CellStyle cellUStyle = workbook.CreateCellStyle();
            cellUStyle = SetCellBackgroudColor(cellUStyle, HSSFColor.LIGHT_GREEN.index);
            cellUStyle.WrapText = true;

            CellStyle cellRStyle = workbook.CreateCellStyle();
            Font fontR = workbook.CreateFont();
            fontR.Color = HSSFColor.RED.index;
            cellRStyle.SetFont(fontR);
            cellRStyle.WrapText = true;


            #endregion  格式設定

            #region sheet1 清冊

            Sheet sheet = workbook.CreateSheet("清冊");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);

            int hRowIdx = 0;
            cols.CreateCell(hRowIdx).SetCellValue("檔號");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單編號");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("購買日期");
            if (VBSFacade.IsEnabledVbsShipRule2k18())
            {
                hRowIdx++;
                cols.CreateCell(hRowIdx).SetCellValue("最晚出貨日");
            }            
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("收件人");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("收件人電話");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("配送地址");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("方案名稱");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("組數");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("品項規格");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("數量");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("應出貨品項規格");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("應出貨數");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單備註");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單狀態");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("退貨管理");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("出貨日期");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("物流代號");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("物流公司名稱");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("貨運單號");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("出貨備註(非必填,最多30字)");
            cols.GetCell(hRowIdx).CellStyle = cellMStyle;
            //if (VBSFacade.IsEnabledVbsShipRule2k18() == false)
            //{
                //刪除出貨條件欄位
                hRowIdx++;
                cols.CreateCell(hRowIdx).SetCellValue("出貨條件");
                cols.GetCell(hRowIdx).CellStyle = cellRStyle;
            //}
           

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState;
            string returnProcessState;

            foreach (var item in orderInfos)
            {
                Row row = sheet.CreateRow(rowIdx);

                switch (item.OrderShipState)
                {
                    case VbsOrderShipState.Shipped:
                        orderShipState = "已出貨";
                        break;

                    case VbsOrderShipState.ShippedAndPartialCancel:
                        orderShipState = "已出貨-部分退貨";
                        break;

                    case VbsOrderShipState.UnShip:
                        orderShipState = "未出貨";
                        break;

                    case VbsOrderShipState.UnShipAndPartialCancel:
                        orderShipState = "未出貨-部分退貨";
                        break;

                    case VbsOrderShipState.ReturnBeforeDealClose:
                        orderShipState = "結檔前退貨";
                        break;

                    case VbsOrderShipState.ReturnAfterDealClose:
                        orderShipState = "結檔後退貨";
                        break;

                    default:
                        orderShipState = string.Empty;
                        break;
                }

                bool isReturnMark = false;
                switch (item.VendorReturnProgressState)
                {
                    case ViewVendorProgressState.Process:
                        returnProcessState = VBSFacade.IsEnabledVbsShipRule2k18() ? "退貨中，請勿出貨" : "待處理";
                        isReturnMark = true;
                        break;

                    default:
                        returnProcessState = string.Empty;
                        break;
                }

                bool isUserMemoMark = false;
                string orderUserMemo = string.Empty;
                if (item.OrderUserMemoInfos.Any())
                {
                    isUserMemoMark = true;
                    orderUserMemo = string.Join("\n", item.OrderUserMemoInfos
                                                            .OrderBy(x => x.CreateTime)
                                                            .Select(x => x.CreateTime.ToString("yyyy/MM/dd") + "  " + x.UserMemo));
                }

                int itemRowIdx = 0;
                row.CreateCell(itemRowIdx).SetCellValue(item.DealInfo.DealUniqueId.ToString(CultureInfo.InvariantCulture));
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.OrderId);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                if (VBSFacade.IsEnabledVbsShipRule2k18())
                {
                    itemRowIdx++;
                    ViewCouponListMain main = _mp.GetCouponListMainByOid(item.OrderGuid);
                    BizLogic.Models.ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                    string lastShippingDate = string.IsNullOrEmpty(shipInfo.LastShippingDate) ? string.Empty : shipInfo.LastShippingDate;
                    row.CreateCell(itemRowIdx).SetCellValue(lastShippingDate); //最晚出貨日
                }
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientName);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientTel);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientAddress);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.DealInfo.DealName);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.PackCount);
                //品項規格
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ItemDescription);
                //依照列數調整列高
                int itemDescWarpCount = Regex.Matches(item.ItemDescription, "\n").Count;
                row.HeightInPoints = Convert.ToSingle((itemDescWarpCount + 1) * sheet.DefaultRowHeight / 20);

                //數量
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ItemCount);
                //應出貨品項規格
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipItemDescription);
                //應出貨數量
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipItemCount);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(orderUserMemo);
                //訂單備註 依照列數調整列高
                row.GetCell(itemRowIdx).CellStyle = cellWStyle;
                int userMemoWarpCount = Regex.Matches(orderUserMemo, "\n").Count;
                if (userMemoWarpCount > itemDescWarpCount)
                {
                    row.HeightInPoints = Convert.ToSingle((userMemoWarpCount + 1) * sheet.DefaultRowHeight / 20);
                }
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(orderShipState);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(returnProcessState);
                if (isReturnMark)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellBStyle;
                }
                itemRowIdx++;
                if (item.ShipTime.HasValue)
                {
                    row.CreateCell(itemRowIdx).SetCellValue(item.ShipTime.Value.ToShortDateString());
                }
                else
                {
                    row.CreateCell(itemRowIdx).SetCellValue(string.Empty);
                }
                row.GetCell(itemRowIdx).CellStyle = cellStyle;

                itemRowIdx++;
                if (item.ShipCompanyId.HasValue)
                {
                    row.CreateCell(itemRowIdx).SetCellValue(item.ShipCompanyId.Value);
                }
                else
                {
                    row.CreateCell(itemRowIdx).SetCellValue(string.Empty);
                }
                row.GetCell(itemRowIdx).CellStyle = cellYStyle;

                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipCompanyName);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipNo);
                row.GetCell(itemRowIdx).CellStyle = cellCStyle;
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipMemo);
                row.GetCell(itemRowIdx).CellStyle = cellMStyle;
                //if(VBSFacade.IsEnabledVbsShipRule2k18() == false)
                //{
                    //刪除出貨條件欄位
                    itemRowIdx++;
                    row.CreateCell(itemRowIdx).SetCellValue(item.DealInfo.DealLabelDesc);
                    row.GetCell(itemRowIdx).CellStyle = cellRStyle;
                //}
                

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 抓取最長長度) 英數字長度*1 中文字長度*2
                    int length = column.ToString().Split("\n").Select(x => System.Text.Encoding.Default.GetBytes(x))
                                                              .Select(x => x.Length)
                                                              .Concat(new[] { 0 })
                                                              .Max() + 2;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }

                    if (column.ColumnIndex < 14)
                    {
                        column.CellStyle = isUserMemoMark
                                            ? cellUStyle
                                            : cellWStyle;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            #endregion sheet1 清冊

            #region sheet2 物流公司對照表

            sheet = workbook.CreateSheet("物流公司對照表");
            cols = sheet.CreateRow(0);

            cols = sheet.CreateRow(1);
            cols.CreateCell(0).SetCellValue("物流代號");
            cols.CreateCell(1).SetCellValue("物流公司名稱");
            rowIdx = 2;
            i = 1;

            foreach (var item in _op.ShipCompanyGetList(null).Where(x => x.Status == true).Select(x => new { Id = x.Id, ShipCompanyName = x.ShipCompanyName }).OrderBy(x => x.Id).ToList())
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.Id);
                row.CreateCell(1).SetCellValue(item.ShipCompanyName);

                rowIdx++;
                i++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);

            #endregion sheet2 物流公司對照表

            //使用非同步寫入該檔次清冊匯出紀錄
            List<Guid> orderGuids = orderInfos.Select(x => x.OrderGuid).Distinct().ToList();
            SetOrderProductInventoryExportLog(orderGuids, VbsCurrent.AccountId, inventoryType);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 新版匯出清冊格式
        /// </summary>
        /// <param name="orderInfos"></param>
        /// <param name="inventoryType"></param>
        /// <returns></returns>
        private static MemoryStream NewShipProductInventoryExport(IEnumerable<VbsOrderListInfo> orderInfos, InventoryType inventoryType)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            DataFormat formatDate = workbook.CreateDataFormat();
            cellStyle = SetCellBackgroudColor(cellStyle, HSSFColor.LIGHT_YELLOW.index);
            cellStyle.DataFormat = formatDate.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle = SetCellBackgroudColor(cellYStyle, HSSFColor.LIGHT_YELLOW.index);

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle = SetCellBackgroudColor(cellCStyle, HSSFColor.LIGHT_YELLOW.index);
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellMStyle = workbook.CreateCellStyle();
            cellMStyle = SetCellBackgroudColor(cellMStyle, HSSFColor.LIGHT_TURQUOISE.index);

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font fontRB = workbook.CreateFont();
            fontRB.Color = HSSFColor.RED.index;
            fontRB.Boldweight = (short)FontBoldWeight.BOLD;
            cellBStyle.SetFont(fontRB);
            cellBStyle.WrapText = true;

            CellStyle cellUStyle = workbook.CreateCellStyle();
            cellUStyle = SetCellBackgroudColor(cellUStyle, HSSFColor.LIGHT_GREEN.index);
            cellUStyle.WrapText = true;

            CellStyle cellRStyle = workbook.CreateCellStyle();
            Font fontR = workbook.CreateFont();
            fontR.Color = HSSFColor.RED.index;
            cellRStyle.SetFont(fontR);
            cellRStyle.WrapText = true;


            #endregion  格式設定

            #region sheet1 清冊

            Sheet sheet = workbook.CreateSheet("清冊");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);

            int hRowIdx = 0;
            cols.CreateCell(hRowIdx).SetCellValue("檔號");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單編號");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("購買日期");
            if (VBSFacade.IsEnabledVbsShipRule2k18())
            {
                hRowIdx++;
                cols.CreateCell(hRowIdx).SetCellValue("最晚出貨日");
            }            
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("收件人");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("收件人電話");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("配送地址");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("方案名稱");
            //↓new var columns↓
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("應出貨品項規格");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("貨號");
            if (VBSFacade.IsEnabledVbsShipRule2k18())
            {
                //hRowIdx++;
                //cols.CreateCell(hRowIdx).SetCellValue("訂購人");
            }            
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("應出貨數");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("售價");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("進貨價");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("運費");
            //↑new var columns↑
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單備註");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("訂單狀態");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("退貨管理");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("出貨日期");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("物流代號");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("物流公司名稱");
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("貨運單號");
            cols.GetCell(hRowIdx).CellStyle = cellYStyle;
            hRowIdx++;
            cols.CreateCell(hRowIdx).SetCellValue("出貨備註(非必填,最多30字)");
            cols.GetCell(hRowIdx).CellStyle = cellMStyle;
            //if (VBSFacade.IsEnabledVbsShipRule2k18() == false)
            //{
                //移除此欄位
                hRowIdx++;
                cols.CreateCell(hRowIdx).SetCellValue("出貨條件");
                cols.GetCell(hRowIdx).CellStyle = cellRStyle;
            //}
            

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState;
            string returnProcessState;
            //code 0 = 24hr出貨
            string labelFor24hr = string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(0));
            bool isDealRow = true;
            VacationCollection recentHolidays = _pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-14), DateTime.Now.AddMonths(14));

            foreach (var item in orderInfos)
            {
                isDealRow = string.IsNullOrEmpty(item.ShipItemDescription) &&
                    string.IsNullOrEmpty(item.ItemNo) &&
                    string.IsNullOrEmpty(item.ShipItemCount);

                Row row = sheet.CreateRow(rowIdx);

                #region before process
                switch (item.OrderShipState)
                {
                    case VbsOrderShipState.Shipped:
                        orderShipState = "已出貨";
                        break;

                    case VbsOrderShipState.ShippedAndPartialCancel:
                        orderShipState = "已出貨-部分退貨";
                        break;

                    case VbsOrderShipState.UnShip:
                        orderShipState = "未出貨";
                        break;

                    case VbsOrderShipState.UnShipAndPartialCancel:
                        orderShipState = "未出貨-部分退貨";
                        break;

                    case VbsOrderShipState.ReturnBeforeDealClose:
                        orderShipState = "結檔前退貨";
                        break;

                    case VbsOrderShipState.ReturnAfterDealClose:
                        orderShipState = "結檔後退貨";
                        break;

                    default:
                        orderShipState = string.Empty;
                        break;
                }

                bool isReturnMark = false;
                switch (item.VendorReturnProgressState)
                {
                    case ViewVendorProgressState.Process:
                        returnProcessState = VBSFacade.IsEnabledVbsShipRule2k18() ? "退貨中，請勿出貨" : "待處理";
                        isReturnMark = true;
                        break;

                    default:
                        returnProcessState = string.Empty;
                        break;
                }
                #endregion

                bool isUserMemoMark = false;
                string orderUserMemo = string.Empty;
                if (item.OrderUserMemoInfos.Any())
                {
                    isUserMemoMark = true;
                    orderUserMemo = string.Join("\n", item.OrderUserMemoInfos
                                                            .OrderBy(x => x.CreateTime)
                                                            .Select(x => x.CreateTime.ToString("yyyy/MM/dd") + "  " + x.UserMemo));
                }

                int itemRowIdx = 0;
                row.CreateCell(itemRowIdx).SetCellValue(item.DealInfo.DealUniqueId.ToString(CultureInfo.InvariantCulture));
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.OrderId);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                if (VBSFacade.IsEnabledVbsShipRule2k18())
                {
                    itemRowIdx++;
                    ViewCouponListMain main = _mp.GetCouponListMainByOid(item.OrderGuid);
                    BizLogic.Models.ShipInfo shipInfo = OrderFacade.GetOrderShipInfo(main);
                    string lastShippingDate = string.IsNullOrEmpty(shipInfo.LastShippingDate) ? string.Empty : shipInfo.LastShippingDate;
                    row.CreateCell(itemRowIdx).SetCellValue(lastShippingDate);//最晚出貨日
                }
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientName);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientTel);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.RecipientAddress);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.DealInfo.DealName.ReplaceIgnoreCase(labelFor24hr, string.Empty));
                //應出貨品項規格
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipItemDescription);
                //貨號
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ItemNo);
                if (VBSFacade.IsEnabledVbsShipRule2k18())
                {
                    //訂購人
                    //itemRowIdx++;
                    //row.CreateCell(itemRowIdx).SetCellValue(item.OrderName);
                }
                //應出貨數量
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipItemCount);
                //售價
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(Convert.ToInt32(item.ItemPrice));
                //進貨價
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(Convert.ToInt32(item.ItemCost));
                //運費
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.FreightAmount);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(orderUserMemo);
                //訂單備註 依照列數調整列高
                row.GetCell(itemRowIdx).CellStyle = cellWStyle;
                int userMemoWarpCount = Regex.Matches(orderUserMemo, "\n").Count;
                row.HeightInPoints = Convert.ToSingle((userMemoWarpCount + 1) * sheet.DefaultRowHeight / 20);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(isDealRow ? orderShipState : string.Empty);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(returnProcessState);
                if (isReturnMark)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellBStyle;
                }
                itemRowIdx++;
                if (item.ShipTime.HasValue)
                {
                    row.CreateCell(itemRowIdx).SetCellValue(item.ShipTime.Value.ToShortDateString());
                }
                else
                {
                    row.CreateCell(itemRowIdx).SetCellValue(string.Empty);
                }
                if (isDealRow)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellStyle;
                }
                itemRowIdx++;
                if (item.ShipCompanyId.HasValue)
                {
                    row.CreateCell(itemRowIdx).SetCellValue(item.ShipCompanyId.Value);
                }
                else
                {
                    row.CreateCell(itemRowIdx).SetCellValue(string.Empty);
                }
                if (isDealRow)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellYStyle;
                }
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipCompanyName);
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipNo);
                if (isDealRow)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellCStyle;
                }
                itemRowIdx++;
                row.CreateCell(itemRowIdx).SetCellValue(item.ShipMemo);
                if (isDealRow)
                {
                    row.GetCell(itemRowIdx).CellStyle = cellMStyle;
                }

                //處理出貨條件
                var dp = _pp.DealPropertyGet(item.DealInfo.DealId);
                var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.DealInfo.DealId);
                //重新判斷出貨類型(改成不看icon標籤)
                if (isDealRow)
                {
                    if (vpd.ShipType == (int)DealShipType.Normal)
                    {
                        if (dp.ShippingdateType == (int)DealShippingDateType.Normal)
                        {
                            item.DealInfo.DealLabelDesc = GetNormalLableDesc(item.OrderCreateTime, item.OrderCreateTime, dp.Shippingdate ?? 0, null, recentHolidays);
                        }
                        else if (dp.ShippingdateType == (int)DealShippingDateType.Special)
                        {
                            item.DealInfo.DealLabelDesc = GetNormalLableDesc(vpd.BusinessHourOrderTimeS, vpd.BusinessHourOrderTimeE, dp.ProductUseDateStartSet ?? 0, dp.ProductUseDateEndSet, recentHolidays);
                        }
                    }
                    else if (dp.ShipType == (int)DealShipType.Ship72Hrs)
                    {
                        item.DealInfo.DealLabelDesc = labelFor24hr;
                    }
                    else
                    {
                        item.DealInfo.DealLabelDesc = string.Empty;
                    }
                }
                //if (VBSFacade.IsEnabledVbsShipRule2k18() == false)
                //{
                    //移除此欄位 - 出貨條件
                    itemRowIdx++;
                    row.CreateCell(itemRowIdx).SetCellValue(isDealRow ? item.DealInfo.DealLabelDesc : string.Empty);
                    row.GetCell(itemRowIdx).CellStyle = cellRStyle;
                //}
                

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 抓取最長長度) 英數字長度*1 中文字長度*2
                    int length = column.ToString().Split("\n").Select(x => System.Text.Encoding.Default.GetBytes(x))
                                                              .Select(x => x.Length)
                                                              .Concat(new[] { 0 })
                                                              .Max() + 2;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }

                    if (column.ColumnIndex < 14)
                    {
                        column.CellStyle = isUserMemoMark
                                            ? cellUStyle
                                            : cellWStyle;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            #endregion sheet1 清冊

            #region sheet2 物流公司對照表

            sheet = workbook.CreateSheet("物流公司對照表");
            cols = sheet.CreateRow(0);

            cols = sheet.CreateRow(1);
            cols.CreateCell(0).SetCellValue("物流代號");
            cols.CreateCell(1).SetCellValue("物流公司名稱");
            rowIdx = 2;
            i = 1;

            foreach (var item in _op.ShipCompanyGetList(null).Where(x => x.Status == true).Select(x => new { Id = x.Id, ShipCompanyName = x.ShipCompanyName }).OrderBy(x => x.Id).ToList())
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.Id);
                row.CreateCell(1).SetCellValue(item.ShipCompanyName);

                rowIdx++;
                i++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);

            #endregion sheet2 物流公司對照表

            //使用非同步寫入該檔次清冊匯出紀錄
            List<Guid> orderGuids = orderInfos.Select(x => x.OrderGuid).Distinct().ToList();
            SetOrderProductInventoryExportLog(orderGuids, VbsCurrent.AccountId, inventoryType, true);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        /// <summary>
        /// 超取匯出清冊格式
        /// </summary>
        /// <param name="orderInfos"></param>
        /// <param name="inventoryType"></param>
        /// <returns></returns>
        private static MemoryStream IspShipProductInventoryExport(IEnumerable<VbsOrderListInfo> orderInfos, InventoryType inventoryType)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            DataFormat formatDate = workbook.CreateDataFormat();
            cellStyle = SetCellBackgroudColor(cellStyle, HSSFColor.LIGHT_YELLOW.index);
            cellStyle.DataFormat = formatDate.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle = SetCellBackgroudColor(cellYStyle, HSSFColor.LIGHT_YELLOW.index);

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle = SetCellBackgroudColor(cellCStyle, HSSFColor.LIGHT_YELLOW.index);
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellMStyle = workbook.CreateCellStyle();
            cellMStyle = SetCellBackgroudColor(cellMStyle, HSSFColor.LIGHT_TURQUOISE.index);

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font fontRB = workbook.CreateFont();
            fontRB.Color = HSSFColor.RED.index;
            fontRB.Boldweight = (short)FontBoldWeight.BOLD;
            cellBStyle.SetFont(fontRB);
            cellBStyle.WrapText = true;

            CellStyle cellUStyle = workbook.CreateCellStyle();
            cellUStyle = SetCellBackgroudColor(cellUStyle, HSSFColor.LIGHT_GREEN.index);
            cellUStyle.WrapText = true;

            CellStyle cellRStyle = workbook.CreateCellStyle();
            Font fontR = workbook.CreateFont();
            fontR.Color = HSSFColor.RED.index;
            cellRStyle.SetFont(fontR);
            cellRStyle.WrapText = true;


            #endregion  格式設定

            #region sheet1 清冊

            Sheet sheet = workbook.CreateSheet("清冊");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);

            cols.CreateCell(0).SetCellValue("檔號");
            cols.CreateCell(1).SetCellValue("訂單編號");
            cols.CreateCell(2).SetCellValue("購買日期");

            cols.CreateCell(3).SetCellValue("超商");
            cols.CreateCell(4).SetCellValue("取件門市");
            cols.CreateCell(5).SetCellValue("物流編號");

            cols.CreateCell(6).SetCellValue("收件人");
            cols.CreateCell(7).SetCellValue("收件人電話");

            cols.CreateCell(8).SetCellValue("方案名稱");
            //↓new var columns↓
            cols.CreateCell(9).SetCellValue("應出貨品項規格");
            cols.CreateCell(10).SetCellValue("貨號");
            cols.CreateCell(11).SetCellValue("應出貨數");
            cols.CreateCell(12).SetCellValue("售價");
            cols.CreateCell(13).SetCellValue("進貨價");
            //↑new var columns↑
            cols.CreateCell(14).SetCellValue("訂單備註");
            cols.CreateCell(15).SetCellValue("出貨日");
            

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState;
            string returnProcessState;
            //code 0 = 24hr出貨
            string labelFor24hr = string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(0));
            bool isDealRow = true;
            VacationCollection recentHolidays = _pp.GetVacationListByPeriod(DateTime.Now.AddMonths(-14), DateTime.Now.AddMonths(14));

            OrderCollection orderList = _op.OrderGet(orderInfos.Select(x => x.OrderGuid).ToList());
            OrderProductDeliveryCollection opdList = _op.OrderProductDeliveryGetListByOrderGuid(orderInfos.Select(x => x.OrderGuid).ToList());

            foreach (var item in orderInfos)
            {
                isDealRow = string.IsNullOrEmpty(item.ShipItemDescription) &&
                    string.IsNullOrEmpty(item.ItemNo) &&
                    string.IsNullOrEmpty(item.ShipItemCount);

                Row row = sheet.CreateRow(rowIdx);

                #region before process
                switch (item.OrderShipState)
                {
                    case VbsOrderShipState.Shipped:
                        orderShipState = "已出貨";
                        break;

                    case VbsOrderShipState.ShippedAndPartialCancel:
                        orderShipState = "已出貨-部分退貨";
                        break;

                    case VbsOrderShipState.UnShip:
                        orderShipState = "未出貨";
                        break;

                    case VbsOrderShipState.UnShipAndPartialCancel:
                        orderShipState = "未出貨-部分退貨";
                        break;

                    case VbsOrderShipState.ReturnBeforeDealClose:
                        orderShipState = "結檔前退貨";
                        break;

                    case VbsOrderShipState.ReturnAfterDealClose:
                        orderShipState = "結檔後退貨";
                        break;

                    default:
                        orderShipState = string.Empty;
                        break;
                }

                switch (item.VendorReturnProgressState)
                {
                    case ViewVendorProgressState.Process:
                        returnProcessState = "待處理";
                        break;

                    default:
                        returnProcessState = string.Empty;
                        break;
                }
                #endregion

                bool isUserMemoMark = false;
                string orderUserMemo = string.Empty;
                if (item.OrderUserMemoInfos.Any())
                {
                    isUserMemoMark = true;
                    orderUserMemo = string.Join("\n", item.OrderUserMemoInfos
                                                            .OrderBy(x => x.CreateTime)
                                                            .Select(x => x.CreateTime.ToString("yyyy/MM/dd") + "  " + x.UserMemo));
                }

                row.CreateCell(0).SetCellValue(item.DealInfo.DealUniqueId.ToString(CultureInfo.InvariantCulture));//檔號
                row.CreateCell(1).SetCellValue(item.OrderId);   //訂單編號
                row.CreateCell(2).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));//購買日期

                //Order order = orderList.Where(x => x.Guid == item.OrderGuid).FirstOrDefault();
                string ispName = string.Empty;
                OrderProductDelivery opd = opdList.Where(x => x.OrderGuid == item.OrderGuid).FirstOrDefault();
                if (opd != null && opd.IsLoaded)
                {
                    if (opd.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup)
                    {
                        ispName = Helper.GetDescription(ServiceChannel.FamilyMart);
                    }
                    else if (opd.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup)
                    {
                        ispName = Helper.GetDescription(ServiceChannel.SevenEleven);
                    }
                }

                string ispStoreName = OrderShipUtility.GetIspStoreName(opd);


                row.CreateCell(3).SetCellValue(ispName);//超商
                row.CreateCell(4).SetCellValue(ispStoreName);//取件門市
                row.CreateCell(5).SetCellValue(opd.PreShipNo);//物流編號

                row.CreateCell(6).SetCellValue(item.RecipientName); //收件人
                row.CreateCell(7).SetCellValue(item.RecipientTel);  //收件人電話

                row.CreateCell(8).SetCellValue(item.DealInfo.DealName.ReplaceIgnoreCase(labelFor24hr, string.Empty));   //方案名稱
                //應出貨品項規格
                row.CreateCell(9).SetCellValue(item.ShipItemDescription);
                int itemDescWarpCount = Regex.Matches(item.ShipItemDescription, "\n").Count;
                row.HeightInPoints = Convert.ToSingle((itemDescWarpCount + 1) * sheet.DefaultRowHeight / 20);
                //貨號
                row.CreateCell(10).SetCellValue(item.ItemNo);
                //應出貨數量
                row.CreateCell(11).SetCellValue(item.ShipItemCount);
                //售價
                row.CreateCell(12).SetCellValue(Convert.ToInt32(item.ItemPrice));
                //進貨價
                row.CreateCell(13).SetCellValue(Convert.ToInt32(item.ItemCost));

                row.CreateCell(14).SetCellValue(orderUserMemo);
                if (item.ShipTime != null)
                {
                    row.CreateCell(15).SetCellValue(item.ShipTime.Value.ToString("yyyy/MM/dd"));
                }
                
                //訂單備註 依照列數調整列高
                //row.GetCell(13).CellStyle = cellWStyle;
                //int userMemoWarpCount = Regex.Matches(orderUserMemo, "\n").Count;
                //row.HeightInPoints = Convert.ToSingle((userMemoWarpCount + 1) * sheet.DefaultRowHeight / 20);

                //row.CreateCell(14).SetCellValue(isDealRow ? orderShipState : string.Empty);
                //row.CreateCell(15).SetCellValue(returnProcessState);
                //if (isReturnMark)
                //{
                //    row.GetCell(15).CellStyle = cellBStyle;
                //}

                //if (item.ShipTime.HasValue)
                //{
                //    row.CreateCell(16).SetCellValue(item.ShipTime.Value.ToShortDateString());
                //}
                //else
                //{
                //    row.CreateCell(16).SetCellValue(string.Empty);
                //}

                //if (item.ShipCompanyId.HasValue)
                //{
                //    row.CreateCell(17).SetCellValue(item.ShipCompanyId.Value);
                //}
                //else
                //{
                //    row.CreateCell(17).SetCellValue(string.Empty);
                //}

                //row.CreateCell(18).SetCellValue(item.ShipCompanyName);
                //row.CreateCell(19).SetCellValue(item.ShipNo);
                //row.CreateCell(20).SetCellValue(item.ShipMemo);

                //if (isDealRow)
                //{
                //    row.GetCell(16).CellStyle = cellStyle;
                //    row.GetCell(17).CellStyle = cellYStyle;
                //    row.GetCell(19).CellStyle = cellCStyle;
                //    row.GetCell(20).CellStyle = cellMStyle;
                //}

                ////處理出貨條件
                //var dp = ViewPponDealManager.DefaultManager.DealPropertyGetByBid(item.DealInfo.DealId);
                //var vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(item.DealInfo.DealId);
                ////重新判斷出貨類型(改成不看icon標籤)
                //if (isDealRow)
                //{
                //    if (dp.ShipType == (int)DealShipType.Normal)
                //    {
                //        if (dp.ShippingdateType == (int)DealShippingDateType.Normal)
                //        {
                //            item.DealInfo.DealLabelDesc = GetNormalLableDesc(item.OrderCreateTime, item.OrderCreateTime, dp.Shippingdate ?? 0, null, recentHolidays);
                //        }
                //        else if (dp.ShippingdateType == (int)DealShippingDateType.Special)
                //        {
                //            item.DealInfo.DealLabelDesc = GetNormalLableDesc(vpd.BusinessHourOrderTimeS, vpd.BusinessHourOrderTimeE, dp.ProductUseDateStartSet ?? 0, dp.ProductUseDateEndSet, recentHolidays);
                //        }
                //    }
                //    else if (dp.ShipType == (int)DealShipType.Ship72Hrs)
                //    {
                //        item.DealInfo.DealLabelDesc = labelFor24hr;
                //    }
                //    else
                //    {
                //        item.DealInfo.DealLabelDesc = string.Empty;
                //    }
                //}

                //row.CreateCell(21).SetCellValue(isDealRow ? item.DealInfo.DealLabelDesc : string.Empty);
                //row.GetCell(21).CellStyle = cellRStyle;

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 抓取最長長度) 英數字長度*1 中文字長度*2
                    int length = column.ToString().Split("\n").Select(x => System.Text.Encoding.Default.GetBytes(x))
                                                              .Select(x => x.Length)
                                                              .Concat(new[] { 0 })
                                                              .Max() + 2;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }

                    if (column.ColumnIndex < 14)
                    {
                        column.CellStyle = isUserMemoMark
                                            ? cellUStyle
                                            : cellWStyle;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            #endregion sheet1 清冊

            

            //使用非同步寫入該檔次清冊匯出紀錄
            List<Guid> orderGuids = orderInfos.Select(x => x.OrderGuid).Distinct().ToList();
            SetOrderProductInventoryExportLog(orderGuids, VbsCurrent.AccountId, inventoryType, true);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        

        /// <summary>
        /// 品項匯出清冊格式
        /// </summary>
        /// <param name="dealInfo"></param>
        /// <param name="productStatisticses"></param>
        /// <returns></returns>
        private static MemoryStream ProductOptionStatisticsExport(DealSaleInfo dealInfo, IEnumerable<OrderProductStatistics> productStatisticses)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 品項統計

            Sheet sheet = workbook.CreateSheet("品項統計");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("[" + dealInfo.DealUniqueId + "] " + dealInfo.DealName);
            cols = sheet.CreateRow(1);
            cols.CreateCell(0).SetCellValue("配送時間：" + dealInfo.ShipPeriodStartTime.ToString("yyyy/MM/dd") + " ~ " + dealInfo.ShipPeriodEndTime.ToString("yyyy/MM/dd"));

            cols = sheet.CreateRow(2);

            cols = sheet.CreateRow(3);
            cols.CreateCell(0).SetCellValue("品項");
            cols.CreateCell(1).SetCellValue("數量");

            #endregion 表頭 及 title

            #region 統計內容

            int rowIdx = 4;

            foreach (var item in productStatisticses)
            {
                Row row = sheet.CreateRow(rowIdx);

                row.CreateCell(0).SetCellValue(item.ProductOption);
                row.CreateCell(1).SetCellValue(item.Count);

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 計算第一行) 英數字長度*1 中文字長度*2
                    byte[] sarr = System.Text.Encoding.Default.GetBytes(column.ToString().Split("\n")[0]);
                    int length = sarr.Length;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
            }

            #endregion 統計內容

            #endregion 品項統計

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }


        /// <summary>
        /// 單一檔次匯出資料
        /// </summary>
        /// <param name="productGuid"></param>
        /// <param name="isHiddenPersonalInfo"></param>
        /// <param name="newVersion"></param>
        /// <returns></returns>
        private List<VbsOrderListInfo> GetOrderShipInventoryList(Guid productGuid, bool isHiddenPersonalInfo, bool newVersion)
        {
            var dealsInfos = GetDealSaleInfo(productGuid);

            if (newVersion)
            {
                return NewOrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo);
            }
            else
            {
                return OrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo, false);
            }
        }

        /// <summary>
        /// 全檔次批次匯出資料
        /// </summary>
        /// <param name="productGuids"></param>
        /// <param name="isHiddenPersonalInfo"></param>
        /// <param name="newVersion"></param>
        /// <returns></returns>
        private List<VbsOrderListInfo> GetOrderShipInventoryList(IEnumerable<Guid> productGuids, bool isHiddenPersonalInfo, bool newVersion)
        {
            var dealsInfos = GetDealSaleInfo(productGuids); ;

            if (newVersion)
            {
                return NewOrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo);
            }
            else
            {
                return OrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo, false);
            }
        }

        

        /// <summary>
        /// 取得舊版/超取匯出相關資料
        /// </summary>
        /// <param name="dealsInfos"></param>
        /// <param name="isHiddenPersonalInfo"></param>
        /// <param name="isIspOrder"></param>
        /// <returns></returns>
        private List<VbsOrderListInfo> OrderShipInventoryListGet(
            IEnumerable<DealSaleInfo> dealsInfos,
            bool isHiddenPersonalInfo,
            bool isIspOrder)
        {
            var infos = new List<VbsOrderListInfo>();
            ILookup<Guid, ViewOrderReturnFormList> returnLists = null;
            Dictionary<Guid, List<OrderUserMemoList>> orderUserMemoLists = null;


            StringBuilder builder = new StringBuilder();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 1");

            //撈取該檔次訂單之品項數量 
            var dealGuids = dealsInfos.Select(x => x.DealId);

            ProposalPerformanceLog log = new ProposalPerformanceLog();
            log.Link = "OrderShipInventoryListGet";
            log.ContentLog = dealGuids.Any() ? new JsonSerializer().Serialize(dealGuids) : string.Empty;
            log.CreateTime = DateTime.Now;
            log.CreateUser = Session[VbsSession.UserId.ToString()].ToString();
            _sp.ProposalPerformanceLogSet(log);

            var dealIds = dealsInfos.Select(x => x.DealUniqueId);
            var orderProductOptionLists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds);
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 2");
            var orderItemLists = _op.GetOrderProductOptionList(orderProductOptionLists)
                                        .GroupBy(x => x.OrderGuid)
                                        .Select(x => new
                                            {
                                                x.Key,
                                                OptionDescription = string.Join("\n", x.Select(o => o.OptionDescription)),
                                                ItemNo = string.Join("\n", x.Select(o => o.ItemNo.Trim(','))),
                                                Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                                ItemCount = x.Sum(o => o.Quantity)
                                            })
                                        .ToDictionary(x => x.Key, x => x);

            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 3");
            //撈取應出貨品項數量(排除退貨中及退貨完成品項數量)
            var unReturnOptionLists = orderProductOptionLists.Where(x => !x.IsReturned && !x.IsReturning)
                                                             .ToList();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 4");
            var unReturnItemLists = _op.GetOrderProductOptionList(unReturnOptionLists)
                                            .GroupBy(x => x.OrderGuid)
                                            .Select(x => new
                                            {
                                                x.Key,
                                                OptionDescription = string.Join("\n", x.Select(o => o.OptionDescription)),
                                                ItemNo = string.Join("\n", x.Select(o => o.ItemNo.Trim(','))),
                                                Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                                ItemCount = x.Sum(o => o.Quantity)
                                            })
                                            .ToDictionary(x => x.Key, x => x);
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 5");
            List<ViewOrderShipList> orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids).ToList();
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 5-1");
            //撈取該檔次訂單之退貨單
            returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                .ToLookup(x => x.OrderGuid);

            //returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids,
            //                                                            new List<int> {
            //                                                                (int)ProgressStatus.Processing,
            //                                                                (int)ProgressStatus.AtmQueueing,
            //                                                                (int)ProgressStatus.Completed,
            //                                                                (int)ProgressStatus.AtmQueueSucceeded,
            //                                                                (int)ProgressStatus.AtmFailed,
            //                                                                (int)ProgressStatus.CompletedWithCreditCardQueued },
            //                                                            new List<int> { }, new List<int> { }).ToLookup(x => x.OrderGuid);


            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 6");
            List<Guid> orderGuids = orderShipList.Select(x => x.OrderGuid).ToList();
            orderUserMemoLists = _op.OrderUserMemoListGetList(orderGuids)
                                    .GroupBy(x => x.OrderGuid)
                                    .ToDictionary(x => x.Key, x => x.ToList());
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 7");
            var orderProductDelivery = _op.OrderProductDeliveryGetListByOrderGuid(orderGuids);

            if (isIspOrder)
            {
                orderShipList = orderShipList.Where(x => x.ProductDeliveryType != (int)ProductDeliveryType.Normal).ToList();
            }
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 8");
            Dictionary<Guid, List<DealCost>> dicDealCost = _pp.DealCostGetList(dealGuids).GroupBy(z => z.BusinessHourGuid).ToDictionary(x => x.Key, y => y.ToList());
            Dictionary<Guid?, Item> ic = _ip.ItemGetByBid(dealGuids).ToDictionary(x => x.BusinessHourGuid, x => x);
            Dictionary<Guid, List<CashTrustLog>> dicCashTrustLog = _mp.GetCashTrustLogCollectionByOid(dealGuids).GroupBy(z => z.OrderGuid).ToDictionary(x => x.Key, y => y.ToList());

            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 9");
            if (orderShipList.Count > 0)
            {
                foreach (ViewOrderShipList order in orderShipList)
                {
                    VbsOrderShipState orderShipState = VbsOrderShipState.UnShip;
                    ViewVendorProgressState? vendorReturnProgressState = null;
                    List<OrderUserMemoInfo> orderUserMemoInfos = new List<OrderUserMemoInfo>();
                    bool isPartialCancel = false;

                    //抓不到檔次資訊 視為異常 不顯示該筆訂單
                    var dealInfo = dealsInfos.FirstOrDefault(x => x.DealId == order.ProductGuid);
                    if (dealInfo == null)
                    {
                        continue;
                    }

                    //抓不到訂單品項資訊 視為異常 不顯示該筆訂單
                    if (!orderItemLists.ContainsKey(order.OrderGuid))
                    {
                        continue;
                    }
                    //抓取訂單之退貨管理狀態
                    if (returnLists.Contains(order.OrderGuid))
                    {
                        if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Process;
                        }
                        else if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndNoRetrieving ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedByCustomerService ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.Automatic))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Finished;
                        }
                    }
                    //抓取未退貨品項數量
                    if (unReturnItemLists.ContainsKey(order.OrderGuid))
                    {
                        //部分退貨 : 訂單數量-可出貨訂單數量 > 0
                        isPartialCancel = orderItemLists[order.OrderGuid].ItemCount - unReturnItemLists[order.OrderGuid].ItemCount > 0;
                    }
                    else
                    {
                        //沒有可出貨品項 且 廠商退貨處理完成 or 其他退款狀態(如退款失敗) 訂單 不顯示
                        if (vendorReturnProgressState != ViewVendorProgressState.Process)
                        {
                            continue;
                        }
                    }
                    //訂單出貨狀態
                    if (!order.ShipTime.HasValue ||
                        DateTime.Compare(order.ShipTime.Value, DateTime.Today) > 0)
                    {
                        orderShipState = isPartialCancel
                                            ? VbsOrderShipState.UnShipAndPartialCancel
                                            : VbsOrderShipState.UnShip;
                    }
                    else
                    {
                        orderShipState = isPartialCancel
                                            ? VbsOrderShipState.ShippedAndPartialCancel
                                            : VbsOrderShipState.Shipped;
                    }
                    //抓取訂單備註
                    if (orderUserMemoLists.ContainsKey(order.OrderGuid))
                    {
                        foreach (var userMemo in orderUserMemoLists[order.OrderGuid])
                        {
                            var orderUserMemoInfo = new OrderUserMemoInfo
                            {
                                CreateTime = userMemo.CreateTime,
                                UserMemo = userMemo.UserMemo
                            };
                            orderUserMemoInfos.Add(orderUserMemoInfo);
                        }
                    }
                    //運費
                    List<CashTrustLog> cashTrustLog = new List<CashTrustLog>();
                    if (dicCashTrustLog.ContainsKey(order.OrderGuid))
                    {
                        cashTrustLog = dicCashTrustLog[order.OrderGuid];
                    }
                    int freightAmount = cashTrustLog.Where(x => Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freight)).Sum(x => x.CreditCard + x.Pcash + x.Scash + x.Atm + x.Bcash);


                    //進貨價
                    List<DealCost> dealCost = new List<DealCost>();
                    if (dicDealCost.ContainsKey(order.ProductGuid))
                    {
                        dealCost = dicDealCost[order.ProductGuid];
                    }

                    //賣價
                    Item item = new Item();
                    if (ic.ContainsKey(order.ProductGuid))
                    {
                        item = ic[order.ProductGuid];
                    }

                    if (isHiddenPersonalInfo)
                    {
                        order.MemberName = "***";
                        order.PhoneNumber = "******";
                        order.DeliveryAddress = "*********";
                    }

                    string ispStoreName = string.Empty;
                    string trackingNumber = string.Empty;
                    bool isShipmentPdf = false;
                    int productDeliveryType = (int)ProductDeliveryType.Normal;
                    OrderProductDelivery opd = orderProductDelivery.Where(x => x.OrderGuid == order.OrderGuid).FirstOrDefault();
                    if (opd != null)
                    {
                        ispStoreName = OrderShipUtility.GetIspStoreName(opd);
                        trackingNumber = !string.IsNullOrEmpty(opd.PreShipNo) ? opd.PreShipNo : string.Empty;
                        isShipmentPdf = opd.IsShipmentPdf;
                        productDeliveryType = opd.ProductDeliveryType;
                    }
                    var info = new VbsOrderListInfo
                    {
                        DealInfo = dealInfo,
                        OrderGuid = order.OrderGuid,
                        OrderId = order.OrderId,
                        OrderCreateTime = order.OrderCreateTime,
                        PackCount = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].ItemCount / dealInfo.ComboPackCount : 0,
                        ItemDescription = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].OptionDescription : string.Empty,
                        ItemNo = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].ItemNo : string.Empty,
                        ItemCount = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].Quantity : string.Empty,
                        ItemPrice = item.ItemPrice,
                        ItemCost = dealCost.Count > 0 ? (dealCost[dealCost.Count() - 1].Cost ?? 0m) : 0m,
                        RecipientName = order.MemberName,
                        RecipientTel = order.PhoneNumber,
                        RecipientAddress = order.DeliveryAddress,
                        RecipientRemark = order.UserMemo,
                        OrderShipId = order.OrderShipId,
                        ShipCompanyId = order.ShipCompanyId,
                        ShipCompanyName = order.ShipCompanyName,
                        ShipTime = order.ShipTime,
                        ShipNo = order.ShipNo,
                        OrderShipState = orderShipState,
                        HasOrderShip = order.ShipTime.HasValue,
                        OrderStatus = order.OrderStatus,
                        ShipMemo = order.ShipMemo,
                        OrderUserMemoInfos = orderUserMemoInfos,
                        VendorReturnProgressState = vendorReturnProgressState,
                        ShipItemDescription = unReturnItemLists.ContainsKey(order.OrderGuid) ? unReturnItemLists[order.OrderGuid].OptionDescription : string.Empty,
                        ShipItemCount = unReturnItemLists.ContainsKey(order.OrderGuid) ? unReturnItemLists[order.OrderGuid].Quantity : string.Empty,
                        FreightAmount = freightAmount,
                        IspStoreName = ispStoreName,
                        TrackingNumber = trackingNumber,
                        IsShipmentPdf = isShipmentPdf,
                        ProductDeliveryType = productDeliveryType,
                    };
    
                    infos.Add(info);
                }
            }
            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " 10");

            ProposalFacade.ProposalPerformanceLogSet("OrderShipInventoryListGet", builder.ToString(), UserName);


            return infos;
        }

        /// <summary>
        /// 取得新版匯出相關資料
        /// </summary>
        /// <param name="dealsInfos"></param>
        /// <param name="isHiddenPersonalInfo"></param>
        /// <returns></returns>
        private List<VbsOrderListInfo> NewOrderShipInventoryListGet(IEnumerable<DealSaleInfo> dealsInfos, bool isHiddenPersonalInfo)
        {
            var infos = new List<VbsOrderListInfo>();
            ILookup<Guid, ViewOrderReturnFormList> returnLists = null;
            Dictionary<Guid, List<OrderUserMemoList>> orderUserMemoLists = null;

            //撈取該檔次訂單之品項數量 
            var dealGuids = dealsInfos.Select(x => x.DealId);
            var dealIds = dealsInfos.Select(x => x.DealUniqueId);
            var orderProductOptionLists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds);
            var orderItemLists = _op.GetOrderProductOptionList(orderProductOptionLists)
                                        .GroupBy(x => new { x.OrderGuid, x.OptionDescription, x.ItemNo, x.Quantity })
                                        .Select(x => new
                                        {
                                            OrderGuid = x.Key.OrderGuid,
                                            OptionDescription = x.Key.OptionDescription,
                                            ItemNo = x.Key.ItemNo.Trim(','),
                                            Quantity = Convert.ToString(x.Key.Quantity),
                                            ItemCount = x.Sum(o => o.Quantity)
                                        }).ToList();
            //撈取應出貨品項數量(排除退貨中及退貨完成品項數量)
            var unReturnOptionLists = orderProductOptionLists.Where(x => !x.IsReturned && !x.IsReturning)
                                                             .ToList();
            var unReturnItemLists = _op.GetOrderProductOptionList(unReturnOptionLists)
                                            .GroupBy(x => new { x.OrderGuid, x.OptionDescription, x.ItemNo, x.Quantity })
                                            .Select(x => new
                                            {
                                                OrderGuid = x.Key.OrderGuid,
                                                OptionDescription = x.Key.OptionDescription,
                                                ItemNo = x.Key.ItemNo.Trim(','),
                                                Quantity = Convert.ToString(x.Key.Quantity),
                                                ItemCount = x.Sum(o => o.Quantity)
                                            }).ToList();

            //寫規格項目用
            var itemLists = _op.GetOrderProductOptionList(orderProductOptionLists)
                                            .GroupBy(x => new { x.OrderGuid, x.OptionDescription, x.ItemNo, x.Quantity })
                                            .Select(x => new
                                            {
                                                OrderGuid = x.Key.OrderGuid,
                                                OptionDescription = x.Key.OptionDescription,
                                                ItemNo = x.Key.ItemNo.Trim(','),
                                                Quantity = Convert.ToString(x.Key.Quantity),
                                                ItemCount = x.Sum(o => o.Quantity)
                                            }).ToList();

            var orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids);
            //撈取該檔次訂單之退貨單
            returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                .ToLookup(x => x.OrderGuid);
            List<Guid> orderGuids = orderShipList.Select(x => x.OrderGuid).ToList();
            orderUserMemoLists = _op.OrderUserMemoListGetList(orderGuids)
                                    .GroupBy(x => x.OrderGuid)
                                    .ToDictionary(x => x.Key, x => x.ToList());

            //撈取對應之提案單
            ProposalMultiDealCollection multiDeals = _sp.ProposalMultiDealGetListByBid(dealGuids.Select(x => x).ToList());


            if (orderShipList.Count > 0)
            {
                foreach (ViewOrderShipList order in orderShipList)
                {
                    VbsOrderShipState orderShipState = VbsOrderShipState.UnShip;
                    ViewVendorProgressState? vendorReturnProgressState = null;
                    List<OrderUserMemoInfo> orderUserMemoInfos = new List<OrderUserMemoInfo>();
                    bool isPartialCancel = false;

                    //抓不到檔次資訊 視為異常 不顯示該筆訂單
                    var dealInfo = dealsInfos.FirstOrDefault(x => x.DealId == order.ProductGuid);
                    if (dealInfo == null)
                    {
                        continue;
                    }

                    //抓不到訂單品項資訊 視為異常 不顯示該筆訂單
                    if (!orderItemLists.Any(x => x.OrderGuid == order.OrderGuid))
                    {
                        continue;
                    }

                    //抓取訂單之退貨管理狀態
                    if (returnLists.Contains(order.OrderGuid))
                    {
                        if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Process;
                        }
                        else if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndNoRetrieving ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedByCustomerService ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.Automatic))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Finished;
                        }
                    }

                    //抓取未退貨品項數量
                    if (unReturnItemLists.Any(x => x.OrderGuid == order.OrderGuid))
                    {
                        //部分退貨 : 訂單數量-可出貨訂單數量 > 0
                        isPartialCancel = 
                            orderItemLists.Where(x => x.OrderGuid == order.OrderGuid).Sum(x => x.ItemCount)
                            - unReturnItemLists.Where(x => x.OrderGuid == order.OrderGuid).Sum(x => x.ItemCount) > 0;
                    }
                    else
                    {
                        //沒有可出貨品項 且 廠商退貨處理完成 or 其他退款狀態(如退款失敗) 訂單 不顯示
                        if (vendorReturnProgressState != ViewVendorProgressState.Process)
                        {
                            continue;
                        }
                    }

                    //訂單出貨狀態
                    if (!order.ShipTime.HasValue ||
                        DateTime.Compare(order.ShipTime.Value, DateTime.Today) > 0)
                    {
                        orderShipState = isPartialCancel
                                            ? VbsOrderShipState.UnShipAndPartialCancel
                                            : VbsOrderShipState.UnShip;
                    }
                    else
                    {
                        orderShipState = isPartialCancel
                                            ? VbsOrderShipState.ShippedAndPartialCancel
                                            : VbsOrderShipState.Shipped;
                    }

                    //抓取訂單備註
                    if (orderUserMemoLists.ContainsKey(order.OrderGuid))
                    {
                        foreach (var userMemo in orderUserMemoLists[order.OrderGuid])
                        {
                            var orderUserMemoInfo = new OrderUserMemoInfo
                            {
                                CreateTime = userMemo.CreateTime,
                                UserMemo = userMemo.UserMemo
                            };
                            orderUserMemoInfos.Add(orderUserMemoInfo);
                        }
                    }

                    //運費
                    var cashTrustLog = _mp.CashTrustLogGetListByOrderGuid(order.OrderGuid, OrderClassification.LkSite);
                    int freightAmount = cashTrustLog.Where(x => Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freight)).Sum(x => x.CreditCard + x.Pcash + x.Scash + x.Atm + x.Bcash + x.Tcash + x.Pscash);

                    //進貨價
                    var dealCost = _pp.DealCostGetList(order.ProductGuid);

                    //賣價
                    var item = ProviderFactory.Instance().GetProvider<IItemProvider>().ItemGetByBid(order.ProductGuid);

                    if (isHiddenPersonalInfo)
                    {
                        order.MemberName = "***";
                        order.PhoneNumber = "******";
                        order.DeliveryAddress = "*********";
                    }

                    //var mds = multiDeals.Where(x => x.Bid == order.ProductGuid);
                    //if (mds != null && mds.Count() > 0)
                    //{
                    //    if(mds.FirstOrDefault().Gifts != "")
                    //    {
                    //        List<string> gifts = new JsonSerializer().Deserialize<List<string>>(mds.FirstOrDefault().Gifts);
                    //        if(gifts != null && gifts.Count > 0)
                    //        {
                    //            orderUserMemoInfos.Add(new OrderUserMemoInfo
                    //            {
                    //                UserMemo = string.Format("贈品：{0}", string.Join(",", gifts)),
                    //                CreateTime = order.OrderCreateTime
                    //            });
                    //        }
                    //    }
                    //}

                    var info = new VbsOrderListInfo
                    {
                        DealInfo = dealInfo,
                        OrderGuid = order.OrderGuid,
                        OrderId = order.OrderId,
                        OrderCreateTime = order.OrderCreateTime,
                        PackCount = 0,
                        ItemDescription = string.Empty,
                        ItemNo = string.Empty,
                        ItemCount = string.Empty,
                        ItemPrice = item.ItemPrice,
                        ItemCost = dealCost.Count > 0 ? (dealCost[dealCost.Count() - 1].Cost ?? 0m) : 0m,
                        RecipientName = order.MemberName,
                        RecipientTel = order.PhoneNumber,
                        RecipientAddress = order.DeliveryAddress,
                        RecipientRemark = order.UserMemo,
                        OrderShipId = order.OrderShipId,
                        ShipCompanyId = order.ShipCompanyId,
                        ShipCompanyName = order.ShipCompanyName,
                        ShipTime = order.ShipTime,
                        ShipNo = order.ShipNo,
                        OrderShipState = orderShipState,
                        HasOrderShip = order.ShipTime.HasValue,
                        OrderStatus = order.OrderStatus,
                        ShipMemo = order.ShipMemo,
                        OrderUserMemoInfos = orderUserMemoInfos,
                        VendorReturnProgressState = vendorReturnProgressState,
                        ShipItemDescription = string.Empty,
                        ShipItemCount = string.Empty,
                        FreightAmount = freightAmount
                    };

                    infos.Add(info);

                    #region 依照要出貨的規格數量再多寫筆數
                    foreach (var oilRow in itemLists.Where(x => x.OrderGuid == order.OrderGuid))
                    {
                        string itemNo = oilRow.ItemNo;
                        try
                        {
                            if (string.IsNullOrEmpty(oilRow.OptionDescription) && string.IsNullOrEmpty(itemNo))
                            {
                                CashTrustLogCollection ctls = _mp.CashTrustLogGetListByOrderGuid(oilRow.OrderGuid);
                                if (ctls.Count() > 0)
                                {
                                    Guid bid = ctls.FirstOrDefault().BusinessHourGuid ?? Guid.Empty;
                                    if (bid != Guid.Empty)
                                    {
                                        ProposalMultiDeal pmd = _sp.ProposalMultiDealGetByBid(bid);
                                        ViewComboDealCollection vcds = _pp.GetViewComboDealAllByBid(bid);
                                        Guid mainBid = bid;
                                        if (vcds.Count > 0)
                                        {
                                            if (vcds.FirstOrDefault().MainBusinessHourGuid.Value != bid)
                                            {
                                                bid = vcds.FirstOrDefault().MainBusinessHourGuid.Value;
                                            }
                                        }
                                        ProposalMultiOptionSpecCollection optSpecs = _sp.ProposalMultiOptionSpecGetByBid(bid);
                                        if (optSpecs.Count() > 0)
                                        {
                                            ProposalMultiOptionSpec spec = optSpecs.Where(x => x.MultiOptionId == pmd.Id).FirstOrDefault();
                                            if(spec != null)
                                            {
                                                ProductItem proItem = _pp.ProductItemGet(spec.ItemGuid);
                                                if (proItem.IsLoaded)
                                                {
                                                    ProductInfo product = _pp.ProductInfoGet(proItem.InfoGuid);
                                                    if (product.IsLoaded)
                                                    {
                                                        if (product.IsMulti == false)
                                                        {
                                                            if (!string.IsNullOrEmpty(proItem.ProductCode))
                                                            {
                                                                itemNo = proItem.ProductCode;
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                        }
                        

                        var optInfo = new VbsOrderListInfo
                        {
                            DealInfo = dealInfo,
                            OrderGuid = order.OrderGuid,
                            OrderId = order.OrderId,
                            OrderCreateTime = order.OrderCreateTime,
                            PackCount = 0,
                            ItemDescription = string.Empty,
                            ItemNo = itemNo,
                            ItemCount = string.Empty,
                            ItemPrice = 0m,
                            ItemCost = 0m,
                            RecipientName = order.MemberName,
                            RecipientTel = order.PhoneNumber,
                            RecipientAddress = order.DeliveryAddress,
                            RecipientRemark = order.UserMemo,
                            OrderShipId = order.OrderShipId,
                            ShipCompanyId = null,
                            ShipCompanyName = string.Empty,
                            ShipTime = null,
                            ShipNo = string.Empty,
                            OrderShipState = orderShipState,
                            HasOrderShip = order.ShipTime.HasValue,
                            OrderStatus = -1,
                            ShipMemo = string.Empty,
                            OrderUserMemoInfos = new List<OrderUserMemoInfo>(),
                            VendorReturnProgressState = null,
                            ShipItemDescription = oilRow.OptionDescription,
                            ShipItemCount = oilRow.Quantity,
                            FreightAmount = 0
                        };

                        infos.Add(optInfo);
                    }
                    #endregion
                }
            }

            return infos;
        }


        /// <summary>
        /// 匯出紀錄log
        /// </summary>
        /// <param name="orderGuids"></param>
        /// <param name="accountId"></param>
        /// <param name="inventoryType"></param>
        /// <param name="newVersion"></param>
        private static void SetOrderProductInventoryExportLog(IEnumerable<Guid> orderGuids, string accountId, InventoryType inventoryType, bool newVersion = false)
        {
            string inventoryTypeDesc = string.Empty;
            string newVersionDesc = newVersion ? "(新版)" : string.Empty;
            switch (inventoryType)
            {
                case InventoryType.AllShip:
                    inventoryTypeDesc = "出貨清冊";
                    break;
                case InventoryType.UnShip:
                    inventoryTypeDesc = "待出貨清冊";
                    break;
                case InventoryType.ComboDeals:
                    inventoryTypeDesc = "各別出貨清冊";
                    break;
                case InventoryType.BatchUnShip:
                    inventoryTypeDesc = "一次撈單待出貨清冊";
                    break;
                default:
                    return;
            }

            Action<string, IEnumerable<Guid>> setExportLog = delegate (string userId, IEnumerable<Guid> odrGuids)
            {
                logger.InfoFormat(" 使用者:{0} 寫入匯出{1}{2}Log Begin...", userId, inventoryTypeDesc, newVersionDesc);

                foreach (var orderGuid in odrGuids)
                {
                    CommonFacade.AddAudit(orderGuid, AuditType.Order, string.Format("匯出{0}{1}", inventoryTypeDesc, newVersionDesc), userId, true);
                }

                logger.InfoFormat(" 使用者:{0} 寫入匯出{1}{2}Log End。共匯出:{3}筆", userId, inventoryTypeDesc, newVersionDesc, odrGuids.Count());
            };

            setExportLog.BeginInvoke(accountId, orderGuids, null, null);
        }



        /// <summary>
        /// 取得品項相關資料
        /// </summary>
        /// <param name="isMergeCount"></param>
        /// <param name="orderProductOptionlists"></param>
        /// <returns></returns>
        private IEnumerable<OrderProductStatistics> GetOrderProductStatistics(bool isMergeCount, List<ViewOrderProductOptionList> orderProductOptionlists)
        {
            if (!orderProductOptionlists.Any())
            {
                return new List<OrderProductStatistics>();
            }

            var shipFacade = new VBSShipFacade();

            if (!isMergeCount)
            {
                return shipFacade.GetIndividualProductStatistics(orderProductOptionlists)
                        .Select(item => new OrderProductStatistics
                        {
                            ProductOption = item.Key,
                            Count = item.Value
                        }).ToList();
            }

            return shipFacade.GetMergeProductStatistics(orderProductOptionlists)
                    .Select(item => new OrderProductStatistics
                    {
                        ProductOption = item.Key,
                        Count = item.Value
                    }).ToList();
        }


        

        private static CellStyle SetCellBackgroudColor(CellStyle cellStyle, short colorIndex)
        {
            cellStyle.FillForegroundColor = colorIndex;
            cellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            return cellStyle;
        }

        private static string DownloadFileNameFormat(string fileName)
        {
            //檔名含全形雙引號 會導致excel無法正常開啟(空白) 故置換為全形單引號
            fileName = fileName.Replace("“", "‘").Replace("”", "’");
            List<char> formatStr = new List<char> { '/', '\\' };

            return formatStr.Aggregate(fileName, (current, fs) => current.Replace(fs, '_'));
        }

        private string DownloadFileNameEncode(string fileName)
        {
            fileName = DownloadFileNameFormat(fileName);
            string browser = HttpContext.Request.UserAgent.ToUpper();
            if (browser.Contains("IE"))
            {
                return HttpUtility.UrlEncode(fileName);
            }

            return fileName;
        }
        #endregion 出貨管理 / 線上清冊 / 備料統計

        #region 超取

        /// <summary>
        /// 超取匯出資料
        /// </summary>
        /// <param name="productGuids"></param>
        /// <param name="isHiddenPersonalInfo"></param>
        /// <param name="isIspOrder"></param>
        /// <returns></returns>
        private List<VbsOrderListInfo> GetOrderShipInventoryListByIsp(
            IEnumerable<Guid> productGuids,
            bool isHiddenPersonalInfo,
            bool isIspOrder)
        {
            var dealsInfos = GetDealSaleInfo(productGuids); ;

            return OrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo, isIspOrder);
        }

        private void FilterIspShipData(
            ref List<VbsOrderListInfo> resultData,
            string orderId,
            string dealUniqueId,
            string dateStart,
            string dateEnd,
            int shipStatusPrint,
            int shipStatusReport,
            string d)
        {
            #region filter
            var dealGuids = resultData.Select(x => x.DealInfo.DealId);

            var returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                               .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                           x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                           x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                           x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                           x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                           x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                               .ToLookup(x => x.OrderGuid);

            resultData = resultData.Where(x => !Helper.IsFlagSet(x.OrderStatus, OrderStatus.Cancel) && !Helper.IsFlagSet(x.OrderStatus, OrderStatus.Nothing)).ToList();

            if (!string.IsNullOrEmpty(orderId))
            {
                resultData = resultData.Where(x => x.OrderId == orderId).ToList();
            }
            if (!string.IsNullOrEmpty(dealUniqueId))
            {
                int deal_unique_id = 0;
                int.TryParse(dealUniqueId, out deal_unique_id);
                if (deal_unique_id != 0)
                {
                    resultData = resultData.Where(x => x.DealInfo.DealUniqueId == deal_unique_id).ToList();
                }
            }
            if (!string.IsNullOrEmpty(dateStart) || !string.IsNullOrEmpty(dateEnd))
            {
                DateTime dtS = DateTime.MinValue.Date;
                DateTime.TryParse(dateStart, out dtS);
                DateTime dtE = DateTime.MinValue.Date;
                DateTime.TryParse(dateEnd, out dtE);
                //訂購日
                if (dtS != DateTime.MinValue.Date)
                {
                    resultData = resultData.Where(x => x.OrderCreateTime >= dtS).ToList();
                }
                if (dtE != DateTime.MinValue.Date)
                {
                    resultData = resultData.Where(x => x.OrderCreateTime <= dtE.AddDays(1)).ToList();
                }
            }
            if (d.Equals("1"))
            {
                //待出貨清單(此頁顯示 (待出貨) 及 (未產生物流編號) 訂單)
                resultData = resultData.Where(x => x.ShipTime == null).ToList();
                resultData = resultData.Where(x => string.IsNullOrEmpty(x.TrackingNumber)).ToList();
                //排除退貨的資料
                resultData = resultData.Where(x => !returnLists.Contains(x.OrderGuid)).ToList();
            }
            else if (d.Equals("2"))
            {
                //列印標籤(此頁顯示（已產生物流編號）及（未回報出貨完成）訂單)
                if (shipStatusPrint == 1)
                {
                    //未列印標籤
                    resultData = resultData.Where(x => x.ShipTime == null).ToList();
                    resultData = resultData.Where(x => !string.IsNullOrEmpty(x.TrackingNumber) && x.IsShipmentPdf == false).ToList();
                }
                else
                {
                    //已列印標籤
                    resultData = resultData.Where(x => !string.IsNullOrEmpty(x.TrackingNumber) && x.IsShipmentPdf == true).ToList();
                }
                //排除退貨的資料
                //不排除退貨資料，只要取貨號，就預設要印標
            }
            else if (d.Equals("3"))
            {
                //回報出貨完成(此頁顯示（已產生物流編號）及（未回報出貨完成）訂單)
                if (shipStatusReport == 1)
                {
                    //未回報出貨
                    resultData = resultData.Where(x => x.ShipTime == null).ToList();
                    resultData = resultData.Where(x => !string.IsNullOrEmpty(x.TrackingNumber) && x.IsShipmentPdf && x.ShipTime == null).ToList();
                }
                else if (shipStatusReport == 2)
                {
                    //已回報出貨
                    resultData = resultData.Where(x => !string.IsNullOrEmpty(x.TrackingNumber) && x.IsShipmentPdf && x.ShipTime != null).ToList();
                }
                //排除退貨的資料
                //不排除退貨資料，因為商品都寄出去了，要讓商家可以確認回報
            }
            else if (d.Equals("4"))
            {
                //首頁使用,已產編但尚未列印標籤+尚未出貨
                resultData = resultData.Where(x => x.ShipTime == null).ToList();
                resultData = resultData.Where(x => !string.IsNullOrEmpty(x.TrackingNumber)).ToList();

            }




            #endregion filter
        }

        #endregion

        #region 批次出貨管理

        private ImportResult ProcessImportOrderShip(IEnumerable<DealSaleInfo> dealInfos, ShipBatchImportModel vdlModel)
        {
            var importResult = new ImportResult();
            var failInfo = new ImportFailInfo();
            var failInfos = new List<ImportFailInfo>();
            var tempDate = DateTime.MinValue;
            var remindInfo = new RemindingInfo();
            var remindInfos = new List<RemindingInfo>();
            var dealGuids = dealInfos.Select(x => x.DealId);
            var dealIds = dealInfos.Select(x => x.DealUniqueId);
            importResult.ImportTime = DateTime.Now;
            importResult.InsertCount = 0;
            importResult.UpdateCount = 0;
            importResult.FailCount = 0;
            importResult.NotYetCount = 0;
            importResult.ImportIndentity = vdlModel.DealUniqueId;

            if (vdlModel.ProductInventoryFile != null)
            {
                string importFileDir = string.Format("{0}/{1}/{2}", Server.MapPath(_conf.ShipBatchImport), VbsCurrent.AccountId, "InventoryImportTemp");
                if (!Directory.Exists(importFileDir))
                {
                    Directory.CreateDirectory(importFileDir);
                }
                else
                {
                    //超過45天的就清掉
                    DirectoryInfo di = new DirectoryInfo(importFileDir);
                    foreach (var fi in di.GetFiles())
                    {
                        if (DateTime.Compare(fi.LastWriteTime.AddDays(45), DateTime.Now) < 0)
                        {
                            fi.Delete();
                        }
                    }
                }

                vdlModel.ProductInventoryFile.SaveAs(importFileDir + "/" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".xls");
                if (vdlModel.ProductInventoryFile.ContentLength > 0)
                {
                    HSSFWorkbook workbook = null;
                    try
                    {
                        workbook = new HSSFWorkbook(vdlModel.ProductInventoryFile.InputStream);
                    }
                    catch (Exception ex)
                    {
                        importResult.FailCount++;
                        failInfo = new ImportFailInfo
                        {
                            DealUniqueId = string.Empty,
                            OrderId = string.Empty,
                            OrderGuid = Guid.Empty,
                            Message = "檔案讀取有誤，請確認檔案格式或重新另存新檔並重新匯入清冊檔"
                        };
                        failInfos.Add(failInfo);

                        importResult.FailInfos = failInfos;
                        if (failInfos.Count > 0)
                        {
                            importResult.FailFileName = WriteToImportFailFile(importResult);
                        }

                        importResult.RemindingInfos = remindInfos;
                        return importResult;
                    }
                    
                    Sheet currentSheet = workbook.GetSheetAt(0);
                    Row row;
                    ILookup<string, ViewOrderShipList> orderShipList = null;
                    List<ShipCompany> shipCompanyInfo = _op.ShipCompanyGetList(null).ToList();
                    ILookup<Guid, CashTrustLog> cashTrustLogs = null;

                    #region 檢查新舊版本
                    int oldCheckColIdxN = 21;
                    int oldCheckColIdx = 20;
                    if (VBSFacade.IsEnabledVbsShipRule2k18())
                    {
                        oldCheckColIdxN = 22;
                        oldCheckColIdx = 21;
                    }

                    //先檢查檔案是否正確
                    if (!(vdlModel.NewVersion && Convert.ToString(currentSheet.GetRow(0).GetCell(oldCheckColIdxN)).Equals("出貨條件")) &&
                        !(!vdlModel.NewVersion && Convert.ToString(currentSheet.GetRow(0).GetCell(oldCheckColIdx)).Equals("出貨條件")))
                    {
                        importResult.FailCount++;
                        failInfo = new ImportFailInfo
                        {
                            DealUniqueId = string.Empty,
                            OrderId = string.Empty,
                            OrderGuid = Guid.Empty,
                            Message = "檔案格式不符，請重新匯入正確清冊檔"
                        };
                        failInfos.Add(failInfo);

                        importResult.FailInfos = failInfos;
                        if (failInfos.Count > 0)
                        {
                            importResult.FailFileName = WriteToImportFailFile(importResult);
                        }

                        importResult.RemindingInfos = remindInfos;
                        return importResult;
                    }
                    #endregion

                    if (vdlModel.DealType == VbsDealType.PiinLife)
                    {
                        //抓取未退貨(包含部分退貨)的訂單 完全退貨不需填寫出貨資訊
                        cashTrustLogs = _mp.CashTrustLogGetAllListByHiDealProductId(int.Parse(vdlModel.DealUniqueId))
                                            .Where(x => x.Status == (int)TrustStatus.Initial ||
                                                        x.Status == (int)TrustStatus.Trusted ||
                                                        x.Status == (int)TrustStatus.Verified)
                                            .ToLookup(x => x.OrderGuid);
                        //抓取包含出貨資訊訂單資料
                        orderShipList = _op.ViewOrderShipListGetListByProductGuid(Guid.Parse(vdlModel.DealId), OrderClassification.HiDeal)
                                            .ToLookup(x => x.OrderId);
                    }
                    else if (vdlModel.DealType == VbsDealType.Ppon)
                    {
                        //抓取未退貨(包含部分退貨)的訂單 完全退貨不需填寫出貨資訊
                        cashTrustLogs = _mp.CashTrustLogGetListByBid(dealGuids)
                                            .Where(x => x.Status == (int)TrustStatus.Initial ||
                                                        x.Status == (int)TrustStatus.Trusted ||
                                                        x.Status == (int)TrustStatus.Verified)
                                            .ToLookup(x => x.OrderGuid);
                        //抓取包含出貨資訊訂單資料
                        orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids)
                                            .ToLookup(x => x.OrderId);
                    }
                    //抓取該檔次之狀態為處理中之退貨申請單
                    var returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                        .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                                    x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                                    x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                                    x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                                    x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                                    x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                        .ToLookup(x => x.OrderGuid);

                    var exchangeLogs = GetHasExchangeLogOrderInfos(dealGuids);

                    var shipTime = DateTime.MinValue;
                    var orderId = string.Empty;
                    var tempOrderId = string.Empty;
                    var shipNo = string.Empty;
                    var shipMemo = string.Empty;
                    int shipCompanyId;
                    int uniqueId;
                    int orderClassification = 0;
                    int shipMemoLimit = 30;
                    int newVersionIndex = vdlModel.NewVersion ? 1 : 0;
                    if (VBSFacade.IsEnabledVbsShipRule2k18())
                    {
                        newVersionIndex += 1;
                    }

                    #region excel column index setup
                                        
                    const int dealIdColumnIndex = 0;                        //檔號
                    const int orderIdColumnIndex = 1;                       //訂單編號
                    int shipTimeColumnIndex = 15 + newVersionIndex;         //出貨日期
                    int shipCompanyIdColumnIndex = 16 + newVersionIndex;    //物流代號
                    int shipNoColumnIndex = 18 + newVersionIndex;           //貨運單號
                    int shipMemoColumnIndex = 19 + newVersionIndex;         //出貨備註

                    #endregion excel column index setup

                    for (int k = 1; k <= currentSheet.LastRowNum; k++)
                    {
                        row = currentSheet.GetRow(k);

                        if (row == null)
                        {
                            continue;
                        }
                        if (row.GetCell(dealIdColumnIndex) == null || row.GetCell(dealIdColumnIndex).ToString() == string.Empty)
                        {
                            continue;
                        }
                        if (!int.TryParse(row.GetCell(dealIdColumnIndex).ToString(), out uniqueId))
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = string.Empty,
                                OrderId = string.Empty,
                                OrderGuid = Guid.Empty,
                                Message = "檔案格式不符，請重新下載清冊匯出檔"
                            };
                            failInfos.Add(failInfo);
                            break;
                        }
                        
                        orderId = (row.GetCell(orderIdColumnIndex) == null || row.GetCell(orderIdColumnIndex).ToString() == string.Empty) ? string.Empty : row.GetCell(orderIdColumnIndex).StringCellValue.Trim();
                        //訂單編號不為null才需處理
                        if (string.IsNullOrEmpty(orderId))
                        {
                            continue;
                        }

                        //新版清冊會將檔次依照規格數量顯示, 故匯入時只取同訂單的第一筆訂單資料回寫宅配資訊
                        if (vdlModel.NewVersion)
                        {
                            if (tempOrderId == orderId)
                            {
                                continue;
                            }

                            tempOrderId = orderId;
                        }

                        if (!dealIds.Contains(uniqueId))
                        {//確認匯入檔案之檔次內容是否與目前檔次相符
                            importResult.FailCount++;
                            Order order = _op.OrderGet(Order.Columns.OrderId, orderId);
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = order.Guid,
                                Message = "匯入檔案與該檔次不符或匯入之檔次非[出貨中]狀態"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        //多檔次可能有重複訂單編號存在 故須加上bid篩選
                        var os = orderShipList.Contains(orderId) ? orderShipList[orderId].FirstOrDefault(x => dealGuids.Contains(x.ProductGuid)) : null;
                        //檢查該訂單編號是否存在
                        if (os == null)
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os != null ? os.OrderGuid : Guid.Empty,
                                Message = "檔次無符合該筆訂單資料"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }
                        //檢查是否為24H出貨就排除
                        if(os.ProductDeliveryType == (int)ProductDeliveryType.Wms)
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os != null ? os.OrderGuid : Guid.Empty,
                                Message = "該筆為24H到貨訂單資料"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        var dealInfo = dealInfos.First(x => x.DealId == os.ProductGuid);
                        var deliveryStartTime = dealInfo.ShipPeriodStartTime;
                        var ctl = cashTrustLogs.Contains(os.OrderGuid) ? cashTrustLogs[os.OrderGuid].FirstOrDefault() : null;
                        //檢查是否為退貨訂單 退貨訂單則不須異動出貨資料 也不須顯示失敗訊息
                        if (ctl == null)
                        {
                            continue;
                        }

                        if (returnLists.Contains(os.OrderGuid))
                        {
                            //訂單有退貨申請單且狀態為處理中 不能使用批次管理出貨
                            if (returnLists[os.OrderGuid].Any(x => x.VendorProgressStatus == null ||
                                                                    x.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                    x.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                    x.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                    x.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed))
                            {
                                importResult.FailCount++;
                                failInfo = new ImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    OrderGuid = os.OrderGuid,
                                    Message = "此訂單尚有未處理的退貨申請。請至出貨管理中進行單筆管理。"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                        }
                        
                        #region 出貨日期格式檢查

                        shipTime = DateTime.MinValue;
                        if (row.GetCell(shipTimeColumnIndex) != null)
                        {
                            switch (row.GetCell(shipTimeColumnIndex).CellType)
                            {//檢查出貨日期格式 於判斷是否為空值之前 避免有輸入資料大於日期數值轉換限制情況時 而導致系統錯誤發生
                                case CellType.STRING:
                                case CellType.FORMULA:
                                    if (!string.IsNullOrEmpty(row.GetCell(shipTimeColumnIndex).StringCellValue))
                                    {
                                        if (!DateTime.TryParse(row.GetCell(shipTimeColumnIndex).StringCellValue, out shipTime))
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                OrderGuid = os.OrderGuid,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    break;

                                case CellType.NUMERIC:
                                    if (DateUtil.IsCellDateFormatted(row.GetCell(shipTimeColumnIndex)) && row.GetCell(shipTimeColumnIndex).NumericCellValue < 2958465) //數值(距1904天數)轉換日期限制 9999/12/31
                                    {
                                        if (!DateTime.TryParse(row.GetCell(shipTimeColumnIndex).DateCellValue.ToString("yyyy/MM/dd"), out shipTime))
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                OrderGuid = os.OrderGuid,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        if (row.GetCell(shipTimeColumnIndex).NumericCellValue > 0)
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                OrderGuid = os.OrderGuid,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }

                        #endregion 出貨日期格式檢查

                        #region 運單編號格式檢查

                        shipNo = string.Empty;
                        if (row.GetCell(shipNoColumnIndex) != null)
                        {
                            switch (row.GetCell(shipNoColumnIndex).CellType)
                            {//檢查運單編號格式 避免有輸入格式為公式 而導致系統錯誤發生
                                case CellType.NUMERIC:
                                    shipNo = row.GetCell(shipNoColumnIndex).NumericCellValue.ToString();
                                    break;

                                case CellType.FORMULA:
                                default:
                                    shipNo = row.GetCell(shipNoColumnIndex).ToString();
                                    break;
                            }
                        }

                        #endregion 運單編號格式檢查

                        #region 物流代號格式檢查

                        shipCompanyId = 0;
                        if (row.GetCell(shipCompanyIdColumnIndex) != null && !string.IsNullOrEmpty(row.GetCell(shipCompanyIdColumnIndex).ToString()))
                        {
                            if (!int.TryParse(row.GetCell(shipCompanyIdColumnIndex).ToString(), out shipCompanyId)
                             || !shipCompanyInfo.Any(x => x.Id.Equals(shipCompanyId)))
                            {
                                importResult.FailCount++;
                                failInfo = new ImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    OrderGuid = os.OrderGuid,
                                    Message = "格式錯誤:物流代號"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                        }

                        #endregion 物流代號格式檢查

                        #region 物流代號與運單編號數量檢查
                        var shipCompany = _op.ShipCompanyGet(shipCompanyId);
                        if (shipCompany.IsLoaded)
                        {

                            if (shipCompany.Min != 0 && shipCompany.Min > shipNo.Length)
                            {
                                importResult.FailCount++;
                                failInfo = new ImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    OrderGuid = os.OrderGuid,
                                    Message = "貨運單號錯誤"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                            
                        }

                        #endregion 物流代號與運單編號數量檢查

                        if (shipTime.Equals(DateTime.MinValue) && shipCompanyId.Equals(0) && string.IsNullOrEmpty(shipNo))
                        {//檢查出貨日期、物流代號、貨運單號是否皆填寫 若皆未填寫視為不處理 也不須顯示失敗訊息
                            importResult.NotYetCount++;
                            continue;
                        }

                        if (shipTime.Equals(DateTime.MinValue) || shipCompanyId.Equals(0) || string.IsNullOrEmpty(shipNo))
                        {//檢查出貨日期、物流代號、貨運單號是否皆填寫 任一欄為空白都需顯示失敗訊息
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os.OrderGuid,
                                Message = "出貨資訊不可有空白"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        #region 出貨備註

                        if (row.GetCell(shipMemoColumnIndex) == null || row.GetCell(shipMemoColumnIndex).ToString() == string.Empty)
                        {
                            shipMemo = string.Empty;
                        }
                        else
                        {
                            switch (row.GetCell(shipMemoColumnIndex).CellType)
                            {
                                case CellType.NUMERIC:
                                    shipMemo = row.GetCell(shipMemoColumnIndex).NumericCellValue.ToString().Trim();
                                    break;

                                default:
                                    shipMemo = row.GetCell(shipMemoColumnIndex).StringCellValue.Trim();
                                    break;
                            }
                        }

                        if (shipMemo.Length > shipMemoLimit)
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os.OrderGuid,
                                Message = "出貨備註過長，只限輸入30個字。"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        #endregion 出貨備註

                        shipTime = DateTime.Parse(shipTime.ToString("yyyy/MM/dd"));
                        //出貨日期、物流公司及貨運單號都未變更 則不須進行異動處理
                        if (shipCompanyId.Equals(os.ShipCompanyId) && shipTime.Equals(os.ShipTime) &&
                            shipNo.Equals(os.ShipNo) && shipMemo.Equals(os.ShipMemo))
                        {
                            importResult.NotYetCount++;
                            continue;
                        }

                        if (exchangeLogs.Contains(os.OrderGuid))
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os.OrderGuid,
                                Message = "此訂單已有換貨紀錄，不允許異動原始出貨資訊。"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        if (OrderShipUtility.CheckDealShipNoExist(dealInfo.DealId, (OrderClassification)os.OrderClassification, os.OrderGuid, shipNo))
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os.OrderGuid,
                                Message = "貨運單號重複"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        //若物流公司選擇[其他] 需顯示提示訊息(非錯誤訊息) 仍可儲存
                        if (shipCompanyId.Equals(17))
                        {
                            if (remindInfos.Count == 0 || !remindInfos.Any(x => x.Type.Equals(RemindType.ShipCompanyIdOther)))
                            {
                                remindInfo = new RemindingInfo
                                {
                                    Type = RemindType.ShipCompanyIdOther,
                                    Message = "請聯絡服務專員協助您新增配合的物流公司：|" + _conf.BusinessServiceOnDutyTel
                                };
                                remindInfos.Add(remindInfo);
                            }
                        }

                        //出貨日期輸入檢查
                        if (shipTime > DateTime.Now.Date)
                        {
                            importResult.FailCount++;
                            failInfo = new ImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                OrderGuid = os.OrderGuid,
                                Message = "您無法儲存超過今日以後的出貨日"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        if (os.ShipTime.HasValue)
                        {//出貨資訊已存在
                            if (!os.ShipTime.Value.Equals(shipTime))
                            {//出貨日期異動檢查
                                if (ctl.Status.Equals((int)TrustStatus.Verified))
                                {
                                    importResult.FailCount++;
                                    failInfo = new ImportFailInfo
                                    {
                                        DealUniqueId = uniqueId.ToString(),
                                        OrderId = orderId,
                                        OrderGuid = os.OrderGuid,
                                        Message = "修改出貨日期已禁止，若需修改請洽客服人員"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                                if (shipTime < DateTime.Now.Date)
                                {
                                    importResult.FailCount++;
                                    failInfo = new ImportFailInfo
                                    {
                                        DealUniqueId = uniqueId.ToString(),
                                        OrderId = orderId,
                                        OrderGuid = os.OrderGuid,
                                        Message = "修改的出貨日期不能小於今日日期，若需修改請洽客服人員"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                            }
                        }
                        else
                        {//出貨資訊未存在或清空
                            if (shipTime < deliveryStartTime)
                            {
                                importResult.FailCount++;
                                failInfo = new ImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    OrderGuid = os.OrderGuid,
                                    Message = "輸入的出貨日期不能小於配送開始日期，若有其他問題請洽客服人員"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                        }

                        //出貨資訊更新
                        var orderShip = new OrderShip();
                        orderShip.ModifyId = VbsCurrent.AccountId;
                        orderShip.ModifyTime = DateTime.Now;
                        orderShip.ShipCompanyId = shipCompanyId;
                        orderShip.ShipNo = shipNo;
                        orderShip.ShipTime = shipTime;
                        if (os.OrderShipId.HasValue)//由order_ship id判斷為新增或修改
                        {//order_ship 修改
                            orderShip.Id = os.OrderShipId.Value;
                            orderShip.ShipMemo = shipMemo;
                            if (!OrderShipUtility.OrderShipUpdate(orderShip))
                            {
                                importResult.FailCount++;
                                failInfo = new ImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    OrderGuid = os.OrderGuid,
                                    Message = "修改失敗，請聯絡客服人員"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                            else
                            {
                                importResult.UpdateCount++;
                                string newVersionDesc = vdlModel.NewVersion ? "(新版)" : string.Empty;
                                CommonFacade.AddAudit(os.OrderGuid.ToString(), AuditType.Order, string.Format("{0}{1}", "匯入待出貨清冊修改成功", newVersionDesc), VbsCurrent.AccountId, true);
                            }
                        }
                        else
                        {//order_ship 新增
                            if (vdlModel.DealType == VbsDealType.Ppon)
                            {
                                orderClassification = (int)OrderClassification.LkSite;
                            }
                            else if (vdlModel.DealType == VbsDealType.PiinLife)
                            {
                                orderClassification = (int)OrderClassification.HiDeal;
                            }

                            //檢查出貨資訊是否已存在(避免重複按下匯入按鈕)
                            if (OrderShipUtility.CheckShipDataExist(os.OrderGuid, (OrderClassification)orderClassification))
                            {
                                importResult.InsertCount++;
                                continue;
                            }
                            else
                            {
                                orderShip.OrderGuid = os.OrderGuid;
                                orderShip.ShipMemo = shipMemo;
                                orderShip.OrderClassification = orderClassification;
                                orderShip.CreateId = orderShip.ModifyId;
                                orderShip.CreateTime = orderShip.ModifyTime;
                                orderShip.Type = (int)OrderShipType.Normal;

                                //宅配已配送出貨要發推播給使用者(僅只有新增的第一次需要push)
                                bool IsCanPush = false;
                                var order = _op.OrderGet(os.OrderGuid);
                                var m = _mp.MemberGet(order.UserId);
                                if (!orderShip.SentArrivalNoticePush)
                                {
                                    IsCanPush = true;
                                    orderShip.SentArrivalNoticePush = true;//寧願漏發也不要不正確發放
                                }

                                if (!OrderShipUtility.OrderShipSet(orderShip))
                                {
                                    importResult.FailCount++;
                                    failInfo = new ImportFailInfo
                                    {
                                        DealUniqueId = uniqueId.ToString(),
                                        OrderId = orderId,
                                        OrderGuid = os.OrderGuid,
                                        Message = "新增失敗，請聯絡客服人員"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                                else
                                {
                                    importResult.InsertCount++;
                                    string newVersionDesc = vdlModel.NewVersion ? "(新版)" : string.Empty;
                                    CommonFacade.AddAudit(os.OrderGuid.ToString(), AuditType.Order, string.Format("{0}{1}", "匯入待出貨清冊成功", newVersionDesc), VbsCurrent.AccountId, true);

                                    if (IsCanPush)
                                    {
                                        var msg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", order.OrderId);
                                        NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, msg,
                                            os.OrderGuid);
                                        logger.Info("宅配已出貨 VendorBillingSystemShip.ProcessImportOrderShip: " +
                                                    string.Format("UserId={0}, pmsg={1}, Guid={2}", order.UserId, msg,
                                                        order.OrderId));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                importResult.FailCount++;
                failInfo = new ImportFailInfo
                {
                    DealUniqueId = string.Empty,
                    OrderId = string.Empty,
                    OrderGuid = Guid.Empty,
                    Message = "請選擇檔案"
                };
                failInfos.Add(failInfo);
            }

            importResult.FailInfos = failInfos;
            if (failInfos.Count > 0)
            {
                importResult.FailFileName = WriteToImportFailFile(importResult);

                string newVersionDesc = vdlModel.NewVersion ? "(新版)" : string.Empty;
                foreach (ImportFailInfo  info in failInfos)
                {
                    CommonFacade.AddAudit(info.OrderGuid.ToString(), AuditType.Order, string.Format("{0}{1}{2}", "匯入待出貨清冊失敗", newVersionDesc,";" + info.Message), VbsCurrent.AccountId, true);
                }
            }
            else
            {
                LogShipFile(vdlModel.NewVersion);
            }

            importResult.RemindingInfos = remindInfos;

            return importResult;
        }

        private string WriteToImportFailFile(ImportResult result)
        {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("匯入失敗結果");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("匯入時間：" + result.ImportTime.ToString("yyyy/MM/dd HH:mm"));
            cols.CreateCell(1).SetCellValue("新增成功：" + result.InsertCount);
            cols.CreateCell(2).SetCellValue("更新成功：" + result.UpdateCount);
            cols.CreateCell(3).SetCellValue("匯入失敗：" + result.FailCount);
            cols = sheet.CreateRow(1);
            cols = sheet.CreateRow(2);
            cols.CreateCell(0).SetCellValue("檔號");
            cols.CreateCell(1).SetCellValue("訂單編號");
            cols.CreateCell(2).SetCellValue("匯入失敗原因");
            int rowIdx = 3;
            foreach (var item in result.FailInfos)
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.DealUniqueId);
                row.CreateCell(1).SetCellValue(item.OrderId);
                row.CreateCell(2).SetCellValue(item.Message);

                rowIdx++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            string filePath = Path.Combine(HttpContext.Server.MapPath("~" + _conf.ShipBatchImport + "/"), VbsCurrent.AccountId, result.ImportIndentity);
            string fileName = string.Format("{0}\\{1}.xls", filePath, result.ImportTime.ToString("yyyyMMdd_HHmm"));
            // Create the path if it doesn't exist.
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            int i = 1;
            while (System.IO.File.Exists(fileName))
            {
                i++;
                if (i == 2)
                {
                    fileName = string.Format("{0}({1}).xls", fileName.Substring(0, fileName.Length - 4), i);
                }
                else
                {
                    fileName = string.Format("{0}({1}).xls", fileName.Substring(0, fileName.Length - 7), i);
                }
            }
            FileStream file = new FileStream(fileName, FileMode.CreateNew);
            workbook.Write(file);
            fileName = string.Format("{0}{1}/{2}/{3}/{4}", WebUtility.GetSiteRoot(), _conf.ShipBatchImport, VbsCurrent.AccountId, result.ImportIndentity, Path.GetFileName(file.Name));
            file.Close();

            return fileName;
        }

        private List<ImportFailFileInfo> GetImportFailFileInfo(string path)
        {
            var fileInfos = new List<ImportFailFileInfo>();
            string filePath = Path.Combine(HttpContext.Server.MapPath("~" + _conf.ShipBatchImport + "/"), path);
            string downloadPath = WebUtility.GetSiteRoot() + _conf.ShipBatchImport + "/" + path;
            if (Directory.Exists(filePath))
            {
                DirectoryInfo dir = new DirectoryInfo(filePath);
                FileInfo[] fileList = dir.GetFiles("*.xls");
                if (fileList.Any())
                {
                    foreach (var file in fileList.OrderByDescending(x => x.LastWriteTime).Take(6))
                    {
                        var fileInfo = new ImportFailFileInfo
                        {
                            FileName = file.Name,
                            FilePath = downloadPath + "/" + file.Name
                        };
                        fileInfos.Add(fileInfo);
                    }
                }
            }

            return fileInfos;
        }

        #endregion 批次出貨管理

        #region 權限檢查

        private List<IVbsVendorAce> GetAllowedDeals(string accountId, Guid? merchandiseGuid)
        {
            var allowedDeals = (new VbsVendorAclMgmtModel(accountId)).GetAllowedDealUnits(merchandiseGuid, DeliveryType.ToHouse, VbsRightFlag.Verify).ToList();

            return allowedDeals;
        }

        private bool CheckIsAllowedDeal(bool is17LifeEmployee, string accountId, Guid merchandiseGuid)
        {
            if (!is17LifeEmployee)
            {
                return GetAllowedDeals(accountId, merchandiseGuid).Count > 0;
            }

            return true;
        }

        #endregion 權限檢查

        #region 退貨管理

        private static PagerList<VbsReturnOrderListInfo> GetPagerVbsReturnOrderInfos(
            List<VbsReturnOrderListInfo> infos, ViewVendorProgressState? viewVendorProgressState, string selQueryOption, string queryKeyWord,
            int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in infos select t);

            if (viewVendorProgressState.Equals(ViewVendorProgressState.Process))
            {
                query = query.Where(t => !t.ProgressStatus.Equals(ProgressStatus.Canceled) &&
                                        (t.VendorProgressStatus.Equals(VendorProgressStatus.Processing) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.Retrieving) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.Unreturnable) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.UnreturnableProcessed)))
                             .ToList();
            }
            else if (viewVendorProgressState.Equals(ViewVendorProgressState.Finished))
            {
                query = query.Where(t => t.ProgressStatus.Equals(ProgressStatus.Canceled) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.CompletedAndRetrievied) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.CompletedAndNoRetrieving) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.CompletedAndUnShip) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.CompletedByCustomerService) ||
                                         t.VendorProgressStatus.Equals(VendorProgressStatus.Automatic))
                             .ToList();
            }

            if (!string.IsNullOrEmpty(queryKeyWord))
            {
                switch (selQueryOption)
                {
                    case "orderId":
                        query = query.Where(t => t.OrderId.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    case "recipientName":
                        query = query.Where(t => t.RecipientName.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    case "recipientTel":
                        query = query.Where(t => t.RecipientTel.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    default:
                        break;
                };
            }

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case VbsReturnOrderListInfo.Columns.OrderId:
                        query = query.OrderBy(t => t.OrderId);
                        break;

                    case VbsReturnOrderListInfo.Columns.RecipientName:
                        query = query.OrderBy(t => t.RecipientName);
                        break;

                    case VbsReturnOrderListInfo.Columns.RecipientTel:
                        query = query.OrderBy(t => t.RecipientTel);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnApplicationTime:
                        query = query.OrderBy(t => t.ReturnApplicationTime);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnReason:
                        query = query.OrderBy(t => t.ReturnReason);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnItemDescription:
                        query = query.OrderBy(t => t.ReturnItemDescription);
                        break;

                    case VbsReturnOrderListInfo.Columns.ShipTime:
                        query = query.OrderBy(t => t.ShipTime);
                        break;

                    case VbsReturnOrderListInfo.Columns.VendorProgressStatus:
                        query = query.OrderBy(t => t.VendorProgressStatus);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case VbsReturnOrderListInfo.Columns.OrderId:
                        query = query.OrderByDescending(t => t.OrderId);
                        break;

                    case VbsReturnOrderListInfo.Columns.RecipientName:
                        query = query.OrderByDescending(t => t.RecipientName);
                        break;

                    case VbsReturnOrderListInfo.Columns.RecipientTel:
                        query = query.OrderByDescending(t => t.RecipientTel);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnApplicationTime:
                        query = query.OrderByDescending(t => t.ReturnApplicationTime);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnReason:
                        query = query.OrderByDescending(t => t.ReturnReason);
                        break;

                    case VbsReturnOrderListInfo.Columns.ReturnItemDescription:
                        query = query.OrderByDescending(t => t.ReturnItemDescription);
                        break;

                    case VbsReturnOrderListInfo.Columns.ShipTime:
                        query = query.OrderByDescending(t => t.ShipTime);
                        break;

                    case VbsReturnOrderListInfo.Columns.VendorProgressStatus:
                        query = query.OrderByDescending(t => t.VendorProgressStatus);
                        break;

                    default:
                        break;
                }
            }

            var paging = new PagerList<VbsReturnOrderListInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.Count());

            return paging;
        }

        private Dictionary<int, string> GetReturnOrderProductItemDescription(IEnumerable<OrderProductOptionInfo> orderOptionInfo)
        {
            return orderOptionInfo
                    .GroupBy(x => x.ReturnFormId)
                    .ToDictionary(x => x.Key,
                                  x => string.Join("\n", x.Select(item => string.Format("{0} {1}", item.OptionDescription, item.Quantity))));
        }

        private List<VbsReturnOrderListInfo> GetReturnOrderList(List<ViewOrderReturnFormList> returnList)
        {
            List<VbsReturnOrderListInfo> infos = new List<VbsReturnOrderListInfo>();
            if (returnList.Count > 0)
            {
                foreach (ViewOrderReturnFormList order in returnList)
                {
                    ReturnFormEntity rfEntity = ReturnFormRepository.FindById(order.ReturnFormId);
                    var info = new VbsReturnOrderListInfo
                    {
                        OrderId = order.OrderId,
                        OrderGuid = order.OrderGuid,
                        ReturnApplicationTime = order.ReturnApplicationTime,
                        OrderItemDescription = rfEntity.GetReturnSpec(),
                        ReturnReason = order.ReturnReason,
                        VendorProgressStatus = order.VendorProgressStatus.HasValue
                                                ? (VendorProgressStatus)order.VendorProgressStatus.Value
                                                : VendorProgressStatus.Processing,
                        ReturnItemDescription = rfEntity.GetCollectedSpec(),
                        VendorProgressCompletedTime = order.ModifyTime.HasValue
                                                        ? order.ModifyTime.Value
                                                        : DateTime.MinValue
                    };

                    infos.Add(info);
                }
            }

            return infos;
        }

        private bool? ValidateVendorProgressState(ProgressStatus progressStatus, VendorProgressStatus vendorProgressStatus)
        {
            //依據退貨管理列表檢視退貨狀態的規則 判斷是否 已完成退貨管理
            if (progressStatus != ProgressStatus.Canceled &&
                (vendorProgressStatus == VendorProgressStatus.Processing ||
                 vendorProgressStatus == VendorProgressStatus.Retrieving ||
                 vendorProgressStatus == VendorProgressStatus.Unreturnable ||
                 vendorProgressStatus == VendorProgressStatus.UnreturnableProcessed))
            {
                return false;
            }

            if (progressStatus == ProgressStatus.Canceled ||
                vendorProgressStatus == VendorProgressStatus.Automatic ||
                vendorProgressStatus == VendorProgressStatus.CompletedAndNoRetrieving ||
                vendorProgressStatus == VendorProgressStatus.CompletedAndRetrievied ||
                vendorProgressStatus == VendorProgressStatus.CompletedAndUnShip ||
                vendorProgressStatus == VendorProgressStatus.CompletedByCustomerService)
            {
                return true;
            }

            //其餘為不顯示於退貨管理之狀態
            return null;
        }

        #endregion 退貨管理

        #region 照會

        private int[] GetOrderUserMemoListCount(IEnumerable<DealSaleInfo> dealInfos, VbsDealType type)
        {
            //撈取該檔次訂單之cash_trust_log(抓取trust_id 以抓取cash_trust_status_log)
            ILookup<Guid, CashTrustLog> cashTrustLogs = null;
            var dealGuids = dealInfos.Select(x => x.DealId);
            var dealIds = dealInfos.Select(x => x.DealUniqueId);
            if (type == VbsDealType.Ppon)
            {
                cashTrustLogs = _mp.CashTrustLogGetListByBid(dealGuids)
                                    .ToLookup(x => x.OrderGuid);
            }
            else if (type == VbsDealType.PiinLife)
            {
                var productId = dealIds.First();
                cashTrustLogs = _mp.CashTrustLogGetAllListByHiDealProductId(int.Parse(productId.ToString()))
                                    .ToLookup(x => x.OrderGuid);
            }
            var orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids)
                                .GroupBy(x => x.OrderGuid)
                                .ToDictionary(x => x.Key, x => x.First());
            //抓取訂單備註異動紀錄list
            var orderUserMemoLists = _op.OrderUserMemoListGetList(orderShipList.Keys)
                                        .Where(x => x.UserMemo.Contains("**照會成功，請正常出貨**"))
                                        .GroupBy(x => x.OrderGuid)
                                        .ToDictionary(x => x.Key, x => x.ToList());
            int countShipRefer = 0;
            int countShipReferUnShip = 0;
            int[] returnCountShipRefer = { 0, 0 };

            foreach (var orderGuid in orderUserMemoLists.Keys)
            {
                //若對應不到cash_trust_log資料 則不顯示該筆訂單
                if (!cashTrustLogs.Contains(orderGuid))
                {
                    continue;
                }
                if (cashTrustLogs[orderGuid].Any(x => x.Status == (int)TrustStatus.Refunded ||
                                                      x.Status == (int)TrustStatus.Returned))
                {
                    //出貨管理顯示的訂單不包含退貨
                    continue;
                }

                if (orderShipList.ContainsKey(orderGuid))
                {
                    countShipRefer++;

                    //數量只顯示『照會完成但尚未開始出貨』的筆數，因此只要沒有出貨日期都算還沒出貨
                    if (!orderShipList[orderGuid].ShipTime.HasValue)
                    {
                        countShipReferUnShip++;
                    }
                }
            }
            returnCountShipRefer[0] = countShipReferUnShip;
            returnCountShipRefer[1] = countShipRefer;
            return returnCountShipRefer;
        }

        #endregion 照會

        #region 換貨管理

        private static PagerList<VbsExchangeOrderListInfo> GetPagerVbsExchangeOrderInfos(
            List<VbsExchangeOrderListInfo> infos, ViewVendorProgressState? viewVendorProgressState, string selQueryOption, string queryKeyWord,
            int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in infos select t);

            if (viewVendorProgressState.Equals(ViewVendorProgressState.Process))
            {
                query = query.Where(t => !t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeCancel) &&
                                         !t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeSuccess) &&
                                         !t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeFailure) &&
                                         !t.VendorProgressStatus.Equals(ExchangeVendorProgressStatus.ExchangeCompleted))
                             .ToList();
            }
            else if (viewVendorProgressState.Equals(ViewVendorProgressState.Finished))
            {
                query = query.Where(t => t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeCancel) ||
                                         t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeSuccess) ||
                                         t.ExchangeStatus.Equals(OrderReturnStatus.ExchangeFailure) ||
                                         t.VendorProgressStatus.Equals(ExchangeVendorProgressStatus.ExchangeCompleted))
                             .ToList();
            }

            if (!string.IsNullOrEmpty(queryKeyWord))
            {
                switch (selQueryOption)
                {
                    case "orderId":
                        query = query.Where(t => t.OrderId.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    case "recipientName":
                        query = query.Where(t => t.RecipientName.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    case "recipientTel":
                        query = query.Where(t => t.RecipientTel.Equals(queryKeyWord))
                                     .ToList();
                        break;

                    default:
                        break;
                };
            }

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case VbsExchangeOrderListInfo.Columns.OrderId:
                        query = query.OrderBy(t => t.OrderId);
                        break;

                    case VbsExchangeOrderListInfo.Columns.RecipientName:
                        query = query.OrderBy(t => t.RecipientName);
                        break;

                    case VbsExchangeOrderListInfo.Columns.RecipientTel:
                        query = query.OrderBy(t => t.RecipientTel);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeApplicationTime:
                        query = query.OrderBy(t => t.ExchangeApplicationTime);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeReason:
                        query = query.OrderBy(t => t.ExchangeReason);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeMessage:
                        query = query.OrderBy(t => t.ExchangeMessage);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ShipTime:
                        query = query.OrderBy(t => t.ShipTime);
                        break;

                    case VbsExchangeOrderListInfo.Columns.VendorProgressStatus:
                        query = query.OrderBy(t => t.VendorProgressStatus);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case VbsExchangeOrderListInfo.Columns.OrderId:
                        query = query.OrderByDescending(t => t.OrderId);
                        break;

                    case VbsExchangeOrderListInfo.Columns.RecipientName:
                        query = query.OrderByDescending(t => t.RecipientName);
                        break;

                    case VbsExchangeOrderListInfo.Columns.RecipientTel:
                        query = query.OrderByDescending(t => t.RecipientTel);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeApplicationTime:
                        query = query.OrderByDescending(t => t.ExchangeApplicationTime);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeReason:
                        query = query.OrderByDescending(t => t.ExchangeReason);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ExchangeMessage:
                        query = query.OrderByDescending(t => t.ExchangeMessage);
                        break;

                    case VbsExchangeOrderListInfo.Columns.ShipTime:
                        query = query.OrderByDescending(t => t.ShipTime);
                        break;

                    case VbsExchangeOrderListInfo.Columns.VendorProgressStatus:
                        query = query.OrderByDescending(t => t.VendorProgressStatus);
                        break;

                    default:
                        break;
                }
            }

            var paging = new PagerList<VbsExchangeOrderListInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.Count());

            return paging;
        }

        private bool? ValidateVendorProgressState(OrderReturnStatus progressStatus, ExchangeVendorProgressStatus vendorProgressStatus)
        {
            //依據換貨管理列表檢視換貨狀態的規則 判斷是否 已完成換貨管理
            if (progressStatus != OrderReturnStatus.ExchangeCancel &&
                progressStatus != OrderReturnStatus.ExchangeSuccess &&
                progressStatus != OrderReturnStatus.ExchangeFailure &&
                vendorProgressStatus != ExchangeVendorProgressStatus.ExchangeCompleted)
            {
                return false;
            }

            if (progressStatus == OrderReturnStatus.ExchangeCancel ||
                progressStatus == OrderReturnStatus.ExchangeSuccess ||
                progressStatus == OrderReturnStatus.ExchangeFailure ||
                vendorProgressStatus == ExchangeVendorProgressStatus.ExchangeCompleted)
            {
                return true;
            }

            //其餘為不顯示於換貨管理之狀態
            return null;
        }

        private IEnumerable<VendorProcessLog> GetReturnStatusLog(int returnId)
        {
            var statusLog = _op.ReturnFormStatusLogGetListByReturnFormId(returnId)
                .Where(x => !x.ModifyId.Contains("@") && x.ModifyId != "sys")
                .OrderByDescending(x => x.ModifyTime)
                .ToList();

            List<VendorProcessLog> logs = new List<VendorProcessLog>();
            int i = 0;
            statusLog.ForEach(x => {
                logs.Add(new VendorProcessLog
                {
                    CreateTime = x.ModifyTime,
                    Message = string.Format("帳號:{0} 將狀態從「{1}」更改至「{2}」，備註:{3}", x.ModifyId, (i != statusLog.Count() - 1 ? Helper.GetLocalizedEnum((VendorProgressStatus)statusLog[i + 1].VendorProgressStatus) : string.Empty), Helper.GetLocalizedEnum((VendorProgressStatus)x.VendorProgressStatus), x.VendorMemo ?? string.Empty)
                });
                i++;
            });

            return logs;
        }

        private IEnumerable<VendorProcessLog> GetExchangeStatusLog(int exchangeLogId, bool messageUpdateOnly = false)
        {
            var statusLog = _op.OrderStatusLogGetVendorNotifyList(exchangeLogId)
                .Where(x => !string.IsNullOrEmpty(x.Message))
                .OrderByDescending(x=>x.CreateTime)
                .ToList();
            if (messageUpdateOnly)
            {
                statusLog = statusLog.Where(x => x.MessageUpdate).ToList();
            }
            return statusLog.Select(x => new VendorProcessLog
            {
                CreateTime = x.CreateTime,
                Message = x.Message
            })
                .ToList();
        }

        private ProductOrderShip GetExchangeOrderShipInfo(int? shipId)
        {
            var exchangeShipInfo = new ProductOrderShip();

            if (shipId.HasValue)
            {
                var os = _op.GetOrderShipById(shipId.Value);
                if (os.IsLoaded)
                {
                    exchangeShipInfo = new ProductOrderShip {
                        OrderGuid = os.OrderGuid,
                        OrderClassification = os.OrderClassification,
                        OrderShipId = os.Id,
                        ShipCompanyId = os.ShipCompanyId,
                        ShipNo = os.ShipNo,
                        ShipTime = os.ShipTime,
                        ShipMemo = os.ShipMemo
                    };
                }
            }

            return exchangeShipInfo;
        }

        public bool CheckEverExchange(Guid orderGuid)
        {
            //檢查是否有未結案之換貨單
            return PponOrderManager.CheckEverExchange(orderGuid);
        }
        
        public ILookup<Guid, ViewPponOrderReturnList> GetHasExchangeLogOrderInfos(IEnumerable<Guid> dealGuids)
        {
            return PponOrderManager.GetHasExchangeLogOrderInfos(dealGuids);
        }

        #endregion 換貨管理

        #region 出貨檔案紀錄
        /// <summary>
        /// 出貨檔案紀錄
        /// </summary>
        /// <param name="isNewVersion">是否為新版檔案</param>
        private void LogShipFile(bool isNewVersion)
        {
            VbsShipfileImportLog vsil = new VbsShipfileImportLog();
            vsil.AccountId = VbsCurrent.AccountId;
            vsil.IsNewVersion = isNewVersion;
            vsil.CreateTime = DateTime.Now;

            _mp.VbsShipfileImportLogSet(vsil);
        }
        #endregion

        #region 24H到貨倉儲配送  查詢訂單配送與物流

        private string[] GetWmsOrderDeliveryFilter(WmsOrderDeliveryModel model)
        {
            if (model == null)
            {
                return null;
            }

            System.Collections.ArrayList filterList = new System.Collections.ArrayList();

            if (!string.IsNullOrEmpty(model.ProposalId))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.Pid + " = " + model.ProposalId);
            }

            if (!string.IsNullOrEmpty(model.BID))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.BusinessHourGuid + " = " + model.BID);
            }

            if (!string.IsNullOrEmpty(model.UniqueId))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.UniqueId + " = " + model.UniqueId);
            }

            if (!string.IsNullOrEmpty(model.UniqueId))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.UniqueId + " = " + model.UniqueId);
            }

            if (!string.IsNullOrEmpty(model.BrandName))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.BrandName + " like %" + model.BrandName +"%");
            }

            if (!string.IsNullOrEmpty(model.ItemName))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.ItemName + " like %" + model.ItemName +"%");
            }

            if (!string.IsNullOrEmpty(model.OrderId))
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.OrderId + " = " + model.OrderId);
            }

            if (model.OrderCreateTimeS != null && model.OrderCreateTimeE != null)
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.OrderCreateTime + " between " + ((DateTime)model.OrderCreateTimeS).ToString("yyyy/MM/dd") + " 00:00:00 " + " and " + ((DateTime)model.OrderCreateTimeE).ToString("yyyy/MM/dd") + " 23:59:59 ");
            }
            else if (model.OrderCreateTimeS == null && model.OrderCreateTimeE != null)
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.OrderCreateTime + " <= " + ((DateTime)model.OrderCreateTimeE).ToString("yyyy/MM/dd") + " 23:59:59 ");
            }
            else if (model.OrderCreateTimeS != null && model.OrderCreateTimeE == null)
            {
                filterList.Add(ViewWmsProposalOrderDelivery.Columns.OrderCreateTime + " >= " + ((DateTime)model.OrderCreateTimeS).ToString("yyyy/MM/dd") + " 00:00:00 ");
            }

            if (filterList.Count > 0)
            {
                string[] rtn = new string[filterList.Count];
                for (int i = 0; i < filterList.Count; i++)
                {
                    rtn[i] = (string)filterList[i];
                }
                return rtn;
            }

            return null;
        }


        private WmsOrderDeliveryModel GetWmsOrderDelivery(WmsOrderDeliveryModel model)
        {
            //查詢參數
            string[] queryParams = GetWmsOrderDeliveryFilter(model);

            //商家Guid
            string sellerGuids = string.Join("','", _mp.ResourceAclGetListByAccountId(VbsCurrent.AccountId)
                .Select(x => Convert.ToString(x.ResourceGuid)).Distinct().ToList());
            sellerGuids = "'" + sellerGuids + "'";

            if (model == null)
            {
                model = new WmsOrderDeliveryModel();
            }

            model.WmsOrderDeliveryList = new List<WmsOrderDeliveryData>();

            ViewWmsProposalOrderDeliveryCollection podList = _wp.GetViewWmsProposalOrderDeliveryByVbs(ViewWmsProposalOrderDelivery.Columns.Id, sellerGuids, true, queryParams);

            if (podList != null)
            {

                var podListGroup = podList.ToLookup(x => x.OrderGuid, x => x);

                foreach (var row in podListGroup)
                {
                    var result = row.FirstOrDefault();
                    if (result == null)
                    {
                        continue;
                    }

                    var itemInfoWithQtysGroup = row.ToLookup(x => x.Guid, x => x);
                    List<string> itemInfoWithQtys = new List<string>();
                    foreach (var item in itemInfoWithQtysGroup)
                    {
                        itemInfoWithQtys.Add(string.Format("（{0}） {1} ＊ {2}", item.First().PchomeProdId, item.First().OptionName, item.Select(x => x.Id).Distinct().Count()));
                    }

                    model.WmsOrderDeliveryList.Add(new WmsOrderDeliveryData()
                    {
                        OrderId = result.OrderId,
                        UniqueIdWithAppTitle = string.Format("（{0}） {1}", result.UniqueId, result.AppTitle),
                        ItemInfoWithQtys = itemInfoWithQtys,
                        ShipStatus = string.Join("\r\n", podList.Where(x => x.OrderGuid == result.OrderGuid && !string.IsNullOrEmpty(x.ShipNo)).OrderBy(o => o.Id).GroupBy(g => new { ShipNo = g.ShipNo, ShipStatus = g.ShipStatus }).Select(s => Helper.GetDescription((WmsDeliveryStatus)s.Key.ShipStatus)).ToArray()),
                        ShipCompanyWithNo = string.Join("\r\n", podList.Where(x => x.OrderGuid == result.OrderGuid && !string.IsNullOrEmpty(x.ShipNo)).OrderBy(o => o.Id).GroupBy(g => new { ShipNo = g.ShipNo, ShipCompanyName = g.ShipCompanyName }).Select(s => string.Format("（{0}） {1}", s.Key.ShipCompanyName, s.Key.ShipNo)).ToArray()),
                    });
                }
            }

            return model;
        }

        /// <summary>
        /// PCHOME查詢訂單配送與物流匯出清冊格式
        /// </summary>
        /// <param name="orderInfos"></param>
        /// <param name="inventoryType"></param>
        /// <returns></returns>
        private static MemoryStream WmsOrderDeliveryExport(List<WmsOrderDeliveryData> list)
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet("24H到貨訂單配送與物流清冊");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);

            cols.CreateCell(0).SetCellValue("訂單編號");
            cols.CreateCell(1).SetCellValue("（檔號）檔名");
            cols.CreateCell(2).SetCellValue("（倉儲商品ID）商品 ＊數量");
            cols.CreateCell(3).SetCellValue("出貨進度");
            cols.CreateCell(4).SetCellValue("（物流公司）單號");


            #endregion 表頭 及 title


            #region 格式設定

            sheet.SetColumnWidth(0, 5000);
            sheet.SetColumnWidth(1, 15000);
            sheet.SetColumnWidth(2, 15000);
            sheet.SetColumnWidth(3, 5000);
            sheet.SetColumnWidth(4, 5000);


            #endregion  格式設定



            #region 內容

            int rowIdx = 1;

            foreach (WmsOrderDeliveryData data in list)
            {
                Row row = sheet.CreateRow(rowIdx);

                //會有換行航設定高度
                row.HeightInPoints = row.HeightInPoints * data.ItemInfoWithQtys.Count();
                row.CreateCell(0).SetCellValue(data.OrderId);
                row.CreateCell(1).SetCellValue(data.UniqueIdWithAppTitle);
                row.CreateCell(2).SetCellValue(string.Join(Convert.ToChar(10).ToString(), data.ItemInfoWithQtys));
                row.GetCell(2).CellStyle.WrapText = true;//允許換行
                row.CreateCell(3).SetCellValue(data.ShipStatus);
                row.CreateCell(4).SetCellValue(data.ShipCompanyWithNo);

                rowIdx++;
            }

            #endregion 內容


            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        #endregion

        #endregion method
    }
}
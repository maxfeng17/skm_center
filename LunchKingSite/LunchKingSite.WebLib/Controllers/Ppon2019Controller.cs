﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.WebLib.Models.Ppon;
using LunchKingSite.Core.Models;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;
using log4net;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Ppon;

namespace LunchKingSite.WebLib.Controllers
{
    /// <summary>
    /// 使用2018年layout的放這邊
    /// </summary>
    [RoutePrefix("ppon")]
    public class Ppon2019Controller : Controller
    {
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ICacheProvider _cache = ProviderFactory.Instance().GetDefaultProvider<ICacheProvider>();
        private IOrderProvider _op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
        private IMemberProvider _mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger("PayService");


        [Route("~/m/p24h")]
        public ActionResult Page24hChannelForMobile(List<int> cidList)
        {
            return this.Content("<script>alert('很抱歉，這個連結已經失效。\\n\\n我們將帶您進入 17Life 首頁。'); location.href='/m';</script>", "text/html", Encoding.UTF8);
        }
        
        [Route("~/p24h")]
        public ActionResult Page24hChannel(List<int> cidList)
        {
            return this.Content("<script>alert('很抱歉，這個連結已經失效。\\n\\n我們將帶您進入 17Life 首頁。'); location.href='/';</script>", "text/html", Encoding.UTF8);
            cidList = cidList ?? new List<int>();

            List<CategoryNodeViewLite> nodeViews = new List<CategoryNodeViewLite>();
            ApiCategoryNode categoryNode = PponDealPreviewManager.GetArrival24hCategoryNode();
            foreach (ApiCategoryTypeNode typeNode in categoryNode.NodeDatas)
            {
                if (typeNode.NodeType != CategoryType.DealCategory)
                {
                    continue;
                }                
                foreach (ApiCategoryNode nodeLvl1 in typeNode.CategoryNodes)
                {
                    if (nodeLvl1.ExistsDeal == false)
                    {
                        continue;
                    }
                    if (nodeLvl1.ShortName == "超商取貨")
                    {
                        continue;
                    }
                    var nodeLvl1Style = CategoryManager.GetCategoryIconTypeById(nodeLvl1.CategoryId);
                    bool nodeLvl1IsHot = nodeLvl1Style == CategoryIconType.Hot;
                    bool nodeLvl1IsNew = nodeLvl1Style == CategoryIconType.New;

                    var nodeView = new CategoryNodeViewLite
                    {
                        isHot = nodeLvl1IsHot,
                        isNew = nodeLvl1IsNew,
                        title = nodeLvl1.ShortName,
                        hasSub = nodeLvl1.NodeDatas.Count > 0,
                        subNodes = new List<CategoryNodeViewLite.SubNode>(),
                        id = nodeLvl1.CategoryId
                    };
                    nodeViews.Add(nodeView);

                    foreach (var typeNode2 in nodeLvl1.NodeDatas)
                    {
                        if (typeNode2.NodeType != CategoryType.DealCategory)
                        {
                            continue;
                        }
                        foreach (ApiCategoryNode nodeLvl2 in typeNode2.CategoryNodes)
                        {
                            if (nodeLvl2.ExistsDeal == false)
                            {
                                continue;
                            }
                            var subNode = new CategoryNodeViewLite.SubNode
                            {
                                title = nodeLvl2.ShortName,
                                id = nodeLvl2.CategoryId
                            };
                            nodeView.subNodes.Add(subNode);
                        }
                    }
                }
            }

            List<ContentBannerView> contentBanners = SystemDataManager.GetFromCache<List<ContentBannerView>>("P24HContentBanner") 
                ?? new List<ContentBannerView>();

            return View(new Page24hChannelModel
            {
                ContentBanners = contentBanners,
                NodeViews = nodeViews
            });
        }
        [HttpPost]
        [AjaxCall]
        public ActionResult GetArrival24hDeals(Arrival24hDealsInputModel model)
        {
            #region 收藏

            DateTime baseDate = PponDealHelper.GetTodayBaseDate();
            HashSet<Guid> collectBids;
            if (User.Identity.IsAuthenticated)
            {
                int userId = PponIdentity.Current.Id;
                collectBids = new HashSet<Guid>(
                    _mp.MemberCollectDealGetOnlineDealGuids(userId, baseDate.AddDays(-1), baseDate.AddDays(1)));
            }
            else
            {
                collectBids = new HashSet<Guid>();
            }
            #endregion

            var cidList = model.cids ?? new List<int>();
            cidList = cidList.Distinct().OrderBy(t => t).ToList();

            List<MultipleMainDealPreview> mmdeals = PponDealPreviewManager.GetArrival24hCategoryMMDeals(cidList);            
            List<DealViewLite> deals = new List<DealViewLite>();
            foreach(var mmdeal in mmdeals)
            {
                //for (int i = 0; i < 50; i++) //for test lazy loading
                {
                    deals.Add(PponFacade2.ConvertToChannelDealView(mmdeal, collectBids));
                }
            }
            int dataSize = 50;
            bool completed = dataSize + model.dataStart >= deals.Count;
            deals = deals.Skip(model.dataStart).Take(dataSize).ToList();

            return Json(new { deals, completed });
        }

        [Route("~/testpage")]
        public ActionResult TestPage()
        {
            return View();
        }


        #region View utilities 

        private List<ViewMultipleMainDeal> GetViewMultipleMainDeal(List<MultipleMainDealPreview> deals, PponDealListCondition condition)
        {
            List<ViewMultipleMainDeal> viewMultipleMainDeals = new List<ViewMultipleMainDeal>();

            foreach (var deal in deals)
            {
                ViewMultipleMainDeal viewDealPreview = new ViewMultipleMainDeal();
                viewDealPreview.deal = deal;
                bool _isKindDeal = Helper.IsFlagSet(deal.PponDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                int cityId = condition.CityId;
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                //貼上檔次 Icon 標籤
                string icon_string = PponFacade.GetDealIconHtmlContentList(deal.PponDeal, 2);


                viewDealPreview.deal_Label_1 = icon_string;
                viewDealPreview.deal_Label_2 = icon_string;
                viewDealPreview.clit_CityName =
                    deal.PponDeal.IsMultipleStores
                    ? "<div class='hover_place' style='display:none;'><span class='hover_place_text'><i class='fa fa-map-marker'></i>"
                        + deal.PponDeal.HoverMessage + "</span></div>"
                   : string.Empty;

                viewDealPreview.clit_CityName2 =
                    ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(pponcity, deal.PponDeal.BusinessHourGuid);


                switch (deal.PponDeal.GetDealStage())
                {
                    case PponDealStage.Created:
                    case PponDealStage.Ready:
                        viewDealPreview.litCountdownTime = "即將開賣";
                        break;

                    case PponDealStage.ClosedAndFail:
                    case PponDealStage.ClosedAndOn:
                    case PponDealStage.CouponGenerated:
                        viewDealPreview.litCountdownTime = "已結束販售";
                        break;
                    case PponDealStage.Running:
                    case PponDealStage.RunningAndFull:
                    case PponDealStage.RunningAndOn:
                    default:
                        if (PponFacade.IsEveryDayNewDeal(deal.PponDeal.BusinessHourGuid))
                        {
                            EveryDayNewDeal flag = ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(deal.PponDeal.BusinessHourGuid);
                            DateTime end = DateTime.Now;
                            if (flag == EveryDayNewDeal.Limited24HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(1);
                            else if (flag == EveryDayNewDeal.Limited48HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(2);
                            else if (flag == EveryDayNewDeal.Limited72HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(3);
                            else
                                end = deal.PponDeal.BusinessHourOrderTimeE;

                            int daysToClose = (int)(end - DateTime.Now).TotalDays;

                            if (daysToClose > 3)
                            {
                                viewDealPreview.litCountdownTime = "限時優惠中!";
                            }
                            else
                            {
                                viewDealPreview.litDays = daysToClose + Phrase.Day;
                                viewDealPreview.litCountdownTime = SetCountdown(end);
                                viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataeverydaynewdealend = end.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_everydaynewdeal = true;
                            }
                        }
                        else
                        {
                            int daysToClose = (int)(deal.PponDeal.BusinessHourOrderTimeE - DateTime.Now).TotalDays;
                            if (daysToClose > 3)
                            {
                                viewDealPreview.litCountdownTime = "限時優惠中!";
                            }
                            else
                            {
                                viewDealPreview.litDays = daysToClose + Phrase.Day;
                                viewDealPreview.litCountdownTime = SetCountdown(deal.PponDeal.BusinessHourOrderTimeE);

                                viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_everydaynewdeal = false;
                            }
                        }

                        break;
                }

                if (_isKindDeal)
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "公益";
                }
                else if (deal.PponDeal.ItemPrice == deal.PponDeal.ItemOrigPrice || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (cityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                        }
                        else
                        {
                            viewDealPreview.discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, cityId), deal.PponDeal.ItemOrigPrice);
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                            viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        viewDealPreview.discount_1 = viewDealPreview.discount_2 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, cityId), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        viewDealPreview.discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            viewDealPreview.discount_2 += discount.Substring(1, length - 1);
                        }

                        viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                        viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                    }
                }

                if (config.InstallmentPayEnabled == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else if (deal.PponDeal.Installment3months.GetValueOrDefault() == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p class='price_installment'>分期0利率</p>";
                }
                viewMultipleMainDeals.Add(viewDealPreview);
                decimal ratingScore;
                int ratingNumber;
                bool ratingShow;
                viewDealPreview.RatingString = PponFacade.GetDealStarRating(
                    deal.PponDeal.BusinessHourGuid, "pc",
                    out ratingScore, out ratingNumber, out ratingShow);
                viewDealPreview.RatingScore = ratingScore;
                viewDealPreview.RatingNumber = ratingNumber;
                viewDealPreview.RatingShow = ratingShow;

                viewDealPreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
            }

            return viewMultipleMainDeals;
        }

        private string SetCountdown(DateTime timeE)
        {
            TimeSpan ts = timeE - DateTime.Now;
            int hourUntil = ts.Hours;
            int minutesUntil = ts.Minutes;
            int secondsUntil = ts.Seconds;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div class='TimeConDigit hn'>{0}</div>", hourUntil);
            sb.AppendFormat("<div class='TimeConUnit hl'>{0}</div>", Phrase.Hour);
            sb.AppendFormat("<div class='TimeConDigit mn'>{0}</div>", minutesUntil);
            sb.AppendFormat("<div class='TimeConUnit ml'>{0}</div>", "分");
            sb.AppendFormat("<div class='TimeConDigit sn'>{0}</div>", secondsUntil);
            sb.AppendFormat("<div class='TimeConUnit sl'>{0}</div>", Phrase.Second);
            return sb.ToString();
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.GameEntities;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.ActionFilter;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.Game;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Controllers
{
    public class GameController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IGameProvider gp = ProviderFactory.Instance().GetProvider<IGameProvider>();
        private static ILog logger = LogManager.GetLogger(typeof(GameController));

        //const int DEFAULT_BONUS_VALUE = 25 * 10;
        public const int DEFAULT_BONUS_VALUE = 5 * 10;

        public ActionResult Home(string campaignName)
        {
            GameCampaign campaign = null;
            if (string.IsNullOrEmpty(campaignName) == false)
            {
                campaign = gp.GetGameCampaign(campaignName);
            }
            else
            {
                campaign = gp.GetCurrentGameCampaign();
            }
            if (campaign == null)
            {
                return Content(string.Format(@"
                    <script type='text/javascript'>
                        alert('活動結束');
                        location.href='{0}';
                    </script>", config.SiteUrl));
            }

            List<GameActivityView> activitiyViews = gp.GetGameActivityViewByCampaignId(campaign.Id);
            List<TopGameView> topGames = gp.GetTopGameActivityViewList(campaign.Id);
            HomeModel model = new HomeModel
            {
                TopGames = topGames,
                Campaign = campaign,
                ActivitiyViews = activitiyViews
            };
            ViewBag.Title = model.Campaign.Title;
            return View(model);
        }

        [CacheNoStore]
        public ActionResult StartGame(Guid guid)
        {
            GameActivity activity = gp.GetGameActivity(guid);
            int userId = PponIdentity.Current.Id;
            if (activity != null)
            {
                GameRound round = gp.GetGameRoundByActivityId(activity.Id, userId, GameRoundStatus.Running);
                if (round != null)
                {
                    return RedirectToAction("ShowGame", new {roundGuid = round.Guid });
                }
            }

            GameCampaign campaign = gp.GetGameCampaign(activity.CampaignId);
            List<GameGame> games = gp.GetGamesByActivityId(activity.Id);


            List<bool> playableChecks = new List<bool>();
            foreach(var game in games)
            {
                playableChecks.Add(GameFacade.PponAdapter.UserCanPlay(userId, game.ReferenceId));
            }

            StartGameModel model = new StartGameModel
            {
                Campaign = campaign,
                Activity = activity,
                Games = games,
                PlayableChecks = playableChecks,
                DetailUrl = string.Format("{0}?bid={1}", Url.Action("ProductDetail"), activity.ReferenceId.ToString())
            };
            return View(model);
        }


        [HttpPost]
        public ActionResult CreateGame(Guid activityGuid, GameLevel level)
        {
            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToLogin(Url.Action("StartGame", new {guid = activityGuid}));
            }

            int userId = PponIdentity.Current.Id;

            GameActivity activity = gp.GetGameActivity(activityGuid);
            GameCampaign campaign = gp.GetGameCampaign(activity.CampaignId);            
            GameRound round = gp.GetGameRoundByActivityId(activity.Id, userId, GameRoundStatus.Running);
            if (activity.RunOut)
            {
                return RedirectToAction("Home", new { homeId = campaign.Name });
            }
            //新增遊戲
            bool firstCreate = false;
            if (round == null)
            {
                List<GameGame> games = gp.GetGamesByActivityId(activity.Id);
                GameGame game = games.FirstOrDefault(t => t.RuleLevel == level);
                if (game.RunOut)
                {
                    return RedirectToAction("Home", new { homeId = campaign.Name });
                }                
                DateTime expiredTime = GameFacade.GetGameRoundExpiredTime(game, activity, campaign);
                round = gp.InsertGameRoundWhenNoOtherRunningOne(userId, game, expiredTime, User.Identity.Name);
                firstCreate = true;
            }

            if (round != null)
            {
                return RedirectToAction("ShowGame", new { roundGuid = round.Guid, firstCreate = firstCreate }); 
            }
            else
            {
                return RedirectToAction("Home", new { homeId = campaign.Name });
            }            
        }

        [Authorize]
        public ActionResult ShowGame(Guid roundGuid, bool firstCreate = false)
        {
            //用firstCreate讓它先跳分享
            GameRound round = gp.GetGameRound(roundGuid);
            GameGame game = gp.GetGame(round.GameId);
            GameActivity activity = gp.GetGameActivity(game.ActivityId);
            List<GameBuddy> buddies = gp.GetGameBuddyList(round.Id);            

            GameFacade.ResetGameGroundStatus(round, game, buddies, User.Identity.Name);

            GameRoundView roundView = gp.GetGameRoundView(roundGuid);

            string favourUrl = Helper.CombineUrl(config.SiteUrl,
                string.Format("{0}?roundGuid={1}", Url.Action("GuestEntrance"), roundGuid));
            string facebookFavourUrl = GameFacade.GetFacebookFavourUrl(favourUrl);
            string lineFavourUrl = GameFacade.GetLineFavourUrl(favourUrl);

            return View(
                new ShowGameModel
                {
                    Activity = activity,
                    Game = game,
                    RoundView = roundView,
                    Buddies = buddies,
                    RewardUrl = Helper.CombineUrl(config.SiteUrl, "ppon/buy.aspx?bid=" + game.ReferenceId.ToString()),
                    DetailUrl = string.Format("{0}?bid={1}", Url.Action("ProductDetail"), activity.ReferenceId.ToString()),
                    LineShareUrl = lineFavourUrl,
                    FacebookShareUrl = facebookFavourUrl,
                    FirstCreate = firstCreate,
                });
        }

        [Authorize]
        public ActionResult MyGames()
        {
            int userId = PponIdentity.Current.Id;
            GameCampaign campaign = gp.GetCurrentGameCampaign();
            List<GameRoundView> gameRoundViews = gp.GetGameRoundViewList(userId, campaign);
            MyGamesModel model = new MyGamesModel
            {
                Campaign = campaign,
                GameRoundViews = gameRoundViews
            };
            return View(model);
        }

        /// <summary>
        /// 開發測試用
        /// 拔刀相助
        /// </summary>
        /// <param name="roundGuid"></param>
        /// <returns></returns>
        public ActionResult Hit(Guid roundGuid)
        {
            GameRound round = gp.GetGameRound(roundGuid);
            GameGame game = gp.GetGame(round.GameId);
            if (round == null)
            {
                throw new Exception("GameRound not found.");
            }
            int count = gp.GetGameBuddyList(round.Id).Count;
            if (count < game.RulePeople - 1)
            {
                GameBuddy buddy = new GameBuddy
                {
                    RoundId = round.Id,
                    Source = (int)SingleSignOnSource.Facebook,
                    Pic = "https://scontent.xx.fbcdn.net/v/t1.0-1/c4.0.50.50/p50x50/1908225_10200501768745573_725906037_n.jpg?_nc_cat=104&oh=cc493cca1a2cfd32df89a02ac0c3371c&oe=5C40CF60",
                    Message = "hello",
                    Name = "MR." + Guid.NewGuid().ToString().Substring(0, 3).ToUpper(),
                    ExternalId = Guid.NewGuid().ToString(),
                    CreateTime = DateTime.Now
                };
                gp.SaveGameBuddy(buddy);
                return Content("ok, " + (count + 1) + " buddies");
            }
            return Content("need last 1");
        }

        #region 應援團

        //非主揪者的授權
        public ActionResult GuestEntrance(Guid roundGuid)
        {
            GameRound round = gp.GetGameRound(roundGuid);
            if (round == null)
                return RedirectToAction("Home");

            GameGame game = gp.GetGame(round.GameId);
            GameActivity activity = gp.GetGameActivity(game.ActivityId);

            Session[LkSiteSession.RoundGuid.ToString()] = roundGuid;
            string state = Uri.EscapeDataString(roundGuid.ToString());

            if (string.IsNullOrEmpty(activity.Title)==false)
            {
                ViewBag.OgTitle = activity.Title;
            }
            if (string.IsNullOrEmpty(activity.SubTitle)==false)
            {
                ViewBag.OgDescription = activity.SubTitle;
            }
            if (string.IsNullOrEmpty(activity.Image)==false)
            {
                ViewBag.OgImage = activity.Image;
            }

            return View(new GuestEntranceModel
            {
                Activity = activity,
                Game = game,
                Round = round,
                FbLoginUrl = FacebookUtility.FbGameAuthUrl(state),
                LineLoginUrl = LineUtility.LineLoginUrl(state, config.GameLineAuthRedirectUri),
            });
        }

        //非主揪者的砍價
        public ActionResult GuestEvent(string atk, Guid roundGuid, int source)
        {
            if (string.IsNullOrWhiteSpace(atk))
            {
                return RedirectToAction("Home");
            }

            ExternalUser user = (SingleSignOnSource)source == SingleSignOnSource.Facebook ? 
                (ExternalUser)FacebookUser.Get(atk, false)
                : (SingleSignOnSource)source == SingleSignOnSource.Line ? LineUser.Get(atk) : null;

            if (user == null)
            {
                return RedirectToAction("Home");
            }
            var gameUser = new GuestMember(user, (SingleSignOnSource)source);
            Session["guestMember"] = gameUser;

            GameBuddy gameBuddy = gp.GetGameBuddy(roundGuid, gameUser.Source, user.Id);
            if (gameBuddy != null)
            {
                if (gameBuddy.RewardId.HasValue)
                {
                    ViewBag.AlertMsg = "這個帳號已經幫忙過囉";
                }
                else
                {
                    return RedirectToAction("GuestSuccess", new {roundGuid});
                }
            }

            GameRound round = gp.GetGameRound(roundGuid);
            GameGame game = gp.GetGame(round.GameId);
            GameActivity activity = gp.GetGameActivity(game.ActivityId);
            List<GameBuddy> buddies = gp.GetGameBuddyList(round.Id);
            GameFacade.ResetGameGroundStatus(round, game, buddies, User.Identity.Name);
            GameRoundView roundView = gp.GetGameRoundView(roundGuid);

            string favourUrl = Helper.CombineUrl(config.SiteUrl,
                string.Format("{0}?roundGuid={1}", Url.Action("GuestEntrance"), roundView.Guid));
            string facebookFavourUrl = GameFacade.GetFacebookFavourUrl(favourUrl);
            string lineFavourUrl = GameFacade.GetLineFavourUrl(favourUrl);

            return View(
                new GuestEventModel
                {
                    Activity = activity,
                    Game = game,
                    RoundView = roundView,
                    Buddies = buddies,
                    BargainUrl = string.Format("{0}?roundGuid={1}", Url.Action("GuestBargain"), roundGuid),
                    LineShareUrl = lineFavourUrl,
                    FacebookShareUrl = facebookFavourUrl,
                    SuccessUrl = string.Format("{0}?roundGuid={1}", Url.Action("GuestSuccess"), roundGuid),
                    GameHomeUrl = GameFacade.GetCurrentCampaignUrl(Url),
                    DetailUrl = string.Format("{0}?bid={1}", Url.Action("ProductDetail"), activity.ReferenceId.ToString())
                });
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GuestBargain(Guid roundGuid)
        {
            try
            {
                GuestMember guestMember = (GuestMember)Session["guestMember"];
                ExternalUser externalUser = guestMember == null
                    ? null
                    : new ExternalUser()
                    {
                        Pic = guestMember.Pic,
                        FirstName = guestMember.FirstName,
                        LastName = guestMember.LastName,
                        Id = guestMember.Id,
                    };

                string message;
                bool success = GameFacade.AddGameBuddy(roundGuid
                    , externalUser, guestMember == null ? SingleSignOnSource.Undefined : guestMember.Source
                    , PponIdentity.Current.Name, out message);
                return Json(new
                {
                    Success = success,
                    Message = message
                });
            }
            catch (Exception ex)
            {
                logger.WarnFormat("GuestBargain error, roundGuid={0}, ex={1}", roundGuid, ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

        //非主揪者的砍價成功
        public ActionResult GuestSuccess(Guid roundGuid)
        {
            GameRound round = gp.GetGameRound(roundGuid);

            int campaignId = gp.GetCampaignIdFromRoundGuid(roundGuid);
            GameCampaign campaign = GameFacade.GetCurrentGameCampaign();
            GuestMember user = (GuestMember)Session["guestMember"];
            if (user == null || campaign == null)
            {
                return RedirectToAction("GuestEntrance", new { roundGuid });
            }

            List<GameActivityView> activitiyViews = 
                gp.GetGameActivityViewByCampaignId(campaignId).OrderBy(x => Guid.NewGuid()).Take(4).ToList();

            GameBuddy gameBuddy = gp.GetGameBuddy(roundGuid, user.Source, user.Id);
            int state = 0;
            if (gameBuddy == null)
            {
                state = -1;
            }
            else if (gameBuddy.RewardId == null)
            {
                state = 0;
            }
            else
            {
                return RedirectToAction("Home", new { homeId = campaign.Name });
            }

            GuestSuccessModel model = new GuestSuccessModel
            {
                ActivitiyViews = activitiyViews,
                EnableBuddyReward = campaign.EnableBuddyReward,
                State = state,
                RoundGuid = roundGuid,
                SenderSource = user.Source,
                SenderId = user.Id,
                GameHomeUrl = GameFacade.GetCurrentCampaignUrl(Url),
                HomeUrl = Helper.CombineUrl(config.SiteUrl),
            };
            
            return View(model);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GiveGift(GiveGiftModel data)
        {
            try
            {
                string message;
                RewardMemberGameBonusStatus status = GameFacade.RewardMemberGameBouns(
                    data.SenderSource, data.SenderId,
                    DEFAULT_BONUS_VALUE, data.ReceiverName,
                    data.RoundGuid,
                    out message);
                return Json(new
                {
                    Success = status,
                    Message = message
                });
            } catch (Exception ex)
            {
                logger.WarnFormat("UpdateCampaign error, data={0}, ex={1}", JsonConvert.SerializeObject(data), ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

        //商品說明頁
        public ActionResult ProductDetail(Guid bid)
        {
            IViewPponDeal mainDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            var model = ProductDetailModel.Create(mainDeal);

            return View(model);
        }

        #endregion

        [Serializable]
        public class GuestMember
        {
            public string Id { get; set; }
            public string Email { get; set; }
            public DateTime? Birthday { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string Name { get; private set; }
            public string Pic { get; set; }
            public int? Gender { get; set; }

            public SingleSignOnSource Source { get; set; }

            public GuestMember(ExternalUser user, SingleSignOnSource source)
            {
                Id = user.Id;
                Email = user.Email;
                Birthday = user.Birthday;
                FirstName = user.FirstName;
                LastName = user.LastName;
                Name = FirstName + LastName;
                Pic = user.Pic;
                Gender = user.Gender;
                Source = source;
            }
        }
    }
}

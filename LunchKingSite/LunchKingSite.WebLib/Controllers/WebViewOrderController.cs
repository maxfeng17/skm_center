﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.WebViewOrder;
using LunchKingSite.WebLib.Models.WebViewVendorSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class WebViewOrderController : BaseController
    {
        #region Property

        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();

        private int UserId
        {
            get
            {
                var user = this.User.Identity as PponIdentity;

                if (user != null)
                {
                    return user.Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion Property

        #region Action

        [WebViewAuthorize(AuthorizedUserType = AuthorizedUserType.Consumer)]
        public ActionResult ExchangeApplication(string guid)
        {
            var model = new ExchangeApplicationModel();

            var member = mp.MemberGet(UserId);
            if (!member.IsLoaded || member == null)
            {
                return RedirectToAction("LoginError", "WebViewVendorSystem", new { result = LoginFailResult.InvalidAccount });
            }

            Guid orderGuid;  
            string failMessage = string.Empty;
            if (!Guid.TryParse(guid, out orderGuid))
            {
                model.Message = "訂單資訊有誤，請確認後重試。";
                return View("ExchangeApplication", model);
            }

            var orderInfo = mp.GetCouponListMainByOid(orderGuid);
            if (!CheckOrderInfo(orderInfo, out failMessage))
            {
                model.Message = failMessage;
            }
            else
            {
                model.OrderGuid = orderInfo.Guid;
                model.IsCoupon = orderInfo.DeliveryType == (int)DeliveryType.ToShop;
                var order = op.OrderGet(orderInfo.Guid);
                if (order.IsLoaded)
                {
                    model.ReceiverName = order.MemberName;
                    model.ReceiverAddress = order.DeliveryAddress;
                }
            }

            return View("ExchangeApplication", model);
        }

        [HttpPost]
        [WebViewAuthorize(AuthorizedUserType = AuthorizedUserType.Consumer)]
        public JsonResult ExchangeLogCreate(ExchangeApplicationModel model)
        {
            bool result = true;
            try
            {
                var now = DateTime.Now;
                var rtnl = new OrderReturnList
                {
                    OrderGuid = model.OrderGuid,
                    Type = (int)OrderReturnType.Exchange,
                    Reason = model.ExchangeReason,
                    Status = (int)OrderReturnStatus.SendToSeller,
                    VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing,
                    CreateTime = now,
                    CreateId = this.UserName,
                    ModifyTime = now,
                    ModifyId = this.UserName,
                    ReceiverName = model.ReceiverName,
                    ReceiverAddress = model.ReceiverAddress
                };

                if (OrderFacade.SetOrderReturList(rtnl))
                {
                    int exchangeLogId = op.OrderReturnListGetbyType(model.OrderGuid, (int)OrderReturnType.Exchange).Id;
                    string auditMessage = string.Format("換貨單({0})建立. 換貨描述:{1}", exchangeLogId, model.ExchangeReason);
                    CommonFacade.AddAudit(model.OrderGuid, AuditType.Refund, auditMessage, this.UserName, false);

                    OrderFacade.SendCustomerExchangeApplicationMail(exchangeLogId);
                    EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Exchange, exchangeLogId);
                }
            }
            catch
            {
                result = false;
            }

            return Json(new { Success = result});
        }

        #endregion Action

        #region Method

        private bool CheckOrderInfo(ViewCouponListMain orderInfo, out string failMessage)
        {
            failMessage = string.Empty;
            string failReson = string.Empty;
            int slug;

            if (orderInfo == null || !orderInfo.IsLoaded)
            {
                failMessage = "訂單資訊有誤，請確認後重試。";
            }
            else if (orderInfo.BusinessHourOrderTimeE <= DateTime.Now 
                  && int.TryParse(orderInfo.Slug, out slug) 
                  && slug < orderInfo.BusinessHourOrderMinimum)
            {
                //已結檔且未達門檻
                failMessage = "未達門檻無法換貨。";
            }
            else if ((orderInfo.OrderStatus & (int)Core.OrderStatus.Complete) == 0)
            {
                if ((orderInfo.OrderStatus & (int)Core.OrderStatus.ATMOrder) > 0)
                {
                    failMessage = orderInfo.CreateTime.Date.Equals(DateTime.Now.Date)
                                    ? "尚未完成ATM付款，無法換貨。"
                                    : "ATM 逾期未付款，無法換貨。";
                }
                else
                {
                    failMessage = "未完成付款的訂單，無法換貨。";
                }
            }
            else if (!OrderExchangeUtility.AllowExchange(orderInfo.Guid, out failReson))
            {
                failMessage = failReson;
            }

            if ((orderInfo.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                   orderInfo.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup) && !(orderInfo.IsReceipt ?? false))
            {
                failMessage = "尚未取貨的訂單，無法換貨。";
            }

            return string.IsNullOrEmpty(failMessage);
        }

        #endregion Method

    }
}

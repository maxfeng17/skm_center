﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using LunchKingSite.WebLib.Models.WebViewVendorSystem;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class WebViewVendorSystemController : BaseController
    {
        #region Property

        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IVerificationProvider vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();

        private int UserId
        {
            get
            {
                var user = this.User.Identity as PponIdentity;

                if (user != null)
                {
                    return user.Id;
                }
                else
                {
                    return 0;
                }
            }
        }

        #endregion Property

        #region Action

        [WebViewAuthorize]
        public ActionResult VerificationCouponListByStore(string storeId, int? day, int? pageNumber, string only, string mailTo)
        {
            bool isPostBack = day != null || pageNumber != null;
            pageNumber = pageNumber == null ? 0 : pageNumber - 1;
            day = day ?? 1;
            const int pageSize = 10;
            var now = DateTime.Today;

            var vms = mp.VbsMembershipGetByUserId(UserId);

            if (!vms.IsLoaded || vms == null)
            {
                return RedirectToAction("LoginError", new { result = LoginFailResult.InvalidAccount });
            }

            if (vms.AccountType != (int)VbsMembershipAccountType.VendorAccount)
            {
                return RedirectToAction("LoginError", new { result = LoginFailResult.ForbiddenAccount });
            }

            var deals = (new VbsVendorAclMgmtModel(vms.AccountId)).GetAllowedDealUnits(null, DeliveryType.ToShop, VbsRightFlag.VerifyShop)
                                    .Where(x => now >= x.DealUseStartTime && now <= x.DealUseEndTime.Value.AddDays(day.Value));

            var model = new VerificationCouponListByStoreModel();
            if (deals.Count() > 0)
            {
                deals.GroupBy(x => new { x.StoreGuid, x.StoreName })
                     .Select(s => new { StoreGuid = s.Key.StoreGuid.Value, StoreName = s.Key.StoreName })
                     .ForEach(x => model.StoreList.Add(x.StoreGuid.ToString(), x.StoreName.Length > 9 ? x.StoreName.Substring(9, x.StoreName.Length - 9) : x.StoreName));

                model.VerifyTime = new Dictionary<string, string>
                    {
                        {"1","24小時以內" },
                        {"3","72小時以內" },
                        {"7","一星期以內" },
                        {"30","一個月以內" },
                        {"90","三個月以內" },
                        {"0","所有可兌換商品" },
                    };

                var isSeller = deals.Any(x => x.IsSellerPermissionAllow);
                Guid? storeGuid = null;
                if (isSeller)
                {
                    model.StoreList.Add("all", "所有分店");
                    model.IsSellerPermission = true;
                }

                if (!string.IsNullOrEmpty(storeId) && storeId != "all")
                {
                    storeGuid = isSeller ? Guid.Parse(storeId) : storeGuid;
                    model.StoreId = storeGuid.ToString();
                }
                storeGuid = !isSeller || storeId == "all" ? null : storeGuid;
                only = !isPostBack && !isSeller ? "on" : only;
                model.IsOnlyAccount = !string.IsNullOrEmpty(only);
                var bids = deals.GroupBy(x => new { x.MerchandiseId, x.MerchandiseGuid })
                                .Select(s => new { MerchandiseId = s.Key.MerchandiseId, MerchandiseGuid = s.Key.MerchandiseGuid })
                                .ToDictionary(d => d.MerchandiseId, d => d.MerchandiseGuid);
                List<VbsVerificationCoupon> list = new List<VbsVerificationCoupon>();
                foreach (var bid in bids)
                {
                    var items = vp.GetCashTrustLogGetListByProductGuid(bid.Value.Value, OrderClassification.LkSite)
                                    .Where(x => x.Status == (int)TrustStatus.Verified
                                            && (storeGuid != null
                                                ? x.StoreGuid == storeGuid
                                                : (isSeller
                                                    ? true
                                                    : model.StoreList.Any(y => y.Key == x.StoreGuid.ToString())))
                                                    && (only == "on" ? x.VerifyCreateId == vms.AccountId : true));
                    items.ForEach(x => list.Add(new VbsVerificationCoupon
                    {
                        DealName = string.Format(@"[{0}]{1}", bid.Key, x.ItemName),
                        VerifyCount = 1,
                        CouponId = x.CouponSequenceNumber,
                        VerifiedDateTime = x.VerifyTime,
                        VerifyAccount = x.VerifyCreateId,
                        VerifiedStore = x.StoreName,
                    }));
                }

                DateTime? newVerifyTime;
                List<VbsVerificationCoupon> data = new List<VbsVerificationCoupon>();
                if (list.Count > 0)
                {
                    if (!isSeller)
                    {
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)
                                      && x.VerifyAccount == vms.AccountId)
                            .OrderByDescending(x => x.VerifiedDateTime)
                            .ForEach(x => data.Add(x));
                        newVerifyTime = data.Count > 0 ? data.Max(m => m.VerifiedDateTime) : null;
                        if (data.Count > 0)
                        {
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                        list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)
                                      && x.VerifyAccount != vms.AccountId)
                                      .OrderByDescending(x => x.VerifiedDateTime)
                                      .ForEach(x => data.Add(x));
                        if (newVerifyTime == null)
                        {
                            newVerifyTime = data.Max(m => m.VerifiedDateTime);
                            data.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        }
                    }
                    else
                    {
                        newVerifyTime = list.Max(m => m.VerifiedDateTime);
                        list.ForEach(x => x.IsNew = x.VerifiedDateTime >= newVerifyTime.Value.AddSeconds(-3));
                        data = list.Where(x => (day > 0 ? x.VerifiedDateTime >= now.AddDays(-1 * day.Value) : true)).OrderByDescending(x => x.VerifiedDateTime).ToList();
                    }
                }

                if (!string.IsNullOrEmpty(mailTo))
                {
                    //寄送憑證清冊
                    var dummy = VerificationFacade.GetVbsVerificationCouponInfosExcelByStore(data,
                                        string.Format("{0},{1},{2}", vms.AccountId, vms.Name, mailTo));
                }

                model.Coupons = new PagerList<VbsVerificationCoupon>(data.Skip(pageNumber.Value * pageSize).Take(pageSize),
                    new PagerOption(pageSize, pageNumber.Value * pageSize, VbsVerificationCoupon.Columns.VerifiedDateTime, false),
                    pageNumber.Value, data.Count);

                return View("VerificationCouponListByStore", model);
            }

            return RedirectToAction("NoRecord");
        }

        public ActionResult LoginError(LoginFailResult result)
        {
            var model = new LoginErrorModel();

            switch (result)
            {
                case LoginFailResult.LoginInfoExpired:
                    model.Message = "登入資訊過期，請重新登入。";
                    break;
                case LoginFailResult.InvalidAccount:
                    model.Message = "查無此帳號，請檢查登入帳號是否有誤或連繫客服同仁協助確認。";
                    break;
                case LoginFailResult.ForbiddenAccount:
                    model.Message = "無權限檢視此頁面，請檢查登入帳號是否有誤或連繫客服同仁協助確認。";
                    break;
                case LoginFailResult.LoginInfoError:
                default:
                    model.Message = "登入資訊有誤，請確認登入帳號密碼是否有誤後；再重新登入。";
                    break;
            }

            return View("LoginError", model);
        }

        [WebViewAuthorize]
        public ActionResult NoRecord()
        {
            return View();
        }

        #endregion Action

        #region Method

        #endregion Method
    }
}


﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.MemberActions;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Component.MemberActions;
using LunchKingSite.WebLib.Models.NewMember;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class NewMemberController : BaseController
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected IOAuthProvider oAuthp = ProviderFactory.Instance().GetProvider<IOAuthProvider>();

        private ILog logger = LogManager.GetLogger(typeof(MobileController));

        private ExternalMemberInfo ExternalMember
        {
            get { return (ExternalMemberInfo)Session[LkSiteSession.ExternalMemberId.ToString("g")]; }
        }

        public ActionResult ConfirmMember()
        {
            if(ExternalMember == null || ExternalMember.Count == 0)
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "m/login"));
            }

            ViewBag.ApiBaseUrl = config.SSLSiteUrl;
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = (string)TempData["ErrorMessage"];
            }

            return View();
        }

        [HttpPost]
        public ActionResult ConfirmExisted(string emailOrPhoneNum)
        {
            var externalMember = ExternalMember[0];
            IBindingMember bindingMember = CreateBindingMember(externalMember);

            if(bindingMember.GetAccountAvailableStatus() == AccountAvailableStatus.WasUsed)
            {
                TempData["ErrorMessage"] = bindingMember.ToString() + "已經註冊過";
                return RedirectToAction("ConfirmMember");
            }
            
            var life17Account = new Life17Member(emailOrPhoneNum);
            var verifiedAccountAvailableStatus = life17Account.GetAccountAvailableStatus();
            if (verifiedAccountAvailableStatus == AccountAvailableStatus.IllegalFormat 
                || verifiedAccountAvailableStatus == AccountAvailableStatus.Disposable || verifiedAccountAvailableStatus == AccountAvailableStatus.Undefined)
            {
                TempData["ErrorMessage"] = "請輸入正確的電子信箱/手機號碼。";
                return RedirectToAction("ConfirmMember");
            }

            if (life17Account.IsEmailFormat())
            {
                MemberLinkCollection mlCol = null;
                SignInReply reply = MemberUtility.SingnInfo(emailOrPhoneNum,ref mlCol);

                const string replyMessage = "此信箱應使用{0}登入或是請再改試其他信箱或手機號碼";

                if (reply == SignInReply.MemberLinkExistButWrongWay)
                {
                    string message;
                    if (mlCol.Count > 1)
                    {
                        message = replyMessage.FormatWith("其他方式");
                    }
                    else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.PayEasy)
                    {
                        message = replyMessage.FormatWith("PayEasy");
                    }
                    else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.Facebook)
                    {
                        message = replyMessage.FormatWith("Facebook");
                    }
                    else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.Line)
                    {
                        message = replyMessage.FormatWith("Line");
                    }
                    else
                    {
                        message = replyMessage.FormatWith("其他方式");
                    }

                    TempData["ErrorMessage"] = message;
                    return RedirectToAction("ConfirmMember");
                }
            }

            Member mem = MemberFacade.GetMember(emailOrPhoneNum);
            MemberLink link = mp.MemberLinkGet(mem.UniqueId, externalMember.Source);
            if (link.IsLoaded)
            {
                TempData["ErrorMessage"] = "該帳號已被綁定過，請重新輸入。";
                return RedirectToAction("ConfirmMember");
            }

            if (verifiedAccountAvailableStatus == AccountAvailableStatus.WasUsed)
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "NewMember/MemberBinding?user=" + Helper.EncryptEmail(emailOrPhoneNum ?? string.Empty)));
            }
            if (life17Account.IsEmailFormat() && verifiedAccountAvailableStatus == AccountAvailableStatus.Authorizing)
            {
                Session["RegisterByEmailWaitConfirmMssage"] = "請啟用帳號後，再重新綁定";
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                string authKey = mai.AuthKey;
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?send=1&key=" + authKey));
            }
            if (life17Account.IsEmailFormat() && !MemberUtilityCore.CheckDEAfilter(emailOrPhoneNum))
            {   
                TempData["ErrorMessage"] = "不能使用拋棄式信箱，請重新輸入";
                return RedirectToAction("ConfirmMember");
            }

            if (life17Account.IsMobileFormat())
            {
                if (verifiedAccountAvailableStatus == AccountAvailableStatus.Available)
                {
                    NewMemberUtility.RegisterMobileMember(emailOrPhoneNum, null);
                    return Redirect(Helper.CombineUrl(config.SSLSiteUrl,
                        "NewMember/RegisterMobileValidation?user=" + Helper.EncryptEmail(emailOrPhoneNum ?? string.Empty)));
                }
                if (verifiedAccountAvailableStatus == AccountAvailableStatus.Authorizing)
                {
                    return Redirect(Helper.CombineUrl(config.SSLSiteUrl,
                        "NewMember/RegisterMobileValidation?user=" + Helper.EncryptEmail(emailOrPhoneNum ?? string.Empty)));
                }            
            }

            return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "NewMember/Register?user=" + Helper.EncryptEmail(emailOrPhoneNum ?? string.Empty)));
        }

        private IBindingMember CreateBindingMember(ExternalLoginInfo member)
        {
            IBindingMember bindingMember;
            if (member.Source == SingleSignOnSource.Facebook)
            {
                bindingMember = new FBBindingMember(member.ExternalId);
            }
            else if (member.Source == SingleSignOnSource.Line)
            {
                bindingMember = new LineBindingMember(member.ExternalId);
            }
            else if (member.Source == SingleSignOnSource.PayEasy)
            {
                bindingMember = new PayEasyBindingMember(member.ExternalId);
            }
            else
            {
                throw new NotImplementedException();
            }

            return bindingMember;
        }

        public ActionResult MemberBinding(string user)
        {
            if(ExternalMember == null || ExternalMember.Count == 0 
                || string.IsNullOrWhiteSpace(user))
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "m/login"));
            }

            var email = Helper.DecryptEmail(user);
            ViewBag.Email = email;

            var returnUrl = Session[LkSiteSession.NowUrl.ToString()];
            ViewBag.ForgetPasswordUrl = Helper.CombineUrl(config.SSLSiteUrl, "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(email));

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = (string)TempData["ErrorMessage"];
            }

            return View();
        }

        public ActionResult Binding(LoginViewModel model)
        {
            string message = string.Empty;
            string redirectUrl = string.Empty;
            string forgotPasswordUrl = string.Empty;
            var returnUrl = Session[LkSiteSession.NowUrl.ToString()];

            var member = ExternalMember[0];
            var tmpSource = member.Source;
            var tmpExternalId = member.ExternalId;

            MemberLinkCollection mlCol = null;
            Member mem;
            string userName;
            SingleSignOnSource source;
            
            var replyData = MemberUtility.ContactLogin(model.Account, model.Password,
                ref mlCol, out userName, out source, out mem);
            SignInReply reply = replyData.Reply;

            const string replyMessage = "此信箱應使用{0}登入或是請再改試其他信箱或手機號碼";

            if (reply == SignInReply.Success)
            {
                var memberLink = mlCol.FirstOrDefault(x => x.ExternalOrg == (int)source);
                MemberFacade.AddMemberLink(memberLink.UserId, tmpSource, tmpExternalId, memberLink.CreateId);

                redirectUrl = Helper.CombineUrl(config.SSLSiteUrl + (returnUrl == null ? "" : returnUrl.ToString()));
                message = "綁定成功";
            }
            else if (reply == SignInReply.MemberLinkExistButWrongWay)
            {
                if (mlCol.Count > 1)
                {
                    message = replyMessage.FormatWith("其他方式");
                }
                else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.PayEasy)
                {
                    message = replyMessage.FormatWith("PayEasy");
                }
                else if (mlCol[0].ExternalOrg == (int)SingleSignOnSource.Facebook)
                {
                    message = replyMessage.FormatWith("Facebook");
                }
                else
                {
                    message = replyMessage.FormatWith("其他方式");
                }
            }
            else if (reply == SignInReply.EmailInactive)
            {
                Session["RegisterByEmailWaitConfirmMssage"] = "請啟用帳號後，再重新綁定";
                MemberAuthInfo mai = mp.MemberAuthInfoGet(mem.UniqueId);
                string authKey = mai.AuthKey;
                redirectUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?send=1&key=" + authKey);
            }
            else if (reply == SignInReply.MobileInactive)
            {
                redirectUrl = Helper.CombineUrl(config.SSLSiteUrl, "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(model.Account));
            }
            else if (reply == SignInReply.PasswordError)
            {
                message = replyData.ReplyMessage;
            }
            else if (reply == SignInReply.AccountNotExist)
            {
                message = "帳號不存在";
                redirectUrl = FormsAuthentication.LoginUrl + (returnUrl == null ? "" : "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl.ToString()));
            }
            else if (reply == SignInReply.AccountIsLockedOut)
            {
                message = replyData.ReplyMessage;
                forgotPasswordUrl = Helper.CombineUrl(config.SSLSiteUrl, "NewMember/ForgetPassword.aspx?account=" + HttpUtility.UrlEncode(model.Account));
                redirectUrl = FormsAuthentication.LoginUrl + (returnUrl == null ? "" : "?ReturnUrl=" + HttpUtility.UrlEncode(returnUrl.ToString()));
            }

            return Json(new
            {
                Reply = reply,
                RedirectUrl = redirectUrl,
                ForgotPasswordUrl = forgotPasswordUrl,
                Message = message
            });
        }

        public ActionResult Register(string user, string resetPasswordKey)
        {
            if(ExternalMember == null || ExternalMember.Count == 0
                || string.IsNullOrWhiteSpace(user))
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "m/login"));
            }

            var emailOrPhoneNum = Helper.DecryptEmail(user);
            ViewBag.Email = emailOrPhoneNum;

            //若是手機要驗有沒有過
            var life17Account = new Life17Member(emailOrPhoneNum);
            if (life17Account.IsMobileFormat())
            {
                MobileMember mm = MemberFacade.GetMobileMember(emailOrPhoneNum);
                if (!mm.IsLoaded || mm.Status == (int)MobileMemberStatusType.None)
                {
                    TempData["ErrorMessage"] = "手機會員未認証";
                    return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "m/login"));
                }
            }

            RegisterViewModel register = new RegisterViewModel();

            List<City> citys = CityManager.Citys.Where(x => x.Code != "com" && x.Code != "SYS").ToList();
            citys.Insert(0, new City { Id = 0, CityName = "請選擇" });
            ViewBag.Citys = citys;

            ViewBag.ResetPasswordKey = resetPasswordKey;

            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = (string)TempData["ErrorMessage"];
            }
                       
            return View(register);
        }

        public ActionResult RegisterConfirm(RegisterViewModel register)
        {
            string message;
            string redirectUrl = "";
            if (CheckData(register, out message))
            {
                IMemberRegister reg;
                var externalMember = ExternalMember[0];
                var life17Account = new Life17Member(register.Account);
                if (life17Account.IsMobileFormat())
                {
                    MobileMember mm = MemberFacade.GetMobileMember(register.Account);
                    if (!mm.IsLoaded || mm.Status == (int)MobileMemberStatusType.None)
                    {
                        message = "手機會員未認証";
                    }
                    else
                    {
                        Member mem = mp.MemberGet(mm.UserId);
                        MemberSetPasswordReply result = MemberFacade.SetMobleMemberPassword(mm.UserId, register.Password);

                        var returnUrl = Session[LkSiteSession.NowUrl.ToString()];
                        switch (result)
                        {
                            case MemberSetPasswordReply.Success:
                                message = "綁定成功";
                                BindingProcessing(externalMember, mm, mem);
                                redirectUrl = Helper.CombineUrl(config.SSLSiteUrl + (returnUrl == null ? "" : returnUrl.ToString()));
                                break;
                            case MemberSetPasswordReply.EmptyError:
                                message = "密碼輸入有誤，麻煩您重新輸入。";
                                break;
                            case MemberSetPasswordReply.DataError:
                                message = "驗證碼已過有效時間。";
                                break;
                            case MemberSetPasswordReply.OtherError:
                                message = "這個手機號碼己經被使用了。";
                                break;
                            case MemberSetPasswordReply.PasswordFormatError:
                                message = "密碼格式錯誤。";
                                break;
                            default:
                                message = "系統繁忙中，請稍後再試!!";
                                break;
                        }
                    }
                }
                else if (life17Account.IsEmailFormat())
                {
                    if (ExternalMember.Count == 0)
                    {
                        reg = new Life17MemberRegister(register.Account, register.Password, int.Parse(register.City));
                    }
                    else if (externalMember.Source == SingleSignOnSource.Line)
                    {
                        reg = new LineBindingMemberRegister(externalMember.ExternalId, register.Account, register.Password, int.Parse(register.City));
                    }
                    else if (externalMember.Source == SingleSignOnSource.Facebook)
                    {
                        reg = new FBBindingMemberRegister(externalMember.ExternalId, register.Account, register.Password, int.Parse(register.City));
                    }
                    else if (externalMember.Source == SingleSignOnSource.PayEasy)
                    {
                        reg = new PayEasyBindingMemberRegister(externalMember.ExternalId, register.Account, register.Password, int.Parse(register.City));
                    }
                    else
                    {
                        throw new NotImplementedException("不合法的綁定");
                    }

                    MemberRegisterReplyType reply = reg.Process(register.ReceiveEDM);
                    message = reg.Message;

                    if (reply == MemberRegisterReplyType.RegisterSuccess)
                    {
                        MemberLink link = mp.MemberLinkGetByExternalId(externalMember.ExternalId, externalMember.Source);
                        MemberAuthInfo mai = mp.MemberAuthInfoGet(link.UserId);
                        string authKey = mai.AuthKey;
                        redirectUrl = Helper.CombineUrl(config.SSLSiteUrl, "/mvc/NewMember/RegisterByEmailWaitConfirm?key=" + authKey);
                    }
                }
                else
                {
                    throw new NotImplementedException("目前只有email、手機註冊");
                }
            }

            return Json(new
            {
                RedirectUrl = redirectUrl,
                Message = message,
            });
        }

        private void BindingProcessing(ExternalLoginInfo member, MobileMember mm, Member mem)
        {
            MemberFacade.AddMemberLink(mm.UserId, member.Source, member.ExternalId, mem.UserName);
            MemberFacade.SetMemberActive(mem.UniqueId);
            NewMemberUtility.UserSignIn(MemberFacade.GetUserName(mm.UserId), member.Source, false);
        }

        public ActionResult RegisterMobileValidation(string user)
        {
            if(ExternalMember == null || ExternalMember.Count == 0
                || string.IsNullOrWhiteSpace(user))
            {
                return Redirect(Helper.CombineUrl(config.SSLSiteUrl, "m/login"));
            }

            var mobile = Helper.DecryptEmail(user);
            ViewBag.Mobile = mobile;
            ViewBag.ApiBaseUrl = config.SSLSiteUrl;
            ViewBag.EncryptMobile = user;
            if (TempData["ErrorMessage"] != null)
            {
                ViewBag.ErrorMessage = (string)TempData["ErrorMessage"];
            }
                       
            return View();
        }

        [HttpGet]
        public ActionResult RegisterByEmail(string key)
        {
            if (!string.IsNullOrWhiteSpace(key))
            {
                ViewBag.Send = 1;
            }
            return View();
        }

        [HttpGet]
        public ActionResult RegisterByEmailWaitConfirm(string key, int send = 0)
        {
            if (string.IsNullOrWhiteSpace(key))
            {
                return Redirect(config.SiteUrl);
            }

            IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
            var mai = mp.MemberAuthInfoGetByKey(key);
            if (mai.IsLoaded == false)
            {
                //資料不存在
                return Redirect(config.SiteUrl);
            }

            Member mem = MemberFacade.GetMember(mai.UniqueId);
            string correctionMailUrl = "/mvc/NewMember/RegisterByEmail";
            correctionMailUrl += "?key=" + key;
            
            ViewBag.CorrectionMailUrl = correctionMailUrl;

            if(1 == send)
            {
               MemberSendConfirmMailReplyType replyType = MemberUtility.SendAccountConfirmMail(mem.UserEmail);
            }

            if (Session["RegisterByEmailWaitConfirmMssage"] != null)
            {
                ViewBag.ErrorMessage = Session["RegisterByEmailWaitConfirmMssage"].ToString();
                Session["RegisterByEmailWaitConfirmMssage"] = "";
            }

            return View();
        }

        private bool CheckData(RegisterViewModel register, out string message)
        {
            message = string.Empty;
            bool correctness = true;

            if (RegExRules.CheckPassword(register.Password) == false)
            {
                message += "請輸入有效6碼以上英文或數字密碼。";
                correctness = false;
            }

            if (register.Password.Equals(register.ConPassword) == false)
            {
                message += "您輸入的密碼不符合，請重新輸入。";
                correctness = false;
            }

            if (int.Equals(0, register.City))
            {
                message += "請選擇所在地";
                correctness = false;
            }

            if (!register.Agree)
            {
                message += "noAgree();";
                correctness = false;
            }

            if (String.IsNullOrWhiteSpace(register.Account))
            {
                message += "帳號未輸入";
                correctness = false;
            }

            if (!RegExRules.CheckEmail(register.Account) && !RegExRules.CheckMobile(register.Account))
            {
                message += "帳號格式不正確";
                correctness = false;
            }

            return correctness;
        }


    }
}

﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.WebLib.Models.Ppon;
using LunchKingSite.Core.Models;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;
using System.Net.Mail;
using log4net;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.WebLib.Controllers
{
    [RoutePrefix("ppon")]
    public class PponController : Controller
    {
        protected ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ICacheProvider _cache = ProviderFactory.Instance().GetDefaultProvider<ICacheProvider>();
        private IOrderProvider _op = ProviderFactory.Instance().GetDefaultProvider<IOrderProvider>();
        private IMemberProvider _mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();
        private static ILog logger = LogManager.GetLogger("PayService");

        [HttpPost]
        public ActionResult LoadSideDeals(List<Guid> bids, int workCityId, int takeCount)
        {
            workCityId = workCityId == PponCityGroup.DefaultPponCityGroup.NewTaipeiCity.CityId
                                 ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId
                                 : workCityId;
            List<SideDeal> sidedeals = PponFacade.GetSideDeals(bids, workCityId, takeCount);
            return PartialView("_PponSideDealsPartial", sidedeals);
        }

        #region SearchDeal

        [HttpPost]
        public ActionResult LoadSearchDeals(string querystring, string sortType, int? searchEngine, int? takeSize)
        {
            querystring = HttpUtility.UrlDecode(querystring, Encoding.UTF8);
            string cpa_code = "17_search";
            List<IViewPponDeal> searchDeals = null;

            DiscountCampaign campaign;

            if (SearchFacade.TryGetDiscountCampaignIdByCode(PponIdentity.Current.Id, querystring, out campaign))
            {
                searchDeals = SearchFacade.GetSearchDealsByDiscountCode(querystring, sortType, campaign, takeSize ?? 200);
            }
            else
            {
                searchDeals = SearchFacade.GetSearchDeals(querystring, sortType, searchEngine);
            }

            List<MultipleMainDealPreview> mmdeals = new List<MultipleMainDealPreview>();
            foreach (var deal in searchDeals)
            {
                mmdeals.Add(new MultipleMainDealPreview(0, 0, deal, new List<int>(), new DealTimeSlotStatus(), 0));
            }

            ViewBag.searchString = querystring;
            ViewBag.jsContentIds = PponFacade.GetSearchDealBid(searchDeals, (int)FacebookPixelCode.JsCode);
            ViewBag.nsContentIds = PponFacade.GetSearchDealBid(searchDeals, (int)FacebookPixelCode.NsCode);

            PponFacade.AddPponSearchLog(querystring, mmdeals.Count, Session.SessionID, User.Identity.Name);

            var mainDeals = GetViewMultipleMainDeal(mmdeals, new PponDealListCondition());
            ViewBag.CityId = 0;
            return View("_PponSearchDealsPartial", new KeyValuePair<List<ViewMultipleMainDeal>, string>(mainDeals, cpa_code));
        }

        public string GetDisCountString(IViewPponDeal deal)
        {
            string discount_string = string.Empty;

            if (Helper.IsFlagSet(deal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal))
            {
                discount_string = "公益";
            }
            else if (deal.ItemPrice == deal.ItemOrigPrice || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                  || Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
            {
                discount_string = "特選";
            }
            else if (deal.ItemPrice == 0)
            {
                decimal zeroprice = PponFacade.CheckZeroPriceToShowPrice(deal);
                if (deal.ExchangePrice.HasValue && deal.ExchangePrice.Value > default(decimal))
                {
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                    {
                        discount_string = "特選";
                    }
                    else
                    {
                        discount_string = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), zeroprice, deal.ItemOrigPrice);
                        discount_string += "<span class=\"smalltext\">折</span>";
                    }
                }
                else
                {
                    discount_string = "優惠";
                }
            }

            else
            {
                string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), deal.ItemPrice, deal.ItemOrigPrice);
                if (!string.IsNullOrEmpty(discount))
                {
                    discount_string = discount.Substring(0, 1);
                    if (discount.IndexOf('.') != -1 && discount.Length > 2)
                    {
                        discount_string += discount.Substring(1, discount.Length - 1);
                    }

                    discount_string += "<span class=\"smalltext\">折</span>";
                }
            }
            return discount_string;
        }

        public string GetDealIconClass(string labelIconList)
        {
            if (string.IsNullOrEmpty(labelIconList))
            {
                return string.Empty;
            }
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            List<int> iconList = labelIconList.Split(',').Select(int.Parse).ToList();
            foreach (int icon in iconList)
            {
                if (icon == (int)DealLabelSystemCode.CanBeUsedImmediately)
                {
                    sb.AppendFormat(" {0}", "Immediately");
                }
                else if (icon == (int)DealLabelSystemCode.ArriveIn24Hrs)
                {
                    sb.AppendFormat(" {0}", "ArriveIn24Hrs");
                }
                else if (icon == (int)DealLabelSystemCode.ArriveIn72Hrs)
                {
                    sb.AppendFormat(" {0}", "ArriveIn72Hrs");
                }
            }

            return sb.ToString();
        }
        #endregion

        #region TodayDeals

        [HttpPost]
        public ActionResult LoadSearchTodayDeals(int? cityid, int areaid, int? days = 0)
        {
            cityid = cityid ?? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            var pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid.Value);
            if (areaid == 0)
            {
                areaid = pponCity.CategoryId;
            }

            List<Guid> bids = PponDealApiManager.GetTodayChoicePponDealSynopsesByCache(areaid).Select(t => t.Bid).Take(9).ToList();

            List<MultipleMainDealPreview> result = new List<MultipleMainDealPreview>();
            foreach (Guid bid in bids)
            {
                var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                if (deal != null)
                {
                    result.Add(new MultipleMainDealPreview(0, 0, deal, new List<int>(), new DealTimeSlotStatus(), 0));
                }
            }
            var mainDeals = GetViewMultipleMainDeal(result, new PponDealListCondition());
            string cpa_code = "17_searchpromo";
            ViewBag.CityId = 0;
            return PartialView("_PponSearchDealsPartial", new KeyValuePair<List<ViewMultipleMainDeal>, string>(mainDeals, cpa_code));
        }

        [HttpPost]
        public ActionResult LoadTodayDeals(int cityid, int areaid, int? days = 0)
        {
            TodayDealCitySynopsis citydeals = GetTodayDeals(cityid, areaid, days);
            return PartialView("_PponTodayDealsPartial", citydeals);

        }

        private TodayDealCitySynopsis GetTodayDeals(int cityid, int areaid, int? days)
        {
            TodayDealCitySynopsis citydeals = new TodayDealCitySynopsis();
            PponCity city;

            if ((city = PponCityGroup.DefaultPponCityGroup.FirstOrDefault(x => x.CityId == cityid)) != null)
            {

                List<TodayDealSynopsis> deals = FilterByDays(ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(city.CityId), days.Value).Select(x => new TodayDealSynopsis(x)
                {
                    DealIcon = PponFacade.GetDealIconHtmlContent(x, 2),
                    DealPromoImageTag = (string.IsNullOrEmpty(x.DealPromoImage)) ? string.Empty : string.Format(@"<img src='{0}' alt='' />"
                                        , ImageFacade.GetMediaPath(x.DealPromoImage, MediaType.DealPromoImage)),
                    EventImageTag = ImageFacade.GetMediaPathsFromRawData(x.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(config.SiteUrl + "/Themes/PCweb/images/ppon-M1_pic.jpg").First(),
                    SubstringName = CommonFacade.GetStringByByteLength(GetDealShortTitle(x), 49, "..."),
                    SubstringTitle = CommonFacade.GetStringByByteLength(x.EventTitle, 112, "..."),
                    CountDownString = GetCountdown(x.BusinessHourOrderTimeE, DateTime.Now),
                    OrderedQuantityShow = OrderedQuantityHelper.Show(x, OrderedQuantityHelper.ShowType.TodayDeals),
                    DiscountString = GetDisCountString(x),
                    DealIconClass = GetDealIconClass(x.LabelIconList),
                    EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(x.BusinessHourGuid),
                }).ToList();

                #region 地區->SPA專區撈取

                if (cityid == PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId && cityid != areaid)
                {
                    List<CategoryNode> spaAreaNodes = new List<CategoryNode>();
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaipei);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaoyuan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleHsinchu);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTaichung);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleTainan);
                    spaAreaNodes.Add(CategoryManager.Default.FemaleKaohsiung);

                    var spaAreaNode = spaAreaNodes.FirstOrDefault(x => x.CityId == areaid);

                    if (spaAreaNode != null)
                    {
                        var areaCategoryId = (spaAreaNode.CityId.HasValue) ? spaAreaNode.CategoryId : spaAreaNodes.First().CategoryId;

                        List<MultipleMainDealPreview> multideals;

                        if (spaAreaNode.CityId.HasValue)
                        {
                            multideals = ViewPponDealManager.DefaultManager.MultipleMainDealPreviewGetListByChannel(city.ChannelId.Value, areaCategoryId, null);
                            List<TodayDealSynopsis> tempdeals = new List<TodayDealSynopsis>();

                            foreach (var deal in deals)
                            {
                                if (multideals.Any(z => z.PponDeal.BusinessHourGuid == deal.Bid))
                                {
                                    tempdeals.Add(deal);
                                }
                            }
                            deals = tempdeals;
                        }
                    }

                }

                #endregion 地區->SPA專區撈取

                int i = 0;
                foreach (var item in deals.GroupBy(x => x.BusinessHourOrderTimeE))
                {
                    foreach (var inneritem in item)
                    {
                        inneritem.GroupByCountDown = string.Format("{0}_{1}", cityid, i);
                    }
                    i++;
                }
                if (deals.Count > 0)
                {
                    citydeals.GroupByDate = deals.OrderBy(x => x.BusinessHourOrderTimeE).Last().BusinessHourOrderTimeE;
                }
                citydeals.Deals = deals;
                citydeals.CityName = city.CityName;
                citydeals.CityId = city.CityId;
                citydeals.CountDownTotal = i;
            }
            return citydeals;
        }

        private string GetDealShortTitle(IViewPponDeal deal)
        {
            string shortTitle = (string.IsNullOrEmpty(deal.AppTitle)) ? deal.ItemName : deal.AppTitle;
            shortTitle = (deal.DeliveryType == (int)DeliveryType.ToHouse) ? string.Format("宅配：{0}", shortTitle) : shortTitle;
            return shortTitle.Replace("[72H到貨]", "").Replace("[24H到貨]", "");
        }

        private string GetTravelPlace(Guid bid, ViewPponDeal deal)
        {
            string place = deal.TravelPlace;
            if (string.IsNullOrEmpty(place))
            {
                return CommonFacade.GetStringByByteLength(GetDealShortTitle(deal), 50, "...");
            }
            else
            {
                return CommonFacade.GetStringByByteLength(string.Format("{0}-{1}", place, GetDealShortTitle(deal)), 50, "...").Replace("[72H到貨]", "").Replace("[24H到貨]", "");
            }
        }

        private List<IViewPponDeal> FilterByDays(List<IViewPponDeal> vpdCol, int days)
        {
            List<IViewPponDeal> result = new List<IViewPponDeal>();
            if (days == 0)
            {
                return vpdCol;
            }
            else
            {
                result.AddRange(vpdCol.Where(t => (DateTime.Now - t.BusinessHourOrderTimeS).TotalDays < days).ToList());
                return result;
            }
        }

        private string GetCountdown(DateTime timeE, DateTime timeS)
        {
            DateTime now = DateTime.Now;
            int year = timeE.Year;
            int month = timeE.Month;
            int day = timeE.Day;
            int hour = timeE.Hour;
            int min = timeE.Minute;
            int sec = timeE.Second;
            if (DateTime.Compare(new DateTime(now.Year, now.Month, now.Day, hour, min, sec), now) > 0)
            {
                if ((now - timeS).Days < 1 && (timeE - now).Days >= 1)
                {
                    year = now.AddDays(1).Year;
                    month = now.AddDays(1).Month;
                    day = now.AddDays(1).Day;
                }
                else
                {
                    year = now.Year;
                    month = now.Month;
                    day = now.Day;
                }
            }
            else
            {
                year = now.AddDays(1).Year;
                month = now.AddDays(1).Month;
                day = now.AddDays(1).Day;
            }
            TimeSpan ts = timeE - DateTime.Now;
            int hourUntil = (int)Math.Floor(ts.TotalHours);
            int minutesUntil = (int)Math.Floor(ts.TotalMinutes - hourUntil * 60);
            int secondsUntil = (int)Math.Floor(ts.TotalSeconds - hourUntil * 3600 - minutesUntil * 60);
            return hourUntil + I18N.Phrase.Hour + minutesUntil + I18N.Phrase.Minute + secondsUntil + I18N.Phrase.Second;
        }
        #endregion

        #region PartialView
        /// <summary>
        /// 回傳區域選單PartialView
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public ActionResult CategoryTree(string url)
        {
            PponDealListCondition condition = new PponDealListCondition(url);
            int channelId = condition.ChannelId;
            List<CategoryTreeNode> categoryTree = CategoryManager._channelCategoryTree;
            CategoryTreeNode treeNode = categoryTree.Where(x => x.CategoryId == channelId).FirstOrDefault();

            ViewBag.CurrentCityId = condition.CityId;
            ViewBag.CurrentRegionId = condition.AreaId;
            ViewBag.CurrentCategoryId = condition.SubCategoryId;

            string areaTitle = string.Empty;
            //var root = HxRootCategory.Create();
            //if (condition.AreaId != 0)
            //{
            //    areaTitle += root.Get(condition.AreaId).Name;
            //    if (condition.SubCategoryId != 0 && condition.AreaId != condition.SubCategoryId)
            //    {
            //        areaTitle += " / " + root.Get(condition.SubCategoryId).Name;
            //    }
            //}
            //else
            //{
            //    areaTitle = CategoryManager.GetRegionDisplayRuleByCid(condition.ChannelId).SummaryItemName;
            //}
            if (condition.AreaId != 0)
            {
                CategoryTreeNode area = CategoryManager.Find(condition.ChannelId, condition.AreaId);
                if (area != null)
                {
                    areaTitle = area.Name;
                }
                if (condition.SubCategoryId != 0 && condition.AreaId != condition.SubCategoryId)
                {
                    CategoryTreeNode subArea = CategoryManager.Find(condition.ChannelId, condition.SubCategoryId);
                    if (subArea != null)
                    {
                        areaTitle += " / " + subArea.Name;
                    }
                }
            }
            else
            {
                areaTitle = CategoryManager.GetRegionDisplayRuleByCid(condition.ChannelId).SummaryItemName;
            }
            ViewBag.LocationName = areaTitle;


            return PartialView("_PponCategoryTree", treeNode);
        }



        /// <summary>
        /// 根據條件回傳首頁檔次列表的PartialView
        /// </summary>
        /// <param name="url"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public PartialViewResult PponDealList(string url, int? page)
        {
            if (page > 1) {
                _cache.Increment("DefaultPponDealList");
            }
            List<ViewMultipleMainDeal> model = new List<ViewMultipleMainDeal>();
            if (page.HasValue || !page.Equals(0))
            {
                //取得查詢條件
                PponDealListCondition condition = new PponDealListCondition(url);
                int size = config.NumberOfDealInOnePage;
                int skipCount = ((int)page - 1) * size;
                ViewBag.CityId = condition.CityId;

                //根據條件取得列表，並排序
                CategorySortType sortType = (CategorySortType)(System.Web.HttpContext.Current.Session["SortType"] ?? CategorySortType.Default);

                List<MultipleMainDealPreview> dealList;

                dealList = PponFacade.GetDealListByCondition(condition);
                dealList = ViewPponDealManager.DefaultManager.SortMultipleMainDealPreview(dealList, sortType).ToList();

                ViewBag.IsEnd = (page * size) >= dealList.Count();

                ViewBag.ContentName = PponFacade.GetContentName(condition.ChannelId, condition.NewDealCategoryId);
                ViewBag.ContentCategory = PponFacade.GetContentCategory(condition.ChannelId, condition.NewDealCategoryId);
                ViewBag.jsContentIds = PponFacade.GetPponDealBid(dealList, (int)FacebookPixelCode.JsCode);
                ViewBag.nsContentIds = PponFacade.GetPponDealBid(dealList, (int)FacebookPixelCode.NsCode);
                ViewBag.ChannelId = condition.ChannelId;

                //分頁
                var dealListInRange = dealList.Skip(skipCount).Take(size).ToList();
                model = GetViewMultipleMainDeal(dealListInRange, condition);
            }
            else
            {
                ViewBag.IsEnd = true;
            }

            return PartialView("_PponDealList", model);
        }

        /// <summary>
        /// 回傳店家資訊PartialView
        /// </summary>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public PartialViewResult StoreInformation(ViewPponDeal vpd)
        {
            var model = new List<StoreInformation>();
            if (vpd != null && vpd.IsLoaded)
            {
                var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(vpd);
                if (!string.IsNullOrWhiteSpace(contentLite.Availability))
                {
                    model = new StoreInformation().GetStoreInformationListByAvailability(contentLite.Availability);
                }
            }

            return PartialView("_StoreInformation", model);
        }
        #endregion

        #region Private method

        #region 前台檔次列表相關

        /// <summary>
        /// 將Default頁檔次列表整理成PartialView要顯示的資訊 (改寫自Default.aspx.cs的SetMultipleMainDeal)
        /// </summary>
        /// <param name="deals"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        private List<ViewMultipleMainDeal> GetViewMultipleMainDeal(List<MultipleMainDealPreview> deals, PponDealListCondition condition)
        {
            List<ViewMultipleMainDeal> viewMultipleMainDeals = new List<ViewMultipleMainDeal>();

            foreach (var deal in deals)
            {
                ViewMultipleMainDeal viewDealPreview = new ViewMultipleMainDeal();
                viewDealPreview.deal = deal;
                bool _isKindDeal = Helper.IsFlagSet(deal.PponDeal.GroupOrderStatus ?? 0, GroupOrderStatus.KindDeal);
                int cityId = condition.CityId;
                PponCity pponcity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(deal.CityID);

                //貼上檔次 Icon 標籤
                string icon_string = PponFacade.GetDealIconHtmlContentList(deal.PponDeal, 2);


                viewDealPreview.deal_Label_1 = icon_string;
                viewDealPreview.deal_Label_2 = icon_string;
                viewDealPreview.clit_CityName =
                    deal.PponDeal.IsMultipleStores
                    ? "<div class='hover_place' style='display:none;'><span class='hover_place_text'><i class='fa fa-map-marker'></i>"
                        + deal.PponDeal.HoverMessage + "</span></div>"
                   : string.Empty;

                viewDealPreview.clit_CityName2 =
                    ViewPponDealManager.DefaultManager.GetCityNameOrTravelPlace(pponcity, deal.PponDeal.BusinessHourGuid);


                switch (deal.PponDeal.GetDealStage())
                {
                    case PponDealStage.Created:
                    case PponDealStage.Ready:
                        viewDealPreview.litCountdownTime = "即將開賣";
                        break;

                    case PponDealStage.ClosedAndFail:
                    case PponDealStage.ClosedAndOn:
                    case PponDealStage.CouponGenerated:
                        viewDealPreview.litCountdownTime = "已結束販售";
                        break;
                    case PponDealStage.Running:
                    case PponDealStage.RunningAndFull:
                    case PponDealStage.RunningAndOn:
                    default:
                        if (PponFacade.IsEveryDayNewDeal(deal.PponDeal.BusinessHourGuid))
                        {
                            EveryDayNewDeal flag = ViewPponDealManager.DefaultManager.GetEveryDayNewDealByBid(deal.PponDeal.BusinessHourGuid);
                            DateTime end = DateTime.Now;
                            if (flag == EveryDayNewDeal.Limited24HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(1);
                            else if (flag == EveryDayNewDeal.Limited48HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(2);
                            else if (flag == EveryDayNewDeal.Limited72HR)
                                end = deal.PponDeal.BusinessHourOrderTimeS.AddDays(3);
                            else
                                end = deal.PponDeal.BusinessHourOrderTimeE;

                            int daysToClose = (int)(end - DateTime.Now).TotalDays;

                            if (daysToClose > 3)
                            {
                                viewDealPreview.litCountdownTime = "限時優惠中!";
                            }
                            else
                            {
                                viewDealPreview.litDays = daysToClose + Phrase.Day;
                                viewDealPreview.litCountdownTime = SetCountdown(end);
                                viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataeverydaynewdealend = end.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_everydaynewdeal = true;
                            }
                        }
                        else
                        {
                            int daysToClose = (int)(deal.PponDeal.BusinessHourOrderTimeE - DateTime.Now).TotalDays;
                            if (daysToClose > 3)
                            {
                                viewDealPreview.litCountdownTime = "限時優惠中!";
                            }
                            else
                            {
                                viewDealPreview.litDays = daysToClose + Phrase.Day;
                                viewDealPreview.litCountdownTime = SetCountdown(deal.PponDeal.BusinessHourOrderTimeE);

                                viewDealPreview.timerTitleField_dataStart = deal.PponDeal.BusinessHourOrderTimeS.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_dataEnd = deal.PponDeal.BusinessHourOrderTimeE.ToJavaScriptMilliseconds();
                                viewDealPreview.timerTitleField_everydaynewdeal = false;
                            }
                        }

                        break;
                }

                if (_isKindDeal)
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "公益";
                }
                else if (deal.PponDeal.ItemPrice == deal.PponDeal.ItemOrigPrice || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown)
                      || Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.PriceZeorShowOriginalPrice))
                {
                    viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                }
                else if (deal.PponDeal.ItemPrice == 0)
                {
                    if (cityId.Equals(PponCityGroup.DefaultPponCityGroup.Family.CityId) && deal.PponDeal.ExchangePrice.HasValue && deal.PponDeal.ExchangePrice.Value > default(decimal))
                    {
                        if (Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown))
                        {
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 = "特選";
                        }
                        else
                        {
                            viewDealPreview.discount_2 = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, cityId), deal.PponDeal.ItemOrigPrice);
                            viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                            viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                        }
                    }
                    else
                    {
                        viewDealPreview.discount_1 = viewDealPreview.discount_2 = "優惠";
                    }
                }
                else
                {
                    string discount = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(deal.PponDeal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), PponFacade.CheckZeroPriceToShowPrice(deal.PponDeal, cityId), deal.PponDeal.ItemOrigPrice);
                    int length = discount.Length;
                    if (length > 0)
                    {
                        viewDealPreview.discount_2 = discount.Substring(0, 1);
                        if (discount.IndexOf('.') != -1 && length > 2)
                        {
                            viewDealPreview.discount_2 += discount.Substring(1, length - 1);
                        }

                        viewDealPreview.discount_1 = viewDealPreview.discount_2 + "折";
                        viewDealPreview.discount_2 += "<span class=\"smalltext\">折</span>";
                    }
                }

                if (config.InstallmentPayEnabled == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else if (deal.PponDeal.Installment3months.GetValueOrDefault() == false)
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p>好康價</p>";
                }
                else
                {
                    viewDealPreview.litInstallmentPriceLabel = "<p class='price_installment'>分期0利率</p>";
                }
                viewMultipleMainDeals.Add(viewDealPreview);
                decimal ratingScore;
                int ratingNumber;
                bool ratingShow;
                viewDealPreview.RatingString = PponFacade.GetDealStarRating(
                    deal.PponDeal.BusinessHourGuid, "pc",
                    out ratingScore, out ratingNumber, out ratingShow);
                viewDealPreview.RatingScore = ratingScore;
                viewDealPreview.RatingNumber = ratingNumber;
                viewDealPreview.RatingShow = ratingShow;

                viewDealPreview.EveryDayNewDeal = PponFacade.GetEveryDayNewDealFlag(deal.PponDeal.BusinessHourGuid);
            }

            return viewMultipleMainDeals;
        }
        /// <summary>
        /// 控制項DealTimer.ascx中的function
        /// </summary>
        /// <param name="timeE"></param>
        /// <returns></returns>
        private string SetCountdown(DateTime timeE)
        {
            TimeSpan ts = timeE - DateTime.Now;
            int hourUntil = ts.Hours;
            int minutesUntil = ts.Minutes;
            int secondsUntil = ts.Seconds;

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<div class='TimeConDigit hn'>{0}</div>", hourUntil);
            sb.AppendFormat("<div class='TimeConUnit hl'>{0}</div>", Phrase.Hour);
            sb.AppendFormat("<div class='TimeConDigit mn'>{0}</div>", minutesUntil);
            sb.AppendFormat("<div class='TimeConUnit ml'>{0}</div>", "分");
            sb.AppendFormat("<div class='TimeConDigit sn'>{0}</div>", secondsUntil);
            sb.AppendFormat("<div class='TimeConUnit sl'>{0}</div>", Phrase.Second);
            return sb.ToString();
        }
        #endregion

        #endregion

        [Route("hotTags")]
        [SwitchableHttps]
        public ActionResult HotTags()
        {
            HotTagModel model = PromotionFacade.GetPromoSearchKeys();
            return View(model);
        }

        /// <summary>
        /// App檔次內頁說明顯示
        /// </summary>
        /// <param name="bid">檔次編號</param>
        /// <param name="type">1:Description  2:Introduction 3: piinLife</param>
        /// <param name="tab"></param>
        /// <param name="section">1:優惠方案 2: 詳細介紹 4: 商品規格 7: 全部</param>
        /// <returns></returns>
        public ActionResult PponDescription(Guid bid, int type, string tab, int section = 7)
        {
            ViewBag.rtnObj = LunchKingSite.BizLogic.Component.API.PponDealApiManager.PponDealDescription(bid, type, tab, section);

            return View();
        }

        public ActionResult AppTopBanner(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            List<int> cityList = new JsonSerializer().Deserialize<List<int>>(deal.CityList);

            ViewCmsRandomCollection data = CmsRandomFacade.GetOrAddData("/ppon/default.aspx_pponnews", RandomCmsType.PponRandomCms);
            DateTime nowDateTime = DateTime.Now;

            List<string> rtn = data.Where(x => cityList.Contains(x.CityId)
                   && x.StartTime <= nowDateTime && x.EndTime >= nowDateTime
                   && x.Body.Contains("BrandEvent.aspx", StringComparison.InvariantCultureIgnoreCase))
                   .Select(x => x.Body).Take(5)//.Distinct()
                   .OrderBy(t => Guid.NewGuid()).ToList();

            return View(rtn);
        }

        #region PponDescription 非同步的版本
        public async Task<ActionResult> PponDescriptionAsync(Guid bid, int type, string tab, int section = 7)
        {
            var viewResult = new ViewResult()
            {
                ViewName = "PponDescription",
            };
            viewResult.ViewBag.rtnObj = await PponDescriptionTask(bid, type, tab, section);
            return viewResult;
        }

        private async Task<string> PponDescriptionTask(Guid bid, int type, string tab, int section = 7)
        {
            return await Task.Run<string>(delegate ()
            {
                string res = LunchKingSite.BizLogic.Component.API.PponDealApiManager.PponDealDescription(bid, type, tab, section);
                return res;
            });
        }
        #endregion

        [ChildActionOnly]
        public ActionResult RandomParagraphCuration()
        {
            int cityId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            List<ViewCmsRandom> rtn = CmsRandomFacade.GetViewCmsRandomsByCity(RandomCmsType.PponMasterPage, "curation", cityId);
            return View(rtn);
        }

        #region Contact us
        public ActionResult ContactUs()
        {
            List<IViewPponDeal> vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByTopSalesDeal(3);
            return View(vpd);
        }

        [HttpPost]
        public JsonResult SendBusinessMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope)
        {
            ILog log = LogManager.GetLogger("MailLog");
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            log.InfoFormat("招商專區(實體商品)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope }))
            {
                log.InfoFormat("招商專區(實體商品)->{0}({1}) validation error.", companyName, email);
                return Json(new { success = false, Message = "input validation error." });
            }

            try
            {
                SolicitEmailBody sbe = new SolicitEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(實體商品)-{0}", companyName), companyName, sbe.GetBusinessMailBody(hope), out sendResultMessage, (int)ContactType.BusinessMail);
            }
            catch (Exception ex)
            {
                log.InfoFormat("招商專區(實體商品)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return Json(new { success = sendSuccess, Message = sendResultMessage });
        }

        [HttpPost]
        public JsonResult SendMarketMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope)
        {
            ILog log = LogManager.GetLogger("MailLog");
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            log.InfoFormat("招商專區(非實體商品)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope }))
            {
                log.InfoFormat("招商專區(非實體商品)->{0}({1}) validation error.", companyName, email);
                return Json(new { success = false, Message = "input validation error." });
            }

            try
            {
                SolicitEmailBody sbe = new SolicitEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(非實體商品)-{0}", companyName), companyName, sbe.GetMarketMailBody(hope), out sendResultMessage, (int)ContactType.BusinessMail);
            }
            catch (Exception ex)
            {
                log.InfoFormat("招商專區(非實體商品)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return Json(new { success = sendSuccess, Message = sendResultMessage });
        }

        [HttpPost]
        public JsonResult SendKindMail(string companyName, string address, string contactName, string contactPhoneNo,
            string callBackTime, string email, string hope)
        {
            ILog log = LogManager.GetLogger("MailLog");
            string sendResultMessage = string.Empty;
            bool sendSuccess = false;

            log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}) Send Mail Start.", companyName, email);
            if (!checkStringNull(new string[] { companyName, address, contactName, contactPhoneNo, email, hope }))
            {
                log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}) validation error.", companyName, email);
                return Json(new { success = false, Message = "input validation error." });
            }

            try
            {
                SolicitEmailBody sbe = new SolicitEmailBody(companyName, address, contactName, contactPhoneNo, callBackTime, email);
                sendSuccess = SendMail(string.Format("招商專區(行銷與公益合作)-{0}", companyName), companyName, sbe.GetKindMailBody(hope), out sendResultMessage, (int)ContactType.BusinessMail);
            }
            catch (Exception ex)
            {
                log.InfoFormat("招商專區(行銷與公益合作)->{0}({1}):{2}, {3}", companyName, email, ex.Message, ex.StackTrace);
            }
            return Json(new { success = sendSuccess, Message = sendResultMessage });
        }

        private static bool SendMail(string subject, string companyName, string contentHtml, out string resultMessage, int contactType)
        {
            ISysConfProvider conf = ProviderFactory.Instance().GetConfig();
            ILog log = LogManager.GetLogger("MailLog");

            try
            {
                using (MailMessage msg = new MailMessage())
                {
                    StringBuilder sb = new StringBuilder();

                    sb.AppendLine("<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
                    sb.AppendLine("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" /><title>");
                    sb.AppendLine("招商信件通知</title></head><body>");

                    msg.From = new MailAddress(conf.PponServiceEmail, companyName);

                    if (contactType == (int)ContactType.BusinessMail)
                    {
                        if (conf.SolicitBusinessEmail.Contains(";"))
                        {
                            var tmpAddr = conf.SolicitBusinessEmail.Split(";");
                            foreach (var t in tmpAddr)
                            {
                                msg.To.Add(new MailAddress(t, conf.ServiceName));
                            }
                        }
                        else
                        {
                            msg.To.Add(new MailAddress(conf.SolicitBusinessEmail, conf.ServiceName));
                        }
                    }
                    else if (contactType == (int)ContactType.MarketMail)
                    {
                        if (conf.SolicitMarketEmail.Contains(";"))
                        {
                            var tmpAddr = conf.SolicitMarketEmail.Split(";");
                            foreach (var t in tmpAddr)
                            {
                                msg.To.Add(new MailAddress(t, conf.ServiceName));
                            }
                        }
                        else
                        {
                            msg.To.Add(new MailAddress(conf.SolicitMarketEmail, conf.ServiceName));
                        }
                    }
                    else if (contactType == (int)ContactType.KindMail)
                    {
                        if (conf.SolicitKindEmail.Contains(";"))
                        {
                            var tmpAddr = conf.SolicitKindEmail.Split(";");
                            foreach (var t in tmpAddr)
                            {
                                msg.To.Add(new MailAddress(t, conf.ServiceName));
                            }
                        }
                        else
                        {
                            msg.To.Add(new MailAddress(conf.SolicitKindEmail, conf.ServiceName));
                        }
                    }

                    sb.AppendLine("<h2>" + subject + "</h2>");
                    sb.Append(contentHtml);
                    sb.AppendLine("</body></html>");

                    msg.Subject = subject;
                    msg.Body = sb.ToString();
                    sb.Clear();
                    msg.IsBodyHtml = true;
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                    resultMessage = "Send Success";
                }
            }
            catch (Exception ex)
            {
                log.InfoFormat("招商專區({0})->{1}:{2}, {3}", subject, companyName, ex.Message, ex.StackTrace);
                resultMessage = ex.Message;
                return false;
            }
            return true;
        }

        private bool checkStringNull(string[] checkStringList)
        {
            for (int i = 0; i < checkStringList.Length; i++)
            {
                if (string.IsNullOrEmpty(checkStringList[i]))
                {
                    return false;
                }
            }
            return true;
        }
        #endregion

        /// <summary>
        /// 國泰PG OTP Step2
        /// </summary>
        /// <returns></returns>
        [Route("CathayOtpRedirect")]
        public ActionResult CathayOtpRedirect()
        {
            logger.Info("[CathayOtpRedirect]");
            string redirectUrl = config.SSLSiteUrl;
            try
            {
                string body = "";
                using (var sr = new System.IO.StreamReader(Request.InputStream))
                {
                    body = sr.ReadToEnd();
                }
                logger.Info("[body]" + body);
                Order o = null;
                string transId = string.Empty;
                string strOrderInfo = body.Contains("&strOrderInfo=") ? body.Split("&strOrderInfo=")[1] : string.Empty;
                string strRsXML = body.Contains("strRsXML=") ? body.Split("&strOrderInfo=")[0].Replace("strRsXML=", string.Empty).Replace("&strRsJSON=", string.Empty) : string.Empty;
                if (!string.IsNullOrEmpty(strOrderInfo))
                {
                    //step2
                    logger.Info("[step2]導回訂單完成頁");
                    System.Xml.XmlDocument xml = new System.Xml.XmlDocument();
                    strOrderInfo = HttpUtility.UrlDecode(strOrderInfo);
                    xml.LoadXml(strOrderInfo);
                    string json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xml);
                    var orderResult = Newtonsoft.Json.JsonConvert.DeserializeObject<CathayPaymentOtpFinalResult>(json);

                    transId = orderResult.CUBXML.ORDERINFO.ORDERNUMBER.ToLower();
                    PaymentTransaction ptCreditCard = _op.PaymentTransactionGet(transId, LunchKingSite.Core.PaymentType.Creditcard, PayTransType.Authorization);
                    o = _op.OrderGet(ptCreditCard.OrderGuid.Value);

                    TempCreditcardOtp otpInfo = _op.TempCreditcardOtpGet(transId);
                    string ticketId = otpInfo.TicketId;
                    var paymentDto = PponBuyFacade.GetPponPaymentDTOFromSession(ticketId);
                    //APP導回otploading 、 web導回buy頁
                    redirectUrl = Helper.CombineUrl(config.SSLSiteUrl, string.Format("ppon/buy.aspx?TransId={0}&TicketId={1}", transId, ticketId));
                 
                    switch (o.OrderFromType)
                    {
                        case (int)OrderFromType.ByAndroid:
                        case (int)OrderFromType.ByIOS:
                        case (int)OrderFromType.ByWinPhone:
                            string retCode = "";
                            string retMsg = "";
                            string apiUserId = otpInfo.AppUserId;
                            if (!string.IsNullOrEmpty(strRsXML))
                            {
                                strRsXML = HttpUtility.UrlDecode(strRsXML);
                                xml = new System.Xml.XmlDocument();
                                xml.LoadXml(strRsXML);
                                json = Newtonsoft.Json.JsonConvert.SerializeXmlNode(xml);
                                var authResult = Newtonsoft.Json.JsonConvert.DeserializeObject<CathayPaymentOtpFirstResult>(json);
                                retCode = authResult.CUBXML.AUTHINFO.AUTHCODE;
                                retMsg = authResult.CUBXML.AUTHINFO.AUTHMSG;
                            }
                            redirectUrl = Helper.CombineUrl(config.SSLSiteUrl, string.Format("/ppon/OTPLoading?oGuid={0}&ticketid={1}&apiUserId={2}&ret_code={3}&ret_msg={4}", o.Guid, ticketId, apiUserId, retCode, retMsg));
                            break;
                    }
                    logger.Info("[redirectUrl]" + redirectUrl);

                    logger.Info("[deleteOtp]transId:" + otpInfo.TransId);
                    _op.TempCreditcardOtpDelete(otpInfo.Id);

                    return Redirect(redirectUrl);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return Redirect(redirectUrl);
        }

        /// <summary>
        /// 導到國泰OTP驗證頁面
        /// </summary>
        /// <returns></returns>
        [Route("CathayOtpConfirmPage")]
        public ActionResult CathayOtpConfirmPage(string transId, string ticketId, string appUserId = "")
        {
            logger.Info("[CathayOtpConfirmPage]");
            DateTime now = DateTime.Now;
            string otpUrl = config.CathayPaymnetGatewayApi + "Payment/PaymentInitial.aspx";
            TempCreditcardOtp otp = _op.TempCreditcardOtpGet(transId);
            if (otp.IsLoaded)
            {
                //檢查建立時間是否小於1分鐘，否則失敗
                if ((now.Ticks - otp.CreateTime.Ticks) / 10000000 < 60)
                {
                    //檢查userid
                    //int userId = MemberFacade.GetUniqueId(User.Identity.Name);
                    //logger.Info("[userId]" + userId.ToString());
                    //if (otp.UserId.Equals(userId))
                    //{
                    //logger.Info("[Create Otp Session]");

                    logger.Info("[Post OTPInfo]");
                    //取出發送的加密資訊後馬上清空
                    var otpData = MemberFacade.DecryptCreditCardInfo(otp.OtpInfo, otp.EncryptSalt);

                    otp.OtpInfo = string.Empty;
                    otp.EncryptSalt = string.Empty;
                    otp.TicketId = ticketId;
                    otp.AppUserId = appUserId;
                    _op.TempCreditcardOtpSet(otp);

                    string postData ="<!DOCTYPE html><html><head><meta charset='utf-8'></head>";
                    postData += "<body><form name='main' method='post' action='" + otpUrl + "'>";
                    postData += "<input type='hidden' name='strRqXML' value='" + otpData + "'>";
                    postData += "</form><script>document.main.submit();</script></body></html>";
                        
                    return Content(postData);
                }

                //清空otpinfo
                logger.Info("[deleteOtp]transId:" + otp.TransId);
                _op.TempCreditcardOtpDelete(otp.Id);
            }

            return View();
        }

        [Route("OTPLoading")]
        public ActionResult OTPLoading(string oGuid = "", string ticketId = "", string apiUserId = "", string ret_code = "", string ret_msg = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(ticketId) && !string.IsNullOrEmpty(apiUserId))
                {
                    apiUserId = apiUserId.Replace("?s_mid=", "");
                    Guid guid = Guid.Empty;
                    Guid.TryParse(oGuid, out guid);
                    if (guid != Guid.Empty)
                    {
                        Order o = _op.OrderGet(Guid.Parse(oGuid));
                        int userId = o.UserId;
                        //取回paymentDTO
                        PaymentDTO paymentDTO = PaymentFacade.GetPaymentDTOFromSession(ticketId);
                        if (paymentDTO != null)
                        {
                            Security sec = new Security(SymmetricCryptoServiceProvider.AES);
                            string creditcardInfo = sec.Encrypt(paymentDTO.CreditcardNumber);

                            PaymentFacade.OTPApiLog(paymentDTO.TransactionId, Newtonsoft.Json.JsonConvert.SerializeObject(new { oGuid = oGuid.ToString(), ticketId = ticketId, apiUserId = apiUserId, ret_code = ret_code, ret_msg = ret_msg }));


                            OTPMakePaymentModel model = new OTPMakePaymentModel();
                            model.ticketId = ticketId;
                            model.apiUserId = apiUserId;
                            model.userId = userId;
                            model.paymentDTO = paymentDTO;

                            string transId = paymentDTO.TransactionId;//失敗會清空,先記錄

                            ApiReturnObject result = PaymentFacade.OTPMakePayment(model);
                            result.Message = result.Message.Replace("\r", "").Replace("\n", "");

                            PaymentFacade.OTPApiLog(transId, Newtonsoft.Json.JsonConvert.SerializeObject(new { code = (int)result.Code, message = result.Message }));

                            if (result.Code == ApiReturnCode.Success)
                            {
                                string isATM = result.Data.GetPropertyValue("IsATM").ToString();
                                string orderGuid = result.Data.GetPropertyValue("OrderGuid").ToString();


                                Response.Redirect(config.SSLSiteUrl + "/ppon/OTPFinish?code=" + (int)result.Code + "&isATM=" + isATM.ToString() + "&orderGuid=" + orderGuid + "&message=" + result.Message);
                            }
                            else
                            {
                                Response.Redirect(config.SSLSiteUrl + "/ppon/OTPFinish?code=" + (int)result.Code + "&message=" + result.Message);
                            }
                        }
                        else
                        {
                            PaymentFacade.OTPApiLog("", Newtonsoft.Json.JsonConvert.SerializeObject(new { oGuid = oGuid.ToString(), ticketId = ticketId, apiUserId = apiUserId, ret_code = ret_code, ret_msg = ret_msg }));
                            logger.Error("OTPLoading找不到paymentDTO");
                        }
                    }
                    else
                    {
                        PaymentFacade.OTPApiLog("", Newtonsoft.Json.JsonConvert.SerializeObject(new { oGuid = oGuid.ToString(), ticketId = ticketId, apiUserId = apiUserId, ret_code = ret_code, ret_msg = ret_msg }));
                        logger.Error("OTPLoading資料異常oGuid=" + guid);
                    }
                }
                else
                {
                    PaymentFacade.OTPApiLog("", Newtonsoft.Json.JsonConvert.SerializeObject(new { oGuid = oGuid.ToString(), ticketId = ticketId, apiUserId = apiUserId, ret_code = ret_code, ret_msg = ret_msg }));
                    logger.Error("OTPLoading資料異常:ticketId=" + ticketId + " apiUserId=" + apiUserId);
                }
            }
            catch (Exception ex)
            {
                PaymentFacade.OTPApiLog("", Newtonsoft.Json.JsonConvert.SerializeObject(new { oGuid = oGuid.ToString(), ticketId = ticketId, apiUserId = apiUserId, ret_code = ret_code, ret_msg = ret_msg }));
                logger.Error("OTPLoading錯誤", ex);
            }

            return View();
        }

        [Route("OTPFinish")]
        public ActionResult OTPFinish(string code, string isATM, string orderGuid, string message)
        {
            return View();
        }

        [Route("PezExchange")]
        public ActionResult PezExchange()
        {
            string externalUserId = string.Empty;
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);//app開webview會帶入身分token
            PcashMemberLink oldLink = _mp.PcashMemberLinkGet(userId);
            if (oldLink.IsLoaded)
            {
                externalUserId = oldLink.ExternalUserId;
            }


            ViewBag.ExternalUserId = externalUserId;
            ViewBag.MerchantContent = PayeasyFacade.GetMerchantContent();
            ViewBag.SSLSiteUrl = config.SSLSiteUrl;
            ViewBag.PcashXchClientId = config.PcashXchClientId;
            ViewBag.PcashXchClientSecret = config.PcashXchClientSecret;
            ViewBag.UserName = User.Identity.Name;
            return View();
        }

        [Route("PezExchangeFinish")]
        public ActionResult PezExchangeFinish()
        {
            return View();
        }

        [Route("PscashExchange")]
        public ActionResult PscashExchange()
        {
            return View();
        }
    }
}
﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers
{
    public class VbsProposalController : BaseProposalController
    {
        #region property
        public string UserId
        {
            get
            {
                return Session[VbsSession.UserId.ToString()].ToString();
            }
        }
        #endregion property

        #region action
        #region 提案單
        [VbsAuthorize]
        public override ActionResult HouseProposalContent(int? pid)
        {
            try
            {
                if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    ViewBag.LoginBy17LifeSimulate = true;
                }

                Proposal pro = sp.ProposalGet(pid.GetValueOrDefault());
                ProposalLog vbsLogs = null;
                if (pro.IsLoaded)
                {
                    List<Seller> vbsSellers = ProposalFacade.GetVbsSellerByCol(UserId, true);
                    if (vbsSellers.Where(x => x.Guid == pro.SellerGuid).FirstOrDefault() == null)
                    {
                        throw new Exception("錯誤的賣家帳號 \n " + "pid=" + pro.Id + " \n SellerGuid=" + pro.SellerGuid + "\n vbsSellers=" + new JsonSerializer().Serialize(vbsSellers.Select(x=>new { SellerGuid = x.Guid, SellerName = x.SellerName })));
                    }
                    if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                    {
                        return Redirect("/vbs/ProposalContent?Pid=" + pro.Id);
                    }
                    //退回原因
                    if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Returned))
                    {
                        string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Returned);
                        vbsLogs = sp.ProposalLogGetList(pro.Id, ProposalLogType.Initial).OrderByDescending(x => x.CreateTime)
                            .Where(y => y.ChangeLog.Contains(title)).FirstOrDefault();
                    }
                }
                base.HouseProposalContent(pid);



                ViewBag.VbsLogs = vbsLogs;
                ViewBag.IsVbsPro = true;
            }
            catch(Exception ex)
            {
                logger.Error(ex);
                return Redirect("/vbs/proposallist");
            }
            return View("~/Views/Proposal/HouseProposalContent.cshtml");
        }

        [VbsAuthorize]
        [HttpPost]
        public ActionResult HouseProposalContentSave(SellerProposalHouseModel model)
        {
            bool flag = false;
            string error = string.Empty;
            List<ProposalMultiDealModel> multiDeals = new List<ProposalMultiDealModel>();
            try
            {
                Seller seller = sp.SellerGet(model.SellerGuid);
                string message = CheckHouseProposalSave(model, seller);
                if (!string.IsNullOrEmpty(message))
                {
                    return Json(new
                    {
                        isSuccess = flag,
                        multiDeals = multiDeals,
                        message = message
                    });
                }

                List<string> disallowUri = PponFacade.GetOuterUri(model.Description);
                if(disallowUri.Count > 0)
                {
                    return Json(new
                    {
                        isSuccess = flag,
                        multiDeals = multiDeals,
                        message = string.Format("不允許以下網址：" + string.Join(",", disallowUri))
                    });
                }

                string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
                string saveMessage = HouseProposalContentVbsSave(model, seller, modifyId, out multiDeals);
                if (!string.IsNullOrEmpty(saveMessage))
                {
                    return Json(new
                    {
                        isSuccess = flag,
                        multiDeals = multiDeals,
                        message = saveMessage
                    });
                }
                flag = true;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                logger.Error("提案單號：" + model.ProId.ToString() + Environment.NewLine + ex);
                flag = false;
            }
            dynamic obj = new
            {
                isSuccess = flag,
                multiDeals = multiDeals,
                message = error
            };
            return Json(obj);
        }

        [VbsAuthorize]
        public override ActionResult ProposalPreview(int pid) {
            base.ProposalPreview(pid);
            return View("~/Views/Proposal/ProposalPreview.cshtml");
        }
        #endregion 提案單


        #region 商品資訊
        [VbsAuthorize]
        public ActionResult ProductOption()
        {
            if (Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
            {
                ViewBag.LoginBy17LifeSimulate = true;
            }

            List<Seller> vbsSellers = ProposalFacade.GetVbsSellerByCol(UserId, true);
            ViewBag.VbsSellers = new JsonSerializer().Serialize(vbsSellers.Select(x => new
            {
                SellerGuid = x.Guid,
                SellerId = x.SellerId,
                SellerName = x.SellerName
            }));
            ViewBag.IsVbsPro = true;

            Dictionary<int, string> stockFilter = GetStockFilter();
            ViewBag.StockFilter = stockFilter;
            ViewBag.SiteUrl = _config.SiteUrl;

            return View("~/Views/Proposal/ProductOption.cshtml");
        }
        [VbsAuthorize]
        [HttpPost]
        public ActionResult GetProductOption(int pageStart, int pageLength, int? productNo, string productBrandName, string productName, 
            string gtin, string mpn, string productCode, string items, int? stockStatus, bool? hideDisabled, Guid? bid, 
            int? uniqueId, int? warehouseType)
        {
            List<Guid> sellerGuids = ProposalFacade.GetVbsSeller(UserId, true).Select(x => x.Key).ToList();
            var vpiList = PponFacade.GetProductOptionListForVbs(sellerGuids, productNo, productBrandName, productName, gtin, productCode, items, stockStatus,
                hideDisabled, bid, uniqueId, null, warehouseType);
            var result = PponFacade.GetBaseProductOption(vpiList, pageStart, pageLength);
            return Json(result);
        }

        [VbsAuthorize]
        public override ActionResult ProductOptionContent(Guid sid, Guid? pid, Guid? did)
        {
            List<Seller> vbsSellers = ProposalFacade.GetVbsSellerByCol(UserId, true);
            if (sid == null || !vbsSellers.Select(x => x.Guid).ToList().Contains(sid))
            {
                return View("~/Views/Proposal/ProductOption.cshtml");
            }
            base.ProductOptionContent(sid, pid, did);

            ViewBag.IsVbsPro = true;
            return View("~/Views/Proposal/ProductOptionContent.cshtml");
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProductOptionContent(FormCollection forms)
        {
            ProductInfoModel product = new JsonSerializer().Deserialize<ProductInfoModel>(forms["postData"]);
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            dynamic obj = SaveProducts(product, modifyId);
            return Json(obj);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ProductOptionContentByPro(FormCollection forms)
        {
            ProductInfoModel product = new JsonSerializer().Deserialize<ProductInfoModel>(forms["ProductInfoData"]);
            dynamic obj = SaveProducts(product, UserName);
            return Json(obj);
        }

        /// <summary>
        /// 刪除商品
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult DeleteProduct(Guid pid)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            dynamic flag = DeleteBaseProduct(pid, modifyId);
            return Json(flag);
        }

        /// <summary>
        /// 停用商品
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult DisabledProduct(Guid did, int status)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            dynamic flag = DisabledBaseProduct(did, status, modifyId);
            return Json(flag);
        }
        [VbsAuthorize]
        public override ActionResult ExportProductFailList(Guid gid)
        {
            return base.ExportProductFailList(gid);
        }
        #endregion 商品資訊

        #endregion action

        #region web method
        /// <summary>
        /// 商家編輯提案
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult EditSellerProposal(int pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            bool flag = false;

            if (pro != null && pro.IsLoaded)
            {
                pro.SellerProposalFlag = 0;
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Saved));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                pro.ModifyId = UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SellerEditor);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Initial);
                flag = true;
            }

            return Json(flag, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 商家重新編輯提案
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ReEditSellerProposal(int pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            bool flag = false;

            if (pro != null && pro.IsLoaded)
            {
                pro.SellerProposalFlag = 0;
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Saved));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                //全部變回初始，流程要重跑
                pro.ApplyFlag = (int)ProposalApplyFlag.Apply;
                pro.ApproveFlag = (int)ProposalApproveFlag.Initial;
                pro.BusinessFlag = (int)ProposalBusinessFlag.Initial;//影響主管確認
                pro.ListingFlag = (int)ProposalListingFlag.Initial;  //影響製檔
                pro.FlowCompleteTime = null;//要重跑流程
                pro.ModifyId = UserName;
                pro.ModifyTime = DateTime.Now;
                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SellerEditor);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Initial);
                flag = true;
            }

            return Json(flag, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 刪除提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult DeleteProposal(int pid)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            return DeleteBaseProposal(pid, modifyId);
        }
        /// <summary>
        /// 複製提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult CloneProposal(int pid, bool isWms)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            return CloneBaseProposal(pid, ProposalCopyType.Seller, true, modifyId, isWms);
        }

        /// <summary>
        /// 檢查商品是否已被提案單使用
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult CheckDeletedItemUsed(string item_guid)
        {
            string message = string.Empty;
            if (CheckProductUsedWmsPurchaseOrder(item_guid))
            {
                return Json(new
                {
                    Message = "已建立進倉單，無法停用",
                    IsSuccess = false
                });
            }
            bool flag= CheckItemUsedByProposal(item_guid, out message);
            if (flag)
            {
                return Json(new {
                    IsSuccess = false,
                    Message = string.Format("已被提案單「{0}」使用，不可刪除", message)
                });
            }
            else
            {
                return Json(new
                {
                    IsSuccess = true,
                    Message = string.Empty
                });
            }
        }

        /// <summary>
        /// 商家提報送審
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult SendSellerProposal(int pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            dynamic obj = new
            {
                IsSuccess = false,
                Message = ""
            };

            string error = string.Empty;
            bool isVaild = CheckSendData(pro, out error);
            if (isVaild == false)
            {
                return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = error
                    }, JsonRequestBehavior.AllowGet);
            }

            if (pro != null && pro.IsLoaded)
            {
                string message = "";
                //message = ProposalContentCheckSave(pro);

                if (!string.IsNullOrEmpty(message))
                {
                    message += "請確認您已先儲存資料後再送出。\r\n";
                    return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = message
                    }, JsonRequestBehavior.AllowGet);
                }

                pro.PassSeller = 1;
                pro.SendSeller = 1;
                pro.ApplyFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ApplyFlag, ProposalApplyFlag.Apply));//直接幫這關過掉，因為業務沒這關可以按了
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));//商家確認=線上簽核
                pro.Status = (int)ProposalStatus.Apply;
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Returned));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Send));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Check));
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PaperContractCheck)); //自動勾起附約已回
                pro.ModifyTime = DateTime.Now;
                pro.ModifyId = modifyId;
                sp.ProposalSet(pro);

                //紀錄Log
                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Send);
                ProposalFacade.ProposalLog(pro.Id, "[" + title + "] ", modifyId, ProposalLogType.Initial);


                //商家提案通知信
                ViewEmployee opSalesEmp = hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.OperationSalesId);
                string SaleName = "";
                string SaleMail = "";
                if (opSalesEmp.IsLoaded && opSalesEmp != null)
                {
                    SaleName = opSalesEmp.EmpName;
                    SaleMail = opSalesEmp.Email;

                    string Memo = @"• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔";

                    ProposalFacade.ProposalLog(pro.Id, "[商家提案通知信件]" + SaleMail, modifyId, ProposalLogType.Initial);

                    Seller s = sp.SellerGet(pro.SellerGuid);
                    string subject = string.Format("【商家提案通知】 單號 NO.{0} {1} {2}", pro.Id, !string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName + pro.DealName, s.SellerName);

                    ProposalFacade.SendEmail(new List<string> { SaleMail }, subject,
                        ProposalFacade.SendSellerProposalMailContent(new Proposal()
                        {
                            Id = pro.Id,
                            SellerGuid = pro.SellerGuid,
                            CreateId = pro.CreateId
                        }
                        , SaleName, _config.SiteUrl + "/sal/proposal/house/proposalcontent?pid=",
                        Memo));
                }


                return Json(
                    new
                    {
                        IsSuccess = true,
                        Message = ""
                    }, JsonRequestBehavior.AllowGet);
            }

            return Json(
                    new
                    {
                        IsSuccess = false,
                        Message = ""
                    }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 暫停銷售
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult DealPause(int pid, bool pause)
        {
            return DealPauseSet(pid, pause);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetProductBusinessHour(int pro_no)
        {
            return ProductBusinessHourGet(pro_no);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetProposalDealSource(int pid)
        {
            return GetBaseProposalDealSource(pid);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult UploadProposalDealSource(int pid)
        {
            return UploadBaseProposalDealSource(pid);
        }
        [VbsAuthorize]
        public ActionResult DownloadProposalDealSource(int pid, int id)
        {
            return DownloadBaseProposalDealSource(pid, id);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult DeleteProposalDealSource(int pid, int id)
        {
            return DeleteBaseProposalDealSource(pid, id);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetProposalLogs(int pageStart, int pageLength, int pid)
        {
            return GetBaseProposalLogs(pageStart, pageLength, pid);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetProposalMessage(int pageStart, int pageLength, int pid)
        {
            return GetBaseProposalMessage(pageStart, pageLength, pid);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult ReadProposalMessage(int pid)
        {
            return ReadBaseProposalMessage(pid, true);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult LeaveMessage(int pid, string message)
        {
            string dept = string.Empty;
            bool flag = LeaveMessageSet(pid, message, dept); ;
            return Json(flag);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetSellerProducts(Guid sid, Guid infoGuid, string productBrandName, string productName, string productCode, int warehouseType)
        {
            return GetBaseSellerProducts(sid, infoGuid, productBrandName, productName, productCode, warehouseType);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetProductItems(List<ProductItemSourceModel> items)
        {
            return GetBaseProductItem(items);
        }

        [HttpPost]
        [VbsAuthorize]
        public ActionResult UploadProductInfo(Guid sid)
        {
            string modifyId = !string.IsNullOrEmpty(VbsCurrent.LoginBy17LifeSimulateUserName) ? VbsCurrent.LoginBy17LifeSimulateUserName : VbsCurrent.AccountId;
            return ImportProductInfoExcel(sid, modifyId);
        }
        /// <summary>
        /// 取得宅配業務清單
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetEmployeeNameArray(string empName)
        {
            return GetBaseEmployeeNameArray(empName);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetEmployeeByName(string empName)
        {
            empName = empName.Replace("'", "").Replace("<", "").Replace(">", "").Replace("\\", "");
            int userId = 0;

            try
            {
                //宅配部門
                List<Department> dept = new List<Department>();
                dept.Add(HumanFacade.GetDepartmentByDeptId(EmployeeChildDept.S010.ToString()));


                if (!string.IsNullOrEmpty(empName) && empName.Length >= 2 && empName.Length < 6)
                {
                    userId = SellerFacade.GetEmployeeIdArray(empName);
                }
            }
            catch
            {

            }
            return Json(userId);
        }
        /// <summary>
        /// 取得宅配員工資料
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        [HttpPost]
        [VbsAuthorize]
        public ActionResult GetEmployeeName(string empName)
        {
            return GetBaseEmployeeName(empName);
        }
        [HttpPost]
        [VbsAuthorize]
        public ActionResult CheckSpecStock(int pid)
        {
            string message = string.Empty;
            bool flag = CheckMultiOptionSpecStock(pid, out message);
            return Json(flag);
        }
        #endregion web method
    }
}

﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models;
using log4net;
using LunchKingSite.Core.Models;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class PromoEventSystemController : ControllerExt
    {
        private readonly ILog logger = log4net.LogManager.GetLogger(typeof(PponSetupController));
        private IPponProvider _pp;
        private ISellerProvider _sellerProv;
        private ISysConfProvider _config;
        private IMemberProvider _memProv;

        public PromoEventSystemController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        #region AppWalletPromo

        public ActionResult AppWalletEventList()
        {
            
            return View();
        }

        [HttpPost]
        public ActionResult AppWalletEventList(DateTime sendDate)
        {


            return View();
        }




        #endregion





        #region Method

        //取得策展
        //[HttpPost]
        //[AjaxCall]
        //public ActionResult GetBrand(int id)
        //{
        //    Brand brand = _pp.GetBrand(id);
        //    brand.EventListImage = ImageFacade.GetMediaPath(brand.EventListImage, MediaType.DealPromoImage);
        //    brand.Url = PromotionFacade.GetCurationTwoLink(brand);
        //    return Json(brand);
        //}

        #endregion

    }
}

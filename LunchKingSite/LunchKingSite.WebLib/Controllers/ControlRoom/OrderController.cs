﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.ControlRoom.Order;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web;
using Ionic.Zip;
using log4net;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    public class OrderController : BaseController
    {
        #region property

        private static IPponProvider _pp;
        private static IOrderProvider _op;
        private static IMemberProvider _mp;
        private static ISysConfProvider _conf;
        private static ILog logger = LogManager.GetLogger("ShipProductInventory");

        static OrderController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _conf = ProviderFactory.Instance().GetConfig();
        }

        #endregion property

        #region Action

        public ActionResult ReturnFormList(ReturnFormListModel model, int? pageNumber, string sortCol, bool? sortDesc)
        {
            pageNumber = pageNumber ?? 1;
            List<ReturnFormInfo> infos = new List<ReturnFormInfo>();
            sortDesc = sortDesc.GetValueOrDefault(false);

            #region 輸入查詢條件 type check
            model = QueryRequestCheck(model);
            model.DeliveryType = model.DeliveryType == 0 ? null : model.DeliveryType;
            #endregion 輸入查詢條件 type check

            if (model.QueryRequest || !string.IsNullOrEmpty(sortCol))
            {
                if (string.IsNullOrEmpty(sortCol))
                {
                    sortCol = "ApplicationTime";
                    sortDesc = true;
                }

                ViewOrderReturnFormListCollection returnFormList = GetReturnFormListFromQueryRequest(model);

                List<int> returnFormIds = returnFormList.Select(x => x.ReturnFormId).ToList();
                Dictionary<int, int> applicationProductCountInfos = _op.ReturnFormProductGetList(returnFormIds)
                                                                    .GroupBy(x => x.ReturnFormId)
                                                                    .Select(x => new { ReturnFormId = x.Key, Count = x.Count() })
                                                                    .ToDictionary(x => x.ReturnFormId, x => x.Count);
                Dictionary<int, int> refundedProductCountInfos = _op.ReturnFormRefundGetList(returnFormIds)
                                                                    .Where(x => x.IsRefunded.GetValueOrDefault(false) && !x.IsFreight)
                                                                    .GroupBy(x => x.ReturnFormId)
                                                                    .Select(x => new { ReturnFormId = x.Key, Count = x.Count() })
                                                                    .ToDictionary(x => x.ReturnFormId, x => x.Count);

                foreach (var returnForm in returnFormList)
                {
                    int comboPackCount = returnForm.ComboPackCount;
                    string processStatusDesc = string.Empty;
                    string orderStatusDesc = string.Empty;
                    string vendorProgressStatusDesc = string.Empty;

                    #region 廠商進度

                    switch ((VendorProgressStatus)returnForm.VendorProgressStatus)
                    {
                        case VendorProgressStatus.Unknown:
                            break;

                        case VendorProgressStatus.CompletedAndNoRetrieving:
                        case VendorProgressStatus.CompletedAndRetrievied:
                        case VendorProgressStatus.CompletedAndUnShip:
                        case VendorProgressStatus.CompletedByCustomerService:
                            vendorProgressStatusDesc = "退貨完成";
                            break;
                        case VendorProgressStatus.ConfirmedForUnArrival:
                            vendorProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorProgressStatus)(returnForm.VendorProgressStatus)) + "(已前往" + Helper.GetDescription((WmsRefundFrom)returnForm.RefundFrom) + "收件)";
                            break;
                        default:
                            vendorProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorProgressStatus)(returnForm.VendorProgressStatus ?? (int)VendorProgressStatus.Processing));
                            break;

                    }

                    #endregion 廠商進度

                    #region 處理進度

                    switch ((RefundType)returnForm.RefundType)
                    {
                        case RefundType.Atm:
                            processStatusDesc = "[退ATM]";
                            break;

                        case RefundType.Cash:
                            processStatusDesc = "[刷退]";
                            break;

                        case RefundType.Scash:
                            processStatusDesc = "[退17Life購物金]";
                            break;

                        case RefundType.Tcash:
                            processStatusDesc = string.Format("[退{0}]", Helper.GetEnumDescription((ThirdPartyPayment)returnForm.ThirdPartyPaymentSystem));
                            break;

                        case RefundType.ScashToCash:
                            processStatusDesc = "[購物金轉刷退]";
                            break;

                        case RefundType.ScashToAtm:
                            processStatusDesc = "[購物金轉退ATM]";
                            break;
                        case RefundType.ScashToTcash:
                            processStatusDesc = string.Format("[購物金轉退{0}]", Helper.GetEnumDescription((ThirdPartyPayment)returnForm.ThirdPartyPaymentSystem));
                            break;
                        default:
                            break;
                    }

                    switch ((ProgressStatus)returnForm.ProgressStatus)
                    {
                        case ProgressStatus.AtmQueueing:
                        case ProgressStatus.Processing:
                        case ProgressStatus.AtmQueueSucceeded:
                            processStatusDesc += "<br/>" + "退貨處理中";
                            break;
                        case ProgressStatus.Retrieving:
                            processStatusDesc += "<br/>" + "已前往收件";
                            break;
                        case ProgressStatus.RetrieveToCustomer:
                            processStatusDesc += "<br/>" + "前往消費者處收件";
                            break;
                        case ProgressStatus.RetrieveToPC:
                            processStatusDesc += "<br/>" + "前往PC倉庫收件";
                            break;
                        case ProgressStatus.ConfirmedForCS:
                        case ProgressStatus.ConfirmedForUnArrival:
                            processStatusDesc += "<br/>" + "客服待處理";
                            break;
                        case ProgressStatus.ConfirmedForVendor:
                            processStatusDesc += "<br/>" + "廠商待確認";
                            break;
                        case ProgressStatus.AtmFailed:
                            processStatusDesc += "<br/>" + "退貨處理中-ATM匯款失敗";
                            break;

                        case ProgressStatus.Completed:
                        case ProgressStatus.CompletedWithCreditCardQueued:
                            processStatusDesc += "<br/>" + "退貨完成";
                            break;

                        case ProgressStatus.Canceled:
                            processStatusDesc += "<br/>" + "取消退貨";
                            break;

                        case ProgressStatus.Unreturnable:
                            processStatusDesc += "<br/>" + "退貨失敗";
                            break;

                        default:
                            break;
                    }

                    #endregion 處理進度

                    ReturnFormInfo info = new ReturnFormInfo
                    {
                        DealId = returnForm.ProductId,
                        DealGuid = returnForm.ProductGuid,
                        DealName = returnForm.DealName,
                        DealCloseTime = returnForm.DealEndTime,
                        OrderId = returnForm.OrderId,
                        OrderGuid = returnForm.OrderGuid,
                        ApplicationTime = returnForm.ReturnApplicationTime,
                        ApplicationProductCount = applicationProductCountInfos.ContainsKey(returnForm.ReturnFormId)
                                                    ? applicationProductCountInfos[returnForm.ReturnFormId] > 0
                                                        ? applicationProductCountInfos[returnForm.ReturnFormId] / comboPackCount
                                                        : 0
                                                    : 0, //申請退貨數以組數(成套販售)顯示
                        ProcessTime = returnForm.ModifyTime,
                        ProcessedProductCount = refundedProductCountInfos.ContainsKey(returnForm.ReturnFormId)
                                                    ? refundedProductCountInfos[returnForm.ReturnFormId]
                                                    : 0, //退貨完成數以組數(成套販售)顯示 return_form_refund以cash_trust_log紀錄 故不需另外處理
                        Reason = returnForm.ReturnReason,
                        VendorProcessStatusDesc = vendorProgressStatusDesc,
                        ProcessStatusDesc = processStatusDesc
                    };

                    infos.Add(info);
                }

                model.ReturnFormInfos = GetPagerReturnFormInfo(infos, 20, pageNumber.Value - 1, sortCol, sortDesc.Value);
            }

            ViewBag.IsEnableVbsNewShipFile = _conf.IsEnableVbsNewShipFile;
            ViewBag.ShowExportAccountingReportBtn = CommonFacade.IsInSystemFunctionPrivilege(this.UserName, SystemFunctionType.ExportAccountingReport);
            return View("ReturnFormList", model);
        }

        public ActionResult ReturnFormListExport(ReturnFormListModel model, bool isAccountingReport = false)
        {
            bool sortDesc = true;
            string sortCol = "ApplicationTime";
            List<ReturnFormInfo> infos = new List<ReturnFormInfo>();

            #region 輸入查詢條件 type check

            model = QueryRequestCheck(model);

            #endregion 輸入查詢條件 type check

            //前台是否按下查詢按鈕進行過查詢
            if (model.QueryRequest)
            {
                ViewOrderReturnFormListCollection returnFormList = GetReturnFormListFromQueryRequest(model);

                //取得商品品項資訊
                List<int> productReturnFormIds = returnFormList.Where(x => x.DeliveryType == (int)DeliveryType.ToHouse).Select(x => x.ReturnFormId).ToList();
                List<int> couponReturnFormIds = returnFormList.Where(x => x.DeliveryType == (int)DeliveryType.ToShop).Select(x => x.ReturnFormId).ToList();
                Dictionary<int, string> orderItemLists = GetReturnItemSpec(productReturnFormIds, DeliveryType.ToHouse);
                Dictionary<int, string> couponItemLists = GetReturnItemSpec(couponReturnFormIds, DeliveryType.ToShop);
                foreach (int couponReturnFormId in couponItemLists.Keys)
                {
                    orderItemLists.Add(couponReturnFormId, couponItemLists[couponReturnFormId]);
                }

                List<Guid> orderGuids = returnFormList.Select(x => x.OrderGuid).Distinct().ToList();
                Dictionary<Guid, int> orderProductCountInfos = _op.OrderProductGetList(orderGuids)
                                                                    .GroupBy(x => x.OrderGuid)
                                                                    .ToDictionary(x => x.Key, x => x.Count(p => p.IsCurrent));

                List<int> returnFormIds = returnFormList.Select(x => x.ReturnFormId).ToList();
                var returnFormRefundList = _op.ReturnFormRefundGetList(returnFormIds);                

                foreach (var returnForm in returnFormList)
                {
                    string processStatusDesc = string.Empty;
                    int comboPackCount = returnForm.ComboPackCount;
                    string orderStatusDesc = string.Empty;
                    string vendorProgressStatusDesc = string.Empty;

                    #region 處理進度

                    switch ((RefundType)returnForm.RefundType)
                    {
                        case RefundType.Atm:
                            processStatusDesc = "[退ATM]";
                            break;

                        case RefundType.Cash:
                            processStatusDesc = "[刷退]";
                            break;

                        case RefundType.Scash:
                            processStatusDesc = "[退17Life購物金]";
                            break;

                        case RefundType.Tcash:
                            processStatusDesc = string.Format("[退{0}]", Helper.GetEnumDescription((ThirdPartyPayment)returnForm.ThirdPartyPaymentSystem));
                            break;

                        case RefundType.ScashToCash:
                            processStatusDesc = "[購物金轉刷退]";
                            break;

                        case RefundType.ScashToAtm:
                            processStatusDesc = "[購物金轉退ATM]";
                            break;
                        case RefundType.ScashToTcash:
                            processStatusDesc = string.Format("[購物金轉退{0}]", Helper.GetEnumDescription((ThirdPartyPayment)returnForm.ThirdPartyPaymentSystem));
                            break;
                        default:
                            break;
                    }

                    switch ((ProgressStatus)returnForm.ProgressStatus)
                    {
                        case ProgressStatus.AtmQueueing:
                        case ProgressStatus.Processing:
                        case ProgressStatus.AtmQueueSucceeded:
                            processStatusDesc += "\n" + "退貨處理中";
                            break;
                        case ProgressStatus.Retrieving:
                            processStatusDesc += "\n" + "已前往收件";
                            break;
                        case ProgressStatus.RetrieveToCustomer:
                            processStatusDesc += "\n" + "前往消費者處收件";
                            break;
                        case ProgressStatus.RetrieveToPC:
                            processStatusDesc += "\n" + "前往PC倉庫收件";
                            break;
                        case ProgressStatus.ConfirmedForCS:
                        case ProgressStatus.ConfirmedForUnArrival:
                            processStatusDesc += "\n" + "客服待處理";
                            break;
                        case ProgressStatus.ConfirmedForVendor:
                            processStatusDesc += "\n" + "廠商待確認";
                            break;
                        case ProgressStatus.AtmFailed:
                            processStatusDesc += "\n" + "退貨處理中-ATM匯款失敗";
                            break;

                        case ProgressStatus.Completed:
                        case ProgressStatus.CompletedWithCreditCardQueued:
                            processStatusDesc += "\n" + "退貨完成";
                            break;

                        case ProgressStatus.Canceled:
                            processStatusDesc += "\n" + "取消退貨";
                            break;

                        case ProgressStatus.Unreturnable:
                            processStatusDesc += "\n" + "退貨失敗";
                            break;

                        default:
                            break;
                    }

                    #endregion 處理進度

                    #region 廠商處理進度

                    switch ((VendorProgressStatus)returnForm.VendorProgressStatus)
                    {
                        case VendorProgressStatus.Unknown:
                            break;

                        case VendorProgressStatus.CompletedAndNoRetrieving:
                        case VendorProgressStatus.CompletedAndRetrievied:
                        case VendorProgressStatus.CompletedAndUnShip:
                        case VendorProgressStatus.CompletedByCustomerService:
                            vendorProgressStatusDesc = "退貨完成";
                            break;
                        case VendorProgressStatus.ConfirmedForUnArrival:
                            vendorProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorProgressStatus)(returnForm.VendorProgressStatus)) + "(已前往" + Helper.GetDescription((WmsRefundFrom)returnForm.RefundFrom) + "收件)";
                            break;
                        default:
                            vendorProgressStatusDesc = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, (VendorProgressStatus)(returnForm.VendorProgressStatus ?? (int)VendorProgressStatus.Processing));
                            break;

                    }



                    #endregion 廠商處理進度

                    #region 財會報表部分資料
                    var dealCost = new DealCostCollection();
                    var refundFreight = 0;
                    if (isAccountingReport)
                    {
                        if (returnFormRefundList.Any(x => x.ReturnFormId == returnForm.ReturnFormId))
                        {
                            var cashTrustLog = _mp.CashTrustLogGetList(returnFormRefundList.Where(x => x.ReturnFormId == returnForm.ReturnFormId).Select(x => x.TrustId).ToList());
                            refundFreight = cashTrustLog.Where(x => Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freight)).Sum(x => x.CreditCard + x.Pcash + x.Scash + x.Atm + x.Bcash);
                        }

                        //進貨價
                        dealCost = _pp.DealCostGetList(returnForm.ProductGuid);
                    }

                    var itemCost = dealCost.Count > 0 ? Convert.ToInt32((dealCost[dealCost.Count() - 1].Cost ?? 0m)) : 0;
                    #endregion

                    if (returnForm.DeliveryType == (int)DeliveryType.ToHouse)
                    {
                        if (PponOrderManager.CheckIsOverTrialPeriod(returnForm.OrderGuid))
                        {
                            orderStatusDesc = I18N.Phrase.GoodsExpired;
                        }
                        else
                        {
                            orderStatusDesc = I18N.Phrase.GoodsNotExpired;
                        }
                    }
                    else if (returnForm.DeliveryType == (int)DeliveryType.ToShop)
                    {
                        if (DateTime.Compare(returnForm.ReturnApplicationTime, returnForm.UseEndTime.Value) > 0)
                        {
                            orderStatusDesc = I18N.Phrase.CouponExpired;
                        }
                        else
                        {
                            orderStatusDesc = I18N.Phrase.CouponNotExpired;
                        }
                    }

                    ReturnFormInfo info = new ReturnFormInfo
                    {
                        DealId = returnForm.ProductId,
                        DealName = returnForm.DealName,
                        DealCloseTime = returnForm.DealEndTime,
                        OrderId = returnForm.OrderId,
                        OrderProductCount = orderProductCountInfos.ContainsKey(returnForm.OrderGuid)
                                                ? orderProductCountInfos[returnForm.OrderGuid] > 0
                                                    ? orderProductCountInfos[returnForm.OrderGuid] / comboPackCount
                                                    : 0
                                                : 0, //訂單商品數以組數(成套販售)顯示
                        OrderAmount = returnForm.Total,
                        ApplicationTime = returnForm.ReturnApplicationTime,
                        ApplicationItemDesc = orderItemLists.ContainsKey(returnForm.ReturnFormId)
                                                ? orderItemLists[returnForm.ReturnFormId]
                                                : string.Empty,
                        ShipTime = (returnForm.DeliveryType == (int)DeliveryType.ToHouse ?
                                        (returnForm.ShipTime == null ? "未出貨" : ((DateTime)returnForm.ShipTime).ToString("yyyy/MM/dd")) : string.Empty),
                        ProcessTime = returnForm.ModifyTime,
                        Reason = returnForm.ReturnReason,
                        ProcessStatusDesc = processStatusDesc,
                        VendorProcessStatusDesc = vendorProgressStatusDesc,
                        OrderStatusDesc = orderStatusDesc,
                        SellerName = returnForm.SellerName,
                        RecipientName = string.IsNullOrEmpty(returnForm.ReceiverName) ? returnForm.MemberName : returnForm.ReceiverName,
                        RecipientTel = returnForm.PhoneNumber,
                        RecipientAddress = string.IsNullOrEmpty(returnForm.ReceiverAddress) ? returnForm.DeliveryAddress : returnForm.ReceiverAddress,
                        ReturnContactName = returnForm.ReturnedPersonName,
                        ReturnContactTel = returnForm.ReturnedPersonTel,
                        ReturnContactEmail = returnForm.ReturnedPersonEmail,
                        SalesName = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId,returnForm.DeEmpName).EmpName + (returnForm.OpEmpName != null ?  "/" + 
                                    HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.UserId, returnForm.DeEmpName).EmpName : ""),
                        ItemCost = itemCost,
                        RefundAmount = itemCost + refundFreight,
                        RefundFreight = refundFreight
                    };

                    infos.Add(info);
                }

                model.ReturnFormInfos = GetPagerReturnFormInfo(infos, int.MaxValue, 0, sortCol, sortDesc);
                MemoryStream outputStream;
                string fileName;

                if (isAccountingReport)
                {
                    outputStream = RefundAccountingReportExport(model.ReturnFormInfos);
                    fileName = "RefundAccountingReport.xls";
                }
                else
                {
                    outputStream = RefundReportExport(model.ReturnFormInfos);
                    fileName = "RefundReport.xls";
                }

                //搭配View $.fileDownload使用 ↓
                Response.Headers["Set-Cookie"] = "fileDownload=true; Path=/";
                //搭配View $.fileDownload使用 ↑

                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = fileName };
            }

            return RedirectToAction("ReturnFormList", model);
        }

        [HttpPost]
        public ActionResult GetBankInfo(string editBank, string editBranch, string bankType)
        {
            if (bankType == "bank")
            {
                var searchbankList = _op.BankInfoGetMainList().Select(x => (x.BankNo + " " + x.BankName)).Where(x => x.Contains(HttpUtility.UrlDecode(editBank, Encoding.UTF8))).ToList().Take(10);
                if (searchbankList.Any())
                {
                    return Json(new { success = true, bankList = searchbankList });
                }
                else
                {
                    return Json(new { success = false });
                }
            }
            else if (bankType == "bankBranch")
            {
                var searchbankBranchList = _op.BankInfoGetBranchList(editBank).Select(x => (x.BranchNo + " " + x.BranchName)).Where(x => x.Contains(HttpUtility.UrlDecode(editBranch, Encoding.UTF8))).ToList().Take(10);
                if (searchbankBranchList.Any())
                {
                    return Json(new { success = true, bankList = searchbankBranchList });
                }
                else
                {
                    return Json(new { success = false });
                }
            }
            return Json(new { success = false });
        }

        #region 換貨管理

        public ActionResult OrderShipInfo(int id)
        {
            var model = new ExchangeShipInfoModel();
            var info = GetExchangeLogInfo(id);
            if (info.OrderShipId.HasValue)
            { 
                var os = _op.GetOrderShipById(info.OrderShipId.Value);
                model.OrderId = info.OrderId;
                model.ApplicationTime = info.ApplicationTime;
                model.ExchangeLogId = info.ExchangeLogId;
                model.Reason = info.Reason;
                model.VendorMemo = info.VendorMemo;
                model.VendorProgressStatus = info.VendorProgressStatus;
                model.OrderShipId = info.OrderShipId;
                model.ShipNo = os.ShipNo;
                model.ShipCompanyId = os.ShipCompanyId;
                model.ShipTime = os.ShipTime;
                model.ShipMemo = os.ShipMemo;
            }
            model.ShipCompanyInfos = _op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();
            model.Modifyable = info.Modifyable;

            return View("OrderShipInfo", model);
        }

        [HttpPost]
        public JsonResult OrderShipUpdate(ExchangeShipInfoModel model)
        {
            var now = DateTime.Now;
            var exchangeLog = _op.OrderReturnListGet(model.ExchangeLogId);

            if (!exchangeLog.IsLoaded)
            {
                return Json(new { IsSuccess = false, Message = "查無換貨單" });
            }
            var os = new OrderShip();

            if (exchangeLog.OrderShipId.HasValue)
            {
                os = _op.GetOrderShipById(exchangeLog.OrderShipId.Value);
                if (!os.IsLoaded)
                {
                    return Json(new { IsSuccess = false, Message = "查無出貨紀錄" });
                }
            }
            else
            {
                os.Type = (int)OrderShipType.Exchange;
                os.OrderGuid = exchangeLog.OrderGuid;
                os.OrderClassification = (int)OrderClassification.LkSite;
                os.CreateId = User.Identity.Name;
                os.CreateTime = now;
            }

            if (model.ShipNo != os.ShipNo ||
                model.ShipTime != os.ShipTime ||
                model.ShipCompanyId != os.ShipCompanyId ||
                model.ShipMemo != os.ShipMemo)
            { 
                os.ShipNo = model.ShipNo;
                os.ShipTime = model.ShipTime;
                os.ShipCompanyId = model.ShipCompanyId;
                os.ShipMemo = model.ShipMemo;
                os.ModifyId = User.Identity.Name;
                os.ModifyTime = now;
                
                if (!OrderShipUtility.ExchangeOrderShipSet(os, exchangeLog))
                {
                    return Json(new { IsSuccess = false, Message = "儲存出現錯誤狀況" });
                }
            }

            return Json(new { IsSuccess = true, Message = string.Empty });
        }

        [HttpPost]
        public JsonResult OrderShipDelete(int id)
        {
            var exchangeLog = _op.OrderReturnListGet(id);

            if (!exchangeLog.IsLoaded)
            {
                return Json(new { IsSuccess = false, Message = "查無換貨單" });
            }
            var os = new OrderShip();

            if (exchangeLog.OrderShipId.HasValue)
            {
                os = _op.GetOrderShipById(exchangeLog.OrderShipId.Value);
                if (!os.IsLoaded)
                {
                    return Json(new { IsSuccess = false, Message = "查無出貨紀錄" });
                }

                os.ShipCompanyId = null;
                os.ShipNo = null;
                os.ShipTime = null;
                os.ModifyId = User.Identity.Name;
                os.ModifyTime = DateTime.Now;
                os.ShipMemo = null;

                try
                {
                    if (OrderShipUtility.OrderShipUpdate(os))
                    {
                        exchangeLog.Message = string.Format("出貨資訊異動：出貨日期->{0}, 貨運單號->{1}, 物流公司->{2}, 出貨備註->{3}",
                                                string.Empty, string.Empty, string.Empty, string.Empty);
                        exchangeLog.MessageUpdate = false;
                        exchangeLog.ModifyId = os.ModifyId;
                        exchangeLog.ModifyTime = os.ModifyTime;
                        PaymentFacade.SaveOrderStatusLog(exchangeLog);
                    }
                }
                catch
                {
                    return Json(new { IsSuccess = false, Message = "刪除出現錯誤狀況" });
                }
            }

            return Json(new { IsSuccess = true, Message = string.Empty });
        }

        public ActionResult ExchangeMessage(int id)
        {
            var model = GetExchangeLogInfo(id);

            return View("ExchangeMessage", model);
        }

        [HttpPost]
        public JsonResult ExchangeMessageUpdate(ExchangeMessageModel model)
        {
            var exchangeLog = _op.OrderReturnListGet(model.ExchangeLogId);

            if (!exchangeLog.IsLoaded)
            {
                return Json(new { IsSuccess = false, Message = "查無換貨單" });
            }

            if (!string.IsNullOrEmpty(model.Message))
            {
                exchangeLog.Message = model.Message;
                exchangeLog.MessageUpdate = true;
                if (!exchangeLog.VendorProgressStatus.HasValue ||
                    exchangeLog.VendorProgressStatus == (int)ExchangeVendorProgressStatus.Unknown)
                { 
                    exchangeLog.VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing;
                }
                exchangeLog.ModifyId = User.Identity.Name;
                exchangeLog.ModifyTime = DateTime.Now;

                OrderFacade.SetOrderReturList(exchangeLog);
            }

            return Json(new { IsSuccess = true, Message = string.Empty });
        }

        #endregion 換貨管理:出貨資訊

        #region 小倉管理
        public ActionResult ConsignmentShipMgnt()
        {
            var vdlModel = new ConsignmentBatchInventoryImportModel();
            vdlModel.ConsignmentShipImportResult = TempData["importResult"] as ConsignmentShipImportResult;
            //尋找匯入失敗檔案
            string path = string.Format("{0}", "BatchInventoryImport");
            vdlModel.ConsignmentImportFailFileInfos = GetImportFailFileInfo(path);

            ViewBag.IsConsignments = _conf.IsConsignment;
            ViewBag.AccountID = User.Identity.Name;
            return View(vdlModel);
        }

        public ActionResult ConsignmentBatchInventoryExport()
        {
            #region 一次匯出出貨狀態為:處理中的所有未回填出貨單之訂單清冊
            var now = DateTime.Now;
            var infos = new List<ConsignmentShipDealInfo>();

            //取得小倉檔次
            List<Guid> dealprops = (_conf.IsConsignment == true ? _pp.ConsignmentDealPropertyGet(_conf.IsConsignment) : null);

            if (dealprops != null && dealprops.Count > 0)
            {
                foreach (Guid dealprop in dealprops)
                {
                    List<ViewVbsToHouseDeal> deals = _pp.ViewVbsToHouseDealGetList("productGuid", dealprop.ToString(), null, null)
                    .Where(x =>
                    {
                        if (!x.DealStartTime.HasValue || !x.DealEndTime.HasValue ||
                            !x.UseStartTime.HasValue || !x.UseEndTime.HasValue) //不撈取未設定販賣時間及兌換時間之異常檔次
                        {
                            return false;
                        }

                        if (x.BusinessHourStatus.HasValue)
                        {
                            if (Helper.IsFlagSet(x.BusinessHourStatus.Value, BusinessHourStatus.ComboDealSub)) //不撈取多檔次子檔
                            {
                                return false;
                            }
                        }

                        //開賣時間還沒開始，不顯示
                        if (DateTime.Compare(now, x.DealStartTime.Value) < 0)
                        {
                            return false;
                        }

                        if (x.DealEstablished.HasValue)
                        {
                            return int.Equals(1, x.DealEstablished); //過門檻
                        }

                        if (x.DealType.Equals((int)VbsDealType.PiinLife))
                        {
                            return true;
                        }

                        return DateTime.Compare(now, x.DealEndTime.Value) < 0; //目前時間未超過結檔時間   
                    })
                    .ToList();
                    if (deals.Count > 0)
                    {
                        foreach (var deal in deals)
                        {
                            var shipState = deal.FinalBalanceSheetDate.HasValue
                                            ? ShipState.Finished
                                            : deal.ShippedDate.HasValue
                                                ? ShipState.ReturnAndChanging
                                                : DateTime.Compare(deal.UseEndTime.Value.Date, now.Date) < 0
                                                    ? ShipState.ShippingOverdue
                                                    : DateTime.Compare(deal.UseStartTime.Value.Date, now.Date) > 0
                                                        ? ShipState.NotYetShip
                                                        : ShipState.Shipping;

                            var info = new ConsignmentShipDealInfo
                            {
                                //抓取24H / 72H到貨 標籤
                                DealName = string.Format("{0}{1}", GetLableDesc(deal.LabelIconList), deal.ProductName),
                                DealId = deal.ProductGuid,
                                DealType = (VbsDealType)deal.DealType,
                                DealUniqueId = deal.ProductId,
                                ShipPeriodStartTime = deal.UseStartTime.Value,
                                ShipPeriodEndTime = deal.UseEndTime.Value,
                                ShipState = shipState,
                                SellerId = deal.SellerId,
                                SellerName = deal.SellerName
                            };

                            infos.Add(info);
                        }
                    }
                }
            }
            //只處理未回填出貨回覆日之檔次
            infos = infos.Where(x => x.ShipState == ShipState.Shipping || x.ShipState == ShipState.ShippingOverdue)
                .OrderBy(x => x.ShipState)
                .ThenBy(x => x.ShipPeriodStartTime)
                .ThenBy(x => x.ShipPeriodEndTime).ToList();

            var inventoryType = InventoryType.BatchUnShip;
            var dealOrderInfos = GetPagerVbsOrderShipInfos(GetConsignmentOrderShipInventoryList(infos.Select(x => x.DealId), false), OrderShipState.UnShip, int.MaxValue, 0, "OrderCreateTime", false, null, null)
                                    .OrderBy(x => x.OrderCreateTime).ThenBy(x => x.DealInfo.DealUniqueId).ThenBy(x => x.OrderId);
            var orderCountLimit = 1000000;

            //超過excel行數限制則拆成兩個excel檔匯出並壓縮成壓縮檔
            if (dealOrderInfos.Count() > orderCountLimit)
            {
                var workStream = new MemoryStream();

                using (var zip = new ZipFile(System.Text.Encoding.Default))
                {
                    int idx = 0;
                    int index = 1;
                    while (idx <= dealOrderInfos.Count() - 1)
                    {
                        var outputStream = ConsignmentShipProductInventoryExport(dealOrderInfos.Skip(idx).Take(orderCountLimit), inventoryType);

                        var bytes = new byte[outputStream.Length];
                        outputStream.Read(bytes, 0, bytes.Length);
                        zip.AddEntry(DownloadFileNameFormat(string.Format("{0:yyyyMMddHHmmss}_小倉撈單待出貨清冊_Part{1}.xls", now, index)), bytes);
                        idx += orderCountLimit;
                        index++;
                    }

                    zip.Save(workStream);
                }
                workStream.Position = 0;

                var fileResult = new FileStreamResult(workStream, System.Net.Mime.MediaTypeNames.Application.Zip)
                {
                    FileDownloadName = DownloadFileNameEncode(string.Format("{0:yyyyMMddHHmmss}_小倉撈單待出貨清冊.zip", now))
                };

                return fileResult;
            }
            else
            {
                MemoryStream outputStream = ConsignmentShipProductInventoryExport(dealOrderInfos, inventoryType);
                return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = DownloadFileNameEncode(string.Format("{0:yyyyMMddHHmmss}_小倉撈單待出貨清冊.xls", now)) };
            }

            #endregion 一次匯出出貨狀態為:處理中的所有未回填出貨單之訂單清冊
        }

        [HttpPost]
        public ActionResult ConsignmentBatchImportProcess(ConsignmentShipImportModel vdlModel)
        {
            var importResult = ProcessImportOrderShip(vdlModel);

            TempData["importResult"] = importResult;
            return RedirectToAction("ConsignmentShipMgnt", "Order");
        }
        #endregion

        #region 舊訂單補更新cash_trust_log uninvoice_amount處理

        [HttpGet]
        public JsonResult OldOrderUninvoiceAmountUpdate(int? processCount)
        {
            var message = new StringBuilder();
            var orderGuids = _op.GetOldOrderWithNoUninvoiceAmount(processCount);
            var cashTrusLogs = _mp.GetCashTrustLogCollectionByOid(orderGuids)
                                    .GroupBy(x => x.OrderGuid)
                                    .ToDictionary(x => x.Key, x => x.ToList());
            var successCount = 0;

            if (processCount.HasValue)
            {
                if (processCount.Value > 2000)
                {
                    return Json(new { Message = "每次處理資料筆數請勿輸入超過2000筆之數值，避免影響正式網站效能" }, JsonRequestBehavior.AllowGet);
                }

                message.Append(string.Format("預計處理資料筆數： {0} 筆<br/><br/>", processCount));
            }

            foreach (var orderGuid in orderGuids)
            {
                var ctls = cashTrusLogs.ContainsKey(orderGuid) ? cashTrusLogs[orderGuid] : null;
                if (ctls == null)
                {
                    continue;
                }
                //抓取發票金額以還原當時未開立發票金額
                int uninvoiced = 0;
                var invoices = _op.EinvoiceMainGetListByOrderGuid(orderGuid).Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701).ToList();
                if (invoices.Any())
                {
                    uninvoiced = (int)invoices.Sum(x => x.OrderAmount);
                }
                else
                {
                    var creditCardAmount = ctls.Sum(x => x.CreditCard);
                    var cashAmount = creditCardAmount + ctls.Sum(x => x.Atm) + ctls.Sum(x => x.Tcash);
                    //舊訂單皆為購買後開立發票 故無需考慮會有未開立發票購物金問題 
                    //發票紀錄於order中資料 則未開立發票金額以刷卡金額為主
                    //若einvoice_main和order皆無發票資訊 視為無需開立發票 未開立發票金額 以購買訂單時使用現金為主
                    var oldInvoice = _op.OrderGet(orderGuid);
                    uninvoiced = !string.IsNullOrEmpty(oldInvoice.InvoiceNo)
                                ? creditCardAmount
                                : cashAmount;
                }

                if (uninvoiced == ctls.Sum(x => x.UninvoicedAmount))
                {
                    continue;
                }

                try
                { 
                    using (var scope = TransactionScopeBuilder.CreateReadCommitted())
                    {
                        var ctlc = new CashTrustLogCollection();

                        foreach (var ctl in ctls)
                        {
                            #region 紀錄刷卡未開立發票金額

                            if (uninvoiced > 0)
                            {
                                int cashAmount = ctl.CreditCard + ctl.Atm + ctl.Tcash;
                                ctl.UninvoicedAmount = (uninvoiced - cashAmount > 0) ? cashAmount : uninvoiced;
                                uninvoiced -= ctl.UninvoicedAmount;

                                ctlc.Add(ctl);
                                CommonFacade.AddAudit(ctl.TrustId, AuditType.CashTrustLog, string.Format("舊訂單補現金付款uninvoiced_amount處理：更新uninvoiced_amount由 0 調整為 {0}", ctl.UninvoicedAmount), User.Identity.Name, false);
                            }

                            #endregion 紀錄刷卡未開立發票金額
                        }

                        #region 紀錄購物金未開立發票金額

                        foreach (var ctl in ctlc)
                        {
                            if (uninvoiced > 0)
                            {
                                int oldUninvoicedAmount = ctl.UninvoicedAmount;
                                int newScash = (uninvoiced - ctl.Scash > 0) ? ctl.Scash : uninvoiced;
                                ctl.UninvoicedAmount += newScash;
                                uninvoiced -= newScash;

                                CommonFacade.AddAudit(ctl.TrustId, AuditType.CashTrustLog, string.Format("舊訂單補購物金付款uninvoiced_amount處理：更新uninvoiced_amount由 {0} 調整為 {1}", oldUninvoicedAmount, ctl.UninvoicedAmount), User.Identity.Name, false);
                            }
                        }

                        #endregion 紀錄購物金未開立發票金額

                        _mp.CashTrustLogCollectionSet(ctlc);
                        successCount++;

                        scope.Complete();
                    }
                }
                catch (Exception ex)
                {
                    message.Append(string.Format("舊訂單補uninvoiced_amount處理失敗，order_guid:{0}{1}{2}<br/><br/>", 
                            orderGuid, Environment.NewLine, ex.StackTrace.Length > 2500 ? ex.StackTrace.Substring(0, 2500) : ex.StackTrace));
                }
            }

            message.Append(string.Format("完成更新資料筆數： {0} 筆", successCount));

            return Json(new { Message = message.ToString() }, JsonRequestBehavior.AllowGet);
        }

        #endregion 舊訂單補更新cash_trust_log uninvoice_amount處理

        #endregion Action

        #region Method

        private PagerList<ReturnFormInfo> GetPagerReturnFormInfo(List<ReturnFormInfo> infos, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in infos select t);

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case ReturnFormInfo.Columns.DealId:
                        query = query.OrderBy(t => t.DealId);
                        break;

                    case ReturnFormInfo.Columns.OrderId:
                        query = query.OrderBy(t => t.OrderId);
                        break;

                    case ReturnFormInfo.Columns.DealCloseTime:
                        query = query.OrderBy(t => t.DealCloseTime);
                        break;

                    case ReturnFormInfo.Columns.ApplicationTime:
                        query = query.OrderBy(t => t.ApplicationTime);
                        break;

                    case ReturnFormInfo.Columns.ProcessTime:
                        query = query.OrderBy(t => t.ProcessTime);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case ReturnFormInfo.Columns.DealId:
                        query = query.OrderByDescending(t => t.DealId);
                        break;

                    case ReturnFormInfo.Columns.OrderId:
                        query = query.OrderByDescending(t => t.OrderId);
                        break;

                    case ReturnFormInfo.Columns.DealCloseTime:
                        query = query.OrderByDescending(t => t.DealCloseTime);
                        break;

                    case ReturnFormInfo.Columns.ApplicationTime:
                        query = query.OrderByDescending(t => t.ApplicationTime);
                        break;

                    case ReturnFormInfo.Columns.ProcessTime:
                        query = query.OrderByDescending(t => t.ProcessTime);
                        break;

                    default:
                        break;
                }
            }

            var paging = new PagerList<ReturnFormInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.ToList().Count);

            return paging;
        }

        private Dictionary<int, string> GetReturnItemSpec(IEnumerable<int> returnFormIds, DeliveryType deliveryType)
        {
            Dictionary<int, string> orderItemDescList = new Dictionary<int, string>();
            if (deliveryType == DeliveryType.ToHouse)
            {
                orderItemDescList = _op.GetReturnOrderProductOptionDesc(returnFormIds);
            }
            else if (deliveryType == DeliveryType.ToShop)
            {
                ReturnFormRefundCollection returnFormRefundList = _op.ReturnFormRefundGetList(returnFormIds);
                Dictionary<int, List<Guid>> returnFormTrustIds = returnFormRefundList
                                                                    .GroupBy(x => x.ReturnFormId)
                                                                    .ToDictionary(x => x.Key, x => x.Select(t => t.TrustId).ToList());
                List<Guid> trustIds = returnFormRefundList
                                        .Select(x => x.TrustId)
                                        .ToList();
                Dictionary<Guid, string> couponNumbers = _mp.CashTrustLogGetList(trustIds)
                                                                .ToDictionary(x => x.TrustId, x => x.CouponSequenceNumber);


                foreach (int returnFormId in returnFormTrustIds.Keys)
                {
                    StringBuilder resultSb = new StringBuilder();

                    foreach (var trustId in returnFormTrustIds[returnFormId])
                    {
                        if (couponNumbers.ContainsKey(trustId))
                        {
                            if (resultSb.Length > 0)
                            {
                                resultSb.Append("\n");
                            }
                            resultSb.Append(couponNumbers[trustId]);
                        }
                    }
                    orderItemDescList.Add(returnFormId, resultSb.ToString());
                }
            }

            return orderItemDescList;
        }

        private MemoryStream RefundReportExport(List<ReturnFormInfo> infos)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            NPOI.SS.UserModel.DataFormat format_date = workbook.CreateDataFormat();
            cellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;
            cellStyle.DataFormat = format_date.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellYStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellCStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font font1 = workbook.CreateFont();
            font1.Color = NPOI.HSSF.Util.HSSFColor.BLUE.index;
            cellBStyle.SetFont(font1);
            cellBStyle.WrapText = true;

            #endregion  格式設定

            Sheet sheet = workbook.CreateSheet("退貨報表");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("處理進度");
            cols.CreateCell(1).SetCellValue("檔號");
            cols.CreateCell(2).SetCellValue("訂單編號");
            cols.CreateCell(3).SetCellValue("申請時間");
            cols.CreateCell(4).SetCellValue("結檔時間");
            cols.CreateCell(5).SetCellValue("廠商名稱");
            cols.CreateCell(6).SetCellValue("商品名稱");
            cols.CreateCell(7).SetCellValue("收件人");
            cols.CreateCell(8).SetCellValue("收件人電話");
            cols.CreateCell(9).SetCellValue("配送地址");
            cols.CreateCell(10).SetCellValue("數量");
            cols.CreateCell(11).SetCellValue("金額");
            cols.CreateCell(12).SetCellValue("取消理由");
            cols.CreateCell(13).SetCellValue("處理備註");
            cols.CreateCell(14).SetCellValue("退換貨聯絡電子信箱");
            cols.CreateCell(15).SetCellValue("廠商處理進度");
            cols.CreateCell(16).SetCellValue("業務");

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState = string.Empty;
            string returnProcessState = string.Empty;

            foreach (var item in infos)
            {
                Row row = sheet.CreateRow(rowIdx);

                row.CreateCell(0).SetCellValue(item.ProcessStatusDesc);

                //處理狀態自動換列 並依照列數調整列高
                row.GetCell(0).CellStyle = cellWStyle;
                int statusDescWarpCount = Regex.Matches(item.ProcessStatusDesc, "\n").Count;
                row.HeightInPoints = (float)((statusDescWarpCount + 1) * sheet.DefaultRowHeight / 20);

                row.CreateCell(1).SetCellValue(item.DealId.ToString());
                row.CreateCell(2).SetCellValue(item.OrderId);
                row.CreateCell(3).SetCellValue(item.ApplicationTime.ToString("yyyy/MM/dd HH:mm"));
                row.CreateCell(4).SetCellValue(item.DealCloseTime.ToString("yyyy/MM/dd"));
                row.CreateCell(5).SetCellValue(item.SellerName);
                row.CreateCell(6).SetCellValue(item.DealName);
                row.CreateCell(7).SetCellValue(item.RecipientName);
                row.CreateCell(8).SetCellValue(item.RecipientTel);
                row.CreateCell(9).SetCellValue(item.RecipientAddress);
                row.CreateCell(10).SetCellValue(item.OrderProductCount);
                row.CreateCell(11).SetCellValue(Convert.ToInt32(item.OrderAmount).ToString());
                row.CreateCell(12).SetCellValue(item.Reason);
                row.CreateCell(13).SetCellValue(string.Empty);
                row.CreateCell(14).SetCellValue(item.ReturnContactEmail);
                row.CreateCell(15).SetCellValue(item.VendorProcessStatusDesc);
                row.CreateCell(16).SetCellValue(item.SalesName);

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 計算第一行) 英數字長度*1 中文字長度*2
                    byte[] sarr = System.Text.Encoding.Default.GetBytes(column.ToString().Split("\n")[0]);
                    int length = sarr.Length;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private MemoryStream RefundAccountingReportExport(List<ReturnFormInfo> infos)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            NPOI.SS.UserModel.DataFormat format_date = workbook.CreateDataFormat();
            cellStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;
            cellStyle.DataFormat = format_date.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellYStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle.FillForegroundColor = NPOI.HSSF.Util.HSSFColor.LIGHT_YELLOW.index;
            cellCStyle.FillPattern = NPOI.SS.UserModel.FillPatternType.SOLID_FOREGROUND;
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font font1 = workbook.CreateFont();
            font1.Color = NPOI.HSSF.Util.HSSFColor.BLUE.index;
            cellBStyle.SetFont(font1);
            cellBStyle.WrapText = true;

            #endregion  格式設定

            Sheet sheet = workbook.CreateSheet("退貨報表");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("處理進度");
            cols.CreateCell(1).SetCellValue("檔號");
            cols.CreateCell(2).SetCellValue("訂單編號");
            cols.CreateCell(3).SetCellValue("申請時間");
            cols.CreateCell(4).SetCellValue("出貨日期");
            cols.CreateCell(5).SetCellValue("廠商的退貨處理時間");
            cols.CreateCell(6).SetCellValue("結檔時間");
            cols.CreateCell(7).SetCellValue("廠商名稱");
            cols.CreateCell(8).SetCellValue("商品名稱");
            cols.CreateCell(9).SetCellValue("收件人");
            cols.CreateCell(10).SetCellValue("收件人電話");
            cols.CreateCell(11).SetCellValue("配送地址");
            cols.CreateCell(12).SetCellValue("數量");
            cols.CreateCell(13).SetCellValue("金額");
            cols.CreateCell(14).SetCellValue("進貨單價");
            cols.CreateCell(15).SetCellValue("運費");
            cols.CreateCell(16).SetCellValue("貨款總額");
            cols.CreateCell(17).SetCellValue("取消理由");
            cols.CreateCell(18).SetCellValue("處理備註");
            cols.CreateCell(19).SetCellValue("退換貨聯絡電子信箱");
            cols.CreateCell(20).SetCellValue("廠商處理進度");
            cols.CreateCell(21).SetCellValue("業務");

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState = string.Empty;
            string returnProcessState = string.Empty;

            foreach (var item in infos)
            {
                Row row = sheet.CreateRow(rowIdx);

                row.CreateCell(0).SetCellValue(item.ProcessStatusDesc);

                //處理狀態自動換列 並依照列數調整列高
                row.GetCell(0).CellStyle = cellWStyle;
                int statusDescWarpCount = Regex.Matches(item.ProcessStatusDesc, "\n").Count;
                row.HeightInPoints = (float)((statusDescWarpCount + 1) * sheet.DefaultRowHeight / 20);

                row.CreateCell(1).SetCellValue(item.DealId.ToString());
                row.CreateCell(2).SetCellValue(item.OrderId);
                row.CreateCell(3).SetCellValue(item.ApplicationTime.ToString("yyyy/MM/dd HH:mm"));
                row.CreateCell(4).SetCellValue(item.ShipTime);
                row.CreateCell(5).SetCellValue(item.ProcessTime == null ? string.Empty : ((DateTime)item.ProcessTime).ToString("yyyy/MM/dd HH:mm"));
                row.CreateCell(6).SetCellValue(item.DealCloseTime.ToString("yyyy/MM/dd"));
                row.CreateCell(7).SetCellValue(item.SellerName);
                row.CreateCell(8).SetCellValue(item.DealName);
                row.CreateCell(9).SetCellValue(item.RecipientName);
                row.CreateCell(10).SetCellValue(item.RecipientTel);
                row.CreateCell(11).SetCellValue(item.RecipientAddress);
                row.CreateCell(12).SetCellValue(item.OrderProductCount);
                row.CreateCell(13).SetCellValue(Convert.ToInt32(item.OrderAmount).ToString());
                row.CreateCell(14).SetCellValue(item.ItemCost);
                row.CreateCell(15).SetCellValue(item.RefundFreight);
                row.CreateCell(16).SetCellValue(item.RefundAmount);
                row.CreateCell(17).SetCellValue(item.Reason);
                row.CreateCell(18).SetCellValue(string.Empty);
                row.CreateCell(19).SetCellValue(item.ReturnContactEmail);
                row.CreateCell(20).SetCellValue(item.VendorProcessStatusDesc);
                row.CreateCell(21).SetCellValue(item.SalesName);

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 計算第一行) 英數字長度*1 中文字長度*2
                    byte[] sarr = System.Text.Encoding.Default.GetBytes(column.ToString().Split("\n")[0]);
                    int length = sarr.Length;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private ReturnFormListModel QueryRequestCheck(ReturnFormListModel model)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["DeliveryType"]) || !string.IsNullOrEmpty(Request.QueryString["ProductDeliveryType"]))
            {
                DeliveryType deliveryType;
                if (Enum.TryParse<DeliveryType>(Request.QueryString["DeliveryType"], out deliveryType))
                {
                    model.DeliveryType = deliveryType;
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ProductDeliveryType"]))
            {
                ProductDeliveryType productDeliveryType;
                if (Enum.TryParse<ProductDeliveryType>(Request.QueryString["ProductDeliveryType"], out productDeliveryType))
                {
                    model.ProductDeliveryType = productDeliveryType;
                }
            }
            if (model.DeliveryType == 0 && model.ProductDeliveryType == 0)
            {
                throw new Exception("銷售類型輸入有誤");
            }

            if (!string.IsNullOrEmpty(Request.QueryString["ProgressState"]))
            {
                ProgressState progressState;
                if (Enum.TryParse<ProgressState>(Request.QueryString["ProgressState"], out progressState))
                {
                    model.ProgressState = progressState;
                }
                else
                {
                    throw new Exception("處理進度輸入有誤");
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["VendorProgressState"]))
            {
                VendorProgressState vendorProgressState;
                if (Enum.TryParse<VendorProgressState>(Request.QueryString["VendorProgressState"], out vendorProgressState))
                {
                    model.VendorProgressState = vendorProgressState;
                }
                else
                {
                    throw new Exception("廠商進度輸入有誤");
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ApplicationTimeS"]))
            {
                DateTime applicationTimeS;
                if (DateTime.TryParse(Request.QueryString["ApplicationTimeS"], out applicationTimeS))
                {
                    model.ApplicationTimeS = applicationTimeS;
                }
                else
                {
                    throw new Exception("申請時間起輸入有誤");
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ApplicationTimeE"]))
            {
                DateTime applicationTimeE;
                if (DateTime.TryParse(Request.QueryString["ApplicationTimeE"], out applicationTimeE))
                {
                    model.ApplicationTimeE = applicationTimeE;
                }
                else
                {
                    throw new Exception("申請時間迄輸入有誤");
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["SelQueryOption"]))
            {
                model.SelQueryOption = Request.QueryString["SelQueryOption"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["QueryKeyWord"]))
            {
                model.QueryKeyWord = Request.QueryString["QueryKeyWord"].Trim();
            }
            if (!string.IsNullOrEmpty(Request.QueryString["SignCompanyID"]))
            {
                model.SignCompanyID = Request.QueryString["SignCompanyID"];
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ProcessTimeS"]))
            {
                DateTime processTimeS;
                if (DateTime.TryParse(Request.QueryString["ProcessTimeS"], out processTimeS))
                {
                    model.ProcessTimeS = processTimeS;
                }
                else
                {
                    throw new Exception("廠商處理時間起輸入有誤");
                }
            }
            if (!string.IsNullOrEmpty(Request.QueryString["ProcessTimeE"]))
            {
                DateTime processTimeE;
                if (DateTime.TryParse(Request.QueryString["ProcessTimeE"], out processTimeE))
                {
                    model.ProcessTimeE = processTimeE;
                }
                else
                {
                    throw new Exception("廠商處理時間迄輸入有誤");
                }
            }

            return model;
        }

        private ViewOrderReturnFormListCollection GetReturnFormListFromQueryRequest(ReturnFormListModel model)
        {
            List<int> progressStatusLists = new List<int>();
            List<int> vendorProgressStatusLists = new List<int>();

            switch (model.ProgressState)
            {
                case ProgressState.ReturnProcessing:
                    progressStatusLists.Add((int)ProgressStatus.Processing);
                    progressStatusLists.Add((int)ProgressStatus.AtmQueueing);
                    progressStatusLists.Add((int)ProgressStatus.AtmQueueSucceeded);
                    progressStatusLists.Add((int)ProgressStatus.AtmFailed);
                    break;

                case ProgressState.ReturnProcessing_AtmFailed:
                    progressStatusLists.Add((int)ProgressStatus.AtmFailed);
                    break;

                case ProgressState.ReturnCompleted:
                    progressStatusLists.Add((int)ProgressStatus.Completed);
                    progressStatusLists.Add((int)ProgressStatus.CompletedWithCreditCardQueued);
                    break;

                case ProgressState.ReturnCancel:
                    progressStatusLists.Add((int)ProgressStatus.Canceled);
                    break;

                case ProgressState.ReturnFail:
                    progressStatusLists.Add((int)ProgressStatus.Unreturnable);
                    break;
                case ProgressState.ToBeConfirmed:
                    progressStatusLists.Add((int)ProgressStatus.ConfirmedForCS);
                    progressStatusLists.Add((int)ProgressStatus.ConfirmedForUnArrival);
                    break;

                case ProgressState.All:
                default:
                    break;
            }

            switch (model.VendorProgressState)
            {
                case VendorProgressState.Processing:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.Processing);
                    break;

                case VendorProgressState.Retrieving:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.Retrieving);
                    break;

                case VendorProgressState.UnreturnableProcessed:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.UnreturnableProcessed);
                    break;

                case VendorProgressState.UnreturnableUnProcess:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.Unreturnable);
                    break;
                case VendorProgressState.CompletedByCustomerService:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.CompletedByCustomerService);
                    break;
                case VendorProgressState.Automatic:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.Automatic);
                    break;
                case VendorProgressState.ConfirmedForVendor:
                    vendorProgressStatusLists.Add((int)VendorProgressStatus.ConfirmedForVendor);
                    break;

                case VendorProgressState.All:
                default:
                    break;
            }


            ViewOrderReturnFormListCollection returnFormList = _op.ViewOrderReturnFormListGetList(model.DeliveryType, model.ProductDeliveryType, progressStatusLists,
                vendorProgressStatusLists, model.SelQueryOption, model.QueryKeyWord, model.ApplicationTimeS, model.ApplicationTimeE, model.SignCompanyID,
                model.ProcessTimeS, model.ProcessTimeE, model.SelAllowanceOption);

            FilterDealName(returnFormList);

            return returnFormList;
        }

        private void FilterDealName(ViewOrderReturnFormListCollection returnFormList)
        {
            foreach (var o in returnFormList.Where(x => !string.IsNullOrEmpty(x.LabelIconList)))
            {
                string fastget = "";
                foreach (var s in o.LabelIconList.Split(','))
                {
                    if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() ||
                        s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                    {
                        fastget += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s)));
                    }
                }
                o.DealName = fastget + o.DealName;
            }
        }

        private ExchangeLogModel GetExchangeLogInfo(int id)
        {
            var exchangeLogInfo = new ExchangeLogModel();
            var exchangeLog = _op.OrderReturnListGet(id);

            if (exchangeLog.IsLoaded)
            {
                var order = _op.OrderGet(exchangeLog.OrderGuid);
                if (order.IsLoaded)
                { 
                    exchangeLogInfo.OrderId = order.OrderId;
                    exchangeLogInfo.ExchangeLogId = exchangeLog.Id;
                    exchangeLogInfo.ApplicationTime = exchangeLog.CreateTime;
                    exchangeLogInfo.Reason = exchangeLog.Reason;
                    exchangeLogInfo.VendorMemo = exchangeLog.VendorMemo;
                    exchangeLogInfo.VendorProgressStatus = exchangeLog.VendorProgressStatus;
                    exchangeLogInfo.OrderShipId = exchangeLog.OrderShipId;
                    exchangeLogInfo.Modifyable = exchangeLog.Status != (int)OrderReturnStatus.ExchangeSuccess &&
                                                 exchangeLog.Status != (int)OrderReturnStatus.ExchangeCancel &&
                                                 exchangeLog.Status != (int)OrderReturnStatus.ExchangeFailure;
                }
            }

            return exchangeLogInfo;
        }
        private string GetLableDesc(string labelIcons)
        {
            string labelDesc = string.Empty;
            if (!string.IsNullOrEmpty(labelIcons))
            {
                List<int> labelIconList = new List<int>(labelIcons.Split(',').Select(int.Parse));
                List<int> arriveList = new List<int> { (int)DealLabelSystemCode.ArriveIn24Hrs, (int)DealLabelSystemCode.ArriveIn72Hrs };

                foreach (int label in labelIconList.Where(i => arriveList.Contains(i)))
                {
                    labelDesc += string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(label));
                }
            }

            return labelDesc;
        }
        private List<ConsignmentImportFailFileInfo> GetImportFailFileInfo(string path)
        {
            var fileInfos = new List<ConsignmentImportFailFileInfo>();
            string filePath = Path.Combine(HttpContext.Server.MapPath("~" + _conf.ShipBatchImport + "/"), path);
            string downloadPath = WebUtility.GetSiteRoot() + _conf.ShipBatchImport + "/" + path;
            if (Directory.Exists(filePath))
            {
                DirectoryInfo dir = new DirectoryInfo(filePath);
                FileInfo[] fileList = dir.GetFiles("*.xls");
                if (fileList.Any())
                {
                    foreach (var file in fileList.OrderByDescending(x => x.LastWriteTime).Take(1))
                    {
                        var fileInfo = new ConsignmentImportFailFileInfo
                        {
                            FileName = file.Name,
                            FilePath = downloadPath + "/" + file.Name
                        };
                        fileInfos.Add(fileInfo);
                    }
                }
            }

            return fileInfos;
        }
        private IEnumerable<DealSaleInfo> GetConsignmentDealSaleInfo(IEnumerable<Guid> dealGuids)
        {
            var dealGuidList = new List<Guid>();
            foreach (var dealGuid in dealGuids)
            {
                //有啟用小倉功能，才取得是否小倉檔次
                if (_conf.IsConsignment)
                {
                    bool isConsignment = (bool)_pp.DealPropertyGet(dealGuid).Consignment;
                    //小倉檔次才add list
                    if (isConsignment)
                    {
                        dealGuidList.Add(dealGuid);
                        var subDealIds = GetComboSubDealGuids(dealGuid);
                        if (subDealIds != null)
                        {
                            dealGuidList.AddRange(subDealIds.Keys);
                        }
                    }
                }
                else
                {
                    dealGuidList.Add(dealGuid);
                    var subDealIds = GetComboSubDealGuids(dealGuid);
                    if (subDealIds != null)
                    {
                        dealGuidList.AddRange(subDealIds.Keys);
                    }
                }
            }

            return DealSaleInfoGet(dealGuidList);
        }
        private IEnumerable<DealSaleInfo> GetConsignmentDealSaleInfoByUniqueId(IEnumerable<int> uniqueIds)
        {
            var dealGuidList = new List<Guid>();
            bool isConsignment;
            Guid dealGuid;

            foreach (var uniqueId in uniqueIds)
            {
                //有啟用小倉功能，才取得是否小倉檔次
                if (_conf.IsConsignment)
                {
                    var dealProperty = _pp.DealPropertyGet(uniqueId);
                    if (dealProperty == null || !dealProperty.IsLoaded)
                    {
                        continue;
                    }

                    isConsignment = dealProperty.Consignment;
                    dealGuid = dealProperty.BusinessHourGuid;

                    //小倉檔次才add list
                    if (isConsignment)
                    {
                        dealGuidList.Add(dealGuid);
                        var subDealIds = GetComboSubDealGuids(dealGuid);
                        if (subDealIds != null)
                        {
                            dealGuidList.AddRange(subDealIds.Keys);
                        }
                    }
                }
            }

            return DealSaleInfoGet(dealGuidList);
        }

        private List<OrderListInfo> GetConsignmentOrderShipInventoryList(IEnumerable<Guid> productGuids, bool isHiddenPersonalInfo)
        {
            var dealsInfos = GetConsignmentDealSaleInfo(productGuids);
            return OrderShipInventoryListGet(dealsInfos, isHiddenPersonalInfo);
        }
        private static PagerList<OrderListInfo> GetPagerVbsOrderShipInfos(
    IEnumerable<OrderListInfo> infos, OrderShipState? orderShipState, int pageSize, int pageIndex, string sortCol, bool sortDesc, bool? hasOrderShip, ViewVendorProgressState? viewVendorReturnProgressState)
        {
            var query = (from t in infos select t);

            if (orderShipState == OrderShipState.Shipped)
            {
                query = query.Where(t => t.OrderShipState == OrderShipState.Shipped ||
                                         t.OrderShipState == OrderShipState.ShippedAndPartialCancel).ToList();
            }
            else if (orderShipState == OrderShipState.UnShip)
            {
                query = query.Where(t => t.OrderShipState == OrderShipState.UnShip ||
                                         t.OrderShipState == OrderShipState.UnShipAndPartialCancel).ToList();
            }
            else if (orderShipState == OrderShipState.Return)
            {
                query = query.Where(t => t.OrderShipState == OrderShipState.Return ||
                                         t.OrderShipState == OrderShipState.ReturnAfterDealClose ||
                                         t.OrderShipState == OrderShipState.ReturnBeforeDealClose).ToList();
            }
            else if (orderShipState.HasValue)
            {
                query = query.Where(t => t.OrderShipState == orderShipState.Value).ToList();
            }

            if (hasOrderShip.HasValue)
            {
                query = query.Where(t => t.HasOrderShip.Equals(hasOrderShip.Value)).ToList();
            }

            if (viewVendorReturnProgressState.HasValue)
            {
                query = query.Where(t => t.VendorReturnProgressState == viewVendorReturnProgressState.Value).ToList();
            }

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case OrderListInfo.Columns.OrderId:
                        query = query.OrderBy(t => t.OrderId);
                        break;

                    case OrderListInfo.Columns.ItemDescription:
                        query = query.OrderBy(t => t.ItemDescription);
                        break;

                    case OrderListInfo.Columns.RecipientName:
                        query = query.OrderBy(t => t.RecipientName);
                        break;

                    case OrderListInfo.Columns.RecipientTel:
                        query = query.OrderBy(t => t.RecipientTel);
                        break;

                    case OrderListInfo.Columns.RecipientAddress:
                        query = query.OrderBy(t => t.RecipientAddress);
                        break;

                    case OrderListInfo.Columns.OrderShipState:
                        query = query.OrderBy(t => t.OrderShipState);
                        break;

                    case OrderListInfo.Columns.OrderCreateTime:
                        query = query.OrderBy(t => t.OrderCreateTime);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case OrderListInfo.Columns.OrderId:
                        query = query.OrderByDescending(t => t.OrderId);
                        break;

                    case OrderListInfo.Columns.ItemDescription:
                        query = query.OrderByDescending(t => t.ItemDescription);
                        break;

                    case OrderListInfo.Columns.RecipientName:
                        query = query.OrderByDescending(t => t.RecipientName);
                        break;

                    case OrderListInfo.Columns.RecipientTel:
                        query = query.OrderByDescending(t => t.RecipientTel);
                        break;

                    case OrderListInfo.Columns.RecipientAddress:
                        query = query.OrderByDescending(t => t.RecipientAddress);
                        break;

                    case OrderListInfo.Columns.OrderShipState:
                        query = query.OrderByDescending(t => t.OrderShipState);
                        break;

                    default:
                        break;
                }
            }

            var paging = new PagerList<OrderListInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.ToList().Count);

            return paging;
        }
        private Dictionary<Guid, int> GetComboSubDealGuids(Guid id)
        {
            //撈取多檔次之子檔bid, unique_id
            var subDeals = _pp.ViewPponDealGetListByMainBid(id);
            if (subDeals.Any())
            {
                return subDeals.Where(x => x.UniqueId.HasValue)
                               .ToDictionary(x => x.BusinessHourGuid,
                                             x => x.UniqueId.Value);
            }

            return null;
        }
        private IEnumerable<DealSaleInfo> DealSaleInfoGet(IEnumerable<Guid> dealGuids)
        {
            var dealSaleInfos = new List<DealSaleInfo>();
            var tempDateTime = DateTime.MinValue;
            var now = DateTime.Today;

            //過濾未達門檻子檔
            var deals = _pp.ViewVbsToHouseDealGetListByProductGuid(dealGuids)
                            .Where(x =>
                            {
                                if (!x.DealStartTime.HasValue || !x.DealEndTime.HasValue ||
                                    !x.UseStartTime.HasValue || !x.UseEndTime.HasValue) //不撈取未設定販賣時間或兌換時間之異常檔次
                                {
                                    return false;
                                }
                                if (x.DealEstablished.HasValue)
                                {
                                    return int.Equals(1, x.DealEstablished); //過門檻
                                }

                                return true;
                            });
            foreach (var deal in deals)
            {
                var labelDesc = GetLableDesc(deal.LabelIconList);
                var dealSaleInfo = new DealSaleInfo
                {
                    DealId = deal.ProductGuid,
                    DealType = (VbsDealType)deal.DealType,
                    //抓取24H / 72H到貨 標籤
                    DealName = string.Format("{0}{1}", labelDesc, deal.ProductName),
                    DealUniqueId = deal.ProductId,
                    DealOrderStartTime = deal.DealStartTime ?? tempDateTime,
                    DealOrderEndTime = deal.DealEndTime ?? tempDateTime,
                    ShipPeriodStartTime = deal.UseStartTime ?? tempDateTime,
                    ShipPeriodEndTime = deal.UseEndTime ?? tempDateTime,
                    //成套販售倍數(若 >1 視為成套販售檔次)
                    ComboPackCount = deal.ComboPackCount ?? 1,
                    DealEndSalesCount = deal.Slug,
                    DealLabelDesc = labelDesc.Replace("[", "").Replace("]", ""),
                    SellerId = deal.SellerId,
                    SellerName = deal.SellerName
                };

                ShipState shipState;
                if (deal.ShippedDate.HasValue)
                {
                    shipState = deal.FinalBalanceSheetDate.HasValue
                                    ? ShipState.Finished
                                    : ShipState.ReturnAndChanging;
                }
                else
                {
                    shipState = DateTime.Compare(deal.UseEndTime.Value, now) < 0
                                    ? ShipState.ShippingOverdue
                                    : DateTime.Compare(deal.UseStartTime.Value, now) > 0
                                        ? ShipState.NotYetShip
                                        : ShipState.Shipping;
                }
                dealSaleInfo.ShipState = shipState;

                dealSaleInfos.Add(dealSaleInfo);
            }

            return dealSaleInfos;
        }
        private List<OrderListInfo> OrderShipInventoryListGet(IEnumerable<DealSaleInfo> dealsInfos, bool isHiddenPersonalInfo)
        {
            var infos = new List<OrderListInfo>();
            ILookup<Guid, ViewOrderReturnFormList> returnLists = null;
            Dictionary<Guid, List<OrderUserMemoList>> orderUserMemoLists = null;

            //撈取該檔次訂單之品項數量 
            var dealGuids = dealsInfos.Select(x => x.DealId);
            var dealIds = dealsInfos.Select(x => x.DealUniqueId);
            var orderProductOptionLists = _op.ViewOrderProductOptionListGetIsCurrentList(dealIds);
            var orderItemLists = _op.GetOrderProductOptionList(orderProductOptionLists)
                                        .GroupBy(x => x.OrderGuid)
                                        .Select(x => new
                                        {
                                            x.Key,
                                            OptionDescroption = string.Join("\n", x.Select(o => o.OptionDescription)),
                                            ItemNo = string.Join("\n", x.Select(o => o.ItemNo)),
                                            Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                            ItemCount = x.Sum(o => o.Quantity)
                                        })
                                        .ToDictionary(x => x.Key, x => x);
            //撈取應出貨品項數量(排除退貨中及退貨完成品項數量)
            var unReturnOptionLists = orderProductOptionLists.Where(x => !x.IsReturned && !x.IsReturning)
                                                             .ToList();
            var unReturnItemLists = _op.GetOrderProductOptionList(unReturnOptionLists)
                                            .GroupBy(x => x.OrderGuid)
                                            .Select(x => new
                                            {
                                                x.Key,
                                                OptionDescroption = string.Join("\n", x.Select(o => o.OptionDescription)),
                                                Quantity = string.Join("\n", x.Select(o => o.Quantity)),
                                                ItemCount = x.Sum(o => o.Quantity)
                                            })
                                            .ToDictionary(x => x.Key, x => x);
            var orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids);
            //撈取該檔次訂單之退貨單
            returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                            x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                .ToLookup(x => x.OrderGuid);
            List<Guid> orderGuids = orderShipList.Select(x => x.OrderGuid).ToList();
            orderUserMemoLists = _op.OrderUserMemoListGetList(orderGuids)
                                    .GroupBy(x => x.OrderGuid)
                                    .ToDictionary(x => x.Key, x => x.ToList());

            if (orderShipList.Count > 0)
            {
                foreach (ViewOrderShipList order in orderShipList)
                {
                    OrderShipState orderShipState = OrderShipState.UnShip;
                    ViewVendorProgressState? vendorReturnProgressState = null;
                    List<OrderUserMemoInfo> orderUserMemoInfos = new List<OrderUserMemoInfo>();
                    bool isPartialCancel = false;

                    //抓不到檔次資訊 視為異常 不顯示該筆訂單
                    var dealInfo = dealsInfos.FirstOrDefault(x => x.DealId == order.ProductGuid);
                    if (dealInfo == null)
                    {
                        continue;
                    }

                    //抓不到訂單品項資訊 視為異常 不顯示該筆訂單
                    if (!orderItemLists.ContainsKey(order.OrderGuid))
                    {
                        continue;
                    }

                    //抓取訂單之退貨管理狀態
                    if (returnLists.Contains(order.OrderGuid))
                    {
                        if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                  x.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Process;
                        }
                        else if (returnLists[order.OrderGuid].Any(x => x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndNoRetrieving ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndRetrievied ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedAndUnShip ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.CompletedByCustomerService ||
                                                                       x.VendorProgressStatus == (int)VendorProgressStatus.Automatic))
                        {
                            vendorReturnProgressState = ViewVendorProgressState.Finished;
                        }
                    }

                    //抓取未退貨品項數量
                    if (unReturnItemLists.ContainsKey(order.OrderGuid))
                    {
                        //部分退貨 : 訂單數量-可出貨訂單數量 > 0
                        isPartialCancel = orderItemLists[order.OrderGuid].ItemCount - unReturnItemLists[order.OrderGuid].ItemCount > 0;
                    }
                    else
                    {
                        //沒有可出貨品項 且 廠商退貨處理完成 or 其他退款狀態(如退款失敗) 訂單 不顯示
                        if (vendorReturnProgressState != ViewVendorProgressState.Process)
                        {
                            continue;
                        }
                    }

                    //訂單出貨狀態
                    if (!order.ShipTime.HasValue ||
                        DateTime.Compare(order.ShipTime.Value, DateTime.Today) > 0)
                    {
                        orderShipState = isPartialCancel
                                            ? OrderShipState.UnShipAndPartialCancel
                                            : OrderShipState.UnShip;
                    }
                    else
                    {
                        orderShipState = isPartialCancel
                                            ? OrderShipState.ShippedAndPartialCancel
                                            : OrderShipState.Shipped;
                    }

                    //抓取訂單備註
                    if (orderUserMemoLists.ContainsKey(order.OrderGuid))
                    {
                        foreach (var userMemo in orderUserMemoLists[order.OrderGuid])
                        {
                            var orderUserMemoInfo = new OrderUserMemoInfo
                            {
                                CreateTime = userMemo.CreateTime,
                                UserMemo = userMemo.UserMemo
                            };
                            orderUserMemoInfos.Add(orderUserMemoInfo);
                        }
                    }

                    if (isHiddenPersonalInfo)
                    {
                        order.MemberName = "***";
                        order.PhoneNumber = "******";
                        order.DeliveryAddress = "*********";
                    }

                    var info = new OrderListInfo
                    {
                        DealInfo = dealInfo,
                        OrderGuid = order.OrderGuid,
                        OrderId = order.OrderId,
                        OrderCreateTime = order.OrderCreateTime,
                        PackCount = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].ItemCount / dealInfo.ComboPackCount : 0,
                        ItemDescription = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].OptionDescroption : string.Empty,
                        ItemCount = (orderItemLists.ContainsKey(order.OrderGuid)) ? orderItemLists[order.OrderGuid].Quantity : string.Empty,
                        RecipientName = order.MemberName,
                        RecipientTel = order.PhoneNumber,
                        RecipientAddress = order.DeliveryAddress,
                        RecipientRemark = order.UserMemo,
                        OrderShipId = order.OrderShipId,
                        ShipCompanyId = order.ShipCompanyId,
                        ShipCompanyName = order.ShipCompanyName,
                        ShipTime = order.ShipTime,
                        ShipNo = order.ShipNo,
                        OrderShipState = orderShipState,
                        HasOrderShip = order.ShipTime.HasValue,
                        OrderStatus = order.OrderStatus,
                        ShipMemo = order.ShipMemo,
                        OrderUserMemoInfos = orderUserMemoInfos,
                        VendorReturnProgressState = vendorReturnProgressState,
                        ShipItemDescription = unReturnItemLists.ContainsKey(order.OrderGuid) ? unReturnItemLists[order.OrderGuid].OptionDescroption : string.Empty,
                        ShipItemCount = unReturnItemLists.ContainsKey(order.OrderGuid) ? unReturnItemLists[order.OrderGuid].Quantity : string.Empty
                    };

                    infos.Add(info);
                }
            }

            return infos;
        }

        #region 小倉匯出
        public MemoryStream ConsignmentShipProductInventoryExport(IEnumerable<OrderListInfo> orderInfos, InventoryType inventoryType)
        {
            Workbook workbook = new HSSFWorkbook();

            #region 格式設定

            CellStyle cellStyle = workbook.CreateCellStyle();
            DataFormat formatDate = workbook.CreateDataFormat();
            cellStyle = SetCellBackgroudColor(cellStyle, HSSFColor.LIGHT_YELLOW.index);
            cellStyle.DataFormat = formatDate.GetFormat("yyyy/m/d");

            CellStyle cellYStyle = workbook.CreateCellStyle();
            cellYStyle = SetCellBackgroudColor(cellYStyle, HSSFColor.LIGHT_YELLOW.index);

            CellStyle cellCStyle = workbook.CreateCellStyle();
            cellCStyle = SetCellBackgroudColor(cellCStyle, HSSFColor.LIGHT_YELLOW.index);
            //設定格式為文字
            cellCStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("@");

            CellStyle cellMStyle = workbook.CreateCellStyle();
            cellMStyle = SetCellBackgroudColor(cellMStyle, HSSFColor.LIGHT_TURQUOISE.index);

            CellStyle cellWStyle = workbook.CreateCellStyle();
            cellWStyle.WrapText = true;

            CellStyle cellBStyle = workbook.CreateCellStyle();
            Font fontRB = workbook.CreateFont();
            fontRB.Color = HSSFColor.RED.index;
            fontRB.Boldweight = (short)FontBoldWeight.BOLD;
            cellBStyle.SetFont(fontRB);
            cellBStyle.WrapText = true;

            CellStyle cellUStyle = workbook.CreateCellStyle();
            cellUStyle = SetCellBackgroudColor(cellUStyle, HSSFColor.LIGHT_GREEN.index);
            cellUStyle.WrapText = true;

            CellStyle cellRStyle = workbook.CreateCellStyle();
            Font fontR = workbook.CreateFont();
            fontR.Color = HSSFColor.RED.index;
            cellRStyle.SetFont(fontR);
            cellRStyle.WrapText = true;


            #endregion  格式設定

            #region sheet1 清冊

            Sheet sheet = workbook.CreateSheet("清冊");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("商家編號");
            cols.CreateCell(1).SetCellValue("商家名稱");
            cols.CreateCell(2).SetCellValue("檔號");
            cols.CreateCell(3).SetCellValue("訂單編號");
            cols.CreateCell(4).SetCellValue("購買日期");
            cols.CreateCell(5).SetCellValue("收件人");
            cols.CreateCell(6).SetCellValue("收件人電話");
            cols.CreateCell(7).SetCellValue("配送地址");
            cols.CreateCell(8).SetCellValue("方案名稱");
            cols.CreateCell(9).SetCellValue("組數");
            cols.CreateCell(10).SetCellValue("品項規格");
            cols.CreateCell(11).SetCellValue("數量");
            cols.CreateCell(12).SetCellValue("應出貨品項規格");
            cols.CreateCell(13).SetCellValue("應出貨數");
            cols.CreateCell(14).SetCellValue("訂單備註");
            cols.CreateCell(15).SetCellValue("訂單狀態");
            cols.CreateCell(16).SetCellValue("退貨管理");
            cols.CreateCell(17).SetCellValue("出貨日期");
            cols.GetCell(17).CellStyle = cellYStyle;
            cols.CreateCell(18).SetCellValue("物流代號");
            cols.GetCell(18).CellStyle = cellYStyle;
            cols.CreateCell(19).SetCellValue("物流公司名稱");
            cols.CreateCell(20).SetCellValue("貨運單號");
            cols.GetCell(20).CellStyle = cellYStyle;
            cols.CreateCell(21).SetCellValue("出貨備註(非必填,最多30字)");
            cols.GetCell(21).CellStyle = cellMStyle;
            cols.CreateCell(22).SetCellValue("出貨條件");
            cols.GetCell(22).CellStyle = cellRStyle;

            #endregion 表頭 及 title

            #region 清冊內容

            int rowIdx = 1;
            int i = 1;
            string orderShipState;
            string returnProcessState;

            foreach (var item in orderInfos)
            {
                Row row = sheet.CreateRow(rowIdx);

                switch (item.OrderShipState)
                {
                    case OrderShipState.Shipped:
                        orderShipState = "已出貨";
                        break;

                    case OrderShipState.ShippedAndPartialCancel:
                        orderShipState = "已出貨-部分退貨";
                        break;

                    case OrderShipState.UnShip:
                        orderShipState = "未出貨";
                        break;

                    case OrderShipState.UnShipAndPartialCancel:
                        orderShipState = "未出貨-部分退貨";
                        break;

                    case OrderShipState.ReturnBeforeDealClose:
                        orderShipState = "結檔前退貨";
                        break;

                    case OrderShipState.ReturnAfterDealClose:
                        orderShipState = "結檔後退貨";
                        break;

                    default:
                        orderShipState = string.Empty;
                        break;
                }

                bool isReturnMark = false;
                switch (item.VendorReturnProgressState)
                {
                    case ViewVendorProgressState.Process:
                        returnProcessState = "待處理";
                        isReturnMark = true;
                        break;

                    default:
                        returnProcessState = string.Empty;
                        break;
                }

                bool isUserMemoMark = false;
                string orderUserMemo = string.Empty;
                if (item.OrderUserMemoInfos.Any())
                {
                    isUserMemoMark = true;
                    orderUserMemo = string.Join("\n", item.OrderUserMemoInfos
                                                            .OrderBy(x => x.CreateTime)
                                                            .Select(x => x.CreateTime.ToString("yyyy/MM/dd") + "  " + x.UserMemo));
                }

                row.CreateCell(0).SetCellValue(item.DealInfo.SellerId);
                row.CreateCell(1).SetCellValue(item.DealInfo.SellerName);
                row.CreateCell(2).SetCellValue(item.DealInfo.DealUniqueId.ToString(CultureInfo.InvariantCulture));
                row.CreateCell(3).SetCellValue(item.OrderId);
                row.CreateCell(4).SetCellValue(item.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"));
                row.CreateCell(5).SetCellValue(item.RecipientName);
                row.CreateCell(6).SetCellValue(item.RecipientTel);
                row.CreateCell(7).SetCellValue(item.RecipientAddress);
                row.CreateCell(8).SetCellValue(item.DealInfo.DealName);
                row.CreateCell(9).SetCellValue(item.PackCount);
                //品項規格
                row.CreateCell(10).SetCellValue(item.ItemDescription);
                //依照列數調整列高
                int itemDescWarpCount = Regex.Matches(item.ItemDescription, "\n").Count;
                row.HeightInPoints = Convert.ToSingle((itemDescWarpCount + 1) * sheet.DefaultRowHeight / 20);

                //數量
                row.CreateCell(11).SetCellValue(item.ItemCount);
                //應出貨品項規格
                row.CreateCell(12).SetCellValue(item.ShipItemDescription);
                //應出貨數量
                row.CreateCell(13).SetCellValue(item.ShipItemCount);

                row.CreateCell(14).SetCellValue(orderUserMemo);
                //訂單備註 依照列數調整列高
                row.GetCell(14).CellStyle = cellWStyle;
                int userMemoWarpCount = Regex.Matches(orderUserMemo, "\n").Count;
                if (userMemoWarpCount > itemDescWarpCount)
                {
                    row.HeightInPoints = Convert.ToSingle((userMemoWarpCount + 1) * sheet.DefaultRowHeight / 20);
                }

                row.CreateCell(15).SetCellValue(orderShipState);
                row.CreateCell(16).SetCellValue(returnProcessState);
                if (isReturnMark)
                {
                    row.GetCell(16).CellStyle = cellBStyle;
                }

                if (item.ShipTime.HasValue)
                {
                    row.CreateCell(17).SetCellValue(item.ShipTime.Value.ToShortDateString());
                }
                else
                {
                    row.CreateCell(17).SetCellValue(string.Empty);
                }
                row.GetCell(17).CellStyle = cellStyle;

                if (item.ShipCompanyId.HasValue)
                {
                    row.CreateCell(18).SetCellValue(item.ShipCompanyId.Value);
                }
                else
                {
                    row.CreateCell(18).SetCellValue(string.Empty);
                }
                row.GetCell(18).CellStyle = cellYStyle;

                row.CreateCell(19).SetCellValue(item.ShipCompanyName);
                row.CreateCell(20).SetCellValue(item.ShipNo);
                row.GetCell(20).CellStyle = cellCStyle;
                row.CreateCell(21).SetCellValue(item.ShipMemo);
                row.GetCell(21).CellStyle = cellMStyle;
                row.CreateCell(22).SetCellValue(item.DealInfo.DealLabelDesc);
                row.GetCell(22).CellStyle = cellRStyle;

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 抓取最長長度) 英數字長度*1 中文字長度*2
                    int length = column.ToString().Split("\n").Select(x => System.Text.Encoding.Default.GetBytes(x))
                                                              .Select(x => x.Length)
                                                              .Concat(new[] { 0 })
                                                              .Max() + 2;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }

                    if (column.ColumnIndex < 14)
                    {
                        column.CellStyle = isUserMemoMark
                                            ? cellUStyle
                                            : cellWStyle;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
                i++;
            }

            #endregion 清冊內容

            #endregion sheet1 清冊

            #region sheet2 物流公司對照表

            sheet = workbook.CreateSheet("物流公司對照表");
            cols = sheet.CreateRow(0);

            cols = sheet.CreateRow(1);
            cols.CreateCell(0).SetCellValue("物流代號");
            cols.CreateCell(1).SetCellValue("物流公司名稱");
            rowIdx = 2;
            i = 1;

            foreach (var item in _op.ShipCompanyGetList(null).Select(x => new { Id = x.Id, ShipCompanyName = x.ShipCompanyName }).OrderBy(x => x.Id).ToList())
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.Id);
                row.CreateCell(1).SetCellValue(item.ShipCompanyName);

                rowIdx++;
                i++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);

            #endregion sheet2 物流公司對照表

            //使用非同步寫入該檔次清冊匯出紀錄
            List<Guid> orderGuids = orderInfos.Select(x => x.OrderGuid).Distinct().ToList();
            SetOrderProductInventoryExportLog(orderGuids, User.Identity.Name, inventoryType);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }
        private static void SetOrderProductInventoryExportLog(IEnumerable<Guid> orderGuids, string accountId, InventoryType inventoryType)
        {
            string inventoryTypeDesc = string.Empty;
            switch (inventoryType)
            {
                case InventoryType.AllShip:
                    inventoryTypeDesc = "出貨清冊";
                    break;
                case InventoryType.UnShip:
                    inventoryTypeDesc = "待出貨清冊";
                    break;
                case InventoryType.ComboDeals:
                    inventoryTypeDesc = "各別出貨清冊";
                    break;
                case InventoryType.BatchUnShip:
                    inventoryTypeDesc = "一次撈單待出貨清冊";
                    break;
                default:
                    return;
            }

            Action<string, IEnumerable<Guid>> setExportLog = delegate(string userId, IEnumerable<Guid> odrGuids)
            {
                logger.InfoFormat(" 使用者:{0} 寫入匯出{1}Log Begin...", userId, inventoryTypeDesc);

                foreach (var orderGuid in odrGuids)
                {
                    CommonFacade.AddAudit(orderGuid, AuditType.Order, string.Format("匯出{0}", inventoryTypeDesc), userId, true);
                }

                logger.InfoFormat(" 使用者:{0} 寫入匯出{1}Log End。共匯出:{2}筆", userId, inventoryTypeDesc, odrGuids.Count());
            };

            setExportLog.BeginInvoke(accountId, orderGuids, null, null);
        }
        #endregion 小倉匯出
        #region 小倉匯入
        private ConsignmentShipImportResult ProcessImportOrderShip(ConsignmentShipImportModel vdlModel)
        {
            var importResult = new ConsignmentShipImportResult();
            var failInfo = new ConsignmentImportFailInfo();
            var failInfos = new List<ConsignmentImportFailInfo>();
            var tempDate = DateTime.MinValue;
            var remindInfo = new ConsignmentRemindingInfo();
            var remindInfos = new List<ConsignmentRemindingInfo>();
            importResult.ImportTime = DateTime.Now;
            importResult.InsertCount = 0;
            importResult.UpdateCount = 0;
            importResult.FailCount = 0;
            importResult.ImportIndentity = vdlModel.DealUniqueId;
            
            if (vdlModel.ProductInventoryFile != null)
            {
                if (vdlModel.ProductInventoryFile.ContentLength > 0)
                {
                    HSSFWorkbook workbook = new HSSFWorkbook(vdlModel.ProductInventoryFile.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);
                    Row row;
                    ILookup<string, ViewOrderShipList> orderShipList = null;
                    List<ShipCompany> shipCompanyInfo = _op.ShipCompanyGetList(null).ToList();
                    ILookup<Guid, CashTrustLog> cashTrustLogs = null;
                    List<dynamic> xlsInfo = new List<dynamic>();

                    var shipTime = DateTime.MinValue;
                    var orderId = string.Empty;
                    var shipNo = string.Empty;
                    var shipMemo = string.Empty;
                    int shipCompanyId;
                    int uniqueId;
                    int orderClassification = 0;
                    int shipMemoLimit = 30;

                    #region excel column index setup

                    const int dealIdColumnIndex = 2;            //檔號
                    const int orderIdColumnIndex = 3;           //訂單編號
                    const int shipTimeColumnIndex = 17;         //出貨日期
                    const int shipCompanyIdColumnIndex = 18;    //物流代號
                    const int shipNoColumnIndex = 20;           //貨運單號
                    const int shipMemoColumnIndex = 21;         //出貨備註

                    #endregion excel column index setup

                    for (int k = 1; k <= currentSheet.LastRowNum; k++)
                    {
                        #region content check
                        row = currentSheet.GetRow(k);
                        if (row == null)
                        {
                            continue;
                        }
                        if (row.GetCell(dealIdColumnIndex) == null || row.GetCell(dealIdColumnIndex).ToString() == string.Empty)
                        {
                            continue;
                        }
                        if (!int.TryParse(row.GetCell(dealIdColumnIndex).ToString(), out uniqueId))
                        {
                            importResult.FailCount++;
                            failInfo = new ConsignmentImportFailInfo
                            {
                                DealUniqueId = string.Empty,
                                OrderId = string.Empty,
                                Message = "檔案格式不符，請重新下載清冊匯出檔"
                            };
                            failInfos.Add(failInfo);
                            break;
                        }
                        orderId = (row.GetCell(orderIdColumnIndex) == null || row.GetCell(orderIdColumnIndex).ToString() == string.Empty) ? string.Empty : row.GetCell(orderIdColumnIndex).StringCellValue.Trim();
                        //訂單編號不為null才需處理
                        if (string.IsNullOrEmpty(orderId))
                        {
                            continue;
                        }

                        #region 出貨日期格式檢查

                        shipTime = DateTime.MinValue;
                        if (row.GetCell(shipTimeColumnIndex) != null)
                        {
                            switch (row.GetCell(shipTimeColumnIndex).CellType)
                            {//檢查出貨日期格式 於判斷是否為空值之前 避免有輸入資料大於日期數值轉換限制情況時 而導致系統錯誤發生
                                case CellType.STRING:
                                case CellType.FORMULA:
                                    if (!string.IsNullOrEmpty(row.GetCell(shipTimeColumnIndex).StringCellValue))
                                    {
                                        if (!DateTime.TryParse(row.GetCell(shipTimeColumnIndex).StringCellValue, out shipTime))
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ConsignmentImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    break;

                                case CellType.NUMERIC:
                                    if (DateUtil.IsCellDateFormatted(row.GetCell(shipTimeColumnIndex)) && row.GetCell(shipTimeColumnIndex).NumericCellValue < 2958465) //數值(距1904天數)轉換日期限制 9999/12/31
                                    {
                                        if (!DateTime.TryParse(row.GetCell(shipTimeColumnIndex).DateCellValue.ToString("yyyy/MM/dd"), out shipTime))
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ConsignmentImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    else
                                    {
                                        if (row.GetCell(shipTimeColumnIndex).NumericCellValue > 0)
                                        {
                                            importResult.FailCount++;
                                            failInfo = new ConsignmentImportFailInfo
                                            {
                                                DealUniqueId = uniqueId.ToString(),
                                                OrderId = orderId,
                                                Message = "格式錯誤:出貨日期"
                                            };
                                            failInfos.Add(failInfo);
                                            continue;
                                        }
                                    }
                                    break;

                                default:
                                    break;
                            }
                        }

                        #endregion 出貨日期格式檢查

                        #region 運單編號格式檢查

                        shipNo = string.Empty;
                        if (row.GetCell(shipNoColumnIndex) != null)
                        {
                            switch (row.GetCell(shipNoColumnIndex).CellType)
                            {//檢查運單編號格式 避免有輸入格式為公式 而導致系統錯誤發生
                                case CellType.NUMERIC:
                                    shipNo = row.GetCell(shipNoColumnIndex).NumericCellValue.ToString();
                                    break;

                                case CellType.FORMULA:
                                default:
                                    shipNo = row.GetCell(shipNoColumnIndex).ToString();
                                    break;
                            }
                        }

                        #endregion 運單編號格式檢查

                        #region 物流代號格式檢查

                        shipCompanyId = 0;
                        if (row.GetCell(shipCompanyIdColumnIndex) != null && !string.IsNullOrEmpty(row.GetCell(shipCompanyIdColumnIndex).ToString()))
                        {
                            if (!int.TryParse(row.GetCell(shipCompanyIdColumnIndex).ToString(), out shipCompanyId)
                             || !shipCompanyInfo.Any(x => x.Id.Equals(shipCompanyId)))
                            {
                                importResult.FailCount++;
                                failInfo = new ConsignmentImportFailInfo
                                {
                                    DealUniqueId = uniqueId.ToString(),
                                    OrderId = orderId,
                                    Message = "格式錯誤:物流代號"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                        }

                        #endregion 物流代號格式檢查

                        if (shipTime.Equals(DateTime.MinValue) && shipCompanyId.Equals(0) && string.IsNullOrEmpty(shipNo))
                        {//檢查出貨日期、物流代號、貨運單號是否皆填寫 若皆未填寫視為不處理 也不須顯示失敗訊息
                            continue;
                        }

                        if (shipTime.Equals(DateTime.MinValue) || shipCompanyId.Equals(0) || string.IsNullOrEmpty(shipNo))
                        {//檢查出貨日期、物流代號、貨運單號是否皆填寫 任一欄為空白都需顯示失敗訊息
                            importResult.FailCount++;
                            failInfo = new ConsignmentImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                Message = "出貨資訊不可有空白"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        #region 出貨備註

                        if (row.GetCell(shipMemoColumnIndex) == null || row.GetCell(shipMemoColumnIndex).ToString() == string.Empty)
                        {
                            shipMemo = string.Empty;
                        }
                        else
                        {
                            switch (row.GetCell(shipMemoColumnIndex).CellType)
                            {
                                case CellType.NUMERIC:
                                    shipMemo = row.GetCell(shipMemoColumnIndex).NumericCellValue.ToString().Trim();
                                    break;

                                default:
                                    shipMemo = row.GetCell(shipMemoColumnIndex).StringCellValue.Trim();
                                    break;
                            }
                        }

                        if (shipMemo.Length > shipMemoLimit)
                        {
                            importResult.FailCount++;
                            failInfo = new ConsignmentImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                Message = "出貨備註過長，只限輸入30個字。"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }

                        #endregion 出貨備註

                        //若物流公司選擇[其他] 需顯示提示訊息(非錯誤訊息) 仍可儲存
                        if (shipCompanyId.Equals(17))
                        {
                            if (remindInfos.Count == 0 || !remindInfos.Any(x => x.Type.Equals(LunchKingSite.WebLib.Models.VendorBillingSystem.RemindType.ShipCompanyIdOther)))
                            {
                                remindInfo = new ConsignmentRemindingInfo
                                {
                                    Type = LunchKingSite.WebLib.Models.ControlRoom.Order.RemindType.ShipCompanyIdOther,
                                    Message = "請聯絡服務專員協助您新增配合的物流公司：|" + _conf.BusinessServiceOnDutyTel
                                };
                                remindInfos.Add(remindInfo);
                            }
                        }

                        //出貨日期輸入檢查
                        if (shipTime > DateTime.Now.Date.AddDays(_conf.ShipTimeDayDiffLimit))
                        {
                            importResult.FailCount++;
                            failInfo = new ConsignmentImportFailInfo
                            {
                                DealUniqueId = uniqueId.ToString(),
                                OrderId = orderId,
                                Message = "您無法儲存超過" + _conf.ShipTimeDayDiffLimit + "天以後的出貨日"
                            };
                            failInfos.Add(failInfo);
                            continue;
                        }
                        #endregion

                        xlsInfo.Add(new
                        {
                            ShipTime = shipTime,
                            OrderId = orderId,
                            ShipNo = shipNo,
                            ShipMemo = shipMemo,
                            ShipCompanyId = shipCompanyId,
                            UniqueId = uniqueId
                        });
                    }

                    #region data process
                    if (xlsInfo.Count > 0)
                    {
                        List<int> uniqueIds = new List<int>();
                        foreach (var rowData in xlsInfo)
                        {
                            uniqueIds.Add(rowData.UniqueId);
                        }

                        var dealInfos = GetConsignmentDealSaleInfoByUniqueId((IEnumerable<int>)uniqueIds);
                        var dealGuids = dealInfos.Select(x => x.DealId);
                        var dealIds = dealInfos.Select(x => x.DealUniqueId);

                        foreach (var rowData in xlsInfo)
                        {
                            if (vdlModel.DealType == VbsDealType.Ppon)
                            {
                                //抓取未退貨(包含部分退貨)的訂單 完全退貨不需填寫出貨資訊
                                cashTrustLogs = _mp.CashTrustLogGetListByBid(dealGuids)
                                                    .Where(x => x.Status == (int)TrustStatus.Initial ||
                                                                x.Status == (int)TrustStatus.Trusted ||
                                                                x.Status == (int)TrustStatus.Verified)
                                                    .ToLookup(x => x.OrderGuid);
                                //抓取包含出貨資訊訂單資料
                                orderShipList = _op.ViewOrderShipListGetListByProductGuid(dealGuids)
                                                    .ToLookup(x => x.OrderId);
                            }
                            //抓取該檔次之狀態為處理中之退貨申請單
                            var returnLists = _op.ViewOrderReturnFormListGetListByDealGuid(dealGuids)
                                                .Where(x => x.ProgressStatus.Equals((int)ProgressStatus.Processing) ||
                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueing) ||
                                                            x.ProgressStatus.Equals((int)ProgressStatus.Completed) ||
                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmQueueSucceeded) ||
                                                            x.ProgressStatus.Equals((int)ProgressStatus.AtmFailed) ||
                                                            x.ProgressStatus.Equals((int)ProgressStatus.CompletedWithCreditCardQueued))
                                                .ToLookup(x => x.OrderGuid);

                            var exchangeLogs = PponOrderManager.GetHasExchangeLogOrderInfos(dealGuids);

                            if (!dealIds.Contains((int)rowData.UniqueId))
                            {//確認匯入檔案之檔次內容是否與目前檔次相符
                                importResult.FailCount++;
                                failInfo = new ConsignmentImportFailInfo
                                {
                                    DealUniqueId = rowData.UniqueId.ToString(),
                                    OrderId = string.Empty,
                                    Message = "匯入檔案與該檔次不符或匯入之檔次非[出貨中]狀態"
                                };
                                failInfos.Add(failInfo);
                                break;
                            }
                            //多檔次可能有重複訂單編號存在 故須加上bid篩選
                            orderId = rowData.OrderId;
                            var os = orderShipList.Contains(orderId) ? orderShipList[orderId].FirstOrDefault(x => dealGuids.Contains(x.ProductGuid)) : null;
                            //檢查該訂單編號是否存在
                            if (os == null)
                            {
                                importResult.FailCount++;
                                failInfo = new ConsignmentImportFailInfo
                                {
                                    DealUniqueId = rowData.UniqueId.ToString(),
                                    OrderId = rowData.OrderId,
                                    Message = "檔次無符合該筆訂單資料"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                            var dealInfo = dealInfos.First(x => x.DealId == os.ProductGuid);
                            var deliveryStartTime = dealInfo.ShipPeriodStartTime;
                            var ctl = cashTrustLogs.Contains(os.OrderGuid) ? cashTrustLogs[os.OrderGuid].FirstOrDefault() : null;
                            //檢查是否為退貨訂單 退貨訂單則不須異動出貨資料 也不須顯示失敗訊息
                            if (ctl == null)
                            {
                                continue;
                            }

                            if (returnLists.Contains(os.OrderGuid))
                            {
                                //訂單有退貨申請單且狀態為處理中 不能使用批次管理出貨
                                if (returnLists[os.OrderGuid].Any(x => x.VendorProgressStatus == null ||
                                                                        x.VendorProgressStatus == (int)VendorProgressStatus.Processing ||
                                                                        x.VendorProgressStatus == (int)VendorProgressStatus.Retrieving ||
                                                                        x.VendorProgressStatus == (int)VendorProgressStatus.Unreturnable ||
                                                                        x.VendorProgressStatus == (int)VendorProgressStatus.UnreturnableProcessed))
                                {
                                    importResult.FailCount++;
                                    failInfo = new ConsignmentImportFailInfo
                                    {
                                        DealUniqueId = rowData.UniqueId.ToString(),
                                        OrderId = rowData.OrderId,
                                        Message = "此訂單尚有未處理的退貨申請。請至出貨管理中進行單筆管理。"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                            }

                            shipTime = DateTime.Parse(rowData.ShipTime.ToString("yyyy/MM/dd"));
                            //出貨日期、物流公司及貨運單號都未變更 則不須進行異動處理
                            if (rowData.ShipCompanyId.Equals(os.ShipCompanyId) && shipTime.Equals(os.ShipTime) &&
                                rowData.ShipNo.Equals(os.ShipNo) && rowData.ShipMemo.Equals(os.ShipMemo))
                            {
                                continue;
                            }

                            if (exchangeLogs.Contains(os.OrderGuid))
                            {
                                importResult.FailCount++;
                                failInfo = new ConsignmentImportFailInfo
                                {
                                    DealUniqueId = rowData.UniqueId.ToString(),
                                    OrderId = rowData.OrderId,
                                    Message = "此訂單已有換貨紀錄，不允許異動原始出貨資訊。"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }

                            if (OrderShipUtility.CheckDealShipNoExist(dealInfo.DealId, (OrderClassification)os.OrderClassification, os.OrderGuid, rowData.ShipNo))
                            {
                                importResult.FailCount++;
                                failInfo = new ConsignmentImportFailInfo
                                {
                                    DealUniqueId = rowData.UniqueId.ToString(),
                                    OrderId = rowData.OrderId,
                                    Message = "貨運單號重複"
                                };
                                failInfos.Add(failInfo);
                                continue;
                            }
                            if (os.ShipTime.HasValue)
                            {//出貨資訊已存在
                                if (!os.ShipTime.Value.Equals(shipTime))
                                {//出貨日期異動檢查
                                    if (ctl.Status.Equals((int)TrustStatus.Verified))
                                    {
                                        importResult.FailCount++;
                                        failInfo = new ConsignmentImportFailInfo
                                        {
                                            DealUniqueId = rowData.UniqueId.ToString(),
                                            OrderId = rowData.OrderId,
                                            Message = "修改出貨日期已禁止，若需修改請洽客服人員"
                                        };
                                        failInfos.Add(failInfo);
                                        continue;
                                    }
                                    if (shipTime < DateTime.Now.Date)
                                    {
                                        importResult.FailCount++;
                                        failInfo = new ConsignmentImportFailInfo
                                        {
                                            DealUniqueId = rowData.UniqueId.ToString(),
                                            OrderId = rowData.OrderId,
                                            Message = "修改的出貨日期不能小於今日日期，若需修改請洽客服人員"
                                        };
                                        failInfos.Add(failInfo);
                                        continue;
                                    }
                                }
                            }
                            else
                            {//出貨資訊未存在或清空
                                if (shipTime < deliveryStartTime)
                                {
                                    importResult.FailCount++;
                                    failInfo = new ConsignmentImportFailInfo
                                    {
                                        DealUniqueId = rowData.UniqueId.ToString(),
                                        OrderId = rowData.OrderId,
                                        Message = "輸入的出貨日期不能小於配送開始日期，若有其他問題請洽客服人員"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                            }

                            //出貨資訊更新
                            var orderShip = new OrderShip();
                            orderShip.ModifyId = User.Identity.Name;
                            orderShip.ModifyTime = DateTime.Now;
                            orderShip.ShipCompanyId = rowData.ShipCompanyId;
                            orderShip.ShipNo = rowData.ShipNo;
                            orderShip.ShipTime = shipTime;
                            if (os.OrderShipId.HasValue)//由order_ship id判斷為新增或修改
                            {//order_ship 修改
                                orderShip.Id = os.OrderShipId.Value;
                                orderShip.ShipMemo = rowData.ShipMemo;
                                if (!OrderShipUtility.OrderShipUpdate(orderShip))
                                {
                                    importResult.FailCount++;
                                    failInfo = new ConsignmentImportFailInfo
                                    {
                                        DealUniqueId = rowData.UniqueId.ToString(),
                                        OrderId = rowData.OrderId,
                                        Message = "修改失敗，請聯絡客服人員"
                                    };
                                    failInfos.Add(failInfo);
                                    continue;
                                }
                                else
                                {
                                    importResult.UpdateCount++;
                                }
                            }
                            else
                            {//order_ship 新增
                                if (vdlModel.DealType == VbsDealType.Ppon)
                                {
                                    orderClassification = (int)OrderClassification.LkSite;
                                }
                                else if (vdlModel.DealType == VbsDealType.PiinLife)
                                {
                                    orderClassification = (int)OrderClassification.HiDeal;
                                }

                                //檢查出貨資訊是否已存在(避免重複按下匯入按鈕)
                                if (OrderShipUtility.CheckShipDataExist(os.OrderGuid, (OrderClassification)orderClassification))
                                {
                                    importResult.InsertCount++;
                                    continue;
                                }
                                else
                                {
                                    orderShip.OrderGuid = os.OrderGuid;
                                    orderShip.ShipMemo = rowData.ShipMemo;
                                    orderShip.OrderClassification = orderClassification;
                                    orderShip.CreateId = orderShip.ModifyId;
                                    orderShip.CreateTime = orderShip.ModifyTime;
                                    orderShip.Type = (int)OrderShipType.Normal;
                                    if (!OrderShipUtility.OrderShipSet(orderShip))
                                    {
                                        importResult.FailCount++;
                                        failInfo = new ConsignmentImportFailInfo
                                        {
                                            DealUniqueId = rowData.UniqueId.ToString(),
                                            OrderId = rowData.OrderId,
                                            Message = "新增失敗，請聯絡客服人員"
                                        };
                                        failInfos.Add(failInfo);
                                        continue;
                                    }
                                    else
                                    {
                                        importResult.InsertCount++;
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                }
            }
            else
            {
                importResult.FailCount++;
                failInfo = new ConsignmentImportFailInfo
                {
                    DealUniqueId = string.Empty,
                    OrderId = string.Empty,
                    Message = "請選擇檔案"
                };
                failInfos.Add(failInfo);
            }

            importResult.ConsignmentFailInfos = failInfos;
            if (failInfos.Count > 0)
            {
                importResult.FailFileName = WriteToImportFailFile(importResult);
            }

            importResult.ConsignmentRemindingInfos = remindInfos;

            return importResult;
        }
        private string WriteToImportFailFile(ConsignmentShipImportResult result)
        {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("匯入失敗結果");
            Row cols = sheet.CreateRow(0);
            cols.CreateCell(0).SetCellValue("匯入時間：" + result.ImportTime.ToString("yyyy/MM/dd HH:mm"));
            cols.CreateCell(1).SetCellValue("新增成功：" + result.InsertCount);
            cols.CreateCell(2).SetCellValue("更新成功：" + result.UpdateCount);
            cols.CreateCell(3).SetCellValue("匯入失敗：" + result.FailCount);
            cols = sheet.CreateRow(1);
            cols = sheet.CreateRow(2);
            cols.CreateCell(0).SetCellValue("檔號");
            cols.CreateCell(1).SetCellValue("訂單編號");
            cols.CreateCell(2).SetCellValue("匯入失敗原因");
            int rowIdx = 3;
            foreach (var item in result.ConsignmentFailInfos)
            {
                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(item.DealUniqueId);
                row.CreateCell(1).SetCellValue(item.OrderId);
                row.CreateCell(2).SetCellValue(item.Message);

                rowIdx++;
            }
            sheet.AutoSizeColumn(0);
            sheet.AutoSizeColumn(1);
            sheet.AutoSizeColumn(2);
            sheet.AutoSizeColumn(3);
            string filePath = Path.Combine(HttpContext.Server.MapPath("~" + _conf.ShipBatchImport + "/"), result.ImportIndentity);
            string fileName = string.Format("{0}\\{1}.xls", filePath, result.ImportTime.ToString("yyyyMMdd_HHmm"));
            // Create the path if it doesn't exist.
            if (!Directory.Exists(filePath))
            {
                Directory.CreateDirectory(filePath);
            }
            int i = 1;
            while (System.IO.File.Exists(fileName))
            {
                i++;
                if (i == 2)
                {
                    fileName = string.Format("{0}({1}).xls", fileName.Substring(0, fileName.Length - 4), i);
                }
                else
                {
                    fileName = string.Format("{0}({1}).xls", fileName.Substring(0, fileName.Length - 7), i);
                }
            }
            FileStream file = new FileStream(fileName, FileMode.CreateNew);
            workbook.Write(file);
            file.Close();

            return fileName;
        }

        #endregion 小倉匯入

        #region Excel處理
        private static string DownloadFileNameFormat(string fileName)
        {
            //檔名含全形雙引號 會導致excel無法正常開啟(空白) 故置換為全形單引號
            fileName = fileName.Replace("“", "‘").Replace("”", "’");
            List<char> formatStr = new List<char> { '/', '\\' };

            return formatStr.Aggregate(fileName, (current, fs) => current.Replace(fs, '_'));
        }
        private string DownloadFileNameEncode(string fileName)
        {
            fileName = DownloadFileNameFormat(fileName);
            string browser = HttpContext.Request.UserAgent.ToUpper();
            if (browser.Contains("IE"))
            {
                return HttpUtility.UrlEncode(fileName);
            }

            return fileName;
        }
        private static CellStyle SetCellBackgroudColor(CellStyle cellStyle, short colorIndex)
        {
            cellStyle.FillForegroundColor = colorIndex;
            cellStyle.FillPattern = FillPatternType.SOLID_FOREGROUND;

            return cellStyle;
        }
        #endregion Excel處理

        #endregion Method
    }
}

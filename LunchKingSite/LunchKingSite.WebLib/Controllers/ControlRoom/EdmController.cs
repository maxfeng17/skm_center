﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using log4net;
using LunchKingSite.Core.Models;
using System.Data;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class EdmController : ControllerExt
    {
        private readonly ILog logger = log4net.LogManager.GetLogger(typeof(PponSetupController));
        private IPponProvider pp;
        private ISellerProvider _sellerProv;
        private ISysConfProvider _config;
        private IMemberProvider _memProv;
        private ISerializer _serializer;

        public EdmController()
        {
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _serializer = ProviderFactory.Instance().GetDefaultSerializer();
        }

        #region DailyEDM

        public ActionResult EdmMainSetup(string sendDate)
        {
            ViewBag.ToSendDate = sendDate;
            return View();
        }

        [HttpPost]
        public ActionResult EdmMainSetup(DateTime sendDate)
        {
            var tmpDate = new DateTime(sendDate.Year, sendDate.Month, sendDate.Day, 0, 0, 0);
            EdmMainCollection EdmList = pp.GetEdmMainByDate(tmpDate, EdmMainType.Daily, true);
            var EdmMainDataList = new List<EdmMainData>();
            var edmMainData = new EdmMainData();

            #region 條件整理
            var TaipeiMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId);
            var TaipeAreaCount = (TaipeiMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(TaipeiMain.Id).Count : 0;
            var TaipeDetailCount = (TaipeiMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(TaipeiMain.Id).Count : 0;

            var TaoyuanMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId);
            var TaoyuanAreaCount = (TaoyuanMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(TaoyuanMain.Id).Count : 0;
            var TaoyuanDetailCount = (TaoyuanMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(TaoyuanMain.Id).Count : 0;

            var TaichungMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Taichung.CityId);
            var TaichungAreaCount = (TaichungMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(TaichungMain.Id).Count : 0;
            var TaichungDetailCount = (TaichungMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(TaichungMain.Id).Count : 0;

            var KaohsiungMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId);
            var KaohsiungAreaCount = (KaohsiungMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(KaohsiungMain.Id).Count : 0;
            var KaohsiungDetailCount = (KaohsiungMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(KaohsiungMain.Id).Count : 0;

            var allCountryMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
            var AllCountryAreaCount = (allCountryMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(allCountryMain.Id).Count : 0;
            var AllCountryDetailCount = (allCountryMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(allCountryMain.Id).Count : 0;

            var travelMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Travel.CityId);
            var TravelAreaCount = (travelMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(travelMain.Id).Count : 0;
            var TravelDetailCount = (travelMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(travelMain.Id).Count : 0;

            var piinlifeMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Piinlife.CityId);
            var PiinlifeAreaCount = (piinlifeMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(piinlifeMain.Id).Count : 0;
            var PiinlifeDetailCount = (piinlifeMain.Id != 0) ? pp.GetEdmDetailCollectionByPid(piinlifeMain.Id).Count : 0;
            #endregion

            #region 品生活

            edmMainData.EdmMainId = piinlifeMain.Id;
            edmMainData.CityName = "品生活";
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 12:30";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (piinlifeMain.Id != 0)
            {
                if (PiinlifeDetailCount == 0)
                {
                    if (PiinlifeAreaCount == 0)
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
                    }
                    else
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Open;
                    }
                }
                else
                {
                    edmMainData.CombinationEdmType = (int)EDMAreaType.Delete;
                }

                if (piinlifeMain.Message != null && piinlifeMain.Message.Contains("edit"))
                {
                    var tmpCount = piinlifeMain.Message.LastIndexOf("edit");
                    var tmpStr = piinlifeMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = piinlifeMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (piinlifeMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = piinlifeMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
                edmMainData.Enabled = piinlifeMain.Pause == false;
            }
            else
            {
                edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
            }

            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 宅配
            edmMainData = new EdmMainData();
            edmMainData.EdmMainId = allCountryMain.Id;
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
            edmMainData.CityName = "宅配";
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:08";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (allCountryMain.Id != 0)
            {
                if (AllCountryDetailCount == 0)
                {
                    if (AllCountryAreaCount == 0 || TravelAreaCount == 0 || PiinlifeAreaCount == 0)
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
                    }
                    else
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Open;
                    }
                }
                else
                {
                    edmMainData.CombinationEdmType = (int)EDMAreaType.Delete;
                }

                if (allCountryMain.Message != null && allCountryMain.Message.Contains("edit"))
                {
                    var tmpCount = allCountryMain.Message.LastIndexOf("edit");
                    var tmpStr = allCountryMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = allCountryMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (allCountryMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = allCountryMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
                edmMainData.Enabled = allCountryMain.Pause == false;
            }
            else
            {
                edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
            }

            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 旅遊玩美休閒
            edmMainData = new EdmMainData();
            edmMainData.EdmMainId = travelMain.Id;
            edmMainData.CityName = "旅遊玩美休閒";
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:07";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (travelMain.Id != 0)
            {
                if (TravelDetailCount == 0)
                {
                    if (TravelAreaCount == 0 || PiinlifeAreaCount == 0 || AllCountryAreaCount == 0)
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
                    }
                    else
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Open;
                    }
                }
                else
                {
                    edmMainData.CombinationEdmType = (int)EDMAreaType.Delete;
                }

                if (travelMain.Message != null && travelMain.Message.Contains("edit"))
                {
                    var tmpCount = travelMain.Message.LastIndexOf("edit");
                    var tmpStr = travelMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = travelMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (travelMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = travelMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
                edmMainData.Enabled = travelMain.Pause == false;
            }
            else
            {
                edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
            }

            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 在地

            #region 台北

            edmMainData = GetLocationEdmMainData(TaipeiMain.Id, TaipeAreaCount, AllCountryAreaCount, TravelAreaCount,
                PiinlifeAreaCount, TaipeDetailCount);
            edmMainData.CityName = "台北";
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:00";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (TaipeiMain.Id != 0)
            {
                if (TaipeiMain.Message != null && TaipeiMain.Message.Contains("edit"))
                {
                    var tmpCount = TaipeiMain.Message.LastIndexOf("edit");
                    var tmpStr = TaipeiMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = TaipeiMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (TaipeiMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = TaipeiMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
            }
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId;
            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 桃竹苗
            edmMainData = GetLocationEdmMainData(TaoyuanMain.Id, TaoyuanAreaCount, AllCountryAreaCount, TravelAreaCount,
                PiinlifeAreaCount, TaoyuanDetailCount);
            edmMainData.CityName = "桃竹苗";
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId;
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:01";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (TaoyuanMain.Id != 0)
            {
                if (TaoyuanMain.Message != null && TaoyuanMain.Message.Contains("edit"))
                {
                    var tmpCount = TaoyuanMain.Message.LastIndexOf("edit");
                    var tmpStr = TaoyuanMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = TaoyuanMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (TaoyuanMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = TaoyuanMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
            }
            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 中彰投
            edmMainData = GetLocationEdmMainData(TaichungMain.Id, TaichungAreaCount, AllCountryAreaCount, TravelAreaCount,
                PiinlifeAreaCount, TaichungDetailCount);
            edmMainData.CityName = "中彰投";
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.Taichung.CityId;
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:03";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (TaichungMain.Id != 0)
            {
                if (TaichungMain.Message != null && TaichungMain.Message.Contains("edit"))
                {
                    var tmpCount = TaichungMain.Message.LastIndexOf("edit");
                    var tmpStr = TaichungMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = TaichungMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (TaichungMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = TaichungMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
            }
            EdmMainDataList.Add(edmMainData);

            #endregion

            #region 雲嘉南高
            edmMainData = GetLocationEdmMainData(KaohsiungMain.Id, KaohsiungAreaCount, AllCountryAreaCount, TravelAreaCount,
                PiinlifeAreaCount, KaohsiungDetailCount);
            edmMainData.CityName = "雲嘉南高";
            edmMainData.CityId = PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId;
            edmMainData.UpdataTime = sendDate.ToString("MMdd") + " 07:05";
            edmMainData.EidtTime = "未編輯";
            edmMainData.SysUpdataTime = "未上傳";
            if (KaohsiungMain.Id != 0)
            {
                if (KaohsiungMain.Message != null && KaohsiungMain.Message.Contains("edit"))
                {
                    var tmpCount = KaohsiungMain.Message.LastIndexOf("edit");
                    var tmpStr = KaohsiungMain.Message.Substring(tmpCount, 15).Replace("edit ", "");
                    edmMainData.EidtTime = tmpStr;
                }
                else
                {
                    edmMainData.EidtTime = KaohsiungMain.CreateDate.Value.ToString("MMdd HH:mm");
                }
                if (KaohsiungMain.UploadDate != null)
                {
                    edmMainData.SysUpdataTime = KaohsiungMain.UploadDate.Value.ToString("MMdd HH:mm");
                }
            }
            EdmMainDataList.Add(edmMainData);

            #endregion

            #endregion

            ViewBag.SendDate = sendDate.ToString("yyyy/MM/dd");

            ViewBag.CityDataList = EdmMainDataList;

            return View();
        }

        public ActionResult PiinlifeEdmDetailSetup()
        {
            return RedirectToAction("EdmMainSetup");
        }

        [HttpPost]
        public ActionResult PiinlifeEdmDetailSetup(int mainId, DateTime sendDate)
        {
            var Delivery_Date = sendDate;
            var cid = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
            PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cid);
            ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, Delivery_Date);
            var mainData = EmailFacade.GetNewEdmMainById(mainId);
            string subjectStr = string.Empty;

            foreach (var ad in ad_list)
            {
                ad.Body = HttpUtility.HtmlDecode(ad.Body);
            }

            var data = GetEdmHotDealList(mainId, city.CityId, sendDate);

            if (mainData.Id == 0 && string.IsNullOrEmpty(mainData.Subject))
            {
                if (data.Keys.FirstOrDefault() != null)
                {
                    if (data.Keys.First().Count > 0)
                    {
                        var bid = data.Keys.First().First().Key;
                        var tmp = pp.ViewPponDealGetByBusinessHourGuid(bid);
                        PponDeal deal = pp.PponDealGet(tmp.BusinessHourGuid);
                        var discountPriceStr = ((deal.ItemDetail.ItemPrice / deal.ItemDetail.ItemOrigPrice) * 10).ToString("N0");
                        subjectStr = tmp.EventName + discountPriceStr + "折" + tmp.EventTitle;
                    }
                }
            }
            else
            {
                subjectStr = mainData.Subject;
            }

            ViewBag.AdId = data.Values.FirstOrDefault();
            ViewBag.Subject = subjectStr;
            ViewBag.ADList = ad_list;
            ViewBag.SendDate = sendDate.ToString("yyyy/MM/dd");
            ViewBag.BidList = data.Keys.FirstOrDefault();
            ViewBag.Cid = city.CityId;
            ViewBag.MainId = mainId;
            return View();
        }


        [HttpPost]
        public ActionResult PiinlifeEdmDetailSave(FormCollection formValues)
        {
            int tmpId, count = 0;
            DateTime tmpDate;
            List<EdmAreaDetail> edmdetailList = new List<EdmAreaDetail>();
            EdmAreaDetail tmpDetail = new EdmAreaDetail();
            var sendDate = DateTime.TryParse(Request.Form["sendDate"], out tmpDate) ? tmpDate : DateTime.Now;
            var cid = int.TryParse(Request.Form["cityId"], out tmpId) ? tmpId : 0;
            var cityName = Request.Form["cityName"];
            var maidId = int.TryParse(Request.Form["maidId"], out tmpId) ? tmpId : 0;
            var Subject = Request.Form["txt_Subject"];
            var Cpa = Request.Form["txt_Cpa"];
            Guid tmpData;

            EdmMain edmmain;

            #region main

            if (maidId != 0)
            {
                edmmain = EmailFacade.GetNewEdmMainById(maidId);
                edmmain.Subject = Subject;
                edmmain.Cpa = Cpa;
                edmmain.Pause = true;
                edmmain.Message += UserName + ":edit " + DateTime.Now.ToString("MMdd HH:mm") + ";";
            }
            else
            {
                edmmain = new EdmMain()
                {
                    Subject = Subject,
                    Cpa = Cpa,
                    DeliveryDate = EmailFacade.GetEdmSendTime(cid, sendDate),
                    CityId = cid,
                    CityName = cityName,
                    Creator = UserName,
                    CreateDate = DateTime.Now,
                    Type = (int)EdmMainType.Daily,
                    Status = true,
                    Pause = true
                };
            }

            #endregion main

            int pid = EmailFacade.SaveNewEdmMain(edmmain);
            EmailFacade.DeleteEdmAreaDetailByPid(pid);

            #region detail
            for (int i = 1; i < 9; i++)
            {

                var Deals1Bid = Request.Form["Deals_1_Bid_" + i];
                var Deals1Title = Request.Form["Deals_1_Title_" + i];
                if (Guid.TryParse(Deals1Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals1Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }

            }
            #endregion detail

            Combination(edmmain.Id, sendDate);

            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }



        public ActionResult AllCountryEdmDetailSetup()
        {
            return RedirectToAction("EdmMainSetup");
        }

        [HttpPost]
        public ActionResult AllCountryEdmDetailSetup(int mainId, DateTime sendDate)
        {
            var Delivery_Date = sendDate;
            var cid = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId;
            PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cid);
            ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, Delivery_Date);
            var mainData = EmailFacade.GetNewEdmMainById(mainId);
            string subjectStr = string.Empty;

            foreach (var ad in ad_list)
            {
                ad.Body = HttpUtility.HtmlDecode(ad.Body);
            }

            var data = GetEdmHotDealList(mainId, city.CityId, sendDate);


            #region 郵件主旨
            if (mainData.Id == 0 && string.IsNullOrEmpty(mainData.Subject))
            {
                #region EDM小標
                if (Delivery_Date.Month < 10)
                {
                    subjectStr += Delivery_Date.Month.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Month.ToString();
                }
                subjectStr += "/";
                if (Delivery_Date.Day < 10)
                {
                    subjectStr += Delivery_Date.Day.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Day.ToString();
                }
                #endregion

                var tmp = data.Keys.FirstOrDefault();
                subjectStr += " 宅配 精選優惠：";
                int count = 0;
                if (tmp != null)
                {
                    foreach (var t in tmp)
                    {
                        subjectStr += (count == 0) ? (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]) : "、" + (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]);
                        if (count == 2)
                        {
                            break;
                        }
                        count++;
                    }
                }

            }
            else
            {
                subjectStr = mainData.Subject;
            }
            #endregion

            ViewBag.AdId = data.Values.FirstOrDefault();
            ViewBag.Subject = subjectStr;
            ViewBag.ADList = ad_list;
            ViewBag.SendDate = sendDate.ToString("yyyy/MM/dd");
            ViewBag.BidList = data.Keys.FirstOrDefault();
            ViewBag.Cid = city.CityId;
            ViewBag.MainId = mainId;
            return View();
        }

        [HttpPost]
        public ActionResult AllCountryEdmDetailSave(FormCollection formValues)
        {
            int tmpId, count = 0;
            DateTime tmpDate;
            List<EdmAreaDetail> edmdetailList = new List<EdmAreaDetail>();
            EdmAreaDetail tmpDetail = new EdmAreaDetail();
            var sendDate = DateTime.TryParse(Request.Form["sendDate"], out tmpDate) ? tmpDate : DateTime.Now;
            var sl_AD = int.TryParse(Request.Form["sl_AD"], out tmpId) ? tmpId : 0;
            var cid = int.TryParse(Request.Form["cityId"], out tmpId) ? tmpId : 0;
            var cityName = Request.Form["cityName"];
            var maidId = int.TryParse(Request.Form["maidId"], out tmpId) ? tmpId : 0;
            var Deals1Bid1 = Request.Form["Deals_1_Bid_1"];
            var Deals1Title1 = Request.Form["Deals_1_Title_1"];
            var Deals1Bid2 = Request.Form["Deals_1_Bid_2"];
            var Deals1Title2 = Request.Form["Deals_1_Title_2"];
            var Subject = Request.Form["txt_Subject"];
            var Cpa = Request.Form["txt_Cpa"];
            Guid tmpData;

            EdmMain edmmain;

            #region main

            if (maidId != 0)
            {
                edmmain = EmailFacade.GetNewEdmMainById(maidId);
                edmmain.Subject = Subject;
                edmmain.Cpa = Cpa;
                edmmain.Pause = true;
                edmmain.Message += UserName + ":edit " + DateTime.Now.ToString("MMdd HH:mm") + ";";
            }
            else
            {
                edmmain = new EdmMain()
                {
                    Subject = Subject,
                    Cpa = Cpa,
                    DeliveryDate = EmailFacade.GetEdmSendTime(cid, sendDate),
                    CityId = cid,
                    CityName = cityName,
                    Creator = UserName,
                    CreateDate = DateTime.Now,
                    Type = (int)EdmMainType.Daily,
                    Status = true,
                    Pause = true
                };
            }

            #endregion main

            int pid = EmailFacade.SaveNewEdmMain(edmmain);
            EmailFacade.DeleteEdmAreaDetailByPid(pid);

            #region detail

            #region Deals1
            if (Guid.TryParse(Deals1Bid1, out tmpData))
            {
                tmpDetail = new EdmAreaDetail();
                tmpDetail.Pid = pid;
                tmpDetail.CityId = cid;
                tmpDetail.CityName = cityName;
                tmpDetail.Bid = tmpData;
                tmpDetail.Title = Deals1Title1;
                tmpDetail.Sequence = count;
                edmdetailList.Add(tmpDetail);
                count++;
                EmailFacade.SaveEdmAreaDetail(tmpDetail);
            }
            tmpData = new Guid();
            if (Guid.TryParse(Deals1Bid2, out tmpData))
            {
                tmpDetail = new EdmAreaDetail();
                tmpDetail.Pid = pid;
                tmpDetail.CityId = cid;
                tmpDetail.CityName = cityName;
                tmpDetail.Bid = tmpData;
                tmpDetail.Title = Deals1Title2;
                tmpDetail.Sequence = count;
                edmdetailList.Add(tmpDetail);
                count++;
                EmailFacade.SaveEdmAreaDetail(tmpDetail);
            }
            #endregion 

            #region Deals2
            for (int i = 1; i < 22; i++)
            {
                var Deals2Bid = Request.Form["Deals_2_Bid_" + i];
                var Deals2Title = Request.Form["Deals_2_Title_" + i];
                if (Guid.TryParse(Deals2Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals2Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }
            }

            #endregion 

            #region Deals3
            for (int i = 1; i < 19; i++)
            {
                var Deals3Bid = Request.Form["Deals_3_Bid_" + i];
                var Deals3Title = Request.Form["Deals_3_Title_" + i];
                if (Guid.TryParse(Deals3Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals3Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }
            }

            #endregion 

            #endregion detail

            #region ad

            EdmAreaDetail edmdetail_ad = new EdmAreaDetail() { Pid = pid, AdId = sl_AD, Type = (int)EdmDetailType.AD, CityName = "AD" };
            EmailFacade.SaveEdmAreaDetail(edmdetail_ad);

            #endregion ad

            AutoCombination(edmmain);

            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }

        public ActionResult TravelEdmDetailSetup()
        {
            return RedirectToAction("EdmMainSetup");
        }

        [HttpPost]
        public ActionResult TravelEdmDetailSetup(int mainId, DateTime sendDate)
        {
            var Delivery_Date = sendDate;
            var travelCid = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
            PponCity travelCity = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == travelCid);
            ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(travelCity, Delivery_Date);
            var mainData = EmailFacade.GetNewEdmMainById(mainId);
            string subjectStr = string.Empty;
            foreach (var ad in ad_list)
            {
                ad.Body = HttpUtility.HtmlDecode(ad.Body);
            }

            var data = GetEdmHotDealList(mainId, travelCity.CityId, sendDate);

            if (mainData.Id == 0 && string.IsNullOrEmpty(mainData.Subject))
            {
                #region EDM小標
                if (Delivery_Date.Month < 10)
                {
                    subjectStr += Delivery_Date.Month.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Month.ToString();
                }
                subjectStr += "/";
                if (Delivery_Date.Day < 10)
                {
                    subjectStr += Delivery_Date.Day.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Day.ToString();
                }
                #endregion

                var tmp = data.Keys.FirstOrDefault();
                subjectStr += " 旅遊 精選優惠：";
                int count = 0;
                if (tmp != null)
                {
                    foreach (var t in tmp)
                    {
                        subjectStr += (count == 0) ? (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]) : "、" + (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]);
                        if (count == 2)
                        {
                            break;
                        }
                        count++;
                    }
                }

            }
            else
            {
                subjectStr = mainData.Subject;
            }

            ViewBag.AdId = data.Values.FirstOrDefault();
            ViewBag.Subject = subjectStr;
            ViewBag.ADList = ad_list;
            ViewBag.SendDate = sendDate.ToString("yyyy/MM/dd");
            ViewBag.BidList = data.Keys.FirstOrDefault();
            ViewBag.Cid = travelCity.CityId;
            ViewBag.MainId = mainId;
            return View();
        }

        [HttpPost]
        public ActionResult TravelEdmDetailSave(FormCollection formValues)
        {
            int tmpId, count = 0;
            DateTime tmpDate;
            List<EdmAreaDetail> edmdetailList = new List<EdmAreaDetail>();
            EdmAreaDetail tmpDetail = new EdmAreaDetail();
            var sendDate = DateTime.TryParse(Request.Form["sendDate"], out tmpDate) ? tmpDate : DateTime.Now;
            var cid = int.TryParse(Request.Form["cityId"], out tmpId) ? tmpId : 0;
            var cityName = Request.Form["cityName"];
            var sl_AD = int.TryParse(Request.Form["sl_AD"], out tmpId) ? tmpId : 0;
            var maidId = int.TryParse(Request.Form["maidId"], out tmpId) ? tmpId : 0;
            var Subject = Request.Form["txt_Subject"];
            var Cpa = Request.Form["txt_Cpa"];
            Guid tmpData;

            EdmMain edmmain;

            #region main

            if (maidId != 0)
            {
                edmmain = EmailFacade.GetNewEdmMainById(maidId);
                edmmain.Subject = Subject;
                edmmain.Cpa = Cpa;
                edmmain.Pause = true;
                edmmain.Message += UserName + ":edit " + DateTime.Now.ToString("MMdd HH:mm") + ";";
            }
            else
            {
                edmmain = new EdmMain()
                {
                    Subject = Subject,
                    Cpa = Cpa,
                    DeliveryDate = EmailFacade.GetEdmSendTime(cid, sendDate),
                    CityId = cid,
                    CityName = cityName,
                    Creator = UserName,
                    CreateDate = DateTime.Now,
                    Type = (int)EdmMainType.Daily,
                    Status = true,
                    Pause = true
                };
            }

            #endregion main

            int pid = EmailFacade.SaveNewEdmMain(edmmain);
            EmailFacade.DeleteEdmAreaDetailByPid(pid);

            #region detail
            for (int i = 1; i < 10; i++)
            {
                var Deals1Bid = Request.Form["Deals_1_Bid_" + i];
                var Deals1Title = Request.Form["Deals_1_Title_" + i];
                if (Guid.TryParse(Deals1Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals1Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }
            }
            for (int i = 1; i < 7; i++)
            {
                var Deals2Bid = Request.Form["Deals_2_Bid_" + i];
                var Deals2Title = Request.Form["Deals_2_Title_" + i];
                if (Guid.TryParse(Deals2Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals2Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }
            }
            #endregion detail

            #region ad

            EdmAreaDetail edmdetail_ad = new EdmAreaDetail() { Pid = pid, AdId = sl_AD, Type = (int)EdmDetailType.AD, CityName = "AD" };
            EmailFacade.SaveEdmAreaDetail(edmdetail_ad);

            #endregion ad

            AutoCombination(edmmain);

            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }


        public ActionResult LocationEdmDetailSetup()
        {
            return RedirectToAction("EdmMainSetup");
        }

        [HttpPost]
        public ActionResult LocationEdmDetailSetup(int cid, int mainId, DateTime sendDate)
        {
            var Delivery_Date = sendDate;
            PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cid);
            ViewCmsRandomCollection ad_list = EmailFacade.GetNewEdmADByCityIdDate(city, Delivery_Date);
            var mainData = EmailFacade.GetNewEdmMainById(mainId);
            string subjectStr = string.Empty;

            foreach (var ad in ad_list)
            {
                ad.Body = HttpUtility.HtmlDecode(ad.Body);
            }

            var data = GetEdmHotDealList(mainId, city.CityId, sendDate);

            if (mainData.Id == 0 && string.IsNullOrEmpty(mainData.Subject))
            {
                #region EDM小標
                if (Delivery_Date.Month < 10)
                {
                    subjectStr += Delivery_Date.Month.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Month.ToString();
                }
                subjectStr += "/";

                if (Delivery_Date.Day < 10)
                {
                    subjectStr += Delivery_Date.Day.ToString().Replace("0", "");
                }
                else
                {
                    subjectStr += Delivery_Date.Day.ToString();
                }


                if (cid == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
                {
                    ViewBag.CityName = "台北";
                    ViewBag.Cpa = "eDM_TP";
                    subjectStr += " 台北";
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
                {
                    ViewBag.CityName = "桃竹苗";
                    ViewBag.Cpa = "eDM_TY";
                    subjectStr += " 桃竹";
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
                {
                    ViewBag.CityName = "中彰投";
                    ViewBag.Cpa = "eDM_TC";
                    subjectStr += " 中彰";
                }
                else if (cid == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
                {
                    ViewBag.CityName = "雲嘉南高";
                    ViewBag.Cpa = "eDM_GX";
                    subjectStr += " 南部";
                }
                #endregion

                var tmp = data.Keys.FirstOrDefault();
                subjectStr += " " + "精選優惠：";
                int count = 0;
                if (tmp != null)
                {
                    foreach (var t in tmp)
                    {
                        subjectStr += (count == 0) ? (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]) : "、" + (!string.IsNullOrEmpty(t.Value[1]) ? t.Value[1] : t.Value[0]);
                        if (count == 2)
                        {
                            break;
                        }
                        count++;
                    }
                }
            }
            else
            {
                ViewBag.Cpa = mainData.Cpa;
                subjectStr = mainData.Subject;
            }

            ViewBag.AdId = data.Values.FirstOrDefault();
            ViewBag.Subject = subjectStr;
            ViewBag.ADList = ad_list;
            ViewBag.BidList = data.Keys.FirstOrDefault();
            ViewBag.Cid = city.CityId;
            ViewBag.MainId = mainId;
            ViewBag.SendDate = sendDate.ToString("yyyy/MM/dd"); ;

            return View();
        }

        [HttpPost]
        public ActionResult LocationEdmDetailSave(FormCollection formValues)
        {
            int tmpId, count = 0;
            DateTime tmpDate;
            List<EdmAreaDetail> edmdetailList = new List<EdmAreaDetail>();
            EdmAreaDetail tmpDetail = new EdmAreaDetail();
            var sendDate = DateTime.TryParse(Request.Form["sendDate"], out tmpDate) ? tmpDate : DateTime.Now;
            var sl_AD = int.TryParse(Request.Form["sl_AD"], out tmpId) ? tmpId : 0;
            var cid = int.TryParse(Request.Form["cityId"], out tmpId) ? tmpId : 0;
            var cityName = Request.Form["cityName"];
            var maidId = int.TryParse(Request.Form["maidId"], out tmpId) ? tmpId : 0;
            var Deals1Bid1 = Request.Form["Deals_1_Bid_1"];
            var Deals1Title1 = Request.Form["Deals_1_Title_1"];
            var Subject = Request.Form["txt_Subject"];
            var Cpa = Request.Form["txt_Cpa"];
            Guid tmpData;

            EdmMain edmmain;

            #region main

            if (maidId != 0)
            {
                edmmain = EmailFacade.GetNewEdmMainById(maidId);
                edmmain.Subject = Subject;
                edmmain.Cpa = Cpa;
                edmmain.Pause = false;
                edmmain.Message += UserName + ":edit " + DateTime.Now.ToString("MMdd HH:mm") + ";";
            }
            else
            {
                edmmain = new EdmMain()
                {
                    Subject = Subject,
                    Cpa = Cpa,
                    DeliveryDate = EmailFacade.GetEdmSendTime(cid, sendDate),
                    CityId = cid,
                    CityName = cityName,
                    Creator = UserName,
                    CreateDate = DateTime.Now,
                    Type = (int)EdmMainType.Daily,
                    Status = true,
                    Pause = false
                };
            }

            #endregion main

            int pid = EmailFacade.SaveNewEdmMain(edmmain);
            EmailFacade.DeleteEdmAreaDetailByPid(pid);

            #region detail

            #region Deals1
            if (Guid.TryParse(Deals1Bid1, out tmpData))
            {
                tmpDetail = new EdmAreaDetail();
                tmpDetail.Pid = pid;
                tmpDetail.CityId = cid;
                tmpDetail.CityName = cityName;
                tmpDetail.Bid = tmpData;
                tmpDetail.Title = Deals1Title1;
                tmpDetail.Sequence = count;
                edmdetailList.Add(tmpDetail);
                count++;
                EmailFacade.SaveEdmAreaDetail(tmpDetail);
            }
            #endregion 

            #region Deals2
            for (int i = 1; i < 10; i++)
            {
                var Deals2Bid = Request.Form["Deals_2_Bid_" + i];
                var Deals2Title = Request.Form["Deals_2_Title_" + i];
                if (Guid.TryParse(Deals2Bid, out tmpData))
                {
                    tmpDetail = new EdmAreaDetail();
                    tmpDetail.Pid = pid;
                    tmpDetail.CityId = cid;
                    tmpDetail.Bid = tmpData;
                    tmpDetail.Title = Deals2Title;
                    tmpDetail.CityName = cityName;
                    tmpDetail.Sequence = count;
                    edmdetailList.Add(tmpDetail);
                    count++;
                    EmailFacade.SaveEdmAreaDetail(tmpDetail);
                }
            }

            #endregion 

            #endregion detail

            #region ad

            EdmAreaDetail edmdetail_ad = new EdmAreaDetail() { Pid = pid, AdId = sl_AD, Type = (int)EdmDetailType.AD, CityName = "AD" };
            EmailFacade.SaveEdmAreaDetail(edmdetail_ad);

            #endregion ad

            AutoCombination(edmmain);

            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }

        [HttpPost]
        public ActionResult DeleteEdm(int mainId, DateTime sendDate)
        {
            EmailFacade.DeleteNewEdmDetailByPid(mainId);
            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }

        [HttpPost]
        public ActionResult EnableEdm(int mainId, DateTime sendDate, int edmEnabled)
        {
            EmailFacade.EnableEdmByPid(mainId, edmEnabled == 1);
            return RedirectToAction("EdmMainSetup", new { sendDate = sendDate });
        }

        #endregion 

        #region SoloEDM

        public ActionResult SoloEdmList()
        {
            ViewBag.EdmList = EmailFacade.GetSoloEdmMainByType((int)SoloEdmType.Default)
                .OrderByDescending(t => t.Id).ToList();
            ViewBag.User = UserName;
            return View();
        }

        public ActionResult SoloEdmSetup(int id = 0, string type = "")
        {
            ViewBag.MainId = id;
            ViewBag.MsgType = true;
            ViewBag.BrandType = true;
            ViewBag.PponType = true;
            ViewBag.SendDataType = true;
            if (id != 0)
            {
                var tmp = EmailFacade.GetSoloEdmMainById(id);

                if (type == "copy")
                {
                    ViewBag.MainId = 0;
                }
                else
                {
                    ViewBag.SendeEmailCount = EmailFacade.GetSoloEdmSendListCountById(id);
                }

                ViewBag.EdmData = tmp;
                if (string.IsNullOrEmpty(tmp.Message))
                {
                    ViewBag.MsgType = false;
                }
                ViewBag.SendDateStr = tmp.SendDate.ToString("yyyy/MM/dd");
                ViewBag.SendTimeStr = tmp.SendDate.Hour;
                var brandData = EmailFacade.GetSoloEdmBrandById(id);
                if (brandData.Count > 0)
                {
                    var brandList = new List<BrandList>();
                    foreach (var d in brandData)
                    {
                        Brand brand = pp.GetBrand(d.BrandId ?? 0);
                        var t = new BrandList();
                        t.BrandId = d.BrandId ?? 0;
                        t.BrandName = brand.BrandName;
                        t.GroupId = d.GroupId ?? 0;
                        t.GroupName = d.GroupName;
                        t.Url = d.Url;
                        t.ImgUrl = ImageFacade.GetMediaPath(brand.EventListImage, MediaType.DealPromoImage);

                        brandList.Add(t);
                    }

                    ViewBag.BrandList = brandList.GroupBy(x => x.GroupId);
                }
                else
                {
                    ViewBag.BrandType = false;
                }
                var pponData = EmailFacade.GetSoloEdmPponById(id);
                if (pponData.Count > 0)
                {

                    var PponList = new List<PponList>();
                    foreach (var d in pponData)
                    {
                        var deal = EmailFacade.ViewPponDealGetByBusinessHourGuid(d.Bid);
                        var t = new PponList();
                        t.Bid = d.Bid;
                        t.BlackTitle = d.BlackTitle;
                        t.GroupId = d.GroupId;
                        t.GroupName = d.GroupName;
                        t.OrangeTitle = d.OrangeTitle;
                        t.Url = d.Url;
                        t.ImgUrl = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).First();

                        PponList.Add(t);
                    }

                    ViewBag.PponList = PponList.GroupBy(x => x.GroupId);
                }
                else
                {
                    ViewBag.PponType = false;
                }
            }

            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SoloEdmSetup(HttpPostedFileBase file, EditSoloEdmMainData data)
        {
            List<BrandList> brandList = new List<BrandList>();
            List<PponList> pponList = new List<PponList>();

            var mainData = new SoloEdmMain();

            if (data.EdmMainId != 0)
            {
                mainData = EmailFacade.GetSoloEdmMainById(data.EdmMainId);
            }

            mainData.CreateDate = DateTime.Now;
            mainData.Creator = UserName;
            mainData.PromoImg = data.PromoImg;
            mainData.SendDate = data.SendDate;
            mainData.Subject = data.Subject;
            mainData.Cpa = data.Cpa;
            mainData.Title = data.Title;
            mainData.TitleContent = data.TitleContent;
            mainData.Type = 0;

            if (!data.CloseMsgType)
            {
                mainData.Message = data.Message;
            }

            EmailFacade.SaveSoloEdmMain(mainData);

            if (!data.CloseBrandType)
            {
                if (!string.IsNullOrEmpty(data.BrandData))
                {
                    EmailFacade.DeleteSoloEdmBrandByMainId(mainData.Id);
                    brandList = _serializer.Deserialize<List<BrandList>>(data.BrandData);
                }
                foreach (var brand in brandList)
                {
                    var tmp = new SoloEdmBrand();
                    tmp.MainId = mainData.Id;
                    tmp.BrandId = brand.BrandId;
                    tmp.GroupId = brand.GroupId;
                    tmp.GroupName = brand.GroupName;
                    tmp.Url = brand.Url;
                    EmailFacade.SaveSoloEdmBrand(tmp);
                }
            }

            if (!data.ClosePponType)
            {
                if (!string.IsNullOrEmpty(data.PponData))
                {
                    EmailFacade.DeleteSoloEdmPponByMainId(mainData.Id);
                    pponList = _serializer.Deserialize<List<PponList>>(data.PponData);
                }
                foreach (var ppon in pponList)
                {
                    var tmp = new SoloEdmPpon();
                    tmp.MainId = mainData.Id;
                    tmp.Bid = ppon.Bid;
                    tmp.BlackTitle = ppon.BlackTitle;
                    tmp.OrangeTitle = ppon.OrangeTitle;
                    tmp.GroupId = ppon.GroupId;
                    tmp.GroupName = ppon.GroupName;
                    tmp.Url = ppon.Url;
                    EmailFacade.SaveSoloEdmPpon(tmp);
                }
            }

            if (!data.CheckSendUpdata)
            {
                if (data.CheckAllUser)
                {
                    EmailFacade.SoloEdmSetAllMembers(mainData.Id);
                }
                else
                {
                    if (file != null)
                    {
                        List<string> lines;
                        HashSet<string> emails = new HashSet<string>();
                        if (file.FileName.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
                        {
                            lines = Helper.GetLinesFromZippedStream(file.InputStream);
                        }
                        else
                        {
                            lines = Helper.GetLinesFromStream(file.InputStream);
                        }

                        foreach (string line in lines)
                        {
                            if (string.IsNullOrWhiteSpace(line))
                            {
                                continue;
                            }
                            string[] parts = line.Split(",");
                            if (parts.Length == 1)
                            {
                                if (RegExRules.CheckEmail(parts[0]) == false)
                                {
                                    continue;
                                }
                                emails.Add(parts[0]);
                            }
                            else if (parts.Length == 3)
                            {
                                if (RegExRules.CheckEmail(parts[0]) == false)
                                {
                                    continue;
                                }
                                if (emails.Contains(parts[0]))
                                {
                                    continue;
                                }
                                emails.Add(parts[0]);
                            }
                        }
                        if (emails.Count > 0)
                        {
                            EmailFacade.ResetSoloEdmSendEmails(mainData.Id, emails);
                        }
                    }
                }
            }

            return RedirectToAction("SoloEdmList");
        }

        #endregion



        #region EdmHotDeal
        public ActionResult EdmHotDealList()
        {

            var modelList = new List<EdmHotDealModel>();
            EdmHotDealCollection ehdc = pp.EdmHotDealGet();
            foreach (var ehd in ehdc)
            {
                EdmHotDealModel model = new EdmHotDealModel();
                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(ehd.Bid);
                model.id = ehd.Id;
                model.uniqueId = Convert.ToInt32(vpd.UniqueId);
                model.itemName = vpd.ItemName;
                model.startDate = ehd.StartDate;
                model.endDate = ehd.EndDate;
                model.inTime = ehd.StartDate <= DateTime.Now.Date && ehd.EndDate > DateTime.Now.Date;
                modelList.Add(model);

            }
            ViewBag.EdmHotDealList = modelList;
            return View();
        }

        public ActionResult AddEdmHotDeal(string strGuid, string strStartDate)
        {
            Guid guid = Guid.Empty;
            Guid.TryParse(strGuid, out guid);
            DateTime startDate = DateTime.MinValue;
            DateTime.TryParse(strStartDate, out startDate);

            if (guid != Guid.Empty)
            {
                //DealProperty dp = pp.DealPropertyGet(uniqueId);
                BusinessHour bh = pp.BusinessHourGet(guid);
                if (bh.IsLoaded)
                {
                    EdmHotDealCollection ehd = pp.EdmHotDealGetByBid(bh.Guid, DateTime.Now);
                    if (!ehd.Any())
                    {
                        if (!Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {
                            EdmHotDeal edmHotDeal = new EdmHotDeal();
                            edmHotDeal.Bid = guid;
                            edmHotDeal.StartDate = startDate;
                            edmHotDeal.EndDate = DateTime.Now.Date.AddDays(7);
                            edmHotDeal.CreateId = UserName;
                            edmHotDeal.CreateTime = DateTime.Now;
                            edmHotDeal.Status = 1;
                            pp.EdmHotDealSet(edmHotDeal);

                            return Json(new { IsSuccess = true, Message = "新增成功" });
                        }
                        else
                        {
                            return Json(new { IsSuccess = false, Message = "不允許輸入子檔" });
                        }

                    }
                    else
                    {

                        return Json(new { IsSuccess = false, Message = "重複檔次資料" });
                    }

                }
                else
                {
                    return Json(new { IsSuccess = false, Message = "無該檔次資料" });
                }
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "輸入錯誤" });
            }

        }

        public ActionResult DeleteEdmHotDeal(string strIdList)
        {
            try
            {
                List<int> idList = strIdList.Split(",").Select(Int32.Parse).ToList();

                foreach (int id in idList)
                {
                    EdmHotDeal ehd = pp.EdmHotDealGetById(id);
                    ehd.Status = 0;
                    ehd.ModifyId = UserName;
                    ehd.ModifyTime = DateTime.Now;
                    pp.EdmHotDealSet(ehd);
                }

                return Json(new { IsSuccess = true, Message = "刪除成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = "刪除失敗" });
            }


        }

        public ActionResult ExtendEdmHotDeal(string strIdList, string strEndDate)
        {
            try
            {
                DateTime endDate = DateTime.MinValue;
                DateTime.TryParse(strEndDate, out endDate);

                if (endDate != DateTime.MinValue)
                {
                    List<int> idList = strIdList.Split(",").Select(Int32.Parse).ToList();
                    EdmHotDealCollection ehdc = new EdmHotDealCollection();
                    string extendErrorId = "";
                    string repeatErrorId = "";
                    foreach (int id in idList)
                    {
                        EdmHotDeal ehd = pp.EdmHotDealGetById(id);
                        if (ehd.EndDate >= endDate)
                        {
                            extendErrorId += id + ",";
                        }
                        else if (ehdc.Where(x => x.Bid == ehd.Bid).Any())
                        {
                            repeatErrorId += id + ",";
                        }
                        else
                        {
                            ehd.EndDate = endDate;
                            ehd.ModifyId = UserName;
                            ehd.ModifyTime = DateTime.Now;
                            ehdc.Add(ehd);
                        }

                    }

                    if (string.IsNullOrEmpty(extendErrorId) && string.IsNullOrEmpty(repeatErrorId))
                    {
                        pp.EdmHotDealCollectionSet(ehdc);
                        return Json(new { IsSuccess = true, Message = "延長成功" });
                    }
                    else if (!string.IsNullOrEmpty(extendErrorId))
                    {
                        return Json(new { IsSuccess = false, Message = "延長失敗，延長時間<=原結束時間 \n id:" + extendErrorId.Substring(0, extendErrorId.Length - 1) });
                    }
                    else
                    {
                        return Json(new { IsSuccess = false, Message = "延長失敗，同檔次不允許同時延長 \n id:" + repeatErrorId.Substring(0, repeatErrorId.Length - 1) });
                    }

                }
                else
                {
                    return Json(new { IsSuccess = false, Message = "延長失敗，時間輸入錯誤" });
                }
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = "延長失敗" + ex.Message });
            }


        }

        public class EdmHotDealModel
        {
            public int id { get; set; }
            public int uniqueId { get; set; }
            public string itemName { get; set; }
            public DateTime startDate { get; set; }
            public DateTime endDate { get; set; }
            public bool inTime { get; set; }
        }
        #endregion

        #region Method

        #region Daily EDM

        private void Combination(int mainId, DateTime sendDate)
        {
            var EdmList = pp.GetEdmMainByDate(sendDate, EdmMainType.Daily, true);
            var tmpData = EmailFacade.GetEdmAreaDetailsByPid(mainId);
            var tmp = tmpData.Where(x => x.CityId != null);
            if (tmp.Any())
            {
                var tmpCid = tmp.First().CityId;

                if (tmpCid == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
                {
                    #region 品生活

                    EdmDetail detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = 16,
                        CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.Piinlife.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 2,
                        RowNumber = 4,
                        Sequence = 0,
                        SortType = 3
                    };

                    EmailFacade.SaveNewEdmDetail(detail);
                    var count = 0;
                    foreach (var item in tmpData)
                    {
                        detail = new EdmDetail();
                        detail.Pid = mainId;
                        detail.Bid = item.Bid;
                        detail.Title = item.Title;
                        detail.Type = 17;
                        detail.Sequence = count;
                        count++;
                        EmailFacade.SaveNewEdmDetail(detail);
                    }
                    #endregion
                }
                else if (tmpCid == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                {
                    #region 宅配
                    var piinlifData = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId).Id).First();
                    var travelData = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId).Id).Take(10);
                    var allCountryList = tmpData.Where(x => x.Bid != null);
                    var adId = tmpData.First(x => x.AdId != null).AdId;

                    EdmDetail detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = 2,
                        CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.AllCountry.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 2,
                        RowNumber = 1,
                        Sequence = 0,
                        SortType = 3
                    };
                    EmailFacade.SaveNewEdmDetail(detail);
                    int count = 0, sqCount = 0, typeCount = 0;

                    foreach (var item in allCountryList)
                    {
                        detail = new EdmDetail();
                        if (count == 0)
                        {
                            detail.Pid = mainId;
                            detail.Bid = item.Bid;
                            detail.Title = item.Title;
                            detail.Type = 3;
                            detail.Sequence = 0;
                            count++;
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                        else if (count == 1)
                        {
                            detail.Pid = mainId;
                            detail.Bid = item.Bid;
                            detail.Title = item.Title;
                            detail.Type = 3;
                            detail.Sequence = 1;
                            EmailFacade.SaveNewEdmDetail(detail);
                            detail = new EdmDetail
                            {
                                Pid = mainId,
                                Type = typeCount = 4,
                                CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                                CityName = PponCityGroup.DefaultPponCityGroup.AllCountry.CityName,
                                DisplayCityName = false,
                                ColumnNumber = 3,
                                RowNumber = 7,
                                Sequence = 0,
                                SortType = 3
                            };
                            count++;
                            typeCount++;
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                        else if (count == 23)
                        {
                            typeCount++;
                            detail = new EdmDetail
                            {
                                Pid = mainId,
                                Type = typeCount,
                                CityId = PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityId,
                                CityName = PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityName,
                                DisplayCityName = true,
                                ColumnNumber = 3,
                                RowNumber = 6,
                                Sequence = 0,
                                SortType = 3
                            };
                            sqCount = 0;
                            count++;
                            typeCount++;
                            EmailFacade.SaveNewEdmDetail(detail);

                            detail = new EdmDetail
                            {
                                Pid = mainId,
                                Bid = item.Bid,
                                Title = item.Title,
                                Type = typeCount,
                                Sequence = sqCount
                            };
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                        else
                        {
                            detail.Pid = mainId;
                            detail.Bid = item.Bid;
                            detail.Title = item.Title;
                            detail.Type = typeCount;
                            detail.Sequence = sqCount;
                            sqCount++;
                            count++;
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                    }

                    detail = new EdmDetail();
                    typeCount++;
                    detail.Pid = mainId;
                    detail.Type = typeCount;
                    detail.CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId;
                    detail.CityName = "旅遊";
                    detail.DisplayCityName = true;
                    detail.ColumnNumber = 3;
                    detail.RowNumber = 3;
                    detail.Sequence = 0;
                    detail.SortType = 3;
                    typeCount++;
                    sqCount = 0;
                    EmailFacade.SaveNewEdmDetail(detail);

                    foreach (var item in travelData)
                    {
                        detail = new EdmDetail
                        {
                            Pid = mainId,
                            Bid = item.Bid,
                            Title = item.Title,
                            Type = typeCount,
                            Sequence = sqCount
                        };
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                    }

                    detail = new EdmDetail();
                    detail.Pid = mainId;
                    detail.Type = 16;
                    detail.CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId;
                    detail.CityName = PponCityGroup.DefaultPponCityGroup.Piinlife.CityName;
                    detail.DisplayCityName = true;
                    detail.ColumnNumber = 1;
                    detail.RowNumber = 1;
                    detail.Sequence = 0;
                    detail.SortType = 3;
                    EmailFacade.SaveNewEdmDetail(detail);

                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Bid = piinlifData.Bid,
                        Title = piinlifData.Title,
                        Type = 17,
                        Sequence = 0
                    };
                    EmailFacade.SaveNewEdmDetail(detail);

                    EdmDetail edmdetailAd = new EdmDetail() { Pid = mainId, AdId = adId, Type = (int)EdmDetailType.AD, CityName = "AD", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetailAd);

                    #endregion
                }
                else if (tmpCid == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
                {
                    #region 旅遊．玩美．休閒
                    var piinlifData = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId).Id).First();
                    var allCountry = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).Id).Take(13);
                    var travel = tmpData.Where(x => x.Bid != null);
                    var adId = tmpData.First(x => x.AdId != null).AdId;
                    int count = 0, sqCount = 0, typeCount;

                    EdmDetail detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = typeCount = 2,
                        CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.Travel.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 4,
                        RowNumber = 10,
                        Sequence = 0,
                        SortType = 3
                    };
                    typeCount++;
                    EmailFacade.SaveNewEdmDetail(detail);
                    foreach (var item in travel)
                    {
                        detail = new EdmDetail
                        {
                            Pid = mainId,
                            Bid = item.Bid,
                            Title = item.Title,
                            Type = typeCount,
                            Sequence = sqCount
                        };
                        count++;
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                        if (count == 9)
                        {
                            typeCount++;
                            detail = new EdmDetail();
                            detail.Pid = mainId;
                            detail.Type = typeCount;
                            detail.CityId = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId;
                            detail.CityName = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName;
                            detail.DisplayCityName = true;
                            detail.ColumnNumber = 3;
                            detail.RowNumber = 2;
                            detail.Sequence = 0;
                            detail.SortType = 3;
                            typeCount++;
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                    }
                    typeCount++;
                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = typeCount,
                        CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.AllCountry.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 3,
                        RowNumber = 4,
                        Sequence = 0,
                        SortType = 3
                    };
                    typeCount++;
                    EmailFacade.SaveNewEdmDetail(detail);
                    foreach (var item in allCountry)
                    {
                        detail = new EdmDetail();
                        detail.Pid = mainId;
                        detail.Bid = item.Bid;
                        detail.Title = item.Title;
                        detail.Type = typeCount;
                        detail.Sequence = sqCount;
                        count++;
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                    }

                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = 16,
                        CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.Piinlife.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 1,
                        RowNumber = 1,
                        Sequence = 0,
                        SortType = 3
                    };
                    EmailFacade.SaveNewEdmDetail(detail);
                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Bid = piinlifData.Bid,
                        Title = piinlifData.Title,
                        Type = 17,
                        Sequence = 0
                    };
                    EmailFacade.SaveNewEdmDetail(detail);

                    EdmDetail edmdetailAd = new EdmDetail() { Pid = mainId, AdId = adId, Type = (int)EdmDetailType.AD, CityName = "AD", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetailAd);

                    #endregion
                }
                else
                {
                    #region 在地
                    var piinlifData = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId).Id).First();
                    var allCountry = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).Id).Take(13);
                    var travel = EmailFacade.GetEdmAreaDetailsByPid(EdmList.First(x => x.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId).Id).Where(x => x.Bid != null);
                    PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == tmp.First().CityId);
                    var location = tmpData.Where(x => x.Bid != null);
                    var adId = tmpData.First(x => x.AdId != null).AdId;
                    int count = 0, sqCount = 0, typeCount;
                    string cityName = "";
                    if (city.CityId == PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId)
                    {
                        cityName = "台北";
                    }
                    else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taoyuan.CityId)
                    {
                        cityName = "桃竹苗";
                    }
                    else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Taichung.CityId)
                    {
                        cityName = "中彰投";
                    }
                    else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Kaohsiung.CityId)
                    {
                        cityName = "雲嘉南高";
                    }

                    EdmDetail detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = typeCount = 2,
                        CityId = city.CityId,
                        CityName = cityName,
                        DisplayCityName = true,
                        ColumnNumber = 1,
                        RowNumber = 1,
                        Sequence = 0,
                        SortType = 3
                    };
                    typeCount++;
                    EmailFacade.SaveNewEdmDetail(detail);

                    foreach (var item in location)
                    {
                        if (count == 1)
                        {
                            typeCount++;
                            detail = new EdmDetail
                            {
                                Pid = mainId,
                                Type = typeCount,
                                CityId = city.CityId,
                                CityName = cityName,
                                DisplayCityName = false,
                                ColumnNumber = 3,
                                RowNumber = 3,
                                Sequence = 0,
                                SortType = 3
                            };

                            EmailFacade.SaveNewEdmDetail(detail);
                            sqCount = 0;
                            typeCount++;
                        }
                        detail = new EdmDetail();
                        detail.Pid = mainId;
                        detail.Bid = item.Bid;
                        detail.Title = item.Title;
                        detail.Type = typeCount;
                        detail.Sequence = sqCount;
                        count++;
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                    }

                    typeCount++;
                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = typeCount,
                        CityId = PponCityGroup.DefaultPponCityGroup.Travel.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.Travel.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 3,
                        RowNumber = 3,
                        Sequence = 0,
                        SortType = 3
                    };
                    typeCount++;
                    EmailFacade.SaveNewEdmDetail(detail);
                    foreach (var item in travel)
                    {
                        detail = new EdmDetail
                        {
                            Pid = mainId,
                            Bid = item.Bid,
                            Title = item.Title,
                            Type = typeCount,
                            Sequence = sqCount
                        };
                        count++;
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                        if (count == 19)
                        {
                            typeCount++;
                            detail = new EdmDetail();
                            detail.Pid = mainId;
                            detail.Type = typeCount;
                            detail.CityId = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId;
                            detail.CityName = PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityName;
                            detail.DisplayCityName = true;
                            detail.ColumnNumber = 3;
                            detail.RowNumber = 2;
                            detail.Sequence = 0;
                            detail.SortType = 3;
                            typeCount++;
                            EmailFacade.SaveNewEdmDetail(detail);
                        }
                    }
                    typeCount++;
                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = typeCount,
                        CityId = PponCityGroup.DefaultPponCityGroup.AllCountry.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.AllCountry.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 3,
                        RowNumber = 4,
                        Sequence = 0,
                        SortType = 3
                    };
                    typeCount++;
                    EmailFacade.SaveNewEdmDetail(detail);
                    foreach (var item in allCountry)
                    {
                        detail = new EdmDetail();
                        detail.Pid = mainId;
                        detail.Bid = item.Bid;
                        detail.Title = item.Title;
                        detail.Type = typeCount;
                        detail.Sequence = sqCount;
                        count++;
                        sqCount++;
                        EmailFacade.SaveNewEdmDetail(detail);
                    }

                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Type = 16,
                        CityId = PponCityGroup.DefaultPponCityGroup.Piinlife.CityId,
                        CityName = PponCityGroup.DefaultPponCityGroup.Piinlife.CityName,
                        DisplayCityName = true,
                        ColumnNumber = 1,
                        RowNumber = 1,
                        Sequence = 0,
                        SortType = 3
                    };
                    EmailFacade.SaveNewEdmDetail(detail);
                    detail = new EdmDetail
                    {
                        Pid = mainId,
                        Bid = piinlifData.Bid,
                        Title = piinlifData.Title,
                        Type = 17,
                        Sequence = 0
                    };
                    EmailFacade.SaveNewEdmDetail(detail);

                    EdmDetail edmdetailAd = new EdmDetail() { Pid = mainId, AdId = adId, Type = (int)EdmDetailType.AD, CityName = "AD", DisplayCityName = false };
                    EmailFacade.SaveNewEdmDetail(edmdetailAd);

                    #endregion
                }
            }

        }

        private void AutoCombination(EdmMain edmmain)
        {
            #region 條件整理

            var sendDate = edmmain.DeliveryDate;
            var tmpDate = new DateTime(sendDate.Year, sendDate.Month, sendDate.Day, 0, 0, 0);
            var EdmList = pp.GetEdmMainByDate(tmpDate, EdmMainType.Daily, true);
            var AllCountryMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.AllCountry.CityId);
            var AllCountryAreaCount = (AllCountryMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(AllCountryMain.Id).Count : 0;
            var TravelMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Travel.CityId);
            var TravelAreaCount = (TravelMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(TravelMain.Id).Count : 0;
            var PiinlifeMain = GetEdmId(EdmList, PponCityGroup.DefaultPponCityGroup.Piinlife.CityId);
            var PiinlifeAreaCount = (PiinlifeMain.Id != 0) ? pp.GetEdmAreaCollectionByPid(PiinlifeMain.Id).Count : 0;

            #endregion
            if (edmmain.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId || edmmain.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                if (AllCountryAreaCount > 0 && TravelAreaCount > 0 && PiinlifeAreaCount > 0)
                {
                    Combination(AllCountryMain.Id, tmpDate);
                    Combination(TravelMain.Id, tmpDate);
                }
            }
            else
            {
                Combination(edmmain.Id, tmpDate);
            }
        }

        private EdmMain GetEdmId(EdmMainCollection data, int cityId)
        {

            var tmpData = data.Where(x => x.CityId == cityId);
            if (tmpData.Any())
            {
                return tmpData.First();
            }
            return new EdmMain();
        }

        private Dictionary<Dictionary<Guid, string>, string> GetEdmPponList(int mainId, int cityId, DateTime sendDate)
        {
            Dictionary<Guid, string> PponDictionary = new Dictionary<Guid, string>();
            Dictionary<Dictionary<Guid, string>, string> Data = new Dictionary<Dictionary<Guid, string>, string>();
            PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cityId);
            var viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetListByCityDate(city.CityId, sendDate).Take(50);
            var tmpData = EmailFacade.GetEdmAreaDetailsByPid(mainId);
            var adId = "";
            if (city.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                #region 旅遊
                if (tmpData.Count > 0 && tmpData.Any(x => x.AdId != null))
                {
                    adId = tmpData.First(x => x.AdId != null).AdId.ToString();
                }
                var detailData = tmpData.Where(x => x.Bid != null);

                if (detailData.Any())
                {
                    foreach (var d in detailData)
                    {
                        if (!PponDictionary.ContainsKey(d.Bid ?? new Guid()))
                        {
                            PponDictionary.Add(d.Bid ?? new Guid(), d.Title);
                        }
                    }
                }

                if (PponDictionary.Count < 20)
                {
                    var tmpCount = PponDictionary.Count;
                    foreach (var d in viewPponDeal.Take(20))
                    {
                        if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                        {
                            PponDictionary.Add(d.BusinessHourGuid, d.ItemName);
                        }
                        if (PponDictionary.Count >= 20)
                        {
                            break;
                        }
                    }
                }

                viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetListByCityDate(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, sendDate).Take(20);
                foreach (var d in viewPponDeal)
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, d.ItemName);
                    }
                }
                #endregion
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                #region 宅配

                if (tmpData.Count > 0 && tmpData.Any(x => x.AdId != null))
                {
                    adId = tmpData.First(x => x.AdId != null).AdId.ToString();
                }
                var detailData = tmpData.Where(x => x.Bid != null);

                if (detailData.Any())
                {
                    foreach (var d in detailData)
                    {
                        if (!PponDictionary.ContainsKey(d.Bid ?? new Guid()))
                        {
                            PponDictionary.Add(d.Bid ?? new Guid(), d.Title);
                        }
                    }
                }
                foreach (var d in viewPponDeal.Take(22))
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, d.ItemName);
                    }
                }

                viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetListByCityDate(PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityId, sendDate).Take(20);
                foreach (var d in viewPponDeal)
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, d.ItemName);
                    }
                }

                #endregion
            }
            else
            {
                if (tmpData.Count > 0 && tmpData.Any(x => x.AdId != null))
                {
                    adId = tmpData.First(x => x.AdId != null).AdId.ToString();
                }
                var detailData = tmpData.Where(x => x.Bid != null);

                if (detailData.Any())
                {
                    foreach (var d in detailData)
                    {
                        if (!PponDictionary.ContainsKey(d.Bid ?? new Guid()))
                        {
                            PponDictionary.Add(d.Bid ?? new Guid(), d.Title);
                        }
                    }
                }

                foreach (var d in viewPponDeal)
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, d.ItemName);
                    }
                }
            }


            Data.Add(PponDictionary, adId);

            return Data;
        }

        private Dictionary<Dictionary<Guid, List<string>>, string> GetEdmHotDealList(int mainId, int cityId, DateTime sendDate)
        {
            Dictionary<Guid, List<string>> PponDictionary = new Dictionary<Guid, List<string>>();
            Dictionary<Guid, List<string>> tmpPponDictionary = new Dictionary<Guid, List<string>>();
            Dictionary<Dictionary<Guid, List<string>>, string> Data = new Dictionary<Dictionary<Guid, List<string>>, string>();
            PponCity city = PponCityGroup.DefaultPponCityGroup.First(x => x.CityId == cityId);

            var edmHotDeal = pp.EdmHotDealByCityIdDate(city.CityId, sendDate.ToString("yyyy/MM/dd"));
            var edmPriorityDeal = pp.EdmPriorityDealByCityIdDate(city.CityId, sendDate.ToString("yyyy/MM/dd"));

            bool supplement = false;
            int hot = 0;
            int priority = 0;
            int fastShip = 0;
            int count = 1;
            var adId = "";

            #region 各頻道筆數
            if (city.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                //旅遊
                hot = 3;      //3筆強打
                priority = 2; //營業毛利訂單各2筆
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                //宅配
                hot = 8;      //8筆強打
                priority = 5; //營業毛利訂單各5筆
                fastShip = 6; //24hr 營業毛利訂單各6筆
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.Piinlife.CityId)
            {
                //品生活
                hot = 2;      //2筆強打
                priority = 2; //營業毛利訂單各2筆
            }
            else
            {
                //在地
                hot = 4;      //4筆強打
                priority = 2; //營業毛利訂單各2筆
            }
            #endregion

            if (edmPriorityDeal.Rows.Count == 0)
            {
                supplement = true;
            }
            else
            {
                DataTable edmRevenueDeal = new DataTable();
                DataTable edmGrossProfitDeal = new DataTable();
                DataTable edmOrderCountDeal = new DataTable();

                if (edmPriorityDeal.Rows.Count > 0)
                {
                    edmRevenueDeal = edmPriorityDeal.Select("type='營業額'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                    edmRevenueDeal.DefaultView.Sort = "num desc";
                    edmRevenueDeal = edmRevenueDeal.DefaultView.ToTable();

                    edmGrossProfitDeal = edmPriorityDeal.Select("type='毛利額'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                    edmGrossProfitDeal.DefaultView.Sort = "num desc";
                    edmGrossProfitDeal = edmGrossProfitDeal.DefaultView.ToTable();

                    edmOrderCountDeal = edmPriorityDeal.Select("type='訂單數'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                    edmOrderCountDeal.DefaultView.Sort = "num desc";
                    edmOrderCountDeal = edmOrderCountDeal.DefaultView.ToTable();
                }

                var tmpData = EmailFacade.GetEdmAreaDetailsByPid(mainId);//已編輯過的EDM


                #region 塞強打、營業額、毛利額、訂單數檔次



                //AD資料
                if (tmpData.Count > 0 && tmpData.Any(x => x.AdId != null))
                {
                    adId = tmpData.First(x => x.AdId != null).AdId.ToString();
                }

                //現有EDM資料
                var detailData = tmpData.Where(x => x.Bid != null);

                if (detailData.Any())
                {
                    foreach (var d in detailData)
                    {
                        if (!PponDictionary.ContainsKey(d.Bid ?? new Guid()))
                        {
                            PponDictionary.Add(d.Bid ?? new Guid(), new List<string>() { d.Title, d.Title });
                        }
                    }
                }

                //強打檔次
                count = 1;
                if (edmHotDeal.Rows.Count > 0)
                {
                    foreach (DataRow row in edmHotDeal.Rows)
                    {
                        if (!PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                        {
                            PponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                            count++;
                        }
                        if (count == hot + 1)
                            break;
                    }
                }


                //營業額
                count = 1;
                if (edmRevenueDeal.Rows.Count > 0)
                {
                    foreach (DataRow row in edmRevenueDeal.Rows)
                    {
                        if (!tmpPponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())) && !PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                        {
                            tmpPponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                            count++;
                        }
                        if (count == priority + 1)
                            break;
                    }
                }


                //毛利額
                count = 1;
                if (edmGrossProfitDeal.Rows.Count > 0)
                {
                    foreach (DataRow row in edmGrossProfitDeal.Rows)
                    {
                        if (!tmpPponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())) && !PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                        {
                            tmpPponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                            count++;
                        }
                        if (count == priority + 1)
                            break;
                    }
                }

                //訂單數
                count = 1;
                if (edmOrderCountDeal.Rows.Count > 0)
                {
                    foreach (DataRow row in edmOrderCountDeal.Rows)
                    {
                        if (!tmpPponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())) && !PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                        {
                            tmpPponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                            count++;
                        }
                        if (count == priority + 1)
                            break;
                    }

                }

                //強打檔次不夠時用其他營業額來補檔
                int additional = hot - edmHotDeal.Rows.Count;
                if (additional > 0)
                {
                    if (edmRevenueDeal.Rows.Count > 0)
                    {
                        for (int i = 0; i < edmRevenueDeal.Rows.Count; i++)
                        {
                            DataRow row = edmRevenueDeal.Rows[i];
                            if (!PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())) && !tmpPponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                            {
                                PponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                                additional--;
                                if (additional == 0)
                                    break;
                            }
                        }
                    }
                }


                foreach (KeyValuePair<Guid, List<string>> item in tmpPponDictionary)
                {
                    PponDictionary.Add(item.Key, item.Value);
                }
                #endregion
            }



            #region 資料不足隨機補檔 
            int all = hot + (3 * priority);
            int notEnough = all - PponDictionary.Count;
            if (all - PponDictionary.Count > 0)
                supplement = true;
            count = 1;

            if (supplement)
            {
                var viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetListByCityDate(city.CityId, sendDate).Take(30);
                foreach (var d in viewPponDeal)
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, new List<string>() { d.ItemName, d.AppTitle });
                        count++;
                    }
                    if (count == notEnough + 1)
                        break;
                }
            }
            #endregion

            #region 宅配+24HR/旅遊+玩美休閒(第二部分)

            if (city.CityId == PponCityGroup.DefaultPponCityGroup.Travel.CityId)
            {
                var viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetListByCityDate(PponCityGroup.DefaultPponCityGroup.PBeautyLocation.CityId, sendDate).Take(20);
                foreach (var d in viewPponDeal)
                {
                    if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                    {
                        PponDictionary.Add(d.BusinessHourGuid, new List<string>() { d.ItemName, d.AppTitle });
                    }
                }
            }
            else if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
            {
                //string notContainBid = "'" + string.Join("','", PponDictionary.Select(x => x.Key)) + "'";
                //var viewPponDeal = EmailFacade.ViewPponDealTimeSlotByFastShip(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId, sendDate, notContainBid).Take(20);

                supplement = false;
                if (edmPriorityDeal.Rows.Count == 0)
                {
                    supplement = true;
                }
                else
                {
                    var edmPriorityDeal24 = pp.EdmPriorityDealByCityIdDate24(city.CityId, sendDate.ToString("yyyy/MM/dd"));
                    DataTable edmRevenueDeal24 = new DataTable();
                    DataTable edmGrossProfitDeal24 = new DataTable();
                    DataTable edmOrderCountDeal24 = new DataTable();

                    if (edmPriorityDeal24.Rows.Count > 0)
                    {
                        edmRevenueDeal24 = edmPriorityDeal24.Select("type='24營業額'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                        edmRevenueDeal24.DefaultView.Sort = "num desc";
                        edmRevenueDeal24 = edmRevenueDeal24.DefaultView.ToTable();

                        edmGrossProfitDeal24 = edmPriorityDeal24.Select("type='24毛利額'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                        edmGrossProfitDeal24.DefaultView.Sort = "num desc";
                        edmGrossProfitDeal24 = edmGrossProfitDeal24.DefaultView.ToTable();

                        edmOrderCountDeal24 = edmPriorityDeal24.Select("type='24訂單數'").CopyToDataTable().DefaultView.ToTable(true, new string[] { "bid", "item_name", "app_title", "num" });
                        edmOrderCountDeal24.DefaultView.Sort = "num desc";
                        edmOrderCountDeal24 = edmOrderCountDeal24.DefaultView.ToTable();
                    }

                    //營業額
                    count = 1;
                    if (edmRevenueDeal24.Rows.Count > 0)
                    {
                        foreach (DataRow row in edmRevenueDeal24.Rows)
                        {
                            if (!PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                            {
                                PponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                                count++;
                            }
                            if (count == fastShip + 1)
                                break;
                        }
                    }


                    //毛利額
                    count = 1;
                    if (edmGrossProfitDeal24.Rows.Count > 0)
                    {
                        foreach (DataRow row in edmGrossProfitDeal24.Rows)
                        {
                            if (!PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                            {
                                PponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                                count++;
                            }
                            if (count == fastShip + 1)
                                break;
                        }
                    }

                    //訂單數
                    count = 1;
                    if (edmOrderCountDeal24.Rows.Count > 0)
                    {
                        foreach (DataRow row in edmOrderCountDeal24.Rows)
                        {
                            if (!PponDictionary.ContainsKey(Guid.Parse(row["bid"].ToString())))
                            {
                                PponDictionary.Add(Guid.Parse(row["bid"].ToString()), new List<string>() { row["item_name"].ToString(), row["app_title"].ToString() });
                                count++;
                            }
                            if (count == fastShip + 1)
                                break;
                        }

                    }
                }


                #region 資料不足隨機補檔 
                all = hot + (3 * priority) + (3 * fastShip);
                notEnough = all - PponDictionary.Count;
                if (all - PponDictionary.Count > 0)
                    supplement = true;
                count = 1;

                if (supplement)
                {
                    var viewPponDeal = EmailFacade.ViewPponDealTimeSlotGetWmsListByCityDate(sendDate).Take(30);
                    foreach (var d in viewPponDeal)
                    {
                        if (!PponDictionary.ContainsKey(d.BusinessHourGuid))
                        {
                            if (city.CityId == PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)
                            {
                                IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(d.BusinessHourGuid);
                                if (theDeal.IsWms)
                                {
                                    PponDictionary.Add(d.BusinessHourGuid, new List<string>() { d.ItemName, d.AppTitle });
                                    count++;
                                }
                            }
                            else
                            {
                                PponDictionary.Add(d.BusinessHourGuid, new List<string>() { d.ItemName, d.AppTitle });
                                count++;
                            }
                        }
                        if (count == notEnough + 1)
                            break;
                    }
                }
                #endregion

            }
            #endregion

            Data.Add(PponDictionary, adId);

            return Data;
        }

        private EdmMainData GetLocationEdmMainData(int mainId, int areaCount, int allCountryAreaCount, int travelAreaCount, int piinlifeAreaCount, int detailCount)
        {
            var edmMainData = new EdmMainData();
            edmMainData.EdmMainId = mainId;
            if (mainId != 0)
            {
                if (detailCount == 0)
                {
                    if (areaCount == 0 || allCountryAreaCount == 0 || travelAreaCount == 0 || piinlifeAreaCount == 0)
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
                    }
                    else
                    {
                        edmMainData.CombinationEdmType = (int)EDMAreaType.Open;
                    }
                }
                else
                {
                    edmMainData.CombinationEdmType = (int)EDMAreaType.Delete;
                }

            }
            else
            {
                edmMainData.CombinationEdmType = (int)EDMAreaType.Close;
            }
            return edmMainData;
        }

        public class EdmMainData
        {
            public int EdmMainId { get; set; }
            public int CombinationEdmType { get; set; }
            public int CityId { get; set; }
            public string CityName { get; set; }
            public string UpdataTime { get; set; }
            public string EidtTime { get; set; }
            public string SysUpdataTime { get; set; }
            public bool Enabled { get; set; }
        }

        #endregion

        #region Solo EDM

        //刪除SoloEDM
        [HttpPost]
        [AjaxCall]
        public ActionResult SoloEdmDelete(int eid)
        {
            var tmp = EmailFacade.GetSoloEdmMainById(eid);
            tmp.Type = (int)SoloEdmType.Close;
            EmailFacade.SaveSoloEdmMain(tmp);
            return Json("OK");
        }

        //測試發送SoloEDM
        [HttpPost]
        [AjaxCall]
        public ActionResult SoloEdmSend(int eid, string testMail)
        {
            string msg = "";
            if (!string.IsNullOrEmpty(testMail) && eid != 0)
            {
                string[] emails = testMail.Replace("\n", ",").Replace("\r", ",").Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
                var tmp = EmailFacade.GetSoloEdmMainById(eid);
                tmp.Type = (int)SoloEdmType.Close;
                msg = EmailFacade.GenerateSoloEdmFile(eid, emails);
            }
            return Json(msg);
        }

        public class EditSoloEdmMainData
        {
            public int EdmMainId { get; set; }
            public string Subject { get; set; }
            public string Cpa { get; set; }
            public string Title { get; set; }
            public string TitleContent { get; set; }
            public DateTime SendDate { get; set; }
            public int Type { get; set; }
            public string Creator { get; set; }
            public DateTime CreateDate { get; set; }
            public DateTime UploadDate { get; set; }
            public string PromoImg { get; set; }
            public string Message { get; set; }

            public string BrandData { get; set; }
            public string PponData { get; set; }
            public bool CheckAllUser { get; set; }
            public bool CloseBrandType { get; set; }
            public bool ClosePponType { get; set; }
            public bool CloseMsgType { get; set; }
            public bool CheckSendUpdata { get; set; }

        }

        public class BrandList
        {
            public int BrandId { get; set; }
            public string BrandName { get; set; }
            public string Url { get; set; }
            public int GroupId { get; set; }
            public string GroupName { get; set; }
            public string ImgUrl { get; set; }
        }

        public class PponList
        {
            public Guid Bid { get; set; }
            public string BlackTitle { get; set; }
            public string OrangeTitle { get; set; }
            public string Url { get; set; }
            public int GroupId { get; set; }
            public string GroupName { get; set; }
            public string ImgUrl { get; set; }
        }

        #endregion

        //取得檔次
        [HttpPost]
        [AjaxCall]
        public ActionResult GetSearchDeal(string bid)
        {
            Guid Bid;
            Dictionary<Guid, string> PponDictionary = new Dictionary<Guid, string>();
            ViewPponDeal deal = new ViewPponDeal();
            if (Guid.TryParse(bid, out Bid))
            {
                deal = EmailFacade.ViewPponDealGetByBusinessHourGuid(Bid);
                deal.AppDealPic = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).First();
                deal.CustomTag =
                    ViewPponDealManager.GetDealDiscountString(
                        Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), deal.ItemPrice,
                        deal.ItemOrigPrice) + "折 / $" + deal.ItemPrice.ToString("f0") + " 搶購";

            }
            return Json(deal);
        }

        //取得策展
        [HttpPost]
        [AjaxCall]
        public ActionResult GetBrand(int id)
        {
            Brand brand = pp.GetBrand(id);
            brand.EventListImage = ImageFacade.GetMediaPath(brand.EventListImage, MediaType.DealPromoImage);
            brand.Url = PromotionFacade.GetCurationTwoLink(brand);
            return Json(brand);
        }

        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models;
using LunchKingSite.WebLib.Models.IDEAS;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class IDEASController : Controller
    {
        private static readonly IPponProvider PponProvider;
        private static readonly ISellerProvider SellerProvider;
        private static readonly IItemProvider ItemProvider;
        private static readonly IOrderProvider OrderProvider;
        private static readonly IMemberProvider MemberProvider;
        private static readonly IEventProvider EventProvider;
        private static readonly IHiDealProvider HiDealProvider;
        private static readonly ISysConfProvider config;


        static IDEASController()
        {
            PponProvider = ProviderFactory.Instance().GetProvider<IPponProvider>();
            SellerProvider = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ItemProvider = ProviderFactory.Instance().GetProvider<IItemProvider>();
            OrderProvider = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            MemberProvider = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            EventProvider = ProviderFactory.Instance().GetProvider<IEventProvider>();
            HiDealProvider = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            config = ProviderFactory.Instance().GetConfig();
        }

        [HttpGet]
        public ActionResult IDEASStationAdd(int? Id)
        {
            if (Id == null)
            {
                ViewData.Model = new IDEASStationData();
                return View();
            }
            else
            {
                var station = ApiLocationDealManager.IDEASStationDataGetList().Where(x => x.Id == Id).FirstOrDefault();
                if (station == null)
                {
                    return this.Content("查無符合條件的資料。");
                }
                return View(station);
            }
        }

        [HttpPost]
        public ActionResult IDEASStationAdd(IDEASStationData data)
        {
            if (this.HttpContext.User.Identity.Name == null)
            {
                return this.Content("尚未登入!");
            }
            string userName = this.HttpContext.User.Identity.Name;

            if (this.ModelState.IsValid)
            {
                PponProvider.SaveIdeasStationItem(new IdeasStation() { 
                    Id = data.Id,
                    Title = data.Title,
                    CouponEventUrl = string.Format("IDEASCoupon{0}", data.Id),
                    CouponDealBroadcastMinutes = (byte)data.CouponDealBroadcastMinutes,
                    DeliveryEventUrl = string.Format("IDEASDelivery{0}", data.Id),
                    DeliveryDealBroadcastMinutes = (byte)data.DeliveryDealBroadcastMinutes,
                    SpecialEventUrl = string.Format("IDEASSpecial{0}", data.Id),
                    SpecialDealBroadcastTimeStartTimeFormat1 = "yyyyMMdd120000",
                    SpecialDealBroadcastTimeEndTimeFormat1 = "yyyyMMdd130000",
                    SpecialDealBroadcastTimeStartTimeFormat2 = "yyyyMMdd180000",
                    SpecialDealBroadcastTimeEndTimeFormat2 = "yyyyMMdd190000",
                    VourcherEventUrl = string.Format("IDEASVourcher{0}", data.Id),
                    VourcherStoreBroadcastMinutes = (byte)data.VourcherStoreBroadcastMinutes,
                    Status = true,
                    Creator = userName,
                    CreateDate = DateTime.Now
                });

                return RedirectToAction("IDEASStationList");
            }

            return View(data);
        }

        [HttpGet]
        public ActionResult IDEASStationList()
        {
            List<IDEASStationData> stationList = ApiLocationDealManager.IDEASStationDataGetList();
            IDEASStationListModel model = new IDEASStationListModel();
            model.DataList = new List<IDEASStationDataModel>();
            foreach (var ideasStationData in stationList)
            {
                IDEASStationDataModel dataModel = new IDEASStationDataModel();
                dataModel.StationId = ideasStationData.Id;
                dataModel.Title = ideasStationData.Title;
                dataModel.CouponEventUrl = ideasStationData.CouponEventUrl;
                dataModel.DeliveryEventUrl = ideasStationData.DeliveryEventUrl;
                dataModel.SpecialEventUrl = ideasStationData.SpecialEventUrl;
                dataModel.VourcherEventUrl = ideasStationData.VourcherEventUrl;

                model.DataList.Add(dataModel);
            }

            return View(model);
        }

        [HttpGet]
        public ActionResult IDEASStationShow(int id)
        {
            List<IDEASStationData> stationList = ApiLocationDealManager.IDEASStationDataGetList();
            List<IDEASStationData> findList = stationList.Where(x => x.Id == id).ToList();
            if (findList.Count == 0)
            {
                return this.Content("查無符合條件的資料。");
            }
            IDEASStationData stationData = findList.First();

            List<string> urlList = new List<string>();
            if (!string.IsNullOrWhiteSpace(stationData.CouponEventUrl))
            {
                urlList.Add(stationData.CouponEventUrl);
            }
            if (!string.IsNullOrWhiteSpace(stationData.DeliveryEventUrl))
            {
                urlList.Add(stationData.DeliveryEventUrl);
            }
            if (!string.IsNullOrWhiteSpace(stationData.SpecialEventUrl))
            {
                urlList.Add(stationData.SpecialEventUrl);
            }

            ViewEventPromoCollection pponPromoItemList = PponProvider.ViewEventPromoGetList(urlList, null);

            IDEASStationDealDataModel model = new IDEASStationDealDataModel();
            model.StationId = stationData.Id;
            model.Title = stationData.Title;

            IDEASStationGroup couponGroup = new IDEASStationGroup();
            couponGroup.GroupEventUrl = stationData.CouponEventUrl;
            couponGroup.GroupTitle = "團購";
            couponGroup.ItemType = EventPromoItemType.Ppon;
            foreach (ViewEventPromo promoItem in pponPromoItemList.Where(x => x.Url == stationData.CouponEventUrl).OrderByDescending(x => x.ItemEndDate))
            {
                couponGroup.ItemList.Add(GetIDEASStationDealItemByViewEventPromo(promoItem));
            }

            IDEASStationGroup deliveryGroup = new IDEASStationGroup();
            deliveryGroup.GroupEventUrl = stationData.DeliveryEventUrl;
            deliveryGroup.GroupTitle = "宅配";
            deliveryGroup.ItemType = EventPromoItemType.Ppon;
            foreach (ViewEventPromo promoItem in pponPromoItemList.Where(x => x.Url == stationData.DeliveryEventUrl).OrderByDescending(x => x.ItemEndDate))
            {
                deliveryGroup.ItemList.Add(GetIDEASStationDealItemByViewEventPromo(promoItem));
            }

            IDEASStationGroup specialGroup = new IDEASStationGroup();
            specialGroup.GroupEventUrl = stationData.SpecialEventUrl;
            specialGroup.GroupTitle = "特選";
            specialGroup.ItemType = EventPromoItemType.Ppon;
            foreach (ViewEventPromo promoItem in pponPromoItemList.Where(x => x.Url == stationData.SpecialEventUrl).OrderByDescending(x => x.ItemEndDate))
            {
                specialGroup.ItemList.Add(GetIDEASStationDealItemByViewEventPromo(promoItem));
            }

            IDEASStationGroup vourcherGroup = new IDEASStationGroup();
            vourcherGroup.GroupEventUrl = stationData.VourcherEventUrl;
            vourcherGroup.GroupTitle = "優惠券";
            vourcherGroup.ItemType = EventPromoItemType.Vourcher;
            ViewEventPromoVourcherCollection vourcherPromoItemList = PponProvider.ViewEventPromoVourcherGetList(stationData.VourcherEventUrl, null);
            foreach (ViewEventPromoVourcher promoItem in vourcherPromoItemList.OrderByDescending(x => x.ItemEndDate))
            {
                vourcherGroup.ItemList.Add(GetIDEASStationVourcherItemByViewEventPromo(promoItem));
            }

            model.Groups.Add(couponGroup);
            model.Groups.Add(deliveryGroup);
            model.Groups.Add(specialGroup);
            model.Groups.Add(vourcherGroup);

            return View(model);
        }

        /// <summary>
        /// 進入商品新增模式
        /// </summary>
        /// <param name="id">電子看板編號</param>
        /// <param name="type">要新增的商品類型EventPromoItemType</param>
        /// <param name="url">EventPromo的URL欄位值</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult IDEASStationDealAdd(int id, EventPromoItemType type, string url)
        {
            EventPromo eventPromo = PponProvider.GetEventPromo(url, EventPromoType.Ppon);
            if (!eventPromo.IsLoaded)
            {
                return this.Content("查無商品主題活動。");
            }
            List<IDEASStationData> stationList = ApiLocationDealManager.IDEASStationDataGetList();
            List<IDEASStationData> findList = stationList.Where(x => x.Id == id).ToList();
            if (findList.Count == 0)
            {
                return this.Content("查無符合條件的資料。");
            }
            IDEASStationData stationData = findList.First();

            //IDEASStationData stationData = this.GetIDEASStationData(id);

            //if (stationData == null)
            //{
            //    return this.Content("查無符合條件的資料。");
            //}
            IDEASStationEventEditModel model = new IDEASStationEventEditModel();
            model.EventId = eventPromo.Id;
            model.StationId = stationData.Id;
            model.ItemType = type;
            model.EventPromoUrl = url;
            model.ItemData = null;//新增模式，設為null
            return View(model);
        }

        [HttpGet]
        public ActionResult IDEASStationDealView(int id, EventPromoItemType type, string url, int promoItemId)
        {
            EventPromo eventPromo = PponProvider.GetEventPromo(url, EventPromoType.Ppon);
            if (!eventPromo.IsLoaded)
            {
                return this.Content("查無商品主題活動。");
            }
            EventPromoItem eventPromoItem = PponProvider.GetEventPromoItem(promoItemId);
            if (!eventPromoItem.IsLoaded)
            {
                return this.Content("查無商品紀錄。");
            }

            List<IDEASStationData> stationList = ApiLocationDealManager.IDEASStationDataGetList();
            List<IDEASStationData> findList = stationList.Where(x => x.Id == id).ToList();
            if (findList.Count == 0)
            {
                return this.Content("查無符合條件的資料。");
            }
            IDEASStationData stationData = findList.First();
            //IDEASStationData stationData = this.GetIDEASStationData(id);

            //if (stationData == null)
            //{
            //    return this.Content("查無符合條件的資料。");
            //}
            IDEASStationEventEditModel model = new IDEASStationEventEditModel();
            model.EventId = eventPromo.Id;
            model.StationId = stationData.Id;
            model.ItemType = type;
            model.EventPromoUrl = url;

            ItemDataModel itemData = GetItemDataModel(eventPromoItem.ItemId,
                                                      (EventPromoItemType)eventPromoItem.ItemType, eventPromo.Id, eventPromoItem);

            model.ItemData = itemData;

            return View("IDEASStationDealAdd", model);
        }
        [HttpPost]
        public JsonResult GetItemData(int itemId, int itemType, int eventId)
        {
            EventPromoItemType type;
            if (!Enum.TryParse(itemType.ToString(), out type) || !Enum.IsDefined(typeof(EventPromoItemType), type))
            {
                //參數錯誤
                return Json(null);
            }

            ItemDataModel model = GetItemDataModel(itemId, type, eventId, null);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteEventItem(int itemId)
        {
            EventPromoItem item = PponProvider.GetEventPromoItem(itemId);
            if (item.IsLoaded)
            {
                PponProvider.DeleteEventPromoItem(item);
                return Json(new { IsSuccess = true, Message = "" });
            }
            else
            {
                return Json(new { IsSuccess = false, Message = "查無資料!" });
            }
        }

        /// <summary>
        /// 修改與新增商品主題活動的商品內容，依據itemId判別，如果此商品已新增過，則修改內容
        /// </summary>
        /// <param name="stationId">此商品所屬看板編號</param>
        /// <param name="eventId">要新增到那個eventPromo</param>
        /// <param name="itemType">商品類型</param>
        /// <param name="itemId">商品識別ID</param>
        /// <param name="seq">排序編號</param>
        /// <param name="itemTitle">商品標題</param>
        /// <param name="itemDescription">商品說明</param>
        /// <param name="itemStartDate">展示起日</param>
        /// <param name="itemEndDate">展示迄日</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult IDEASStationDealEdit(int stationId, int eventId, int itemType, int itemId, int seq, string itemTitle, string itemDescription
            , string itemStartDate, string itemEndDate)
        {

            if (this.HttpContext.User.Identity.Name == null)
            {
                return Json(new { IsSuccess = false, Message = "尚未登入!" });
            }
            string userName = this.HttpContext.User.Identity.Name;

            EventPromoItemType type;
            if (!Enum.TryParse(itemType.ToString(), out type))
            {
                //參數錯誤
                return Json(new { IsSuccess = false, Message = "參數錯誤!" });
            }
            DateTime startDate;
            DateTime endDate;
            if (!DateTime.TryParse(itemStartDate, out startDate) || !DateTime.TryParse(itemEndDate, out endDate))
            {
                return Json(new { IsSuccess = false, Message = "起始與截止時間錯誤!" });
            }
            if (startDate >= endDate)
            {
                return Json(new { IsSuccess = false, Message = "起始時間需小於截止時間!" });
            }

            EventPromoItemCollection items = PponProvider.EventPromoItemGetList(EventPromoItem.Columns.MainId + " = " + eventId,
                EventPromoItem.Columns.ItemId + " = " + itemId,
                EventPromoItem.Columns.ItemType + " = " + itemType);
            EventPromoItem saveItem = new EventPromoItem();

            string message;
            if (!CheckItemId(itemId, type, out message))
            {
                return Json(new { IsSuccess = false, Message = message });
            }

            if (items.Count > 0)
            {
                //有查到資料,為修改狀態
                saveItem = items.First();

                saveItem.Message += ("modified by " + userName + " " + DateTime.Now.ToString("yyyy/MM/dd"));

            }
            else
            {
                saveItem.MainId = eventId;
                saveItem.Creator = userName;
                saveItem.Cdt = DateTime.Now;
                saveItem.ItemId = itemId;
            }
            saveItem.Title = itemTitle;
            saveItem.Description = itemDescription;
            saveItem.Status = true;
            saveItem.Seq = seq;
            saveItem.ItemType = itemType;
            saveItem.ItemStartDate = startDate;
            saveItem.ItemEndDate = endDate;

            PponProvider.SaveEventPromoItem(saveItem);

            return Json(new { IsSuccess = true, Message = "" });
        }
        #region private
        /// <summary>
        /// 檢查輸入資料是否正確，可新增為TRUE，否則為FALSE
        /// </summary>
        /// <param name="itemId">要新增的物件ID</param>
        /// <param name="type">新增的資料類別</param>
        /// <param name="message">回傳訊息</param>
        /// <returns></returns>
        private bool CheckItemId(int itemId, EventPromoItemType type, out string message)
        {
            message = string.Empty;
            switch (type)
            {
                case EventPromoItemType.Ppon:
                    //檢查檔次是否存在
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(itemId);
                    if (!deal.IsLoaded)
                    {
                        message = "查無此團購檔次資料";
                        return false;
                    }
                    //檢查是否是成套販售，多檔次母檔需檢查每個子檔。
                    if ((deal.BusinessHourStatus & (int)BusinessHourStatus.ComboDealMain) > 0)
                    {
                        //為多檔次母檔檢查每個子檔
                        DealPropertyCollection subDeals =
                            PponProvider.DealPropertyGetSubDealIsPackSet(deal.BusinessHourGuid);
                        //有成套販售的檔次，回傳錯誤訊息
                        if (subDeals.Count > 0)
                        {
                            message = "部分子檔次為成套販售，不可提供資策會。";
                            return false;
                        }
                    }
                    else if (deal.ShoppingCart.HasValue && deal.ShoppingCart.Value && deal.ComboPackCount > 0)
                    {
                        //非多檔次，且為購物車，且有設定成套販售
                        message = "此檔次為成套販售，不可提供資策會。";
                        return false;
                    }
                    break;
                case EventPromoItemType.Vourcher:
                    //檢查有無此編號的優惠券資料
                    VourcherEvent vourcherEvent = EventProvider.VourcherEventGetById(itemId);
                    if (!vourcherEvent.IsLoaded)
                    {
                        message = "查無此優惠券資料。";
                        return false;
                    }
                    break;
            }
            return true;
        }

        /// <summary>
        /// 依據ViewEventPromo物件，產出前端畫面顯示用的IDEASStationDealItem物件
        /// </summary>
        /// <param name="promoItem"></param>
        /// <returns></returns>
        private IDEASStationItem GetIDEASStationDealItemByViewEventPromo(ViewEventPromo promoItem)
        {
            IDEASStationItem modelItem = new IDEASStationItem();
            modelItem.EventItemId = promoItem.Id;
            modelItem.Seq = promoItem.Seq;
            modelItem.ItemTitle = promoItem.Title;
            modelItem.StartDate = promoItem.BusinessHourOrderTimeS;
            modelItem.EndDate = promoItem.BusinessHourOrderTimeE;
            modelItem.Status = promoItem.Status;
            modelItem.ItemStartDate = promoItem.ItemStartDate;
            modelItem.ItemEndDate = promoItem.ItemEndDate;
            modelItem.ShowDateStringS = promoItem.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm:ss");
            modelItem.ShowDateStringE = promoItem.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm:ss");
            return modelItem;
        }
        private IDEASStationItem GetIDEASStationVourcherItemByViewEventPromo(ViewEventPromoVourcher promoItem)
        {
            IDEASStationItem modelItem = new IDEASStationItem();
            modelItem.EventItemId = promoItem.EventPromoItemId;
            modelItem.Seq = promoItem.Seq;
            modelItem.ItemTitle = promoItem.Title;
            modelItem.StartDate = promoItem.VourcherStartDate == null ? DateTime.MinValue : promoItem.VourcherStartDate.Value;
            modelItem.EndDate = promoItem.VourcherEndDate == null ? DateTime.MinValue : promoItem.VourcherEndDate.Value;
            modelItem.Status = promoItem.PromoItemStatus;
            modelItem.ItemStartDate = promoItem.ItemStartDate;
            modelItem.ItemEndDate = promoItem.ItemEndDate;
            modelItem.ShowDateStringS = promoItem.VourcherStartDate.HasValue ? promoItem.VourcherStartDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";
            modelItem.ShowDateStringE = promoItem.VourcherEndDate.HasValue ? promoItem.VourcherEndDate.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";
            return modelItem;
        }
        public ItemDataModel GetItemDataModel(int itemId, EventPromoItemType type, int eventId, EventPromoItem item)
        {
            //資料檢查
            if ((item != null) && (item.ItemId != itemId || item.ItemType != (int)type))
            {
                //有傳入item可是資料與前面傳遞的參數對應不起來，直接回傳null
                return null;
            }

            ItemDataModel model = new ItemDataModel();
            DateTime start = new DateTime();
            DateTime? end = new DateTime();
            string itemTitle = string.Empty;
            string itemDescription = string.Empty;

            switch ((EventPromoItemType)type)
            {
                case EventPromoItemType.Ppon:
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByUniqueId(itemId);
                    if (deal != null && deal.IsLoaded)
                    {
                        itemTitle = deal.ItemName;
                        itemDescription = deal.EventTitle;
                        start = deal.BusinessHourOrderTimeS;
                        end = deal.BusinessHourOrderTimeE;
                    }
                    break;
                case EventPromoItemType.Vourcher:
                    VourcherEvent vourcher = EventProvider.VourcherEventGetById(itemId);
                    if (vourcher.IsLoaded && vourcher.StartDate.HasValue)
                    {
                        Seller seller = SellerProvider.SellerGet(vourcher.SellerGuid);
                        itemTitle = seller.SellerName;
                        itemDescription = vourcher.Contents;
                        start = vourcher.StartDate.Value;
                        end = vourcher.EndDate;
                    }
                    break;
                default:
                    break;
            }
            if (string.IsNullOrWhiteSpace(itemTitle) && string.IsNullOrWhiteSpace(itemDescription))
            {
                //查無資料，回傳NULL
                return null;
            }

            if (item != null)
            {
                //有傳入item，相關資料已item的內容為準
                model.ItemId = item.ItemId;
                model.ItemTitle = item.Title;
                model.ItemDescription = item.Description;
                model.Seq = item.Seq;
                model.ItemStartDate = item.ItemStartDate;
                model.ItemEndDate = item.ItemEndDate;
            }
            else
            {
                model.ItemId = itemId;
                model.ItemTitle = itemTitle;
                model.ItemDescription = itemDescription;
                model.Seq = PponProvider.EventPromoItemGetListCount(eventId, type) + 1;
            }

            model.ShowDateStringS = start.ToString("yyyy/MM/dd HH:mm:ss");
            model.ShowDateStringE = end.HasValue ? end.Value.ToString("yyyy/MM/dd HH:mm:ss") : "";

            model.DealStatusDesc = start <= DateTime.Now ? "已開賣" : ("未開賣 (開賣時間:" + start.ToString("yyyy/MM/dd HH:mm:ss") + ")");

            return model;
        }

        #endregion private
    }
}

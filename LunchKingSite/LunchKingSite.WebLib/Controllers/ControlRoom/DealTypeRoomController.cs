﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    /// <summary>
    /// 後臺銷售分類新增/維護
    /// </summary>
    public class DealTypeRoomController : ControllerExt
    {
        static ISerializer serialize= ProviderFactory.Instance().GetSerializer();
        static ILog logger = LogManager.GetLogger(typeof(DealTypeRoomController));
        [HttpGet]
        public ActionResult Index()
        {
            List<DealTypeNode> categories = DealTypeFacade.GetDealTypeNodes();
            ViewBag.categoryData = serialize.Serialize(categories, false, new JsonIgnoreSetting(typeof(DealTypeNode), "Parent"));

            return View();
        }

        /// <summary>
        /// 以Excel格式(xls)，下載目前的分類資料
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DownloadExcel()
        {
            var categories = DealTypeFacade.GetDealTypeNodes();
            MemoryStream ms = new MemoryStream();
            DealTypeFacade.ExportExcelToMemoryStream(ms, categories);

            string fileDownname = string.Format("產品分類-{0}.xls", DateTime.Now.ToString("yyyyMMdd"));
            return File(ms.ToArray(), "application/vnd.ms-excel", fileDownname);
        }

        [HttpPost]
        public ActionResult UploadExcelToUpdate()
        {
            List<ProductCategoryModifiedLog> logs = new List<ProductCategoryModifiedLog>();
            List<string> messages = new List<string>();
            string token = Guid.NewGuid().ToString();
            try
            {
                if (Request.Files.Count == 0)
                {
                    throw new Exception("找不到上傳檔案");
                }
                var categories = DealTypeFacade.GetDealTypeNodes();
                var uploadFile = Request.Files[0];
                List<DealTypeNode> newCategories =
                    DealTypeFacade.GetDealTypeNodesFromExcel(uploadFile.InputStream);
                
                Session[token] = newCategories;
                DealTypeFacade.GetConfirmedLog(newCategories, categories, logs, out messages);
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
            }

            return Json(new {
                Token = token,
                Data = logs,
                Messages = messages
            }, "application/json", Encoding.UTF8, new JsonIgnoreSetting(typeof(DealTypeNode), "Children" ));
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult SaveChanges(string token)
        {
            List<ProductCategoryModifiedLog> logs = new List<ProductCategoryModifiedLog>();
            try
            {
                //string token = data["Token"] == null ? string.Empty : data["Token"].ToString();
                List<DealTypeNode> newCategories = Session[token] as List<DealTypeNode>;
                Session.Remove(token);

                if (newCategories == null)
                {
                    throw new Exception("資料己過期，請重新操作，如果仍有問題請連絡IT");
                }
                var categories = DealTypeFacade.GetDealTypeNodes();
                List<string> messages;
                DealTypeFacade.GetConfirmedLog(newCategories, categories, logs, out messages);
                DateTime now = DateTime.Now;
                string createId = User.Identity.Name;
                int rowCount = DealTypeFacade.SaveDealTypeNodes(
                    logs.Where(t => t.Action == ModifiedStatusType.IsNew || t.Action == ModifiedStatusType.Modified
                    ).Select(t => t.Target).ToList(), createId, now);


                return Json(new
                {
                    Success = 1,
                    RowCount = rowCount
                });
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                return Json(new
                {
                    Success = 0,
                    Message = ex.Message
                });
            }            
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult SaveDirectly(string id, string title, string googleCode, bool enabled, 
            string childLines)
        {
            try
            {
                string message;
                DateTime now = DateTime.Now;
                string createId = User.Identity.Name;
                string[] childNodeLines = null;
                if (string.IsNullOrEmpty(childLines)==false)
                {
                    childNodeLines = childLines.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
                }
                int rowCount = DealTypeFacade.SaveDirectly(id, title, googleCode, enabled,
                    childNodeLines, createId, now);

                var categories = DealTypeFacade.GetDealTypeNodes();
                DealTypeNode node = DealTypeFacade.Find(categories, int.Parse(id));

                return Json(new
                {
                    Success = 1,
                    RowCount = rowCount,
                    NodeJson = serialize.Serialize(node, false, new JsonIgnoreSetting(typeof(DealTypeNode), "Parent"))
                });
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                return Json(new
                {
                    Success = 0,
                    Message = ex.Message
                });
            }            
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetGoogleProductCategoryCodes()
        {
            var googleCodes = DealTypeFacade.GetAllGoogleCodes();
            return Json(new
            {
                Success = 1,
                Data = serialize.Serialize(googleCodes, false)
            });
        }
    }
}

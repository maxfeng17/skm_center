﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Models;
using log4net;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class PponSetupController : ControllerExt
    {
        private readonly ILog logger = log4net.LogManager.GetLogger(typeof(PponSetupController));
        private IPponProvider _pponProv;
        private ISellerProvider _sellerProv;
        private ISysConfProvider _config;
        private IMemberProvider _memProv;

        public PponSetupController()
        {
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _memProv = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        }

        #region PartialView
        /// <summary>
        /// 回傳「發送文字稿(COD)」預覽頁面的View
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="isMultiDeal">是否為多檔次</param>
        /// <returns></returns>
        public ActionResult SetupPageConfirmPreview(string bid, bool isMultiDeal)
        {
            List<ViewPponDeal> model = new List<ViewPponDeal>();
            
            #region 取得頁面資訊
            Guid bGuid = new Guid(bid);
            ViewProposalSeller vps = _sellerProv.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, bGuid);

            if (isMultiDeal)
            {
                var list = _pponProv.ViewPponDealListGetByMainbid(bGuid).OrderBy(x => x.ComboDealSeq);
                model.AddRange(list.ToList());
            }
            else
            {
                var vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bGuid);
                model.Add(vpd);
            }
            #endregion

            ViewBag.IsMultiDeal = isMultiDeal;
            ViewBag.ViewProposalSeller = vps;
            ViewBag.IsPreviewInterface = true;//true:預覽畫面，需要輸出按鈕與js/false:Email內容，不需要輸出按鈕與js
            ViewBag.ProposalLink = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", _config.SiteUrl, vps.Id);

            return PartialView("_PponSetupPageConfirmPreview", model);
        }
        #endregion

        #region 發送文字稿(COD)
        public JsonResult SendSetupPageConfirmMail(string bid, bool isMultiDeal)
        {
            ProposalFacade.ProposalPerformanceLogSet("SendSetupPageConfirmMail", "SendSetupPageConfirmMail", "sys");

            var result = new AjaxResult();
            bool success = false;
            int proposalId = 0;
            var empName = string.Empty;

            try
            {
                List<ViewPponDeal> model = new List<ViewPponDeal>();

                #region 取得頁面資訊
                Guid bGuid = new Guid(bid);
                ViewProposalSeller vps = _sellerProv.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, bGuid);
                proposalId = vps.Id;
                empName = vps.EmpName;

                ViewBag.IsMultiDeal = isMultiDeal;
                ViewBag.ViewProposalSeller = vps;
                ViewBag.ProposalLink = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", _config.SiteUrl, vps.Id);

                if (isMultiDeal)
                {
                    var list = _pponProv.ViewPponDealListGetByMainbid(bGuid).OrderBy(x => x.ComboDealSeq);
                    model.AddRange(list.ToList());
                }
                else
                {
                    var vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bGuid);
                    model.Add(vpd);
                }
                #endregion

                #region 取得Mail內容、相關欄位
                string sender = _config.SystemEmail;
                string subject = string.Format("【文字稿確認通知】＜{0}＞{1}上檔-{2} 單號{3}", empName, string.Format("{0:MM/dd}", model[0].BusinessHourOrderTimeS), vps.BrandName, vps.Id);//0:業務姓名/1:上檔日/2:品牌名稱/3:單號
                string mailContent = RenderRazorViewToString("_PponSetupPageConfirmPreview", model);
                #endregion

                success = EmailFacade.SendSetupPageConfirmMail(sender, subject, mailContent, vps.DevelopeDeptId, vps.OperationDeptId);
                string message = success ? "發送成功" : "發送失敗，請聯繫管理員!";
                result = new AjaxResult(success, message);
            }
            catch (Exception ex)
            {
                logger.Info("SendSetupPageConfirmMail failed\nException:\n{0}", ex);
                result = new AjaxResult(false, "發送失敗，請聯繫管理員!\n" + ex.Message, ex.StackTrace);
            }

            #region Log
            if (!success)
            {
                logger.Info("SendSetupPageConfirmMail failed!\nSomething happened while sending mail.");
            }
            ProposalFacade.ProposalLog(proposalId, string.Format("發送文字稿(COD){0}", success ? "成功" : "失敗"), User.Identity.Name, ProposalLogType.Initial);
            #endregion

            return Json(result);
        }

        //文字稿預覽+寄信
        public JsonResult SendSetupPageMail(string bid, bool isMultiDeal)
        {
            var result = new AjaxResult();
            bool success = false;
            int proposalId = 0;
            string itemName = string.Empty;
            bool isToShop = false;

            try
            {
                List<ViewPponDeal> model = new List<ViewPponDeal>();

                #region 取得頁面資訊
                Guid bGuid = new Guid(bid);
                ViewProposalSeller vps = _sellerProv.ViewProposalSellerGet(ViewProposalSeller.Columns.BusinessHourGuid, bGuid);
                proposalId = vps.Id;
                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vps.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(vps.OperationSalesId ?? 0);

                ViewBag.IsMultiDeal = isMultiDeal;
                ViewBag.ViewProposalSeller = vps;
                ViewBag.ProposalLink = string.Format("{0}/sal/ProposalContent.aspx?pid={1}", _config.SiteUrl, vps.Id);

                if (isMultiDeal)
                {
                    var list = _pponProv.ViewPponDealListGetByMainbid(bGuid).OrderBy(x => x.ComboDealSeq);
                    model.AddRange(list.ToList());
                    
                    //多檔次取主檔資料
                    Item item = _pponProv.ItemGetBySubDealBid(bGuid);
                    if (item.IsLoaded)
                    {
                        itemName = item.ItemName;
                    }
                }
                else
                {
                    var vpd = _pponProv.ViewPponDealGetByBusinessHourGuid(bGuid);
                    itemName = vpd.ItemName;
                    model.Add(vpd);
                }

                //檢查是不是憑證檔
                if (vps.DeliveryType == (int)DeliveryType.ToShop)
                {
                    isToShop = true;
                }

                #endregion

                #region 取得Mail內容、相關欄位
                string subject = string.Empty;
                string sender = _config.SystemEmail;
                string content = string.Empty;
                string footerContent = string.Empty;
                string mailContent = string.Empty;
                string preWorkDay = string.Empty;
                string RemittanceType = string.Empty;

                #region 檢查preWorkDay是否為假日，是的話再往前延一天                
                int minDay = -1;
                int preDay = (int)model[0].BusinessHourOrderTimeS.AddDays(minDay).DayOfWeek;
                DateTime preWork = new DateTime();
                if (preDay == 0)
                {
                    minDay = minDay - 2;
                }
                else if (preDay == 6)
                {
                    minDay = minDay - 1;
                }
                preWork= model[0].BusinessHourOrderTimeS.AddDays(minDay);

                //檢查preWorkDay是否有在放假條件內
                while (_pponProv.GetVacationByDate(preWork).IsLoaded)
                {
                    preWork = preWork.AddDays(-1);
                }

                preWorkDay = string.Format("{0:yyyy\\/MM\\/dd}", preWork);
                #endregion

                //出帳方式
                DealAccounting dAccounting = _pponProv.DealAccountingGet(model[0].BusinessHourGuid);
                if (dAccounting.IsLoaded)
                {
                    RemittanceType = WayOut((RemittanceType)dAccounting.RemittanceType);
                }

                if (vps.DeliveryType == (int)DeliveryType.ToShop)
                {
                    List<LunchKingSite.BizLogic.Facade.SellerAccountModel> sellerModel = PponFacade.GetSellerAccount(model[0].BusinessHourGuid);

                    content = string.Format("您好：<br />此次合作方案將於<font color='red'><b>{0}</b></font>上檔，我們希望於<font color='red'><b>{1} 14:00</b></font>前，向您完成確認工作，為了讓您在17Life的這檔活動達到最好效果，我們的創意專員已經依照17Life與貴公司簽訂的合約，擬定出標題、價格、權益說明如下表。<br />", string.Format("{0:yyyy\\/MM\\/dd HH:mm}", model[0].BusinessHourOrderTimeS), preWorkDay);
                    subject = string.Format("【17Life文字稿確認通知】{0}上檔【{1}】 ", string.Format("{0:yyyy\\/MM\\/dd}", model[0].BusinessHourOrderTimeS), itemName);//0:上檔日/1:母檔訂單短標
                    footerContent = "隨信附上核銷的網址、帳號、系統使用說明手冊及核銷紀錄表格，若臨時出現無法核銷、系統當機...等無法立即解決的問題，<br />";
                    footerContent = footerContent + "請務必紀錄客人的憑證編號、確認碼、姓名、聯絡電話等，以利後續查詢。<br /><br />";
                    footerContent = footerContent + "商家系統網址：<a href='" + _config.SSLSiteUrl + "/vbs' target='_blank'>" + _config.SSLSiteUrl + "/vbs</a><br /><br />";
                    footerContent = footerContent + "本次合作檔次可使用的商家帳號詳列如下表。<br />";
                    footerContent = footerContent + "預設密碼為123456，僅限首次合作店家使用，若原已有變更密碼者，請繼續沿用舊密碼<br />";
                    footerContent = footerContent + "<table style='border-collapse:collapse;'><tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>商家名稱</td><td style='width:150px;border:1px solid black;'>商家預設帳號</td></tr>";
                    foreach (var item in sellerModel)
                    {
                        footerContent = footerContent + "<tr><td style='border:1px solid black; text-align: right; width:250px; padding-right:15px;'>" + item.sellerName + "</td><td style='width:150px;border:1px solid black;'>" + item.accountId + "</td></tr>";
                    }
                    footerContent = footerContent + "</table>";
                    footerContent = footerContent + "提醒您！請務必即時核銷，兌換逾期/已退貨恕無法核銷，若造成損失由店家承擔！<br />";
                    footerContent=footerContent+"出帳方式："+ RemittanceType + "，並於對帳單產出後將發票開立寄回以免影響撥款<br />";
                    footerContent = footerContent + "若資訊有任何問題，請來信聯繫業務/業助窗口，未反應則視為確認完成，感謝您的配合，預祝 銷售長紅！";
                    mailContent = content + RenderRazorViewToString("_PponSetupPageConfirmPreview", model)+ footerContent;
                }
                else
                {
                    content = string.Format("您好：<br />此次合作方案將於<font color='red'><b>{0}</b></font>上檔，我們希望於<font color='red'><b>{1} 14:00</b></font>前，向您完成確認工作，17Life與貴公司簽定的合約，擬定出標題、價格、權益說明如下表：<br />", string.Format("{0:yyyy\\/MM\\/dd HH:mm}", model[0].BusinessHourOrderTimeS), preWorkDay);
                    subject = string.Format("【17Life合作上檔通知】{0}上檔-{1}-{2}", string.Format("{0:yyyy\\/MM\\/dd}", model[0].BusinessHourOrderTimeS), vps.BrandName, vps.Id);//0:上檔日/1:品牌名稱/2:單號
                    footerContent = string.Format("<br />若有任何問題請於<font color='red'><b>{0} 14:00</b></font>前來信聯繫業務/業助窗口，未反應則視為確認完成，感謝您的配合，<br />預祝 銷售長紅！！<br />", preWorkDay);
                    footerContent = footerContent + "提醒您！檔次上檔販售後請留意每日訂單相關資訊，並協助『準時出貨』。<br />商家系統網址：<a href='" + _config.SSLSiteUrl + "/vbs' target='_blank'>" + _config.SSLSiteUrl + "/vbs</a>";
                    mailContent = content + RenderRazorViewToString("_PponSetupPageConfirmPreview", model)+footerContent;
                }
                #endregion

                success = EmailFacade.SendSetupPageMail(sender, subject, mailContent, vps, isToShop, deSalesEmp, opSalesEmp);
                string message = success ? "發送成功" : "發送失敗，請聯繫管理員!";
                result = new AjaxResult(success, message);
            }
            catch (Exception ex)
            {
                logger.Info("SendSetupPageMail failed\nException:\n{0}", ex);
                result = new AjaxResult(false, "發送失敗，請聯繫管理員!\n" + ex.Message, ex.StackTrace);
            }

            #region Log
            if (!success)
            {
                logger.Info("SendSetupPageMail failed!\nSomething happened while sending mail.");
            }
            ProposalFacade.ProposalLog(proposalId, string.Format("發送文字稿(COD){0}", success ? "成功" : "失敗"), User.Identity.Name, ProposalLogType.Initial);
            #endregion

            return Json(result);
        }
        /// <summary>
        /// 將Razor的View轉成字串
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        private string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        private string WayOut(RemittanceType type)
        {
            string result = string.Empty;
            switch (type)
            {
                case RemittanceType.Others:
                    result = "其他付款方式";
                    break;
                case RemittanceType.AchWeekly:
                    result = "ACH每週出帳";
                    break;
                case RemittanceType.AchMonthly:
                    result = "ACH每月出帳";
                    break;
                case RemittanceType.ManualPartially:
                    result = "商品(暫付70%)";
                    break;
                case RemittanceType.ManualWeekly:
                    result = "人工每週出帳";
                    break;
                case RemittanceType.ManualMonthly:
                    result = "人工每月出帳";
                    break;
                case RemittanceType.Flexible:
                    result = "彈性選擇出帳";
                    break;
                case RemittanceType.Weekly:
                    result = "新每週出帳";
                    break;
                case RemittanceType.Monthly:
                    result = "新每月出帳";
                    break;
                default:
                    result = "其他付款方式";
                    break;
            }

            return result;
        }

        #endregion

    }
}

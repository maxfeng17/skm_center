﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.SellerModels;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.ControlRoom.Seller;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using SellerTree = LunchKingSite.DataOrm.SellerTree;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    public class SellerController : BaseController
    {
        private const string _ERROR_MESSAGE = "ErrorMessage";
        private ISellerProvider sp;
        private IPponProvider pp;
        private IHiDealProvider hdp;
        private ISerializer json;
        public SellerController()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            hdp = ProviderFactory.Instance().GetProvider<IHiDealProvider>();
            json = ProviderFactory.Instance().GetSerializer();
        }

        #region Action

        public ActionResult SellerList(
            [Bind(Prefix = "item")] string searchItem,
            [Bind(Prefix = "search")] string searchValue, int page = 1,
            [Bind(Prefix = "lk")] bool showLKSeller = false)
        {
            ViewBag.SellerDeals = SellerFacade.GetViewSellerDealList(searchItem, searchValue, showLKSeller, page);
            return View();
        }

        public ActionResult NewSeller()
        {
            if (TempData[_ERROR_MESSAGE] != null)
            {
                ViewBag.Message = TempData[_ERROR_MESSAGE];
            }
            return View();
        }

        [HttpPost]
        public ActionResult NewSeller(string sellerName, DepartmentTypes department)
        {
            Seller seller = new Seller();
            seller.Guid = Helper.GetNewGuid(seller);
            seller.SellerId = SellerFacade.GetNewSellerID();
            seller.SellerName = sellerName;
            seller.CreateId = User.Identity.Name;
            seller.CreateTime = DateTime.Now;
            seller.CityId = CityManager.TownShipGetByName("中正區").Id; //就只是一個預設值
            seller.Department = (int)department;

            ModelChecker checker = new ModelChecker();
            if (checker.Check(ActionFlags.New, seller).IsValid)
            {
                sp.SellerSet(seller);
                return Redirect(string.Format("~/controlroom/Seller/seller_add.aspx?sid={0}&thin=1", seller.Guid));
            }
            else
            {
                TempData[_ERROR_MESSAGE] = "新增賣家失敗";
            }
            return RedirectToAction("NewSeller");
        }

        public ActionResult EditSeller([Bind(Prefix = "sid")] Guid sellerGuid)
        {
            if (TempData[_ERROR_MESSAGE] != null)
            {
                ViewBag.Message = TempData[_ERROR_MESSAGE];
            }

            Seller seller = sp.SellerGet(sellerGuid);
            if (seller.IsLoaded)
            {
                ViewBag.Seller = seller;

                // prepare photos
                List<ImageItem> imgeUrls = new List<ImageItem>();
                string[] paths = ImageFacade.GetMediaPathsFromRawData(seller.SellerLogoimgPath, MediaType.SellerPhotoLarge);
                foreach (string s in paths)
                {
                    imgeUrls.Add(new ImageItem
                    {
                        Url = s,
                        Title = Path.GetFileName(s)
                    });
                }
                ViewBag.ImageUrls = imgeUrls;
                ViewBag.OriginalPicsToJson = json.Serialize(imgeUrls);

            }
            return View();
        }

        [HttpPost]
        public ActionResult EditSeller(
            [Bind(Prefix = "sid")] Guid sellerGuid,
            EditSellerModel model)
        {
            Seller seller = sp.SellerGet(sellerGuid);
            if (seller.IsLoaded)
            {
                seller.SellerName = model.SellerName;
                seller.IsCloseDown = model.IsCloseDown;
                if (model.IsCloseDown)
                {
                    seller.CloseDownDate = model.CloseDownDate;
                }
                else
                {
                    seller.CloseDownDate = null;
                }

                sp.SellerSet(seller);
            }
            return RedirectToAction("SellerList", new
            {
                item = "seller_name",
                search = seller.SellerName
            });
        }

        [HttpPost]
        public ActionResult EditSellerPics([Bind(Prefix = "sid")] Guid sellerGuid, SellerPicsModel model)
        {
            Seller seller = sp.SellerGet(sellerGuid);
            Func<ImageItem[], string> GetRawData = delegate(ImageItem[] imgItems)
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < imgItems.Length; i++)
                {
                    if (i > 0)
                    {
                        sb.Append('|');
                    }
                    sb.Append(seller.SellerId);
                    sb.Append(',');
                    sb.Append(imgItems[i].Title);
                }
                return sb.ToString();
            };


            if (seller.IsLoaded)
            {
                string rawData = GetRawData(model.OriginalPics);
                foreach (HttpPostedFileBase pFile in model.PhotoUpload)
                {
                    if (pFile == null)
                    {
                        break;
                    }
                    string destFileName = "logo-" + DateTime.Now.ToString("yyMMddHHmmss");
                    if (pFile.ContentType == "image/png" || pFile.ContentType == "image/jpeg" || pFile.ContentType == "image/gif")
                    {

                        ImageUtility.UploadFile(pFile.ToAdapter(), UploadFileType.DelicaciesSellerPhoto,
                            seller.SellerId, destFileName);

                        if (!string.IsNullOrEmpty(rawData))
                        {
                            rawData = Helper.GenerateRawDataFromRawPaths(rawData,
                                ImageFacade.GenerateMediaPath(seller.SellerId, destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType)));
                        }
                        else
                        {
                            rawData = ImageFacade.GenerateMediaPath(seller.SellerId,
                                destFileName + "." + Helper.GetExtensionByContentType(pFile.ContentType));
                        }
                    }
                }
                if (seller.SellerLogoimgPath != rawData)
                {
                    seller.SellerLogoimgPath = rawData;
                    sp.SellerSet(seller);
                }
            }
            return RedirectToAction("SellerList", new
            {
                item = "seller_name",
                search = seller.SellerName
            });
        }

        public ActionResult EditStore(Guid? sellerGuid, Guid? storeGuid)
        {
            if (sellerGuid == null && storeGuid == null)
            {
                throw new ArgumentNullException();
            }
            Store store;
            if (storeGuid.HasValue)
            {
                store = sp.StoreGet(storeGuid.Value);
            }
            else
            {
                store = new Store();
                store.SellerGuid = sellerGuid.Value;
            }

            SellerStoreModel model = new SellerStoreModel(sp.SellerGet(store.SellerGuid).SellerName, store);
            return View(model);
        }

        [HttpPost]
        public ActionResult EditStore(SellerStoreModel model)
        {
            Store store;
            if (model.StoreGuid == Guid.Empty)
            {
                store = new Store();
                model.CopyTo(store, isNew: true);
            }
            else
            {
                store = sp.StoreGet(model.StoreGuid);
                SellerFacade.AuditStoreSet(model, store);
                model.CopyTo(store, isNew: false);

            }


            sp.StoreSet(store);

            return RedirectToAction("EditSeller", new { sid = store.SellerGuid, tab = 1 });
        }

        [ChildActionOnly]
        public ActionResult SellerDeals(Guid sellerGuid)
        {
            ViewPponDealCollection vpc = pp.ViewPponDealGetListPaging(1, 15,
                ViewPponDeal.Columns.BusinessHourOrderTimeS + " desc", ViewPponDeal.Columns.SellerGuid + " = " + sellerGuid,
                ViewPponDeal.Columns.UniqueId + " is not null");
            return View(vpc);
        }

        [ChildActionOnly]
        public ActionResult SellerHiDeals(Guid sellerGuid)
        {
            ViewSellerHiDealCollection deals = hdp.ViewSellerHiDealGetList(sellerGuid);
            return View(deals);
        }

        [ChildActionOnly]
        public ActionResult SellerStores(Guid sellerGuid)
        {
            StoreCollection stores = sp.StoreGetListBySellerGuid(sellerGuid);
            return View(stores);
        }

        [ChildActionOnly]
        public ActionResult AuditBoard(string referenceId)
        {
            ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            AuditCollection auditCol = cp.AuditGetList(referenceId, true);
            return View(auditCol);
        }

        [ChildActionOnly]
        public ActionResult ChangeLog(string referenceType, string referenceId)
        {
            IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            ChangeLogCollection changeCol = pp.ChangeLogCollectionGetAll(referenceType, referenceId);
            return View(changeCol);
        }

        [HttpPost]
        public ActionResult CalculateCoordinates(int townshipId, string address)
        {
            ApiCoordinates coordinate = LocationFacade.GetApiCoordinates(townshipId, address);
            return Json(coordinate);
        }

        public ActionResult TransferStoreToSeller(SearchSellerModel model)
        {
            if (string.IsNullOrEmpty(model.SearchOption))
            {
                return View(model);
            }
            
            var sellers = new SellerCollection();

            if(!string.IsNullOrEmpty(model.SearchOption) &&
                !string.IsNullOrEmpty(model.SearchExpression))
            {
                switch(model.SearchOption)
                {
                    case "SellerName":
                        sellers = sp.SellerGetListByLikelyName(model.SearchExpression);
                        break;
                    case "CompanyId":
                        sellers = sp.SellerGetList(Seller.Columns.CompanyID, model.SearchExpression);
                        break;
                }
            }

            if (sellers.Any())
            {
                var sellerStores = sp.StoreGetListBySellerGuidList(sellers.Select(x => x.Guid).ToList())
                                    .Where(x => x.Status == (int)StoreStatus.Available)
                                    .GroupBy(x => x.SellerGuid)
                                    .ToDictionary(x => x.Key, x => x.Select(s => s.Guid).ToList());
                var storeGuids = new List<Guid>();
                foreach (var store in sellerStores.Values)
                {
                    storeGuids.AddRange(store);
                }
                var tsellerGuids = sp.SellerGetList(storeGuids)
                                    .Select(x => x.Guid)
                                    .ToList();
                model.SearchResult = sellers.Select(x => new SellerStoreInfo
                {
                    SellerGuid = x.Guid,
                    SellerName = string.Format("{0} {1}", x.SellerId, x.SellerName),
                    IsTransfered = sellerStores.ContainsKey(x.Guid)
                                    ? tsellerGuids.Any() && tsellerGuids.Count(s => sellerStores[x.Guid].Contains(s)) == sellerStores[x.Guid].Count
                                    : (bool?)null
                }
                );
            }

            model.Searched = true;

            return View(model);
        }

        [HttpPost]
        public JsonResult TransferStores(TransferModel model)
        {
            var message = new StringBuilder(); 
            var sellerGuidList = !string.IsNullOrEmpty(model.SellerGuids)
                            ? model.SellerGuids.Split(',').Select(Guid.Parse).ToList()
                            : new List<Guid>();
            if (model.StoreGetByPponDeal)
            {
                var sellerStores = sp.StoreGetListByPponDeal()
                    .GroupBy(x => x.SellerGuid)
                    .ToDictionary(x => x.Key, x => x.ToList());
                foreach (var sellerStore in sellerStores)
                {
                    var oSeller = sp.SellerGet(sellerStore.Key);
                    message.Append(StoreTransferToSeller(oSeller, sellerStores[sellerStore.Key]));
                }
            }
            else if (sellerGuidList.Any())
            {
                var sellerStores = sp.StoreGetListBySellerGuidList(sellerGuidList)
                                        .Where(x => x.Status == (int)StoreStatus.Cancel)
                                        .GroupBy(x => x.SellerGuid)
                                        .ToDictionary(x => x.Key, x => x.ToList());
                foreach (var sellerGuid in sellerGuidList)
                {
                    if (!sellerStores.ContainsKey(sellerGuid) || !sellerStores[sellerGuid].Any())
                    {
                        continue;
                    }

                    var oSeller = sp.SellerGet(sellerGuid);
                    message.Append(StoreTransferToSeller(oSeller, sellerStores[sellerGuid]));
                }
            }

            return Json(new { Result = message.Length == 0, Message = message.ToString() });
        }

        [HttpPost]
        public JsonResult DealMoveSeller(Guid dealGuid, string sellerId)
        {
            var newSeller = sp.SellerGetList(Seller.Columns.SellerId, sellerId);
            if (!newSeller.Any())
            {
                return Json(new { Result = false, Message = "查無此賣家編號，請確認是否輸入有誤！" });
            }

            if (newSeller.Count > 1)
            {
                return Json(new { Result = false, Message = "賣家編號重複，請聯繫相關人員協助進行確認處理！" });
            }

            var modifySellerGuid = newSeller.First().Guid;
            var bids = new List<Guid> { dealGuid };
            var subDeals = pp.ViewPponDealGetListByMainBid(dealGuid);
            //多檔次
            if (subDeals.Any())
            { 
                bids.AddRange(subDeals.Select(x => x.BusinessHourGuid));
            }
            //搬移賣家處理
            var message = ProcessMoveSeller(bids, modifySellerGuid);

            return Json(new { Result = message.Length == 0, Message = message.ToString() });
        }

        #endregion Action

        #region Method

        private StringBuilder StoreTransferToSeller(Seller oSeller, IEnumerable<Store> stores)
        {
            var message = new StringBuilder();
            var now = DateTime.Now;
            foreach (var oStore in stores)
            {
                var options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.ReadCommitted;
                using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    #region insert into Seller

                    var store = sp.SellerGet(oStore.Guid);
                    if (!store.IsLoaded)
                    {
                        var newSeller = oSeller.Clone();
                        newSeller.Guid = oStore.Guid;
                        newSeller.SellerName = oStore.StoreName;
                        newSeller.SellerId = SellerFacade.GetNewSellerID();
                        newSeller.StoreAddress = oStore.AddressString;
                        newSeller.StoreCityId = oStore.CityId;
                        newSeller.StoreRemark = oStore.Remarks;
                        newSeller.StoreStatus = oStore.Status;
                        newSeller.StoreTel = oStore.Phone;
                        newSeller.StoreTownshipId = oStore.TownshipId;
                        newSeller.OpenTime = oStore.OpenTime;
                        newSeller.CloseDate = oStore.CloseDate;
                        newSeller.Mrt = oStore.Mrt;
                        newSeller.Car = oStore.Car;
                        newSeller.Bus = oStore.Bus;
                        newSeller.OtherVehicles = oStore.OtherVehicles;
                        newSeller.WebUrl = oStore.WebUrl;
                        newSeller.FacebookUrl = oStore.FacebookUrl;
                        newSeller.BlogUrl = oStore.BlogUrl;
                        newSeller.OtherUrl = oStore.OtherUrl;
                        newSeller.IsOpenBooking = oStore.IsOpenBooking;
                        newSeller.IsOpenReservationSetting = oStore.IsOpenReservationSetting;
                        newSeller.CompanyNotice = null;
                        newSeller.Coordinate = string.IsNullOrEmpty(oStore.Coordinate) ? null : oStore.Coordinate;
                        newSeller.VbsPrefix = null;
                        newSeller.ApplyId = null;
                        newSeller.ApplyTime = null;
                        newSeller.ReturnTime = null;
                        newSeller.TempStatus = (int)SellerTempStatus.Applied;
                        newSeller.CompanyAccount = oStore.CompanyAccount;
                        newSeller.CompanyAccountName = oStore.CompanyAccountName;
                        newSeller.CompanyBankCode = oStore.CompanyBankCode;
                        newSeller.CompanyBranchCode = oStore.CompanyBranchCode;
                        newSeller.CompanyBossName = oStore.CompanyBossName;
                        newSeller.CompanyID = oStore.CompanyID;
                        newSeller.CompanyName = oStore.CompanyName;
                        newSeller.AccountantName = oStore.AccountantName;
                        newSeller.AccountantTel = oStore.AccountantTel;
                        newSeller.CompanyEmail = oStore.CompanyEmail;
                        newSeller.CompanyNotice = oStore.CompanyNotice;
                        newSeller.StoreRelationCode = oStore.StoreRelationCode;
                        newSeller.CreateTime = now;
                        newSeller.CreateId = User.Identity.Name;
                        newSeller.ModifyTime = null;
                        newSeller.ModifyId = null;

                        try
                        {
                            sp.SellerSet(newSeller);
                        }
                        catch (Exception)
                        {
                            message.Append(string.Format("賣家編號:{0} 分店:{1} 轉換賣家失敗\r\n", oSeller.SellerId, oStore.StoreName));
                        }
                    }

                    #endregion insert into Seller

                    #region insert SellerTree

                    var sellerTree = sp.SellerTreeGetListBySellerGuid(oStore.Guid);
                    if (sellerTree.Any(x => x.ParentSellerGuid == oSeller.Guid))
                    {
                        var updateSellerTree = sellerTree.First(x => x.ParentSellerGuid == oSeller.Guid);

                        updateSellerTree.ModifyTime = now;
                        updateSellerTree.ModifyId = User.Identity.Name;
                        try
                        {
                            sp.SellerTreeSet(updateSellerTree);
                        }
                        catch (Exception)
                        {
                            message.Append(string.Format("賣家編號:{0} 分店:{1} 更新賣家階層狀態失敗\r\n", oSeller.SellerId, oStore.StoreName));
                        }
                    }
                    else
                    {
                        var newSellerTree = new SellerTree
                        {
                            SellerGuid = oStore.Guid,
                            ParentSellerGuid = oSeller.Guid,
                            RootSellerGuid = SellerFacade.GetRootSellerGuid(oSeller.Guid),
                            CreateId = User.Identity.Name,
                            CreateTime = now
                        };
                        try
                        {
                            sp.SellerTreeSet(newSellerTree);
                        }
                        catch (Exception)
                        {
                            message.Append(string.Format("賣家編號:{0} 分店:{1} 轉換賣家階層資料失敗\r\n", oSeller.SellerId, oStore.StoreName));
                        }
                    }

                    #endregion insert SellerTree

                    scope.Complete();
                }
            }

            return message;
        }

        private StringBuilder ProcessMoveSeller(IEnumerable<Guid> bids, Guid sellerGuid)
        {
            var message = new StringBuilder();
            var vps = pp.ViewPponDealGetByBusinessHourGuidList(bids.ToList()).ToDictionary(x => x.BusinessHourGuid, x => x);

            foreach (var bid in bids)
            {
                var vp = vps.ContainsKey(bid) ? vps[bid] : null;
                if (vp != null && vp.OrderedQuantity == 0)
                {
                    BusinessHour bh = sp.BusinessHourGet(vp.BusinessHourGuid);
                    if (bh != null)
                    {
                        var orignalSellerGuid = bh.SellerGuid;

                        if (orignalSellerGuid == sellerGuid)
                        {
                            continue;
                        }

                        var options = new TransactionOptions();
                        options.IsolationLevel = IsolationLevel.ReadCommitted;
                        using (var scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                        {
                            try
                            {

                                bh.SellerGuid = sellerGuid;
                                bh.ModifyTime = DateTime.Now;
                                bh.ModifyId = User.Identity.Name;

                                if (sp.BusinessHourSet(bh))
                                {
                                    //搬移賣家同步清除ppon_store設定
                                    pp.PponStoreDeleteByBid(bid);

                                    //寫入異動紀錄
                                    pp.ChangeLogInsert("ViewPponDeal", bh.Guid.ToString(), string.Format("檔次搬移賣家處理 - 由SellerGuid: {0} 搬移至SellerGuid: {1}", orignalSellerGuid, sellerGuid));
                                    pp.ChangeLogInsert("PponStore", bh.Guid.ToString(), "檔次搬移賣家處理 - 分店及核銷相關設定進行清除處理");
                                }
                            }
                            catch (Exception)
                            {
                                message.Append(string.Format("檔次[{0}]搬賣家失敗，搬移檔次賣家處理出現錯誤狀況！\r\n", vp.UniqueId));
                            }

                            scope.Complete();
                        }
                    }
                    else
                    {
                        message.Append(string.Format("檔次[{0}]搬賣家失敗，查無此檔！\r\n", vp.UniqueId));
                    }
                }
                else
                {
                    message.Append(string.Format("檔次[{0}]搬賣家失敗，已有訂單之檔次無法使用此功能！\r\n", vp == null ? bid.ToString() : vp.UniqueId.ToString()));
                }
            }

            return message;
        }

        #endregion Method

        #region validate methods

        public enum ActionFlags
        {
            New
        }

        public class ModelChecker
        {
            public string[] Messages { get; set; }

            public ModelChecker Check(ActionFlags action, Seller seller)
            {
                List<string> err = new List<string>();
                if (action == ActionFlags.New)
                {
                    if (string.IsNullOrWhiteSpace(seller.SellerName))
                    {
                        err.Add("賣家名稱未設定");
                    }
                }
                Messages = err.ToArray();
                return this;
            }

            public bool IsValid
            {
                get { return Messages == null || Messages.Length == 0; }
            }
        }

        #endregion
    }
}

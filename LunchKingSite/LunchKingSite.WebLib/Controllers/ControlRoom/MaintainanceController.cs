﻿using LunchKingSite.BizLogic.Model.Refund;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    public class MaintainanceController : BaseController
    {
        public ActionResult UpdateOrderReturnStatus(Guid orderGuid)
        {
            bool result;
            try
            {
                ReturnService.UpdateOrderReturnStatus(orderGuid);
                result = true;
            }
            catch (Exception)
            {
                result = false;
            }

            return Json(new { Success = result }, JsonRequestBehavior.AllowGet);
        }
    }
}

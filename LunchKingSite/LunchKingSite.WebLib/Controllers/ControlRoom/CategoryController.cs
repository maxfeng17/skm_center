﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.SellerModels;
using LunchKingSite.WebLib.Models.Category;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using SubSonic;
using System.Text.RegularExpressions;
using System.Drawing;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class CategoryController : ControllerExt
    {
        private ISellerProvider sp;
        private IPponProvider pp;
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly List<int> _standardRegion = new List<int> { 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242 };
        private static readonly List<KeyValuePair<int, int>> _specialRegion = new List<KeyValuePair<int, int>>
        {
        new KeyValuePair<int, int>(94,308),new KeyValuePair<int, int>(95,309),new KeyValuePair<int, int>(96,310)
        ,new KeyValuePair<int, int>(97,311),new KeyValuePair<int, int>(98,312),new KeyValuePair<int, int>(99,313)
        };

        public CategoryController()
        {
            sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        #region AddRegion

        public ActionResult AddRegion()
        {
            CategoryTypeNode channelTypeNode = CategoryManager.PponChannelCategoryTree;
            //美食  &  玩美‧休閒
            List<List<KeyValuePair<string, string>>> channelCategoryList = GetChannelCategoryList(CategoryType.PponChannelArea, channelTypeNode.CategoryNodes.Where(x => x.CategoryId == 87).ToList());

            //子區域行政區
            var allCityRegionList = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList().Where(x => !x.IsFinal && _standardRegion.Contains(x.Id));

            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(CategoryManager.Default.Taipei.CategoryId);
            var dependencylist =
                dependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1).Select(z => z.CategoryId);

            //旅遊
            List<List<KeyValuePair<string, string>>> travelChannelCategoryList = GetChannelCategoryList(CategoryType.PponChannelArea, channelTypeNode.CategoryNodes.Where(x => x.CategoryId == 90).ToList());

            AddRegionModel vModel = new AddRegionModel();
            vModel.ChannelCategoryList = channelCategoryList;
            vModel.CityRegionList = allCityRegionList.Select(x => new CityRegionOption { CityId = x.Id, CityName = x.NameInConsole, IsSelected = dependencylist.Contains(x.Id) }).ToList();
            vModel.TravelChannelCategoryList = travelChannelCategoryList;
            return View(vModel);
        }

        [HttpPost]
        public JsonResult ChangeRegion(int channelId, int regionId)
        {
            var allCityRegionList = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList().Where(x => !x.IsFinal && _standardRegion.Contains(x.Id));

            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(regionId);
            var dependencylist =
                dependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1).Select(z => z.CategoryId);


            return Json(new { Data = true, Nodes = allCityRegionList.Select(x => new CityRegionOption { CityId = x.Id, CityName = x.NameInConsole, IsSelected = dependencylist.Contains(x.Id) }).ToList() });
        }

        [HttpPost]
        public JsonResult AddNewRegion(int regionId, int areaId, int categoryType, string newRegionName, string[] cityList)
        {
            if (categoryType == 1)
            {
                #region 新增 商圈‧景點

                List<Category> categoryCol = sp.CategoryGetAll(Category.Columns.Id).ToList();
                List<Category> specialRegionCol = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList();

                Category newCategory = new Category();

                newCategory.Id = (categoryCol.OrderByDescending(x => x.Id).First().Id) + 1;
                newCategory.Name = newRegionName;
                newCategory.Rank = (specialRegionCol.OrderByDescending(x => x.Rank).First().Rank) + 1;
                newCategory.Code = (categoryCol.OrderByDescending(x => x.Code).First().Code) + 1; //修改為所有category的max，而不是同type的max
                newCategory.CreateId = UserName;
                newCategory.CreateTime = DateTime.Now;
                newCategory.Type = (int)CategoryType.PponChannelArea;
                newCategory.Status = 1;
                newCategory.IsFinal = true;
                newCategory.NameInConsole = newRegionName;

                sp.CategorySet(newCategory);

                CategoryDependency newCategoryDependency = new CategoryDependency();

                KeyValuePair<int, int> specialregion = _specialRegion.FirstOrDefault(x => x.Key == areaId);
                if (specialregion.Key > 0)
                {
                    CategoryDependencyCollection dependencyCol = sp.CategoryDependencyByRegionGet(specialregion.Value);
                    ViewCategoryDependencyCollection vDependencyCol = sp.ViewCategoryDependencyGetByParentId(specialregion.Value);

                    newCategoryDependency.ParentId = specialregion.Value;
                    newCategoryDependency.CategoryId = newCategory.Id;
                    newCategoryDependency.Seq = (vDependencyCol.ToList().Any(x => x.CategoryType.HasValue && x.CategoryType == (int)CategoryType.PponChannelArea)) ? dependencyCol.First().Seq + 1 : 1;
                    sp.CategoryDependencySet(newCategoryDependency);
                }

                #endregion 新增 商圈‧景點

                return Json(new { Success = true, Message = "分類新增完成" });
            }
            else if (categoryType == 2)
            {
                #region 更新 行政區關聯

                ViewCategoryDependencyCollection originaldependencyCol = sp.ViewCategoryDependencyGetByParentId(areaId);
                var originaldependencylist =
                    originaldependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1 && _standardRegion.Contains(x.CategoryId)).Select(z => z.CategoryId);

                foreach (var city in cityList)
                {
                    int citycategoryid = int.Parse(city);
                    var categoryCol = sp.CategoryDependencyGet(areaId, citycategoryid);

                    if (categoryCol.Any())
                    {
                        //已有關聯
                    }
                    else
                    {
                        //未有關聯，新增
                        CategoryDependency newCategoryDependency = new CategoryDependency();

                        CategoryDependencyCollection dependencyCol = sp.CategoryDependencyByRegionGet(areaId);
                        ViewCategoryDependencyCollection vDependencyCol = sp.ViewCategoryDependencyGetByParentId(areaId);

                        newCategoryDependency.ParentId = areaId;
                        newCategoryDependency.CategoryId = citycategoryid;
                        newCategoryDependency.Seq = (vDependencyCol.ToList().Any(x => x.CategoryType.HasValue && x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1)) ? dependencyCol.First().Seq + 1 : 1;
                        //美食
                        sp.CategoryDependencySet(newCategoryDependency);
                        //玩美‧休閒
                        sp.CategoryDependencySet(new CategoryDependency { ParentId = areaId + 48, CategoryId = newCategoryDependency.CategoryId, Seq = newCategoryDependency.Seq });
                    }

                }


                ////檢查是否需要刪除行政區關聯
                foreach (var cate in originaldependencylist)
                {
                    if (cityList.Contains(cate.ToString()))
                    {
                        //存在不動
                    }
                    else
                    {
                        //不存在，刪除category_dependency
                        CategoryDependencyCollection dependencys;
                        //美食
                        dependencys = sp.CategoryDependencyGet(areaId, cate);
                        sp.CategoryDependencyDelete(dependencys);
                        //玩美‧休閒
                        dependencys = sp.CategoryDependencyGet(areaId + 48, cate);
                        sp.CategoryDependencyDelete(dependencys);
                    }
                }

                #endregion 更新 行政區關聯

                return Json(new { Success = true, Message = "行政區更新完成" });
            }
            else if (categoryType == 3 || categoryType == 4)
            {
                #region 新增 旅遊商圈‧景點 & 旅遊子區域

                List<Category> categoryCol = sp.CategoryGetAll(Category.Columns.Id).ToList();
                List<Category> specialRegionCol = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList();

                Category newCategory = new Category();

                newCategory.Id = (categoryCol.OrderByDescending(x => x.Id).First().Id) + 1;
                newCategory.Name = newRegionName;
                newCategory.Rank = (specialRegionCol.OrderByDescending(x => x.Rank).First().Rank) + 1;
                newCategory.Code = (categoryCol.OrderByDescending(x => x.Code).First().Code) + 1; //修改為所有category的max，而不是同type的max
                newCategory.CreateId = UserName;
                newCategory.CreateTime = DateTime.Now;
                newCategory.Type = (int)CategoryType.PponChannelArea;
                newCategory.Status = 1;
                newCategory.IsFinal = (categoryType == 4) ? true : false;
                newCategory.NameInConsole = newRegionName;

                sp.CategorySet(newCategory);



                CategoryDependency newCategoryDependency = new CategoryDependency();

                if (categoryType == 3)
                {
                    //旅遊商圈列表
                    List<int> districtList = sp.ViewCategoryDependencyGetByParentId(areaId).Where(x => x.CategoryName.Contains("商圈‧景點")).Select(x => x.CategoryId).ToList();
                    if (districtList.Any())
                    {
                        int districtID = districtList.FirstOrDefault();
                        CategoryDependencyCollection dependencyCol = sp.CategoryDependencyByRegionGet(districtID);
                        ViewCategoryDependencyCollection vDependencyCol = sp.ViewCategoryDependencyGetByParentId(districtID);

                        newCategoryDependency.ParentId = districtID;
                        newCategoryDependency.CategoryId = newCategory.Id;
                        newCategoryDependency.Seq = (vDependencyCol.ToList().Any(x => x.CategoryType.HasValue && x.CategoryType == (int)CategoryType.PponChannelArea)) ? dependencyCol.First().Seq + 1 : 1;
                        sp.CategoryDependencySet(newCategoryDependency);
                    }
                }
                else if (categoryType == 4)
                {
                    CategoryDependencyCollection dependencyCol = sp.CategoryDependencyByRegionGet(areaId);
                    ViewCategoryDependencyCollection vDependencyCol = sp.ViewCategoryDependencyGetByParentId(areaId);

                    newCategoryDependency.ParentId = areaId;
                    newCategoryDependency.CategoryId = newCategory.Id;
                    newCategoryDependency.Seq = (vDependencyCol.ToList().Any(x => x.CategoryType.HasValue && x.CategoryType == (int)CategoryType.PponChannelArea)) ? dependencyCol.First().Seq + 1 : 1;
                    sp.CategoryDependencySet(newCategoryDependency);
                }

                #endregion 新增 旅遊商圈‧景點 & 旅遊子區域

                return Json(new { Success = true, Message = "旅遊分類新增完成" });
            }
            else
            {
                return Json(new { Success = false, Message = "無此區域分類類別，請重新輸入。" });
            }


        }

        #endregion AddRegion

        #region ManagerRegion

        public ActionResult ManagerRegion(int? regionId)
        {
            int regionid = CategoryManager.Default.Taipei.CategoryId;
            CategoryTypeNode channelTypeNode = CategoryManager.PponChannelCategoryTree;
            //美食 &  玩美‧休閒
            List<List<KeyValuePair<string, string>>> channelCategoryList = GetChannelCategoryList(CategoryType.PponChannelArea, channelTypeNode.CategoryNodes.Where(x => x.CategoryId == 87).ToList());
            //旅遊
            List<List<KeyValuePair<string, string>>> travelChannelCategoryList = GetChannelCategoryList(CategoryType.PponChannelArea, channelTypeNode.CategoryNodes.Where(x => x.CategoryId == 90).ToList());

            var allCityRegionList = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList().Where(x => !x.IsFinal && _standardRegion.Contains(x.Id));

            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(regionid);
            var dependencylist =
                dependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1).Select(z => z.CategoryId);

            //商圈。景點(美食 &  玩美‧休閒)
            KeyValuePair<int, int> specialregion = _specialRegion.FirstOrDefault(x => x.Key == regionid);
            var allSpRegion = sp.ViewCategoryDependencyGetByParentId(specialregion.Value);
            List<SpecialRegionOption> specialRegionList = new List<SpecialRegionOption>();

            foreach (var region in allSpRegion.Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
            {
                specialRegionList.Add(new SpecialRegionOption { ChannelId = regionid, SpecialRegionId = region.CategoryId, SpecialRegionName = region.CategoryName, Seq = region.Seq });
            }

            //商圈。景點(旅遊)
            int travelregionid = CategoryManager.Default.TravelNorth.CategoryId;
            int districtID = sp.ViewCategoryDependencyGetByParentId(travelregionid).Where(x => x.CategoryName.Contains("商圈‧景點")).Select(x => x.CategoryId).ToList().FirstOrDefault();
            var allTravelSpRegion = sp.ViewCategoryDependencyGetByParentId(districtID);
            List<SpecialRegionOption> travelSpecialRegionList = new List<SpecialRegionOption>();
            List<SpecialRegionOption> subTravelSpecialRegionList = new List<SpecialRegionOption>();

            foreach (var region in allTravelSpRegion.Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
            {
                travelSpecialRegionList.Add(new SpecialRegionOption { ChannelId = regionid, SpecialRegionId = region.CategoryId, SpecialRegionName = region.CategoryName, Seq = region.Seq });
                foreach (var subregion in sp.ViewCategoryDependencyGetByParentId(region.CategoryId).Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
                {
                    subTravelSpecialRegionList.Add(new SpecialRegionOption { ChannelId = region.CategoryId, SpecialRegionId = subregion.CategoryId, SpecialRegionName = subregion.CategoryName, Seq = subregion.Seq });
                }
            }

            ManagerRegionModel vModel = new ManagerRegionModel();
            vModel.ChannelCategoryList = channelCategoryList;
            vModel.TravelChannelCategoryList = travelChannelCategoryList;
            vModel.SpecialRegionList = specialRegionList;
            vModel.TravelSpecialRegionList = travelSpecialRegionList;
            vModel.SubTravelSpecialRegionList = subTravelSpecialRegionList;
            //vModel.StandardRegionList = allRegionList;
            vModel.CityRegionList = allCityRegionList.Select(x => new CityRegionOption { CityId = x.Id, CityName = x.NameInConsole, IsSelected = dependencylist.Contains(x.Id) }).ToList();
            return View(vModel);
        }

        [HttpPost]
        public JsonResult ChangeChannelRegion(int channelId, int regionId)
        {
            var allCityRegionList = sp.CategoryGetList((int)CategoryType.PponChannelArea).ToList().Where(x => !x.IsFinal && _standardRegion.Contains(x.Id));

            ViewCategoryDependencyCollection dependencyCol = sp.ViewCategoryDependencyGetByParentId(regionId);
            var dependencylist =
                dependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1).Select(z => z.CategoryId);

            //KeyValuePair<int, int> specialregion = _specialRegion.FirstOrDefault(x => x.Key == regionId);
            //var allSpRegion = sp.ViewCategoryDependencyGetByParentId(specialregion.Value);

            int districtID = sp.ViewCategoryDependencyGetByParentId(regionId).Where(x => x.CategoryName.Contains("商圈‧景點")).Select(x => x.CategoryId).ToList().FirstOrDefault();
            var allSpRegion = sp.ViewCategoryDependencyGetByParentId(districtID);

            List<SpecialRegionOption> specialRegionList = new List<SpecialRegionOption>();

            foreach (var region in allSpRegion.Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
            {
                specialRegionList.Add(new SpecialRegionOption { ChannelId = regionId, SpecialRegionId = region.CategoryId, SpecialRegionName = region.CategoryName, Seq = region.Seq });
            }

            return Json(new { Success = true, Message = "", SpecialNodes = specialRegionList, Nodes = allCityRegionList.Select(x => new CityRegionOption { CityId = x.Id, CityName = x.NameInConsole, IsSelected = dependencylist.Contains(x.Id) }).ToList() });
        }

        [HttpPost]
        public JsonResult DeleteSpecialRegion(int regionId, int areaId, int categoryId)
        {
            //檢查有無子節點(旅遊)
            int subNodes = sp.CategoryDependencyByRegionGet(categoryId).Count;
            if (subNodes > 0)
            {
                return Json(new { Success = false, Message = "刪除失敗，請先將子節點刪除，再執行刪除動作。" });
            }

            ViewCategoryDependencyCollection vcd = sp.ViewCategoryDependencyGetByParentId(areaId);
            var categroy = sp.CategoryGet(categoryId);
            if (categroy.IsLoaded)
            {
                int districtID = vcd.Where(x => x.CategoryName.Contains("商圈‧景點")).Select(x => x.CategoryId).ToList().FirstOrDefault();
                if (districtID > 0)
                {
                    CategoryDependencyCollection col;
                    col = sp.CategoryDependencyGet(districtID, categoryId);
                    sp.CategoryDependencyDelete(col);
                    sp.CategoryDelete(categroy);
                    return Json(new { Success = true, Message = "資料已刪除" });
                }
            }

            return Json(new { Success = false, Message = "查無此筆分類" });
        }
        [HttpPost]
        public JsonResult DeleteTravelSpecialRegion(int parentid, int sonid)
        {
            //檢查有無子節點(旅遊)
            int subNodes = sp.CategoryDependencyByRegionGet(sonid).Count;
            if (subNodes > 0)
            {
                return Json(new { Success = false, Message = "刪除失敗，請先將子節點刪除，再執行刪除動作。" });
            }

            ViewCategoryDependencyCollection vcd = sp.ViewCategoryDependencyGetByParentId(parentid);
            var categroy = sp.CategoryGet(sonid);
            if (categroy.IsLoaded)
            {
                int districtID = vcd.Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea && x.CategoryId == sonid).Select(x => x.ParentId).ToList().FirstOrDefault();
                if (districtID == parentid)
                {
                    CategoryDependencyCollection col;
                    col = sp.CategoryDependencyGet(parentid, sonid);
                    sp.CategoryDependencyDelete(col);
                    sp.CategoryDelete(categroy);
                    return Json(new { Success = true, Message = "資料已刪除" });
                }
            }

            return Json(new { Success = false, Message = "查無此筆分類" });
        }

        [HttpPost]
        public JsonResult UpdateRegionSetting(int regionId, int areaId, string[] specialRegionList, string[] cityList)
        {
            #region 更新 商圈景點

            if (specialRegionList != null)
            {
                foreach (var item in specialRegionList)
                {
                    var array = item.Split(',');
                    int categoryid = int.Parse(array[0]);
                    string categoryname = array[1];
                    int categoryseq = int.Parse(array[2]);
                    Category category = sp.CategoryGet(categoryid);

                    if (category.IsLoaded)
                    {
                        category.Name = category.NameInConsole = categoryname;
                        sp.CategorySet(category);
                    }
                    KeyValuePair<int, int> specialregion = _specialRegion.FirstOrDefault(x => x.Key == areaId);
                    CategoryDependencyCollection col = sp.CategoryDependencyGet(specialregion.Value, categoryid);
                    sp.CategoryDependencyDelete(col);
                    sp.CategoryDependencySet(new CategoryDependency
                    {
                        CategoryId = categoryid,
                        ParentId = specialregion.Value,
                        Seq = categoryseq
                    });
                }
            }

            #endregion 更新 商圈景點

            #region 更新 行政區關聯

            if (cityList != null)
            {
                ViewCategoryDependencyCollection originaldependencyCol = sp.ViewCategoryDependencyGetByParentId(areaId);
                var originaldependencylist =
                    originaldependencyCol.ToList().Where(x => x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1 && _standardRegion.Contains(x.CategoryId)).Select(z => z.CategoryId);

                foreach (var city in cityList)
                {
                    int citycategoryid = int.Parse(city);
                    var categoryCol = sp.CategoryDependencyGet(areaId, citycategoryid);

                    if (categoryCol.Any())
                    {
                        //已有關聯
                    }
                    else
                    {
                        //未有關聯，新增
                        CategoryDependency newCategoryDependency = new CategoryDependency();

                        CategoryDependencyCollection dependencyCol = sp.CategoryDependencyByRegionGet(areaId);
                        ViewCategoryDependencyCollection vDependencyCol = sp.ViewCategoryDependencyGetByParentId(areaId);

                        newCategoryDependency.ParentId = areaId;
                        newCategoryDependency.CategoryId = citycategoryid;
                        newCategoryDependency.Seq = (vDependencyCol.ToList().Any(x => x.CategoryType.HasValue && x.CategoryType == (int)CategoryType.PponChannelArea && x.CategoryStatus == 1)) ? dependencyCol.First().Seq + 1 : 1;
                        //美食
                        sp.CategoryDependencySet(newCategoryDependency);
                        //玩美‧休閒
                        sp.CategoryDependencySet(new CategoryDependency { ParentId = areaId + 48, CategoryId = newCategoryDependency.CategoryId, Seq = newCategoryDependency.Seq });
                    }

                }


                ////檢查是否需要刪除行政區關聯
                foreach (var cate in originaldependencylist)
                {
                    if (cityList.Contains(cate.ToString()))
                    {
                        //存在不動
                    }
                    else
                    {
                        //不存在，刪除category_dependency
                        CategoryDependencyCollection dependencys;
                        //美食
                        dependencys = sp.CategoryDependencyGet(areaId, cate);
                        sp.CategoryDependencyDelete(dependencys);
                        //玩美‧休閒
                        dependencys = sp.CategoryDependencyGet(areaId + 48, cate);
                        sp.CategoryDependencyDelete(dependencys);
                    }
                }
            }

            #endregion 更新 行政區關聯

            return Json(new { Success = true, Message = "資料已更新" });
        }

        [HttpPost]
        public JsonResult UpdateTravelRegionSetting(string[] parentList, string[] sonList)
        {
            #region 更新 旅遊商圈景點和子節點
            if (sonList != null)
            {
                int i = 0;
                foreach (var item in sonList)
                {
                    var array = item.Split(',');
                    int categoryid = int.Parse(array[0]);
                    string categoryname = array[1];
                    int categoryseq = int.Parse(array[2]);
                    int parentid = int.Parse(parentList[i]);

                    Category category = sp.CategoryGet(categoryid);

                    if (category.IsLoaded)
                    {
                        category.Name = category.NameInConsole = categoryname;
                        sp.CategorySet(category);
                    }

                    CategoryDependencyCollection col = sp.CategoryDependencyGet(parentid, categoryid);
                    sp.CategoryDependencyDelete(col);
                    sp.CategoryDependencySet(new CategoryDependency
                    {
                        CategoryId = categoryid,
                        ParentId = parentid,
                        Seq = categoryseq
                    });

                    i++;
                }
            }
            #endregion 更新 商圈景點
            return Json(new { Success = true, Message = "資料已更新" });
        }


        public ActionResult GetTravelRegion(int regionId)
        {
            List<SpecialRegionOption> travelSpecialRegionList = new List<SpecialRegionOption>();
            List<SpecialRegionOption> subTravelSpecialRegionList = new List<SpecialRegionOption>();

            int travelregionid = regionId;
            int districtID = sp.ViewCategoryDependencyGetByParentId(travelregionid).Where(x => x.CategoryName.Contains("商圈‧景點")).Select(x => x.CategoryId).ToList().FirstOrDefault();
            var allTravelSpRegion = sp.ViewCategoryDependencyGetByParentId(districtID);

            foreach (var region in allTravelSpRegion.Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
            {
                travelSpecialRegionList.Add(new SpecialRegionOption { ChannelId = regionId, SpecialRegionId = region.CategoryId, SpecialRegionName = region.CategoryName, Seq = region.Seq });
                foreach (var subregion in sp.ViewCategoryDependencyGetByParentId(region.CategoryId).Where(x => x.CategoryType.HasValue && x.CategoryType.Value == (int)CategoryType.PponChannelArea).OrderBy(z => z.Seq))
                {
                    subTravelSpecialRegionList.Add(new SpecialRegionOption { ChannelId = region.CategoryId, SpecialRegionId = subregion.CategoryId, SpecialRegionName = subregion.CategoryName, Seq = subregion.Seq });
                }
            }

            ManagerRegionModel vModel = new ManagerRegionModel();
            vModel.TravelSpecialRegionList = travelSpecialRegionList;
            vModel.SubTravelSpecialRegionList = subTravelSpecialRegionList;
            ViewBag.DistrictID = districtID;

            return PartialView("_TravelRegionPartial", vModel);
        }
        #endregion ManagerRegion

        #region AddCategory

        public ActionResult AddCategory()
        {
            return View();
        }

        [HttpPost]
        public JsonResult AddNewCategory(string categoryName, int parentCategoryId, HttpPostedFileBase file)
        {
            List<int> piinlifeCategoryCodelist = new List<int> { 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010 };
            string fileFullName = string.Empty;
            if (file != null)
            {
                var img = Image.FromStream(file.InputStream, true, true);
                //呼叫function
                string fileExtension = Helper.GetExtensionByContentType(file.ContentType);
                string fileName = DateTime.Now.Ticks.ToString();
                fileFullName = config.MediaBaseUrl + "Category/" + fileName + "." + fileExtension;
                ImageUtility.UploadFile(file.ToAdapter(), UploadFileType.PponEvent, "Category", fileName);

            }


            if (parentCategoryId == 0)
            {
                List<Category> CategoryCol = sp.CategoryGetAll(Category.Columns.Id).ToList();
                List<Category> dealCategoryCol = sp.CategoryGetList((int)CategoryType.DealCategory).ToList();

                Category newCategory = new Category();
                newCategory.Id = (CategoryCol.OrderByDescending(x => x.Id).First().Id) + 1;
                newCategory.Name = categoryName;
                newCategory.Rank = ((dealCategoryCol.Where(x => x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)).ToList()).OrderByDescending(x => x.Rank).First().Rank) + 1;
                newCategory.Code = (CategoryCol.OrderByDescending(x => x.Code).First().Code) + 1; //修改為所有category的max，而不是同type的max
                newCategory.CreateId = UserName;
                newCategory.CreateTime = DateTime.Now;
                newCategory.Type = (int)CategoryType.DealCategory;
                newCategory.Status = 1;
                newCategory.IsFinal = true;
                newCategory.NameInConsole = categoryName;

                if (!string.IsNullOrEmpty(fileFullName))
                    newCategory.Image = fileFullName;

                sp.CategorySet(newCategory);
            }
            else
            {
                List<Category> CategoryCol = sp.CategoryGetAll(Category.Columns.Id).ToList();
                List<Category> dealCategoryCol = sp.CategoryGetList((int)CategoryType.DealCategory).ToList();

                Category newCategory = new Category();
                newCategory.Id = (CategoryCol.OrderByDescending(x => x.Id).First().Id) + 1;
                newCategory.Name = categoryName;
                newCategory.Rank = ((dealCategoryCol.Where(x => x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)).ToList()).OrderByDescending(x => x.Rank).First().Rank) + 1;
                newCategory.Code = (CategoryCol.OrderByDescending(x => x.Code).First().Code) + 1; //修改為所有category的max，而不是同type的max
                newCategory.CreateId = UserName;
                newCategory.CreateTime = DateTime.Now;
                newCategory.Type = (int)CategoryType.DealCategory;
                newCategory.Status = 1;
                newCategory.IsFinal = true;
                newCategory.NameInConsole = categoryName;

                if (!string.IsNullOrEmpty(fileFullName))
                    newCategory.Image = fileFullName;

                sp.CategorySet(newCategory);

                //categorydependency sub關係
                var dependencycol = sp.ViewCategoryDependencyGetByParentId(parentCategoryId);

                Category parentCategory = sp.CategoryGet(parentCategoryId);
                if (parentCategory.IsFinal)
                {
                    parentCategory.IsFinal = false;
                    sp.CategorySet(parentCategory);
                }
                CategoryDependency categorydependency = new CategoryDependency();
                categorydependency.ParentId = parentCategoryId;
                categorydependency.CategoryId = newCategory.Id;
                categorydependency.Seq = (dependencycol.Count) + 1;
                sp.CategoryDependencySet(categorydependency);
            }

            return Json(new { Success = true, Message = "分類已經新增" });
        }

        [HttpPost]
        public JsonResult SearchDealCategory(string searchStr)
        {
            //All DealCategory

            var viewCategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
            var categoryCol = sp.CategoryGetList((int)CategoryType.DealCategory).ToList().OrderBy(x => x.Rank);
            List<int> piinlifeCategoryCodelist = new List<int> { 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010 };

            List<DealCategoryItem> resultList = new List<DealCategoryItem>();

            //所有分類都建構於_config.SkmCategordId(新光三越頻道 TYPE=4)底下
            var skmCategoryCol = sp.CategoryDependencyByRegionGet(config.SkmCategordId).Select(p=>p.CategoryId);

            if (string.IsNullOrEmpty(searchStr))
            {
                foreach (var ca in categoryCol.Where(x => x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)))
                {
                    if (viewCategoryCol.Any(x => x.ParentType == (int)CategoryType.DealCategory && x.CategoryId.Equals(ca.Id)))
                    {
                    }
                    else
                    {
                        if (!skmCategoryCol.Any(p => p.Equals(ca.Id)))
                        {
                            resultList.Add(new DealCategoryItem { ParentId = 0, CategoryId = ca.Id, CategoryName = ca.Name, ConsoleName = ca.Name, CategorySeq = ((ca.Rank.HasValue) ? ca.Rank.Value : 0), IsShowAddSub = true, IsShowFrontEnd = ca.IsShowFrontEnd, IsShowBackEnd = ca.IsShowBackEnd, IconType = ca.IconType, ImagePath = ca.Image ?? "" });

                            if (viewCategoryCol.Any(x => x.ParentId.Equals(ca.Id)))
                            {
                                resultList.AddRange(viewCategoryCol.Where(x => x.ParentId.Equals(ca.Id)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, CategoryName = y.CategoryName, ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = false, IsShowFrontEnd = y.IsShowFrontEnd, IsShowBackEnd = y.IsShowBackEnd, IconType = y.IconType, ImagePath = "" }).ToList());

                            }
                        }
                    }
                }
            }
            else
            {
                foreach (var ca in categoryCol.Where(x => x.Name.Contains(searchStr) && x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)))
                {
                    if (viewCategoryCol.Any(x => x.ParentType == (int)CategoryType.DealCategory && x.CategoryId.Equals(ca.Id)))
                    {
                    }
                    else
                    {
                        if (!skmCategoryCol.Any(p => p.Equals(ca.Id)))
                        {
                            resultList.Add(new DealCategoryItem
                            {
                                ParentId = 0, CategoryId = ca.Id, CategoryName = ca.Name, ConsoleName = ca.Name,
                                CategorySeq = ((ca.Rank.HasValue) ? ca.Rank.Value : 0), IsShowAddSub = true,
                                IsShowFrontEnd = ca.IsShowFrontEnd, IsShowBackEnd = ca.IsShowBackEnd,
                                IconType = ca.IconType,
                                ImagePath = ca.Image ?? ""
                            });
                            if (viewCategoryCol.Any(x => x.ParentId.Equals(ca.Id)))
                            {
                                resultList.AddRange(viewCategoryCol.Where(x => x.ParentId.Equals(ca.Id))
                                    .OrderBy(z => z.Seq).Select(y => new DealCategoryItem
                                    {
                                        ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId,
                                        CategoryName = y.CategoryName,
                                        ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq,
                                        IsShowAddSub = false, IsShowFrontEnd = y.IsShowFrontEnd,
                                        IsShowBackEnd = y.IsShowBackEnd, IconType = y.IconType,
                                        ImagePath = ""
                                    }).ToList());
                            }
                        }
                    }
                }
            }

            if (resultList.Count <= 0)
            {
                return Json(new { Success = false, Message = "查無相關分類", CategoryNodes = resultList });
            }

            return Json(new { Success = true, Message = "", CategoryNodes = resultList });

        }

        [HttpPost]
        public JsonResult UpdateCategoryName(int parentId, int categoryId, string categoryName, bool isShowFrontEnd, bool isShowBackEnd, int iconType, HttpPostedFileBase file)
        {
            Category category = sp.CategoryGet(categoryId);
            string fileFullName = string.Empty;
            if (file != null)
            {
                var img = Image.FromStream(file.InputStream, true, true);
                //呼叫function
                string fileExtension = Helper.GetExtensionByContentType(file.ContentType);
                string fileName = DateTime.Now.Ticks.ToString();
                fileFullName = config.MediaBaseUrl + "Category/" + fileName + "." + fileExtension;
                ImageUtility.UploadFile(file.ToAdapter(), UploadFileType.PponEvent, "Category", fileName);

            }

            if (!isShowFrontEnd)
            {
                ViewCategoryDependencyCollection vcdc = sp.ViewCategoryDependencyGetByParentId(categoryId);
                var vcd = vcdc.Where(x => x.IsShowFrontEnd == true).FirstOrDefault();
                if (vcd != null)
                {
                    return Json(new { Success = false, Message = "請先將子項目的「顯示於前台」都關閉，再關閉母項目!!" });
                }
            }
            else
            {
                ViewCategoryDependencyCollection vcs = sp.ViewCategoryDependencyGetByCid(categoryId);
                foreach (ViewCategoryDependency vc in vcs)
                {
                    Category ca = sp.CategoryGet(vc.ParentId);
                    if (ca.IsLoaded && !ca.IsShowFrontEnd)
                    {
                        return Json(new { Success = false, Message = "請先將母項目的「顯示於前台」都開啟，再開啟子項目!!" });
                    }
                }
            }

            if (!isShowBackEnd)
            {
                ViewCategoryDependencyCollection vcdc = sp.ViewCategoryDependencyGetByParentId(categoryId);
                var vcd = vcdc.Where(x => x.IsShowBackEnd == true).FirstOrDefault();
                if (vcd != null)
                {
                    return Json(new { Success = false, Message = "請先將子項目的「顯示於後台」都關閉，再關閉母項目!!" });
                }
            }
            else
            {
                ViewCategoryDependencyCollection vcs = sp.ViewCategoryDependencyGetByCid(categoryId);
                foreach (ViewCategoryDependency vc in vcs)
                {
                    Category ca = sp.CategoryGet(vc.ParentId);
                    if (ca.IsLoaded && !ca.IsShowBackEnd)
                    {
                        return Json(new { Success = false, Message = "請先將母項目的「顯示於後台」都開啟，再開啟子項目!!" });
                    }
                }
            }

            category.Name = category.NameInConsole = categoryName;
            category.IsShowFrontEnd = isShowFrontEnd;
            category.IsShowBackEnd = isShowBackEnd;
            category.IconType = iconType;
            if (!string.IsNullOrEmpty(fileFullName))
                category.Image = fileFullName;

            sp.CategorySet(category);


            return Json(new { Success = true, Message = "分類資料已更新!!" });
        }

        [HttpPost]
        public JsonResult DeleteCategory(int parentId, int categoryId)
        {
            if (parentId == 0)
            {
                //MainDealCategory
                var categroy = sp.CategoryGet(categoryId);
                if (categroy.IsLoaded)
                {
                    var dependencycol = sp.ViewCategoryDependencyGetByParentId(categoryId);
                    if (dependencycol.Any())
                    {
                        return Json(new { Success = false, Message = "尚有子分類，無法刪除" });
                    }
                    else
                    {
                        var viewCategoryDependencyList = sp.ViewCategoryDependencyGetByParentType((int)CategoryType.PponChannel).ToList();
                        foreach (var viewCategoryDependency in viewCategoryDependencyList)
                        {
                            CategoryDependencyCollection col = sp.CategoryDependencyGet(viewCategoryDependency.ParentId, categoryId);
                            sp.CategoryDependencyDelete(col);
                        }
                        sp.CategoryDelete(categroy);
                        return Json(new { Success = true, Message = "分類已經刪除" });
                    }
                }
            }
            else
            {
                //SubDealCategory
                var categroy = sp.CategoryGet(categoryId);
                if (categroy.IsLoaded)
                {
                    CategoryDependencyCollection col = sp.CategoryDependencyGet(parentId, categoryId);
                    sp.CategoryDependencyDelete(col);
                    sp.CategoryDelete(categroy);

                    var dependencycol = sp.ViewCategoryDependencyGetByParentId(parentId);

                    Category parentCategory = sp.CategoryGet(parentId);
                    if (parentCategory.IsFinal && dependencycol.Count <= 0)
                    {
                        parentCategory.IsFinal = true;
                        sp.CategorySet(parentCategory);
                    }
                }
                return Json(new { Success = true, Message = "分類已經刪除" });
            }

            return Json(new { Success = false, Message = "查無此筆分類" });


        }

        #endregion AddCategory

        #region ManagerCategory

        public ActionResult ManagerCategory(int? channelId, bool? IsSubShow)
        {
            CategoryTypeNode channelTypeNode = CategoryManager.PponChannelCategoryTree;

            List<KeyValuePair<string, string>> channelCategoryList = channelTypeNode.CategoryNodes.Where(x => x.CategoryId == 87 || x.CategoryId == 88 || x.CategoryId == 89 || x.CategoryId == 90 || x.CategoryId == 148).Select(y => new KeyValuePair<string, string>(y.CategoryId.ToString(), y.CategoryName)).ToList();

            ManagerCategoryModel vModel = new ManagerCategoryModel();
            vModel.ChannelCategoryList = channelCategoryList;

            if (channelId != null && IsSubShow != null)
            {
                List<DealCategoryItem> resultList = getChannel(channelId ?? 87, IsSubShow ?? true);
                ViewBag.CategoryItems = resultList;
                ViewBag.channelId = channelId;
            }

            return View(vModel);
        }

        public ActionResult CategoryDealSet(int chid, int caid, int? subid, string dateS, string dateE, int? query)
        {
            HttpContext.Response.Cache.SetExpires(DateTime.Now.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();

            ViewCategoryDealListCollection categoryDealList = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealAddList = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealExistList = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealRemoveList = new ViewCategoryDealListCollection();
            int maxCount = 100000;
            int lastid = 0;
            int isQuery = query == null ? 0 : (int)query;
            int set;

            ViewBag.channelTitle = sp.CategoryGet(chid);
            ViewBag.categoryTitle = sp.CategoryGet(caid);
            if (subid != null)
            {
                ViewBag.subcategoryTitle = "-" + sp.CategoryGet((int)subid);
                lastid = (int)subid;
            }
            else
            {
                ViewBag.subcategoryTitle = "";
                lastid = caid;
            }

            HttpCookie cookie = Request.Cookies["cd"];
            if (cookie != null)
            {
                set = int.Parse(cookie["set"].ToString());
            }
            else
            {
                set = 0;
            }

            categoryDealList = pp.GetCategoryDealListByCidAndOrderTime(lastid, dateS, dateE);

            if (set == 0 || isQuery == 1)
            {
                cookie = CookieManager.NewCookie("cd");
                cookie.Expires = DateTime.Now.AddDays(1);
                cookie["set"] = "0";
                Response.Cookies.Add(cookie);
            }
            else
            {
                categoryDealAddList = (ViewCategoryDealListCollection)TempData["categoryDealAddList"];
                categoryDealExistList = (ViewCategoryDealListCollection)TempData["categoryDealExistList"];
                categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
                foreach (ViewCategoryDealList v in categoryDealExistList)
                {
                    if (categoryDealList.Any(x => x.Id == v.Id))
                    {
                        ViewCategoryDealList existItem = categoryDealList.First(x => x.Id == v.Id);
                        categoryDealList.Remove(existItem);
                    }
                }
                foreach (ViewCategoryDealList v in categoryDealRemoveList)
                {
                    if (categoryDealList.Any(x => x.Id == v.Id))
                    {
                        ViewCategoryDealList removeItem = categoryDealList.First(x => x.Id == v.Id);
                        categoryDealList.Remove(removeItem);
                    }
                }
            }

            int allCount = categoryDealList != null ? (int)categoryDealList.Count() : 0;
            int addCount = categoryDealAddList != null ? (int)categoryDealAddList.Count() : 0;
            int removeCount = categoryDealRemoveList != null ? (int)categoryDealRemoveList.Count() : 0;
            ViewBag.allCount = allCount;
            ViewBag.addCount = addCount;
            ViewBag.removeCount = removeCount;
            if (allCount > maxCount)
            {
                categoryDealList.Clear();
            }
            TempData["categoryDealAddList"] = categoryDealAddList;
            TempData["categoryDealExistList"] = categoryDealExistList;
            TempData["categoryDealRemoveList"] = categoryDealRemoveList;
            TempData.Keep();

            return View(categoryDealList);
        }

        [HttpPost]
        public JsonResult RemoveDeal(int id, string seller_name, string item_name, string bid, int cid, int outdate)
        {
            ViewCategoryDealListCollection categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
            ViewCategoryDealList item = new ViewCategoryDealList();

            item.Id = id;
            item.SellerName = seller_name;
            item.ItemName = item_name;
            item.Bid = Guid.Parse(bid);
            item.Cid = cid;
            item.Outdate = outdate;
            if (!categoryDealRemoveList.Contains(item))
            {
                categoryDealRemoveList.Add(item);
            }
            TempData.Keep();

            return Json(new { success = true, removeCount = categoryDealRemoveList.Count() });
        }

        [HttpPost]
        public JsonResult AddDeal(string bids, int lastid, string dateS, string dateE)
        {
            ViewCategoryDealListCollection categoryDealAddList = (ViewCategoryDealListCollection)TempData["categoryDealAddList"];
            ViewCategoryDealListCollection categoryDealExistList = (ViewCategoryDealListCollection)TempData["categoryDealExistList"];
            ViewCategoryDealListCollection categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
            ViewCategoryDealListCollection categoryDealCheck = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealAdd = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealExist = new ViewCategoryDealListCollection();
            ViewCategoryDealListCollection categoryDealListOutsideOfOrdertime = new ViewCategoryDealListCollection();
            BusinessHour businessHour;
            ViewPponDeal vpd;
            Regex reg = new Regex("^[0-9a-fA-F]{8}(-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}$");
            List<string> bidList = new List<string>();
            List<string> addBidList = new List<string>();
            string errorMessage = "";
            string bidString;
            int isNotGUID = 0;
            int outDateDeals = 0;
            int oldDateDeals = 0;

            bids = bids.Replace(",\n", ",");
            bids = bids.Replace("\t", "");
            bidString = bids.Replace("\n", ",");

            char[] splitchars = { '\n', ','};
            string[] bid = bids.Split(splitchars);
            int bidOriginLength = bid.Length;

            foreach (string i in bid)
            {
                //GUID格式check
                if (!reg.Match(i).Success)
                {
                    isNotGUID++;
                }
                //GUID重複check
                if (!bidList.Contains(i))
                {
                    bidList.Add(i);
                }
            }
            
            if (isNotGUID > 0)
            {
                if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                errorMessage += "內含不符合檔次格式" + isNotGUID + "筆，請先移除。";
            }
            else
            {
                foreach (string i in bid)
                {
                    //現有檔次check
                    if (categoryDealAddList.Any(x => x.Bid == Guid.Parse(i)))
                    {
                        ViewCategoryDealList checkItem = categoryDealAddList.First(x => x.Bid == Guid.Parse(i));
                        categoryDealCheck.Add(checkItem);
                    }
                    //未設定檔期check
                    businessHour = pp.BusinessHourGet(Guid.Parse(i));
                    if (businessHour.BusinessHourOrderTimeE.Date == DateTime.MaxValue.Date)
                    {
                        outDateDeals++;
                    }
                    //午餐王檔次check
                    businessHour = pp.BusinessHourGet(Guid.Parse(i));
                    if (businessHour.BusinessHourOrderTimeE.Date == Convert.ToDateTime("1900/1/1").Date)
                    {
                        oldDateDeals++;
                    }
                }

                if (bid.Count() != bidList.Count())
                {
                    if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                    errorMessage += "內含重複檔次" + (bid.Count()- bidList.Count()) + "筆，請檢查。";
                }
                if (outDateDeals > 0)
                {
                    if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                    errorMessage += "內含未設定檔期檔次" + outDateDeals + "筆，請先移除檔次。";
                }
                if (oldDateDeals > 0)
                {
                    if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                    errorMessage += "內含午餐王舊檔次" + oldDateDeals + "筆，請先移除檔次。";
                }

                if (string.IsNullOrEmpty(errorMessage))
                {
                    categoryDealListOutsideOfOrdertime = pp.GetCategoryDealListByBidAndCidOutsideOfOrdertime(bidString, lastid, dateS, dateE);
                    categoryDealExist = pp.GetCategoryDealListByBidAndCidAndOrderTime(bidString, lastid, dateS, dateE);
                    foreach (ViewCategoryDealList v in categoryDealRemoveList)
                    {
                        if (categoryDealExist.Any(x => x.Id == v.Id))
                        {
                            ViewCategoryDealList removeItem = categoryDealExist.First(x => x.Id == v.Id);
                            categoryDealExist.Remove(removeItem);
                        }
                    }

                    foreach (string i in bid)
                    {
                        if (categoryDealAddList.Any(x => x.Bid == Guid.Parse(i)))
                        {
                            bid = bid.Where(x => x != i).ToArray();
                        }
                        if (categoryDealExist.Any(x => x.Bid == Guid.Parse(i)))
                        {
                            bid = bid.Where(x => x != i).ToArray();
                        }
                        if (categoryDealListOutsideOfOrdertime.Any(x => x.Bid == Guid.Parse(i)))
                        {
                            bid = bid.Where(x => x != i).ToArray();
                        }
                    }

                    foreach (string i in bid)
                    {
                        ViewCategoryDealList addItem = new ViewCategoryDealList();
                        vpd = pp.ViewPponDealGetByBusinessHourGuid(Guid.Parse(i));
                        addItem.SellerName = vpd.SellerName;
                        addItem.ItemName = vpd.ItemName;
                        addItem.Bid = vpd.BusinessHourGuid;
                        addItem.Outdate = vpd.BusinessHourOrderTimeE.Date >= DateTime.Now.Date ? 0 : 1;
                        categoryDealAdd.Add(addItem);
                        categoryDealAddList.Add(addItem);
                    }

                    if (categoryDealExist.Count() > 0 || categoryDealListOutsideOfOrdertime.Count() > 0 || categoryDealCheck.Count() > 0)
                    {
                        errorMessage += "您輸入的" + bidOriginLength + "筆檔次中，";

                        if (categoryDealCheck.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                            errorMessage += "有" + categoryDealCheck.Count() + "筆檔次在「此次加入檔次」列表，";
                        }
                        if (categoryDealExist.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                            errorMessage += "有" + categoryDealExist.Count() + "筆檔次在「已加入檔次」列表，";
                        }
                        if (categoryDealListOutsideOfOrdertime.Count() > 0)
                        {
                            if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n";
                            errorMessage += "有" + categoryDealListOutsideOfOrdertime.Count() + "筆檔次在「設定期間之外已加入檔次」列表，";
                        }
                        if (!string.IsNullOrEmpty(errorMessage)) errorMessage += "\n\n";
                        errorMessage += "本次實際匯入的檔次數量為：" + categoryDealAdd.Count() + "筆。";
                    }

                    foreach (ViewCategoryDealList v in categoryDealExistList)
                    {
                        if (categoryDealExist.Any(x => x.Id == v.Id))
                        {
                            ViewCategoryDealList existItem = categoryDealExist.First(x => x.Id == v.Id);
                            categoryDealExist.Remove(existItem);
                        }
                    }
                    foreach (ViewCategoryDealList v in categoryDealExist)
                    {
                        categoryDealExistList.Add(v);
                    }
                }
            }
            TempData.Keep();

            return Json(new { success = true, addCount = categoryDealAddList.Count(), addData=categoryDealAdd, existData=categoryDealExist, Message= errorMessage});
        }

        [HttpPost]
        public JsonResult RemoveAddDeal(string bid)
        {
            ViewCategoryDealListCollection categoryDealAddList = (ViewCategoryDealListCollection)TempData["categoryDealAddList"];
            if (categoryDealAddList.Any(x => x.Bid == Guid.Parse(bid)))
            {
                ViewCategoryDealList item = categoryDealAddList.First(x => x.Bid == Guid.Parse(bid));
                categoryDealAddList.Remove(item);
            }
            TempData.Keep();

            return Json(new { success = true, addCount = categoryDealAddList.Count() });
        }

        [HttpPost]
        public JsonResult RemoveExistDeal(string bid)
        {
            ViewCategoryDealListCollection categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
            ViewCategoryDealListCollection categoryDealExistList = (ViewCategoryDealListCollection)TempData["categoryDealExistList"];
            if (categoryDealExistList.Any(x => x.Bid == Guid.Parse(bid)))
            {
                ViewCategoryDealList item = categoryDealExistList.First(x => x.Bid == Guid.Parse(bid));
                categoryDealExistList.Remove(item);
                categoryDealRemoveList.Add(item);
            }
            TempData.Keep();

            return Json(new { success = true, removeCount = categoryDealRemoveList.Count() });
        }

        public ActionResult CategoryDealList(int chid, int caid, int? subid, string dateS, string dateE)
        {
            ViewCategoryDealListCollection categoryDealAddList = (ViewCategoryDealListCollection)TempData["categoryDealAddList"];
            ViewCategoryDealListCollection categoryDealExistList = (ViewCategoryDealListCollection)TempData["categoryDealExistList"];
            ViewCategoryDealListCollection categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
            int addCount = categoryDealAddList != null ? (int)categoryDealAddList.Count() : 0;
            int existCount = categoryDealExistList != null ? (int)categoryDealExistList.Count() : 0;
            int removeCount = categoryDealRemoveList != null ? (int)categoryDealRemoveList.Count() : 0;
            int lastid = 0;

            if (subid != null)
            {
                lastid = (int)subid;
            }
            else
            {
                lastid = caid;
            }

            HttpCookie cookie = Request.Cookies["cd"];
            if (cookie != null)
            {
                cookie["set"] = "1";
                Response.SetCookie(cookie);
            }
            else
            {
                cookie = CookieManager.NewCookie("cd");
                cookie.Expires = DateTime.Now.AddDays(1);
                cookie["set"] = "1";
                Response.Cookies.Add(cookie);
            }

            if (categoryDealRemoveList == null && categoryDealExistList == null && categoryDealAddList == null)
            {
                return RedirectToAction("ManagerCategory", "Category");
            }
            
            ViewBag.channelTitle = sp.CategoryGet(chid);
            ViewBag.categoryTitle = sp.CategoryGet(caid);
            if (subid != null)
            {
                ViewBag.subcategoryTitle = "-" + sp.CategoryGet((int)subid);
            }
            else
            {
                ViewBag.subcategoryTitle = "";
            }
            ViewBag.addCount = addCount;
            ViewBag.removeCount = removeCount;
            ViewBag.inputCount = addCount + existCount;
            TempData.Keep();

            return View();
        }

        [HttpPost]
        public JsonResult SaveAllDeal(int chid, int caid, int? subid)
        {
            ViewCategoryDealListCollection categoryDealAddList = (ViewCategoryDealListCollection)TempData["categoryDealAddList"];
            ViewCategoryDealListCollection categoryDealRemoveList = (ViewCategoryDealListCollection)TempData["categoryDealRemoveList"];
            CategoryDealCollection categoryDealAdd = new CategoryDealCollection();
            CategoryDealCollection categoryDealRemove = new CategoryDealCollection();
            string errMessage = "";
            string Message = "";

            foreach (ViewCategoryDealList v in categoryDealAddList)
            {
                if (pp.GetCategoryDealCountByBidAndCid(v.Bid, chid) == 0)
                {
                    CategoryDeal addItem = new CategoryDeal();
                    addItem.Bid = v.Bid;
                    addItem.Cid = chid;
                    categoryDealAdd.Add(addItem);
                }
                if (pp.GetCategoryDealCountByBidAndCid(v.Bid, caid) == 0)
                {
                    CategoryDeal addItem = new CategoryDeal();
                    addItem.Bid = v.Bid;
                    addItem.Cid = caid;
                    categoryDealAdd.Add(addItem);
                }
                if (subid != null)
                {
                    if (pp.GetCategoryDealCountByBidAndCid(v.Bid, (int)subid) == 0)
                    {
                        CategoryDeal addItem = new CategoryDeal();
                        addItem.Bid = v.Bid;
                        addItem.Cid = (int)subid;
                        categoryDealAdd.Add(addItem);
                    }
                }
            }

            foreach (ViewCategoryDealList v in categoryDealRemoveList)
            {
                CategoryDeal removeSubidItem = new CategoryDeal();
                CategoryDeal removeCaidItem = new CategoryDeal();
                CategoryDeal removeChidItem = new CategoryDeal();

                if (subid != null)
                {
                    removeSubidItem.Bid = v.Bid;
                    removeSubidItem.Cid = (int)subid;
                    categoryDealRemove.Add(removeSubidItem);

                    CategoryDependencyCollection subidList = pp.GetCategoryDependencyByParentId(caid);
                    string subidString = string.Join(",", subidList.Select(x => x.CategoryId));
                    int subidInCategoryDeal = pp.GetCategoryDealCountByBidAndCidString(v.Bid, subidString);

                    if (subidInCategoryDeal == 1)
                    {
                        removeCaidItem.Bid = v.Bid;
                        removeCaidItem.Cid = caid;
                        categoryDealRemove.Add(removeCaidItem);

                        CategoryDependencyCollection caidList = pp.GetCategoryDependencyByParentId(chid);
                        string caidString = string.Join(",", caidList.Select(x => x.CategoryId));
                        int caidInCategoryDeal = pp.GetCategoryDealCountByBidAndCidString(v.Bid, caidString);

                        if (caidInCategoryDeal == 1)
                        {
                            removeChidItem.Bid = v.Bid;
                            removeChidItem.Cid = chid;
                            categoryDealRemove.Add(removeChidItem);
                        }
                    }
                }
                else
                {
                    removeCaidItem.Bid = v.Bid;
                    removeCaidItem.Cid = caid;
                    categoryDealRemove.Add(removeCaidItem);

                    CategoryDependencyCollection caidList = pp.GetCategoryDependencyByParentId(chid);
                    string caidString = string.Join(",", caidList.Select(x => x.CategoryId));
                    int caidInCategoryDeal = pp.GetCategoryDealCountByBidAndCidString(v.Bid, caidString);

                    if (caidInCategoryDeal == 1)
                    {
                        removeChidItem.Bid = v.Bid;
                        removeChidItem.Cid = chid;
                        categoryDealRemove.Add(removeChidItem);
                    }
                }
            }

            HttpCookie cookie = Request.Cookies["cd"];
            if (cookie != null)
            {
                cookie.Expires = DateTime.Now.AddDays(-1);
                Response.SetCookie(cookie);
            }

            if (categoryDealAdd.Count() > 0)
            {
                try
                {
                    pp.SaveCategoryDeal(categoryDealAdd);

                    //同步提案單&dealProperty
                    ProposalCategoryDealCollection allCategory = new ProposalCategoryDealCollection();
                    foreach (CategoryDeal cd in categoryDealAdd)
                    {
                        Proposal pro = sp.ProposalGet(cd.Bid);
                        if (pro.IsLoaded)
                        {
                            ProposalCategoryDealCollection newCategory = sp.ProposalCategoryDealsGetList(pro.Id);
                            ProposalCategoryDealCollection oriCategory = newCategory.Clone();

                            ProposalCategoryDeal addc = new ProposalCategoryDeal();
                            addc.Pid = pro.Id;
                            addc.Cid = cd.Cid;
                            allCategory.Add(addc);

                            newCategory.Add(addc);
                            ProposalFacade.CompareProposalCategoryDeal(newCategory, oriCategory, UserName);
                        }
                        DealProperty dp = pp.DealPropertyGet(cd.Bid);
                        if (dp.IsLoaded)
                        {
                            CategoryDealCollection categories = pp.CategoryDealsGetList(dp.BusinessHourGuid);
                            dp.CategoryList = new JsonSerializer().Serialize(categories.Select(t => t.Cid).Distinct().ToList());
                            pp.DealPropertySet(dp);
                        }
                        
                    }
                    if (allCategory.Count>0)
                    {
                        sp.ProposalCategoryDealSetList(allCategory);
                    }
                }
                catch (Exception ex)
                {
                    errMessage += ex;
                }
            }
            if (categoryDealRemove.Count() > 0)
            {
                try
                {
                    pp.RemoveCategoryDeal(categoryDealRemove);

                    //同步提案單&dealProperty
                    foreach (CategoryDeal cd in categoryDealRemove)
                    {
                        Proposal pro = sp.ProposalGet(cd.Bid);
                        if (pro.IsLoaded)
                        {
                            ProposalCategoryDealCollection newCategory = sp.ProposalCategoryDealsGetList(pro.Id);
                            ProposalCategoryDealCollection oriCategory = newCategory.Clone();

                            sp.DeleteProposalCategoryDealsByCategoryType(pro.Id, cd.Cid);

                            newCategory = sp.ProposalCategoryDealsGetList(pro.Id);
                            ProposalFacade.CompareProposalCategoryDeal(newCategory, oriCategory, UserName);
                        }

                        DealProperty dp = pp.DealPropertyGet(cd.Bid);
                        if (dp.IsLoaded)
                        {
                            CategoryDealCollection categories = pp.CategoryDealsGetList(dp.BusinessHourGuid);
                            dp.CategoryList = new JsonSerializer().Serialize(categories.Select(t => t.Cid).Distinct().ToList());
                            pp.DealPropertySet(dp);
                        }
                    }   
                }
                catch (Exception ex)
                {
                    errMessage += ex;
                }
            }

            if(errMessage == "")
            {
                Message = "檔次分類異動完成";
            }
            else
            {
                Message = errMessage;
            }
            Message += "，回到頻道分類管理首頁。";

            return Json(new { success = true, Message = Message });
        }

        [HttpPost]
        public JsonResult ChangeChannel(int channelId, bool IsSubShow)
        {
            List<DealCategoryItem> resultList = getChannel(channelId, IsSubShow);

            return Json(new { Success = true, Message = "", CategoryNodes = resultList });
        }

        private List<DealCategoryItem> getChannel(int channelId, bool IsSubShow)
        {
            var categoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);

            List<DealCategoryItem> resultList = new List<DealCategoryItem>();

            foreach (var ca in categoryCol.Where(x => x.ParentType == (int)CategoryType.PponChannel && x.ParentId == channelId).OrderBy(z => z.Seq).ToList())
            {
                resultList.Add(new DealCategoryItem { ParentId = ca.ParentId, ParentName = ca.ParentName, CategoryId = ca.CategoryId, CategoryName = ca.CategoryName, ConsoleName = ca.CategoryName, CategorySeq = ca.Seq, IsShowAddSub = (ca.ParentType != ca.CategoryType), IsShowFrontEnd = ca.IsShowFrontEnd });
                if (IsSubShow)
                {
                    var subcategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
                    if (subcategoryCol.Any(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)))
                    {
                        resultList.AddRange(subcategoryCol.Where(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, CategoryName = y.CategoryName, ConsoleName = y.ParentName + "-" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = (y.ParentType != y.CategoryType), IsShowFrontEnd = y.IsShowFrontEnd }).ToList());
                    }
                }
            }

            return resultList;
        }

        [HttpPost]
        public JsonResult SearchCategory(int channelId, string searchStr)
        {
            var viewCategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
            var categoryCol = sp.CategoryGetList((int)CategoryType.DealCategory).ToList().OrderBy(x => x.Rank);
            List<int> piinlifeCategoryCodelist = new List<int> { 10003, 10004, 10005, 10006, 10007, 10008, 10009, 10010 };

            List<DealCategoryItem> resultList = new List<DealCategoryItem>();

            //parentId

            if (string.IsNullOrEmpty(searchStr))
            {
                foreach (var ca in categoryCol.Where(x => x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)))
                {
                    if (viewCategoryCol.Any(x => x.ParentType == (int)CategoryType.DealCategory && x.CategoryId.Equals(ca.Id)))
                    {
                    }
                    else
                    {
                        resultList.Add(new DealCategoryItem { ParentId = 0, CategoryId = ca.Id, CategoryName = ca.Name, ConsoleName = ca.Name, CategorySeq = ((ca.Rank.HasValue) ? ca.Rank.Value : 0), IsShowAddSub = true, IsShowFrontEnd = ca.IsShowFrontEnd });

                        //if (viewCategoryCol.Any(x => x.ParentId.Equals(ca.Id)))
                        //{
                        //    resultList.AddRange(viewCategoryCol.Where(x => x.ParentId.Equals(ca.Id)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = false }).ToList());
                        //}
                    }
                }
            }
            else
            {
                foreach (var ca in categoryCol.Where(x => x.Name.Contains(searchStr) && x.Code.HasValue && !piinlifeCategoryCodelist.Contains(x.Code.Value)))
                {
                    if (viewCategoryCol.Any(x => x.ParentType == (int)CategoryType.DealCategory && x.CategoryId.Equals(ca.Id)))
                    {
                    }
                    else
                    {
                        resultList.Add(new DealCategoryItem { ParentId = 0, CategoryId = ca.Id, CategoryName = ca.Name, ConsoleName = ca.Name, CategorySeq = ((ca.Rank.HasValue) ? ca.Rank.Value : 0), IsShowAddSub = true, IsShowFrontEnd = ca.IsShowFrontEnd });

                        //if (viewCategoryCol.Any(x => x.ParentId.Equals(ca.Id)))
                        //{
                        //    resultList.AddRange(viewCategoryCol.Where(x => x.ParentId.Equals(ca.Id)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = false }).ToList());
                        //}
                    }
                }

            }

            return Json(new { Success = true, Message = "", CategoryNodes = resultList });
        }

        [HttpPost]
        public JsonResult SaveCategorySort(int channelId, string[] categoryList)
        {
            //原始分類
            var categoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);

            List<DealCategoryItem> resultList = new List<DealCategoryItem>();

            //parentId

            foreach (var ca in categoryCol.Where(x => x.ParentType == (int)CategoryType.PponChannel && x.ParentId == channelId).OrderBy(z => z.Seq).ToList())
            {
                resultList.Add(new DealCategoryItem { ParentId = ca.ParentId, ParentName = ca.ParentName, CategoryId = ca.CategoryId, CategoryName = ca.CategoryName, ConsoleName = ca.CategoryName, CategorySeq = ca.Seq, IsShowAddSub = (ca.ParentType != ca.CategoryType), IsShowFrontEnd = ca.IsShowFrontEnd });
                var subcategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
                if (subcategoryCol.Any(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)))
                {
                    resultList.AddRange(subcategoryCol.Where(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, CategoryName = y.CategoryName, ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = (y.ParentType != y.CategoryType), IsShowFrontEnd = y.IsShowFrontEnd }).ToList());
                }
            }



            int parentId = default(int);
            int categoryId = default(int);

            foreach (var item in categoryList)
            {
                var array = item.Split(",");
                parentId = int.Parse(array[0]);
                categoryId = int.Parse(array[1]);

                if (resultList.Any(x => x.ParentId == parentId && x.CategoryId == categoryId))
                {
                    //原有的不處理
                }
                else
                {
                    //new categoryDependency

                    if (channelId == parentId)
                    {
                        //main categoryDependency
                        categoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
                        CategoryDependency categoryDependency = new CategoryDependency();
                        categoryDependency.ParentId = channelId;
                        categoryDependency.CategoryId = categoryId;
                        categoryDependency.Seq = categoryCol.Where(x => x.ParentType == (int)CategoryType.PponChannel && x.ParentId == channelId).OrderByDescending(z => z.Seq).Count() + 1;

                        sp.CategoryDependencySet(categoryDependency);
                    }
                }
            }

            #region 移除的Category
            //移除的Category

            var newCategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);

            List<DealCategoryItem> newResultList = new List<DealCategoryItem>();

            //parentId

            foreach (var ca in newCategoryCol.Where(x => x.ParentType == (int)CategoryType.PponChannel && x.ParentId == channelId).OrderBy(z => z.Seq).ToList())
            {
                newResultList.Add(new DealCategoryItem { ParentId = ca.ParentId, ParentName = ca.ParentName, CategoryId = ca.CategoryId, CategoryName = ca.CategoryName, ConsoleName = ca.CategoryName, CategorySeq = ca.Seq, IsShowAddSub = (ca.ParentType != ca.CategoryType), IsShowFrontEnd = ca.IsShowFrontEnd });
                var subcategoryCol = sp.ViewCategoryDependencyGetByCategoryId((int)CategoryType.DealCategory);
                if (subcategoryCol.Any(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)))
                {
                    newResultList.AddRange(subcategoryCol.Where(x => x.ParentId.Equals(ca.CategoryId) && x.ParentName.Contains(ca.CategoryName)).OrderBy(z => z.Seq).Select(y => new DealCategoryItem { ParentId = y.ParentId, ParentName = y.ParentName, CategoryId = y.CategoryId, CategoryName = y.CategoryName, ConsoleName = y.ParentName + "->" + y.CategoryName, CategorySeq = y.Seq, IsShowAddSub = (y.ParentType != y.CategoryType), IsShowFrontEnd = y.IsShowFrontEnd }).ToList());
                }
            }

            foreach (var result in resultList.Select(x => "" + x.ParentId + "," + x.CategoryId).ToList())
            {
                if (!categoryList.Contains(result))
                {
                    var array = result.Split(",");
                    var col = sp.CategoryDependencyGet(int.Parse(array[0]), int.Parse(array[1]));
                    if (col.Any() && col.Count() == 1 && int.Parse(array[0]) == channelId)
                    {
                        sp.CategoryDependencyDelete(col);
                    }
                }
            }
            #endregion 移除的Category

            return Json(new { Success = true, Message = "分類已更新" });
        }

        [HttpPost]
        public JsonResult RefreshCategory()
        {
            try
            {
                SortedDictionary<string, string> serverIPs =
                    ProviderFactory.Instance().GetSerializer().Deserialize<SortedDictionary<string, string>>(config.ServerIPs);
                string result = SystemFacade.SyncCommand(serverIPs, SystemFacade._RELOAD_CATEGORY, string.Empty, string.Empty);

                return Json(new { Success = true , Message = result });
            }
            catch (Exception ex)
            {
                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult UpdateCategorySort(int channelId, string[] categoryList)
        {
            int parentId = default(int);
            int categoryId = default(int);
            int categorySeq = default(int);

            foreach (var item in categoryList)
            {
                var array = item.Split(",");
                parentId = int.Parse(array[0]);
                categoryId = int.Parse(array[1]);
                categorySeq = int.Parse(array[2]);

                CategoryDependencyCollection col = sp.CategoryDependencyGet(parentId, categoryId);
                if (col.Any() && col.Count == 1)
                {
                    CategoryDependency categorydependency = col.First();
                    if (categorydependency.Seq != categorySeq)
                    {
                        categorydependency.Seq = categorySeq;
                        sp.CategoryDependencySet(categorydependency);
                    }
                }
            }

            return Json(new { Success = true, Message = "分類排序已更新" });
        }

        #endregion ManagerCategory

        private static List<List<KeyValuePair<string, string>>> GetChannelCategoryList(CategoryType categoryType, List<CategoryNode> categoryNodes)
        {
            List<List<KeyValuePair<string, string>>> channelCategoryList = new List<List<KeyValuePair<string, string>>>();

            foreach (var node in categoryNodes)
            {
                List<KeyValuePair<string, string>> dealCategories = new List<KeyValuePair<string, string>>();
                List<CategoryTypeNode> typeNodes = node.NodeDatas.Where(x => x.NodeType == categoryType).ToList();
                if (typeNodes.Count > 0)
                {
                    dealCategories = typeNodes.First().CategoryNodes.Select(x => new KeyValuePair<string, string>(node.CategoryId + "," + x.CategoryId, x.CategoryName)).ToList();
                }
                channelCategoryList.Add(dealCategories);
            }
            return channelCategoryList;
        }

        private static List<KeyValuePair<string, string>> GetChannelRegionList(CategoryType categoryType, List<CategoryNode> categoryNodes, int regionCategoryId)
        {
            List<KeyValuePair<string, string>> channelRegionList = new List<KeyValuePair<string, string>>();

            foreach (var a in CategoryManager.Default.Taipei.NodeDatas.Where(x => x.NodeType == categoryType))
            {
                List<KeyValuePair<string, string>> regionCategories = new List<KeyValuePair<string, string>>();

                regionCategories = a.CategoryNodes.Where(x => x.CategoryType == categoryType).Select(x => new KeyValuePair<string, string>("test," + x.CategoryId, x.CategoryName)).ToList();

                channelRegionList.AddRange(regionCategories);
            }

            return channelRegionList;
        }
    }
}

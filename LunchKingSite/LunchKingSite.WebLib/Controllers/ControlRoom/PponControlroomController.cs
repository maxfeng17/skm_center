﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Caching;
using System.Web;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models.Ppon;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using SubSonic;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    public class PponControlroomController : Controller
    {
        IPponEntityProvider _pep = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        IOrderProvider _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        ILog logger = LogManager.GetLogger(typeof(PponControlroomController));

        #region GoogleFeedReport

        [HttpGet]
        public ActionResult GoogleFeedReport()
        {
            return View();
        }

        [HttpGet]
        public ActionResult DownloadGoogleFeedReport()
        {
            //unicorn到底有多懶啊 -_-|||
            string sql = @"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

select * from (

select vpd.unique_id as 母檔編號, 
format(vpd.business_hour_order_time_e, 'yyyy-MM-dd') as 結檔時間, 
vpd.seller_name as 商家名稱,
vpd.business_hour_guid as 母檔BID,
(mem_sales.last_name + mem_sales.first_name) as 業務名字,
 (
select code_name from system_code where code_group = 'DealType' and code_id = vpd.deal_type
)
 as 銷售分類,
isnull((
	select top 1 pi.gtins from proposal_multi_option_spec pmos with(nolock)
	inner join product_item pi with(nolock) on pi.guid = pmos.item_guid
	where 
	pmos.business_hour_guid = vpd.business_hour_guid
	and pi.gtins <> ''
), '') as GTIN,
isnull((
	select top 1 pi.mpn from proposal_multi_option_spec pmos with(nolock)
	inner join product_item pi with(nolock) on pi.guid = pmos.item_guid
	where 
	pmos.business_hour_guid = vpd.business_hour_guid
	and pi.mpn <> ''
), '') as MPN,
vpd.coupon_usage 母檔名稱,
isnull(p.brand_name, '') as 提案單品牌名稱,
isnull(p.deal_name, '') as 提案單商品名稱,
gsp.approved as google刊登狀態,
gsp.quality_issue as google拒登原因,
(
	select sum(ctl.amount - ctl.discount_amount) from cash_trust_log ctl
	where ctl.business_hour_guid in 
	(
		select cd.BusinessHourGuid from combo_deals cd with(nolock) 
		where cd.MainBusinessHourGuid = vpd.business_hour_guid
		union 
		select vpd.business_hour_guid
	)
	and ctl.status in (1,2)
)
as 扣除折價券營業額,
'' as 扣除折價券毛利額,
sl.seller_contact_person as 商家聯絡人,
sl.seller_tel as 商家聯絡電話,
sl.seller_email as 商家聯絡Email,
format(vpd.business_hour_order_time_s, 'yyyy-MM-dd') as 開檔時間

from view_ppon_deal vpd with(nolock)
left join member mem_sales with(nolock) on mem_sales.unique_id = vpd.develope_sales_id
left join seller sl with(nolock) on sl.GUID = vpd.seller_GUID
left join google_shopping_product gsp with(nolock) on gsp.bid = vpd.business_hour_guid
left join proposal p with(nolock) on p.business_hour_guid = vpd.business_hour_guid
where vpd.business_hour_guid in
(
	select distinct business_hour_guid from deal_time_slot dts with(nolock) where dts.effective_start > @startDate and dts.effective_start < @endDate
)
and vpd.business_hour_status &  2048 = 0
and vpd.business_hour_order_time_s < getdate() and vpd.business_hour_order_time_e > getdate()
and vpd.item_price > 0

) as t 
order by t.扣除折價券營業額 desc
                          ";
            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            DateTime baseDate = DateTime.ParseExact(DateTime.Today.ToString("yyyyMMdd120000"), "yyyyMMddHHmmss", null);
            qc.Parameters.Add("@startDate", baseDate.AddDays(-1), DbType.DateTime);
            qc.Parameters.Add("@endDate", baseDate.AddDays(1), DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);

            MemoryStream ms = DataTableToExcelFile(ds.Tables[0]);
            return File(ms.ToArray(), "application/ms-excel", "GoogleFeedReport.xls");

        }

        public ActionResult OnlineDealDiscountReport()
        {
            List<DealDiscountProperty> dealDiscounts = _pep.GeOnlineDealDiscountProperties();
            if (dealDiscounts.Count == 0)
            {
                List<int> campaignIds = _op.DiscountCampaignIdGetListOnline();
                ViewBag.ErrorMessage = string.Format(
                    "沒有任何符合折扣價資料，目前符合折價的折扣券活動 {0} 筆", campaignIds.Count);
            }

            return View(dealDiscounts);
        }

        [HttpPost]
        public ActionResult UploadReportLiteFile(HttpPostedFileBase[] uploadFile)
        {
            if (uploadFile == null || uploadFile.Length == 0)
            {
                return Json(new { success = false, message = "no file." });
            }
            Sheet sheet;
            try
            {
                HSSFWorkbook workbook = new HSSFWorkbook(uploadFile[0].InputStream);
                //讀取Sheet1 工作表            
                sheet = workbook.GetSheetAt(0);
            }
            catch (Exception)
            {
                return Json(new { success = false, message = "read sheet error." });
            }
            //first row is header
            List<GoogleProductUpdateData> updataItems = new List<GoogleProductUpdateData>();
            for (int i = sheet.TopRow + 1; i <= sheet.LastRowNum; i++)
            {
                var row = sheet.GetRow(i);
                var cellBid = row.GetCell(0);
                var cellBrand = row.GetCell(1);
                var cellGtin = row.GetCell(2);
                var cellMpn = row.GetCell(3);
                if (cellBid != null)
                {
                    Guid bid;
                    if (Guid.TryParse(cellBid.StringCellValue, out bid))
                    {
                        GoogleProductUpdateData item = new GoogleProductUpdateData
                        {
                            Bid = bid
                        };
                        if (cellBrand != null)
                        {
                            item.Brand = cellBrand.ToString();
                            if (item.Brand.Trim() == "無")
                            {
                                item.Brand = string.Empty;
                            }
                        }
                        if (cellGtin != null)
                        {
                            item.Gtin = cellGtin.ToString();
                            if (item.Gtin.Trim() == "無")
                            {
                                item.Gtin = string.Empty;
                            }
                        }
                        if (cellMpn != null)
                        {
                            item.Mpn = cellMpn.ToString();
                            if (item.Mpn.Trim() == "無")
                            {
                                item.Mpn = string.Empty;
                            }
                        }
                        updataItems.Add(item);
                    }
                }
            }
            if (updataItems.Count == 0)
            {
                return Json(new { success = false, message = "沒有需要更新的資料" });
            }
            foreach (var updataItem in updataItems)
            {
                var metadata = _pep.DealGoogleMetadataGet(updataItem.Bid);
                if (metadata == null)
                {
                    metadata = new Core.Models.Entities.DealGoogleMetadata()
                    {
                        Bid = updataItem.Bid
                    };
                }
                metadata.Brand = updataItem.Brand ?? string.Empty;
                metadata.Gtin = updataItem.Gtin ?? string.Empty;
                metadata.Mpn = updataItem.Mpn ?? string.Empty;
                _pep.DealGoogleMetadataSet(metadata);
            }


            return Json(new { success = true, message = "更新 " + updataItems.Count + " 筆資料." });
        }

        [HttpGet]
        [RolePage]
        public ActionResult HomePageEditor()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHomepageBlocks()
        {
            IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
            List<HomepageBlock> blocks = pep.GetLatestHomepageBlocks();            
            List<HomepageBlockSetupModel> items = new List<HomepageBlockSetupModel>();
            List<string> errorMessages = new List<string>();
            foreach (HomepageBlock t in blocks)
            {
                dynamic content = JsonConvert.DeserializeObject(t.Content);
                List <HomepageBlockSetupDeal> deals = new List<HomepageBlockSetupDeal>();
                if (t.TypeId == 4 || t.TypeId == 5)
                {
                    List<string> validBids = new List<string>();
                    if (content != null && content.deals != null)
                    {
                        foreach (string bid in content.deals)
                        {
                            Guid theBid;
                            if (Guid.TryParse(bid, out theBid) == false)
                            {
                                errorMessages.Add(string.Format("檔次{0}無法解析，被移出清單", bid));
                                continue;
                            }
                            if (PponFacade2.CheckOnlineAndAvailable(theBid) == false)
                            {
                                errorMessages.Add(string.Format("[{0}]檔次{1}因售完或下檔，被移出清單", t.Title, bid));
                                continue;
                            }
                            var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(Guid.Parse(bid), true);
                            deals.Add(new HomepageBlockSetupDeal()
                            {
                                imagePath = PponFacade.GetPponDealFirstImagePath(deal.AppDealPic),
                                title = deal.ItemName,
                                price = deal.ItemPrice,
                                bid = deal.BusinessHourGuid.ToString(),
                            });
                            validBids.Add(bid);
                        }
                        content.deals =new JArray(validBids);
                    }
                    if (t.Enabled && validBids.Count < 8)
                    {
                        errorMessages.Add(string.Format("[{0}]有效檔次少於8，請補充", t.Title));
                    }
                }

                items.Add(new HomepageBlockSetupModel
                {
                    type = t.TypeId,
                    title = t.Title,
                    content = content,
                    enabled = t.Enabled,
                    sequence = t.Sequence,
                    guid = t.DocGuid,
                    deals = deals
                });
            }
            string jsonStr = JsonConvert.SerializeObject(new
            {
                pageBlocks = items,
                errorMessages = errorMessages
            });
            return Json(jsonStr);
        }

        [AjaxCall]
        [HttpPost]
        public ActionResult SaveHomepageBlocks(HomepageBlockSetupModel model)
        {
            if (CommonFacade.IsInSystemFunctionPrivilege(
                User.Identity.Name, SystemFunctionType.Read, "/controlroom/ppon/SaveHomepageBlocks") == false)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "異動失敗，沒有修改的權限"
                });
            }
            IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
            bool result = pep.SaveHomepageBlocks(new HomepageBlock
            {
                DocGuid = model.guid,
                Latest = true,
                Title = model.title,
                CreateId = this.User.Identity.Name,
                CreateTime = DateTime.Now,
                TypeId = model.type,
                Sequence = model.sequence,
                Enabled = model.enabled,
                Content = model.contentJson,
            });
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = result
            });
        }

        [AjaxCall]
        [HttpPost]
        public ActionResult ClearHomepageBlocksCache()
        {
            //先清本機
            string indexCacheKey = string.Format("page://m-home/{0}", DateTime.Now.ToString("yyyyMMddHH"));
            MemoryCache.Default.Remove(indexCacheKey);
            string cacheKey = string.Format("api://GetHomepage/{0}", DateTime.Now.ToString("yyyyMMddHH"));
            MemoryCache.Default.Remove(cacheKey);
            //清其它機器
            string message = SystemFacade.ClearCacheByObjectNameAndCacheKey("HomepageBlocks", "all");
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = message
            });
        }

        private MemoryStream DataTableToExcelFile(DataTable dt)
        {
            //建立Excel 2003檔案
            HSSFWorkbook wb = new HSSFWorkbook();
            Sheet ws;


            if (dt.TableName != string.Empty)
            {
                ws = wb.CreateSheet(dt.TableName);
            }
            else
            {
                ws = wb.CreateSheet("Sheet1");
            }

            ws.CreateRow(0);//第一行為欄位名稱
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                ws.GetRow(0).CreateCell(i).SetCellValue(dt.Columns[i].ColumnName);
            }

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ws.CreateRow(i + 1);
                for (int j = 0; j < dt.Columns.Count; j++)
                {
                    var cell = ws.GetRow(i + 1).CreateCell(j);
                    cell.CellStyle.WrapText = true;
                    cell.SetCellValue(dt.Rows[i][j].ToString());
                }
            }
            MemoryStream ms = new MemoryStream();
            wb.Write(ms);
            return ms;
        }


        #region 企業帳號購物金

        [HttpGet]
        [RolePage("/controlroom/ppon/enterprisemember", SystemFunctionType.Read)]
        public ActionResult EnterpriseMember()
        {
            return View();
        }

        [HttpPost]
        [AjaxCall]
        [RolePage("/controlroom/ppon/enterprisemember", SystemFunctionType.Read)]
        public ActionResult GetEnterpriseMembers()
        {
            List<EnterpriseMember> eeMembers = _pep.GetEnterpriseMembers();


            List<EnterpriseMembeModel> memberViews = new List<EnterpriseMembeModel>();
            foreach (var eeMember in eeMembers)
            {
                Member mem = MemberFacade.GetMember(eeMember.UserId);
                if (mem.IsLoaded == false)
                {
                    continue;
                }

                decimal scashE7;
                decimal scashPez;
                decimal scash = OrderFacade.GetSCashSum2(mem.UniqueId, out scashE7, out scashPez);

                memberViews.Add(new EnterpriseMembeModel
                {
                    userName = mem.UserName,
                    displayName = mem.DisplayName,
                    scashAmount = (int)scashE7,
                    scashAmountStr = scashE7.ToString("#,0"),
                    deleted = eeMember.IsDeleted,
                    id = eeMember.Id
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = memberViews
            });
        }
        [HttpPost]
        [AjaxCall]
        public ActionResult AddEnterpriseMember(AddEnterpriseUsersInput input)
        {
            var member = MemberFacade.GetMember(input.Account.Trim());
            if (member == null || member.IsLoaded == false)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "找不到會員"
                });
            }
            if (_pep.GetEnterpriseMemberByUserId(member.UniqueId) != null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "無法重複新增"
                });
            }
            _pep.SaveEnterpriseMember(
                new EnterpriseMember
                {
                    CreateId = User.Identity.Name,
                    DeletedTime = null,
                    IsDeleted = false,
                    UserId = member.UniqueId
                }
            );
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }
        [HttpPost]
        [AjaxCall]
        [RolePage("/controlroom/ppon/enterprisemember", SystemFunctionType.Read)]
        public ActionResult DeleteEnterpriseMember(DeleteEnterpriseUsersInput input)
        {
            _pep.MarkEnterpriseMemberDeleted(input.id, User.Identity.Name);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }
        [HttpPost]
        [AjaxCall]
        [RolePage("/controlroom/ppon/enterprisemember", SystemFunctionType.Read)]
        public ActionResult ViewEnterpriseDepisitLogs(ViewEnterpriseDepisitLogInput input)
        {
            List<EnterpriseDepisitLog> logs = new List<EnterpriseDepisitLog>();
            var depositOrders = _pep.GetEnterpriseDepisitOrders(input.id);
            foreach(var depositOrder in depositOrders)
            {
                logs.Add(new EnterpriseDepisitLog
                {
                    time = depositOrder.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                    action = "加值",
                    amount = depositOrder.Amount.GetValueOrDefault().ToString("0,0")
                });
                var withdrawalOrders = _pep.GetEnterpriseWithdrawalOrders(depositOrder.Id);
                foreach (var withdrawalOrder in withdrawalOrders)
                {
                    logs.Add(new EnterpriseDepisitLog
                    {
                        time = withdrawalOrder.CreateTime.ToString("yyyy-MM-dd HH:mm"),
                        action = "收回",
                        amount = withdrawalOrder.Amount.GetValueOrDefault().ToString("0,0")
                    });
                }
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = logs
            });
        }

        [HttpPost]
        [AjaxCall]
        [RolePage("/controlroom/ppon/enterprisemember", SystemFunctionType.Read)]
        public ActionResult CreateDepositOrder(CreateDepositOrderInput input)
        {
            if (input == null || input.id == 0 || input.amount <= 0)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                });
            }
            var eem = _pep.GetEnterpriseMember(input.id);
            if (eem == null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                });
            }            
            var mem = MemberFacade.GetMember(eem.UserId);

            try
            {
                if (MemberFacade.ImportScash(mem.UniqueId, input.amount, "企業帳號加值購物金"))
                {
                    _pep.SaveEnterpriseDepisitOrder(eem.Id, input.amount, eem.UserId, User.Identity.Name);
                }

                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "加值完成"
                });
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("匯入購生金發生錯誤, eemId={0}, amount={1}, ex={2}",
                    input.id, input.amount, ex);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "發生錯誤"
                });
            }
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult CreateWithdrawalOrder(CreateWithdrawalOrderInput input)
        {
            if (input == null || input.id == 0 || input.amount <= 0)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                });
            }
            var eem = _pep.GetEnterpriseMember(input.id);
            if (eem == null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "參數錯誤"
                });
            }
            var mem = MemberFacade.GetMember(eem.UserId);
            decimal scashE7;
            decimal scashPez;
            decimal scash = OrderFacade.GetSCashSum2(mem.UniqueId, out scashE7, out scashPez);
            if (scashE7 < input.amount)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "回收金額超過可用購物金"
                });
            }
            try
            {
                if (MemberFacade.ExportScash(mem.UniqueId, input.amount, "企業帳號回收購物金", User.Identity.Name))
                {
                    _pep.SaveEnterpriseWithdrawalOrder(eem.Id, input.amount, eem.UserId, User.Identity.Name);
                }

                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "回收完成"
                });
            }
            catch (Exception ex)
            {
                logger.ErrorFormat("匯入購生金發生錯誤, eemId={0}, amount={1}, ex={2}",
                    input.id, input.amount, ex);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "發生錯誤"
                });
            }
        }


        #endregion

        #region  models

        protected class GoogleProductUpdateData
        {
            public Guid Bid { get; set; }
            public string Brand { get; set; }
            public string Gtin { get; set; }
            public string Mpn { get; set; }
        }

        public class AddEnterpriseUsersInput
        {
            public string Account { get; set; }
        }

        public class DeleteEnterpriseUsersInput
        {
            public int id { get; set; }
        }

        public class EnterpriseMembeModel
        {
            public int id { get; set; }
            public string userName { get; set; }
            public string displayName { get; set; }
            public int scashAmount { get;  set; }
            public string scashAmountStr { get; set; }
            public bool deleted { get; set; }
            
        }

        public class ViewEnterpriseDepisitLogInput
        {
            public int id { get; set; }
        }

        public class CreateDepositOrderInput
        {
            public int id { get; set; }
            public int amount { get; set; }
        }

        public class CreateWithdrawalOrderInput
        {
            public int id { get; set; }
            public int amount { get; set; }
        }

        public class EnterpriseDepisitLog
        {
            public string action { get; set; }
            public string amount { get; set; }
            public string time { get; set; }
        }

        #endregion

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class OAuthController : Controller
    {
        protected static IMemberProvider Mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        protected static ISellerProvider Sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();

        public ActionResult ListApp()
        {
            List<OAuthClientModel> clients = OAuthFacade.GetClientList();
            ViewBag.Items = clients;
            ViewBag.Message = TempData["Message"];
            return View();
        }

        public ActionResult CreateApp()
        {
            return View();
        }

        [HttpPost]
        public ActionResult CreateApp(string name, string owner, string returnUrl)
        {
            OAuthFacade.AddClient(name, owner, returnUrl);
            return RedirectToAction("ListApp");
        }

        [HttpPost]
        public JsonResult GetVbsMembershipUserIdForAccountId(string accountId)
        {
            if (!string.IsNullOrEmpty(accountId))
            {
                var vbsMembership = Mp.VbsMembershipGetByAccountId(accountId);
                if (vbsMembership.IsLoaded)
                {
                    return Json(new { Success = true, UserId= vbsMembership.UserId.ToString() });
                }
            }
            return Json(new { Success = false, Message = "找不到對應的userid" });
        }

        [HttpPost]
        public JsonResult CheckVbsMembershipUserId(int userId)
        {
            var vbsMembership = Mp.VbsMembershipGetByUserId(userId);
            return vbsMembership.IsLoaded ? Json(new { Success = true }) : Json(new { Success = false, Message = "找不到此userid" });
        }

        public ActionResult EditScope(string appId)
        {
            var client = OAuthFacade.GetClient(appId);
            if (client == null)
            {
                ViewBag.Data = new ErrorMessage
                {
                    Title = "找不到資料..."
                };
            }
            else
            {
                var sellerCol=Sp.SellerGetListByAppId(appId);
                if (sellerCol.Any())
                {
                    ViewBag.SellerGuid = sellerCol.Select(x=>x.Guid).FirstOrDefault();
                }                   
                ViewBag.Client = client;                
                TokenScope[] ClientScopes = client.Permissions.Select(t => t.ScopeEnum).ToArray();

                ViewBag.AllScopes = (from TokenScope t in Enum.GetValues(typeof(TokenScope))
                                     select new HtmlOption { Text = t.ToString(), Value = ((int)t).ToString(), Checked = ClientScopes.Contains(t) })
                                      .ToArray();
            }
            return View();
        }

        [HttpPost]
        public ActionResult EditScope(string appId, string name, string description, string returnUrl, TokenScope[] scopes, decimal? mingrossmargin,
            decimal? generalgrossmargin, decimal? generalcoupongrossmargin, decimal? generaldeliverygrossmargin, decimal? mincommission
            , decimal? generalcommission, string checkoutday, int checktype, string checkcycle, int? sellerUserId, string sellerGuid, string usingTokenToGustId
            , string groupCoupon, string pezGroupCoupon, string famiGroupCoupon, string hiLifeGroupCoupon)
        {
            var client = OAuthFacade.GetClient(appId);
            var channelClientProperty = OAuthFacade.GetChannelClientProperty(appId);

            if (client != null)
            {
                //熟客卡綁定賣家GUID                
                if (!string.IsNullOrEmpty(sellerGuid))
                {
                    var updateSellerGuid = Guid.Parse(sellerGuid);
                    var seller= Sp.SellerGet(updateSellerGuid);
                    if(seller.IsLoaded)
                    {
                        var sellerCol = Sp.SellerGetListByAppId(appId);
                        if(sellerCol.Any())
                        {
                            foreach(var item in sellerCol)
                            {
                                if (item.Guid != updateSellerGuid)
                                {
                                    item.OauthClientAppId = "";
                                    Sp.SellerSet(item);
                                }                                
                            }                            
                        }
                        seller.OauthClientAppId = appId;
                        Sp.SellerSet(seller);
                    }
                    else
                    {
                        TempData["Message"] = " 找不到此賣家Guid!";
                    }
                    ViewBag.SellerGuid = seller.Guid;
                }

                client.ReturnUrl = returnUrl;
                client.Name = name;
                client.Description = description;
                if (channelClientProperty != null)
                {
                    channelClientProperty.MinGrossMargin = mingrossmargin / 100;
                    channelClientProperty.GeneralGrossMargin = generalgrossmargin / 100;
                    channelClientProperty.MinCommission = mincommission / 100;
                    channelClientProperty.GeneralCommission = generalcommission / 100;
                    channelClientProperty.CheckoutDay = checkoutday;
                    channelClientProperty.CheckType = checktype;
                    channelClientProperty.CheckCycle = checkcycle;
                    channelClientProperty.SellerUserId = sellerUserId;
                    channelClientProperty.GeneralCouponGrossMargin = generalcoupongrossmargin / 100;
                    channelClientProperty.GeneralDeliveryGrossMargin = generaldeliverygrossmargin / 100;
                    channelClientProperty.UsingTokenToGuestId = usingTokenToGustId == "on";
                    channelClientProperty.GroupCoupon = groupCoupon == "on";
                    channelClientProperty.PezGroupCoupon = pezGroupCoupon == "on";
                    channelClientProperty.FamiGroupCoupon = famiGroupCoupon == "on";
                    channelClientProperty.HiLifeGroupCoupon = hiLifeGroupCoupon == "on";
                }
                if (client.Permissions == null)
                {
                    client.Permissions = new OauthClientPermissionCollection();
                }
                else
                {
                    client.Permissions.Clear();
                }
                if (scopes != null)
                {
                    foreach (TokenScope scope in scopes)
                    {
                        client.Permissions.Add(new OauthClientPermission
                        {
                            ClientId = client.Id,
                            ScopeEnum = scope
                        });
                    }
                }
                OAuthFacade.UpdateClient(client, channelClientProperty);
                TempData["Message"] = client.Name + " 資料已更新.";
            }
            return RedirectToAction("ListApp");
        }

        public ActionResult ResetAppSecret(string appId)
        {
            OAuthFacade.RenewClientSecret(appId);
            return RedirectToAction("ListApp");
        }

        [HttpPost]
        public ActionResult GetLongLivedToken(string clientId, bool reset)
        {
            string token = OAuthFacade.RefreshClientTokenAndCancelOlders(clientId, reset);
            return Json(token);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Net.Mail;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.SellerModels;
using LunchKingSite.WebLib.Models.Category;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using SubSonic;
using SMS = LunchKingSite.BizLogic.Component.SMS;

namespace LunchKingSite.WebLib.Controllers
{
    [RolePage]
    public class PartnerReportingSystemController : BaseController
    {
        private IPponProvider _pp;
        private ISysConfProvider _cp;
        public PartnerReportingSystemController()
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _cp = ProviderFactory.Instance().GetConfig();
        }

        #region Action

        public enum MessageType
        {
            SMS = 0,
            EMail = 1,            
        }

        #region 服務商列表
        public ActionResult PartnerList(int? page, string message)
        {
            List<string> filter = new List<string>();
            int totalLogCount = _pp.RelatedSystemPartnerGetCount(filter.ToArray());

            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            int pagnumber = Convert.ToInt16(page);
            ViewBag.PageIndex = pagnumber = (pagnumber == 0) ? 1 : pagnumber;
            ViewBag.EventList = _pp.RelatedSystemPartnerGetByPage(pagnumber, 10, RelatedSystemPartner.Columns.Id, filter.ToArray());
            ViewBag.message = message;
            return View();
        }

        [HttpPost]
        public ActionResult PartnerList(RelatedSystemPartner item, bool Pstatus)
        {
            item.Status = Pstatus;
            _pp.RelatedSystemPartnerSet(item);
            return RedirectToAction("PartnerList", new { message = "新增完成" });
        }

        [HttpPost]
        public ActionResult PartnerEdit(int p_id, string p_name, string p_contact, string p_mobile, string p_email, bool p_status, int? pp)
        {
            RelatedSystemPartner PartnerData = _pp.RelatedSystemPartnerGet(p_id);

            PartnerData.PartnerName = p_name;
            PartnerData.PartnerContact = p_contact;
            PartnerData.PartnerMobile = p_mobile;
            PartnerData.PartnerEmail = p_email;
            PartnerData.Status = p_status;

            _pp.RelatedSystemPartnerSet(PartnerData);

            if (pp != null && pp > 0)
            {
                return RedirectToAction("PartnerList", new { page = pp });
            }
            else
                return RedirectToAction("PartnerList");
        }
        #endregion 服務商列表

        #region 通知服務商
        public ActionResult Notification(int? page, string message,string search)
        {
            string filter = string.Empty;
            if (search != null  && search !="")
            {
                filter = RelatedSystemPartner.Columns.PartnerName + " like %" + search + "%";
                ViewBag.search = search;
            }
            int totalLogCount = _pp.RelatedSystemPartnerGetCount(filter);

            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            int pagnumber = Convert.ToInt16(page);
            ViewBag.PageIndex = pagnumber = (pagnumber == 0) ? 1 : pagnumber;
            ViewBag.EventList = _pp.RelatedSystemPartnerGetByPage(pagnumber, 10, RelatedSystemPartner.Columns.Id, filter);
            ViewBag.message = message;
            return View();
        }

        [HttpPost]
        public ActionResult MessageSend(string p_list, int p_type, string p_content)
        {
            string message=string.Empty;
            RelatedSystemPartnerCollection partnerdata;

            string VaildAllFilter = RelatedSystemPartner.Columns.Status + "=true";
            if (p_list == "All")
            {
                partnerdata = _pp.RelatedSystemPartnerGetList(VaildAllFilter);
            }
            else
            {
                partnerdata = _pp.RelatedSystemPartnerGetList(RelatedSystemPartner.Columns.Id + " in (" + p_list + ")");
            }

            RelatedSystemPartnerLog partnerlog;
            foreach (RelatedSystemPartner pdata in partnerdata)
            {
                partnerlog = new RelatedSystemPartnerLog();
                partnerlog.PartnerId = pdata.Id;
                partnerlog.MessageType =p_type;
                partnerlog.MessageContent = p_content;
                partnerlog.CreateId = this.UserName;
                partnerlog.CreateTime = DateTime.Now;
                partnerlog.Status = 0;
                                
                try
                {
                    _pp.RelatedSystemPartnerLogSet(partnerlog);
                    string Subject = "服務商通告";
                    //發送SMS
                    if (p_type == 0) {                        
                        SMS sms = new SMS();
                        sms.SendMessage(Subject, p_content, "", pdata.PartnerMobile, this.UserName);

                        partnerlog.Status = 1;
                        _pp.RelatedSystemPartnerLogSet(partnerlog);
                        message = "簡訊發送完畢";
                    }
                    else if (p_type ==1) //發送EMAIL
                    {
                        MailMessage msg = new MailMessage();
                        msg.From = new MailAddress(_cp.SystemEmail);
                        msg.To.Add(pdata.PartnerEmail);
                        msg.Subject = Subject;
                        msg.Body = p_content;
                        msg.IsBodyHtml = true;
                        PostMan.Instance().Send(msg, SendPriorityType.Normal);

                        partnerlog.Status = 1;
                        _pp.RelatedSystemPartnerLogSet(partnerlog);
                        message = "Email發送完畢";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("資料未設定完整:" + ex.Message);
                }
            }


            return RedirectToAction("Notification", new {message = message });
        }
        #endregion 通知服務商

        #endregion Action
    }
}

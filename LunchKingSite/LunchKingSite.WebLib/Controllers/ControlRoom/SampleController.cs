﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Models.SampleTestData;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RequireHttps]
    public class SampleController : BaseController
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        [HttpGet]
        public ActionResult Binary(int n)
        {
            int number = 1;
            List<int> selecedNumber = new List<int>();
            while(n > 0)
            {
                if((n & number) > 0)
                {
                    selecedNumber.Add(number);
                    n = n - number;
                }
                number = number * 2;

                if(number > int.MaxValue)
                {
                    break;
                }
            }
            return Json(selecedNumber, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Bootstrap3()
        {
            return View();
        }


        #region 建立PCP測試資料

        [HttpGet]
        public ActionResult PCPTest()
        {
            ViewBag.SearchRegularsCard = config.SiteUrl + "/ControlRoom/vbs/membership/SearchRegularsCard";
            return View();
        }

        public JsonResult GenAccessToken(LoginInfo loginData)
        {
            var result = new ApiResult() { Code = ApiResultCode.Success };
            IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

            string userName = loginData.UserName.ToUpper();
            string clientId = loginData.ClientId;
            int userId = mp.VbsMembershipGetByAccountId(userName).UserId.Value;
            string code = OAuthFacade.CreateAuthorizeCode(userId, clientId);
            LunchKingSite.BizLogic.Model.OAuth.TokenResponse accesstoken = OAuthFacade.ExchangeToken(code);
            result.Data = LunchKingSite.BizLogic.Model.OAuth.ApiTokenResponse.GetApiTokenResponse(accesstoken);

            return Json(new { Code = (int)ApiResultCode.Success, Data = result.Data, UserId = userId });
        }

        public JsonResult VbsStores(UserInfo info)
        {
            var result = new ApiVersionResult();

            var allowStores = VBSFacade.GetVBSMemberAllowStoreList(info.UserId);
            List<StoreModel> rtnList = new List<StoreModel>();
            foreach (var allowStore in allowStores)
            {
                rtnList.Add(new StoreModel(allowStore));
            }
            result.Data = rtnList;

            return Json(new { Code = (int)result.Code, Data = result.Data });
        }

        public JsonResult CreateIdentityCode(IdentityModel inputData)
        {
            //create identity
            var cardId = inputData.CardId;
            var identityUserid = inputData.IdentityUserid;
            var identity = BonusFacade.GenIdentityCode(cardId, inputData.UserId, DateTime.Now, identityUserid, 100);

            var result = new ApiResult() { Code = ApiResultCode.Success };
            result.Data = identity;
            return Json(new { Code = (int)result.Code, Data = result.Data, Message = result.Message });
        }

        public JsonResult CreateDiscountCampaign(UserInfo inputData)
        {
            var insertOK = new PcpAssignmentUtility(BonusFacade.CreateDiscountCampaign4PCPTest(inputData.UserId)).InsertPcpAssignmentMember();
            var result = new ApiResult() { Code = ApiResultCode.Success };
            if (!insertOK)
            {
                result.Code = ApiResultCode.SaveFail;
                result.Message = "產券錯誤，請手動把任務刪除。";
            }

            return Json(new { Code = (int)result.Code, Data = result.Data, Message = result.Message });
        }



        #endregion
    }
}

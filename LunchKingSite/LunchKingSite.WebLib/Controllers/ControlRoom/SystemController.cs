﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.WebLib.Models.ControlRoom.System;
using Newtonsoft.Json.Linq;
using LunchKingSite.WebLib.Models.ControlRoom.Seller;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using System.Text.RegularExpressions;
using MongoDB.Driver.Linq;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class SystemController : ControllerExt
    {
        private ICmsProvider cp;
        private IOrderProvider op;
        private IPponProvider pp;
        private IMemberProvider mpr;
        private IMarketingProvider mp;
        private ISysConfProvider config;
        private ISkmProvider _skm;
        private ISellerProvider _sp;
        private IChannelProvider _ch;
        private ISystemProvider _sys;
        private IFamiportProvider fami;
        private IPponEntityProvider ppe;

        public SystemController()
        {
            cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
            op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            mpr = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            mp = ProviderFactory.Instance().GetProvider<IMarketingProvider>();
            config = ProviderFactory.Instance().GetConfig();
            _sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
            _ch = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            _sys = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
            ppe = ProviderFactory.Instance().GetProvider<IPponEntityProvider>();
        }

        public ActionResult ChoiceDealSetting()
        {
            var systemData = _sys.SystemDataGet("ChoiceDealSetting");
            ApiChoiceDealSettingModel model = new JsonSerializer().Deserialize<ApiChoiceDealSettingModel>(systemData.Data);

            List<int> channelList = new List<int>();
            channelList.Add(CategoryManager.Default.Delivery.CategoryId);
            channelList.Add(CategoryManager.Default.PponDeal.CategoryId);
            channelList.Add(CategoryManager.Default.Travel.CategoryId);
            channelList.Add(CategoryManager.Default.Beauty.CategoryId);
            channelList.Add(CategoryManager.Default.Family.CategoryId);

            ViewBag.CagegoryList = channelList;

            return View(model);
        }
        [HttpPost]
        public ActionResult SaveChoiceDealSetting(SaveChoiceDealSettingModel model)
        {
            var provider = ProviderFactory.Instance().GetDefaultProvider<ISystemProvider>();
            var sysData =
                provider.SystemDataGet("ChoiceDealSetting");
            if (sysData.IsLoaded)
            {
                sysData.Data = JsonConvert.SerializeObject(model);
                sysData.ModifyTime = DateTime.Now;
                sysData.ModifyId = this.User.Identity.Name;
                provider.SystemDataSet(sysData);
            }
            return Json(new { success = 1});
        }

        #region 策展BannerSort
        public ActionResult CurationBannerSort(int? channelId)
        {

            //取頻道
            CurationBannerSortModel sort = new CurationBannerSortModel();


            List<PponCity> cities = PponCityGroup.DefaultPponCityGroup.GetPponCityForSetting();
            string[] cityUnwant = {
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn24Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.ArriveIn72Hrs.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.PEZ.CityCode,
                                      PponCityGroup.DefaultPponCityGroup.Tmall.CityCode };
            cities = cities.Where(x => !cityUnwant.Any(y => y.Equals(x.CityCode, StringComparison.OrdinalIgnoreCase))).ToList();

            sort.cities = cities;


            //取頻道底下的banner
            if (channelId != null)
            {

                ViewCmsRandomCollection resultList = cp.GetViewCmsRandomCollectionByCityIdDateNoPast("curation",DateTime.Now, (int)channelId, RandomCmsType.PponMasterPage);
                ViewBag.CurationBannerItems = resultList;
                ViewBag.channelId = channelId;
            }


            return View(sort);
        }


        [HttpPost]
        public JsonResult UpdateCurationBannerSort(int channelId, string[] categoryList)
        {
            int BannerId = default(int);
            int BannerSeq = default(int);
            
            foreach (var item in categoryList)
            {
                var array = item.Split(",");
                BannerId = int.Parse(array[0]);
                BannerSeq = int.Parse(array[1]);

                CmsRandomCity city = new CmsRandomCity();
                city.Seq = BannerSeq;
                city.Id = BannerId;
                cp.CmsRandomSortUpdate(city);
            }
            HttpRuntime.Cache.Remove("curation");

            return Json(new { Success = true, Message = "分類排序已更新" });
        }

        public ActionResult DefaultCurationSortedSet(string msg)
        {
            Dictionary<int, string> EventDictionary = new Dictionary<int, string>();
            string dataName = "DefaultCurationTwoSortedEventIds";
            var data = _sys.SystemDataGet(dataName);
            var sortedEventIds = Helper.SplitToIntList(data.Data);
            foreach (var id in sortedEventIds)
            {
                if (!EventDictionary.ContainsKey(id))
                {
                    var brand = pp.GetBrand(id);
                    EventDictionary.Add(id, brand.BrandName);
                }      
            }
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.msg = msg;
            }
            ViewBag.EventIdList = EventDictionary;
            return View();
        }
        [HttpPost]
        public ActionResult CurationSortedSave(string curationSortedStr)
        {
            if (string.IsNullOrEmpty(curationSortedStr))
            {
                return Redirect("/ControlRoom/System/DefaultCurationSortedSet?msg=更新失敗");
            }
            string dataName = "DefaultCurationTwoSortedEventIds";
            var data = _sys.SystemDataGet(dataName);
            data.Data = curationSortedStr;
            data.ModifyId = UserName;
            data.ModifyTime = DateTime.Now;
            _sys.SystemDataSet(data);
            return Redirect("/ControlRoom/System/DefaultCurationSortedSet?msg=更新成功");
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetBrandName(int brandId)
        {
            var brand = pp.GetBrand(brandId);
            return Json(brand.BrandName);
        }

        #endregion

        #region 銀行分行列表

        public ActionResult BankInfoList(string bankNo, string search)
        {
            ViewBag.Banks = op.BankInfoGetMainList();
            if (string.IsNullOrWhiteSpace(search))
            {
                ViewBag.Branches = op.BankInfoGetBranchList(bankNo);
            }
            else
            {
                List<BankInfo> branches = new List<BankInfo>();
                BankInfo branch = op.BankInfoGetByBranch(bankNo, search);
                if (branch.IsLoaded)
                {
                    branches.Add(branch);
                }
                ViewBag.Branches = branches;
            }
            return View();
        }

        public ActionResult BankInfoEdit(int id)
        {
            if (id == 0)
            {
                // add mode
                ViewBag.Banks = op.BankInfoGetMainList();
            }
            ViewBag.Branch = op.BankInfoGetById(id);
            return View();
        }

        [HttpPost]
        public ActionResult BankInfoEdit(int id, BankInfo model)
        {
            model.BranchNo = model.BranchNo.Trim();
            BankInfo origin = op.BankInfoGetById(id);
            if (!origin.IsLoaded)
            {
                origin.BankNo = model.BankNo;
                origin.BankName = model.BankName;
            }
            if (origin.BranchNo != model.BranchNo)
            {
                BankInfo branch = op.BankInfoGetByBranch(origin.BankNo, model.BranchNo);
                if (branch.Id != 0 && branch.Id != id)
                {
                    ViewBag.Banks = op.BankInfoGetMainList();
                    ViewBag.Branch = origin;
                    ViewBag.ErrorMessage = string.Format("分行代號 {0} 已存在!! 分行名稱: {1}", branch.BranchNo, branch.BranchName);
                    return View();
                }
            }
            origin.BranchNo = model.BranchNo;
            origin.BranchName = model.BranchName;
            origin.Address = model.Address;
            origin.Telephone = model.Telephone;
            op.BankInfoSet(origin);

            ViewBag.ErrorMessage = string.Empty;
            return RedirectToAction("BankInfoList", new
            {
                bankNo = origin.BankNo,
                search = string.Empty
            });
        }

        public ActionResult BankEdit(int id)
        {
            if (id == 0)
            {
                // add mode
                ViewBag.Banks = op.BankInfoGetMainList();
            }
            ViewBag.Branch = op.BankInfoGetById(id);
            return View();
        }

        [HttpPost]
        public ActionResult BankEdit(int id, BankInfo model)
        {
            model.BankNo = model.BankNo.Trim();
            BankInfo origin = op.BankInfoGetById(id);
            BankInfo bank = op.BankInfoGet(model.BankNo);
            if (bank.Id != 0 && bank.Id != id)
            {
                ViewBag.Banks = op.BankInfoGetMainList();
                ViewBag.Branch = origin;
                ViewBag.ErrorMessage = string.Format("銀行代號 {0} 已存在!! 銀行名稱: {1}", bank.BankNo, bank.BankName);
                return View();
            }

            origin.BankNo = model.BankNo;
            origin.BankName = model.BankName;
            origin.Address = model.Address;
            origin.Telephone = model.Telephone;
            op.BankInfoSet(origin);

            ViewBag.ErrorMessage = string.Empty;
            return RedirectToAction("BankInfoList", new
            {
                bankNo = origin.BankNo,
                search = string.Empty
            });
        }

        #endregion

        #region 簡訊發送平台

        [RolePage]
        public ActionResult SmsSend(string content, string phone_list, SmsSystem provider = SmsSystem.Undefined)
        {
            if (string.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                return this.Content("尚未登入!");
            }

            if (string.Equals("POST", HttpContext.Request.HttpMethod))
            {
                string sms_content = System.Text.RegularExpressions.Regex.Replace(content, @"\t|\n|\r", string.Empty);
                string[] phone_list_array = phone_list.Trim().Split("\r\n");
                SMS sms = new SMS();
                if (provider == SmsSystem.iTe2)
                {
                    //iTe2 not support multiple number send the same time
                    foreach(string phone in phone_list_array)
                    {
                        sms.SendMessage(string.Empty, sms_content.Trim(), DateTime.Now.ToString(), new string[] { phone }, this.HttpContext.User.Identity.Name, SmsType.System, new Guid(), "0", provider);
                    }
                }
                else
                {
                    sms.SendMessage(string.Empty, sms_content.Trim(), DateTime.Now.ToString(), phone_list_array, this.HttpContext.User.Identity.Name, SmsType.System, new Guid(), "0", provider);
                }

            }
            return View();
        }
        #endregion

        #region TSPG TEST

        [RolePage]
        public ActionResult PaymentRequestTest(string oid, string submitButton)
        {
            Guid orderGuid;
            PaymentRequestTestModel model = new PaymentRequestTestModel();
            string errMsg = "";
            if (!string.IsNullOrEmpty(oid) && Guid.TryParse(oid,out orderGuid))
            {

                //只做pincode卡在退貨中
                Order o = op.OrderGet(orderGuid);
                List<Guid> list = new List<Guid>();
                list.Add(orderGuid);
                List<CashTrustLog> ctlc = mpr.GetCashTrustLogCollectionByOid(list).ToList();
                var bid = mpr.GetCouponListMainByOid(orderGuid).BusinessHourGuid;
                var famiEvent = fami.GetFamilyNetEvent(bid);
                var pincodes = fami.GetFamilyNetListPincode(orderGuid).Where(x => (x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Returning) && !x.IsVerified && !x.IsVerifying).ToList();

                OrderFacade.FamilyNetPincodeListReturn(ctlc, pincodes, o, bid, famiEvent.ActCode, out errMsg, FamilyNetReturnType.JobRetry);
                model.result = errMsg;
            }

            return View(model);
        }

        

        #endregion

        #region 搜尋關鍵字列表

        public ActionResult KeywordList(string search)
        {
            if (!string.IsNullOrWhiteSpace(search))
            {
                ViewBag.Keywords = pp.KeywordGetList(search);
                return View();
            }
            else
            {
                if (search != null)
                {
                    ViewBag.ErrorMessage = "請輸入搜尋條件!!";
                }
                return View();
            }
        }

        public ActionResult KeywordEdit(int id, string search)
        {
            ViewBag.Keyword = pp.KeywordGet(id);
            return View();
        }

        public ActionResult KeywordDelete(int id, string search)
        {
            pp.KeywordDelete(id);

            return RedirectToAction("KeywordList", new
            {
                search = search
            });
        }

        public ActionResult ResetPponDealIndex()
        {
            throw new Exception("請洽IT");
            return RedirectToAction("KeywordList");
        }

        [HttpPost]
        public ActionResult KeywordEdit(Keyword model, string search)
        {
            Keyword origin = pp.KeywordGet(model.Id);
            if (model.Id == 0)
            {
                Keyword wordCheck = pp.KeywordGet(model.Word);
                if (wordCheck.IsLoaded)
                {
                    ViewBag.ErrorMessage = string.Format("關鍵字 \"{0}\" 已存在!!", model.Word);
                    ViewBag.Keyword = origin;
                    return View();
                }
                origin.Word = model.Word;
                origin.CreateId = this.UserName;
                origin.CreateTime = DateTime.Now;
                search = model.Word;
            }

            origin.Status = model.Status;
            pp.KeywordSet(origin);

            return RedirectToAction("KeywordList", new
            {
                search = search
            });
        }

        #endregion

        #region 搜尋使用統計
        public ActionResult SearchUsage()
        {
            return View();
        }

        /// <summary>
        /// ajax call 
        /// </summary>
        /// <param name="queryType"></param>
        /// <param name="queryStart"></param>
        /// <param name="queryEnd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchUsage(string queryType, DateTime queryStart, DateTime queryEnd)
        {
            List<dynamic> result = new List<dynamic>();

            if (queryType == "usage")
            {
                List<Tuple<string, int>> tmpList = pp.SearchUsageQueryByMonth(queryStart, queryEnd);
                foreach (Tuple<string, int> tmp in tmpList)
                {
                    result.Add(new
                    {
                        y = tmp.Item2,
                        label = tmp.Item1
                    });
                }
            }
            else if (queryType == "usageByDay")
            {
                List<Tuple<string, int>> tmpList = pp.SearchUsageQueryByDay(queryStart, queryEnd);
                foreach (Tuple<string, int> tmp in tmpList)
                {
                    result.Add(new
                    {
                        y = tmp.Item2,
                        label = tmp.Item1
                    });
                }
            }
            else if (queryType == "topKeywords" || queryType == "topKeywordsByDay")
            {
                List<Tuple<string, string, int>> tmpList = (queryType == "topKeywordsByDay") ? pp.SearchTopKeywordsByDay(queryStart, queryEnd, 100)
                    : pp.SearchTopKeywordsByMonth(queryStart, queryEnd, 100);
                var grps = tmpList.GroupBy(t => t.Item1).ToList().OrderByDescending(t => t.Key);

                foreach (var grp in grps)
                {
                    dynamic dataByMonth = new
                    {
                        label = grp.Key,
                        items = new List<dynamic>()
                    };
                    foreach (Tuple<string, string, int> tmp in grp)
                    {
                        dataByMonth.items.Add(new
                        {
                            word = tmp.Item2,
                            hits = tmp.Item3
                        });
                    }
                    result.Add(dataByMonth);
                }
            }
            else if (queryType == "hope")
            {
                var tmpList = pp.SearchHopeData(queryStart, queryEnd);
                foreach (Tuple<int, string, int> tmp in tmpList)
                {
                    result.Add(new
                    {
                        row = tmp.Item1,
                        word = tmp.Item2,
                        hits = tmp.Item3
                    });
                }
            }

            return Json(result);
        }

        /// <summary>
        /// ajax call 
        /// </summary>
        /// <param name="queryStart"></param>
        /// <param name="queryEnd"></param>
        /// <param name="keyWord"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SearchUsageWord(DateTime queryStart, DateTime queryEnd, string keyWord)
        {
            List<dynamic> result = new List<dynamic>();

            List<Tuple<string, string, int>> tmpList = pp.SearchTopKeywordsByWord(keyWord, queryStart, queryEnd);
            var grps = tmpList.GroupBy(t => t.Item1).ToList().OrderByDescending(t => t.Key);

            foreach (Tuple<string, string, int> tmp in tmpList)
            {
                result.Add(new
                {
                    y = tmp.Item3,
                    label = tmp.Item1
                });
            }

            return Json(result);
        }

        #endregion

        #region SystemDataEdit

        [RolePage]
        public ActionResult SystemDataEdit()
        {
            if (string.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                return this.Content("尚未登入!");
            }
            SystemData systemData = SystemCodeManager.GetSystemDataByName("PersonalDataCollectMatterContent");
            return View(systemData);
        }

        [RolePage]
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SystemDataUpdate(string taMainContent)
        {
            if (string.IsNullOrEmpty(this.HttpContext.User.Identity.Name))
            {
                return this.Content("尚未登入!");
            }
            SystemData systemdata = SystemCodeManager.GetSystemDataByName("PersonalDataCollectMatterContent");

            systemdata.Data = taMainContent;
            systemdata.ModifyId = this.HttpContext.User.Identity.Name;
            systemdata.ModifyTime = DateTime.Now;
            bool settingResult = SystemCodeManager.SetSystemData(systemdata);

            if (!settingResult)
            {
                return this.Content("更新失敗，請聯絡技術部。");
            }

            return RedirectToAction("SystemDataEdit");
        }


        #endregion SystemDateEdit

        #region Ga
        public ActionResult GaDealList(bool only_enabled = false)
        {
            return View(mp.GetViewGaDealList(only_enabled));
        }

        [HttpPost]
        public ActionResult GaDealList(Guid? search, int type)
        {
            return View(mp.GetViewGaDealList(search, type));
        }

        public ActionResult EditGaDealList(int? id, int type = (int)GaDealListType.BySchedule)
        {
            GaDealList item;
            if (id.HasValue)
            {
                item = mp.GetGaDealListById(id.Value);
            }
            else
            {
                item = new GaDealList() { Enabled = true, StartDate = DateTime.Now, EndDate = DateTime.Now.AddDays(1), Type = type };
            }
            return View("EditGaDealList", item);
        }

        [HttpPost]
        public ActionResult EditGaDealList(GaDealList item)
        {
            if (ModelState.IsValid)
            {
                item.EndDate = item.EndDate.AddDays(1);
                GaDealList original = new GaDealList();
                if (item.Id != 0)
                {
                    original = mp.GetGaDealListById(item.Id);
                    original.EndDate = item.EndDate;
                    original.Enabled = item.Enabled;
                    original.StartDate = item.StartDate;
                    original.Type = item.Type;
                    mp.SetGaDealList(original);
                }
                else
                {
                    mp.SetGaDealList(item);
                    if (item.Type == (int)GaDealListType.ByUser)
                    {
                        AnalyticsFacade.GetGaByDeal(item.Id);
                        return RedirectToAction("GaDealReport", new { gid = item.Id });
                    }
                }
                return RedirectToAction("GaDealList");
            }
            else
            {
                var errors = ModelState.Values.SelectMany(x => x.Errors);
                if (errors.Any(x => x.ErrorMessage.Contains("bid", StringComparison.CurrentCultureIgnoreCase)))
                {
                    ModelState.AddModelError("", "Bid格式錯誤");
                }
                else if (errors.Any(x => x.ErrorMessage.Contains("date", StringComparison.CurrentCultureIgnoreCase)))
                {
                    ModelState.AddModelError("", "日期格式錯誤");
                }
                return View(item);
            }
        }

        [HttpPost]
        public ActionResult GaDealReport(int searchsite, DateTime start_date, DateTime end_date)
        {
            GaDealCollection ga = mp.GetGaDealCollectionBySiteDate(searchsite, start_date, end_date.AddDays(1));
            GaViewIDMap site;
            string site_name = string.Empty;
            if (Enum.TryParse<GaViewIDMap>(searchsite.ToString(), out site))
            {
                site_name = site == GaViewIDMap.Lunchking_17LIFE ? "17Life主站" : "17Life APP";
            }
            string title = string.Format("{0}<br/>({1}~{2})", site_name, start_date.ToString("yyyy/MM/dd 00:00"), end_date.AddDays(-1).ToString("yyyy/MM/dd 23:59"));
            GaInfo info = new GaInfo() { GaDealData = ga, ViewGaDealListInfo = null, Title = title, JsonData = JsonConvert.SerializeObject(ga) };
            return View(info);
        }

        public ActionResult GaDealReport(int gid)
        {
            GaDealCollection ga = mp.GetGaDealCollectionByGid(gid);
            ViewGaDealList vga = mp.GetViewGaDealList(gid);
            string title = string.Empty;
            if (vga.Bid != Guid.Empty)
            {
                title = string.Format("{0}<br/>{1}<br/>({2}~{3})", vga.Name.TrimToMaxLength(15, "..."), vga.Bid, vga.StartDate.ToString("yyyy/MM/dd 00:00"), vga.EndDate.AddDays(-1).ToString("yyyy/MM/dd 23:59"));
            }
            else if (ga.Count > 0)
            {
                GaDeal first = ga.OrderBy(x => x.StartDate).First();
                GaDeal last = ga.OrderByDescending(x => x.EndDate).First();
                title = string.Format("{0}<br/>({1}~{2})", first.Bid, first.StartDate.ToString("yyyy/MM/dd 00:00"), last.EndDate.AddDays(-1).ToString("yyyy/MM/dd 23:59"));
            }
            var ga_json = ga.Where(x => x.Type == (int)GaDealType.Summary);
            if (vga.Type == (int)GaDealListType.ByUser)
            {
                ga_json = ga_json.Where(x => x.Sort != 0);
            }
            GaInfo info = new GaInfo() { GaDealData = ga, ViewGaDealListInfo = vga, Title = title, JsonData = JsonConvert.SerializeObject(ga_json) };
            return View(info);
        }

        [HttpPost]
        public ActionResult GetDealBusinessHourOrderTimes(Guid bid)
        {
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            return Json(new { Name = deal.ItemName, BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS, BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE });
        }

        [HttpPost]
        public ActionResult DeleteGaDeal(int gid)
        {
            mp.DeleteGaDeal(gid);
            mp.DeleteGaDealList(gid);
            return RedirectToAction("GaDealList");
        }
        #endregion

        #region 員工折價券
        public ActionResult StaffDiscountList(int? page)
        {
            List<string> filter = new List<string>();
            filter.Add(DiscountEvent.Columns.EventType + "=" + (int)DiscountActivityType.StaffEvent);
            int totalLogCount = op.DiscountEventGetCount(filter.ToArray());

            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            int pagnumber = Convert.ToInt16(page);
            ViewBag.PageIndex = pagnumber = (pagnumber == 0) ? 1 : pagnumber;
            ViewBag.EventList = op.DiscountEventGetByPage(pagnumber, 10, DiscountEvent.Columns.Id, filter.ToArray());
            return View();
        }

        [HttpPost]
        public ActionResult StaffDiscountList(DiscountEvent item, string txtSDate, string txtEDate, string shh, string smm, string ehh, string emm)
        {
            DateTime date;
            item.StartTime = DateTime.TryParse(txtSDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
            item.EndTime = DateTime.TryParse(txtEDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
            item.CreateId = this.UserName;
            item.CreateTime = DateTime.Now;
            item.ModifyId = this.UserName;
            item.ModifyTime = DateTime.Now;
            item.EventType = (int)DiscountActivityType.StaffEvent;
            op.DiscountEventSet(item);
            return RedirectToAction("StaffDiscountList");
        }

        public ActionResult StaffDiscountEdit(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);
            ViewBag.campaignList = campaignList;
            ViewBag.usersCount = op.DiscountUserGetCount(id);
            ViewBag.sentQty = op.DiscountUserGetCount(id, DiscountUserStatus.Sent);
            ViewBag.shortageQty = op.DiscountUserGetCount(id, DiscountUserStatus.Shortage);
            ViewBag.totalAmount = string.Format("每人發送 ${0} 元", campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value.ToString("N0"));
            return View(eventData);
        }
        [HttpPost]
        public ActionResult StaffDiscountImport(int id, HttpPostedFileBase upfile)
        {

            List<int> users = new List<int>();
            if (upfile != null)
            {
                using (StreamReader reader = new StreamReader(upfile.InputStream))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
            }
            OrderFacade.ImportDiscountUsers(id, users, this.UserName);
            return RedirectToAction("StaffDiscountEdit", new { id = id });
        }

        [HttpPost]
        public ActionResult StaffDiscountEdit(int id, string name, string sDate, string eDate, string shh, string smm, string ehh, string emm, string type)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            int status;
            DateTime date;
            eventData.Name = name;
            eventData.StartTime = DateTime.TryParse(sDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
            eventData.EndTime = DateTime.TryParse(eDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
            eventData.ModifyId = this.UserName;
            eventData.ModifyTime = DateTime.Now;
            eventData.Status = int.TryParse(type, out status) ? status : 0;
            op.DiscountEventSet(eventData);
            return RedirectToAction("StaffDiscountList");
        }

        public ActionResult StaffEventCampaignSet(int id, string message)
        {
            var campaignList = op.ViewDiscountEventCampaignGetList(id);
            ViewBag.totalAmount = string.Format("每人發送 ${0} 元", campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value.ToString("N0"));
            ViewBag.campaignList = campaignList;
            ViewBag.eventId = id;
            ViewBag.message = message;
            return View();
        }
        [HttpPost]
        public ActionResult SaveStaffEventCampaign(int eventId, string[] countSet, string[] campaignId)
        {
            int i = 0;
            Dictionary<int, int> eventCampaignDictionary = new Dictionary<int, int>();
            foreach (var val in countSet)
            {
                int count;
                string message;
                if (!int.TryParse(val, out count))
                {
                    message = "請輸入正確數量";
                    return RedirectToAction("StaffEventCampaignSet", new { id = eventId, message = message });
                }
                int cid;
                if (!int.TryParse(campaignId[i], out cid))
                {
                    message = "請輸入正確數量";
                    return RedirectToAction("StaffEventCampaignSet", new { id = eventId, message = message });
                }
                eventCampaignDictionary.Add(cid, count);
                i++;
            }

            op.EventCampaignDeleteList(eventId);
            OrderFacade.SetDiscountEventCampaignList(eventId, eventCampaignDictionary);

            return RedirectToAction("StaffDiscountEdit", new { id = eventId });
        }
        [HttpPost]
        public ActionResult AddCampaign(string eventId, DiscountEvent item, string txtSDate, string txtEDate, string shh, string smm, string ehh, string emm, string amount,
            string texqty, string fiftyqty, string txtcust, string txtcustqty, string minicheck, string miniAmount, string minGrossMargin)
        {
            int dataId = Convert.ToInt16(eventId);
            int number;
            int quantity;
            int minimum_amount;
            int grossMargin;
            string message;
            DateTime start_date, end_date;
            DiscountCampaign campaign = new DiscountCampaign();
            switch (amount)
            {
                case "rbcust":
                    if (!int.TryParse(txtcust, out number))
                    {
                        message = "請輸入金額";
                        return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
                    }
                    if (!int.TryParse(txtcustqty, out quantity))
                    {
                        message = "請輸入數量";
                        return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
                    }
                    campaign.Amount = number;
                    campaign.Qty = quantity;
                    break;
                case "rbfifty":
                    if (!int.TryParse(fiftyqty, out quantity))
                    {
                        message = "請輸入數量";
                        return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
                    }
                    campaign.Amount = 50;
                    campaign.Qty = quantity;
                    break;
                default:
                    if (!int.TryParse(texqty, out quantity))
                    {
                        message = "請輸入數量";
                        return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
                    }
                    campaign.Amount = 10;
                    campaign.Qty = quantity;
                    break;
            }

            if (!DateTime.TryParse(txtSDate, out start_date))
            {
                message = "請輸入開始時間;";
                return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
            }

            if (!DateTime.TryParse(txtEDate, out end_date))
            {
                message = "請輸入結束時間;";
                return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
            }

            if (!int.TryParse(minGrossMargin, out grossMargin))
            {
                message = "請輸入最低毛利率;";
                return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
            }

            start_date = new DateTime(start_date.Year, start_date.Month, start_date.Day, int.Parse(shh), int.Parse(smm), 0);
            end_date = new DateTime(end_date.Year, end_date.Month, end_date.Day, int.Parse(ehh), int.Parse(emm), 0);
            campaign.StartTime = start_date;
            campaign.EndTime = end_date;
            campaign.MinGrossMargin = grossMargin;

            campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
            campaign.Flag = campaign.Flag | (int) DiscountCampaignUsedFlags.InstantGenerate;//及時產
            campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.OwnerUseOnly;//限本人使用
            campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;
            if (minicheck != null)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;//最低消費金額限制
                if (!int.TryParse(miniAmount, out minimum_amount))
                {
                    message = "請輸入限制最低金額";
                    return RedirectToAction("StaffEventCampaignSet", new { id = dataId, message = message });
                }
                campaign.MinimumAmount = minimum_amount;
            }
            campaign.Name = item.Name;
            campaign.CreateTime = DateTime.Now;
            campaign.CreateId = "Sys_StaffDiscount";
            campaign.ApplyTime = DateTime.Now;
            campaign.ApplyId = this.UserName;

            op.DiscountCampaignSet(campaign);
            Dictionary<int, int> eventCampaign = new Dictionary<int, int>();
            eventCampaign.Add(campaign.Id,1);
            OrderFacade.SetDiscountEventCampaignList(dataId, eventCampaign);
            return RedirectToAction("StaffEventCampaignSet", new { id = dataId });

        }
        public ActionResult DelCampaign(int id,int campaignId)
        {
            op.DiscountEventCampaignDelete(id);
            return RedirectToAction("StaffEventCampaignSet", new { id = campaignId });
        }

        #endregion 員工折價券

        #region 活動折價券

        public ActionResult DiscountEventList(int? page , string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.message = msg;
            }
            List<string> filter = new List<string>();
            filter.Add(DiscountEvent.Columns.EventType + " = 0 ");
            int totalLogCount = op.DiscountEventGetCount(filter.ToArray());
            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            int pagnumber = Convert.ToInt16(page);
            ViewBag.PageIndex = pagnumber = (pagnumber == 0) ? ViewBag.PageCount : pagnumber;
            ViewBag.EventList = op.DiscountEventGetByPage(pagnumber, 10, DiscountEvent.Columns.Id, filter.ToArray());

            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();

            return View();
        }

        [HttpPost]
        public ActionResult DiscountEventList(string eventName, string txtSDate, string txtEDate)
        {
            List<string> filter = new List<string>();
            filter.Add(DiscountEvent.Columns.EventType + " = " + (int)DiscountActivityType.DiscountEvent);
            if (!string.IsNullOrEmpty(eventName))
            {
                filter.Add(DiscountEvent.Columns.Name + " like %" + eventName + "%");
            }
            DateTime start_date, end_date;
            if (!string.IsNullOrEmpty(txtSDate) && !DateTime.TryParse(txtSDate, out start_date))
            {
                filter.Add(DiscountEvent.Columns.StartTime + ">=" + start_date.ToString("yyyy-MM-dd HH:mm"));
            }
            if (!string.IsNullOrEmpty(txtEDate) && !DateTime.TryParse(txtEDate, out end_date))
            {
                filter.Add(DiscountEvent.Columns.EndTime + "<=" + end_date.ToString("yyyy-MM-dd HH:mm"));
            }

            ViewBag.EventList = op.DiscountEventGetByPage(1, 10, DiscountEvent.Columns.Id, filter.ToArray());

            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();

            return View();
        }

        public ActionResult EventCampaignSet(int? id)
        {
            int eventId = Convert.ToInt32(id);
            if (eventId == 0)
            {
                return RedirectToAction("DiscountEventList");
            }
            DiscountEvent item = new DiscountEvent();
            item = op.DiscountEventGet(eventId);
            var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
            int campaignid = 0;
            if (campaignList.Count > 0 )
            {
                campaignid = campaignList.First().CampaignId;
            }
            var categoryList = op.DiscountCategoryGetList(campaignid);
            string channelstr = "";
            foreach (var category in categoryList)
            {
                channelstr += CategoryManager.CategoryGetById(category.CategoryId).Name + ",";
            }
            if (channelstr=="")
            {
                channelstr = "全站";
            }
            ViewBag.saveType = "Update";
            ViewBag.EventName = item.Name;
            ViewBag.EventId = item.Id;
            ViewBag.StartTime = item.StartTime;
            ViewBag.EndTime = item.EndTime;
            ViewBag.Channel = channelstr;
            ViewBag.campaignList = campaignList;
            return View();
        }

        [HttpPost]
        public ActionResult EventCampaignSet(FormCollection formValues)
        {
            DateTime date;
            int campaignflag = 0;
            var channelList = CategoryManager.PponChannelCategoryTree.CategoryNodes.OrderBy(x => x.Seq);
            var txtEName = Request.Form["txtEName"];
            var txtSDate = Request.Form["txtSDate"];
            var shh = Request.Form["shh"];
            var smm = Request.Form["smm"];
            var txtEDate = Request.Form["txtEDate"];
            var ehh = Request.Form["ehh"];
            var emm = Request.Form["emm"];
            var discountType = Request.Form["discountType"];
            var chkOwnerUseOnly = Request.Form["chkOwnerUseOnly"];
            var chkVisaOnly = Request.Form["chkVisaOnly"];
            var chkBindEventPromoId = Request.Form["chkBindEventPromoId"];
            var eventPromoId = Request.Form["EventPromoId"];
            var chkAppOnly = Request.Form["chkAppOnly"];
            SortedList channelId = new SortedList();
            SortedList channelName = new SortedList();

            foreach (var channel in channelList)
            {
                channelId.Add(channel.CategoryId.ToString(), Request.Form["channelId" + channel.CategoryId].TrimEnd(",").Replace(",", "|"));
                channelName.Add(channel.CategoryId.ToString(), Request.Form["channelName" + channel.CategoryId].TrimEnd(","));
            }

            switch (discountType)
            {
                case "0": campaignflag = campaignflag | (int)DiscountCampaignUsedFlags.InstantGenerate; break;//及時產
                case "1": campaignflag = campaignflag | (int)DiscountCampaignUsedFlags.SingleSerialKey; break;//預產折價券
                case "2": campaignflag = campaignflag | (int)DiscountCampaignUsedFlags.SingleSerialKey; break;//統一序號
            }

            if (chkOwnerUseOnly != null)
            {
                campaignflag = campaignflag | (int)DiscountCampaignUsedFlags.OwnerUseOnly;
            }

            if (chkVisaOnly != null)
            {
                campaignflag = campaignflag | (int)DiscountCampaignUsedFlags.VISA;
            }

            if (chkBindEventPromoId != null)
            {
                int tmpId = 0;
                Guid bid = Guid.Empty;
                if (Guid.TryParse(eventPromoId, out bid))
                {
                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(bid);
                    if (vpd.IsLoaded)
                    {
                        if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {
                            EventPromo promo = new EventPromo();
                            promo.Title = vpd.ItemName;
                            promo.Url = string.Format("discount_{0}", vpd.UniqueId);
                            promo.StartDate = DateTime.TryParse(txtSDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
                            promo.EndDate = DateTime.TryParse(txtEDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
                            promo.BgColor = "FFFFFF";
                            promo.Status = true;
                            promo.Creator = this.UserName;
                            promo.Cdt = DateTime.Now;
                            promo.TemplateType = (int)EventPromoTemplateType.PponOnly;
                            promo.ShowInWeb = true;
                            MarketingFacade.SaveCurationAndResetMemoryCache(promo);

                            EventPromoItem promoItemitem = new EventPromoItem();
                            promoItemitem.MainId = promo.Id;
                            promoItemitem.Title = vpd.ItemName;
                            promoItemitem.Description = vpd.EventTitle;
                            promoItemitem.Seq = 1;
                            promoItemitem.Status = true;
                            promoItemitem.Creator = this.UserName;
                            promoItemitem.Cdt = DateTime.Now;
                            promoItemitem.ItemId = vpd.UniqueId.Value;
                            promoItemitem.ItemType = (int)EventPromoItemType.Ppon;
                            pp.SaveEventPromoItem(promoItemitem);
                            ViewBag.EventPromoId = promo.Id.ToString();
                        }
                        else
                        {
                            return RedirectToAction("DiscountEventList", new { msg = "無法綁定子檔次，請輸入母擋bid" });
                        }
                    }
                    else
                    {
                        return RedirectToAction("DiscountEventList", new { msg = "bid錯誤，無法綁定" });
                    }
                    
                }

                if (!int.TryParse(eventPromoId, out tmpId))
                {
                    return RedirectToAction("DiscountEventList", new { msg = "請輸入商品主題活動ID" });
                }
                else
                {
                    EventPromo eventPromo = pp.GetEventPromo(tmpId);
                    if (!eventPromo.IsLoaded)
                    {
                        return RedirectToAction("DiscountEventList", new { msg = "查無此商品主題活動ID" });
                    }
                    ViewBag.EventPromoId = eventPromoId;
                }
            }

            DiscountEvent item = new DiscountEvent();
            item.Name = txtEName;
            item.Status = 0;
            item.StartTime = DateTime.TryParse(txtSDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
            item.EndTime = DateTime.TryParse(txtEDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
            item.CreateId = this.UserName;
            item.CreateTime = DateTime.Now;
            item.ModifyId = this.UserName;
            item.ModifyTime = DateTime.Now;
            item.EventType = 0;

            op.DiscountEventSet(item);

            ViewBag.SaveType = "New";
            ViewBag.Campaignflag = campaignflag;
            ViewBag.EventId = item.Id;
            ViewBag.EventName = txtEName;
            ViewBag.StartTime = item.StartTime;
            ViewBag.EndTime = item.EndTime;
            ViewBag.DiscountType = discountType;
            ViewBag.AppOnly = chkAppOnly == null ? "" : "checked";
            ViewBag.EventPromoId = eventPromoId;
            ViewBag.ChannelIdArray = channelId;
            ViewBag.ChannelNameArray = channelName;

            ViewBag.CategoryDictionary = PromotionFacade.GetCategorieList();
            return View();
        }

        public ActionResult EventCampaignSave()
        {
            return RedirectToAction("DiscountEventList");
        }

        [HttpPost]
        public ActionResult EventCampaignSave(FormCollection formValues)
        {
            var saveType = Request.Form["saveType"];
            var sDate = Request.Form["sDate"];
            var eDate = Request.Form["eDate"];
            var campaignFlag = Request.Form["campaignFlag"];
            var eventPromoId = Request.Form["eventPromoId"];
            var eventId = Convert.ToInt32(Request.Form["eventId"]);

            var chkAppOnly = Request.Form["chkAppOnly"] == null ? null : Request.Form["chkAppOnly"].Split(',');
            var discountName = Request.Form["discountName"] == null ? null : Request.Form["discountName"].Split(',');
            var discountAmount = Request.Form["discountAmount"] == null ? null : Request.Form["discountAmount"].Split(',');
            var discountMin = Request.Form["discountMin"] == null ? null : Request.Form["discountMin"].Split(',');
            var count = Request.Form["count"] == null ? null : Request.Form["count"].Split(',');
            var discountAll = Request.Form["discountAll"] == null ? null : Request.Form["discountAll"].Split(',');
            var campaignCode = Request.Form["campaignCode"] == null ? null : Request.Form["campaignCode"].Split(',');
            var campaignId = Request.Form["campaignId"] == null ? null : Request.Form["campaignId"].Split(',');
            
            if (saveType == "New")
            {
                DiscountEvent item = op.DiscountEventGet(eventId);

                int allQuantity = 0;
                DateTime start_date, end_date;
                Dictionary<int, int> eventCampaign = new Dictionary<int, int>();

                for (int i = 0; i < discountName.Count(); i++)
                {
                    Dictionary<int, string> channelId = new Dictionary<int, string>();
                    if (Request.Form["channelId" + i]!="")
                    {
                        var channelVal = Request.Form["channelId" + i] == null ? null : Request.Form["channelId" + i].TrimEnd(",");
                        var channelArr = channelVal == null ? null : channelVal.Split(',');
                        if (channelArr != null)
                        {
                            foreach (var channel in channelArr)
                            {
                                var tmp = channel == null ? null : channel.Split('|');
                                var str = channel.Replace("|", ",");
                                int cid = Convert.ToInt32(tmp[0]);
                                if (!string.IsNullOrEmpty(channel))
                                {
                                    channelId.Add(cid, str);
                                }
                            }
                        }
                    }
                    DiscountCampaign campaign = new DiscountCampaign();
                    DateTime.TryParse(sDate, out start_date);
                    DateTime.TryParse(eDate, out end_date);

                    var Name = discountName[i];
                    var amount = Convert.ToInt32(discountAmount[i]);
                    var c = Convert.ToInt32(count[i]);
                    var quantity = Convert.ToInt32(discountAll[i]);
                    var minimum_amount = Convert.ToInt32(discountMin[i]);
                    allQuantity += c;
                    campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
                    campaign.Name = Name;
                    campaign.Amount = amount;
                    campaign.Qty = quantity;
                    campaign.MinimumAmount = minimum_amount;
                    campaign.CreateId = this.UserName;
                    campaign.CreateTime = DateTime.Now;
                    campaign.StartTime = start_date;
                    campaign.EndTime = end_date;
                    campaign.Flag = Convert.ToInt32(campaignFlag) | (int)DiscountCampaignUsedFlags.MinimumAmount;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;
                    
                    if (chkAppOnly != null)
                    {
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AppUseOnly; 
                    }
                    if (channelId.Any())
                    {
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                    }
                    op.DiscountCampaignSet(campaign);

                    if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
                    {
                        var code = string.Empty;
                        if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                        {
                            code = string.IsNullOrEmpty(campaignCode[i].Trim()) ? PromotionFacade.GetDiscountCode(campaign.Amount.Value) : campaignCode[i].Trim();
                        }

                        // 若為限本人使用勢必需要歸戶，因此程式改為歸戶時才產生資料
                        if ((campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) == 0)
                        {
                            DiscountCodeCollection dataList = new DiscountCodeCollection();
                            for (var a = 0; a < campaign.Qty; a++)
                            {
                                DiscountCode dc = new DiscountCode();
                                dc.CampaignId = campaign.Id;
                                dc.Amount = campaign.Amount;
                                if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) == 0)
                                {
                                    code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                                }
                                dc.Code = code;
                                dataList.Add(dc);
                            }

                            if (dataList.Count > 0)
                            {
                                op.DiscountCodeCollectionBulkInsert(dataList);
                            }
                            
                        }
                    }

                    eventCampaign.Add(campaign.Id, c);

                    string channelstr = "";
                    
                    foreach (var k in channelId)
                    {
                        if (k.Value.IndexOf(",") > 0)
                        {
                            var tmp = k.Value.Split(",");
                            foreach (var t in tmp)
                            {
                                if (!string.IsNullOrEmpty(t))
                                {
                                channelstr += CategoryManager.CategoryGetById(Convert.ToInt32(t)).Name + ",";
                                DiscountCategory dc = new DiscountCategory();
                                dc.CampaignId = campaign.Id;
                                dc.ChannelId = k.Key;
                                dc.CategoryId = Convert.ToInt32(t);
                                op.DiscountCategorySet(dc);   
                            }
                        }
                        }
                        else
                        {
                            DiscountCategory dc = new DiscountCategory();
                            dc.CampaignId = campaign.Id;
                            dc.ChannelId = k.Key;
                            dc.CategoryId = Convert.ToInt32(k.Value);
                            channelstr += CategoryManager.CategoryGetById(Convert.ToInt32(k.Value)).Name + ",";
                            op.DiscountCategorySet(dc);
                        }
                    }
                    
                    if (eventPromoId != null) 
                    {
                        int tmpId = 0;
                        if (!int.TryParse(eventPromoId, out tmpId)) 
                        {
                            DiscountEventPromoLink promoLink = new DiscountEventPromoLink();
                            promoLink.CampaignId = campaign.Id;
                            promoLink.EventPromoId = tmpId;
                            op.DiscountEventPromoLinkSet(promoLink);
                            }
                        }    

                }

                OrderFacade.SetDiscountEventCampaignList(eventId, eventCampaign);
                var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
                item.Qty = 0;
                item.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value); ;
                op.DiscountEventSet(item);

            }
            else
            {
                int i = 0;
                int allQuantity = 0;
                Dictionary<int, int> eventCampaignDictionary = new Dictionary<int, int>();
                foreach (var val in count)
                {
                    eventCampaignDictionary.Add(Convert.ToInt32(campaignId[i]), Convert.ToInt32(val));
                    allQuantity += Convert.ToInt32(val);
                    i++;
                }

                op.EventCampaignDeleteList(eventId);
                OrderFacade.SetDiscountEventCampaignList(eventId, eventCampaignDictionary);
                var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
                DiscountEvent item = op.DiscountEventGet(eventId);
                item.Qty = 0;
                item.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value);
                op.DiscountEventSet(item);
                
            }

            return RedirectToAction("DiscountEventEdit", new { id = eventId });

        }
        
        public ActionResult DiscountEventEdit(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);

            List<string> channelstrList = new List<string>();

            foreach (var campaign in campaignList)
            {
                var categoryList = op.DiscountCategoryGetList(campaign.CampaignId);
            string channelstr = categoryList.Aggregate("", (current, category) => current + (CategoryManager.CategoryGetById(category.CategoryId).Name + ","));
            if (channelstr == "")
            {
                channelstr = "全站";
            }
                channelstrList.Add(channelstr);
            }

            ViewBag.campaignList = campaignList;
            ViewBag.usersCount = op.DiscountUserGetCount(id);
            ViewBag.sentQty = op.DiscountUserGetCount(id, DiscountUserStatus.Sent);
            ViewBag.shortageQty = op.DiscountUserGetCount(id, DiscountUserStatus.Shortage);
            ViewBag.totalAmount = string.Format("每人發送 ${0} 元", campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value.ToString("N0"));
            ViewBag.Channel = channelstrList;
            return View(eventData);
        }

        public ActionResult DiscountEventImport(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            if (eventData.ApplyTime == null)
            {
                ApplyDiscountEvent(eventData);
            }
            OrderFacade.ImportDiscountUserForAllMembers(id, UserName);
            return RedirectToAction("DiscountEventEdit", new { id });
        }

        [HttpPost]
        public ActionResult DiscountEventImport(int id, HttpPostedFileBase upfile)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            if (eventData.ApplyTime == null)
            {
                ApplyDiscountEvent(eventData);
            }
            List<int> users = new List<int>();
            if (upfile != null)
            {
                using (StreamReader reader = new StreamReader(upfile.InputStream))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
            }
            OrderFacade.ImportDiscountUsers(id, users, UserName);
            return RedirectToAction("DiscountEventEdit", new {id});
        }

        
        [HttpPost]
        public ActionResult DiscountEventSave(int id, string name, string sDate, string eDate, string shh, string smm, string ehh, string emm)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            DateTime date;
            eventData.Name = name;
            eventData.StartTime = DateTime.TryParse(sDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
            eventData.EndTime = DateTime.TryParse(eDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
            eventData.ModifyId = UserName;
            eventData.ModifyTime = DateTime.Now;
            eventData.Status = (int)DiscountEventStatus.Initial;
            op.DiscountEventSet(eventData);

            return RedirectToAction("DiscountEventList");
        }

        public ActionResult DelEventCampaign(int id ,int eventId, int campaignId)
        {
            op.DiscountEventCampaignDelete(id);
            DiscountCampaign dca = op.DiscountCampaignGet(campaignId);
            dca.ApplyTime = DateTime.Now;
            dca.ApplyId = UserName;
            //刪除後即失效
            dca.StartTime = DateTime.Now.AddDays(-2);
            dca.EndTime = DateTime.Now.AddDays(-1);

            op.DiscountCampaignSet(dca);
            var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
            DiscountEvent item = op.DiscountEventGet(eventId);
            var sum = campaignList.Sum(x => x.Amount);
            if (sum != null)
                item.Qty = 0;
            var sum1 = campaignList.Sum(x => x.Amount * (x.EventQty ?? 0));
            if (sum1 != null)
                item.Total = Convert.ToInt32(sum1.Value);
            op.DiscountEventSet(item);
            return RedirectToAction("EventCampaignSet", new { id = eventId });
        }

        public ActionResult DelEvent(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            eventData.EventType = (int)DiscountActivityType.DeleteEvent;
            eventData.StartTime = DateTime.Now.AddDays(-2);
            eventData.EndTime = DateTime.Now.AddDays(-1);
            op.DiscountEventSet(eventData);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);
            foreach (var campaign in campaignList)
            {
                DiscountCampaign dca = op.DiscountCampaignGet(campaign.CampaignId);
                dca.ApplyTime = DateTime.Now;
                dca.ApplyId = UserName;
                //刪除後即失效
                dca.StartTime = DateTime.Now.AddDays(-2);
                dca.EndTime = DateTime.Now.AddDays(-1);
                op.DiscountCampaignSet(dca);
            }
            op.EventCampaignDeleteList(id);
            return RedirectToAction("DiscountEventList");
        }

        public ActionResult ApplyEvent(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            ApplyDiscountEvent(eventData);
            return RedirectToAction("DiscountEventList");
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult GetLog(int id)
        {

            DiscountEvent eventData = op.DiscountEventGet(id);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);

            List<string> channelstrList = new List<string>();
            string eventLimitStr="";
            foreach (var campaign in campaignList)
            {
                var categoryList = op.DiscountCategoryGetList(campaign.CampaignId);              
                string channelstr = categoryList.Aggregate("", (current, category) => current + (CategoryManager.CategoryGetById(category.CategoryId).Name + ","));
                if (channelstr == "")
                {
                    channelstr = "全站";
                }
                channelstrList.Add(channelstr);
                if ((Convert.ToInt32(campaign.Flag) & (int)DiscountCampaignUsedFlags.OwnerUseOnly) > 0)
                {
                    eventLimitStr = "限本人使用";
                }
                if ((Convert.ToInt32(campaign.Flag) & (int)DiscountCampaignUsedFlags.VISA) > 0)
                {
                    eventLimitStr = "限VISA使用";
                }
            }

            LogData data = new LogData
            {
                EventName = eventData.Name,
                StartTime = eventData.StartTime.ToString("yyyy-MM-dd HH:mm"),
                EndTime = eventData.EndTime.ToString("yyyy-MM-dd HH:mm"),
                EventLimit = eventLimitStr==""?"無": eventLimitStr,
                CampaignList = campaignList,
                ChannelstrList = channelstrList,
            };
            return Json(data);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult ImportCampaign(int id, int campaignId)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            DiscountCampaign dc = op.DiscountCampaignGet(campaignId);
            if (dc != null)
            {
                if (eventData.StartTime.Date == dc.StartTime.Value.Date && eventData.EndTime.Date == dc.EndTime.Value.Date)
                {
                    Dictionary<int, int> eventCampaignDictionary = new Dictionary<int, int>();
                    eventCampaignDictionary.Add(Convert.ToInt32(campaignId), 1);
                    OrderFacade.SetDiscountEventCampaignList(id, eventCampaignDictionary);
                    var campaignList = op.ViewDiscountEventCampaignGetList(id);
                    eventData.Qty = 0;
                    eventData.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount*(x.EventQty ?? 0)));
                    op.DiscountEventSet(eventData);
                    if (eventData.ApplyTime != null && dc.ApplyTime == null)
                    {
                        dc.ApplyTime = DateTime.Now;
                        dc.ApplyId = UserName;
                        op.DiscountCampaignSet(dc);
                        return Json("折價券已匯入成功,並自動啟用");
                    }
                    return Json("折價券已匯入成功");
                }
                else
                {
                    return Json("折價券時間與活動時間不同，無法匯入");
                }
            }
            else
            {
                return Json("無此折價券，請輸入正確ID");
            }
        }

        public class LogData
        {
            public string EventName { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string EventLimit { get; set; }
            public ViewDiscountEventCampaignCollection CampaignList { get; set; }

            public List<string> ChannelstrList
            {
                get
                {
                    return channelstrList;
                }

                set
                {
                    channelstrList = value;
                }
            }

            private List<string> channelstrList = new List<string>();

        }

        private void ApplyDiscountEvent(DiscountEvent eventData)
        {
            var campaignList = op.ViewDiscountEventCampaignGetList(eventData.Id);        
            eventData.ApplyTime = DateTime.Now;
            op.DiscountEventSet(eventData);
            foreach (var campaign in campaignList)
            {
                DiscountCampaign dca = op.DiscountCampaignGet(campaign.CampaignId);
                dca.ApplyTime = DateTime.Now;
                dca.ApplyId = UserName;
                op.DiscountCampaignSet(dca);
            }
        }

        #endregion 活動折價券

        #region 全家

        public ActionResult FamiUsage()
        {
            return View();
        }

        /// <summary>
        /// ajax call 
        /// </summary>
        /// <param name="queryType"></param>
        /// <param name="queryStart"></param>
        /// <param name="queryEnd"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult FamiUsage(string queryType, DateTime queryStart, DateTime queryEnd)
        {
            List<dynamic> result = new List<dynamic>();
            List<Tuple<string, int>> tmpList = new List<Tuple<string, int>>();
            if (queryType == "usageByMonth")
            {
                tmpList = pp.FamiUsageQuery(queryStart, queryEnd, false, false);
            }
            else if (queryType == "usageByDay")
            {
                tmpList = pp.FamiUsageQuery(queryStart, queryEnd, true, false);
            }
            else if (queryType == "usageByHour")
            {
                tmpList = pp.FamiUsageQuery(queryStart, queryEnd, true, true);
            }
            foreach (Tuple<string, int> tmp in tmpList)
            {
                result.Add(new
                {
                    y = tmp.Item2,
                    label = tmp.Item1
                });
            }
            return Json(result);
        }

        #endregion

        #region 上傳多張大圖

        public ActionResult PicSetup(Guid bid)
        {
            ViewBag.bid = bid;
            CouponEventContent content = pp.CouponEventContentGet(CouponEventContent.Columns.BusinessHourGuid, bid);

            string[] images = ImageFacade.GetMediaPathsFromRawData(content.ImagePath, MediaType.PponDealPhoto);

            ViewBag.images = images.Select((x, i) => new { ImgPath = x, Index = i }).Where(x => x.Index != 1 && x.Index != 2 && x.ImgPath != string.Empty);

            return View();
        }

        [HttpPost]
        public ActionResult UpLoadImg(Guid bid, string printWatermark)
        {
            Dictionary<string, object> jo = new Dictionary<string, object>();
            IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid);
            string imagePath = deal.EventImagePath;
            if (deal == null || deal.BusinessHourGuid == Guid.Empty)
            {
                jo.Add("success", false);
                jo.Add("message", "查無檔次.");
                return Json(jo);
            }
            bool isPrintWatermark = printWatermark != null;
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                bool isUploadByOrder = false;
                bool isSideDeal = false;
                imagePath = ImageUtility.UploadPponImageAndGetNewPath(file, deal.SellerId, bid.ToString(), imagePath, isUploadByOrder, isPrintWatermark, isSideDeal);
                Thread.Sleep(config.PicSetupUploadImageSleepTime);
            }

            pp.CouponEventContentUpdatePic(imagePath, string.Empty, string.Empty, bid);

            jo.Add("success", true);
            return Json(jo);
        }
        #endregion

        #region PromoSearchKeySetUp

        /// <summary>
        /// 熱門關鍵字後台顯示兼查詢
        /// </summary>
        /// <returns></returns>
        public ActionResult PromoSearchKeySetUp()
        {
            //給預設值 1:WEB 2:RWD 3:APP 4:Mobile
            List<PromoSearchKey> data = pp.PromoSearchKeyGetAllColl(PromoSearchKeyUseMode.APP).Where(t=>t.IsEnable).ToList();
            PromoSearchKeyModel vModel = new PromoSearchKeyModel();

            Dictionary<string, int> keywords = PromotionFacade.GetGetOnlineMainDealNoSkmNoTmallNoHiddenKeywordDict();
            
            List<PromoSearchKeyItem> items = data.Select(x => new PromoSearchKeyItem(x)).ToList();
            foreach (PromoSearchKeyItem item in items)
            {
                //item.DayAverageHitCountAtLastSevenDays = 170;
                if (keywords.ContainsKey(item.Keyword))
                {
                    item.MatchDealsCount = keywords[item.Keyword];
                }
            }

            //參考名單1，依照符合的檔次數
            var sortedRecommendKeywords = keywords
                .OrderByDescending(t => t.Value)
                .Take(200)
                .Select(t=>new PromoSearchKeyModel.SortedRecommendKeyword{ Keyword = t.Key, Count = t.Value})
                .ToList();
            HashSet<string> keywordSet = new HashSet<string>(items.Select(t => t.Keyword));
            foreach (var recommdneKeyword in sortedRecommendKeywords)
            {
                if (keywordSet.Contains(recommdneKeyword.Keyword))
                {
                    recommdneKeyword.IsExist = true;
                }
            }

            vModel.PromoSearchKeyList = items;
            vModel.MatchDeals = sortedRecommendKeywords;

            return View(vModel);
        }

        /// <summary>
        /// 熱門關鍵字後台新增
        /// </summary>
        /// <param name="keyword"></param>
        /// <param name="picUrl"></param>
        /// <param name="useMode">enum PromoSearchKeyUseMode</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PromoSearchKeySetUpAdd(string keyword, string picUrl)
        {
            #region check text
            if (string.IsNullOrWhiteSpace(keyword))
            {
                return Json(new { Success = false, Message = "關鍵字不得為空白，請重新確認！" });
            }

            if (string.IsNullOrWhiteSpace(picUrl))
            {
                return Json(new { Success = false, Message = "圖片網址不得為空白，請重新確認！" });
            }

            if (keyword.Length > 12)
            {
                return Json(new { Success = false, Message = "關鍵字長度超過12個字，請重新確認！" });
            }

            #endregion

            PromoSearchKeyCollection data = pp.PromoSearchKeyGetColl(PromoSearchKeyUseMode.APP);

            if (data.Any(d => d.KeyWord == keyword))
            {
                return Json(new { Success = false, Message = "此關鍵字已存在，無法新增成功，請重新確認！" });
            }
            Dictionary<string, int> keywords = PromotionFacade.GetGetOnlineMainDealNoSkmNoTmallNoHiddenKeywordDict();
            int matchDealsCount = 0;
            if (keywords.ContainsKey(keyword))
            {
                matchDealsCount = keywords[keyword];
            }

            PromoSearchKey addPromoSerchKey = new PromoSearchKey
            {
                Seq = pp.PromoSearchKeyMaxSeq(PromoSearchKeyUseMode.APP) + 1,
                KeyWord = keyword,
                PicUrl = picUrl,
                IsEnable = true,
                UseMode = (int)PromoSearchKeyUseMode.APP,
                CreateTime = DateTime.Now,
                CreateUserId = MemberFacade.GetUniqueId(User.Identity.Name),
                ModifyTime = null,
                ModifyUserDi = null
            };

            pp.PromoSearchKeydSet(addPromoSerchKey);
            
            return Json(new { Success = true,
                Data = new
                {
                    Keyword = addPromoSerchKey.KeyWord,
                    addPromoSerchKey.Id,
                    PicUrl = addPromoSerchKey.PicUrl,
                    CreateTime = addPromoSerchKey.CreateTime.ToString("yyyy/MM/dd"),
                    MatchDealsCount = matchDealsCount,
                    CreateUserName = MemberFacade.GetMember(addPromoSerchKey.CreateUserId).DisplayName
                },
                Message = "新增成功！稍後將反映到前台！" });
        }

        /// <summary>
        /// 熱門關鍵字後台修改
        /// </summary>
        /// <param name="picUrl"></param>
        /// <param name="keyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PromoSearchKeySetUpEdit(int keyId, string picUrl)
        {            
            PromoSearchKey data = pp.PromoSearchKeyGet(keyId);
            int modifyUserId = MemberFacade.GetUniqueId(User.Identity.Name);

            if (data.IsLoaded)
            {
                data.PicUrl = picUrl;
                data.ModifyTime = DateTime.Now;
                data.ModifyUserDi = modifyUserId;
                pp.PromoSearchKeydSet(data);
            }            

            return Json(
                new
                {
                    Success = true, Message = "修改成功！稍後將反映到前台！" ,
                    Data = new {data.Id, data.PicUrl}
                });
        }

        /// <summary>
        /// 熱門關鍵字後台刪除
        /// </summary>
        /// <param name="keyId"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PromoSearchKeySetUpDel(int keyId)
        {
            PromoSearchKey item = pp.PromoSearchKeyGet(keyId);

            item.IsEnable = false;
            item.ModifyTime = DateTime.Now;
            item.ModifyUserDi = MemberFacade.GetUniqueId(User.Identity.Name);
            pp.PromoSearchKeydSet(item);

            return Json(
                new
                {
                    Success = true,
                    Data = new {Id = item.Id}
                }
            );
        }

        /// <summary>
        /// 熱門關鍵字後台排序
        /// </summary>
        /// <param name="promoSearchKeyList"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult PromoSearchKeySetUpSort(int[] promoSearchKeyList)
        {
            int seq = 0;
            foreach (int searchKeyId in promoSearchKeyList)
            {
                PromoSearchKey item = pp.PromoSearchKeyGet(searchKeyId);
                if (item.IsLoaded == false)
                {
                    continue;
                }
                item.Seq = seq;
                pp.PromoSearchKeydSet(item);
                seq++;
            }
            return Json(new { Success = true, Message = "排序已更新" });
        }

        [HttpPost]
        public JsonResult PromoSearchKeyQueryPicsByKeyword(string keyword)
        {
            List<IViewPponDeal> deals = SearchFacade.GetSearchDeals(keyword, string.Empty, null);
            List<string> picUrls = new List<string>();
            foreach (IViewPponDeal deal in deals)
            {
                if (deal != null && string.IsNullOrEmpty(deal.AppDealPic) == false)
                {
                    picUrls.Add(PponFacade.GetPponDealFirstImagePath(deal.AppDealPic));
                    if (picUrls.Count >= 10)
                    {
                        break;
                    }
                }
            }
            return Json(new {Success = true, Data = picUrls});
        }

        #endregion PromoSearchKeySetUp

        #region Ichannel相關

        public ActionResult IchannelIndex()
        {
            return View();
        }

        [HttpPost]
        public ActionResult IchannelIndex(Guid guid)
        {
            var data = OrderFacade.GetViewIChannelInfoByOrderId(guid);
            ViewBag.guid = guid;
            ViewBag.data = data;
            return View();
        }

        [HttpPost]
        public ActionResult OrderSend(Guid guid)
        {
            //var vic = OrderFacade.GetViewIChannelInfoByOrderId(guid);
            var log = OrderFacade.IChannelSend(guid);
            Dictionary<string, object> jo = new Dictionary<string, object>();
            jo.Add("success", true);
            return Json(jo);
        }

        #endregion

        #region ECouponSellerManagement
        /// <summary>
        /// 客服平台賣家管理系統
        /// </summary>
        /// <returns></returns>
        public ActionResult ECouponSellerManagement()
        {
            Dictionary<AgentChannel, string> allowNameSEnum = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Except(new AgentChannel[] { AgentChannel.NONE }).ToDictionary(t => (AgentChannel)t, t => t.ToString());
            ViewBag.allowNameSEnum = allowNameSEnum.Keys;

            //如json格式有遺漏或新增則修正(新增)格式
            SystemData systemData = SystemCodeManager.GetSystemDataByName("AgentOrderClassification");
            AgentOrderClassJsonModel jsonData = JsonConvert.DeserializeObject<AgentOrderClassJsonModel>(systemData.Data);
            List<string>jArray = new List<string>();

            foreach (var key in allowNameSEnum.Keys)
            {
                if (!jsonData.AllowName.Any(x => x.Key == (int)key))
                {
                    jsonData.AllowName.Add((int)key, jArray);
                    systemData.Data = JsonConvert.SerializeObject(jsonData);
                }
            }
            return View(systemData);
        }

        /// <summary>
        /// 客服平台賣家管理系統-儲存
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult ECouponSellerManagementSave(string data)
        {
            if (string.IsNullOrEmpty(data))
            {
                return Json(new { Success = false, Message = "Data為空值，請聯絡技術部。" });
            }

            JObject jsonData = JObject.Parse(data);
            Dictionary<int, string> allowNameSEnum = Enum.GetValues(typeof(AgentChannel)).Cast<AgentChannel>().Except(new AgentChannel[] { AgentChannel.NONE }).ToDictionary(t => (int)t, t => t.ToString());

            if (jsonData["ExceptionName"] == null)
            {
                return Json(new { Success = false, Message = "ExceptionName格式錯誤，請聯絡技術部。" });
            }

            foreach (var key in allowNameSEnum.Keys)
            {
                if (jsonData["AllowName"][key.ToString()] == null)
                {
                    return Json(new { Success = false, Message = "AllowName格式錯誤，請聯絡技術部。" });
                }
            }
            
            SystemData systemdata = SystemCodeManager.GetSystemDataByName("AgentOrderClassification");
            systemdata.Data = data;
            systemdata.ModifyId = this.HttpContext.User.Identity.Name;
            systemdata.ModifyTime = DateTime.Now;

            bool settingResult = SystemCodeManager.SetSystemData(systemdata);

            if (!settingResult)
            {
                return Json(new { Success = false, Message = "更新失敗，請聯絡技術部。" });
            }

            return Json(new { Success = true, Message = "更新成功，稍後將反映到前台！" });
        }
        #endregion ECouponSellerManagement

        #region OdmChannelRole
        /// <summary>
        /// 取得擁有的Root底下第一層Seller權限
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        private List<Guid> GetAllStoreGuidByUserName(string userName, Guid rootSellerGuid)
        {
            int userId = MemberFacade.GetUniqueId(userName);
            var sellerRoleGuids = ChannelFacade.GetOdmSellerRoleGuidsByUserId(userId, rootSellerGuid).ToList();
            var mainStoreRoles = ChannelFacade.GetOwnOneDownSeller(sellerRoleGuids, rootSellerGuid);
            return mainStoreRoles;
        }

        /// <summary>
        /// 更新Odm權限  ChannelOdmSellerRole [全刪後全加]
        /// </summary>
        /// <param name="guidArray"></param>
        /// <param name="userName"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OdmSellerRoleUpdate(string guidArray, string userName, Guid rootSellerGuid)
        {
            try
            {
                using (var trans = TransactionScopeBuilder.CreateReadCommitted())
                {
                    //1. 組ChannelOdmSellerRoleCollection
                    List<Guid> storeGuidList = JsonConvert.DeserializeObject<List<Guid>>(guidArray);
                    int userId = MemberFacade.GetUniqueId(userName);
                    ChannelOdmSellerRoleCollection collection = new ChannelOdmSellerRoleCollection();
                    foreach (Guid sellerguid in storeGuidList)
                    {
                        ChannelOdmSellerRole cor = new ChannelOdmSellerRole()
                        {
                            UserId = userId,
                            SellerGuid = sellerguid,
                            CreateUser = MemberFacade.GetUniqueId(User.Identity.Name),
                            CreateTime = DateTime.Now
                        };
                        collection.Add(cor);
                    }

                    if (collection.Any())
                    {
                        //2. 先刪
                        _ch.ChannelOdmSellerRoleDeleteByUserid(userId);
                        //3. 後加
                        bool setSuccess = _ch.ChannelOdmSellerRoleCollectionSet(collection);
                    }else
                    {
                        _ch.ChannelOdmSellerRoleDeleteByUserid(userId);
                    }
                    trans.Complete();
                }
            }
            catch (Exception e)
            {
                return Json(new { syscode = false, sysmsg = e.Message });
            }
            return Json(new { syscode = true, sysmsg = "成功" });
        }

        /// <summary>
        /// 初始化SkmShoppe角色Tree狀結構
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        public JsonResult OdmSellerRoleInit(string userName, Guid rootSellerGuid)
        {
            //取得user擁有的第一層權限
            List<Guid> roleGuidList = GetAllStoreGuidByUserName(userName, rootSellerGuid);

            //目前暫時取得IsShoppe，之後可從此將樹狀結構展開 目前為兩層
            List<JsTreeModel> skmShopps = new List<JsTreeModel>();
            skmShopps = _skm.SkmShoppeGetAll().Select(p => new JsTreeModel { Id = p.StoreGuid.Value, Text = p.BrandCounterName, Parent = p.SellerGuid.ToString(), State = { Opened = true } }).ToList();

            //取得skmParent 分店層 (暫時只有新光)
            List<JsTreeModel> skmParent = _sp.SellerGetSkmParentList().Select(p => new JsTreeModel { Id = p.Guid, Text = p.SellerName, Parent = "#", State = { Opened = true } }).ToList();
            skmShopps.AddRange(skmParent);

            foreach (var i in skmShopps)
            {
                if (roleGuidList.Contains(i.Id))
                {
                    i.State = new JsTreeStataModel() { Opened = true, Selected = true };
                }
            }

            return Json(skmShopps, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 增加Skm帳號的瀏覽資訊的權限
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="rootSellerGuid"></param>
        /// <returns></returns>
        public ActionResult OdmSellerRoleAdd(string userName, string rootSellerGuid)
        {
            OdmSellerRoleModel model = new OdmSellerRoleModel();

            model.OdmList = _ch.ChannelOdmCustomerGetAll().Select(p => new SelectListItem { Text = p.CustomerName, Value = p.SellerRootGuid.ToString() }).ToList();

            if (!string.IsNullOrEmpty(userName))
            {
                model.Memeber = MemberFacade.GetMember(userName);
            }

            return View(model);
        }
        #endregion

        #region FrontDealsDailyStat 成效報表

        public ActionResult FrontDealsDailyStat(FrontDealsDailyStatModel model)
        {
            model = model ?? new FrontDealsDailyStatModel();
            return View(model);
        }

        public ActionResult FrontDealsDailyStatExport(FrontDealsDailyStatModel model)
        {
            #region 輸入查詢條件 type check

            QueryRequestCheck(model);

            #endregion 輸入查詢條件 type check

            List<FrontDealsDailyStat> frontDealsDailyStats = ppe.GetFrontDealsDailyStatListByRange(model.FrontDealsStatStart.Value, model.FrontDealsStatEnd.Value.AddDays(1));

            var frontDealsDailyStatsInfo = frontDealsDailyStats.GroupBy(fddsgb => fddsgb.UserId).Select(fdds => new FrontDealsDailyStatInfo
            {
                UserId = fdds.Key,
                ABGroup = Enum.GetName(typeof(AppFrontDealManager.TesterGroup), AppFrontDealManager.ClassifysMemberTesterGroup(fdds.Key)),
                OSDevice = (fdds.Any(f=>f.DeviceAndroid) && fdds.Any(f=>f.DeviceIos))? "Both" : (fdds.Any(f=>f.DeviceAndroid) ? "Android" : (fdds.Any(f=>f.DeviceIos) ? "iOS" : "")),
                OrderCount = fdds.Sum(f => f.AppOrderCount),
                OrderTurnoverTotal =  fdds.Sum(f => f.AppOrderTurnoverTotal),
                OrderGrossProfitTotal = fdds.Sum(f => f.AppOrderGrossProfitTotal),
                FrontpageViewCount = fdds.Sum(f => f.AppFrontpageViewCount),
                FrontpageViewTotal = fdds.Sum(f => f.AppFrontpageViewTotal),
            }).ToList();

            string fileName = string.Format("FrontDealsStatReport{0:yyyy_MM_dd}_{1:yyyy_MM_dd}.xls", model.FrontDealsStatStart.Value, model.FrontDealsStatEnd.Value);

            var outputStream = FrontDealsStatReportExport(frontDealsDailyStatsInfo);

            //搭配View $.fileDownload使用 
            Response.Headers["Set-Cookie"] = "fileDownload=true; Path=/";

            return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = fileName };
        }

        private void QueryRequestCheck(FrontDealsDailyStatModel model)
        {
            if (model == null || !model.FrontDealsStatStart.HasValue || !model.FrontDealsStatEnd.HasValue)
            {
                throw new Exception("時間輸入有誤");
            }
        }

        private MemoryStream FrontDealsStatReportExport(List<FrontDealsDailyStatInfo> frontDealsDailyStatsInfo)
        {
            Workbook workbook = new HSSFWorkbook();
            Sheet sheet = workbook.CreateSheet("成效報表");
            
            List<string> titleDescs = new List<string>
            {
                "User_ID",
                "A/B群\n(奇數A/偶數B)",
                "OS-Device",
                "訂單數",
                "購買金額\n營業額\n(扣除現金券)_退貨後_未稅",
                "購買金額毛利額\n(扣除現金券)_退貨後_未稅",
                "首頁訪問深度\n(本次最遠走到的深度)",
                "首頁訪問次數",
            };
            FillTitle(sheet, titleDescs);

            FillDetail(sheet, frontDealsDailyStatsInfo);

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private static void FillDetail(Sheet sheet, List<FrontDealsDailyStatInfo> frontDealsStatInfos)
        {
            int rowIndex = 1;
            foreach (var frontDealsStatInfo in frontDealsStatInfos)
            {
                Cell newCell = null;
                Row row = sheet.CreateRow(rowIndex++);
                int colIndex = 0;

                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.UserId);
                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.ABGroup);
                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.OSDevice);
                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.OrderCount);

                newCell = row.CreateCell(colIndex++);
                newCell.SetCellType(CellType.NUMERIC);
                newCell.SetCellValue(Convert.ToDouble(frontDealsStatInfo.OrderTurnoverTotal));

                newCell = row.CreateCell(colIndex++);
                newCell.SetCellType(CellType.NUMERIC);
                newCell.SetCellValue(Convert.ToDouble(frontDealsStatInfo.OrderGrossProfitTotal));

                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.FrontpageViewCount);
                row.CreateCell(colIndex++).SetCellValue(frontDealsStatInfo.FrontpageViewTotal);
            }

            sheet.SetColumnWidth(0, 15 * 256);
        }

        private static void FillTitle(Sheet sheet, List<string> titleDescrs)
        {
            short lightPink = 45;
            CellStyle titleStyle = sheet.Workbook.CreateCellStyle();
            titleStyle.BackgroundColor(lightPink);
            titleStyle.WrapText = true;
            titleStyle.Alignment = HorizontalAlignment.CENTER;
            titleStyle.VerticalAlignment = VerticalAlignment.CENTER;

            Row cols = sheet.CreateRow(0);

            int colIndex = 0;
            foreach (var titleDescr in titleDescrs)
            {
                var cell = cols.CreateCell(colIndex);
                cell.SetCellValue(titleDescr);
                cell.CellStyle = titleStyle;
                sheet.AutoSizeColumn(colIndex);
                colIndex++;
            }

            int descWarpCount = titleDescrs.Max(titleDescr => Regex.Matches(titleDescr, "\n").Count);
            cols.HeightInPoints = Convert.ToSingle((descWarpCount + 1) * sheet.DefaultRowHeight / 20);
        }

        #endregion
    }

    public class PaymentRequestTestModel
    {
        public string result { get; set; }
    }

    public class SaveChoiceDealSettingModel
    {
        public List<Item> ChannelList { get; set; }
        public int TotalCount { get; set; }

        public class Item
        {
            public string ChannelId { get; set; }
            public int TakeCount { get; set; }
        }
    }
}

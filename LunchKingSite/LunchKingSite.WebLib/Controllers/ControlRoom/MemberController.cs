﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.BizLogic.Models.CustomerService;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using LunchKingSite.I18N;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.ControlRoom.Order;
using LunchKingSite.WebLib.Views;
using FamilyNetPincode = LunchKingSite.Core.Models.Entities.FamilyNetPincode;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class MemberController : BaseController
    {
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private ICmsProvider cp = ProviderFactory.Instance().GetProvider<ICmsProvider>();
        private IFamiportProvider fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private Core.IServiceProvider csp = ProviderFactory.Instance().GetProvider<Core.IServiceProvider>();
        private IAccountingProvider ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
        private ILog log = LogManager.GetLogger("MemberController");

        /// <summary>
        /// 訂單基本資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public ActionResult OrderDetail(Guid orderGuid)
        {
            var model = new MemberOrderModel();

            ViewOrderMemberBuildingSeller vombs = op.ViewOrderMemberBuildingSellerGet(orderGuid);
            if (!vombs.IsLoaded) return Content("查無訂單!");
            model.Vombs = vombs;

            var deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vombs.BusinessHourGuid);
            model.ViewPponDeal = deal;

            model.OrderGuid = orderGuid;
            model.IsCoupon = deal.DeliveryType == (int)DeliveryType.ToShop;
            model.IsIsp = vombs.ProductDeliveryType != (int)ProductDeliveryType.Normal;
            model.IsGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);

            Order order = op.OrderGet(vombs.OrderGuid);
            CashTrustLogCollection ctls = mp.CashTrustLogGetListByOrderGuid(vombs.OrderGuid, OrderClassification.LkSite);
            
            #region 收摺
            
            //訂單基本資料
            DateTime? payDate = OrderFacade.GetPaymentCompletedDate(vombs.OrderGuid);
            model.OrderInfo = new OrderInfo
            {
                OrderGuid = vombs.OrderGuid,
                OrderId = vombs.OrderId,
                OrderCreateTime = vombs.OrderCreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                DealName = deal.ItemName,
                DealNavigateUrl = string.Format("{0}/controlroom/ppon/setup.aspx?bid={1}", config.SiteUrl, deal.BusinessHourGuid),
                Bid = vombs.BusinessHourGuid,
                Price = vombs.Subtotal.ToString("C0"),
                DealNameTags = GetDealNameTags(deal),
                PaymentDetail = GetPaymentDetail(ctls, order), //付款資訊
                PayStatus = payDate == null ? "未付款" : "已付款",
                PayDate = payDate == null ? string.Empty : ((DateTime)payDate).ToString("yyyy/MM/dd HH:mm:ss"),
                OrderStatus = GetOrderStatusLocalized(vombs.OrderStatus),
            };

            //檔次與商家基本資料
            model.DealAndVendorInfo = new DealAndVendorInfo
            {
                DealUniqueId = vombs.UniqueId,
                Price = deal.ItemPrice.ToString("C0"),
                BusinessHourOrderTimeS = deal.BusinessHourOrderTimeS.ToString("yyyy/MM/dd HH:mm:ss"),
                BusinessHourOrderTimeE = deal.BusinessHourOrderTimeE.ToString("yyyy/MM/dd HH:mm:ss"),
                BusinessHourDeliverTimeS = deal.BusinessHourDeliverTimeS == null ? string.Empty : ((DateTime)deal.BusinessHourDeliverTimeS).ToString("yyyy/MM/dd"),
                BusinessHourDeliverTimeE = deal.BusinessHourDeliverTimeE == null ? string.Empty : ((DateTime)deal.BusinessHourDeliverTimeE).ToString("yyyy/MM/dd"),
                SellerName = string.Format("{0} {1}", vombs.SellerId, vombs.SellerName),
                SellerNavigateUrl = string.Format("../seller/seller_add.aspx?sid={0}", vombs.SellerGuid),
                VendorTel = vombs.SellerTel,
                VendorFax = vombs.SellerFax
            };

            if (deal.DeliveryType == (int)DeliveryType.ToShop)
            {
                var coupons = pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, vombs.OrderGuid);
                var coupon = coupons.FirstOrDefault();
                if (coupon != null)
                {
                    model.DealAndVendorInfo.StoreName = string.Format("{0} {1}", coupon.StoreSellerId, coupon.StoreName);
                    model.DealAndVendorInfo.StoreNavigateUrl = coupon.StoreGuid == null ? string.Empty : string.Format("../seller/seller_add.aspx?sid={0}", coupon.StoreGuid);
                    model.DealAndVendorInfo.StoreExpireInfo = GetStoreExpireDateContent(coupon);
                }
            }

            //訂購人基本資料
            model.ReceiverInfo = new ReceiverInfo
            {
                ReceiverName = vombs.MemberName, //收件人
                BuyerNavigateUrl = "../user/users_edit.aspx?username=" + System.Web.HttpUtility.UrlEncode(vombs.MemberEmail),
                Gender = vombs.Gender ?? 0,
                BuyerTel = vombs.OrderPhone,
                BuyerMobilePhone = vombs.OrderMobile,
                ReceiverAddress = vombs.DeliveryAddress != null ? vombs.DeliveryAddress.Replace("<hr />", string.Empty) : string.Empty
            };

            #endregion

            int refundCnt = op.GetRefundCountByOrderGuid(orderGuid);
            int exchangeCnt = op.GetExchangeCountByOrderGuid(orderGuid);
            model.IsShowRefundRingIcon = refundCnt + exchangeCnt > 0;

            var csCase = csp.GetViewCustomerServiceMessageByOrderGuid(orderGuid).OrderByDescending(x => x.ModifyTime).FirstOrDefault();
            if (csCase != null)
            {
                model.ServiceNo = csCase.ServiceNo;
            }

            return View("OrderDetail", model);
        }
        
        /// <summary>
        /// 宅配商品資訊
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="receiverName"></param>
        /// <param name="receiverAddress"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult DeliveryProductInfo(Guid orderGuid, string receiverName, string receiverAddress)
        {
            PponOrder odr = new PponOrder(orderGuid);
            DeliveryProductInfo model = new DeliveryProductInfo
            {
                ProductSpecList = odr.GetToHouseProductSummary().ToList(),
                Bid = odr.Bid,
                OrderGuid = orderGuid,
                ReceiverName = receiverName,
                ReceiverAddress = receiverAddress,
                IsNeedFillAtmInfoBeforeReturn = ReturnService.IsNeedFillAtmInfoBeforeReturn(orderGuid)
            };
            
            return PartialView("DeliveryProductInfo", model);
        }

        /// <summary>
        /// 憑證商品資訊
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult PponProductInfo(Guid orderGuid)
        {
            PponProductInfo model = new PponProductInfo();

            var pponInfos = new List<PponInfo>();
            ViewPponCouponCollection coupons = pp.ViewPponCouponGetListRegularOnly(ViewPponCoupon.Columns.OrderGuid, orderGuid);
            CashTrustLogCollection ctlogs = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            CashTrustLogCollection returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(orderGuid);

            foreach (var coupon in coupons)
            {
                if (coupon.CouponId == null) continue;

                var couponId = (int)coupon.CouponId;
                var ctl = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
                var returningCtl = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);

                bool isReservationLock = coupon.IsReservationLock.GetValueOrDefault(false);
                bool isPponChecked = !isReservationLock && IsCouponReturnable(couponId, ctlogs, returnFormCtlogs);
                var pi = new PponInfo
                {
                    CouponId = couponId,
                    SequenceNumber = coupon.SequenceNumber,
                    CouponCode = coupon.CouponCode,
                    CouponNavigateUrl = "../Verification/CouponVerification.aspx?csn=" + coupon.SequenceNumber,
                    CouponStatusDesc = CouponFacade.GetCouponStatus(ctl, returningCtl),
                    SmsSentCount = pp.SMSLogGetCountByPpon(coupon.MemberEmail, coupon.CouponId.ToString()),
                    IsCouponReturnable = isPponChecked,
                    IsPponChecked = isPponChecked
                };
                if (Helper.IsFlagSet(coupon.BusinessHourStatus, BusinessHourStatus.GroupCoupon))
                {
                    pi.IsCouponReturnable = false;
                }

                SetPponBookingInfo(pi, coupon);
                pponInfos.Add(pi);
            }

            model.PponInfos = pponInfos;
            model.Bid = ctlogs[0].BusinessHourGuid;
            model.OrderGuid = orderGuid;
            model.IsNeedFillAtmInfoBeforeReturn = ReturnService.IsNeedFillAtmInfoBeforeReturn(orderGuid);
            var ctlFirst = ctlogs.FirstOrDefault();
            model.BuyerUserId = ctlFirst != null ? ctlFirst.UserId : 0;
            
            return PartialView("PponProductInfo", model);
        }

        /// <summary>
        /// ATM 填單
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="buyerUserId"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult AtmFillForm(Guid orderGuid, int? buyerUserId = null)
        {
            if (buyerUserId == null || buyerUserId == 0)
            {
                var order = op.OrderGet(orderGuid);
                buyerUserId = order.IsLoaded ? order.UserId : 0;
            }

            var model = new AtmInfo();
            model.OrderGuid = orderGuid;
            model.BuyerUserId = buyerUserId;
            model.LastCtAtm = op.CtAtmRefundGetList(CtAtmRefund.Columns.Si, CtAtmRefund.Columns.OrderGuid + "=" + orderGuid).Reverse();
            
            return View("AtmFillForm", model);
        }

        /// <summary>
        /// 出貨資訊
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult OrderDetailProductShippingInfo(Tuple<ViewOrderMemberBuildingSeller, string> input)
        {
            ViewOrderMemberBuildingSeller vombs = input.Item1;
            string restrictions = input.Item2;
            
            List<ProductShippingInfo> model = new List<ProductShippingInfo>();

            List<ViewOrderShipList> voslList = op.ViewOrderShipListGetListByOrderGuid(vombs.OrderGuid).OrderByDescending(x => x.OrderShipModifyTime).ToList();
            ViewOrderShipList voslFirst = voslList.FirstOrDefault();

            bool hasDispatched = voslFirst != null && voslFirst.OrderShipModifyTime != null;
            bool isIsp = vombs.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup ||
                vombs.ProductDeliveryType == (int)ProductDeliveryType.SevenPickup;

            #region 已出貨

            if (isIsp || hasDispatched)
            {
                var sellerHistory = ISPFacade.GetIspOrderHistory(vombs.OrderGuid);
                foreach (var vosl in voslList)
                {
                    var si = new ProductShippingInfo();
                    si.OrderShipId = vosl.OrderShipId ?? 0;
                    si.CreateDate = vosl.OrderShipCreateTime == null ? string.Empty : ((DateTime)vosl.OrderShipCreateTime).ToString("yyyy/MM/dd");
                    si.ShippingType = isIsp ? "超取" : "宅配"; //Helper.GetDescription((ProductDeliveryType)vombs.ProductDeliveryType);

                    if (isIsp)
                    {
                        //超取
                        si.ShippingStatusDesc = Helper.GetDescription((GoodsStatus)(vosl.SellerGoodsStatus ?? 0));
                        si.ShippingCompanyName = Helper.GetDescription((ProductDeliveryType)vombs.ProductDeliveryType);
                        si.IspStoreName = vombs.ProductDeliveryType == (int)ProductDeliveryType.FamilyPickup 
                            ? vosl.FamilyStoreName : vosl.SevenStoreName;
                        si.ShippingNo = vosl.PreShipNo;
                        si.ActualShippingDate = sellerHistory.Where(p => p.DelvieryStatus == vosl.SellerGoodsStatus)
                                        .OrderByDescending(p => p.DeliveryTime).Select(p => p.DeliveryTime)
                                        .FirstOrDefault().ToString("yyyy/MM/dd");
                    }
                    else
                    {
                        //宅配
                        si.ShippingStatusDesc = "已出貨";
                        si.ShippingCompanyId = vosl.ShipCompanyId ?? -1;
                        si.ShippingCompanyName = string.IsNullOrEmpty(vosl.ShipCompanyName) ? string.Empty : vosl.ShipCompanyName;
                        si.ShippingNo = vosl.ShipNo;
                        si.ShippingMemo = vosl.ShipMemo;
                        si.ActualShippingDate = vosl.ShipTime == null ? string.Empty : ((DateTime)vosl.ShipTime).ToString("yyyy/MM/dd");
                        si.OrderShipModifyTime = vosl.OrderShipModifyTime == null ? string.Empty : ((DateTime)vosl.OrderShipModifyTime).ToString("yyyy/MM/dd HH:mm:ss");
                        si.OrderShipModifyId = vosl.OrderShipModifyId;
                    }
                    model.Add(si);
                }
            }
            
            #endregion

            ViewBag.OrderGuid = vombs.OrderGuid;
            ViewBag.Bid = vombs.BusinessHourGuid;
            ViewBag.HasDispatched = hasDispatched;
            ViewBag.IsIsp = isIsp;
            ViewBag.ShippingCompany = op.ShipCompanyGetList(null).Where(x => x.Status == true).OrderBy(x => x.Sequence).ToList();  //物流公司
            ViewBag.DealRestrictions = restrictions; //權益說明

            return View("ProductShippingInfo", model);
        }

        /// <summary>
        /// 退換貨記錄
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="isCoupon"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult ProductRefundAndExchangeInfo(Guid orderGuid, bool isCoupon, Guid bid)
        {
            #region 權限
            
            //登入者是否有退貨的權限
            bool canRefund = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Refund);

            //代銷訂單只限特定權限可使用取消退貨
            var channelAgent = AgentChannel.NONE;
            var canCancelRefund = channelAgent != AgentChannel.NONE
                ? CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.CancelAgentOrderRefund)
                : CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name, SystemFunctionType.Refund);

            #endregion

            ProductRefundAndExchangeInfo model = new ProductRefundAndExchangeInfo();
            model.IsCoupon = isCoupon;

            #region 退貨記錄

            model.ReturnInfos = new List<ReturnInfo>();
            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(orderGuid).OrderByDescending(x => x.CreateTime).ToList();
            foreach (var form in returnForms)
            {
                var ri = new ReturnInfo();
                ri.RefundId = form.Id;
                ri.CreateTime = form.CreateTime.ToString("yyyy/MM/dd HH:mm");
                ri.ItemSpec = form.GetReturnSpec().Replace(Environment.NewLine, "<br/>");
                ri.ReturnReason = form.ReturnReason;
                ri.ReceiverName = form.ReceiverName;
                ri.ReceiverAddress = form.ReceiverAddress;
                ri.VendorProcessTime = form.VendorProcessTime.ToString("yyyy/MM/dd HH:mm");

                #region 廠商處理進度
                ri.VendorProcessStatus = Helper.GetLocalizedEnum(form.VendorProgressStatus) ?? "資料有問題, 請聯絡技術部"; //VendorProgressStatus
                switch (form.VendorProgressStatus)
                {
                    case VendorProgressStatus.Unreturnable:
                    case VendorProgressStatus.UnreturnableProcessed:
                        ri.VendorProcessStatus += ":<br/>" + form.VendorMemo;
                        break;
                    case VendorProgressStatus.CompletedAndRetrievied:
                        ri.VendorProcessStatus += "<br/>" + form.GetCollectedSpec().Replace(Environment.NewLine, "<br/>");
                        break;
                }
                ri.UnreturnableProcessedVisible = form.ProgressStatus == ProgressStatus.Processing && 
                    form.VendorProgressStatus == VendorProgressStatus.Unreturnable;
                ri.RecoverVisible = form.VendorProgressStatus != VendorProgressStatus.Automatic && 
                    (form.ProgressStatus == ProgressStatus.Processing || form.ProgressStatus == ProgressStatus.Unreturnable);
                #endregion

                #region 退貨進度
                string refundDesc;
                switch (form.RefundType)
                {
                    case RefundType.Scash:
                        refundDesc = "退購物金";
                        break;
                    case RefundType.Cash:
                    case RefundType.ScashToCash:
                        refundDesc = "刷退";
                        break;
                    case RefundType.Atm:
                    case RefundType.ScashToAtm:
                        refundDesc = "退ATM";
                        break;
                    case RefundType.Tcash:
                    case RefundType.ScashToTcash:
                        refundDesc = string.Format("退{0}", Helper.GetEnumDescription(form.ThirdPartyPaymentSystem));
                        break;
                    default:
                        refundDesc = "資料有問題, 請聯絡技術部";
                        break;
                }
                ri.RefundStatus = string.Format("[{0}]{1}", refundDesc, OrderFacade.GetRefundStatus(form));
                #endregion

                ri.ReturnedItemSpec = form.GetRefundedSpec().Replace(Environment.NewLine, "<br/>");

                #region 功能
                
                //立即退貨
                ri.ImmediateRefundVisible = form.ProgressStatus == ProgressStatus.Processing &&
                    !form.ReturnFormRefunds.Any(x => x.IsInRetry) && form.RefundType != RefundType.ScashToAtm &&
                    form.RefundType != RefundType.ScashToCash && form.RefundType != RefundType.ScashToTcash;
                ri.ImmediateRefundEnable = canRefund;

                //取消退貨
                ri.CancelRefundVisible = form.IsCancelable();
                ri.CancelRefundEnable = canCancelRefund;

                //已更新資料
                ri.UpdateAtmRefundVisible = form.ProgressStatus == ProgressStatus.AtmFailed;

                //購物金轉現金
                string dummy;
                ri.ScashToCashVisible = form.CanChangeToCashRefund(out dummy);

                //轉退購物金
                string dummy2;
                ri.CashToSCashVisible = form.CanChangeToSCashRefund(out dummy2);
                
                //[回復]退購物金完成
                ri.RecoverToRefundScashVisible = (form.RefundType == RefundType.ScashToCash || form.RefundType == RefundType.ScashToTcash) &&
                    form.ProgressStatus == ProgressStatus.Processing;

                //人工匯退
                CtAtmRefund car = OrderFacade.GetCtAtmRefundByOid(orderGuid);
                ri.ArtificialRefundVisible = car.Status == (int)AtmRefundStatus.AchSend ||
                    car.Status == (int)AtmRefundStatus.AchProcess;
                // || car.Status == (int)AtmRefundStatus.RefundFail) 暫隱藏Atm退款失敗 可使用人工匯退功能

                //編輯收件資料
                ri.EditReceiverVisible = form.ProgressStatus == ProgressStatus.Processing && !form.ReturnFormRefunds.Any(x => x.IsInRetry);

                #endregion

                #region 折讓單

                EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(EinvoiceMain.Columns.OrderGuid, orderGuid.ToString(), true);
                EinvoiceMain einvoice = einvCol.FirstOrDefault(t => string.IsNullOrEmpty(t.InvoiceNumber));//優先取未開發票的資料
                if (einvoice == null)  //如果沒有未開發票的，則取第一筆資料 (?) 11/10 改取最後一筆
                { 
                    einvoice = einvCol.Count > 0 ? einvCol.OrderBy(x => x.InvoiceNumber).LastOrDefault() : new EinvoiceMain();
                }

                //目前是否為紙本折讓單
                bool isPaperAllowance = einvoice != null && einvoice.AllowanceStatus != (int)AllowanceStatus.ElecAllowance;

                //是否啟用轉換電子折讓單
                bool isEnableElecAllowance = einvoice != null && einvoice.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                    einvoice.OrderTime > config.EinvAllowanceStartDate;
                
                //是否走作廢
                bool isCancel = form.CreditNoteType == (int)AllowanceStatus.None;

                //發票折讓單的狀態
                ri.AllowanceDesc = GetAllowanceDesc(form, true);

                //紙本轉電子 (退貨處理中、目前為紙本、未收到CreditNote、需要折讓單)
                ri.ChangeToElcAllowanceVisible = form.ProgressStatus == ProgressStatus.Processing && isPaperAllowance 
                    && !form.IsCreditNoteReceived && !isCancel;
                ri.ChangeToElcAllowanceEnable = canRefund && isPaperAllowance && isEnableElecAllowance; //有退貨權限、可轉紙本、已啟用電子折讓單

                //電子轉紙本 (退貨處理中、目前非紙本、需要折讓單)
                ri.ChangeToPaperAllowanceVisible = form.ProgressStatus == ProgressStatus.Processing && !isPaperAllowance && !isCancel;
                ri.ChangeToPaperAllowanceEnable = canRefund && !isPaperAllowance; //有退貨權限、目前非紙本

                //折讓單已回
                bool isInvoiceCreate = ReturnService.IsInvoiceCreate(form);
                ri.ReceivedCreditNoteVisible = (form.ProgressStatus == ProgressStatus.Processing || form.IsCreditNoteReceived) &&
                    isPaperAllowance && isInvoiceCreate && !isCancel;
                ri.ReceivedCreditNoteEnable = !form.IsCreditNoteReceived && canRefund;

                #endregion
                
                ri.FinishDescription = OrderFacade.GetFinishStatusDescription(form);
                if (form.FinishTime != null)
                {
                    if (!string.IsNullOrEmpty(ri.FinishDescription))
                    {
                        ri.FinishDescription += "<br/>";
                    }
                    ri.FinishDescription += ((DateTime)form.FinishTime).ToString("yyyy/MM/dd HH:mm");
                }

                model.ReturnInfos.Add(ri);
            }

            #endregion

            #region 換貨記錄

            model.ExchangeInfos = new List<ExchangeInfo>();
            if (!isCoupon)
            {
                var exchangeInfos = OrderExchangeUtility.FindAllExchangeLog(orderGuid);
                foreach (var info in exchangeInfos)
                {
                    var ei = new ExchangeInfo();
                    ei.ExchangeId = info.Id;
                    ei.CreateTime = info.CreateTime.ToString("yyyy/MM/dd HH:mm:ss");
                    ei.ExchangeReason = info.Reason.Replace(Environment.NewLine, "<br/>"); ;
                    ei.ReceiverName = info.ReceiverName;
                    ei.ReceiverAddress = info.ReceiverAddress;
                    ei.VendorProcessTime = info.VendorProcessTime == null ? string.Empty : ((DateTime)info.VendorProcessTime).ToString("yyyy/MM/dd HH:mm:ss");
                    ei.VendorProcessTime = info.VendorProcessTime == null ? string.Empty : ((DateTime)info.VendorProcessTime).ToString("yyyy/MM/dd HH:mm:ss");
                    ei.VendorProcessStatus = info.VendorProgressStatus == null
                        ? string.Empty : Helper.GetLocalizedEnum((ExchangeVendorProgressStatus)info.VendorProgressStatus);
                    ei.FinishDescription = Helper.GetLocalizedEnum((OrderReturnStatus)info.Status) ?? "UnKnown";

                    ei.CompleteExchangeVisible = info.VendorProgressStatus == (int)ExchangeVendorProgressStatus.ExchangeCompleted &&
                        info.Status != (int)OrderReturnStatus.ExchangeSuccess && info.Status != (int)OrderReturnStatus.ExchangeCancel &&
                        info.Status != (int)OrderReturnStatus.ExchangeFailure;

                    ei.UpdateOrderShipVisible = info.OrderShipId != null;
                    if (info.Status == (int)OrderReturnStatus.ExchangeSuccess ||
                        info.Status == (int)OrderReturnStatus.ExchangeCancel ||
                        info.Status == (int)OrderReturnStatus.ExchangeFailure)
                    {
                        if (info.ModifyTime != null)
                        {
                            if (!string.IsNullOrEmpty(ei.FinishDescription))
                            {
                                ei.FinishDescription += "<br/>";
                            }
                            ei.FinishDescription += info.ModifyTime.Value.ToString("yyyy/MM/dd HH:mm");
                        }
                        ei.CancelExchangeVisible = false;
                        ei.ExchangeMessageVisible = false;
                        ei.EditExchangeReceiverVisible = false;
                    }
                    else
                    {
                        ei.CancelExchangeVisible = true;
                        ei.ExchangeMessageVisible = true;
                        ei.EditExchangeReceiverVisible = true;
                    }

                    model.ExchangeInfos.Add(ei);
                }
            }

            #endregion

            //退貨歷程
            model.AuditReturnHistory = new List<AuditHistory>();
            AuditCollection auditCol = cp.AuditGetListByIdAndType(orderGuid.ToString(), AuditType.Refund);
            foreach (var audit in auditCol.OrderByDescending(x => x.Id))
            {
                model.AuditReturnHistory.Add(new AuditHistory
                {
                    CreateTime = audit.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    Message = audit.Message,
                    CreateId = audit.CreateId
                });
            }

            //換貨歷程
            model.ExchangeHistory = new List<ExchangeLog>();
            List<OrderStatusLog> exchangeHistory = op.OrderStatusLogGetList(orderGuid).OrderByDescending(x => x.CreateTime).ToList();
            foreach (var excLog in exchangeHistory)
            {
                string vendorProgressStatus = excLog.VendorProgressStatus == null 
                    ? string.Empty
                    : Helper.GetLocalizedEnum((ExchangeVendorProgressStatus)excLog.VendorProgressStatus);

                model.ExchangeHistory.Add(new ExchangeLog
                {
                    CreateTime = excLog.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    Status = GetOrderLogStatusDesc((OrderLogStatus)excLog.Status),
                    VendorProgressStatus = vendorProgressStatus,
                    Message = excLog.Message,
                    CreateId = excLog.CreateId
                });
            }

            ViewBag.Bid = bid;

            return View("ProductRefundAndExchangeInfo", model);
        }

        /// <summary>
        /// 發票資訊
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult InvoiceInfo(Guid orderGuid)
        {
            InvoiceInfo model = new InvoiceInfo();
            model.OrderGuid = orderGuid;
            EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(EinvoiceMain.Columns.OrderGuid, orderGuid.ToString(), true);
            //優先取未開發票的資料
            EinvoiceMain einvoice = einvCol.FirstOrDefault(t => string.IsNullOrEmpty(t.InvoiceNumber));
            //如果沒有未開發票的，則取第一筆資料 (?) 11/10 改取最後一筆
            
            if (einvoice == null)
            {
                einvoice = einvCol.Any() ? einvCol.OrderBy(x => x.InvoiceNumber).LastOrDefault() : new EinvoiceMain();
            }

            if (einvoice != null && einvoice.Id != 0)
            {
                #region 發票基本資料

                model.Id = einvoice.Id;
                model.InvoiceNo = einvoice.InvoiceNumber;
                model.InvoiceCount = einvCol.Count;
                //捐贈發票
                model.InvoiceDonateMark = einvoice.IsDonateMark ? "是，" + EinvoiceFacade.GetDonateCodeName(einvoice) : "否";

                //二聯
                if (einvoice.IsEinvoice2)
                {
                    model.InvoiceMode = InvoiceMode2.Duplicate;
                    model.InvoiceModeDesc = Helper.GetEnumDescription(InvoiceMode2.Duplicate);
                    model.BuyerName = einvoice.InvoiceBuyerName;
                    model.BuyerAddress = einvoice.InvoiceBuyerAddress;

                    int nextMon = einvoice.InvoiceNumberTime != null ? (((DateTime)einvoice.InvoiceNumberTime).Month % 2 == 0 ? 1 : 2) : 0;
                    model.IsReadOnly = einvoice.IsIntertemporal && einvoice.InvoiceNumberTime != null &&
                        ((DateTime)einvoice.InvoiceNumberTime).AddMonths(nextMon).AddDays(config.InvoiceBufferDays) < DateTime.Now;
                }

                //三聯
                if (einvoice.IsEinvoice3)
                {
                    model.InvoiceMode = InvoiceMode2.Triplicate;
                    model.InvoiceModeDesc = Helper.GetEnumDescription(InvoiceMode2.Triplicate);
                    model.BuyerName = einvoice.InvoiceBuyerName;
                    model.BuyerAddress = einvoice.InvoiceBuyerAddress;
                    model.CompanyId = einvoice.InvoiceComId;
                    model.InvoiceTitle = einvoice.InvoiceComName;
                }

                //發票版本 與 載具
                if (einvoice.Version == (int)InvoiceVersion.Old)
                {
                    //model.InvoiceVersion = "【舊電子發票】";
                }
                else if (einvoice.Version == (int)InvoiceVersion.CarrierEra)
                {
                    //model.InvoiceVersion = "【發票載具】";
                    CarrierType carrierType = (CarrierType)einvoice.CarrierType;
                    model.CarrierType = carrierType;
                    model.CarrierTypeDesc = Helper.GetLocalizedEnum(carrierType) +
                        (carrierType == CarrierType.Phone || carrierType == CarrierType.PersonalCertificate
                            ? " : " + einvoice.CarrierId : string.Empty);
                    model.CarrierId = carrierType == CarrierType.Phone || carrierType == CarrierType.PersonalCertificate
                        ? einvoice.CarrierId
                        : string.Empty;
                }

                var member = MemberFacade.GetMember(einvoice.UserId);
                model.BuyerEmail = member.UserEmail;
                model.ViewReturnDiscountFormUrl = string.Format("../../User/ReturnDiscount.aspx?u={0}&oid={1}", member.UserName, orderGuid);
                model.ExportReturnDiscountFormUrl = string.Format("../../Service/returndiscount.ashx?u={0}&oid={1}", member.UserName, orderGuid);

                model.InvoiceRequestTime = einvoice.InvoiceRequestTime;
                model.PrintInvoiceVisible = !string.IsNullOrEmpty(einvoice.InvoiceNumber) && 
                    einvoice.IsAllowPaperd && einvoice.InvoiceSumAmount > 0 && 
                    !einvoice.IsDonateMark && (einvoice.InvoiceStatus ?? 0) == (int)EinvoiceType.C0401 && 
                    einvoice.InvoiceRequestTime == null;

                #endregion

                //發票歷程
                model.EinvoiceChangeLog = EinvoiceFacade.GetEinvoiceChangeLogMainData(orderGuid);
            }

            

            return View("InvoiceInfo", model);
        }

        /// <summary>
        /// 客服案件資訊
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult CustomerServiceInfo(Guid orderGuid)
        {
            CustomerServiceInfo model = new CustomerServiceInfo();
            
            #region 關聯案件同訂單
            
            var relationCaseList = csp.GetViewCustomerServiceMessageByOrderGuid(orderGuid).OrderByDescending(x=>x.ModifyTime).ToList();
            foreach (var rc in relationCaseList)
            {
                model.RelationCase.Add(new CustomerServiceRelationCase {
                    ServiceNo = rc.ServiceNo,
                    Status = Helper.GetEnumDescription((statusConvert)rc.CustomerServiceStatus),
                    Priority = Helper.GetEnumDescription((priorityConvert)rc.CasePriority),
                    CreateDate = rc.CreateTime.ToString("yyyy/MM/dd HH:mm"),
                    ServicePeople = rc.ServicePeopleName,
                    ParentServiceNo = rc.ParentServiceNo
                });
            }

            #endregion
            
            return View("CustomerServiceInfo", model);
        }

        public ActionResult CustomerServiceCase(string serviceNo)
        {
            CaseData model = new CaseData();
            ViewCustomerServiceList vcsl = csp.GetViewCustomerServiceMessageByServiceNo(serviceNo);
            if (vcsl.IsLoaded)
            {
                #region 客戶資料
                //if (vcsl.UserId != null)
                //{
                //    Member m = mp.MemberGet((int)vcsl.UserId);
                //    model.CustomerMail = m.UserEmail;

                //    ViewMemberBuildingCityParentCity vmbcpc = mp.ViewMemberBuildingCityParentCityGetByUserName(m.UserName);
                //    model.CustomerMobile = vmbcpc.Mobile;
                //}
                #endregion
                
                //案件資料
                model = CustomerServiceFacade.GetCaseData(vcsl, MemberFacade.GetUniqueId(User.Identity.Name));
            }

            return View("CustomerServiceCase", model);
        }

        #region WebApi 客服操作功能

        #region PART 1. 更新基本資料功能

        /// <summary>
        /// 更新基本資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="phone">買家電話</param>
        /// <param name="mobile">買家手機</param>
        /// <param name="address">出貨地址</param>
        /// <returns></returns>
        public JsonResult UpdataOrderReceiver(Guid orderGuid, string phone, string mobile, string address)
        {
            Order o = op.OrderGet(orderGuid);
   
            string auditStr = Phrase.ResourceManager.GetString("Edit") + " ";
            if ((o.MobileNumber ?? string.Empty) != mobile)
            {
                auditStr += Phrase.ResourceManager.GetString("MobileNumber") + ": " + o.MobileNumber + "->" + mobile + "|";
                o.MobileNumber = mobile;
            }

            if ((o.PhoneNumber ?? string.Empty) != phone)
            {
                auditStr += Phrase.ResourceManager.GetString("CompanyTel") + ": " + o.PhoneNumber + "->" + phone + "|";
                o.PhoneNumber = phone;
            }

            if ((o.DeliveryAddress ?? string.Empty) != address)
            {
                auditStr += Phrase.ResourceManager.GetString("DeliveryAddress") + ": " + o.DeliveryAddress + "->" + address + "|";
                o.DeliveryAddress = address;
            }

            if (o.IsDirty)
            {
                op.OrderSet(o);
                CommonFacade.AddAudit(o.Guid.ToString(), AuditType.Order, auditStr, User.Identity.Name, false);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "更新成功"
                });
            }
            
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "資料未變更"
            });
        }

        /// <summary>
        /// 補憑證信託資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public JsonResult CashTrustlogFilled(Guid orderGuid)
        {
            string resultMsg = ReturnService.PatchCashTrustLog(orderGuid, User.Identity.Name);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = string.IsNullOrEmpty(resultMsg) ? "執行成功!" : resultMsg
            });
        }

        /// <summary>
        /// 成套票券退款細目
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public JsonResult GetTrustCouponDetail(Guid orderGuid)
        {
            List<TrustCouponDetail> trustCoupons = new List<TrustCouponDetail>();
            CashTrustLogCollection ctls = mp.CashTrustLogGetListByOrderGuid(orderGuid, OrderClassification.LkSite);
            CashTrustLogCollection returnFormCtlogs = mp.PponRefundingCashTrustLogGetListByOrderGuid(orderGuid);
            foreach (var ctl in ctls.OrderByDescending(x => x.Amount))
            {
                trustCoupons.Add(new TrustCouponDetail {
                    TrustSequenceNumber = ctl.TrustSequenceNumber ?? string.Empty,
                    Amount = ctl.Amount == 0 ? "--" : (ctl.Amount - ctl.DiscountAmount).ToString("F0"),
                    CouponStatusDesc = GetCouponStatus(ctl.CouponId ?? 0, ctls, returnFormCtlogs),
                    UsageVerifiedTime = ctl.UsageVerifiedTime == null ? string.Empty : ((DateTime)ctl.UsageVerifiedTime).ToString("yyyy/MM/dd HH:mm:ss")
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = trustCoupons
            });
        }

        /// <summary>
        /// 查詢記錄
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public JsonResult GetAuditHistory(Guid orderGuid)
        {
            List<AuditHistory> auditHistory = new List<AuditHistory>();
            List<Audit> auditList = cp.AuditGetList(orderGuid.ToString(), true).OrderByDescending(x => x.CreateTime).ToList();
            foreach (var audit in auditList)
            {
                auditHistory.Add(new AuditHistory
                {
                    CreateTime = audit.CreateTime.ToString("yyyy/MM/dd HH:mm:ss"),
                    Message = audit.Message,
                    CreateId = audit.CreateId
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = auditHistory
            });
        }

        /// <summary>
        /// 新增記錄
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        public JsonResult AddAudit(Guid orderGuid, string msg)
        {
            CommonFacade.AddAudit(orderGuid, AuditType.Member, msg.Trim(), User.Identity.Name, false);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }
        
        /// <summary>
        /// 查詢訂單備註
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public JsonResult GetOrderUserMemo(Guid orderGuid)
        {
            List<OrderUserMemoDetail> memoList = new List<OrderUserMemoDetail>();
            var orderMemoCol = op.OrderUserMemoListGetList(orderGuid).OrderByDescending(x => x.CreateTime);
            foreach (var memo in orderMemoCol)
            {
                memoList.Add(new OrderUserMemoDetail {
                    Id = memo.Id,
                    CreateId = memo.CreateId,
                    UserMemo = memo.UserMemo,
                    CreateTime = memo.CreateTime.ToString("yyyy/MM/dd HH:mm")
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = memoList
            });
        }

        /// <summary>
        /// 新增訂單備註
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="orderUserMemo"></param>
        /// <returns></returns>
        public JsonResult AddOrderUserMemo(Guid orderGuid, string orderUserMemo)
        {
            var userMemo = new OrderUserMemoList
            {
                OrderGuid = orderGuid,
                DealType = (int)VbsDealType.Ppon,
                UserMemo = orderUserMemo.Trim(),
                CreateId = User.Identity.Name,
                CreateTime = DateTime.Now
            };
            op.OrderUserMemoListSet(userMemo);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }

        /// <summary>
        /// 刪除訂單備註
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult DeleteOrderUserMemo(int id)
        {
            op.OrderUserMemoListDelete(id);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }

        /// <summary>
        /// 照會處理
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="referFail"></param>
        /// <returns></returns>
        public JsonResult SetCreditcardRefer(Guid orderGuid, bool referFail)
        {
            CreditcardOrder co = op.CreditcardOrderGetByOrderGuid(orderGuid);
            bool isProcessSuccess = false;
            if (co.IsLoaded)
            {
                isProcessSuccess = referFail 
                    ? PaymentFacade.SetCreditcardOrderReferFail(co.Id.ToString().Split(","), User.Identity.Name) 
                    : PaymentFacade.SetCreditcardOrderRefer(co.Id.ToString().Split(","), User.Identity.Name);
            }

            if (isProcessSuccess)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "處理成功"
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "處理失敗"
            });
        }

        /// <summary>
        /// 廠商罰款 - 扣款/異動原因
        /// </summary>
        /// <returns></returns>
        public JsonResult GetVendorFineCategory(bool isCoupon)
        {
            var vendorFineCategory = ap.VendorFineCategoryGetList().ToList();
            if (isCoupon)
            {
                vendorFineCategory = vendorFineCategory.Where(x => x.Type == (int)VendorFineType.Change).ToList();
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = vendorFineCategory
            });
        }

        /// <summary>
        /// 取得對帳號
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public JsonResult GetBalanceSheetList(Guid bid)
        {
            var vbslc = ap.ViewBalanceSheetListGetList(new List<Guid> { bid }).Where(x => !(x.IsConfirmedReadyToPay && x.EstAmount != 0))
                .OrderByDescending(x=>x.IntervalEnd).ToList();
            List<KeyValuePair<int, string>> valanceSheetList = new List<KeyValuePair<int, string>>();
            foreach (var vbsl in vbslc)
            {
                string value = GetBalanceSheetFrequenyDesc(vbsl.GenerationFrequency, vbsl.Year + "/" + vbsl.Month, vbsl.IntervalEnd);
                valanceSheetList.Add(new KeyValuePair<int, string>(vbsl.Id, value));
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = valanceSheetList
            });
        }

        /// <summary>
        /// 更新對帳單
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public JsonResult UpdateNextBalanceSheet(VendorPaymentFineInputModel input)
        {
            if (!input.IsCompensate)
            {
                input.PromotionBonus = 0;
                input.BonusAction = string.Empty;
            }

            /* 從 order_detail.aspx.cs 搬過來的 */
            try
            {
                int vendorPaymentId = 0;
                if (input.FineType == (int)VendorFineType.Change)
                {
                    //異動金額
                    var vpc = new VendorPaymentChange
                    {
                        BusinessHourGuid = input.Bid,
                        BalanceSheetId = input.BalanceSheetId,
                        Reason = input.Reason,
                        Amount = input.Amount,
                        PromotionValue = input.PromotionBonus,

                        AllowanceAmount = input.Amount,
                        CreateId = User.Identity.Name,
                        CreateTime = DateTime.Now,
                        ModifyId = User.Identity.Name,
                        ModifyTime = DateTime.Now,
                    };
                    vendorPaymentId = ap.VendorPaymentChangeSet(vpc);
                }
                else if (input.FineType == (int)VendorFineType.Overdue)
                {
                    //逾期出貨
                    var vpo = new VendorPaymentOverdue
                    {
                        BusinessHourGuid = input.Bid,
                        BalanceSheetId = input.BalanceSheetId,
                        Reason = input.Reason,
                        Amount = input.Amount,
                        PromotionAmount = input.PromotionBonus,

                        OrderGuid = input.OrderGuid,
                        CreateId = User.Identity.Name,
                        CreateTime = DateTime.Now
                    };
                    vendorPaymentId = ap.VendorPaymentOverdueSet(vpo);
                }

                //異動之前對帳單
                if (input.BalanceSheetId != null)
                {
                    var updateBsIds = new List<int>();

                    #region 更新廠商補扣款資料

                    if (vendorPaymentId != 0)
                    {
                        if (input.FineType == (int)VendorFineType.Change)
                        {
                            var paymentChanges = ap.VendorPaymentChangeGetList(new List<int>() { vendorPaymentId }.AsEnumerable());
                            foreach (var paymentChange in paymentChanges)
                            {
                                try
                                {
                                    if (paymentChange.BalanceSheetId.HasValue)
                                    {
                                        updateBsIds.Add(paymentChange.BalanceSheetId.Value);
                                    }
                                    var updateBsId = input.BalanceSheetId;
                                    var actionDesc = string.Format("Id:{0} 廠商補扣款關聯對帳單更新 : {1} -> {2}", paymentChange.Id, paymentChange.BalanceSheetId, updateBsId);
                                    paymentChange.BalanceSheetId = updateBsId;
                                    ap.VendorPaymentChangeSet(paymentChange);
                                    CommonFacade.AddAudit(paymentChange.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                                }
                                catch (Exception)
                                {
                                    return Json(new ApiResult
                                    {
                                        Code = ApiResultCode.Error,
                                        Message = string.Format("Id:{0} 廠商補扣款關聯對帳單更新失敗，請洽技術部。\r\n", paymentChange.Id)
                                    });
                                }
                            }
                        }
                        else if (input.FineType == (int)VendorFineType.Overdue)
                        {
                            var paymentOverdues = ap.VendorPaymentOverdueGetListByOverdueId(vendorPaymentId);
                            foreach (var paymentOverdue in paymentOverdues)
                            {
                                try
                                {
                                    if (paymentOverdue.BalanceSheetId.HasValue)
                                    {
                                        updateBsIds.Add(paymentOverdue.BalanceSheetId.Value);
                                    }
                                    var updateBsId = input.BalanceSheetId;
                                    var actionDesc = string.Format("Id:{0} 廠商補扣款關聯對帳單更新 : {1} -> {2}", paymentOverdue.Id, paymentOverdue.BalanceSheetId, updateBsId);
                                    paymentOverdue.BalanceSheetId = updateBsId;
                                    ap.VendorPaymentOverdueSet(paymentOverdue);
                                    CommonFacade.AddAudit(paymentOverdue.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                                }
                                catch (Exception)
                                {
                                    return Json(new ApiResult
                                    {
                                        Code = ApiResultCode.Error,
                                        Message = string.Format("Id:{0} 廠商補扣款關聯對帳單更新失敗，請洽技術部。\r\n", paymentOverdue.Id)
                                    });
                                }
                            }
                        }


                    }

                    #endregion 更新廠商補扣款資料

                    #region 更新對帳單金額

                    foreach (var bsId in updateBsIds)
                    {
                        var bsModel = new BalanceSheetModel(Convert.ToInt32(input.BalanceSheetId));
                        var bs = bsModel.GetDbBalanceSheet();
                        var newAmount = (int)bsModel.GetAccountsPayable().TotalAmount;
                        var newOverdueAmount = (int)bsModel.GetAccountsPayable().OverdueAmount;
                        var newVendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                        var newVendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;
                        //更新對帳單金額
                        //只有尚未確認或是金額為0才能異動
                        if (bs.EstAmount != newAmount && (!bs.IsConfirmedReadyToPay || bs.EstAmount == 0))
                        {
                            try
                            {
                                var actionDesc = string.Format("Id : {0} UpdateBalanceSheetEstAmount : {1} -> {2}", bs.Id, bs.EstAmount, newAmount);
                                bs.EstAmount = newAmount;
                                bs.OverdueAmount = newOverdueAmount;
                                ap.BalanceSheetEstAmountSet(bs, newVendorPositivePaymentOverdueAmount, newVendorNegativePaymentOverdueAmount);
                                CommonFacade.AddAudit(bs.ProductGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                                var bsInfos = ap.GetPayByBillBalanceSheetHasNegativeAmount(input.Bid);
                                if (bsInfos.AsEnumerable().Any())
                                {
                                    BalanceSheetManager.ProductBalanceSheetWithNegativeAmountMailNotify(bsInfos);
                                }
                            }
                            catch (Exception)
                            {
                                return Json(new ApiResult
                                {
                                    Code = ApiResultCode.Error,
                                    Message = string.Format("Id:{0} 對帳單應付金額更新失敗，請洽技術部。\r\n", bsId)
                                });
                            }
                        }
                    }


                    #endregion 更新對帳單金額
                }

                //補給消費者紅利金
                if (input.IsCompensate)
                {
                    Order o = op.OrderGet(input.OrderGuid);
                    var mpd = new MemberPromotionDeposit
                    {
                        OrderGuid = input.OrderGuid,
                        StartTime = input.BonusStartTime,
                        ExpireTime = input.BonusExpireTime,
                        PromotionValue = input.PromotionBonus * 10, // 紅利金換算規則為 : 台幣金額 * 10倍
                        Action = input.BonusAction,
                        CreateId = User.Identity.Name,
                        CreateTime = DateTime.Now,
                        UserId = o.UserId,
                        OrderClassification = (int)OrderClassification.LkSite,
                        VendorPaymentChangeId = vendorPaymentId
                    };
                    MemberFacade.MemberPromotionProcess(mpd, o.CreateId, false);
                }

                //紀錄log
                string msg = string.Format("扣款/異動金額：{0}元 <br /> 扣款 / 異動原因：{1}<br />  紅利金摘要：{2} <br />  須給消費者紅利金：{3}元", 
                    input.Amount, input.Reason, input.BonusAction, input.PromotionBonus);
                CommonFacade.AddAudit(input.OrderGuid, AuditType.Order, msg, User.Identity.Name, false);
                
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "異動成功"
                });
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "異常錯誤，請洽技術部!"
                });
            }
        }

        #endregion

        #region PART 2. 退換貨功能

        /// <summary>
        /// 發送憑證簡訊
        /// </summary>
        /// <param name="couponId"></param>
        /// <returns></returns>
        public JsonResult SendPponSms(int couponId)
        {
            ViewPponCoupon c = pp.ViewPponCouponGet(couponId);
            Member m = mp.MemberGet(c.MemberEmail);


            SmsContent sc = pp.SMSContentGet(c.BusinessHourGuid);
            string[] msgs = sc.Content.Split('|');
            if (c.CouponId != null)
            {
                //判斷天貓檔次
                if ((c.BusinessHourStatus & (int)BusinessHourStatus.TmallDeal) > 0)
                {
                    OrderCorresponding oc = op.OrderCorrespondingListGetByOrderGuid(c.OrderGuid);
                    m.Mobile = oc.Mobile;
                }
                DateTime finalExpireDate = GetFinalExpireDate(c.OrderGuid.ToString());
                string branch = OrderFacade.SmsGetPhone(pp.CouponGet(c.CouponId.Value).OrderDetailId);    //分店資訊 & 多重選項
                string msg = CouponFacade.SmsMessage(c.BusinessHourGuid, c.GroupOrderStatus, c.CouponCode, branch, msgs, c.SequenceNumber,
                    c.BusinessHourDeliverTimeS, finalExpireDate, c.CouponStoreSequence);

                SMS sms = new SMS();
                sms.SendMessage("", msg, "", m.Mobile, m.UserName, SmsType.Ppon, c.BusinessHourGuid, c.CouponId.ToString());
                var smsCount = pp.SMSLogGetCountByPpon(m.UserName, couponId.ToString());
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = smsCount
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error
            });
        }

        /// <summary>
        /// 建立宅配退/換貨單
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public JsonResult CreateProductRefund(ProductRefundInputModel i)
        {
            if (string.IsNullOrEmpty(i.ReceiverName) || 
                string.IsNullOrEmpty(i.ReceiverAddress) ||
                string.IsNullOrEmpty(i.RefundReason))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.SaveFail,
                    Message = "申請原因或收件資訊沒有填!"
                });
            }

            string message;
            bool isSuccess = i.IsExchange 
                ? CreateExchange(i, out message) 
                : CreateRefund(i, out message);
            
            if (i.IsExchange || !i.IsExchange && isSuccess)
            {
                //換貨 or 退貨成功時
                ClearFinalBalanceSheetDate(i.OrderGuid, i.Bid);
            }
            
            return Json(new ApiResult
            {
                Code = isSuccess ? ApiResultCode.Success : ApiResultCode.SaveFail,
                Message = message
            });
        }

        /// <summary>
        /// 建立憑證退/換貨單
        /// </summary>
        /// <param name="i"></param>
        /// <returns></returns>
        public JsonResult CreatePponRefund(ProductPponInputModel i)
        {
            if (!i.CouponIdList.Any())
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "請選擇憑證!"
                });
            }

            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGet("order_guid", i.OrderGuid.ToString(), true);
            if (emc.Count > 0)
            {
                if (emc.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401).ToList().Count > 0)
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "發票二聯改三聯尚未完成，無法申請退貨"
                    });
                }
            }

            ViewCouponListMain vclm = mp.GetCouponListMainByOid(i.OrderGuid);
            string errorMsg = string.Empty;
            if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int) BusinessHourStatus.GroupCoupon) &&
                Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int) GroupOrderStatus.FamiDeal))
            {
                //全家寄杯退貨
                if (!OrderFacade.FamilyNetPincodeReturn(i.OrderGuid, i.Bid, out errorMsg))
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = errorMsg
                    });
                }

            }
            else if (Helper.IsFlagSet(vclm.BusinessHourStatus, (int) BusinessHourStatus.GroupCoupon) &&
                     Helper.IsFlagSet(vclm.GroupOrderStatus.Value, (int) GroupOrderStatus.HiLifeDeal))
            {
                //萊爾富寄杯
                errorMsg = string.Empty;
                if (config.EnableHiLifeDealSetup && !OrderFacade.HiLifePincodeReturn(i.OrderGuid, out errorMsg))
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = errorMsg
                    });
                }
            }

            int? returnFormId;
            CreateReturnFormResult result = ReturnService.CreateCouponReturnForm(
                i.OrderGuid, i.CouponIdList, i.IsSCashOnly, User.Identity.Name, i.RefundReason, out returnFormId, true);

            if (result != CreateReturnFormResult.Created)
            {
                switch (result)
                {
                    case CreateReturnFormResult.ProductsUnreturnable:
                        errorMsg = "欲退項目的狀態無法退貨";
                        break;
                    case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                        errorMsg = "ATM訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                        errorMsg = "舊訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.InvalidArguments:
                        errorMsg = "無法建立退貨單, 請洽技術部";
                        break;
                    case CreateReturnFormResult.OrderNotCreate:
                        errorMsg = "訂單未完成付款, 無法建立退貨單";
                        break;
                    case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                        errorMsg = "分期且部分退, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.UnusedCouponLock:
                        errorMsg = @"注意！本套成套票券憑證中，有處於鎖定憑證的狀態，目前無法申請退貨。\n 若您需申請退貨，請先將鎖定的憑證依下列建議先行處理：\n 1.先將鎖定的憑證兌換完畢後，再進行退貨申請\n 2.依照旅遊定型化契約規範與商家先行聯繫處理後，再進行退貨申請。";
                        break;
                    default:
                        errorMsg = "不明錯誤, 請洽技術部";
                        break;
                }

                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = errorMsg
                });
            }
            if (emc.Count > 0 && emc.Any(x => x.InvoiceRequestTime.HasValue))
            {
                UserRefundFacade.EinvocieCancelRequestProcess(emc, i.OrderGuid, i.CouponIdList.ToList(), DeliveryType.ToShop, User.Identity.Name);
            }
            string auditMessage = string.Format("退貨單({0})建立. 原因:{1}", returnFormId, i.RefundReason);
            CommonFacade.AddAudit(i.OrderGuid, AuditType.Refund, auditMessage, User.Identity.Name, false);

            if (returnFormId != null) OrderFacade.SendCustomerReturnFormApplicationMail((int)returnFormId);
            //憑證不用寄(宅配)退貨通知給廠商

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = string.Format("退貨單({0})建立成功!", returnFormId)
            });

        }

        /// <summary>
        /// 取得銀行資料
        /// </summary>
        /// <returns></returns>
        public JsonResult GetBankInfo()
        {
            var bankinfos = op.BankInfoGetMainList();
            List<AtmBankInfo> banks = new List<AtmBankInfo>();
            foreach (var bank in bankinfos)
            {
                banks.Add(new AtmBankInfo
                {
                    BankNo = bank.BankNo,
                    BankName = bank.BankName
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = banks
            });
        }

        /// <summary>
        /// 取得分行資料
        /// </summary>
        /// <param name="bankNo"></param>
        /// <returns></returns>
        public JsonResult GetBankBranchInfo(string bankNo)
        {
            var branchs = op.BankInfoGetBranchList(bankNo);
            List<AtmBankBranch> banks = new List<AtmBankBranch>();
            foreach (var branch in branchs)
            {
                banks.Add(new AtmBankBranch
                {
                    BranchNo = branch.BranchNo,
                    BranchName = branch.BranchName
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = banks
            });
        }

        /// <summary>
        /// 儲存ATM資料
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="buyerUserId"></param>
        /// <param name="atmForm"></param>
        /// <returns></returns>
        public JsonResult SaveAtmInfo(Guid orderGuid, int buyerUserId, CtAtmRefundModel atmForm)
        {
            if (buyerUserId == 0)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "買家UserId錯誤!"
                });
            }

            if (atmForm.BankNo == "000" || atmForm.BranchNo == "000")
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "請檢查銀行/分行!"
                });
            }

            string edit = UserRefundFacade.SetCtAtmRefundModel(atmForm, orderGuid, buyerUserId) ? "新增" : "修改";

            CommonFacade.AddAudit(orderGuid, AuditType.Order,
                edit + " ATM匯款帳戶：戶名:" + atmForm.AccountName + "，ID:" + atmForm.Id + "，" +
                atmForm.BankName + "(" + atmForm.BranchName + ")，帳戶:" + atmForm.AccountNumber + ")，手機:" +
                atmForm.Phone, User.Identity.Name, true);
            
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = edit + "成功!"
            });
        }

        /// <summary>
        /// 廠商處理進度-已處理
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ProcessedVendorUnreturnable(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            returnForm.VendorUnreturnableProcessed(User.Identity.Name);
            ReturnFormRepository.Save(returnForm);
            var msg = string.Format("退貨單{0} - 廠商無法退貨已處理", returnForm.Id);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, msg, User.Identity.Name, false);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "標註已處理失敗, 請洽技術部!"
            });
        }

        /// <summary>
        /// 廠商處理進度-復原回未處理狀態
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult RecoverVendorProgress(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);

            #region check
            
            if (returnForm == null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "無此退貨單!"
                });
            }
            if (!returnForm.ProgressStatus.Equals((int)ProgressStatus.Processing))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "狀態已為待處理!"
                });
            }
            if (returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndNoRetrieving &&
                returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndRetrievied &&
                returnForm.VendorProgressStatus != VendorProgressStatus.CompletedAndUnShip)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "只限處理 廠商退貨處理進度為退貨完成(不包含[系統自動退]及[退貨完成:客服已處理]，這兩種狀態不能使用復原功能) 之退貨單"
                });
            }

            #endregion

            bool isSuccess = returnForm.VendorReturnProcessRecover(User.Identity.Name);
            ReturnFormRepository.Save(returnForm);

            var msg = string.Format("退貨單{0} - 復原廠商狀態", returnForm.Id);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, msg, User.Identity.Name, false);

            if (isSuccess)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = msg
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "復原失敗, 請洽技術部!"
            });
        }

        /// <summary>
        /// 取消退貨
        /// </summary>
        /// <param name="refundId"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        public JsonResult CancelRefund(int refundId, Guid bid)
        {
            IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
            if (Helper.IsFlagSet(vpd.BusinessHourStatus, (int)BusinessHourStatus.GroupCoupon) &&
                Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, (int)GroupOrderStatus.FamiDeal) && vpd.ItemPrice > 0)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "全家寄杯無法取消退貨。"
                });
            }

            string msg;
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            if (ReturnService.Cancel(returnForm, User.Identity.Name))
            {
                msg = string.Format("取消退貨單({0})成功!", returnForm.Id);
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, msg, User.Identity.Name, true);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = msg
                });
            }

            msg = string.Format("取消退貨單({0})失敗", returnForm.Id);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, msg, User.Identity.Name, true);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = msg + "，請洽技術部!"
            });
        }

        /// <summary>
        /// 立即退貨
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ImmediateRefund(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);

            List<FamilyNetPincode> pincodes = fami.GetFamilyNetListPincode(returnForm.OrderGuid);
            if (pincodes.Any(x => 
                x.Status == (byte)FamilyNetPincodeStatus.Completed && 
                x.ReturnStatus != (int)FamilyNetPincodeReturnStatus.Completed && !x.IsVerified))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "Pincode尚未註銷完畢，無法立即退貨。"
                });
            }
            
            ReturnService.Refund(returnForm, User.Identity.Name, true);
            ReturnFormEntity form = ReturnFormRepository.FindById(refundId);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = OrderFacade.GetRefundStatus(form)
            });
        }

        /// <summary>
        /// 購物金轉現金
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ScashToCash(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);

            string cannotReason;
            if (returnForm.CanChangeToCashRefund(out cannotReason))
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("退貨單({0}) - 處理購物金轉現金", refundId), User.Identity.Name, false);
                ReturnService.RefundScashToCash(refundId, User.Identity.Name);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "處理完成!"
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = cannotReason
            });
        }

        /// <summary>
        /// 轉退購物金
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult CashToSCash(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);

            string cannotReason;
            if (returnForm.CanChangeToSCashRefund(out cannotReason))
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, string.Format("退貨單({0}) - 處理退現改退購物金", refundId), User.Identity.Name, false);
                ReturnService.RefundCashToSCash(refundId, User.Identity.Name);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "處理完成!"
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = cannotReason
            });
        }

        /// <summary>
        /// [回復]轉退購物金
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult RecoverToRefundScash(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            ReturnService.RecoverToRefundScash(returnForm, User.Identity.Name);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "處理完畢"
            });
        }

        /// <summary>
        /// 已更新資料
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult UpdateAtmRefund(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            if (ReturnService.AtmRefundAccountExist(returnForm.OrderGuid))
            {
                returnForm.ChangeProgressStatus(ProgressStatus.Processing, User.Identity.Name);
                ReturnFormRepository.Save(returnForm);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "處理完畢"
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "請先更新ATM退款的帳戶資訊。"
            });
        }

        /// <summary>
        /// 人工匯退
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ArtificialRefund(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            CtAtmRefund atmR = op.CtAtmRefundGetLatest(returnForm.OrderGuid);
            //Atm退款失敗 若標註人工匯退狀態
            //則視同人工退款完成 並標註flag為未完成
            //由隔天排程去更新退款狀態
            if (atmR.Status == (int)AtmRefundStatus.RefundFail)
            {
                atmR.Flag ^= (int)AtmRefundFlag.Checked;
                //恢復退貨單為Atm退款中狀態
                returnForm.ChangeProgressStatus(ProgressStatus.AtmQueueing, User.Identity.Name);
                ReturnFormRepository.Save(returnForm);
            }
            atmR.Status = (int)AtmRefundStatus.RefundSuccess;
            atmR.ProcessDate = DateTime.Now;
            op.CtAtmRefundSet(atmR);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, 
                string.Format("ATM退款申請序號({0}) - 因[人工匯退]處理，更新[ATM退款狀態]為[退貨完成]", atmR.Si), User.Identity.Name, false);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "處理完畢"
            });
        }
        
        /// <summary>
        /// 取消換貨
        /// </summary>
        /// <param name="exchangeId"></param>
        /// <returns></returns>
        public JsonResult CancelExchange(int exchangeId)
        {
            var exchangeLog = op.OrderReturnListGet(exchangeId);
            if (exchangeLog.Status == (int)OrderReturnStatus.ExchangeSuccess)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "已確認完成之換貨單不允許進行換貨取消作業"
                });
            }

            var modifyMessage = "17Life確認換貨取消";
            exchangeLog.Status = (int)OrderReturnStatus.ExchangeCancel;
            exchangeLog.MessageUpdate = false;
            exchangeLog.ModifyTime = DateTime.Now;
            exchangeLog.ModifyId = User.Identity.Name;

            if (OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "已取消換貨!"
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "確認換貨取消作業失敗, 請洽技術部!"
            });
        }

        /// <summary>
        /// 換貨完成結案
        /// </summary>
        /// <param name="exchangeId"></param>
        /// <returns></returns>
        public JsonResult CompleteExchange(int exchangeId)
        {
            var exchangeLog = op.OrderReturnListGet(exchangeId);

            if (exchangeLog.VendorProgressStatus != (int)ExchangeVendorProgressStatus.ExchangeCompleted)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "廠商已標註換貨完成之換貨單方可進行確認完成作業"
                });
            }

            var modifyMessage = "17Life確認換貨完成結案";
            exchangeLog.Status = (int)OrderReturnStatus.ExchangeSuccess;
            exchangeLog.MessageUpdate = false;
            exchangeLog.ModifyTime = DateTime.Now;
            exchangeLog.ModifyId = User.Identity.Name;

            if (OrderFacade.SetOrderReturList(exchangeLog, modifyMessage))
            {
                OrderFacade.SendCustomerExchangeCompleteMail(exchangeId);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "已完成結案!"
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "確認換貨完成作業失敗, 請洽技術部!"
            });
        }

        /// <summary>
        /// 改用電子折讓
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ChangeToElcAllowance(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            try
            {
                if (returnForm.ProgressStatus == ProgressStatus.Processing)
                {
                    var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                                      .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                                      .OrderByDescending(x => x.InvoiceNumberTime)
                                      .ToList();

                    //宅配
                    if (returnForm.DeliveryType == DeliveryType.ToHouse)
                    {
                        var einv = eInvoices.FirstOrDefault();

                        if (einv != null)
                        {
                            if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                                einv.AllowanceStatus == (int)AllowanceStatus.PaperAllowance && 
                                einv.OrderTime > config.EinvAllowanceStartDate)
                            {
                                einv.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                                EinvoiceFacade.SetEinvoiceMainWithLog(einv, User.Identity.Name);
                                CommonFacade.AddAudit(returnForm.OrderGuid, 
                                    AuditType.Refund, string.Format("發票號碼:{0}，紙本折讓改電子折讓", einv.InvoiceNumber), User.Identity.Name, true);
                            }
                        }
                    }
                    else
                    {
                        var couponIds = returnForm.GetReturnCouponIds();
                        foreach (var couponId in couponIds)
                        {
                            var emc = eInvoices.Where(x => x.CouponId == couponId).ToList();
                            //成套禮券發票無couponId
                            emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                            foreach (var einv in emc)
                            {
                                if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                                    einv.AllowanceStatus == (int)AllowanceStatus.PaperAllowance && 
                                    einv.OrderTime > config.EinvAllowanceStartDate)
                                {
                                    einv.AllowanceStatus = (int)AllowanceStatus.ElecAllowance;
                                    EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                                        string.Format("發票號碼:{0}，紙本折讓改電子折讓", einv.InvoiceNumber), User.Identity.Name, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                    string.Format("退貨單號:{0}，紙本折讓改電子折讓失敗{1}", refundId, ex), User.Identity.Name, true);
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "處理完畢!"
            });
        }

        /// <summary>
        /// 改用紙本折讓
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ChangeToPaperAllowance(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            try
            {
                if (returnForm.ProgressStatus == ProgressStatus.Processing)
                {
                    var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                                      .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                                      .OrderByDescending(x => x.InvoiceNumberTime)
                                      .ToList();

                    if (returnForm.DeliveryType == DeliveryType.ToHouse)
                    {
                        var einv = eInvoices.FirstOrDefault();

                        if (einv != null)
                        {
                            if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                                einv.AllowanceStatus == (int)AllowanceStatus.ElecAllowance)
                            {
                                einv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                                EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                                    string.Format("發票號碼:{0}，電子折讓改紙本折讓", einv.InvoiceNumber), User.Identity.Name, true);
                            }
                        }
                    }
                    else
                    {
                        var couponIds = returnForm.GetReturnCouponIds();
                        foreach (var couponId in couponIds)
                        {
                            var emc = eInvoices.Where(x => x.CouponId == couponId).ToList();
                            //成套禮券發票無couponId
                            emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                            foreach (var einv in emc)
                            {
                                if (einv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                                    einv.AllowanceStatus == (int)AllowanceStatus.ElecAllowance)
                                {
                                    einv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                                    EinvoiceFacade.SetEinvoiceMainWithLog(einv);
                                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                                        string.Format("發票號碼:{0}，電子折讓改紙本折讓", einv.InvoiceNumber), User.Identity.Name, true);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                    string.Format("退貨單號:{0}，電子折讓改紙本折讓失敗{1}", refundId, ex), User.Identity.Name, true);
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "處理完畢!"
            });
        }

        /// <summary>
        /// 折讓單已回
        /// </summary>
        /// <param name="refundId"></param>
        /// <returns></returns>
        public JsonResult ReceivedCreditNote(int refundId)
        {
            ReturnFormEntity returnForm = ReturnFormRepository.FindById(refundId);
            returnForm.ReceivedCreditNote(User.Identity.Name);
            ReturnFormRepository.Save(returnForm);
            CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Order, 
                string.Format("退貨單-{0} 折讓單已收回", returnForm.Id), User.Identity.Name, true);
            
            var eInvoices = op.EinvoiceMainGetListByOrderGuid(returnForm.OrderGuid)
                              .Where(x => x.InvoiceStatus != (int)EinvoiceType.C0701)
                              .OrderByDescending(x => x.InvoiceNumberTime)
                              .ToList();

            if (returnForm.DeliveryType == DeliveryType.ToHouse)
            {
                var einv = eInvoices.FirstOrDefault(x => x.AllowanceStatus == (int)AllowanceStatus.PaperAllowance);

                if (einv != null)
                {
                    einv.InvoiceMailbackAllowance = true;
                    EinvoiceFacade.SetEinvoiceMainWithLog(einv, User.Identity.Name);
                    CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                        string.Format("發票號碼:{0}，折讓單已收回", einv.InvoiceNumber), User.Identity.Name, true);
                }
            }
            else
            {
                var couponIds = returnForm.GetReturnCouponIds();
                foreach (var couponId in couponIds)
                {
                    var emc = eInvoices.Where(x => x.CouponId == couponId && x.AllowanceStatus == (int)AllowanceStatus.PaperAllowance).ToList();
                    //成套禮券發票無couponId
                    emc.AddRange(ReturnService.GroupCouponOrder(eInvoices));

                    foreach (var einv in emc)
                    {
                        einv.InvoiceMailbackAllowance = true;
                        EinvoiceFacade.SetEinvoiceMainWithLog(einv, User.Identity.Name);
                        CommonFacade.AddAudit(returnForm.OrderGuid, AuditType.Refund, 
                            string.Format("發票號碼:{0}，折讓單已收回", einv.InvoiceNumber), User.Identity.Name, true);
                    }
                }
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "處理完畢!"
            });
        }

        #endregion

        #region PART 3. 出貨管理 超取歷程

        /// <summary>
        /// 新增出貨資訊
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public JsonResult CreateOrderShip(CreateOrderShipInputModel input)
        {
            #region check

            //檢查該訂單是否有可出貨商品
            bool isShipable = PponOrderManager.CheckIsShipable(input.OrderGuid);

            if (!isShipable)
            {
                return Json(new ApiResult { Code = ApiResultCode.Error, Message = "已退貨的訂單無法新增出貨資訊！" });
            }

            int shippingMemoLimit = 30;
            if (input.ShippingMemo.Length > shippingMemoLimit)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = string.Format("出貨備註只限最多 {0} 個字。", shippingMemoLimit)
                });
            }

            if (OrderShipUtility.CheckShipDataExist(input.OrderGuid, OrderClassification.LkSite))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "出貨資訊已存在，請勿重複點選新增！"
                });
            }

            if (OrderShipUtility.CheckDealShipNoExist(input.Bid, OrderClassification.LkSite, input.OrderGuid, input.TrackingNumber))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "運單編號重複，請選擇別的單號！"
                });
            }

            if (OrderShipUtility.CheckShipDataExist(input.OrderGuid, OrderClassification.LkSite))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "出貨資訊已存在，請勿重複點選新增！"
                });
            }

            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(input.Bid);
            if (!(deal.BusinessHourDeliverTimeS != null && input.DispatchDate >= deal.BusinessHourDeliverTimeS))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "您所填的出貨日期必須大於預計出貨日期！"
                });
            }

            #endregion

            OrderShip os = new OrderShip
            {
                OrderClassification = (int)OrderClassification.LkSite,
                OrderGuid = input.OrderGuid,
                ShipCompanyId = input.LogisticsCorpId,
                ShipTime = input.DispatchDate,
                ShipNo = input.TrackingNumber,
                ShipMemo = input.ShippingMemo,
                Type = (int)OrderShipType.Normal,
                CreateId = User.Identity.Name,
                CreateTime = DateTime.Now,
                ModifyId = User.Identity.Name,
                ModifyTime = DateTime.Now
            };

            var orderShipId = op.OrderShipSet(os);

            if (orderShipId > 0)
            {
                //宅配已配送出貨要發推播給使用者(僅只有新增的第一次需要push)
                var order = op.OrderGet(os.OrderGuid);
                var msg = string.Format("17Life商品(訂單：{0})已出貨囉，查看詳情", order.OrderId);
                NotificationFacade.PushMemberOrderMessageOnWorkTime(order.UserId, msg, os.OrderGuid);
                //log.Info(string.Format("宅配已出貨 OrderDetail.OnAddOrderShip: UserId={0}, pmsg={1}, Guid={2}", order.UserId, msg, order.OrderId));
            }

            #region 資策會訂單紀錄
            try
            {
                //同部異動資策會訂單，標註要更新到資策會那邊
                op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
            }
            catch (Exception ex)
            {
                log.Error("異動companyUserOrder狀態時發生錯誤", ex);
            }

            #endregion 資策會訂單紀錄

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = string.Format("({0})出貨單建立成功!", os.ShipNo)
            });
        }

        /// <summary>
        /// 刪除出貨資訊
        /// </summary>
        /// <param name="orderShipId"></param>
        /// <returns></returns>
        public JsonResult DeleteOrderShip(int orderShipId)
        {
            OrderShip os = op.GetOrderShipById(orderShipId);

            bool checkIsShipable = PponOrderManager.CheckIsShipable(os.OrderGuid);
            bool checkIsExchanging = PponOrderManager.CheckEverExchange(os.OrderGuid);

            if (!checkIsShipable && checkIsExchanging)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "已退貨或換貨處理中的訂單無法刪除出貨資訊!"
                });
            }

            if (!op.OrderShipDelete(orderShipId))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "刪除失敗!"
                });
            }

            OrderShipLog osl = new OrderShipLog
            {
                OrderShipId = os.Id,
                ShipCompanyId = null,
                ShipTime = null,
                ShipNo = null,
                ShipMemo = null,
                CreateId = User.Identity.Name,
                CreateTime = DateTime.Now
            };
            op.OrderShipLogSet(osl);

            #region 資策會訂單紀錄
            try
            {
                //同部異動資策會訂單，標註要更新到資策會那邊
                op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
            }
            catch (Exception ex)
            {
                log.Error("異動companyUserOrder狀態時發生錯誤", ex);
            }
            #endregion 資策會訂單紀錄

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "刪除成功!"
            });
        }

        /// <summary>
        /// 儲存編輯出貨資訊
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public JsonResult UpdateOrderShip(UpdateOrderShipInputModel input)
        {
            #region check

            int shippingMemoLimit = 30;
            if (input.ShippingMemo.Length > shippingMemoLimit)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = string.Format("出貨備註只限最多 {0} 個字。", shippingMemoLimit)
                });
            }

            //檢查出貨日期是否大於等於 "預計出貨日期"
            ViewPponDeal deal = pp.ViewPponDealGetByBusinessHourGuid(input.Bid);
            if (!(deal.BusinessHourDeliverTimeS != null && input.DispatchDate >= deal.BusinessHourDeliverTimeS))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "修改失敗! 您所填的出貨日期必須大於預計出貨日期!"
                });
            }

            //檢查運單編號是否重複
            if (OrderShipUtility.CheckDealShipNoExist(input.Bid, OrderClassification.LkSite, input.OrderGuid, input.TrackingNumber))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "修改失敗! 運單編號重複, 請選擇別的單號!"
                });
            }

            #endregion

            OrderShip os = op.GetOrderShipById(input.OrderShipId);
            os.ShipCompanyId = input.LogisticsCorpId;
            os.ShipTime = input.DispatchDate;
            os.ShipNo = input.TrackingNumber;
            os.ShipMemo = input.ShippingMemo;
            os.ModifyId = User.Identity.Name;
            os.ModifyTime = DateTime.Now;

            if (!op.OrderShipUpdate(os))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "修改失敗!"
                });
            }

            OrderShipLog osl = new OrderShipLog
            {
                OrderShipId = os.Id,
                ShipCompanyId = os.ShipCompanyId,
                ShipTime = os.ShipTime,
                ShipNo = os.ShipNo,
                ShipMemo = os.ShipMemo,
                CreateId = os.ModifyId,
                CreateTime = os.ModifyTime
            };
            op.OrderShipLogSet(osl);

            #region 資策會訂單紀錄
            try
            {
                //同部異動資策會訂單，標註要更新到資策會那邊
                op.CompanyUserOrderUpdateWaitUpdate(os.OrderGuid, true);
            }
            catch (Exception ex)
            {
                log.Error("異動companyUserOrder狀態時發生錯誤", ex);
            }
            #endregion 資策會訂單紀錄

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "修改成功!"
            });
        }

        /// <summary>
        /// 更新退換貨收件人
        /// </summary>
        /// <param name="id"></param>
        /// <param name="receiverName"></param>
        /// <param name="receiverAddress"></param>
        /// <param name="isExchange"></param>
        /// <returns></returns>
        public JsonResult UpdateReceiver(int id, string receiverName, string receiverAddress, bool isExchange)
        {
            if (isExchange)
            {
                //換貨
                OrderReturnList exchangeLog = op.OrderReturnListGet(id);
                if (exchangeLog.IsLoaded)
                {
                    string changelog = string.Empty;
                    if (exchangeLog.ReceiverName != receiverName)
                    {
                        changelog = string.Format("收件人: \"{0}\" → \"{1}\"", exchangeLog.ReceiverName ?? "null", receiverName);
                    }
                    if (exchangeLog.ReceiverAddress != receiverAddress)
                    {
                        changelog += string.IsNullOrEmpty(changelog) ? ", " : "";
                        changelog += string.Format("地址: \"{0}\" → \"{1}\"", exchangeLog.ReceiverAddress ?? "null", receiverAddress);
                    }

                    exchangeLog.ReceiverName = receiverName;
                    exchangeLog.ReceiverAddress = receiverAddress;
                    exchangeLog.ModifyTime = DateTime.Now;
                    exchangeLog.ModifyId = User.Identity.Name;
                    op.OrderReturnListSet(exchangeLog);

                    exchangeLog.Message = string.Format("換貨單({0})更新: {1}", exchangeLog.Id, changelog);
                    PaymentFacade.SaveOrderStatusLog(exchangeLog);

                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Success
                    });
                }
                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "查無換貨單!"
                });
            }
            else
            {
                //退貨
                ReturnForm rf = op.ReturnFormGet(id);
                if (rf.IsLoaded)
                {
                    string changelog = string.Empty;
                    if (rf.ReceiverName != receiverName)
                    {
                        changelog = string.Format("收件人: \"{0}\" → \"{1}\"", rf.ReceiverName ?? "null", receiverName);
                    }
                    if (rf.ReceiverAddress != receiverAddress)
                    {
                        changelog += string.IsNullOrEmpty(changelog) ? ", " : "";
                        changelog += string.Format("地址: \"{0}\" → \"{1}\"", rf.ReceiverAddress ?? "null", receiverAddress);
                    }

                    rf.ReceiverName = receiverName;
                    rf.ReceiverAddress = receiverAddress;
                    rf.ModifyTime = DateTime.Now;
                    rf.ModifyId = User.Identity.Name;
                    op.ReturnFormSet(rf);
                    
                    CommonFacade.AddAudit(rf.OrderGuid, AuditType.Refund, string.Format("退貨單({0})更新: {1}", id, changelog), User.Identity.Name, true);

                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Success
                    });
                }

                return Json(new ApiResult
                {
                    Code = ApiResultCode.InputError,
                    Message = "查無退貨單!"
                });
            }
        }

        /// <summary>
        /// 超取歷程
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public string GetIspHistoryList(Guid orderGuid)
        {
            List<IspHistory> ispHistory = new List<IspHistory>();
            var sellerHistory = ISPFacade.GetIspOrderHistory(orderGuid).OrderBy(p => p.SerialNo).ToList();
            foreach (var sh in sellerHistory)
            {
                if (sh.HistoryType != (int)IspHistoryType.Seller) continue;
                ispHistory.Add(new IspHistory
                {
                    DeliveryTime = sh.DeliveryTime.ToString("yyyy-MM-dd HH:mm"),
                    DelvieryStatus = sh.DelvieryStatus,
                    DelvieryContent = sh.DelvieryContent,
                });
            }

            return new JsonSerializer().Serialize(ispHistory);
        }

        #endregion

        #region PART 4. 發票管理

        /// <summary>
        /// 儲存修改發票
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public JsonResult SaveEinvoice(EditInvoiceModel input)
        {
            #region Check

            UpdatableOrderInfo oi = new UpdatableOrderInfo();

            //處理三聯
            if (input.InvoiceMode == (int)InvoiceMode2.Triplicate) 
            {
                if (!string.IsNullOrEmpty(RegExRules.CompanyNoCheck(input.CompanyId)) || string.IsNullOrEmpty(input.InvoiceTitle))
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "若欲變更為三聯式發票，請填寫正確的統編與抬頭，謝謝！"
                    });
                }
                oi.IsUpdateInvoice = true;
                oi.InvoiceType = ((int)InvoiceMode2.Triplicate).ToString();
                oi.CarrierType = CarrierType.None; //三聯僅提供紙本
                oi.DrType = (int)DonationReceiptsType.DoNotContribute; //三聯不可捐贈
                oi.UniCode = input.CompanyId;
                oi.Title = input.InvoiceTitle;
            }
            //處理二聯
            else if (input.InvoiceMode == (int)InvoiceMode2.Duplicate) 
            {
                oi.IsUpdateInvoice = true;
                oi.InvoiceType = ((int)InvoiceMode2.Duplicate).ToString();
                oi.CarrierType = (CarrierType)input.CarrierType;
                oi.DrType = (int)DonationReceiptsType.DoNotContribute; //預設不捐贈
                oi.UniCode = string.Empty; //二聯不填統編
                oi.Title = string.Empty; //二聯不填抬頭
            }

            EinvoiceMain em = op.EinvoiceMainGetById(input.Id);

            //處理新版發票載具
            if (em.Version == (int)InvoiceVersion.CarrierEra)
            {
                oi.CarrierType = (CarrierType)input.CarrierType;
                switch (input.CarrierType)
                {
                    case (int)CarrierType.None:
                        oi.CarrierId = null;
                        break;
                    case (int)CarrierType.Member:
                        oi.CarrierId = em.UserId.ToString();
                        break;
                    case (int)CarrierType.Phone:
                    case (int)CarrierType.PersonalCertificate:
                        oi.CarrierId = input.CarrierId.ToUpper();
                        break;
                }
            }

            //發票相關退貨單據是否己寄回
            var receipt = op.EntrustSellReceiptGetByOrder(input.OrderGuid);
            oi.ReceiptMailbackPaper = receipt.MailbackPaper; //退貨申請書已寄回
            oi.ReceiptMailbackAllowance = receipt.MailbackAllowance; //折讓單已寄回
            oi.ReceiptMailbackReceipt = receipt.MailbackReceipt; //代收轉付收據已寄回

            #endregion

            #region 修改發票

            DateTime now = DateTime.Now;
            List<string> multiMessage = new List<string>();
            IList<ReturnFormEntity> returnForms = ReturnFormRepository.FindAllByOrder(input.OrderGuid);
            EinvoiceMainCollection einvCol = op.EinvoiceMainCollectionGet(EinvoiceMain.Columns.OrderGuid, input.OrderGuid.ToString(), true);
            foreach (EinvoiceMain inv in einvCol)
            {
                bool isVaildEinvoice = inv.InvoiceStatus != (int)EinvoiceType.C0501 && inv.InvoiceStatus != (int)EinvoiceType.D0401; //是否未作廢
                bool invoiceFrom3To2 = inv.IsEinvoice3 && input.InvoiceMode == (int)InvoiceMode2.Duplicate; //是否三聯轉二聯
                bool invoiceFrom2To3 = inv.IsEinvoice2 && input.InvoiceMode == (int)InvoiceMode2.Triplicate;//是否二聯轉三聯
                
                //目前走作廢重開立，之後補上可轉註銷:未印製、取消原本紙本申請時間(註銷上傳後重壓)
                bool invoiceChangeComInfo = (inv.InvoiceComName != oi.Title || inv.InvoiceComId != oi.UniCode) && 
                        !invoiceFrom2To3 && 
                        inv.InvoiceStatus == (int)EinvoiceType.C0401;

                #region Check 退貨狀態、是否重開期間、是否康迅發票

                //確認退貨
                ReturnFormEntity form = returnForms
                    .Where(x => x.ProgressStatus.EqualsAny(ProgressStatus.Processing, ProgressStatus.AtmQueueing, ProgressStatus.AtmQueueSucceeded))
                    .OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                if (form == null)
                {
                    form = returnForms.OrderByDescending(x => x.VendorProcessTime).FirstOrDefault();
                }
                if (form != null && form.ProgressStatus != ProgressStatus.Canceled && form.ProgressStatus != ProgressStatus.Completed && invoiceFrom2To3)
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "2聯轉3聯需待退貨處理結束才可進行!"
                    });
                }

                if (invoiceFrom2To3 &&
                    !string.IsNullOrEmpty(inv.InvoiceNumber) &&
                    Convert.ToInt32(inv.InvoiceStatus) == (int)EinvoiceType.Initial)
                {
                    //status = C0701仍允許做2轉3
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "重開期間不可進行2聯轉3聯"
                    });
                }

                if ((invoiceFrom2To3 || invoiceChangeComInfo) &&
                    inv.InvoiceNumberTime < config.PayeasyToContact &&
                    inv.InvoiceStatus == (int)EinvoiceType.C0401)
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "康迅發票不可進行2聯轉3聯"
                    });
                }

                #endregion

                //是否變更載具
                bool invoiceChangeCarrierType =
                    //載具變更僅有2聯,會員載具有時不會存入CarrierId 故不視為變更
                    (inv.CarrierType != (int)oi.CarrierType || (inv.CarrierId != oi.CarrierId && inv.CarrierType != (int)CarrierType.Member)) && 
                    inv.InvoiceMode2 == (int)InvoiceMode2.Duplicate && 
                    !invoiceFrom3To2 && 
                    inv.InvoiceStatus == (int)EinvoiceType.C0401 && 
                    !string.IsNullOrEmpty(inv.InvoiceNumber); 

                EinvoiceSerial einvocieSerial = inv.InvoiceNumberTime != null 
                    ? op.EinvoiceSerialCollectionGetByInvInfo(inv.InvoiceNumber, inv.InvoiceNumberTime.Value) 
                    : null;
                bool invoiceIntertemporal = inv.IsIntertemporal;
                string changeCarrierMsg = invoiceChangeCarrierType 
                    ? inv.CarrierType != (int)oi.CarrierType
                        ? (CarrierType)inv.CarrierType + "->" + oi.CarrierType
                        : inv.CarrierId + "->" + oi.CarrierId 
                    : string.Empty;

                #region 折讓/作廢
                
                if (!string.IsNullOrEmpty(inv.InvoiceNumber) && isVaildEinvoice && inv.VerifiedTime >= config.PayeasyToContact && (
                    (inv.InvoiceMode2 != (int)InvoiceMode2.Triplicate && invoiceFrom2To3 && 
                    (!invoiceIntertemporal || (invoiceIntertemporal && einvocieSerial != null && now < einvocieSerial.EndDate.AddDays(config.InvoiceBufferDays)))) || 
                    (inv.InvoiceMode2 == (int)InvoiceMode2.Triplicate && !invoiceFrom2To3 && invoiceChangeComInfo)))
                {
                    //已開立則作廢重開
                    //新開立一筆main
                    EinvoiceMain newInv = new EinvoiceMain();
                    newInv = inv.Clone();
                    newInv.Id = 0;
                    newInv.InvoiceNumber = null;
                    newInv.InvoiceNumberTime = null;
                    newInv.VerifiedTime = newInv.InvoiceRequestTime = now;
                    newInv.InvoiceComId = oi.UniCode;
                    newInv.InvoiceComName = oi.Title;
                    newInv.InvoiceSerialId = 0;
                    newInv.InvoiceFileSerial = 0;
                    newInv.InvoiceBuyerName = oi.EinvoiceName;
                    newInv.InvoiceBuyerAddress = oi.EinvoiceAddress;
                    newInv.Message = invoiceChangeComInfo ? "變更抬頭重開" : "二聯改三聯";
                    newInv.InvoiceMode = 0; //not use
                    newInv.InvoiceMode2 = (int)InvoiceMode2.Triplicate;
                    newInv.CarrierType = (int)CarrierType.None;
                    newInv.CarrierId = string.Empty;
                    newInv.InvoicePapered = false;
                    newInv.InvoicePaperedTime = null;
                    newInv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                    newInv.LoveCode = string.Empty;
                    newInv.InvoiceVoidMsg = string.Empty;
                    newInv.InvoiceVoidTime = null;

                    EinvoiceFacade.SetEinvoiceMainWithLog(newInv);

                    //原則上二聯轉三聯作廢僅限當期
                    int einvType = !invoiceIntertemporal || (invoiceIntertemporal && now < einvocieSerial.EndDate.AddDays(config.InvoiceBufferDays))
                        ? (int)EinvoiceType.C0501 : (int)EinvoiceType.D0401; 

                    //detail 作廢方式 -> 開立一筆InvType為作廢的Detail 開立發票號碼時會再創建一筆新的mainId的Detail
                    EinvoiceFacade.GenerateCancelFiles(inv.Id, einvType, User.Identity.Name); //含作廢Main

                    string msg = string.Format("{0}發票:{1}", einvType == (int)EinvoiceType.D0401 ? "折讓" : "作廢", inv.InvoiceNumber);
                    multiMessage.Add(msg);
                    CommonFacade.AddAudit(input.OrderGuid, AuditType.Order, msg, User.Identity.Name, true);

                    continue;
                }

                #endregion

                #region 變更載具，註銷重開
                
                if (isVaildEinvoice)
                {
                    inv.InvoiceComId = oi.UniCode;
                    inv.InvoiceComName = oi.Title;
                    inv.InvoiceBuyerName = oi.EinvoiceName;
                    inv.InvoiceBuyerAddress = oi.EinvoiceAddress;
                    inv.InvoiceMode = 0; //the field will be delete

                    if (invoiceFrom2To3)
                    {
                        if (inv.VerifiedTime == null)
                        {
                            inv.VerifiedTime = now;
                        }
                        inv.InvoiceRequestTime = now;
                        inv.AllowanceStatus = (int)AllowanceStatus.PaperAllowance;
                    }

                    if (inv.Version == (int)InvoiceVersion.CarrierEra)
                    {
                        inv.InvoiceMode2 = Convert.ToInt32(oi.InvoiceType);
                        if (invoiceFrom2To3 || invoiceFrom3To2)
                        {
                            inv.CarrierType = (int)CarrierType.None;
                            inv.CarrierId = null;
                        }
                        else
                        {
                            inv.CarrierType = (int)oi.CarrierType;
                            inv.CarrierId = oi.CarrierId;
                        }
                    }
                    if (invoiceChangeCarrierType && !invoiceIntertemporal && inv.InvoiceRequestTime == null)
                    {
                        inv.InvoiceStatus = (int)EinvoiceType.C0701;
                        inv.InvoiceVoidTime = now;
                        inv.InvoiceVoidMsg = "變更載具，註銷重開";
                        multiMessage.Add(inv.InvoiceVoidMsg);
                        CommonFacade.AddAudit(input.OrderGuid, AuditType.Order, 
                            string.Format("註銷發票:{0}，原因:變更載具{1}", inv.InvoiceNumber, changeCarrierMsg), User.Identity.Name, true);
                    }
                }

                #endregion
                
                #region 註銷
                
                if (invoiceIntertemporal && invoiceChangeCarrierType)
                {
                    //會有多張不同期發票
                    multiMessage.Add(string.Format("發票:{0}已跨期，無法再進行註銷作業", inv.InvoiceNumber));
                    CommonFacade.AddAudit(input.OrderGuid, AuditType.Order, 
                        string.Format("載具已變更:" + changeCarrierMsg + "， 但發票:{0}未註銷，因已跨期", inv.InvoiceNumber), User.Identity.Name, true);
                    continue;
                }

                if (invoiceChangeCarrierType && inv.InvoiceRequestTime != null && !invoiceFrom2To3)
                {
                    multiMessage.Add(string.Format("發票:{0}已申請紙本發票，無法再進行註銷作業", inv.InvoiceNumber));
                    CommonFacade.AddAudit(input.OrderGuid, AuditType.Order, 
                        string.Format("載具已變更:" + changeCarrierMsg + "，但發票:{0}未註銷，因已申請紙本發票", inv.InvoiceNumber), User.Identity.Name, true);
                    continue;
                }

                #endregion
            }

            #endregion

            EinvoiceFacade.SetEinvoiceMainCollectionWithLog(einvCol, User.Identity.Name, now);
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = multiMessage
            });
        }

        /// <summary>
        /// 寄出折讓單
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="buyerEmail"></param>
        /// <returns></returns>
        public JsonResult SendReturnDiscount(Guid orderGuid, string buyerEmail)
        {
            if (!RegExRules.CheckEmail(buyerEmail))
            {
                var msg = "退回或折讓證明單未寄出! Email格式有誤! " + buyerEmail;
                CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, User.Identity.Name, true);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = msg
                });
            }

            try
            {
                var msg = "退回或折讓證明單已寄出 - " + buyerEmail;
                OrderFacade.SendPponMail(MailContentType.ReturnDiscountResend, orderGuid, true, null, string.Empty, buyerEmail);
                CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, User.Identity.Name, true);
                
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = msg
                });
            }
            catch (Exception)
            {
                var msg = "退回或折讓證明單未寄出! - " + buyerEmail;
                CommonFacade.AddAudit(orderGuid, AuditType.Order, msg, User.Identity.Name, true);
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = msg
                });
            }
        }

        /// <summary>
        /// 申請紙本發票
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="invoiceNo"></param>
        /// <returns></returns>
        public JsonResult PrintInvoicePaper(string orderGuid, string invoiceNo)
        {
            EinvoiceMain einv = op.EinvoiceMainGet(invoiceNo);
            if (string.IsNullOrEmpty(einv.InvoiceBuyerName) || string.IsNullOrEmpty(einv.InvoiceBuyerAddress))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "收件人或地址不可為空白!"
                });
            }

            if (einv.InvoiceRequestTime == null)
            {
                einv.InvoiceRequestTime = DateTime.Now;
                //更新原有的發票數據
                EinvoiceFacade.SetEinvoiceMainWithLog(einv, User.Identity.Name, einv.InvoiceRequestTime);
                CommonFacade.AddAudit(orderGuid, AuditType.Order, "發票號碼: " + invoiceNo + " 申請紙本發票", einv.InvoiceBuyerName, true);
            }
        
            if (einv.InvoiceRequestTime != null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = string.Format("{0} 已申請紙本", ((DateTime)einv.InvoiceRequestTime).ToString("yyyy/MM/dd HH:mm")),
                    Message = "申請紙本成功!"
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "申請紙本失敗!"
            });
        }

        #endregion

        #region PART 5. 客服案件

        /// <summary>
        /// 加入處理人員 2
        /// </summary>
        /// <param name="serviceNo"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public JsonResult JoinWorkerTwo(string serviceNo, int userId)
        {
            var csm = csp.CustomerServiceMessageGet(serviceNo);
            csm.CoWorkUser = userId;
            csp.CustomerServiceMessageSet(csm);

            CustomerServiceInsideLog csi = new CustomerServiceInsideLog();
            csi.Content = "";
            csi.ServiceNo = serviceNo;
            csi.Status = (int)statusConvert.secServiceNotify;
            csi.CreateTime = DateTime.Now;
            csi.CreateId = userId;
            csp.CustomerServiceInsideLogSet(csi);
            EmailFacade.SendNotifyToCs(serviceNo, statusConvert.secServiceNotify);
            
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "邀請信件已寄出!"
            });
        }

        /// <summary>
        /// 查詢案件資料
        /// </summary>
        /// <param name="serviceNo"></param>
        /// <returns></returns>
        public JsonResult GetServiceCase(string serviceNo)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);

            var caseData = CustomerServiceFacade.GetCaseDataByServiceNo(serviceNo, userId);
            if (caseData != null)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = caseData
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.DataNotFound,
                Message = "查無案件!"
            });
        }

        /// <summary>
        /// 認領客服案件
        /// </summary>
        /// <param name="serviceNo"></param>
        /// <returns></returns>
        public JsonResult ClaimCustomerServiceCase(string serviceNo)
        {
            var csm = csp.CustomerServiceMessageGet(serviceNo);
            if (csm.IsLoaded)
            {
                if (csm.WorkUser != null)
                {
                    return Json(new ApiResult
                    {
                        Code = ApiResultCode.Error,
                        Message = "重覆認領!"
                    });
                }
                var userId = MemberFacade.GetUniqueId(User.Identity.Name);
                csm.Status = (int)statusConvert.process;
                csm.ModifyId = userId;
                csm.ModifyTime = DateTime.Now;
                csm.WorkUser = userId;
                csp.CustomerServiceMessageSet(csm);

                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Message = "認領成功!"
                });
            }
            return Json(new ApiResult
            {
                Code = ApiResultCode.Error,
                Message = "錯誤案件!"
            });
        }

        /// <summary>
        /// 通知我
        /// </summary>
        /// <param name="serviceNo"></param>
        /// <param name="statusConvert"></param>
        /// <returns></returns>
        public JsonResult NotifyWorker(string serviceNo, int statusConvert)
        {
            CustomerServiceInsideLog csi = new CustomerServiceInsideLog();
            csi.Content = "";
            csi.ServiceNo = serviceNo;
            csi.Status = statusConvert;
            csi.CreateTime = DateTime.Now;
            csi.CreateId = MemberFacade.GetUniqueId(User.Identity.Name);
            csp.CustomerServiceInsideLogSet(csi);
            EmailFacade.SendNotifyToCs(serviceNo, (statusConvert)statusConvert);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "邀請信件已寄出!"
            });
        }

        /// <summary>
        /// 客服回覆訊息
        /// </summary>
        /// <returns></returns>
        public JsonResult SaveInsideMessage()
        {
            #region check
            
            if (string.IsNullOrEmpty(Request.Form["priority"]))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "問題等級錯誤!"
                });
            }
            if (string.IsNullOrEmpty(Request.Form["category"]))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "問題分類錯誤!"
                });
            }
            if (string.IsNullOrEmpty(Request.Form["subCategory"]))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "問題主因錯誤!"
                });
            }
            if (string.IsNullOrEmpty(Request.Form["status"]))
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message = "案件狀態錯誤!"
                });
            }

            #endregion

            string serviceNo = Request.Form["serviceNo"];
            string content = Request.Form["content"];
            int priority = int.Parse(Request.Form["priority"]);
            int category = int.Parse(Request.Form["category"]);
            int subCategory = int.Parse(Request.Form["subCategory"]);
            int status = int.Parse(Request.Form["status"]);
            HttpPostedFileBase images = Request.Files["images"];

            int beforeStatus = 0;
            int userId = MemberFacade.GetUniqueId(User.Identity.Name);

            var entity = csp.CustomerServiceMessageGet(serviceNo);
            if (entity.IsLoaded)
            {
                beforeStatus = entity.Status;
                entity.Priority = priority;
                entity.Category = category;
                entity.SubCategory = subCategory;
                entity.Status = status;
                entity.ModifyId = userId;
                entity.ModifyTime = DateTime.Now;
                csp.CustomerServiceMessageSet(entity);
            }

            bool hasFile = images != null;
            if (!string.IsNullOrEmpty(content) || hasFile)
            {
                //新增資料
                CustomerServiceInsideLog csi = new CustomerServiceInsideLog();
                csi.Content = content;
                csi.ServiceNo = serviceNo;
                csi.Status = status;
                csi.CreateTime = DateTime.Now;
                csi.CreateId = userId;

                #region 上傳圖片
                
                if (hasFile)
                {
                    csi.File = serviceNo + "_" + DateTime.Now.ToString("MMddHHmmss") + Path.GetExtension(images.FileName);
                    string path = Server.MapPath(config.InSideUrl);
                    if (Directory.Exists(path) == false)
                    {
                        Directory.CreateDirectory(path);
                    }
                    HttpPostedFileBase hpfb = images;
                    ImageUtility.UploadFileWithResizeForFileBase(hpfb, path, serviceNo, csi.File, 640);
                }

                #endregion

                int csiId = csp.CustomerServiceInsideLogSet(csi);
                if (beforeStatus == (int)statusConvert.transfer)
                {
                    EmailFacade.SendTransferMailToSeller(csiId);
                }
            }
            
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success
            });
        }

        /// <summary>
        /// 取得客服案件問題分類
        /// </summary>
        /// <returns></returns>
        public JsonResult GetCsCategory()
        {
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = csp.GetCustomerServiceCategory().OrderBy(x => x.CategoryId)
                    .Select(x => new KeyValuePair<int, string>(x.CategoryId, x.CategoryName)).ToList()
            });
        }

        /// <summary>
        /// 取得客服案件問題主因
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public JsonResult GetCsSubcategory(int categoryId)
        {
            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Data = csp.GetCustomerServiceCategoryListByCategoryId(categoryId).OrderBy(x=>x.CategoryId)
                    .Select(x => new KeyValuePair<int, string>(x.CategoryId, x.CategoryName)).ToList()
            });
        }

        /// <summary>
        /// 以問題分類編號取得罐頭訊息
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        public JsonResult GetCsSampleMessage(int categoryId)
        {
            MainModel main = CustomerServiceFacade.GetSample(categoryId);
            if (main != null && main.sampleList.Any())
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Success,
                    Data = main.sampleList
                });
            }

            return Json(new ApiResult
            {
                Code = ApiResultCode.DataNotFound,
                Data = new List<SampleModel>()
            });
        }

        /// <summary>
        /// 新增客服案件
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public JsonResult AddCustomerServiceCase(AddCustomerServiceCaseInputModel input)
        {
            var thisUserId = MemberFacade.GetUniqueId(User.Identity.Name);
            DataOrm.CustomerServiceMessage entity = csp.CustomerServiceMessageGet(input.ServiceNo);
            if (!entity.IsLoaded)
            {
                entity = new DataOrm.CustomerServiceMessage();
                entity.WorkUser = thisUserId;
                entity.CreateId = User.Identity.Name;
                entity.CreateTime = DateTime.Now;
            }
            else if (entity.WorkUser != thisUserId)
            {
                return Json(new ApiResult
                {
                    Code = ApiResultCode.Error,
                    Message= "案件編號重覆"
                });
            }
            Member member = mp.MemberGet(input.CustomerUserId);
            if (member.IsLoaded)
            {
                entity.Email = member.TrueEmail;
                entity.Phone = member.Mobile;
                entity.Name = member.LastName + member.FirstName;
            }

            entity.Category = input.Category;
            entity.SubCategory = input.SubCategory;
            entity.Status = input.Status;
            entity.ModifyId = thisUserId;
            entity.ModifyTime = DateTime.Now;
            CustomerServiceCategory csc = csp.CustomerServiceCategoryGet(input.SubCategory);
            if (csc.IsLoaded)
            {
                if (csc.IsNeedOrderId)
                {
                    entity.OrderGuid = op.OrderGuidGetById(input.OrderId);
                }
            }
            entity.MessageType = input.QuestionFrom;
            entity.Priority = input.QuestoinLevel;
            entity.UserId = input.CustomerUserId;
            csp.CustomerServiceMessageSet(entity);

            CustomerServiceInsideLog logentity = new CustomerServiceInsideLog();
            logentity.ServiceNo = input.ServiceNo;
            logentity.Content = input.Content;
            logentity.Status = input.Status;
            logentity.CreateTime = DateTime.Now;
            logentity.CreateId = thisUserId;
            csp.CustomerServiceInsideLogSet(logentity);

            return Json(new ApiResult
            {
                Code = ApiResultCode.Success,
                Message = "新增成功!"
            });
        }

        #endregion

        #endregion

        #region Private Method

        #region 處理訂單基本資料

        /// <summary>
        /// 檔次名稱標記
        /// </summary>
        /// <param name="deal"></param>
        /// <returns></returns>
        private List<string> GetDealNameTags(IViewPponDeal deal)
        {
            var tags = new List<string>();

            //[寄倉檔次]
            if (deal.Consignment.GetValueOrDefault(false))
            {
                tags.Add("商品寄倉");
            }

            //[24H][72H]
            if (!string.IsNullOrWhiteSpace(deal.LabelIconList))
            {
                foreach (var s in deal.LabelIconList.Split(','))
                {
                    if (s == ((int)DealLabelSystemCode.ArriveIn24Hrs).ToString() || s == ((int)DealLabelSystemCode.ArriveIn72Hrs).ToString())
                    {
                        tags.Add(string.Format("[{0}]", SystemCodeManager.GetDealLabelCodeName(Convert.ToInt32(s))));
                    }
                }
            }

            //[宅配][憑證][憑證x成套票券A][憑證x成套票券B]
            switch (deal.DeliveryType)
            {
                case (int)DeliveryType.ToShop:
                    bool isGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                    tags.Add("憑證");
                    if (isGroupCoupon)
                    {
                        if ((deal.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.AvgAssign)
                        {
                            tags.Add("憑證x成套票券A");
                        }
                        if ((deal.GroupCouponDealType ?? 0) == (int)GroupCouponDealType.CostAssign)
                        {
                            tags.Add("憑證x成套票券B");
                        }
                    }
                    break;
                case (int)DeliveryType.ToHouse:
                    tags.Add("宅配");
                    break;
                default:
                    break;
            }

            return tags;
        }

        /// <summary>
        /// 訂單狀態
        /// </summary>
        /// <param name="orderStatus"></param>
        /// <returns></returns>
        private Dictionary<int, string> GetOrderStatusLocalized(int orderStatus)
        {
            Dictionary<int, string> status = new Dictionary<int, string>();
            foreach (int val in Enum.GetValues(typeof(OrderStatus)))
            {
                if (val > 0 && (val & orderStatus) > 0)
                {
                    status.Add(val, Helper.GetLocalizedEnum(Phrase.ResourceManager, (OrderStatus)val));
                }
            }
            return status;
        }

        /// <summary>
        /// 支付明細
        /// </summary>
        /// <param name="ctls"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        private List<string> GetPaymentDetail(CashTrustLogCollection ctls, Order order)
        {
            List<string> result = new List<string>();

            var paymentDetail = new PaymentDetail();
            if (ctls.Any())
            {
                //信用卡
                var creditCard = ctls.Sum(x => x.CreditCard);
                if (creditCard > 0)
                {
                    var creditDesc = string.Format("信用卡 {0:N0}", creditCard);
                    if (order.Installment > 0)
                    {
                        creditDesc += "(分 " + order.Installment + " 期)";
                    }
                    if (order.MobilePayType != null)
                    {
                        creditDesc += "( " + (MobilePayType)order.MobilePayType + " )";
                    }
                    result.Add(creditDesc);
                }

                //各種cash
                var pcash = ctls.Sum(x => x.Pcash);
                if (pcash > 0)
                {
                    result.Add(string.Format("PEZ購物金 {0:N0}", pcash));
                }
                var bcash = ctls.Sum(x => x.Bcash);
                if (bcash > 0)
                {
                    result.Add(string.Format("紅利金 {0:N0}", bcash));
                }
                var scash = ctls.Sum(x => x.Scash);
                if (scash > 0)
                {
                    result.Add(string.Format("購物金 {0:N0}", scash));
                }
                var lcash = ctls.Sum(x => x.Lcash);
                if (lcash > 0)
                {
                    result.Add(string.Format("API付款 {0:N0}", lcash));
                }
                var tcash = ctls.Sum(x => x.Tcash);
                if (tcash > 0)
                {
                    result.Add(string.Format("{0} {1:N0}", Helper.GetEnumDescription((ThirdPartyPayment)ctls.First().ThirdPartyPayment), tcash));
                }

                //可能未成單的付款
                var atm = ctls.Sum(x => x.Atm);
                if (atm > 0)
                {
                    result.Add(string.Format("ATM付款 {0:N0}",atm));
                }
                var familycash = ctls.Sum(x => x.FamilyIsp);
                if (familycash > 0)
                {
                    result.Add(string.Format("全家超商付款 {0:N0}", familycash));
                }
                var sevencash = ctls.Sum(x => x.SevenIsp);
                if (sevencash > 0)
                {
                    result.Add(string.Format("7-11超商付款 {0:N0}", sevencash));
                }

                //折價券
                var discountAmount = ctls.Sum(x => x.DiscountAmount);
                if (discountAmount > 0)
                {
                    var discountStr = string.Format("折價券 {0:N0}", discountAmount);
                    var vddCol = OrderFacade.GetViewDiscountDetailByOrderGuid(order.Guid);
                    if (vddCol.Any())
                    {
                        var vdd = vddCol.FirstOrDefault();
                        if (vdd != null)
                        {
                            discountStr += " (" + vdd.Name + ")";
                        }
                    }
                    result.Add(discountStr);
                }
            }
            return result;
        }

        /// <summary>
        /// 店到家期資訊
        /// </summary>
        /// <param name="coupon"></param>
        /// <returns></returns>
        private string GetStoreExpireDateContent(ViewPponCoupon coupon)
        {
            if (coupon == null)
                return string.Empty;

            if (coupon.StoreCloseDownDate.HasValue && coupon.StoreCloseDownDate.Value < coupon.BusinessHourDeliverTimeE)
                return string.Format("(結束營業{0})", coupon.StoreCloseDownDate.Value.ToString("yyyy/MM/dd"));

            if (coupon.StoreChangedExpireDate.HasValue && coupon.StoreChangedExpireDate.Value < coupon.BusinessHourDeliverTimeE)
                return string.Format("(停止使用{0})", coupon.StoreChangedExpireDate.Value.ToString("yyyy/MM/dd"));

            return string.Empty;
        }

        #endregion

        #region 處理憑證商品、退換貨
        
        /// <summary>
        /// 憑證訂位資訊
        /// </summary>
        /// <param name="pi"></param>
        /// <param name="coupon"></param>
        private void SetPponBookingInfo(PponInfo pi, ViewPponCoupon coupon)
        {
            ViewBookingSystemCouponReservationCollection vbscrCol =
                    BookingSystemFacade.GetViewBookingSystemCouponReservationBySequenceNumber(coupon.OrderDetailGuid, coupon.SequenceNumber);
            if (vbscrCol.Any())
            {
                var vbscr = vbscrCol.FirstOrDefault();
                if (vbscr != null)
                {
                    pi.ReservationCreateDate = vbscr.CreateDatetime.ToString("yyyy/MM/dd HH:mm:ss");
                    pi.ReservationDateTime = vbscr.ReservationDateTime == null
                        ? string.Empty : ((DateTime)vbscr.ReservationDateTime).ToString("yyyy/MM/dd HH:mm");
                }

                var lastVbscr = vbscrCol.OrderByDescending(x => x.CreateDatetime).FirstOrDefault();
                if (lastVbscr != null)
                {
                    if (lastVbscr.IsCancel)
                    {
                        pi.ReservationStatus = "已取消";
                        if (lastVbscr.CancelDate != null)
                        {
                            pi.ReservationStatus += "(" + ((DateTime)lastVbscr.CancelDate).ToString("yyyy/MM/dd") + ")";
                        }
                    }
                    if (lastVbscr.ReservationDate < DateTime.Today)
                    {
                        pi.ReservationStatus = "已過期";
                    }
                }
            }
        }

        /// <summary>
        /// 憑證是否可退
        /// </summary>
        /// <param name="couponId"></param>
        /// <param name="ctlogs"></param>
        /// <param name="returnFormCtlogs"></param>
        /// <returns></returns>
        private bool IsCouponReturnable(int couponId, CashTrustLogCollection ctlogs, CashTrustLogCollection returnFormCtlogs)
        {
            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return false;
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
                        if (returningCtlog != null)
                        {
                            return false;
                        }
                        return true;
                    default:
                        return false;
                }
            }

            return false;
        }
        
        #endregion

        #region 處理宅配商品、退換貨

        /// <summary>
        /// 建立退貨單 
        /// </summary>
        /// <param name="i"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool CreateRefund(ProductRefundInputModel i, out string message)
        {
            #region Check

            if (ReturnService.HasProcessingReturnForm(i.OrderGuid)) //仍有退款中之退貨單，無法再申請。
            {
                message = "仍有退款中之退貨單，無法再申請。";
                return false;
            }

            var totalRefundCnt = i.SelectedRefundItems.Sum(x => x.SelectedCount);
            if (ReturnService.HasExchangingLog(i.OrderGuid, totalRefundCnt))
            {
                message = "仍有換貨處理中訂單，不允許全部退貨。";
                return false;
            }

            EinvoiceMainCollection emc = op.EinvoiceMainCollectionGet("order_guid", i.OrderGuid.ToString(), true);
            if (emc.Count > 0)
            {
                if (emc.Where(x => x.InvoiceMode2 == (int)InvoiceMode2.Triplicate && x.InvoiceNumber == null && x.InvoiceStatus == (int)EinvoiceType.C0401).ToList().Count > 0)
                {
                    message = "發票二聯改三聯尚未完成，無法申請退貨";
                    return false;
                }
            }

            #endregion

            #region 進行退貨

            List<int> orderProductIds = new List<int>();
            foreach (var item in i.SelectedRefundItems)
            {
                var cnt = item.SelectedCount;
                string[] pids = item.Pids.Split(',');
                pids.Take(cnt).ForEach(id => orderProductIds.Add(int.Parse(id)));
            }

            int? dummy;
            CreateReturnFormResult result = ReturnService.CreateRefundForm(i.OrderGuid, orderProductIds, i.IsSCashOnly, User.Identity.Name,
                i.RefundReason, i.ReceiverName, i.ReceiverAddress, null, null, out dummy, true);

            if (result != CreateReturnFormResult.Created)
            {
                switch (result)
                {
                    case CreateReturnFormResult.ProductsUnreturnable:
                        message = "欲退項目的狀態無法退貨";
                        break;
                    case CreateReturnFormResult.AtmOrderNeedAtmRefundAccount:
                        message = "ATM訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.OldOrderNeedAtmRefundAccount:
                        message = "舊訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.IspOrderNeedAtmRefundAccount:
                        message = "超商付款訂單, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.InvalidArguments:
                        int comboCount = ReturnService.QueryComboPackCount(i.OrderGuid);
                        message = !Equals(0, orderProductIds.Count % comboCount)
                            ? string.Format("此訂單為成套售出，退貨需成套 [({0})的倍數] 申請。", comboCount)
                            : "無法建立退貨單, 請洽技術部";
                        break;
                    case CreateReturnFormResult.OrderNotCreate:
                        message = "訂單未完成付款, 無法建立退貨單";
                        break;
                    case CreateReturnFormResult.InstallmentOrderNeedAtmRefundAccount:
                        message = "分期且部分退, 請先設定ATM退款帳號";
                        break;
                    case CreateReturnFormResult.ProductsIsExchanging:
                        message = "訂單有換貨處理中商品，無法建立退貨單";
                        break;
                    default:
                        message = "不明錯誤, 請洽技術部";
                        break;
                }
                return false;
            }

            if (emc.Count > 0 && emc.Any(x => x.InvoiceRequestTime.HasValue))
            {
                UserRefundFacade.EinvocieCancelRequestProcess(emc, i.OrderGuid, orderProductIds.ToList(), DeliveryType.ToHouse, User.Identity.Name);
            }

            int returnFormId = op.ReturnFormGetListByOrderGuid(i.OrderGuid).OrderByDescending(x => x.Id).First().Id;
            OrderFacade.SendCustomerReturnFormApplicationMail(returnFormId);
            EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Refund, returnFormId);
            ClearFinalBalanceSheetDate(i.OrderGuid, i.Bid);

            //訊息記錄
            string auditMessage = string.Format("退貨單({0})建立. 退貨原因:{1}", returnFormId, i.RefundReason);
            CommonFacade.AddAudit(i.OrderGuid, AuditType.Refund, auditMessage, User.Identity.Name, false);
            message = string.Format("退貨單({0})建立成功!", returnFormId);
            return true;

            #endregion
        }

        /// <summary>
        /// 建立換貨單
        /// </summary>
        /// <param name="i"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool CreateExchange(ProductRefundInputModel i, out string message)
        {
            if (!OrderExchangeUtility.AllowExchange(i.OrderGuid, out message, true))
            {
                return false;
            }

            var now = DateTime.Now;
            var exchangeLog = new OrderReturnList
            {
                OrderGuid = i.OrderGuid,
                Type = (int)OrderReturnType.Exchange,
                Reason = i.RefundReason,
                Status = (int)OrderReturnStatus.SendToSeller,
                VendorProgressStatus = (int)ExchangeVendorProgressStatus.Processing,
                MessageUpdate = false,
                CreateTime = now,
                CreateId = User.Identity.Name,
                ModifyTime = now,
                ModifyId = User.Identity.Name,
                ReceiverName = i.ReceiverName,
                ReceiverAddress = i.ReceiverAddress
            };

            message = "換貨單建立";
            OrderFacade.SetOrderReturList(exchangeLog, message);
            EmailFacade.SendProductRefundOrExchangeToSeller(OrderReturnType.Exchange, exchangeLog.Id);
            ClearFinalBalanceSheetDate(i.OrderGuid, i.Bid);

            //訊息記錄
            string auditMessage = string.Format("換貨單建立. 換貨原因:{0}", i.RefundReason);
            CommonFacade.AddAudit(i.OrderGuid, AuditType.Refund, auditMessage, User.Identity.Name, false);
            return true;
        }

        /// <summary>
        /// 退換貨 ClearFinalBalanceSheetDate
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <param name="bid"></param>
        private void ClearFinalBalanceSheetDate(Guid orderGuid, Guid bid)
        {
            if (PponOrderManager.CheckIsOverTrialPeriod(orderGuid))
            {
                DealAccounting da = pp.DealAccountingGet(bid);
                if (da.IsLoaded && da.FinalBalanceSheetDate.HasValue)
                {
                    da.FinalBalanceSheetDate = null;
                    pp.DealAccountingSet(da);
                }

                ComboDeal cd = pp.GetComboDeal(bid);
                if (cd.IsLoaded)
                {
                    Guid mainGuid;
                    Guid.TryParse(cd.MainBusinessHourGuid.ToString(), out mainGuid);
                    DealAccounting mainDa = pp.DealAccountingGet(mainGuid);
                    if (mainDa.IsLoaded && mainDa.FinalBalanceSheetDate.HasValue)
                    {
                        mainDa.FinalBalanceSheetDate = null;
                        pp.DealAccountingSet(mainDa);
                    }
                }
            }
        }

        /// <summary>
        /// 折讓單說明
        /// </summary>
        /// <param name="form"></param>
        /// <param name="isPaperAllowance"></param>
        /// <returns></returns>
        private string GetAllowanceDesc(ReturnFormEntity form, bool isPaperAllowance)
        {
            //發票折讓單的狀態是紙本的話，isPaperAllowance = true
            //bool isPaperAllowance = (ViewState != null && ViewState["IsPaperAllowance"] != null) ? (bool)ViewState["IsPaperAllowance"] : true;
            //if (einvoice.AllowanceStatus != (int)AllowanceStatus.ElecAllowance) View.IsPaperAllowance = true;
            //OnChangeToPaperAllowance => if (returnForm.ProgressStatus == ProgressStatus.Processing) View.IsPaperAllowance = true; 
            if (form.CreditNoteType.Equals(0))
            {
                if ((form.ProgressStatus == ProgressStatus.Completed || form.ProgressStatus == ProgressStatus.Canceled) &&
                    isPaperAllowance && !form.IsCreditNoteReceived)
                {
                    return "";
                }
                return isPaperAllowance ? "使用紙本折讓" : "同意電子折讓";
            }

            switch (form.CreditNoteType)
            {
                case (int)AllowanceStatus.PaperAllowance:
                    return "使用紙本折讓";
                case (int)AllowanceStatus.ElecAllowance:
                    return "同意電子折讓";
                case (int)AllowanceStatus.None:
                    return "不需要折讓";
                default:
                    return "";
            }
        }

        #endregion

        #region 到期日
        private DateTime GetFinalExpireDate(string p)
        {
            Guid oid;
            if (Guid.TryParse(p, out oid))
            {
                ExpirationDateSelector selector = GetExpirationComponent(oid);

                if (selector != null)
                {
                    return selector.GetExpirationDate();
                }
            }
            return new DateTime(1900, 1, 1);
        }
        private ExpirationDateSelector GetExpirationComponent(Guid orderGuid)
        {
            Dictionary<Guid, ExpirationDateSelector> expirationComponents = new Dictionary<Guid, ExpirationDateSelector>();
            if (expirationComponents.ContainsKey(orderGuid))
            {
                return expirationComponents[orderGuid];
            }

            ExpirationDateSelector selector = new ExpirationDateSelector();
            selector.ExpirationDates = new PponUsageExpiration(orderGuid);

            expirationComponents.Add(orderGuid, selector);
            return selector;
        }
        #endregion

        #region 訂單、憑證 各種狀態
        
        private string GetOrderLogStatusDesc(OrderLogStatus status)
        {
            string statusDesc = Helper.GetLocalizedEnum(status) ?? "UnKnown";
            switch (status)
            {
                case OrderLogStatus.Processing:
                case OrderLogStatus.AutoProcessing:
                    statusDesc += "-刷退";
                    break;
                case OrderLogStatus.AutoProcessingScashOnly:
                case OrderLogStatus.ProcessingScashOnly:
                    statusDesc += "-退購物金";
                    break;
            }
            return statusDesc;
        }

        private string GetCouponStatus(int couponId, CashTrustLogCollection ctlogs, CashTrustLogCollection returnFormCtlogs)
        {
            CashTrustLog ctlog = ctlogs.FirstOrDefault(x => x.CouponId == couponId);
            if (ctlog != null)
            {
                if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.AtmRefunding))
                {
                    return "ATM退款中，無法退貨";
                }

                switch ((TrustStatus)ctlog.Status)
                {
                    case TrustStatus.Initial:
                    case TrustStatus.Trusted:
                        CashTrustLog returningCtlog = returnFormCtlogs.FirstOrDefault(x => x.CouponId == couponId);
                        if (returningCtlog != null)
                        {
                            return "已在退貨單上，無法退貨";
                        }
                        return "未核銷，可進行退貨";
                    case TrustStatus.Verified:
                        DateTime modifyDT = ctlog.ModifyTime;
                        string modifyTime = string.Format("{0:yyyy/MM/dd HH:mm} ", modifyDT);

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationForced))
                        {
                            return modifyTime + "已強制核銷";
                        }

                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.VerificationLost))
                        {
                            return modifyTime + "已核銷，清冊遺失";
                        }

                        return modifyTime + "已核銷";
                    case TrustStatus.Returned:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已退購物金，無法再退貨";
                    case TrustStatus.Refunded:
                        if (Helper.IsFlagSet(ctlog.SpecialStatus, TrustSpecialStatus.ReturnForced))
                        {
                            return "已強制退貨";
                        }

                        return "已轉退現金，無法再退貨";
                    case TrustStatus.ATM:
                        return "未付款，無法退貨";
                    default:
                        return "查無即時核銷資訊";
                }
            }

            return "查無即時核銷資訊";
        }

        private string GetBalanceSheetFrequenyDesc(int generationFrequency, string yearMonth, DateTime intervalEnd)
        {
            switch ((BalanceSheetGenerationFrequency)generationFrequency)
            {
                case BalanceSheetGenerationFrequency.MonthBalanceSheet:
                    return yearMonth;
                case BalanceSheetGenerationFrequency.WeekBalanceSheet:
                case BalanceSheetGenerationFrequency.FortnightlyBalanceSheet:
                    return string.Format("{0:yyyy/MM/dd}", intervalEnd.AddDays(-1));
                default:
                    return string.Format("{0:yyyy/MM/dd}", intervalEnd);
            }
        }

        #endregion

        #endregion


    }

}

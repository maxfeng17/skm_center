﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.BalanceSheet;
using LunchKingSite.BizLogic.Vbs.BS;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.WebLib.Models.ControlRoom.Finance;
using LunchKingSite.WebLib.Models.VendorBillingSystem;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using IsolationLevel = System.Transactions.IsolationLevel;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using System.Web;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    public class FinanceController : ControllerExt
    {
        #region property

        private static IAccountingProvider _ap;
        private static ISysConfProvider _config;
        private static IMemberProvider _mp;
        private static IPponProvider _pp;
        private static IVerificationProvider _vp;
        private static IOrderProvider _op;

        static FinanceController()
        {
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        }

        #endregion property

        #region Action

        public ActionResult ToHouseDealPaymentAudit(ToHouseDealPaymentAuditModel model, int? pageNumber, bool? excel, bool? exportBalanceSheet)
        {
            pageNumber = pageNumber ?? 1;
            excel = excel ?? false;
            exportBalanceSheet = exportBalanceSheet ?? false;
            var infos = new List<DealPaymentInfo>();

            if (model.QueryRequest)
            {
                string sortCol = model.OrderByColumn;
                var dealInfos = GetDealPaymentInfoFromQueryRequest(model);
                var dealGuids = dealInfos.Select(x => x.BusinessHourGuid).ToList();
                var paymentChangeInfos = _ap.VendorPaymentChangeGetList(dealGuids)
                                            .GroupBy(x => x.BusinessHourGuid)
                                            .ToDictionary(x => x.Key,
                                                          x => x.Sum(a => a.AllowanceAmount != 0 ? a.AllowanceAmount : a.Amount));

                //撈取對帳單資料
                var bscs = GetBalanceSheetInfos(dealGuids)
                            .GroupBy(x => x.ProductGuid)
                            .ToDictionary(x => x.Key, x => x.Count(b => !b.IsConfirmedReadyToPay));

                //撈取 多檔次子檔對應之母檔 資訊 
                var subDeals = GetComboSubDealsInfo(dealGuids);
                var allDealGuids = dealGuids;
                var attachDealGuids = subDeals.Keys.Where(x => !dealGuids.Contains(x))
                                                   .ToList();
                allDealGuids.AddRange(attachDealGuids);
                var mainDealGuids = subDeals.Values.Where(x => !dealGuids.Contains(x)).ToList();
                foreach (var item in mainDealGuids)
                {
                    if (!attachDealGuids.Contains(item))
                    {
                        attachDealGuids.Add(item);
                    }
                }
                var balanceSheetInfos = GetHasLumpSumBalanceSheetInfo(allDealGuids);
                var tempDealInfos = dealInfos.ToList();
                tempDealInfos.AddRange(_pp.ViewDealPropertyBusinessHourContentGetList(attachDealGuids));
                var allDealInfos = tempDealInfos.ToDictionary(x => x.BusinessHourGuid, x => x);

                foreach (var deal in dealInfos)
                {
                    Guid? mainDealGuid = null;
                    int tempSlug;
                    string balanceSheetCnt = "";
                    mainDealGuid = subDeals.ContainsKey(deal.BusinessHourGuid)
                                    ? subDeals[deal.BusinessHourGuid]
                                    : mainDealGuid;
                    var subDealGuids = mainDealGuid == null
                            ? new List<Guid> { deal.BusinessHourGuid }
                            : subDeals.Where(x => x.Value == mainDealGuid &&
                                                  allDealInfos.ContainsKey(x.Key) &&
                                                  int.TryParse(allDealInfos[x.Key].Slug, out tempSlug) &&
                                                  tempSlug >= allDealInfos[x.Key].BusinessHourOrderMinimum)
                                      .Select(x => x.Key);
                    var mainDealInfo = mainDealGuid == null
                                        ? allDealInfos.ContainsKey(deal.BusinessHourGuid)
                                            ? allDealInfos[deal.BusinessHourGuid]
                                            : null
                                        : allDealInfos.ContainsKey(mainDealGuid.Value)
                                            ? allDealInfos[mainDealGuid.Value]
                                            : null;

                    if (bscs.ContainsKey(deal.BusinessHourGuid))
                    {
                        if (bscs[deal.BusinessHourGuid] > 0)
                        {
                            balanceSheetCnt = string.Format("尚未完成 ( {0} )", bscs[deal.BusinessHourGuid]);
                        }
                        else
                        {
                            balanceSheetCnt = "已完成";
                        }
                    }

                    var info = new DealPaymentInfo
                    {
                        DealGuid = deal.BusinessHourGuid,
                        DealId = deal.UniqueId,
                        DealName = deal.CouponUsage,
                        DealContent = deal.DealName,
                        RemittanceType = (RemittanceType)deal.RemittanceType,
                        DealOrderStartTime = deal.BusinessHourOrderTimeS,
                        DealOrderEndTime = deal.BusinessHourOrderTimeE,
                        SalesName = deal.DeEmpName + (!string.IsNullOrEmpty(deal.OpEmpName) ? "/" + deal.OpEmpName : ""),
                        SignCompanyId = mainDealInfo == null ? deal.SignCompanyID : mainDealInfo.SignCompanyID,
                        SignCompanyName = mainDealInfo == null ? deal.CompanyName : mainDealInfo.CompanyName,
                        CompanyBossName = mainDealInfo == null ? deal.CompanyBossName : mainDealInfo.CompanyBossName,
                        AccountName = mainDealInfo == null ? deal.CompanyAccountName : mainDealInfo.CompanyAccountName,
                        AccountId = mainDealInfo == null ? deal.CompanyID : mainDealInfo.CompanyID,
                        AccountNo = mainDealInfo == null ? deal.CompanyAccount : mainDealInfo.CompanyAccount,
                        BankCode = mainDealInfo == null ? deal.CompanyBankCode : mainDealInfo.CompanyBankCode,
                        BranchCode = mainDealInfo == null ? deal.CompanyBranchCode : mainDealInfo.CompanyBranchCode,
                        IsTax = mainDealInfo == null ? deal.IsTax : mainDealInfo.IsTax,
                        ShippedDate = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub)
                                        ? deal.MainDealShippedDate
                                        : deal.ShippedDate,
                        PartiallyPaymentDate = deal.PartiallyPaymentDate.HasValue
                                                ? string.Format("{0:yyyy/MM/dd}", deal.PartiallyPaymentDate.Value)
                                                : deal.RemittanceType == (int)RemittanceType.Others
                                                    ? "無暫付"
                                                    : Helper.IsFlagSet(deal.GroupOrderStatus.GetValueOrDefault((int)GroupOrderStatus.None), GroupOrderStatus.PartialFail)
                                                     ? "退貨超過三成"
                                                     : string.Empty,
                        FinalBalanceSheetDate = deal.FinalBalanceSheetDate,
                        BalanceSheetCreateDate = deal.BalanceSheetCreateDate,
                        CompanyEmail = deal.CompanyEmail,
                        BillGetDate = deal.FinanceGetDate,
                        BillCompanyId = deal.InvoiceComId,
                        BillNumber = deal.BillNumber,
                        VendorPaymentChangeAmountDesc = paymentChangeInfos.ContainsKey(deal.BusinessHourGuid)
                                                        ? paymentChangeInfos[deal.BusinessHourGuid].ToString()
                                                        : string.Empty,
                        HasBalanceSheet = ((Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub) &&
                                          deal.MainDealBalanceSheetCreateDate.HasValue &&
                                          DateTime.Compare(DateTime.Now, deal.MainDealBalanceSheetCreateDate.Value) > 0) ||
                                          !Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub)) &&
                                          subDealGuids.All(x => allDealInfos.ContainsKey(x) &&
                                                                allDealInfos[x].BalanceSheetCreateDate.HasValue &&
                                                                DateTime.Compare(DateTime.Now, allDealInfos[x].BalanceSheetCreateDate.Value) > 0 &&
                                                                balanceSheetInfos.ContainsKey(x) && balanceSheetInfos[x] > 0),
                        IsDealEstablished = int.TryParse(deal.Slug, out tempSlug) &&
                                            tempSlug >= deal.BusinessHourOrderMinimum,
                        IsFreeze = Helper.IsFlagSet(deal.Flag, (int)AccountingFlag.Freeze),
                        BalanceSheetCompleteCnt = balanceSheetCnt,
                        //先收單據後付款檔次 且 檔次仍在對帳區間 才有整檔凍結功能
                        EnableFreeze = (deal.RemittanceType == (int)RemittanceType.Flexible || 
                                        deal.RemittanceType == (int)RemittanceType.Monthly || 
                                        deal.RemittanceType == (int)RemittanceType.Weekly ||
                                        deal.RemittanceType == (int)RemittanceType.Fortnightly) &&
                                        (BalanceSheetManager.IsInBalanceTimeRange(deal.BusinessHourGuid, DateTime.Today, true))
                    };
                    infos.Add(info);
                }

                if (excel.Value)
                {
                    MemoryStream outputStream = ToHouseDealPaymentExport(infos, sortCol, false);
                    return new FileStreamResult(outputStream, "application/vnd.ms-excel") { FileDownloadName = "商品檔週期維護檔次列表.xls" };
                }

                if (exportBalanceSheet.Value)
                {
                    var bsModel = new BalanceSheetsLumpSumModel();
                    bsModel.BalanceSheetLumpSumInfo = GetBalanceSheetsDeliverModels(infos.Where(x => x.HasBalanceSheet).Select(x => x.DealGuid));
                    return View("SimulateBalanceSheetLumpSum", bsModel);
                }

                model.DealPaymentInfos = GetPagerDealPaymentInfo(infos, 50, pageNumber.Value - 1, sortCol, false);
                model.IsFinishReturnAndExchangeDateEdit = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name,
                                                                                   SystemFunctionType.SetFinishReturnAndExchangeDate);
                model.IsRelatedFinanceDateEdit = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name,
                                                                                   SystemFunctionType.SetRelatedFinanceDateAndAllowanceAmount);
            }

            return View("ToHouseDealPaymentAudit", model);
        }

        [HttpPost]
        public JsonResult UpdateDateRequest(UpdateDateRequestModel model)
        {
            var dealInfos = model.UpdateDealInfo.Split(",");
            var dealIds = new Dictionary<Guid, int>();
            foreach (var dealId in dealInfos.Select(info => info.Split(";")).Where(dealId => dealId.Length == 2))
            {
                Guid productGuid;
                Guid.TryParse(dealId[0], out productGuid);
                int productId;
                int.TryParse(dealId[1], out productId);
                dealIds.Add(productGuid, productId);
            }
            var dealAccountings = _pp.ViewDealPropertyBusinessHourContentGetList(dealIds.Keys)
                                        .ToDictionary(x => x.BusinessHourGuid, x => x);
            var dealSalesSummarys = _vp.GetPponToHouseDealSummary(dealIds.Keys)
                                        .ToDictionary(x => x.MerchandiseGuid, x => x);
            var balanceSheetInfos = new Dictionary<Guid, int>();
            var vendorPaymentInfos = new Dictionary<Guid, int>();

            switch (model.UpdateDateOption)
            {
                case UpdateDateOption.SetBalanceSheetCreateDate:
                    balanceSheetInfos = GetHasLumpSumBalanceSheetInfo(dealIds.Keys);
                    vendorPaymentInfos = _ap.VendorPaymentChangeGetList(dealIds.Keys)
                                            .GroupBy(x => x.BusinessHourGuid)
                                            .ToDictionary(x => x.Key, x => x.Count());
                    break;
                default:
                    break;
            }
            var message = new StringBuilder();
            DateTime? setDate = DateTime.Now;

            foreach (var deal in dealIds)
            {
                if (dealAccountings.ContainsKey(deal.Key))
                {
                    bool isPartiallyPaymentDateNeeded = IsPartiallyPaymentDateNeeded((RemittanceType)dealAccountings[deal.Key].RemittanceType,
                                                        dealAccountings[deal.Key].GroupOrderStatus.GetValueOrDefault((int)GroupOrderStatus.None));

                    #region 狀態異動檢查

                    int tempSlug;
                    if (dealAccountings[deal.Key].BusinessHourOrderTimeE > DateTime.Now ||
                        !int.TryParse(dealAccountings[deal.Key].Slug, out tempSlug) ||
                        tempSlug < dealAccountings[deal.Key].BusinessHourOrderMinimum)
                    {
                        message.Append(string.Format("檔號[{0}]尚未結檔或未成檔，無法異動。\r\n", deal.Value));
                        continue;
                    }

                    var orderedQuantity = 0;
                    if (dealSalesSummarys.ContainsKey(deal.Key))
                    {
                        orderedQuantity = dealSalesSummarys[deal.Key].UnverifiedCount + dealSalesSummarys[deal.Key].VerifiedCount;
                    }
                    if (orderedQuantity > 0 && !dealAccountings[deal.Key].ShippedDate.HasValue)
                    {
                        message.Append(string.Format("檔號[{0}]尚未設定出貨回覆日，無法異動。\r\n", deal.Value));
                        continue;
                    }

                    #region 更新暫付七成日、退換貨完成日 檢查

                    if (model.UpdateDateOption == UpdateDateOption.SetFinalBalanceSheetDate ||
                        model.UpdateDateOption == UpdateDateOption.SetFinalBalanceSheetDateNull ||
                        model.UpdateDateOption == UpdateDateOption.SetPartiallyPaymentDate ||
                        model.UpdateDateOption == UpdateDateOption.SetPartiallyPaymentDateNull)
                    {
                        if (model.UpdateDateOption == UpdateDateOption.SetFinalBalanceSheetDate)
                        {
                            if (orderedQuantity > 0 && isPartiallyPaymentDateNeeded)
                            {
                                if (!dealAccountings[deal.Key].PartiallyPaymentDate.HasValue)
                                {
                                    message.Append(string.Format("檔號[{0}]尚未設定暫付七成付款日，無法異動。\r\n", deal.Value));
                                    continue;
                                }
                                if (DateTime.Compare(setDate.Value, dealAccountings[deal.Key].PartiallyPaymentDate.Value) < 0)
                                {
                                    message.Append(string.Format("檔號[{0}]欲設定之日期須大於暫付七成付款日。\r\n", deal.Value));
                                    continue;
                                }
                            }
                        }
                        if (model.UpdateDateOption == UpdateDateOption.SetPartiallyPaymentDate)
                        {
                            if (orderedQuantity == 0 || !isPartiallyPaymentDateNeeded)
                            {
                                message.Append(string.Format("檔號[{0}]無須填寫暫付七成付款日。\r\n", deal.Value));
                                continue;
                            }
                            setDate = model.UpdatePartiallyPaymentDate;
                        }
                        if (model.UpdateDateOption == UpdateDateOption.SetFinalBalanceSheetDateNull ||
                            model.UpdateDateOption == UpdateDateOption.SetPartiallyPaymentDateNull)
                        {
                            if (dealAccountings[deal.Key].BalanceSheetCreateDate.HasValue)
                            {
                                message.Append(string.Format("檔號[{0}]已設定對帳單開立日，無法異動。\r\n", deal.Value));
                                continue;
                            }
                            if (model.UpdateDateOption == UpdateDateOption.SetPartiallyPaymentDateNull &&
                                dealAccountings[deal.Key].FinalBalanceSheetDate.HasValue)
                            {
                                message.Append(string.Format("檔號[{0}]已設定退換貨處理完成日，無法異動。\r\n", deal.Value));
                                continue;
                            }
                            setDate = null;
                        }
                    }

                    #endregion 更新暫付七成日、退換貨完成日 檢查

                    #region 更新對帳單開立日 檢查

                    else if (model.UpdateDateOption == UpdateDateOption.SetBalanceSheetCreateDate)
                    {
                        if (dealAccountings[deal.Key].RemittanceType == (int)RemittanceType.Flexible ||
                            dealAccountings[deal.Key].RemittanceType == (int)RemittanceType.Monthly ||
                            dealAccountings[deal.Key].RemittanceType == (int)RemittanceType.Weekly)
                        {
                            message.Append(string.Format("檔號[{0}]出帳方式為先收單據後付款檔次，無須壓記對帳單開立日。\r\n", deal.Value));
                            continue;
                        }
                        if (orderedQuantity > 0 && isPartiallyPaymentDateNeeded)
                        {
                            if (!dealAccountings[deal.Key].PartiallyPaymentDate.HasValue)
                            {
                                message.Append(string.Format("檔號[{0}]尚未設定暫付七成付款日，無法異動。\r\n", deal.Value));
                                continue;
                            }
                            if (DateTime.Compare(setDate.Value, dealAccountings[deal.Key].PartiallyPaymentDate.Value) < 0)
                            {
                                message.Append(string.Format("檔號[{0}]欲設定之日期須大於暫付七成付款日。\r\n", deal.Value));
                                continue;
                            }
                        }
                        if (!dealAccountings[deal.Key].FinalBalanceSheetDate.HasValue)
                        {
                            message.Append(string.Format("檔號[{0}]尚未設定退換貨完成日，無法異動。\r\n", deal.Value));
                            continue;
                        }
                        if (DateTime.Compare(setDate.Value, dealAccountings[deal.Key].FinalBalanceSheetDate.Value) < 0)
                        {
                            message.Append(string.Format("檔號[{0}]欲設定之日期須大於退換貨完成日。\r\n", deal.Value));
                            continue;
                        }
                        if ((!balanceSheetInfos.ContainsKey(deal.Key) ||
                            (balanceSheetInfos.ContainsKey(deal.Key) && balanceSheetInfos[deal.Key] == 0)) &&
                            !vendorPaymentInfos.ContainsKey(deal.Key))
                        {
                            message.Append(string.Format("檔號[{0}]將由系統自動壓上對帳單開立日，無法異動。\r\n", deal.Value));
                            continue;
                        }
                    }
                    else if (model.UpdateDateOption == UpdateDateOption.SetBalanceSheetCreateDateNull)
                    {
                        if (dealAccountings[deal.Key].FinanceGetDate.HasValue)
                        {
                            message.Append(string.Format("檔號[{0}]已設定發票收到日，無法異動。\r\n", deal.Value));
                            continue;
                        }
                        setDate = null;
                    }

                    #endregion 更新對帳單開立日 檢查

                    #endregion 狀態異動檢查

                    message.Append(ProcessUpdateDateOption(model.UpdateDateOption, deal.Key, setDate, User.Identity.Name)
                                       ? string.Format("檔號[{0}]更新成功。\r\n", deal.Value)
                                       : string.Format("檔號[{0}]更新失敗，請洽技術部。\r\n", deal.Value));
                }
                else
                {
                    message.Append(string.Format("查無檔號[{0}]資訊\r\n", deal.Value));
                }
            }

            return Json(new { Message = message.ToString() });
        }


        [HttpPost]
        public JsonResult FreezeDealRequest(string DealGuid, string tp, string FreezeReason)
        {
            /*
             * 整檔凍結
             * */
            var message = new StringBuilder();
            Guid guid;
            bool flag = Guid.TryParse(DealGuid, out guid);
            var da = _pp.DealAccountingGet(guid);

            if (tp == "1")
            {
                //整檔凍結
                da.Flag = (int)Helper.SetFlag(true, da.Flag, (int)AccountingFlag.Freeze);
            }
            else
            {
                //整檔取消凍結
                da.Flag = (int)Helper.SetFlag(false, da.Flag, AccountingFlag.Freeze);
            }
            //更新資料庫
            _pp.DealAccountingSet(da);

            //寫入Log
            CommonFacade.AddAudit(DealGuid, AuditType.DealAccounting, (tp == "1" ? "【系統凍結請款】" : "【系統取消凍結請款】") + ":" + FreezeReason, User.Identity.Name, false);

            //發信給業務/主管/特助
            EmailFacade.SendFreezeMail((tp == "1" ? FreezeType.FreezeAccounting : FreezeType.CancelFreezeAccounting), guid, FreezeReason);

            message.Append(string.Format("檔號[{0}]更新成功。\r\n", DealGuid));

            return Json(new { Message = message.ToString() });
        }

        public JsonResult FreezeOrderRequest(string OrderInfo, string tp, string FreezeReason)
        {
            var message = new StringBuilder();

            OrderCollection Orders = _pp.PponOrderGetByOrderId(OrderInfo);
            string _reason = "";
            if (Orders.Count > 0)
            {
                Guid guid = Orders[0].Guid;
                List<Guid> guids = new List<Guid>();
                foreach (Order _order in Orders)
                {
                    guids.Add(_order.Guid);
                }
                CashTrustLogCollection ctlogs = _mp.GetCashTrustLogCollectionByOid(guids);
                bool isFreeze = false;

                foreach (CashTrustLog ctl in ctlogs)
                {
                    if (tp == "1")
                    {
                        //凍結單筆定單
                        ctl.SpecialStatus = (int)Helper.SetFlag(true, ctl.SpecialStatus, TrustSpecialStatus.Freeze);
                    }
                    else
                    {
                        //取消單筆訂單
                        ctl.SpecialStatus = (int)Helper.SetFlag(false, ctl.SpecialStatus, TrustSpecialStatus.Freeze);
                    }
                    /*
                    * 更新cash_trust_status_log
                    * */
                    var ctsl = new CashTrustStatusLog
                    {
                        TrustId = ctl.TrustId,
                        TrustProvider = ctl.TrustProvider,
                        Amount = ctl.Amount,
                        Status = ctl.Status,
                        ModifyTime = DateTime.Now,
                        CreateId = "sys",
                        SpecialStatus = ctl.SpecialStatus,
                        SpecialOperatedTime = ctl.SpecialOperatedTime
                    };

                    //更新訂單的凍結狀態
                    _mp.CashTrustLogSet(ctl);

                    //更新cash_trust_status_log
                    _mp.CashTrustStatusLogSet(ctsl);
                }
                foreach (Guid g in guids)
                {
                    isFreeze = (tp == "1" ? true : false);
                    _reason = (isFreeze ? "【系統凍結請款】" : "【系統取消凍結請款】");
                    //寫入記錄檔
                    CommonFacade.AddAudit(g, AuditType.Freeze, _reason + ":" + FreezeReason, User.Identity.Name, false);

                    EmailFacade.SendFreezeMail((tp == "1" ? FreezeType.FreezeOrder : FreezeType.CancelFreezeOrder), g, FreezeReason);
                }
                message.Append(_reason + "成功!");
            }
            else
            {
                message.Append(_reason + "失敗!");
            }



            return Json(new { Message = message.ToString() });
        }

        public ActionResult ToHouseDealPaymentManage(Guid dealGuid)
        {
            var model = GetToHouseDealPaymentManageModel(dealGuid, null, new ToHouseDealPaymentManageModel());
            var dealInfo = _pp.ViewDealPropertyBusinessHourContentGetByBid(dealGuid);
            model.DealGuid = dealGuid;
            model.DealId = dealInfo.UniqueId;
            model.DealContent = dealInfo.DealName;

            return View("ToHouseDealPaymentManage", model);
        }

        [HttpPost]
        public ActionResult SaveVendorPaymentChange(PaymentChangeModel model)
        {
            var viewModel = new ToHouseDealPaymentManageModel();
            DateTime now = DateTime.Now;
            var orders = string.IsNullOrEmpty(model.OrderIdInfo)
                            ? new List<string>()
                            : SplitOrderIdInfo(model.OrderIdInfo);
            var orderInfo = _pp.ViewPponOrderGetList(model.DealGuid)
                                .Where(x => orders.Contains(x.OrderId))
                                .ToDictionary(x => x.Guid, x => x);
            model.OrderCount = orderInfo.Any()
                                ? orderInfo.Count
                                : 1;
            if (orders.Count != orderInfo.Count)
            {
                viewModel.ReturnMessage = "輸入之訂單編號有誤，請重新確認";
                viewModel.IsModificationSuccess = false;
            }
            if (!model.Amount.HasValue)
            {
                viewModel.ReturnMessage = "未輸入廠商補扣款金額";
                viewModel.IsModificationSuccess = false;
            }

            if (viewModel.IsModificationSuccess.GetValueOrDefault(true))
            {
                model.PromotionValue = model.PromotionValue ?? 0;
                if (model.PromotionValue.Value < 0)
                {
                    throw new Exception("Promtion value not allow < 0");
                }

                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    #region 新增 vendor_payment_change

                    try
                    {
                        var vpc = new VendorPaymentChange
                        {
                            BusinessHourGuid = model.DealGuid,
                            Reason = model.Reason,
                            Amount = model.OrderCount * model.Amount.Value,
                            PromotionValue = model.OrderCount * model.PromotionValue.Value,
                            CreateId = User.Identity.Name,
                            CreateTime = now,
                            AllowanceAmount = model.OrderCount * model.Amount.Value,
                            ModifyId = User.Identity.Name,
                            ModifyTime = now
                        };

                        model.PaymentChangeId = _ap.VendorPaymentChangeSet(vpc);
                    }
                    catch
                    {
                        viewModel.ReturnMessage = "儲存發生錯誤，請洽技術部";
                        viewModel.IsModificationSuccess = false;
                    }

                    #endregion 新增 vendor_payment_change

                    #region 新增 補消費者紅利紀錄

                    if (viewModel.IsModificationSuccess.GetValueOrDefault(true) &&
                        orderInfo.Any() &&
                        model.PromotionValue > 0 &&
                        model.StartTime.HasValue &&
                        model.ExpireTime.HasValue)
                    {
                        foreach (var promotion in orderInfo)
                        {
                            int userId = promotion.Value.UserId;
                            var mpd = new MemberPromotionDeposit
                            {
                                OrderGuid = promotion.Key,
                                StartTime = model.StartTime.Value,
                                ExpireTime = model.ExpireTime.Value,
                                PromotionValue = model.PromotionValue.Value * 10, // 紅利金換算規則為 : 台幣金額 * 10倍
                                Action = model.Action,
                                CreateId = User.Identity.Name,
                                CreateTime = now,
                                UserId = userId,
                                OrderClassification = (int)OrderClassification.LkSite,
                                VendorPaymentChangeId = model.PaymentChangeId
                            };
                            string userName = promotion.Value.MemberUserName;
                            MemberFacade.MemberPromotionProcess(mpd, userName, false);

                            //紀錄log
                            string msg = "扣款/異動金額：" + model.Amount.ToString() + "元 <br /> 扣款 / 異動原因：" + model.Reason + "<br />  紅利金摘要： " + model.Reason + " <br />  須給消費者紅利金：" + model.PromotionValue.Value + "元";
                            CommonFacade.AddAudit(promotion.Key, AuditType.Order, msg, User.Identity.Name, false);
                        }
                    }
                    else
                    {
                        //不須補紅利金，需記錄訂單編號

                        foreach (var promotion in orderInfo)
                        {
                            var mnds = new MemberNoneDeposit
                            {
                                OrderGuid = promotion.Key,
                                CreateId = User.Identity.Name,
                                CreateTime = now,
                                VendorPaymentChangeId = model.PaymentChangeId
                            };

                            _mp.MemberNoneDepositSet(mnds);
                        }

                    }

                    #endregion 新增 補消費者紅利紀錄

                    scope.Complete();
                }

                viewModel.IsModificationSuccess = viewModel.IsModificationSuccess ?? true;
                viewModel = GetToHouseDealPaymentManageModel(model.DealGuid, model.PaymentChangeId, viewModel);
            }

            return View("ToHouseDealPaymentManage", viewModel);
        }

        [HttpPost]
        public ActionResult SaveAllowanceAmount(PaymentChangeModel model)
        {
            var viewModel = new ToHouseDealPaymentManageModel();
            var now = DateTime.Now;

            if (!model.PaymentChangeId.HasValue)
            {
                viewModel.ReturnMessage = "資料異常，請洽技術部";
                viewModel.IsModificationSuccess = false;
            }

            #region 更新折讓扣抵金額

            if (viewModel.IsModificationSuccess.GetValueOrDefault(true) &&
                model.AllowanceAmount.HasValue)
            {
                var paymentChange = _ap.VendorPaymentChangeGet(model.PaymentChangeId.Value);
                //若扣抵金額未異動 則不需更新
                if (paymentChange.AllowanceAmount != model.AllowanceAmount)
                {
                    try
                    {
                        paymentChange.AllowanceAmount = model.AllowanceAmount.Value;
                        paymentChange.ModifyId = User.Identity.Name;
                        paymentChange.ModifyTime = now;
                        _ap.VendorPaymentChangeSet(paymentChange);
                    }
                    catch
                    {
                        viewModel.ReturnMessage = "儲存發生錯誤，請洽技術部";
                        viewModel.IsModificationSuccess = false;
                    }
                }

                viewModel.IsModificationSuccess = viewModel.IsModificationSuccess ?? true;
                viewModel = GetToHouseDealPaymentManageModel(model.DealGuid, model.PaymentChangeId, viewModel);
            }

            #endregion 更新折讓扣抵金額

            return View("ToHouseDealPaymentManage", viewModel);
        }

        public ActionResult SimulateBalanceSheetLumpSum(Guid dealGuid)
        {
            var model = new BalanceSheetsLumpSumModel();

            model.BalanceSheetLumpSumInfo = GetBalanceSheetsDeliverModels(new List<Guid> { dealGuid });

            return View("SimulateBalanceSheetLumpSum", model);
        }

        [HttpPost]
        public JsonResult UpdateBalanceSheetEstAmount(ChangeEstAmountModel model)
        {
            var changeIds = !string.IsNullOrEmpty(model.PaymentChangeIds)
                            ? model.PaymentChangeIds.Split(',').Select(int.Parse).ToList()
                            : new List<int>();
            var message = new StringBuilder();
            var updateBsIds = new List<int>();
            if (model.BalanceSheetId > 0)
            {
                var bs = _ap.BalanceSheetGet(model.BalanceSheetId);
                if (bs == null)
                {
                    return Json(new { Result = false, Message = "查無對帳單資料。" });
                }
                updateBsIds.Add(model.BalanceSheetId);
            }

            var updateFail = false;

            #region 更新廠商補扣款資料
            
            if (changeIds.Any())
            { 
                var paymentChanges = _ap.VendorPaymentChangeGetList(changeIds);
                foreach (var paymentChange in paymentChanges)
                {
                    try
                    {
                        if (paymentChange.BalanceSheetId.HasValue)
                        {
                            updateBsIds.Add(paymentChange.BalanceSheetId.Value);
                        }
                        var updateBsId = model.BalanceSheetId > 0 ? model.BalanceSheetId : (int?)null;
                        var actionDesc = string.Format("Id:{0} 廠商補扣款關聯對帳單更新 : {1} -> {2}", paymentChange.Id, paymentChange.BalanceSheetId, updateBsId);
                        paymentChange.BalanceSheetId = updateBsId;
                        _ap.VendorPaymentChangeSet(paymentChange);
                        CommonFacade.AddAudit(paymentChange.BusinessHourGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                    }
                    catch (Exception)
                    {
                        updateFail = true;
                        message.Append(string.Format("Id:{0} 廠商補扣款關聯對帳單更新失敗，請洽技術部。\r\n", paymentChange.Id));
                    }
                }
            }

            #endregion 更新廠商補扣款資料
            
            #region 更新對帳單金額

            if (!updateFail)
            {
                foreach (var bsId in updateBsIds)
                {
                    var bsModel = new BalanceSheetModel(bsId);
                    var bs = bsModel.GetDbBalanceSheet();
                    var amount = (int)bsModel.GetAccountsPayable().TotalAmount;
                    var vendorPositivePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorPositivePaymentOverdueAmount;
                    var vendorNegativePaymentOverdueAmount = (int)bsModel.GetAccountsPayable().VendorNegativePaymentOverdueAmount;
                    //更新對帳單金額
                    //只有尚未確認或是金額為0才能異動
                    if (bs.EstAmount != amount && (!bs.IsConfirmedReadyToPay || bs.EstAmount == 0))
                    {
                        try
                        {
                            var actionDesc = string.Format("Id : {0} UpdateBalanceSheetEstAmount : {1} -> {2}", bs.Id, bs.EstAmount, amount);
                            bs.EstAmount = amount;
                            _ap.BalanceSheetEstAmountSet(bs, vendorPositivePaymentOverdueAmount, vendorNegativePaymentOverdueAmount);
                            CommonFacade.AddAudit(bs.ProductGuid, AuditType.DealAccounting, actionDesc, User.Identity.Name, true);
                            var bsInfos = _ap.GetPayByBillBalanceSheetHasNegativeAmount(model.DealGuid);
                            if (bsInfos.AsEnumerable().Any())
                            { 
                                BalanceSheetManager.ProductBalanceSheetWithNegativeAmountMailNotify(bsInfos);
                            }
                        }
                        catch (Exception)
                        {
                            updateFail = true;
                            message.Append(string.Format("Id:{0} 對帳單應付金額更新失敗，請洽技術部。\r\n", bsId));
                        }
                    }
                }
            }

            #endregion 更新對帳單金額

            return Json(new { Result = !updateFail, Message = message.ToString() });
        }

        #region  pcp_point_Transaction_manage

        public ActionResult PcpPointTransactionManage()
        {
            Dictionary<string, string> typeList = Enum.GetValues(typeof(ManageVendorSearchOption)).Cast<ManageVendorSearchOption>().Where(x => (int)x > 0 && (int)x < 3).ToDictionary(x => ((int)x).ToString(), x => Helper.GetEnumDescription(x));
            ViewBag.TypeList = new SelectList(typeList, "key", "value");

            return View();
        }

        [HttpPost]
        public JsonResult SendPcpTransactionOrder(string accountId, string transactionAmount, string regularsPoint, string favorPoint, string einvoiceMode, string favorPointStartTime, string favorPointExpireTime, string description, string invoiceTitle, string invoiceCompanyId, string invoiceName, string invoiceAddress)
        {
            //熟客點儲值動作
            var transactionOrder = new PcpPointTransactionOrder();
            transactionOrder.Guid = Guid.NewGuid();
            transactionOrder.TransactionAmount = int.Parse(transactionAmount);
            transactionOrder.RegularsPoint = int.Parse(regularsPoint);
            transactionOrder.FavorPoint = int.Parse(favorPoint);
            transactionOrder.OderStatus = (byte)PcpTransactionOrderStatus.Success;
            transactionOrder.EinvoiceMode = (int.Parse(einvoiceMode) == (int)InvoiceMode2.Duplicate) ? (byte)InvoiceMode2.Duplicate : (byte)InvoiceMode2.Triplicate;
            if (int.Parse(favorPoint) > 0)
            {
                DateTime favorpointstarttime, favorpointexpiretime;
                if (DateTime.TryParse(favorPointStartTime, out favorpointstarttime))
                {
                    transactionOrder.FavorPointStartTime = favorpointstarttime;
                }

                if (DateTime.TryParse(favorPointExpireTime, out favorpointexpiretime))
                {
                    transactionOrder.FavorPointExpireTime = favorpointexpiretime;
                }

                transactionOrder.Description = string.Format("儲值 {0} 元，增加 {1} 熟客點，增加 {2} 公關點", transactionAmount, regularsPoint, favorPoint);
            }
            else
            {
                transactionOrder.Description = string.Format("儲值 {0} 元，增加 {1} 熟客點", transactionAmount, regularsPoint);
            }

            transactionOrder.CreateId = MemberFacade.GetUniqueId(User.Identity.Name);
            transactionOrder.CreateTime = DateTime.Now;

            bool transactionResult = BonusFacade.PcpTransactionOrderSet(transactionOrder, accountId, invoiceTitle, invoiceCompanyId, invoiceName, invoiceAddress);
            string message = "SendPcpTransactionOrder Success";

            return Json(new
            {
                msg = message
            });

        }


        [HttpPost]
        public JsonResult SearchPcpTransactionOrderOverage(string accountId, string transactionOrderId)
        {
            //熟客儲值定單餘額檢查動作
            int transactionorderid;
            if (int.TryParse(transactionOrderId, out transactionorderid))
            {
                var overageTransactionOrder = BonusFacade.PcpTransactionOrderOverageGet(accountId, transactionorderid);

                //計算訂單退款金額10%手續費發票金額

                var concreteRefundAmount = default(double);
                var costOfProcedure = Math.Floor(((double)overageTransactionOrder.TransactionAmount * 0.1));

                concreteRefundAmount = concreteRefundAmount - costOfProcedure;

                return Json(new
                {
                    transactionOrderId = overageTransactionOrder.Id,
                    transactionAmount = overageTransactionOrder.TransactionAmount,
                    regularsPoint = overageTransactionOrder.RegularsPoint,
                    //favorPoint = overageTransactionOrder.FavorPoint,  //公關點不回收，故永遠為0
                    favorPoint = 0,
                    desription = overageTransactionOrder.Description,
                    msg = "SearchPcpTransactionOrderOverage",
                    concreterefundamount = concreteRefundAmount,
                    costofprocedure = costOfProcedure,
                    error = string.Empty
                });

            }
            return Json(new
            {
                msg = "SearchPcpTransactionOrderOverage",
                error = "查無此資料"
            });

        }


        [HttpPost]
        public JsonResult SendPcpTransactionRefund(string accountId, string transactionOrderId, string refundDescription)
        {
            //熟客點退款動作
            int transactionorderid;
            if (int.TryParse(transactionOrderId, out transactionorderid))
            {
                BonusFacade.GeneratePcpTransactionRefund(accountId, transactionorderid, refundDescription, MemberFacade.GetUniqueId(User.Identity.Name));
                return Json(new
                {
                    msg = "SendPcpTransactionRefund-Success",
                    error = ""
                });
            }

            return Json(new
            {
                msg = "SendPcpTransactionRefund-Fail",
                error = "查無此資料"
            });

        }

        [HttpPost]
        public JsonResult GetAccountSuperBonusExchangableAmount(string accountId)
        {
            //帳戶現有超級紅利金

            double totalSuperBonus = BonusFacade.TotalSuperBonusAmountGetByAccountId(accountId);

            //帳戶可兌換超級紅利金及請款金額

            KeyValuePair<double, int> superBonusResult = BonusFacade.ExchangeabledSuperBonusAmountGetByAccountId(accountId);

            return Json(new
            {
                totalsuperbonus = totalSuperBonus,
                exchangesuperbonus = superBonusResult.Key,
                exchangetransactionamount = superBonusResult.Value,
                msg = "GetAccountSuperBonusExchangableAmount",
                error = "查無此資料"
            });

        }

        [HttpPost]
        public JsonResult SellerTriplicateInvoiceInfoGet(string accountId, Guid sellerGuid)
        {
            var data = PcpTransactionUtility.GetSellerTriplicateInvoiceInfo(accountId, sellerGuid);
            return Json(new { result = 1, data = data });
        }

        [HttpPost]
        public JsonResult SendPcpTransactionExchangeOrder(string accountId, int exchangeSuperBonus, int exchangeTransactionAmount, string exchangeDescription)
        {
            //超級紅利金請款動作

            var exchangeOrder = new PcpPointTransactionExchangeOrder
            {
                Guid = Guid.NewGuid(),
                TransactionAmount = exchangeTransactionAmount,
                SuperBonus = exchangeSuperBonus,
                OrderStatus = (byte)PcpTransactionOrderStatus.Success,
                Description = string.Format("請款 {0} 元，使用 {1} 超級紅利金", exchangeTransactionAmount, exchangeSuperBonus) + exchangeDescription,
                CreateId = MemberFacade.GetUniqueId(User.Identity.Name),
                CreateTime = DateTime.Now
            };

            BonusFacade.GeneratePcpTransactionExchangeOrder(accountId, exchangeOrder, MemberFacade.GetUniqueId(User.Identity.Name));

            //紅利金歷程

            return Json(new
            {
                msg = "SendPcpTransactionExchangeOrder"
            });

        }

        [HttpPost]
        public JsonResult RegularsPointOrderDataGet(string accountId)
        {

            var data = PcpTransactionUtility.GetPcpTransactionOrderData(accountId, PcpPointType.RegularsPoint);
            return Json(new { result = 1, data = data.OrderByDescending(x => x.orderDate) });
        }

        [HttpPost]
        public JsonResult FavorPointOrderDataGet(string accountId)
        {

            var data = PcpTransactionUtility.GetPcpTransactionOrderData(accountId, PcpPointType.FavorPoint);
            return Json(new { result = 1, data = data.OrderByDescending(x => x.orderDate) });
        }

        [HttpPost]
        public JsonResult SuperBonusDataGet(string accountId)
        {
            var data = PcpTransactionUtility.GetSuperBonusData(accountId);
            return Json(new { result = 1, data = data.OrderByDescending(x => x.transactionDate) });
        }

        #endregion pcp_point_transaction_manage

        public ActionResult CreditcardFraudImport()
        {
            return View();

        }

        public ActionResult CreditcardFraudDataUpload(HttpPostedFileBase excel)
        {
            int k = 1;
            try
            {
                if (excel == null)
                {
                    TempData["message"] = "匯入失敗! 沒收到檔案.";
                    return RedirectToAction("CreditcardFraudImport");
                }

                if (Path.GetExtension(excel.FileName) != ".xls")
                {
                    TempData["message"] = "匯入失敗! 副檔名不對, 請使用 Excel 97 - 2003.";
                    return RedirectToAction("CreditcardFraudImport");
                }

                HSSFWorkbook workbook = null;
                try
                {
                    workbook = new HSSFWorkbook(excel.InputStream);
                }
                catch (IOException)
                {
                    TempData["message"] = "匯入失敗! 檔案開啟失敗, 請檢查檔案是否毀損!";
                    return RedirectToAction("CreditcardFraudImport");
                }

                //if (!CheckACHHeader(sheet))
                //{
                //    ViewBag.Message = "匯入失敗! 檔案表頭欄位不合!";
                //    //return View("DirectDepositUploadResult");
                //}

                Sheet sheet = workbook.GetSheetAt(0);
                Row row;

                Security sec = new Security(SymmetricCryptoServiceProvider.AES);
                CreditcardFraudDatumCollection cfdc = new CreditcardFraudDatumCollection();
                for (k = 1; k <= sheet.LastRowNum; k++)
                {
                    row = sheet.GetRow(k);

                    if (row == null)
                    {
                        continue;
                    }
                    CreditcardFraudDatum cfd = new CreditcardFraudDatum();

                    if (row.GetCell(0).NumericCellValue.ToString().Trim() == "0" && row.GetCell(1).StringCellValue.ToString().Trim() == string.Empty && 
                        row.GetCell(2).StringCellValue.ToString().Trim() == string.Empty && row.GetCell(3).StringCellValue.ToString().Trim()==string.Empty)
                    {
                        continue;
                    }
                    cfd.UserId = Convert.ToInt32(row.GetCell(0).NumericCellValue.ToString().Trim());

                    //正規化地址
                    string address = Helper.AddrConvertRegular(row.GetCell(1).StringCellValue.ToString().Trim());
                    cfd.Address = address;

                    cfd.Ip= row.GetCell(2).StringCellValue.ToString().Trim();
                    //cfd.CardNumber = row.GetCell(2).NumericCellValue.ToString("#");
                    //cfd.CardNumber = row.GetCell(2).StringCellValue.ToString();
                    if (row.GetCell(3).StringCellValue.ToString().Trim()==string.Empty)
                    {
                        cfd.CardNumber = string.Empty;
                    }
                    else
                    {
                        cfd.CardNumber = sec.Encrypt(row.GetCell(3).StringCellValue.ToString().Trim());
                    }
                    
                    cfd.CreateId = UserName;
                    cfd.CreateTime = DateTime.Now;
                    cfd.Type = (int)CreditcardFraudDataType.CustomerCare;


                    //確認有無已存在資料
                    CreditcardFraudDatum oldData = _op.CreditcardFraudDataGet(true, cfd.Ip, cfd.Address, cfd.CardNumber, null, ((int)CreditcardFraudDataType.CustomerCare).ToString());
                    //確認有無於匯入資料
                    int importData = cfdc.Where(x => x.Address == cfd.Address && x.Ip == cfd.Ip && x.CardNumber == cfd.CardNumber).Count();


                    if (!oldData.IsLoaded && importData == 0)
                    {
                        cfdc.Add(cfd);
                    }
                    
                }

                if (cfdc.Count>0)
                {
                    _op.CreditcardFraudDataBulkInsert(cfdc);
                }

                TempData["message"] = "匯入成功" + cfdc.Count.ToString() + "筆";
                return RedirectToAction("CreditcardFraudImport");
            }
            catch (Exception ex)
            {
                TempData["message"] = "第" + k.ToString() + "列" + ex.Message.Trim();
                return RedirectToAction("CreditcardFraudImport");
            }
            

        }

        public ActionResult CreditcardFraudManage()
        {
            CreditcardFraudDatum cfd = _op.CreditcardFraudDataGetById("1");
            CreditcardFraudDatum cfd2 = _op.CreditcardFraudDataGetById("2");
            ViewBag.Ip = cfd.Ip;
            ViewBag.Address = cfd.Address;
            ViewBag.UserId = cfd2.UserId;
            return View();
        }

        public ActionResult CreditcardFraudUpdate(string ip, string address)
        {
            CreditcardFraudDatum cfd = _op.CreditcardFraudDataGetById("1");
            cfd.Ip = ip;
            cfd.Address = Helper.AddrConvertRegular(address);
            _op.CreditcardFraudDataSet(cfd);
            return RedirectToAction("CreditcardFraudManage");

        }

        public ActionResult CreditcardFraudUpdateAccount(int userId)
        {
            CreditcardFraudDatum cfd = _op.CreditcardFraudDataGetById("2");
            cfd.UserId = userId;
            _op.CreditcardFraudDataSet(cfd);
            return RedirectToAction("CreditcardFraudManage");

        }
        
        public ActionResult DirectTrust(string endDate)
        {
            TrustVerificationReport last_rp = null;
            string errMsg = string.Empty;
            if (!string.IsNullOrEmpty(endDate))
            {
                DateTime d;
                last_rp = _mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin, TrustVerificationReportType.CouponAndCash, false);
                if (last_rp.Flag != (int)TrustFlag.SendReport)
                {
                    errMsg = "尚有信託未完成結算，請完成後再重新執行。";
                }
                else if (!DateTime.TryParse(endDate, out d))
                {
                    errMsg = "日期格式不正確。";
                }
                else if (d.Ticks <= last_rp.ReportIntervalEnd.Ticks)
                {
                    errMsg = "此結算日已完成信託，請重新選擇";
                }
                else if (d.Ticks >= DateTime.Now.Date.Ticks)
                {
                    errMsg = "結算日最大不能超過本日。";
                }
                else
                {
                    TrustVerificationReport rp = new TrustVerificationReport();
                    rp.TrustProvider = (int)TrustProvider.TaiShin;
                    rp.ReportDate = DateTime.Now;
                    rp.ReportGuid = Guid.Empty;
                    rp.AmountIncrease = rp.AmountDecrease = rp.TotalTrustAmount = rp.TotalCount = rp.OverOneYearTotal = 0;
                    rp.ReportType = (int)TrustVerificationReportType.CouponAndCash;
                    rp.UserId = UserName;
                    rp.ReportIntervalStart = last_rp.ReportIntervalEnd.AddDays(1).Date;
                    rp.ReportIntervalEnd = d.AddDays(1).AddSeconds(-1);
                    rp.Flag = (int)TrustFlag.Initial;
                    _mp.TrustVerificationReportSet(rp);
                }
            }
            last_rp = _mp.TrustVerificationReportGetLatest(TrustProvider.TaiShin, TrustVerificationReportType.CouponAndCash, false);
            ViewBag.Interval = last_rp.Flag < (int)TrustFlag.SendReport ? string.Format("{0}-{1}", last_rp.ReportIntervalStart.ToString("yyyy/MM/dd"), last_rp.ReportIntervalEnd.ToString("yyyy/MM/dd")) : string.Empty;
            ViewBag.MinDate = last_rp.ReportIntervalEnd.AddDays(1).Date.ToString("yyyy/MM/dd");
            ViewBag.ErrMsg = errMsg;
            return View();
        }

        #endregion Action

        #region Method

        private PagerList<DealPaymentInfo> GetPagerDealPaymentInfo(List<DealPaymentInfo> infos, int pageSize, int pageIndex, string sortCol, bool sortDesc)
        {
            var query = (from t in infos select t);

            if (sortDesc == false)
            {
                switch (sortCol)
                {
                    case DealPaymentInfo.Columns.DealOrderEndTime:
                        query = query.OrderBy(t => t.DealOrderEndTime);
                        break;

                    case DealPaymentInfo.Columns.SignCompanyName:
                        query = query.OrderBy(t => t.SignCompanyName);
                        break;

                    default:
                        break;
                }
            }
            else
            {
                switch (sortCol)
                {
                    case DealPaymentInfo.Columns.DealOrderEndTime:
                        query = query.OrderByDescending(t => t.DealOrderEndTime);
                        break;

                    case DealPaymentInfo.Columns.SignCompanyName:
                        query = query.OrderByDescending(t => t.SignCompanyName);
                        break;

                    default:
                        break;
                }
            }

            var paging = new PagerList<DealPaymentInfo>(
                query.Skip(pageIndex * pageSize).Take(pageSize).ToList(),
                new PagerOption(pageSize, pageIndex * pageSize, sortCol, sortDesc),
                pageIndex,
                query.ToList().Count);

            return paging;
        }

        private IEnumerable<ViewDealPropertyBusinessHourContent> GetDealPaymentInfoFromQueryRequest(ToHouseDealPaymentAuditModel model)
        {
            if (string.IsNullOrEmpty(model.QueryKeyWord) &&
                !model.QueryDateS.HasValue &&
                !model.QueryDateE.HasValue &&
                !model.HasNoBalanceSheetCreateDate &&
                !model.HasNoFinalBalanceSheetDate &&
                !model.HasNoPartiallyPaymentDate &&
                !model.HasVendorPaymentChange)
            {
                return new List<ViewDealPropertyBusinessHourContent>();
            }

            var columnNull = new List<string>();

            if (model.HasNoBalanceSheetCreateDate)
            {
                columnNull.Add(ViewDealPropertyBusinessHourContent.Columns.BalanceSheetCreateDate);
            }
            if (model.HasNoFinalBalanceSheetDate)
            {
                columnNull.Add(ViewDealPropertyBusinessHourContent.Columns.FinalBalanceSheetDate);
            }
            if (model.HasNoPartiallyPaymentDate)
            {
                model.RemittanceType = RemittanceType.ManualPartially;
                columnNull.Add(ViewDealPropertyBusinessHourContent.Columns.PartiallyPaymentDate);
            }

            var infos = _pp.ViewDealPropertyBusinessHourContentGetToHouseDealList(model.QueryOption, model.QueryKeyWord,
                    model.QueryDateOption, model.QueryDateS, model.QueryDateE,
                    model.RemittanceType, columnNull, model.HasVendorPaymentChange);

            return infos.Where(x => !Helper.IsFlagSet(x.BusinessHourStatus, BusinessHourStatus.ComboDealMain));
        }

        private bool ProcessUpdateDateOption(UpdateDateOption dateOption, Guid dealGuid, DateTime? date, string userId)
        {
            var da = _pp.DealAccountingGet(dealGuid);
            if (da == null || !da.IsLoaded) return false;

            SetDealAccounting(da, dateOption, date, userId);

            #region 更新多檔次母檔檢查

            var comboDeal = _pp.GetComboDeal(dealGuid);
            if (!comboDeal.IsLoaded) return true;
            var mda = _pp.DealAccountingGet(comboDeal.MainBusinessHourGuid.Value);
            if (mda == null || !mda.IsLoaded) return false;

            //多檔次子檔之對帳單開立日、退換貨完成日或暫付七成日 被清成空白
            //一併清除母檔資訊
            if (dateOption == UpdateDateOption.SetBalanceSheetCreateDateNull ||
                dateOption == UpdateDateOption.SetFinalBalanceSheetDateNull ||
                dateOption == UpdateDateOption.SetPartiallyPaymentDateNull)
            {
                return SetDealAccounting(mda, dateOption, date, userId);
            }

            //多檔次子檔 壓上對帳單開立日、退換貨完成日或暫付七成日
            //需檢查是否所有子檔皆已壓上 若是則一併更新母檔資訊
            int tempSlug;
            var subDeals = GetComboSubDealsInfo(new List<Guid> { dealGuid });
            var subDealInfos = _pp.ViewDealPropertyBusinessHourContentGetList(subDeals.Keys)
                                    .Where(x => int.TryParse(x.Slug, out tempSlug) &&
                                                tempSlug >= x.BusinessHourOrderMinimum);
            if (!subDealInfos.Any()) return true;

            var updateMainDeal = false;
            switch (dateOption)
            {
                case UpdateDateOption.SetBalanceSheetCreateDate:
                    if (subDealInfos.All(x => x.BalanceSheetCreateDate.HasValue))
                    {
                        updateMainDeal = true;
                    }
                    break;
                case UpdateDateOption.SetFinalBalanceSheetDate:
                    if (subDealInfos.All(x => x.FinalBalanceSheetDate.HasValue))
                    {
                        updateMainDeal = true;
                    }
                    break;
                case UpdateDateOption.SetPartiallyPaymentDate:
                    if (subDealInfos.All(x => x.PartiallyPaymentDate.HasValue ||
                                              (!x.PartiallyPaymentDate.HasValue &&
                                               x.GroupOrderStatus.HasValue &&
                                               Helper.IsFlagSet(x.GroupOrderStatus.Value, GroupOrderStatus.PartialFail))))
                    {
                        updateMainDeal = true;
                    }
                    break;
            }

            if (updateMainDeal)
            {
                return SetDealAccounting(mda, dateOption, date, userId);
            }

            #endregion 更新多檔次母檔檢查

            return true;
        }

        private static bool SetDealAccounting(DealAccounting da, UpdateDateOption dateOption, DateTime? date, string userId)
        {
            DateTime? originDate = null;

            try
            {
                switch (dateOption)
                {
                    case UpdateDateOption.SetBalanceSheetCreateDate:
                    case UpdateDateOption.SetBalanceSheetCreateDateNull:
                        originDate = da.BalanceSheetCreateDate;
                        da.BalanceSheetCreateDate = date;
                        break;
                    case UpdateDateOption.SetFinalBalanceSheetDate:
                    case UpdateDateOption.SetFinalBalanceSheetDateNull:
                        originDate = da.FinalBalanceSheetDate;
                        da.FinalBalanceSheetDate = date;
                        break;
                    case UpdateDateOption.SetPartiallyPaymentDate:
                    case UpdateDateOption.SetPartiallyPaymentDateNull:
                        originDate = da.PartiallyPaymentDate;
                        da.PartiallyPaymentDate = date;
                        break;
                }
                if (da.IsDirty)
                {
                    var actionDesc = string.Format("{0} : {1} -> {2}", dateOption, originDate, date);
                    _pp.DealAccountingSet(da);
                    CommonFacade.AddAudit(da.BusinessHourGuid, AuditType.DealAccounting, actionDesc, userId, true);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private IEnumerable<VendorPaymentChangeInfo> GetVendorPaymentChangeInfo(Guid dealGuid, int? paymentChangeId)
        {
            var infos = new List<VendorPaymentChangeInfo>();
            var paymentInfo = _ap.VendorPaymentChangeGetList(dealGuid).ToList();

            if (paymentChangeId.HasValue)
            {
                paymentInfo = paymentInfo.Where(x => x.Id == paymentChangeId.Value)
                                         .ToList();
            }
            var memberPromotionInfo = _mp.MemberPromotionDepositGetListByVendorPaymentChangeIds(paymentInfo.Select(x => x.Id))
                                            .Where(x => x.VendorPaymentChangeId.HasValue)
                                            .GroupBy(x => x.VendorPaymentChangeId)
                                            .ToDictionary(x => x.Key, x => x.ToList());

            var membernoneInfo = _mp.MemberNoneDepositGetListByVendorPaymentChangeIds(paymentInfo.Select(x => x.Id))
                                            .Where(x => x.VendorPaymentChangeId.HasValue)
                                            .GroupBy(x => x.VendorPaymentChangeId)
                                            .ToDictionary(x => x.Key, x => x.ToList());

            var orderInfo = _pp.ViewPponOrderGetList(dealGuid)
                                .ToDictionary(x => x.Guid, x => x.OrderId);

            var freezeInfo = _mp.GetCashTrustLogCollectionByOid(orderInfo.Select(x => x.Key))
                                .Where(x => Helper.IsFlagSet(x.SpecialStatus, (int)TrustSpecialStatus.Freeze))
                                .GroupBy(x => x.OrderGuid)
                                .ToDictionary(x => x.Key);

            var bsInfo = _ap.BalanceSheetGetListByIds(paymentInfo.Where(x => x.BalanceSheetId.HasValue)
                                                                  .Select(x => x.BalanceSheetId.Value)
                                                                  .ToList())
                            .ToDictionary(x => x.Id, x => x.IsConfirmedReadyToPay);

            foreach (var payment in paymentInfo)
            {
                var info = new VendorPaymentChangeInfo
                {
                    PaymentChangeId = payment.Id,
                    Reason = payment.Reason,
                    Amount = payment.Amount,
                    PromotionValue = payment.PromotionValue,
                    AllowanceAmount = payment.AllowanceAmount,
                    OrderCount = 1,
                    CreateId = payment.CreateId,
                    CreateTime = payment.CreateTime,
                    BusinessHourId = payment.BusinessHourGuid,
                    BalanceSheetId = payment.BalanceSheetId,
                    IsEditable = !payment.BalanceSheetId.HasValue || (!bsInfo.ContainsKey(payment.BalanceSheetId.Value) || !bsInfo[payment.BalanceSheetId.Value])
                };
                if (payment.PromotionValue > 0)
                {
                    if (memberPromotionInfo.ContainsKey(payment.Id))
                    {
                        var promotionInfo = memberPromotionInfo[payment.Id].First();
                        info.Action = promotionInfo.Action;
                        info.StartTime = promotionInfo.StartTime;
                        info.ExpireTime = promotionInfo.ExpireTime;
                        info.OrderCount = memberPromotionInfo[payment.Id].Count;
                        info.OrderIdInfo = string.Join("<br/>", orderInfo
                                                            .Where(x => memberPromotionInfo[payment.Id].Select(o => o.OrderGuid).Contains(x.Key))
                                                            .Select(x => x.Value));
                        info.IsFreeze = promotionInfo.OrderGuid.HasValue && freezeInfo.ContainsKey(promotionInfo.OrderGuid.Value);
                    }
                }
                else
                {
                    if (membernoneInfo.ContainsKey(payment.Id))
                    {
                        var noneInfo = membernoneInfo[payment.Id].First();
                        info.OrderIdInfo = string.Join("<br/>", orderInfo
                                                                .Where(x => membernoneInfo[payment.Id].Select(o => o.OrderGuid).Contains(x.Key))
                                                                .Select(x => x.Value));
                        info.IsFreeze = noneInfo.OrderGuid.HasValue && freezeInfo.ContainsKey(noneInfo.OrderGuid.Value);
                    }

                }
                infos.Add(info);
            }

            return infos.OrderByDescending(x => x.CreateTime); ;
        }

        private static bool GetIsLumpSumBalanceSheetCreate(Guid dealGuid)
        {
            return _ap.BalanceSheetGetListByProductGuid(dealGuid)
                      .Any(x => x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet);
        }

        private static List<string> SplitOrderIdInfo(string orderIdInfo)
        {
            return orderIdInfo.Trim().Replace("/t", "").Split(";").ToList();
        }

        private ToHouseDealPaymentManageModel GetToHouseDealPaymentManageModel(Guid dealGuid, int? paymentChangeId, ToHouseDealPaymentManageModel model)
        {
            model.IsAllowanceAmountEdit = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name,
                                                                                SystemFunctionType.SetRelatedFinanceDateAndAllowanceAmount);
            model.IsVendorPaymentChangeEdit = CommonFacade.IsInSystemFunctionPrivilege(User.Identity.Name,
                                                                                    SystemFunctionType.SetVendorPaymentChangeWithOutAllowanceAmount);
            model.VendorPaymentChangeInfos = GetVendorPaymentChangeInfo(dealGuid, paymentChangeId);
            model.BalanceSheetInfos = GetBalanceSheetInfos(new List<Guid> { dealGuid });
            model.IsBalanceSheetCreate = GetIsLumpSumBalanceSheetCreate(dealGuid) || model.BalanceSheetInfos.Any();
            var lastBs = model.BalanceSheetInfos.FirstOrDefault();
            model.IsInBalanceTimeRange = lastBs == null || BalanceSheetManager.IsInBalanceTimeRange(dealGuid, lastBs.IntervalEnd, true);

            return model;
        }

        private static bool IsPartiallyPaymentDateNeeded(RemittanceType remittanceType, int groupOrderStatus)
        {
            return remittanceType == RemittanceType.ManualPartially &&
                   !Helper.IsFlagSet(groupOrderStatus, GroupOrderStatus.PartialFail);
        }

        private MemoryStream ToHouseDealPaymentExport(IEnumerable<DealPaymentInfo> paymentInfos, string sortCol, bool sortDesc)
        {
            Workbook workbook = new HSSFWorkbook();

            Sheet sheet = workbook.CreateSheet();
            CellStyle cellNStyle = workbook.CreateCellStyle();
            cellNStyle.DataFormat = HSSFDataFormat.GetBuiltinFormat("0");

            #region 表頭 及 title

            Row cols = sheet.CreateRow(0);

            cols.CreateCell(0).SetCellValue("開檔日期");
            cols.CreateCell(1).SetCellValue("結檔日期");
            cols.CreateCell(2).SetCellValue("業務");
            cols.CreateCell(3).SetCellValue("檔名");
            cols.CreateCell(4).SetCellValue("會計檔號");
            cols.CreateCell(5).SetCellValue("是否成檔");
            cols.CreateCell(6).SetCellValue("母檔出貨回覆日");
            cols.CreateCell(7).SetCellValue("暫付七成金額概算");
            cols.CreateCell(8).SetCellValue("結檔份數");
            cols.CreateCell(9).SetCellValue("進貨價");
            cols.CreateCell(10).SetCellValue("暫付七成付款日");
            cols.CreateCell(11).SetCellValue("對帳單開立日");
            cols.CreateCell(12).SetCellValue("廠商mail");
            cols.CreateCell(13).SetCellValue("付款方式");
            cols.CreateCell(14).SetCellValue("退換貨處理完成日");
            cols.CreateCell(15).SetCellValue("變動金額");
            cols.CreateCell(16).SetCellValue("簽約公司");
            cols.CreateCell(17).SetCellValue("簽約統編");
            cols.CreateCell(18).SetCellValue("負責人");
            cols.CreateCell(19).SetCellValue("匯款戶名");
            cols.CreateCell(20).SetCellValue("受款ID");
            cols.CreateCell(21).SetCellValue("匯款帳號");
            cols.CreateCell(22).SetCellValue("銀行代號");
            cols.CreateCell(23).SetCellValue("廠商發票狀況");
            cols.CreateCell(24).SetCellValue("尾款付款日");
            cols.CreateCell(25).SetCellValue("發票號碼");
            cols.CreateCell(26).SetCellValue("發票日期");
            cols.CreateCell(27).SetCellValue("開立統編");
            cols.CreateCell(28).SetCellValue("未稅金額");
            cols.CreateCell(29).SetCellValue("稅額");
            cols.CreateCell(30).SetCellValue("總計");
            cols.CreateCell(31).SetCellValue("請款金額");

            #endregion 表頭 及 title

            #region 報表內容

            switch (sortCol)
            {
                case DealPaymentInfo.Columns.DealOrderEndTime:
                    paymentInfos = (sortDesc == false)
                                    ? paymentInfos.OrderBy(t => t.DealOrderEndTime)
                                    : paymentInfos.OrderByDescending(t => t.DealOrderEndTime);
                    break;

                case DealPaymentInfo.Columns.SignCompanyName:
                    paymentInfos = (sortDesc == false)
                                    ? paymentInfos.OrderBy(t => t.SignCompanyName)
                                    : paymentInfos.OrderByDescending(t => t.SignCompanyName);
                    break;

                default:
                    break;
            }
            var tempDate = new DateTime();
            var dealGuids = paymentInfos.Select(x => x.DealGuid);
            var partialPaymentDealGuids = paymentInfos.Where(x => DateTime.TryParse(x.PartiallyPaymentDate, out tempDate) ||
                                                                  x.BalanceSheetCreateDate.HasValue)
                                                      .Select(x => x.DealGuid);
            var bsModels = GetBalanceSheetsDeliverModels(partialPaymentDealGuids)
                            .ToDictionary(x => x.DealId, x => x);
            //撈取 多檔次子檔對應之母檔 資訊 
            var subDeals = GetComboSubDealsInfo(dealGuids);
            int rowIdx = 1;
            foreach (var item in paymentInfos)
            {
                var mainDealGuid = subDeals.ContainsKey(item.DealGuid)
                                       ? subDeals[item.DealGuid]
                                       : item.DealGuid;
                var bsModel = bsModels.ContainsKey(mainDealGuid)
                                ? bsModels[mainDealGuid]
                                : null;
                var subBsModel = bsModel == null
                                ? null
                                : bsModels[mainDealGuid].DealInfos
                                    .FirstOrDefault(x => x.DealGuid == item.DealGuid);

                Row row = sheet.CreateRow(rowIdx);
                row.CreateCell(0).SetCellValue(string.Format("{0:yyyy/MM/dd}", item.DealOrderStartTime));
                row.CreateCell(1).SetCellValue(string.Format("{0:yyyy/MM/dd}", item.DealOrderEndTime));
                row.CreateCell(2).SetCellValue(item.SalesName);
                row.CreateCell(3).SetCellValue(item.DealName);
                row.CreateCell(4).SetCellValue(item.DealId);
                row.CreateCell(5).SetCellValue(DateTime.Compare(DateTime.Now, item.DealOrderEndTime) > 0 ? item.IsDealEstablished ? "v" : "x" : "未結檔");
                row.CreateCell(6).SetCellValue(item.ShippedDate.HasValue ? string.Format("{0:yyyy/MM/dd}", item.ShippedDate) : string.Empty);
                row.CreateCell(7).SetCellValue(subBsModel == null ? string.Empty : subBsModel.PartiallyPaymentAmount.ToString());
                row.GetCell(7).CellStyle = cellNStyle;
                row.CreateCell(8).SetCellValue(subBsModel == null ? string.Empty : subBsModel.DealResultCount.ToString());
                row.CreateCell(9).SetCellValue(subBsModel == null ? string.Empty : subBsModel.DealCost.ToString());
                row.CreateCell(10).SetCellValue(item.PartiallyPaymentDate);
                row.CreateCell(11).SetCellValue(item.BalanceSheetCreateDate.HasValue ? string.Format("{0:yyyy/MM/dd}", item.BalanceSheetCreateDate) : string.Empty);
                row.CreateCell(12).SetCellValue(item.CompanyEmail);
                row.CreateCell(13).SetCellValue(Helper.GetLocalizedEnum(item.RemittanceType));
                row.CreateCell(14).SetCellValue(item.FinalBalanceSheetDate.HasValue ? string.Format("{0:yyyy/MM/dd}", item.FinalBalanceSheetDate) : string.Empty);
                row.CreateCell(15).SetCellValue(item.VendorPaymentChangeAmountDesc);
                row.CreateCell(16).SetCellValue(item.SignCompanyName);
                row.CreateCell(17).SetCellValue(item.SignCompanyId);
                row.CreateCell(18).SetCellValue(item.CompanyBossName);
                row.CreateCell(19).SetCellValue(item.AccountName);
                row.CreateCell(20).SetCellValue(item.AccountId);
                row.CreateCell(21).SetCellValue(item.AccountNo);
                row.CreateCell(22).SetCellValue(string.Format("{0}{1}", item.BankCode, item.BranchCode));
                row.CreateCell(23).SetCellValue(!item.IsTax ? "免稅" : "應稅");
                //尾款付款日:收到發票日的下一個禮拜五
                var remainAmountPaymentDate = item.BillGetDate.HasValue
                                                ? string.Format("{0:yyyy/MM/dd}", item.BillGetDate.Value.AddDays(
                                                    Convert.ToDouble((0 - Convert.ToInt16(item.BillGetDate.Value.DayOfWeek)) + 5 + 7)))
                                                : string.Empty;
                row.CreateCell(24).SetCellValue(remainAmountPaymentDate);
                row.CreateCell(25).SetCellValue(item.BillNumber);
                row.CreateCell(26).SetCellValue(item.BillGetDate.HasValue ? string.Format("{0:yyyy/MM/dd}", item.BillGetDate) : string.Empty);
                row.CreateCell(27).SetCellValue(item.BillCompanyId);
                var totalAmount = subBsModel == null
                                      ? 0
                                      : subBsModel.DealTotalAmount + subBsModel.FareAmount + subBsModel.DebitCaseAmount;
                var unTaxAmount = item.IsTax
                                ? Convert.ToInt32(Math.Round(totalAmount / (1 + _config.BusinessTax), 0))
                                : totalAmount;
                row.CreateCell(28).SetCellValue(!item.HasBalanceSheet || subBsModel == null ? string.Empty : unTaxAmount.ToString());
                row.CreateCell(29).SetCellValue(!item.HasBalanceSheet || subBsModel == null ? string.Empty : (totalAmount - unTaxAmount).ToString());
                row.CreateCell(30).SetCellValue(!item.HasBalanceSheet || subBsModel == null ? string.Empty : totalAmount.ToString());
                row.CreateCell(31).SetCellValue(!item.HasBalanceSheet || subBsModel == null ? string.Empty : subBsModel.DealTotalFinalAmount.ToString());

                #region adjust column width

                var columns = sheet.GetRow(rowIdx).GetCellEnumerator();
                while (columns.MoveNext())
                {
                    HSSFCell column = (HSSFCell)columns.Current;
                    int columnWidth = sheet.GetColumnWidth(column.ColumnIndex) / 256;
                    //計算顯示長度(若有斷行 抓取最長長度) 英數字長度*1 中文字長度*2
                    int length = column.ToString().Split("\n").Select(x => Encoding.Default.GetBytes(x))
                                                              .Select(x => x.Length)
                                                              .Concat(new[] { 0 })
                                                              .Max() + 1;

                    if (columnWidth < length)
                    {
                        columnWidth = length;
                    }
                    sheet.SetColumnWidth(column.ColumnIndex, columnWidth * 256);

                    if (!column.CellStyle.VerticalAlignment.Equals(VerticalAlignment.CENTER))
                    {
                        column.CellStyle.VerticalAlignment = VerticalAlignment.CENTER;
                    }
                }

                #endregion  adjust column width

                rowIdx++;
            }

            #endregion 報表內容

            MemoryStream outputStream = new MemoryStream(32768);
            workbook.Write(outputStream);
            outputStream.Seek(0, SeekOrigin.Begin);
            return outputStream;
        }

        private Dictionary<Guid, int> GetHasLumpSumBalanceSheetInfo(IEnumerable<Guid> dealGuids)
        {
            return _ap.ViewBalanceSheetListGetList(dealGuids)
                        .GroupBy(x => x.ProductGuid)
                        .ToDictionary(x => x.Key,
                                      x => x.Count(b => b.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet));
        }

        private IEnumerable<BalanceSheetsDeliverModel> GetBalanceSheetsDeliverModels(IEnumerable<Guid> dealGuids)
        {
            var models = new List<BalanceSheetsDeliverModel>();
            if (!dealGuids.Any())
            {
                return models;
            }
            //撈取 多檔次子檔對應之母檔 資訊 
            var subDeals = GetComboSubDealsInfo(dealGuids);
            //多檔次母/子檔 及 非多檔次 bid
            var allDealGuids = subDeals.Select(x => x.Key).ToList();
            //撈取 母檔及非多檔次 bid
            //非多檔次
            var mainDealGuids = dealGuids.Where(x => !subDeals.ContainsKey(x)).ToList();
            allDealGuids.AddRange(mainDealGuids);
            foreach (var main in subDeals.Values)
            {
                //母檔
                if (!mainDealGuids.Contains(main))
                {
                    mainDealGuids.Add(main);
                }
                if (!allDealGuids.Contains(main))
                {
                    allDealGuids.Add(main);
                }
            }
            var dealInfos = _pp.ViewDealPropertyBusinessHourContentGetList(allDealGuids)
                                  .ToDictionary(x => x.BusinessHourGuid, x => x);

            var bsDetailInfos = _ap.ViewBalanceSheetListGetList(allDealGuids)
                                    .Where(x => x.ProductType == (int)BusinessModel.Ppon &&
                                                x.GenerationFrequency == (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet)
                                    .GroupBy(x => x.ProductGuid)
                                    .ToDictionary(x => x.Key, x => x.Count());

            var deliverSubInfo = (from x in _pp.BalanceSheetDeliverSubInfoGetTable(allDealGuids).AsEnumerable()
                                  select new
                                  {
                                      DealGuid = x.Field<Guid>("GUID"),
                                      FreightAmount = x.Field<int>("fareAmount"),
                                      IsTax = x.Field<bool>("is_tax"),
                                      PartialPaymentDate = x.Field<DateTime?>("partially_payment_date"),
                                      RemittanceType = x.Field<int>("remittance_type"),
                                      TotalCount = x.Field<int>("totalCount"),
                                      DebitCaseAmount = x.Field<int?>("debitCaseAmount") ?? 0,
                                      DebitAllowanceAmount = x.Field<int?>("debitAllowanceAmount") ?? 0,
                                      BalanceCreateDate = x.Field<DateTime?>("balance_sheet_create_date"),
                                      DealCost = Convert.ToInt32(x.Field<decimal>("cost")),
                                      GroupOrderStatus = x.Field<int>("status")
                                  })
                                 .ToLookup(x => x.DealGuid);
            foreach (var main in mainDealGuids)
            {
                if (!dealInfos.ContainsKey(main))
                {
                    continue;
                }
                int tempSlug;
                var model = new BalanceSheetsDeliverModel();
                var infos = new List<BalanceSheetsDeliverInfo>();
                var deals = subDeals.Values.Contains(main)
                            ? subDeals.Where(x => x.Value == main &&
                                                  dealInfos.ContainsKey(x.Key) &&
                                                  int.TryParse(dealInfos[x.Key].Slug, out tempSlug) &&
                                                  tempSlug >= dealInfos[x.Key].BusinessHourOrderMinimum)
                                      .Select(x => x.Key)
                                      .ToList()
                            : new List<Guid> { main };
                foreach (var item in deals)
                {
                    if (!dealInfos.ContainsKey(item) ||
                        !deliverSubInfo.Contains(item))
                    {
                        continue;
                    }
                    var deal = dealInfos[item];
                    var tmp = deliverSubInfo[item].First();
                    var result = new BalanceSheetsDeliverInfo
                    {
                        DealGuid = item,
                        DealName = deal.CouponUsage,
                        //出貨份數: balance_sheet_detail count - cash_trust_log special_status & 16 > 0 count
                        DealCount = tmp.TotalCount,
                        //結檔份數
                        DealResultCount = string.IsNullOrEmpty(deal.Slug)
                                            ? 0
                                            : int.Parse(deal.Slug),
                        DealCost = Convert.ToInt32(tmp.DealCost),
                        DealUniqueId = deal.UniqueId,
                        FareAmount = Convert.ToInt32(tmp.FreightAmount),
                        IsTax = tmp.IsTax,
                        DebitCaseAmount = tmp.DebitCaseAmount,
                        DebitAllowanceAmount = tmp.DebitAllowanceAmount,
                        DealRemittanceType = tmp.RemittanceType,
                        PartiallyPaymentDate = tmp.PartialPaymentDate,
                        BalanceCreatDate = tmp.BalanceCreateDate,
                        PartialFail = Helper.IsFlagSet(tmp.GroupOrderStatus, GroupOrderStatus.PartialFail)
                    };

                    result.DealTotalAmount = result.DealCount * result.DealCost;

                    //區分暫付七成或其他
                    if (result.DealRemittanceType.Equals((int)RemittanceType.ManualPartially))
                    {
                        //區分異常或正常
                        var desc = result.PartialFail
                                    ? "退貨超過三成"
                                    : string.Format("{0:yyyy/MM/dd}", result.PartiallyPaymentDate);
                        result.DescPartialPaymentDate = string.Format("[{0}]{1}", result.DealUniqueId, desc);
                        result.PartiallyPaymentAmount = result.PartialFail
                            ? 0
                            : Convert.ToInt32(Math.Ceiling(Convert.ToDouble(tmp.DealCost * result.DealResultCount * 0.7)));
                    }
                    else if (result.DealRemittanceType.Equals((int)RemittanceType.Others))
                    {
                        result.DescPartialPaymentDate = string.Format("[{0}]無暫付", result.DealUniqueId);
                        result.PartiallyPaymentAmount = 0;
                    }

                    result.DealTotalFinalAmount = result.DealTotalAmount - result.PartiallyPaymentAmount +
                                                  result.DebitCaseAmount + result.FareAmount;

                    infos.Add(result);
                }

                model.DealInfos = infos.OrderBy(x => x.DealUniqueId);
                model.DealId = dealInfos[main].BusinessHourGuid;
                model.DealUniqueId = dealInfos[main].UniqueId;
                model.DealName = dealInfos[main].CouponUsage;
                model.DeliverStartDate = string.Format("{0:yyyy/MM/dd}", dealInfos[main].BusinessHourDeliverTimeS);
                model.DeliverEndDate = string.Format("{0:yyyy/MM/dd}", dealInfos[main].BusinessHourDeliverTimeE);
                model.IsTax = dealInfos[main].IsTax;
                model.mVendorReceiptType = dealInfos[main].VendorReceiptType;
                model.DealRemittanceType = dealInfos[main].RemittanceType;
                model.mIsBalanceCreaDate = dealInfos[main].BalanceSheetCreateDate.HasValue &&
                                           model.DealInfos.All(x => x.BalanceCreatDate.HasValue &&
                                                                    bsDetailInfos.ContainsKey(x.DealGuid) &&
                                                                    bsDetailInfos[x.DealGuid] > 0);

                //DebitCaseAmount       對帳單扣除金額 allowance_amount=0 就用amount
                //DebitAllowanceAmount  發票扣除金額  只看allowance_amount
                model.DebitCaseAmount = model.DealInfos.Sum(x => x.DebitCaseAmount);
                model.DebitAllowanceAmount = model.DealInfos.Sum(x => x.DebitAllowanceAmount);
                model.DealTotalAmount = model.DealInfos.Sum(x => x.DealTotalAmount);
                model.DealPartialAmount = model.DealInfos.Sum(x => x.PartiallyPaymentAmount);
                model.DealFareAmount = model.DealInfos.Sum(x => x.FareAmount);
                model.DealChineseFareAmount = CommonFacade.GetFullChineseAmountString(model.DealFareAmount);
                model.DealTotalFinalAmount = model.DealInfos.Sum(x => x.DealTotalFinalAmount);
                //稅
                //應稅 或 單據開立方式為其他或收據 則運費一起算
                //免稅將運費分開
                decimal taxRate = 1 + _config.BusinessTax;
                model.DealTotalAmountWithFare = (model.IsTax ||
                                                 model.mVendorReceiptType == (int)VendorReceiptType.Other ||
                                                 model.mVendorReceiptType == (int)VendorReceiptType.Receipt)
                                                ? (model.DealTotalAmount + model.DealFareAmount + model.DebitAllowanceAmount)
                                                : (model.DealTotalAmount + model.DebitAllowanceAmount);
                if (model.IsTax)
                {
                    model.mDealTaxAmount = Convert.ToInt32(Math.Round(model.DealTotalAmountWithFare / taxRate, 0));
                }
                else
                {
                    model.mDealTaxAmount = 0;
                    model.mDealFareTaxAmount = Convert.ToInt32(Math.Round(model.DealFareAmount / taxRate, 0));
                }
                model.DealChineseTotalAmount = CommonFacade.GetFullChineseAmountString(model.DealTotalAmountWithFare);

                var transferInfo = new TransferInfo
                {
                    ReceiverTitle = dealInfos[main].CompanyAccountName,
                    ReceiverCompanyId = dealInfos[main].CompanyID,
                    ReceiverAccountBankNo = dealInfos[main].CompanyBankCode,
                    ReceiverAccountBranchNo = dealInfos[main].CompanyBranchCode,
                    ReceiverAccountNo = dealInfos[main].CompanyAccount,
                    ReceiverBossName = dealInfos[main].CompanyBossName
                };
                model.TransferInfo = transferInfo;

                models.Add(model);
            }

            return models;
        }

        private Dictionary<Guid, Guid> GetComboSubDealsInfo(IEnumerable<Guid> dealGuids)
        {
            return _pp.GetComboDealByBid(dealGuids)
                        .Where(x => x.BusinessHourGuid != x.MainBusinessHourGuid)
                        .GroupBy(x => new { x.BusinessHourGuid, x.MainBusinessHourGuid })
                        .ToDictionary(x => x.Key.BusinessHourGuid, x => x.Key.MainBusinessHourGuid.Value);
        }

        private IEnumerable<BalanceSheetInfo> GetBalanceSheetInfos(IEnumerable<Guid> dealGuids)
        {
            return _ap.ViewBalanceSheetListGetList(dealGuids)
                        .Select(b => new BalanceSheetInfo
                        {
                            ProductGuid = b.ProductGuid,
                            BalanceSheeetId = b.Id,
                            BalanceSheetFrequencyDesc = GetBalanceSheetFrequenyDesc(b.GenerationFrequency, string.Format("{0}/{1}", b.Year, b.Month), b.IntervalEnd),
                            IntervalEnd = b.IntervalEnd,
                            IsConfirmedReadyToPay = (b.IsConfirmedReadyToPay && b.EstAmount != 0)
                        })
                        .OrderByDescending(x => x.IntervalEnd);
        }

        private string GetBalanceSheetFrequenyDesc(int generationFrequency, string yearMonth, DateTime intervalEnd)
        {
            switch ((BalanceSheetGenerationFrequency)generationFrequency)
            {
                case BalanceSheetGenerationFrequency.MonthBalanceSheet:
                    return yearMonth;
                case BalanceSheetGenerationFrequency.WeekBalanceSheet:
                case BalanceSheetGenerationFrequency.FortnightlyBalanceSheet:
                    return string.Format("{0:yyyy/MM/dd}", intervalEnd.AddDays(-1));
                default:
                    return string.Format("{0:yyyy/MM/dd}", intervalEnd);
            }
        }

        #endregion Method
    }
}

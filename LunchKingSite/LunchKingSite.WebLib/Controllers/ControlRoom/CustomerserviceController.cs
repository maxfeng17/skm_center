﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Web;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core.Enumeration;
using System.Net.Mail;

namespace LunchKingSite.WebLib.Controllers
{
    public class CustomerserviceController : Controller
    {
        [Authorize, HttpPost]
        public JsonResult Upload()
        {

            var result = new ApiResult() { Code = ApiResultCode.Success };

            string serviceNo = Request.Form["serviceNo"];
            string content = Request.Form["customerTxtArea"];
            int method = Request.Form["method"] == "" ? 0 : int.Parse(Request.Form["method"]);
            HttpPostedFileBase images = Request.Files["images"];
            string mailTo = Request.Form["mailTo"];
            string mobileTo = Request.Form["mobileTo"];

            var data = CustomerServiceFacade.ReplyCustomer(User.Identity.Name, serviceNo, content, method, images, mailTo, mobileTo);

            if (string.IsNullOrEmpty(mailTo))
            {
                result.Code = ApiResultCode.DataNotFound;
                result.Message = "消費者無填寫Email,寄信通知失敗";
            }
                
            result.Data = data;

            return Json(result);
        }

    }
}

﻿using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;
using log4net;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.GameEntities;
using System.Collections.Generic;
using System;
using LunchKingSite.WebLib.Models.Game;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.Game;
using LunchKingSite.DataOrm;
using System.Web;
using System.IO;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class GameRoomController : ControllerExt
    {
        private readonly ILog logger = LogManager.GetLogger(typeof(ControllerExt));
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IGameProvider gp = ProviderFactory.Instance().GetDefaultProvider<IGameProvider>();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();

        public ActionResult Home()
        {
            List<GameCampaign> campaign = gp.GetAllGameCampaign();
          

            return View(campaign);
        }

        public ActionResult Edit(int campaignId, int tabIndex = 0 )
        {
            var campaign = gp.GetGameCampaign(campaignId);
            if (campaign == null)
            {
                throw new Exception("目前沒有可編輯的遊戲活動");
            }
            List<GameActivity> activities;
            List<List<GameGame>> activityGames;
            
            if (gp.GetGameActivitiesAndGamesByCampaignId(campaignId, out activities, out activityGames)==false)
            {
                throw new Exception("無法取得編輯資料，或資料有誤。");
            }

            return View(new GameEditViewModel
            {
                Campaign = campaign,
                Activities = activities,
                ActivityGames = activityGames,
                TabIndex = tabIndex,
                GivenBouns = mp.MemberBounsGetGivenTotal(campaign.Guid),
                UsedBonus = mp.MemberBounsGetUsedTotal(campaign.Guid)
            });
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult UpdateCampaign(UpdateCampaignModel data)
        {
            string message;
            if (data.CheckValid(out message)==false)
            {
                return Json(new
                {
                    Message = message,
                    Success = 0
                });
            }
            try
            {
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                if (data.ImageFile != null)
                {
                    //新上傳
                    var file = data.ImageFile;

                    fileExtension = Path.GetExtension(file.FileName);
                    fileName = config.MediaBaseUrl + "Game/" + data.Name + fileExtension;
                    ImageUtility.UploadFile(file.ToAdapter(), UploadFileType.PponEvent, "Game", data.Name);

                }
                else if (!string.IsNullOrEmpty(data.Image))
                {
                    //舊圖不改
                    fileName = data.Image;
                }
                data.Image = fileName;
                GameFacade.UpdateGameCampaign(data, User.Identity.Name);
                return Json(new
                {
                    FileName= fileName + "?ver=" + DateTime.Now.Ticks,
                    Success = 1
                });
            } catch (Exception ex)
            {
                logger.Warn("UpdateCampaign error", ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult UpdateActivity(UpdateActivityModel data)
        {
            string message;
            if (data.CheckValid(out message) == false)
            {
                return Json(new
                {
                    Message = message,
                    Success = 0
                });
            }
            try
            {
                GameFacade.UpdateGameActivity(data, User.Identity.Name);
                return Json(new
                {
                    Success = 1
                });
            } catch (Exception ex)
            {
                logger.Warn("UpdateActivity error", ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult SyncActivity(int activityId)
        {
            string message;
          
            try
            {
                GameActivity activity = gp.GetGameActivity(activityId);
                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(activity.ReferenceId);
                activity.Title = vpd.ItemName;
                activity.SubTitle = vpd.EventTitle;
                activity.Image = ImageFacade.GetDealAppPic(vpd, false);
                activity.Image2 = ImageFacade.GetDealPic(vpd);
                
                gp.SaveGameActivity(activity, UserName);
                return Json(new
                {
                    Success = 1
                });
            }
            catch (Exception ex)
            {
                logger.Warn("SyncActivity error", ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

        [HttpPost]
        public ActionResult Update(int campaignId)
        {
            return RedirectToAction("Edit", new { campaignId });
        }

        public ActionResult Add()
        {
            return View();
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult AddCampaign(AddCampaignModel data)
        {

            string message;
            if (data.CheckValid(out message) == false)
            {
                return Json(new
                {
                    Message = message,
                    Success = 0
                });
            }
            try
            {
                string fileName = string.Empty;
                string fileExtension = string.Empty;
                if (data.ImageFile != null)
                {
                    var file = data.ImageFile;

                    fileExtension = Path.GetExtension(file.FileName);
                    fileName = config.MediaBaseUrl + "Game/" + data.Name + fileExtension;
                    ImageUtility.UploadFile(file.ToAdapter(), UploadFileType.PponEvent, "Game", data.Name);

                }

                GameCampaign campaign = new GameCampaign
                {
                    Name = data.Name,
                    Title = data.Title,
                    BonusTotal = data.BonusTotal,
                    EnableBuddyReward = data.EnableBuddyReward,
                    StartTime = Convert.ToDateTime(data.StartTime),
                    EndTime = Convert.ToDateTime(data.EndTime),
                    Image = fileName
                };
                GameFacade.SaveGameCampaign(campaign, User.Identity.Name);

                return Json(new
                {
                    Success = 1
                });
            }
            catch (Exception ex)
            {
                logger.Warn("AddCampaign error", ex);
                return Json(new
                {
                    Message = "新增資料發生問題",
                    Success = 0
                });
            }
        }

        public ActionResult ErrorHandler(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult CheckImportDeal(int campaignId, List<string> dealIds)
        {
            string message;
            
            try
            {
                string errMessage = string.Empty;
                List<Guid> importedBids = new List<Guid>();
                GameCampaign campaign = gp.GetGameCampaign(campaignId);                    
                foreach (string data in dealIds)
                {
                    Guid bid = Guid.Empty;
                    Guid.TryParse(data, out bid);                   

                    if (bid == Guid.Empty)
                    {
                        errMessage += data + " 格式有誤<br />";
                    }

                    BusinessHour bh = pp.BusinessHourGet(bid);
                    if (bh.IsLoaded == false)
                    {
                        errMessage += data + " 找無該檔次<br />";
                    }

                    DealProperty dp = pp.DealPropertyGet(bh.Guid);
                    if (dp.IsGame == false)
                    {
                        errMessage += data + " 僅遊戲檔可加入<br />";
                    }
                    if (bh.BusinessHourOrderTimeS > DateTime.Now || bh.BusinessHourOrderTimeE < campaign.EndTime)
                    {
                        errMessage += data + " 僅活動區間內可加入<br />";
                    }
                    if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.NotShowDealDetailOnWeb) == false)
                    {
                        errMessage += data + " 僅隱藏檔才可以加入<br />";
                    }
                    if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                    {
                        errMessage += data + " 僅母檔或單檔可加入<br>";
                    }
                    if (importedBids.Contains(bh.Guid))
                    {
                        errMessage += data + " 該檔重複出現";
                    }

                    GameActivity ga = gp.GetGameActivityBycampaignId(bh.Guid, campaignId);
                    if (ga != null)
                    {
                        errMessage += data + " 該檔已加入";
                    }

                    bool isMainDeal = Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain);

                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(bh.Guid);
                    string image = ImageFacade.GetDealAppPic(vpd, false);
                    string image2 = ImageFacade.GetDealPic(vpd);
                    if (string.IsNullOrEmpty(image))
                    {
                        errMessage += data + " 圖檔1錯誤<br />";
                    }
                    if (string.IsNullOrEmpty(image2))
                    {
                        errMessage += data + " 圖檔2錯誤<br />";
                    }
                    if (isMainDeal)
                    {
                        //多檔
                        ComboDealCollection cd = pp.GetComboDealByBid(bh.Guid, false);
                        if (cd.Count < 1 || cd.Count > 3)
                        {
                            errMessage += data + " 子檔數量錯誤<br />";
                        }
                    }
                    importedBids.Add(bh.Guid);
                }

                if (errMessage == string.Empty && importedBids.Count != 0)
                {
                    return Json(new
                    {
                        Success = 1
                    });
                }
                else
                {
                    return Json(new
                    {
                        Message = errMessage,
                        Success = 0
                    });
                }
               
            }
            catch (Exception ex)
            {
                logger.Warn("CheckImportDeal error", ex);
                return Json(new
                {
                    Message = "分析資料發生問題",
                    Success = 0
                });
            }
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult ImportDeal(int campaignId, List<string> datas)
        {
            try
            {
                List<Guid> importDealIds = datas.ConvertAll<Guid>(i => Guid.Parse(i));

                GameCampaign campaign = gp.GetGameCampaign(campaignId);
                List<GameRule> rules = gp.GetAllGameRules();

                foreach (Guid importDealId in importDealIds)
                {
                    GameFacade.ImportGameActivityAndGames(campaign, rules, importDealId, User.Identity.Name);
                }

            }
            catch (Exception ex)
            {
                logger.Warn("ImportDeal error", ex);
                return Json(new
                {
                    Message = "匯入資料發生問題",
                    Success = 0
                });
            }

            return Json(new
            {
                Success = 1
            });
        }
        [HttpPost]
        [AjaxCall]
        public ActionResult UpdateSequence(List<UpdateActivityModel> data)
        {
            string message;
            try
            {
                GameFacade.UpdateGameActivitySequence(data, User.Identity.Name);
                return Json(new
                {
                    Success = 1
                });
            }
            catch (Exception ex)
            {
                logger.Warn("UpdateSequence error", ex);
                return Json(new
                {
                    Message = "更新資料發生問題",
                    Success = 0
                });
            }
        }

    }
}

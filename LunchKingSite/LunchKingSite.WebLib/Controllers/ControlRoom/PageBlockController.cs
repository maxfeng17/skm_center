﻿using System.Web.Mvc;
using LunchKingSite.WebLib.Component;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage("/controlroom/system/randomcms.aspx", Core.SystemFunctionType.Read)]    
    public class WebEditController : ControllerExt
    {


        public WebEditController()
        {

        }

        public ActionResult PageBlock()
        {
            return View();
        }

    }
}

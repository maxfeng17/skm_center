﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Interface;
using LunchKingSite.WebLib.Models.ControlRoom.LimitedTimeSelection;
using LunchKingSite.Core.Models.PponEntities;
using log4net;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.LimitedTimeSelection;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage("/ControlRoom/LimitedTimeSelection", SystemFunctionType.Read)]
    public class LimitedTimeSelectionRoomController : ControllerExt
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static IPponEntityProvider pep = ProviderFactory.Instance().GetDefaultProvider<IPponEntityProvider>();
        private static IMarketingEntityProvider marketingEntity = ProviderFactory.Instance().GetProvider<IMarketingEntityProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private ILog logger = LogManager.GetLogger(typeof(LimitedTimeSelectionRoomController));

        const int _SELECTION_DEAL_MAX = 30;

        public ActionResult Index(int? yy, int? mm)
        {
            MonthDaysListModel model = new MonthDaysListModel();
            yy = yy ?? DateTime.Now.Year;
            mm = mm ?? DateTime.Now.Month;

            DateTime theMonth = new DateTime(yy.Value, mm.Value, 1) ;
            int daySpan = theMonth.DayOfWeek == DayOfWeek.Sunday ? 7 : (int)theMonth.DayOfWeek;
            DateTime startDate = theMonth.AddDays(-1 * daySpan);
            DateTime endDate = startDate.AddDays(6 * 7);

            
            var summaries = pep.GetLimitedTimeSelectionMainSummary(startDate, endDate);
            List<DaySelection> items = new List<DaySelection>();
            for (DateTime theDate = startDate; theDate < endDate; theDate = theDate.AddDays(1))
            {
                var item = new DaySelection
                {
                    TheDate = theDate,
                    State = DayEditState.None,
                    DayInMonth = theDate.Year == theMonth.Year && theDate.Month == theMonth.Month,
                    DealsCount = 0
                };
                if (summaries.ContainsKey(item.TheDate))
                {
                    item.State = DayEditState.InProgress;
                    if (summaries[item.TheDate] >= _SELECTION_DEAL_MAX)
                    {
                        item.State = DayEditState.Done;
                    }
                }
                else if (LimitedTimeSelectionMain.IsReadOnly(theDate))
                {
                    item.State = DayEditState.Expired;
                }
                items.Add(item);
            }

            model.Items = items;
            model.TodayMonth = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            model.TheMonth = theMonth;
            return View(model);
        }

        [HttpGet]
        public ActionResult Edit(DateTime theDate)
        {
            LimitedTimeSelectionMain main = EventFacade.GetLimitedTimeSelectionMain(theDate);
            bool isNew;
            List<LimitedTimeSelectionDeal> deals;
            if (main == null)
            {
                if (LimitedTimeSelectionMain.IsReadOnly(theDate))
                {
                    ViewBag.Message = "不能為過去時間 " + theDate.ToString("yyyy-MM-dd") + "，建立限時優惠資料。";
                    return View(DailySelectionEditModel.Empty);
                }

                isNew = true;
                deals = new List<LimitedTimeSelectionDeal>();
                main = new LimitedTimeSelectionMain()
                {
                    TheDate = theDate.Date
                };
                EventFacade.SaveLimitedTimeSelectionMain(main, User.Identity.Name, DateTime.Now);
            }
            else
            {
                isNew = false;
                deals = EventFacade.GetLimitedTimeSelectionValidDeals(main.Id);
            }

            var rushBuyTrackings = marketingEntity.RushBuyTrackingsByDateTime(theDate.AddHours(12), theDate.AddDays(1).AddHours(12));
            List<DailySelectionDealInfo> dealViews = new List<DailySelectionDealInfo>();
            foreach(var selectionDeal in deals)
            {
                IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(selectionDeal.Bid, true);
                var orderCount = pp.RushBuyGetCompleteOrderCount(selectionDeal.Bid, theDate.AddHours(12), theDate.AddDays(1).AddHours(12));
                dealViews.Add(new DailySelectionDealInfo
                {
                    Bid = selectionDeal.Bid,
                    ItemName = deal.ItemName,
                    MainPic = deal.DefaultDealImage,
                    DealState = Helper.GetEnumDescription(EventFacade.GetSelectionDealState(deal)),
                    OrderCount = orderCount,
                    HitCount = rushBuyTrackings.Count(x => x.Bid == selectionDeal.Bid),
                    Sequence = selectionDeal.Sequence
                });
            }

            bool isRunning = LimitedTimeSelectionMain.IsRunning(main);
            bool isReadOnly = LimitedTimeSelectionMain.IsReadOnly(main);
            string previewUrl;
            if (isRunning)
            {
                previewUrl = Helper.CombineUrl(config.SiteUrl, "rushbuy/" + theDate.ToString("yyyMMdd"));
            }
            else
            {
                previewUrl = Helper.CombineUrl(config.SiteUrl,
                    "rushbuy/" + theDate.ToString("yyyMMdd") + "?code=" + main.PreviewCode);
            }

            var LogViews = EventFacade.GetLimitedTimeSelectionChangeLogViews(main.Id);

            DailySelectionEditModel model = new DailySelectionEditModel
            {
                ReadOnly = isReadOnly,
                IsNew = isNew,
                MainId = main.Id,
                PreviewUrl = previewUrl,
                TheDate = theDate,
                DealViews = dealViews,
                LogViews = LogViews,
            };
            return View(model);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult AddBids(AddSelectionDealsInputModel model)
        {
            try
            {
                var main = EventFacade.GetLimitedTimeSelectionMain(model.MainId);
                var allDeals = EventFacade.GetLimitedTimeSelectionAllDeals(model.MainId);
                var validDeals = allDeals.Where(t => t.Enabled).ToList();

                if (LimitedTimeSelectionMain.IsReadOnly(main))
                {
                    return Json(new
                    {
                        Message = "過期的限時優惠無法再異動。",
                        Success = 0
                    });
                }


                string userId = User.Identity.Name;
                DateTime now = DateTime.Now;
                int nextSeq = validDeals.Count == 0 ? 0 :
                    validDeals.Max(t => t.Sequence) + 1;
                List<string> messages = new List<string>();

                List<LimitedTimeSelectionDeal> newDealLogItems = new List<LimitedTimeSelectionDeal>();
                foreach (string bidStr in model.Bids)
                {
                    Guid bid;
                    if (Guid.TryParse(bidStr, out bid) == false)
                    {
                        messages.Add(string.Format("{0} 格式不符合，請確認", bidStr));
                        continue;
                    }
                    IViewPponDeal deal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(bid, true);
                    if (Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                    {
                        messages.Add(string.Format("{0} 是子檔不加入", deal.BusinessHourGuid));
                        continue;
                    }
                    if (validDeals.Count + newDealLogItems.Count >= _SELECTION_DEAL_MAX)
                    {
                        messages.Add(string.Format("{0} 超過上限 {1}", deal.BusinessHourGuid, _SELECTION_DEAL_MAX));
                        continue;
                    }
                    var selDeal = allDeals.FirstOrDefault(t => t.Bid == bid);
                    if (selDeal != null && selDeal.Enabled)
                    {
                        messages.Add(string.Format("{0} 已存在，不新增", deal.BusinessHourGuid));
                        continue;
                    }

                    if (selDeal == null)
                    {
                        selDeal = new LimitedTimeSelectionDeal
                        {
                            MainId = main.Id,
                            Bid = bid,
                            Sequence = (short)nextSeq,
                            Enabled = true,
                            CreateId = userId,
                            CreateTime = now
                        };
                        allDeals.Add(selDeal);
                    }
                    else if (selDeal.Enabled == false)
                    {
                        selDeal.Enabled = true;
                        selDeal.ModifyId = userId;
                        selDeal.ModifyTime = now;
                        selDeal.Sequence = (short)nextSeq;
                    }

                    int affctedCount = EventFacade.SaveLimitedTimeSelectionDeal(selDeal, userId, now);
                    if (affctedCount == 0)
                    {
                        messages.Add(string.Format("{0} 新增失敗", deal.BusinessHourGuid));
                        continue;
                    }
                    newDealLogItems.Add(selDeal);

                    nextSeq++;
                }
                //關於 log
                if (newDealLogItems.Count > 0)
                {
                    EventFacade.AddLimitedTimeSelectionChangeLog(model.MainId, 
                        DailySelectionEditLogView._ACTION_ADD,
                        newDealLogItems, User.Identity.Name);
                }

                return Json(new
                {
                    Message = string.Join("\r\n", messages),
                    RowCount = newDealLogItems.Count,
                    Success = 1
                });
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                return Json(new
                {
                    Success = 0,
                    Message = ex.Message
                });
            } 
        }
        [HttpPost]
        [AjaxCall]
        public ActionResult SaveChanges(UpdateSelectionDealsInputModel model)
        {
            try
            {
                LimitedTimeSelectionMain main = EventFacade.GetLimitedTimeSelectionMain(model.MainId);
                if (main == null)
                {
                    throw new Exception("資料錯誤");
                }
                if (LimitedTimeSelectionMain.IsReadOnly(main))
                {
                    return Json(new
                    {
                        Message = "過期的限時優惠無法再異動。",
                        Success = 0
                    });
                }

                var deletedItems = model.SelectionDeals.Where(t => t.Deleted).Select(t=>t.Bid).ToList();

                List<DailySelectionEditLogViewItem> deletedDealLogViewItems;
                EventFacade.SetLimitedTimeSelectionDealsDisabled(main, deletedItems,
                    User.Identity.Name, DateTime.Now, out deletedDealLogViewItems);
                var deals = EventFacade.GetLimitedTimeSelectionValidDeals(main.Id);

                List<LimitedTimeSelectionDeal> modifiedDealLogItems = new List<LimitedTimeSelectionDeal>();
                foreach (LimitedTimeSelectionDeal deal in deals)
                {
                    int seq = model.SelectionDeals.First(t => t.Bid == deal.Bid).Sequence;                
                    if (deal.Sequence != (short)seq)
                    {
                        deal.Sequence = (short)seq;
                        int affectedCount = EventFacade.SaveLimitedTimeSelectionDeal(deal, User.Identity.Name, DateTime.Now);
                        if (affectedCount>0)
                        {
                            modifiedDealLogItems.Add(deal);
                        }
                    }                    
                    seq++ ;
                }
                //關於 log
                if (deletedDealLogViewItems.Count > 0)
                {
                    EventFacade.AddLimitedTimeSelectionChangeLog(model.MainId,
                        DailySelectionEditLogView._ACTION_REMOVE,
                        deletedDealLogViewItems, User.Identity.Name);
                }
                if (modifiedDealLogItems.Count > 0)
                {
                    EventFacade.AddLimitedTimeSelectionChangeLog(model.MainId, 
                        DailySelectionEditLogView._ACTION_SORT,
                        modifiedDealLogItems.OrderBy(t => t.Sequence).ToList(), User.Identity.Name);
                }
                return Json(new
                {
                    RowCount = deletedDealLogViewItems.Count + modifiedDealLogItems.Count,
                    Success = 1
                });
            }
            catch (Exception ex)
            {
                logger.Warn(ex);
                return Json(new
                {
                    Success = 0,
                    Message = ex.Message
                });
            } 
        }      
    }
}

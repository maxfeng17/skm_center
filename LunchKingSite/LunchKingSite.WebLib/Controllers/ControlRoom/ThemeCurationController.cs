﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using System.Web.Services;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using System.Web;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [Authorize]
    public class ThemeCurationController : ControllerExt
    {
        #region property

        private static IAccountingProvider _ap;
        private static ISysConfProvider _config;
        private static IMemberProvider _mp;
        private static IPponProvider _pp;
        private static IVerificationProvider _vp;
        private static IOrderProvider _op;
        private static ISystemProvider _sp;

        static ThemeCurationController()
        {
            _ap = ProviderFactory.Instance().GetProvider<IAccountingProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _vp = ProviderFactory.Instance().GetProvider<IVerificationProvider>();
            _op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _sp = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        }

        #endregion property

        #region Action

        /// <summary>
        /// 主題策展-設定首頁
        /// </summary>
        /// <returns></returns>
        public ActionResult ThemeCurationMainList()
        {
            return View();
        }
        
        /// <summary>
        /// 主題策展-設定首頁搜尋
        /// </summary>
        /// <param name="is3Month">起始日三個月內的資料</param>
        /// <param name="keyword">關鍵字</param>
        /// <returns></returns>
        public ActionResult GetThemeCurationMainList(bool is3Month, string keyword)
        {
            var model = _pp.GetAllThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Normal);
            List<ThemeCurationMain> m;
            if (is3Month && !string.IsNullOrEmpty(keyword))
            {
                m = model.Where(x => x.StartDate >= DateTime.Now.AddMonths(-3) & x.Title.Contains(keyword)).ToList();
            }
            else if (is3Month && string.IsNullOrEmpty(keyword))
            {
                m = model.Where(x => x.StartDate >= DateTime.Now.AddMonths(-3)).ToList();
            }
            else if
              (!is3Month && !string.IsNullOrEmpty(keyword))
            {
                m = model.Where(x => x.Title.Contains(keyword)).ToList();
            }
            else
            {
                m = model.ToList();
            }
            return PartialView("_ThemeList", m);
        }

        /// <summary>
        /// 主題策展-群組設定
        /// </summary>
        /// <param name="mainId"></param>
        /// <returns></returns>
        public ActionResult ThemeCurationGroup(int mainId)
        {
            var main = _pp.GetThemeCurationMainById(mainId);
            ViewBag.MainId = main.Id;
            ViewBag.Main = main;

            if (!main.IsLoaded)
            {
                return View();
            }

            var groups = _pp.GetThemeCurationGroupCollectionByMainId(mainId).OrderBy(x => x.Seq).ToList();
            ViewBag.Groups = groups;
            
            var groupCurationDic = new Dictionary<int, List<KeyValuePair<int, ApiEventPromo>>>();
            foreach (var group in groups.OrderBy(x=>x.Seq))
            {
                List<ThemeCurationItem> themeCurationItems = 
                    _pp.GetThemeCurationItemCollectionByGroupId(group.Id).OrderBy(x => x.Seq).ToList();

                var curationList = new List<KeyValuePair<int, ApiEventPromo>>();
                foreach (var item in themeCurationItems)
                {
                    ApiEventPromo curation = item.CurationType == (int)EventPromoEventType.CurationTwo
                        ? MarketingFacade.GetApiBrandByIdFromDB(item.CurationId, false)
                        : MarketingFacade.GetApiEventPromoByIdFromDB(item.CurationId, false);

                    if (curation == null) continue;
                    curationList.Add(new KeyValuePair<int, ApiEventPromo>(item.Id, curation));
                }
                groupCurationDic.Add(group.Id, curationList);
            }
            ViewBag.GroupCurationDic = groupCurationDic;

            return View();
        }

        /// <summary>
        /// 主題策展-策展設定
        /// </summary>
        /// <param name="mainId"></param>
        /// <param name="groupId"></param>
        /// <returns></returns>
        public ActionResult ThemeCurationList(int mainId, int groupId)
        {
            var themeCurationItems = _pp.GetThemeCurationItemCollectionByGroupId(groupId).OrderBy(x => x.Seq).ToList();
            ViewBag.MainId = mainId;
            ViewBag.GroupId = groupId;
            ViewBag.ThemeCurationItems = themeCurationItems;
            return View();
        }


        #endregion

        /// <summary>
        /// 新增主題
        /// </summary>
        /// <param name="themeName"></param>
        /// <param name="themeMainImage"></param>
        /// <param name="themeMobileMainImage"></param>
        /// <param name="themeMainBackgroundImage"></param>
        /// <param name="themeMainSpacerImage"></param>
        /// <param name="startDate"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SaveNewTheme(string themeName, string themeMainImage, string themeMobileMainImage, string themeMainBackgroundImage, string themeMainSpacerImage, DateTime startDate, DateTime endTime)
        {
            var result = new JsonResult();
            var theme = new ThemeCurationMain();
            theme.Title = themeName;
            theme.MainImage = (!string.IsNullOrEmpty(themeMainImage)) ? themeMainImage : null;
            theme.MobileMainImage = (!string.IsNullOrEmpty(themeMobileMainImage)) ? themeMobileMainImage : null;
            theme.MainBackgroundImage = (!string.IsNullOrEmpty(themeMainBackgroundImage)) ? themeMainBackgroundImage : null;
            theme.MainSpacerImage = (!string.IsNullOrEmpty(themeMainSpacerImage)) ? themeMainSpacerImage : null;
            theme.StartDate = startDate;
            theme.EndTime = endTime;
            theme.CreateTime = DateTime.Now;
            theme.ModifyUser = User.Identity.Name;

            var themeId = _pp.SaveThemeCurationMain(theme);
            if (themeId > 0)
            {
                result.Data = new
                {
                    IsSuccess = true,
                    Theme = new
                    {
                        theme.Id,
                        theme.Title,
                        theme.MainImage,
                        theme.MobileMainImage,
                        theme.MainBackgroundImage,
                        theme.MainSpacerImage,
                        StartDate = theme.StartDate.ToString("yyyy/MM/dd HH:mm:ss"),
                        EndTime = theme.EndTime.ToString("yyyy/MM/dd HH:mm:ss")
                    }
                };

                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(theme));
            }
            else
            {
                result.Data = new { IsSuccess = false };
            }

            return result;
        }

        /// <summary>
        /// 儲存編輯主題
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        [ValidateInput(false)]
        public JsonResult SaveEditTheme()
        {
            var result = new JsonResult();
            int themeid = int.Parse(Request["themeId"]);

            ThemeCurationMain theme = _pp.GetThemeCurationMainById(themeid);
            if (!theme.IsLoaded)
            {
                //新增模式預設值
                theme = new ThemeCurationMain();
                theme.ShowInWeb = true;
                theme.ShowInApp = true;
            }
            theme.Title = Request["themeName"];
            if (!string.IsNullOrEmpty(Request["startDate"])) theme.StartDate = DateTime.Parse(Request["startDate"]);
            if (!string.IsNullOrEmpty(Request["endTime"])) theme.EndTime = DateTime.Parse(Request["endTime"]);
            theme.FbShareTitle = Request["fbShareTitle"];
            theme.WebRuleContent = Request["webRuleContent"];
            theme.MobileRuleContent = Request["mobileRuleContent"];
            theme.CssStyle = string.Format("{0}@{1}", Request["webCssStyle"], Request["mobileCssStyle"]);
            theme.ModifyTime = DateTime.Now;
            theme.ModifyUser = User.Identity.Name;
            theme.RedirectUrl = Request["RedirectUrl"];

            if (!theme.IsLoaded)
            {
                _pp.SaveThemeCurationMain(theme);
            }

            string imagePath = "";

            //## 讀取指定的上傳檔案ID
            var fileWebMain = Request.Files["fileWebMain"];
            if (fileWebMain != null)
            {
                imagePath = UploadImageFile(fileWebMain, "MainImage", theme.Id.ToString());
                theme.MainImage = imagePath;
            }
            else
            {
                if (Request["fileWebMain"].Contains("false"))
                {
                    theme.MainImage = null;
                }
            }

            var fileWebBG = Request.Files["fileWebBG"];
            if (fileWebBG != null)
            {
                imagePath = UploadImageFile(fileWebBG, "MainBackgroundImage", theme.Id.ToString());
                theme.MainBackgroundImage = imagePath;
            }
            else
            {
                if (Request["fileWebBG"].Contains("false"))
                {
                    theme.MainBackgroundImage = null;
                }
            }

            var fileWebSpacer = Request.Files["fileWebSpacer"];
            if (fileWebSpacer != null)
            {
                imagePath = UploadImageFile(fileWebSpacer, "MainSpacerImage", theme.Id.ToString());
                theme.MainSpacerImage = imagePath;
            }
            else
            {
                if (Request["fileWebSpacer"].Contains("false"))
                {
                    theme.MainSpacerImage = null;
                }
            }

            var fileWebCategory = Request.Files["fileWebCategory"];
            if (fileWebCategory != null)
            {
                imagePath = UploadImageFile(fileWebCategory, "WebCategoryImage", theme.Id.ToString());
                theme.WebCategoryImage = imagePath;
            }
            else
            {
                if (Request["fileWebCategory"].Contains("false"))
                {
                    theme.WebCategoryImage = null;
                }
            }

            var fileWebRuleBanner = Request.Files["fileWebRuleBanner"];
            if (fileWebRuleBanner != null)
            {
                imagePath = UploadImageFile(fileWebRuleBanner, "WebRuleBanner", theme.Id.ToString());
                theme.WebRuleBanner = imagePath;
            }
            else
            {
                if (Request["fileWebRuleBanner"].Contains("false"))
                {
                    theme.WebRuleBanner = null;
                }
            }

            var fileMobileMain = Request.Files["fileMobileMain"];
            if (fileMobileMain != null)
            {
                imagePath = UploadImageFile(fileMobileMain, "MobileMainImage", theme.Id.ToString());
                theme.MobileMainImage = imagePath;
            }
            else
            {
                if (Request["fileMobileMain"].Contains("false"))
                {
                    theme.MobileMainImage = null;
                }
            }

            var fileMobileCategory = Request.Files["fileMobileCategory"];
            if (fileMobileCategory != null)
            {
                imagePath = UploadImageFile(fileMobileCategory, "MobileCategoryImage", theme.Id.ToString());
                theme.MobileCategoryImage = imagePath;
            }
            else
            {
                if (Request["fileMobileCategory"].Contains("false"))
                {
                    theme.MobileCategoryImage = null;
                }
            }

            var fileMobileRuleBanner = Request.Files["fileMobileRuleBanner"];
            if (fileMobileRuleBanner != null)
            {
                imagePath = UploadImageFile(fileMobileRuleBanner, "MobileRuleBanner", theme.Id.ToString());
                theme.MobileRuleBanner = imagePath;
            }
            else
            {
                if (Request["fileMobileRuleBanner"].Contains("false"))
                {
                    theme.MobileRuleBanner = null;
                }
            }

            theme.MobileCover = null;

            var fileAppIndex = Request.Files["fileAppIndex"];
            if (fileAppIndex != null)
            {
                imagePath = UploadImageFile(fileAppIndex, "AppIndexImage", theme.Id.ToString());
                theme.AppIndexImage = imagePath;
            }
            else
            {
                if (Request["fileAppIndex"].Contains("false"))
                {
                    theme.AppIndexImage = null;
                }
            }

            var fileMobileSmallIcon = Request.Files["fileMobileSmallIcon"]; //ThemeOtherPic
            if (fileMobileSmallIcon != null)
            {
                imagePath = UploadImageFile(fileMobileSmallIcon, "ThemeOtherPic", theme.Id.ToString());
                theme.ThemeOtherPic = imagePath;
            }
            else
            {
                if (Request["fileMobileSmallIcon"].Contains("false"))
                {
                    theme.ThemeOtherPic = null;
                }
            }

            var fileFBShare = Request.Files["fileFBShare"];
            if (fileFBShare != null)
            {
                imagePath = UploadImageFile(fileFBShare, "FbShareIamge", theme.Id.ToString());
                theme.FbShareIamge = imagePath;
            }
            else
            {
                if (Request["fileFBShare"].Contains("false"))
                {
                    theme.FbShareIamge = null;
                }
            }

            try
            {
                _pp.SaveThemeCurationMain(theme);
                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(theme));
                ViewBag.Main = theme;
                result.Data = new { IsSuccess = true, MainId = theme.Id, Theme = theme };
                return result;
            }
            catch
            {
                result.Data = new { IsSuccess = false };
                return result;
            }
        }

        /// <summary>
        /// 啟用策展活動
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SetThemeActive(int themeId, bool isActive, string type)
        {
            var result = new JsonResult();

            var theme = _pp.GetThemeCurationMainById(themeId);
            if (theme.IsLoaded)
            {
                if (type == "WEB") {
                    theme.ShowInWeb = isActive;
                }

                if (type == "APP")
                {
                    theme.ShowInApp = isActive;
                }

                theme.ModifyTime = DateTime.Now;
                theme.ModifyUser = User.Identity.Name;
                _pp.SaveThemeCurationMain(theme);

                result.Data = new { IsSuccess = true };
            }
            else
            {
                result.Data = new { IsSuccess = false };
            }

            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(new { themeId, isActive, type }));
            return result;
        }

        /// <summary>
        /// 新增群組
        /// </summary>
        /// <param name="mainId"></param>
        /// <param name="groupName"></param>
        /// <param name="isActive"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SaveNewGroup(int mainId, string groupName, bool isActive, DateTime startTime, DateTime endTime)
        {
            var result = new JsonResult();
            var group = new ThemeCurationGroup();
            group.MainId = mainId;
            group.Seq = _pp.GetThemeCurationGroupNewSeq(mainId);
            group.Enabled = isActive;
            group.GroupName = groupName;
            group.StartTime = startTime;
            group.EndTime = endTime;
            group.CreateTime = DateTime.Now;
            group.ModifyUser = User.Identity.Name;

            var groupId = _pp.SaveThemeCurationGroup(group);
            if (groupId > 0)
            {
                result.Data = new { IsSuccess = true,
                    Group = new {
                        group.Id,
                        group.Seq,
                        StartTime = group.StartTime.ToString("yyyy/MM/dd HH:mm:ss"),
                        EndTime = group.EndTime.ToString("yyyy/MM/dd HH:mm:ss")
                    } };

                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(group));
            }
            else
            {
                result.Data = new { IsSuccess = false };
            }
            
            return result;
        }

        /// <summary>
        /// 儲存編輯群組
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="groupName"></param>
        /// <param name="isActive"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SaveEditGroup(int groupId, string groupName, bool isActive, DateTime startTime, DateTime endTime)
        {
            var result = new JsonResult();
            var group = _pp.GetThemeCurationGroupByGroupId(groupId);
            if (group.IsLoaded)
            {
                group.GroupName = groupName;
                group.Enabled = isActive;
                group.StartTime = startTime;
                group.EndTime = endTime;
                group.ModifyTime = DateTime.Now;
                group.ModifyUser = User.Identity.Name;
                _pp.SaveThemeCurationGroup(group);
                
                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(group));

                result.Data = new { IsSuccess = true };
                return result;
            }
            result.Data = new { IsSuccess = false };
            return result;
        }

        /// <summary>
        /// 儲存編輯置頂群組
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="groupName"></param>
        /// <param name="isActive"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SaveEditTopGroup(int mainId, int groupId, string groupName, bool isActive, DateTime startTime, DateTime endTime)
        {
            var result = new JsonResult();
            if (groupId > 0)
            {
                //編輯模式模式
                var group = _pp.GetThemeCurationGroupByGroupId(groupId);
                if (group.IsLoaded)
                {
                    group.GroupName = groupName;
                    group.Enabled = isActive;
                    group.StartTime = startTime;
                    group.EndTime = endTime;
                    group.ModifyTime = DateTime.Now;
                    group.ModifyUser = User.Identity.Name;
                    group.IsTopGroup = true;
                    _pp.SaveThemeCurationGroup(group);

                    _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(group));

                    result.Data = new { IsSuccess = true };
                    return result;
                }
                else
                {
                    result.Data = new { IsSuccess = false };
                    return result;
                }
            }
            else
            {
                //新增模式
                var group = new ThemeCurationGroup();
                group.MainId = mainId;
                group.Seq = 1;
                group.Enabled = isActive;
                group.GroupName = groupName;
                group.StartTime = startTime;
                group.EndTime = endTime;
                group.CreateTime = DateTime.Now;
                group.ModifyUser = User.Identity.Name;
                group.IsTopGroup = true;

                groupId = _pp.SaveThemeCurationGroup(group);
                if (groupId > 0)
                {
                    result.Data = new
                    {
                        IsSuccess = true,
                        Group = new
                        {
                            group.Id,
                            group.Seq,
                            StartTime = group.StartTime.ToString("yyyy/MM/dd HH:mm:ss"),
                            EndTime = group.EndTime.ToString("yyyy/MM/dd HH:mm:ss")
                        }
                    };

                    _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(group));
                }
                else
                {
                    result.Data = new { IsSuccess = false };
                }

                return result;
            }
        }


        /// <summary>
        /// 刪除群組及群組內策展
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult DeleteGroupAndCuration(int groupId)
        {
            _pp.DeleteThemeCurationGroupByGroupId(groupId);
            _pp.DeleteThemeCurationItemByGroupId(groupId);

            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(groupId));

            return new JsonResult { Data = new { IsSuccess = true } };
        }

        /// <summary>
        /// 啟用群組
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="isActive"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SetGroupActive(int groupId, bool isActive)
        {
            var result = new JsonResult();

            var group = _pp.GetThemeCurationGroupByGroupId(groupId);
            if (group.IsLoaded)
            {
                group.Enabled = isActive;
                group.ModifyTime = DateTime.Now;
                group.ModifyUser = User.Identity.Name;
                _pp.SaveThemeCurationGroup(group);

                result.Data = new { IsSuccess = true };
            }
            else
            {
                result.Data = new { IsSuccess = false };
            }

            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(new { groupId, isActive }));
            return result;
        }

        /// <summary>
        /// 設定群組順序
        /// </summary>
        /// <param name="groupIds"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SetGroupSeq(string groupIds)
        {
            var result = new JsonResult();
            var strGroupIds = groupIds.Split(',');

            int newSeq = 1;
            var allCurationGroup = new ThemeCurationGroupCollection();
            foreach (var strGroupId in strGroupIds)
            {
                int groupId;
                if (!int.TryParse(strGroupId, out groupId))
                {
                    result.Data = new { IsSuccess = false, Msg = "群組編號錯誤" };
                    return result;
                }

                var curationGroup = _pp.GetThemeCurationGroupByGroupId(groupId);
                if (!curationGroup.IsLoaded)
                {
                    result.Data = new { IsSuccess = false, Msg = "查無群組編號" };
                    return result;
                }
                curationGroup.Seq = newSeq;
                curationGroup.ModifyTime = DateTime.Now;
                curationGroup.ModifyUser = User.Identity.Name;
                allCurationGroup.Add(curationGroup);
                newSeq++;
            }

            //更新排序
            var updateCnt = _pp.SaveAllThemeCurationGroupCollection(allCurationGroup);
            if (updateCnt <= 0)
            {
                result.Data = new { IsSuccess = false, Msg = "更新失敗" };
                return result;
            }

            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(groupIds));

            result.Data = new { IsSuccess = true, Msg = "更新成功" };
            return result;
        }

        /// <summary>
        /// 新增策展ID
        /// </summary>
        /// <param name="groupId"></param>
        /// <param name="curationId"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult AddCuration(int groupId, int curationId,bool isGroupOffline)
        {
            var result = new JsonResult();
            result.ContentType = "application/json; charset=utf-8";

            //檢查有無此策展
            //var curation = MarketingFacade.GetApiEventPromoOrBrandById(curationId, (int)EventPromoEventType.CurationTwo, false);
            var curation = MarketingFacade.GetApiBrandByIdFromDB(curationId, false);

            if (curation == null)
            {
                result.Data = new { IsSuccess = false, Msg = "無此策展資料" };
                return result;
            }

            var items = _pp.GetThemeCurationItemCollectionByGroupId(groupId);
            if (items.Any(x => x.CurationId == curationId))
            {
                result.Data = new { Msg = "策展編號重覆" };
                return result;
            }

            var addThemeCuration = new ThemeCurationItem
            {
                GroupId = groupId,
                CurationType = (int)EventPromoEventType.CurationTwo,
                CurationId = curationId,
                Seq = items.Max(x => x.Seq) + 1 ?? 1,
                CreateTime = DateTime.Now,
                ModifyUser = User.Identity.Name
            };

            var itemId = _pp.SaveThemeCurationItem(addThemeCuration);
            if (itemId > 0)
            {
                result.Data = new
                {
                    IsSuccess = true,
                    Msg = "新增成功",
                    CurationItemId = itemId,
                    EventPromo = curation,
                    GroupOnline = (!isGroupOffline) ? !isGroupOffline : (DateTime.Now < curation.EndDate && curation.Status)
                };
            
                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(addThemeCuration));
            }
            else
            {
                result.Data = new { IsSuccess = false, Msg = "新增失敗" };
            }

            return result;
        }
        
        /// <summary>
        /// 移除策展ID
        /// </summary>
        /// <param name="curationItemId"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult RemoveCuration(int curationItemId, int groupId)
        {
            _pp.DeleteThemeCurationItemByItemId(curationItemId);
            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(curationItemId));

            List<int> remainList = _pp.GetThemeCurationItemCollectionByGroupId(groupId).Select(x => x.CurationId).ToList();
            var brandList = _pp.BrandGetByBrandIdList(remainList);
            var groupOnline = false;
            foreach (Brand brand in brandList) {
                if ((DateTime.Now < brand.EndTime && brand.Status)) {
                    groupOnline = true;
                    break;
                }
            }

            return new JsonResult { Data = new { IsSuccess = true, GroupOnline = groupOnline } };
        }

        /// <summary>
        /// 設定策展順序
        /// </summary>
        /// <param name="itemIds"></param>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SetCurationItemSeq(string itemIds)
        {
            var result = new JsonResult();
            var strItemIds = itemIds.Split(',');

            int newSeq = 1;
            var allItems = new ThemeCurationItemCollection();
            foreach (var strItemId in strItemIds)
            {
                int itemId;
                if (!int.TryParse(strItemId, out itemId))
                {
                    result.Data = new { IsSuccess = false, Msg = "策展列表編號錯誤" };
                    return result;
                }

                var curationList = _pp.GetThemeCurationItemByItemId(itemId);
                if (!curationList.IsLoaded)
                {
                    result.Data = new { IsSuccess = false, Msg = "查無策展列表編號" };
                    return result;
                }
                curationList.Seq = newSeq;
                curationList.ModifyTime = DateTime.Now;
                curationList.ModifyUser = User.Identity.Name;
                allItems.Add(curationList);
                newSeq++;
            }

            //更新排序
            var updateCnt = _pp.SaveAllThemeCurationItemCollection(allItems);
            if (updateCnt <= 0)
            {
                result.Data = new { IsSuccess = false, Msg = "更新失敗" };
                return result;
            }

            _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(itemIds));

            result.Data = new { IsSuccess = true, Msg = "更新成功" };
            return result;
        }
        
        /// <summary>
        /// 主題策展-頻道設定
        /// </summary>
        public ActionResult ThemeCurationChannel()
        {
            var main = (_pp.GetAllThemeCurationMainCollection().Where(x => x.ThemeType == (int)ThemeType.Channel).FirstOrDefault()) ?? new ThemeCurationMain();
            var mainId = main.Id;
            ViewBag.MainId = mainId;
            ViewBag.Main = main;

            if (main.Id == 0)
            {
                return View();
            }

            var groups = _pp.GetThemeCurationGroupCollectionByMainId(mainId).OrderBy(x => x.Seq).ToList();
            ViewBag.Groups = groups;

            var groupCurationDic = new Dictionary<int, List<KeyValuePair<int, ApiEventPromo>>>();
            foreach (var group in groups.OrderBy(x => x.Seq))
            {
                List<ThemeCurationItem> themeCurationItems =
                    _pp.GetThemeCurationItemCollectionByGroupId(group.Id).OrderBy(x => x.Seq).ToList();

                var curationList = new List<KeyValuePair<int, ApiEventPromo>>();
                foreach (var item in themeCurationItems)
                {
                    ApiEventPromo curation = item.CurationType == (int)EventPromoEventType.CurationTwo
                        ? MarketingFacade.GetApiBrandByIdFromDB(item.CurationId, false)
                        : MarketingFacade.GetApiEventPromoByIdFromDB(item.CurationId, false);

                    if (curation == null) continue;
                    curationList.Add(new KeyValuePair<int, ApiEventPromo>(item.Id, curation));
                }
                groupCurationDic.Add(group.Id, curationList);
            }
            ViewBag.GroupCurationDic = groupCurationDic;

            return View();
        }

        /// <summary>
        /// 儲存編輯策展頻道
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [WebMethod]
        public JsonResult SaveEditThemeChannel()
        {
            var result = new JsonResult();
            int themeid = int.Parse(Request["themeId"]);

            ThemeCurationMain themeChannel = _pp.GetThemeCurationMainById(themeid);
            if (!themeChannel.IsLoaded)
            {
                //新增模式預設值
                themeChannel = new ThemeCurationMain();
                themeChannel.Title = "策展頻道";
                themeChannel.ChannelTitle = "策展頻道";
                themeChannel.ShowInWeb = true;
                themeChannel.ShowInApp = true;
                themeChannel.ThemeType = (int)ThemeType.Channel;
                themeChannel.StartDate = DateTime.Now;
                themeChannel.EndTime = DateTime.MaxValue;
                themeChannel.CreateTime = DateTime.Now;
                _pp.SaveThemeCurationMain(themeChannel);
            }
            else
            {
                themeChannel.ModifyTime = DateTime.Now;
                themeChannel.ModifyUser = User.Identity.Name;
            }

            string imagePath = "";

            //## 讀取指定的上傳檔案ID
            var fileWebMain = Request.Files["fileWebMain"];
            if (fileWebMain != null)
            {
                imagePath = UploadImageFile(fileWebMain, "MainImage", themeChannel.Id.ToString());
                themeChannel.MainImage = imagePath;
            }
            else
            {
                if (Request["fileWebMain"].Contains("false"))
                {
                    themeChannel.MainImage = null;
                }
            }

            var fileMobileMain = Request.Files["fileMobileMain"];
            if (fileMobileMain != null)
            {
                imagePath = UploadImageFile(fileMobileMain, "MobileMainImage", themeChannel.Id.ToString());
                themeChannel.MobileMainImage = imagePath;
            }
            else
            {
                if (Request["fileMobileMain"].Contains("false"))
                {
                    themeChannel.MobileMainImage = null;
                }
            }
            
            try
            {
                _pp.SaveThemeCurationMain(themeChannel);
                _pp.ChangeLogInsert(this.GetType().Name, MethodBase.GetCurrentMethod().Name, new JsonSerializer().Serialize(themeChannel));
                ViewBag.Main = themeChannel;
                result.Data = new { IsSuccess = true, MainId = themeChannel.Id, Theme = themeChannel };
                return result;
            }
            catch
            {
                result.Data = new { IsSuccess = false };
                return result;
            }
        }

        private string UploadImageFile(HttpPostedFileBase uploadedFile,string subName, string prefix = "")
        {
            if (uploadedFile != null)
            {
                string fileName = prefix + "_" + subName;
                string fileNameWithExtension = fileName + "." + Helper.GetExtensionByContentType(uploadedFile.ContentType) + "?ts=" + DateTime.Now.Ticks.ToString("x"); //for 圖片被 瀏覽器 或 APP Cache住舊圖加上時間雜湊函數
                ImageUtility.UploadFile(uploadedFile.ToAdapter(), UploadFileType.ThemeImage, "Theme", fileName);

                return ImageFacade.GenerateMediaPath("Theme", fileNameWithExtension);
            }
            return string.Empty;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.DiscountCode;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage]
    public class DiscountCodeController : ControllerExt
    {

        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();

        #region 申請折價券

        public ActionResult CampaignAdd(string msg)
        {
            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.msg = msg;
            }
            return View();
        }
        public ActionResult CampaignCreate()
        {
            return RedirectToAction("CampaignAdd");
        }
        [HttpPost]
        public ActionResult CampaignCreate(CreateDiscountCodeData discountCode)
        {
            DiscountCampaign campaign = new DiscountCampaign();
            campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
            campaign.Amount = discountCode.DiscountAmount;
            campaign.Qty = discountCode.Quantity;
            campaign.StartTime = discountCode.StartDate;
            campaign.EndTime = discountCode.EndDate;
            campaign.IsDiscountPriceForDeal = discountCode.SpecifiedForDiscountPrice;

            if (discountCode.MinimumAmount != 0)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.MinimumAmount;
                campaign.MinimumAmount = discountCode.MinimumAmount;
            }

            if (discountCode.DiscountGiftType == 1)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AutoSend;
            }

            if (discountCode.DiscountGiftType == 2)
            {
                campaign.EventDateS = discountCode.EventDateS;
                campaign.EventDateE = discountCode.EventDateE;
                campaign.FreeGiftDiscount = discountCode.FreeGiftDiscount;
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.FreeGiftWithPurchase;
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.TakeCeiling;
            }

            if (discountCode.UseOnlyType != 0)
            {
                campaign.Flag = campaign.Flag | discountCode.UseOnlyType;
            }

            if (discountCode.AppOnly != 0)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AppUseOnly;
            }

            campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;

            campaign.Name = discountCode.EventName;
            campaign.CreateTime = DateTime.Now;
            campaign.CreateId = UserName;

            if (discountCode.DiscountType == 0)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.InstantGenerate;
            }

            if (discountCode.DiscountType == 2)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.SingleSerialKey;
                if (!string.IsNullOrEmpty(discountCode.CustomCode))
                {
                    discountCode.CustomCode = discountCode.CustomCode.Trim();
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CustomSerialKey;
                    ViewDiscountDetail viewDiscountDetail = op.ViewDiscountDetailCheckSingleSerialDuplicate(discountCode.CustomCode, discountCode.StartDate, discountCode.EndDate);
                    if (viewDiscountDetail.IsLoaded)
                    {
                        //自訂代碼重複
                        return RedirectToAction("CampaignAdd", new { msg = "自訂代碼重複，無法建立" });
                    }
                }
            }

            if (discountCode.MinimumGrossMargin != 0)
            {
                campaign.MinGrossMargin = discountCode.MinimumGrossMargin;
            }

            if (discountCode.DiscountType != 0 && (discountCode.CanReused & (int)DiscountCampaignUsedFlags.CanReused) > 0)
            {
                campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CanReused;
            }

            op.DiscountCampaignSet(campaign);

            if (discountCode.DiscountType != 0)
            {
                DateTime start = DateTime.Now;
                DiscountCodeCollection dataList = new DiscountCodeCollection();
                var code = string.IsNullOrEmpty(discountCode.CustomCode.Trim()) ? PromotionFacade.GetDiscountCode(campaign.Amount.Value) : discountCode.CustomCode.Trim();
                for (var i = 0; i < campaign.Qty; i++)
                {
                    DiscountCode dc = new DiscountCode();
                    dc.CampaignId = campaign.Id;
                    dc.Amount = campaign.Amount;
                    if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) == 0)
                    {
                        code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                    }
                    dc.Code = code;
                    dataList.Add(dc);
                }

                if (dataList.Count > 0)
                {
                    op.DiscountCodeCollectionBulkInsert(dataList);
                }

            }

            //頻道
            if (discountCode.ChannelType == 1)
            {
                if (!string.IsNullOrEmpty(discountCode.CategorieId))
                {
                    var CategorieLink = discountCode.CategorieId.Split(',');
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                    op.DiscountCampaignSet(campaign);
                    foreach (var channel in CategorieLink)
                    {
                        var tmpStr = channel.Split('|');
                        int channelId, categoryId;
                        if (int.TryParse(tmpStr[0], out channelId) && int.TryParse(tmpStr[1], out categoryId))
                        {
                            DiscountCategory dc = new DiscountCategory();
                            dc.CampaignId = campaign.Id;
                            dc.ChannelId = channelId;
                            dc.CategoryId = categoryId;
                            op.DiscountCategorySet(dc);
                        }

                    }
                }
            }

            //策展2.0
            if (discountCode.ChannelType == 2)
            {
                if (!string.IsNullOrEmpty(discountCode.BrandId))
                {
                    var brandLink = discountCode.BrandId.Split(',');
                    int brandId;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                    op.DiscountCampaignSet(campaign);
                    foreach (var brandStr in brandLink)
                    {
                        if (int.TryParse(brandStr, out brandId))
                        {
                            DiscountBrandLink brand = new DiscountBrandLink();
                            brand.CampaignId = campaign.Id;
                            brand.BrandId = brandId;
                            op.DiscountBrandLinkSet(brand);
                        }
                    }
                }
            }

            //商品主題活動
            if (discountCode.ChannelType == 3 || discountCode.ChannelType == 4)
            {
                if (!string.IsNullOrEmpty(discountCode.EventId))
                {
                    var eventPromoList = discountCode.EventId.Split(',');
                    int promoId;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                    op.DiscountCampaignSet(campaign);
                    foreach (var promoStr in eventPromoList)
                    {
                        int.TryParse(promoStr, out promoId);
                        DiscountEventPromoLink promoLink = new DiscountEventPromoLink();
                        promoLink.CampaignId = campaign.Id;
                        promoLink.EventPromoId = promoId;
                        op.DiscountEventPromoLinkSet(promoLink);
                    }
                }
            }

            return Redirect("/ControlRoom/discount/campaignmain.aspx");
        }

        public class BrandAjaxData
        {
            public int Status { get; set; }
            public int EventId { get; set; }
            public string EventName { get; set; }
            public int UniqueId { get; set; }
            public string Msg { get; set; }
            public decimal GrossMargin { get; set; }
        }

        #endregion

        #region 折價券黑名單

        public ActionResult CampaignBlackList(string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                if (msg.Contains("|"))
                {
                    var tmp = msg.Trim().Split('|');
                    ViewBag.TrueCount = tmp[0];
                    ViewBag.FalseCount = tmp[1];
                }
                else
                {
                    ViewBag.msg = msg;
                }

            }
            ViewBag.DiscountLimitList = new List<ViewDiscountLimit>();
            ViewBag.Search = false;
            return View();
        }

        [HttpPost]
        public ActionResult CampaignBlackList(CampaignBlackData data)
        {
            var list = op.DiscountLimitGetAll().ToList();            

            if (data.BlackDealType == (int)CampaignBlackDealType.Sold)
            {
                list = list.Where(x => x.BusinessHourOrderTimeE > DateTime.Now).ToList();
            }
            else if (data.BlackDealType == (int)CampaignBlackDealType.Expire)
            {
                list = list.Where(x => x.BusinessHourOrderTimeE < DateTime.Now).ToList();
            }

            Dictionary<Guid, List<int>> dealCids = pp.DealCategoriesDictGetAll();

            foreach (var d in list)
            {
                if (dealCids.ContainsKey(d.Bid) == false)
                {
                    continue;
                }
                var cids = dealCids[d.Bid];

                if (cids.Contains(CategoryManager.pponChannelId.Food))
                {
                    d.ChannelStr += "美食";
                }
                if (cids.Contains(CategoryManager.pponChannelId.Delivery))
                {
                    d.ChannelStr += "宅配";
                }
                if (cids.Contains(CategoryManager.pponChannelId.Travel))
                {
                    d.ChannelStr += "旅遊";
                }
                if (cids.Contains(CategoryManager.pponChannelId.Beauty))
                {
                    d.ChannelStr += "玩美休閒";
                }
                if (cids.Contains(CategoryManager.pponChannelId.PiinLife))
                {
                    d.ChannelStr += "品生活";
                }
                if (cids.Contains(CategoryManager.pponChannelId.FamilyMart))
                {
                    d.ChannelStr += "全家";
                }
                d.ChannelIdList = cids;
            }
            list = list.Where(t => t.ChannelIdList != null).ToList();
            if (data.ChannelType != 0)
            {
                list = list.Where(t => t.ChannelIdList.Contains(data.ChannelType)).ToList();
            }
            if (!string.IsNullOrEmpty(data.SearchData))
            {
                if (data.SearchType == 0)
                {
                    Guid bid;
                    if (Guid.TryParse(data.SearchData, out bid))
                    {
                        list = list.Where(x => x.Bid == bid).ToList();
                    }
                    else
                    {
                        return RedirectToAction("CampaignBlackList", new { msg = "Bid格式錯誤" });
                    }
                }
                else
                {
                    list = list.Where(x => x.CouponUsage.Contains(data.SearchData)).ToList();
                }
            }

            ViewBag.DiscountLimitList = list;
            ViewBag.BlackDealType = data.BlackDealType;
            ViewBag.ChannelType = data.ChannelType;
            ViewBag.SearchData = data.SearchData;
            ViewBag.SearchType = data.SearchType;
            ViewBag.Search = true;

            return View();
        }

        [HttpPost]
        public ActionResult BlackImportByBID(string bid)
        {
            Guid Bid = new Guid();
            string Msg = "";
            if (!string.IsNullOrEmpty(bid))
            {
                if (Guid.TryParse(bid.Trim(), out Bid))
                {
                    ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(Bid);
                    if (vpd.IsLoaded)
                    {
                        if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                        {
                            DiscountLimitSet(Bid);
                            return RedirectToAction("CampaignBlackList", new { msg = "新增成功" });
                        }
                        else
                        {
                            Msg = "無法綁定子檔";
                        }
                    }
                    else
                    {
                        Msg = "無此Bid";
                    }

                }
                else
                {
                    Msg = "Bid格式錯誤";
                }
            }
            else
            {
                Msg = "Bid格式錯誤";
            }
            return RedirectToAction("CampaignBlackList", new { msg = Msg });
        }

        [HttpPost]
        public ActionResult BlackImportByFile(HttpPostedFileBase upfile)
        {
            Guid Bid = new Guid();
            List<Guid> bids = new List<Guid>();
            int trueCount = 0, falseCount = 0;
            if (upfile != null)
            {
                using (StreamReader reader = new StreamReader(upfile.InputStream))
                {
                    do
                    {
                        string textLine = reader.ReadLine();

                        if (Guid.TryParse(textLine, out Bid))
                        {
                            ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(Bid);
                            if (vpd.IsLoaded)
                            {
                                bids.Add(Bid);
                                trueCount++;
                            }
                        }
                        else
                        {
                            falseCount++;
                        }
                    } while (reader.Peek() != -1);
                    reader.Close();
                }

                foreach (var bid in bids)
                {
                    DiscountLimitSet(bid);
                }
            }

            var msg = trueCount + "|" + falseCount;

            return RedirectToAction("CampaignBlackList", new { msg = msg });
        }

        [HttpPost]
        public ActionResult DelBlacklist(string list)
        {
            if (!string.IsNullOrEmpty(list))
            {
                var idList = list.Split(',');
                int id;
                foreach (var tmpId in idList)
                {
                    if (int.TryParse(tmpId, out id))
                    {
                        var tmpData = op.DiscountLimitGetById(id);
                        if (tmpData != null)
                        {
                            tmpData.Type = 0;
                            tmpData.ModifyId = UserName;
                            tmpData.ModifyTime = DateTime.Now;
                            op.DiscountLimitSet(tmpData);
                        }
                    }
                }
            }
            return RedirectToAction("CampaignBlackList");
        }


        private void DiscountLimitSet(Guid Bid)
        {
            DiscountLimit dl = new DiscountLimit();
            dl = op.DiscountLimitGetByBid(Bid);
            dl.Bid = Bid;
            dl.Type = 1;
            dl.GrossMargin = pp.ViewDealBaseGrossMarginGet(Bid).GrossMargin ?? 0;
            dl.CreateId = UserName;
            dl.CreateTime = DateTime.Now;
            dl.ModifyId = UserName;
            dl.ModifyTime = DateTime.Now;
            op.DiscountLimitSet(dl);
        }

        #endregion

        #region 折價券列表

        public ActionResult CampaignList(int? page)
        {
            int pageval = page.GetValueOrDefault();
            pageval = (pageval < 1) ? 1 : pageval;
            List<string> ll = new List<string>();
            ll.Add(ViewDiscountMain.Columns.User + " <> " + "Sys_StaffDiscount");
            ViewBag.CampaignMainList = op.ViewDiscountMainGetList(pageval, 20, "", ll.ToArray());
            ViewBag.CampaignCount = op.ViewDiscountMainGetCount(ll.ToArray());
            ViewBag.PageVal = pageval;
            return View();
        }

        [HttpPost]
        public ActionResult CampaignList(DiscountCodeListData data)
        {
            int pageVal = data.Page;
            List<string> ll = new List<string>();
            ll.Add(ViewDiscountMain.Columns.User + " <> " + "Sys_StaffDiscount");
            if (!string.IsNullOrEmpty(data.EventName))
            {
                ll.Add(ViewDiscountMain.Columns.Name + " like " + "%" + data.EventName + "%");
            }
            if (data.StartDate != null)
            {
                ll.Add(ViewDiscountMain.Columns.StartTime + " >= " + data.StartDate.Value.ToString("yyyy-MM-dd"));
            }
            if (data.EndDate != null)
            {
                ll.Add(ViewDiscountMain.Columns.EndTime + " <= " + data.EndDate.Value.ToString("yyyy-MM-dd"));
            }
            var campaignCount = op.ViewDiscountMainGetCount(ll.ToArray());
            ViewBag.CampaignCount = campaignCount;
            if (campaignCount > 0)
            {
                if (pageVal > Math.Ceiling((decimal)campaignCount / 20))
                {
                    pageVal = 1;
                }
            }
            ViewBag.CampaignMainList = op.ViewDiscountMainGetList(pageVal, 20, "", ll.ToArray());
            ViewBag.PageVal = pageVal;
            ViewBag.EventName = data.EventName;
            ViewBag.StartDate = data.StartDate;
            ViewBag.EndDate = data.EndDate;
            return View();
        }

        #endregion

        #region 活動折價券

        public ActionResult DiscountEventList(int? page, string msg)
        {
            if (!string.IsNullOrEmpty(msg))
            {
                ViewBag.message = msg;
            }
            List<string> filter = new List<string>();
            filter.Add(DiscountEvent.Columns.EventType + " = 0 ");
            int totalLogCount = op.DiscountEventGetCount(filter.ToArray());
            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            int pagnumber = Convert.ToInt16(page);
            ViewBag.EventName = "";
            ViewBag.txtSDate = "";
            ViewBag.txtEDate = "";
            ViewBag.PageIndex = pagnumber = (pagnumber == 0) ? 1 : pagnumber;
            ViewBag.EventList = op.DiscountEventGetByPage(pagnumber, 10, DiscountEvent.Columns.Id, filter.ToArray());
            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();

            return View();
        }

        [HttpPost]
        public ActionResult DiscountEventList(string eventName, string txtSDate, string txtEDate, int pageIndex)
        {
            ViewBag.EventName = "";
            ViewBag.txtSDate = "";
            ViewBag.txtEDate = "";
            List<string> filter = new List<string>();
            filter.Add(DiscountEvent.Columns.EventType + " = " + (int)DiscountActivityType.DiscountEvent);
            if (!string.IsNullOrEmpty(eventName))
            {
                filter.Add(DiscountEvent.Columns.Name + " like %" + eventName + "%");
                ViewBag.EventName = eventName;
            }
            DateTime start_date, end_date;
            if (!string.IsNullOrEmpty(txtSDate) && !DateTime.TryParse(txtSDate, out start_date))
            {
                filter.Add(DiscountEvent.Columns.StartTime + ">=" + start_date.ToString("yyyy-MM-dd HH:mm"));
                ViewBag.txtSDate = start_date.ToString("yyyy-MM-dd");
            }
            if (!string.IsNullOrEmpty(txtEDate) && !DateTime.TryParse(txtEDate, out end_date))
            {
                filter.Add(DiscountEvent.Columns.EndTime + "<=" + end_date.ToString("yyyy-MM-dd HH:mm"));
                ViewBag.txtSDate = end_date.ToString("yyyy-MM-dd");
            }

            int totalLogCount = op.DiscountEventGetCount(filter.ToArray());
            ViewBag.TotalCount = totalLogCount;
            ViewBag.PageCount = totalLogCount / 10;
            if (totalLogCount % 10 > 0)
            {
                ViewBag.PageCount = ViewBag.PageCount + 1;
            }

            pageIndex = (pageIndex == 0) ? 1 : pageIndex;
            ViewBag.EventList = op.DiscountEventGetByPage(pageIndex, 10, DiscountEvent.Columns.Id, filter.ToArray());
            ViewBag.PageIndex = pageIndex;
            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();

            return View();
        }

        public ActionResult EventCampaignSet(int? id)
        {
            int eventId = Convert.ToInt32(id);
            if (eventId == 0)
            {
                return RedirectToAction("DiscountEventList");
            }
            DiscountEvent item = new DiscountEvent();
            item = op.DiscountEventGet(eventId);
            var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
            int campaignid = 0;
            if (campaignList.Count > 0)
            {
                campaignid = campaignList.First().CampaignId;
            }
            var categoryList = op.DiscountCategoryGetList(campaignid);
            string channelstr = "";
            foreach (var category in categoryList)
            {
                channelstr += CategoryManager.CategoryGetById(category.CategoryId).Name + ",";
            }
            if (channelstr == "")
            {
                channelstr = "全站";
            }
            ViewBag.saveType = "Update";
            ViewBag.EventName = item.Name;
            ViewBag.EventId = item.Id;
            ViewBag.StartTime = item.StartTime;
            ViewBag.EndTime = item.EndTime;
            ViewBag.Channel = channelstr;
            ViewBag.campaignList = campaignList;
            return View();
        }

        public ActionResult NewEventCampaignSet()
        {
            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();
            return View();
        }

        [HttpPost]
        public ActionResult NewEventCampaignSet(EventCampaignData data)
        {
            ViewBag.categoryDictionary = PromotionFacade.GetCategorieList();
            return View();
        }

        public ActionResult EventCampaignSave()
        {
            return RedirectToAction("DiscountEventList");
        }

        [HttpPost]
        public ActionResult EventCampaignSave(FormCollection formValues)
        {
            var saveType = Request.Form["saveType"];
            var sDate = Request.Form["sDate"];
            var eDate = Request.Form["eDate"];
            var campaignFlag = Request.Form["campaignFlag"];
            var eventPromoId = Request.Form["eventPromoId"];
            var eventId = Convert.ToInt32(Request.Form["eventId"]);

            var chkAppOnly = Request.Form["chkAppOnly"] == null ? null : Request.Form["chkAppOnly"].Split(',');
            var discountName = Request.Form["discountName"] == null ? null : Request.Form["discountName"].Split(',');
            var discountAmount = Request.Form["discountAmount"] == null ? null : Request.Form["discountAmount"].Split(',');
            var discountMin = Request.Form["discountMin"] == null ? null : Request.Form["discountMin"].Split(',');
            var count = Request.Form["count"] == null ? null : Request.Form["count"].Split(',');
            var discountAll = Request.Form["discountAll"] == null ? null : Request.Form["discountAll"].Split(',');
            var campaignCode = Request.Form["campaignCode"] == null ? null : Request.Form["campaignCode"].Split(',');
            var campaignId = Request.Form["campaignId"] == null ? null : Request.Form["campaignId"].Split(',');

            if (saveType == "New")
            {
                DiscountEvent item = op.DiscountEventGet(eventId);

                int allQuantity = 0;
                DateTime start_date, end_date;
                Dictionary<int, int> eventCampaign = new Dictionary<int, int>();

                for (int i = 0; i < discountName.Count(); i++)
                {
                    Dictionary<int, string> channelId = new Dictionary<int, string>();
                    if (Request.Form["channelId" + i] != "")
                    {
                        var channelVal = Request.Form["channelId" + i] == null ? null : Request.Form["channelId" + i].TrimEnd(",");
                        var channelArr = channelVal == null ? null : channelVal.Split(',');
                        if (channelArr != null)
                        {
                            foreach (var channel in channelArr)
                            {
                                var tmp = channel == null ? null : channel.Split('|');
                                var str = channel.Replace("|", ",");
                                int cid = Convert.ToInt32(tmp[0]);
                                if (!string.IsNullOrEmpty(channel))
                                {
                                    channelId.Add(cid, str);
                                }
                            }
                        }
                    }
                    DiscountCampaign campaign = new DiscountCampaign();
                    DateTime.TryParse(sDate, out start_date);
                    DateTime.TryParse(eDate, out end_date);

                    var Name = discountName[i];
                    var amount = Convert.ToInt32(discountAmount[i]);
                    var c = Convert.ToInt32(count[i]);
                    var quantity = Convert.ToInt32(discountAll[i]);
                    var minimum_amount = Convert.ToInt32(discountMin[i]);
                    allQuantity += c;
                    campaign.Type = (int)DiscountCampaignType.PponDiscountTicket;
                    campaign.Name = Name;
                    campaign.Amount = amount;
                    campaign.Qty = quantity;
                    campaign.MinimumAmount = minimum_amount;
                    campaign.CreateId = this.UserName;
                    campaign.CreateTime = DateTime.Now;
                    campaign.StartTime = start_date;
                    campaign.EndTime = end_date;
                    campaign.Flag = Convert.ToInt32(campaignFlag) | (int)DiscountCampaignUsedFlags.MinimumAmount;
                    campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.Ppon;

                    if (chkAppOnly != null)
                    {
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.AppUseOnly;
                    }
                    if (channelId.Any())
                    {
                        campaign.Flag = campaign.Flag | (int)DiscountCampaignUsedFlags.CategoryLimit;
                    }
                    op.DiscountCampaignSet(campaign);

                    if ((campaign.Flag & (int)DiscountCampaignUsedFlags.InstantGenerate) == 0)
                    {
                        var code = string.Empty;
                        if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) > 0)
                        {
                            code = string.IsNullOrEmpty(campaignCode[i].Trim()) ? PromotionFacade.GetDiscountCode(campaign.Amount.Value) : campaignCode[i].Trim();
                        }

                        // 若為限本人使用勢必需要歸戶，因此程式改為歸戶時才產生資料
                        if ((campaign.Flag & (int)DiscountCampaignUsedFlags.OwnerUseOnly) == 0)
                        {
                            DiscountCodeCollection dataList = new DiscountCodeCollection();
                            for (var a = 0; a < campaign.Qty; a++)
                            {
                                DiscountCode dc = new DiscountCode();
                                dc.CampaignId = campaign.Id;
                                dc.Amount = campaign.Amount;
                                if ((campaign.Flag & (int)DiscountCampaignUsedFlags.SingleSerialKey) == 0)
                                {
                                    code = PromotionFacade.GetDiscountCode(dc.Amount.Value);
                                }
                                dc.Code = code;
                                dataList.Add(dc);
                            }

                            if (dataList.Count > 0)
                            {
                                op.DiscountCodeCollectionBulkInsert(dataList);
                            }

                        }
                    }

                    eventCampaign.Add(campaign.Id, c);

                    string channelstr = "";

                    foreach (var k in channelId)
                    {
                        if (k.Value.IndexOf(",") > 0)
                        {
                            var tmp = k.Value.Split(",");
                            foreach (var t in tmp)
                            {
                                if (!string.IsNullOrEmpty(t))
                                {
                                    channelstr += CategoryManager.CategoryGetById(Convert.ToInt32(t)).Name + ",";
                                    DiscountCategory dc = new DiscountCategory();
                                    dc.CampaignId = campaign.Id;
                                    dc.ChannelId = k.Key;
                                    dc.CategoryId = Convert.ToInt32(t);
                                    op.DiscountCategorySet(dc);
                                }
                            }
                        }
                        else
                        {
                            DiscountCategory dc = new DiscountCategory();
                            dc.CampaignId = campaign.Id;
                            dc.ChannelId = k.Key;
                            dc.CategoryId = Convert.ToInt32(k.Value);
                            channelstr += CategoryManager.CategoryGetById(Convert.ToInt32(k.Value)).Name + ",";
                            op.DiscountCategorySet(dc);
                        }
                    }

                    if (eventPromoId != null)
                    {
                        int tmpId = 0;
                        if (!int.TryParse(eventPromoId, out tmpId))
                        {
                            DiscountEventPromoLink promoLink = new DiscountEventPromoLink();
                            promoLink.CampaignId = campaign.Id;
                            promoLink.EventPromoId = tmpId;
                            op.DiscountEventPromoLinkSet(promoLink);
                        }
                    }

                }

                OrderFacade.SetDiscountEventCampaignList(eventId, eventCampaign);
                var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
                item.Qty = 0;
                item.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value); ;
                op.DiscountEventSet(item);

            }
            else
            {
                int i = 0;
                int allQuantity = 0;
                Dictionary<int, int> eventCampaignDictionary = new Dictionary<int, int>();
                foreach (var val in count)
                {
                    eventCampaignDictionary.Add(Convert.ToInt32(campaignId[i]), Convert.ToInt32(val));
                    allQuantity += Convert.ToInt32(val);
                    i++;
                }

                op.EventCampaignDeleteList(eventId);
                OrderFacade.SetDiscountEventCampaignList(eventId, eventCampaignDictionary);
                var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
                DiscountEvent item = op.DiscountEventGet(eventId);
                item.Qty = 0;
                item.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value);
                op.DiscountEventSet(item);

            }

            return RedirectToAction("DiscountEventEdit", new { id = eventId });

        }

        public ActionResult DiscountEventEdit(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);

            List<string> channelstrList = new List<string>();

            foreach (var campaign in campaignList)
            {
                var categoryList = op.DiscountCategoryGetList(campaign.CampaignId);
                string channelstr = categoryList.Aggregate("", (current, category) => current + (CategoryManager.CategoryGetById(category.CategoryId).Name + ","));
                if (channelstr == "")
                {
                    channelstr = "全站";
                }
                channelstrList.Add(channelstr);
            }

            ViewBag.campaignList = campaignList;
            ViewBag.usersCount = op.DiscountUserGetCount(id);
            ViewBag.sentQty = op.DiscountUserGetCount(id, DiscountUserStatus.Sent);
            ViewBag.shortageQty = op.DiscountUserGetCount(id, DiscountUserStatus.Shortage);
            ViewBag.totalAmount = string.Format("每人發送 ${0} 元", campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)).Value.ToString("N0"));
            ViewBag.Channel = channelstrList;
            return View(eventData);
        }

        public ActionResult DiscountEventImport(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            if (eventData.ApplyTime == null)
            {
                ApplyDiscountEvent(eventData);
            }
            OrderFacade.ImportDiscountUserForAllMembers(id, UserName);
            return RedirectToAction("DiscountEventEdit", new { id });
        }

        [HttpPost]
        public ActionResult DiscountEventImport(int id, HttpPostedFileBase upfile)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            if (eventData.ApplyTime == null)
            {
                ApplyDiscountEvent(eventData);
            }
            List<int> users = new List<int>();
            if (upfile != null)
            {
                using (StreamReader reader = new StreamReader(upfile.InputStream))
                {
                    do
                    {
                        string textLine = reader.ReadLine();
                        int userId = int.TryParse(textLine, out userId) ? userId : 0;
                        users.Add(userId);
                    } while (reader.Peek() != -1);
                    reader.Close();
                }
            }
            OrderFacade.ImportDiscountUsers(id, users, UserName);
            return RedirectToAction("DiscountEventEdit", new { id });
        }

        [HttpPost]
        public ActionResult DiscountEventSave(int id, string name, string sDate, string eDate, string shh, string smm, string ehh, string emm)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            DateTime date;
            eventData.Name = name;
            eventData.StartTime = DateTime.TryParse(sDate + " " + shh + ":" + smm, out date) ? date : DateTime.Now;
            eventData.EndTime = DateTime.TryParse(eDate + " " + ehh + ":" + emm, out date) ? date : DateTime.Now;
            eventData.ModifyId = UserName;
            eventData.ModifyTime = DateTime.Now;
            eventData.Status = (int)DiscountEventStatus.Initial;
            op.DiscountEventSet(eventData);

            return RedirectToAction("DiscountEventList");
        }

        public ActionResult DelEventCampaign(int id, int eventId, int campaignId)
        {
            op.DiscountEventCampaignDelete(id);
            DiscountCampaign dca = op.DiscountCampaignGet(campaignId);
            dca.ApplyTime = DateTime.Now;
            dca.ApplyId = UserName;
            //刪除後即失效
            dca.StartTime = DateTime.Now.AddDays(-2);
            dca.EndTime = DateTime.Now.AddDays(-1);

            op.DiscountCampaignSet(dca);
            var campaignList = op.ViewDiscountEventCampaignGetList(eventId);
            DiscountEvent item = op.DiscountEventGet(eventId);
            var sum = campaignList.Sum(x => x.Amount);
            if (sum != null)
                item.Qty = 0;
            var sum1 = campaignList.Sum(x => x.Amount * (x.EventQty ?? 0));
            if (sum1 != null)
                item.Total = Convert.ToInt32(sum1.Value);
            op.DiscountEventSet(item);
            return RedirectToAction("EventCampaignSet", new { id = eventId });
        }

        public ActionResult DelEvent(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            eventData.EventType = (int)DiscountActivityType.DeleteEvent;
            eventData.StartTime = DateTime.Now.AddDays(-2);
            eventData.EndTime = DateTime.Now.AddDays(-1);
            op.DiscountEventSet(eventData);
            var campaignList = op.ViewDiscountEventCampaignGetList(id);
            foreach (var campaign in campaignList)
            {
                DiscountCampaign dca = op.DiscountCampaignGet(campaign.CampaignId);
                dca.ApplyTime = DateTime.Now;
                dca.ApplyId = UserName;
                //刪除後即失效
                dca.StartTime = DateTime.Now.AddDays(-2);
                dca.EndTime = DateTime.Now.AddDays(-1);
                op.DiscountCampaignSet(dca);
            }
            op.EventCampaignDeleteList(id);
            return RedirectToAction("DiscountEventList");
        }

        public ActionResult ApplyEvent(int id)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            ApplyDiscountEvent(eventData);
            return RedirectToAction("DiscountEventList");
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult ImportCampaign(int id, int campaignId)
        {
            DiscountEvent eventData = op.DiscountEventGet(id);
            DiscountCampaign dc = op.DiscountCampaignGet(campaignId);
            if (dc != null)
            {
                if (eventData.StartTime.Date == dc.StartTime.Value.Date && eventData.EndTime.Date == dc.EndTime.Value.Date)
                {
                    Dictionary<int, int> eventCampaignDictionary = new Dictionary<int, int>();
                    eventCampaignDictionary.Add(Convert.ToInt32(campaignId), 1);
                    OrderFacade.SetDiscountEventCampaignList(id, eventCampaignDictionary);
                    var campaignList = op.ViewDiscountEventCampaignGetList(id);
                    eventData.Qty = 0;
                    eventData.Total = Convert.ToInt32(campaignList.Sum(x => x.Amount * (x.EventQty ?? 0)));
                    op.DiscountEventSet(eventData);
                    if (eventData.ApplyTime != null && dc.ApplyTime == null)
                    {
                        dc.ApplyTime = DateTime.Now;
                        dc.ApplyId = UserName;
                        op.DiscountCampaignSet(dc);
                        return Json("折價券已匯入成功,並自動啟用");
                    }
                    return Json("折價券已匯入成功");
                }
                else
                {
                    return Json("折價券時間與活動時間不同，無法匯入");
                }
            }
            else
            {
                return Json("無此折價券，請輸入正確ID");
            }
        }

        private void ApplyDiscountEvent(DiscountEvent eventData)
        {
            var campaignList = op.ViewDiscountEventCampaignGetList(eventData.Id);
            eventData.ApplyTime = DateTime.Now;
            op.DiscountEventSet(eventData);
            foreach (var campaign in campaignList)
            {
                DiscountCampaign dca = op.DiscountCampaignGet(campaign.CampaignId);
                dca.ApplyTime = DateTime.Now;
                dca.ApplyId = UserName;
                op.DiscountCampaignSet(dca);
            }
        }

        #endregion 活動折價券

        #region 新會員折價券設定

        public ActionResult NewMemberSet()
        {
            string dataName = "NewMemberDiscountCode";
            var data = ss.SystemDataGet(dataName);

            JArray ja = new JArray();
            var discountList = data.Data.Split('|');
            foreach (var discount in discountList)
            {
                var tmpStr = discount.Split('#');
                int discountId, discountQua;
                if (int.TryParse(tmpStr[0], out discountId) && int.TryParse(tmpStr[1], out discountQua))
                {
                    JObject jo = new JObject
                    {
                            {"Id", discountId},
                            {"Quantity", discountQua},
                    };
                    ja.Add(jo);
                }
            }
            ViewBag.Discount = ja;
            return View();
        }

        [HttpPost]
        public ActionResult NewMemberSet(string discountStr)
        {
            string dataName = "NewMemberDiscountCode";
            var data = ss.SystemDataGet(dataName);
            data.Data = discountStr;
            data.ModifyId = UserName;
            data.ModifyTime = DateTime.Now;
            ss.SystemDataSet(data);
            return Redirect("/ControlRoom/System/DiscountEventList?msg=更新成功");
        }

        #endregion

        #region Ajax

        //商品主題活動
        [HttpPost]
        [AjaxCall]
        public ActionResult GetEventPromoId(int id = 0)
        {
            BrandAjaxData data = new BrandAjaxData();
            if (id == 0)
            {
                data.Status = 0;
                data.Msg = "請確認商品主題活動ID";
            }
            else
            {
                EventPromo eventPromo = pp.GetEventPromo(id);
                if (!eventPromo.IsLoaded)
                {
                    data.Status = 0;
                    data.Msg = "查無此商品主題活動ID";
                }
                else
                {
                    data.Status = 1;
                    data.EventId = eventPromo.Id;
                    data.EventName = eventPromo.Title;
                    data.Msg = "OK";
                }
            }
            return Json(data);
        }

        //策展
        [HttpPost]
        [AjaxCall]
        public ActionResult GetBrandId(int id = 0)
        {
            BrandAjaxData data = new BrandAjaxData();
            if (id == 0)
            {
                data.Status = 0;
                data.Msg = "請確認策展版型活動ID";
            }
            else
            {
                DataOrm.Brand brand = pp.GetBrand(id);
                if (!brand.IsLoaded)
                {
                    data.Status = 0;
                    data.Msg = "查無此策展版型活動ID";
                }
                else
                {
                    data.Status = 1;
                    data.EventId = brand.Id;
                    data.EventName = brand.BrandName;
                    data.Msg = "OK";
                }
            }
            return Json(data);
        }

        //單檔
        [HttpPost]
        [AjaxCall]
        public ActionResult GetPromoIdByBid(string bid, int evenId = 0)
        {
            Guid Bid;
            BrandAjaxData data = new BrandAjaxData();
            if (Guid.TryParse(bid.Trim(), out Bid))
            {
                #region 如若輸入的是bid，則自動建立一筆商品主題活動

                ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(Bid);
                if (vpd.IsLoaded)
                {
                    if (!Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                    {
                        if (evenId == 0)
                        {
                            DateTime start_date = vpd.BusinessHourOrderTimeS;
                            DateTime end_date = vpd.BusinessHourOrderTimeE;

                            EventPromo promo = new EventPromo();
                            promo.Title = vpd.ItemName;
                            promo.Url = string.Format("discount_{0}", vpd.UniqueId);
                            promo.StartDate = start_date;
                            promo.EndDate = end_date;
                            promo.BgColor = "FFFFFF";
                            promo.Status = true;
                            promo.Creator = UserName;
                            promo.Cdt = DateTime.Now;
                            promo.TemplateType = (int)EventPromoTemplateType.PponOnly;
                            promo.ShowInWeb = true;
                            MarketingFacade.SaveCurationAndResetMemoryCache(promo);

                            EventPromoItem item = new EventPromoItem();
                            item.MainId = promo.Id;
                            item.Title = vpd.ItemName;
                            item.Description = vpd.EventTitle ?? string.Empty;
                            item.Seq = 1;
                            item.Status = true;
                            item.Creator = UserName;
                            item.Cdt = DateTime.Now;
                            item.ItemId = vpd.UniqueId.Value;
                            item.ItemType = (int)EventPromoItemType.Ppon;
                            pp.SaveEventPromoItem(item);

                            data.Status = 1;
                            data.EventId = promo.Id;
                            data.EventName = vpd.ItemName;
                            data.UniqueId = vpd.UniqueId.Value;
                            data.GrossMargin = pp.ViewDealBaseGrossMarginGet(vpd.BusinessHourGuid).GrossMargin ?? 0;
                            data.Msg = "OK";

                        }
                        else
                        {
                            EventPromoItem item = new EventPromoItem();
                            item.MainId = evenId;
                            item.Title = vpd.ItemName;
                            item.Description = vpd.EventTitle ?? string.Empty;
                            item.Seq = 1;
                            item.Status = true;
                            item.Creator = UserName;
                            item.Cdt = DateTime.Now;
                            item.ItemId = vpd.UniqueId.Value;
                            item.ItemType = (int)EventPromoItemType.Ppon;
                            pp.SaveEventPromoItem(item);

                            data.Status = 1;
                            data.EventId = evenId;
                            data.EventName = vpd.ItemName;
                            data.UniqueId = vpd.UniqueId.Value;
                            data.GrossMargin = pp.ViewDealBaseGrossMarginGet(vpd.BusinessHourGuid).GrossMargin ?? 0;
                            data.Msg = "OK";
                        }
                    }
                    else
                    {
                        data.Status = 0;
                        data.Msg = "無法綁定子檔次，請輸入母擋bid";
                    }
                }
                else
                {
                    data.Status = 0;
                    data.Msg = "bid錯誤，無法綁定";
                }

                #endregion
            }
            else
            {
                data.Status = 0;
                data.Msg = "bid錯誤，無法綁定";
            }
            return Json(data);
        }

        [HttpPost]
        [AjaxCall]
        public ActionResult DelPromoItem(int uniqueId, int evenId)
        {
            BrandAjaxData data = new BrandAjaxData();
            var tmpItem = pp.GetEventPromoItem(evenId, uniqueId);
            if (tmpItem != null)
            {
                pp.DeleteEventPromoItem(tmpItem);
                data.Status = 1;
                data.Msg = "OK";
            }
            else
            {
                data.Status = 0;
                data.Msg = "無此資料";
            }
            return Json(data);
        }


        #endregion
    }
}
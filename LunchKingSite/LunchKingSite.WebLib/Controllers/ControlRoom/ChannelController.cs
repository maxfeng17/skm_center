﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using LunchKingSite.BizLogic.Facade;
using Newtonsoft.Json;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.BizLogic.Models.Channel;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebLib.Models.ControlRoom.Channel;
using LunchKingSite.WebLib.Models.ControlRoom.System;
using Newtonsoft.Json.Linq;
using LunchKingSite.WebLib.Models.ControlRoom.Seller;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace LunchKingSite.WebLib.Controllers.ControlRoom
{
    [RolePage("/ControlRoom/Channel", SystemFunctionType.Read)]
    public class ChannelController : ControllerExt
    {
        private static IOrderProvider _orderProv;
        private static IChannelProvider _channelProv;
        private static ISellerProvider _sellerProv;
        private static IPponProvider _pponProv;
        private static ISysConfProvider _config;

        public ChannelController()
        {
            _orderProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _channelProv = ProviderFactory.Instance().GetProvider<IChannelProvider>();
            _sellerProv = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            _pponProv = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
        }

        #region TaishinMallBanner
        /// <summary>
        /// 台新商城Banner管理系統Query
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult TaishinMallBanner(Guid? id)
        {
            var bannerSystemData = ChannelFacade.GetTaishinMallBanner(true);
            List<TaishinMallBannerDDL> tempList = new List<TaishinMallBannerDDL>();
            if (bannerSystemData == null || !bannerSystemData.Any())
            {
                if(bannerSystemData == null)
                {
                    ViewBag.IsFirstAdd = "Y";                    
                }               
            }
            else
            {
                foreach (var item in bannerSystemData)
                {
                    item.Body = item.Body.Replace("\r\n", "");
                    item.Body = item.Body.Replace(@"""", "'");                    
                }
                ViewBag.Data = JsonConvert.SerializeObject(bannerSystemData);
                foreach (var item in bannerSystemData)
                {
                    tempList.Add(new TaishinMallBannerDDL
                    {
                        Id=item.Id,
                        Title = item.Title,
                        StartDate = item.StartDate,
                        EndDate = item.EndDate,
                        DisplayText = string.Format("{0} ({1}~{2})", item.Title, item.StartDate.ToString("yyyy/MM/dd"), item.EndDate.ToString("yyyy/MM/dd"))
                    });
                }
            }
            tempList.Add(new TaishinMallBannerDDL
            {
                DisplayText = "請選擇"
            });
            SelectList bannerList = new SelectList(tempList.OrderByDescending(x => x.StartDate), "Id", "DisplayText");

            ViewBag.TaishinMallBannerList = bannerList;
            TaishinMallBanner model = new TaishinMallBanner();
            if (id != null)
            {
                var data = bannerSystemData.Where(x => x.Id == id).FirstOrDefault();
                if(data != null)
                {
                    model.Id = data.Id;
                    model.Title = data.Title;
                    model.Body = data.Body;
                    model.StartDate = Convert.ToDateTime(data.StartDate);
                    model.EndDate = Convert.ToDateTime(data.EndDate);
                    ViewBag.IsEdit = "";
                }                              
                return View(model);
            }
            else
            {
                model.StartDate = DateTime.Today;
                model.EndDate = DateTime.Today.AddDays(1);
                ViewBag.IsNew = "";
            }
            return View(model);
        }
        
        /// <summary>
        /// 台新商城Banner管理系統-儲存
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public JsonResult TaishinMallBannerSave(string data,string status)
        {
            //新增TaishinBanner資料列
            if (string.IsNullOrEmpty(data))
            {
                return Json(new { Success = false, Message = "Data為空值，請聯絡技術部。" });
            }

            SystemData systemdata = SystemCodeManager.GetSystemDataByName("TaishinMallBanner");
            if (systemdata.IsLoaded == false)
            {
                systemdata = new SystemData
                {
                    CreateId = User.Identity.Name,
                    CreateTime = DateTime.Now,
                    Name = "TaishinMallBanner",
                    Data = data
                };
            }
            else
            {
                TaishinMallBanner jsonData = JsonConvert.DeserializeObject<TaishinMallBanner>(data);
                var bannerDataList = ChannelFacade.GetTaishinMallBanner();
                var updateData = bannerDataList.Where(x => x.Id == jsonData.Id).FirstOrDefault();
                if (updateData != null)
                {
                    if (status == "d")
                    {
                        bannerDataList.Remove(updateData);
                    }
                    else
                    {
                        updateData.Title = jsonData.Title;
                        updateData.StartDate = jsonData.StartDate;
                        updateData.EndDate = jsonData.EndDate;
                        updateData.Body = jsonData.Body;
                        updateData.Id = jsonData.Id;
                    }                    
                }
                else
                {
                    bannerDataList.Add(jsonData);
                }
                systemdata.Data = JsonConvert.SerializeObject(bannerDataList);
                systemdata.ModifyId = User.Identity.Name;
                systemdata.ModifyTime = DateTime.Now;
            }
            bool settingResult = SystemCodeManager.SetSystemData(systemdata);

            if (!settingResult)
            {
                return Json(new { Success = false, Message = "更新失敗，請聯絡技術部。" });
            }
            return Json(new { Success = true, Message = "更新成功，稍後將反映到前台！" });
        }

        #endregion TaishinMallBanner

        #region MasterpassBankLog

        [Authorize(Roles = "Administrator, CustomerCare,Planning")]
        public ActionResult MasterpassBankLog(int? bankId, string startDate, string endDate)
        {
            //下拉式選單
            var bankInfo = _orderProv.CreditcardBankGetList();
            ViewMasterpassBankLogCollection allCol = ChannelFacade.GetMasterpassBankLogCol(null);
            var bankLogList = bankInfo.Where(x => allCol.Select(y => y.BankId).Contains(x.Id));
            SelectList bankList = new SelectList(bankLogList, "Id", "BankName");
            ViewBag.bankList = bankList;
            //預設為一個月內
            startDate = string.IsNullOrEmpty(startDate) ? DateTime.Now.AddMonths(-1).ToString("yyyy/MM/dd HH:mm") : startDate;
            endDate = string.IsNullOrEmpty(endDate) ? DateTime.Now.ToString("yyyy/MM/dd HH:mm") : endDate;
            var logCol = bankId.HasValue && bankId != 0 ? allCol.Where(x=>x.BankId==bankId).ToList():allCol.ToList();            
            ViewBag.List=logCol.Where(x=>x.CreateTime >= Convert.ToDateTime(string.Format("{0}:{1}", startDate,"00")) && x.CreateTime < Convert.ToDateTime(string.Format("{0}:{1}", endDate, "00"))).OrderByDescending(x=>x.CreateTime).ToList();            
            logCol.Where(x => x.Indicator != "error" && x.BankId==bankId ).OrderByDescending(x => x.CreateTime).ToList();
            //近七天筆數
            Dictionary<string, int> weekDatas = new Dictionary<string, int>()
            {
                {"24小時內",0 }
            };

            foreach (var item in logCol)
            {
                if(item.CreateTime >= DateTime.Now.AddHours(-24))
                {
                    weekDatas["24小時內"] += 1;
                }
                for(int weekIndex=0; weekIndex < 7; weekIndex++)
                {
                    if (!weekDatas.ContainsKey(DateTime.Now.AddDays(-weekIndex).Date.ToString("yyyy/MM/dd")))
                    {
                        weekDatas.Add(DateTime.Now.AddDays(-weekIndex).Date.ToString("yyyy/MM/dd"), 0);
                    }
                    if (item.CreateTime.Date == DateTime.Now.AddDays(-weekIndex).Date)
                    {                        
                        weekDatas[DateTime.Now.AddDays(-weekIndex).Date.ToString("yyyy/MM/dd")] += 1;
                    }                    
                }                
            }
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            ViewBag.SpData = weekDatas;
            ViewBag.Bank = bankId.HasValue && bankId!=0 ? logCol.Select(x => x.BankName).FirstOrDefault():"全部";
            ViewBag.Id = bankId;
            return View();
        }
        #endregion

        #region ichannel & shopback 分潤設定

        /// <summary>
        /// 後台設定頁
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult CommissionSetting(byte channel = 1)
        {
            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Read))
            {
                Response.Redirect(_config.SSLSiteUrl);

            }
            var source = (ChannelSource) channel;
            if (source == ChannelSource.Default)
            {
                return View(new CommissionRuleModel());
            }

            var rules = _channelProv.GetChannelCommissionRules(source);
            var couponDefault = rules.FirstOrDefault(x => x.RuleType == (byte) CommissionRuleType.CouponDefault);
            var deliveryDefault = rules.FirstOrDefault(x => x.RuleType == (byte) CommissionRuleType.DeliveryDefault);

            var model = new CommissionRuleModel
            {
                Source = (ChannelSource)channel,
                AgentChannel = (AgentChannel)channel,
                CouponDefaultCode = couponDefault != null ? couponDefault.ReturnCode : string.Empty,
                CouponDefaultValue = couponDefault != null ? couponDefault.GrossMarginLimit : 0,
                DeliveryDefaultCode = deliveryDefault != null ? deliveryDefault.ReturnCode : string.Empty,
                DeliveryDefaultValue = deliveryDefault != null ? deliveryDefault.GrossMarginLimit : 0,
                DealTypeSystemCode = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null && x.CodeId != 1999).OrderBy(x => x.Seq).ToList()
            };
            
            return View(model);
        }

        /// <summary>
        /// 取得業績歸屬子分類
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult GetSubDealType(GetSubDealTypeModel model)
        {
            return Json(new {data = SystemCodeManager.GetDealType()
                    .Where(x => x.ParentCodeId == model.MainDealType)
                    .OrderBy(x => x.Seq)
                    .ToList()});
        }

        /// <summary>
        /// 取得所有已設定的一般條件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult CommissionRuleList(CommissionRuleListModel model)
        {
            var source = (ChannelSource)model.Source;
            if (source == ChannelSource.Default)
            {
                return Json(new { Success = false, Message = "請選擇通路商", Data = new List<ChannelCommissionRule>() });
            }

            var rules = _channelProv.GetChannelCommissionRules(source).Where(x=>x.RuleType == (byte)CommissionRuleType.NormalCondition)
                .Select(x=>new RuleData(x)).OrderBy(x=>x.Id);
            return Json(new { Success = true, Message = string.Empty, Data = rules });
        }

        /// <summary>
        /// 取得一筆已設定的一般條件
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult CommissionRuleGet(CommissionRuleListModel model)
        {
            var source = (ChannelSource)model.Source;
            if (source == ChannelSource.Default)
            {
                return Json(new { Success = false, Message = "請選擇通路商", Data = new ChannelCommissionRule() });
            }

            var rule = _channelProv.GetChannelCommissionRule(model.Id);

            var sc = SystemCodeManager.GetDealType()
                .FirstOrDefault(x => x.CodeId == rule.DealType);

            return Json(new { Success = true, Message = string.Empty, Data = rule, dealType = sc == null ? 0 : sc.ParentCodeId });
        }

        /// <summary>
        /// 設定預設分潤條件(在地/宅配)
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult CommissionSettingDefault(CommissionDefaultSetting model)
        {
            var source = (ChannelSource)model.Source;
            if (source == ChannelSource.Default)
            {
                return Json(new { Success = false, Message = "請選擇通路商" });
            }

            var rules = _channelProv.GetChannelCommissionRules(source);
            var couponDefault = rules.FirstOrDefault(x => x.RuleType == (byte) CommissionRuleType.CouponDefault) ??
                                new ChannelCommissionRule
                                {
                                    ChannelSource = (byte)source,
                                    DealType = 0,
                                    IsDeleted = false,
                                    RuleType = (byte)CommissionRuleType.CouponDefault
                                };
            var deliveryDefault = rules.FirstOrDefault(x => x.RuleType == (byte) CommissionRuleType.DeliveryDefault) ??
                                  new ChannelCommissionRule
                                  {
                                      ChannelSource = (byte)source,
                                      DealType = 0,
                                      IsDeleted = false,
                                      RuleType = (byte)CommissionRuleType.DeliveryDefault
                                  };

            couponDefault.ReturnCode = model.CouponCode;
            couponDefault.GrossMarginLimit = model.CouponVal;
            deliveryDefault.ReturnCode = model.DeliveryCode;
            deliveryDefault.GrossMarginLimit = model.DeliveryVal;

            if (_channelProv.InsertOrUpdateChannelCommissionRule(new List<ChannelCommissionRule>
                {
                    couponDefault,
                    deliveryDefault
                }))
            {
                return Json(new {Success = true, Message = "儲存成功" });
            }
            
            return Json(new { Success = false, Message = "儲存失敗" });            
        }

        /// <summary>
        /// 儲存一般規則
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult CommissionSettingRule(AddRuleModel model)
        {
            var source = (ChannelSource)model.Source;
            if (source == ChannelSource.Default)
            {
                return Json(new { Success = false, Message = "儲存失敗" });
            }
            var ok = _channelProv.InsertOrUpdateChannelCommissionRule(source, model.DealType, model.Keyword, model.GrossMarginLimit, model.ReturnCode, model.Id);
            return Json(new { Success = ok, Message = ok ? "儲存成功" : "儲存失敗" });
        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionSettingRuleDel(RuleDelModel model)
        {
            var source = (ChannelSource)model.Source;
            if (source == ChannelSource.Default)
            {
                return Json(new { Success = false });
            }

            return Json(new {Success = _channelProv.DeleteChannelCommissionRule(model.Id)});
        }

        [HttpPost]
        [Authorize(Roles = "Administrator")]
        public JsonResult TestiChannelCommission(ChannelCommissionTestModel model)
        {
            var result = OrderFacade.IChannelAPI(model.StartDate, model.EndDate, true);
            return Json(new { Data = result });
        }


        /// <summary>
        /// 代銷導購過檔設定
        /// </summary>
        /// <param name="channel"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public ActionResult CommissionListSetting()
        {
            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Read))
            {
                Response.Redirect(_config.SSLSiteUrl);

            }
            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionListGet(int channel)
        {
            var list = _channelProv.ChannelCommissionListGet(channel);

            List<CommissionDetailList> black = new List<CommissionDetailList>();
            List<CommissionDetailList> white = new List<CommissionDetailList>();

            if (list.Any())
            {
                List<ChannelCommissionList>  blackList = list.Where(x => x.Type == (int)CommissionListType.BlackList).ToList();
                List<ChannelCommissionList>  whiteList = list.Where(x => x.Type == (int)CommissionListType.WhiteList).ToList();

                foreach (ChannelCommissionList bl in blackList)
                {
                    CommissionDetailList detail = new CommissionDetailList();
                    if (bl.SellerGuid != null && bl.MainBid == null &&  bl.Bid == null)
                    {
                        //商家
                        Seller s = _sellerProv.SellerGet((Guid)bl.SellerGuid);
                        detail.ID = bl.Id;
                        detail.Guid = s.SellerId;
                        detail.Name = s.SellerName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/seller/seller_add.aspx?sid=" + s.Guid;
                    }
                    else if (bl.MainBid != null && bl.Bid == null)
                    {
                        //母檔
                        DealProperty dp = _pponProv.DealPropertyGet((Guid)bl.MainBid);
                        Item item = _pponProv.ItemGetByBid((Guid)bl.MainBid);
                        detail.ID = bl.Id;
                        detail.Guid = dp.UniqueId.ToString();
                        detail.Name = item.ItemName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/ppon/setup.aspx?bid=" + dp.BusinessHourGuid;
                    }
                    else if (bl.Bid != null)
                    {
                        //子檔or單檔
                        DealProperty dp = _pponProv.DealPropertyGet((Guid)bl.Bid);
                        Item item = _pponProv.ItemGetByBid((Guid)bl.Bid);
                        detail.ID = bl.Id;
                        detail.Guid = dp.UniqueId.ToString();
                        detail.Name = item.ItemName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/ppon/setup.aspx?bid=" + dp.BusinessHourGuid;
                    }
                    black.Add(detail);
                }
                foreach (ChannelCommissionList wl in whiteList)
                {
                    CommissionDetailList detail = new CommissionDetailList();
                    if (wl.SellerGuid != null && wl.MainBid == null &&  wl.Bid == null)
                    {
                        //商家
                        Seller s = _sellerProv.SellerGet((Guid)wl.SellerGuid);
                        detail.ID = wl.Id;
                        detail.Guid = s.SellerId;
                        detail.Name = s.SellerName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/seller/seller_add.aspx?sid=" + s.Guid;
                    }
                    else if (wl.MainBid != null && wl.Bid == null)
                    {
                        //母檔
                        DealProperty dp = _pponProv.DealPropertyGet((Guid)wl.MainBid);
                        Item item = _pponProv.ItemGetByBid((Guid)wl.MainBid);
                        detail.ID = wl.Id;
                        detail.Guid = dp.UniqueId.ToString();
                        detail.Name = item.ItemName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/ppon/setup.aspx?bid=" + dp.BusinessHourGuid;
                    }
                    else if (wl.Bid != null)
                    {
                        //子檔or單檔
                        DealProperty dp = _pponProv.DealPropertyGet((Guid)wl.Bid);
                        Item item = _pponProv.ItemGetByBid((Guid)wl.Bid);
                        detail.ID = wl.Id;
                        detail.Guid = dp.UniqueId.ToString();
                        detail.Name = item.ItemName;
                        detail.Href = _config.SSLSiteUrl + "/ControlRoom/ppon/setup.aspx?bid=" + dp.BusinessHourGuid;
                    }
                    white.Add(detail);
                }
            }
            

            var model = new CommissionListSettingModel
            {
                Source = channel,
                BlackList = black,
                WhiteList = white,
            };


            return Json(new { Data = model });
        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionLisCheck(int channel, string id, int type)
        {
            if (!string.IsNullOrEmpty(id))
            {
                int uniqueId = 0;
                int.TryParse(id, out uniqueId);


                if (uniqueId != 0)
                {
                    //檔次
                    DealProperty dp = _pponProv.DealPropertyGet(uniqueId);
                    if (dp.IsLoaded)
                    {
                        BusinessHour bh = _pponProv.BusinessHourGet(dp.BusinessHourGuid);
                        if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                        {
                            //母檔
                            ChannelCommissionList list = _channelProv.ChannelCommissionListGetByMainBid(channel, dp.BusinessHourGuid);
                            if (list.IsLoaded)
                            {
                                return Json(new { Success = false, Message = "該檔次資料重複存在，請重新確認" });
                            }
                        }
                        else
                        {
                            //子檔or單檔
                            ChannelCommissionList list = _channelProv.ChannelCommissionListGetByBid(channel, dp.BusinessHourGuid);
                            if (list.IsLoaded)
                            {
                                return Json(new { Success = false, Message = "該檔次資料重複存在，請重新確認" });
                            }
                        }

                    }
                    else
                    {
                        return Json(new { Success = false, Message = "找不到該資料" });

                    }

                }
                else
                {
                    //商家編號
                    Seller s = _sellerProv.SellerGet(Seller.Columns.SellerId, id);
                    if (s.IsLoaded)
                    {
                        ChannelCommissionList list = _channelProv.ChannelCommissionListGetBySid(channel, s.Guid);
                        if (list.IsLoaded)
                        {
                            return Json(new { Success = false, Message = "該商家資料重複存在，請重新確認" });
                        }
                    }
                    else
                    {
                        return Json(new { Success = false, Message = "找不到該資料" });

                    }

                }
            }
            else
            {
                return Json(new { Success = false, Message = "請輸入商家編號或檔號" });
            }
            


            return Json(new { Success = true, Message = "可以加入" });
        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionLisAdd(int channel, string id, int type)
        {
            int uniqueId = 0;
            int.TryParse(id, out uniqueId);


            ChannelCommissionList list = new ChannelCommissionList();
            if (uniqueId != 0)
            {
                //檔次
                DealProperty dp = _pponProv.DealPropertyGet(uniqueId);
                BusinessHour bh = _pponProv.BusinessHourGet(dp.BusinessHourGuid);
                if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                {
                    list.SellerGuid = bh.SellerGuid;
                    list.MainBid = dp.BusinessHourGuid;
                }
                else if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                {
                    ComboDeal cd = _pponProv.GetComboDeal(dp.BusinessHourGuid);
                    list.SellerGuid = bh.SellerGuid;
                    list.MainBid = cd.MainBusinessHourGuid;
                    list.Bid = dp.BusinessHourGuid;
                }
                else
                {
                    list.SellerGuid = bh.SellerGuid;
                    list.Bid = dp.BusinessHourGuid;
                }


                list.ChannelSource = channel;
                list.Type = type;
                list.CreateId = UserName;
                list.CreateTime = DateTime.Now;
                _channelProv.ChannelCommissionListSet(list);
            }
            else
            {
                //商家編號
                //檔次
                Seller s = _sellerProv.SellerGet(Seller.Columns.SellerId, id);
                list.ChannelSource = channel;
                list.SellerGuid = s.Guid;
                list.Type = type;
                list.CreateId = UserName;
                list.CreateTime = DateTime.Now;
                _channelProv.ChannelCommissionListSet(list);
            }

            return Json(new { Success = true, Message = "加入成功" });
        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionLisDelete(int id)
        {
            _channelProv.ChannelCommissionListDelete(id);
            return Json(new { Success = true, Message = "刪除成功" });
        }

        [HttpGet]
        [Authorize]
        public ActionResult CommissionSpecialSetting()
        {

            return View();
        }

        [HttpPost]
        [Authorize]
        public JsonResult ChannelSpecialCommissionGet(int channel, int uniqueId, int pageStart)
        {
            int pageLength = 100;
            List<CommissionSpecialSettingModel> model = new List<CommissionSpecialSettingModel>();
            List<ChannelSpecialCommission> specials = _channelProv.ChannelSpecialCommissionGetByChannel(channel).ToList();
            if (uniqueId != 0)
            {
                DealProperty dp = _pponProv.DealPropertyGet(uniqueId);
                specials = specials.Where(x => x.Bid == dp.BusinessHourGuid).ToList();
            }
            List<ChannelSpecialCommission> data = specials.Skip((pageStart - 1) * pageLength).Take(pageLength).ToList();
            List<SystemCode> dealTypes = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId != null && x.CodeId != 1999).ToList();


            foreach (ChannelSpecialCommission special in data)
            {
                CommissionSpecialSettingModel s = new CommissionSpecialSettingModel();
                ViewPponDeal deal = _pponProv.ViewPponDealGetByBusinessHourGuid(special.Bid);
                s.UniqueId = deal.UniqueId ?? 0;
                s.ItemName = deal.ItemName;
                if (channel == (int)AgentChannel.EzTravel)
                    s.OriCommission = "6%";
                else if (channel == (int)AgentChannel.PChome)
                {
                    SystemCode dealType = dealTypes.Where(x => x.CodeId == (int)deal.DealType).FirstOrDefault();
                    if (dealType.ParentCodeId == 1001 && dealType.CodeName.Contains("餐廳_吃到飽"))
                        s.OriCommission = "2%";
                    else if (dealType.ParentCodeId == 1001 && !dealType.CodeName.Contains("餐廳_吃到飽"))
                        s.OriCommission = "4%";
                    else if (dealType.ParentCodeId == 1009)
                        s.OriCommission = "9%";
                    else if (dealType.ParentCodeId == 1011)
                        s.OriCommission = "8%";
                    else if (dealType.ParentCodeId == 1010 && dealType.CodeName.Contains("住宿"))
                        s.OriCommission = "4.2%";
                    else if (dealType.ParentCodeId == 1010 && dealType.CodeName.Contains("門票"))
                        s.OriCommission = "2.5%";
                    else
                        s.OriCommission = "";
                }
                s.ItemPrice = deal.ItemPrice;
                s.SpecialCost = special.Cost != null ? special.Cost.ToString() : "";
                s.SpecialCommission = (special.Commission * 100).ToString() + "%";

                model.Add(s);
            }
            int page = (specials.Count / pageLength) + (specials.Count % pageLength != 0 ? 1 : 0);



            return Json(new { Success = true, Data = model, TotalCount = specials.Count, TotalPage = page, CurrentPage = pageStart });

        }

        [HttpPost]
        [Authorize]
        public JsonResult ChannelSpecialCommissionInsert(int channel, string strUniqueId, string strCost, string strCommission)
        {
            bool success = true;
            string errMessage = "";
            int uniqueId = 0;
            decimal cost = 0;
            decimal commission = 0;
            if (!int.TryParse(strUniqueId, out uniqueId))
            {
                success = false;
                errMessage = "檔號不正確";
            }
            else if (channel != (int)AgentChannel.EzTravel && !decimal.TryParse(strCost, out cost))
            {
                success = false;
                errMessage = "特談成本不正確";
            }
            else if (!decimal.TryParse(strCommission, out commission))
            {
                success = false;
                errMessage = "特談分潤不正確";
            }
            else
            {
                DealProperty dp = _pponProv.DealPropertyGet(uniqueId);
                if (!dp.IsLoaded)
                {
                    success = false;
                    errMessage = "檔號不正確";
                }
                else
                {
                    BusinessHour bh = _pponProv.BusinessHourGet(dp.BusinessHourGuid);
                    if (Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealMain))
                    {
                        success = false;
                        errMessage = "不允許匯入母檔";
                    }
                    else
                    {
                        ChannelSpecialCommission OldSpecial = _channelProv.ChannelCommissionListGetByChannelBid(channel, bh.Guid);
                        if (OldSpecial.IsLoaded)
                        {
                            OldSpecial.Cost = cost == 0 ? default(decimal?) : cost;
                            OldSpecial.Commission = commission / 100;
                            OldSpecial.ModifyTime = DateTime.Now;
                            OldSpecial.ModifyId = UserName;
                            _channelProv.ChannelSpecialCommissionSet(OldSpecial);
                        }
                        else
                        {
                            ChannelSpecialCommission newSpecial = new ChannelSpecialCommission();
                            newSpecial.ChannelSource = channel;
                            newSpecial.Bid = bh.Guid;
                            newSpecial.Cost = cost == 0 ? default(decimal?) : cost;
                            newSpecial.Commission = commission / 100;
                            newSpecial.CreateTime = DateTime.Now;
                            newSpecial.CreateId = UserName;
                            _channelProv.ChannelSpecialCommissionSet(newSpecial);

                        }
                    }
                }
            }

            if (success)
                return Json(new { Success = true });
            else
                return Json(new { Success = false, Message = errMessage });

        }

        [HttpPost]
        [Authorize]
        public JsonResult CommissionSpecialUpload(int channel)
        {
            bool success = true;
            string errMessage = "";
            ChannelSpecialCommissionCollection specials = new ChannelSpecialCommissionCollection();
            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                if (file.FileName.Split('.')[1] != "xls")
                {
                    success = false;
                    errMessage = "檔案格式僅能.xls";
                    break;
                }
                HSSFWorkbook hssfworkbook = new HSSFWorkbook(file.InputStream);
                Sheet sheet = hssfworkbook.GetSheetAt(0);
                Row row;

                for (int k = 5; k <= sheet.LastRowNum; k++)
                {
                    row = sheet.GetRow(k);
                    if (row != null)
                    {
                        string strUniqueId = row.GetCell(0).ToString();
                        string strCost = row.GetCell(1).ToString();
                        string strCommission = row.GetCell(2).ToString().Replace("%", "");

                        int uniqueId = 0;
                        decimal cost = 0;
                        decimal commission = 0;
                        if (!int.TryParse(strUniqueId, out uniqueId))
                        {
                            success = false;
                            errMessage = "第" + (k - 4) + "筆：檔號不正確";
                            break;
                        }
                        else if　(channel != (int)AgentChannel.EzTravel && !decimal.TryParse(strCost,out cost))
                        {
                            success = false;
                            errMessage = "第" + (k - 4) + "筆：特談成本不正確";
                            break;
                        }
                        else if (!decimal.TryParse(strCommission, out commission))
                        {
                            success = false;
                            errMessage = "第" + (k - 4) + "筆：特談分潤不正確";
                            break;
                        }
                        else
                        {
                            DealProperty dp = _pponProv.DealPropertyGet(uniqueId);
                            if (!dp.IsLoaded)
                            {
                                success = false;
                                errMessage = "第" + (k - 4) + "筆：檔次不存在";
                                break;
                            }
                            else
                            {
                                BusinessHour bh = _pponProv.BusinessHourGet(dp.BusinessHourGuid);
                                if (Helper.IsFlagSet(bh.BusinessHourStatus,BusinessHourStatus.ComboDealMain))
                                {
                                    success = false;
                                    errMessage = "第" + (k - 4) + "筆：不允許匯入母檔";
                                    break;
                                }
                                else
                                {
                                    if (specials.Where(x => x.Bid == bh.Guid).Any())
                                    {
                                        success = false;
                                        errMessage = "第" + (k - 4) + "筆：重複輸入";
                                        break;
                                    }
                                    else
                                    {
                                        ChannelSpecialCommission OldSpecial = _channelProv.ChannelCommissionListGetByChannelBid(channel, bh.Guid);
                                        if (OldSpecial.IsLoaded)
                                        {
                                            OldSpecial.Cost = cost == 0 ? default(decimal?) : cost;
                                            OldSpecial.Commission = commission / 100;
                                            OldSpecial.ModifyTime = DateTime.Now;
                                            OldSpecial.ModifyId = UserName;
                                            specials.Add(OldSpecial);
                                        }
                                        else
                                        {
                                            ChannelSpecialCommission newSpecial = new ChannelSpecialCommission();
                                            newSpecial.ChannelSource = channel;
                                            newSpecial.Bid = bh.Guid;
                                            newSpecial.Cost = cost == 0 ? default(decimal?) : cost;
                                            newSpecial.Commission = commission / 100;
                                            newSpecial.CreateTime = DateTime.Now;
                                            newSpecial.CreateId = UserName;
                                            specials.Add(newSpecial);

                                        }
                                    }
                                }
                            }

                        }

                    }
                }
            }

            if (success)
            {
                success = _channelProv.ChannelSpecialCommissionSaveAll(specials);
                if (!success)
                    errMessage = "匯入失敗";
            }

            if (success)
            {
                return Json(new { Success = true, Message = "上傳成功" });
            }
            else
            {
                return Json(new { Success = false, Message = errMessage });
            }


        }

        #endregion ichannel & shopback 分潤設定
    }

    #region Controller Model

    public class GetSubDealTypeModel
    {
        public int MainDealType { get; set; }
    }

    #endregion
}

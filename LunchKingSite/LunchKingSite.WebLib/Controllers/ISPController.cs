﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Models.ISP;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebLib.Models.ISP;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class ISPController : PponControllerBase
    {
        private readonly ISysConfProvider _config;
        private static Dictionary<IspChannelServiceType, string> _uid;

        public ISPController()
        {
            _config = ProviderFactory.Instance().GetConfig();
            _uid = new Dictionary<IspChannelServiceType, string>();
        }

        public ActionResult EMapReceive()
        {
            return RedirectToAction("AppEMap", new {storeId = Request.Form["storeid"], storeName = Request.Form["storename"], addr = Request.Form["address"] });
        }

        public ActionResult AppEMap(string storeId, string storeName, string addr)
        {
            return new EmptyResult();
        }

        /// <summary>
        /// 導 7-11 電子地圖
        /// </summary>
        /// <param name="tempvar">7-11 tempvar</param>
        /// <param name="sellerId">seller guid</param>
        /// <param name="storeId">member keep seven storeid</param>
        /// <param name="app">is mobile</param>
        /// <param name="b2C">is b2c else c2c</param>
        /// <returns></returns>
        public ActionResult SevenEMap(string tempvar, string storeId, bool app)
        {
            SevenEmapModel model = new SevenEmapModel
            {
                TempVar = tempvar,
                Display = app ? "touch" : "page",
                StoreId = ISPFacade.IsExistServiceChannelStoreData(storeId, ServiceChannel.SevenEleven) ? storeId : string.Empty,
                EmapUrl = _config.SevenEmapUrl,
                Url = app ? string.Format("{0}/ISP/EmapReceive", _config.SSLSiteUrl)
                    : string.Format("{0}/ppon/buy.aspx?TicketId={1}&csvemap=2", _config.SSLSiteUrl, tempvar),
                SiteUrl = _config.SSLSiteUrl,
                RedirectSevenMap = _config.RedirectSevenMap
            };
            
            var uid = ISPFacade.GetUidFromIspChannelServiceType(true);
            if (uid == string.Empty)
            {
                return View(new SevenEmapModel());
            }
            model.Uid = uid;
            model.Eshopid = "L00";
            
            return View(model);
        }
    }
}

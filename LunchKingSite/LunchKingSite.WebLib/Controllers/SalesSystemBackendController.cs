﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System.Linq;
using Newtonsoft.Json;

namespace LunchKingSite.WebLib.Controllers
{
    
    public class SalesSystemBackendController : BaseController
    {

        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static ISystemProvider ss = ProviderFactory.Instance().GetProvider<ISystemProvider>();
        public SalesSystemBackendController()
        {

        }


        [RolePage]
        public ActionResult ProposalRestriction()
        {
            ProposalRestrictionCollection prc = sp.ProposalRestrictionGet();
            List<ProposalRestrictionModel> list = new List<ProposalRestrictionModel>();

            foreach (var pr in prc)
            {
                ProposalRestrictionModel model = new ProposalRestrictionModel();
                model.Id = pr.Id;
                model.DeliveryType = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, (DeliveryType)pr.DeliveryType);
                model.DealType = ss.SystemCodeGetByCodeGroupId("DealType", (int)pr.ParentCodeId).CodeName + "/" + ss.SystemCodeGetByCodeGroupId("DealType", (int)pr.CodeId).CodeName;

                if (pr.GrossMarginRange != null)
                {
                    model.Restriction += "獲利毛利率限制";
                    if (pr.GrossMarginRange == 1)
                        model.Restriction += "<=";
                    else if (pr.GrossMarginRange == 2)
                        model.Restriction += "<";
                    else if (pr.GrossMarginRange == 3)
                        model.Restriction += "=";
                    else if (pr.GrossMarginRange == 4)
                        model.Restriction += ">=";
                    else if (pr.GrossMarginRange == 5)
                        model.Restriction += ">";

                    model.Restriction += pr.GrossMarginNumber.ToString() + "%";
                }

                if (pr.CostPercentRange != null)
                {
                    if (!string.IsNullOrEmpty(model.Restriction))
                        model.Restriction += "<br>";

                    model.Restriction += "洽談毛利率限制";
                    if (pr.CostPercentRange == 1)
                        model.Restriction += "<=";
                    else if (pr.CostPercentRange == 2)
                        model.Restriction += "<";
                    else if (pr.CostPercentRange == 3)
                        model.Restriction += "=";
                    else if (pr.CostPercentRange == 4)
                        model.Restriction += ">=";
                    else if (pr.CostPercentRange == 5)
                        model.Restriction += ">";

                    model.Restriction += pr.CostPercentNumber.ToString() + "%";
                }

                model.Enable = pr.Enable ? "開啟" : "關閉";
                list.Add(model);
            }
            
            return View(list);
        }

        /// <summary>
        /// 取得子分類
        /// </summary>
        /// <param name="parentId">母分類ID</param>
        /// <returns></returns>
        public JsonResult GetDealType2(int parentId)
        {
            var scs = PponFacade.DealType2GetList(parentId);
            return Json(new { Result = scs });
        }

        /// <summary>
        /// 儲存提案限制
        /// </summary>
        /// <param name="type"></param>
        /// <param name="grossMarginRange"></param>
        /// <param name="grossMarginNumber"></param>
        /// <param name="costPercentRange"></param>
        /// <param name="costPercentNumber"></param>
        /// <param name="enable"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveRestriction(int? id, string type, int? grossMarginRange, int? grossMarginNumber, int? costPercentRange, int? costPercentNumber, bool enable)
        {
            try
            {
                if (id != null)
                {
                    //編輯
                    ProposalRestriction pr = sp.ProposalRestrictionGetById((int)id);
                    ProposalRestriction oriPr = pr.Clone();
                    pr.DeliveryType = (int)DeliveryType.ToHouse;//先預設
                    pr.GrossMarginRange = grossMarginRange;
                    pr.GrossMarginNumber = grossMarginNumber;
                    pr.CostPercentRange = costPercentRange;
                    pr.CostPercentNumber = costPercentNumber;
                    pr.Enable = enable;
                    pr.ModifyId = UserName;
                    pr.ModifyTime = DateTime.Now;
                    sp.ProposalRestrictionSet(pr);

                    #region 更新提案單
                    ProposalCollection pros = sp.ProposalGetListByRestriction(pr.CodeId);
                    foreach(Proposal pro in pros)
                    {
                        ProposalFacade.ProposalFlowCompleted(pro, true, UserName);
                        if (oriPr.GrossMarginRange != grossMarginRange ||
                            oriPr.GrossMarginNumber != grossMarginNumber ||
                            oriPr.CostPercentRange != costPercentRange ||
                            oriPr.CostPercentNumber != costPercentNumber)
                        {
                            ProposalFacade.ProposalLog(pro.Id, string.Format("後台異動毛利率限制,ID={0},獲利毛利率限制={1}=>{2},洽談毛利率限制={3}=>{4}", pr.Id, grossMarginNumber, oriPr.GrossMarginNumber, costPercentNumber, oriPr.CostPercentNumber), UserName);
                        }
                    }
                    #endregion 更新提案單

                }

                else
                {
                    //反序列化
                    List<string> typeList = JsonConvert.DeserializeObject<List<string>>(type);
                    //序列化
                    //string json = JsonConvert.SerializeObject(typeList);


                    //新增
                    foreach (var t in typeList)
                    {
                        ProposalRestriction pr = new ProposalRestriction();
                        pr.DeliveryType = (int)DeliveryType.ToHouse;//先預設
                        pr.ParentCodeId = Convert.ToInt32(t.Split('/')[0]);
                        pr.CodeId = Convert.ToInt32(t.Split('/')[1]);
                        pr.GrossMarginRange = grossMarginRange;
                        pr.GrossMarginNumber = grossMarginNumber;
                        pr.CostPercentRange = costPercentRange;
                        pr.CostPercentNumber = costPercentNumber;
                        pr.Enable = enable;
                        pr.CreateId = UserName;
                        pr.CreateTime = DateTime.Now;
                        sp.ProposalRestrictionSet(pr);


                    }
                }

                TempData["Result"] = "儲存成功";
                return RedirectToAction("ProposalRestriction");
            }
            catch (Exception ex)
            {
                TempData["Result"] = ex.Message;
                return RedirectToAction("ProposalRestriction");
            }
        }




        public ActionResult ProposalRestrictionData(int? id)
        {
            List<SystemCode> dealType1 = SystemCodeManager.GetDealType().Where(x => x.ParentCodeId == null).OrderBy(x => x.Seq).ToList();
            ViewBag.Dealtype1 = dealType1;

            if (id != null)
            {
                ProposalRestriction pr = sp.ProposalRestrictionGetById((int)id);
                ViewBag.DealType = ss.SystemCodeGetByCodeGroupId("DealType", (int)pr.ParentCodeId).CodeName + "/" + ss.SystemCodeGetByCodeGroupId("DealType", (int)pr.CodeId).CodeName;
                
                return View(pr);
            }
            else
            {
                ProposalRestriction pr = new ProposalRestriction();
                ViewBag.CodeIdList = string.Join(",", sp.ProposalRestrictionGet().Select(x => x.CodeId).ToList());
                return View(pr);
            }
                

            
        }

    }

}

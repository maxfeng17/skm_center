﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.WebLib.Component;
using Newtonsoft.Json;
using NPOI.SS.UserModel;

namespace LunchKingSite.WebLib.Controllers
{
    public class BaseController : Controller
    {
        public string UserName
        {
            get { return this.HttpContext.User.Identity.Name; }
        }
        public ActionResult Excel(Workbook workbook, string fileName)
        {
            var memStream = new MemoryStream();
            workbook.Write(memStream);
            memStream.Position = 0;
            return File(memStream, "application/vnd.ms-excel", fileName);
        }

        public ActionResult RedirectToLogin(string returnUrl)
        {
            string url = FormsAuthentication.LoginUrl + "?returnUrl=" + Uri.EscapeDataString(returnUrl);
            return Redirect(url);
        }
    }

    /// <summary>
    /// 測試一陣子再跟 BaseController 合併
    /// </summary>
    public class ControllerExt : BaseController
    {
        public JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonIgnoreSetting jsonIgnore)
        {
            var jsonResolver = new PropertyRenameAndIgnoreSerializerContractResolver();
            foreach(string propName in jsonIgnore.PropNames)
            {
                jsonResolver.IgnoreProperty(jsonIgnore.IgnoreType, propName);
            }            

            var serializerSettings = new JsonSerializerSettings();
            serializerSettings.ContractResolver = jsonResolver;

            return new JsonNetResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                SerializerSettings = serializerSettings
            };
        }

        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            if (behavior == JsonRequestBehavior.DenyGet
                && string.Equals(this.Request.HttpMethod, "GET",
                                 StringComparison.OrdinalIgnoreCase))
                //Call JsonResult to throw the same exception as JsonResult
                return new JsonResult();
            return new JsonNetResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding
            };
        }
    }

    public class PponControllerBase : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            PponIdentity pponUser = this.User.Identity as PponIdentity;
            if (pponUser == null)
            {
                pponUser = PponIdentity.Get(this.User.Identity.Name);
            }
            if (pponUser == null)
            {
                this.MemberPic = string.Empty;
                this.UserId = 0;
                this.UserFullName = string.Empty;
            } 
            else 
            {
                this.MemberPic = pponUser.Pic;
                this.UserId = pponUser.Id;
                this.UserFullName = pponUser.DisplayName;
            }

            base.OnActionExecuting(filterContext);
        }

        public string UserFullName { get;private set; }

        public string MemberPic { get;private set; } 

        public int UserId { get;private set; }
    }

    public class QuestionnaireBase : PponControllerBase
    {
        protected static IQuestionProvider Qp = ProviderFactory.Instance().GetProvider<IQuestionProvider>();
        protected bool QuestEventCheck(FormCollection form, out QuestionEvent ev, out string message)
        {
            message = string.Empty;
            ev = new QuestionEvent();

            if (form["token"] == string.Empty)
            {
                message = "Event is not exist.";
                return false;
            }

            ev = Qp.GetQuestionEvent(form["token"]);
            if (ev == null || ev.Id == 0)
            {
                message = "Event is not exist.";
                return false;
            }
            return true;
        }

        protected bool AuthLoginMode(QuestionEvent ev, out string redirectLoginUrl)
        {
            redirectLoginUrl = string.Empty;

            if (ev.LoginMode == (byte)QuestionEventLoginMode.MemberOnly
                && !User.Identity.IsAuthenticated)
            {
                if (HttpContext.Request.HttpMethod == "POST" && HttpContext.Request.UrlReferrer != null)
                {
                    var refer = HttpContext.Request.UrlReferrer;
                    var url = string.Format("{0}{1}signed=true", refer.AbsoluteUri
                        , refer.AbsoluteUri.IndexOf('?') > -1 ? "&" : "?");

                    redirectLoginUrl = string.Format("{0}?ReturnUrl={1}"
                            , FormsAuthentication.LoginUrl
                            , HttpUtility.UrlEncode(url));
                    return false;
                }

                if (HttpContext.Request.Url != null)
                {
                    var requestUrl = HttpContext.Request.Url;
                    redirectLoginUrl = string.Format("{0}{1}{2}"
                        , FormsAuthentication.LoginUrl
                        , requestUrl != null ? "?ReturnUrl=" : string.Empty
                        , requestUrl != null ? HttpUtility.UrlEncode(HttpContext.Request.Url.AbsoluteUri) : string.Empty);
                    return false;
                }
            }

            return true;
        }
    }

    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept");
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Methods", "GET,POST,OPTIONS");
            base.OnActionExecuting(filterContext);
        }
    }

    public class SwitchableHttpsAttribute : RequireHttpsAttribute
    {
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext == null)
            {
                throw new ArgumentNullException("filterContext");
            }

            if (config.SwitchableHttpsEnabled && filterContext.HttpContext.Request.IsSecureConnection == false)
            {
                HandleNonHttpsRequest(filterContext);
            }
        }
    }
}

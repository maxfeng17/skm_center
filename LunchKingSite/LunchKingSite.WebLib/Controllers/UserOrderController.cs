﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.EInvoice;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace LunchKingSite.WebLib.Controllers
{
    public class UserOrderController : PponControllerBase
    {
        private static ILog logger = LogManager.GetLogger("ShipProductInventory");
        private static ISysConfProvider config;
        private static ILocationProvider lp = ProviderFactory.Instance().GetProvider<ILocationProvider>();
        private static IMemberProvider mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
        private static IOrderProvider op = ProviderFactory.Instance().GetProvider<IOrderProvider>();
        private static ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private static IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();

        public int CookieCityId
        {
            get
            {
                HttpCookie cookie = Request.Cookies.Get(LkSiteCookie.CityId.ToString("g"));

                if (cookie != null)
                {
                    try
                    {
                        return int.Parse(cookie.Value);
                    }
                    catch (Exception)
                    {
                        return -1;
                    }
                }

                return -1;
            }
        }

        static UserOrderController()
        {
            config = ProviderFactory.Instance().GetConfig();
        }

        #region action
        public ActionResult OrderList(string tid)
        {
            if (string.IsNullOrEmpty(tid)) {
                return Redirect(config.SiteUrl);
            }

            MainCartDTO mainCartDTO = CartFacade.SmallCartListGet(null, UserName, tid);

            //重新比對售價/運費
            PponDeliveryInfo pponDeliveryInfo = new PponDeliveryInfo();

            int DealCount = 0;
            int totalAmount = 0;    //總金額
            int totalFreights = 0;  //總運費
            int discountCount = 0;  //折價券數量
            foreach (SmallCart smallCart in mainCartDTO.SmallCart)
            {
                int FreightsId = smallCart.FreightsId;
                ShoppingCartFreight scf = sp.ShoppingCartFreightsGet(FreightsId);
                smallCart.Freights = scf.Freights;
                totalFreights += smallCart.ActualFreights;
                foreach (CartPaymentDTO dto in smallCart.PaymentDTOList)
                {
                    DealCount++;
                    IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(dto.BusinessHourGuid);

                    decimal BaseGrossMargin = PponFacade.GetMinimumGrossMarginFromBids(vpd.BusinessHourGuid, true) * 100; //取百分比   
                    int bizHourStatus = vpd.BusinessHourStatus;
                    int Amt = Convert.ToInt32(vpd.ItemPrice) * dto.Quantity;
                    dto.ItemPrice = Convert.ToInt32(vpd.ItemPrice);
                    dto.Amount = Amt;
                    totalAmount += Amt;
                    dto.IsDealUseDiscount = IsDiscountGet(config.DiscountCodeUsed, bizHourStatus, BaseGrossMargin);//適用優惠券
                    dto.DealStatus = PponFacade.GetCartDealStatus(vpd);

                    dto.isGroupCoupon = Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
                    dto.MaxItemCount = vpd.MaxItemCount ?? 1;

                    //憑證/宅配
                    dto.PponDeliveryType = vpd.DeliveryType ?? (int)DeliveryType.ToHouse;
                    //折價券
                    if (UserId != 0)
                    {
                        IEnumerable<ViewDiscountDetail> DiscountList = PromotionFacade.GetMemberDiscountList(UserId, vpd.BusinessHourGuid);
                        dto.DiscountList = DiscountList;

                        dto.IsDealHasDiscount = false;
                        if (config.EnableBuyMemberDiscountList)
                        {
                            if (config.DiscountCodeUsed)
                            {
                                if (DiscountList.Any())
                                {
                                    dto.IsDealHasDiscount = true;
                                }

                                discountCount = DiscountList.Count();
                            }
                        }
                    }
                }
            }

            pponDeliveryInfo.Freight = totalFreights;   //總運費
            mainCartDTO.DeliveryInfo = pponDeliveryInfo;
            mainCartDTO.Amount = totalAmount;   //總金額

            double bcash = 0;
            decimal scash = 0;
            if (!string.IsNullOrEmpty(UserName))
            {
                bcash = MemberFacade.GetAvailableMemberPromotionValue(UserName);//紅利金
                bcash = (Math.Floor(bcash / 10));
                scash = OrderFacade.GetSCashSum(UserName);  //購物金
            }

            ViewBag.tid = tid;
            ViewBag.bcash = bcash;
            ViewBag.scash = scash;
            ViewBag.SiteUrl = config.SiteUrl;
            ViewBag.DealCount = DealCount;
            ViewBag.DiscountCount = discountCount;
            return View(mainCartDTO);
        }

        public ActionResult OrderAddOn(string sid, string fid)
        {
            List<IViewPponDeal> vpds = CartFacade.OrderAddOn(sid, fid);

            ViewBag.SiteUrl = config.SiteUrl;
            return View(vpds);
        }

        /// <summary>
        /// 付款頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult OrderPayment(string tid)
        {
            if (string.IsNullOrEmpty(tid))
            {
                return Redirect(config.SiteUrl);
            }
            else
            {
                ShoppingCartItemOption scio = op.ShoppingCartItemOptionGetByTicketid(tid)
                       .OrderByDescending(x => x.CreateTime).FirstOrDefault();
                if (scio == null)
                {
                    return Redirect(config.SiteUrl);
                }
            }
            Dictionary<string, string> mdcs = new Dictionary<string, string>();
            string UserName = "";
            int UserId = 0;
            bool IsGuest = false;
            bool IsNew = false;
            string BuyerName = "";
            string BuyerAddress = "";
            string BuyerMobile = "";
            int cityId = 0;
            int areaId = 0;
            string address = string.Empty;
            Guid buildingGuid = Guid.Empty;

            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                //使用者已登入
                UserName = User.Identity.Name;
                Member mem = MemberFacade.GetMember(UserName);
                if (mem.IsLoaded && mem != null)
                {
                    UserId = mem.UniqueId;
                    IsGuest = true;
                    IsNew = mem.IsNew;
                    BuyerName = mem.FirstName + mem.LastName;
                    BuyerAddress = mem.CompanyAddress;
                    BuyerMobile = mem.Mobile;

                    if (mem.BuildingGuid.HasValue && !Guid.Equals(BizLogic.Component.MemberActions.MemberUtilityCore.DefaultBuildingGuid, mem.BuildingGuid.Value))
                    {
                        Building bu = lp.BuildingGet(mem.BuildingGuid.Value);
                        City area = lp.CityGet(bu.CityId);
                        City city = lp.CityGet(area.ParentId.Value);
                        if (!string.IsNullOrEmpty(mem.CompanyAddress))
                        {
                            address = mem.CompanyAddress.Replace(city.CityName, string.Empty).Replace(area.CityName, string.Empty);
                        }
                        cityId = city.Id;
                        areaId = area.Id;
                        buildingGuid = bu.Guid;
                    }

                    BuyerAddress = string.Format("{0}|{1}|{2}|{3}",
                                        cityId,
                                        areaId,
                                        buildingGuid,
                                        address);

                }
            }

            Dictionary<int, string> Citys = CityManager.Citys.Where(x => x.CityName.ToLower() != "default").ToDictionary(x => x.Id, y => y.CityName);

            MainCartDTO mainCartDTO = new MainCartDTO();

            if (!string.IsNullOrEmpty(tid) || !string.IsNullOrEmpty(UserName))
            {
                mainCartDTO = CartFacade.SmallCartListGet(null, UserName, tid);
                if (mainCartDTO.SmallCart == null)
                {
                    return Redirect(config.SiteUrl);
                }
            }

            if (UserId != 0)
            {
                mdcs.Add("", "--選擇收件人資料--");
                MemberDeliveryCollection mds = mp.MemberDeliveryGetCollection(UserId);
                foreach (MemberDelivery md in mds)
                {
                    mdcs.Add(md.Id.ToString(), md.ContactName);
                }
            }

            //手機條碼載具
            Dictionary<string, string> dicCarrierType = new Dictionary<string, string>();
            dicCarrierType.Add(((int)CarrierType.Member).ToString(), Helper.GetLocalizedEnum(CarrierType.Member));
            dicCarrierType.Add(((int)CarrierType.Phone).ToString(), Helper.GetLocalizedEnum(CarrierType.Phone));
            dicCarrierType.Add(((int)CarrierType.PersonalCertificate).ToString(), Helper.GetLocalizedEnum(CarrierType.PersonalCertificate));
            dicCarrierType.Add(((int)CarrierType.None).ToString(), "個人紙本發票(二聯式)");

            ViewBag.MemberDeliverys = mdcs;
            ViewBag.TradeAmount = mainCartDTO.Amount;
            ViewBag.CreditcardReferAmount = config.CreditcardReferAmount;
            ViewBag.CartDetailHtml = GetCartDetailHtml(mainCartDTO);
            ViewBag.CartSummaryHtml = GetCartSummary(mainCartDTO);  //超過X元需登入購買
            ViewBag.TotalAmount = mainCartDTO.Amount;   //總消費金額
            //*----會員相關 Start------*//
            ViewBag.IsGuest = IsGuest;
            ViewBag.IsNew = IsNew;
            ViewBag.UserName = UserName;
            ViewBag.BuyerName = BuyerName;
            ViewBag.BuyerAddress = BuyerAddress;
            ViewBag.BuyerMobile = BuyerMobile;
            //*----會員相關 End------*//
            ViewBag.Citys = Citys;  //縣市
            ViewBag.CarrierType = dicCarrierType;   //手機條碼載具
            ViewBag.TransactionId = tid;

            string pezDonationCode = "23981";//南投縣青少年空手道推展協會 愛心碼
            string genesisDonationCode = "919";//創世社會福利基金會 愛心碼
            string sunDonationCode = "13579";//財團法人陽光社會福利基金會 愛心碼

            Dictionary<string, string> dicInvoiceDonation = new Dictionary<string, string>();
            CodeName[] loveCodes = LoveCodeManager.Instance.LoveCodes;
            // 南投縣青少年空手道推展協會
            CodeName pezDonation = loveCodes.Where(x => string.Equals(pezDonationCode, x.Code)).FirstOrDefault();
            if (pezDonation != null)
            {
                dicInvoiceDonation.Add(pezDonation.Code, "南投縣青少年空手道推展協會");
            }
            // 創世社會福利基金會
            CodeName genesisDonation = loveCodes.Where(x => string.Equals(genesisDonationCode, x.Code)).FirstOrDefault();
            if (genesisDonation != null)
            {
                dicInvoiceDonation.Add(genesisDonation.Code, "創世社會福利基金會");
            }
            // 財團法人陽光社會福利基金會
            CodeName sunDonation = loveCodes.Where(x => string.Equals(sunDonationCode, x.Code)).FirstOrDefault();
            if (sunDonation != null)
            {
                dicInvoiceDonation.Add(sunDonation.Code, "財團法人陽光社會福利基金會");
            }
            dicInvoiceDonation.Add(string.Empty, "其它捐贈單位");

            ViewBag.InvoiceDonation = dicInvoiceDonation;
            ViewBag.pezDonationCode = pezDonationCode;//南投縣青少年空手道推展協會 愛心碼
            ViewBag.genesisDonationCode = genesisDonationCode;//創世社會福利基金會 愛心碼
            ViewBag.sunDonationCode = sunDonationCode;//財團法人陽光社會福利基金會 愛心碼

            ViewBag.tid = tid;
            ViewBag.SiteUrl = config.SiteUrl;

            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PayWay"></param>
        /// <param name="CardNo1"></param>
        /// <param name="CardNo2"></param>
        /// <param name="CardNo3"></param>
        /// <param name="CardNo4"></param>
        /// <param name="ExpMonth"></param>
        /// <param name="ExpYear"></param>
        /// <param name="SecurityCode"></param>
        /// <param name="BuyerEmail"></param>
        /// <param name="BuyerName"></param>
        /// <param name="BuyerMobile"></param>
        /// <param name="ReceiverName"></param>
        /// <param name="ReceiverMobile"></param>
        /// <param name="ddlBuyerCity"></param>
        /// <param name="ddlBuyerArea"></param>
        /// <param name="ReceiverAddress"></param>
        /// <param name="chkRememberUserInfo"></param>
        /// <param name="InvoiceMode"></param>
        /// <param name="chkPersonalCarrier"></param>
        /// <param name="ddlCarrierType"></param>
        /// <param name="txtPhoneCarrier">手機條碼</param>
        /// <param name="txtPersonalCertificateCarrier"></param>
        /// <param name="InvoiceBuyerName">收件人</param>
        /// <param name="InvoiceAddress">發票地址</param>
        /// <param name="ddlInvoiceDonation">捐贈單位</param>
        /// <param name="txtLoveCode">請輸入愛心碼</param>
        /// <param name="InvoiceTitle">發票抬頭</param>
        /// <param name="InvoiceNumber">統一編號</param>
        /// <param name="InvoiceBuyerName2">收件人</param>
        /// <param name="InvoiceAddress2">發票地址</param>
        /// <param name="InvoiceSave">儲存本次填寫內容</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult OrderPayment(string TransactionId, string PayWay,
            string CardNo1, string CardNo2, string CardNo3, string CardNo4, string ExpMonth, string ExpYear, string SecurityCode,
            string BuyerEmail, string BuyerName, string BuyerMobile, string ReceiverName, string ReceiverMobile,
            string ddlBuyerCity, string ddlBuyerArea, string ReceiverAddress, string chkRememberUserInfo,
            string InvoiceMode, string chkPersonalCarrier, string ddlCarrierType, string txtPhoneCarrier, string txtPersonalCertificateCarrier,
            string InvoiceBuyerName, string InvoiceAddress, string ddlInvoiceDonation, string txtLoveCode,
            string InvoiceTitle, string InvoiceNumber, string InvoiceBuyerName2, string InvoiceAddress2,
            string InvoiceSave

            )
        {
            string userName = "";
            if (!string.IsNullOrEmpty(UserName))
            {
                userName = User.Identity.Name;
            } else
            {
                userName = BuyerEmail;
            }
            MainCartDTO mainCartDTO = new MainCartDTO();
            Member mem = new Member();

            if (TransactionId != null && !string.IsNullOrEmpty(userName))
            {
                mainCartDTO = CartFacade.SmallCartListGet(null, userName, TransactionId);
                mem = MemberFacade.GetMember(userName);
            }

            int Quantity = 0;
            foreach (SmallCart smallCart in mainCartDTO.SmallCart)
            {
                foreach (CartPaymentDTO paymentDTOList in smallCart.PaymentDTOList)
                {
                    Quantity += paymentDTOList.Quantity;
                }
            }

            int _PayWay = (int)Core.PaymentType.Creditcard;
            int.TryParse(PayWay, out _PayWay);
            Core.PaymentType paymentType = (Core.PaymentType)((int)_PayWay);


            //mainCartDTO.BuyerEmail = BuyerEmail;
            //mainCartDTO.BuyerName = BuyerName;
            //mainCartDTO.BuyerMobile = BuyerMobile;
            //mainCartDTO.ReceiverName = ReceiverName;
            //mainCartDTO.ReceiverMobile = ReceiverMobile;
            //mainCartDTO.BuyerCity = ddlBuyerCity;
            //mainCartDTO.BuyerArea = ddlBuyerArea;
            //mainCartDTO.ReceiverAddress = ReceiverAddress;
            mainCartDTO.RememberUserInfo = chkRememberUserInfo;

            mainCartDTO.InvoiceType = InvoiceMode;  //發票開立方式
            mainCartDTO.PersonalCarrier = chkPersonalCarrier;   //使用手機條碼載具、自然人憑證載具或需索取紙本
            mainCartDTO.CarrierType = ddlCarrierType;   //使用手機條碼載具或自然人憑證載具
            mainCartDTO.PhoneCarrier = txtPhoneCarrier; //手機條碼
            mainCartDTO.PersonalCertificateCarrier = txtPersonalCertificateCarrier; //自然人憑證
            mainCartDTO.InvoiceBuyerName = InvoiceBuyerName;    //發票收件人
            mainCartDTO.InvoiceAddress = InvoiceAddress;    //發票收件地址
            mainCartDTO.InvoiceDonation = ddlInvoiceDonation;   //捐贈單位
            mainCartDTO.LoveCode = txtLoveCode; //愛心碼
            mainCartDTO.InvoiceTitle = InvoiceTitle;    //發票抬頭
            mainCartDTO.InvoiceNumber = InvoiceNumber;  //統一編號
            mainCartDTO.InvoiceBuyerName2 = InvoiceBuyerName2;    //發票收件人
            mainCartDTO.InvoiceAddress2 = InvoiceAddress2;    //發票收件地址

            //*------PponDeliveryInfo----*//
            PponDeliveryInfo pponDeliveryInfo = new PponDeliveryInfo();
            pponDeliveryInfo.BuyerUserId = mem.UniqueId;    //購買人UserId
            pponDeliveryInfo.InvoiceBuyerName = BuyerName;  //買受人姓名
            pponDeliveryInfo.InvoiceBuyerAddress = "";

            int Area = 0;
            int.TryParse(ddlBuyerArea, out Area);
            City town = CityManager.TownShipGetById(Area);
            City city = CityManager.CityGetById(town.ParentId.Value);
            pponDeliveryInfo.WriteAddress = city.CityName + town.CityName + ReceiverAddress; //收件地址
            pponDeliveryInfo.WriteZipCode = city.ZipCode;  //收件地址郵遞區號
            pponDeliveryInfo.WriteName = ReceiverName;  //收件人姓名
            pponDeliveryInfo.Phone = ReceiverMobile;    //手機
            pponDeliveryInfo.PayType = paymentType;     //付款方式
            pponDeliveryInfo.Quantity = Quantity;   //數量
            pponDeliveryInfo.BonusPoints = 0;   //紅利金
            pponDeliveryInfo.PayEasyCash = 0;   //PayEasy購物金
            pponDeliveryInfo.SCash = 0;         //17Life購物金
            //pponDeliveryInfo.DeliveryType = "";   //多商品，不固定
            if (!string.IsNullOrEmpty(ddlInvoiceDonation))
            {
                int InvoiceDonation = 0;
                int.TryParse(ddlInvoiceDonation, out InvoiceDonation);
                pponDeliveryInfo.ReceiptsType = (DonationReceiptsType)InvoiceDonation;  //捐獻單位
            }
            if (!string.IsNullOrEmpty(ddlCarrierType))
            {
                int CarrierType = 0;
                int.TryParse(ddlCarrierType, out CarrierType);
                pponDeliveryInfo.CarrierType = (CarrierType)CarrierType;    //載具類型
                //載具號碼
                switch ((CarrierType)CarrierType)
                {
                    case Core.CarrierType.Member:
                        break;
                    case Core.CarrierType.PersonalCertificate:
                        pponDeliveryInfo.CarrierId = txtPersonalCertificateCarrier;
                        break;
                    case Core.CarrierType.Phone:
                        pponDeliveryInfo.CarrierId = txtPhoneCarrier;
                        break;
                    default:
                        break;
                }
            }
            pponDeliveryInfo.LoveCode = txtLoveCode;    //愛心碼
            pponDeliveryInfo.Version = InvoiceVersion.CarrierEra;   //發票版本(自 2014/03/01 起加入發票載具及愛心碼資訊)
            pponDeliveryInfo.InvoiceType = InvoiceMode;
            pponDeliveryInfo.IsInvoiceSave = (InvoiceSave != null && InvoiceSave == "Y" ? true : false);
            //pponDeliveryInfo.InvoiceSaveId = InvoiceNumber; //三聯式發票儲存ID
            pponDeliveryInfo.CompanyTitle = InvoiceTitle;   //公司抬頭
            pponDeliveryInfo.UnifiedSerialNumber = InvoiceNumber;   //統一編號


            mainCartDTO.DeliveryInfo = pponDeliveryInfo;
            //*------PponDeliveryInfo----*//


            mainCartDTO.CreditcardNumber = CardNo1 + CardNo2 + CardNo3 + CardNo4;
            mainCartDTO.CreditcardExpireMonth = ExpMonth;
            mainCartDTO.CreditcardExpireYear = ExpYear;
            mainCartDTO.CreditcardSecurityCode = SecurityCode;


            ApiReturnObject rtnObject = CartFacade.MakeOrderByCart(mainCartDTO, userName, true, true);

            return RedirectToAction("OrderPaymentResult");
        }

        /// <summary>
        /// 付款成功失敗結果頁面
        /// </summary>
        /// <returns></returns>
        public ActionResult OrderPaymentResult()
        {
            return View();
        }

        /// <summary>
        /// 付款結果頁面
        /// </summary>
        /// <returns></returns>
        //public ActionResult PaymentResult(string ticketId, string transactionId, string atmAccount, string atmOrderTotal, bool isPaidByATM,
        //    Guid orderGuid, ViewPponDeal vpd, PaymentResultPageType paymentResutType, ThirdPartyPayment thirdPartyPayment,
        //    Order order = null, string reason = null)
        public ActionResult PaymentResult()
        {
            //必須傳進來的資料 假資料
            string ticketId = "ni2tzs3gsok4s3psuxvsrhzn_a4dd589f-2a3b-4bee-8748-ff18a1b03d3a_avaa4qlince";
            string transactionId = "pe3tmy4qaom800836048";
            string reason = string.Empty;
            Guid orderGuid = new Guid("03a54d08-4b92-4ccf-bf64-de82d61d797a");
            Order order = null;
            IViewPponDeal vpd = null;
            string orderArg = string.Empty;
            PaymentResultPageType paymentResutType = PaymentResultPageType.ThirdPartyPaySuccess;
            ThirdPartyPayment thirdPartyPayment = ThirdPartyPayment.TaishinPay;

            string atmAccount = "057633671111";
            string atmOrderTotal = "1266";
            bool isPaidByATM = true;

            string successType = "1"; //successType 1:Atm訪客首購 2:atm訪客非首購 3:atm會員 4:信用卡訪客首購 5:信用卡會員首購 6:信用卡會員非首購 7:行銷滿額贈活動 8:折價券活動 (不用請刪除 測試用)
            //必須傳進來的資料 假資料

            bool paySuccess = false;
            bool showJs = false;
            string strSuccessHtml = string.Empty;
            string strFailHtml = string.Empty;

            if (vpd == null && !string.IsNullOrEmpty(transactionId))
            {
                vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(new Guid(ticketId.Split('_')[1]));
            }

            if (order != null)
            {
                orderGuid = order.Guid;

                if ((vpd.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0 && vpd.ItemPrice > 0)
                {
                    try
                    {
                        orderArg = RetargetingUtility.GetOrderArgs(vpd, order);
                    }
                    catch (Exception ex)
                    {
                        logger.InfoFormat("Retargeting Exception -> {0}. {1}", ex.Message, ex.StackTrace);
                    }
                }
                showJs = true;
            }

            List<BuyRelatedDeal> buyRelatedDealinfo = this.SetRecommendDeals(vpd);

            switch (paymentResutType)
            {
                case PaymentResultPageType.CreditcardSuccess:
                case PaymentResultPageType.PromotionSuccess:
                case PaymentResultPageType.PcashSuccess:
                case PaymentResultPageType.ThirdPartyPaySuccess:
                    strSuccessHtml = GetSuccessHtml(vpd, atmAccount, atmOrderTotal, isPaidByATM, orderGuid, successType);
                    paySuccess = true;
                    break;
                case PaymentResultPageType.CreditcardFailed:
                case PaymentResultPageType.PcashFailed:
                case PaymentResultPageType.ThirdPartyPayCancel:
                case PaymentResultPageType.ThirdPartyPayFail:
                    strFailHtml = GetFailedHtml(paymentResutType, thirdPartyPayment, transactionId, reason);
                    break;
                default:
                    Response.Redirect("~/error-sgo.aspx");
                    break;
            }

            ViewBag.ShowJs = showJs;
            ViewBag.PaymentSuccess = paySuccess;
            ViewBag.OrderArg = orderArg;
            ViewBag.FailedHtml = strFailHtml;
            ViewBag.SuccessHtml = strSuccessHtml;

            return View(buyRelatedDealinfo);
        }

        /// <summary>
        /// 重新購買按鈕
        /// </summary>
        /// <returns></returns>
        public ActionResult BuyAgain()
        {
            return View("PaymentResult");
        }

        /// <summary>
        /// 待用憑證
        /// </summary>
        /// <returns></returns>
        public ActionResult Coupons()
        {
            if (!User.Identity.IsAuthenticated)
            {
                FormsAuthentication.RedirectToLoginPage();
                return null;
            }

            #region 篩選分類
            var enumList = WebUtility.GetListFromEnum(I18N.Phrase.ResourceManager, NewCouponListFilterType.None);
            Dictionary<string, string> orderSelectArea = new Dictionary<string, string>();
            
            foreach (var item in enumList)
            {
                if (item.Value == Convert.ToString((int)NewCouponListFilterType.NotShipped) ||
                    item.Value == Convert.ToString((int)NewCouponListFilterType.Product) ||
                    item.Value == Convert.ToString((int)NewCouponListFilterType.OverdueNotUsed) ||
                    item.Value == Convert.ToString((int)NewCouponListFilterType.GroupCouponNotUsed) ||
                    (!config.IsShowSKMFilter && item.Value == Convert.ToString((int)NewCouponListFilterType.SKM)))
                {
                    continue;
                }
                
                orderSelectArea.Add(item.Text, item.Value);
            }
            #endregion
            
            ViewBag.OrderSelectArea = orderSelectArea;
            return View();
        }
        #endregion action
        

        #region webmethod
        [HttpPost]
        public ActionResult GetBuyerCity(int city)
        {
            var Towns = CityManager.TownShipGetListByCityId(city).ToList();
            return Json(Towns);
        }
        [HttpPost]
        public ActionResult GetDiscountList(string Bid, string ticketId)
        {
            if (string.IsNullOrEmpty(UserName))
            {
                return null;
            }

            string discountState = string.Empty;
            Guid BusinessHourGuid = Guid.Empty;
            Guid.TryParse(Bid, out BusinessHourGuid);           
            if (BusinessHourGuid == Guid.Empty)
            {
                return null;
            }
            IEnumerable<ViewDiscountDetail> DiscountList = PromotionFacade.GetMemberDiscountList(UserId, BusinessHourGuid);

            List<SimpleDiscount> discountList = new List<SimpleDiscount>();

            foreach (ViewDiscountDetail dis in DiscountList)
            {
                if (dis.EndTime != null)
                {                    
                    switch (CartFacade.GetDiscountState(dis.Code, ticketId, this.Request.UrlReferrer.Query.Replace("?tid=", ""), UserId, BusinessHourGuid))
                    {
                        case CartDiscountUseState.CanUse:
                            discountState = "canuse";
                            break;
                        case CartDiscountUseState.Using:
                            discountState = "on";
                            break;
                        case CartDiscountUseState.NoDiscount:
                        case CartDiscountUseState.Used:
                            discountState = "used";
                            break;
                    }

                    discountList.Add(new SimpleDiscount()
                    {
                        Amount = dis.Amount ?? 0,
                        MinimumAmount = dis.MinimumAmount ?? 0,
                        EndTime = (dis.EndTime ?? DateTime.Now).ToString("yyyy/MM/dd HH:mm:ss"),
                        DiscountCode = dis.Code,
                        DiscountState = discountState
                    });
                }
            }

            return Json(discountList);
        }

        /// <summary>
        /// 更新成套票券使用狀態
        /// </summary>
        /// <param name="discountCode">成套票券序號</param>
        /// <param name="ticketId">TicketId</param>
        /// <param name="bid">BusinessHourGuid</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UpdateDiscountCode(string discountCode, string ticketId, string bid)
        {
            Guid businessHourGuid;

            if (!Guid.TryParse(bid, out businessHourGuid))
            {
                return Json(false);
            }

            string tid = this.Request.UrlReferrer.Query.Replace("?tid=", "");
            decimal discountAmount;
            bool result = CartFacade.SetDiscountToSmallCartItem(discountCode, ticketId, businessHourGuid, this.UserId, tid, out discountAmount);
            dynamic data = new
            {
                result = result,
                discountAmount = discountAmount.ToString("N0")
            };

            return Json(data);
        }

        /// <summary>
        /// 檢查折價券
        /// </summary>
        /// <param name="discountCode">成套票券序號</param>
        /// <param name="ticketId">TicketId</param>
        /// <param name="bid">BusinessHourGuid</param>
        /// <returns></returns>
        public ActionResult CheckDiscountCode(string discountCode, string ticketId, string bid)
        {
            Guid BusinessHourGuid = Guid.Empty;
            Guid.TryParse(bid, out BusinessHourGuid);

            if (BusinessHourGuid == Guid.Empty || string.IsNullOrEmpty(discountCode) || string.IsNullOrEmpty(ticketId))
            {
                return Json("{ \"result\": false, \"message\": \"\" }");
            }
            
            string errmsg = string.Empty;
            bool result = false;
            switch (CartFacade.GetDiscountState(discountCode, ticketId, this.Request.UrlReferrer.Query.Replace("?tid=", ""), UserId, BusinessHourGuid))
            {
                case CartDiscountUseState.NoDiscount:
                    errmsg = "無效的序號";
                    break;
                case CartDiscountUseState.Used:
                    errmsg = "序號已使用在其他商品中";
                    break;
                case CartDiscountUseState.Using:
                    errmsg = "序號已使用在此商品中";
                    break;
                case CartDiscountUseState.CanUse:
                    result = true;
                    break;
            }

            dynamic data = new
            {
                result = result,
                message = errmsg
            };

            return Json(data);
        }

        /// <summary>
        /// 取得憑證資訊
        /// </summary>
        /// <returns></returns>
        /// <param name="page">頁次</param>
        /// <param name="filter">篩選分類</param>
        [HttpPost]
        public ActionResult GetCouponList(int page, int filter)
        {
            int pageLenght = 10;//一頁幾筆

            var userId = MemberFacade.GetUniqueId(UserName);
            ViewCouponOnlyListMainCollection data = MemberFacade.ViewCouponListMainCanUseGetListByUser(page, pageLenght, userId, (NewCouponListFilterType)filter, null, false);
            
            int pageCount = MemberFacade.ViewCouponOnlyListMainGetCount(userId, (NewCouponListFilterType)filter, null);
            int totalPage = (pageCount / pageLenght) + ((pageCount % pageLenght > 0) ? 1 : 0);

            dynamic obj = new
            {
                data = this.RenderViewToString(this.ControllerContext, "_CouponData", CouponFacade.GetCouponsDataForWebView(data)),
                pageCount = pageCount,
                totalPage = totalPage
            };

            return Json(obj);
        }

        public ActionResult DeleteOrder(string tid, string oid)
        {
            List<string> result = new List<string>();
            result = CartFacade.RemoveItemFromCart(tid, oid, UserName, UserId);
            bool flag = Convert.ToBoolean(result[0]);
            int TotalAmount = Convert.ToInt32(result[1]);
            int TotalFreight = Convert.ToInt32(result[2]);
            int Result = Convert.ToInt32(result[3]);

            return Json(new { Amount = TotalAmount, Freights = TotalFreight, Result = Result });
        }

        public ActionResult UpdateOrder(string tid, string oid, int freightsId, int quantity)
        {
            List<ShoppingCartItemOption> scios = new List<ShoppingCartItemOption>();
            if (!string.IsNullOrEmpty(UserName) && UserId != 0)
            {
                scios = op.ShoppingCartItemOptionGetByUid(UserId).ToList();
            }
            else if (oid != null)
            {
                scios = op.ShoppingCartItemOptionGetByTicketid(oid).ToList();
            }

            int Amount = 0;
            int TotalAmount = 0;
            int TotalFreight = 0;
            int Result = 0;
            if (scios.Count > 0)
            {
                ShoppingCartItemOption sco = scios[0];
                MainCartDTO mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(sco.ItemOptions);
                SmallCart smallCart = mainCartDTO.SmallCart.Where(x => x.FreightsId == freightsId).FirstOrDefault();
                CartPaymentDTO list  = smallCart.PaymentDTOList.Where(x=>x.TicketId == tid).FirstOrDefault();
                list.Quantity = quantity;
                list.Amount = quantity * list.ItemPrice;

                
                foreach (var s in smallCart.PaymentDTOList)
                {
                    Amount = Amount + s.Amount;
                }

                smallCart.Amount = Amount;
                if (smallCart.Amount < smallCart.NoFreightLimit)
                    smallCart.ActualFreights = smallCart.Freights;
                else
                    smallCart.ActualFreights = 0;


                foreach (var s in mainCartDTO.SmallCart)
                {
                    TotalAmount = TotalAmount + Convert.ToInt32(s.Amount);
                    TotalFreight = TotalFreight + s.ActualFreights;
                }
                mainCartDTO.Amount = TotalAmount;
                Result = TotalAmount + TotalFreight - mainCartDTO.BCash - mainCartDTO.SCash;
                sco.ItemOptions = new JsonSerializer().Serialize(mainCartDTO);
                op.ShoppingCartItemOptionSet(sco);
            }

            return Json(new { Amount = TotalAmount, Freights = TotalFreight, Result = Result });
        }

        public ActionResult UpdateCash(string oid, int bcash, int scash)
        {
            List<ShoppingCartItemOption> scios = new List<ShoppingCartItemOption>();
            if (!string.IsNullOrEmpty(UserName) && UserId != 0)
            {
                scios = op.ShoppingCartItemOptionGetByUid(UserId).ToList();
            }
            else if (oid != null)
            {
                scios = op.ShoppingCartItemOptionGetByTicketid(oid).ToList();
            }

            if (scios.Count > 0)
            {
                ShoppingCartItemOption sco = scios[0];
                MainCartDTO mainCartDTO = new JsonSerializer().Deserialize<MainCartDTO>(sco.ItemOptions);
                mainCartDTO.BCash = bcash;
                mainCartDTO.SCash = scash;
                sco.ItemOptions = new JsonSerializer().Serialize(mainCartDTO);
                op.ShoppingCartItemOptionSet(sco);
            }

            return null;
        }
        #endregion webmethod

        #region private method
        private bool IsDiscountGet(bool isUseDiscountCode, int bizHourStatus, decimal BaseGrossMargin)
        {
            bool flag = true;
            if (config.EnableGrossMarginRestrictions)
            {
                if (isUseDiscountCode)
                {
                    if ((BaseGrossMargin <= Convert.ToDecimal(config.GrossMargin) && (!Helper.IsFlagSet(bizHourStatus, (int)BusinessHourStatus.LowGrossMarginAllowedDiscount))))
                    {
                        flag = false;
                    }
                }
            }
            return flag;
        }
        /// <summary>
        /// 取得訂單明細
        /// </summary>
        /// <param name="mainCartDTO"></param>
        /// <returns></returns>
        private string GetCartDetailHtml(MainCartDTO mainCartDTO)
        {
            string strHtml = "";
            foreach (SmallCart smallCart in mainCartDTO.SmallCart)
            {
                int cartIdx = 0;
                foreach (CartPaymentDTO dto in smallCart.PaymentDTOList)
                {
                    strHtml += "<tr>";
                    strHtml += "<td>";
                    strHtml += "<div class=\"OrderDetail-item-name\">" + dto.DealName + "</div>";   //好康名稱
                    strHtml += "<div class=\"OrderDetail-item-detail\">共" + dto.Quantity + "件：(黑色) (43) x 150 , (黑色) (41) x 20</div>";
                    strHtml += "</td>";
                    strHtml += "<td>$" + dto.Amount + "</td>";  //售價
                    strHtml += "<td>" + dto.Quantity + "</td>"; //數量
                    strHtml += "<td>-$" + dto.DiscountCode + "</td>";  //折價券折抵
                    if (smallCart.PaymentDTOList.Count == 1)
                    {
                        strHtml += "<td>" + (dto.Amount * dto.Quantity) + "</td>";   //小計
                        strHtml += "<td>$" + dto.DeliveryCharge + "</td>";       //運費
                    }
                    else
                    {
                        if (cartIdx == 0)
                        {
                            strHtml += "<td rowspan=\"" + smallCart.PaymentDTOList.Count + "\">$" + smallCart.PaymentDTOList.Select(x => x.Amount).Sum() + "</td>";    //小計
                            strHtml += "<td rowspan=\"" + smallCart.PaymentDTOList.Count + "\">$" + smallCart.Freights + "</td>";        //運費
                        }
                    }
                    strHtml += "</tr>";
                    cartIdx += 1;
                }
            }
            return strHtml;
        }
        /// <summary>
        /// 取得購物車總金額
        /// </summary>
        /// <param name="mainCartDTO"></param>
        /// <returns></returns>
        private string GetCartSummary(MainCartDTO mainCartDTO)
        {
            string _Amount = mainCartDTO.Amount.ToString();
            string _SCash = mainCartDTO.SCash.ToString();
            string _BCash = mainCartDTO.BCash.ToString();

            int Amount = 0;
            int SCash = 0;
            int BCash = 0;

            int.TryParse(_Amount, out Amount);
            int.TryParse(_SCash, out SCash);
            int.TryParse(_BCash, out BCash);

            int totalAmount = Amount - SCash - BCash;
            if (totalAmount <= 0)
            {
                totalAmount = 0;
            }

            string strHtml = "";
            strHtml += "<table class=\"OrderDetail-recheck-summary\">";
            strHtml += "	<tbody>";
            strHtml += "		<tr>";
            strHtml += "			<td>";
            strHtml += "				商品金額：";
            strHtml += "			</td>";
            strHtml += "			<td class=\"OrderDetail-recheck-summary-cost\">";
            strHtml += "				$" + Amount + "";
            strHtml += "			</td>";
            strHtml += "		</tr>";
            strHtml += "		<tr>";
            strHtml += "			<td>";
            strHtml += "				17life購物金折抵：";
            strHtml += "			</td>";
            strHtml += "			<td class=\"OrderDetail-recheck-summary-cost\">";
            strHtml += "				$" + SCash + "";
            strHtml += "			</td>";
            strHtml += "		</tr>";
            strHtml += "		<tr>";
            strHtml += "			<td>";
            strHtml += "				17life紅利金折抵：";
            strHtml += "			</td>";
            strHtml += "			<td class=\"OrderDetail-recheck-summary-cost\">";
            strHtml += "				$" + BCash + "";
            strHtml += "			</td>";
            strHtml += "		</tr>";
            strHtml += "		<tr class=\"OrderDetail-recheck-summary-total\">";
            strHtml += "			<td>";
            strHtml += "				總金額：：";
            strHtml += "			</td>";
            strHtml += "			<td class=\"OrderDetail-recheck-summary-cost\">";
            strHtml += "				$" + totalAmount;
            strHtml += "			</td>";
            strHtml += "		</tr>";
            strHtml += "</table>";
            strHtml += "</table>";
            strHtml += "</table>";
            strHtml += "</table>";
            return strHtml;
        }

        /// <summary>
        /// 是否符合首購送500元折價券的資格
        /// </summary>
        /// <param name="o"></param>
        /// <param name="deal"></param>
        private bool IsFirstBuyDiscountCode(Order o, IViewPponDeal deal)
        {
            if (config.FirstBuyDiscountCodeEnabled)
            {
                int minAmount = 50;
                if (((o.OrderStatus & (int)OrderStatus.FirstOrder) == (int)OrderStatus.FirstOrder)
                    && o.Subtotal > minAmount && (deal.GroupOrderStatus & (int)GroupOrderStatus.KindDeal) == 0)
                {
                    return true;
                }
            }
            return false;
        }
        
        /// <summary>
        /// 取得錯誤頁面訊息
        /// </summary>
        /// <param name="paymentType">付款類別</param>
        /// <param name="trirdPartyPayment">第三方付款類別</param>
        /// <param name="transactionId">交易ID</param>
        /// <param name="reason">失敗原因</param>
        /// <returns></returns>
        private string GetFailedHtml(PaymentResultPageType paymentType, ThirdPartyPayment trirdPartyPayment, string transactionId, string reason)
        {
            string strHtml = string.Empty;

            switch (paymentType)
            {
                case PaymentResultPageType.CreditcardFailed:
                    strHtml = @"<p class=""info"">
                                    本次交易失敗可能是因為 <span class=""important"">刷卡授權失敗</span> 所造成。<br />
                                    請確實使用本人信用卡，或洽客服人員處理。<br />
                                    若您重新確認刷卡資料後仍無法成功授權交易，請聯絡您的發卡銀行！
                                </p>";
                    break;
                case PaymentResultPageType.PcashFailed:
                    strHtml = @"<p class=""info"">
                                    本次交易失敗可能是因為<span class=""important"">金額扣抵失敗（17Life購物金或17Life紅利金）</span>
                                    <br/>
                                    所造成。 您可嘗試重新購買扣抵，或改以其他付款方式購買。
                                </p>";
                    break;
                case PaymentResultPageType.ThirdPartyPayCancel:
                    strHtml = @"<p class=""info"">
                                    本次交易失敗可能是因為<span class=""important"">金額扣抵失敗</span> 或<span class=""important"">刷卡授權失敗</span>
                                    所造成。<br/>
                                    若您重新確認刷卡資料後仍無法成功授權交易，請聯絡您的發卡銀行！
                                </p>";
                    break;
                case PaymentResultPageType.ThirdPartyPayFail:
                    string msg = string.Empty;
                    if (trirdPartyPayment == ThirdPartyPayment.TaishinPay)
                    {
                        if (string.IsNullOrWhiteSpace(transactionId))
                        {
                            //未扣款訊息
                            msg = "若您重新確認資料後仍無法成功授權交易，請洽台新銀行處理。";
                        }
                        else
                        {
                            msg = "若您的儲值支付帳戶已被扣款，我們將於2~5個工作天內全額退款至您的儲值支付帳戶，請您稍候，造成您的不便請見諒，謝謝。";
                        }
                    }
                    else
                    {
                        msg = string.Format("若您重新確認資料後仍無法成功授權交易，請洽{0}處理。", Helper.GetEnumDescription(trirdPartyPayment));
                    }

                    strHtml = string.Format(@"<p class=""info"">
                                                  本次交易失敗可能是因為 <span class=""important"">{0}</span><br/>
                                                  {1}
                                              </p>", string.IsNullOrEmpty(reason) ? string.Format("{0}授權失敗。", Helper.GetEnumDescription(trirdPartyPayment)) : reason, msg);
                    break;
            }

            return strHtml;
        }

        /// <summary>
        /// 取得成功頁面訊息
        /// </summary>
        /// <param name="vpd">ViewPponDeal Data</param>
        /// <param name="atmAccount">付款帳號</param>
        /// <param name="atmOrderTotal">付款總金額</param>
        /// <param name="isPaidByATM">是否為ATM</param>
        /// <param name="orderGuid">Order Guid</param>
        /// <returns></returns>
        private string GetSuccessHtml(IViewPponDeal vpd, string atmAccount, string atmOrderTotal, bool isPaidByATM, Guid orderGuid, string pageType)
        {
            string rtnHtml = string.Empty;
            string mainHtmlGuest, mainHtmlMember, mainHtmlGuestForAtm, mainHtmlMemberForAtm;
            string atmDetailHtml;
            string footerHtml, footerHtmlByFirst, footerHtmlByNotFirst, footerHtmlByFirstForCredit;
            string firstBuyDiscountHtml, discountPromotionHtml, discountCodeHtml;
            string guestBtnHtml, memberBtnHtml;
            string buySuccessText = string.Empty;
            string itemName = string.Empty;
            bool isDiscountPromotion = false;
            bool isPiinlife = Request.Url.AbsolutePath.StartsWith("/piinlife", StringComparison.OrdinalIgnoreCase);

            buySuccessText = vpd.CityList.IndexOf(Convert.ToString(PponCityGroup.DefaultPponCityGroup.AllCountry.CityId)) > 0 ? "出貨狀態" : "憑證號碼";
            itemName = vpd == null ? string.Empty : vpd.ItemName;

            Order order = ProviderFactory.Instance().GetProvider<IOrderProvider>().OrderGet(orderGuid);
            List<DiscountCampaign> campaignList = PromotionFacade.GetPromotionDiscountCampaignList(DiscountCampaignUsedFlags.FreeGiftWithPurchase);
            Member mem = mp.MemberGet(order.UserId);
            bool isFirstBuyDiscountCode = IsFirstBuyDiscountCode(order, vpd);


            #region Main Data Html
            //1級會員 ATM付款
            mainHtmlMemberForAtm = string.Format(
                        @"<p class=""info"">
                             感謝您訂購【{0}】優質好康。請於今晚 23:59:59 前使用ATM或網路ATM完成付款，您的訂單才算交易成功喔！<br/>
                             付款成功後，您隨時可至 會員中心 &gt; <a href=""/user/coupon_List.aspx"">訂單/憑證</a> 查詢詳細訂單明細、付款金額、發票，以及{1}。
                          </p>", itemName, buySuccessText);

            //非會員 / 0級會員 ATM付款
            mainHtmlGuestForAtm = string.Format(
                        @"<p class=""info"">
                             感謝您訂購【{0}】優質好康。請於今日 23:59:59 前使用ATM或網路ATM完成付款，您的訂單才算交易成功喔！<br/>
                             完成付款後17Life將自動發送兌換券簡訊至您的手機及 E-mail 中，宅配商品將發送訂單明細至您的E-mail。欲查詢{1}，可隨時至 客服中心 &gt; 其他服務 &gt; <a href = ""/User/SendCouponListToEMail.aspx""> 購買紀錄查詢 </a>
                         </p>", itemName, buySuccessText);

            //1級會員 信用卡
            mainHtmlMember = string.Format(
                        @"<p class=""info"">
                             您已成功購得【{0}】好康。<br/>
                             您隨時可至 會員中心 &gt; <a href=""/User/coupon_List.aspx"">訂單/憑證</a> 查詢訂單明細、付款金額、發票，以及{1}。
                          </p>", itemName, buySuccessText);

            //非會員 / 0級會員 信用卡
            mainHtmlGuest =
                        @"<p class=""info"">
                             17Life將自動發送兌換券簡訊至您的手機及 email 中，宅配商品將發送訂單明細至您的email。<br/>
                             欲查詢憑證狀態，可隨時至 客服中心 &gt; 其他服務 &gt; <a href = ""/User/SendCouponListToEMail.aspx"" target=""_blank""> 購買紀錄查詢</a>
                          </p>";
            #endregion

            #region Detail Data Html
            atmDetailHtml = string.Format(
                       @"<h2 class=""important rd-payment-h2"">訂單付款資訊</h2>
                           <div class=""grui-form"">
                               <div class=""form-unit"">
                                   <label class=""unit-label title"">銀行代碼</label>
                                   <div class=""data-input"">
                                       <p>中國信託商業銀行(822)</p>
                                   </div>
                               </div>
                               <div class=""form-unit"">
                                   <label class=""unit-label title"">銀行帳號</label>
                                   <div class=""data-input"">
                                       <p>{0} (共14碼)</p>
                                   </div>
                               </div>
                               <div class=""form-unit"">
                                   <label class=""unit-label title"">繳費金額</label>
                                   <div class=""data-input"">
                                       <p>{1} 元</p>
                                   </div>
                               </div>
                               <div class=""form-unit"">
                                   <label class=""unit-label title"">付款期限</label>
                                   <div class=""data-input"">
                                       <p>{2} 23:59:59 (請於下單購買當日的 23:59:59 前完成付款)</p>
                                   </div>
                               </div>
                               <div class=""form-unit"">
                                   <div class=""data-input"">
                                       <p class=""list-warning-b"">若您超過這個時間付款，ATM會顯示交易失敗，您的訂單將被取消。</p>
                                   </div>
                               </div>
                          </div>", atmAccount, atmOrderTotal, DateTime.Today.ToString("yyyy-MM-dd"));
            #endregion

            #region Footer Data Html
            footerHtml = @"<div class=""member"">";

            //ATM付款_首購
            footerHtmlByFirst = string.Format(
                          @"  <p class=""info import_info member_buy_atm_first_import"" style=""display: block;"">
                                  已發送認證信至您的E-mail：{0}，請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，此筆訂單匯款完成後，我們將送您500元首購折價券！需登入會員才可使用，請盡早完成認證喔！
                              </p>", mem.UserEmail);

            //ATM付款_非首購
            footerHtmlByNotFirst = string.Format(
                          @"  <p class=""info import_info member_buy_atm_nonfirst_import"" style=""display: block;"">
                                  已發送認證信至您的E-mail：{0}，請前往收信認證並設定密碼，即可登入會員中心，查看訂單/憑證等資訊，並可使用折價券！
                              </p>", mem.UserEmail);

            //信用卡_會員首購
            footerHtmlByFirstForCredit =
                          @"  <p class=""info import_info"">
                                  感謝您首次購買17Life好康！已贈送您500元折價券，請至 會員中心 &gt; 折價券 查詢，歡迎多加使用喔！
                              </p>";
            #endregion

            #region Other Data Html
            //首購折價券
            firstBuyDiscountHtml = @"<div class=""member"">
                                        <p class=""info"">
                                            <span class=""hint"">首購折價券有使用效期，請盡早完成認證才可使用喔！</span>
                                        </p>
                                        <div class=""fd_success_sn fistbuy-bg"">
                                            <a href=""/Ppon/NewbieGuide.aspx?s=5&q=20510#anc20510"" target=""_blank"">
                                            <img src=""../Themes/PCweb/images/firstbuy_610x340.jpg""></a>
                                        </div>
                                     </div>";

            //行銷滿額贈活動
            discountPromotionHtml = @"<div class=""fd_success_sn pay-evt-bg"">
                                          <p class=""pay-evt"">
                                             已獲得<span class=""s-imp"">{0}</span>次<br>
                                             抽刮刮樂機會，總獎金超過億元<br/>
                                             還差<span class=""s-imp"">{1}</span>元即可再多一次
                                             <a href=""{2}"" target=""_blank"">活動詳情</a>
                                          </p>
                                      </div>";

            //折價券活動
            discountCodeHtml = @"<div class =""fd_success_sn"">
                                     <p class=""pay-evt"">
                                        恭喜您，該筆訂單可獲得""買多少送多少折價券回饋""，<br/>
                                        將匯入您的帳戶。折價券回饋明細，請按 <a href=""/User/DiscountList.aspx"" target=""_blank"">折價券</a> 查詢
                                     </p>
                                 </div>";
            #endregion

            #region Button Html
            //非會員 / 0級會員
            guestBtnHtml = string.Format(
                           @"<p class=""info btn-center"">
                                <input type=""hidden"" class=""hidAuthEmail"" value=""{0}"">
                                <input type=""button"" class=""btnAuthMail btn btn-large btn-primary btn-primary-flat"" value=""重發認證信"" onclick=""ResendMail()"">
                             </p>", mem.UserEmail);

            //1級會員
            memberBtnHtml = string.Format(
                            @"<div class=""info btn-group btn-group-full-page-center"">
                                 <div class=""btn-half"">
                                    <a class=""btn btn-default btn-default-flat"" href=""{0}"">
                                    繼續購物
                                    </a>
                                 </div>
                                 <div class=""btn-half"">
                                    <a class=""btn btn-primary btn-primary-flat"" href=""/User/coupon_list.aspx"">
                                    查詢訂單
                                    </a>
                                 </div>
                                 <div class=""clearfix""></div>
                             </div>", config.SiteUrl);
            #endregion

            #region 測試資料頁面 不用請刪除
            //TEST!!!!!!=========================================
            //pageType 1:Atm訪客首購 2:atm訪客非首購 3:atm會員 4:信用卡訪客首購 5:信用卡會員首購 6:信用卡會員非首購 7:行銷滿額贈活動 8:折價券活動
            switch (pageType)
            {
                case "1":
                    isPaidByATM = true;
                    mem.IsGuest = true;
                    isFirstBuyDiscountCode = true;
                    break;
                case "2":
                    isPaidByATM = true;
                    mem.IsGuest = true;
                    isFirstBuyDiscountCode = false;
                    break;
                case "3":
                    isPaidByATM = true;
                    mem.IsGuest = false;
                    break;
                case "4":
                    isPaidByATM = false;
                    mem.IsGuest = true;
                    break;
                case "5":
                    isPaidByATM = false;
                    mem.IsGuest = false;
                    isFirstBuyDiscountCode = true;
                    break;
                case "6":
                    isPaidByATM = false;
                    mem.IsGuest = false;
                    isFirstBuyDiscountCode = false;
                    break;
                case "7":
                    isDiscountPromotion = true;
                    discountPromotionHtml = string.Format(discountPromotionHtml, "3", "200", config.SpecialConsumingDiscountUrl);
                    break;
                case "8":
                    campaignList.Add(new DiscountCampaign() { Amount = 0m });
                    break;
            }

            //TEST!!!!!!=========================================
            #endregion

            //ATM
            if (isPaidByATM)
            {
                rtnHtml = mem.IsGuest ?
                    (mainHtmlGuestForAtm + atmDetailHtml + "{0}" + "{1}" + footerHtml + (isFirstBuyDiscountCode ? footerHtmlByFirst : footerHtmlByNotFirst) + "</div>") :
                    (mainHtmlMemberForAtm + atmDetailHtml + "{0}" + "{1}");
            }
            //Credit Card
            else
            {
                rtnHtml = mem.IsGuest ?
                    (mainHtmlGuest + "{0}" + "{1}" + footerHtml + footerHtmlByNotFirst + "</div>") :
                    (mainHtmlMember + "{0}" + "{1}" + footerHtml + (isFirstBuyDiscountCode ? footerHtmlByFirstForCredit : string.Empty) + "</div>");
            }

            //行銷滿額贈活動
            if (config.IsEnabledSpecialConsumingDiscount)
            {
                if (config.SpecialConsumingDiscountCid == 0 ||
                   (isPiinlife && config.SpecialConsumingDiscountCid == PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId) ||
                   (!isPiinlife && config.SpecialConsumingDiscountCid != PponCityGroup.DefaultPponCityGroup.Piinlife.CategoryId))
                {
                    DateTime dateStart = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[0]);
                    DateTime dateEnd = DateTime.Parse(config.SpecialConsumingDiscountPeriod.Split('|')[1]);
                    if (DateTime.Now > dateStart && DateTime.Now < dateEnd)
                    {
                        int amount = PromotionFacade.GetSpecialConsumingAmount(UserId);
                        //Session[LkSiteSession.SpecialConsumingDiscount.ToString()] = amount;
                        KeyValuePair<int, int> consumingDiscount = PromotionFacade.GetSpecialConsumingDiscount(amount);
                        discountPromotionHtml = string.Format(discountPromotionHtml, consumingDiscount.Value.ToString(), consumingDiscount.Key.ToString(), config.SpecialConsumingDiscountUrl);
                        isDiscountPromotion = true;
                    }
                }
            }

            //{0}:行銷滿額贈活動 {1}:折價券活動
            rtnHtml = string.Format(rtnHtml, isDiscountPromotion ? discountPromotionHtml : string.Empty, campaignList.Count() > 0 ? discountCodeHtml : string.Empty);

            //顯示信用卡首購禮
            if (!isPaidByATM && isFirstBuyDiscountCode && mem.IsGuest)
            {
                rtnHtml += firstBuyDiscountHtml;
            }

            //按鈕
            rtnHtml = rtnHtml + (mem.IsGuest ? guestBtnHtml : memberBtnHtml);

            return rtnHtml;
        }

        /// <summary>
        /// 取得相關好康推薦
        /// </summary>
        /// <param name="vpd">ViewPponDeal Data</param>
        /// <returns></returns>
        private List<BuyRelatedDeal> SetRecommendDeals(IViewPponDeal vpd)
        {
            int workCityId = Equals(-1, CookieCityId) ? PponCityGroup.DefaultPponCityGroup.TaipeiCity.CityId : CookieCityId;
            PponCity pponCity = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(workCityId);
            if (pponCity == null)
            {
                pponCity = PponCityGroup.DefaultPponCityGroup.TaipeiCity;
            }
            int categoryId = pponCity.CityId;
            List<BuyRelatedDeal> relatedDeals = PponFacade.GetRelatedDeals(vpd.BusinessHourGuid, categoryId, workCityId);
            var mainBid = ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(vpd.BusinessHourGuid);

            if (relatedDeals.Count < 6)
            {
                int i = relatedDeals.Count();
                var deals = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(workCityId);

                if ((deals.Count + i) < 6)
                {
                    var tmpdata = ViewPponDealManager.DefaultManager.ViewPponDealGetListForPponByCityWithDataSlot(
                        PponCityGroup.DefaultPponCityGroup.AllCountry.CityId).Where(x => mainBid != ViewPponDealManager.DefaultManager.MainBusinessHourGuidGetByBid(x.BusinessHourGuid));
                    deals.AddRange(tmpdata);
                }

                var sideDeals = deals.Where(x => !Equals(vpd.BusinessHourGuid, x.BusinessHourGuid));

                foreach (ViewPponDeal row in sideDeals)
                {
                    BuyRelatedDeal buyRelatedDeal = new BuyRelatedDeal();
                    buyRelatedDeal.BusinessHourGuid = row.BusinessHourGuid;
                    buyRelatedDeal.CouponUsage = row.CouponUsage;
                    buyRelatedDeal.EventTitle = row.EventTitle;
                    buyRelatedDeal.ItemPrice = row.ItemPrice;
                    buyRelatedDeal.ItemOrigPrice = row.ItemOrigPrice;
                    buyRelatedDeal.EventImagePath = row.EventImagePath;
                    buyRelatedDeal.BusinessHourStatus = row.BusinessHourStatus;
                    relatedDeals.Add(buyRelatedDeal);
                    i++;
                    if (i >= 6)
                    {
                        break;
                    }
                }
            }
            foreach (BuyRelatedDeal row in relatedDeals)
            {
                row.NavigateUrl = Url.Content(string.Format("~/{0}/{1}", row.BusinessHourGuid.ToString(), "cpa-17_payment"));
                row.PriceText = Convert.ToInt32(row.ItemPrice).ToString("D");
                row.discountText = ViewPponDealManager.GetDealDiscountString(Helper.IsFlagSet(row.BusinessHourStatus, BusinessHourStatus.NoDiscountShown), row.ItemPrice, row.ItemOrigPrice);
                row.ImageUrl = ImageFacade.GetMediaPathsFromRawData(row.EventImagePath, MediaType.PponDealPhoto).DefaultIfEmpty(string.Empty).First();
            }

            return relatedDeals;
        }

        /// <summary>
        /// 處理呈現頁面成字串
        /// </summary>
        /// <returns></returns>
        private string RenderViewToString(ControllerContext context, string viewPath, object model = null)
        {
            context.Controller.ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindView(context, viewPath, null);
                var viewContext = new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }

}

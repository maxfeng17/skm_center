﻿using System.Web.Mvc;
using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.MasterPass;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebLib.Controllers
{
    [RequireHttps]
    public class MasterPassController : ControllerExt
    {
        private readonly ISysConfProvider _config;
        private static ILog _logger = LogManager.GetLogger("masterpass");

        public MasterPassController()
        {
            _config = ProviderFactory.Instance().GetConfig();
        }

        /// <summary>
        /// PreCheckout with buy for API.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GotoMasterPass(GotoMasterPassWithCheckoutInputModel model)
        {
            ViewBag.IsBinding = false;
            MasterPassData mpData = new MasterPassData();
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {                
                return View(mpData);
            }
            
            ViewBag.MasterPassLightboxJsUrl = _config.LightboxJsUrl;
            ViewBag.CardId = model.CardId;
            ViewBag.PrecheckoutTransactionId = model.PrecheckoutTransactionId;
            ViewBag.WalletName = model.WalletName;
            ViewBag.ConsumerWalletId = model.ConsumerWalletId;
            mpData.IsValid = model.IsValid;

            if (!model.IsValid)
            {
                _logger.InfoFormat("Model Valid Fail({0}):SubTotal:{1},CardId:{2},PrecheckoutTransactionId:{3},WalletName:{4},ConsumerWalletId:{5}",
                    System.Web.HttpContext.Current.User.Identity.Name
                    ,model.SubTotal, model.CardId, model.PrecheckoutTransactionId, model.WalletName, model.ConsumerWalletId);
                return View(mpData);
            }
            
            MasterPass mPass = new MasterPass();
            mpData = mPass.GetRequestToken(model.SubTotal);
            mpData.IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            return View(mpData);
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="st"></param>
        /// <param name="ci"></param>
        /// <param name="pt"></param>
        /// <param name="wn"></param>
        /// <param name="cw"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GotoMasterPass2(int st, string ci, string pt, string wn, string cw)
        {
            MasterPassData mpData = new MasterPassData();

            //if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            //{
            //    return RedirectToAction("GotoMasterPass", mpData);
            //}

            ViewBag.MasterPassLightboxJsUrl = _config.LightboxJsUrl;
            ViewBag.CardId = ci;
            ViewBag.PrecheckoutTransactionId = pt;
            ViewBag.WalletName = wn;
            ViewBag.ConsumerWalletId = cw;
            ViewBag.IsBinding = false;

            MasterPass mPass = new MasterPass();
            mpData = mPass.GetRequestToken(st);
            mpData.IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            return View("GotoMasterPass", mpData);
        }

        /// <summary>
        /// member binding flow for API.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GotoMasterPassBinding()
        {
            MasterPassData mpData = new MasterPassData();
            ViewBag.IsBinding = false;
            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                return View("GotoMasterPass", mpData);
            }

            ViewBag.MasterPassLightboxJsUrl = _config.LightboxJsUrl;
            ViewBag.IsBinding = true;            

            MasterPass mPass = new MasterPass();
            mpData = mPass.GetRequestTokenByBinding();
            mpData.IsLogin = System.Web.HttpContext.Current.User.Identity.IsAuthenticated;
            return View("GotoMasterPass", mpData);
        }

        [HttpGet]
        public ActionResult BindingCallBack()
        {
            MasterPassCallBackModel model = new MasterPassCallBackModel
            {
                AuthToken = string.Empty,
                VerifierCode = 0,
                Message = "綁定成功",
                ResultCode = MasterPassApiResult.Success
            };

            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                model.ResultCode = MasterPassApiResult.UserNotSingIn;
                model.Message = "使用者未登入";
                return RedirectToAction("MasterPassBindingResult", new { model.ResultCodeNum, model.Message });
            }

            string masterPassPairingToken = Request.QueryString["pairing_token"];
            string masterPassPairingVerifier = Request.QueryString["pairing_verifier"];

            if (string.IsNullOrEmpty(masterPassPairingToken) || string.IsNullOrEmpty(masterPassPairingVerifier))
            {
                model.ResultCode = MasterPassApiResult.Cancel;
                model.Message = "取消綁定 MasterPass";
                return RedirectToAction("MasterPassBindingResult", new { model.ResultCodeNum, model.Message });
            }

            string userName = HttpContext.User.Identity.Name;
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            MasterPass mpass = new MasterPass();
            //綁定商家
            mpass.GetPairingToken(masterPassPairingToken, masterPassPairingVerifier, memberUniqueId);

            return RedirectToAction("MasterPassBindingResult", new { model.ResultCodeNum, model.Message });
        }

        [HttpGet]
        public ActionResult CheckoutCallBack()
        {
            MasterPassCallBackModel model = new MasterPassCallBackModel
            {
                AuthToken = string.Empty,
                VerifierCode = 0,
                ResultCode = MasterPassApiResult.DataNotExist,
                Message = "無法取得 MasterPass 交易資料，取消本次交易。"
            };

            if (!System.Web.HttpContext.Current.User.Identity.IsAuthenticated)
            {
                model.ResultCode = MasterPassApiResult.UserNotSingIn;
                model.Message = "使用者未登入";
                return RedirectToAction("MasterPassTransResult", new { model.ResultCodeNum, model.VerifierCode, model.AuthToken });
            }

            string oauthVerifier = Request.QueryString["oauth_verifier"];
            string oauthToken = Request.QueryString["oauth_token"];
            string checkoutResourceUrl = Request.QueryString["checkout_resource_url"];
            string masterPassPairingToken = Request.QueryString["pairing_token"];
            string masterPassPairingVerifier = Request.QueryString["pairing_verifier"];

            if (string.IsNullOrEmpty(oauthVerifier) || string.IsNullOrEmpty(oauthToken) || string.IsNullOrEmpty(checkoutResourceUrl))
            {
                model.ResultCode = MasterPassApiResult.Cancel;
                model.Message = "取消使用 MasterPass";
                return RedirectToAction("MasterPassTransResult", new { model.ResultCodeNum, model.VerifierCode, model.AuthToken });
            }

            MasterPass mpass = new MasterPass();

            string userName = HttpContext.User.Identity.Name;
            int memberUniqueId = MemberFacade.GetUniqueId(userName);

            //綁定商家
            if (!string.IsNullOrEmpty(masterPassPairingToken) &&
                    !string.IsNullOrEmpty(masterPassPairingVerifier))
            {
                mpass.GetPairingToken(masterPassPairingToken, masterPassPairingVerifier, memberUniqueId);
            }

            CreditCardInfo cardInfo = mpass.MasterPassCardInfo(oauthToken, oauthVerifier, checkoutResourceUrl);
            MasterpassCardInfoLog infoLog = mpass.GetCardInfoLog(cardInfo, memberUniqueId);

            if (infoLog.IsLoaded && infoLog.IsValid)
            {
                model.AuthToken = infoLog.AuthToken;
                model.VerifierCode = infoLog.VerifierCode;
                model.Message = "Success";
                model.ResultCode = MasterPassApiResult.Success;
            }

            return RedirectToAction("MasterPassTransResult", new { model.ResultCodeNum, model.VerifierCode, model.AuthToken, model.Message });
        }

        [HttpGet]
        public ActionResult MasterPassTransResult(int resultCodeNum, int verifierCode, string authToken, string message)
        {
            return new EmptyResult();
        }

        [HttpGet]
        public ActionResult MasterPassBindingResult(int resultCodeNum, string message)
        {
            return new EmptyResult();
        }
    }
}

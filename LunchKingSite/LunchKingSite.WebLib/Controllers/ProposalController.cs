﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using LunchKingSite.WebLib.Component;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.IO;
using LunchKingSite.BizLogic.Model.SellerSales;
using LunchKingSite.BizLogic.Component;
using System.Transactions;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebLib.Controllers
{
    public class ProposalController : BaseProposalController
    {
        #region property
        public int UserId
        {
            get
            {
                PponIdentity pponUser = this.User.Identity as PponIdentity;
                if (pponUser == null)
                {
                    return 0;
                }
                else
                {
                    return pponUser.Id;
                }
            }
        }
        #endregion property

        #region action
        [Authorize]
        public override ActionResult HouseProposalContent(int? pid)
        {
            try
            {
                Proposal pro = sp.ProposalGet(pid.GetValueOrDefault());
                ProposalLog vbsLogs = null;
                
                if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller && !Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                {
                    TempData["error"] = "提案單商家編輯中無法查看。";
                }
                if (pro.ProposalSourceType == (int)ProposalSourceType.Original)
                {
                    return Redirect("/sal/ProposalContent.aspx?pid=" + pro.Id);
                }


                #region 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的提案單)

                string privilegeUri = "/sal/ProposalContent.aspx";
                //登入者
                ViewEmployee emp = HumanFacade.ViewEmployeeGetByUserId(MemberFacade.GetUniqueId(UserName));
                //ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);//之後可測試這樣是否比較快?
                if (emp.IsLoaded)
                {
                    if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Read, privilegeUri))
                    {
                        TempData["error"] = "無查看提案單權限。";
                    }
                    else if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.ReadAll, privilegeUri))
                    {

                        //提案單業務
                        ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);

                        if (CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.CrossDeptTeam, privilegeUri))
                        {
                            if (string.IsNullOrEmpty(emp.CrossDeptTeam) && emp.TeamNo == null)
                            {
                                TempData["error"] = "身份權限未明，無法查看資料，請洽產品或技術人員協助。";
                            }
                            else
                            {
                                #region 本區檢視
                                string message1 = "";
                                string message2 = "";
                                bool checkDeSales = ProposalFacade.GetCrossPrivilege(2, emp, deSalesEmp.DeptId, deSalesEmp.TeamNo, deSalesEmp.DeptName, null, ref message1);


                                if (!checkDeSales)
                                {
                                    if (opSalesEmp.IsLoaded)
                                    {
                                        bool checkOpSales = ProposalFacade.GetCrossPrivilege(2, emp, opSalesEmp.DeptId, opSalesEmp.TeamNo, opSalesEmp.DeptName, null, ref message2);

                                        if (!checkOpSales)
                                        {
                                            TempData["error"] = (message1 == message2) ? message1 : (message1 + "\\n" + message2);
                                        }
                                    }
                                }

                                #endregion
                            }
                        }
                        else
                        {
                            #region 無檢視權限(僅能查看負責業務是自己的提案單)

                            if ((deSalesEmp.IsLoaded && deSalesEmp.Email != UserName) && ((!opSalesEmp.IsLoaded) || opSalesEmp.IsLoaded && opSalesEmp.Email != UserName))
                            {
                                TempData["error"] = string.Format("無法查看負責業務 {0} {1} 的提案單資訊。", deSalesEmp.EmpName, opSalesEmp.EmpName);
                            }

                            #endregion
                        }
                    }
                }
                else
                {
                    TempData["error"] = "查無員工資料，請重新檢視員工資料維護設定。";
                }

                #endregion 瀏覽權限檢查 (無全區瀏覽權限，會判斷商家是否有綁定業務，有的話只能檢視自己的提案單)

                base.HouseProposalContent(pid);


                //退回原因
                if (Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.ReturnToSales))
                {
                    string title = "【退件】";
                    vbsLogs = sp.ProposalLogGetList(pro.Id, ProposalLogType.Initial).OrderByDescending(x => x.CreateTime)
                        .Where(y => y.ChangeLog.Contains(title)).FirstOrDefault();
                }
                if (Helper.IsFlagSet(pro.ApproveFlag, LunchKingSite.Core.ProposalApproveFlag.QCcheck))
                {
                    if (pro.DealTypeDetail == null)
                    {
                        ViewBag.ShowDealTypeToastMessage = true;
                    }
                }
                
                ViewBag.VbsLogs = vbsLogs;
                ViewBag.IsVbsPro = false;
                ViewBag.UserId = UserId;

                ViewBag.DealTypeJson = ProviderFactory.Instance().GetSerializer().Serialize(
                    DealTypeFacade.GetDealTypeNodes(true), false,
                    new JsonIgnoreSetting(typeof(DealTypeNode), "Parent"));

            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Redirect("/sal/proposallist.aspx");
            }
            return View("~/Views/Proposal/HouseProposalContent.cshtml");

        }
        [Authorize]
        [HttpPost]
        public ActionResult HouseProposalContentSave(SellerProposalHouseModel model)
        {
            bool flag = false;
            string error = string.Empty;
            List<ProposalMultiDealModel> multiDeals = new List<ProposalMultiDealModel>();
            try
            {
                Proposal pro = sp.ProposalGet(model.ProId);
                Proposal ori_pro = pro.Clone();
                Seller seller = sp.SellerGet(model.SellerGuid);
                string message = CheckHouseProposalSave(model, seller);
                if (!string.IsNullOrEmpty(message))
                {
                    return Json(new
                    {
                        isSuccess = flag,
                        multiDeals = multiDeals,
                        message = message
                    });
                }

                if (model.AncestorBid != null)
                {
                    if(model.AncestorBid.Value == pro.BusinessHourGuid.Value)
                    {
                        return Json(new
                        {
                            isSuccess = flag,
                            multiDeals = multiDeals,
                            message = "續接檔次數量不可為自己"
                        });
                    }
                }
                //業務審核通過再檢查銷售分類是否填寫正確
                if (Helper.IsFlagSet(pro.ApproveFlag, LunchKingSite.Core.ProposalApproveFlag.QCcheck))
                {
                    if (model.DealType2 != null && DealTypeFacade.CheckSelfAndParentsAllEnabled(model.DealType2.Value) == false)
                    {
                        return Json(new
                        {
                            isSuccess = flag,
                            multiDeals = multiDeals,
                            message = "請設定有效的銷售分類，請重整網頁再確認"
                        });
                    }
                }

                //之後要加商家頁面,商家編輯時不存避免誤蓋
                string saveMessage = HouseProposalContentVbsSave(model, seller, UserName, out multiDeals);//商家頁面
                if (!string.IsNullOrEmpty(saveMessage))
                {
                    return Json(new
                    {
                        isSuccess = flag,
                        multiDeals = multiDeals,
                        message = saveMessage
                    });
                }
                string cRst = HouseProposalContentSalesSave(model);   //業務頁面
                if (!string.IsNullOrEmpty(cRst))
                {
                    return Json(new
                    {
                        isSuccess = false,
                        multiDeals = multiDeals,
                        message = string.Format("同步失敗「{0}」", cRst)
                    });
                }


                //業務審核過之後，如果變更商品分線，需判斷是否押上檔期，避免因分線不同，毛利率影響到主管不用審核，這樣就無法壓到檔期了
                if(pro.ApproveFlag == (int)ProposalApproveFlag.QCcheck
                    && ori_pro.DealType2 != pro.DealType)
                {
                    if(pro.OrderTimeS == null)
                    {
                        ProposalFacade.ProposalFlowCompleted(pro, true, UserName);
                    }                    
                }

                if(ori_pro.NoTaxNotification == false)
                {
                    if (model.NoTaxNotification)
                    {
                        #region 若為免稅通知信 通知財務
                        StringBuilder builder = new StringBuilder();
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信][Begin]");
                        Seller s = SellerFacade.SellerGet(pro.SellerGuid);
                        ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                        List<string> mailToUserTax = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.TaxFreeNotice, "/sal/ProposalContent.aspx");

                        //【ＸＸＸ】NO.單號 品名 商家
                        string subjectTax = string.Format("【免稅】 單號 NO.{0} {1} {2}", pro.Id, !string.IsNullOrEmpty(model.AppTitle) ? model.AppTitle : model.DealName, s.SellerName);
                        string txtUrl = string.Format("{0}/controlroom/ppon/setup.aspx?bid={1}", _config.SiteUrl, pro.BusinessHourGuid.ToString());
                        ProposalFacade.SendEmail(mailToUserTax, subjectTax, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, txtUrl));
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信]" + string.Join(",", mailToUserTax));
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信][End]");
                        ProposalFacade.ProposalPerformanceLogSet("NoTaxNotification", builder.ToString(), UserName);
                        #endregion
                    }
                }

                flag = true;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                logger.Error(ex);
                flag = false;
            }
            return Json(new
            {
                isSuccess = flag,
                multiDeals = multiDeals,
                message = error
            });
        }

        [Authorize]
        public override ActionResult ProposalPreview(int pid)
        {
            base.ProposalPreview(pid);
            return View("~/Views/Proposal/ProposalPreview.cshtml");
        }

        /// <summary>
        /// 下載合約
        /// </summary>
        /// <param name="pid">提案單單號</param>
        /// <param name="IsListCheck">是否為上架確認單</param>
        /// <returns></returns>
        public JsonResult PropsaolDownload(int pid, bool IsListCheck)
        {
            SalesProposalModel model;
            SellerCollection stores;//宅配沒再用
            ProposalStoreCollection psc;//所有的proposalstore//宅配沒再用
            Proposal pro = sp.ProposalGet(pid);
            ProposalFacade.GetPponDealData(pro, out model, out stores, out psc);

            string fileName = "";
            if (pro.SellerGuid == Guid.Empty)
                fileName = pro.BrandName;
            else
                fileName = model.SellerProperty.SellerName + "_" + pro.BrandName;

            DeliveryType type = (DeliveryType)pro.DeliveryType;
            Dictionary<string, string> dataList = ProposalFacade.GetPponDealDictionary(model, pro, IsListCheck);

            //草稿要抓新的status
            bool status = true;
            #region get template file

            string wordFile = string.Empty;
            string footer = Server.MapPath("~/template/SalesDocs/BusinessOrderFooter.docx");//蓋章

            #region 列印一般合約範本
            if (!IsListCheck)
            {

                if (status)
                {
                    wordFile = Server.MapPath("~/template/SalesDocs/ProposalContractProductNew.docx");

                }
                else
                {
                    //草稿
                    wordFile = Server.MapPath("~/template/SalesDocs/ProposalContractProductNewDraft.docx");
                }
            }
            #endregion

            #region 列印上架確認單範本
            if (IsListCheck)
            {
                wordFile = Server.MapPath("~/template/SalesDocs/ProposalContractProductListCheck.docx");
            }
            #endregion

            #endregion get template file

            fileName = fileName.Replace("{", "{{").Replace("}", "}}");
            // Load the document.
            using (Novacode.DocX document = Novacode.DocX.Load(wordFile))
            {


                if (!IsListCheck)
                {
                    //草稿要抓新的status
                    if (status)
                    {
                        //草稿階段不列印「用印」
                        document.InsertDocument(Novacode.DocX.Load(footer));
                    }
                }
                else
                {
                    fileName = fileName + "_上架確認單";
                }

                // Replace text in this document.
                foreach (KeyValuePair<string, string> item in dataList)
                {
                    document.ReplaceText(item.Key, !string.IsNullOrEmpty(item.Value) ? item.Value : string.Empty);
                }





                // Save changes made to this document.
                MemoryStream memoryStream = new MemoryStream();
                document.SaveAs(memoryStream);

                // print word
                Response.ContentType = "application/msword";
                Response.AddHeader("content-disposition", "attachment; filename=\"" + string.Format(fileName + "_{0}", DateTime.Now.ToString("yyyy_MM_dd")) + ".doc\"");
                memoryStream.WriteTo(Response.OutputStream);
                Response.End();
            } // Release this document from memory.

            return Json(new { Success = true });
        }



        #region 商品資料
        [Authorize]
        public ActionResult ProductOption(Guid sid)
        {
            Seller seller = sp.SellerGet(sid);
            if (!seller.IsLoaded)
            {
                throw new Exception(string.Format("賣家資料異常" + sid));
            }
            List<object> vbsSellers = new List<object>();
            vbsSellers.Add(new
            {
                SellerGuid = seller.Guid,
                SellerId = seller.SellerId,
                SellerName = seller.SellerName
            });
            ViewBag.VbsSellers = new JsonSerializer().Serialize(vbsSellers);
            ViewBag.IsVbsPro = false;
            ViewBag.SellerGuid = seller.Guid;

            Dictionary<int, string> stockFilter = GetStockFilter();
            ViewBag.StockFilter = stockFilter;
            ViewBag.SiteUrl = _config.SiteUrl;

            return View("~/Views/Proposal/ProductOption.cshtml");
        }
        [Authorize]
        [HttpPost]
        public ActionResult GetProductOption(int pageStart, int pageLength, int? productNo, string productBrandName, string productName, 
            string gtin, string mpn, string productCode, string items, int? stockStatus, bool? hideDisabled, Guid sellerGuid, Guid? bid, 
            int? uniqueId, int warehouseType)
        {
            var vpiList = PponFacade.GetProductOptionListForSal(sellerGuid, productNo, productBrandName, productName, gtin, productCode, items, stockStatus,
                hideDisabled, bid, uniqueId, warehouseType);
            var result = PponFacade.GetBaseProductOption(vpiList, pageStart, pageLength);
            return Json(result);
        }
        [Authorize]
        public override ActionResult ProductOptionContent(Guid sid, Guid? pid, Guid? did)
        {

            base.ProductOptionContent(sid, pid, did);
            ViewBag.IsVbsPro = false;
            ViewBag.SellerGuid = sid;
            return View("~/Views/Proposal/ProductOptionContent.cshtml");
        }
        [HttpPost]
        [Authorize]
        public ActionResult ProductOptionContent(FormCollection forms)
        {
            ProductInfoModel product = new JsonSerializer().Deserialize<ProductInfoModel>(forms["postData"]);
            dynamic obj = SaveProducts(product,UserName);
            return Json(obj);
        }

        [HttpPost]
        [Authorize]
        public ActionResult ProductOptionContentByPro(FormCollection forms)
        {
            ProductInfoModel product = new JsonSerializer().Deserialize<ProductInfoModel>(forms["ProductInfoData"]);
            dynamic obj = SaveProducts(product, UserName);
            return Json(obj);
        }
        /// <summary>
        /// 刪除商品
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult DeleteProduct(Guid pid)
        {
            dynamic flag = DeleteBaseProduct(pid, UserName);
            return Json(flag);
        }

        /// <summary>
        /// 停用商品
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult DisabledProduct(Guid did, int status)
        {
            dynamic flag = DisabledBaseProduct(did, status, UserName);
            return Json(flag);
        }

        [Authorize]
        public ActionResult BatchCreateProposal()
        {
            return View("~/Views/Proposal/BatchCreateProposal.cshtml");
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetAnalyzeDataByBid(List<string> bids)
        {
            List<dynamic> rtnList = new List<dynamic>();
            foreach(string bid in bids.Distinct())
            {
                if (string.IsNullOrEmpty(bid.Trim()))
                {
                    continue;
                }
                Guid businessHourGuid = Guid.Empty;
                Guid.TryParse(bid.Trim(), out businessHourGuid);
                if(businessHourGuid == Guid.Empty)
                {
                    rtnList.Add(new {
                        Bid = businessHourGuid,
                        ResultId = (int)ProposalBatchBidStatus.Error,
                        ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                        Message = "Bid錯誤"
                    });
                    continue;
                }
                else
                {
                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(businessHourGuid);
                    if(theDeal.DeliveryType != (int)DeliveryType.ToHouse)
                    {
                        rtnList.Add(new
                        {
                            Bid = businessHourGuid,
                            ResultId = (int)ProposalBatchBidStatus.Error,
                            ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                            Message = "不是宅配檔次"
                        });
                        continue;
                    }
                    else
                    {
                        if (!User.IsInRole(MemberRoles.SalesAssistant.ToString()) && !User.IsInRole(MemberRoles.Production.ToString()))
                        {
                            if (theDeal.DevelopeSalesId != UserId)
                            {
                                rtnList.Add(new
                                {
                                    Bid = businessHourGuid,
                                    ResultId = (int)ProposalBatchBidStatus.Error,
                                    ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                                    Message = "操作帳號沒有該檔所屬權限"
                                });
                                continue;
                            }
                        }                
                        
                        if(theDeal.BusinessHourOrderTimeE <= DateTime.Now.Date || theDeal.Slug != null)
                        {
                            rtnList.Add(new
                            {
                                Bid = businessHourGuid,
                                ResultId = (int)ProposalBatchBidStatus.Error,
                                ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                                Message = "無法處理已結檔舊提案"
                            });
                            continue;
                        }
                    }

                    Proposal pro = sp.ProposalGet(businessHourGuid);
                    if (pro.IsLoaded)
                    {
                        if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                        {
                            rtnList.Add(new
                            {
                                Bid = businessHourGuid,
                                ResultId = (int)ProposalBatchBidStatus.Error,
                                ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                                Message = "無法處理新版提案"
                            });
                            continue;
                        }
                    }

                    ViewComboDealCollection comboDeals = pp.GetViewComboDealAllByBid(businessHourGuid);
                    if(comboDeals.Count > 0)
                    {
                        if(comboDeals.FirstOrDefault().MainBusinessHourGuid != businessHourGuid)
                        {
                            rtnList.Add(new
                            {
                                Bid = businessHourGuid,
                                ResultId = (int)ProposalBatchBidStatus.Error,
                                ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                                Message = "子檔Bid無法處理"
                            });
                            continue;
                        }                        
                    }

                    rtnList.Add(new
                    {
                        Bid = businessHourGuid,
                        ResultId = (int)ProposalBatchBidStatus.Ready,
                        ResultMsg = ProposalBatchBidStatus.Ready.ToString(),
                        Message = string.Empty
                    });
                }
            }
            return Json(rtnList);
        }

        /// <summary>
        /// 舊宅配&OD檔長出新版提案單
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult BatchCreateHouseProposal(string bid)
        {
            Guid businessHourGuid = Guid.Empty;
            Guid.TryParse(bid, out businessHourGuid);

            IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(businessHourGuid);
            if (theDeal.BusinessHourOrderTimeE <= DateTime.Now || theDeal.Slug != null)
            {
                return Json(new
                {
                    IsSuccess = false,
                    ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                    Message = "無法處理已結檔舊提案"
                });
            }

            int id = 0;
            Proposal pro = sp.ProposalGet(businessHourGuid);
            Proposal oriPro = pro.Clone();            
            if (pro.IsLoaded)
            {
                if(pro.ProposalSourceType == (int)ProposalSourceType.House)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                        Message = "無法處理新版提案"
                    });
                }
                else
                {
                    if(pro.DealType == (int)ProposalDealType.Travel || pro.DealType == (int)ProposalDealType.Piinlife)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            ResultMsg = ProposalBatchBidStatus.Error.ToString(),
                            Message = "無法處理旅遊/品生活提案"
                        });
                    }
                }
            }
            else
            {
                pro = new Proposal();
                pro.BusinessHourGuid = businessHourGuid;
            }
            ProposalFacade.ProposalLog(pro.Id, new JsonSerializer().Serialize(oriPro), UserName, ProposalLogType.HTML);

            //舊版宅配提案單換成新版UI
            pro.ProposalSourceType = (int)ProposalSourceType.House;

            
            BusinessHour bh = pp.BusinessHourGet(businessHourGuid);
            if (bh.IsLoaded)
            {
                #region 後台
                DealProperty dp = pp.DealPropertyGet(businessHourGuid);
                //dp.IsHouseNewVer = true;
                //pp.DealPropertySet(dp);
                DealAccounting da = pp.DealAccountingGet(businessHourGuid);
                Item item = pp.ItemGetByBid(businessHourGuid);
                GroupOrder go = op.GroupOrderGetByBid(businessHourGuid);
                CouponEventContent cec = pp.CouponEventContentGetByBid(businessHourGuid);
                PponStoreCollection psc = pp.PponStoreGetListByBusinessHourGuid(businessHourGuid);
                #endregion 後台

                TransactionOptions options = new TransactionOptions();
                options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
                using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
                {
                    #region Proposal
                    SystemCode sc = sysProv.ParentSystemCodeGetByCodeGroupId(SystemCodeGroup.DealType.ToString(), Convert.ToInt32(dp.DealType));
                    pro.SellerGuid = bh.SellerGuid;
                    pro.DeliveryType = (int)DeliveryType.ToHouse;
                    pro.DealType = (int)ProposalDealType.Product;
                    pro.DevelopeSalesId = dp.DevelopeSalesId;
                    pro.OperationSalesId = dp.OperationSalesId;
                    pro.DealType1 = sc.CodeId;
                    pro.DealName = item.ItemName;
                    pro.DealType2 = dp.DealType;
                    pro.DealTypeDetail = dp.DealTypeDetail;
                    pro.AncestorSequenceBusinessHourGuid = dp.AncestorSequenceBusinessHourGuid;
                    pro.PicAlt = bh.PicAlt;
                    pro.OrderTimeS = bh.BusinessHourOrderTimeS;
                    pro.OrderTimeE = bh.BusinessHourOrderTimeE;
                    if(da.VendorReceiptType == (int)VendorReceiptType.Invoice && Helper.IsFlagSet(go.Status.GetValueOrDefault(0), GroupOrderStatus.NoTax))
                    {
                        pro.VendorReceiptType = (int)VendorReceiptType.NoTaxInvoice;
                    }
                    else
                    {
                        pro.VendorReceiptType = da.VendorReceiptType;
                    }
                    
                    pro.RemittanceType = da.RemittanceType;
                    if (!pro.IsLoaded)
                    {
                        pro.ProposalCreatedType = (int)ProposalCreatedType.Sales;
                        pro.CreateId = UserName;
                        pro.CreateTime = DateTime.Now;
                    }
                    if(pro.DeliveryMethod == null)
                    {
                        pro.DeliveryMethod = (int)ProposalDeliveryMethod.Normal;
                    }

                    pro.ShipType = dp.ShipType.GetValueOrDefault(0);
                    pro.ShipText1 = dp.ProductUseDateStartSet.GetValueOrDefault(0).ToString();
                    pro.ShipText2 = dp.ProductUseDateEndSet.GetValueOrDefault(0).ToString();
                    pro.ShipText3 = dp.Shippingdate.GetValueOrDefault(0).ToString();
                    pro.ShippingdateType = dp.ShippingdateType;

                    pro.ModifyId = UserName;
                    pro.ModifyTime = DateTime.Now;
                    pro.Mohist = false;
                    #region 狀態
                    pro.ApplyFlag = (int)ProposalApplyFlag.Apply;
                    pro.ApproveFlag = (int)ProposalApplyFlag.Initial;
                    pro.BusinessCreateFlag = (int)ProposalBusinessCreateFlag.Initial;
                    pro.BusinessFlag = (int)ProposalBusinessFlag.Initial;
                    pro.EditFlag = (int)ProposalEditorFlag.Initial;
                    pro.ListingFlag = (int)ProposalListingFlag.Initial;
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));  //等待商家覆核           
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));   //商家編輯中
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));          //提案覆核

                    pro.SpecialFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SpecialFlag, ProposalSpecialFlag.YearContract));          //長期上架，會影響出貨日期

                    pro.PassSeller = 1;

                    pro.Status = (int)ProposalStatus.Approve;
                    #endregion 狀態

                    pro.NoTax = Helper.IsFlagSet(go.Status ?? 0, GroupOrderStatus.NoTax);
                    
                    sp.ProposalSet(pro);

                    id = pro.Id;
                    #endregion Proposal

                    #region ProposalStore
                    pp.ProposalStoreDelete(pro.Id);
                    ProposalStoreCollection psList = new ProposalStoreCollection();

                    foreach (PponStore ps in psc)
                    {
                        psList.Add(new ProposalStore()
                        {
                            ProposalId = pro.Id,
                            StoreGuid = pro.SellerGuid,
                            TotalQuantity = null,
                            VbsRight = (int)VbsRightFlag.Verify + (int)VbsRightFlag.Accouting + (int)VbsRightFlag.ViewBalanceSheet,
                            ResourceGuid = Guid.NewGuid(),
                            CreateId = UserName,
                            CreateTime = DateTime.Now
                        });
                    }
                    pp.ProposalStoreSetList(psList);
                    #endregion ProposalStore

                    #region ProposalCouponEventContent
                    ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
                    if (!pcec.IsLoaded)
                    {
                        pcec = new ProposalCouponEventContent();
                        pcec.ProposalId = pro.Id;
                    }
                    pcec.Title = cec.Title;
                    pcec.AppTitle = cec.AppTitle;
                    pcec.Name = cec.Name;
                    pcec.ImagePath = cec.ImagePath;
                    pcec.Introduction = cec.Introduction;
                    //pcec.Restrictions = cec.Restrictions;
                    pcec.Description = cec.Description;
                    pcec.Remark = cec.Remark;
                    pcec.CouponUsage = cec.CouponUsage;
                    pcec.AppDealPic = cec.AppDealPic;
                    pcec.ProductSpec = cec.ProductSpec;

                    pp.ProposalCouponEventContentSet(pcec);
                  
                    #endregion ProposalCouponEventContent

                    #region proposal_category_deals
                    CategoryDealCollection cds = pp.CategoryDealsGetList(businessHourGuid);
                    sp.ProposalCategoryDealsDelete(pro.Id);
                    ProposalCategoryDealCollection pcds = new ProposalCategoryDealCollection();
                    foreach (CategoryDeal cd in cds)
                    {
                        pcds.Add(new ProposalCategoryDeal {
                            Pid = pro.Id,
                            Cid = cd.Cid,
                            Startdt = cd.Startdt,
                            Enddt = cd.Enddt
                        });
                    }
                    sp.ProposalCategoryDealSetList(pcds);
                    #endregion proposal_category_deals

                    

                    sp.ProposalMultiDealDeleteByPid(pro.Id);
                    ComboDealCollection comboDeals = pp.GetComboDealByBid(businessHourGuid, false);
                    if (comboDeals.Count > 0)
                    {
                        //母子檔
                        int combo_idx = 1;
                        foreach (ComboDeal combo in comboDeals)
                        {
                            GroupOrder combo_go = op.GroupOrderGetByBid(combo.BusinessHourGuid);
                            if (combo_go.IsLoaded)
                            {
                                if(Helper.IsFlagSet(combo_go.Status.GetValueOrDefault(0) ,GroupOrderStatus.Completed) || combo_go.Slug != null)
                                {
                                    //已結檔，不產生方案
                                    continue;
                                }
                            }
                            AddProposalMultiDeal(pro.Id, combo.BusinessHourGuid, combo_idx);
                            combo_idx++;
                        }
                    }
                    else
                    {
                        //單檔
                        AddProposalMultiDeal(pro.Id, businessHourGuid, 1);
                    }

                    if (oriPro.Id != 0)
                    {
                        ProposalFacade.ProposalLog(pro.Id, "置換新版UI(轉舊版提案單)", UserName);
                    }
                    else
                    {
                        ProposalFacade.ProposalLog(pro.Id, "置換新版UI(轉OD)", UserName);
                    }
                    

                    scope.Complete();
                }
            }

            return Json(new {
                IsSuccess = true,
                ResultMsg = string.Empty,
                Message = id
            });
        }

        private void AddProposalMultiDeal(int pid, Guid bid, int sort)
        {
            BusinessHour combo_bh = pp.BusinessHourGet(bid);
            DealProperty combo_dp = pp.DealPropertyGet(bid);
            //combo_dp.IsHouseNewVer = true;
            pp.DealPropertySet(combo_dp);
            Item combo_item = pp.ItemGetByBid(bid);
            DealCostCollection combo_dc = pp.DealCostGetList(bid);
            CouponFreightCollection combo_cf = pp.CouponFreightGetList(bid);
            decimal cost = 0;
            if (combo_dc.Count > 0)
            {
                cost = combo_dc.ToList().OrderByDescending(x => x.Cost).FirstOrDefault().Cost.GetValueOrDefault(0);
            }
            decimal freight = 0;
            if (combo_cf.Count > 0)
            {
                freight = combo_cf.Where(x => x.StartAmount == 0).FirstOrDefault().FreightAmount;
            }

            ProposalMultiDeal pmd = new ProposalMultiDeal()
            {
                Pid = pid,
                Bid = bid,
                ItemName = combo_item.ItemName,
                OrigPrice = combo_item.ItemOrigPrice,
                ItemPrice = combo_item.ItemPrice,
                Cost = cost,
                GroupCouponBuy = 0,
                GroupCouponPresent = 0,
                SlottingFeeQuantity = 0,
                OrderMaxPersonal = combo_item.MaxItemCount.GetValueOrDefault(0),
                OrderTotalLimit = Convert.ToInt32(combo_bh.OrderTotalLimit.GetValueOrDefault(0)),
                AtmMaximum = combo_bh.BusinessHourAtmMaximum,
                Freights = freight,
                NoFreightLimit = 0,
                Options = string.Empty,
                SellerOptions = string.Empty,
                Sort = sort,
                CreateId = UserName,
                CreateTime = DateTime.Now,
                ModifyId = UserName,
                ModifyTime = DateTime.Now,
                Unit = "入",
                QuantityMultiplier = combo_dp.QuantityMultiplier.GetValueOrDefault(1).ToString(),
                ComboPackCount = combo_dp.ComboPackCount.ToString(),
                BoxQuantityLimit = "0"
            };

            sp.ProposalMultiDealsSet(pmd);
        }
        #endregion 商品資料

        #endregion action

        #region private method
        /// <summary>
        /// 建檔前檢核
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool CheckCreateBusinessHour(Proposal pro, out string message)
        {
            Seller s = sp.SellerGet(pro.SellerGuid);
            #region 核銷店家
            ProposalStoreCollection prostore = pp.ProposalStoreGetListByProposalId(pro.Id);
            if (prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Verify)).Count() == 0 || prostore.Where(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.Accouting)).Count() == 0)
            {
                message = "請勾選核銷店家";
                return false;
            }
            #endregion 核銷店家

            #region 店家審核
            List<Seller> TempStatus = new List<Seller>();
            List<Seller> Accouting = new List<Seller>();
            ProposalStoreCollection psc = pp.ProposalStoreGetListByProposalId(pro.Id);
            foreach (var ps in psc)
            {
                Seller AddSeller = sp.SellerGet(ps.StoreGuid);
                TempStatus.Add(AddSeller);

                if (Helper.IsFlagSet(ps.VbsRight, VbsRightFlag.Accouting))
                {
                    if (_config.IsRemittanceFortnightly && SellerFacade.IsAgreeNewContractSeller(AddSeller.Guid, (int)DeliveryType.ToHouse))
                    {
                        if (string.IsNullOrEmpty(AddSeller.CompanyName) || string.IsNullOrEmpty(AddSeller.CompanyBossName) || AddSeller.CompanyBankCode == "-1" ||
                        string.IsNullOrEmpty(AddSeller.CompanyBranchCode) || string.IsNullOrEmpty(AddSeller.CompanyID) || string.IsNullOrEmpty(AddSeller.CompanyAccount) ||
                        string.IsNullOrEmpty(AddSeller.CompanyAccountName) || string.IsNullOrEmpty(AddSeller.AccountantName) || string.IsNullOrEmpty(AddSeller.AccountantTel) ||
                        string.IsNullOrEmpty(AddSeller.CompanyEmail) || AddSeller.VendorReceiptType == null)
                            Accouting.Add(AddSeller);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(AddSeller.CompanyName) || string.IsNullOrEmpty(AddSeller.CompanyBossName) || AddSeller.CompanyBankCode == "-1" ||
                        string.IsNullOrEmpty(AddSeller.CompanyBranchCode) || string.IsNullOrEmpty(AddSeller.CompanyID) || string.IsNullOrEmpty(AddSeller.CompanyAccount) ||
                        string.IsNullOrEmpty(AddSeller.CompanyAccountName) || string.IsNullOrEmpty(AddSeller.AccountantName) || string.IsNullOrEmpty(AddSeller.AccountantTel) ||
                        string.IsNullOrEmpty(AddSeller.CompanyEmail) || AddSeller.RemittanceType == null || AddSeller.VendorReceiptType == null)
                            Accouting.Add(AddSeller);
                    }
                }

                
            }
            if (!TempStatus.Contains(s))
                TempStatus.Add(s);

            //尚未完成審核的商家
            TempStatus = TempStatus.Where(x => x.TempStatus == (int)SellerTempStatus.Applied || x.TempStatus == (int)SellerTempStatus.Returned).ToList();

            if (TempStatus.Count() != 0)
            {
                message = "商家尚未審核。尚未審核清單：" + string.Join("、", TempStatus.Select(x => x.SellerId));
                return false;
            }
            if (Accouting.Count() != 0)
            {
                message = "財務資料未填寫完整，請完成填寫，並確實完成【申請核准】。未填寫完整清單：" + string.Join("、", Accouting.Select(x => x.SellerId));
                return false;
            }
            #endregion 店家審核

            #region 店家聯絡人
            int NormalSeller = 0;
            int ReturnPersonSeller = 0;
            List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(s.Contacts);
            NormalSeller = contacts.Where(x => x.Type == ((int)ProposalContact.Normal).ToString()).Count();
            ReturnPersonSeller = contacts.Where(x => x.Type == ((int)ProposalContact.ReturnPerson).ToString()).Count();
            if (pro.DeliveryType == (int)DeliveryType.ToHouse && (NormalSeller == 0 || ReturnPersonSeller == 0))
            {
                message = "宅配提案單須至少一個一般(頁確)聯絡人，一個退換貨聯絡人。請完成填寫，並確實完成【申請核准】";
                return false;
            }
            #endregion 店家聯絡人

            #region 合約            
            bool isContract = Helper.IsFlagSet(pro.ListingFlag, ProposalListingFlag.PaperContractCheck);
            if (isContract == false)
            {
                message = "請確認合約已上傳。";
                return false;
            }
            #endregion 合約
            #region 業務資料
            ProposalSalesModel proposalSalesModel = ProposalFacade.ProposalSaleGetByPid(pro.Id);
            if (!string.IsNullOrEmpty(SellerFacade.CheckSales(1, proposalSalesModel.DevelopeSalesEmail, proposalSalesModel.OperationSalesEmail)))
            {
                message = "負責業務1、2 欄位，沒有完整填入資料，無法建檔，請重新輸入。";
                return false;
            }
            #endregion 業務資料

            #region 相關欄位檢核
            //商品名稱
            if (string.IsNullOrEmpty(pro.DealName))
            {
                message = "請填寫商品名稱。";
                return false;
            }
            //請款單據
            if (pro.VendorReceiptType == null)
            {
                message = "請選擇請款單據。";
                return false;
            }
            //配送溫層
            if (pro.DeliveryMethod == 0)
            {
                message = "請選擇配送溫層。";
                return false;
            }
            //商品來源
            if (pro.DealSource == 0)
            {
                message = "請選擇商品來源。";
                return false;
            }
            if (pro.ShipType == (int)DealShipType.Other)
            {
                if(pro.DeliveryTimeE != null)
                {
                    if(pro.DeliveryTimeE <= DateTime.Now)
                    {
                        message = "出貨類型，「最晚出貨日」不可小於今日。";
                        return false;
                    }
                    if (pro.OrderTimeE != null)
                    {
                        if (pro.OrderTimeE.Value > pro.DeliveryTimeE.Value)
                        {
                            message = "「檔期結束日」不可大於「最晚出貨日」";
                            return false;
                        }
                    }
                }
            }
            //銷售方案
            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pro.Id);
            if (pmds.Count() == 0)
            {
                message = "請填寫銷售方案。";
                return false;
            }
            else
            {
                foreach(ProposalMultiDeal pmd in pmds)
                {
                    if (string.IsNullOrEmpty(pmd.ItemName))
                    {
                        message = string.Format("{0}方案與商品名稱不得為空值", pmd.BrandName + pmd.ItemName);
                        return false;
                    }
                    if (string.IsNullOrEmpty(pmd.QuantityMultiplier) || string.IsNullOrEmpty(pmd.Unit))
                    {
                        message = string.Format("{0}組數不得為空值", pmd.BrandName + pmd.ItemName);
                        return false;
                    }
                    if (pmd.ItemPrice == 0)
                    {
                        message = string.Format("{0}售價不得為0", pmd.BrandName + pmd.ItemName);
                        return false;
                    }
                    if (pmd.OrderMaxPersonal == 0)
                    {
                        message = string.Format("{0}每人限購不得為0", pmd.BrandName + pmd.ItemName);
                        return false;
                    }
                    if (string.IsNullOrEmpty(pmd.Options))
                    {
                        message = string.Format("{0}銷售商品與規格不得為空值", pmd.BrandName + pmd.ItemName);
                        return false;
                    }
                }
            }
            


            //商品分線
            if (pro.DealType2 == 0)
            {
                message = "請選擇商品分線。";
                return false;
            }
            //關鍵字
            if (string.IsNullOrEmpty(pro.PicAlt))
            {
                message = "請填寫關鍵字。";
                return false;
            }
            //比價資訊
            if (!string.IsNullOrEmpty(pro.SaleMarketAnalysis))
            {
                //檢查比價內容
            }
            else
            {
                message = "請填寫比價資訊。";
                return false;
            }
            //頻道
            ProposalCategoryDealCollection categories = sp.ProposalCategoryDealsGetList(pro.Id);
            if (categories.Count() == 0)
            {
                message = "請選擇頻道。";
                return false;
            }
            //對帳方式
            //if (pro.RemittanceType == 0)
            //{
            //    message = "請選擇對帳方式。";
            //    return false;
            //}
            //權益說明
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pro.Id);
            if (pcec.IsLoaded)
            {
                if (string.IsNullOrEmpty(pcec.Restrictions))
                {
                    message = "請填寫權益說明。";
                    return false;
                }
            }
            else
            {
                message = "請填寫權益說明。";
                return false;
            }
            
            //必買特色
            if (string.IsNullOrEmpty(pcec.Remark) || pcec.Remark.Length < 10)
            {
                message = "請填寫必買特色，字數至少10字元。";
                return false;
            }
            //商品詳細介紹
            if (string.IsNullOrEmpty(pcec.Description) || pcec.Description.Length < 50)
            {
                message = "請填寫商品詳細介紹，字數至少50字元。";
                return false;
            }
            //商品規格
            if (string.IsNullOrEmpty(pcec.ProductSpec) || pcec.ProductSpec.Length < 50)
            {
                message = "請填寫商品規格，字數至少50字元。";
                return false;
            }
            //電腦版網頁主圖
            if (string.IsNullOrEmpty(pcec.ImagePath))
            {
                message = "請上傳電腦版網頁主圖。";
                return false;
            }
            //APP / 行動版網頁主圖
            if (string.IsNullOrEmpty(pcec.AppDealPic))
            {
                message = "請上傳APP/行動版網頁主圖。";
                return false;
            }
            //Google廣告去背圖
            if (string.IsNullOrEmpty(pcec.RemoveBgPic))
            {
                message = "請上傳Google廣告去背圖。";
                return false;
            }
            //檔次行銷文案
            if (string.IsNullOrEmpty(pcec.Title))
            {
                message = "請填寫檔次行銷文案。";
                return false;
            }

            #endregion 相關欄位檢核

            #region Multi_Deals
            bool isDealOn = false;
            if(pro.BusinessHourGuid != null)
            {
                BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                if (bh.IsLoaded)
                {
                    if(bh.BusinessHourOrderTimeS <= DateTime.Now && bh.BusinessHourOrderTimeE >= DateTime.Now)
                    {
                        isDealOn = true;
                    }
                }
            }
            if (!isDealOn)
            {
                //上檔後不需再檢查庫存，否則賣出就無法送單了
                bool isStock = CheckMultiOptionSpecStock(pro.Id, out message);
                if (!isStock)
                {
                    return false;
                }
            }            
            #endregion Multi_Deals

            message = string.Empty;
            return true;
        }
        /// <summary>
        /// 新版宅配提案單建檔
        /// </summary>
        /// <param name="pro"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        private bool CreateBusinessHour(Proposal pro, out string message)
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建檔開始]" + pro.Id);
                Proposal vpro = sp.ProposalGet(pro.Id);
                ProposalMultiDealCollection multiDeals = ProposalFacade.ProposalMultiDealGetByPid(pro.Id);
                if (multiDeals != null && multiDeals.Count > 0)
                {
                    Guid bid = Guid.Empty;
                    ProposalMultiDeal model = multiDeals.OrderBy(x => x.ItemPrice).FirstOrDefault();

                    #region 建立新檔
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建立新檔開始]");
                    bid = PponFacade.PponDealCreateByHouseProposal(UserName, vpro, model, multiDeals.ToList());
                    if(bid == null && bid == Guid.Empty)
                    {
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[UniqueId]" + MemberFacade.GetUniqueId(_config.ServiceEmail));
                        throw new Exception("[母檔建檔失敗]");
                    }
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建立新檔結束]");

                    //多重選項
                    if (!string.IsNullOrEmpty(model.Options))
                    {
                        //List<dynamic> uiOptions;
                        //if (!PponFacade.TryParseOptions(model.Options, out uiOptions))
                        //{
                        //    message = "多重選項格式有誤，請重新輸入。";
                        //    return false;
                        //}

                        //foreach (var option in uiOptions)
                        //{
                        //    if (option.OptItem.Length > 50)
                        //    {
                        //        message = "多重選項格式有誤或項目字數大於50，請重新輸入。";
                        //        return false;
                        //    }

                        //    if (option.OptItemNo != null && option.OptItemNo.Length > 100)
                        //    {
                        //        message = "多重選項格式有誤或貨號大於100，請重新輸入。";
                        //        return false;
                        //    }
                        //}

                        //PponFacade.ProcessOptions(model.Options, bid, pp.DealPropertyGet(bid).UniqueId, UserName);
                    }
                    #endregion

                    if (bid != Guid.Empty)
                    {
                        #region 多檔次建立
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[多檔次建立開始]");
                        if (multiDeals.Count > 1)
                        {
                            var sortbyMultiDeals = multiDeals.OrderBy(x => x.Sort);
                            foreach (ProposalMultiDeal sub in sortbyMultiDeals)
                            {
                                string msg;
                                Guid newGuid;
                                PponFacade.CopyDeal(bid, true, false, out msg, out newGuid, UserName);
                                if (newGuid != Guid.Empty)
                                {
                                    PponFacade.PponDealUpdateByHouseroposal(newGuid, UserName, vpro, sub);
                                }
                                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建檔時須補回Bid]");
                                //建檔時須補回bid
                                ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(sub.Id);
                                pmd.Bid = newGuid;
                                sp.ProposalMultiDealsSet(pmd);
                            }
                        }
                        else
                        {
                            //單檔也須補bid
                            ProposalMultiDeal pmd = ProposalFacade.ProposalMultiDealGetById(model.Id);
                            pmd.Bid = bid;
                            sp.ProposalMultiDealsSet(pmd);
                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[多檔次建立結束]");
                        #endregion

                        #region 更新提案單串接資訊
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[更新提案單串接資訊開始]");
                        //pro.BusinessHourGuid = bid;
                        pro.BusinessCreateFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessCreateFlag, ProposalBusinessCreateFlag.Created));
                        pro.Status = SellerFacade.GetFlagForProposalFlagCheck(vpro);
                        pro.ModifyId = UserName;
                        pro.ModifyTime = DateTime.Now;
                        sp.ProposalSet(pro);
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[更新提案單串接資訊結束]");

                        ProposalFacade.ProposalLog(pro.Id, "[系統] 建檔", UserName);

                        #endregion

                        #region 不可使用折價券黑名單
                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.NotAllowedDiscount))
                        {
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[不可使用折價券黑名單開始]");
                            PromotionFacade.DiscountLimitSet(bid, UserName, DiscountLimitType.Enabled);
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[不可使用折價券黑名單結束]");
                        }
                        #endregion
                    }
                    else
                    {
                        message = "建檔失敗，請洽IT人員。";
                        return false;
                    }
                }
                else
                {
                    message = "多檔次資料不正確，請至少設定一個檔次。";
                    return false;
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[建檔結束]");
            }
            catch (Exception ex)
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + "[Error]" + ex.Message + ex.StackTrace + ex.Source);
            }
            finally
            {
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/CreateBusinessHour", builder.ToString(), UserName);
            }


            message = string.Empty;
            return true;
        }
        #endregion private method

        #region web method

        /// <summary>
        /// 業務審核通過
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public ActionResult SaleConfirmProposal(int pid)
        {
            StringBuilder builder = new StringBuilder();
            string eMessage = string.Empty;
            try
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Begin]");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[pid]" + pid);
                Proposal pro = sp.ProposalGet(pid);
                ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
                if (pro.IsLoaded)
                {
                    //未來是否要鎖誰能審核
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[檢查權限]");
                    string privilegeUri = "/sal/ProposalContent.aspx";
                    bool privilege = IsInSystemFunctionPrivilege(UserName, LunchKingSite.Core.SystemFunctionType.QCcheck, privilegeUri);
                    if (!privilege)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "您無業務審核權限"
                        });
                    }

                    if (pro.ProposalCreatedType == (int)ProposalCreatedType.Seller
                        && Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.SaleEditor)
                        && !Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "請先申請商家覆核"
                        });
                    }

                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[建檔前資料檢核][Begin]");
                    string message = string.Empty;
                    bool isChk = CheckCreateBusinessHour(pro, out message);
                    if (!isChk)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = message
                        });
                    }

                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[建檔前資料檢核][End]");
                    message = string.Empty;
                    if (Helper.IsFlagSet(pro.ApplyFlag, ProposalApplyFlag.Apply))
                    {
                        #region 建檔
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[建檔][Begin]");
                        if (pro.BusinessHourGuid != null)
                        {
                            BusinessHour bh = sp.BusinessHourGet(pro.BusinessHourGuid.Value);
                            if (!bh.IsLoaded)
                            {
                                bool isCreate = CreateBusinessHour(pro, out message);
                                if (!isCreate)
                                {
                                    return Json(new
                                    {
                                        IsSuccess = false,
                                        Message = "建檔失敗"
                                    });
                                }
                            }
                        }
                        else
                        {
                            return Json(new
                            {
                                IsSuccess = false,
                                Message = "Bid資料異常"
                            });
                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[建檔][End]");
                        #endregion 建檔

                        #region 業務審核
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[業務審核][Begin]");
                        pro.ApproveFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ApproveFlag, ProposalApproveFlag.QCcheck));
                        pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));
                        pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.ReturnToSales));
                        pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                        pro.Status = (int)ProposalStatus.Approve;
                        sp.ProposalSet(pro);
                        ProposalFacade.ProposalLog(pid, "[業務確認] ", UserName, ProposalLogType.Initial);
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[業務審核][End]");
                        #endregion 業務審核

                        #region 同步後臺檔次
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][Begin]");
                        string cRst = ProposalFacade.ProposalFlowCompleted(pro, true, UserName);
                        if (!string.IsNullOrEmpty(cRst))
                        {
                            //還原審核狀態
                            pro.ApproveFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ApproveFlag, ProposalApproveFlag.QCcheck));
                            pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));
                            sp.ProposalSet(pro);

                            return Json(new
                            {
                                IsSuccess = false,
                                Message = string.Format("同步失敗「{0}」", cRst)
                            });
                        }
                        builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][End]");
                        #endregion 同步後臺檔次

                        #region 若為免稅標記，寄送通知信予 免稅通知人員

                        if (Helper.IsFlagSet(pro.SpecialFlag, ProposalSpecialFlag.FreeTax))
                        {
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信][Begin]");
                            Seller s = SellerFacade.SellerGet(pro.SellerGuid);
                            ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                            List<string> mailToUserTax = FunctionPrivilegeManager.GetPrivilegesByFunctionType(SystemFunctionType.TaxFreeNotice, "/sal/ProposalContent.aspx");

                            //【ＸＸＸ】NO.單號 品名 商家
                            string subjectTax = string.Format("【免稅】 單號 NO.{0} {1} {2}", pro.Id, !string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.DealName, s.SellerName);
                            string txtUrl = string.Format("{0}/controlroom/ppon/setup.aspx?bid={1}", _config.SiteUrl, pro.BusinessHourGuid.ToString());
                            ProposalFacade.SendEmail(mailToUserTax, subjectTax, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, txtUrl));
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信]" + string.Join(",", mailToUserTax));
                            builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[免稅通知信][End]");
                        }

                        #endregion
                    }
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[End]");
            }
            catch (Exception ex)
            {
                eMessage = "業務審核時發生異常，請聯絡IT人員";
                logger.Error(ex);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Error]" + ex.Message + ex.StackTrace + ex.Source);
            }
            finally
            {
                ProposalFacade.ProposalPerformanceLogSet("/proposal/SaleConfirmProposal", builder.ToString(), UserName);
            }
            if (!string.IsNullOrEmpty(eMessage))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = eMessage
                });
            }

            return Json(new {
                IsSuccess = true,
                Message = string.Empty
            });
        }
        /// <summary>
        /// 刪除提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult DeleteProposal(int pid)
        {
            return DeleteBaseProposal(pid, UserName);
        }
        /// <summary>
        /// 複製提案單
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult CloneProposal(int pid, bool isWms)
        {
            return CloneBaseProposal(pid, ProposalCopyType.Same, false, UserName, isWms);
        }

        /// <summary>
        /// 製檔完成
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult PageCheckProposal(int pid)
        {
            StringBuilder builder = new StringBuilder();
            string eMessage = string.Empty;
            try
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Begin]");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[pid]" + pid);
                Proposal pro = sp.ProposalGet(pid);
                if (pro != null && pro.IsLoaded)
                {

                    //未來是否要鎖誰能審核


                    string privilegeUri = "/sal/ProposalContent.aspx";
                    bool privilege = IsInSystemFunctionPrivilege(UserName, LunchKingSite.Core.SystemFunctionType.PageCheck, privilegeUri);
                    if (!privilege)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "您無製檔權限"
                        });
                    }
                    if (!Helper.IsFlagSet(pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit))
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "主管尚未審核通過"
                        });
                    }

                    if (ProposalFacade.IsProposalProductionComplete(pro))
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "無需確認"
                        });
                    }

                    List<int> assignLogs = sp.ProposalAssignLogGetList(pro.Id).Select(x=>x.AssignFlag.GetValueOrDefault(0)).Distinct().ToList();

                    foreach (var item in Enum.GetValues(typeof(ProposalEditorFlag)))
                    {
                        if (assignLogs.Contains((int)item))
                        {
                            pro.EditFlag = Convert.ToInt32(Helper.SetFlag(true, pro.EditFlag, (ProposalEditorFlag)item));
                        }
                    }


                    pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(true, pro.ListingFlag, ProposalListingFlag.PageCheck));
                    pro.Status = (int)ProposalStatus.Listing;
                    sp.ProposalSet(pro);

                    ProposalFacade.ProposalLog(pid, "[製檔確認] ", UserName, ProposalLogType.Initial);

                    //同步後臺檔次
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][Begin]");
                    ProposalFacade.ProposalFlowCompleted(pro, true, UserName);
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][End]");

                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[end]");
                }
            }
            catch(Exception ex)
            {
                eMessage = "製檔時發生異常，請聯絡IT人員";
                logger.Error(ex);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Error]" + ex.Message + ex.StackTrace + ex.Source);
            }
            finally
            {
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/PageCheckProposal", builder.ToString(), UserName);
            }
            if (!string.IsNullOrEmpty(eMessage))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = eMessage
                });
            }

            return Json(new
            {
                IsSuccess = true,
                Message = string.Empty
            });
        }
        /// <summary>
        /// 暫停銷售
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult DealPause(int pid, bool pause)
        {
            return DealPauseSet(pid, pause);   
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetProductBusinessHour(int pro_no)
        {
            return ProductBusinessHourGet(pro_no);
        }
        /// <summary>
        /// 主管審核通過
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult ProposalAudit(int pid)
        {
            StringBuilder builder = new StringBuilder();
            string eMessage = string.Empty;
            try
            {
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Begin]");
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[pid]" + pid);
                Proposal pro = sp.ProposalGet(pid);
                if (pro != null && pro.IsLoaded)
                {

                    //未來是否要鎖誰能審核


                    if (!Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "業務尚未確認"
                        });
                    }

                    string privilegeUri = "/sal/ProposalContent.aspx";
                    bool privilege = IsInSystemFunctionPrivilege(UserName, LunchKingSite.Core.SystemFunctionType.ProposalAudit, privilegeUri);
                    if (!privilege)
                    {
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "您無主管審核權限"
                        });
                    }

                    pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(true, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));
                    pro.Status = (int)ProposalStatus.BusinessCheck;
                    sp.ProposalSet(pro);

                    ProposalFacade.ProposalLog(pid, "[主管確認] ", UserName, ProposalLogType.Initial);

                    if(ProposalFacade.IsProposalProductionComplete(pro) == false)
                    {
                        //寄信通知創意製檔
                        List<string> mailToUser = new List<string>();
                        mailToUser.AddRange(HumanFacade.GetUsesEmailInRole(MemberRoles.ProposalSetting.ToString()));

                        ViewEmployee emp = hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                        string SaleName = "";
                        string SaleMail = "";
                        if (emp.IsLoaded && emp != null)
                        {
                            SaleName = emp.EmpName;
                            SaleMail = emp.Email;

                            //string title = "【申請製檔通知】 NO." + pro.Id + "  " + pro.BrandName;
                            Seller s = sp.SellerGet(pro.SellerGuid);
                            string title = string.Format("【申請製檔通知】 單號 NO.{0} {1} {2}", pro.Id, pro.BrandName + pro.DealName, s.SellerName);

                            if (pro.OrderTimeS != null && pro.OrderTimeE != null)
                            {
                                title += string.Format("({0}~{1})", pro.OrderTimeS.Value.ToString("yyyy/MM/dd"), pro.OrderTimeE.Value.ToString("yyyy/MM/dd"));
                            }

                            string Memo = string.Empty;
                            if (pro.OrderTimeS != null && pro.OrderTimeE != null)
                            {
                                Memo += "上檔日期：" + pro.OrderTimeS.Value + "~" + pro.OrderTimeE.Value;
                            }
                            Memo += "• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信";
                            ProposalFacade.SendEmail(mailToUser, title,
                               ProposalFacade.SendSellerProposalMailContent(new Proposal
                               {
                                   Id = pro.Id,
                                   SellerGuid = pro.SellerGuid,
                                   CreateId = pro.CreateId
                               }, SaleName, _config.SiteUrl + "/sal/proposal/house/proposalcontent?pid=",
                               Memo));
                        }
                    }

                    //同步後臺檔次
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][Begin]");
                    ProposalFacade.ProposalFlowCompleted(pro, true, UserName);
                    builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[同步後臺檔次][End]");
                }
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[End]");
            }
            catch (Exception ex)
            {
                eMessage = "主管審核發生異常，請聯絡IT人員";
                logger.Error(ex);
                builder.AppendLine(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss:fff") + " " + "[Error]" + ex.Message + ex.StackTrace + ex.Source);
            }
            finally
            {
                ProposalFacade.ProposalPerformanceLogSet("/Proposal/ProposalAudit", builder.ToString(), UserName);
            }
            if (!string.IsNullOrEmpty(eMessage))
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = eMessage
                });
            }
            return Json(new
            {
                IsSuccess = true,
                Message = string.Empty
            });
        }

        /// <summary>
        /// 檢查商品是否已被提案單使用
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult CheckDeletedItemUsed(string item_guid)
        {
            string message = string.Empty;
            if (CheckProductUsedWmsPurchaseOrder(item_guid))
            {
                return Json(new
                {
                    Message = "已建立進倉單，無法停用",
                    IsSuccess = false
                });
            }
            bool flag = CheckItemUsedByProposal(item_guid, out message);
            if (flag)
            {
                return Json(new
                {
                    IsSuccess = false,
                    Message = string.Format("已被提案單「{0}」使用，不可刪除", message)
                });
            }
            else
            {
                return Json(new
                {
                    IsSuccess = true,
                    Message = string.Empty
                });
            }
        }

        /// <summary>
        /// 業務編輯提案
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SellerProposalSaleEditor(int pid)
        {
            bool flag = false;
            Proposal pro = sp.ProposalGet(pid);
            if (pro != null && pro.IsLoaded)
            {
                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck) 
                    && !ProposalFacade.IsProposalGrossMarginRestrictionComplete(pro)
                    && !ProposalFacade.IsProposalProductionComplete(pro)
                    )
                {
                    return Json(flag);
                }
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));  //等待商家覆核           
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));   //商家編輯中
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));          //提案覆核

                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleEditor);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Initial);
                flag = true;
            }
            return Json(flag);
        }

        /// <summary>
        /// 業務重新編輯提案
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult ReSellerProposalSaleEditor(int pid)
        {
            bool flag = false;
            Proposal pro = sp.ProposalGet(pid);
            if (pro != null && pro.IsLoaded)
            {
                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck)
                    && !ProposalFacade.IsProposalGrossMarginRestrictionComplete(pro)
                    && !ProposalFacade.IsProposalProductionComplete(pro)
                    )
                {
                    return Json(flag);
                }
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));  //等待商家覆核           
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SellerEditor));   //商家編輯中
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));          //提案覆核

                pro.ApplyFlag = (int)ProposalApplyFlag.Apply;
                pro.ApproveFlag = (int)ProposalApproveFlag.Initial;
                pro.BusinessFlag = (int)ProposalBusinessFlag.Initial;//影響主管確認
                pro.ListingFlag = (int)ProposalListingFlag.Initial;  //影響製檔
                pro.FlowCompleteTime = null;//要重跑流程

                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.SaleEditor);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Initial);
                flag = true;
            }
            return Json(flag);
        }
        /// <summary>
        /// 業務退回給商家
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SellerProposalReturned(int pid, string content)
        {
            bool flag = false;
            Proposal pro = sp.ProposalGet(pid);
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
            if (pro != null && pro.IsLoaded)
            {
                //變回草稿
                pro.SellerProposalFlag = (int)SellerProposalFlag.Apply;
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Returned));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.ReturnToSales));
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));
                pro.ApplyFlag = (int)ProposalApplyFlag.Initial;
                pro.Status = (int)ProposalStatus.Apply;
                pro.FlowCompleteTime = null;
                sp.ProposalSet(pro);


                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.Returned);
                ProposalFacade.ProposalLog(pid, "[" + title + "] " + content, UserName, ProposalLogType.Initial);

                #region mail(寄給 賣家一般聯絡人)
                List<string> MailUsers = new List<string>();

                //賣家一般聯絡人
                Seller seller = sp.SellerGet(pro.SellerGuid);
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                if (contacts != null)
                {
                    //一般聯絡人
                    foreach (Seller.MultiContracts c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString())
                        {
                            MailUsers.Add(c.ContactPersonEmail);
                        }
                    }
                }


                ViewEmployee emp = hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                string SaleName = "";
                string SaleMail = "";
                if (emp.IsLoaded && emp != null)
                {
                    SaleName = emp.EmpName;
                    SaleMail = emp.Email;

                    string Memo = string.Format(@"退件原因：{0}<p />
                                    • 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                    • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔", content);

                    Seller s = sp.SellerGet(pro.SellerGuid);
                    string subject = string.Format("【提案覆核通知】 單號 NO.{0} {1} {2}", pro.Id, !string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName + pro.DealName, s.SellerName);

                    ProposalFacade.SendEmail(MailUsers, subject,
                       ProposalFacade.SendSellerProposalMailContent(new Proposal
                       {
                           Id = pro.Id,
                           SellerGuid = pro.SellerGuid,
                           CreateId = pro.CreateId
                       }, SaleName, _config.SiteUrl + "/vbs/proposal/house/proposalcontent?pid=",
                       Memo));
                }

                #endregion
                flag = true;
            }
            return Json(flag);
        }


        /// <summary>
        /// 退回給業務
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="content"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SalesProposalReturned(int pid, string[] reasonItem,  string reason)
        {
            bool flag = false;
            Proposal pro = sp.ProposalGet(pid);
            if (pro != null && pro.IsLoaded)
            {

                pro.ApproveFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ApproveFlag, ProposalApproveFlag.QCcheck));
                pro.BusinessFlag = Convert.ToInt32(Helper.SetFlag(false, pro.BusinessFlag, ProposalBusinessFlag.ProposalAudit));
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PageCheck));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.ReturnToSales));
                if (!Helper.IsFlagSet(pro.SellerProposalFlag, SellerProposalFlag.Check))
                {
                    //業務自己提的可直接編輯
                    pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                }
                pro.FlowCompleteTime = null;
                pro.Status = (int)ProposalStatus.Apply;
                sp.ProposalSet(pro);


                string changeLog = "【退件】 " + reason + "|退件類型：" + string.Join(" / ", reasonItem);
                ProposalFacade.ProposalLog(pid, changeLog, UserName, ProposalLogType.Initial);

                #region 通知人員
                List<string> mailToUser = new List<string>();
                ViewEmployee deSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.DevelopeSalesId);
                ViewEmployee opSalesEmp = HumanFacade.ViewEmployeeGetByUserId(pro.OperationSalesId ?? 0);

                // 業務
                if (opSalesEmp.IsLoaded)
                    mailToUser.Add(opSalesEmp.Email);
                if (deSalesEmp.DeptId != EmployeeChildDept.S010.ToString())
                    mailToUser.Add(deSalesEmp.Email);
                //全國宅配特助群組
                mailToUser.Add(_config.SalesSpecialAssistantShippingEmail);
                
                Seller s = sp.SellerGet(pro.SellerGuid);
                string subject = string.Format("【退件】 單號 NO.{0} {1} {2}", pro.Id, pro.BrandName + pro.DealName, s.SellerName);

                string url = string.Format("{0}/sal/proposal/house/proposalcontent?pid=" + pid, _config.SiteUrl, pro.Id);
                ProposalFacade.SendEmail(mailToUser, subject, ProposalFacade.GetProposalBodyHtml(pro.Id, s.SellerName, deSalesEmp.EmpName, url, changeLog));
                #endregion 通知人員

                flag = true;
            }


            return Json(flag);
        }
        /// <summary>
        /// 業務申請商家覆核
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SellerProposalCheckWaitting(int pid)
        {
            bool flag = false;
            string message = string.Empty;
            Proposal pro = sp.ProposalGet(pid);
            ProposalCouponEventContent pcec = pp.ProposalCouponEventContentGetByPid(pid);
            if (pro != null && pro.IsLoaded)
            {

                if (Helper.IsFlagSet(pro.ApproveFlag, ProposalApproveFlag.QCcheck))
                {
                    return Json(new {
                        IsSuccess = flag,
                        Message = "業務已審核，無法再編輯。"
                    });
                }
                ResourceAcl acl = mp.ResourceAclGetListByResourceGuid(pro.SellerGuid);
                if (acl == null || !acl.IsLoaded)
                {
                    return Json(new
                    {
                        IsSuccess = flag,
                        Message = "尚未建立商家帳號。"
                    });
                }

                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.Apply));   //需要是草稿，商家才能查到
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(true, pro.SellerProposalFlag, SellerProposalFlag.CheckWaitting));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleEditor));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.Check));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleSend));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.SaleCreated));
                pro.SellerProposalFlag = Convert.ToInt32(Helper.SetFlag(false, pro.SellerProposalFlag, SellerProposalFlag.ReturnToSales));
                pro.ListingFlag = Convert.ToInt32(Helper.SetFlag(false, pro.ListingFlag, ProposalListingFlag.PaperContractCheck));//線上簽核取消，放在這是為了業務提案可共用
                pro.ApplyFlag = (int)ProposalApplyFlag.Initial;
                pro.Status = (int)ProposalStatus.Initial;
                sp.ProposalSet(pro);

                string title = Helper.GetLocalizedEnum(LunchKingSite.I18N.Phrase.ResourceManager, SellerProposalFlag.CheckWaitting);
                ProposalFacade.ProposalLog(pid, "[" + title + "] ", UserName, ProposalLogType.Initial);


                #region mail(寄給 頁確聯絡人)
                List<string> MailUsers = new List<string>();

                //頁確聯絡人(賣家一般聯絡人)
                Seller seller = sp.SellerGet(pro.SellerGuid);
                List<Seller.MultiContracts> contacts = new JsonSerializer().Deserialize<List<Seller.MultiContracts>>(seller.Contacts);
                if (contacts != null)
                {
                    //一般聯絡人
                    foreach (Seller.MultiContracts c in contacts)
                    {
                        if (c.Type == ((int)ProposalContact.Normal).ToString())
                        {
                            MailUsers.Add(c.ContactPersonEmail);
                        }
                    }
                }

                ViewEmployee emp = hmp.ViewEmployeeGet(ViewEmployee.Columns.UserId, pro.DevelopeSalesId);
                string SaleName = "";
                string SaleMail = "";
                if (emp.IsLoaded && emp != null)
                {
                    SaleName = emp.EmpName;
                    SaleMail = emp.Email;

                    string Memo = @"• 此信件為系統自動發出，如有任何問題，請直接洽詢負責業務，請勿直接回信<p />
                                • 請先以提案者帳號登入後，進行提案單覆核，以利後續製檔";

                    Seller s = sp.SellerGet(pro.SellerGuid);
                    string subject = string.Format("【請求覆核通知】 單號 NO.{0} {1} {2}", pro.Id, !string.IsNullOrEmpty(pcec.AppTitle) ? pcec.AppTitle : pro.BrandName + pro.DealName, s.SellerName);

                    ProposalFacade.SendEmail(MailUsers, subject,
                        ProposalFacade.SendSellerProposalMailContent(new Proposal
                        {
                            Id = pro.Id,
                            CreateId = pro.CreateId
                        }, 
                        SaleName, _config.SiteUrl + "/vbs/proposal/house/proposalcontent?pid=" + pid,
                        Memo));
                }
                #endregion mail(寄給 頁確聯絡人)

                flag = true;
            }
            return Json(new
            {
                IsSuccess = flag,
                Message = ""
            });
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetProposalDealSource(int pid)
        {
            return GetBaseProposalDealSource(pid);
        }
        [HttpPost]
        [Authorize]
        public ActionResult UploadProposalDealSource(int pid)
        {
            return UploadBaseProposalDealSource(pid);
        }
        [Authorize]
        public ActionResult DownloadProposalDealSource(int pid, int id)
        {
            return DownloadBaseProposalDealSource(pid, id);
        }
        [HttpPost]
        [Authorize]
        public ActionResult DeleteProposalDealSource(int pid, int id)
        {
            return DeleteBaseProposalDealSource(pid, id);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetProposalLogs(int pageStart, int pageLength, int pid)
        {
            return GetBaseProposalLogs(pageStart, pageLength, pid);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetProposalMessage(int pageStart, int pageLength, int pid)
        {
            return GetBaseProposalMessage(pageStart, pageLength, pid);
        }
        [HttpPost]
        [Authorize]
        public ActionResult ReadProposalMessage(int pid)
        {
            return ReadBaseProposalMessage(pid, false);
        }
        [HttpPost]
        [Authorize]
        public ActionResult LeaveMessage(int pid, string message)
        {
            string dept = string.Empty;
            ViewEmployee emp = hmp.ViewEmployeeGet(ViewEmployee.Columns.Email, UserName);
            if (emp.IsLoaded)
            {
                dept = emp.DeptName;
            }
            bool flag = LeaveMessageSet(pid, message, dept);
            return Json(flag);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetSellerProducts(Guid sid, Guid infoGuid, string productBrandName, string productName, string productCode,int warehouseType)
        {
            return GetBaseSellerProducts(sid, infoGuid, productBrandName, productName, productCode, warehouseType);
        }
        [HttpPost]
        [Authorize]
        public ActionResult GetProductItems(List<ProductItemSourceModel> items)
        {
            return GetBaseProductItem(items);
        }
        [HttpPost]
        [Authorize]
        public ActionResult UploadProductInfo(Guid sid)
        {
            return ImportProductInfoExcel(sid, UserName);
        }
        /// <summary>
        /// 取得宅配業務清單
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult GetEmployeeNameArray(string empName)
        {
            return GetBaseEmployeeNameArray(empName);
        }

        /// <summary>
        /// 取得宅配員工資料
        /// </summary>
        /// <param name="empName"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult GetEmployeeName(string empName)
        {
            return GetBaseEmployeeName(empName);
        }

        /// <summary>
        /// 取得業務
        /// </summary>
        /// <param name="userName"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult GetReferralSales(string userName)
        {
            List<AjaxResponseResult> res = new List<AjaxResponseResult>();
            if (!string.IsNullOrEmpty(userName) && userName.Length >= 2)
            {
                List<string> emps = SellerFacade.GetSalesmanNameArray(userName);
                foreach (string emp in emps)
                {
                    res.Add(new AjaxResponseResult
                    {
                        Value = emp,
                        Label = emp
                    });
                }
            }

            return Json(res, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// 結檔
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult DealClose(int pid)
        {
            StringBuilder builder = new StringBuilder();
            string errorMessage = string.Empty;
            bool isSuccess = true;
            try
            {
                DateTime dateStart = DateTime.Now;
                int min = dateStart.Minute;
                while (min % 5 != 0)
                {
                    dateStart = dateStart.AddMinutes(-1);
                    min = dateStart.Minute;
                }


                builder.AppendLine("[dateStart]" + dateStart);

                Proposal pro = sp.ProposalGet(pid);
                if (pro.BusinessHourGuid != null)
                {
                    ViewPponDealCollection deals = new ViewPponDealCollection();
                    ViewPponDeal theDeal = pp.ViewPponDealGetByBusinessHourGuid(pro.BusinessHourGuid.Value);
                    if (theDeal.IsLoaded)
                    {
                        GroupOrder go = op.GroupOrderGetByBid(theDeal.BusinessHourGuid);
                        //判斷未結檔
                        if (go.Slug == null)
                        {
                            //if (theDeal.BusinessHourOrderTimeE >= DateTime.Now)
                            //{
                            //將日期改到五分鐘內
                            BusinessHour bh = pp.BusinessHourGet(theDeal.BusinessHourGuid);
                            bh.BusinessHourOrderTimeE = dateStart;
                            pp.BusinessHourSet(bh);

                            builder.AppendLine("[BusinessHourSet]");

                            pro.OrderTimeE = dateStart;
                            sp.ProposalSet(pro);

                            builder.AppendLine("[ProposalSet]");

                            ProposalFacade.ProposalLog(pro.Id, "[手動結檔]" + dateStart, UserName);

                            deals.Add(theDeal);
                            //ViewComboDealCollection comboDeals = ViewPponDealManager.DefaultManager.GetViewComboDealCollectionByMainGuid(theDeal.BusinessHourGuid);
                            ViewComboDealCollection comboDeals = pp.GetViewComboDealAllByBid(theDeal.BusinessHourGuid);
                            foreach (ViewComboDeal comboDeal in comboDeals)
                            {
                                if (deals.Where(x => x.BusinessHourGuid == comboDeal.BusinessHourGuid).FirstOrDefault() == null)
                                {
                                    //判斷未結檔
                                    GroupOrder comboGo = op.GroupOrderGetByBid(comboDeal.BusinessHourGuid);
                                    if (comboGo.Slug == null)
                                    {
                                        ViewPponDeal vpd = pp.ViewPponDealGetByBusinessHourGuid(comboDeal.BusinessHourGuid);

                                        BusinessHour comboBh = pp.BusinessHourGet(comboDeal.BusinessHourGuid);
                                        comboBh.BusinessHourOrderTimeE = dateStart;
                                        pp.BusinessHourSet(comboBh);

                                        deals.Add(vpd);
                                    }
                                }
                            }
                            PponDealClose _dealClose = new PponDealClose();
                            IEnumerable<ViewPponDeal> dealIds = from x in deals.Distinct() select x;
                            builder.AppendLine("[deals]");

                            List<DealCloseResult> results = _dealClose.CloseDeal(dealIds, UserName);

                            if (results.Any())
                            {
                                foreach (var item in results)
                                {
                                    var deal = deals.FirstOrDefault(x => x.BusinessHourGuid == item.BusinessHourGuid);
                                    if (deal == null)
                                    {
                                        errorMessage = string.Format("檔號[{0}]資料錯誤結檔失敗，請聯絡系統管理員\\n", deal.UniqueId);
                                        isSuccess = false;
                                    }

                                    if (item.Result == DealCloseResultStatus.AlreadyClose)
                                    {
                                        errorMessage = string.Format("檔號[{0}]無須結檔\\n", deal.UniqueId);
                                        isSuccess = false;
                                    }
                                    else if (item.Result == DealCloseResultStatus.Success)
                                    {
                                        errorMessage = "";
                                        isSuccess = true;
                                    }
                                    else if (item.Result == DealCloseResultStatus.TimeError)
                                    {
                                        errorMessage = string.Format("檔號[{0}]結檔時間設定有誤，請聯絡系統管理員\\n", deal.UniqueId);
                                        isSuccess = false;
                                    }
                                    else if (item.Result == DealCloseResultStatus.Fail)
                                    {
                                        errorMessage = string.Format("檔號[{0}]結檔失敗，請聯絡系統管理員\\n", deal.UniqueId);
                                        isSuccess = false;
                                    }
                                    else
                                    {
                                        errorMessage = string.Format("檔號[{0}]系統錯誤，請聯絡系統管理員\\n", deal.UniqueId);
                                        isSuccess = false;
                                    }
                                }
                            }
                            else
                            {
                                errorMessage = string.Format("結檔失敗，請聯絡系統管理員\\n");
                                isSuccess = false;
                            }

                            if (isSuccess)
                            {
                                DealTimeSlotCollection dts = PponFacade.GetDealTimeSlotList(theDeal.BusinessHourGuid);
                                //母檔才需要改dealtimeslot
                                if (!Helper.IsFlagSet(bh.BusinessHourStatus, BusinessHourStatus.ComboDealSub))
                                {
                                    pp.DealTimeSlotDeleteAfterNewEndTime(bh.Guid, DateTime.Now);

                                    var newDealTimeSlotCollection = PponFacade.GetDealTimeSlotList(theDeal.BusinessHourGuid).Where(x => x.EffectiveEnd > DateTime.Now);
                                    foreach (var newDealTimeSlot in newDealTimeSlotCollection)
                                    {
                                        newDealTimeSlot.EffectiveEnd = DateTime.Now;
                                        pp.DealTimeSlotSet(newDealTimeSlot);
                                    }
                                }
                            }
                            builder.AppendLine("[results]" + "[" + isSuccess.ToString() + "]" + results.Count.ToString() + "筆" + new JsonSerializer().Serialize(results));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isSuccess = false;
                logger.Error(ex);
            }
            finally
            {
                ProposalFacade.ProposalPerformanceLogSet("DealClose", builder.ToString() + errorMessage, UserName);
            }

            return Json(new {
                IsSuccess = isSuccess,
                Message = errorMessage
            });
        }


        /// <summary>
        /// 業務轉件
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="salesType"></param>
        /// <param name="sales"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult ReferralSales(int pid, SellerSalesType salesType, string sales)
        {

            string message = ProposalFacade.ReferralSales(pid, salesType, sales, UserName);

            if (string.IsNullOrEmpty(message))
            {
                ViewEmployee emp = HumanFacade.ViewEmployeeGet(ViewEmployee.Columns.Email, sales);
                return Json(new { Success = true, Message = emp.EmpName + emp.Email, OpSalesId = emp.UserId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Message = message }, JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// 賣家移轉
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="sellerId"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public JsonResult ChangeSeller(int pid, string sellerId)
        {
            string message = ProposalFacade.ChangeSeller(ProposalSourceType.House, pid, sellerId, UserName);

            if (string.IsNullOrEmpty(message))
            {
                return Json(new { Success = true, Message = sellerId }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false, Message = message }, JsonRequestBehavior.AllowGet);
            }
        }




        /// <summary>
        /// 取得子分類
        /// </summary>
        /// <param name="parentId">母分類ID</param>
        [HttpPost]
        [Authorize]
        public JsonResult GetDealType2(int parentId)
        {
            var scs = PponFacade.DealType2GetList(parentId);
            return Json(new { Result = scs });
        }



        /// <summary>
        /// 取得最新合約內容表格
        /// </summary>
        /// <param name="pid">提案單單號</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult ProposalContractFileGet(int pid)
        {
            ProposalContractFileCollection pcfc = sp.ProposalContractFileGetListBypid(pid);
            return PartialView("_ProposalContractPartial", pcfc);
        }
        [HttpPost]
        [Authorize]
        public ActionResult CheckSpecStock(int pid)
        {
            string message = string.Empty;
            bool flag = CheckMultiOptionSpecStockForCreateProposal(pid, out message);
            return Json(flag);
        }

        /// <summary>
        /// 檢查庫存是否0庫存
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult CheckZeroStock(int pid)
        {
            //PChome倉儲的提案單，可以允許沒有庫存下，業務也能審核通過
            var proposal = sp.ProposalGet(pid);
            if (proposal.IsWms)
            {
                return Json(true);
            }

            bool flag = false;
            ProposalMultiDealCollection pmds = sp.ProposalMultiDealGetByPid(pid);
            foreach (ProposalMultiDeal pmd in pmds)
            {
                List<ProposalMultiDealsSpec> specs = new JsonSerializer().Deserialize<List<ProposalMultiDealsSpec>>(pmd.Options);

                foreach (ProposalMultiDealsSpec spec in specs)
                {
                    //    //ProposalMultiOptionSpecCollection specs = sp.ProposalMultiOptionSpecGetByMid(pmd.Id);
                    foreach (ProposalMultiDealsItem di in spec.Items)
                    {
                        ProductItem item = pp.ProductItemGet(di.item_guid);
                        ProductInfo product = pp.ProductInfoGet(item.InfoGuid);
                        string brandName = string.Empty;
                        if (product.IsLoaded)
                        {
                            brandName = product.ProductBrandName + product.ProductName;
                        }
                        if (item.IsLoaded)
                        {
                            //檢查庫存不能為0
                            if (item.Stock > 0)
                            {
                                //至少一項庫存>0，才可儲存
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                }
                if (flag)
                {
                    break;
                }
            }
            return Json(flag);
        }


        [HttpPost]
        [Authorize]
        public ActionResult UpdateWeeklyPayAccount(Guid gid)
        {
            bool flag = true;
            ProposalCollection pros = sp.ProposalGetListByHouse();
            foreach (Proposal pro in pros)
            {
                if(pro.BusinessHourGuid == null)
                {
                    continue;
                }
                List<Guid> bids = new List<Guid>();
                bids.Add(pro.BusinessHourGuid.Value);
                ComboDealCollection cdc = pp.GetComboDealByBid(pro.BusinessHourGuid.Value, false);
                foreach (ComboDeal cd in cdc)
                {
                    if (!bids.Contains(cd.BusinessHourGuid))
                    {
                        bids.Add(cd.BusinessHourGuid);
                    }
                }
                foreach (Guid bid in bids)
                {
                    BusinessHour bh = pp.BusinessHourGet(bid);
                    Item item = pp.ItemGetByBid(bid);
                    #region week_pay_account
                    var insStores = new PponStoreCollection();
                    insStores = pp.PponStoreGetListByBusinessHourGuid(bh.Guid);
                    var accounts = op.WeeklyPayAccountGetList(bh.Guid);
                    if (accounts.Count() > 0)
                    {
                        continue;
                    }
                    var stores = sp.SellerGetList(insStores.Where(t => Helper.IsFlagSet(t.VbsRight, VbsRightFlag.Accouting)).Select(x => x.StoreGuid));
                    foreach (var store in stores)
                    {
                        var account = accounts.FirstOrDefault(x => x.StoreGuid.HasValue && x.StoreGuid.Value == store.Guid)
                                    ?? new WeeklyPayAccount
                                    {
                                        BusinessHourGuid = bh.Guid,
                                        CreateDate = DateTime.Now,
                                        Creator = bh.CreateId
                                    };
                        account.StoreGuid = store.Guid;
                        account.EventName = (item.ItemName.Length > 100)
                                                ? item.ItemName.Substring(0, 100) + "..."
                                                : item.ItemName;
                        account.CompanyName = store.CompanyName;
                        account.SellerName = store.CompanyBossName;
                        account.AccountId = store.CompanyID;
                        account.AccountName = store.CompanyAccountName;
                        account.AccountNo = store.CompanyAccount;
                        account.Email = store.CompanyEmail;
                        account.BankNo = store.CompanyBankCode;
                        account.BranchNo = store.CompanyBranchCode;
                        account.Message = store.CompanyNotice;
                        account.SignCompanyID = store.SignCompanyID;

                        accounts.Add(account);
                    }
                    op.WeeklyPayAccountSetList(accounts);

                    #endregion week_pay_account
                }
            }
            return Json(flag);
        }

        [HttpPost]
        [Authorize]
        public ActionResult AddRelatedDeal(int pid, int related_pid)
        {
            Proposal ori_pro = sp.ProposalGet(pid);
            if(pid == related_pid)
            {
                return Json(new {
                    IsSuccess = false,
                    Message = "不可加入本身"
                });
            }
            if (ori_pro.IsLoaded)
            {
                Proposal rel_pro = sp.ProposalGet(related_pid);
                if (rel_pro.BusinessHourGuid != null)
                {
                    IViewPponDeal theDeal = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(rel_pro.BusinessHourGuid.Value);
                    if (Helper.IsFlagSet(theDeal.GroupOrderStatus.GetValueOrDefault(0), GroupOrderStatus.Completed) || theDeal.Slug != null)
                    {
                        //已結檔
                        return Json(new
                        {
                            IsSuccess = false,
                            Message = "不可新增已結檔之檔次"
                        });
                    }
                }
                ProposalRelatedDealCollection prds = sp.ProposalRelatedDealGetByPid(pid);
                if(prds.Count >= 5)
                {
                    return Json(new
                    {
                        IsSuccess = false,
                        Message = "至多新增五檔"
                    });
                }
                ProposalRelatedDeal deal = prds.Where(x => x.RelatedId == related_pid).FirstOrDefault();
                if (deal == null)
                {
                    deal = new ProposalRelatedDeal();
                }
                deal.ProposalId = ori_pro.Id;
                deal.RelatedId = related_pid;
                deal.CreateId = UserName;
                deal.CreateTime = DateTime.Now;

                sp.ProposalRelatedDealSet(deal);

                ProposalFacade.ProposalLog(ori_pro.Id, "[新增關聯檔次]" + related_pid, UserName);

                return Json(new
                {
                    IsSuccess = true,
                    Message = string.Empty
                });
            }
            return Json(new
            {
                IsSuccess = false,
                Message = "找不到提案單號"
            });
        }

        [HttpPost]
        [Authorize]
        public ActionResult DeleteRelatedDeal(int pid, int related_pid)
        {
            Proposal pro = sp.ProposalGet(pid);
            if (pro.IsLoaded)
            {
                ProposalRelatedDealCollection prds = sp.ProposalRelatedDealGetByPid(pid);
                ProposalRelatedDeal deal = prds.Where(x => x.RelatedId == related_pid).FirstOrDefault();
                if (deal != null)
                {
                    sp.ProposalRelatedDealDelete(deal);
                    ProposalFacade.ProposalLog(pro.Id, "[刪除關聯檔次]" + related_pid, UserName);
                }

                return Json(true);
            }
            return Json(new ProposalRelatedDeal());
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetRelatedDeal(int pid)
        {
            List<ProposalRelatedDealModel> list = ProposalFacade.GetRelatedDeal(pid);

            return Json(list);
        }

        [HttpPost]
        [Authorize]
        public ActionResult GetBrandId(int id)
        {
            Brand brand = pp.GetBrand(id);
            if (brand.IsLoaded)
            {
                return Json(new { IsSuccess = true, BrandName = brand.BrandName });
            }
            else
            {
                return Json(new { IsSuccess = false, BrandName = string.Empty });
            }
        }

        #region 進倉單管理
        [Authorize]
        [HttpPost]
        public ActionResult GetWmsPurchaseOrder(int pageStart, int pageLength, string sellerName, int? productNo, string specs, string pchomePurchaseOrderId, int? pid, string productBrandName,
                                                string productName, string productCode, DateTime? startTime, DateTime? endTime, int purchaseOrderStatus, bool? dontDisplayInvalidation, string pchomeProdId)
        {
            var vpiList = PponFacade.GetWmsProchaseOrder(null, pid, null, null, productBrandName, productName, productNo, specs, productCode
                , pchomePurchaseOrderId, purchaseOrderStatus, sellerName, startTime, endTime, 1, UserName, dontDisplayInvalidation, pchomeProdId);
            var result = PponFacade.GetWmsPurchaseOrder(vpiList, pageStart, pageLength);

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public ActionResult WmsPurchaseOrderVerify(string orderList)
        {
            try
            {
                List<Guid> orderIdList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

                wp.UpdtaeWmsPurchaseOrderStatusWithInvalidationStatus(orderIdList, (int)PurchaseOrderStatus.Verify, UserName, false);
                WmsFacade.SaveWmsPurchaseOrderLog(orderIdList, (int)PurchaseOrderStatus.Verify, UserName);

                ProposalFacade.SendWmsPurchaseOrderMail(orderIdList, false, true);

                return Json(new { IsSuccess = true, Message = "處理成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult WmsPurchaseOrderFail(string orderList)
        {
            try
            {
                List<Guid> orderIdList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

                wp.UpdtaeWmsPurchaseOrderStatusWithInvalidationStatus(orderIdList, (int)PurchaseOrderStatus.Fail, UserName, true);
                WmsFacade.SaveWmsPurchaseOrderLog(orderIdList, (int)PurchaseOrderStatus.Fail, UserName);

                ProposalFacade.SendWmsPurchaseOrderMail(orderIdList, false, false);

                return Json(new { IsSuccess = true, Message = "處理成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        [Authorize]
        public ActionResult WmsManage()
        {
            // 特殊標記
            Dictionary<int, string> dcStatus = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(PurchaseOrderStatus)))
            {
                if ((PurchaseOrderStatus)item != PurchaseOrderStatus.Initial)
                    dcStatus[(int)item] = Helper.GetDescription((PurchaseOrderStatus)item);
            }

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Read))
            {
                TempData["error"] = "無查看進倉單管理權限。";
            }


            ViewBag.SiteUrl = _config.SiteUrl;
            ViewBag.Edit = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update);
            ViewBag.Status = dcStatus;
            return View();
        } 

        [Authorize]
        [HttpPost]
        public ActionResult ChangeWmsPurchaseOrderInvalidationStatus(string purchaseOrderGuid)
        {
            try
            {
                bool IsSuccess = PponFacade.ChangeWmsPurchaseOrderInvalidationStatus(purchaseOrderGuid, UserName);

                return Json(new
                {
                    IsSuccess = IsSuccess,
                    Message = "處理成功",
                });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        #endregion

        #region 還貨單管理
        [Authorize]
        [HttpPost]
        public ActionResult GetWmsReturnOrder(int pageStart, int pageLength, string sellerName, int? productNo, string specs, string pchomeProId, string productBrandName,
                                                string productName, string productCode, DateTime? startTime, DateTime? endTime, int? returnOrderStatus,int? sourceStatus, bool dontDisplayInvalidation)
        {
            //取得還貨單資料
            var vpiList = PponFacade.GetWmsReturnOrder(productBrandName, productName, productNo, specs, productCode
                , pchomeProId, returnOrderStatus,sourceStatus, sellerName, startTime, endTime, (int)WareHouseType.Pchome24, UserName, dontDisplayInvalidation);
            //分頁處理
            var result = PponFacade.GetWmsReturnOrder(vpiList, pageStart, pageLength);

            return Json(result);
        }

        [Authorize]
        [HttpPost]
        public ActionResult WmsReturnOrderVerify(string orderList)
        {
            try
            {
                
                List<Guid> orderIdList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

                wp.UpdtaeWmsReturnOrderStatus(orderIdList, (int)WmsReturnOrderStatus.Verify, UserName);
                WmsFacade.SaveWmsReturnOrderLog(orderIdList, (int)WmsReturnOrderStatus.Verify, UserName);
                ProposalFacade.SendWmsReturnOrderMail(orderIdList, false, true);
                
                return Json(new { IsSuccess = true, Message = "處理成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }

        [Authorize]
        [HttpPost]
        public ActionResult WmsReturnOrderFail(string orderList)
        {
            try
            {
                List<Guid> orderIdList = new JsonSerializer().Deserialize<List<Guid>>(orderList);

                wp.UpdtaeWmsReturnOrderStatus(orderIdList, (int)WmsReturnOrderStatus.Fail, UserName);
                WmsFacade.SaveWmsReturnOrderLog(orderIdList, (int)WmsReturnOrderStatus.Fail, UserName);

                ProposalFacade.SendWmsReturnOrderMail(orderIdList, false, false);

                return Json(new { IsSuccess = true, Message = "處理成功" });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }


        [Authorize]
        public ActionResult WmsReturn()
        {
            // 還貨狀態
            Dictionary<int, string> dcStatus = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(WmsReturnOrderStatus)))
            {
                if ((WmsReturnOrderStatus)item != WmsReturnOrderStatus.Initial)
                    dcStatus[(int)item] = Helper.GetDescription((WmsReturnOrderStatus)item);
            }

            // 來源
            Dictionary<int, string> dcSource = new Dictionary<int, string>();
            foreach (var item in Enum.GetValues(typeof(WmsReturnSource)))
            {
                    dcSource[(int)item] = Helper.GetDescription((WmsReturnSource)item);
            }

            if (!CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Read))
            {
                TempData["error"] = "無查看還貨單管理權限。";
            }


            ViewBag.SiteUrl = _config.SiteUrl;
            ViewBag.Edit = CommonFacade.IsInSystemFunctionPrivilege(UserName, SystemFunctionType.Update);
            ViewBag.Status = dcStatus;
            ViewBag.Source = dcSource;
            return View();
        }

        [Authorize]
        [HttpPost]
        public ActionResult ChangeWmsReturnOrderInvalidationStatus(string returnOrderGuid)
        {
            try
            {
                bool IsSuccess = PponFacade.ChangeWmsReturnOrderInvalidationStatus(returnOrderGuid,UserName);

                return Json(new
                {
                    IsSuccess = IsSuccess,
                    Message = "處理成功",
                });
            }
            catch (Exception ex)
            {
                return Json(new { IsSuccess = false, Message = ex.Message });
            }
        }
        #endregion

        #endregion web method
    }


}

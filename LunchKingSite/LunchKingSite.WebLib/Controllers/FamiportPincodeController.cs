﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using LunchKingSite.BizLogic.Model.FamiPort;
using LunchKingSite.DataOrm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.BizLogic.Facade;
using NPOI;
using NPOI.HPSF;
using NPOI.HSSF;
using NPOI.HSSF.Record.Formula.Functions;
using NPOI.HSSF.UserModel;
using NPOI.POIFS;
using NPOI.Util;
using NPOI.SS.UserModel;
using System.Net.Mail;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using Famiport = LunchKingSite.DataOrm.Famiport;
using Peztemp = LunchKingSite.DataOrm.Peztemp;
using FamilyNetEvent = LunchKingSite.Core.Models.Entities.FamilyNetEvent;
using Row = NPOI.SS.UserModel.Row;
using LunchKingSite.Core.Models;
using LunchKingSite.BizLogic.Model;
using Newtonsoft.Json;
using JsonSerializer = LunchKingSite.Core.JsonSerializer;

namespace LunchKingSite.WebLib.Controllers
{
    public class FamiportPincodeController : PponControllerBase
    {
        private static IPponProvider _pp;
        private static ISysConfProvider _config;
        private static IMemberProvider _mp;
        private static IFamiportProvider _fami;        

        static FamiportPincodeController() 
        {
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
            _config = ProviderFactory.Instance().GetConfig();
            _mp = ProviderFactory.Instance().GetProvider<IMemberProvider>();
            _fami = ProviderFactory.Instance().GetProvider<IFamiportProvider>();            
        }

        [Authorize(Roles = "FamiportPincode, Production, Administrator, ME2O")]
        public ActionResult Apply(int? famiportId, string fromAction)
        {
            if (string.IsNullOrEmpty(fromAction))
            {
                ViewData["FromAction"] = (User.IsInRole("Production")) ? "List" : "FamiList" ;
            }
            else
            {
                ViewData["FromAction"] = fromAction;
            }

            Famiport famiport = new Famiport();
            if (famiportId != null)
            {
                famiport = _pp.FamiportGet((int)famiportId);
            }
            else
            {
                famiport.TestCodeQty = 3;
                famiport.EventAttention1 = "請於兌換期間完成兌換優惠購買，逾期兌換無效且不得以任何理由要求補發本卷。";
                famiport.EventAttention2 = "本卷不得要求找零、折抵現金或優惠購買非指定商品，此活動恕不得與其它優惠折扣併用。";
                famiport.EventAttention3 = "結帳時請出示本卷，且完成兌換優惠購買後店舖人員將會收回此卷，並不得重複使用。";
                famiport.EventAttention4 = "全家便利商店與17Life保留本活動修改及更換指定商品之權利，且不另行通知。";
                famiport.ItemAttention1 = "本活動相關事宜或對本活動若有任何疑問，請洽全家便利商店店舖人員。";
                famiport.ItemAttention2 = "活動疑問可撥17Life客服，" + _config.ServiceTel;
                famiport.ItemAttention3 = "全台全家便利商店皆可兌換，商品數量以店舖實際陳列為主";
                famiport.ItemAttention4 = "此商品規格32G，原價15元，買一送一";
                famiport.Type =(int)FamilyDealType.General;
                famiport.DisplayType =(int)FamilyDisplayType.Day;
                famiport.IsLock =false;
            }
            SetPageDefault();
            return View(famiport);
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult SaveDraftThenPreview(FormCollection form) 
        {
            Famiport famiport = new Famiport();

            if (int.Parse(form["famiportId"].ToString()) != 0) 
            {
                famiport = _pp.FamiportGet(int.Parse(form["famiportId"].ToString()));
            }

            famiport.VendorId = Convert.ToString((int)PeztempServiceCode.Famiport).PadLeft(3, '0');
            famiport.ItemName = form["itemName"].ToString();
            famiport.ItemCode = form["itemCode"].ToString();
            famiport.Type = int.Parse(form["rdoFamiType"].ToString());
            if (famiport.Type == 0)
            {
                //全家專區檔設定
                famiport.DisplayType = 0;
                famiport.IsLock = false;
            }
            else
            {
                //Beacon檔設定
                try
                {
                    famiport.DisplayType = int.Parse(form["rdoPeriod"].ToString());
                    famiport.IsLock = (form["rdoLockStatus"].ToString()) == "1";
                }
                catch 
                {
                    famiport.DisplayType = 0;
                    famiport.IsLock = false;

                }
            }

            famiport.PincodeQty = int.Parse(form["pincodeQty"].ToString());
            famiport.TestCodeQty = int.Parse(form["testcodeQty"]);
            famiport.BusinessHourDeliverTimeS = DateTime.Parse(form["deliverTimeS"].ToString());
            famiport.BusinessHourDeliverTimeE = DateTime.Parse(form["deliverTimeE"].ToString());
            famiport.ItemOrigPrice = decimal.Parse(form["itemOrigPrice"].ToString());
            famiport.ItemPrice = decimal.Parse(form["itemPrice"].ToString());
            famiport.Remark = form["eventDescription"].ToString();
            famiport.EventAttention1 = form["eventAttention1"].ToString();
            famiport.EventAttention2 = form["eventAttention2"].ToString();
            famiport.EventAttention3 = form["eventAttention3"].ToString();
            famiport.EventAttention4 = form["eventAttention4"].ToString();
            famiport.BusinessHourOrderTimeS = DateTime.Parse(form["orderTimeS"].ToString());
            famiport.BusinessHourOrderTimeE = DateTime.Parse(form["orderTimeE"].ToString());
            famiport.ItemRemark = form["itemDescription"].ToString();
            famiport.ItemAttention1 = form["itemAttention1"].ToString();
            famiport.ItemAttention2 = form["itemAttention2"].ToString();
            famiport.ItemAttention3 = form["itemAttention3"].ToString();
            famiport.ItemAttention4 = form["itemAttention4"].ToString();

            famiport.IsOutOfDate = false;

            if (famiport.Id != 0)
            {
                _pp.FamiportUpdate(famiport);
            }
            else
            {
                famiport.CreateId = form["userName"].ToString();
                famiport.CreateTime = System.DateTime.Now;
                _pp.FamiportSet(famiport);
                _pp.FamiportUpdateEventId(famiport.Id, PeztempServiceCode.Famiport);
            }

            ViewData["FromAction"] = "FamiList";
            return View("Preview", famiport);
        }

        [HttpGet]
        [Authorize(Roles = "FamiportPincode, Production, Administrator, ME2O")]
        public ActionResult Preview(int famiportId, string fromAction) 
        {
            ViewData["FromAction"] = fromAction;
            SetPageDefault();
            Famiport famiport = _pp.FamiportGet(famiportId);
            return View(famiport);
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult SendApply(FormCollection form)
        {
            Famiport famiport = _pp.FamiportGet(int.Parse(form["famiportId"].ToString()));
            bool isAccept = bool.Parse(form["isAccept"].ToString());

            if (isAccept)
            {
                famiport.Status = (int)FamiportStatus.OnTheMake;
                famiport.BusinessHourDeliverTimeE = (famiport.BusinessHourDeliverTimeE.HasValue)? famiport.BusinessHourDeliverTimeE.Value.AddDays(1): famiport.BusinessHourDeliverTimeE;
                famiport.BusinessHourOrderTimeE = (famiport.BusinessHourOrderTimeE.HasValue) ? famiport.BusinessHourOrderTimeE.Value.AddDays(1) : famiport.BusinessHourOrderTimeE;
                _pp.FamiportSet(famiport);

                if (!string.IsNullOrEmpty(_config.FamiDealBuildEmail)) 
                {
                    MailMessage msg = new MailMessage();
                    foreach (string email in _config.FamiDealBuildEmail.Split(';'))
                    {
                        msg.To.Add(email);
                    }
                    msg.Subject = string.Format("全家專區檔次申請通知【{0}】", famiport.ItemName);
                    msg.From = new MailAddress(_config.AdminEmail);
                    msg.IsBodyHtml = true;
                    msg.Body = string.Format(@"您好，全家已於【{0}】申請【{1}】製檔， 
                                煩請前往<a href='{2}' target='fami_window'>17Life紅利pin碼系統</a>了解處理，謝謝。"
                                , System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")
                                , famiport.ItemName
                                , string.Format("{0}/FamiportPincode/List", _config.SiteUrl));
                    PostMan.Instance().Send(msg, SendPriorityType.Immediate);
                }

                return RedirectToAction("FamiList", new { page = 1, order = Famiport.Columns.BusinessHourOrderTimeS, isOrderASC = false });
            }
            else 
            {
                ViewData["FromAction"] = "FamiList";
                return View("Apply", famiport);   
            }
        }

        [Authorize(Roles = "Production, Administrator, ME2O")]
        public ActionResult List(int? page, string order, bool? isOrderASC) 
        {
            SetPageDefault();
            FamiportCollection famiportCol = famiportColPager(page, order, isOrderASC, false, false);
            return View(famiportCol);
        }

        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult FamiList(int? page, string order, bool? isOrderASC) 
        {
            SetPageDefault();
            FamiportCollection famiportCol = famiportColPager(page, order, isOrderASC, true, false);
            return View(famiportCol);
        }

        [Authorize(Roles = "FamiportPincode, FamiportCustomer, Production, Administrator, ME2O")]
        public ActionResult EventList(int? page, string order, bool? isOrderASC) 
        {
            SetPageDefault();
            FamiportCollection famiportCol = famiportColPager(page, order, isOrderASC, true, true);
            return View(famiportCol);
        }

        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult FamilyNetEventList(int? page, string filter)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            FamilyNetEventListModel model = new FamilyNetEventListModel();
            SetPageDefault();

            var list = _fami.GetFamilyNetEventMainList();

            int pageCount = 10;
            int lastPage = list.Count > 0 ? list.Count / pageCount + (list.Count % pageCount > 0 ? 1 : 0) : 1;
            page = page ?? 1;
            page = page <= lastPage ? page : 1;
            int skip = page > 1 ? (page.Value - 1) * pageCount : 0;
            filter = filter ?? string.Empty;
            int flag = 0;
            bool isOrderbyDesc = int.TryParse(filter, out flag) && flag > 0 ? Helper.IsFlagSet(flag, PageFilterField.Desc) : true;
            filter = flag > 0 ? Helper.GetEnumDescription(isOrderbyDesc ? (PageFilterField)flag - 1 : (PageFilterField)flag) : string.Empty;
            //filter = FamilyNetEvent..Schema.Columns.Any(x => x.PropertyName == filter) ? filter : ViewExternalDeal.Schema.GetColumn(ViewExternalDeal.Columns.ModifyTime).PropertyName;
            var prop = typeof(FamilyNetEvent).GetProperty(filter);
            if (prop != null)
            {
                list = isOrderbyDesc ? list.OrderByDescending(x => prop.GetValue(x, null)).Skip(skip).Take(pageCount).ToList(): list.OrderBy(x => prop.GetValue(x, null)).Skip(skip).Take(pageCount).ToList();
            }
            else
            {
                list = isOrderbyDesc ? list.Skip(skip).Take(pageCount).ToList() : list.Skip(skip).Take(pageCount).ToList();
            }
            model.List = list;
            model.Page = page.Value;
            model.LastPage = lastPage;
            model.DealCount = list.Count;
            model.Filter = flag;
            model.IsME2O = User.IsInRole("ME2O");
            model.IsProduction = User.IsInRole("Production");
            model.IsAdministrator= User.IsInRole("Administrator");

            ViewBag.SiteUrl = _config.SiteUrl;

            return View(model);
        }

        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult FamilyNetEventDealInfo(int? id, string page,string filter)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault();
            FamilyNetEventDealModel deal = new FamilyNetEventDealModel();

            bool isNew = id ==null;
            if (!isNew)
            {                
                System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
                FamilyNetEvent ed = _fami.GetFamilyNetEventById(id);
                int mainId = ed.ComboDealMainId;                
                deal.Id = ed.Id;
                deal.ActCode = ed.ActCode;
                deal.ItemCode = ed.ItemCode;
                deal.ItemName = ed.ItemName;
                deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
                deal.ItemPrice = Convert.ToInt32(ed.ItemPrice);
                deal.PurchasePrice = Convert.ToInt32(ed.PurchasePrice);
                deal.MaxItemCount = int.Parse(ed.MaxItemCount.ToString());
                deal.Remark = ed.Remark;
                deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy h:mm tt", culture);
                deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy h:mm tt", culture);
                deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS.ToString("ddd MMM dd yyyy h:mm tt", culture);
                deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE.ToString("ddd MMM dd yyyy h:mm tt", culture);
                deal.Status = ed.Status;
                deal.ComboDealMainId = ed.ComboDealMainId;
                deal.IsComboDeal = FamilyNetEventIsComboDeal.Single;
                deal.Version = (FamilyBarcodeVersion)ed.Version;

                deal.Buy = ed.Buy;
                deal.Free = ed.Free;
                if (ed.IsComboDeal)
                {
                    deal.IsComboDeal = FamilyNetEventIsComboDeal.Multi;
                    List<FamilyNetEvent> comboDealList = _fami.GetFamilyNetEventDealSonListByMainId(mainId);
                    if (comboDealList.Any())
                    {
                        deal.FamilyNetEventComboDealList = comboDealList;
                    }
                }
            }
            else
            {
                deal.MainBusinessHourGuid = string.Empty;
                deal.BusinessHourGuid = string.Empty;
            }
            deal.Page = page;
            deal.Filter = filter;
            deal.CreateId = userId;
            deal.IsAllowEdit = deal.Status == (int)FamilyNetEventStatus.Draft;
            deal.IsAfterSaveAllowEdit = deal.Status < (int)FamilyNetEventStatus.Confirmed || (deal.Status==(int)FamilyNetEventStatus.Confirmed && Convert.ToDateTime(deal.BusinessHourOrderTimeE) < DateTime.Now) || deal.Status == (int)FamilyNetEventStatus.Shelves;

            ViewBag.EnableFamilyBarcodeVersion = _config.EnableFamilyBarcodeVersion;

            return View(deal);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult SaveTempData(FamilyNetEventDealModel model)
        {
            FamilyNetEvent deal = _fami.GetFamilyNetEventById(model.Id);
            
            model.IsAllowEdit = deal!=null ? deal.Status == (int)FamilyNetEventStatus.Draft : true;

            if (deal.IsComboDeal)
            {
                List<FamilyNetEvent> comboDealList = _fami.GetFamilyNetEventDealSonListByMainId(deal.ComboDealMainId);
                if (comboDealList.Any())
                {
                    model.FamilyNetEventComboDealList = comboDealList;                    
                }
            }

            TempData["entity"] = model;
            return Json(new { Result = true, Message = "" }, JsonRequestBehavior.AllowGet);
        }

        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult FamilyNetEventDealPreview(string id)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            SetPageDefault();
            FamilyNetEventDealModel deal = new FamilyNetEventDealModel();

            if (TempData["entity"] != null)
            {
                deal = (FamilyNetEventDealModel)TempData["entity"];
                FamilyNetEvent existDeal = _fami.GetFamilyNetEventById(deal.Id);
                deal.Status = existDeal != null ? existDeal.Status : (int)FamilyNetEventStatus.Draft;
                deal.CreateId = userId;
                deal.ItemOrigPrice = Convert.ToInt32(existDeal.ItemOrigPrice);
                deal.ItemPrice = Convert.ToInt32(existDeal.ItemPrice);
                deal.PurchasePrice = Convert.ToInt32(existDeal.PurchasePrice);
                return View(deal);
            }

            int mainId = string.IsNullOrEmpty(id) ? 0 : int.Parse(id);

            System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");

            FamilyNetEvent ed = _fami.GetFamilyNetEventById(mainId);

            if (ed == null)
                return RedirectToAction("FamilyNetEventList");

            deal.Id = ed.Id;
            deal.ActCode = ed.ActCode;
            deal.ItemCode = ed.ItemCode;
            deal.ItemName = ed.ItemName;
            deal.ItemOrigPrice = Convert.ToInt32(ed.ItemOrigPrice);
            deal.ItemPrice = Convert.ToInt32(ed.ItemPrice);
            deal.PurchasePrice = Convert.ToInt32(ed.PurchasePrice);
            deal.MaxItemCount = int.Parse(ed.MaxItemCount.ToString());
            deal.Remark = ed.Remark;
            deal.BusinessHourOrderTimeS = ed.BusinessHourOrderTimeS.ToString("ddd MMM dd yyyy h:mm tt", culture);
            deal.BusinessHourOrderTimeE = ed.BusinessHourOrderTimeE.ToString("ddd MMM dd yyyy h:mm tt", culture);
            deal.BusinessHourDeliverTimeS = ed.BusinessHourDeliverTimeS.ToString("ddd MMM dd yyyy h:mm tt", culture);
            deal.BusinessHourDeliverTimeE = ed.BusinessHourDeliverTimeE.ToString("ddd MMM dd yyyy h:mm tt", culture);
            deal.Status = ed.Status;
            deal.ComboDealMainId = ed.ComboDealMainId;
            deal.Buy = ed.Buy;
            deal.Free = ed.Free;
            deal.Version = (FamilyBarcodeVersion)ed.Version;
                
            if (ed.IsComboDeal)
            {
                List<FamilyNetEvent> comboDealList = _fami.GetFamilyNetEventDealSonListByMainId(deal.Id);
                if (comboDealList.Any())
                {
                    deal.FamilyNetEventComboDealList = comboDealList;
                    deal.ComboDealJsonList = JsonConvert.SerializeObject(comboDealList);
                    deal.IsComboDeal = FamilyNetEventIsComboDeal.Multi;
                }
            }

            deal.IsAllowEdit = deal.Status == (int)FamilyNetEventStatus.Draft;
            ViewBag.EnableFamilyBarcodeVersion = _config.EnableFamilyBarcodeVersion;
            return View(deal);
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult FamilyNetEventSaveDraft(FamilyNetEventDealModel model)
        {
            var userId = MemberFacade.GetUniqueId(User.Identity.Name);
            int mainId = model.Id==0 ? 0 : model.Id;
            bool isNew=false;
            var deal = _fami.GetFamilyNetEventById(mainId);
            if (deal == null)
            {
                isNew = true;
            }            
            bool flag = false;
            DateTime now = DateTime.Now;

            deal = isNew ? new FamilyNetEvent() : deal;

            if (model.Status != (int)FamilyNetEventStatus.Confirmed) //除已確認不可更新
            {
                deal.ActCode = model.ActCode;
                deal.ItemCode = model.ItemCode;
                deal.ItemName = model.ItemName.Trim();
                deal.ItemOrigPrice = model.ItemOrigPrice;
                deal.ItemPrice = model.ItemPrice;
                deal.PurchasePrice = model.PurchasePrice;
                deal.MaxItemCount = model.MaxItemCount;
                deal.Remark = model.Remark;
                deal.Buy = model.Buy;
                deal.Free = model.Free;
                deal.BusinessHourOrderTimeS = ConvertDateAndTime(model.BusinessHourOrderTimeS).Value;
                deal.BusinessHourOrderTimeE = ConvertDateAndTime(model.BusinessHourOrderTimeE).Value;
                deal.BusinessHourDeliverTimeS = ConvertDateAndTime(model.BusinessHourDeliverTimeS).Value;
                deal.BusinessHourDeliverTimeE = ConvertDateAndTime(model.BusinessHourDeliverTimeE).Value;
                deal.Version = (int)FamilyBarcodeVersion.FamiSingleBarcode;
                if (model.IsComboDeal==FamilyNetEventIsComboDeal.Multi)
                {
                    deal.IsComboDeal = true;
                    deal.ActCode = string.Empty;
                    deal.ItemCode = string.Empty;
                    //deal.MaxItemCount = 0;
                    deal.Buy = 1;
                    deal.Free = 0;
                }

                deal.Version = (short)model.Version;

                if (!isNew)
                {
                    deal.ModifyTime = now;
                }
                else
                {
                    deal.Status = (int)FamilyNetEventStatus.Draft;
                    deal.CreateId = userId;
                    deal.CreateTime = now;
                    deal.ModifyTime = now;
                    deal = _fami.AddFamilyNetEvent(deal);
                    deal.ComboDealMainId = deal.Id;
                }
                _fami.UpdateFamilyNetEvent(deal);                

                if (model.IsComboDeal == FamilyNetEventIsComboDeal.Multi && !string.IsNullOrEmpty(model.ComboDealJsonList))//子檔
                {
                    var ComboDealCol = Newtonsoft.Json.JsonConvert.DeserializeObject<List<FamilyNetEvent>>(model.ComboDealJsonList);
                    List<FamilyNetEvent> removeItems = new List<FamilyNetEvent>();
                    if (mainId != 0)
                    {
                        removeItems = _fami.GetFamilyNetEventDealSonListByMainId(mainId);
                    }
                    List<FamilyNetEvent> newItems = new List<FamilyNetEvent>();

                    foreach (var item in ComboDealCol)
                    {
                        if (removeItems.Any(x => x.ActCode == item.ActCode && x.ItemCode == item.ItemCode && x.ItemName == item.ItemName && x.ItemOrigPrice == item.ItemOrigPrice && x.ItemPrice == item.ItemPrice && x.PurchasePrice == item.PurchasePrice && x.MaxItemCount == item.MaxItemCount && x.Buy == item.Buy && x.Free == item.Free))
                        {
                            newItems.Add(removeItems.FirstOrDefault(x => x.ActCode == item.ActCode && x.ItemCode == item.ItemCode && x.ItemName == item.ItemName && x.ItemOrigPrice == item.ItemOrigPrice && x.ItemPrice == item.ItemPrice && x.PurchasePrice == item.PurchasePrice && x.MaxItemCount == item.MaxItemCount && x.Buy == item.Buy && x.Free == item.Free));
                            removeItems.Remove(removeItems.FirstOrDefault(x => x.ActCode == item.ActCode && x.ItemCode == item.ItemCode && x.ItemName == item.ItemName && x.ItemOrigPrice == item.ItemOrigPrice && x.ItemPrice == item.ItemPrice && x.PurchasePrice == item.PurchasePrice && x.MaxItemCount == item.MaxItemCount && x.Buy == item.Buy && x.Free == item.Free));
                        }
                        else
                        {
                            newItems.Add(new FamilyNetEvent
                            {
                                BusinessHourGuid = item.BusinessHourGuid,
                                ActCode = item.ActCode,
                                ItemCode = item.ItemCode,
                                ItemName = item.ItemName,
                                ItemOrigPrice = item.ItemOrigPrice,
                                ItemPrice = item.ItemPrice,
                                PurchasePrice = item.PurchasePrice,
                                MaxItemCount = item.MaxItemCount,
                                ModifyTime = now,
                                Buy = item.Buy,
                                Free = item.Free,
                                Status = (int)FamilyNetEventStatus.Draft,
                                CreateId = userId,
                                CreateTime = now,
                                ComboDealMainId = deal.Id,
                                IsComboDeal = true,
                            });
                        }
                    }
                    foreach(var item in newItems)
                    {
                        item.Remark = model.Remark;
                        item.BusinessHourOrderTimeS = ConvertDateAndTime(model.BusinessHourOrderTimeS).Value;
                        item.BusinessHourOrderTimeE = ConvertDateAndTime(model.BusinessHourOrderTimeE).Value;
                        item.BusinessHourDeliverTimeS = ConvertDateAndTime(model.BusinessHourDeliverTimeS).Value;
                        item.BusinessHourDeliverTimeE = ConvertDateAndTime(model.BusinessHourDeliverTimeE).Value;
                        item.Version = (short)model.Version;
                    }

                    _fami.DeleteFamilyNetEventComboDealByItems(removeItems);
                    _fami.SetFamilyNetEventComboDealList(newItems);
                    
                }
                #region 更新時間同步至17後台要有bid才可進入
                if(deal.Status >= (int)FamilyNetEventStatus.OnTheMake && deal.Status!=(int)FamilyNetEventStatus.Confirmed)
                {
                    List<FamilyNetEvent> deals = _fami.GetFamilyNetEventDealAllListByMainId(mainId);
                    //刪除DealTimeSlot及更新BusinessHour
                    DateTime updateDay = DateTime.Today.AddDays(-1);
                    updateDay = updateDay.AddHours(12);
                    FamilyNetFacade.RemoveFamilyDealsTime(deals, updateDay,true);
                    //新增DealTimeSlot及更新usinessHour
                    FamilyNetFacade.AddFamilyDealsTime(deals, deal);
                }                
                #endregion
            }

            flag = true;
            string back = string.Format("?filter=0&page={0}", model.Page);
            return Json(new { Result = flag, Message = "", BackUrl = back });
        }

        /// <summary>
        /// 全家寄杯檔預覽後建檔
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult GoNext(string id)
        {
            string resultMsg = "";
            string username = User.Identity.Name;
            Guid mainBid = Guid.Empty;

            int mainId;
            if (!int.TryParse(id, out mainId) && TempData["entity"] == null)
            {
                resultMsg = "傳入參數有誤";
                return Json(new { Result = false, Message = resultMsg });
            }
            else if(!int.TryParse(id, out mainId))
            {
                FamilyNetEventDealModel model = (FamilyNetEventDealModel)TempData["entity"];
                mainId = model.ComboDealMainId;
            }
            else
            {
                mainId = int.Parse(id);
            }

            List<FamilyNetEvent> deals = _fami.GetFamilyNetEventDealAllListByMainId(mainId);
            bool flag = deals.Count > 0 || TempData["entity"] != null;
            if (flag)
            {
                FamilyNetEvent deal = _fami.GetFamilyNetEventById(mainId);                
                Dictionary<int, Guid> bids = new Dictionary<int, Guid>();
                DateTime now = DateTime.Now;
                int status = (int)FamilyNetEventStatus.Draft;
                status = deal != null ? deal.Status : status;

                //檢查權限、檢查是否進入下一關
                switch (status)
                {
                    case (int)FamilyNetEventStatus.Draft:
                        if (TempData["entity"] != null)
                        {
                            FamilyNetEventDealModel model = (FamilyNetEventDealModel)TempData["entity"];
                            FamilyNetEventSaveDraft(model);                                                        
                            FamilyNetFacade.DealApplyNotify(deal);
                            //建檔
                            resultMsg = FamilyNetFacade.OnSaveBusinessHour(deals,  out bids);
                            if (bids.Count > 0)
                            {
                                //強制隱藏檔次
                                foreach (var item in deals)
                                {
                                    var dtCol = _pp.DealTimeSlotGetAllCol(item.BusinessHourGuid);
                                    foreach (var dt in dtCol)
                                    {
                                        dt.Status = (int)DealTimeSlotStatus.NotShowInPponDefault;
                                        _pp.DealTimeSlotSet(dt);
                                    }
                                }
                                status += 1;                                                               
                            }
                            else
                            {
                                resultMsg = "檔次建置未成功";
                                flag = false;
                            }                            
                        }
                        else
                        {
                            resultMsg = "送出失敗";
                            flag = false;
                        }
                        break;
                    case (int)FamilyNetEventStatus.OnTheMake:
                        status += 1;
                        break;
                    case (int)FamilyNetEventStatus.WaitingConfirm:
                        //開放隱藏檔次
                        foreach(var item in deals)
                        {
                            var dtCol=_pp.DealTimeSlotGetAllCol(item.BusinessHourGuid);
                            foreach(var dt in dtCol)
                            {
                                dt.Status = (int)DealTimeSlotStatus.Default;
                                _pp.DealTimeSlotSet(dt);
                            }                            
                        }
                        status += 1;
                        break;                    
                    case (int)FamilyNetEventStatus.Confirmed:
                        status += 1;
                        break;
                }

                foreach (var bid in bids)
                {
                    //母檔
                    deals.Where(x => x.Id == bid.Key && x.ComboDealMainId==bid.Key).ForEach(
                            rs =>
                            {
                                mainBid = bid.Value;
                                deal.MainBusinessHourGuid = bid.Value;
                                deal.BusinessHourGuid = bid.Value;
                                deal.ModifyTime = now;
                            });

                    //子檔
                    deals.Where(x => x.Id == bid.Key && x.ComboDealMainId != bid.Key).ForEach(
                            item =>
                            {
                                item.MainBusinessHourGuid = mainBid;
                                item.BusinessHourGuid = bid.Value;
                                item.ModifyTime = now;
                            });
                    //連結子母檔 
                    PponDeal mainDeal = _pp.PponDealGet(mainBid);                    
                    foreach (var item in deals.Where(x => x.Id == bid.Key && x.ComboDealMainId != bid.Key))
                    {
                        PponFacade.AddComboDeal(mainDeal, new ComboItem
                        {
                            Bid = bid.Value,
                            Title = item.ItemName
                        }, username);
                    }
                }

                deal.Status = status;
                deal.ModifyTime = now;
                _fami.UpdateFamilyNetEvent(deal);
                if (deal.IsComboDeal)//更新子檔
                {
                    _fami.SetFamilyNetEventComboDealList(deals.Where(x => x.Id != x.ComboDealMainId).ToList());
                }
            }
            string back = string.Format("?filter=0&page=1");
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult OnShelf(string id)
        {
            string resultMsg = "";
            int mainId;
            if (!int.TryParse(id, out mainId) && TempData["entity"] == null)
            {
                return Json(new { Result = "Error", Message = "id格式錯誤，請檢查!" });
            }

            List<FamilyNetEvent> deals = _fami.GetFamilyNetEventDealAllListByMainId(mainId);
            bool flag = deals.Count > 0;
            if (flag)
            {
                FamilyNetEvent deal = _fami.GetFamilyNetEventById(mainId);
                deal.Status = (int)FamilyNetEventStatus.WaitingConfirm;
                _fami.UpdateFamilyNetEvent(deal);
                /*
                * 重新上架時，把Bid加入DealTimeSlot
                */
                FamilyNetFacade.UpdateFamilyDealsTime(deals, deal);                       
            }

            string back = string.Format("?filter=0&page=1");
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, ME2O, Production")]
        public ActionResult OffShelf(string id)
        {
            string resultMsg = "";
            int mainId;
            if (!int.TryParse(id, out mainId) && TempData["entity"] == null)
            {
                return Json(new { Result = "Error", Message = "id格式錯誤，請檢查!" });
            }

            List<FamilyNetEvent> deals = _fami.GetFamilyNetEventDealAllListByMainId(mainId);
            bool flag = deals.Count > 0;
            if (flag)
            {
                FamilyNetEvent deal = _fami.GetFamilyNetEventById(mainId);

                DateTime updateDay = DateTime.Today.AddDays(-1);
                DateTime now = DateTime.Now;
                updateDay = updateDay.AddHours(12);
                deal.BusinessHourDeliverTimeE = updateDay;
                deal.BusinessHourOrderTimeE = updateDay;
                deal.ModifyTime = now;
                deal.Status = (int)FamilyNetEventStatus.Shelves;
                _fami.UpdateFamilyNetEvent(deal);

                /*
                * 下架時，把多餘的Bid刪除從DealTimeSlot
                */
                FamilyNetFacade.RemoveFamilyDealsTime(deals, updateDay);                
            }

            string back = string.Format("?filter=0&page=1");
            return Json(new { Result = flag, Message = resultMsg, BackUrl = back });
        }

        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult ActionEventPushMessageList(int? page, string order, bool? isOrderASC)
        {
            SetPageDefault();
            ActionEventPushMessageCollection actionEventPushMessageCol = ActionEventPushMessageColPager(page, order, isOrderASC, true, true);
            return View(actionEventPushMessageCol);
        }

        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult StoreInfo(int? page, string order, bool? isOrderASC)
        {
            SetPageDefault();
            List<FamilyStoreInfo> storeInfoList = FamiStoreInfoPartPager(page, order, isOrderASC);
            return View(storeInfoList);
        }

        private List<FamilyStoreInfo> FamiStoreInfoPartPager(int? page, string order, bool? isOrderASC)
        {
            List<FamilyStoreInfo> storeInfoList;
            var tempStoreInfoList = _fami.GetFamilyStoreInfoList();

            ViewData["Order"] = string.IsNullOrEmpty(order) ? "Id" : order;
            ViewData["IsOrderASC"] = isOrderASC ?? false;
            ViewData["PageIndex"] = page ?? 1;
            ViewData["TotalCount"] = tempStoreInfoList.Count;
            ViewData["PageCount"] = (tempStoreInfoList.Count / 100) + 1;

            //排序
            switch (order)
            {
                case "StoreNo":
                    storeInfoList = (isOrderASC ?? false) ? tempStoreInfoList.OrderBy(x => int.Parse(x.StoreCode)).ToList(): tempStoreInfoList.OrderByDescending(x => int.Parse(x.StoreCode)).ToList();
                    break;
                case "StoreName":
                    storeInfoList = (isOrderASC ?? false) ? tempStoreInfoList.OrderBy(x => x.StoreName).ToList() : tempStoreInfoList.OrderByDescending(x => x.StoreName).ToList();
                    break;
                case "Address":
                    storeInfoList = (isOrderASC ?? false) ? tempStoreInfoList.OrderBy(x => x.Address).ToList() : tempStoreInfoList.OrderByDescending(x => x.Address).ToList();
                    break;
                case "CreateTime":
                    storeInfoList = (isOrderASC ?? false) ? tempStoreInfoList.OrderBy(x => x.CreateTime).ToList() : tempStoreInfoList.OrderByDescending(x => x.CreateTime).ToList();
                    break;
                default:
                    storeInfoList = (isOrderASC ?? false) ? tempStoreInfoList.OrderBy(x => x.Id).ToList() : tempStoreInfoList.OrderByDescending(x => x.Id).ToList();
                    break;
            }

            //分頁
            var storeInfoPartPage = storeInfoList.Skip(((page ?? 1) - 1)*100).Take(100);

            return storeInfoPartPage.ToList();
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult FamilyShopeInfoUpload()
        {
            try
            {
                var nowTime = DateTime.Now;
                List<FamilyStoreInfo> storeInfoList = new List<FamilyStoreInfo>();

                if (Request.Files.Count == 0)
                    return Json(new { Success = false, Message = "未選擇任何檔案。" });

                //支援多檔上傳
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    var file = Request.Files[i];

                    //檢查附檔名
                    if(Path.GetExtension(file.FileName) != ".xls")
                        return Json(new { Success = false, Message = "請確認檔案格式是否正確。" });

                    //選擇工作表
                    HSSFWorkbook workbook = new HSSFWorkbook(file.InputStream);
                    Sheet currentSheet = workbook.GetSheetAt(0);

                    //從第二列開始讀取門市資料
                    for (int j = 1; j <= currentSheet.LastRowNum; j++)
                    {
                        Row row = currentSheet.GetRow(j);

                        //店號為空白則跳過
                        if (string.IsNullOrEmpty(CommonFacade.GetSheetCellValueString(row.GetCell(0)))) continue;

                        var storeInfo = new FamilyStoreInfo();
                        int cnt = 0;
                        while (cnt < 3)
                        {
                            switch (cnt % 3)
                            {
                                case 0:
                                    storeInfo.StoreCode = CommonFacade.GetSheetCellValueString(row.GetCell(cnt));
                                    break;
                                case 1:
                                    storeInfo.StoreName = row.GetCell(1).StringCellValue;
                                    break;
                                case 2:
                                    storeInfo.Address = row.GetCell(2).StringCellValue;
                                    break;
                            }
                            cnt++;
                        }
                        storeInfo.CreateTime = nowTime;
                        storeInfoList.Add(storeInfo);
                    }

                }
                _fami.UpdateOrInsertFamilyStoreInfoList(storeInfoList);

                return Json(new {Success = true, Message = "本次成功匯入 " + storeInfoList.Count + " 筆門市資料。" });
            }
            catch
            {
                return Json(new { Success = false, Message = "儲存失敗，請再次確認檔案格式與內容是否正確。" });
            }
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult UpdateActionMessageStatus(int Id)
        {
            var actionPushMessage = _mp.ActionEventPushMessageGetByActionEventPushMessageId(Id);
            if (actionPushMessage.IsLoaded)
            {
                actionPushMessage.Status = !actionPushMessage.Status;
                _mp.ActionEventPushMessageSet(actionPushMessage);
            }

            return Json(new { Success = true, Message = "狀態已更新" });
        }

        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public ActionResult BeaconElectricPowerList(int? page, string order, bool? isOrderASC)
        {
            SetPageDefault();
            ActionEventDeviceInfoCollection actionEventDeviceInfoCol = ActionEventDeviceInfoColPager(page, order, isOrderASC, true, true);
            return View(actionEventDeviceInfoCol);
        }

        [Authorize(Roles = "Administrator, FamiportPincode, ME2O")]

        public ActionResult BeaconEffectAnalysis(FormCollection form)
        {
            SetPageDefault();
            return View();
            //return PartialView("_PartialBeaconEffectResult");
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, FamiportPincode, ME2O")]
        public ActionResult BeaconEffectAnalysisSearch(int searchType, string startDate, string endDate, string order, bool? isOrderASC)
        {
            SetPageDefault();

            DateTime startDateTime;
            DateTime endDateTime;

            startDateTime=DateTime.Parse(startDate);
            endDateTime = DateTime.Parse(endDate);



            if (searchType == 1)
            {
                List<string> filterList = new List<string>();

                filterList.Add(DailyFamiportItemTriggerReport.Columns.DailyStartTime + ">=" + startDateTime);
                filterList.Add(DailyFamiportItemTriggerReport.Columns.DailyEndTime + "<=" + endDateTime);
                var searchBeaconEffectReportCol = _pp.DailyFamiportItemTriggerReportGetListByDate(false,1,15,"",false,filterList.ToArray());
                DailyBeaconEffectReportModel model = new DailyBeaconEffectReportModel();
                model.DailyFamiportItemTriggerTotalReportList = new List<DailyFamiportItemTotalTriggerReport>();
                model.DailyGroupFamiPortTriggerReportList = new List<DailyGroupFamiPortTriggerReport>();
                model.StartDateTime = startDateTime;
                model.EndDateTime = endDateTime;

                //取得全家商品
                DailyFamiportItemTotalTriggerReport generalFamiReport = new DailyFamiportItemTotalTriggerReport();
                generalFamiReport.FamiportType = 0;
                generalFamiReport.FamiportTypeName = "全家專區商品";

                //取得A商品
                DailyFamiportItemTotalTriggerReport unlockBeaconFamiReport = new DailyFamiportItemTotalTriggerReport();
                unlockBeaconFamiReport.FamiportType = 1;
                unlockBeaconFamiReport.FamiportTypeName = "A商品";

                //取得B商品
                DailyFamiportItemTotalTriggerReport lockBeaconFamiReport = new DailyFamiportItemTotalTriggerReport();
                lockBeaconFamiReport.FamiportType = 2;
                lockBeaconFamiReport.FamiportTypeName = "B商品";

                var groupByBeaconEffectReportList =
                    searchBeaconEffectReportCol.GroupBy(x => x.FamiportId)
                        .Select(z => new DailyFamiportItemTriggerReport()
                        {
                            FamiportId = z.Key,
                            FamiportType = z.First().FamiportType,
                            ItemName = z.First().ItemName,
                            BeaconTriggerAmount = z.Sum(item=>item.BeaconTriggerAmount),
                            SoonTriggerAmount = z.Sum(item => item.SoonTriggerAmount),
                            CouponAmount = z.Sum(item => item.CouponAmount),
                            PayAmount = z.Sum(item => item.PayAmount),
                        });
                foreach (var beaconEffectReport in groupByBeaconEffectReportList)
                {
                    if (beaconEffectReport.FamiportType == 0)
                    {
                        generalFamiReport.SubItemList.Add(beaconEffectReport);
                    }
                    else if (beaconEffectReport.FamiportType == 1)
                    {
                        unlockBeaconFamiReport.SubItemList.Add(beaconEffectReport);
                    }
                    else if (beaconEffectReport.FamiportType == 2)
                    {
                        lockBeaconFamiReport.SubItemList.Add(beaconEffectReport);
                    }

                }


                model.DailyFamiportItemTriggerTotalReportList.Add(lockBeaconFamiReport);
                model.DailyFamiportItemTriggerTotalReportList.Add(unlockBeaconFamiReport);
                model.DailyFamiportItemTriggerTotalReportList.Add(generalFamiReport);
                
                //日期TotalGroup
                var groupByDateBeaconEffectReportList = searchBeaconEffectReportCol.GroupBy(x => new { x.DailyStartTime })
                .Select(z => new DailyGroupFamiPortTriggerReport()
                 {
                      ReportDateTime = z.Key.DailyStartTime,
                      LockBeaconTriggerAmount = z.Where(x => x.FamiportType == 2).Sum(item => item.BeaconTriggerAmount),
                      LockSoonTriggerAmount = z.Where(x => x.FamiportType == 2).Sum(item => item.SoonTriggerAmount),
                      LockCouponAmount = z.Where(x => x.FamiportType == 2).Sum(item => item.CouponAmount),
                      LockPayAmount = z.Where(x => x.FamiportType == 2).Sum(item => item.PayAmount),
                      LockItemCount = z.Count(x => x.FamiportType == 2),
                      UnlockCouponAmount = z.Where(x => x.FamiportType == 1).Sum(item => item.CouponAmount),
                      UnlockPayAmount = z.Where(x => x.FamiportType == 1).Sum(item => item.PayAmount),
                      UnLockItemCount = z.Count(x => x.FamiportType == 1),
                      GeneralCouponAmount = z.Where(x => x.FamiportType == 0).Sum(item => item.CouponAmount),
                      GeneralPayAmount = z.Where(x => x.FamiportType == 0).Sum(item => item.PayAmount),
                      GeneralItemCount = z.Count(x => x.FamiportType == 0),
                });
                model.DailyGroupFamiPortTriggerReportList = groupByDateBeaconEffectReportList.ToList();



                return PartialView("_PartialBeaconEffectResult", model);
            }
            else if (searchType == 2)
            {
                List<string> filterList = new List<string>();

                filterList.Add(DailyBeaconStoreEffectReport.Columns.ReportStartDate + ">=" + startDateTime);
                filterList.Add(DailyBeaconStoreEffectReport.Columns.ReportEndDate + "<=" + endDateTime);
                var searchBeaconStoreEffectReportCol = _pp.DailyBeaconStoreEffectReportGetListByDate(false, 1, 100, "", false, filterList.ToArray());

                DailyBeaconStoreEffectReportModel model = new DailyBeaconStoreEffectReportModel();
                model.StartDateTime = startDateTime;
                model.EndDateTime = endDateTime;
                var groupByDateBeaconStoreEffectReportList = searchBeaconStoreEffectReportCol.GroupBy(x => new { x.StoreTenCode })
                .Select(z => new DailyBeaconStoreEffectReport()
                 {
                      StoreTenCode = z.Key.StoreTenCode,
                      StoreName = z.First().StoreName,
                      BeaconTriggerAmount = z.Sum(item => item.BeaconTriggerAmount),
                      SoonTriggerAmount = z.Sum(item => item.SoonTriggerAmount),
                      PayAmount = z.Sum(item => item.PayAmount),
                }).ToList();

                if (order == DailyBeaconStoreEffectReport.Columns.BeaconTriggerAmount)
                {
                    if (isOrderASC ?? true)
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderBy(x => x.BeaconTriggerAmount).ToList();
                    }
                    else
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderByDescending(x => x.BeaconTriggerAmount).ToList();
                    }
                }
                else if (order == DailyBeaconStoreEffectReport.Columns.SoonTriggerAmount)
                {
                    if (isOrderASC ?? true)
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderBy(x => x.SoonTriggerAmount).ToList();
                    }
                    else
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderByDescending(x => x.SoonTriggerAmount).ToList();
                    }

                }
                else if (order == DailyBeaconStoreEffectReport.Columns.PayAmount)
                {
                    if (isOrderASC ?? true)
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderBy(x => x.PayAmount).ToList();
                    }
                    else
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderByDescending(x => x.PayAmount).ToList();
                    }
                }
                else if (order == "AvgPayAmount")
                {
                    if (isOrderASC ?? true)
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderBy(x => x.PayAmount).ToList();
                    }
                    else
                    {
                        groupByDateBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList.OrderByDescending(x => x.PayAmount).ToList();
                    }
                }

                model.DailyBeaconStoreEffectReportList = groupByDateBeaconStoreEffectReportList;

                if (string.IsNullOrEmpty(order) || isOrderASC == null)
                {
                    ViewData["Order"] = DailyBeaconStoreEffectReport.Columns.Id;
                    ViewData["IsOrderASC"] = false;
                }
                else
                {
                    ViewData["Order"] = order;
                    ViewData["IsOrderASC"] = isOrderASC;
                }

                return PartialView("_PartialBeaconStoreEffectResult",model);
            }
            else
            {
                return PartialView("_PartialBeaconEffectResult");
            }
            
        }


        [Authorize(Roles = "FamiportPincode, Administrator, ME2O, FamiportCustomer")]
        public ActionResult RollBackQuery(FormCollection form) 
        {
            ViewFamiportPinRollbackLogCollection vflogCollection = new ViewFamiportPinRollbackLogCollection();
            SetPageDefault();
            if (form["startDate"] != null && form["endDate"] != null) 
            {
                string startDate = form["startDate"].ToString();
                string endDate = form["endDate"].ToString();
                if (string.IsNullOrEmpty(startDate))
                {
                    startDate = DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd");
                }

                if (string.IsNullOrEmpty(endDate))
                {
                    endDate = DateTime.Now.ToString("yyyy/MM/dd");
                }

                DateTime queryDateStart = DateTime.Parse(string.Format("{0} 00:00:00", startDate));
                DateTime queryDateEnd = DateTime.Parse(string.Format("{0} 23:59:59", endDate));

                ViewData["startDate"] = queryDateStart;
                ViewData["endDate"] = queryDateEnd;

                vflogCollection = famiportRollBackLogPager(queryDateStart, queryDateEnd, null, string.Empty, false);
            }
            return View(vflogCollection);
        }

        [Authorize(Roles = "FamiportPincode, Administrator, ME2O, FamiportCustomer")]
        public ActionResult RollBackPager(DateTime startDate, DateTime endDate, int? page, string order, bool? isOrderASC) 
        {
            SetPageDefault();
            ViewData["startDate"] = startDate;
            ViewData["endDate"] = endDate;
            ViewFamiportPinRollbackLogCollection vflogCollection = famiportRollBackLogPager(startDate, endDate, page, order, isOrderASC);
            return View("RollBackQuery", vflogCollection);
        }

        [Authorize(Roles = "Production, Administrator, ME2O")]
        public ActionResult ExtraCodeLog(int famiportId) 
        {
            SetPageDefault();
            return View(ExtraCodeModelGet(famiportId));
        }

        [HttpPost]
        [Authorize(Roles = "Production, Administrator, ME2O")]
        public ActionResult ExtraCode(FormCollection form) 
        {
            SetPageDefault();
            int famiportId = int.Parse(form["famiportId"].ToString());
            int extraQty = int.Parse(form["extraQty"].ToString());
            int peztempBatchLogId = 0;
            Famiport famiport = _pp.FamiportGet(famiportId);
            if (famiport.IsLoaded && famiport.Status == (int)FamiportStatus.OnProduct)
            {
                bool generatePeztemp = GeneratePeztemp(famiportId, extraQty, this.UserName, System.DateTime.Now.ToString("yyyyMMddHHmmss") + " 加碼", false, false, out famiport, out peztempBatchLogId);
                ViewData["ExtraCodeResult"] = generatePeztemp;
                if (peztempBatchLogId != 0 && !generatePeztemp)
                {
                    _pp.PeztempBatchLogDelete(peztempBatchLogId);
                }
                //產碼會更新 status = 產碼中, 產完碼需恢復原狀態
                famiport.Status = (int)FamiportStatus.OnProduct;
                _pp.FamiportSet(famiport);
            }
            else 
            {
                ViewData["ExtraCodeResult"] = false;
            }
            return View("ExtraCodeLog", ExtraCodeModelGet(famiport.Id));
        }

        private ExtraCodeModel ExtraCodeModelGet(int famiportId) 
        {
            Famiport famiport = _pp.FamiportGet(famiportId);
            PeztempBatchLogCollection logs = _pp.PeztempBatchLogGet(famiport.BusinessHourGuid, false);
            ExtraCodeModel extraCode = new ExtraCodeModel();
            extraCode.Famiport = famiport;
            extraCode.PeztempBatchLogs = logs;
            return extraCode;
        }

        [Authorize(Roles = "Administrator, FamiportPincode, FamiportCustomer, Production, ME2O, CustomerCare, CustomerCare1, CustomerCare2")]
        public ActionResult Query(FormCollection form) 
        {
            SetPageDefault();

            QueryPincodeModel query = new QueryPincodeModel();
            if (!string.IsNullOrEmpty(form["pincode"])) 
            {
                PeztempCollection peztemps = _pp.PeztempPincodeColGet(form["pincode"].ToString(), PeztempServiceCode.Famiport);
                query.QueryPincode = form["pincode"].ToString();
                if (peztemps.Any())
                {
                    var famiport = _pp.FamiportGet(Famiport.Columns.BusinessHourGuid, peztemps.FirstOrDefault().Bid);
                    var famiportPeztempLink = _pp.FamiportPeztempLinkGetByPeztempId(peztemps.FirstOrDefault().Id);
                    var famiposBarcode = _pp.FamiposBarcodeGet(peztemps.FirstOrDefault().Id);
                    query.Peztemps = peztemps;
                    query.Famiport = famiport;
                    query.fpLink = famiportPeztempLink;
                    query.fpBarcode = famiposBarcode;
                }
            }

            return View(query);
        }

        [HttpPost]
        [Authorize(Roles = "FamiportCustomer, FamiportPincode, Administrator")]
        public string PincodeRollBack(int peztempId, int famiportId)
        {
            Peztemp peztemp = _pp.PeztempGet(peztempId);
            Famiport fami = _pp.FamiportGet(famiportId);
            FamiportPeztempLink fpl = _pp.FamiportPeztempLinkGetByPeztempId(peztempId);
            Member m = _mp.MemberGet(User.Identity.Name);

            if (peztemp.IsLoaded && fami.IsLoaded && fpl.IsLoaded && m.IsLoaded) 
            {
                FamiportPinRollbackLog log = new FamiportPinRollbackLog();
                log.PeztempId = peztemp.Id;
                log.FamiportId = fami.Id;
                log.TranNo = fpl.TranNo;
                log.TenCode = fpl.TenCode;
                log.MmkId = (string.IsNullOrEmpty(fpl.MmkId)) ? string.Empty : fpl.MmkId;
                log.MmkIdName = (string.IsNullOrEmpty(fpl.MmkIdName)) ? string.Empty : fpl.MmkIdName;
                log.CreateTime = System.DateTime.Now;
                log.CreateUserId = m.UniqueId;
                int result = _pp.FamiportPinRollbackLogSet(log);
                if (result > 0) 
                {
                    peztemp.IsUsed = false;
                    peztemp.Remark = string.Empty;
                    peztemp.UsedTime = null;
                    if (_pp.PeztempSet(peztemp)) 
                    {
                        _pp.FamiportPeztempLinkDelete(fpl);
                    }
                }

                return new JsonSerializer().Serialize(new { Success = true, Message = "解PIN成功", Pincode = peztemp.PezCode });
            }

            return new JsonSerializer().Serialize(new { Success = false, Message = "無法進行解PIN", Pincode = peztempId });
        }

        [Authorize(Roles = "Production, Administrator, ME2O")]
        public ActionResult DownloadBackupPincode(Guid bid) 
        {
            Famiport famiport = _pp.FamiportGet(Famiport.Columns.BusinessHourGuid, bid);
            PeztempBatchLogCollection peztempBatchLogCol = _pp.PeztempBatchLogGet(bid, false);
            PeztempCollection batchPeztempCol = new PeztempCollection();
            PeztempBatchLogCollection batchLogUpdateQueue = new PeztempBatchLogCollection();

            //將未產出備用序號的log加入 Queue
            foreach (PeztempBatchLog log in peztempBatchLogCol.Where(x => x.IsProducedBackup == false))
            {
                    log.IsProducedBackup = true;
                log.BackupCodeQty = (int)(log.Qty * 0.03);
                    log.ProducedBackupCodeTime = System.DateTime.Now;
                batchLogUpdateQueue.Add(log);
            }

            //如果 Queue 有資料, 代表有新的備用序號需產生
            if (batchLogUpdateQueue.Any())
            {
                int peztempBatchLogId = 0;
                if (GeneratePeztemp(famiport.Id, batchLogUpdateQueue.Sum(x => x.BackupCodeQty), User.Identity.Name, "產出備用序號", false, true, out famiport, out peztempBatchLogId))
                {
                    _pp.PeztempBatchLogSave(batchLogUpdateQueue);
                }
                else 
                {
                    _pp.PeztempBatchLogDelete(peztempBatchLogId);
                }
            }

            //取回屬於備用序號的 peztemp batch log
            PeztempBatchLogCollection peztempBatchLogBackupCol = _pp.PeztempBatchLogGet(bid, true);

            //將所屬的 peztemp 撈回產出excel
            foreach (PeztempBatchLog log in peztempBatchLogBackupCol)
            {
                    batchPeztempCol.AddRange(_pp.PeztempGetBackupCode(bid, log.Id));
                }

            List<string> pincodeList = batchPeztempCol.Select(x => x.PezCode).ToList();

            Workbook workbook = new HSSFWorkbook();
            MemoryStream ms = new MemoryStream();
            HSSFSheet sheet = (HSSFSheet)workbook.CreateSheet("備用序號");

            for (int i = 0; i < pincodeList.Count; i++) 
            {
                HSSFRow dataRow = (HSSFRow)sheet.CreateRow(i);
                dataRow.CreateCell(0).SetCellValue(pincodeList[i]);
            }
            
            return Excel(workbook, ((DateTime)famiport.BusinessHourOrderTimeS).ToString("yyyyMMdd") + "_" + famiport.ItemName + ".xls");
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Production, Administrator, ME2O")]
        public string SaveDraft(string famiportId, string itemName, string itemCode, int pincodeQty, DateTime? deliverTimeS,
            DateTime? deliverTimeE, int itemOrigPrice, int itemPrice, string eventDescription, string eventAttention1, string eventAttention2,
            string eventAttention3, string eventAttention4, DateTime? orderTimeS, DateTime? orderTimeE, string itemDescription,
            string itemAttention1, string itemAttention2, string itemAttention3, string itemAttention4, string user_name, int testCodeQty,int dealType,int displayType ,int isLock )
        {

            Famiport famiport = new Famiport();

            if (int.Parse(famiportId) != 0)
            {
                famiport = _pp.FamiportGet(int.Parse(famiportId));
            }
            else 
            {
                famiport.Status = 0;
            }

            famiport.VendorId = Convert.ToString((int)PeztempServiceCode.Famiport).PadLeft(3, '0');
            famiport.ItemName = itemName;
            famiport.ItemCode = itemCode;
            famiport.PincodeQty = pincodeQty;
            famiport.TestCodeQty = testCodeQty;
            famiport.BusinessHourDeliverTimeS = deliverTimeS;
            famiport.BusinessHourDeliverTimeE = deliverTimeE;
            famiport.ItemOrigPrice = itemOrigPrice;
            famiport.ItemPrice = itemPrice;
            famiport.Remark = eventDescription;
            famiport.EventAttention1 = eventAttention1;
            famiport.EventAttention2 = eventAttention2;
            famiport.EventAttention3 = eventAttention3;
            famiport.EventAttention4 = eventAttention4;
            famiport.BusinessHourOrderTimeS = orderTimeS;
            famiport.BusinessHourOrderTimeE = orderTimeE; 
            famiport.ItemRemark = itemDescription;
            famiport.ItemAttention1 = itemAttention1;
            famiport.ItemAttention2 = itemAttention2;
            famiport.ItemAttention3 = itemAttention3;
            famiport.ItemAttention4 = itemAttention4;
            famiport.IsProducedCode = false;
            famiport.IsOutOfDate = false;
            famiport.Type = dealType;
            famiport.DisplayType = displayType;
            famiport.IsLock = (isLock!=0);


            if (int.Parse(famiportId) != 0)
            {
                _pp.FamiportUpdate(famiport);
            }
            else 
            {
                famiport.CreateId = user_name;
                famiport.CreateTime = System.DateTime.Now;
                _pp.FamiportSet(famiport);
                _pp.FamiportUpdateEventId(famiport.Id, PeztempServiceCode.Famiport);
            }
            
            if (famiport.IsLoaded)
            {
                return new JsonSerializer().Serialize(new { Success = true, Message = "儲存成功", famiportId = famiport.Id });
            }
            else 
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "儲存失敗", famiportId = 0 });
            }
        }

        public string UpdateBusinessHourGuid(int famiportId, Guid bid) 
        {
            Famiport famiport = _pp.FamiportGet(Famiport.Columns.BusinessHourGuid, bid.ToString());
            if (famiport.IsLoaded)
            {
                if (famiport.Id == famiportId)
                {
                    return new JsonSerializer().Serialize(new { Success = true, Message = "更新成功", bid = bid.ToString() });
                }
                else
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "您回填的Bid已存在於別筆資料中", bid = bid.ToString() });
                }
            }
            else 
            {
                ViewPponDeal vpd = _pp.ViewPponDealGetByBusinessHourGuid(bid);
                if (!Helper.IsFlagSet(vpd.GroupOrderStatus ?? 0, GroupOrderStatus.FamiDeal))
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "Bid(" + bid.ToString() + ")非全家檔次", bid = bid.ToString() });
                }

                int updateResult = _pp.FamiportUpdateBid(famiportId, bid);
                if (updateResult == 1)
                {
                    return new JsonSerializer().Serialize(new { Success = true, Message = "更新成功", bid = bid.ToString() });
                }
                else
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "更新失敗", bid = bid.ToString() });
                }
            }            
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public string ConfirmFamiport(int famiportId) 
        {
            Famiport famiport = _pp.FamiportGet(famiportId);
            if (!famiport.IsLoaded) 
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "找不到您要檢驗的資料" });
            }

            famiport.Status = (int)FamiportStatus.Confirmed;
            _pp.FamiportSet(famiport);

            return new JsonSerializer().Serialize(new { Success = true, Message = "檢驗通過" });
        }

        [HttpPost]
        [Authorize(Roles = "Production, Administrator, ME2O")]
        public string SetOnProduct(int famiportId) 
        {
            Famiport famiport = _pp.FamiportGet(famiportId);
            if (!famiport.IsLoaded)
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "找不到您要上檔的資料" });
            }

            famiport.Status = (int)FamiportStatus.OnProduct;

            _pp.FamiportSet(famiport);

            return new JsonSerializer().Serialize(new { Success = true, Message = "上檔完成" });
        }

        [HttpPost]
        [Authorize(Roles = "Production, Administrator, ME2O")]
        public string CheckBackupCode(Guid bid) 
        {
            Famiport famiport = _pp.FamiportGet(Famiport.Columns.BusinessHourGuid, bid);
            if (famiport.Status < (int)FamiportStatus.WaitingConfirm)
            {
                return new JsonSerializer().Serialize(new { EnableBackupCode = false });
            }
            else
            {
                return new JsonSerializer().Serialize(new { EnableBackupCode = true });
            }
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Production, Administrator, ME2O")]
        public string GeneratePeztempRandomCodes(int famiportId, Guid bid, string userName)
        {
            Famiport famiport = new Famiport();
            int peztempBatchLogId = 0;
            if (GeneratePeztemp(famiportId, 0, userName, "第一次產Pin碼", true, false, out famiport, out peztempBatchLogId))
            {
                famiport.IsProducedCode = true;
                famiport.Status = (int)FamiportStatus.WaitingConfirm;
                _pp.FamiportSet(famiport);
                if (famiport.Type == (int)FamilyDealType.BeaconEvent )
                {
                    GenerateFamiActionEvent(famiport);
                }
                

                return new JsonSerializer().Serialize(new { Success = true, Message = "產碼成功", Bid = famiport.BusinessHourGuid.ToString() });
            }
            else 
            {
                if (peztempBatchLogId != 0)
                {
                    _pp.PeztempBatchLogDelete(peztempBatchLogId);
                }
                return new JsonSerializer().Serialize(new { Success = false, Message = "產碼失敗", Bid = string.Empty });
            }
            
        }

        [HttpPost]
        [Authorize(Roles = "FamiportPincode, Administrator, ME2O")]
        public string GetTestPinCode(int famiportId, bool isMakeNew) 
        { 
            Famiport famiport = _pp.FamiportGet(famiportId);
            if (famiport.Status < (int)FamiportStatus.WaitingConfirm) 
            {
                return new JsonSerializer().Serialize(new { Success = false, Message = "此檔次尚未確認", Pincode = string.Empty });
            };

            Peztemp peztemp = _pp.PeztempGetTest(famiport.BusinessHourGuid);

            if (peztemp != null && peztemp.IsLoaded)
            {
                return new JsonSerializer().Serialize(new { Success = true, Message = "序號取得成功", Pincode = peztemp.PezCode });
            }
            else 
            {
                if (!isMakeNew) 
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "測試序號尚未產生", Pincode = string.Empty });    
                }

                peztemp = _pp.PeztempMakeTestCode(famiport.BusinessHourGuid);
                if (peztemp.IsLoaded) 
                {
                    return new JsonSerializer().Serialize(new { Success = true, Message = "序號取得成功", Pincode = peztemp.PezCode });
                }
                else
                {
                    return new JsonSerializer().Serialize(new { Success = false, Message = "序號不足，無法取得測試序號", Pincode = string.Empty });
                }
            }
        }

        public ActionResult Logout() 
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Apply") ;
        }

        public ActionResult SetPassword() 
        {
            return Redirect("../User/UserAccount.aspx");
        }

        #region Private Method

        private bool GeneratePeztemp(int famiportId, int pincodeQty, string userName, string remarks, bool isFirst, bool isBackupCode, out Famiport famiport, out int peztempBatchLogId)
        {
            int memberUniqueId = MemberFacade.GetUniqueId(userName);
            famiport = _pp.FamiportGet(famiportId);
            peztempBatchLogId = 0;

            if (!famiport.IsLoaded)
            {
                return false;
            }

            if (!isBackupCode)
            {
            famiport.Status = (isFirst) ? (int)FamiportStatus.OnFirstCreatingCode : (int)FamiportStatus.OnExtraCreatingCode;
            _pp.FamiportSet(famiport);
            }

            PeztempBatchLog log = new PeztempBatchLog();
            log.Bid = famiport.BusinessHourGuid;
            log.CreateTime = DateTime.Now;
            log.MemberUniqueId = memberUniqueId;
            log.ServiceCode = (int)PeztempServiceCode.Famiport;
            log.Qty = pincodeQty;
            log.Remarks = remarks;
            log.IsBackup = isBackupCode;
            if (pincodeQty == 0) 
            {
                log.Qty = famiport.PincodeQty + famiport.TestCodeQty;
            }
            log = _pp.PeztempBatchLogSave(log);

            if (!log.IsLoaded)
            {
                return false;
            }
            peztempBatchLogId = log.Id;
            return OrderFacade.GeneratePezTempRandomCodes(log.Id, log.Bid, log.Qty, log.ServiceCode, isBackupCode);
        }

        /// <summary>
        /// 判斷備用序號是否已滿
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        private bool IsBackupCodeFull(Guid bid)
        {
            PeztempBatchLogCollection peztempBatchLogCollection = _pp.PeztempBatchLogGet(bid, false);
            return !peztempBatchLogCollection.Any(x => x.IsProducedBackup = false);
        }

        private string[] FamiportRollBackFilter(DateTime startDate, DateTime endDate) 
        {
            List<string> filterList = new List<string>();
            filterList.Add(string.Format("{0} between {1} and {2}", ViewFamiportPinRollbackLog.Columns.CreateTime, startDate.ToString("yyyy/MM/dd HH:mm:ss"), endDate.ToString("yyyy/MM/dd HH:mm:ss")));
            return filterList.ToArray();
        }

        private string[] FamiportQueryFilter(bool isFamiRoles, bool onlyOnProduct)
        {
            List<string> filterList = new List<string>();
            filterList.Add(Famiport.Columns.IsOutOfDate + "=false");
            if (!isFamiRoles) 
            {
                filterList.Add(Famiport.Columns.Status + "<>" + (int)FamiportStatus.Draft);
            }
            if (onlyOnProduct) 
            {
                filterList.Add(Famiport.Columns.Status + "=" + (int)FamiportStatus.OnProduct);
            }
            return filterList.ToArray();
        }

        private void SetPageDefault() 
        {
            ViewBag.UserName = this.UserName;
            ViewBag.UserFullName = this.UserFullName;
            ViewBag.SiteUrl = _config.SiteUrl;
        }
        private ViewFamiportPinRollbackLogCollection famiportRollBackLogPager(DateTime startDate, DateTime endDate, int? page, string order, bool? isOrderASC) 
        {
            ViewFamiportPinRollbackLogCollection log;
            string[] filter = FamiportRollBackFilter(startDate, endDate);

            int totalLogCount = _pp.ViewFamiportPinRollbackLogCount(filter);

            if (page == null || string.IsNullOrEmpty(order) || isOrderASC == null)
            {
                ViewData["Order"] = ViewFamiportPinRollbackLog.Columns.CreateTime;
                ViewData["IsOrderASC"] = false;
                log = _pp.ViewFamiportPinRollBackLogGetList(1, 10, string.Empty, false, filter);
            }
            else
            {
                ViewData["Order"] = order;
                ViewData["IsOrderASC"] = isOrderASC;
                log = _pp.ViewFamiportPinRollBackLogGetList((int)page, 10, order, (bool)isOrderASC, filter);
            }

            ViewData["TotalCount"] = totalLogCount;
            ViewData["PageCount"] = totalLogCount / 10;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalLogCount % 10 > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }

            return log;
        }
        private FamiportCollection famiportColPager(int? page, string order, bool? isOrderASC, bool isFamiRoles, bool onlyOnProduct) 
        {
            FamiportCollection famiportCol;


            int totalFamiportCount = _pp.FamiportGetCount(FamiportQueryFilter(isFamiRoles, onlyOnProduct));

            if (page == null || string.IsNullOrEmpty(order) || isOrderASC == null)
            {
                ViewData["Order"] = Famiport.Columns.BusinessHourOrderTimeS;
                ViewData["IsOrderASC"] = false;
                famiportCol = _pp.FamiportGetList(1, 15, Famiport.Columns.BusinessHourOrderTimeS, false, FamiportQueryFilter(isFamiRoles, onlyOnProduct));
            }
            else
            {
                ViewData["Order"] = order;
                ViewData["IsOrderASC"] = isOrderASC;
                famiportCol = _pp.FamiportGetList((int)page, 15, order, (bool)isOrderASC, FamiportQueryFilter(isFamiRoles, onlyOnProduct));
            }

            ViewData["TotalCount"] = totalFamiportCount;
            ViewData["PageCount"] = totalFamiportCount / 15;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalFamiportCount % 15 > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            return famiportCol;
        }

        private ActionEventPushMessageCollection ActionEventPushMessageColPager(int? page, string order, bool? isOrderASC, bool isFamiRoles, bool onlyOnProduct)
        {
            //ActionEventPushMessageCollection actionEventPushMessageCol = _mp.ActionEventPushMessageListGet();
            ActionEventPushMessageCollection actionEventPushMessageCol ;


            int totalActionEventPushMessageCount = _mp.ActionEventPushMessageByFamiGetList().Count;

            if (page == null || string.IsNullOrEmpty(order) || isOrderASC == null)
            {
                ViewData["order"] = ActionEventPushMessage.Columns.SendStartTime;
                ViewData["IsOrderASC"] = false;

                actionEventPushMessageCol = _mp.ActionEventPushMessageGetList(1, 15, ActionEventPushMessage.Columns.Id, false, new List<string>().ToArray());
            }
            else
            {
                ViewData["Order"] = order;
                ViewData["IsOrderASC"] = isOrderASC;

                actionEventPushMessageCol = _mp.ActionEventPushMessageGetList((int)page, 15, order, (bool)isOrderASC, new List<string>().ToArray());

                
            }

            ViewData["TotalCount"] = totalActionEventPushMessageCount;
            ViewData["PageCount"] = totalActionEventPushMessageCount / 15;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalActionEventPushMessageCount % 15 > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            return actionEventPushMessageCol;
        }

        private ActionEventDeviceInfoCollection ActionEventDeviceInfoColPager(int? page, string order, bool? isOrderASC, bool isFamiRoles, bool onlyOnProduct)
        {
            ActionEventDeviceInfoCollection actionEventDeviceInfoCol = _mp.ActionEventDeviceInfoListGet(1);

            List<string> filterList = new List<string>();
            filterList.Add(ActionEventDeviceInfo.Columns.ActionEventInfoId + "=1" );
            


            int totalactionEventDeviceInfoCount = actionEventDeviceInfoCol.Count;

            if (page == null || string.IsNullOrEmpty(order) || isOrderASC == null)
            {
                ViewData["order"] = ActionEventDeviceInfo.Columns.ElectricPower;
                ViewData["IsOrderASC"] = false;

                actionEventDeviceInfoCol = _mp.ActionEventDeviceInfoGetList(1, 15, ActionEventDeviceInfo.Columns.ElectricPower, false, filterList.ToArray());
            }
            else
            {
                ViewData["Order"] = order;
                ViewData["IsOrderASC"] = isOrderASC;
                actionEventDeviceInfoCol = _mp.ActionEventDeviceInfoGetList((int)page, 15, order, (bool)isOrderASC, filterList.ToArray());
            }

            ViewData["TotalCount"] = totalactionEventDeviceInfoCount;
            ViewData["PageCount"] = totalactionEventDeviceInfoCount / 15;
            ViewData["PageIndex"] = (page == null) ? 1 : page;
            if (totalactionEventDeviceInfoCount % 15 > 0)
            {
                ViewData["PageCount"] = (int)ViewData["PageCount"] + 1;
            }
            return actionEventDeviceInfoCol;
        }

        private bool GenerateFamiActionEvent(Famiport famiport)
        {
            if (famiport.Id >0)
            {
                var actionEventPushMessage = _mp.ActionEventPushMessageGetByFamiPortId(famiport.Id);
                if (!actionEventPushMessage.IsLoaded)
                {
                    actionEventPushMessage = new ActionEventPushMessage();                    
                }
                actionEventPushMessage.EventType = (int)ActionEventType.BeaconPush;
                actionEventPushMessage.Status = true;
                actionEventPushMessage.Subject = actionEventPushMessage.Content = ( string.Format("本週好康{0}，限時驚喜優惠，快到全家門市享折扣！", famiport.ItemName));

                actionEventPushMessage.Action = string.Empty;
                if (famiport.BusinessHourOrderTimeS.HasValue)
                {
                    actionEventPushMessage.SendStartTime = famiport.BusinessHourOrderTimeS.Value;
                }
                else
                {
                    try
                    {
                        actionEventPushMessage.SendStartTime = famiport.BusinessHourOrderTimeS.Value;
                    }
                    catch
                    {
                        SetErrorLog();
                    }
                    
                }

                if (famiport.BusinessHourOrderTimeE.HasValue)
                {
                    actionEventPushMessage.SendEndTime = famiport.BusinessHourOrderTimeE.Value;
                }
                else
                {
                    try
                    {
                        actionEventPushMessage.SendEndTime = famiport.BusinessHourOrderTimeE.Value;
                    }
                    catch
                    {
                        SetErrorLog();
                    }
                }
                actionEventPushMessage.CreateTime = DateTime.Now;
                Member member = _mp.MemberGet(User.Identity.Name);
                actionEventPushMessage.CreateId = member.UniqueId;
                actionEventPushMessage.FamportId = famiport.Id;
                actionEventPushMessage.BusinessHourGuid = famiport.BusinessHourGuid;
                _mp.ActionEventPushMessageSet(actionEventPushMessage);
            }


            return true;
        }
        private static DateTime? ConvertDateAndTime(string datetime)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(string.Format("{0}", datetime), out dt))
            {
                return dt;
            }
            return null;
        }

        private void SetErrorLog()
        {
            //todo: 處理ErrorLog問題
        }

        #endregion Private Method
    }
    
    public class ExtraCodeModel 
    {
        public Famiport Famiport { get; set; }
        public PeztempBatchLogCollection PeztempBatchLogs { get; set; }
    }
    public class QueryPincodeModel 
    {
        public string QueryPincode { get; set; }
        public Famiport Famiport { get; set; }
        public PeztempCollection Peztemps { set; get; }
        public FamiportPeztempLink fpLink { set; get; }
        public FamiposBarcode fpBarcode { set; get; }
    }
    public class FamilyNetEventListModel
    {
        public IEnumerable<FamilyNetEvent> List { get; set; }
        public int Page { get; set; }
        public int LastPage { get; set; }
        public int DealCount { get; set; }
        public int Filter { get; set; }
        public bool IsME2O { get; set; }
        public bool IsProduction { get; set; }
        public bool IsAdministrator { get; set; }
    }
    [Serializable()]
    public class FamilyNetEventDealModel
    {
        public int Id { get; set; }
        public string MainBusinessHourGuid { get; set; }
        public string BusinessHourGuid { get; set; }
        public string ActCode { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int ItemOrigPrice { get; set; }
        public int ItemPrice { get; set; }
        public int PurchasePrice { get; set; }
        public int MaxItemCount { get; set; }
        public int Buy { get; set; }
        public int Free { get; set; }
        public string BusinessHourOrderTimeS { get; set; }
        public string BusinessHourOrderTimeE { get; set; }
        public string BusinessHourDeliverTimeS { get; set; }
        public string BusinessHourDeliverTimeE { get; set; }
        public string Remark { get; set; }
        public int CreateId { get; set; }        
        public int Status { get; set; }
        public FamilyNetEventIsComboDeal IsComboDeal { get; set; }
        public int ComboDealMainId { get; set; }
        public List<FamilyNetEvent> FamilyNetEventComboDealList { get; set; }
        public string ComboDealJsonList { get; set; }
        public bool IsAllowEdit { get; set; }
        public bool IsAfterSaveAllowEdit { get; set; }
        public string Page { get; set; }
        public string Filter { get; set; }
        public FamilyBarcodeVersion Version { get; set; }
    }
}

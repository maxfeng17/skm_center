﻿using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace LunchKingSite.WebLib.Controllers.V1
{

    [RequireHttps]
    public class AppController : BaseController
    {
        public JsonResult GetHotSearchKey()
        {
            var result = new ApiResult() { Code = ApiResultCode.Success };

            result.Data = ApiAppManager.GetSearchKeyList((int)PromoSearchKeyUseMode.APP);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public EmptyResult HotSearchKeyRecord(int keyId = 0)
        {

            ApiAppManager.SetSearchKeyRecord(keyId, UserName, "from api");

            return new EmptyResult();
        }

        public JsonResult GetKeywordPromptList(string word)
        {
            var result = new ApiResult() { Code = ApiResultCode.Success };
            result.Data = ApiAppManager.GetKWPromptList(word);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}

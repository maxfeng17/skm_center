using Elmah;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using log4net;

namespace LunchKingSite.WebLib
{
    public static class WebUtility
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        private static string SHORTURL_SITE = config.SiteUrl + "/tinyurl/";

        /// <summary>
        /// sometimes within a try...catch you may want to still log the exception with exception handler, use this function.
        /// </summary>
        /// <param name="e"></param>
        public static void LogExceptionAnyway(Exception e)
        {
            ErrorSignal.FromCurrentContext().Raise(e);
        }

        public static string GetSiteRoot()
        {
            return BizLogic.WebUtility.GetSiteRoot(false);
        }

        public static string GetSiteRoot(bool forceSecure)
        {
            return BizLogic.WebUtility.GetSiteRoot(forceSecure);
        }

        public static bool IsHttps()
        {
            string protocol = HttpContext.Current.Request.ServerVariables["SERVER_PORT_SECURE"];
            return protocol != null && protocol != "1";
        }

        /// <summary>
        /// 取得匿名物件的私有屬性值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static T GetField<T>(this object obj, string fieldName)
        {
            try
            {
                string textFieldName = string.Format("<{0}>i__Field", fieldName);
                // Get the type
                Type t = obj.GetType();
                // Get the field information using reflection
                FieldInfo fi = t.GetField(textFieldName, BindingFlags.NonPublic | BindingFlags.Instance);

                return (T)fi.GetValue(obj);
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        /// <summary>
        /// 設定匿名物件的私有屬性值
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fieldName"></param>
        /// <param name="val"></param>
        public static void SetField<T>(this object obj, string fieldName, T val)
        {
            string textFieldName = string.Format("<{0}>i__Field", fieldName);
            // Get the type
            Type t = obj.GetType();
            // Get the field information using reflection
            FieldInfo fiText = t.GetField(textFieldName, BindingFlags.NonPublic | BindingFlags.Instance);

            // Set the field using reflection
            fiText.SetValue(obj, val);
        }


        /// <summary>
        /// 取得列舉的中文說明
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string GetTextFromEnum(ResourceManager source, Enum value)
        {
            Type enumType = value.GetType();
            bool isFlags = enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0;
            foreach (FieldInfo f in enumType.GetFields(BindingFlags.Public | BindingFlags.Static)) // query each fields from enum
            {
                if (f.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0) // not marked as exlude
                {
                    if (isFlags ? Helper.IsFlagSet(Convert.ToInt64(value), Convert.ToInt64(f.GetRawConstantValue())) : Convert.ToInt64(value) == Convert.ToInt64(f.GetRawConstantValue()))
                    {
                        return Helper.GetLocalizedEnum(source, f);
                    }
                }
            }

            return string.Empty;
        }

        public static dynamic GetListFromEnum(ResourceManager source, Enum value)
        {
            List<dynamic> lic = new List<dynamic>();
            Type enumType = value.GetType();
            bool isFlags = enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0;
            foreach (FieldInfo f in enumType.GetFields(BindingFlags.Public | BindingFlags.Static)) // query each fields from enum
            {
                if (f.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0) // not marked as exlude
                {
                    lic.Add(new
                    {
                        Text = Helper.GetLocalizedEnum(source, f),
                        Value = f.GetRawConstantValue().ToString(),
                    });
                }
            }
            return lic.ToArray();
        }

        public static string GetFacebookLoginUrl()
        {
            string fbRetUrl = Helper.CombineUrl(config.SiteUrl, "/newmember/fbauth.ashx");
            string fbLoginStat = Guid.NewGuid().ToString().Substring(0, 4);
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["FBLoginState"] = fbLoginStat;
            }
            return string.Format(
                "https://www.facebook.com/{0}dialog/oauth?client_id={1}&redirect_uri={2}&scope={3}&state={4}",
                FacebookUtility._FACEBOOK_API_VER_2_10,
                config.FacebookApplicationId, fbRetUrl, FacebookUser.DefaultScope,
                fbLoginStat);
        }

        public static string GetLineLoginUrl()
        {
            string lineLoginState = Guid.NewGuid().ToString().Substring(0, 4);
            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["LineLoginState"] = lineLoginState;
            }
            return LineUtility.LineLoginUrl(lineLoginState, config.LineAuthRedirectUri);
        }

        public static ListItem[] GetListItemFromEnum(Enum value)
        {
            return GetListItemFromEnum(I18N.Phrase.ResourceManager, value);
        }

        public static ListItem[] GetListItemFromEnum(ResourceManager source, Enum value)
        {
            List<ListItem> lic = new List<ListItem>();
            Type enumType = value.GetType();
            bool isFlags = enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0;
            foreach (FieldInfo f in enumType.GetFields(BindingFlags.Public | BindingFlags.Static)) // query each fields from enum
            {
                if (f.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0) // not marked as exlude
                {
                    lic.Add(new ListItem
                    {
                        Text = Helper.GetLocalizedEnum(source, f),
                        Value = f.GetRawConstantValue().ToString(),
                        Selected = (isFlags ? Helper.IsFlagSet(Convert.ToInt64(value), Convert.ToInt64(f.GetRawConstantValue())) : Convert.ToInt64(value) == Convert.ToInt64(f.GetRawConstantValue()))
                    });
                }
            }

            return lic.ToArray();
        }

        #region systemCode版

        public static List<ListItem> GetListItemFromEnumWithSystemCode(Enum value)
        {
            List<ListItem> lic = new List<ListItem>();
            Type enumType = value.GetType();
            bool isFlags = enumType.GetCustomAttributes(typeof(FlagsAttribute), false).Length > 0;
            foreach (FieldInfo f in enumType.GetFields(BindingFlags.Public | BindingFlags.Static)) // query each fields from enum
            {
                if (f.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0) // not marked as exlude
                {
                    SystemCodeAttribute attr = SystemCodeManager.GetSystemCodeAttributeByEnum(f);
                    lic.Add(new ListItem
                    {
                        Text = SystemCodeManager.GetSystemCodeNameByGroupAndId(attr.GroupName, attr.Key),
                        Value = attr.Key.ToString(),
                        Selected =
                                        (isFlags
                                             ? Helper.IsFlagSet(Convert.ToInt64(value),
                                                                Convert.ToInt64(f.GetRawConstantValue()))
                                             : Convert.ToInt64(value) == Convert.ToInt64(f.GetRawConstantValue()))
                    });
                }
            }

            return lic;
        }

        public static string GetOptionNameFromEnumOptionWithSystemCode(Enum value)
        {
            var rtn = string.Empty;
            Type enumType = value.GetType();
            var field = enumType.GetField(value.ToString(), BindingFlags.Public | BindingFlags.Static);
            if (field != null && field.GetCustomAttributes(typeof(ExcludeBindingAttribute), false).Length == 0)
            {
                rtn = SystemCodeManager.GetSystemCodeEnum(field);
            }
            return rtn;
        }

        #endregion systemCode版

        public static string GetPayEasySsoPath(string backUrl)
        {
            ISysConfProvider config = ProviderFactory.Instance().GetConfig();
            if (config.NewPezSso)
            {
                return GetSiteRoot(true) + "/NewMember/PezLogin.aspx";
            }
            else
            {
                return "http://www.payeasy.com.tw/ECShop/Forward.jsp?path=https://www.payeasy.com.tw/MyPayeasy/auth/Login.jsp?ExtraSys=2&sid=24317014_17Life_member&ParaValue=" +
                    HttpUtility.UrlEncode(GetSiteRoot() + backUrl);
            }
        }

        public static string GetPayEasySsoPath()
        {
            return GetPayEasySsoPath("/user/sso.aspx");
        }

        public static string Get17LifeRegisterPath(bool isTest)
        {
            return LunchKingSite.BizLogic.Component.MemberActions.MemberUtilityCore.RegisterUrl;
        }

        public static string GetLoginPath()
        {
            return GetSiteRoot(true) + FormsAuthentication.LoginUrl;
        }


        public static string GetGoogleLoginUrl()
        {
            string returnUrl = Helper.CombineUrl(config.SiteUrl, "newmember/oauth2callback.ashx");
            return string.Format(
                "https://accounts.google.com/o/oauth2/auth?client_id={0}&response_type={1}&scope={2}&redirect_uri={3}&state={4}&login_hint={5}",
                config.GoogleApiKey, "code", GoogleUser.DefaultScope, returnUrl, HttpContext.Current.Session.SessionID, "");
        }

        public static PlaceHolder ClassToControl<T>(T instance) where T : class
        {
            return ClassToControl(instance, ConfigParameterAttribute.ConfigTypes.normal);
        }

        public static PlaceHolder ClassToControl<T>(T instance, ConfigParameterAttribute.ConfigTypes configType) where T : class
        {
            PlaceHolder pc = new PlaceHolder();

            if (instance == null)
            {
                return pc;
            }

            // iterate through parameters
            foreach (PropertyInfo pi in typeof(ISysConfProvider).GetProperties())
            {
                #region 讀取Attributes相關資訊

                object[] cpAttrs = pi.GetCustomAttributes(typeof(ConfigParameterAttribute), true);
                if (cpAttrs.Length == 0)
                {
                    continue;
                }
                if (((ConfigParameterAttribute)cpAttrs[0]).ConfigType != configType)
                {
                    continue;
                }

                #endregion 讀取Attributes相關資訊

                Label name = new Label { Text = Helper.GetLocalizedEnum(pi) + " &nbsp;" + ((ConfigParameterAttribute)cpAttrs[0]).Description, ForeColor = Color.Blue };

                WebControl ctrl = null;

                // checking the type of the property
                if (pi.PropertyType.IsEnum)
                {
                    ListItem[] data = WebUtility.GetListItemFromEnum((Enum)pi.GetValue(instance, null));
                    if (pi.PropertyType.GetCustomAttributes(typeof(FlagsAttribute), true).Length > 0)
                    {
                        ctrl = new CheckBoxList();
                        ((CheckBoxList)ctrl).RepeatColumns = 4;
                    }
                    else
                    {
                        ctrl = new DropDownList();
                    }

                    ((ListControl)ctrl).Items.Clear();
                    ((ListControl)ctrl).Items.AddRange(data);
                }
                else if (pi.PropertyType == typeof(bool))
                {
                    ctrl = new CheckBox();
                    ((CheckBox)ctrl).Checked = (bool)pi.GetValue(instance, null) == true;
                }
                else if (pi.PropertyType == typeof(string[]))
                {
                    ctrl = new TextBox
                    {
                        Text = string.Join(Environment.NewLine, (string[])pi.GetValue(instance, null)),
                        Columns = 30,
                        Rows = 4,
                        TextMode = TextBoxMode.MultiLine
                    };
                    ctrl.Style[HtmlTextWriterStyle.VerticalAlign] = "top";
                }
                else if (pi.PropertyType == typeof(int[]))
                {
                    ctrl = new TextBox
                    {
                        Text = string.Join(Environment.NewLine, (int[])pi.GetValue(instance, null)),
                        Columns = 30,
                        Rows = 4,
                        TextMode = TextBoxMode.MultiLine
                    };
                    ctrl.Style[HtmlTextWriterStyle.VerticalAlign] = "top";
                }
                else
                {
                    ctrl = new TextBox();
                    ((TextBox)ctrl).Text = Convert.ToString(pi.GetValue(instance, null));
                    ((TextBox)ctrl).Columns = 30;
                }

                ctrl.Enabled = pi.CanWrite;
                ctrl.ID = pi.Name;

                pc.Controls.Add(name);
                pc.Controls.Add(ctrl);
                pc.Controls.Add(new Literal() { Text = "<br /><br />" });
            }

            return pc;
        }

        public static PlaceHolder ClassToControl<T>(T instance, ConfigParameterAttribute.ConfigCategories category) where T : class
        {
            PlaceHolder pc = new PlaceHolder();

            if (instance == null)
            {
                return pc;
            }

            ConfigParameterAttribute.ConfigTypes[] confTypes = Enum.GetValues(typeof(ConfigParameterAttribute.ConfigTypes)) as ConfigParameterAttribute.ConfigTypes[];
            Dictionary<ConfigParameterAttribute.ConfigTypes, IList<Control>> controls = new Dictionary<ConfigParameterAttribute.ConfigTypes, IList<Control>>();
            foreach (ConfigParameterAttribute.ConfigTypes type in confTypes)
            {
                IList<Control> temp = new List<Control>();
                temp.Add(new Literal() { Text = "<h4>" + type + "</h4>" });
                controls.Add(type, temp);
            }

            // iterate through parameters
            foreach (PropertyInfo pi in typeof(ISysConfProvider).GetProperties())
            {
                object[] cpAttrs = pi.GetCustomAttributes(typeof(ConfigParameterAttribute), true);
                if (cpAttrs.Length == 0)
                {
                    continue;
                }

                IList<Control> temp = controls[((ConfigParameterAttribute)cpAttrs[0]).ConfigType];
                if (Enum.Equals(category, ((ConfigParameterAttribute)cpAttrs[0]).ConfigCategory))
                {
                    Label name = new Label { Text = Helper.GetLocalizedEnum(pi) + " &nbsp;" + ((ConfigParameterAttribute)cpAttrs[0]).Description + " &nbsp;", ForeColor = Color.Blue };

                    WebControl ctrl = null;

                    #region checking the type of the property

                    if (pi.PropertyType.IsEnum)
                    {
                        ListItem[] data = WebUtility.GetListItemFromEnum((Enum)pi.GetValue(instance, null));
                        if (pi.PropertyType.GetCustomAttributes(typeof(FlagsAttribute), true).Length > 0)
                        {
                            ctrl = new CheckBoxList();
                            ((CheckBoxList)ctrl).RepeatColumns = 4;
                        }
                        else
                        {
                            ctrl = new DropDownList();
                        }

                        ((ListControl)ctrl).Items.Clear();
                        ((ListControl)ctrl).Items.AddRange(data);
                    }
                    else if (pi.PropertyType == typeof(bool))
                    {
                        ctrl = new CheckBox();
                        ((CheckBox)ctrl).Checked = (bool)pi.GetValue(instance, null) == true;
                    }
                    else if (pi.PropertyType == typeof(string[]))
                    {
                        ctrl = new TextBox
                        {
                            Text = string.Join(Environment.NewLine, (string[])pi.GetValue(instance, null)),
                            Columns = 30,
                            Rows = 4,
                            TextMode = TextBoxMode.MultiLine
                        };
                        ctrl.Style[HtmlTextWriterStyle.VerticalAlign] = "top";
                    }
                    else if (pi.PropertyType == typeof(int[]))
                    {
                        ctrl = new TextBox
                        {
                            Text = string.Join(Environment.NewLine, (int[])pi.GetValue(instance, null)),
                            Columns = 30,
                            Rows = 4,
                            TextMode = TextBoxMode.MultiLine
                        };
                        ctrl.Style[HtmlTextWriterStyle.VerticalAlign] = "top";
                    }
                    else
                    {
                        ctrl = new TextBox();
                        ((TextBox)ctrl).Text = Convert.ToString(pi.GetValue(instance, null));
                        ((TextBox)ctrl).Columns = 30;
                    }

                    #endregion checking the type of the property

                    ctrl.Enabled = pi.CanWrite;
                    ctrl.ID = pi.Name;

                    temp.Add(name);
                    temp.Add(ctrl);
                    temp.Add(new Literal() { Text = "<br /><br />" });
                }
                else
                {
                    continue;
                }
            }

            foreach (ConfigParameterAttribute.ConfigTypes type in confTypes)
            {
                IList<Control> ctls = controls[type];
                if (ctls.Count > 1)
                {
                    foreach (Control ctl in ctls)
                    {
                        pc.Controls.Add(ctl);
                    }
                }
            }

            return pc;
        }

        public static string ClassToHtml<T>(T instance) where T : class
        {
            if (instance == null)
            {
                return null;
            }

            string html = string.Empty;
            // iterate through parameters
            foreach (PropertyInfo pi in instance.GetType().GetProperties())
            {
                string tag = pi.Name;

                // checking the type of the property
                if (pi.PropertyType.IsEnum)
                {
                    ListItem[] data = WebUtility.GetListItemFromEnum((Enum)pi.GetValue(instance, null));
                    if (pi.PropertyType.GetCustomAttributes(typeof(FlagsAttribute), true).Length > 0)
                    {
                        tag += string.Format(@"<ul class=""cbl"" id=""{0}"">", pi.Name);
                        foreach (ListItem li in data)
                        {
                            tag += string.Format(@"<li>{0}<input type=""checkbox"" name=""{1}"" {2}/></li>",
                                li.Text, li.Value, li.Selected ? @"checked=""checked""" : string.Empty);
                        }
                        tag += @"</ul>";
                    }
                    else
                    {
                        tag += string.Format(@"<select class=""ddl"" id=""{0}"">", pi.Name);
                        foreach (ListItem li in data)
                        {
                            tag += string.Format(@"<option value=""{0}"" {1}>{2}</option>", li.Value,
                                li.Selected ? @"selected=""selected""" : string.Empty, li.Text);
                        }
                        tag += @"</select>";
                    }
                }
                else if (pi.PropertyType == typeof(bool))
                {
                    tag += string.Format(@"<input type=""checkbox"" name=""{0}"" {1}/>", pi.Name,
                        (bool)pi.GetValue(instance, null) == true ? @"checked=""checked""" : string.Empty);
                }
                else
                {
                    tag += string.Format(@"<input type=""textbox"" name=""{0}"" value=""{1}"" cols=""30"" />",
                        pi.Name, pi.GetValue(instance, null).ToString());
                }
                html += tag + @"<br />";
            }

            return html;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="ctl"></param>
        /// <param name="instance"></param>
        /// <returns>change list</returns>
        public static ConcurrentDictionary<string, string> ControlToClass<T>(Control ctl, T instance) where T : class
        {
            ConcurrentDictionary<string, string> changeCollection = new ConcurrentDictionary<string, string>();
            // iterate through parameters
            foreach (PropertyInfo pi in instance.GetType().GetProperties())
            {
                Control ctrl = ctl.FindControl(pi.Name);

                // if we can't find the control or property can't be set, try next one
                if (ctrl == null || !pi.CanWrite)
                {
                    continue;
                }

                var keyValue = UpdateInstance<T>(instance, pi, ctrl);
                if (keyValue != null)
                {
                    changeCollection.TryAdd(((KeyValuePair<string, string>)keyValue).Key, ((KeyValuePair<string, string>)keyValue).Value);
                }
            }

            return changeCollection;
        }

        public static KeyValuePair<string, string>? UpdateInstance<T>(T instance, PropertyInfo pi, Control ctrl) where T : class
        {
            if (pi.PropertyType.IsEnum)
            {
                int v = 0;
                if (pi.PropertyType.GetCustomAttributes(typeof(FlagsAttribute), true).Length > 0)
                {
                    foreach (ListItem li in ((CheckBoxList)ctrl).Items)
                    {
                        if (li.Selected)
                        {
                            v |= int.Parse(li.Value);
                        }
                    }
                }
                else
                {
                    v = int.Parse(((DropDownList)ctrl).SelectedValue);
                }

                if ((int)pi.GetValue(instance, null) == v) return null;
                pi.SetValue(instance, v, null);
                return new KeyValuePair<string, string>(pi.Name, v.ToString());
            }
            else if (pi.PropertyType == typeof(bool))
            {
                if ((bool)pi.GetValue(instance, null) == ((CheckBox)ctrl).Checked) return null;
                pi.SetValue(instance, ((CheckBox)ctrl).Checked, null);
                return new KeyValuePair<string, string>(pi.Name, ((CheckBox)ctrl).Checked.ToString());
            }
            else if (pi.PropertyType == typeof(Guid))
            {
                if ((Guid)pi.GetValue(instance, null) == new Guid(((TextBox)ctrl).Text)) return null;
                pi.SetValue(instance, new Guid(((TextBox)ctrl).Text), null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else if (pi.PropertyType == typeof(DateTime))
            {
                if ((DateTime)pi.GetValue(instance, null) == DateTime.Parse(((TextBox)ctrl).Text)) return null;
                pi.SetValue(instance, DateTime.Parse(((TextBox)ctrl).Text), null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else if (pi.PropertyType == typeof(int))
            {
                if ((int)pi.GetValue(instance, null) == int.Parse(((TextBox)ctrl).Text)) return null;
                pi.SetValue(instance, int.Parse(((TextBox)ctrl).Text), null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else if (pi.PropertyType == typeof(decimal))
            {
                if ((decimal)pi.GetValue(instance, null) == decimal.Parse(((TextBox)ctrl).Text)) return null;
                pi.SetValue(instance, decimal.Parse(((TextBox)ctrl).Text), null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else if (pi.PropertyType == typeof(string[]))
            {
                string[] tmp = ((TextBox)ctrl).Text.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                if (string.Join(Environment.NewLine, ((string[])pi.GetValue(instance, null))) == string.Join(Environment.NewLine, tmp)) return null;
                pi.SetValue(instance, tmp, null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else if (pi.PropertyType == typeof(int[]))
            {
                var rtnList = new List<int>();
                var srcArray = ((TextBox)ctrl).Text.Split(new[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var item in srcArray)
                {
                    int rtn;
                    if (int.TryParse(item, out rtn))
                    {
                        rtnList.Add(rtn);
                    }
                }
                int[] tmp = rtnList.ToArray();
                if (string.Join(";", ((int[])pi.GetValue(instance, null))) == string.Join(";", tmp)) return null;
                pi.SetValue(instance, tmp, null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
            else
            {
                if ((string)pi.GetValue(instance, null) == ((TextBox)ctrl).Text) return null;
                pi.SetValue(instance, ((TextBox)ctrl).Text, null);
                return new KeyValuePair<string, string>(pi.Name, ((TextBox)ctrl).Text);
            }
        }

        public static string RequestShortUrl(string realUrl)
        {
            DateTime now = DateTime.Now;
            string tinyUrl;
            try
            {
                var req = (HttpWebRequest)WebRequest.Create(SHORTURL_SITE + "register/?realUrl=" + HttpUtility.UrlEncode(realUrl));
                using (var res = (HttpWebResponse)req.GetResponse())
                {
                    using (var resSrm = new StreamReader(res.GetResponseStream()))
                    {
                        tinyUrl = resSrm.ReadToEnd();
                        LogManager.GetLogger("tinyurl").InfoFormat("{0} -> {1} -> {2}",
                            tinyUrl,
                            (DateTime.Now - now).TotalSeconds.ToString("0.###"),
                            realUrl);
                    }
                }
            }
            catch (Exception ex)
            {
                LogManager.GetLogger("tinyurl").InfoFormat("{0} -> {1} -> {2}",
                    ex.Message,
                    (DateTime.Now - now).TotalSeconds.ToString("0.###"),
                    realUrl);
                LogExceptionAnyway(ex);
                tinyUrl = null;
            }

            return string.IsNullOrEmpty(tinyUrl) ? null : SHORTURL_SITE + tinyUrl;
        }
    }
}
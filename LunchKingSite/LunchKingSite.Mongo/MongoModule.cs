﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Reflection;
using System.Text;
using Autofac;
using Vodka.Container;
using Vodka.DataAccess;
using Vodka.Mongo;

namespace LunchKingSite.Mongo
{
    [Export(typeof(IModuleRegistrar))]
    public class MongoModule : IModuleRegistrar
    {
        public void RegisterWithContainer(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(MongoRepository<>)).As(typeof(IRepository<>)).As(typeof(IMongoRepository<>)).InstancePerDependency();
            builder.RegisterGeneric(typeof(EntityMapper<>)).InstancePerLifetimeScope();

            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).AsSelf().AsImplementedInterfaces().InstancePerDependency();
        }

        public void Initialize(IContainer container)
        {
            var builder = new ContainerBuilder();
            builder.Register(c => new MongoSessionLocator(c.ResolveNamed<string>("mongoConnectionString")))
                .As<ISessionLocator>().InstancePerDependency();

            builder.Update(container);
        }
    }
}

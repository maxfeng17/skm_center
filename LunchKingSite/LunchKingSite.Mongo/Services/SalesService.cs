﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Model.Sales;
using MongoDB.Bson;
using MongoDB.Driver;
using Vodka.DataAccess;
using MongoDB.Driver.Builders;
using Vodka.Mongo;
using System.Linq.Expressions;

namespace LunchKingSite.Mongo.Services
{
    public class SalesService
    {
        private IMongoRepository<BusinessOrder> _boRepo;
        private IMongoRepository<SalesStore> _sRepo;
        private IMongoRepository<Counter> _countRepo;
        private IMongoRepository<SalesMember> _mRepo;
        public SalesService(IMongoRepository<BusinessOrder> boRepo, IMongoRepository<SalesStore> sRepo, IMongoRepository<Counter> countRepo, IMongoRepository<SalesMember> mRepo)
        {
            _boRepo = boRepo;
            _sRepo = sRepo;
            _countRepo = countRepo;
            _mRepo = mRepo;
        }

        #region BusinessOrder
        public bool SaveBusinessOrder(BusinessOrder businessOrder)
        {
            _boRepo.Create(businessOrder);
            return true;
        }

        public bool DeleteBusinessOrder(BusinessOrder businessOrder)
        {
            _boRepo.Delete(businessOrder);
            return true;
        }

        public BusinessOrder GetBusinessOrder(ObjectId id)
        {
            return _boRepo.Get(id);
        }

        public BusinessOrder GetBusinessOrder(string businessOrderId)
        {
            return _boRepo.Get(x => x.BusinessOrderId == businessOrderId);
        }

        public BusinessOrder GetBusinessOrderBySellerGuid(Guid sellerGuid)
        {
            return _boRepo.Fetch(x => x.SellerGuid == sellerGuid).OrderByDescending(x => x.ModifyTime).FirstOrDefault();
        }

        public BusinessOrder GetBusinessOrderByBusinessHourGuid(Guid businessHourGuid)
        {
            return _boRepo.Fetch(x => x.BusinessHourGuid == businessHourGuid).OrderByDescending(x => x.ModifyTime).FirstOrDefault();
        }

        public IEnumerable<BusinessOrder> GetBusinessOrderByEmail(string userName)
        {
            return _boRepo.Fetch(x => x.CreateId == userName);
        }

        public IEnumerable<BusinessOrder> GetBusinessOrders()
        {
            return _boRepo.Table.AsEnumerable();
        }

        public IEnumerable<BusinessOrder> GetBusinessOrdersByDate(DateTime d1,DateTime d2)
        {
            return _boRepo.Fetch(x => x.BusinessHourTimeS >= d1 && x.BusinessHourTimeS <= d2);
        }

        /// <summary>
        /// 依據傳入條件查詢業務工單資料
        /// </summary>
        /// <param name="query"></param>
        /// <param name="order"></param>
        /// <param name="pageSize">每頁資料筆數</param>
        /// <param name="pageNum">第幾頁</param>
        /// <returns></returns>
        public IEnumerable<BusinessOrder> BusinessOrderGetListByPage(IMongoQuery query, IMongoSortBy sortBy, int pageSize, int pageNum)
        {
            return _boRepo.Find(query, sortBy, pageSize, pageNum);
        }

        public IEnumerable<BusinessOrder> BusinessOrderGetList(IMongoQuery query)
        {
            return _boRepo.Find(query);
        }

        public IEnumerable<BusinessOrder> BusinessOrderGetListByPage(Expression<Func<BusinessOrder, bool>> exp, Action<Orderable<BusinessOrder>> sortBy, int pageSize, int pageNum)
        {
            return _boRepo.Fetch(exp, sortBy, pageSize, pageNum);
        }

        public int BusinessOrderGetListCount(Expression<Func<BusinessOrder, bool>> exp)
        {
            return _boRepo.Count(exp);
        }

        #endregion

        #region Counter
        public Counter GetCounter(CounterType type)
        {
            return _countRepo.Fetch(x => x.Type == type).FirstOrDefault();
        }
        public int GetSequenceNumber(CounterType type)
        {
            Counter counter = GetCounter(type);
            if (counter != null)
            {
                _countRepo.FindAndModify(Query.EQ("Type", type), Update.Inc("IdentifyId", 1));
                return GetCounter(type).IdentifyId;
            }
            else
            {
                Counter newCounter = new Counter();
                newCounter.Type = type;
                _countRepo.Create(newCounter);

                return GetSequenceNumber(type);
            }
        }
        #endregion

        #region SalesMember
        public bool SaveSalesMember(SalesMember salesMember)
        {
            _mRepo.Create(salesMember);
            return true;
        }

        /// <summary>
        /// 取得僅具有一區審核權限的主管，若有多區，取權限較小者
        /// </summary>
        /// <param name="dept"></param>
        /// <returns></returns>
        public SalesMember GetSalesMemberByDept(string dept)
        {
            IEnumerable<SalesMember> salesMemberList = _mRepo.Fetch(x => x.Area.Contains(dept));
            if (salesMemberList.Count() > 1)
                return salesMemberList.OrderBy(x => x.Area.Split(',').Count()).FirstOrDefault();
            else if (salesMemberList.Count().Equals(1))
                return salesMemberList.FirstOrDefault();
            else
                return null;
        }

        public SalesMember GetSalesMember(string salesId)
        {
            return _mRepo.Fetch(x => x.SalesId == salesId.ToLower()).FirstOrDefault();
        }

        public List<SalesMember> GetSalesMemberList(Level level)
        {
            return _mRepo.Fetch(x => x.Level == level).ToList();
        }

        public List<SalesMember> GetSalesMemberList(Level level, string userId)
        {
            return _mRepo.Fetch(x => x.Level == level && x.SalesId == userId.ToLower()).ToList();
        }

        public bool DeleteSalesMember(SalesMember salesMember)
        {
            _mRepo.Delete(salesMember);
            return true;
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.BizLogic.Model;
using MongoDB.Driver;
using Vodka.Mongo;
using MongoDB.Driver.Builders;
using MongoDB.Bson;

namespace LunchKingSite.Mongo.Services
{
    public class QueueingUserService
    {
        private IMongoRepository<QueueingUser> _userRepo;

        public QueueingUserService()
        {
        }

        public QueueingUserService(IMongoRepository<QueueingUser> userRepo)
        {
            _userRepo = userRepo;
        }

        public QueueingUser GetUserInQueue(int eventId, string userName, DateTime queueTimeLowBound, DateTime queueTimeHighBound)
        {
            return _userRepo.Get(x => x.EventId == eventId && x.UserName == userName && queueTimeLowBound <= x.EnqueueTime && x.EnqueueTime < queueTimeHighBound);
        }

        public IEnumerable<QueueingUser> GetAllWinners(DateTime d,int eventId)
        {
            return _userRepo.Fetch(x => x.EventId == eventId && x.EnqueueTime <= d.AddDays(1) && x.EnqueueTime > d);
        }

        public int GetQueuedUserCount(int eventId, DateTime queueTimeLowBound, DateTime queueTimeHighBound)
        {
            return _userRepo.Count(x => x.EventId == eventId && queueTimeLowBound <= x.EnqueueTime && x.EnqueueTime < queueTimeHighBound);
        }

        public IEnumerable<QueueingUser> GetUserQueueingByDate(int eventId, DateTime queueTimeLowBound, DateTime queueTimeHighBound, int num)
        {
            return _userRepo.Fetch(x => x.EventId == eventId && queueTimeLowBound <= x.EnqueueTime && x.EnqueueTime < queueTimeHighBound && x.Rank == num);
        }

        public IEnumerable<QueueingUser> GetUserQueueingHistory(int eventId, string userName)
        {
            return _userRepo.Fetch(x => x.EventId == eventId && x.UserName == userName, y => y.Asc(x => x.EnqueueTime));
        }

        public IEnumerable<QueueingUser> GetUserQueueingByPromoItemId(int promoItemId)
        {
            return _userRepo.Fetch(x => x.PromoItemId == promoItemId);
        }

        public IEnumerable<QueueingUser> GetUserQueueingByDate(DateTime queueTimeLowBound, DateTime queueTimeHighBound)
        {
            //List<QueryComplete> queryList = new List<QueryComplete>();
            List<IMongoQuery> queryList = new List<IMongoQuery>();
            queryList.Add(Query.And(
                Query.GTE("EnqueueTime", new BsonDateTime(queueTimeLowBound)), 
                Query.LT("EnqueueTime", new BsonDateTime(queueTimeHighBound))));
            queryList.Add(Query.Or(Query.EQ("IsReplicate", MongoDB.Bson.BsonNull.Value), Query.EQ("IsReplicate", false)));

            return _userRepo.Find(Query.And(queryList.ToArray()));
        }

        public bool SaveQueueingUser(QueueingUser user)
        {
            _userRepo.Create(user);
            return true;
        }

        public void UpdateQueueingUser(IEnumerable<QueueingUser> users)
        {
            foreach (var user in users)
            {
                try
                {
                    user.Choosen = true;
                    _userRepo.Update(user);
                }
                catch { }
            }
        }

        public void Test(DateTime d)
        {
           var v= _userRepo.Table.First(x => x.UserName == "super6210056@hotmail.com"&& x.EventId==4);
           if (v != null)
           {
               v.EnqueueTime = d;
               _userRepo.Update(v);
           }
        }
    }
}

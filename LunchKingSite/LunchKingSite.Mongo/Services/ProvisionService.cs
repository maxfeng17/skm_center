﻿using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Model;
using MongoDB.Bson;
using Vodka.Mongo;

namespace LunchKingSite.Mongo.Services
{
    public class ProvisionService
    {
        private IMongoRepository<ProvisionDepartment> _deptRepo;
        private IMongoRepository<ProvisionCategory> _cateRepo;
        private IMongoRepository<ProvisionItem> _itemRepo;
        private IMongoRepository<ProvisionContent> _contentRepo;
        private IMongoRepository<ProvisionDesc> _descRepo;
        public ProvisionService(IMongoRepository<ProvisionDepartment> deptRepo, IMongoRepository<ProvisionCategory> cateRepo, IMongoRepository<ProvisionItem> itemRepo, IMongoRepository<ProvisionContent> contentRepo, IMongoRepository<ProvisionDesc> descRepo)
        {
            _deptRepo = deptRepo;
            _cateRepo = cateRepo;
            _itemRepo = itemRepo;
            _contentRepo = contentRepo;
            _descRepo = descRepo;
        }

        #region ProvisionDepartment
        public bool SaveProvisionDepartment(ProvisionDepartment department)
        {
            _deptRepo.Create(department);
            return true;
        }

        public bool DeleteProvisionDepartment(ProvisionDepartment department)
        {
            _deptRepo.Delete(department);
            return true;
        }

        public ProvisionDepartment GetProvisionDepartment(ObjectId id)
        {
            return _deptRepo.Get(id);
        }

        public IEnumerable<ProvisionDepartment> GetProvisionDepartments()
        {
            return _deptRepo.Table.AsEnumerable();
        }
        #endregion

        #region ProvisionCategory
        public bool SaveProvisionCategory(ProvisionCategory category)
        {
            _cateRepo.Create(category);
            return true;
        }

        public bool DeleteProvisionCategory(ProvisionCategory category)
        {
            _cateRepo.Delete(category);
            return true;
        }

        public ProvisionCategory GetProvisionCategory(ObjectId id)
        {
            return _cateRepo.Get(id);
        }
        #endregion

        #region ProvisionItem
        public bool SaveProvisionItem(ProvisionItem item)
        {
            _itemRepo.Create(item);
            return true;
        }

        public bool DeleteProvisionItem(ProvisionItem item)
        {
            _itemRepo.Delete(item);
            return true;
        }

        public ProvisionItem GetProvisionItem(ObjectId id)
        {
            return _itemRepo.Get(id);
        }
        #endregion

        #region ProvisionContent
        public bool SaveProvisionContent(ProvisionContent content)
        {
            _contentRepo.Create(content);
            return true;
        }

        public bool DeleteProvisionContent(ProvisionContent content)
        {
            _contentRepo.Delete(content);
            return true;
        }

        public ProvisionContent GetProvisionContent(ObjectId id)
        {
            return _contentRepo.Get(id);
        }
        #endregion

        #region ProvisionDesc
        public bool SaveProvisionDesc(ProvisionDesc desc)
        {
            _descRepo.Create(desc);
            return true;
        }

        public bool DeleteProvisionDesc(ProvisionDesc desc)
        {
            _descRepo.Delete(desc);
            return true;
        }

        public ProvisionDesc GetProvisionDesc(ObjectId id)
        {
            return _descRepo.Get(id);
        }
        #endregion
    }
}

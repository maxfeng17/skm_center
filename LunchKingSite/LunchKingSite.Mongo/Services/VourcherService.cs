﻿using LunchKingSite.BizLogic.Model.Vourcher;
using Vodka.Mongo;

namespace LunchKingSite.Mongo.Services
{
    public class VourcherService
    {
        private IMongoRepository<VourcherSellerDocument> _sellerRepo;

        public VourcherService(IMongoRepository<VourcherSellerDocument> sellerRepo)
        {
            _sellerRepo = sellerRepo;
        }

        #region VourcherSellerDocument
        public bool SaveVourcherSellerDocument(VourcherSellerDocument document)
        {
            
            _sellerRepo.Create(document);
            return true;
        }
        #endregion VourcherSellerDocument
    }
}

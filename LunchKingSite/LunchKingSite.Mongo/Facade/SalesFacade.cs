﻿using System.Collections.Generic;
using System.Linq;
using Autofac;
using LunchKingSite.BizLogic.Model.Sales;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.Mongo.Services;
using LunchKingSite.BizLogic.Component;
using System;
using System.Data;
using MongoDB.Bson;

namespace LunchKingSite.Mongo.Facade
{
    public class SalesFacade
    {
        private static SalesService _salServ;
        private static IOrderProvider _ordProv;
        private static IHumanProvider _humProv;
        private static ISystemProvider _sysProv;
        private static IPponProvider _pp;
        static SalesFacade()
        {
            _salServ = CoreModule.Container.Resolve<SalesService>();
            _ordProv = ProviderFactory.Instance().GetProvider<IOrderProvider>();
            _humProv = ProviderFactory.Instance().GetProvider<IHumanProvider>();
            _sysProv = ProviderFactory.Instance().GetProvider<ISystemProvider>();
            _pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        }

        public static void TransferBusinessOrder(BusinessOrder businessOrder)
        {
            // 出帳方式 建立預設值
            if (businessOrder.BusinessType.Equals(BusinessType.Ppon))
            {
                if (businessOrder.IsTravelType)
                    // 旅遊(預設 人工每月出帳)
                    businessOrder.RemittancePaidType = LunchKingSite.Core.RemittanceType.ManualMonthly;
                else
                    // 好康(預設 ACH每週出帳)
                    businessOrder.RemittancePaidType = LunchKingSite.Core.RemittanceType.AchWeekly;
            }
            else if (businessOrder.BusinessType.Equals(BusinessType.Product))
                // 商品(預設 商品(暫付70%))
                businessOrder.RemittancePaidType = LunchKingSite.Core.RemittanceType.ManualPartially;

            _salServ.SaveBusinessOrder(businessOrder);
        }

        public static void TransferAllBusinessOrder()
        {
            var allBusinessOrder = _salServ.GetBusinessOrders();
            foreach (BusinessOrder businessOrder in allBusinessOrder)
            {
                TransferBusinessOrder(businessOrder);
            }
        }

        public static void UpdateBusinessOrderContractStatus()
        {
            var allBusinessOrder = _salServ.GetBusinessOrders();
            foreach (BusinessOrder item in allBusinessOrder)
            {
                item.IsContractSend = item.Status.Equals(BusinessOrderStatus.Create);
                _salServ.SaveBusinessOrder(item);
            }
        }

        public static List<string> GetFilterDataList(int month)
        {
            List<string> dataList = new List<string>();
            // 工單編號,賣家名稱,業務,業務部門,上檔日期,建立日期,送審天數,核准天數,複查天數,排檔天數,建檔天數, 草稿到建檔總計天數
            List<BusinessOrder> businessOrderList = _salServ.GetBusinessOrders().Where(x => x.Status > BusinessOrderStatus.Temp && x.ModifyTime != null && x.ModifyTime.Value.ToLocalTime() >= DateTime.Now.AddMonths(-month)).ToList();
            foreach (BusinessOrder item in businessOrderList)
            {
                History send = item.HistoryList.Where(x => x.Changeset.Contains("更改狀態為\"送審（業務主管）\"")).LastOrDefault();
                History approve = item.HistoryList.Where(x => x.Changeset.Contains("更改狀態為\"核准（各區營管）\"")).LastOrDefault();
                History review = item.HistoryList.Where(x => x.Changeset.Contains("更改狀態為\"已複查（營管）\"")).LastOrDefault();
                History await = item.HistoryList.Where(x => x.Changeset.Contains("更改狀態為\"已排檔（創意編輯）\"")).LastOrDefault();
                History created = item.HistoryList.Where(x => x.Changeset.Contains("更改狀態為\"已建檔（完成）\"")).LastOrDefault();

                if (item.Status == BusinessOrderStatus.Create)
                {
                    // 從草稿到建檔各階段所花費的時間(天)
                    dataList.Add(string.Join(",", item.BusinessOrderId, item.SellerName, item.SalesName, item.SalesDeptId
                        , item.BusinessHourTimeS, item.CreateTime
                        , (send.ModifyTime - item.CreateTime).Days
                        , (approve.ModifyTime - send.ModifyTime).Days
                        , (review.ModifyTime - approve.ModifyTime).Days
                        , (await.ModifyTime - review.ModifyTime).Days
                        , (created.ModifyTime - await.ModifyTime).Days
                        , (created.ModifyTime - item.CreateTime).Days));
                }
            }
            return dataList;
        }

        public static List<BusinessOrder> GetBusinessOrderList(DateTime d1,DateTime d2)
        {
            return _salServ.GetBusinessOrdersByDate(d1, d2).ToList();
        }

        public static AccBusinessGroupCollection AccBusinessGroupGetList()
        {
            return _sysProv.AccBusinessGroupGetList();
        }

        public static DepartmentCollection DepartmentGetList()
        {
            return _humProv.DepartmentGetListBySalesAndMarketing(true);
        }

        public static DataTable GetRemittanceTypeCheckReport()
        {
            return _pp.RemittanceTypeCheck();
        }

        public static int ReferredAllBusinessOrderByEmail(string usename, string referredEmail)
        {
            int referredCount = 0;
            IEnumerable<BusinessOrder> businessOrders = _salServ.GetBusinessOrderByEmail(usename);
            foreach (BusinessOrder order in businessOrders)
            {
                order.CreateId = referredEmail.ToLower();
                order.HistoryList.Add(new History() { Id = ObjectId.GenerateNewId(), Changeset = "轉件", ModifyId = referredEmail, ModifyTime = DateTime.Now });
                _salServ.SaveBusinessOrder(order);
                referredCount++;
            }
            return referredCount;
        }

        public static Dictionary<string, string> GetBusinessOrderDictionary(BusinessOrder businessOrder, SalesStore salesStore, BusinessOrderDownloadType downloadType, bool isPrintMemo = false)
        {
            string newLine = string.Empty;
            string indentation = string.Empty;
            switch (downloadType)
            {
                case BusinessOrderDownloadType.Word:
                    newLine = "\n";
                    indentation = "\t";
                    break;
                case BusinessOrderDownloadType.Pdf:
                    newLine = @"<br />";
                    indentation = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    break;
                default:
                    break;
            }

            Dictionary<string, string> dataList = new Dictionary<string, string>();
            if (salesStore != null && businessOrder.BusinessType.Equals(BusinessType.Ppon))
            {
                #region 分店資料套入

                dataList.Add("SellerName", string.Format("{0}_{1}", businessOrder.SellerName, salesStore.BranchName));
                dataList.Add("CompanyName", salesStore.StoreName);
                dataList.Add("SignCompanyId", salesStore.StoreSignCompanyID);
                dataList.Add("BossName", salesStore.StoreBossName);
                dataList.Add("ContactPerson", salesStore.StoreBossName);
                dataList.Add("BossTel", salesStore.StoreTel);
                dataList.Add("BossPhone", salesStore.StoreTel);
                dataList.Add("BossEmail", salesStore.StoreEmail);
                dataList.Add("BossFax", string.Empty);
                dataList.Add("BossAddress", salesStore.StoreCompanyAddress);
                dataList.Add("StoreName", salesStore.BranchName);
                dataList.Add("SellerAddress", CityManager.CityTownShopStringGet(salesStore.AddressTownshipId) + salesStore.StoreAddress);
                dataList.Add("OpeningTime", salesStore.OpeningTime);
                dataList.Add("CloseDate", salesStore.CloseDate);
                string vehicleInfo = string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(salesStore.Mrt) ? "捷運：" + salesStore.Mrt + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(salesStore.Car) ? "開車：" + salesStore.Car + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(salesStore.Bus) ? "公車：" + salesStore.Bus + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(salesStore.OV) ? "其他：" + salesStore.OV : string.Empty;
                dataList.Add("VehicleInfo", vehicleInfo);
                string webUrl = string.Empty;
                webUrl += !string.IsNullOrEmpty(salesStore.WebUrl) ? "官網網址：" + salesStore.WebUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(salesStore.FBUrl) ? "FaceBook網址：" + salesStore.FBUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(salesStore.PlurkUrl) ? "Plurk網址：" + salesStore.PlurkUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(salesStore.BlogUrl) ? "部落格網址：" + salesStore.BlogUrl : string.Empty;
                webUrl += !string.IsNullOrEmpty(salesStore.OtherUrl) ? "其他網址：" + salesStore.OtherUrl : string.Empty;
                dataList.Add("WebUrl", webUrl);

                dataList.Add("AccountingName", salesStore.StoreAccountingName);
                dataList.Add("AccountingTel", salesStore.StoreAccountingTel);
                dataList.Add("AccountingEmail", salesStore.StoreEmail);
                dataList.Add("AcctBankId", salesStore.StoreBankCode);
                dataList.Add("AcctBankDesc", salesStore.StoreBankDesc);
                dataList.Add("AcctBranchId", salesStore.StoreBranchCode);
                dataList.Add("AcctBranchDesc", salesStore.StoreBranchDesc);
                dataList.Add("AccountingId", salesStore.StoreID);
                dataList.Add("AccountingText", salesStore.StoreAccountName);
                dataList.Add("AccountText", salesStore.StoreAccount);
                dataList.Add("OtherMemo", !string.IsNullOrEmpty(businessOrder.OtherMemo) ? businessOrder.OtherMemo.Replace(System.Environment.NewLine, newLine) + newLine + (!string.IsNullOrEmpty(salesStore.StoreNotice) ? salesStore.StoreNotice : string.Empty) : string.Empty);

                #endregion
            }
            else
            {
                #region 帶入總店資料

                dataList.Add("SellerName", businessOrder.SellerName);
                dataList.Add("CompanyName", businessOrder.CompanyName);
                dataList.Add("SignCompanyId", businessOrder.IsPrivateContract ? businessOrder.PersonalId : businessOrder.GuiNumber);
                dataList.Add("BossName", businessOrder.BossName);
                dataList.Add("ContactPerson", businessOrder.ContactPerson);
                dataList.Add("BossTel", businessOrder.BossTel);
                dataList.Add("BossPhone", businessOrder.BossPhone);
                dataList.Add("BossEmail", businessOrder.BossEmail);
                dataList.Add("BossFax", businessOrder.BossFax);
                dataList.Add("BossAddress", businessOrder.BossAddress);
                dataList.Add("StoreName", businessOrder.StoreName);
                dataList.Add("SellerAddress", CityManager.CityTownShopStringGet(businessOrder.AddressTownshipId) + businessOrder.SellerAddress);
                dataList.Add("OpeningTime", businessOrder.OpeningTime);
                dataList.Add("CloseDate", !string.IsNullOrEmpty(businessOrder.CloseDate) ? businessOrder.CloseDate : "無");
                string vehicleInfo = string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(businessOrder.Mrt) ? "捷運：" + businessOrder.Mrt + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(businessOrder.Car) ? "開車：" + businessOrder.Car + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(businessOrder.Bus) ? "公車：" + businessOrder.Bus + newLine : string.Empty;
                vehicleInfo += !string.IsNullOrEmpty(businessOrder.VehicleInfo) ? "其他：" + businessOrder.VehicleInfo : string.Empty;
                dataList.Add("VehicleInfo", newLine + vehicleInfo.TrimEnd(newLine.ToCharArray()));
                string webUrl = string.Empty;
                webUrl += !string.IsNullOrEmpty(businessOrder.WebUrl) ? "官網網址：" + businessOrder.WebUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(businessOrder.FBUrl) ? "FaceBook網址：" + businessOrder.FBUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(businessOrder.PlurkUrl) ? "Plurk網址：" + businessOrder.PlurkUrl + newLine : string.Empty;
                webUrl += !string.IsNullOrEmpty(businessOrder.BlogUrl) ? "部落格網址：" + businessOrder.BlogUrl : string.Empty;
                webUrl += !string.IsNullOrEmpty(businessOrder.OtherUrl) ? "其他網址：" + businessOrder.OtherUrl : string.Empty;
                dataList.Add("WebUrl", newLine + webUrl.TrimEnd(newLine.ToCharArray()));

                dataList.Add("AccountingName", businessOrder.AccountingName);
                dataList.Add("AccountingTel", businessOrder.AccountingTel);
                dataList.Add("AccountingEmail", businessOrder.AccountingEmail);
                dataList.Add("AcctBankId", businessOrder.AccountingBankId);
                dataList.Add("AcctBankDesc", businessOrder.AccountingBank);
                dataList.Add("AcctBranchId", businessOrder.AccountingBranchId);
                dataList.Add("AcctBranchDesc", businessOrder.AccountingBranch);
                dataList.Add("AccountingId", businessOrder.AccountingId);
                dataList.Add("AccountingText", businessOrder.Accounting);
                dataList.Add("AccountText", businessOrder.Account);
                dataList.Add("OtherMemo", !string.IsNullOrEmpty(businessOrder.OtherMemo) ? businessOrder.OtherMemo.Replace(System.Environment.NewLine, newLine) + newLine : string.Empty);

                #endregion
            }

            #region Part1 基本資料

            dataList.Add("BusinessType", Helper.GetLocalizedEnum(businessOrder.BusinessType));
            // 核銷方式
            AccBusinessGroupCollection abgc = _sysProv.AccBusinessGroupGetList();
            AccBusinessGroup piinlifeAbg = abgc.Where(x => x.AccBusinessGroupName.Equals("品生活", StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            string verifyType = Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, businessOrder.PponVerifyType);
            switch (businessOrder.PponVerifyType)
            {
                case PponVerifyType.OnlineUse:
                    verifyType += newLine + I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineUse;
                    break;

                case PponVerifyType.InventoryOnly:
                    verifyType += newLine + I18N.Message.SalesBusinessOrderPponVerifyTypeInventoryOnly;
                    break;

                case PponVerifyType.OnlineOnly:
                default:
                    if (piinlifeAbg != null && businessOrder.AccBusinessGroupId == piinlifeAbg.AccBusinessGroupId.ToString())
                    {
                        verifyType += newLine + I18N.Message.SalesBusinessOrderPponVerifyTypePiinlifeOnlineOnly;
                    }
                    else
                    {
                        verifyType += newLine + I18N.Message.SalesBusinessOrderPponVerifyTypeOnlineOnly;
                    }
                    break;
            }
            dataList.Add("VerifyType", verifyType);
            dataList.Add("IsEDC ", businessOrder.IsEDC ? "是" : "否");
            dataList.Add("IsKownEDC", businessOrder.IsKownEDC ? "有(請續填2)" : "無");
            dataList.Add("IsKownTaiShinEDC", businessOrder.IsKownTaiShinEDC ? "是(請續填2-1)" : "否");
            dataList.Add("EdcDesc1", businessOrder.EdcDesc1);
            dataList.Add("EdcDesc2", businessOrder.EdcDesc2);
            dataList.Add("EdcDesc3", businessOrder.EdcDesc3);
            dataList.Add("EdcDesc4", businessOrder.EdcDesc4);
            dataList.Add("EdcDesc5", businessOrder.EdcDesc5);
            dataList.Add("ReturnedPerson", businessOrder.ReturnedPerson);
            dataList.Add("ReturnedTel", businessOrder.ReturnedTel);
            dataList.Add("ReturnedEmail", businessOrder.ReturnedEmail);
            string hotelInfo = string.Empty;
            if (businessOrder.IsTravelType)
            {
                hotelInfo += string.Format("民宿業者須註明以下資訊：" + newLine + "1. 民宿登記證字號：{0}  2. 所在地警察局電話：{1}", businessOrder.HotelId, businessOrder.PoliceTel);
            }
            dataList.Add("HotelInfo", hotelInfo);
            dataList.Add("DealAccountingPayType", Helper.GetLocalizedEnum(businessOrder.PayToCompany));
            string invoice = string.Empty;
            switch (businessOrder.Invoice.Type)
            {
                case InvoiceType.DuplicateUniformInvoice:
                    BusinessOrderInvoiceTax inTax = BusinessOrderInvoiceTax.TryParse(businessOrder.Invoice.InTax, out inTax) ? inTax : BusinessOrderInvoiceTax.In5Tax;
                    invoice = Helper.GetLocalizedEnum(businessOrder.Invoice.Type) + "：" + Helper.GetLocalizedEnum(inTax);
                    break;
                case InvoiceType.Other:
                    invoice = Helper.GetLocalizedEnum(businessOrder.Invoice.Type) + "：" + businessOrder.Invoice.Name;
                    break;
                case InvoiceType.General:
                case InvoiceType.NonInvoice:
                case InvoiceType.EntertainmentTax:
                    invoice = Helper.GetLocalizedEnum(businessOrder.Invoice.Type);
                    break;
                default:
                    break;
            }
            dataList.Add("Invoice", invoice);
            BusinessOrderMultiDeal multiDeal = BusinessOrderMultiDeal.TryParse(businessOrder.MultiDeal, out multiDeal) ? multiDeal : BusinessOrderMultiDeal.SinglePriceSingleOption;
            dataList.Add("MultiDeal", Helper.GetLocalizedEnum(multiDeal));
            dataList.Add("HotelReservationTel", businessOrder.HotelReservationTel);
            dataList.Add("ReservationTel", businessOrder.ReservationTel);

            dataList.Add("IsShowSpecailStore", businessOrder.IsShowSpecailStore ? "特選店家不露出" : string.Empty);
            string provisionList = string.Empty;
            int index = 1;
            foreach (var provi in businessOrder.ProvisionList)
            {
                provisionList += "(" + index + "). " + provi.Value + newLine;
                index++;
            }
            dataList.Add("ProvisionList", provisionList.TrimEnd(newLine.ToCharArray()));

            #endregion Part1 基本資料

            #region Part2 好康優惠

            string dealDesc = string.Empty;
            string[] paragraph = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
            for (int i = 0; i < businessOrder.Deals.Count; i++)
            {
                if (!string.IsNullOrEmpty(businessOrder.Deals[i].PurchasePrice) && !businessOrder.Deals[i].PurchasePrice.Equals("0") || (!string.IsNullOrEmpty(businessOrder.Deals[i].MinQuentity) && !businessOrder.Deals[i].MinQuentity.Equals("0")) || (!string.IsNullOrEmpty(businessOrder.Deals[i].MaxQuentity) && !businessOrder.Deals[i].MaxQuentity.Equals("0")))
                {
                    dealDesc += string.Format("{0}.1　", paragraph[i]);
                    if (!string.IsNullOrEmpty(businessOrder.Deals[i].ItemPrice) && !string.IsNullOrEmpty(businessOrder.Deals[i].OrignPrice))
                    {
                        dealDesc += string.Format("賣價${0}{1}；原價${2}；", businessOrder.Deals[i].ItemPrice, businessOrder.Deals[i].IsIncludeFreight ? " (此價格已含運)" : string.Empty, businessOrder.Deals[i].OrignPrice);
                    }

                    dealDesc += string.Format("優惠活動：{0}", businessOrder.Deals[i].ItemName);
                    if (!string.IsNullOrEmpty(businessOrder.Deals[i].MultOption) || businessOrder.Deals[i].IsReferenceAttachment)
                    {
                        dealDesc += string.Format(newLine + "款式選項：{0}", businessOrder.Deals[i].IsReferenceAttachment ? "參見附件" : businessOrder.Deals[i].MultOption.Replace(System.Environment.NewLine, newLine).TrimEnd(newLine.ToCharArray()));
                    }

                    dealDesc += string.Format(newLine + "{0}.2　最高購買數量{1}單位；每人限購{2}單位；", paragraph[i], businessOrder.Deals[i].MaxQuentity, businessOrder.Deals[i].LimitPerQuentity);
                    if (isPrintMemo && businessOrder.Deals[i].IsATM)
                    {
                        dealDesc += string.Format("ATM數量{0}單位", businessOrder.Deals[i].AtmQuantity);
                    }
                    dealDesc += string.Format(newLine + "{0}.3　上架費份數：{1}；進貨價格：{2}(含稅)", paragraph[i], businessOrder.Deals[i].SlottingFeeQuantity, businessOrder.Deals[i].PurchasePrice);
                    if (businessOrder.IsTravelType && !businessOrder.Deals[i].Commission.Equals(0))
                    {
                        dealDesc += string.Format("；發票對開，佣金為：${0}", businessOrder.Deals[i].Commission);
                    }

                    if (businessOrder.BusinessType.Equals(BusinessType.Product) && !businessOrder.Deals[i].IsIncludeFreight)
                    {
                        dealDesc += string.Format(newLine + "{0}.4　運費：{1}元整", paragraph[i], businessOrder.Deals[i].Freight);
                        if (!string.IsNullOrEmpty(businessOrder.Deals[i].NonFreightLimit))
                        {
                            dealDesc += string.Format("；{0}份免運", businessOrder.Deals[i].NonFreightLimit);
                        }

                        dealDesc += string.Format(newLine + indentation + "貨到付運費：{0}；配送外島：{1}", businessOrder.Deals[i].IsFreightToCollect ? "是" : "否", businessOrder.Deals[i].IsNotDeliveryIslands ? "否" : "是");
                    }
                    else if (businessOrder.BusinessType.Equals(BusinessType.Ppon))
                    {
                        string startUnit = string.Empty;
                        switch (businessOrder.Deals[i].StartUseUnit)
                        {
                            case StartUseUnitType.Day:
                                startUnit = "天";
                                break;

                            case StartUseUnitType.Month:
                                startUnit = "月";
                                break;

                            case StartUseUnitType.BeforeDay:
                                startUnit = "前";
                                break;

                            default:
                                break;
                        }
                        dealDesc += string.Format(newLine + "{0}.4　活動使用有效時間：{1}{2}活動使用有效時間為活動開始兌換後{3}{4}皆可使用", paragraph[i], newLine, indentation, businessOrder.Deals[i].StartMonth, startUnit);
                        if (businessOrder.Deals[i].IsRestrictionRule)
                        {
                            dealDesc += string.Format(newLine + "{0}特殊使用日期限制：{1}", indentation, businessOrder.Deals[i].RestrictionRuleDesc);
                        }
                    }
                    dealDesc += newLine + newLine;
                }
            }
            dataList.Add("DealDesc", dealDesc);

            // 商品相關資訊
            if (businessOrder.BusinessType.Equals(BusinessType.Product))
            {
                dataList.Add("IsParallelProduct", businessOrder.IsParallelProduct ? "是" : "否");
                dataList.Add("IsExpiringItems", businessOrder.IsExpiringItems ? "是" : "否");
                dataList.Add("ExpiringItemsDate", businessOrder.IsExpiringItems ? string.Format("（有效期限：{0} ~ {1}）", businessOrder.ExpiringItemsDateStart, businessOrder.ExpiringItemsDateEnd) : string.Empty);
                dataList.Add("ProductPreservation", !string.IsNullOrEmpty(businessOrder.ProductPreservation) ? businessOrder.ProductPreservation.Replace(System.Environment.NewLine, newLine) : "無");
                dataList.Add("ProductUse", !string.IsNullOrEmpty(businessOrder.ProductUse) ? businessOrder.ProductUse.Replace(System.Environment.NewLine, newLine) : "無");
                dataList.Add("ProductSpec", !string.IsNullOrEmpty(businessOrder.ProductSpec) ? businessOrder.ProductSpec.Replace(System.Environment.NewLine, newLine) : "無");
                dataList.Add("ProductIngredients", !string.IsNullOrEmpty(businessOrder.ProductIngredients) ? businessOrder.ProductIngredients.Replace(System.Environment.NewLine, newLine) : "無");
                dataList.Add("SanctionNumber", !string.IsNullOrEmpty(businessOrder.SanctionNumber) ? businessOrder.SanctionNumber.Replace(System.Environment.NewLine, newLine) : "無");
                string inspectRule = string.Empty;
                for (int i = 0; i < businessOrder.InspectRule.Split(',').Length; i++)
                {
                    BusinessOrderInspectRule rule;
                    if (BusinessOrderInspectRule.TryParse(businessOrder.InspectRule.Split(',')[i], out rule))
                    {
                        inspectRule += Helper.GetLocalizedEnum(rule) + ",";
                    }
                }
                dataList.Add("InspectRule", !string.IsNullOrWhiteSpace(inspectRule) ? inspectRule.TrimEnd(',') : "無");

                string businessOrderUseTimeSet = string.Empty;
                List<string> bList = new List<string>();
                bList.Add("商品將依照訂單出貨，於結檔後開始陸續出貨。");
                if (piinlifeAbg != null && businessOrder.AccBusinessGroupId == piinlifeAbg.AccBusinessGroupId.ToString())
                {
                    bList.Add(I18N.Message.SalesBusinessOrderPiinlifeDeliveryDesc);
                }
                else if ((int)businessOrder.ShipType == (int)DealLabelSystemCode.ArriveIn24Hrs)
                {
                    bList.Add("僅限台灣本島地區適用24H出貨條款，離島及部分偏遠地區不包含在內。");
                    bList.Add("週休假日或例假日成立之訂單，均視為次一工作日之訂單。");
                    bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                    bList.Add("供應商同意支付本平台因未於24小時完成出貨之懲罰性違約金以彌補消費者損失及本平台商譽,本平台得直接於供應商之貨款中扣除,違約金計算方式依每一訂單金額計算:訂單1000元以下違約金新台幣50元整;訂單1000元以上違約金100元整。");
                    bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受前條懲罰性違約金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                }
                else if ((int)businessOrder.ShipType == (int)DealLabelSystemCode.ArriveIn72Hrs)
                {
                    bList.Add("僅限台灣本島地區適用24H出貨條款，離島地區不包含在內。");
                    bList.Add("週休假日或例假日成立之訂單，均視為次一工作日之訂單。");
                    bList.Add("此專案供應商每日應登入本平台商家系統回報出貨狀況並回填出貨單號。");
                    bList.Add("供應商同意支付本平台因未於24小時完成出貨之懲罰性違約金以彌補消費者損失及本平台商譽,本平台得直接於供應商之貨款中扣除,違約金計算方式依每一訂單金額計算:訂單1000元以下違約金新台幣50元整;訂單1000元以上違約金100元整。");
                    bList.Add("若因收貨人資訊不完整或臨時更改、因收貨人自身因素致無法收貨、遇颱風地震等天災、公共工程，或遇系統設備維護，倉儲調整、盤點等情況，致出貨時間將順延，將不受前條懲罰性違約金規範。惟供應商於遭遇前述情形時應提前盡速告知本平台，以便本平台通知收貨人，供應商並應盡快將商品配送到貨。");
                }
                else
                {
                    bList.Add((int)businessOrder.ShippingDate == (int)ShippingDateType.Normal
                        ? string.Format("{0}：上檔後+{1}個工作日；最晚出貨日：統一結檔後+{2}個工作日。", "一般出貨",
                            businessOrder.ProductUseDateStartSet, businessOrder.ProductUseDateEndSet)
                        : string.Format("{0}：訂單成立後{1}個工作日內出貨完畢，出貨後配送約2~3個工作日，恕無法指定送達時間。", "一般出貨",
                            businessOrder.ShippingDate));
                    bList.Add("週休假日或例假日成立之訂單，均視為次一工作天之訂單。");
                }

                bList.Add("鑑賞期：消費者收到貨品後七天內(含六日)。");
                bList.Add("商品退貨：於鑑賞期內完成退貨申請，鑑賞期後不再接受消費者退貨。");

                for (int i = 0; i < bList.Count; i++)
                {
                    businessOrderUseTimeSet += "(" + (i + 1) + ").　" + bList[i] + newLine;
                }
                dataList.Add("BusinessOrderUseTimeSet", businessOrderUseTimeSet);
                dataList.Add("ProductGuaranteeClauseDesc", I18N.Message.SalesBusinessOrderProductGuaranteeClauseDesc);
            }

            // 其他特殊設定
            string specialSetting = string.Empty;
            if (businessOrder.IsMohistVerify) // 墨攻
            {
                specialSetting += "配合墨攻核銷；";
            }
            else if (businessOrder.IsTrustHwatai) // 華泰
            {
                specialSetting += "信託華泰銀行；";
            }

            if (businessOrder.IsNotAccessSms)
            {
                specialSetting += "無法使用簡訊憑證；";
            }
            dataList.Add("SpecialSetting", !string.IsNullOrEmpty(specialSetting) ? specialSetting.TrimEnd('；') : "無");

            // 發票開立人
            string invoiceFounder = string.Empty;
            if (businessOrder.InvoiceFounderType != InvoiceFounderType.Company)
            {
                invoiceFounder += "發票開立人：" + newLine;
                invoiceFounder += "公司名稱：" + businessOrder.InvoiceFounderCompanyName + indentation;
                invoiceFounder += "統編 / ID：" + businessOrder.InvoiceFounderCompanyID + indentation;
                invoiceFounder += "負責人：" + businessOrder.InvoiceFounderBossName;
            }
            dataList.Add("FounderInfo", invoiceFounder);

            string othersInfo = "無";
            if (businessOrder.IsTravelType)
            {
                othersInfo = "◆ 此優惠房型皆由飯店視現場情況作調整及安排，恕不接受指定房型。";
            }
            dataList.Add("OthersInfo", othersInfo);

            #endregion Part2 好康優惠

            #region Part4 特店折扣

            if (!string.IsNullOrEmpty(businessOrder.PezStore))
            {
                dataList.Add("PezStores", businessOrder.PezStore);
                dataList.Add("PezStoreNotice", businessOrder.PezStoreNotice);
                DateTime dateStart = businessOrder.PezStoreDateStart ?? DateTime.MinValue;
                DateTime dateEnd = businessOrder.PezStoreDateEnd ?? DateTime.MinValue;
                if (businessOrder.PezStoreDateStart.HasValue && businessOrder.PezStoreDateEnd.HasValue)
                {
                    dataList.Add("PezStoreDate", businessOrder.PezStoreDateStart.Value.ToString("yyyy/MM/dd") + "～" + businessOrder.PezStoreDateEnd.Value.ToString("yyyy/MM/dd"));
                }
            }

            #endregion Part4 特店折扣

            #region 創編下載word需要加開的欄位

            string previewInfo = string.Empty;
            if (isPrintMemo)
            {
                string pictureSource = string.Empty;
                if (!string.IsNullOrWhiteSpace(businessOrder.PictureSource))
                {
                    for (int i = 0; i < businessOrder.PictureSource.Split(',').Length; i++)
                    {
                        PictureSourceType sourceType;
                        if (PictureSourceType.TryParse(businessOrder.PictureSource.Split(',')[i], out sourceType))
                        {
                            pictureSource += Helper.GetLocalizedEnum(sourceType) + ",";
                        }
                    }
                }

                // for sales preview
                previewInfo = string.Format("1.  文案重點1：{1}{0}2.  文案重點2：{2}{0}3.  文案重點3：{3}{0}4.  文案重點4：{4}{0}5.  出帳方式：{5}{0}6.  照片來源：{6}{0}7.  行銷活動：{7}",
                                                    newLine, businessOrder.Important1, businessOrder.Important2, businessOrder.Important3, businessOrder.Important4,
                                                    Helper.GetLocalizedEnum(businessOrder.RemittancePaidType), pictureSource.TrimEnd(','), businessOrder.MarketEvent);

            }
            dataList.Add("PreviewInfo", previewInfo);

            #endregion

            return dataList;
        }

    }
}

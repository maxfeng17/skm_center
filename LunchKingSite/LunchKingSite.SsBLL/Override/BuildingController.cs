using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Collections;
using SubSonic;
using LunchKingSite.SsBLL;

namespace LunchKingSite.DataOrm
{
    public partial class BuildingController
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(BuildingController));

        public static ViewBuildingCityCollection GetBuildingWithinZone(int zoneid, string prefix)
        {
            QueryCommand qc;
            if (zoneid < 0)
            {
                qc = new QueryCommand("select * from " + ViewBuildingCity.Schema.TableName + " where " + ViewBuildingCity.Columns.BuildingOnline + "=1 and (" + ViewBuildingCity.Columns.BuildingAddress + " like @prefix or " + ViewBuildingCity.Columns.BuildingName + " like @prefix) order by " + ViewBuildingCity.Columns.BuildingStreetName + "," + ViewBuildingCity.Columns.BuildingRank, ViewBuildingCity.Schema.Provider.Name);
                qc.AddParameter("@prefix", prefix + "%", DbType.String);
            }
            else
            {
                qc = new QueryCommand("select * from " + ViewBuildingCity.Schema.TableName + " where " + ViewBuildingCity.Columns.BuildingOnline + "=1 and " + ViewBuildingCity.Columns.CityId + " = @zone and (" + ViewBuildingCity.Columns.BuildingAddress + " like @prefix or " + ViewBuildingCity.Columns.BuildingName + " like @prefix) order by " + ViewBuildingCity.Columns.BuildingStreetName + "," + ViewBuildingCity.Columns.BuildingRank, ViewBuildingCity.Schema.Provider.Name);
                qc.AddParameter("@zone", zoneid, DbType.Int32);
                qc.AddParameter("@prefix", prefix + "%", DbType.String);
            }
            //log.Debug(qc.CommandSql);

            ViewBuildingCityCollection bc = new ViewBuildingCityCollection();
            
            IDataReader idr = DataService.GetReader(qc);

            bc.LoadAndCloseReader(idr);
            return bc;
        }
    }
}

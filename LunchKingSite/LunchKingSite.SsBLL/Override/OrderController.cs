using System;
using LunchKingSite.DataOrm;
using SubSonic;
using LunchKingSite.Core;

namespace LunchKingSite.SsBLL.Override
{
    public  class OrderController
    {
        [Obsolete]
        public static bool SetStatus(Guid orderGuid, string userName, OrderStatus status, bool setIt)
        {
            string sql = "update " + Order.Schema.Provider.DelimitDbName(Order.Schema.TableName) +
                " set " + Order.Columns.OrderStatus + " = (" + Order.Columns.OrderStatus;
            string userParam = null;
            if (setIt)
            {
                sql += "|" + status.ToString("d");
                userParam = userName;
            }
            else
            {
                /* sql int type is signed, so max of uint in .net is double of its max value in sql, 
                 * therefore we right shift 1 bit (= divide by 2) first and subtract OrderStatus.Locked to unmask it
                 */
                sql += "&cast(" + ((uint.MaxValue >> 1) - (uint)status).ToString() + " as int)";
            }
            sql += ")";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(userName))
            {
                qc.CommandSql += ", " + Order.Columns.AccessLock + "=@up";
                qc.AddParameter("@up", userParam, System.Data.DbType.String);
            }

            qc.CommandSql += " where " + Order.Columns.Guid + "=@id";
            qc.AddParameter("@id", orderGuid, System.Data.DbType.Guid);
            DataService.ExecuteScalar(qc);

            return true;
        }
    }
}

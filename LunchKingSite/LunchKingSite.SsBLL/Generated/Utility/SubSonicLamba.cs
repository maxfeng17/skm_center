﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    public interface IQueryOver<TRoot> where TRoot : RecordBase<TRoot>, new()
    {
        /// <summary>
        /// ToList() can't query view model(miss data), subsonic bug.
        /// </summary>
        /// <returns></returns>
        List<TRoot> ToList();
        IQueryOver<TRoot> Where(Expression<Func<TRoot, bool>> expression);
        /// <summary>
        /// 沒資料回傳 NULL
        /// </summary>
        /// <returns></returns>
        TRoot FirstOrDefault();
        /// <summary>
        /// 沒資料回傳 NULL
        /// </summary>
        /// <param name="expression"></param>
        /// <returns></returns>
        TRoot FirstOrDefault(Expression<Func<TRoot, bool>> expression);
        int Count(Expression<Func<TRoot, bool>> expression = null);

        IQueryOver<TRoot> OrderBy(Expression<Func<TRoot, object>> expression);
        IQueryOver<TRoot> OrderByDescending(Expression<Func<TRoot, object>> expression);
        IQueryOver<TRoot> Take(int count);
        IQueryOver<TRoot> Skip(int count);
        IQueryOver<TRoot> NoLock();

        TListType ToCollection<TListType>() where TListType : AbstractList<TRoot, TListType>, new();
    }

    /// <summary>
    /// 僅 試作
    /// </summary>
    /// <typeparam name="TRoot"></typeparam>
    public class QueryOver<TRoot> : IQueryOver<TRoot> where TRoot : RecordBase<TRoot>, new()
    {
        private SqlQuery query;
        private int skipCount;
        private int takeCount;
        public QueryOver()
        {
            query = DB.Select().From<TRoot>();
        }

        /// <summary>
        /// Get the results of the root type and fill the <see cref="IList&lt;T&gt;"/>
        /// ToList() can't query view model(miss data), subsonic bug.
        /// </summary>
        /// <returns>The list filled with the results.</returns>
        public List<TRoot> ToList()
        {
            DoPaged();
            var items = query.ExecuteTypedList<TRoot>();
            return items;
        }

        private void DoPaged()
        {
            if (takeCount > 0)
            {
                if (skipCount % takeCount > 0)
                {
                    throw new InvalidOperationException("受限於SubSonic, Skip 必需是 Take 的倍數.");
                }
                int page = (skipCount / takeCount) + 1;
                query.Paged(page, takeCount);
            }
        }

        /// <summary>
        /// 沒值傳回 Null
        /// </summary>
        /// <returns></returns>
        public TRoot FirstOrDefault()
        {
            TRoot result = query.ExecuteSingle<TRoot>();
            if (result.IsLoaded == false)
            {
                return null;
            }
            return result;
        }

        /// <summary>
        /// 沒值傳回 Null
        /// </summary>
        /// <returns></returns>
        public TRoot FirstOrDefault(Expression<Func<TRoot, bool>> expression)
        {
            Where(expression);
            return FirstOrDefault();
        }

        public int Count(Expression<Func<TRoot, bool>> expression = null)
        {
            if (expression != null)
            {
                Where(expression);
            }
            return query.GetRecordCount();
        }

        public IQueryOver<TRoot> Where(Expression<Func<TRoot, bool>> expression)
        {
            Constraint[] constraints = ProcessExpression(expression.Body, ConstraintType.And);
            query.AddConstraints(constraints, false);

            return this;
        }

        private Constraint[] ProcessExpression(Expression expression, ConstraintType ctype, bool inverse = false)
        {
            var binaryExpression = expression as BinaryExpression;
            if (binaryExpression != null)
            {
                return ProcessBinaryExpression(binaryExpression, ctype, inverse);
            }
            else
            {
                return ProcessBooleanExpression(expression, ctype, inverse);
            }
        }

        private Constraint[] ProcessBinaryExpression(BinaryExpression expression, ConstraintType ctype, bool inverse = false)
        {
            switch (expression.NodeType)
            {
                case ExpressionType.AndAlso:
                    return ProcessAndExpression(expression);
                case ExpressionType.OrElse:
                    return ProcessOrExpression(expression);
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                    if (IsMemberExpression(expression.Right))
                        return ProcessMemberExpression(expression);
                    else
                        return ProcessSimpleExpression(expression, ctype, inverse);
                default:
                    throw new Exception("Unhandled binary expression: " + expression.NodeType + ", " + expression);
            }
        }

        private Constraint[] ProcessAndExpression(BinaryExpression expression)
        {
            List<Constraint> result = new List<Constraint>();
            result.AddRange(ProcessExpression(expression.Left, ConstraintType.And));
            result.AddRange(ProcessExpression(expression.Right, ConstraintType.And));
            return result.ToArray();
        }

        private Constraint[] ProcessOrExpression(BinaryExpression expression)
        {
            List<Constraint> result = new List<Constraint>();
            result.AddRange(ProcessExpression(expression.Left, ConstraintType.And));
            result.AddRange(ProcessExpression(expression.Right, ConstraintType.Or));
            OpenAndCloseSubSonicExpress(result);
            return result.ToArray();
        }

        private void OpenAndCloseSubSonicExpress(List<Constraint> result)
        {
            //result[0].ConstructionFragment = "(" + result[0].ColumnName;
            result[0].ConstructionFragment = "(" + result[0].ConstructionFragment;
            Constraint tail = new Constraint(ConstraintType.Where, "##", "##", "##", null)
            {
                Comparison = Comparison.CloseParentheses
            };
            result.Add(tail);
        }

        private Constraint[] ProcessSimpleExpression(BinaryExpression expression, ConstraintType ctype, bool inverse = false)
        {
            if (expression.Left.NodeType == ExpressionType.Call && ((MethodCallExpression)expression.Left).Method.Name == "CompareString")
            {
                return ProcessVisualBasicStringComparison(expression);
            }
            else
            {
                Constraint[] result = ProcessEqualExpression(expression, ctype);
                if (result == null || result.Length == 0)
                {
                    result = ProcessNotEqualExpression(expression, ctype);
                }
                if (result == null || result.Length == 0)
                {
                    result = ProcessGreaterThanExpression(expression, ctype);
                }
                if (result == null || result.Length == 0)
                {
                    result = ProcessGreaterThanOrEqualExpression(expression, ctype);
                }
                if (result == null || result.Length == 0)
                {
                    result = ProcessLessThanExpression(expression, ctype);
                }
                if (result == null || result.Length == 0)
                {
                    result = ProcessLessThanOrEqualExpression(expression, ctype);
                }
                return result;
            }
        }

        private Constraint[] ProcessVisualBasicStringComparison(BinaryExpression be)
        {
            var methodCall = (MethodCallExpression)be.Left;

            if (IsMemberExpression(methodCall.Arguments[1]))
            {
                //ProcessMemberExpression(methodCall.Arguments[0], methodCall.Arguments[1], be.NodeType);
            }
            else
            {
                //ProcessSimpleExpression(methodCall.Arguments[0], methodCall.Arguments[1], be.NodeType);
            }
            return new Constraint[0];
        }

        private Constraint[] ProcessMemberExpression(BinaryExpression expression)
        {
            throw new NotSupportedException();
        }
        private static bool IsMemberExpression(Expression expression)
        {
            if (expression is ParameterExpression)
                return true;

            var memberExpression = expression as MemberExpression;
            if (memberExpression != null)
            {
                if (memberExpression.Expression == null)
                    return false;  // it's a member of a static class

                if (IsMemberExpression(memberExpression.Expression))
                    return true;

                // if the member has a null value, it was an alias
                return EvaluatesToNull(memberExpression.Expression);
            }

            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (!IsConversion(unaryExpression.NodeType))
                    throw new Exception("Cannot interpret member from " + expression);

                return IsMemberExpression(unaryExpression.Operand);
            }

            var methodCallExpression = expression as MethodCallExpression;
            if (methodCallExpression != null)
            {
                string signature = Signature(methodCallExpression.Method);
                //if (_customProjectionProcessors.ContainsKey(signature))
                //  return true;

                if (methodCallExpression.Method.Name == "First")
                {
                    if (IsMemberExpression(methodCallExpression.Arguments[0]))
                        return true;

                    return EvaluatesToNull(methodCallExpression.Arguments[0]);
                }

                if (methodCallExpression.Method.Name == "GetType"
                    || methodCallExpression.Method.Name == "get_Item")
                {
                    if (IsMemberExpression(methodCallExpression.Object))
                        return true;

                    return EvaluatesToNull(methodCallExpression.Object);
                }
            }

            return false;
        }


        private Constraint[] ProcessBooleanExpression(Expression expression, ConstraintType ctype, bool inverse = false)
        {
            var unaryExpression = expression as UnaryExpression;
            if (unaryExpression != null)
            {
                if (unaryExpression.NodeType != ExpressionType.Not)
                    throw new Exception("Cannot interpret member from " + expression);

                if (IsMemberExpression(unaryExpression.Operand) && unaryExpression.NodeType == ExpressionType.Not
                    && unaryExpression.Operand.NodeType == ExpressionType.MemberAccess && unaryExpression.Operand.Type == typeof(bool))
                {
                    //只讓 t->!t.IsSync 這樣的判斷式進來
                    string propName = ((MemberExpression)unaryExpression.Operand).Member.Name;
                    string columnName = GetColumnName(propName);
                    return CreateConstraint(ctype, columnName, Comparison.Equals, false);
                }
                else
                {
                    //return Restrictions.Not(ProcessExpression(unaryExpression.Operand));
                    return ProcessExpression(unaryExpression.Operand, ctype, !inverse);
                }
            }

            var methodCallExpression = expression as MethodCallExpression;
            if (methodCallExpression != null)
            {
                return ProcessCustomMethodCall(methodCallExpression, ctype, inverse);
            }

            //只讓 t->t.IsSync 這樣的判斷式進來
            var memberExpression = expression as MemberExpression;
            if (memberExpression != null && memberExpression.NodeType == ExpressionType.MemberAccess && memberExpression.Type == typeof(bool))
            {                
                string propName = memberExpression.Member.Name;
                string columnName = GetColumnName(propName);
                return CreateConstraint(ctype, columnName, Comparison.Equals, true);                
            }
            return null;
        }

        //private Constraint[] ProcessTrueFalseExpression(Expression expression, ConstraintType ctype, bool value)
        //{
        //}

        private Constraint[] ProcessCustomMethodCall(MethodCallExpression methodCallExpression, ConstraintType ctype, bool inverse = false)
        {
            string signature = methodCallExpression.Method.Name;
            if (signature == "Contains")
            {
                if (inverse == false)
                {
                    return ProcessLikeExpressions(methodCallExpression, ctype);
                }
                else
                {
                    return ProcessNotLikeExpressions(methodCallExpression, ctype);
                }
            }
            else if (signature == "EndsWith")
            {
                if (inverse == false)
                {
                    return ProcessEndsWithExpressions(methodCallExpression, ctype);
                }
                else
                {
                    return ProcessNotEndsWithExpressions(methodCallExpression, ctype);
                }
            }
            else if (signature == "StartsWith")
            {
                if (inverse == false)
                {
                    return ProcessStartsWithExpressions(methodCallExpression, ctype);
                }
                else
                {
                    return ProcessNotStartsWithExpressions(methodCallExpression, ctype);
                }
            }
            else
            {
                throw new NotSupportedException(signature + " 未被支援.");
            }
        }

        public static string Signature(MethodInfo methodInfo)
        {
            return methodInfo.Name;
        }

        private Constraint[] ProcessLikeExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.Like, "%" + value + "%");
        }

        private Constraint[] ProcessNotLikeExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.NotLike, "%" + value + "%");

        }

        private Constraint[] ProcessStartsWithExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.Like, value + "%");
        }

        private Constraint[] ProcessNotStartsWithExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.NotLike, value + "%");
        }

        private Constraint[] ProcessEndsWithExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.Like, "%" + value);
        }

        private Constraint[] ProcessNotEndsWithExpressions(MethodCallExpression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Call)
            {
                return new Constraint[0];
            }

            string propName = (expr.Object as MemberExpression).Member.Name;
            object value = (expr.Arguments[0] as ConstantExpression).Value;
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.NotLike, "%" + value);
        }

        private Constraint[] ProcessEqualExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.Equal)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            object value = FindValue(binaryExpression.Right);


            if (binaryExpression.Left.NodeType == ExpressionType.Not && value is bool)
            {
                bool bvalue = (bool)value;
                return ProcessBooleanExpression(binaryExpression.Left, ctype, !bvalue);
            }
            else if (binaryExpression.Left.NodeType == ExpressionType.Call && value is bool)
            {
                bool bvalue = (bool)value;
                return ProcessBooleanExpression(binaryExpression.Left, ctype, !bvalue);
            }


            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;

            string columnName = GetColumnName(propName);

            if (value == null)
            {
                return CreateConstraint(ctype, columnName, Comparison.Is, value);
            }
            else
            {
                return CreateConstraint(ctype, columnName, Comparison.Equals, value);
            }
        }

        private Constraint[] ProcessNotEqualExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.NotEqual)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;
            object value = FindValue(binaryExpression.Right);
            string columnName = GetColumnName(propName);

            if (value == null)
            {
                return CreateConstraint(ctype, columnName, Comparison.IsNot, value);
            }
            else
            {
                return CreateConstraint(ctype, columnName, Comparison.NotEquals, value);
            }
        }

        private Constraint[] ProcessGreaterThanExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.GreaterThan)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;
            object value = FindValue(binaryExpression.Right);
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.GreaterThan, value);
        }

        private Constraint[] ProcessGreaterThanOrEqualExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.GreaterThanOrEqual)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;
            object value = FindValue(binaryExpression.Right);
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.GreaterOrEquals, value);
        }

        private Constraint[] ProcessLessThanExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.LessThan)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;
            object value = FindValue(binaryExpression.Right);
            string columnName = GetColumnName(propName);

            return CreateConstraint(ctype, columnName, Comparison.LessThan, value);
        }

        private Constraint[] ProcessLessThanOrEqualExpression(Expression expr, ConstraintType ctype)
        {
            if (expr.NodeType != ExpressionType.LessThanOrEqual)
            {
                return new Constraint[0];
            }

            BinaryExpression binaryExpression = expr as BinaryExpression;
            MemberExpression memberExpression = binaryExpression.Left as MemberExpression;
            string propName = memberExpression.Member.Name;
            object value = FindValue(binaryExpression.Right);
            string columnName = GetColumnName(propName);
            return CreateConstraint(ctype, columnName, Comparison.LessOrEquals, value);
        }

        private Constraint[] CreateConstraint(ConstraintType ctype, string columnName, Comparison comparison, object value)
        {
            return new[]{ new Constraint(ctype, columnName)
            {
                Comparison = comparison,
                ParameterValue = value
            }};
        }

        private static readonly ConcurrentDictionary<string, string> dictColumnNames
            = new ConcurrentDictionary<string, string>();
        private string GetColumnName(string propName)
        {
            string key = string.Format("{0}.{1}", typeof(TRoot).Name, propName);
            return dictColumnNames.GetOrAdd(key, delegate(string argKey)
            {
                string argPropName = argKey.Split('.')[1];
                string result = new RecordBase<TRoot>().GetSchema().Columns
                    .Where(t => t.PropertyName == argPropName).Select(t => t.ColumnName).First();
                return result;
            });
        }

        private object FindValue(Expression expression)
        {
            var valueExpression = Expression.Lambda(expression).Compile();
            object value = valueExpression.DynamicInvoke();
            return value;

        }

        private static bool EvaluatesToNull(Expression expression)
        {
            var valueExpression = Expression.Lambda(expression).Compile();
            object value = valueExpression.DynamicInvoke();
            return (value == null);
        }

        private static bool IsConversion(ExpressionType expressionType)
        {
            return (expressionType == ExpressionType.Convert || expressionType == ExpressionType.ConvertChecked);
        }


        public IQueryOver<TRoot> OrderBy(Expression<Func<TRoot, object>> expression)
        {
            UnaryExpression unaryExpr = (UnaryExpression)expression.Body;
            string propName = (unaryExpr.Operand as MemberExpression).Member.Name;
            string columnName = GetColumnName(propName);
            query.OrderAsc(columnName);
            return this;
        }

        public IQueryOver<TRoot> OrderByDescending(Expression<Func<TRoot, object>> expression)
        {
            UnaryExpression unaryExpr = (UnaryExpression)expression.Body;
            string propName = (unaryExpr.Operand as MemberExpression).Member.Name;
            string columnName = GetColumnName(propName);
            query.OrderDesc(columnName);
            return this;
        }

        public IQueryOver<TRoot> Take(int count)
        {
            takeCount = count;
            return this;
        }

        public IQueryOver<TRoot> Skip(int count)
        {
            skipCount = count;
            return this;
        }

        public override string ToString()
        {
            return query.ToString();
        }


        public ListType ToCollection<ListType>() where ListType : AbstractList<TRoot, ListType>, new()
        {
            DoPaged();
            var items = query.ExecuteAsCollection<ListType>();
            return items;
        }

        public IQueryOver<TRoot> NoLock()
        {
            query.NoLock();
            return this;
        }
    }
}

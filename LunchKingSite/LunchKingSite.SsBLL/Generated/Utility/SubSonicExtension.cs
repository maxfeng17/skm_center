using LunchKingSite.Core;
using LunchKingSite.SsBLL;
using SubSonic;

namespace LunchKingSite.DataOrm
{
    public static class SubSonicExtension
    {
        public static void AddConstraints(this SqlQuery quqery, Constraint[] constraints, bool addExpress)
        {
            if (constraints.Length > 1)
            {
                for (int i = 0; i < constraints.Length; i++)
                {
                    SubSonic.Constraint constraint = constraints[i];

                    if (i == 0 && addExpress)
                    {
                        AddConstraint(quqery, constraint, true);
                    }
                    else
                    {
                        AddConstraint(quqery, constraint, false);
                    }
                }
                if (addExpress)
                {
                    quqery.CloseExpression();
                }
            }
            else
            {
                AddConstraint(quqery, constraints[0], false);
            }
        }

        public static void AddConstraint(this SqlQuery query, Constraint constraint, bool isExpress = false)
        {
            string constraintFragment = isExpress ? "(" + constraint.ColumnName : constraint.ColumnName;
            ConstraintType ctype = constraint.Condition;
            if (query.HasWhere == false)
            {
                ctype = ConstraintType.Where;
            }
            var newOne = new Constraint(ctype, constraint.ColumnName,
                constraint.QualifiedColumnName, constraintFragment, query)
            {
                ConstructionFragment = constraint.ConstructionFragment,
                Comparison = constraint.Comparison,
                ParameterValue = constraint.ParameterValue
            };
            query.Constraints.Add(newOne);
        }

    }
}
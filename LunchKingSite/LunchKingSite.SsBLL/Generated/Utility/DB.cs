﻿using System;
using System.Text;
using System.Collections.Generic;
using SubSonic;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using LunchKingSite.Core;
using System.Transactions;
using LunchKingSite.Core.Component;
using log4net;
using System.Web;
using System.Linq;

namespace LunchKingSite.DataOrm
{
    public static class DB
    {
        private static ILog log = LogManager.GetLogger(typeof(DB));
        public static DataProvider _provider = DataService.Providers["LKSiteDB"];
        private static ISysConfProvider _conf = ProviderFactory.Instance().GetConfig();
        static ISubSonicRepository _repository;
        public static ISubSonicRepository Repository
        {
            get
            {
                if (_repository == null)
                    return new SubSonicRepository(_provider);
                return _repository;
            }
            set { _repository = value; }
        }
        public static Select SelectAllColumnsFrom<T>() where T : RecordBase<T>, new()
        {
            return Repository.SelectAllColumnsFrom<T>();
        }
        public static Select Select()
        {
            return Repository.Select();
        }

        public static Select Select(params string[] columns)
        {
            return Repository.Select(columns);
        }

        public static Select Select(params Aggregate[] aggregates)
        {
            return Repository.Select(aggregates);
        }

        public static Update Update<T>() where T : RecordBase<T>, new()
        {
            return Repository.Update<T>();
        }

        public static Insert Insert()
        {
            return Repository.Insert();
        }

        public static Delete Delete()
        {
            return Repository.Delete();
        }

        public static InlineQuery Query()
        {
            return Repository.Query();
        }

        /// <summary>
        /// alpha 0.000001的試作
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static IQueryOver<T> QueryOver<T>() where T : RecordBase<T>, new()
        {
            return new QueryOver<T>();
        }

        public static int Execute(QueryCommand qc)
        {
            int affectCount = DataService.ExecuteQuery(qc);
            return affectCount;
        }

        #region Repository Compliance

        public static T Get<T>(object primaryKeyValue) where T : RepositoryRecord<T>, new()
        {
            return Repository.Get<T>(primaryKeyValue);
        }
        public static T Get<T>(string columnName, object columnValue) where T : RepositoryRecord<T>, new()
        {
            return Repository.Get<T>(columnName, columnValue);
        }

        public static void Delete<T>(string columnName, object columnValue) where T : RepositoryRecord<T>, new()
        {
            Repository.Delete<T>(columnName, columnValue);
        }
        public static void Delete<T>(RepositoryRecord<T> item) where T : RepositoryRecord<T>, new()
        {
            Repository.Delete<T>(item);
        }
        //public static void DeleteByKey<T>(object itemId) where T : RepositoryRecord<T>, new() 
        //{
        //    Repository.DeleteByKey<T>(itemId);
        //}
        public static void Destroy<T>(RepositoryRecord<T> item) where T : RepositoryRecord<T>, new()
        {
            Repository.Destroy<T>(item);
        }

        public static void Destroy<T>(string columnName, object value) where T : RepositoryRecord<T>, new()
        {
            Repository.Destroy<T>(columnName, value);
        }
        //public static void DestroyByKey<T>(object itemId) where T : RepositoryRecord<T>, new() 
        //{
        //    Repository.DestroyByKey<T>(itemId);
        //}
        public static int Save<T>(RepositoryRecord<T> item) where T : RepositoryRecord<T>, new()
        {
            SimulateReadOnly(item.TableName);
            return Repository.Save<T>(item);
        }

        public static int Save<T>(RepositoryRecord<T> item, string userName) where T : RepositoryRecord<T>, new()
        {
            SimulateReadOnly(item.TableName);
            return Repository.Save<T>(item, userName);
        }
        public static int SaveAll<ItemType, ListType>(RepositoryList<ItemType, ListType> itemList)
            where ItemType : RepositoryRecord<ItemType>, new()
            where ListType : RepositoryList<ItemType, ListType>, new()
        {
            if (itemList.Count > 0)
                SimulateReadOnly(itemList[0].TableName);
            return Repository.SaveAll<ItemType, ListType>(itemList);
        }

        public static int SaveAll<ItemType, ListType>(RepositoryList<ItemType, ListType> itemList, string userName)
            where ItemType : RepositoryRecord<ItemType>, new()
            where ListType : RepositoryList<ItemType, ListType>, new()
        {
            return Repository.SaveAll<ItemType, ListType>(itemList, userName);
        }

        /// <summary>
        /// 新版 SaveAll 候選版本(一)
        /// </summary>
        /// <typeparam name="ItemType"></typeparam>
        /// <typeparam name="ListType"></typeparam>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public static int SaveAll2<ItemType, ListType>(RepositoryList<ItemType, ListType> itemList)
            where ItemType : RepositoryRecord<ItemType>, new()
            where ListType : RepositoryList<ItemType, ListType>, new()
        {
            if (itemList.Count == 0)
            {
                return 0;
            }
            string providerName = itemList[0].ProviderName;
            int sqlScriptLengthLimit = 2000; //暫訂
            int affectCount = 0;
            StringBuilder sbSql = new StringBuilder();
            List<QueryParameter> qpList = new List<QueryParameter>();
            int paramSuffix = 0;
            for (int i = 0; i < itemList.Count; i++)
            {
                var item = itemList[i];
                QueryCommand qc = item.IsNew
                    ? ActiveHelper<ItemType>.GetInsertCommand((RecordBase<ItemType>)item, userName: "")
                    : ActiveHelper<ItemType>.GetUpdateCommand((RecordBase<ItemType>)item, userName: "");
                if (qc != null && string.IsNullOrEmpty(qc.CommandSql) == false)
                {
                    string sql = qc.CommandSql;
                    //adjust param name to uniuqe
                    foreach (var parameter in qc.Parameters)
                    {
                        string newParameterName = parameter.ParameterName + paramSuffix.ToString();
                        sql = sql.Replace(parameter.ParameterName, newParameterName);
                        parameter.ParameterName = newParameterName;
                        paramSuffix++;
                    }
                    sbSql.Append(sql);
                    sbSql.Append(';');
                    qpList.AddRange(qc.Parameters.ToArray());

                    // 超過sql執行的長度限制 或 進入尾聲
                    if (sbSql.Length > sqlScriptLengthLimit || i == itemList.Count - 1)
                    {
                        QueryCommand qcBatch = new QueryCommand(sbSql.ToString(), providerName);
                        qcBatch.Parameters.AddRange(qpList);
                        affectCount += Execute(qcBatch);
                        //reset data
                        sbSql.Clear();
                        qpList.Clear();
                    }
                }
            }
            return affectCount;
        }

        /// <summary>
        /// 新版 SaveAll 候選版本(二)
        /// </summary>
        /// <typeparam name="ItemType"></typeparam>
        /// <typeparam name="ListType"></typeparam>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public static int SaveAll3<ItemType, ListType>(RepositoryList<ItemType, ListType> itemList)
            where ItemType : RepositoryRecord<ItemType>, new()
            where ListType : RepositoryList<ItemType, ListType>, new()
        {
            if (itemList.Count == 0)
            {
                return 0;
            }

            string providerName = itemList[0].ProviderName;
            int batchSize = 20;
            int affectCount = 0;
            QueryCommandCollection qcCol = new QueryCommandCollection();
            for (int i = 0; i < itemList.Count; i++)
            {
                var item = itemList[i];
                QueryCommand qc = item.IsNew
                    ? ActiveHelper<ItemType>.GetInsertCommand((RecordBase<ItemType>)item, userName: "")
                    : ActiveHelper<ItemType>.GetUpdateCommand((RecordBase<ItemType>)item, userName: "");
                if (qc != null)
                {
                    qcCol.Add(qc);
                }

                if (qcCol.Count > batchSize || i == itemList.Count - 1)
                {
                    affectCount += qcCol.Count;
                    DataService.ExecuteTransaction(qcCol, providerName);
                    qcCol.Clear();
                }
            }
            return affectCount;
        }

        /// <summary>
        /// Bulk Insert(只提供insert，與SaveAll不同)
        /// </summary>
        /// <typeparam name="ItemType"></typeparam>
        /// <typeparam name="ListType"></typeparam>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public static bool BulkInsert<ItemType, ListType>(RepositoryList<ItemType, ListType> itemList, string dbName = "Default")
            where ItemType : RepositoryRecord<ItemType>, new()
            where ListType : RepositoryList<ItemType, ListType>, new()
        {
            bool result = false;

            if (itemList.Count == 0)
            {
                return result;
            }

            // convert to datatable
            ListType list = new ListType();
            DataTable table = list.ToDataTable();
            foreach (var item in itemList)
            {
                DataRow row = table.NewRow();
                foreach (DataColumn col in table.Columns)
                {
                    row[col] = item.GetColumnValue(col.ToString()) ?? DBNull.Value;
                }
                table.Rows.Add(row);
            }

            using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings[dbName].ConnectionString, SqlBulkCopyOptions.CheckConstraints))
                {
                    bulkCopy.DestinationTableName = table.TableName;
                    bulkCopy.BulkCopyTimeout = 600;

                    try
                    {
                        bulkCopy.WriteToServer(table);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        log.Warn("匯入 " + table.Rows.Count + " 筆資料失敗，ex:" + ex);
                    }
                }
                scope.Complete();
            }

            return result;
        }

        /// <summary>
        /// Bulk Insert(只提供insert，與SaveAll不同)
        /// </summary>
        /// <typeparam name="ItemType"></typeparam>
        /// <typeparam name="ListType"></typeparam>
        /// <param name="itemList"></param>
        /// <returns></returns>
        public static bool BulkInsert(DataTable table, string dbName = "Default")
        {
            bool result = false;
            DateTime logStart = DateTime.Now;
            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings[dbName].ConnectionString, SqlBulkCopyOptions.CheckConstraints))
                {
                    bulkCopy.DestinationTableName = table.TableName;
                    bulkCopy.BulkCopyTimeout = 600;

                    try
                    {
                        bulkCopy.WriteToServer(table);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        log.Warn("匯入 " + table.Rows.Count + " 筆資料失敗，ex:" + ex);
                        result = false;
                    }
                }
                scope.Complete();
            }
            log.Info("匯入 " + table.Rows.Count + " 筆資料耗時 :" + new TimeSpan(DateTime.Now.Ticks - logStart.Ticks).TotalSeconds.ToString("0.##") + "秒");
            return result;
        }

        public static int Update<T>(RepositoryRecord<T> item) where T : RepositoryRecord<T>, new()
        {
            return Repository.Update<T>(item, "");
        }
        public static int Update<T>(RepositoryRecord<T> item, string userName) where T : RepositoryRecord<T>, new()
        {
            return Repository.Update<T>(item, userName);
        }
        public static int Insert<T>(RepositoryRecord<T> item) where T : RepositoryRecord<T>, new()
        {
            return Repository.Insert<T>(item);
        }
        public static int Insert<T>(RepositoryRecord<T> item, string userName) where T : RepositoryRecord<T>, new()
        {
            return Repository.Insert<T>(item, userName);
        }
        #endregion

        /// <summary>
        /// 限制商家模擬權限操作
        /// </summary>
        /// <param name="tableName"></param>
        public static void SimulateReadOnly(string tableName)
        {

            var whiteList = new[] { VbsAccountAudit.Schema.TableName };
            if (!whiteList.Any(x => x == tableName) && HttpContext.Current != null && HttpContext.Current.Request.Url.LocalPath.Contains(_conf.SiteUrl + "/vbs/") && HttpContext.Current.Session != null && HttpContext.Current.Session.Count > 0)
            {
                if (HttpContext.Current.Session[VbsSession.LoginBy17LifeSimulate.ToString()] != null && HttpContext.Current.Session[VbsSession.LoginBy17LifeSimulate.ToString()].ToString() == "True")
                {
                    throw new Exception("模擬身分不允許執行此功能");
                }

            }
        }
    }

}

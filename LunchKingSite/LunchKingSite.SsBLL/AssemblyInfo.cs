using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("LunchKingSite.SsProvider")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyProduct("LunchKingSite.SsProvider")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("c709feb4-5e89-4c01-a115-2222e26e78bd")]

[assembly: InternalsVisibleTo("LunchKingSite.Tests")]
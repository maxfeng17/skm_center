﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class HiLifeDbContextcs : DbContextBase
    {
        public HiLifeDbContextcs()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<HiLifeDbContextcs>(null);
        }

        public DbSet<HiLifeGetPincodeLog> HiLifeGetPincodeLogEntities { get; set; }
        public DbSet<HiLifePincode> HiLifePincodeEntities { get; set; }
        public DbSet<HiLifeVerifyLog> HiLifeVerifyLogEntities { get; set; }
        public DbSet<HiLifeReturnLog> HiLifeReturnLogEntities { get; set; }
        public DbSet<ViewCouponPeztempList> ViewCouponPeztempListEntities { get; set; }
        public DbSet<LunchKingSite.Core.Models.Entities.Peztemp> PeztempEntities { get; set; }
        public DbSet<HiLifeApiLog> HiLifeApiLogEntities { get; set; }
    }
}

﻿using System.Data.Entity;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class ChannelDbContext : DbContextBase
    {
        /// <summary>
        /// 
        /// </summary>
        public ChannelDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<ChannelDbContext>(null);
        }

        public DbSet<ChannelPosApiLog> PosApiLogEntities { get; set; }
        public DbSet<ChannelCommissionRule> ChannelCommissionRuleEntities { get; set; }
    }
}

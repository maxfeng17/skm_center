﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class QuestionDbContext : DbContextBase
    {
        public QuestionDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<QuestionDbContext>(null);
        }

        public DbSet<QuestionAnswer> QuestionAnswerEntities { get; set; }
        public DbSet<QuestionAnswerDiscountLog> QuestionAnswerDiscountLogEntities { get; set; }
        public DbSet<QuestionAnswerOption> QuestionAnswerOptionEntities { get; set; }
        public DbSet<QuestionEvent> QuestionEventEntities { get; set; }
        public DbSet<QuestionEventDiscount> QuestionEventDiscountEntities { get; set; }
        public DbSet<QuestionItem> QuestionItemEntities { get; set; }
        public DbSet<QuestionItemCategory> QuestionItemCategoryEntities { get; set; }
        public DbSet<QuestionItemOption> QuestionItemOptionEntities { get; set; }
        public DbSet<ViewQuestionItem> ViewQuestionItemEntities { get; set; }
    }
}

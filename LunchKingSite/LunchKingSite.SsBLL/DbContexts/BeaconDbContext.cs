﻿using System.Data.Entity;
using log4net;
using LunchKingSite.DataOrm;
using LunchKingSite.Core.Models.Entities;
using System;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class BeaconDbContext : DbContextBase
    {
        public BeaconDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<BeaconDbContext>(null);
        }

        public DbSet<BeaconOauth> BeaconOauthEntities { get; set; }
        public DbSet<BeaconTriggerApp> BeaconTriggerAppEntities { get; set; }
        public DbSet<BeaconTriggerAppField> BeaconTriggerAppFieldEntities { get; set; }
        public DbSet<BeaconField> BeaconFieldEntities { get; set; }
        public DbSet<BeaconFieldDeviceLink> BeaconFieldDeviceLinkEntities { get; set; }
        public DbSet<BeaconDevice> BeaconDeviceEntities { get; set; }
        public DbSet<BeaconEvent> BeaconEventEntities { get; set; }
        public DbSet<BeaconEventTimeSlot> BeaconEventTimeSlotEntities { get; set; }
        public DbSet<BeaconEventTarget> BeaconEventTargetEntities { get; set; }
        public DbSet<BeaconSubevent> BeaconSubeventEntities { get; set; }
        public DbSet<BeaconGroup> BeaconGroupEntities { get; set; }
        public DbSet<BeaconGroupDeviceLink> BeaconGroupDeviceLinkEntities { get; set; }
        public DbSet<BeaconEventGroupLink> BeaconEventGroupLinkEntities { get; set; }
        public DbSet<BeaconEventDeviceLink> BeaconEventDeviceLinkEntities { get; set; }
        public DbSet<BeaconLog> BeaconLogEntities { get; set; }
        public DbSet<BeaconTriggerAppUuid> BeaconTriggerAppUuidEntities { get; set; }
        public DbSet<BeaconEventIdTicker> BeaconEventIdTickerEntities { get; set; }
        public DbSet<SkmBeaconProperty> SkmBeaconPropertyEntities { get; set; }
        public DbSet<Core.Models.Entities.DevicePushRecord> DevicePushRecordEntities { get; set; }
        public DbSet<ViewBeaconDeviceGroup> ViewBeaconDeviceGroupEntities { get; set; }
        public DbSet<ViewBeaconEventTimeSlot> ViewBeaconEventTimeSlotEntities { get; set; }
    }
}

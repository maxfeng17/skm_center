﻿using System.Data.Entity;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class EntrepotDbContext : DbContextBase
    {
        public EntrepotDbContext() : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<EntrepotDbContext>(null);
        }

        public DbSet<EntrepotMember> EntrepotMemberEntities { get; set; }
    }
}

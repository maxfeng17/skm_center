﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Common;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class DbContextBase : DbContext
    {
        ILog logger = LogManager.GetLogger("sql");


        private IsolationLevelNoLockSetter noLockSetter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DbContextBase(string nameOrConnectionString)
            : base(nameOrConnectionString: nameOrConnectionString)
        {
            this.Database.Log = WriteSqlLog;
        }

        private void WriteSqlLog(string str)
        {
            logger.Debug(str);
        }

        public IsolationLevelNoLockSetter NoLock()
        {
            noLockSetter = new IsolationLevelNoLockSetter(this);
            return noLockSetter;
        }

        /// <summary>
        /// 當使用如下raw sql撈 Entity 時，
        /// DbContext.DbSet.SqlQuery("select t.* from T t");
        /// 如果Model的Property 跟 Table 的欄位名稱不一致，譬如大小寫不同，
        /// 撈到的資料無法正確的mapping回model，需要用底下的方式取得修正的sql
        /// 
        /// 回傳t.name as Name, t.id as Id
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public string GetModelSelectColumns<T>()
        {
            StringBuilder sb = new StringBuilder();

            Type t = typeof(T);

            var props = from p in typeof(T).GetProperties()
                        let attr = p.GetCustomAttributes(typeof(ColumnAttribute), true)
                        where attr.Length == 1
                        select new { Property = p, Attribute = attr.First() as ColumnAttribute };


            foreach (var prop in props)
            {
                if (sb.Length > 0)
                {
                    sb.Append(", ");
                }
                sb.Append(prop.Attribute.Name);
                sb.Append(" as ");
                sb.Append(prop.Property.Name);
            }

            return sb.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (noLockSetter != null && noLockSetter.IsDisposed == false)
            {
                noLockSetter.Dispose();
            }
            base.Dispose(disposing);
        }
    }

    public class IsolationLevelNoLockSetter : IDisposable
    {
        private DbContext _ctx;
        public bool IsDisposed { get; private set; }
        public IsolationLevelNoLockSetter(DbContext ctx)
        {
            this._ctx = ctx;
            this._ctx.Database.ExecuteSqlCommand("set transaction isolation level read uncommitted");
        }

        public void Dispose()
        {
            if (IsDisposed == false)
            {
                this._ctx.Database.ExecuteSqlCommand("set transaction isolation level read committed");
            }
            IsDisposed = true;
        }
    }

    public static class DbContextExtensions
    {
        public static Dictionary<T, T2> QueryGroupByCountRawSql<T, T2>(this DbContext dbContext,
            string sql, params SqlParameter[] parameters) where T : struct where T2 : struct
        {
            Dictionary<T, T2> result = new Dictionary<T, T2>();
            SqlConnection connection = dbContext.Database.Connection as SqlConnection;

            connection.Open();

            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = sql;

                foreach (var parameter in parameters)
                {
                    command.Parameters.Add(parameter);
                }

                using (DbDataReader dr = command.ExecuteReader())
                {
                    while (dr.Read())
                    {
                        result.Add((T)dr.GetValue(0), (T2)dr.GetValue(1));
                    }
                }
            }

            return result;
        }
    }

    public class DbContextBase<T> : DbContextBase where T : DbContextBase, new()
    {
        public DbContextBase(string nameOrConnectionString) : base(nameOrConnectionString)
        {
        }        

        public static DbContextWrapper<T> Current
        {
            get
            {
                LocalDataStoreSlot dataslot = Thread.GetNamedDataSlot(DbContextWrapper<T>._SLOT_NAME);
                T t = Thread.GetData(dataslot) as T;
                DbContextWrapper<T> dbWrapper = null;
                if (t == default(T))
                {
                    t = new T();
                    Thread.SetData(dataslot, t);
                    dbWrapper = new DbContextWrapper<T>(t , true);
                }
                else
                {
                    dbWrapper = new DbContextWrapper<T>(t, false);
                }                
                return dbWrapper;
            }
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class DealDbContext : DbContextBase
    {
        public DealDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<DealDbContext>(null);
        }

        public DbSet<GmcAvgPriceDeal> GmcAvgPriceDealEntities { get; set; }
    }
}

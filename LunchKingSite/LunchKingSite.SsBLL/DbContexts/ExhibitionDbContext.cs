﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class ExhibitionDbContext : DbContextBase
    {
        public ExhibitionDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<ExhibitionDbContext>(null);
        }

        public DbSet<ExhibitionEvent> ExhibitionEventEntities { get; set; }
        public DbSet<ExhibitionCategory> ExhibitionCategoryEntities { get; set; }
        public DbSet<ExhibitionDeal> ExhibitionDealEntities { get; set; }
        public DbSet<ExhibitionStore> ExhibitionStoreEntities { get; set; }
        public DbSet<ExhibitionLog> ExhibitionLogEntities { get; set; }
        public DbSet<ExhibitionCode> ExhibitionCodeEntities { get; set; }
        public DbSet<ExhibitionDealTimeSlot> ExhibitionDealTimeSlotEntities { get; set; }
        public DbSet<ViewExternalDealRelationStoreExhibitionCode> ViewExternalDealRelationStoreExhibitionCodeEntities { get; set; }
        public DbSet<ExhibitionCardType> ExhibitionCardTypeEntities { get; set; }
        public DbSet<ViewExhibitionDealExternalDeal> ViewExhibitionDealExternalDealEntities { get; set; }
        public DbSet<ViewExhibitionEvent> ViewExhibitionEvents { get; set; }
        public DbSet<ExhibitionActivity> ExhibitionActivities { get; set; }
        public DbSet<ExhibitionUserGroup> ExhibitionUserGroupEntities { get; set; }
        public DbSet<ExhibitionUserGroupMapping> ExhibitionUserGroupMappingEntities { get; set; }
        public DbSet<ExhibitionUserGroupMemberToken> ExhibitionUserGroupMemberTokenEntities { get; set; }

        public DbSet<ViewExhibitionEventUserGroup> ViewExhibitionEventUserGroupEntities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //HasDatabaseGeneratedOption()標明欄位是PK時不需要當作自動增長的值，可自帶PK值
            //HasColumnOrder()標明若是複合主鍵，則定義複合主鍵的排序
            //HasKey() 因為View設定唯一index還是沒有辦法自動標上[key]，所以在這設定複合主鍵

            #region exhibition_code

            modelBuilder.Entity<ExhibitionCode>().Property(item => item.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            #endregion exhibition_code

            #region exhibition_card_type

            modelBuilder.Entity<ExhibitionCardType>().Property(item => item.ExhibitionEventId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(0);
            modelBuilder.Entity<ExhibitionCardType>().Property(item => item.CardType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(1);

            #endregion exhibition_card_type

            #region view_exhibition_deal_external_deal

            modelBuilder.Entity<ViewExhibitionDealExternalDeal>()
                .HasKey(item => new { item.ExhibitionEventId, item.CategoryId, item.ExternalDealGuid });

            #endregion view_exhibition_deal_external_deal

            #region exhibition_store

            modelBuilder.Entity<ExhibitionStore>().Property(item => item.StoreGuid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(0);
            modelBuilder.Entity<ExhibitionStore>().Property(item => item.EventId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(1);

            #endregion exhibition_store

            #region view_exhibition_event

            modelBuilder.Entity<ViewExhibitionEvent>()
                .HasKey(item => new { item.ExhibitionEventId, item.StoreGuid });

            #endregion view_exhibition_event

            base.OnModelCreating(modelBuilder);
        }
    }
}
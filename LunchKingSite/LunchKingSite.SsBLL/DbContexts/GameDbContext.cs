﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.GameEntities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class GameDbContext : DbContextBase
    {
        public GameDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<GameDbContext>(null);
        }

        public DbSet<GameRound> GameRoundSet { get; set; }
        public DbSet<GameRule> GameRuleSet { get; set; }
        public DbSet<GameCampaign> GameCampaignSet { get; set; }
        public DbSet<GameActivity> GameActivitySet { get; set; }
        public DbSet<GameGame> GameGameSet { get; set; }
        public DbSet<GameBuddy> GameBuddySet { get; set; }
        public DbSet<GameBuddyReward> GameBuddyRewardSet { get; set; }
        public DbSet<GameDealPermission> GameDealPermissionSet { get; set; }
        public DbSet<GameDealOrder> GameDealOrderSet { get; set; }

    }
}

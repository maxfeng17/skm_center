﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Threading.Tasks;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class DbContextWrapper<T> : IDisposable where T : DbContextBase, new()
    {
        public const string _SLOT_NAME = "DbContext";
        private T dbContext;

        public DbContextWrapper(T db, bool isTop)
        {
            dbContext = db;
            IsTop = isTop;
        }

        public Database Database
        {
            get
            {
                return dbContext.Database;
            }
        }
        
        public DbEntityEntry Entry(object entity)
        {
            return dbContext.Entry(entity);
        }
        public DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class
        {
            return dbContext.Entry<TEntity>(entity);
        }
        public int SaveChanges()
        {
            return dbContext.SaveChanges();
        }
        public Task<int> SaveChangesAsync()
        {
            return dbContext.SaveChangesAsync();
        }

        public bool IsTop { get; private set; }

        public void Dispose()
        {
            if (IsTop)
            {
                LocalDataStoreSlot slot = Thread.GetNamedDataSlot(_SLOT_NAME);
                Thread.SetData(slot, null);
                dbContext.Dispose();
            }
        }
    }
}

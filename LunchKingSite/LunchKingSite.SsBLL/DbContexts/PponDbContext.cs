﻿using System.Data.Entity;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Models.PponEntities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class PponDbContext : DbContextBase
    {
        public PponDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<PponDbContext>(null);
        }

        public DbSet<GoogleShoppingProduct> GoogleProductStatusDbSet { get; set; }
        public DbSet<DealArchiveVerificationSummary> DealArchiveVerificationSummaryDbSet { get; set; }
        public DbSet<DealGoogleMetadata> DealGoogleMetadataDbSet { get; set; }
        public DbSet<DealDiscountProperty> DealDiscountPropertyDbSet { get; set; }
        public DbSet<DealDiscountPriceBlacklist> DealDiscountPriceBlacklistDbSet { get; set; }
        /// <summary>
        /// 會員瀏覽記錄 預訂只放一個月內的資料，資料從 uat-server 以排程倒入
        /// </summary>
        public DbSet<DealPageView> DealPageViewDbSet { get; set; }

        /// <summary>
        /// 成效報表
        /// </summary>
        public DbSet<FrontDealsDailyStat> FrontDealsDailyStatsDbSet { get; set; }
        public DbSet<FrontDealOrder> FrontDealOrderDbSet { get; set; }
        public DbSet<FrontViewCount> FrontViewCountDbSet { get; set; }

        //SoloEdm
        public DbSet<SoloEdmSendEmail> SoloEdmSendEmailDbSet { get; set; }

        /// <summary>
        /// 搶購專區
        /// </summary>
        public DbSet<LimitedTimeSelectionDeal> LimitedTimeSelectionDealDbSet { get; set; }
        public DbSet<LimitedTimeSelectionMain> LimitedTimeSelectionMainDbSet { get; set; }
        public DbSet<LimitedTimeSelectionLog> LimitedTimeSelectionLogDbSet { get; set; }

        public DbSet<EventActivityHitLog> EventActivityHitLogDbSet { get; set; }
        /// <summary>
        /// 每日的銷售記錄，用以算轉換率
        /// </summary>
        public DbSet<DealDailySalesInfo> DealDailySalesInfoDbSet { get; set; }

        public DbSet<AdsLog> AdsLogDbSet { get; set; }

        #region Pchome倉儲

        public DbSet<WmsContact> WmsContactDbSet { get; set; }

        #endregion


        public DbSet<QueuedMemberPushMessage> QueuedMemberPushMessageSet { get; set; }


        #region Line 

        public DbSet<LineUserPageView> LineUserPageViewDbSet { get; set; }
        
        #endregion

        /// <summary>
        /// M版首頁
        /// </summary>
        public DbSet<HomepageBlock> HomepageBlockDbSet { get; set; }

        #region 企業帳號購物金記錄

        public DbSet<EnterpriseDepositOrder> EnterpriseDepositOrderSet { get; set; }
        public DbSet<EnterpriseDepositOrderScashRecord> EnterpriseDepositOrderScashRecordSet { get; set; }
        public DbSet<EnterpriseMember> EnterpriseMemberSet { get; set; }
        public DbSet<EnterpriseWithdrawalOrder> EnterpriseWithdrawalOrderSet { get; set; }

        #endregion

    }
}
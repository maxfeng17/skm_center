﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;
using CodeFirstStoreFunctions;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class SkmDbContext : DbContextBase
    {
        public SkmDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<SkmDbContext>(null);
        }

        public DbSet<SkmPayParkingOrderTransLog> SkmPayParkingOrderTransLog { get; set; }
        public DbSet<SkmPayParkingOrder> SkmPayParkingOrder { get; set; }
        public DbSet<SkmActivity> SkmActivityEntities { get; set; }
        public DbSet<SkmActivityItem> SkmActivityItemEntities { get; set; }
        public DbSet<SkmActivityDateRange> SkmActivityDateRangeEntities { get; set; }
        public DbSet<SkmActivityLog> SkmActivityLogEntities { get; set; }
        public DbSet<SkmPayBanner> SkmPayBanner { get; set; }
        public DbSet<ExternalDealCombo> ExternalDealCombo { get; set; }
        public DbSet<ViewSkmActivityLog> ViewSkmActivityLog { get; set; }
        public DbSet<SkmPayOrder> SkmPayOrder { get; set; }
        public DbSet<SkmPayOrderDetail> SkmPayOrderDteailEntities { get; set; }
        public DbSet<SkmPayInvoice> SkmPayInvoice { get; set; }
        public DbSet<SkmPayOrderTransLog> SkmPayOrderTransLog { get; set; }
        public DbSet<SkmPayInvoiceSellDetail> SkmPayInvoiceSellDetail { get; set; }
        public DbSet<SkmPayInvoiceTenderDetail> SkmPayInvoiceTenderDetail { get; set; }
        public DbSet<ViewSkmPayOrder> ViewSkmPayOrder { get; set; }
        public DbSet<SkmInAppMessage> SkmInAppMessage { get; set; }
        public DbSet<ViewSkmPponOrder> ViewSkmPponOrder { get; set; }
        public DbSet<ViewSkmPayPponOrder> ViewSkmPayPponOrder { get; set; }
        public DbSet<ViewSkmPayOrderQueryUmallGift> ViewSkmPayOrderQueryUmallGift { get; set; }
        public DbSet<SkmSerialNumber> SkmSerialNumber { get; set; }
        public DbSet<SkmSerialNumberLog> SkmSerialNumberLog { get; set; }
        public DbSet<ViewSkmPayClearPos> ViewSkmPayClearPosEntities { get; set; }
        public DbSet<SkmPayClearPosTransLog> SkmPayClearPosTransLogEntities { get; set; }
        public DbSet<SkmPayOtp> SkmPayOtpEntities { get; set; }
        public DbSet<SkmStoreDisplayName> SkmStoreDisplayNameEntities { get; set; }
        public DbSet<ViewSkmStoreDisplayName> ViewSkmStoreDisplayNameEntities { get; set; }
        public DbSet<ViewExternalDealStoreDisplayName> ViewExternalDealStoreDisplayNameEntities { get; set; }
        public DbSet<SkmAdBoard> SkmAdBoardEntities { get; set; }
        public DbSet<ViewSkmDealQuantity> ViewSkmDealQuantityEntities { get; set; }
        public DbSet<SkmSetExternalDealInventoryLog> SkmSetExternalDealInventoryLogEntities { get; set; }
        public DbSet<ViewSkmDealGuidQuantity> ViewSkmDealGuidQuantityEntities { get; set; }
        public DbSet<SkmCustomizedBoard> SkmCustomizedBoardEntities { get; set; }
        public DbSet<ViewSkmCashTrustLogNotVerified> ViewSkmCashTrustLogNotVerifiedEntities { get; set; }
        public DbSet<SkmTokenTracking> SkmTokenTrackingEntities { get; set; }
        public DbSet<ViewSkmInstallEventList> ViewSkmInstallEventList { get; set; }
        public DbSet<SkmInstallEvent> SkmInstallEvent { get; set; }
        public DbSet<SkmInstallEventBank> SkmInstallEventBank { get; set; }
        public DbSet<SkmInstallEventPeriod> SkmInstallEventPeriod { get; set; }
        public DbSet<ViewSkmInstallEventBank> ViewSkmInstallEventBank { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ExternalDealCombo>().Property(item => item.ExternalDealGuid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(0);
            modelBuilder.Entity<ExternalDealCombo>().Property(item => item.ShopCode)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(1);

            //指定Key
            modelBuilder.Entity<ViewSkmPayOrderQueryUmallGift>()
                .HasKey(item => new { item.Id, item.TrustId });
            modelBuilder.Entity<ViewSkmPayClearPos>()
                .HasKey(item => new { item.SkmPayOrderDetailId, item.SellShopid, item.SellTranstype, item.SellRate, item.PreSalesStatus, item.SellTender });
            modelBuilder.Entity<ViewSkmDealQuantity>()
                .HasKey(item => new { item.ExternalDealGuid, item.ShopCode, item.SkmEventId, item.ExchangeItemId, item.Bid });
            modelBuilder.Entity<ViewSkmDealGuidQuantity>()
                .HasKey(item => new { item.ExternalDealGuid, item.ShopCode, item.OrderTotalLimit });
            modelBuilder.Entity<ViewSkmCashTrustLogNotVerified>()
                .HasKey(item => new { item.TrustId });
            modelBuilder.Entity<ViewSkmInstallEventList>()
                .HasKey(item => new { item.Id });
            modelBuilder.Entity<ViewSkmInstallEventBank>()
                .HasKey(item => new { item.PeriodId });
            modelBuilder.Entity<ViewSkmPayOrder>()
                .HasKey(item => new { item.OrderId });                

            //Function
            modelBuilder.Conventions.Add(new FunctionsConvention<SkmDbContext>("dbo"));
            modelBuilder.ComplexType<SkmpayOrderInfo>();
            modelBuilder.ComplexType<SkmOnlineDeals>();
            modelBuilder.ComplexType<SkmCurationEvents>();
            modelBuilder.ComplexType<SkmCurationDeals>();
        }
        
        [DbFunction("SkmDbContext", "fn_get_skmpay_order_info")]        
        public IQueryable<SkmpayOrderInfo> FnGetSkmpayOrderInfo(DateTime startDate, DateTime endDate)
        {
            var para1 = new ObjectParameter("startDate", startDate);
            var para2 = new ObjectParameter("endDate", endDate);

            return ((IObjectContextAdapter)this).ObjectContext.
                CreateQuery<SkmpayOrderInfo>("[SkmDbContext].[fn_get_skmpay_order_info] (@startDate, @endDate)", para1, para2);
        }

        [DbFunction("SkmDbContext", "fn_get_skm_online_deals")]
        public IQueryable<SkmOnlineDeals> FnGetSkmOnlineDeals(DateTime startDate, DateTime endDate)
        {
            var para1 = new ObjectParameter("startDate", startDate);
            var para2 = new ObjectParameter("endDate", endDate);

            return ((IObjectContextAdapter)this).ObjectContext.
                CreateQuery<SkmOnlineDeals>("[SkmDbContext].[fn_get_skm_online_deals] (@startDate, @endDate)", para1, para2);
        }

        [DbFunction("SkmDbContext", "fn_get_skm_curation_events")]
        public IQueryable<SkmCurationEvents> FnGetFnGetSkmCurationEvents(DateTime startDate, DateTime endDate)
        {
            var para1 = new ObjectParameter("startDate", startDate);
            var para2 = new ObjectParameter("endDate", endDate);

            return ((IObjectContextAdapter)this).ObjectContext.
                CreateQuery<SkmCurationEvents>("[SkmDbContext].[fn_get_skm_curation_events] (@startDate, @endDate)", para1, para2);
        }

        [DbFunction("SkmDbContext", "fn_get_skm_curation_deals")]
        public IQueryable<SkmCurationDeals> FnGetFnGetSkmCurationDeals(DateTime startDate, DateTime endDate)
        {
            var para1 = new ObjectParameter("startDate", startDate);
            var para2 = new ObjectParameter("endDate", endDate);

            return ((IObjectContextAdapter)this).ObjectContext.
                CreateQuery<SkmCurationDeals>("[SkmDbContext].[fn_get_skm_curation_deals] (@startDate, @endDate)", para1, para2);
        }
    }
}

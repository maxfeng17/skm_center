﻿using System.Data.Entity;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class MailLogDbContext : DbContextBase
    {
        public MailLogDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<MailLogDbContext>(null);
        }

        public DbSet<MailLog> MailLogEntities { get; set; }
        public DbSet<MailContentLog> MailContentLogEntities { get; set; }
       
    }
}

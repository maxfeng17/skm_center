﻿using System;
using System.Data.Entity;
using System.Transactions;
using log4net;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class FamiportDbContext : DbContextBase
    {
        /// <summary>
        /// 
        /// </summary>
        public FamiportDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<FamiportDbContext>(null);
        }

        public DbSet<FamilyNetOrderTicket> FamilyNetOrderTicketEntities { get; set; }
        public DbSet<FamilyNetPincodeTran> FamilyNetPincodeTranEntities { get; set; }        
        public DbSet<FamilyNetPincode> FamilyNetPincodeEntities { get; set; }
        public DbSet<FamilyNetPincodeDetail> FamilyNetPincodeDetailEntities { get; set; }
        public DbSet<FamilyNetEvent> FamilyNetEventEntities { get; set; }
        public DbSet<FamilyNetVerifyLog> FamilyNetVerifyLogEntities { get; set; }
        public DbSet<FamilyNetReturnLog> FamilyNetReturnLogEntities { get; set; }
        public DbSet<Famiport> FamiportEntities { get; set; }
        public DbSet<FamilyStoreInfo> FamilyStoreInfoEntities { get; set; }
        public DbSet<FamilyNetGetBarcodeLog> FamilyNetGetBarcodeLogEntities { get; set; }
        public DbSet<FamilyNetLockLog> FamilyNetLockLogEntities { get; set; }
        public DbSet<Peztemp> PeztempEntities { get; set; }
        public DbSet<FamilyNetOrderExgGate> FamilyNetOrderExgGateEntities { get; set; }
        public DbSet<ViewFamilyNetVerifyRetry> ViewFamilyNetVerifyRetryEntities { get; set; }
    }
}

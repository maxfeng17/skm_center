﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Models.OrderEntities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class OrderDbContext : DbContextBase
    {
        /// <summary>
        /// 
        /// </summary>
        public OrderDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<OrderDbContext>(null);
        }

        public DbSet<PreventingOversell> PreventingOversellEntities { get; set; }

        /// <summary>
        /// Shop Back 資訊
        /// </summary>
        public DbSet<ShopbackInfo> ShopBackInfoDbSet { get; set; }

        public DbSet<ViewShopbackInfo> ViewShopbackInfoDbSet { get; set; }

        public DbSet<ViewShopbackValidationInfo> ViewShopbackValidationInfoDbSet { get; set; }

        /// <summary>
        /// 呈現部份order資料的View
        /// </summary>
        public DbSet<ViewOrderEntity> ViewOrderEntityDbSet { get; set; }

        public DbSet<LineshopInfo> LineshopInfoDbSet { get; set; }

        public DbSet<ViewLineshopOrderinfo> ViewLineshopOrderinfoDbSet { get; set; }

        public DbSet<ViewLineshopOrderfinish> ViewLineshopOrderfinishDbSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ViewLineshopOrderinfo>()
                .HasKey(item => new { item.Guid });
            modelBuilder.Entity<ViewLineshopOrderfinish>()
                .HasKey(item => new { item.Guid });
        }

        public DbSet<AffiliatesInfo> AffiliatesInfoDbSet { get; set; }

        public DbSet<ViewAffiliatesInfo> ViewAffiliatesInfoDbSet { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.GameEntities;
using LunchKingSite.Core.Models.MarketingEntities;
using LunchKingSite.Core.Models.OrderEntities;


namespace LunchKingSite.SsBLL.DbContexts
{
    public class MarketingDbContext : DbContextBase
    {
        public MarketingDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<MarketingDbContext>(null);
        }

        /// <summary>
        /// Shop Back 資訊
        /// </summary>
        public DbSet<RushbuyTracking> RushBuyTrackingDbSet { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Models.Entities;

namespace LunchKingSite.SsBLL.DbContexts
{
    public class EventDbContext : DbContextBase
    {
        public EventDbContext()
            : base(nameOrConnectionString: "default")
        {
            Database.SetInitializer<EventDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PrizeDrawItem>().Property(o => o.WinningRate)
                .HasPrecision(6, 3)
                .HasColumnType("numeric");

            #region ExternalDealRelationStoreDisplayName

            modelBuilder.Entity<ExternalDealRelationStoreDisplayName>().Property(item => item.SkmStoreDisplayNameGuid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(0);
            modelBuilder.Entity<ExternalDealRelationStoreDisplayName>().Property(item => item.ExternalDealGuid)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .HasColumnOrder(1);

            #endregion ExternalDealRelationStoreDisplayName
        }

        public DbSet<PrizeDrawEvent> PrizeDrawEventEntities { get; set; }
        public DbSet<PrizeDrawItem> PrizeDrawItemEntities { get; set; }
        public DbSet<PrizeDrawRecord> PrizeDrawRecordEntities { get; set; }
        public DbSet<PrizePreventingOverdraw> PrizePreventingOverdrawEntities { get; set; }
        public DbSet<ViewPrizeExternalDeal> ViewPrizeExternalDealEntities { get; set; }
        public DbSet<ViewPrizeDrawRecord> ViewPrizeDrawRecordEntities { get; set; }
        public DbSet<ExternalDealIcon> ExternalDealIcons { get; set; }
        public DbSet<ExternalDealExhibitionCode> ExternalDealExhibitionCodes { get; set; }
        public DbSet<ViewExternalDealExhibitionCode> ViewExternalDealExhibitionCodes { get; set; }
        public DbSet<PrizeDrawItemPeriod> PrizeDrawItemPeriodEntities { get; set; }
        public DbSet<PrizeDrawRecordLog> PrizeDrawRecordLogEntities { get; set; }
        public DbSet<ExternalDealRelationStoreDisplayName> ExternalDealRelationStoreDisplayNameEntities { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL.Override;
using SubSonic;
using PaymentType = LunchKingSite.Core.PaymentType;
using log4net;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.SsBLL
{
    public class SSOrderProvider : IOrderProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private IPponProvider pp = ProviderFactory.Instance().GetProvider<IPponProvider>();
        private IHumanProvider hp = ProviderFactory.Instance().GetProvider<IHumanProvider>();
        private static readonly ILog log = LogManager.GetLogger(typeof(SSOrderProvider));

        #region OrderDetail

        public bool OrderDetailDelete(Guid guidOrderDetail)
        {
            DB.Destroy<OrderDetail>(OrderDetail.Columns.Guid, guidOrderDetail);
            return true;
        }

        public OrderDetail OrderDetailGet(Guid guidOrderDetail)
        {
            return DB.Get<OrderDetail>(guidOrderDetail);
        }

        public OrderDetail OrderDetailPartialRefundGet(Guid orderGuid)
        {
            return DB.Select().From(OrderDetail.Schema.TableName).Where(OrderDetail.Columns.OrderGuid).IsEqualTo(orderGuid)
                    .And(OrderDetail.Columns.ItemGuid).IsEqualTo(GlobalProperty.PartialRefundGuid).ExecuteSingle<OrderDetail>();
        }

        public int OrderDetailGetCount(Guid guidOrder)
        {
            return
                new InlineQuery(OrderDetail.Schema.Provider).ExecuteScalar<int>(
                    @"select count(1) from order_detail with(nolock) where order_guid='{0}' and " + GetOrderDetailTypeWhereSql(OrderDetailTypes.Regular, null), guidOrder,
                    (int)OrderDetailStatus.SystemEntry, 0);
        }

        public OrderDetailCollection OrderDetailGetList(Guid guidOrder)
        {
            string sql = @"select * from order_detail with(nolock) where order_guid=@oid ";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@oid", guidOrder, DbType.Guid);
            OrderDetailCollection data = new OrderDetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public OrderDetailCollection OrderDetailGetList(int pageStart, int pageLength, string orderBy, Guid guidOrder)
        {
            return OrderDetailGetList(pageStart, pageLength, orderBy, guidOrder, OrderDetailTypes.Regular);
        }

        public OrderDetailCollection OrderDetailGetList(int pageStart, int pageLengh, string orderBy, Guid guidOrder, OrderDetailTypes type)
        {
            string sql = @"select * from order_detail with(nolock) where order_guid=@oid ";

            if (type != OrderDetailTypes.All)
                sql += "and " + GetOrderDetailTypeWhereSql(type, null);

            sql += "order by " + GetOrderDetailOrderSql(orderBy, null);

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@oid", guidOrder, DbType.Guid);

            qc = SSHelper.MakePagable(qc, pageStart, pageLengh, GetOrderDetailOrderSql(orderBy, null));
            OrderDetailCollection data = new OrderDetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        public OrderDetailCollection OrderDetailGetListAggregate(Guid orderGuid)
        {
            return OrderDetailGetListAggregate(orderGuid, OrderDetailTypes.Regular);
        }

        public OrderDetailCollection OrderDetailGetListAggregate(Guid orderGuid, OrderDetailTypes odType)
        {
            string sql = string.Format(
                @"SELECT CAST ('{0}' AS UNIQUEIDENTIFIER) as GUID, CAST ('{1}' AS UNIQUEIDENTIFIER) as order_GUID, item_name, item_guid, item_unit_price, SUM(item_quantity) as item_quantity, SUM(total) as total,
                    STUFF((SELECT ',' + consumer_name FROM order_detail t1 with(nolock) WHERE t1.item_name=t2.item_name AND t1.order_GUID='{1}' FOR XML PATH('')),1,1,'') as consumer_name,
                    null as consumer_tele_ext, 0 as status, null as create_id, GETDATE() as create_time, null as modify_id, null as modify_time, item_id
                    FROM order_detail t2 with(nolock)
                    WHERE order_GUID='{1}' AND " + GetOrderDetailTypeWhereSql(odType, "t2")
                    + @"GROUP BY item_name, item_GUID, item_unit_price, item_id",
                Guid.Empty, orderGuid);
            return new InlineQuery(OrderDetail.Schema.Provider).ExecuteAsCollection<OrderDetailCollection>(sql);
        }

        public bool OrderDetailSet(OrderDetail od)
        {
            if (od.IsDirty)
            {
                if (od.CreateId.Length > OrderDetail.Schema.GetColumn(OrderDetail.Columns.CreateId).MaxLength)
                    od.CreateId = od.CreateId.Substring(0, OrderDetail.Schema.GetColumn(OrderDetail.Columns.CreateId).MaxLength);
                DB.Save<OrderDetail>(od);
            }
            return true;
        }

        private string GetOrderDetailTypeWhereSql(OrderDetailTypes type, string tableAlias)
        {
            string sql = string.Empty;
            if (string.IsNullOrEmpty(tableAlias))
                tableAlias = OrderDetail.Schema.TableName;

            switch (type)
            {
                case OrderDetailTypes.Regular:
                case OrderDetailTypes.System:
                    sql += "(" + tableAlias + ".status&" + (int)OrderDetailStatus.SystemEntry + (type == OrderDetailTypes.Regular ? "=0" : ">0") + ") ";
                    break;

                default:
                    break;
            }

            return sql;
        }

        private string GetOrderDetailOrderSql(string orderBy, string tableAlias)
        {
            if (string.IsNullOrEmpty(tableAlias))
                tableAlias = OrderDetail.Schema.TableName;
            return "(" + tableAlias + ".status&" + (int)OrderDetailStatus.SystemEntry + ")" +
                   (string.IsNullOrEmpty(orderBy) ? "" : "," + orderBy);
        }

        public int OrderDetailGetOrderedQuantityByBid(Guid businessHourGuid, bool includeRefund = false)
        {
            //**企劃確認購買人(份)數不做退貨完成後數量處理, 但要在此修正上線前的檔次都要照原邏輯執行**
            string refundQuantitySQL = " + COUNT(CASE WHEN ctl.status in (3, 4) AND ctl.special_status & 1 = 0 THEN 1 END) "; //退貨數量SQL

            string strSql = @"
            SELECT
            COUNT(CASE WHEN ctl.status in (0, 1) THEN 1 END) " +
            (includeRefund ? refundQuantitySQL : string.Empty) +
            @" + COUNT(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN 1 END) AS quantity
            FROM [order] AS o WITH(NOLOCK)
            INNER JOIN cash_trust_log AS ctl WITH(NOLOCK) ON ctl.order_guid = o.guid
            AND ctl.order_classification = 1
            AND ctl.special_status <> 16
            INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
            WHERE o.order_status & 8 > 0 and bh.GUID = @bid
            GROUP BY business_hour_guid,o.parent_order_id
            ";

            var odc = new InlineQuery(Order.Schema.Provider).ExecuteScalar<int?>(strSql, businessHourGuid.ToString());

            return (odc != null) ? odc.Value : 0;
        }

        public int OrderDetailGetIfPartialCancel(Guid oid)
        {
            string strSQL = "select isnull(SUM(item_quantity),0) from order_detail with(nolock) where (item_name =N'部分退貨' or status&2=0) and  order_guid=@oid";
            int odc = new InlineQuery(OrderDetail.Schema.Provider).ExecuteScalar<int>(strSQL, oid.ToString());
            return odc;
        }

        public OrderDetailCollection OrderDetailGetListByStatus(Guid guidOrder)
        {
            string sql = @"select * from order_detail with(nolock) where order_guid=@oid and status=0";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@oid", guidOrder, DbType.Guid);
            OrderDetailCollection data = new OrderDetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public OrderDetailCollection OrderDetailGetListByBid(Guid bid)
        {
            string sql = @"SELECT * FROM order_detail where order_GUID IN
            (SELECT [guid] from view_ppon_order where business_hour_guid=@bid)
            ";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            OrderDetailCollection data = new OrderDetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int OrderDetailSetList(OrderDetailCollection ods)
        {
            return DB.SaveAll(ods);
        }

        #endregion OrderDetail

        #region Order

        public Order OrderGet(string column, object value)
        {
            return DB.Get<Order>(column, value);
        }

        public Order OrderGetByOrderId(string orderId)
        {
            return DB.Get<Order>(Order.Columns.OrderId, orderId);
        }

        public OrderCollection OrderGet(List<Guid> guids)
        {
            return DB.SelectAllColumnsFrom<Order>()
                .NoLock()
                .Where(Order.Columns.Guid)
                .In(guids)
                .ExecuteAsCollection<OrderCollection>();
        }

        public Order OrderGet(Guid guidOrder)
        {
            return DB.Get<Order>(guidOrder);
        }

        public bool OrderSet(Order o)
        {
            DB.Save<Order>(o);
            return true;
        }

        public Guid OrderGuidGetById(string orderId)
        {
            return DB.Select(Order.Columns.Guid)
                .From<Order>().NoLock()
                .Where(Order.Columns.OrderId)
                .IsEqualTo(orderId).ExecuteSingle<Guid>();
        }

        public List<Guid> OrderGuidListGetByUser(int userId)
        {
            return DB.Select(Order.Columns.Guid)
                .From<Order>().NoLock()
                .Where(Order.Columns.UserId)
                .IsEqualTo(userId)
                .OrderDesc(Order.Columns.CreateTime)
                .ExecuteTypedList<Guid>();
        }

        public bool OrderSetId(Guid orderGuid, string orderId)
        {
            return DB.Update<Order>().Set(Order.OrderIdColumn).EqualTo(orderId).Where(Order.GuidColumn).IsEqualTo(orderGuid).Execute() > 0;
        }

        public bool OrderDelete(Guid odrGuid)
        {
            Order odr = DB.Get<Order>(odrGuid);
            if (odr.IsLoaded)
            {
                StringBuilder sb = new StringBuilder();
                List<SqlQuery> queries = new List<SqlQuery>();
                queries.Add(new Delete().From(OrderDetail.Schema).Where(OrderDetail.OrderGuidColumn).IsEqualTo(odrGuid));
                queries.Add(new Delete().From(GroupOrder.Schema).Where(GroupOrder.OrderGuidColumn).IsEqualTo(odrGuid));
                queries.Add(new Delete().From(Order.Schema).Where(Order.GuidColumn).IsEqualTo(odrGuid));
                SqlQuery.ExecuteTransaction(queries);
            }

            return true;
        }

        public OrderCollection OrderGetListPaging(int pageStart, int pageLength, OrderStatus statusMask, OrderStatus expectStatus, string orderBy, params string[] filter)
        {
            string defOrderBy = Order.Columns.Guid;
            QueryCommand qc = GetOrderWhereQC(statusMask, expectStatus, filter);
            qc.CommandSql = "select * from " + Order.Schema.Provider.DelimitDbName(Order.Schema.TableName) + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            OrderCollection ocol = new OrderCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public OrderCollection OrderGetListPaging(int pageStart, int pageLength, OrderStatus status, string orderBy, params string[] filter)
        {
            return OrderGetListPaging(pageStart, pageLength, status, status, orderBy, filter);
        }

        public OrderCollection GetExpriedOrderList()
        {
            //todo:找出每天重複做退不掉的退貨，看是否要增加排除條件
            string sql = @"select * from [order] o with(NOLOCK)
inner join (
select ctl.order_guid,
sum(case when bh.business_hour_status & 8388608 > 0 and c.is_reservation_lock = 1 then 1 else 0 end) as lock_count 
from business_hour bh with(NOLOCK)
inner join cash_trust_log ctl with(NOLOCK) on ctl.business_hour_guid = bh.GUID
inner join coupon c with(NOLOCK) on ctl.coupon_id = c.id
inner join group_order g with(NOLOCK) on ctl.business_hour_guid= g.business_hour_guid
where ((bh.business_hour_status & 8388608 > 0 and g.status & 4194304 = 0  and g.status & 134217728 = 0) or bh.business_hour_status & 8388608 = 0) and bh.business_hour_deliver_time_e < '9999/1/1' and
(
(changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/1' and business_hour_deliver_time_e < '2015/5/18' and  convert(char(10),dateadd(day,30,business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
or
(changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/18' and convert(char(10),dateadd(day,@refundScashExpriedDays,business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
or 
(changed_expire_date is NOT NULL and changed_expire_date>='2015/5/1' and changed_expire_date < '2015/5/18' and  convert(char(10),dateadd(day,30,changed_expire_date),111)<=convert(char(10),getdate(),111))
or
(changed_expire_date is NOT NULL and changed_expire_date>='2015/5/18' and  convert(char(10),dateadd(day,@refundScashExpriedDays,changed_expire_date),111)<=convert(char(10),getdate(),111))
)
and ((c.is_reservation_lock = 0 and bh.business_hour_status & 8388608 = 0) or bh.business_hour_status & 8388608 > 0)
and ctl.status < 2
and ctl.trust_id not in 
	(select rfr.trust_id from return_form_refund rfr with(NOLOCK)
	inner join return_form r with(NOLOCK) on rfr.return_form_id = r.id
	where r.progress_status <> 4 
	)
and g.status&17957888=0 
and ctl.order_classification <> 8
and ctl.order_classification <> 11
and ctl.lcash = 0
group by ctl.order_guid
) x on x.order_guid = o.GUID
where order_status&8>0 and x.lock_count = 0";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.CommandTimeout = 600;
            qc.AddParameter("@refundScashExpriedDays", config.SysRefundScashExpriedDays, DbType.Int32);
            OrderCollection data = new OrderCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        /// <summary>
        /// 超取超取付款未取貨的訂單
        /// </summary>
        /// <returns></returns>
        public OrderCollection GetNoReceiptPaidIspOrder()
        {
            string sql = string.Format(@"
select o.guid, o.order_id, o.order_status & 8 from [order] o with(nolock)
inner join order_ship os with(nolock) on o.GUID = os.order_guid
inner join cash_trust_log ctl with(nolock) on os.order_guid = ctl.order_guid
inner join order_product_delivery opd on opd.order_guid = o.GUID
left join return_form rf with(nolock) on rf.order_guid = o.guid
where os.is_receipt = 0 and ctl.status in (0,1,2) and rf.id is null
and opd.product_delivery_type in (1,2)
");
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);

            OrderCollection data = new OrderCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public OrderCollection GetExpriedOrderListOverThreeDays()
        {
            //todo:找出每天重複做退不掉的退貨，看是否要增加排除條件
            string sql = string.Format(@"select * from [order] o with(NOLOCK)
                        inner join (
                        select ctl.order_guid,
                        sum(case when bh.business_hour_status & 8388608 > 0 and c.is_reservation_lock = 1 then 1 else 0 end) as lock_count 
                        from (
	                        select business_hour_guid from deal_property WITH (NOLOCK)
		                        where (deal_property.is_promotion_deal = 1 or deal_property.is_exhibition_deal = 1)
	                    ) dp
                        inner join business_hour bh with(NOLOCK) ON dp.business_hour_guid = bh.guid
                        inner join cash_trust_log ctl with(NOLOCK) on ctl.business_hour_guid = bh.GUID
                        inner join coupon c with(NOLOCK) on c.order_detail_id = ctl.order_detail_guid and c.sequence_number = ctl.coupon_sequence_number and ctl.coupon_id = c.id
                        inner join group_order g with(NOLOCK) on ctl.business_hour_guid= g.business_hour_guid
                        where ((bh.business_hour_status & 8388608 > 0 and g.status & 4194304 = 0) or bh.business_hour_status & 8388608 = 0) and bh.business_hour_deliver_time_e < '9999/1/1' and
                        (
                        (changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/1' and business_hour_deliver_time_e < '2015/5/18' and  convert(char(10),dateadd(day,30,business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
                        or
                        (changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/18' and convert(char(10),dateadd(day,{0},business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
                        or 
                        (changed_expire_date is NOT NULL and changed_expire_date>='2015/5/1' and changed_expire_date < '2015/5/18' and  convert(char(10),dateadd(day,30,changed_expire_date),111)<=convert(char(10),getdate(),111))
                        or
                        (changed_expire_date is NOT NULL and changed_expire_date>='2015/5/18' and  convert(char(10),dateadd(day,{0},changed_expire_date),111)<=convert(char(10),getdate(),111))
                        )
                        and ((c.is_reservation_lock = 0 and bh.business_hour_status & 8388608 = 0) or bh.business_hour_status & 8388608 > 0)
                        and ctl.status < 2
                        and ctl.trust_id not in 
	                        (select rfr.trust_id from return_form_refund rfr with(NOLOCK)
	                        inner join return_form r with(NOLOCK) on rfr.return_form_id = r.id
	                        where r.progress_status <> 4 
	                        )
                        --and g.status&17957888=0 
                        and ctl.order_classification <> 8
                        and ctl.lcash = 0
                        group by ctl.order_guid
                        ) x on x.order_guid = o.GUID
                        where order_status&8>0 and x.lock_count = 0", config.ExhibitionDealExpiredVerifiedExpriedDays);
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);

            OrderCollection data = new OrderCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public OrderCollection GetHiLifeExpriedOrderList()
        {
            //todo:找出每天重複做退不掉的退貨，看是否要增加排除條件
            string sql = @"select * from [order] o with(NOLOCK)
                            inner join (
                            select ctl.order_guid,
                            sum(case when bh.business_hour_status & 8388608 > 0 and c.is_reservation_lock = 1 then 1 else 0 end) as lock_count 
                            from business_hour bh with(NOLOCK)
                            inner join cash_trust_log ctl with(NOLOCK) on ctl.business_hour_guid = bh.GUID
                            inner join coupon c with(NOLOCK) on ctl.coupon_id = c.id
                            inner join group_order g with(NOLOCK) on ctl.business_hour_guid= g.business_hour_guid
                            where bh.business_hour_status & 8388608 > 0 and g.status & 134217728 > 0 and bh.business_hour_deliver_time_e < '9999/1/1' and
                            (
                            (changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/1' and business_hour_deliver_time_e < '2015/5/18' and  convert(char(10),dateadd(day,30,business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
                            or
                            (changed_expire_date is NULL and business_hour_deliver_time_e>='2015/5/18' and convert(char(10),dateadd(day,@refundScashExpriedDays,business_hour_deliver_time_e),111)<=convert(char(10),getdate(),111))
                            or 
                            (changed_expire_date is NOT NULL and changed_expire_date>='2015/5/1' and changed_expire_date < '2015/5/18' and  convert(char(10),dateadd(day,30,changed_expire_date),111)<=convert(char(10),getdate(),111))
                            or
                            (changed_expire_date is NOT NULL and changed_expire_date>='2015/5/18' and  convert(char(10),dateadd(day,@refundScashExpriedDays,changed_expire_date),111)<=convert(char(10),getdate(),111))
                            )
                            and ((c.is_reservation_lock = 0 and bh.business_hour_status & 8388608 = 0) or bh.business_hour_status & 8388608 > 0)
                            and ctl.status < 2
                            and ctl.trust_id not in 
	                            (select rfr.trust_id from return_form_refund rfr with(NOLOCK)
	                            inner join return_form r with(NOLOCK) on rfr.return_form_id = r.id
	                            where r.progress_status <> 4 
	                            )
                            and g.status&17957888=0  --1024+131072+1048576+16777216
                            and ctl.order_classification <> 8
                            and ctl.order_classification <> 11
                            and ctl.lcash = 0
                            group by ctl.order_guid
                            ) x on x.order_guid = o.GUID
                            where order_status&8>0 and x.lock_count = 0";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.CommandTimeout = 600;
            qc.AddParameter("@refundScashExpriedDays", config.SysRefundScashExpriedDays, DbType.Int32);
            OrderCollection data = new OrderCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewPponOrderCollection GetNoUseCouponOrderList(DateTime d)
        {
            string sql = string.Format(@"select vpo.* from view_ppon_order vpo with(nolock)
inner join(
select ctl.order_guid from cash_trust_log ctl with(nolock)
inner join group_order g with(nolock) on g.business_hour_guid = ctl.business_hour_guid
inner join business_hour bh with(nolock) on bh.GUID = ctl.business_hour_guid
where settlement_time BETWEEN DATEADD(MINUTE,-30,'{0}') and '{0}' and ctl.status < 2
and g.status&17956864=0  
and ctl.trust_id not in 
(select rfr.trust_id from return_form_refund rfr with(NOLOCK)
inner join return_form r with(NOLOCK) on rfr.return_form_id = r.id
where r.progress_status <> 4 
) and ctl.order_classification = 11
group by ctl.order_guid
) c on c.order_guid = vpo.GUID
where vpo.order_status & 8 > 0", d.ToString("yyyy/MM/dd HH:mm:ss"));
            QueryCommand qc = new QueryCommand(sql, ViewPponOrder.Schema.Provider.Name);

            ViewPponOrderCollection data = new ViewPponOrderCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int OrderGetCount(params string[] filter)
        {
            return OrderGetCount(OrderStatus.Nothing, OrderStatus.Nothing, filter);
        }

        public bool OrderSetStatus(Guid orderGuid, string userName, OrderStatus status, bool setIt)
        {
            if (userName != null && userName.Contains('@'))
            {
                userName = userName.Remove(userName.IndexOf('@'));
            }

            string sql = "update " + Order.Schema.Provider.DelimitDbName(Order.Schema.TableName) +
                 " set " + Order.Columns.OrderStatus + " = (" + Order.Columns.OrderStatus;
            string userParam = null;
            if (setIt)
            {
                sql += "|" + status.ToString("d");
                userParam = userName;
            }
            else
            {
                /* sql int type is signed, so max of uint in .net is double of its max value in sql, 
                 * therefore we right shift 1 bit (= divide by 2) first and subtract OrderStatus.Locked to unmask it
                 */
                sql += "&cast(" + ((uint.MaxValue >> 1) - (uint)status).ToString() + " as int)";
            }
            sql += ")";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(userName))
            {
                qc.CommandSql += ", " + Order.Columns.AccessLock + "=@up";
                qc.AddParameter("@up", userParam, DbType.String);
            }

            qc.CommandSql += " where " + Order.Columns.Guid + "=@id";
            qc.AddParameter("@id", orderGuid, DbType.Guid);
            DataService.ExecuteScalar(qc);

            return true;
        }

        public int OrderUnlockAll()
        {
            new InlineQuery().Execute("update [order] set order_status=(order_status & " + (~(int)OrderStatus.Locked).ToString() + ") where (order_status & " + ((uint)OrderStatus.Locked).ToString() + ") > 0");
            return 0;
        }

        public int OrderGetCount(OrderStatus statusMask, OrderStatus expectStatus, params string[] filter)
        {
            QueryCommand qc = GetOrderWhereQC(statusMask, expectStatus, filter);
            qc.CommandSql = "select count(1) from " + Order.Schema.Provider.DelimitDbName(Order.Schema.TableName) + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public void OrderUpdateSumTotal(Guid orderGuid)
        {
            decimal sumTotal = new Select(Aggregate.Sum(OrderDetail.TotalColumn)).From(OrderDetail.Schema).Where(OrderDetail.OrderGuidColumn).IsEqualTo(orderGuid).ExecuteScalar<decimal>();
            DB.Update<Order>().SetExpression(Order.TotalColumn).EqualTo(sumTotal).SetExpression(Order.SubtotalColumn).EqualTo(sumTotal).Where(Order.GuidColumn).IsEqualTo(orderGuid).ExecuteScalar();
        }

        public double OrderGetSumTotal(params string[] filter)
        {
            Query qry = SSHelper.GetQueryByFilterOrder(new Query(Order.Schema), null, filter);
            object ret = qry.GetSum(Order.Columns.Total);
            return (ret is DBNull) ? 0.0 : Convert.ToDouble((decimal)ret);
        }
        /// <summary>
        /// 取得會員帶來的營業額，扣折價券
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int OrderGetSuccessTurnover(int userId)
        {
            string sql = @"
    select isnull(sum(ctl.amount - ctl.discount_amount),0) from [order] o with(nolock) 
    inner join cash_trust_log ctl with(nolock) on ctl.order_guid = o.GUID
    where o.user_id = @userId and (o.order_status & 8) > 0 and (o.order_status & 512) = 0
";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            int result = (int)DataService.ExecuteScalar(qc);
            return result;
        }
        /// <summary>
        /// 使用者的未成單數
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public int FailOrderGetCountByUser(int userId)
        {
            string sql = @"select count(*) from [order] with(nolock) where [order].user_id = @userId and ([order].order_status & 8) = 0 ";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            int result = (int)DataService.ExecuteScalar(qc);
            return result;
        }

        protected QueryCommand GetOrderWhereQC(OrderStatus statusMask, OrderStatus expectStatus, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", Order.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(Order.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            if (statusMask != OrderStatus.Nothing)
            {
                if (qc.Parameters.Count == 0)
                    qc.CommandSql += SqlFragment.WHERE;
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += " (" + Order.Columns.OrderStatus + " & @statusMask ) = @expectStatus";
                qc.AddParameter("@statusMask", statusMask, DbType.Int32);
                qc.AddParameter("@expectStatus", expectStatus, DbType.Int32);
            }
            return qc;
        }

        public void OrderSetPartialCancel(Guid oid, bool ispartial)
        {
            string strSQL = "update [order] set order_status=case when order_status&" + (int)OrderStatus.PartialCancel + ">0 then order_status" +
                (ispartial ? "|" : "&~") + (int)OrderStatus.PartialCancel + " else order_status" + (ispartial ? ("|" + (int)OrderStatus.PartialCancel) : string.Empty) + " end  where GUID=@guid";
            QueryCommand qc = new QueryCommand(strSQL, Order.Schema.Provider.Name);
            qc.AddParameter("@guid", oid, DbType.Guid);
            DataService.ExecuteScalar(qc);
            if (ispartial)
            {
                string strSQL2 = "update coupon set available=1 where id in (select id from view_coupon_list_sequence where GUID=@guid2 and status=0)";
                QueryCommand qc2 = new QueryCommand(strSQL2, Order.Schema.Provider.Name);
                qc2.AddParameter("@guid2", oid, DbType.Guid);
                DataService.ExecuteScalar(qc2);
            }
        }

        public DataTable MemberOrderListGet(int pageStart, int pageLength, string userName, string defOrderBy, bool filterFunds, int famCode, string filter)
        {
            string sql = @" select o.order_id, o.GUID,o.member_name,  o.seller_name, o.seller_GUID, o.create_time, o.subtotal, bh.business_hour_order_time_s, bh.business_hour_order_time_e, pt.trans_id, o.order_status, osl.id as log_id, osl.status as log_status,bh.business_hour_deliver_time_e, expChg.bh_changed_expire_date, expChg.store_changed_expire_date, expChg.seller_close_down_date, expChg.store_close_down_date, seller.seller_name as store_name,
dp.label_icon_list,dp.city_list,i.item_name,go1.business_hour_guid" + ((filter != "All") ? ",ctl2.trust_id " : ",'' as trust_id ") +
 @"FROM [order] o with(nolock)
    inner join member m on m.unique_id = o.user_id
    left JOIN group_order go1 with(nolock) ON go1.order_guid = o.parent_order_id
    left join business_hour bh with(nolock) ON bh.GUID = go1.business_hour_guid
    LEFT JOIN payment_transaction pt with(nolock) ON pt.order_guid = o.GUID AND pt.trans_type = 0 AND pt.status & 15 = 4
    left JOIN (
        select osl.id, osl.status, order_guid
        from order_return_list osl with(nolock)
        where osl.id = (select top(1) id from order_return_list with(nolock) where order_guid = osl.order_guid order by modify_time desc)
    ) osl ON osl.order_guid = o.GUID
    LEFT JOIN view_ppon_close_down_expiration_change expChg with(nolock)
        ON o.GUID = expChg.order_guid
    LEFT JOIN seller with(nolock)
        ON expChg.store_guid = seller.Guid
left join deal_property dp with(nolock) on dp.business_hour_guid = go1.business_hour_guid
left join item i with(nolock) on i.business_hour_guid = go1.business_hour_guid";

            if (filter != "All")
            {
                sql += @" CROSS APPLY
                (
                select top(1) ctl.trust_id from cash_trust_log ctl with(NOLOCK)
                inner join deal_property dp2 with(NOLOCK) on dp2.business_hour_guid = ctl.business_hour_guid
                left join (
                select * from return_form_refund r with(NOLOCK)
                inner join return_form rf with(NOLOCK) on r.return_form_id = rf.id
                where r.return_form_id = (select top(1) rfr.return_form_id from return_form_refund rfr with(NOLOCK) where rfr.trust_id = r.trust_id order by rfr.return_form_id desc)
                ) r on r.trust_id = ctl.trust_id
                where ctl.order_guid = o.GUID";
            }

            switch (filter)
            {
                case "All":
                    sql += " WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "NotUsed":
                    sql += " and ctl.status in (0,1) and ((bh.changed_expire_date is null and CONVERT(char(10), bh.business_hour_deliver_time_e,111) >= CONVERT(char(10), GetDate(),111)) or (bh.changed_expire_date is not null and CONVERT(char(10), bh.changed_expire_date,111) >= CONVERT(char(10), GetDate(),111))) and (r.progress_status not in (1,5) or r.progress_status is null) and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL and (o.order_status & 8) > 0";
                    break;
                case "Expired":
                    sql += " and ctl.status in (0,1) and ((bh.changed_expire_date is null and CONVERT(char(10), bh.business_hour_deliver_time_e,111) < CONVERT(char(10), GetDate(),111)) or (bh.changed_expire_date is not null and CONVERT(char(10), bh.changed_expire_date,111) < CONVERT(char(10), GetDate(),111))) and (r.progress_status not in (1,5) or r.progress_status is null) and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL and (o.order_status & 8) > 0";
                    break;
                case "Verified":
                    sql += " and ctl.status = 2 and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "Returning":
                    sql += " and r.progress_status in (1,5)) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "Returned":
                    sql += " and r.is_refunded = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
            }

            if (filterFunds)
            {
                sql += " and i.item_price > 0";
            }

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);


            if (!string.IsNullOrEmpty(defOrderBy))
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by o." + defOrderBy + " desc";

            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            DataTable dt = new DataTable();
            IDataReader idr = DataService.GetReader(qc);
            using (idr)
                dt.Load(idr);

            return dt;
        }

        public int MemberOrderListGetCount(string userName, bool filterFunds, int famCode, string filter)
        {
            string sql = @" select count(1)
 FROM [order] o with(nolock)
    inner join member m on m.unique_id = o.user_id
    left JOIN group_order go1 with(nolock) ON go1.order_guid = o.parent_order_id
    left join business_hour bh with(nolock) ON bh.GUID = go1.business_hour_guid
    LEFT JOIN payment_transaction pt with(nolock) ON pt.order_guid = o.GUID AND pt.trans_type = 0 AND pt.status & 15 = 4
    left JOIN (
        select osl.id, osl.status, order_guid
        from order_return_list osl with(nolock)
        where osl.id = (select top(1) id from order_return_list with(nolock) where order_guid = osl.order_guid order by modify_time desc)
    ) osl ON osl.order_guid = o.GUID
    LEFT JOIN view_ppon_close_down_expiration_change expChg with(nolock)
        ON o.GUID = expChg.order_guid
    LEFT JOIN seller with(nolock)
        ON expChg.store_guid = seller.Guid
left join deal_property dp with(nolock) on dp.business_hour_guid = go1.business_hour_guid
left join item i with(nolock) on i.business_hour_guid = go1.business_hour_guid";

            if (filter != "All")
            {
                sql += @" CROSS APPLY
                (
                select top(1) ctl.trust_id from cash_trust_log ctl with(NOLOCK)
                inner join deal_property dp2 with(NOLOCK) on dp2.business_hour_guid = ctl.business_hour_guid
                left join (
                select * from return_form_refund r with(NOLOCK)
                inner join return_form rf with(NOLOCK) on r.return_form_id = rf.id
                where r.return_form_id = (select top(1) rfr.return_form_id from return_form_refund rfr with(NOLOCK) where rfr.trust_id = r.trust_id order by rfr.return_form_id desc)
                ) r on r.trust_id = ctl.trust_id
                where ctl.order_guid = o.GUID";
            }

            switch (filter)
            {
                case "All":
                    sql += " WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "NotUsed":
                    sql += " and ctl.status in (0,1) and ((bh.changed_expire_date is null and CONVERT(char(10), bh.business_hour_deliver_time_e,111) >= CONVERT(char(10), GetDate(),111)) or (bh.changed_expire_date is not null and CONVERT(char(10), bh.changed_expire_date,111) >= CONVERT(char(10), GetDate(),111))) and (r.progress_status not in (1,5) or r.progress_status is null) and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL and (o.order_status & 8) > 0";
                    break;
                case "Expired":
                    sql += " and ctl.status in (0,1) and ((bh.changed_expire_date is null and CONVERT(char(10), bh.business_hour_deliver_time_e,111) < CONVERT(char(10), GetDate(),111)) or (bh.changed_expire_date is not null and CONVERT(char(10), bh.changed_expire_date,111) < CONVERT(char(10), GetDate(),111))) and (r.progress_status not in (1,5) or r.progress_status is null) and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL and (o.order_status & 8) > 0";
                    break;
                case "Verified":
                    sql += " and ctl.status = 2 and dp2.delivery_type = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "Returning":
                    sql += " and r.progress_status in (1,5)) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
                case "Returned":
                    sql += " and r.is_refunded = 1) ctl2 WHERE m.user_name = @userName AND o.parent_order_id IS not NULL ";
                    break;
            }

            if (filterFunds)
            {
                sql += " and i.item_price > 0";
            }

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);

            return (int)DataService.ExecuteScalar(qc);
        }

        public DataTable MemberLunchKingOrderListGet(int pageStart, int pageLength, string userName, string defOrderBy)
        {
            string sql = @"select
                            o.order_id
                            , o.GUID
                            , o.member_name
                            , o.seller_name
                            , o.seller_GUID
                            , o.create_time
                            , o.subtotal
                            , o.order_status
                            FROM [order] o
                            inner join member m on m.unique_id = o.user_id
                            WHERE m.user_name = @userName  AND o.parent_order_id IS null
                            AND o.order_status & 8 > 0 AND o.order_status & 512 = 0 ";

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);

            if (!string.IsNullOrEmpty(defOrderBy))
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by o." + defOrderBy + " desc";

            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            DataTable dt = new DataTable();
            IDataReader idr = DataService.GetReader(qc);
            using (idr)
                dt.Load(idr);

            return dt;
        }

        public int MemberLunchKingOrderListGetCount(string userName)
        {
            string sql = @" SELECT count(1) from [order] o
                            inner join member m on m.unique_id = o.user_id
                            LEFT join payment_transaction pt ON pt.order_guid = o.GUID AND pt.trans_type = 0 AND pt.status & 15 = 4
                            left JOIN (
                                select max(osl.id) as id, max(osl.status) as status, order_guid
                                from order_status_log osl
                                GROUP BY order_guid
                            ) osl ON osl.order_guid = o.GUID
                            where o.parent_order_id IS null AND m.user_name = @userName
                            AND o.order_status & 8 > 0 AND o.order_status & 512 = 0 ";

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);

            return (int)DataService.ExecuteScalar(qc);
        }

        public List<OrderQuantityAmount> OrderCashTrustLogGetQuantityAmount(IEnumerable<Guid> bid)
        {
            string sql = @"SELECT ctl.business_hour_guid
                            , COUNT(CASE WHEN ctl.status in (0, 1, 6, 7) THEN 1 END)
                            + COUNT(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN 1 END)
                            + COUNT(CASE WHEN ctl.modify_time > bh.business_hour_order_time_e AND ctl.status in (3, 4) AND ctl.special_status & 1 = 0 THEN 1 END) AS quantity --結檔後才退貨也算入
                            , SUM(CASE WHEN ctl.status in (0, 1, 6, 7) THEN ctl.amount ELSE 0 END)
                            + SUM(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN ctl.amount ELSE 0 END)
                            + SUM(CASE WHEN ctl.modify_time > bh.business_hour_order_time_e AND ctl.status in (3, 4) AND ctl.special_status & 1 = 0 THEN ctl.amount ELSE 0 END) AS amount
                            FROM [order] AS o WITH(NOLOCK)
                            INNER JOIN cash_trust_log AS ctl WITH(NOLOCK) ON ctl.order_guid = o.guid  AND ctl.special_status <> 16 --排除運費
                            INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
                            WHERE ((o.order_status & 8 > 0) or (o.order_status & 2097152 > 0 and o.order_status & 512 = 0) or (o.order_status & 4194304 > 0 and o.order_status & 512=0))
                            AND ctl.business_hour_guid IN ('" + string.Join("','", bid) + @"')
                            GROUP BY business_hour_guid";

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);

            List<OrderQuantityAmount> result = new List<OrderQuantityAmount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    OrderQuantityAmount deal = new OrderQuantityAmount
                    {
                        BusinessHourGuid = dr.GetGuid(0),
                        Quantity = dr.GetInt32(1),
                        Amount = dr.GetInt32(2)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public DataTable GetDataTableOrderPaytableAlert(string queryStartTime, string queryEndTime)
        {
            //檢查上一週(週一~週日)已填母檔出貨回覆日，但退貨份數已超過結檔份數的30%的檔次。每檔僅發送一次。

            string sql = string.Format(@"
;with BIDtotal as (
select bh.GUID 'bid',item.item_name,count(bh.guid) 'total',dp.unique_id
from cash_trust_log ctl 
join [order] o with(nolock) on o.GUID=ctl.order_guid
join order_product op with(nolock) on o.guid=op.order_guid
join business_hour bh with(nolock) on bh.GUID = ctl.business_hour_guid
join item with(nolock) on item.business_hour_guid=bh.guid
JOIN dbo.deal_property dp with(nolock) ON dp.business_hour_guid = bh.GUID
where
bh.business_hour_deliver_time_e>=@queryStartTime and bh.business_hour_deliver_time_e <= @queryEndTime
and dp.delivery_type = 2
group by bh.GUID,item.item_name,dp.unique_id
),
BIDreturn as (
select bh.GUID 'bid',count(bh.guid) 'bidreturn'
from cash_trust_log ctl 
join [order] o with(nolock) on o.GUID=ctl.order_guid
join order_product op with(nolock) on o.guid=op.order_guid
join business_hour bh with(nolock) on bh.GUID = ctl.business_hour_guid
join item with(nolock) on item.business_hour_guid=bh.guid
JOIN dbo.deal_property dp with(nolock) ON dp.business_hour_guid = bh.GUID
where
bh.business_hour_deliver_time_e>=@queryStartTime and bh.business_hour_deliver_time_e <= @queryEndTime
and dp.delivery_type = 2
and (op.is_returning =1 or op.is_returned=1)
group by bh.GUID
)
select BIDtotal.*,isnull(BIDreturn.bidreturn,0) 'bidreturn' from BIDtotal left join BIDreturn on BIDreturn.bid=BIDtotal.bid
");

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryStartTime, queryEndTime))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable OrderGetSubtotalByOrderDate(DateTime dateStart, DateTime dateEnd, decimal amount, bool takeCeiling)
        {
            string sql = @" SELECT " + Order.Columns.UserId + @", " + (takeCeiling ? "cast(ceiling(CAST(SUM(" + Order.Columns.Subtotal + @") AS float) /@amount) as int)" : "cast(CAST(SUM(" + Order.Columns.Subtotal + @") AS float) /@amount as int)") + " as total FROM [" + Order.Schema.TableName + @"] with(nolock)
                            WHERE " + Order.Columns.CreateTime + @" between @dateStart AND @dateEnd
                            AND " + Order.Columns.OrderStatus + @" & " + (int)OrderStatus.Complete + @" > 0
                            AND " + Order.Columns.OrderStatus + @" & " + (int)OrderStatus.Cancel + @" = 0
                            AND " + Order.Columns.Subtotal + @" > 0 
                            GROUP BY " + Order.Columns.UserId + @"";

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, amount, dateStart.ToString("yyyy-MM-dd HH:mm:ss.000"), dateEnd.ToString("yyyy-MM-dd HH:mm:ss.000")))
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion Order

        #region OrderProductDelivery
        public OrderProductDelivery OrderProductDeliveryGetByOrderGuid(Guid oid)
        {
            return DB.SelectAllColumnsFrom<OrderProductDelivery>()
                .Where(OrderProductDelivery.Columns.OrderGuid).IsEqualTo(oid)
                .ExecuteSingle<OrderProductDelivery>();
        }

        public OrderProductDeliveryCollection OrderProductDeliveryGetListByOrderGuid(List<Guid> oids)
        {
            OrderProductDeliveryCollection infos = new OrderProductDeliveryCollection();
            List<Guid> tempIds = oids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<OrderProductDelivery>()
                        .Where(OrderProductDelivery.Columns.OrderGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<OrderProductDeliveryCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public bool OrderProductDeliveryCollectionBulkInsert(OrderProductDeliveryCollection opdCol)
        {
            return DB.BulkInsert(opdCol);
        }

        public OrderProductDelivery OrderProductDeliveryGet(int id)
        {
            return DB.Get<OrderProductDelivery>(id);
        }

        public OrderProductDeliveryCollection OrderProductDeliveryGetByReturn(List<Guid> sellerGuids)
        {
            string sid = string.Join("','", sellerGuids);
            if (!string.IsNullOrEmpty(sid))
            {
                if (!sid.StartsWith("'"))
                {
                    sid = "'" + sid;
                }
                if (!sid.EndsWith("'"))
                {
                    sid = sid + "'";
                }
            }
            string sql = @"select opd.*
                        from order_product_delivery opd with(nolock)
                        left join [order] o with(nolock) on opd.order_guid = o.guid
                        where o.seller_GUID in(" + sid + @") and (opd.dc_return = 1 or opd.dc_receive_fail = 1)
                        ";
            QueryCommand qc = new QueryCommand(sql, OrderProductDelivery.Schema.Provider.Name);
            //qc.AddParameter("@sid", sid, DbType.String);

            OrderProductDeliveryCollection result = new OrderProductDeliveryCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public bool OrderProductDeliverySet(OrderProductDelivery opd)
        {
            return DB.Save(opd) > 0;
        }

        public bool OrderProductDeliveryUpdateIsApplicationServerConnectedStatus(IspIsApplicationServerConnectedStatus status, List<Guid> orderGuidList)
        {
            return  DB.Update<OrderProductDelivery>().Set(OrderProductDelivery.IsApplicationServerConnectedColumn).EqualTo((int)status).Where(OrderProductDelivery.OrderGuidColumn)
               .In(orderGuidList).Execute() > 0;
        }

        #endregion

        #region ViewOrderProductDelivery

        /// <summary>
        /// 取得未完成的宅配超取訂單
        /// </summary>
        /// <returns></returns>
        public ViewOrderProductDeliveryCollection ViewOrderProductDeliveryGetUndoneList()
        {
            return DB.SelectAllColumnsFrom<ViewOrderProductDelivery>().NoLock()
                .Where(ViewOrderProductDelivery.Columns.CreateTime).IsGreaterThan(DateTime.Now.AddDays(-90))
                .And(ViewOrderProductDelivery.Columns.IsCancel).IsEqualTo(false)
                .And(ViewOrderProductDelivery.Columns.IsCompleted).IsEqualTo(false)
                .And(ViewOrderProductDelivery.Columns.ProductDeliveryType).In((int)ProductDeliveryType.FamilyPickup, (int)ProductDeliveryType.SevenPickup)
                .ExecuteAsCollection<ViewOrderProductDeliveryCollection>();
        }

        public ViewOrderProductDeliveryCollection ViewOrderProductDeliveryCollectionGetByOrderGuids(List<Guid> orderGuids)
        {
            return DB.SelectAllColumnsFrom<ViewOrderProductDelivery>().NoLock()
                .Where(ViewOrderProductDelivery.Columns.OrderGuid).In(orderGuids)
                .ExecuteAsCollection<ViewOrderProductDeliveryCollection>();
        }

        public ViewOrderProductDelivery ViewOrderProductDeliveryGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewOrderProductDelivery>().NoLock()
                .Where(ViewOrderProductDelivery.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteSingle<ViewOrderProductDelivery>();
        }

        public ViewOrderProductDelivery ViewOrderProductDeliveryGetByShipNo(string shipNo)
        {
            return DB.SelectAllColumnsFrom<ViewOrderProductDelivery>().NoLock()
               .Where(ViewOrderProductDelivery.Columns.PreShipNo).IsEqualTo(shipNo)
               .ExecuteSingle<ViewOrderProductDelivery>();
        }

        #endregion ViewOrderProductDelivery

        #region shopping cart related implementations

        public ShoppingCart ShoppingCartGet(string column, object value)
        {
            return DB.Get<ShoppingCart>(column, value);
        }

        public IList<Order> OrderGetListByShoppingCartGroupGuid(Guid shoppingCartGroupGuid)
        {
            if (shoppingCartGroupGuid == default(Guid))
            {
                return new List<Order>();
            }
            string sql = @"
select o.* from [shopping_cart_group_order] scgo
inner join shopping_cart_group_d scgd on scgo.sub_cart_trans_id = scgd.guid
inner join shopping_cart_group scg on scg.guid = scgd.cart_guid
inner join [order] o on o.GUID = scgo.order_guid
where scg.guid = @shoppingCartGroupGuid
";
            QueryCommand qc = new QueryCommand(sql, ShoppingCartGroup.Schema.Provider.Name);
            qc.AddParameter("@shoppingCartGroupGuid", shoppingCartGroupGuid, DbType.Guid);

            OrderCollection result = new OrderCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ShoppingCart ShoppingCartGet(string ticketId, Guid itemId, string itemName)
        {
            return
                DB.SelectAllColumnsFrom<ShoppingCart>()
                .Where(ShoppingCart.TicketIdColumn).IsEqualTo(ticketId)
                .And(ShoppingCart.ItemGuidColumn).IsEqualTo(itemId)
                .And(ShoppingCart.ItemNameColumn).IsEqualTo(itemName)
                .ExecuteSingle<ShoppingCart>();
        }

        public ShoppingCartCollection ShoppingCartGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<ShoppingCart>()
                .Where(column).IsEqualTo(value)
                .ExecuteAsCollection<ShoppingCartCollection>();
        }

        public ShoppingCartCollection ShoppingCartGetListByFilter(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ShoppingCart, ShoppingCartCollection>(1, -1, null, filter);
        }

        public bool ShoppingCartSet(ShoppingCart cart)
        {
            DB.Save<ShoppingCart>(cart);
            return true;
        }

        public void ShoppingCartDeleteByTicketId(string ticketId)
        {
            string sql = "delete from " + ShoppingCart.Schema.TableName + " where " + ShoppingCart.Columns.TicketId + " = @ticketId";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@ticketId", ticketId, DbType.String);
            DB.Execute(qc);
        }

        public bool ShoppingCartConvert2OrdDtl(OrderDetailCollection odrDtlCol, Order odr, string ticketId, bool updateOrder)
        {
            QueryCommandCollection qcc = new QueryCommandCollection();
            decimal total = 0;

            foreach (OrderDetail od in odrDtlCol)
            {
                total += od.Total.Value;
                qcc.Add(ActiveHelper<OrderDetail>.GetInsertCommand(od, null));
            }
            qcc.Add(new Query(ShoppingCart.Schema).AddWhere(ShoppingCart.Columns.TicketId, ticketId).BuildDeleteCommand());
            if (updateOrder && total > 0)
            {
                odr.Subtotal += total;
                odr.Total += total;
                qcc.Add(ActiveHelper<Order>.GetUpdateCommand(odr, null));
            }
            DataService.ExecuteTransaction(qcc);
            return true;
        }

        public bool ShoppingCartPurge(DateTime deadline)
        {
            string sql = "delete from " + ShoppingCart.Schema.TableName + " where " + ShoppingCart.Columns.CreateTime + " < @bd";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@bd", deadline, DbType.DateTime);
            DB.Execute(qc);
            return true;
        }

        #endregion shopping cart related implementations

        #region group order related functions

        public GroupOrder GroupOrderGet(Guid goId)
        {
            return DB.SelectAllColumnsFrom<GroupOrder>().Where(GroupOrder.GuidColumn).IsEqualTo(goId).ExecuteSingle<GroupOrder>();
        }

        public GroupOrder GroupOrderGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<GroupOrder>()
                .Where(GroupOrder.BusinessHourGuidColumn).IsEqualTo(bid)
                .ExecuteSingle<GroupOrder>();
        }

        public GroupOrder GroupOrderGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<GroupOrder>().Where(GroupOrder.Columns.OrderGuid).IsEqualTo(orderGuid).ExecuteSingle<GroupOrder>();
        }

        public GroupOrderCollection GroupOrderGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<GroupOrder, GroupOrderCollection>(1, -1, null, filter);
        }

        public GroupOrderCollection GroupOrderGetList(string filter, string orderBy)
        {
            string sql = "SELECT * FROM group_order with(nolock) WHERE " + filter + " ORDER BY " + orderBy;

            GroupOrderCollection col = new InlineQuery().ExecuteAsCollection<GroupOrderCollection>(sql);

            return col;
        }

        public bool GroupOrderSet(GroupOrder go)
        {
            DB.Save<GroupOrder>(go);
            return true;
        }

        public bool GroupOrderUpdateClosingTime(Guid goId, DateTime closeTime)
        {
            DB.Update<GroupOrder>().Set(GroupOrder.CloseTimeColumn).EqualTo(closeTime).Where(GroupOrder.GuidColumn).IsEqualTo(goId).Execute();
            return true;
        }

        public bool GroupOrderDelete(string column, object value)
        {
            DB.Destroy<GroupOrder>(column, value);
            return true;
        }

        #endregion group order related functions

        #region Order related auxiliary functions

        public void MakeOrder(Order theOrder, Guid bizHourGuid,
            OrderDetailCollection odrDtlCol, OrderDetailItemCollection odItemCol,
            ViewShoppingCartItemCollection vsci,
            MemberStatusFlag memStUpd, OrderProductDelivery opd)
        {
            QueryCommandCollection qcc = new QueryCommandCollection();
            if (theOrder.IsLoaded)
            {
                qcc.Add(ActiveHelper<Order>.GetUpdateCommand(theOrder, null));
            }
            else
            {
                qcc.Add(ActiveHelper<Order>.GetInsertCommand(theOrder, null));
            }

            foreach (OrderDetail od in odrDtlCol)
                qcc.Add(ActiveHelper<OrderDetail>.GetInsertCommand(od, null));

            foreach (OrderDetailItem odItem in odItemCol)
            {
                qcc.Add(ActiveHelper<OrderDetailItem>.GetInsertCommand(odItem, null));
            }

            foreach (ViewShoppingCartItem item in vsci)
                qcc.Add(ActiveHelper<ShoppingCart>.GetDeleteCommand(ShoppingCart.Columns.Id, item.Id));

            if (memStUpd != MemberStatusFlag.None)
                qcc.Add(GetUpdateMemberStatusQC(theOrder.UserId, memStUpd));

            if(opd != null)
                qcc.Add(ActiveHelper<OrderProductDelivery>.GetInsertCommand(opd, null));

            DataService.ExecuteTransaction(qcc);
        }

        public void MakeOrderWithoutCarts(Order theOrder, Guid bizHourGuid, OrderDetailCollection odrDtlCol, MemberStatusFlag memStUpd)
        {
            QueryCommandCollection qcc = new QueryCommandCollection();
            qcc.Add(ActiveHelper<Order>.GetInsertCommand(theOrder, null));
            foreach (OrderDetail od in odrDtlCol)
                qcc.Add(ActiveHelper<OrderDetail>.GetInsertCommand(od, null));

            if (memStUpd != MemberStatusFlag.None)
                qcc.Add(GetUpdateMemberStatusQC(theOrder.UserId, memStUpd));

            DataService.ExecuteTransaction(qcc);
        }

        public void MakeGroupOrder(Order ord, GroupOrder go, bool setBizHourOpenModeToFull)
        {
            QueryCommandCollection qcc = new QueryCommandCollection();
            qcc.Add(ActiveHelper<Order>.GetInsertCommand(ord, null));
            qcc.Add(ActiveHelper<GroupOrder>.GetInsertCommand(go, null));

            DataService.ExecuteTransaction(qcc);
        }

        public void CompleteGroupOrder(GroupOrder go, Order o, OrderDetailCollection extraOd, MemberStatusFlag memStUpd)
        {
            QueryCommandCollection qcc = new QueryCommandCollection
                                             {
                                                 ActiveHelper<GroupOrder>.GetUpdateCommand(go, null),
                                                 ActiveHelper<Order>.GetUpdateCommand(o, null)
                                             };
            if (extraOd != null)
                foreach (OrderDetail od in extraOd)
                    qcc.Add(ActiveHelper<OrderDetail>.GetInsertCommand(od, null));

            if (memStUpd != MemberStatusFlag.None)
            {
                int userId =
                    ProviderFactory.Instance().GetProvider<IMemberProvider>().MemberGet(go.MemberUserName).UniqueId;
                qcc.Add(GetUpdateMemberStatusQC(userId, memStUpd));
            }

            DataService.ExecuteTransaction(qcc);
        }

        private QueryCommand GetUpdateMemberStatusQC(int userId, MemberStatusFlag status)
        {
            QueryCommand qc = new QueryCommand(@"update member set status=@st where unique_id=@uid", Member.Schema.Provider.Name);
            qc.AddParameter("@st", (int)status, DbType.Int32);
            qc.AddParameter("@uid", userId, DbType.Int32);

            return qc;
        }

        public int OrderGetDailyOrderCount(Guid sellerGuid, DateTime deliveryDate)
        {
            string sql = "select count([order].guid) from [order] with(nolock) " +
                "left outer join group_order with(nolock) on [order].guid = group_order.order_guid " +
                "left outer join business_hour with(nolock) on group_order.business_hour_guid = business_hour.guid " +
                "where [order].seller_guid=@sid and (" +
                "([order].delivery_time >= @ds and [order].delivery_time < @de) " +
                "or (business_hour.business_hour_order_time_s < @bt and group_order.create_time >= @ds and group_order.create_time < @de) )";

            QueryCommand q = new QueryCommand(sql, Order.Schema.Provider.Name);
            q.AddParameter("@sid", sellerGuid, DbType.Guid);
            q.AddParameter("@ds", deliveryDate.Date, DbType.Date);
            q.AddParameter("@de", deliveryDate.Date.AddDays(1), DbType.Date);
            q.AddParameter("@bt", GlobalProperty.TimeBase.AddDays(1), DbType.Date);
            return (int)DataService.ExecuteScalar(q);
        }

        #endregion Order related auxiliary functions

        #region ReturnForm

        public int ReturnFormSet(ReturnForm form)
        {
            return DB.Save(form);
        }

        public ReturnForm ReturnFormGet(int id)
        {
            return DB.Get<ReturnForm>(id);
        }

        public int GetRefundCountByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ReturnForm>().NoLock()
                .Where(ReturnForm.Columns.OrderGuid).IsEqualTo(orderGuid).GetRecordCount();
        }

        public ReturnFormCollection ReturnFormGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ReturnForm>().NoLock()
                     .Where(ReturnForm.Columns.OrderGuid).IsEqualTo(orderGuid)
                     .ExecuteAsCollection<ReturnFormCollection>();
        }

        public ReturnFormCollection ReturnFormGetListByOrderGuidProgressStatusRefundType(Guid orderGuid, ProgressStatus processStatus, RefundType refundType)
        {
            return DB.SelectAllColumnsFrom<ReturnForm>().NoLock()
                     .Where(ReturnForm.Columns.OrderGuid).IsEqualTo(orderGuid)
                     .And(ReturnForm.Columns.ProgressStatus).IsEqualTo((int)processStatus)
                     .And(ReturnForm.Columns.RefundType).IsEqualTo((int)refundType)
                     .ExecuteAsCollection<ReturnFormCollection>();
        }

        public List<int> ReturnFormGetListForScheduledRefund()
        {
            var result = new List<int>();

            var config = ProviderFactory.Instance().GetConfig();

            result.AddRange(DB.SelectAllColumnsFrom<ReturnForm>()
             .Where(ReturnForm.Columns.DeliveryType).IsEqualTo(DeliveryType.ToShop)
             .AndExpression(ReturnForm.Columns.ProgressStatus).IsEqualTo((int)ProgressStatus.Processing)
             .And(ReturnForm.Columns.CreateTime).IsLessThan(DateTime.Today.AddDays((-1) * config.ReturnWaitingDays))
             //按下立即退貨之憑證退貨單 不需經退貨等待期 即可完成退貨程序(退款方式為Atm另由Job:AtmClean處理)
             .Or(ReturnForm.Columns.ProgressStatus).IsEqualTo((int)ProgressStatus.CompletedWithCreditCardQueued)
             .CloseExpression()
             .ExecuteAsCollection<ReturnFormCollection>().Select(x => x.Id));

            result.AddRange(DB.SelectAllColumnsFrom<ReturnForm>()
              .Where(ReturnForm.Columns.ProgressStatus).In((int)ProgressStatus.Processing, (int)ProgressStatus.CompletedWithCreditCardQueued)
              .And(ReturnForm.Columns.DeliveryType).IsEqualTo(DeliveryType.ToHouse)
              .And(ReturnForm.Columns.VendorProgressStatus).In(VendorProgressStatus.CompletedAndNoRetrieving, VendorProgressStatus.CompletedAndRetrievied, VendorProgressStatus.CompletedAndUnShip, VendorProgressStatus.Automatic, VendorProgressStatus.CompletedByCustomerService)
              .ExecuteAsCollection<ReturnFormCollection>().Select(x => x.Id));

            return result;
        }

        public ReturnFormCollection GetExpiredProductReturnFormList()
        {
            string sql = string.Format(@"
select rf.* 
from {0} as rf with(nolock)
inner join {1} as ctl with(nolock) on ctl.order_guid = rf.order_guid
inner join {2} as da with(nolock) on da.business_hour_guid = ctl.business_hour_guid
where rf.progress_status = {3}
and rf.vendor_progress_status = {4}
and dateadd(day, {5}, rf.create_time) < getdate() 
and ctl.status in ({6}, {7}, {8})
and da.remittance_type in ({9}, {10} , {11})
and rf.refund_type in ({12}, {13}, {14}, {15})
", ReturnForm.Schema.TableName
 , CashTrustLog.Schema.TableName
 , DealAccounting.Schema.TableName
 , (int)ProgressStatus.Processing
 , (int)VendorProgressStatus.Processing
 , config.ProductReturnProcessExpiredDay
 , (int)TrustStatus.Initial
 , (int)TrustStatus.Trusted
 , (int)TrustStatus.Verified
 , (int)RemittanceType.Flexible
 , (int)RemittanceType.Weekly
 , (int)RemittanceType.Monthly
 , (int)RefundType.Scash
 , (int)RefundType.Cash
 , (int)RefundType.Atm
 , (int)RefundType.Tcash);

            var qc = new QueryCommand(sql, ReturnForm.Schema.Provider.Name);

            var data = new ReturnFormCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public DataTable ReturnFormToShopCouponID(int rtnID)
        {
            string sql = @"select ctl." + CashTrustLog.Columns.CouponId + " from " + ReturnForm.Schema.TableName + " as rf " +
                            "join " + ReturnFormRefund.Schema.TableName + " as rfr on rf." + ReturnForm.Columns.Id + "=rfr." + ReturnFormRefund.Columns.ReturnFormId + " " +
                            "join " + CashTrustLog.Schema.TableName + " as ctl on ctl." + CashTrustLog.Columns.TrustId + "=rfr." + ReturnFormRefund.Columns.TrustId + " " +
                            "where rf." + ReturnForm.Columns.DeliveryType + "=" + (int)DeliveryType.ToShop +
                            "and rf." + ReturnForm.Columns.ProgressStatus + "=" + (int)ProgressStatus.Processing + " and rf." + ReturnForm.Columns.Id + "=@rtnID ";

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, rtnID))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public ReturnFormCollection GetCancelReturnForm(DateTime s_date, DateTime e_date)
        {
            return DB.SelectAllColumnsFrom<ReturnForm>().NoLock()
                     .Where(ReturnForm.Columns.ProgressStatus).IsEqualTo((int)ProgressStatus.Canceled)
                     .And(ReturnForm.Columns.FinishTime).IsGreaterThanOrEqualTo(s_date)
                     .And(ReturnForm.Columns.FinishTime).IsLessThan(e_date)
                     .ExecuteAsCollection<ReturnFormCollection>();
        }

        public ReturnFormCollection GetNoCompleteAndRequireCreditNote(DateTime s_date, DateTime e_date, InvoiceMode2 invoiceMode2, AllowanceStatus allowanceStatus)
        {
            string sql = string.Format(@"select DISTINCT rf.*
from return_form rf with(nolock)
inner join einvoice_main em with(nolock) on rf.order_guid = em.order_guid
where rf.create_time < '{1}'
and rf.progress_status = @progress_status
and rf.credit_note_type = @credit_note_type
and em.coupon_id is null
and em.invoice_status = @invoice_status
and em.allowance_status = @allowance_status
and em.invoice_mode2 = @invoice_mode2
and em.invoice_number_time < '{2}' ", s_date.ToString("yyyy/MM/dd"), e_date.ToString("yyyy/MM/dd"), e_date.GetFirstDayOfMonth().ToString("yyyy/MM/dd"));

            var qc = new QueryCommand(sql, ReturnForm.Schema.Provider.Name);
            qc.AddParameter("@progress_status", (int)ProgressStatus.Processing, DbType.Int32);
            qc.AddParameter("@credit_note_type", (int)AllowanceStatus.None, DbType.Int32);
            qc.AddParameter("@invoice_status", (int)EinvoiceType.C0401, DbType.Int32);
            qc.AddParameter("@allowance_status", (int)allowanceStatus, DbType.Int32);
            qc.AddParameter("@invoice_mode2", (int)invoiceMode2, DbType.Int32);
            var data = new ReturnFormCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion ReturnForm

        #region ReturnFormProduct

        public ReturnFormProductCollection ReturnFormProductGetList(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<ReturnFormProduct>()
                     .Where(ReturnFormProduct.Columns.ReturnFormId).IsEqualTo(returnFormId)
                     .ExecuteAsCollection<ReturnFormProductCollection>();
        }

        public ReturnFormProductCollection ReturnFormProductGetList(Guid orderGuid, BusinessModel bsModel)
        {
            return DB.SelectAllColumnsFrom<ReturnFormProduct>()
                     .InnerJoin(ReturnForm.IdColumn, ReturnFormProduct.ReturnFormIdColumn)
                     .Where(ReturnForm.OrderGuidColumn).IsEqualTo(orderGuid)
                     .And(ReturnForm.BusinessModelColumn).IsEqualTo((int)bsModel)
                     .ExecuteAsCollection<ReturnFormProductCollection>();
        }

        public ReturnFormProductCollection ReturnFormProductGetList(IEnumerable<int> returnFormIds)
        {
            ReturnFormProductCollection infos = new ReturnFormProductCollection();
            List<int> tempIds = returnFormIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ReturnFormProduct>()
                        .Where(ReturnFormProduct.Columns.ReturnFormId).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ReturnFormProductCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public int ReturnFormProductSetList(ReturnFormProductCollection formProducts)
        {
            return DB.SaveAll3(formProducts);
        }

        #endregion ReturnFormProduct

        #region ReturnFormRefund

        public ReturnFormRefundCollection ReturnFormRefundGetList(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<ReturnFormRefund>()
                     .Where(ReturnFormRefund.Columns.ReturnFormId).IsEqualTo(returnFormId)
                     .ExecuteAsCollection<ReturnFormRefundCollection>();
        }

        public ReturnFormRefundCollection ReturnFormRefundGetList(IEnumerable<int> returnFormIds)
        {
            ReturnFormRefundCollection infos = new ReturnFormRefundCollection();
            List<int> tempIds = returnFormIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ReturnFormRefund>()
                        .Where(ReturnFormRefund.Columns.ReturnFormId).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ReturnFormRefundCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public ReturnFormRefundCollection ReturnFormRefundGetList(Guid trustId)
        {
            return DB.SelectAllColumnsFrom<ReturnFormRefund>()
                     .Where(ReturnFormRefund.Columns.TrustId).IsEqualTo(trustId)
                     .ExecuteAsCollection<ReturnFormRefundCollection>()
                    ?? new ReturnFormRefundCollection();
        }

        public int ReturnFormRefundSetList(ReturnFormRefundCollection formRefunds)
        {
            return DB.SaveAll3(formRefunds);
        }

        #endregion

        #region ReturnFormStatusLog

        public int ReturnFormStatusLogSet(ReturnFormStatusLog log)
        {
            return DB.Save(log);
        }

        public ReturnFormStatusLogCollection ReturnFormStatusLogGetListByReturnFormId(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<ReturnFormStatusLog>()
                     .Where(ReturnFormStatusLog.Columns.ReturnFormId).IsEqualTo(returnFormId)
                     .ExecuteAsCollection<ReturnFormStatusLogCollection>();
        }

        #endregion ReturnFormStatusLog

        #region ViewCashTrustLogReturnFormList

        public ViewCashTrustLogRetrnFormListCollection ViewCashTrustLogRetrnFormListCollectionGet(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<ViewCashTrustLogRetrnFormList>()
                     .Where(ViewCashTrustLogRetrnFormList.Columns.ReturnFormId).IsEqualTo(returnFormId)
                     .ExecuteAsCollection<ViewCashTrustLogRetrnFormListCollection>();
        }

        #endregion

        #region ViewMemberGroupOrder

        public ViewMemberGroupOrder ViewMemberGroupOrderGet(string column, object value)
        {
            ViewMemberGroupOrder view = new ViewMemberGroupOrder();
            view.LoadAndCloseReader(ViewMemberGroupOrder.Query().WHERE(column, value).ExecuteReader());
            return view;
        }

        public ViewMemberGroupOrderCollection ViewMemberGroupOrderGetList(string username, string orderBy, params string[] filter)
        {
            ViewMemberGroupOrderCollection col = new ViewMemberGroupOrderCollection();
            Query qry = ViewMemberGroupOrder.Query().WHERE(ViewMemberGroupOrder.Columns.UserName, username);
            qry = SSHelper.GetQueryByFilterOrder(qry, orderBy, filter);
            col.LoadAndCloseReader(qry.ExecuteReader());
            return col;
        }

        #endregion ViewMemberGroupOrder

        #region ViewShoppingCartItem

        public ViewShoppingCartItemCollection ViewShoppingCartItemGetList(string ticketId)
        {
            //ViewShoppingCartItemCollection vsci = new ViewShoppingCartItemCollection();
            //vsci.LoadAndCloseReader(ViewShoppingCartItem.FetchByParameter(ViewShoppingCartItem.Columns.TicketId, ticketId));
            string strSql = string.Format(@"select * from [dbo].[view_shopping_cart_item] with(nolock) where ticket_Id= '{0}' ", ticketId);
            return new InlineQuery().ExecuteAsCollection<ViewShoppingCartItemCollection>(strSql);
            //return vsci;
        }

        #endregion ViewShoppingCartItem

        #region RedeemOrder APIs

        public RedeemOrderCollection RedeemOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<RedeemOrder, RedeemOrderCollection>(pageStart, pageLength, orderBy, filter);
        }

        public int RedeemOrderGetCount()
        {
            return new Select(Aggregate.Count(RedeemOrder.GuidColumn)).From(RedeemOrder.Schema).ExecuteScalar<int>();
        }

        public RedeemOrder RedeemOrderGet(Guid guid)
        {
            return DB.Get<RedeemOrder>(guid);
        }

        public bool RedeemOrderSet(RedeemOrder odr)
        {
            DB.Save<RedeemOrder>(odr);
            return true;
        }

        public int RedeemOrderGetCreateIdMaxLenght()
        {
            return RedeemOrder.Schema.GetColumn(RedeemOrder.Columns.CreateId).MaxLength;
        }

        public DataTable RedeemGetForCheckOut(DateTime sDate, DateTime eDate)
        {
            string sql = @"
            select ml.external_user_id,ro.member_name,ro.total,+(ro.total/10) as pcash,-+(ro.total/10) as 剩餘pcash
            from redeem_order ro with(nolock)
            JOIN member m ON ro.member_email = m.user_name
            join member_link ml with(nolock) on ml.user_id=m.unique_id " +
            "where ro.create_time between '" + sDate.ToString("yyyy/MM/dd hh:mm") + ":00.000' and '" + eDate.ToString("yyyy/MM/dd hh:mm") + ":00.000' " +
            "order by ro.create_time";

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }

            return dt;
        }

        public DataTable RedeemGetForCheckOutCSV(DateTime sDate, DateTime eDate)
        {
            string sql = @"
            select ml.external_user_id,(ro.total/10) as pcash,'3407'
            from redeem_order ro with(nolock)
            JOIN member m ON ro.member_email = m.user_name
            join member_link ml with(nolock) on ml.user_id=m.unique_id " +
            "where ro.create_time between '" + sDate.ToString("yyyy/MM/dd hh:mm") + ":00.000' and '" + eDate.ToString("yyyy/MM/dd hh:mm") + ":00.000' " +
            "order by ro.create_time";

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }

            return dt;
        }

        public void RedeemUpdateForCheckOut(DateTime sDate, DateTime eDate, DateTime checkouttime)
        {
            string sql = "update redeem_order set delivery_time ='" + checkouttime.ToString("yyyy/MM/dd hh:mm:ss") + "' where GUID in(" +
                        "select ro.GUID from redeem_order ro " +
                        "JOIN member m ON ro.member_email = m.user_name " +
                        "join member_link ml with(nolock) on ml.user_id=m.unique_id " +
                        "where ro.create_time between '" + sDate.ToString("yyyy/MM/dd hh:mm:ss") + "' and '" + eDate.ToString("yyyy/MM/dd hh:mm:ss") + "') ";

            new InlineQuery().Execute(sql);
        }

        #endregion RedeemOrder APIs

        #region RedeemOrderDetail APIs

        public RedeemOrderDetailCollection RedeemOrderDetailGetList(Guid redOrdGuid)
        {
            return RedeemOrderDetailGetList(RedeemOrderDetail.Columns.OrderGuid, redOrdGuid);
        }

        public RedeemOrderDetailCollection RedeemOrderDetailGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<RedeemOrderDetail>().Where(column).IsEqualTo(value).ExecuteAsCollection<RedeemOrderDetailCollection>();
        }

        public int RedeemOrderDetailGetCreateIdMaxLength()
        {
            return RedeemOrderDetail.Schema.GetColumn(RedeemOrderDetail.Columns.CreateId).MaxLength;
        }

        #endregion RedeemOrderDetail APIs

        #region ViewOrderDetailItem

        public ViewOrderDetailItemCollection ViewOrderDetailItemGetList(Guid orderGuid)
        {
            ViewOrderDetailItemCollection view = new ViewOrderDetailItemCollection();
            view.LoadAndCloseReader(
                ViewOrderDetailItem.Query().WHERE(ViewOrderDetailItem.Columns.OrderGuid, orderGuid).ExecuteReader());
            return view;
        }

        #endregion ViewOrderDetailItem

        #region OrderDetailItem

        public OrderDetailItemCollection OrderDetailItemCollectionGetByOrderDetailGuid(Guid order_detail_guid)
        {
            return
             DB.SelectAllColumnsFrom<OrderDetailItem>().Where(OrderDetailItem.OrderDetailGuidColumn).IsEqualTo(order_detail_guid).ExecuteAsCollection<OrderDetailItemCollection>();
        }

        public OrderDetailItemCollection OrderDetailItemCollectionGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<OrderDetailItem>()
                .Where(OrderDetailItem.OrderDetailGuidColumn)
                .In(DB.Select(OrderDetail.GuidColumn.ColumnName)
                    .From<OrderDetail>()
                    .Where(OrderDetail.OrderGuidColumn)
                    .IsEqualTo(orderGuid))
                .ExecuteAsCollection<OrderDetailItemCollection>();
        }

        #endregion OrderDetailItem

        #region ViewOrderMemberBuildingSeller


        public int ViewOrderMemberBuildingSellerGetCount(string orderId, int? uniqueId, string memberName,
                                                             DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,
                                                             int? excludeFamilyMartCityId, bool paymentFreeze)
        {
            string sql = OrderListSql(orderId, uniqueId, memberName, minOrderCreateTime, maxOrderCreateTime, excludeFamilyMartCityId, paymentFreeze);
            sql = sql.Replace("*", "count(1)");
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            return (int)DataService.ExecuteScalar(qc);
        }

        public int ViewOrderMemberBuildingSellerGetCountForNew(string orderId, int? uniqueId, string memberName,
                                                     DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,
                                                     string dateType,string shipType,string csvType)
        {
            string sql = OrderListSqlForNew(orderId, uniqueId, memberName, minOrderCreateTime, maxOrderCreateTime, dateType, shipType, csvType);
            sql = sql.Replace("*", "count(1)");
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            return (int)DataService.ExecuteScalar(qc);
        }

        public int ViewOrderMemberBuildingSellerGetCountForReturn(string orderId,int? uniqueId,string sellerId, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,string dateType,string csvType)
        {
            string sql = OrderListSqlForReturn(orderId, uniqueId, sellerId, minOrderCreateTime, maxOrderCreateTime, dateType, csvType);
            sql = sql.Replace("*", "count(1)");
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            return (int)DataService.ExecuteScalar(qc);

        }

        public ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGet(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewOrderMemberBuildingSeller.Schema, filter);
            qc.CommandSql = "select * from " + ViewOrderMemberBuildingSeller.Schema.Provider.DelimitDbName(ViewOrderMemberBuildingSeller.Schema.TableName) + " with(nolock) " + qc.CommandSql;

            string orderByColumn = ViewOrderMemberBuildingSeller.Columns.OrderGuid;
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
                orderByColumn = orderBy;
            }
            else
            {
                qc.CommandSql += " order by " + orderByColumn;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderByColumn);
            }

            var result = new ViewOrderMemberBuildingSellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPaging(
            int pageStart, int pageLength, string orderBy,
            string orderId, int? uniqueId, string memberName,
            DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, int? excludeFamilyMartCityId, bool paymentFreeze)
        {
            string sql = OrderListSql(orderId, uniqueId, memberName, minOrderCreateTime, maxOrderCreateTime, excludeFamilyMartCityId, paymentFreeze);

            string orderByColumn = ViewOrderMemberBuildingSeller.Columns.OrderGuid;
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                sql += " order by " + orderBy;
                orderByColumn = orderBy;
            }
            else
            {
                sql += " order by " + orderByColumn;
            }

            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderByColumn);
            }

            var result = new ViewOrderMemberBuildingSellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPagingForNew(
    int pageStart, int pageLength, string orderBy,
    string orderId, int? uniqueId, string memberName,
    DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,string dateType,string shipType,string csvType)
        {
            string sql = OrderListSqlForNew(orderId, uniqueId, memberName, minOrderCreateTime, maxOrderCreateTime,dateType,shipType,csvType);

            string orderByColumn = ViewOrderMemberBuildingSeller.Columns.OrderGuid;
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                sql += " order by " + orderBy;
                orderByColumn = orderBy;
            }
            else
            {
                sql += " order by " + orderByColumn;
            }

            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderByColumn);
            }

            var result = new ViewOrderMemberBuildingSellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListPagingForReturn(int pageStart, int pageLength, string orderBy,string orderId, int? uniqueId, string sellerId,DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string csvType)
        {
            string sql = OrderListSqlForReturn(orderId, uniqueId, sellerId, minOrderCreateTime, maxOrderCreateTime, dateType, csvType);

            string orderByColumn = ViewOrderMemberBuildingSeller.Columns.OrderGuid;
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                sql += " order by " + orderBy;
                orderByColumn = orderBy;
            }
            else
            {
                sql += " order by " + orderByColumn;
            }

            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderByColumn);
            }

            var result = new ViewOrderMemberBuildingSellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewOrderMemberBuildingSellerCollection ViewOrderMemberBuildingSellerGetListExcelForReturn(string orderBy, string orderId, int? uniqueId, string sellerId, DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType, string csvType)
        {
            string sql = OrderListSqlForReturn(orderId, uniqueId, sellerId, minOrderCreateTime, maxOrderCreateTime, dateType, csvType);

            string orderByColumn = ViewOrderMemberBuildingSeller.Columns.OrderGuid;
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                sql += " order by " + orderBy;
                orderByColumn = orderBy;
            }
            else
            {
                sql += " order by " + orderByColumn;
            }

            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberBuildingSeller.Schema.Provider.Name);

            /*
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderByColumn);
            }*/

            var result = new ViewOrderMemberBuildingSellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        private static string OrderListSql(string orderId, int? uniqueId, string memberName,
                                        DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, int? excludeFamilyMartCityId, bool paymentFreeze)
        {
            var sqlSb = new StringBuilder();
            sqlSb.Append(string.Format(@"select * from {0}", ViewOrderMemberBuildingSeller.Schema.TableName));

            //paymentFreeze=false 代表查詢所有訂單 故不須另設定where條件
            if (!string.IsNullOrWhiteSpace(orderId)
                || uniqueId.HasValue
                || !string.IsNullOrWhiteSpace(memberName)
                || minOrderCreateTime.HasValue
                || maxOrderCreateTime.HasValue
                || excludeFamilyMartCityId.HasValue
                || paymentFreeze) // has where clause
            {
                sqlSb.Append(" where ");

                var predicates = new List<string>();
                if (!string.IsNullOrWhiteSpace(orderId))
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderId, orderId));
                }

                if (uniqueId.HasValue)
                {
                    predicates.Add(string.Format("{0} = {1}", ViewOrderMemberBuildingSeller.Columns.UniqueId, uniqueId.Value));
                }

                if (!string.IsNullOrWhiteSpace(memberName))
                {
                    predicates.Add(string.Format("{0} = N'{1}'", ViewOrderMemberBuildingSeller.Columns.MemberName, memberName));
                }

                if (minOrderCreateTime.HasValue)
                {
                    predicates.Add(string.Format("{0} > '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                 minOrderCreateTime.Value.ToString("yyyy/MM/dd hh:mm:ss")));
                }

                if (maxOrderCreateTime.HasValue)
                {
                    predicates.Add(string.Format("{0} < '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                 maxOrderCreateTime.Value.ToString("yyyy/MM/dd hh:mm:ss")));
                }

                if (excludeFamilyMartCityId.HasValue)
                {
                    predicates.Add(string.Format("{0} not like '%{1}%'", ViewOrderMemberBuildingSeller.Columns.CityList,
                                                 excludeFamilyMartCityId.Value));
                }

                if (paymentFreeze)
                {
                    predicates.Add(string.Format("({0} > 0 or {1} & {2} > 0)", ViewOrderMemberBuildingSeller.Columns.FreezeCount,
                                                ViewOrderMemberBuildingSeller.Columns.AccountingFlag,
                                                (int)AccountingFlag.Freeze));
                }

                sqlSb.Append(string.Join(" and ", predicates));
            }

            return sqlSb.ToString();
        }

        private static string OrderListSqlForNew(string orderId, int? uniqueId, string memberName,
                                DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime,string dateType,string shipType,string csvType)
        {
            var sqlSb = new StringBuilder();
            sqlSb.Append(string.Format(@"select * from {0}", ViewOrderMemberBuildingSeller.Schema.TableName));

            //paymentFreeze=false 代表查詢所有訂單 故不須另設定where條件
            if (!string.IsNullOrWhiteSpace(orderId)
                || uniqueId.HasValue
                || !string.IsNullOrWhiteSpace(memberName)
                || minOrderCreateTime.HasValue
                || maxOrderCreateTime.HasValue
                || !string.IsNullOrEmpty(shipType)) // has where clause
            {
                sqlSb.Append(" where ");

                var predicates = new List<string>();
                if (!string.IsNullOrWhiteSpace(orderId))
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderId, orderId));
                }

                if (uniqueId.HasValue)
                {
                    predicates.Add(string.Format("{0} = {1}", ViewOrderMemberBuildingSeller.Columns.UniqueId, uniqueId.Value));
                }

                if (!string.IsNullOrWhiteSpace(memberName))
                {
                    predicates.Add(string.Format("{0} = N'{1}'", ViewOrderMemberBuildingSeller.Columns.MemberName, memberName));
                }

                //憑證
                if (shipType == "0")
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.DeliveryType, (int)DeliveryType.ToShop));
                }
                //宅配
                else if (shipType == "1")
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.DeliveryType, (int)DeliveryType.ToHouse));
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.Normal));
                }
                //超取
                else if (shipType == "2")
                {
                    if (string.IsNullOrEmpty(csvType))
                    {
                        predicates.Add(string.Format("{0} in {1}", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, "('" + (int)ProductDeliveryType.FamilyPickup + "','" + (int)ProductDeliveryType.SevenPickup + "')"));
                    }
                    else if (csvType == "1")
                    {
                        predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.FamilyPickup));
                    }
                    else if (csvType == "2")
                    {
                        predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.SevenPickup));
                    }

                }

                //訂購日
                if (dateType == "1")
                {
                    if (minOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("(({0} is null and {1} > '{2}') or ({0} is not null and {0} > '{2}'))",ViewOrderMemberBuildingSeller.Columns.DeliveryTime, ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                     minOrderCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")));
                    }

                    if (maxOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("(({0} is null and {1} < '{2}') or ({0} is not null and {0} < '{2}'))", ViewOrderMemberBuildingSeller.Columns.DeliveryTime, ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                     maxOrderCreateTime.Value.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss")));
                    }
                }
                //應出貨日
                else if (dateType == "2")
                {
                    if (minOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} > '{1}'", ViewOrderMemberBuildingSeller.Columns.ShipTime,
                                                     minOrderCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")));
                    }

                    if (maxOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} < '{1}'", ViewOrderMemberBuildingSeller.Columns.ShipTime,
                                                     maxOrderCreateTime.Value.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss")));
                    }
                }

                sqlSb.Append(string.Join(" and ", predicates));
            }

            return sqlSb.ToString();
        }

        private static string OrderListSqlForReturn(string orderId, int? uniqueId, string sellerId,
                        DateTime? minOrderCreateTime, DateTime? maxOrderCreateTime, string dateType,string csvType)
        {
            var sqlSb = new StringBuilder();
            sqlSb.Append(string.Format(@"select * from {0}", ViewOrderMemberBuildingSeller.Schema.TableName));


            var predicates = new List<string>();
            sqlSb.Append(" where ");
            //paymentFreeze=false 代表查詢所有訂單 故不須另設定where條件
            if (!string.IsNullOrWhiteSpace(orderId)
                || uniqueId.HasValue
                || !string.IsNullOrWhiteSpace(sellerId)
                || minOrderCreateTime.HasValue
                || maxOrderCreateTime.HasValue
                || !string.IsNullOrEmpty(csvType)) // has where clause
            {



                if (!string.IsNullOrWhiteSpace(orderId))
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderId, orderId));
                }

                if (uniqueId.HasValue)
                {
                    predicates.Add(string.Format("{0} = {1}", ViewOrderMemberBuildingSeller.Columns.UniqueId, uniqueId.Value));
                }

                if (!string.IsNullOrWhiteSpace(sellerId))
                {
                    predicates.Add(string.Format("{0} = N'{1}'", ViewOrderMemberBuildingSeller.Columns.SellerId, sellerId));
                }

                //訂購日
                if (dateType == "1")
                {
                    if (minOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} > '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                     minOrderCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")));
                    }

                    if (maxOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} < '{1}'", ViewOrderMemberBuildingSeller.Columns.OrderCreateTime,
                                                     maxOrderCreateTime.Value.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss")));
                    }
                }
                //出貨確認日
                else if (dateType == "2")
                {
                    if (minOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} > '{1}'", ViewOrderMemberBuildingSeller.Columns.ShipTime,
                                                     minOrderCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss")));
                    }

                    if (maxOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("{0} < '{1}'", ViewOrderMemberBuildingSeller.Columns.ShipTime,
                                                     maxOrderCreateTime.Value.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss")));
                    }
                }
                //刷退/驗退日
                else if (dateType == "3")
                {
                    if (minOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("({0} > '{1}' or {2} > '{1}')", ViewOrderMemberBuildingSeller.Columns.DcReceiveFailTime,
                                                     minOrderCreateTime.Value.ToString("yyyy/MM/dd HH:mm:ss"), ViewOrderMemberBuildingSeller.Columns.DcReturnTime));
                    }

                    if (maxOrderCreateTime.HasValue)
                    {
                        predicates.Add(string.Format("({0} < '{1}' or {2} < '{1}')", ViewOrderMemberBuildingSeller.Columns.DcReceiveFailTime,
                                                     maxOrderCreateTime.Value.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"), ViewOrderMemberBuildingSeller.Columns.DcReturnTime));
                    }
                }


                if (string.IsNullOrEmpty(csvType))
                {
                    predicates.Add(string.Format("{0} in {1}", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, "('" + (int)ProductDeliveryType.FamilyPickup + "','" + (int)ProductDeliveryType.SevenPickup + "')"));
                }
                else if (csvType == "1")
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.FamilyPickup));
                }
                else if (csvType == "2")
                {
                    predicates.Add(string.Format("{0} = '{1}'", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.SevenPickup));
                }


            }


            //只需要查詢dcReturn以及dcReceiveFail的狀況
            predicates.Add(string.Format("({0} in {1} or {2} in {3})", ViewOrderMemberBuildingSeller.Columns.DcReturn, "(1,2)", ViewOrderMemberBuildingSeller.Columns.DcReceiveFail, "(1,2)"));
            predicates.Add(string.Format("{0} <> {1}", ViewOrderMemberBuildingSeller.Columns.ProductDeliveryType, (int)ProductDeliveryType.Normal));
            predicates.Add(string.Format("({0} is null and {1}={2} or {1}<>{2})", ViewOrderMemberBuildingSeller.Columns.ShipToStockTime, ViewOrderMemberBuildingSeller.Columns.DcReturn,0));
            sqlSb.Append(string.Join(" and ", predicates));

            return sqlSb.ToString();
        }


        public ViewOrderMemberBuildingSeller ViewOrderMemberBuildingSellerGet(Guid orderGuid)
        {
            ViewOrderMemberBuildingSeller view = new ViewOrderMemberBuildingSeller();
            view.LoadByParam(ViewOrderMemberBuildingSeller.Columns.OrderGuid, orderGuid);
            return view;
        }

        public ViewOrderMemberBuildingSeller ViewOrderMemberBuildingSellerGet(OrderStatus statusMask, OrderStatus expectStatus, string orderBy, params string[] filter)
        {
            QueryCommand qc = GetOrderWhereQC(statusMask, expectStatus, filter);
            if (!string.IsNullOrEmpty(orderBy))
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
            qc.CommandSql = "select top 1 * from " + ViewOrderMemberBuildingSeller.Schema.Provider.DelimitDbName(ViewOrderMemberBuildingSeller.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            ViewOrderMemberBuildingSeller view = new ViewOrderMemberBuildingSeller();
            view.LoadAndCloseReader(DataService.GetReader(qc));

            return view;
        }

        public DataTable ViewOrderMemberBuildingSellerGetListByDepartment(DateTime[] during)
        {
            string sql = "select department, COUNT(order_guid) as orders, COUNT(subtotal) as amount from view_order_member_building_seller with(nolock) "
                       + "where order_status&576=0 and order_create_time between '" + during[0].ToString("yyyy-MM-dd") + "' and '" + during[1].ToString("yyyy-MM-dd") + "' group by department order by department";

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion ViewOrderMemberBuildingSeller

        #region OrderStatusLog

        public OrderStatusLogCollection OrderStatusLogGetList(Guid orderGuid)
        {
            return
                DB.SelectAllColumnsFrom<OrderStatusLog>().Where(OrderStatusLog.OrderGuidColumn).IsEqualTo(orderGuid).
                OrderDesc(OrderStatusLog.CreateTimeColumn.ColumnName).ExecuteAsCollection<OrderStatusLogCollection>();
        }

        public OrderStatusLogCollection OrderStatusLogGetVendorNotifyList(int orderReturnId)
        {
            return DB.SelectAllColumnsFrom<OrderStatusLog>()
                    .Where(OrderStatusLog.Columns.OrderReturnId).IsEqualTo(orderReturnId)
                    .And(OrderStatusLog.Columns.Status).In((int)OrderLogStatus.SendToSeller, (int)OrderLogStatus.ExchangeProcessing, (int)OrderLogStatus.ExchangeSuccess, (int)OrderLogStatus.ExchangeCancel)
                    .And(OrderStatusLog.Columns.Message).IsNotNull()
                    .OrderDesc(OrderStatusLog.Columns.CreateTime)
                    .ExecuteAsCollection<OrderStatusLogCollection>();
        }

        public bool OrderStatusLogSet(OrderStatusLog data)
        {
            DB.Save(data);
            return true;
        }

        public OrderStatusLog OrderStatusLogGetLatest(Guid orderGuid)
        {
            return
            DB.SelectAllColumnsFrom<OrderStatusLog>().Where(OrderStatusLog.OrderGuidColumn).IsEqualTo(orderGuid).
                And(OrderStatusLog.StatusColumn).In((int)OrderLogStatus.Success, (int)OrderLogStatus.Processing, (int)OrderLogStatus.Failure, (int)OrderLogStatus.AutoProcessing, (int)OrderLogStatus.ProcessingScashOnly, (int)OrderLogStatus.AutoProcessingScashOnly).
                OrderDesc(OrderStatusLog.CreateTimeColumn.ColumnName).ExecuteSingle<OrderStatusLog>();
        }

        #endregion OrderStatusLog

        #region PaymentTransaction

        public PaymentTransaction PaymentTransactionGet(int id)
        {
            return DB.Get<PaymentTransaction>(id);
        }

        public PaymentTransaction PaymentTransactionGet(Guid OrderGuid)
        {
            return DB.Get<PaymentTransaction>(PaymentTransaction.Columns.OrderGuid, OrderGuid);
        }

        public PaymentTransaction PaymentTransactionGet(string transId, PaymentType paymentType, PayTransType transType)
        {
            PaymentTransactionCollection ptc = PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.TransId + " = " + transId,
                                                PaymentTransaction.Columns.PaymentType + " = " + (int)paymentType,
                                                PaymentTransaction.Columns.TransType + " = " + (int)transType);
            if (ptc.Count != 0) return ptc[0];
            else return null;
        }

        public PaymentTransactionCollection PaymentTransactionGetListByTransIdAndTransType(string transId, PayTransType transType)
        {
            PaymentTransactionCollection ptc = PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.TransId + " = " + transId,
                                                PaymentTransaction.Columns.TransType + " = " + (int)transType);
            return ptc;
        }

        public PaymentTransactionCollection PaymentTransactionGetListByTransIdAndPaymentType(string transId, PaymentType payType, int transType)
        {
            PaymentTransactionCollection ptc = PaymentTransactionGetListPaging(-1, -1, null, PayTransStatusFlag.None, 0,
                                                PaymentTransaction.Columns.TransId + " = " + transId,
                                                PaymentTransaction.Columns.PaymentType + " = " + (int)payType
                                                , PaymentTransaction.Columns.TransType + " = " + transType.ToString());
            if (ptc.Count != 0) return ptc;
            else return null;
        }

        public PaymentTransactionCollection PaymentTransactionGetList(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            string sql = @"select * from " + PaymentTransaction.Schema.TableName + " with(nolock) where " +
                         PaymentTransaction.Columns.OrderGuid + " = @oGuid and " +
                         PaymentTransaction.Columns.OrderClassification + " = @oClass ";
            QueryCommand qc = new QueryCommand(sql, PaymentTransaction.Schema.Provider.Name);
            qc.AddParameter("@oGuid", order_guid, DbType.Guid);
            qc.AddParameter("@oClass", (int)orderClassification, DbType.Int32);
            PaymentTransactionCollection data = new PaymentTransactionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;

            //return DB.SelectAllColumnsFrom<PaymentTransaction>()
            //    .Where(PaymentTransaction.Columns.OrderGuid).IsEqualTo(order_guid).ExecuteAsCollection<PaymentTransactionCollection>();
            //string strSql = string.Format(@"select * from [dbo].[payment_transaction] with(nolock) where order_guid= '{0}' ", order_guid.ToString());
            //return new InlineQuery().ExecuteAsCollection<PaymentTransactionCollection>(strSql);
        }

        public bool PaymentTransactionSet(PaymentTransaction pt)
        {
            DB.Save<PaymentTransaction>(pt);
            return true;
        }

        public void PaymentTransactionSetAll(PaymentTransactionCollection pts)
        {
            DB.SaveAll(pts);
        }

        public bool PaymentTransactionInsert(PaymentTransaction pt)
        {
            DB.Save(pt);
            return true;
        }

        public void PaymentTransactionDelete(string transId, PayTransType transType)
        {
            string sql = @"DELETE  " + PaymentTransaction.Schema.TableName + @" WHERE  " + PaymentTransaction.Columns.TransId + @"=@transId" +
                " AND " + PaymentTransaction.Columns.TransType + @"=@transType";
            var qc = new QueryCommand(sql, PaymentTransaction.Schema.Provider.Name);
            qc.AddParameter("@transId", transId, DbType.String);
            qc.AddParameter("@transType", (int)transType, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        private QueryCommand GetPaymentTransactionQC(PayTransStatusFlag mask, int expect, params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<PaymentTransaction>(filter);
            if (mask == PayTransStatusFlag.None)
                return qc;

            if (qc.Parameters.Count == 0)
                qc.CommandSql += SqlFragment.WHERE;
            else
            {
                int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
            }
            qc.CommandSql += " (" + PaymentTransaction.Columns.Status + " & @mask) = @expect";
            qc.AddParameter("@mask", mask, DbType.Int32);
            qc.AddParameter("@expect", expect, DbType.Int32);
            return qc;
        }

        public PaymentTransactionCollection PaymentTransactionGetListPaging(int pageStart, int pageLength, string orderByDesc,
                                                        PayTransStatusFlag mask, int expectStatus, params string[] filter)
        {
            string defOrderBy = PaymentTransaction.Columns.Id;
            QueryCommand qc = GetPaymentTransactionQC(mask, expectStatus, filter);
            qc.CommandSql = "select * from " + PaymentTransaction.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderByDesc))
            {
                qc.CommandSql += " order by " + orderByDesc + " desc";
                defOrderBy = orderByDesc + " desc";
            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            PaymentTransactionCollection view = new PaymentTransactionCollection();
            try
            {
                view.LoadAndCloseReader(DataService.GetReader(qc));
            }
            catch (Exception ex)
            {
                StringBuilder sbParameter = new StringBuilder();
                foreach (var pa in qc.Parameters)
                {
                    if (sbParameter.Length > 0)
                    {
                        sbParameter.Append(",");
                    }
                    sbParameter.AppendFormat("{0}={1}\r\n", pa.ParameterName, pa.ParameterValue);
                }
                throw new Exception(string.Format("commandSql={0}, orderBy={1}, parameters={2}",
                    qc.CommandSql, defOrderBy, sbParameter.ToString()), ex);
            }
            return view;
        }

        public PaymentTransactionCollection PaymentTransactionGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter)
        {
            string defOrderBy = PaymentTransaction.Columns.Id;
            QueryCommand qc = SSHelper.GetWhereQC<PaymentTransaction>(filter);
            qc.CommandSql = "select * from " + PaymentTransaction.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderByDesc))
            {
                qc.CommandSql += " order by " + orderByDesc + " desc";
                defOrderBy = orderByDesc + " desc";
            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            PaymentTransactionCollection view = new PaymentTransactionCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public PaymentTransactionCollection PaymentTransactionGetListByOrderGuid(Guid orderGuid)
        {
            string sql = @"select pt.* from payment_transaction pt with(nolock) where pt.trans_id in (
select pt2.trans_id from [order] o 
inner join payment_transaction pt2 on pt2.order_guid = o.GUID
where o.GUID = @orderGuid
)";
            QueryCommand qc = new QueryCommand(sql, PaymentTransaction.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            PaymentTransactionCollection result = new PaymentTransactionCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public PaymentTransactionCollection PaymentTransactionGetNullOrderIdPCash()
        {
            string strSQL = @"select * from payment_transaction pt with(nolock)
where pt.payment_type=2  and pt.trans_type=0 and order_guid is null and result=0 and pt.trans_time>'2011/01/01' and pt.auth_code<>''
and not exists
(select trans_id
from payment_transaction ptt with(nolock)
where
ptt.trans_id=pt.trans_id
and ptt.payment_type=2 and ptt.trans_type=2 and ptt.result=0
)
order by pt.trans_time desc";

            return new InlineQuery().ExecuteAsCollection<PaymentTransactionCollection>(strSQL);
        }

        public PaymentTransactionCollection PaymentTransactionGetListForCharging(bool manual = false)
        {
            string strSQL =
                string.Format(@"
IF OBJECT_ID('tempdb..#tmp') is not null DROP TABLE #tmp
select trans_id into #tmp from payment_transaction with(nolock)
	where order_guid in(select guid from [order] with(nolock) where order_status&8>0 
	and DATEADD(day,-{1},CONVERT(date ,getdate()))<=create_time and create_time<CONVERT(date ,getdate()) and {0})

select * from payment_transaction with(nolock)
where payment_type=1 and trans_type=0 and status&15=3 and amount>0 and trans_id in(
		select trans_id from #tmp
) and trans_id not in
(
	select trans_id from payment_transaction with(nolock)
	where payment_type=1 and trans_type=1 and amount>0 and trans_id in(
		select trans_id from #tmp
	)
)
            ", config.IsApplePayEMVManualProcess && manual ? " mobile_pay_type = 3 " : " isnull(mobile_pay_type,0) <> 3 ",
            config.ChargingDays);
            QueryCommand qc = new QueryCommand(strSQL, PaymentTransaction.Schema.Provider.Name);

            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("預設 command timeout: {0} 秒\r\n", qc.CommandTimeout);
            qc.CommandTimeout = 600;
            sb.AppendFormat("變更 command timeout: {0} 秒\r\n", qc.CommandTimeout);
            DateTime now = DateTime.Now;
            PaymentTransactionCollection result = new PaymentTransactionCollection();
            result.Load(DataService.GetReader(qc));

            sb.AppendFormat("執行時間: {0} 秒", (DateTime.Now - now).TotalSeconds.ToString("#.##"));
            log.Warn("PaymentTransactionGetListForCharging 相關訊息:\r\n" + sb);
            return result;
        }

        public PaymentTransactionCollection PaymentTransactionGetListForRefunding(bool manual = false)
        {
            string sql =
                string.Format(@"select * from payment_transaction with(nolock)
                where payment_type=1 and trans_type=2 and status&15=0 and amount>0
                and trans_id in
	                (select pt2.trans_id from payment_transaction pt2 with(nolock) inner join payment_transaction pt3 with(NOLOCK) on pt2.trans_id = pt3.trans_id and pt3.trans_type = 2 and pt3.payment_type = 5 inner join [order] o with(nolock) on o.guid = pt3.order_guid and {0} where pt2.payment_type = 1 and pt2.trans_type = 1 and pt2.status & 3 > 0 and pt2.trans_time<convert(varchar(10),GETDATE(),120))", config.IsApplePayEMVManualProcess && manual ? @"  o.mobile_pay_type = 3 " : " isnull(o.mobile_pay_type,0) <> 3 ");
            PaymentTransactionCollection result = new InlineQuery().ExecuteAsCollection<PaymentTransactionCollection>(sql);
            return result;
        }

        public bool TransactionidLogSet(TransactionidLog transactionidLog)
        {
            DB.Save<TransactionidLog>(transactionidLog);
            return true;
        }

        public void HitrustRefundRecordSet(HitrustRefundRecord record)
        {
            DB.Save<HitrustRefundRecord>(record);
        }

        #endregion PaymentTransaction

        #region PaymentTypeAmount

        public bool PaymentTypeAmountSet(PaymentTypeAmount paymentTypeAmount)
        {
            DB.Save<PaymentTypeAmount>(paymentTypeAmount);
            return true;
        }

        public bool PaymentTypeAmountCollectionSet(PaymentTypeAmountCollection paymentTypeAmountCollection)
        {
            DB.SaveAll<PaymentTypeAmount, PaymentTypeAmountCollection>(paymentTypeAmountCollection);
            return true;
        }

        #endregion PaymentTypeAmount

        #region ViewPponOrderDetail

        public ViewPponOrderDetailCollection ViewPponOrderDetailGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewPponOrderDetail>()
                .Where(ViewPponOrderDetail.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewPponOrderDetailCollection>();
        }

        public ViewPponOrderDetail ViewPponOrderDetailGet(Guid orderDetailGuid)
        {
            string sql = "select top 1 * from " + ViewPponOrderDetail.Schema.TableName +
                         " where " + ViewPponOrderDetail.Columns.OrderDetailGuid + " = @orderDetailGuid";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@orderDetailGuid", orderDetailGuid, DbType.Guid);
            ViewPponOrderDetail vpod = new ViewPponOrderDetail();
            vpod.LoadAndCloseReader(DataService.GetReader(qc));
            return vpod;
        }

        #endregion ViewPponOrderDetail

        #region ViewOrderMemberPayment

        public ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewOrderMemberPayment, ViewOrderMemberPaymentCollection>(pageStart, pageLength, orderBy, filter);
        }

        public ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForPcashRecords(string userName)
        {
            string sql = @"select * from view_order_member_payment with(nolock) where payment_type=2 and payment_status&7=3 and user_name=@uid order by payment_transaction_id desc";
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberPayment.Schema.Provider.Name);
            qc.Parameters.Add("uid", userName);
            ViewOrderMemberPaymentCollection vomplCol = new ViewOrderMemberPaymentCollection();
            vomplCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vomplCol;
        }

        public int ViewOrderMemberPaymentGetListForPcashRecordsCount(string userName)
        {
            string sql = @"select count(1) from view_order_member_payment with(nolock) where payment_type=2 and payment_status&7=3 and user_name=@uid ";
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberPayment.Schema.Provider.Name);
            qc.Parameters.Add("uid", userName);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForPcashRecords(int pageStart, int pageLength, string userName)
        {
            string sql = @"select * from view_order_member_payment with(nolock) where payment_type=2 and payment_status&7=3 and user_name=@uid order by payment_transaction_id desc";
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberPayment.Schema.Provider.Name);
            qc.Parameters.Add("uid", userName);
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, ViewOrderMemberPayment.Columns.PaymentTransactionId + " desc");
            ViewOrderMemberPaymentCollection vomplCol = new ViewOrderMemberPaymentCollection();
            vomplCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vomplCol;
        }

        public ViewOrderMemberPaymentCollection ViewOrderMemberPaymentGetListForCreditcardRecords(string userName)
        {
            string sql = "select * from view_order_member_payment with(nolock) where payment_type=1 and trans_type<>0 and payment_status&15=3 and user_name=@uid order by payment_transaction_id desc";
            QueryCommand qc = new QueryCommand(sql, ViewOrderMemberPayment.Schema.Provider.Name);
            qc.Parameters.Add("uid", userName);
            ViewOrderMemberPaymentCollection vomplCol = new ViewOrderMemberPaymentCollection();
            vomplCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vomplCol;
        }

        #endregion ViewOrderMemberPayment

        #region ViewPaymentTransactionLeftjoinOrder

        public ViewPaymentTransactionLeftjoinOrderCollection ViewPaymentTransactionLeftjoinOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPaymentTransactionLeftjoinOrder.Columns.PaymentTransactionId;
            QueryCommand qc = GetVploWhereQC(filter);
            qc.CommandSql = "select * from " + ViewPaymentTransactionLeftjoinOrder.Schema.Provider.DelimitDbName(ViewPaymentTransactionLeftjoinOrder.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewPaymentTransactionLeftjoinOrderCollection vploCol = new ViewPaymentTransactionLeftjoinOrderCollection();
            vploCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vploCol;
        }

        public int ViewPaymentTransactionLeftjoinOrderGetCount(params string[] filter)
        {
            QueryCommand qc = GetVploWhereQC(filter);
            qc.CommandSql = "select count(1) from " + ViewPaymentTransactionLeftjoinOrder.Schema.Provider.DelimitDbName(ViewPaymentTransactionLeftjoinOrder.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetVploWhereQC(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPaymentTransactionLeftjoinOrder.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewPaymentTransactionLeftjoinOrder.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return qc;
        }

        #endregion ViewPaymentTransactionLeftjoinOrder

        #region ViewPponOrderStatusLog

        public ViewPponOrderStatusLogCollection ViewPponOrderStatusLogGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponOrderStatusLog.Columns.OrderLogCreateTime;
            QueryCommand qc = GetVposWhereQC(filter);
            qc.CommandSql = "select * from " + ViewPponOrderStatusLog.Schema.Provider.DelimitDbName(ViewPponOrderStatusLog.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewPponOrderStatusLogCollection vposCol = new ViewPponOrderStatusLogCollection();
            vposCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vposCol;
        }

        public ViewPponOrderStatusLogCollection ViewPponOrderStatusLogGetListForApplicationInfo(Guid orderGuid)
        {
            QueryCommand qc = new QueryCommand(" select * from view_ppon_order_status_log with(nolock) where order_guid=@oid " +
                " and (order_log_status= " + (int)OrderLogStatus.Processing + " or order_log_status= " + (int)OrderLogStatus.ProcessingScashOnly +
                " or order_log_status= " + (int)OrderLogStatus.AutoProcessing + " or order_log_status= " + (int)OrderLogStatus.AutoProcessingScashOnly +
                " or order_log_status= " + (int)OrderLogStatus.ExchangeProcessing + " )",
                ViewPponOrderStatusLog.Schema.Provider.Name);

            qc.Parameters.Add("oid", orderGuid.ToString());
            ViewPponOrderStatusLogCollection view = new ViewPponOrderStatusLogCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        protected QueryCommand GetVposWhereQC(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponOrderStatusLog.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewPponOrderStatusLog.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return qc;
        }

        #endregion ViewPponOrderStatusLog

        #region ViewOrderReturnList

        public ViewPponOrderReturnListCollection ViewPponOrderReturnListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponOrderReturnList.Columns.ReturnCreateTime + " desc";
            QueryCommand qc = GetVprlWhereQC(filter);
            qc.CommandSql = "select * from view_ppon_order_return_list with(nolock)" + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewPponOrderReturnListCollection vporlCol = new ViewPponOrderReturnListCollection();
            vporlCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vporlCol;
        }

        public ViewPponOrderReturnListCollection ViewPponOrderReturnListByLastReturnStatusPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponOrderReturnList.Columns.ReturnCreateTime + " desc";
            QueryCommand qc = GetVprlWhereQC(filter);
            string filterSql = qc.CommandSql.Replace(ViewPponOrderReturnList.Schema.QualifiedName, "vporl").Replace("[" + ViewPponOrderReturnList.Schema.TableName + "]", "vporl");
            qc.CommandSql =
                @"select vporl.* from view_ppon_order_return_list vporl with(nolock)
                join (select order_guid,MAX(modify_time) as time from  [order_return_list]  with(nolock)
                group by order_guid  ) b on vporl.order_guid=b.order_guid and b.time=vporl.return_modify_time "
                + filterSql;

            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewPponOrderReturnListCollection vporlCol = new ViewPponOrderReturnListCollection();
            vporlCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vporlCol;
        }

        public int ViewPponOrderReturnListGetCount(params string[] filter)
        {
            QueryCommand qc = GetVprlWhereQC(filter);
            qc.CommandSql = "select count(1) from view_ppon_order_return_list with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public int ViewPponOrderReturnListGetCountByLastReturnStatus(params string[] filter)
        {
            QueryCommand qc = GetVprlWhereQC(filter);
            qc.CommandSql = "select count(1) from view_ppon_order_return_list  with(nolock)" +
                "join (select order_guid,MAX(modify_time) as time from order_return_list group by order_guid )b on [dbo].[view_ppon_order_return_list].order_guid=b.order_guid and b.time=[dbo].[view_ppon_order_return_list].return_modify_time" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetVprlWhereQC(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponOrderReturnList.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewPponOrderReturnList.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return qc;
        }

        public ViewPponOrderReturnListCollection ViewPponOrderReturnListGetList(IEnumerable<Guid> bids, OrderReturnType type)
        {
            var infos = new ViewPponOrderReturnListCollection();
            List<Guid> tempGuids = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 5 ;  // sql server cannot take more than 2100 parameters.
                                     // 17.3.27 mod 因為效能問題改成一次5筆就好
                var batchProd = DB.SelectAllColumnsFrom<ViewPponOrderReturnList>()
                                .Where(ViewPponOrderReturnList.Columns.BusinessHourGuid).In(tempGuids.Skip(idx).Take(batchLimit))
                                .And(ViewPponOrderReturnList.Columns.Type).IsEqualTo((int)type)
                                .ExecuteAsCollection<ViewPponOrderReturnListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewPponOrderReturnListCollection ViewPponOrderReturnListGetList(IEnumerable<Guid> productGuids, OrderReturnType type,
            IEnumerable<int> progressStatus, IEnumerable<int> vendorProgressStatus)
        {
            var infos = new ViewPponOrderReturnListCollection();
            List<Guid> tempGuids = productGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;
                string sql = string.Format(@"
select * from {0} 
where {1} in ('{2}') and {3} = @type", ViewPponOrderReturnList.Schema.TableName, ViewPponOrderReturnList.Columns.BusinessHourGuid,
                                       string.Join("','", tempGuids.Skip(idx).Take(batchLimit)), ViewPponOrderReturnList.Columns.Type);

                if (progressStatus.Any())
                {
                    sql += string.Format(" and {0} in ({1})", ViewPponOrderReturnList.Columns.ReturnStatus, string.Join(",", progressStatus));
                }

                if (vendorProgressStatus.Any())
                {
                    sql += string.Format(" and {0} in ({1})", ViewPponOrderReturnList.Columns.VendorProgressStatus, string.Join(",", vendorProgressStatus));
                }

                var qc = new QueryCommand(sql, ViewPponOrderReturnList.Schema.Provider.Name);
                qc.AddParameter("@type", (int)type, DbType.Int32);

                var data = new ViewPponOrderReturnListCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewPponOrderReturnList ViewOrderReturnListGet(int id)
        {
            return
                DB.SelectAllColumnsFrom<ViewPponOrderReturnList>()
                .Where(ViewPponOrderReturnList.Columns.ReturnId).IsEqualTo(id)
                .ExecuteSingle<ViewPponOrderReturnList>();
        }

        #endregion ViewOrderReturnList

        #region OrderReturnList

        public OrderReturnList OrderReturnListGet(Guid orderGuid)
        {
            return
                DB.SelectAllColumnsFrom<OrderReturnList>().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                ExecuteSingle<OrderReturnList>();
        }

        public OrderReturnList OrderReturnListGet(int id)
        {
            return
                DB.SelectAllColumnsFrom<OrderReturnList>().Where(OrderReturnList.IdColumn).IsEqualTo(id).
                ExecuteSingle<OrderReturnList>();
        }

        public OrderReturnList OrderReturnListGetbyType(Guid orderGuid, int? type)
        {
            if (type == null)
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    OrderDesc(OrderReturnList.Columns.ModifyTime).ExecuteSingle<OrderReturnList>();
            }
            else
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    And(OrderReturnList.TypeColumn).IsEqualTo(type).OrderDesc(OrderReturnList.Columns.ModifyTime).ExecuteSingle<OrderReturnList>();
            }
        }

        public int GetExchangeCountByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<OrderReturnList>().NoLock()
                .Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid)
                .And(OrderReturnList.TypeColumn).IsEqualTo((int)OrderReturnType.Exchange).GetRecordCount();
        }

        public OrderReturnListCollection OrderReturnListGetListByType(Guid orderGuid, int? type)
        {
            if (type == null)
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    OrderDesc(OrderReturnList.Columns.ModifyTime).ExecuteAsCollection<OrderReturnListCollection>();
            }
            else
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    And(OrderReturnList.TypeColumn).IsEqualTo(type).OrderDesc(OrderReturnList.Columns.ModifyTime).ExecuteAsCollection<OrderReturnListCollection>();
            }
        }

        public int OrderReturnListGetCountByType(Guid orderGuid, int? type)
        {
            if (type == null)
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    OrderDesc(OrderReturnList.Columns.ModifyTime).GetRecordCount();
            }
            else
            {
                return
                    DB.SelectAllColumnsFrom<OrderReturnList>().NoLock().Where(OrderReturnList.OrderGuidColumn).IsEqualTo(orderGuid).
                    And(OrderReturnList.TypeColumn).IsEqualTo(type).OrderDesc(OrderReturnList.Columns.ModifyTime).GetRecordCount();
            }
        }

        public bool OrderReturnListSet(OrderReturnList data)
        {
            if (data.IsNew)
            {
                data.CreateId = data.ModifyId;
                data.CreateTime = DateTime.Now;
            }
            DB.Save<OrderReturnList>(data);
            return true;
        }

        #endregion OrderReturnList

        #region CashPointOrder

        public bool CashPointOrderSet(CashpointOrder data)
        {
            DB.Save(data);
            return true;
        }

        #endregion CashPointOrder

        #region CashPointOrderDetail

        public bool CashPointOrderDetailSet(CashpointOrderDetail data)
        {
            DB.Save(data);
            return true;
        }

        #endregion CashPointOrderDetail

        #region CashPointList

        public bool CashPointListSet(CashpointList data)
        {
            if (data.UserId == 0)
            {
                log.WarnFormat("儲存 CashPointListSet 發生異常，userId={0}", data.UserId);
            }
            DB.Save(data);
            return true;
        }

        public decimal CashPointListSum(string username, int lastId)
        {
            string sql = @"select case when SUM(cpl.amount)IS NULL  then 0 else SUM(cpl.amount) end
                from cashpoint_list cpl with(nolock)
                inner join member m with(nolock) on m.unique_id = cpl.user_id
                where m.user_name =@username and cpl.id<=@lastId and cpl.type<>2";
            QueryCommand qc = new QueryCommand(sql, CashpointList.Schema.Provider.Name);
            qc.AddParameter("@username", username, DbType.String);
            qc.AddParameter("@lastId", lastId, DbType.Int32);

            return (decimal)DataService.ExecuteScalar(qc);
        }

        public CashpointListCollection CashpointGetList(Guid guid)
        {
            string sql = @"select * from cashpoint_list with(nolock) where reference=@guid and type<>2";
            QueryCommand qc = new QueryCommand(sql, CashpointList.Schema.Provider.Name);
            qc.AddParameter("@guid", guid, DbType.Guid);
            CashpointListCollection data = new CashpointListCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewMemberCashpointListCollection CashpointOldGetRecordsWithTransScash(int pageStart, int pageLength, string username)
        {
            string sql = @"select * from " + ViewMemberCashpointList.Schema.TableName + " with(nolock) where " +
                ViewMemberCashpointList.Columns.Type + " <> " + (int)CashPointListType.Reject + " and " +
                ViewMemberCashpointList.Columns.Username + "=@username and create_time < '2012-5-08 06:00' order by " + ViewMemberCashpointList.Columns.Id;
            QueryCommand qc = new QueryCommand(sql, ViewMemberCashpointList.Schema.Provider.Name);
            qc.AddParameter("@username", username, DbType.String);
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, GetOrderDetailOrderSql(ViewMemberCashpointList.Columns.Id, null));
            ViewMemberCashpointListCollection data = new ViewMemberCashpointListCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        public int CashpointGetCount(string username)
        {
            string sql = @"select count(*) from " + ViewMemberCashpointList.Schema.TableName + " with(nolock) where " +
                ViewMemberCashpointList.Columns.Username + "=@username and " + ViewMemberCashpointList.Columns.Type + " <>2 ";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@username", username, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion CashPointList

        #region ScashTransaction

        public bool ScashDepositSet(ScashDeposit data)
        {
            if (data.UserId == 0)
            {
                log.WarnFormat("儲存 ScashDepositSet 發生異常，userId={0}", data.UserId);
            }
            DB.Save(data);
            return true;
        }

        public bool ScashDepositDelete(Guid orderGuid)
        {
            DB.Destroy<ScashDeposit>(ScashDeposit.OrderGuidColumn.ColumnName, orderGuid);
            return true;
        }

        public bool ScashWithdrawalSet(ScashWithdrawal data)
        {
            DB.Save(data);
            return true;
        }

        public bool ScashWithdrawalDelete(Guid orderGuid)
        {
            DB.Destroy<ScashWithdrawal>(ScashWithdrawal.OrderGuidColumn.ColumnName, orderGuid);
            return true;
        }

        public ViewScashTransactionCollection ViewScashTransactionGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewScashTransaction.Columns.Invoiced + "," + ViewScashTransaction.Columns.CreateTime + " desc";
            QueryCommand qc = GetDCWhere(ViewScashTransaction.Schema, filter);
            qc.CommandSql = "select * from " + ViewScashTransaction.Schema.Provider.DelimitDbName(ViewScashTransaction.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';');
                defOrderBy = " order by " + ViewScashTransaction.Columns.Invoiced + "," + orderBy + " desc";
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewScashTransactionCollection vscCol = new ViewScashTransactionCollection();
            vscCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vscCol;
        }

        /// <summary>
        /// 購買時，開過發票的購物金優先使用
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewScashTransactionCollection ViewScashTransactionGetListByNotBalanceOrderByInvoicedFirst(int userId)
        {
            string sql = @"
                select * from view_scash_transaction with(nolock) where user_id = @userId 
                    and balance <> 0 order by invoiced desc, create_time";

            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.Parameters.Add("@userId", userId, DbType.Int32);
            ViewScashTransactionCollection vscCol = new ViewScashTransactionCollection();
            vscCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vscCol;
        }

        public ViewScashTransactionCollection ViewScashTransactionGetAll(string username)
        {
            string sql = @"select * from " + ViewScashTransaction.Schema.Provider.DelimitDbName(ViewScashTransaction.Schema.TableName) + " with(nolock) where " + ViewScashTransaction.Columns.UserName + "=@username" +
                " order by " + ViewScashTransaction.Columns.CreateTime + "," + ViewScashTransaction.Columns.WithdrawalId;
            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.AddParameter("@username", username, DbType.String);
            ViewScashTransactionCollection vscCol = new ViewScashTransactionCollection();
            vscCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vscCol;
        }

        public int ViewScashTransactionGetCount(string userName)
        {
            string sql = @"select count(*) from " + ViewScashTransaction.Schema.TableName + " with(nolock) where " +
                ViewScashTransaction.Columns.UserName + "=@username";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@username", userName, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public decimal ScachTransactionSum(string username)
        {
            decimal d = DB.Select(Aggregate.Sum(ViewScashTransaction.Columns.Amount)).From(ViewScashTransaction.Schema)
                .Where(ViewScashTransaction.Columns.UserName).IsEqualTo(username)
                .ExecuteScalar<decimal>();
            return d;
        }

        public decimal ScachTransactionSum(int userId)
        {
            decimal d = DB.Select(Aggregate.Sum(ViewScashTransaction.Columns.Amount)).From(ViewScashTransaction.Schema)
                .Where(ViewScashTransaction.Columns.UserId).IsEqualTo(userId)
                .ExecuteScalar<decimal>();
            return d;
        }

        public ViewScashTransactionCollection ViewScashTransactionGetListHaveBalance(int userId)
        {
            string sql = @"select * from view_scash_transaction where user_id=@userId and balance <> 0";
            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            ViewScashTransactionCollection vscCol = new ViewScashTransactionCollection();
            vscCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vscCol;
        }

        public decimal ScashGetByOrder(Guid orderGuid, out decimal scash, out decimal pscash)
        {
            scash = 0;
            pscash = 0;
            string sql = @"
select 
	isnull(sum(ctl.scash) , 0) as scash,
	isnull(sum(ctl.pscash) , 0) as pscash
from cash_trust_log ctl where ctl.order_guid = @orderGuid
";
            QueryCommand qc = new QueryCommand(sql, ScashWithdrawal.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            using (var reader = DataService.GetReader(qc))
            {
                if (reader.Read())
                {
                    scash = (decimal)reader.GetInt32(0);
                    pscash = (decimal)reader.GetInt32(1);
                }
            }
            return scash + pscash;
        }
        public DataTable PezScashRecordsDataTableGet(int userId)
        {
            string sql = @"
                select * from (

                select 
	                'deposit' as [action],
	                pd.id as deposit_id,	
	                null as withdrawal_id,	
                    pxo.pez_auth_code as pez_auth_code, 
	                pd.message as message, 
	                pd.amount as amount, 
	                case when pd.order_guid is null then N'sys' else pd.create_id end as create_id ,
	                pd.create_time as create_time
                from pscash_deposit pd 
                inner join pcash_xch_order pxo on pxo.id = pd.pcash_xch_order_id
                where pd.user_id = @userId

                union

                select 
	                'withdrawal' as [action],
	                pd.id as deposit_id,	
	                pw.id as withdrawal_id,
                    pxo.pez_auth_code, 
	                pw.message as message, 
	                pw.amount * -1 as amount, 
	                pw.create_id as create_id,
	                pw.create_time as create_time
                from pscash_withdrawal pw
                inner join pscash_deposit pd on pd.id = pw.deposit_id
                inner join pcash_xch_order pxo on pxo.id = pd.pcash_xch_order_id
                where pd.user_id = @userId


                ) as t
                order by t.create_time
            ";
            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);

            return DataService.GetDataSet(qc).Tables[0];
        }
        /// <summary>
        /// 查詢pscash仍有可用額度的的deposit
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public List<PscashDepositBalance> PscashDepositBalanceGetListForWithdrawal(int userId)
        {
            string sql = @"
                select deposit_id, balance, pez_auth_code,guid from (
	                select 
                        pd.id as deposit_id,
	                    pd.amount - isnull( (select sum(amount) from pscash_withdrawal pw where pw.deposit_id = pd.id) , 0) as balance,
                        pxo.pez_auth_code,pxo.guid
                from pscash_deposit pd inner join pcash_xch_order pxo on pxo.id = pd.pcash_xch_order_id
                    where pd.user_id = @userId) as t 
                where t.balance > 0
                order by t.deposit_id asc
            ";
            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);

            List<PscashDepositBalance> result = new List<PscashDepositBalance>();
            using (var reader = DataService.GetReader(qc))
            {
                while(reader.Read())
                {
                    result.Add(new PscashDepositBalance
                    {
                        DepositId = reader.GetInt32(0),
                        Balance = reader.GetDecimal(1),
                        PezAuthCode = reader.GetString(2),
                        Guid = reader.GetGuid(3),
                    });
                }
            }

            return result;
        }
        /// <summary>
        /// 查一張訂單用到了哪幾筆 deposit
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public List<PscashWithdrawalView> PscashWithdrawalViewGetByOrder(Guid orderGuid)
        {
            string sql = @"
                select 	                
	                pw.id, 
                    pw.amount,
	                pw.deposit_id,
	                pw.order_guid, 
	                pd.user_id,
	                pd.pcash_xch_order_id from pscash_withdrawal pw 	
                 inner join pscash_deposit pd on pd.id = pw.deposit_id
                 where pw.order_guid = @orderGuid
            ";
            QueryCommand qc = new QueryCommand(sql, ViewScashTransaction.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);

            List<PscashWithdrawalView> result = new List<PscashWithdrawalView>();
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(new PscashWithdrawalView
                    {
                        Id = reader.GetInt32(0),
                        Amount = reader.GetDecimal(1),
                        DepositId = reader.GetInt32(2),
                        OrderGuid = reader.GetGuid(3),
                        UserId = reader.GetInt32(4),
                        PcashXchOrderId = reader.GetInt32(5)
                    });
                }
            }

            return result;
        }

        public bool PscashDepositDelete(Guid orderGuid)
        {
            DB.Destroy<PscashDeposit>(PscashDeposit.OrderGuidColumn.ColumnName, orderGuid);
            return true;
        }
        public bool PscashWithdrawalDelete(Guid orderGuid)
        {
            DB.Destroy<PscashWithdrawal>(PscashWithdrawal.OrderGuidColumn.ColumnName, orderGuid);
            return true;
        }

        #endregion ScashTransaction

        #region pscash_order

        public PcashXchOrder PcashXchOrderGet(string pezAuthCode)
        {
            return DB.Get<PcashXchOrder>(PcashXchOrder.Columns.PezAuthCode, pezAuthCode);
        }

        public void PcashXchOrderSet(PcashXchOrder pxOrder)
        {
            if (pxOrder.CreateTime == default(DateTime))
            {
                pxOrder.CreateTime = DateTime.Now;
            }
            if (pxOrder.IsDirty)
            {
                DB.Save(pxOrder);
            }
        }

        public PcashXchOrder PscashOrderGetByGuid(Guid pxOrderGuid)
        {
            return DB.Get<PcashXchOrder>(PcashXchOrder.Columns.Guid, pxOrderGuid);
        }

        public void PscashDepositSet(PscashDeposit deposit)
        {
            DB.Save(deposit);
        }

        public void PscashWithdrawalSet(PscashWithdrawal withdrawal)
        {
            DB.Save(withdrawal);
        }

        #endregion pcash_xch_order

        #region Einvoice

        public void EinvoiceSetSerial(EinvoiceSerial data)
        {
            DB.Save<EinvoiceSerial>(data);
        }

        public EinvoiceSerialCollection EinvoiceSerialCollectionGetByDate(string d)
        {
            string sql = @"select * from " + EinvoiceSerial.Schema.TableName + " with(nolock) where @d = " + EinvoiceSerial.Columns.DateCode + " order by " + EinvoiceSerial.Columns.Id;
            QueryCommand qc = new QueryCommand(sql, EinvoiceSerial.Schema.Provider.Name);
            qc.AddParameter("@d", d, DbType.String);
            EinvoiceSerialCollection data = new EinvoiceSerialCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceSerial EinvoiceSerialCollectionGetByInvInfo(string InvoiceNumber, DateTime InvoiceNumberTime)
        {
            int Num = int.Parse(InvoiceNumber.Substring(2, InvoiceNumber.Length - 2));
            string headCode = InvoiceNumber.Substring(0, 2);
            string sql = @"select top 1 * from " + EinvoiceSerial.Schema.TableName + " with(nolock) where @Num BETWEEN min_serial and max_serial and @InvTime BETWEEN start_date and end_date and head_code = @headCode Order BY id desc";
            QueryCommand qc = new QueryCommand(sql, EinvoiceSerial.Schema.Provider.Name);
            qc.AddParameter("@Num", Num, DbType.Int32);
            qc.AddParameter("@InvTime", InvoiceNumberTime, DbType.DateTime);
            qc.AddParameter("@headCode", headCode, DbType.String);
            EinvoiceSerial data = new EinvoiceSerial();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceSerial EinvoiceSerialCollectionGetById(int id)
        {
            string sql = @"select top 1 * from " + EinvoiceSerial.Schema.TableName + " with(nolock) where id = @id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceSerial.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            EinvoiceSerial data = new EinvoiceSerial();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int EinvoiceLogGetCount(string datecode, int invoice_type)
        {
            string sql = @"select count(*) from " + EinvoiceLog.Schema.TableName + " with(nolock) where " +
                EinvoiceLog.Columns.DateCode + "=@datecode and " + EinvoiceLog.Columns.InvoiceType + "=@invoicetype";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@datecode", datecode, DbType.String);
            qc.AddParameter("@invoicetype", invoice_type, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public EinvoiceLogCollection EinvoiceLogCollectionGet(DateTime d1, DateTime d2)
        {
            string sql = @"select * from " + EinvoiceLog.Schema.TableName + " with(nolock) where " + EinvoiceLog.Columns.CreateTime + "  >=@d1 and " + EinvoiceLog.Columns.CreateTime + " <@d2 and " +
                EinvoiceLog.Columns.InvoiceType + "<>" + (int)EinvoiceType.DailyReport + " order by " + EinvoiceLog.Columns.Id;
            QueryCommand qc = new QueryCommand(sql, EinvoiceLog.Schema.Provider.Name);
            qc.AddParameter("@d1", d1, DbType.DateTime);
            qc.AddParameter("@d2", d2, DbType.DateTime);
            EinvoiceLogCollection data = new EinvoiceLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void EinvoiceLogSet(EinvoiceLog data)
        {
            DB.Save<EinvoiceLog>(data);
        }

        public void EinvoiceSetMain(EinvoiceMain main)
        {
            DB.Save<EinvoiceMain>(main);
        }

        public void EinvoiceUpdateSum(decimal sum_amount, int id, int invoice_status)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.Columns.InvoiceSumAmount + "=" + EinvoiceMain.InvoiceSumAmountColumn + "-@sumamount," +
                EinvoiceMain.Columns.InvoiceStatus + "=@invoicestatus  where " + EinvoiceMain.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int64);
            qc.AddParameter("@sumamount", sum_amount, DbType.Decimal);
            qc.AddParameter("@invoicestatus", invoice_status, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceRequestPaper(string message, string invoice_number, string buyer_name, string buyer_address)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.Columns.InvoiceRequestTime + "=getdate()," + EinvoiceMain.Columns.Message + "=@message," +
                EinvoiceMain.Columns.InvoiceBuyerName + "=@buyername," + EinvoiceMain.Columns.InvoiceBuyerAddress + "=@buyeraddress  where " + EinvoiceMain.Columns.InvoiceNumber + "=@invoicenum";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@invoicenum", invoice_number, DbType.String);
            qc.AddParameter("@buyername", buyer_name, DbType.String);
            qc.AddParameter("@buyeraddress", buyer_address, DbType.String);
            qc.AddParameter("@message", message, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceUpdateFileSerial(int id, int fileserial, DateTime now)
        {
            string sql = @"update " + EinvoiceDetail.Schema.TableName + " set " + EinvoiceDetail.Columns.Status + "=1 , " + EinvoiceDetail.Columns.InvoicedTime + "=@now," + EinvoiceDetail.Columns.InvoiceFileSerial + "=@fileserial where " + EinvoiceDetail.IdColumn + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceDetail.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int64);
            qc.AddParameter("@now", now, DbType.DateTime);
            qc.AddParameter("@fileserial", fileserial, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceDetailUpdateAllowanceNumber(int id, int fileserial, string allowanceNum, DateTime now)
        {
            string sql = @"update " + EinvoiceDetail.Schema.TableName + " set " + EinvoiceDetail.Columns.Status + "=1 , " + EinvoiceDetail.Columns.InvoicedTime + "=@now," + EinvoiceDetail.Columns.AllowanceNumber + "=@allowanceNum," + EinvoiceDetail.Columns.InvoiceFileSerial + "=@fileserial where " + EinvoiceDetail.IdColumn + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceDetail.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int64);
            qc.AddParameter("@now", now, DbType.DateTime);
            qc.AddParameter("@allowanceNum", allowanceNum, DbType.String);
            qc.AddParameter("@fileserial", fileserial, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public EinvoiceDetailCollection EinvoiceDetailCollectionGet(int main_id)
        {
            return
                   DB.SelectAllColumnsFrom<EinvoiceDetail>().Where(EinvoiceDetail.MainIdColumn).IsEqualTo(main_id).
                   OrderDesc(EinvoiceDetail.CreateTimeColumn.ColumnName).ExecuteAsCollection<EinvoiceDetailCollection>();
        }

        public void EinvoiceUpdatePaperTime(int id, DateTime now)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.Columns.InvoicePaperedTime + "=@now," + EinvoiceMain.Columns.InvoicePapered + "=1 where " + EinvoiceMain.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@now", now, DbType.DateTime);
            qc.AddParameter("@id", id, DbType.Int64);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceUpdateComInfo(int id, string comtitle, string comid, string name, string address)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceComNameColumn + "=@comtitle,"
                + EinvoiceMain.InvoiceComIdColumn + "=@comid," + EinvoiceMain.InvoiceBuyerNameColumn + "=@name," +
                EinvoiceMain.InvoiceBuyerAddressColumn + "=@address where " + EinvoiceMain.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@comtitle", comtitle, DbType.String);
            qc.AddParameter("@comid", comid, DbType.String);
            qc.AddParameter("@address", address, DbType.String);
            qc.AddParameter("@name", name, DbType.String);
            qc.AddParameter("@id", id, DbType.Int64);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceUpdateComInfo(int id, bool backpaper, bool allowance)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceMailbackPaperColumn + "=@invoice_mailback_paper," + EinvoiceMain.InvoiceMailbackAllowanceColumn + "=@invoice_mailback_allowance" +
                 " where " + EinvoiceMain.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int64);
            qc.AddParameter("@invoice_mailback_paper", backpaper, DbType.Boolean);
            qc.AddParameter("@invoice_mailback_allowance", allowance, DbType.Boolean);
            DataService.ExecuteScalar(qc);
        }

        public EinvoiceMainCollection EinvoiceMainCollectionGet(DateTime date_start, DateTime date_end, DateTime newInvoiceDate, bool isnull)
        {
            string sql = "select * from " + EinvoiceMain.Schema.TableName + " with(nolock) where ((" +
                EinvoiceMain.Columns.OrderTime + "<  @newInvoiceDate and (" + EinvoiceMain.Columns.OrderTime + " >= @date_start and " + EinvoiceMain.Columns.OrderTime + " < @date_end)) or (" +
                EinvoiceMain.Columns.OrderTime + ">= @newInvoiceDate and (" + EinvoiceMain.Columns.VerifiedTime + " >= @date_start and " + EinvoiceMain.Columns.VerifiedTime + " < @date_end))) and " +
                EinvoiceMain.Columns.OrderAmount + ">0 and " +
                EinvoiceMain.Columns.InvoiceNumber + " is " + (isnull ? string.Empty : "not") + " null and " + EinvoiceMain.Columns.InvoiceStatus + "<>" + (int)EinvoiceType.NotComplete +
                " order by " + EinvoiceMain.Columns.VerifiedTime;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@newInvoiceDate", newInvoiceDate, DbType.DateTime);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        //新版發票要加入三聯
        public ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGet(DateTime date_start, DateTime date_end, DateTime newInvoiceDate, int invoice_type, bool with_triplicate)
        {
            string sql = "select top 500 * from " + ViewEinvoiceMaindetail.Schema.TableName + " with(nolock) where ((" +
                ViewEinvoiceMaindetail.Columns.OrderTime + "<  @newInvoiceDate and (" + ViewEinvoiceMaindetail.Columns.OrderTime + " >= @date_start and " + ViewEinvoiceMaindetail.Columns.OrderTime + " < @date_end)) or (" +
                ViewEinvoiceMaindetail.Columns.OrderTime + ">= @newInvoiceDate and (" + ViewEinvoiceMaindetail.Columns.VerifiedTime + " >= @date_start and " + ViewEinvoiceMaindetail.Columns.VerifiedTime + " < @date_end))) and " +
                ViewEinvoiceMaindetail.Columns.Status + " =0 and " + ViewEinvoiceMaindetail.Columns.InvoiceType + "=@invoicetype " + (with_triplicate ? string.Empty : (" and " + ViewEinvoiceMaindetail.Columns.InvoiceMode + " not in (0,18,22)"));
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceMaindetail.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@newInvoiceDate", newInvoiceDate, DbType.DateTime);
            qc.AddParameter("@invoicetype", invoice_type, DbType.Int32);
            ViewEinvoiceMaindetailCollection data = new ViewEinvoiceMaindetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        //新版發票要加入三聯
        public ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGet(DateTime date_start, DateTime date_end, string column_name, int invoice_type, bool with_triplicate)
        {
            string sql = "select top 500 * from " + ViewEinvoiceMaindetail.Schema.TableName + " with(nolock) where " + column_name + " >= @date_start and " + column_name + " <@date_end and "
                + ViewEinvoiceMaindetail.Columns.Status + " =0 and " + ViewEinvoiceMaindetail.Columns.InvoiceType + "=@invoicetype " + (with_triplicate ? string.Empty : ("and " + ViewEinvoiceMaindetail.Columns.InvoiceMode + " not in (0,18,22)"));
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceMaindetail.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@invoicetype", invoice_type, DbType.Int32);
            ViewEinvoiceMaindetailCollection data = new ViewEinvoiceMaindetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceMaindetailCollection EinvoiceMainDetailCollectionGetVoid(DateTime date_start, DateTime date_end, bool IsVoid)
        {
            string sql = "select * from " + ViewEinvoiceMaindetail.Schema.TableName + " with(nolock) where " + ViewEinvoiceMaindetail.Columns.InvoiceVoidTime + " >= @date_start and " + ViewEinvoiceMaindetail.Columns.InvoiceVoidTime + " <@date_end and " + ViewEinvoiceMaindetail.Columns.InvoiceStatus + " =@invoice_type and " + ViewEinvoiceMaindetail.Columns.InvoiceType + "=1 ";
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceMaindetail.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@invoice_type", IsVoid ? (int)EinvoiceType.Initial : (int)EinvoiceType.C0701, DbType.Int32);
            ViewEinvoiceMaindetailCollection data = new ViewEinvoiceMaindetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceMaindetail EinvoiceMainDetailGet(int id, int invoice_type)
        {
            string sql = "select * from " + ViewEinvoiceMaindetail.Schema.TableName + " with(nolock) where " + ViewEinvoiceMaindetail.Columns.DetailId + " =@id and "
                + ViewEinvoiceMaindetail.Columns.Status + "=0";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int64);
            ViewEinvoiceMaindetail data = new ViewEinvoiceMaindetail();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceMaindetail EinvoiceMainDetailGet(int returnFromId)
        {
            string sql = string.Format(@"select * from {0} d with(nolock)
                                        where d.{1} = @rid and d.{1} not in 
                                        (select {1} from {0} 
                                        where {2} in ({3}, {4}) and {1} = d.{1})
                                    ", ViewEinvoiceMaindetail.Schema.TableName
                                    , ViewEinvoiceMaindetail.Columns.ReturnFormId
                                    , ViewEinvoiceMaindetail.Columns.InvoiceType, (int)EinvoiceType.D0501, (int)EinvoiceType.C0701);

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@rid", returnFromId, DbType.Int64);
            ViewEinvoiceMaindetail data = new ViewEinvoiceMaindetail();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoiceMainCollectionGet(string column_name, string value, bool allowEmptyNum)
        {
            string sql;
            if (column_name == Member.Columns.UserName)
            {
                sql = "select  * from " + EinvoiceMain.Schema.TableName + " em with(nolock)" +
                    " inner join " + Member.Schema.TableName + " m with(nolock) on m." + Member.Columns.UniqueId +
                        " =  em." + EinvoiceMain.Columns.UserId +
                    " where m." + column_name + "  =@value ";
            }
            else
            {
                sql = "select  * from " + EinvoiceMain.Schema.TableName +
                    " with(nolock) where " + column_name + "  =@value ";
            }
            sql += allowEmptyNum ? string.Empty : " and " + EinvoiceMain.Columns.InvoiceNumber + " is not null";

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@value", value, DbType.String);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMain EinvoiceMainGet(string num)
        {
            return DB.Get<EinvoiceMain>(EinvoiceMain.Columns.InvoiceNumber, num);
        }

        public EinvoiceMain EinvoiceMainGet(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            return
                DB.SelectAllColumnsFrom<EinvoiceMain>().Where(EinvoiceMain.Columns.OrderGuid).IsEqualTo(order_guid).And(
                    EinvoiceMain.Columns.OrderClassification).IsEqualTo(orderClassification).ExecuteSingle<EinvoiceMain>();
            //return DB.Get<EinvoiceMain>(EinvoiceMain.Columns.OrderGuid, order_guid);
        }

        public EinvoiceMain EinvoiceMainGetById(int id)
        {
            return DB.Get<EinvoiceMain>(EinvoiceMain.Columns.Id, id);
        }

        public EinvoiceMain EinvoiceMainGetByCouponid(int couponId, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            return
                DB.SelectAllColumnsFrom<EinvoiceMain>().Where(EinvoiceMain.Columns.CouponId).IsEqualTo(couponId).And(
                    EinvoiceMain.Columns.OrderClassification).IsEqualTo(orderClassification).ExecuteSingle<EinvoiceMain>();
            //return DB.Get<EinvoiceMain>(EinvoiceMain.Columns.CouponId, couponId);
        }

        public EinvoiceMainCollection EinvoiceMainCollectionGetByOrderId(string orderId, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            return
                DB.SelectAllColumnsFrom<EinvoiceMain>().Where(EinvoiceMain.Columns.OrderId).IsEqualTo(orderId).And(
                    EinvoiceMain.Columns.OrderClassification).IsEqualTo(orderClassification).ExecuteAsCollection<EinvoiceMainCollection>();
        }

        public void EinvoiceMainVerifiedTimeSet(int couponId, OrderClassification orderClassification, DateTime verifiedTime,bool isJf = false)
        {
            if (isJf)
            {
                DB.Update<EinvoiceMain>().Set(EinvoiceMain.Columns.VerifiedTime).EqualTo(verifiedTime)
                    .Where(EinvoiceMain.Columns.CouponId).IsEqualTo(couponId)
                    .And(EinvoiceMain.Columns.OrderClassification).IsEqualTo((int)orderClassification)
                    .And(EinvoiceMain.Columns.VerifiedTime).IsNull().Execute();
            }
            else
            {
                string sql = string.Format("update {0} set {1}=@verified_time where {2}=@coupon_id and {3}=@order_classification and verified_time is null",
                EinvoiceMain.Schema.TableName,
                EinvoiceMain.Columns.VerifiedTime,
                EinvoiceMain.Columns.CouponId,
                EinvoiceMain.Columns.OrderClassification);
                QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
                qc.AddParameter("@verified_time", verifiedTime, DbType.DateTime);
                qc.AddParameter("@coupon_id", couponId, DbType.Int32);
                qc.AddParameter("@order_classification", (int)orderClassification, DbType.Int32);
                DataService.ExecuteScalar(qc);
            }
        }

        public void PartnerEinvoiceMainVerifiedTimeSet(int couponId, DateTime verifiedTime)
        {
            DB.Update<PartnerEinvoiceMain>().Set(PartnerEinvoiceMain.Columns.VerifiedTime).EqualTo(verifiedTime)
                   .Where(PartnerEinvoiceMain.Columns.CouponId).IsEqualTo(couponId)
                   .And(PartnerEinvoiceMain.Columns.VerifiedTime).IsNull().Execute();
        }


        public ViewEinvoiceAllowanceInstoreCollection EinvoiceAllowanceInstoreCollectionGet(DateTime date_start, DateTime date_end)
        {
            string sql = "select  * from " + ViewEinvoiceAllowanceInstore.Schema.TableName + " with(nolock) where " + ViewEinvoiceAllowanceInstore.Columns.RefundTime + " >=@date_start and " + ViewEinvoiceAllowanceInstore.Columns.RefundTime + " <@date_end and " + ViewEinvoiceAllowanceInstore.Columns.InvoiceNumberTime + " is not null ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            ViewEinvoiceAllowanceInstoreCollection data = new ViewEinvoiceAllowanceInstoreCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceAllowanceInstoreIncludeScashCollection EinvoiceAllowanceInstoreIncludeScashCollectionGet(DateTime date_start, DateTime date_end)
        {
            string sql = "select  * from " + ViewEinvoiceAllowanceInstoreIncludeScash.Schema.TableName + " with(nolock) where " + ViewEinvoiceAllowanceInstoreIncludeScash.Columns.RefundTime + " >=@date_start and " + ViewEinvoiceAllowanceInstoreIncludeScash.Columns.RefundTime + " <@date_end and " + ViewEinvoiceAllowanceInstoreIncludeScash.Columns.InvoiceNumberTime + " is not null ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            ViewEinvoiceAllowanceInstoreIncludeScashCollection data = new ViewEinvoiceAllowanceInstoreIncludeScashCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(DateTime date_start, DateTime date_end)
        {
            var sql = string.Format(GetEinvoiceAllowanceSqlWithStringFormatParm(), @"AND ctl.modify_time >= @date_start 
AND ctl.modify_time < @date_end 
AND ed.payment_trans_id is null");

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);

            return GetEinvoiceAllowanceInfoFromQuery(qc);
        }

        public List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetListV2(DateTime date_start, DateTime date_end)
        {
            var sql = string.Format(GetEinvoiceAllowanceSqlWithStringFormatParmV2(), @"AND ctl.modify_time >= @date_start 
AND ctl.modify_time < @date_end 
AND ed.payment_trans_id is null");

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);

            return GetEinvoiceAllowanceInfoFromQuery(qc);
        }

        public List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(int einvoice_id)
        {
            var sql = string.Format(GetEinvoiceAllowanceSqlWithStringFormatParm(), @"AND m.id = @id ");
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@id", einvoice_id, DbType.Int32);

            return GetEinvoiceAllowanceInfoFromQuery(qc);
        }

        public List<EinvoiceAllowanceInfo> EinvoiceAllowanceGetList(List<int> einvoice_id_list)
        {
            var sql = string.Format(GetEinvoiceAllowanceSqlWithStringFormatParm(), string.Format(@"AND m.id in ({0})", string.Join(",", einvoice_id_list)));
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);

            return GetEinvoiceAllowanceInfoFromQuery(qc);
        }

        public void EinvoiceSetDetail(EinvoiceDetail data)
        {
            DB.Save<EinvoiceDetail>(data);
        }

        public void EinvoiceMainCollectionSet(EinvoiceMainCollection invCol)
        {
            DB.SaveAll<EinvoiceMain, EinvoiceMainCollection>(invCol);
        }

        public EinvoiceMain EinvoiceMainGetByUser(int userId, Guid orderGuid, bool hasNum)
        {
            string sql = "select  top 1 * from " + EinvoiceMain.Schema.TableName + " with(nolock) where " + EinvoiceMain.Columns.UserId + " =@userid and  "
                + EinvoiceMain.Columns.OrderGuid + "=@orderguid  " + (hasNum ? (" and " + EinvoiceMain.Columns.InvoiceNumber + " is not null") : string.Empty);
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@userid", userId, DbType.Int32);
            qc.AddParameter("@orderguid", orderGuid, DbType.Guid);
            EinvoiceMain data = new EinvoiceMain();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoiceMainGetListByOrderGuid(Guid order_guid, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            EinvoiceMainCollection em = DB.SelectAllColumnsFrom<EinvoiceMain>().NoLock()
                .Where(EinvoiceMain.Columns.OrderGuid).IsEqualTo(order_guid)
                .And(EinvoiceMain.Columns.OrderClassification).IsEqualTo(orderClassification)
                .ExecuteAsCollection<EinvoiceMainCollection>();
            return em ?? new EinvoiceMainCollection();
        }

        /// <summary>
        /// 訂單裡有開過發票 (發票號碼有值) 的項目
        /// </summary>
        /// <param name="order_guid"></param>
        /// <returns></returns>
        public EinvoiceMainCollection EinvoiceMainInvoicedOnlyGetListByOrderGuid(Guid order_guid)
        {
            EinvoiceMainCollection em = DB.SelectAllColumnsFrom<EinvoiceMain>()
                .Where(EinvoiceMain.Columns.OrderGuid).IsEqualTo(order_guid)
                .And(EinvoiceMain.Columns.InvoiceNumber).IsNotNull().ExecuteAsCollection<EinvoiceMainCollection>();
            return em ?? new EinvoiceMainCollection();
        }

        /// <summary>
        /// 訂單裡未開過發票 (發票號碼無值) 的項目
        /// </summary>
        /// <param name="order_guid"></param>
        /// <returns></returns>
        public EinvoiceMainCollection EinvoiceMainUninvoicedOnlyGetListByOrderGuid(Guid order_guid)
        {
            EinvoiceMainCollection em = DB.SelectAllColumnsFrom<EinvoiceMain>()
                .Where(EinvoiceMain.Columns.OrderGuid).IsEqualTo(order_guid)
                .And(EinvoiceMain.Columns.InvoiceNumber).IsNull()
                .And(EinvoiceMain.Columns.CouponId).IsNull()
                .ExecuteAsCollection<EinvoiceMainCollection>();
            return em ?? new EinvoiceMainCollection();
        }

        public EinvoiceMainCollection EinvoiceMainByUserId(int userId, DateTime d_start, DateTime d_end)
        {
            EinvoiceMainCollection em = DB.SelectAllColumnsFrom<EinvoiceMain>()
                .Where(EinvoiceMain.Columns.UserId).IsEqualTo(userId)
                .And(EinvoiceMain.Columns.InvoiceNumber).IsNotNull()
                .And(EinvoiceMain.Columns.InvoiceNumberTime).IsNotNull()
                .And(EinvoiceMain.Columns.InvoiceWinning).IsEqualTo(true)
                .And(EinvoiceMain.Columns.InvoiceMode2).IsEqualTo((int)InvoiceMode2.Duplicate)
                .And(EinvoiceMain.Columns.InvoiceNumberTime).IsBetweenAnd(d_start, d_end)
                .ExecuteAsCollection<EinvoiceMainCollection>();
            return em ?? new EinvoiceMainCollection();
        }

        public EinvoiceMainCollection EinvoiceMainGetByWinner(int userId, DateTime d_start, DateTime d_end)
        {
            string sql = "select  * from " + EinvoiceMain.Schema.TableName + " with(nolock) where " + EinvoiceMain.Columns.UserId + " =@userid and  " +
                     EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.InvoiceWinning + "=@winner and " +
                     EinvoiceMain.Columns.InvoiceNumberTime + ">=@d_start and " + EinvoiceMain.Columns.InvoiceNumberTime + "<@d_end and " + EinvoiceMain.Columns.InvoiceMode2 + " =" + (int)InvoiceMode2.Duplicate + " and " +
                     EinvoiceMain.Columns.InvoicePaperedTime + " is null";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@userid", userId, DbType.Int32);
            qc.AddParameter("@winner", true, DbType.Boolean);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoiceMainGetByVoid(DateTime d_start, DateTime d_end, bool IsVoid)
        {
            string sql = "select  * from " + EinvoiceMain.Schema.TableName + " with(nolock) where " +
                    EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.InvoiceStatus + "=@status and " +
                    EinvoiceMain.Columns.InvoiceVoidTime + ">=@d_start and " + EinvoiceMain.Columns.InvoiceVoidTime + "<@d_end ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@status", IsVoid ? (int)EinvoiceType.Initial : (int)EinvoiceType.C0701, DbType.Int32);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy,bool hasWinning, InvoiceQueryOption query)
        {
            string sql = "select * from " + EinvoiceMain.Schema.TableName + " with(nolock) where " +
                (query == InvoiceQueryOption.InvoiceNumberTime ? EinvoiceMain.Columns.InvoiceNumberTime : EinvoiceMain.Columns.InvoiceRequestTime) + " >=@date_start and " + (query == InvoiceQueryOption.InvoiceNumberTime ? EinvoiceMain.Columns.InvoiceNumberTime : EinvoiceMain.Columns.InvoiceRequestTime) + " <@date_end and " +
                EinvoiceMain.Columns.InvoiceRequestTime + " is not null and " +
                EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.OrderAmount + ">0 and " +
                EinvoiceMain.Columns.InvoiceSumAmount + ">0 and " + (isCopy ? "" : EinvoiceMain.Columns.InvoicePapered + "=0 and ") +
                EinvoiceMain.Columns.InvoiceMode2 + "=@invoice_mode2 and " +
                EinvoiceMain.Columns.InvoiceStatus + "=@invoice_status" + (hasWinning ? "" : " and " + EinvoiceMain.Columns.InvoiceWinning + "=0");
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@invoice_mode2", (int)invoicemode, DbType.Int32);
            qc.AddParameter("@invoice_status", (int)EinvoiceType.C0401, DbType.Int32);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoicePaperMediaCollectionGetByDate(DateTime date_start, DateTime date_end)
        {
            string sql = "select * from " + EinvoiceMain.Schema.TableName + " with(nolock) where " + EinvoiceMain.Columns.InvoiceRequestTime + " >=@date_start and " + EinvoiceMain.Columns.InvoiceRequestTime + " <@date_end and " +
                  EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.OrderAmount + ">0 and " +
                  EinvoiceMain.Columns.InvoiceSumAmount + ">0 and " +
                  EinvoiceMain.Columns.InvoiceMode2 + " = " + InvoiceMode2.Triplicate;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceMailCollection ViewEinvoiceMailGetListByOrderGuid(Guid orderGuid)
        {
            ViewEinvoiceMailCollection mails = DB.SelectAllColumnsFrom<ViewEinvoiceMail>().Where(ViewEinvoiceMail.Columns.OrderGuid).IsEqualTo(orderGuid).ExecuteAsCollection<ViewEinvoiceMailCollection>();
            return mails ?? new ViewEinvoiceMailCollection();
        }

        public ViewEinvoiceMailCollection EinvoiceMailCollectionGet(DateTime date_start, DateTime date_end)
        {
            string sql = "select  * from " + ViewEinvoiceMail.Schema.TableName + " with(nolock) where " +
                ViewEinvoiceMail.Columns.InvoiceNumberTime + " >=@date_start and " + ViewEinvoiceMail.Columns.InvoiceNumberTime + " <@date_end  and ISNULL(" + ViewEinvoiceMail.Columns.LoveCode + ",'')=''";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.CommandTimeout = 120;
            ViewEinvoiceMailCollection data = new ViewEinvoiceMailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEinvoiceMailCollection EinvoiceWinnerMailCollectionGet(DateTime date_start, DateTime date_end)
        {
            string sql = "select  * from " + ViewEinvoiceMail.Schema.TableName + " with(nolock) where " +
           ViewEinvoiceMail.Columns.InvoiceNumberTime + " >=@date_start and " + ViewEinvoiceMail.Columns.InvoiceNumberTime + " <@date_end  and " +
           ViewEinvoiceMail.Columns.InvoiceWinning + "=@winner and " + ViewEinvoiceMail.Columns.InvoicePaperedTime + " is null and " + ViewEinvoiceMail.Columns.InvoiceMode2 + "=" + (int)InvoiceMode2.Duplicate + " and ISNULL(" + ViewEinvoiceMail.Columns.LoveCode + ",'')=''";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@winner", true, DbType.Boolean);
            qc.CommandTimeout = 120;
            ViewEinvoiceMailCollection data = new ViewEinvoiceMailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int EinvoiceDetailGetCount(int main_id, int invoice_type)
        {
            string sql = @"select count(*) from " + EinvoiceDetail.Schema.TableName + " with(nolock) where " +
                EinvoiceDetail.Columns.MainId + "=@mainid and " + EinvoiceDetail.Columns.InvoiceType + "=@invoicetype";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@mainid", main_id, DbType.UInt32);
            qc.AddParameter("@invoicetype", invoice_type, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public decimal EinvoiceCompareDailyIncome(DateTime d_satrt, DateTime d_end)
        {
            string sql = "select isnull(sum(amount),0) from " + PaymentTransaction.Schema.TableName +
        " with(nolock) where " + PaymentTransaction.Columns.PaymentType + "=1 and " + PaymentTransaction.Columns.TransType + "=0 and " + PaymentTransaction.Columns.Status + "&15=3 and " +
        PaymentTransaction.Columns.OrderGuid + " is not null and " + PaymentTransaction.Columns.Amount + ">0 and @d1<=" + PaymentTransaction.Columns.TransTime + " and " +
        PaymentTransaction.Columns.TransTime + "<@d2 and " + PaymentTransaction.Columns.OrderGuid + " not in(select guid from [dbo].[order] with(nolock))";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@d1", d_satrt, DbType.DateTime);
            qc.AddParameter("@d2", d_end, DbType.DateTime);
            return (decimal)DataService.ExecuteScalar(qc);
        }

        public void EinvoiceUpdateWinner(string winnername, string winnerphone, string winneraddress, int userId, DateTime d_start, DateTime d_end)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceBuyerNameColumn + "=@winnername," +
                EinvoiceMain.InvoiceBuyerAddressColumn + "=@winneraddress," + EinvoiceMain.InvoiceWinnerresponseTimeColumn + "=getdate()," +
                EinvoiceMain.InvoiceWinnerresponsePhoneColumn + "=@winnerphone where " +
                EinvoiceMain.Columns.UserId + " =@userid and  " +
                EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.InvoiceWinning + "=@winner and " +
                EinvoiceMain.Columns.InvoiceNumberTime + ">=@d_start and " + EinvoiceMain.Columns.InvoiceNumberTime + "<@d_end and " + EinvoiceMain.Columns.InvoiceMode2 + " =" + (int)InvoiceMode2.Duplicate + " and " +
                EinvoiceMain.Columns.InvoicePaperedTime + " is null and (" + ViewEinvoiceMail.Columns.LoveCode + " is null or " + ViewEinvoiceMail.Columns.LoveCode + "='')";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@winnername", winnername, DbType.String);
            qc.AddParameter("@winneraddress", winneraddress, DbType.String);
            qc.AddParameter("@winnerphone", winnerphone, DbType.String);
            qc.AddParameter("@userid", userId, DbType.Int32);
            qc.AddParameter("@winner", true, DbType.Boolean);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            DataService.ExecuteScalar(qc);
        }

        public ViewEinvoiceWinnerCollection EinvoiceWinnerGetList(int pageStart, int pageLength, DateTime d_start, DateTime d_end, string filter)
        {
            string defOrderBy = ViewEinvoiceWinner.Columns.InvoiceNumberTime;
            string sql = @"SELECT * from " + ViewEinvoiceWinner.Schema.TableName + " with(nolock) WHERE " + ViewEinvoiceWinner.Columns.InvoiceNumberTime + ">=@d_start and " +
                ViewEinvoiceWinner.Columns.InvoiceNumberTime + "<@d_end " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceWinner.Schema.Provider.Name);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy + " desc");
            }
            ViewEinvoiceWinnerCollection ocol = new ViewEinvoiceWinnerCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewEinvoiceWinnerCollection EinvoiceWinnerGetList(DateTime d_start, DateTime d_end, string filter)
        {
            string defOrderBy = ViewEinvoiceWinner.Columns.InvoiceNumberTime;
            string sql = @"SELECT * from " + ViewEinvoiceWinner.Schema.TableName + " with(nolock) WHERE " + ViewEinvoiceWinner.Columns.InvoiceNumberTime + ">=@d_start and " +
                ViewEinvoiceWinner.Columns.InvoiceNumberTime + "<@d_end " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceWinner.Schema.Provider.Name);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);

            ViewEinvoiceWinnerCollection ocol = new ViewEinvoiceWinnerCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public int EinvoiceWinnerGetCount(DateTime d_start, DateTime d_end, string filter)
        {
            string sql = @"select count(*) from " + ViewEinvoiceWinner.Schema.TableName + " with(nolock) WHERE " + ViewEinvoiceWinner.Columns.InvoiceNumberTime + ">=@d_start and " +
                ViewEinvoiceWinner.Columns.InvoiceNumberTime + "<@d_end " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceWinner.Schema.Provider.Name);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewEinvoiceWinnerCollection EinvoiceWinnerGetByNum(string invoicenum)
        {
            string sql = @"SELECT * from " + ViewEinvoiceWinner.Schema.TableName + " with(nolock) WHERE " + ViewEinvoiceWinner.Columns.InvoiceNumber + "=@invoicenum";
            QueryCommand qc = new QueryCommand(sql, ViewEinvoiceWinner.Schema.Provider.Name);
            qc.AddParameter("@invoicenum", invoicenum, DbType.String);
            ViewEinvoiceWinnerCollection ocol = new ViewEinvoiceWinnerCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public void EinvoiceUpdateWinner(string invoicenumber, string winnername, string winneraddress)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceBuyerNameColumn + "=@winnername," +
                EinvoiceMain.InvoiceBuyerAddressColumn + "=@winneraddress," + EinvoiceMain.MessageColumn + "=" + EinvoiceMain.MessageColumn +
                "+N'(客服修改中獎資訊" + DateTime.Now.ToString("MMddHHmm") + ")' , " +
                EinvoiceMain.InvoiceWinnerresponseTimeColumn + "=case when invoice_winnerresponse_phone is null then GETDATE() else " + EinvoiceMain.InvoiceWinnerresponseTimeColumn + " end where " +
                EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.Columns.InvoiceWinning + "=@winner and " +
                EinvoiceMain.Columns.InvoiceNumber + "=@invoicenumber and " + EinvoiceMain.Columns.InvoiceMode2 + " = " + (int)InvoiceMode2.Duplicate + " and (" + ViewEinvoiceMail.Columns.LoveCode + " is null or " + ViewEinvoiceMail.Columns.LoveCode + "='')";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@winnername", winnername, DbType.String);
            qc.AddParameter("@winneraddress", winneraddress, DbType.String);
            qc.AddParameter("@winner", true, DbType.Boolean);
            qc.AddParameter("@invoicenumber", invoicenumber, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        public void EinvoiceMainCollectionSetWinningStatus(DateTime d_start, DateTime d_end, string number)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceWinningColumn + "=@winning  where " +
                    EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " +
                    EinvoiceMain.Columns.InvoiceNumberTime + ">=@d_start and " + EinvoiceMain.Columns.InvoiceNumberTime + "<@d_end and " +
                    EinvoiceMain.Columns.InvoiceNumber + " like @number";

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@winning", true, DbType.Boolean);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            qc.AddParameter("@number", "%" + number, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        public int EinvoiceMainCollectionSetWinningStatusCount(DateTime d_start, DateTime d_end, string number)
        {
            string sql = "update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceWinningColumn + "=@winning  where " +
                    EinvoiceMain.Columns.InvoiceNumberTime + " is not null  and " + EinvoiceMain.Columns.InvoiceNumber + " is not null and " + EinvoiceMain.InvoiceWinningColumn + "=0 and " +
                    EinvoiceMain.Columns.InvoiceNumberTime + ">=@d_start and " + EinvoiceMain.Columns.InvoiceNumberTime + "<@d_end and " + EinvoiceMain.Columns.InvoiceStatus + "=@invoicestatus and " +
                    EinvoiceMain.Columns.InvoiceNumber + " like @number and " + EinvoiceMain.Columns.CarrierType + " = " + (int)CarrierType.Member;

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@winning", true, DbType.Boolean);
            qc.AddParameter("@d_start", d_start, DbType.DateTime);
            qc.AddParameter("@d_end", d_end, DbType.DateTime);
            qc.AddParameter("@number", "%" + number, DbType.String);
            qc.AddParameter("@invoicestatus", (int)EinvoiceType.C0401, DbType.Int32);
            var result = DB.Execute(qc);
            return result;
        }

        public EinvoiceMainCollection EinvoiceGetListByToHouseTrustIds(string trustIds)
        {
            string sql = string.Format(@"select distinct inv.* from cash_trust_log ctl
            join einvoice_main inv on ctl.order_guid=inv.order_guid where ctl.special_status&{0}=0 {1}",
            (int)TrustSpecialStatus.Freight, !string.IsNullOrEmpty(trustIds) ? "and ctl.trust_id in(" + trustIds + ")" : "and ctl.trust_id is null");
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EinvoiceMainCollection EinvoiceGetListByToShopTrustIds(string trustIds)
        {
            string sql = string.Format(@"select distinct inv.* from cash_trust_log ctl
            join einvoice_main inv on ctl.coupon_id=inv.coupon_id where ctl.special_status&{0}=0 {1}",
            (int)TrustSpecialStatus.Freight, !string.IsNullOrEmpty(trustIds) ? "and ctl.trust_id in(" + trustIds + ")" : "and ctl.trust_id is null");
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            EinvoiceMainCollection data = new EinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void EinvoiceMainCollectionSetWinnersStatus(string invoiceNumbers)
        {
            string sql = string.Format(@"update " + EinvoiceMain.Schema.TableName + " set " + EinvoiceMain.InvoiceWinningColumn + "= @winning  where " +
                    EinvoiceMain.Columns.InvoiceNumber + " is not null and " +
                    EinvoiceMain.Columns.InvoiceNumber + " in ({0}) and " +
                    EinvoiceMain.Columns.InvoiceWinning + " = 0 ", invoiceNumbers);

            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@winning", true, DbType.Boolean);
            //qc.AddParameter("@numbers", invoiceNumbers, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 發生反核銷時取消發票，成套票券檔次不取消發票
        /// </summary>
        /// <param name="oGuid"></param>
        /// <param name="couponId"></param>
        /// <param name="isGroupCoupon"></param>
        public void CancelEinvoiceSetByUndo(Guid oGuid, int couponId, bool isGroupCoupon)
        {
            //todo:已開立發票的作廢流程待補
            try
            {
                if (!isGroupCoupon)
                {
                    EinvoiceMainCollection emc = EinvoiceMainCollectionGet(EinvoiceMain.OrderGuidColumn.ColumnName, oGuid.ToString(), true);
                    EinvoiceMain em = emc.FirstOrDefault(x => x.CouponId == couponId && x.InvoiceNumber == null && x.InvoiceMode2 == (int)InvoiceMode2.Duplicate); //三聯反核銷不影響開立
                    em.VerifiedTime = null;
                    EinvoiceSetMain(em);
                }
            }
            catch
            {

            }
        }

        public void EinvoiceChangeLogSet(EinvoiceChangeLog log)
        {
            DB.Save(log);
        }

        public void EinvoiceChangeLogCollectionSet(EinvoiceChangeLogCollection log)
        {
            DB.SaveAll<EinvoiceChangeLog, EinvoiceChangeLogCollection>(log);
        }

        public ViewEinvoiceChangeLogCollection ViewEinvoiceChangeLogCollectionGet(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewEinvoiceChangeLog>().NoLock()
                .Where(ViewEinvoiceChangeLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewEinvoiceChangeLogCollection>();
        }

        /**********************************************/

        public void AddTaxDeclareEinvoiceMedia(DateTime taxDstart, DateTime taxDend)
        {
            try
            {
                string sql = "insert into EinvoiceMedia(id,InvoiceNumber,InvoiceNumberTime,InvoiceComId,InvoiceAmount,InvoiceTax,InvoiceMode,InvoiceMode2) " +
                            "SELECT id, invoice_number, invoice_number_time, invoice_com_id, order_amount, invoice_tax, invoice_mode2 as invoice_mode, invoice_mode2 as invoice_mode2 " +
                            "from einvoice_main with(nolock) where invoice_number_time >= @taxDstart AND invoice_number_time < @taxDend ";

            }
            catch
            {

            }
        }

        public DataTable GetNonEinvoicedOrderMember(DateTime start, DateTime end)
        {
            string sql = string.Format(@"
	        --依訂單開立之發票(宅配/成套票券)
	        select o.member_name,o.mobile_number,o.order_id,o.create_id,o.create_time,rf.create_time 
	        from [order] o with(nolock) 
	        join return_form rf with(nolock) on rf.order_guid = o.guid
	        join einvoice_main em with(nolock) on em.order_id = o.order_id and em.coupon_id is null
	        where em.invoice_number_time>='{0}' and em.invoice_number_time<'{1}' 
	        and rf.create_time >= '{0}'
	        and em.invoice_mode2={2} 
	        and (em.invoice_number is not null or em.invoice_number!='') 
	        group by o.member_name,o.mobile_number,o.order_id,o.create_id,o.create_time,rf.create_time  
	        union all
	        --依憑證號碼開立之訂單發票
	        select o.member_name,o.mobile_number,o.order_id,o.create_id,o.create_time,rf.create_time  
	        from [order] o with(nolock) 
	        join return_form rf with(nolock) on rf.order_guid = o.guid
	        join return_form_refund rfr with(nolock) on rfr.return_form_id=rf.id and rfr.is_refunded = 1 
			join cash_trust_log ctl with(nolock) on ctl.trust_id = rfr.trust_id
	        join einvoice_main em with(nolock) on rf.order_id=em.order_id and em.coupon_id=ctl.coupon_id
	        where em.invoice_number_time>='{0}' and em.invoice_number_time<'{1}' 
	        and rf.create_time >= '{0}'
	        and em.invoice_mode2={2} and rf.delivery_type={3}  
	        and (em.invoice_number is not null or em.invoice_number!='') 
	        group by o.member_name,o.mobile_number,o.order_id,o.create_id,o.create_time,rf.create_time  
	        order by o.create_time asc", start.ToString("yyyy-MM-dd"), end.ToString("yyyy-MM-dd"),
                                       (int)InvoiceMode2.Triplicate, (int)DeliveryType.ToShop);
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetEinvoiceMedia(string com_id)
        {
            string sql = string.Format(@"select * from einvoice_media with(nolock) where seller_com_id = '{0}' order by id", com_id);

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion Einvoice

        #region VendorEinvoiceMain

        public void VendorEinvoiceMainSet(VendorEinvoiceMain vem)
        {
            DB.Save(vem);
        }

        public void VendorEinvoiceMainCollectionSet(VendorEinvoiceMainCollection vemc)
        {
            DB.SaveAll(vemc);
        }

        public VendorEinvoiceMain VendorEinvoiceMainGet(string num)
        {
            return DB.Get<VendorEinvoiceMain>(VendorEinvoiceMain.Columns.InvoiceNumber, num);
        }

        public VendorEinvoiceMainCollection VendorEinvoiceCollectionGetByCancelDate(DateTime date_start, DateTime date_end)
        {
            return DB.SelectAllColumnsFrom<VendorEinvoiceMain>().NoLock()
               .Where(VendorEinvoiceMain.Columns.CancelDate).IsGreaterThanOrEqualTo(date_start).And(VendorEinvoiceMain.Columns.CancelDate).IsLessThan(date_end)
               .ExecuteAsCollection<VendorEinvoiceMainCollection>();
        }

        public VendorEinvoiceMainCollection VendorEinvoiceMainCollectionGetNew()
        {
            return DB.SelectAllColumnsFrom<VendorEinvoiceMain>().NoLock()
                .Where(VendorEinvoiceMain.Columns.InvoiceNumber).IsNull()
                .ExecuteAsCollection<VendorEinvoiceMainCollection>();
        }

        public VendorEinvoiceMainCollection VendorEinvoiceMainCollectionGetUpload()
        {
            return DB.SelectAllColumnsFrom<VendorEinvoiceMain>().NoLock()
                .Where(VendorEinvoiceMain.Columns.InvoiceNumber).IsNotNull()
                .And(VendorEinvoiceMain.Columns.IsUpload).IsEqualTo(false)
                .ExecuteAsCollection<VendorEinvoiceMainCollection>();
        }

        public VendorEinvoiceMainCollection VendorEinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy)
        {
            string sql = "select * from " + VendorEinvoiceMain.Schema.TableName + " with(nolock) where " +
                VendorEinvoiceMain.Columns.InvoiceNumberTime + " >=@date_start and " + VendorEinvoiceMain.Columns.InvoiceNumberTime + " <@date_end and " +
                VendorEinvoiceMain.Columns.InvoiceNumber + " is not null and " + VendorEinvoiceMain.Columns.OrderAmount + ">0 and " +
                (isCopy ? "" : VendorEinvoiceMain.Columns.InvoicePapered + "=0 and ") +
                VendorEinvoiceMain.Columns.InvoiceMode2 + "=@invoice_mode2 and " +
                VendorEinvoiceMain.Columns.InvoiceStatus + "=@invoice_status";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@invoice_mode2", (int)invoicemode, DbType.Int32);
            qc.AddParameter("@invoice_status", (int)EinvoiceType.C0401, DbType.Int32);
            VendorEinvoiceMainCollection data = new VendorEinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region PartnerEinvoiceMain

        public void PartnerEinvoiceMainSet(PartnerEinvoiceMain main)
        {
            DB.Save<PartnerEinvoiceMain>(main);
        }

        public PartnerEinvoiceMainCollection PartnerEinvoiceMainGet(string pOrderId, int pType)
        {
            return
                DB.SelectAllColumnsFrom<PartnerEinvoiceMain>().Where(PartnerEinvoiceMain.Columns.PartnerOrderId).IsEqualTo(pOrderId).And(PartnerEinvoiceMain.Columns.PartnerType).IsEqualTo(pType).ExecuteAsCollection<PartnerEinvoiceMainCollection>();
        }

        public PartnerEinvoiceMainCollection PartnerEinvoiceMainGetByDate(DateTime sDate, DateTime eDate)
        {
            return
               DB.SelectAllColumnsFrom<PartnerEinvoiceMain>().Where(PartnerEinvoiceMain.Columns.VerifiedTime).IsGreaterThanOrEqualTo(sDate).And(PartnerEinvoiceMain.Columns.VerifiedTime).IsLessThan(eDate).ExecuteAsCollection<PartnerEinvoiceMainCollection>();
        }

        public PartnerEinvoiceMain PartnerEinvoiceMainGet(string num)
        {
            return DB.Get<PartnerEinvoiceMain>(PartnerEinvoiceMain.Columns.InvoiceNumber, num);
        }

        public PartnerEinvoiceMainCollection PartnerEinvoicePaperCollectionGetByDate(DateTime date_start, DateTime date_end, InvoiceMode2 invoicemode, bool isCopy, bool hasWinning, InvoiceQueryOption query)
        {
            string sql = "select * from " + PartnerEinvoiceMain.Schema.TableName + " with(nolock) where " +
                (query == InvoiceQueryOption.InvoiceNumberTime ? PartnerEinvoiceMain.Columns.InvoiceNumberTime : PartnerEinvoiceMain.Columns.InvoiceRequestTime) + " >=@date_start and " + (query == InvoiceQueryOption.InvoiceNumberTime ? PartnerEinvoiceMain.Columns.InvoiceNumberTime : PartnerEinvoiceMain.Columns.InvoiceRequestTime) + " <@date_end and " +
                PartnerEinvoiceMain.Columns.InvoiceRequestTime + " is not null and " +
                PartnerEinvoiceMain.Columns.InvoiceNumber + " is not null and " + PartnerEinvoiceMain.Columns.OrderAmount + ">0 and " +
                PartnerEinvoiceMain.Columns.InvoiceSumAmount + ">0 and " + (isCopy ? "" : PartnerEinvoiceMain.Columns.InvoicePapered + "=0 and ") +
                PartnerEinvoiceMain.Columns.InvoiceMode2 + "=@invoice_mode2 and " +
                PartnerEinvoiceMain.Columns.InvoiceStatus + "=@invoice_status" + (hasWinning ? "" : " and " + PartnerEinvoiceMain.Columns.InvoiceWinning + "=0");
            QueryCommand qc = new QueryCommand(sql, PartnerEinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@invoice_mode2", (int)invoicemode, DbType.Int32);
            qc.AddParameter("@invoice_status", (int)EinvoiceType.C0401, DbType.Int32);
            PartnerEinvoiceMainCollection data = new PartnerEinvoiceMainCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void PartnerEinvoiceUpdatePaperTime(string invoiceNumber, DateTime now)
        {
            string sql = "update " + PartnerEinvoiceMain.Schema.TableName + " set " + PartnerEinvoiceMain.Columns.InvoicePaperedTime + "=@now," + PartnerEinvoiceMain.Columns.InvoicePapered + "=1 where " + PartnerEinvoiceMain.Columns.InvoiceNumber + "=@invoiceNumber";
            QueryCommand qc = new QueryCommand(sql, PartnerEinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@now", now, DbType.DateTime);
            qc.AddParameter("@invoiceNumber", invoiceNumber, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region PartnerEinvoiceDetail

        public void PartnerEinvoiceDetailSet(PartnerEinvoiceDetail detail)
        {
            DB.Save<PartnerEinvoiceDetail>(detail);
        }

        public PartnerEinvoiceDetailCollection PartnerEinvoiceDetailCollectionGetByDate(DateTime sDate,DateTime eDate)
        {
            return
                DB.SelectAllColumnsFrom<PartnerEinvoiceDetail>().Where(PartnerEinvoiceDetail.Columns.CreateTime).IsGreaterThanOrEqualTo(sDate).And(PartnerEinvoiceDetail.Columns.CreateTime).IsLessThan(eDate).ExecuteAsCollection<PartnerEinvoiceDetailCollection>();
        }

        #endregion

        #region ViewPartnerEinvoiceMaindetail

        public ViewPartnerEinvoiceMaindetailCollection ViewPartnerEinvoiceMaindetailCollectionGetByDate(DateTime sDate, DateTime eDate)
        {
            return
                DB.SelectAllColumnsFrom<ViewPartnerEinvoiceMaindetail>().Where(ViewPartnerEinvoiceMaindetail.Columns.DetailCreateTime).IsGreaterThanOrEqualTo(sDate).And(ViewPartnerEinvoiceMaindetail.Columns.DetailCreateTime).IsLessThan(eDate).ExecuteAsCollection<ViewPartnerEinvoiceMaindetailCollection>();
        }

        public ViewPartnerEinvoiceMaindetailCollection ViewPartnerEinvoiceWinnerMailCollectionGet(DateTime date_start, DateTime date_end)
        {
            string sql = "select  * from " + ViewPartnerEinvoiceMaindetail.Schema.TableName + " with(nolock) where " +
           ViewPartnerEinvoiceMaindetail.Columns.InvoiceNumberTime + " >=@date_start and " + ViewPartnerEinvoiceMaindetail.Columns.InvoiceNumberTime + " <@date_end  and " +
           ViewPartnerEinvoiceMaindetail.Columns.InvoiceWinning + "=@winner and " + ViewPartnerEinvoiceMaindetail.Columns.InvoicePaperedTime + " is null and " + ViewPartnerEinvoiceMaindetail.Columns.InvoiceMode2 + "=" + (int)InvoiceMode2.Duplicate + " and ISNULL(" + ViewPartnerEinvoiceMaindetail.Columns.LoveCode + ",'')=''";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@date_start", date_start, DbType.DateTime);
            qc.AddParameter("@date_end", date_end, DbType.DateTime);
            qc.AddParameter("@winner", true, DbType.Boolean);
            qc.CommandTimeout = 120;
            ViewPartnerEinvoiceMaindetailCollection data = new ViewPartnerEinvoiceMaindetailCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        #endregion

        #region 代收轉付

        public void EntrustSellReceiptSet(EntrustSellReceipt receipt)
        {
            DB.Save(receipt);
        }

        public EntrustSellReceipt EntrustSellReceiptGetByOrder(Guid orderGuid)
        {
            return DB.Get<EntrustSellReceipt>(EntrustSellReceipt.Columns.OrderGuid, orderGuid);
        }

        public List<EntrustSellReceipt> EntrustSellReceiptGetListByOrder(Guid orderGuid)
        {
            Query query = new Query(EntrustSellReceipt.Schema.TableName)
                .AddWhere(EntrustSellReceipt.Columns.OrderGuid, Comparison.Equals, orderGuid);

            EntrustSellReceiptCollection result = new EntrustSellReceiptCollection();
            result.LoadAndCloseReader(query.ExecuteReader());
            return result.ToList();
        }

        public EntrustSellReceipt EntrustSellReceiptGetByTrustId(Guid trustId)
        {
            return DB.Get<EntrustSellReceipt>(EntrustSellReceipt.Columns.TrustId, trustId);
        }

        public EntrustSellReceipt EntrustSellReceiptGetByCouponId(int couponId)
        {
            return DB.Get<EntrustSellReceipt>(EntrustSellReceipt.Columns.CouponId, couponId);
        }

        public List<ViewEntrustSellDailyPayment> ViewEntrustSellDailyPaymentList(DateTime? startTime, DateTime? endTime)
        {
            Query query = new Query(ViewEntrustSellDailyPayment.Schema.TableName);
            if (startTime == null && endTime != null)
            {
                query.AddWhere(ViewEntrustSellDailyPayment.Columns.TransTime, Comparison.LessThan, endTime.Value);
            }
            else if (startTime != null && endTime == null)
            {
                query.AddWhere(ViewEntrustSellDailyPayment.Columns.TransTime, Comparison.GreaterOrEquals, startTime.Value);
            }
            else if (startTime != null & endTime != null)
            {
                query.AddBetweenValues(ViewEntrustSellDailyPayment.Columns.TransTime, startTime.Value, endTime.Value);
            }

            query.ORDER_BY(ViewEntrustSellDailyPayment.Columns.TransTime, "desc");
            ViewEntrustSellDailyPaymentCollection result = new ViewEntrustSellDailyPaymentCollection();
            result.LoadAndCloseReader(query.ExecuteReader());
            //result.LoadAndCloseReader(DataService.Providers[config.DbPerformanceMode == DbPerformanceMode.Basic ? Helper.GetLocalizedEnum(DataProviderType.Publisher)
            //                                                                           : Helper.GetLocalizedEnum(SSHelper.GetDeterminableDataProviderType(theType))].GetReader(qc));
            return result.ToList();
        }

        public List<ViewEntrustSellReceipt> ViewEntrustSellReceiptList(string filterColumn, string filterValue,
            DateTime? verifiedStartTime, DateTime? verifiedEndTime, bool? isPhysical, bool? isExported, bool? hasReceiptCode)
        {
            Query query = new Query(ViewEntrustSellReceipt.Schema.TableName);
            if (string.IsNullOrEmpty(filterValue) == false)
            {
                if (filterColumn == "ProductQc")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.ProductQc, Comparison.Equals, filterValue);
                }
                else if (filterColumn == "OrderId")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.OrderId, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "MemberName")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.MemberName, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "SellerName")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.SellerName, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "MemberId")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.MemberId, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "ReceiptCode")
                {
                    query.AddWhere(ViewEntrustSellReceipt.Columns.ReceiptCode, Comparison.Like, "%" + filterValue + "%");
                }
                else
                {
                    throw new Exception("找不到查詢的欄位, filterColumn=" + filterColumn);
                }
            }
            if (verifiedStartTime == null && verifiedEndTime != null)
            {
                query.AddWhere(ViewEntrustSellReceipt.Columns.VerifiedTime, Comparison.LessThan, verifiedEndTime.Value);
            }
            else if (verifiedStartTime != null && verifiedEndTime == null)
            {
                query.AddWhere(ViewEntrustSellReceipt.Columns.VerifiedTime, Comparison.GreaterOrEquals, verifiedStartTime.Value);
            }
            else if (verifiedStartTime != null & verifiedEndTime != null)
            {
                query.AddBetweenValues(ViewEntrustSellReceipt.Columns.VerifiedTime, verifiedStartTime.Value, verifiedEndTime.Value);
            }
            if (isPhysical != null)
            {
                query.AddWhere(ViewEntrustSellReceipt.Columns.IsPhysical, Comparison.Equals, true);
            }
            if (isExported != null)
            {
                query.AddWhere(ViewEntrustSellReceipt.Columns.IsExported, Comparison.Equals, true);
            }
            if (hasReceiptCode != null)
            {
                query.AddWhere(ViewEntrustSellReceipt.Columns.ReceiptCode, Comparison.IsNot, null);
            }

            query.ORDER_BY(ViewEntrustSellReceipt.Columns.ReceiptId, "desc");
            ViewEntrustSellReceiptCollection result = new ViewEntrustSellReceiptCollection();
            result.LoadAndCloseReader(query.ExecuteReader());
            //result.LoadAndCloseReader(DataService.Providers[config.DbPerformanceMode == DbPerformanceMode.Basic ? Helper.GetLocalizedEnum(DataProviderType.Publisher)
            //                                                                           : Helper.GetLocalizedEnum(SSHelper.GetDeterminableDataProviderType(theType))].GetReader(qc));
            return result.ToList();
        }

        /// <summary>
        /// 匯出後更動IsExported以做註記
        /// </summary>
        /// <param name="receiptIds"></param>
        public void EntrustSellReceiptsSetExported(int[] receiptIds)
        {
            foreach (int id in receiptIds)
            {
                EntrustSellReceipt receipt = EntrustSellReceiptGet(id);
                receipt.IsExported = true;
                EntrustSellReceiptSet(receipt);
            }
        }

        public EntrustSellReceipt EntrustSellReceiptGet(int receiptId)
        {
            return DB.Get<EntrustSellReceipt>(EntrustSellReceipt.Columns.Id, receiptId);
        }

        public ViewEntrustsellOrderCollection EntruslSellOrderList(
            string filterColumn, string filterValue, string filterOrderStatus, DateTime? startTime, DateTime? endTime)
        {
            Query query = new Query(ViewEntrustsellOrder.Schema.TableName);
            if (string.IsNullOrEmpty(filterValue) == false)
            {
                if (filterColumn == "ProductQc")
                {
                    query.AddWhere(ViewEntrustsellOrder.Columns.ProductQc, Comparison.Equals, filterValue);
                }
                else if (filterColumn == "OrderId")
                {
                    query.AddWhere(ViewEntrustsellOrder.Columns.OrderId, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "MemberName")
                {
                    query.AddWhere(ViewEntrustsellOrder.Columns.MemberName, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "SellerName")
                {
                    query.AddWhere(ViewEntrustsellOrder.Columns.SellerName, Comparison.Like, "%" + filterValue + "%");
                }
                else if (filterColumn == "MemberId")
                {
                    query.AddWhere(ViewEntrustsellOrder.Columns.MemberId, Comparison.Like, "%" + filterValue + "%");
                }
                else
                {
                    throw new Exception("找不到查詢的欄位, filterColumn=" + filterColumn);
                }
            }

            if (filterOrderStatus == "verified")
            {
                //核銷
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedStatus, Comparison.Equals, TrustStatus.Verified);
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedSpecialStatus, Comparison.Equals, 0);
                query.AddWhere(ViewEntrustsellOrder.Columns.UndoFlag, Comparison.Equals, 0);
            }
            else if (filterOrderStatus == "undoVerified")
            {
                //反核銷
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedStatus, Comparison.In,
                    new[] { (int)TrustStatus.Initial, (int)TrustStatus.Trusted });
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedSpecialStatus, Comparison.Equals, 0);
                query.AddWhere(ViewEntrustsellOrder.Columns.UndoFlag, Comparison.Equals, 1);
            }
            else if (filterOrderStatus == "refundedForced")
            {
                //強制退貨
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedStatus, Comparison.Equals, TrustStatus.Refunded);
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedSpecialStatus, Comparison.Equals, TrustSpecialStatus.ReturnForced);
                query.AddWhere(ViewEntrustsellOrder.Columns.UndoFlag, Comparison.Equals, 1);
            }

            if (startTime == null && endTime != null)
            {
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedTime, Comparison.LessThan, endTime.Value);
            }
            else if (startTime != null && endTime == null)
            {
                query.AddWhere(ViewEntrustsellOrder.Columns.VerifiedTime, Comparison.GreaterOrEquals, startTime.Value);
            }
            else if (startTime != null & endTime != null)
            {
                query.AddBetweenValues(ViewEntrustsellOrder.Columns.VerifiedTime, startTime.Value, endTime.Value);
            }

            query.ORDER_BY(ViewEntrustsellOrder.Columns.CreateTime, "desc");
            ViewEntrustsellOrderCollection result = new ViewEntrustsellOrderCollection();
            result.LoadAndCloseReader(query.ExecuteReader());
            return result;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public List<ViewEntrustSellReceipt> ViewEntrustSellReceiptExportableList()
        {
            Query query = new Query(ViewEntrustSellReceipt.Schema.TableName);
            query.AddWhere(ViewEntrustSellReceipt.Columns.IsExported, Comparison.Equals, false);
            ViewEntrustSellReceiptCollection result = new ViewEntrustSellReceiptCollection();
            result.LoadAndCloseReader(query.ExecuteReader());
            return result.ToList();
        }

        #endregion 代收轉付

        #region discount_campaign

        public void DiscountCampaignSet(DiscountCampaign campaign)
        {
            DB.Save<DiscountCampaign>(campaign);
        }
        public DiscountCampaign DiscountCampaignGet(int id)
        {
            return DB.Get<DiscountCampaign>(id);
        }

        public DiscountCampaignCollection DiscountCampaignGetByDay()
        {
            DateTime nowTime = DateTime.Now;
            return DB.SelectAllColumnsFrom<DiscountCampaign>().NoLock()
                .Where(DiscountCampaign.Columns.ApplyTime).IsNotNull()
                .And(DiscountCampaign.Columns.CancelTime).IsNull()
                .And(DiscountCampaign.Columns.StartTime).IsLessThanOrEqualTo(nowTime)
                .And(DiscountCampaign.Columns.EndTime).IsGreaterThan(nowTime)
                .ExecuteAsCollection<DiscountCampaignCollection>();
        }

        public DiscountCampaignCollection DiscountCampaignGetByDayForShow()
        {
            string sql = @"
select * from discount_campaign dc
left join discount_event_campaign diec on diec.campaign_id = dc.id
left join discount_event de on de.id = diec.event_id
where getdate() between dc.start_time and dc.end_time
	and dc.apply_time is not null and dc.cancel_time is null
	and (de.qty is null or de.qty >= 1000000)
	and (dc.flag & 16) = 16
	and (dc.flag & 64) = 0
order by dc.id

";
            QueryCommand qc = new QueryCommand(sql, DiscountCampaign.Schema.Provider.Name);
            DiscountCampaignCollection dcCol = new DiscountCampaignCollection();
            dcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return dcCol;
        }

        public DiscountCampaignCollection DiscountCampaignGetByDayForBuy()
        {
            string sql = @"
select * from discount_campaign dc
left join discount_event_campaign diec on diec.campaign_id = dc.id
left join discount_event de on de.id = diec.event_id
where getdate() between dc.start_time and dc.end_time
	and dc.apply_time is not null and dc.cancel_time is null
order by dc.id

";
            QueryCommand qc = new QueryCommand(sql, DiscountCampaign.Schema.Provider.Name);
            DiscountCampaignCollection dcCol = new DiscountCampaignCollection();
            dcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return dcCol;
        }


        public DiscountCampaign DiscountCampaignGetByReference(DiscountCampaignUsedFlags flag, int amount, DateTime endDate)
        {
            string sql = @" select * from " + DiscountCampaign.Schema.TableName + " with(nolock) where "
                                            + DiscountCampaign.Columns.Flag + " & @flag > 0 and "
                                            + DiscountCampaign.Columns.Amount + " = @amount and "
                                            + DiscountCampaign.Columns.EndTime + " = @endDate ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceSerial.Schema.Provider.Name);
            qc.AddParameter("@flag", flag, DbType.Int32);
            qc.AddParameter("@amount", amount, DbType.Int32);
            qc.AddParameter("@endDate", endDate, DbType.DateTime);

            var data = new DiscountCampaign();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public void DiscountCampaignDeleteById(int id)
        {
            DB.Delete<DiscountCampaign>(DiscountCampaign.Columns.Id, id);
        }
        public DiscountCampaignCollection DiscountCampaignGetEventList(DiscountCampaignUsedFlags flag)
        {
            string sql = @" select * from " + DiscountCampaign.Schema.TableName + " with(nolock) where "
                                            + DiscountCampaign.Columns.Flag + "& @flag > 0 and "
                                            + DiscountCampaign.Columns.StartTime + " < @dateTime and "
                                            + DiscountCampaign.Columns.EndTime + " > @dateTime ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceSerial.Schema.Provider.Name);
            qc.AddParameter("@flag", flag, DbType.Int32);
            qc.AddParameter("@dateTime", DateTime.Now, DbType.DateTime);
            DiscountCampaignCollection data = new DiscountCampaignCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public DiscountCampaignCollection DiscountCampaignGetByCreator(string createId)
        {
            return DB.SelectAllColumnsFrom<DiscountCampaign>()
                .Where(DiscountCampaign.Columns.CreateId).IsEqualTo(createId)
                .ExecuteAsCollection<DiscountCampaignCollection>();
        }

        public DiscountCampaignCollection DiscountCampaignCollectionGetByCampaignNameAndEffectiveTime(string campaignName)
        {
            DiscountCampaignCollection collection = new DiscountCampaignCollection();
            DateTime nowTime = DateTime.Now;
            var query = DB.SelectAllColumnsFrom<DiscountCampaign>().NoLock()
                           .Where(DiscountCampaign.Columns.Name).Like(string.Format("%{0}%", campaignName))
                             .And(DiscountCampaign.Columns.ApplyTime).IsNotNull()
                             .And(DiscountCampaign.Columns.CancelTime).IsNull()
                               .And(DiscountCampaign.Columns.StartTime).IsLessThanOrEqualTo(nowTime)
                               .And(DiscountCampaign.Columns.EndTime).IsGreaterThan(nowTime)
                           .ExecuteAsCollection<DiscountCampaignCollection>();
            if (query.Count() > 0)
            {
                collection = query;
            }
            return collection;
        }

        public DiscountCampaignCollection DiscountCampaignGetList(List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<DiscountCampaign>().Where(DiscountCampaign.Columns.Id).In(campaignIds)
                .ExecuteAsCollection<DiscountCampaignCollection>();
        }
        /// <summary>
        /// 取得目前仍有效可計算檔次折扣金額的折價券活動，但排除 (未指定分類＆未指定策展＆未指定單檔)，也排除「開始時間」到「結束時間」設定超過2年的
        /// 但如果有設定 is_discount_price_for_deal 為1，即視前面說的條件，即加入計算
        /// </summary>
        /// <returns></returns>
        public List<int> DiscountCampaignIdGetListOnline()
        {
            string sql = @"
select *,
case when flag_instant =1 then 
(select de.qty from  discount_event de with(nolock) where de.id =
(
	select top 1 event_id from discount_event_campaign dec with(nolock)
	where dec.campaign_id = t.id
))
else 0 end as instant_member_count
 
 from (
select dc.id, dc.name, dc.flag, dc.qty, dc.amount, dc.minimum_amount, 
case when dc.flag & 16 = 16 then 1 else 0 end as flag_instant,
DATEDIFF(day, dc.start_time, dc.end_time) interval_days,dc.is_discount_price_for_deal,
dc.start_time, dc.end_time, 
(
	STUFF( (SELECT ',' + cast( dca.category_id as nvarchar(10))
                             FROM discount_category dca where dca.campaign_id = dc.id
                             FOR XML PATH('')), 
                            1, 1, '')
) category_limit,
(
	STUFF( (SELECT ',' + cast( dbl.brand_id as nvarchar(10))
                             FROM discount_brand_link dbl where dbl.campaign_id = dc.id
                             FOR XML PATH('')), 
                            1, 1, '')
) brand_limit,
(
	STUFF( (SELECT ',' + cast( depl.event_promo_id as nvarchar(10))
                             FROM discount_event_promo_link depl where depl.campaign_id = dc.id
                             FOR XML PATH('')), 
                            1, 1, '')
) promotion_limit
from discount_campaign dc
where getdate() between dc.start_time and dc.end_time
	and dc.apply_time is not null and dc.cancel_time is null
) as t where 
(
t.category_limit is not null
and t.interval_days < 710
and (t.flag & 16) = 16
and (t.flag & 64) = 0
and t.qty >= 1000000
and 
(case when flag_instant =1 then 
(select de.qty from  discount_event de with(nolock) where de.id =
(
	select top 1 event_id from discount_event_campaign dec with(nolock)
	where dec.campaign_id = t.id
))
else 0 end) > 1000000
)

or t.is_discount_price_for_deal = 1

order by t.id
";

            QueryCommand qc = new QueryCommand(sql, DiscountCampaign.Schema.Provider.Name);
            List<int> result = new List<int>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetInt32(0));
                }
            }
            return result;
        }

        #endregion discount_campaign

        #region discount_code

        public void DiscountCodeSet(DiscountCode dc)
        {
            DB.Save<DiscountCode>(dc);
        }

        public DiscountCodeCollection DiscountCodeGetByCampaignIdAndUseTimeIsnotnull(int campaignid)
        {
            return DB.SelectAllColumnsFrom<DiscountCode>()
                .Where(DiscountCode.CampaignIdColumn).IsEqualTo(campaignid)
                .And(DiscountCode.UseTimeColumn).IsNotNull()
                .ExecuteAsCollection<DiscountCodeCollection>();
        }

        public void DiscountCodeDeleteByCampaignId(int campaignid)
        {
            DB.Delete<DiscountCode>(DiscountCode.Columns.CampaignId, campaignid);
        }

        public void DiscountCodeCollectionBulkInsert(DiscountCodeCollection dclist)
        {
            DB.BulkInsert(dclist);
        }

        public bool DiscountCodeCellectionBulkInsertFromDataTable(DataTable table)
        {
            return DB.BulkInsert(table);
        }

        public int DiscountCodeUpdateStatusSet(int campaignId, string code, int useamount, int useid, Guid orderguid, int orderamount, int ordercost, int? codeId = null)
        {
            string sql = @"UPDATE TOP (1) " + DiscountCode.Schema.Provider.DelimitDbName(DiscountCode.Schema.TableName) + @"
                           SET use_time = @usetime
                              ,use_amount=@useamount
                              ,use_id = @useId
                              ,order_guid = @orderguid
                              ,order_amount =@orderamount
                              ,order_cost = @ordercost 
                             WHERE use_time is null AND code =@code 
                             AND campaign_id =@campaignId
                               ";
            if (codeId != null && codeId > 0)
            {
                sql += @" AND id=@codeId ";
            }

            var qc = new QueryCommand(sql, DiscountCode.Schema.Provider.Name);
            qc.AddParameter("@campaignId", campaignId, DbType.Int32);
            qc.AddParameter("@code", code, DbType.AnsiString);
            qc.AddParameter("@usetime", DateTime.Now, DbType.DateTime);
            qc.AddParameter("@useamount", useamount, DbType.Int32);
            qc.AddParameter("@useId", useid, DbType.Int32);
            qc.AddParameter("@orderguid", orderguid, DbType.Guid);
            qc.AddParameter("@orderamount", orderamount, DbType.Int32);
            qc.AddParameter("@ordercost", ordercost, DbType.Int32);
            if (codeId != null && codeId > 0)
            {
                qc.AddParameter("@codeId", codeId, DbType.Int32);
            }

            var a = DB.Execute(qc);
            return a;
        }

        public DiscountCodeCollection DiscountCodeGetListByCampaign(int campaignId)
        {
            return
                DB.SelectAllColumnsFrom<DiscountCode>().Where(DiscountCode.Columns.CampaignId).IsEqualTo(campaignId).
                OrderDesc(DiscountCode.Columns.Id).ExecuteAsCollection<DiscountCodeCollection>();
        }

        public DiscountCodeCollection DiscountCodeGetListWithNonOwner(int campaignId)
        {
            return
                DB.SelectAllColumnsFrom<DiscountCode>().Where(DiscountCode.Columns.CampaignId).IsEqualTo(campaignId).
                And(DiscountCode.Columns.Owner).IsNull().
                OrderDesc(DiscountCode.Columns.Id).ExecuteAsCollection<DiscountCodeCollection>();
        }

        public DiscountCodeCollection DiscountCodeGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = DiscountCode.Columns.UseTime + " desc";
            QueryCommand qc = GetDCWhere(DiscountCode.Schema, filter);
            qc.CommandSql = "select * from " + DiscountCode.Schema.Provider.DelimitDbName(DiscountCode.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            DiscountCodeCollection vfcCol = new DiscountCodeCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public DiscountCodeCollection DiscountCodeGetListByOwner(int campaignId, int owner)
        {
            return DB.SelectAllColumnsFrom<DiscountCode>().NoLock().Where(DiscountCode.Columns.CampaignId).IsEqualTo(campaignId)
                .And(DiscountCode.Columns.Owner).IsEqualTo(owner).ExecuteAsCollection<DiscountCodeCollection>();
        }


        public int DiscountCodeGetCount(params string[] filter)
        {
            QueryCommand qc = GetDCWhere(DiscountCode.Schema, filter);
            qc.CommandSql = "select count(*) from " + DiscountCode.Schema.Provider.DelimitDbName(DiscountCode.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public DiscountCode DiscountCodeGetByCode(string code)
        {
            return DB.Get<DiscountCode>(DiscountCode.Columns.Code, code);
        }

        public DiscountCode DiscountCodeGetById(int id)
        {
            return DB.Get<DiscountCode>(DiscountCode.Columns.Id, id);
        }

        public DiscountCode DiscountCodeGetByCodeWithoutUseTime(string code)
        {
            string sql = @"select top 1 d.* from discount_code d inner join discount_campaign c
                            on d.campaign_id = c.id
                            where code = @code 
                            and GETDATE() between c.start_time and c.end_time 
                            and d.use_time is null
                            and c.apply_time is not null";
            var qc = new QueryCommand(sql, DiscountCode.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.AnsiString);

            var detail = new DiscountCode();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public DiscountCode DiscountCodeGetByOrderGuid(Guid orderGuid)
        {
            return DB.Get<DiscountCode>(DiscountCode.Columns.OrderGuid, orderGuid);
        }

        public bool DiscountCodeClean(Guid orderGuid)
        {
            DiscountCode discountcode = DiscountCodeGetByOrderGuid(orderGuid);
            if (discountcode != null)
            {
                discountcode.MarkOld();
                discountcode.UseTime = null;
                discountcode.UseAmount = null;
                discountcode.UseId = null;
                discountcode.OrderAmount = null;
                discountcode.OrderGuid = null;
                discountcode.OrderCost = null;
                DiscountCodeSet(discountcode);

                return true;
            }
            return false;
        }

        #endregion discount_code

        #region discount_limit

        public DiscountLimit DiscountLimitGetById(int id)
        {
            return DB.Get<DiscountLimit>(id);
        }

        public DiscountLimit DiscountLimitGetByBid(Guid bid)
        {
            return DB.Get<DiscountLimit>(DiscountLimit.Columns.Bid, bid);
        }

        public bool DiscountLimitEnabledGetByBid(Guid mainBid)
        {
            var sql = @"select count(1) cnt from " + DiscountLimit.Schema.Provider.DelimitDbName(DiscountLimit.Schema.TableName) + @" with(nolock)
                    where " + DiscountLimit.Columns.Bid + " = @mainBid and " + DiscountLimit.Columns.Type + " = @type " ;
            var qc = new QueryCommand(sql, DiscountLimit.Schema.Provider.Name);
            qc.AddParameter("@mainBid", mainBid, DbType.Guid);
            qc.AddParameter("@type", (int)DiscountLimitType.Enabled, DbType.Int32);
            bool isBlack = (int)DataService.ExecuteScalar(qc) > 0;

            return isBlack;
        }

        public ViewDiscountLimitCollection DiscountLimitGetAll()
        {
            return DB.SelectAllColumnsFrom<ViewDiscountLimit>().Where(ViewDiscountLimit.Columns.Type).IsEqualTo((int)DiscountLimitType.Enabled)
                .ExecuteAsCollection<ViewDiscountLimitCollection>();
        }

        public void DiscountLimitSet(DiscountLimit dl)
        {
            DB.Save<DiscountLimit>(dl);
        }

        public bool DiscountLimitEnabledGetByBrandId(int brandId)
        {
            var sql = "select count(1) cnt from " + DiscountLimit.Schema.TableName + @" as dl with(nolock)
                    inner join (select " + ViewBrandItem.Columns.MainId + "," + ViewBrandItem.Columns.BusinessHourGuid + " from " + ViewBrandItem.Schema.TableName + " with(nolock) where " + ViewBrandItem.Columns.MainId + @"=@brandId) as vbi on dl.bid = vbi.business_hour_guid
                    where " + DiscountLimit.Columns.Type + " = @type ";
            var qc = new QueryCommand(sql, DiscountLimit.Schema.Provider.Name);
            qc.AddParameter("@brandId", brandId, DbType.Int32);
            qc.AddParameter("@type", (int)DiscountLimitType.Enabled, DbType.Int32);
            bool isBlack = (int)DataService.ExecuteScalar(qc) > 0;

            return isBlack;
        }
        #endregion

        #region discount_referrer

        public void DiscountReferrerSet(DiscountReferrer dr)
        {
            DB.Save<DiscountReferrer>(dr);
        }

        public DiscountReferrerCollection DiscountReferrerGetList(DiscountReferrerStatus status)
        {
            return DB.SelectAllColumnsFrom<DiscountReferrer>().Where(DiscountReferrer.Columns.Status).IsEqualTo((int)status).
                OrderDesc(DiscountReferrer.Columns.Id).ExecuteAsCollection<DiscountReferrerCollection>();
        }

        #endregion

        #region discount_referrer_url

        public DiscountReferrerUrl DiscountReferrerUrlGet(string origUrl)
        {
            var sql = @"select top 1 * from " + DiscountReferrerUrl.Schema.Provider.DelimitDbName(DiscountReferrerUrl.Schema.TableName) + @" with(nolock)
                    where " + DiscountReferrerUrl.Columns.OrigUrl + " = @origUrl ";
            var qc = new QueryCommand(sql, DiscountReferrerUrl.Schema.Provider.Name);
            qc.AddParameter("@origUrl", origUrl, DbType.AnsiString);
            DiscountReferrerUrl dru = new DiscountReferrerUrl();
            dru.LoadAndCloseReader(DataService.GetReader(qc));

            return dru;
        }

        public void DiscountReferrerUrlSet(DiscountReferrerUrl dru)
        {
            DB.Save<DiscountReferrerUrl>(dru);
        }

        public DiscountReferrerUrl DiscountReferrerUrlGet(int userId, int eventActivityId)
        {
            var data = DB.SelectAllColumnsFrom<DiscountReferrerUrl>()
                .Where(DiscountReferrerUrl.Columns.UserId).IsEqualTo(userId)
                .And(DiscountReferrerUrl.Columns.EventActivityId).IsEqualTo(eventActivityId)
                .ExecuteAsCollection<DiscountReferrerUrlCollection>();

            return data.Any() ? data.FirstOrDefault() : new DiscountReferrerUrl();
        }

        #endregion

        #region discount_event_campaign

        public void EventCampaignDeleteList(int eventId)
        {
            string sql = @" DELETE " + DiscountEventCampaign.Schema.TableName + @"
                            WHERE " + DiscountEventCampaign.Columns.EventId + @" = @eventId";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@eventId", eventId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public DiscountEventCampaignCollection DiscountEventCampaignGetByEventId(int eventId)
        {
            return DB.SelectAllColumnsFrom<DiscountEventCampaign>()
                .Where(DiscountEventCampaign.Columns.EventId).IsEqualTo(eventId)
                .ExecuteAsCollection<DiscountEventCampaignCollection>();
        }

        #endregion

        #region discount_event

        public DiscountEvent DiscountEventGet(int id)
        {
            return DB.SelectAllColumnsFrom<DiscountEvent>()
                .Where(DiscountEvent.Columns.Id).IsEqualTo(id).ExecuteSingle<DiscountEvent>();
        }

        public void DiscountEventSet(DiscountEvent discountEvent)
        {
            DB.Save<DiscountEvent>(discountEvent);
        }

        public DiscountEventCollection DiscountEventGetByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<DiscountEvent, DiscountEventCollection>(page, pageSize, orderBy, false, filter);
        }

        public int DiscountEventGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<DiscountEvent>(filter);
            qc.CommandSql = "select count(1) from " + DiscountEvent.Schema.Provider.DelimitDbName(DiscountEvent.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public DiscountEvent DiscountEventGet(string column, object value)
        {
            return DB.Get<DiscountEvent>(column, value);
        }

        #endregion

        #region discount_user

        public void DiscountUserSet(DiscountUser discountUser)
        {
            DB.Save<DiscountUser>(discountUser);
        }

        public void DiscountUserCollectionBulkInsert(DiscountUserCollection usersList)
        {
            DB.BulkInsert(usersList);
        }

        public void DiscountUserDeleteList(int eventId)
        {
            string sql = @" DELETE " + DiscountUser.Schema.TableName + @"
                            WHERE " + DiscountUser.Columns.EventId + @" = @eventId";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@eventId", eventId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public int DiscountUserSetAllMembers(int eventId, string username)
        {
            string sql = @" INSERT into " + DiscountUser.Schema.TableName + @"
                            SELECT " + Member.Columns.UniqueId + @", @eventId, 1, @username, @now, NULL, NULL
                            from " + Member.Schema.TableName + @" with(NOLOCK); 
                            SELECT @@ROWCOUNT";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@eventId", eventId, DbType.Int32);
            qc.AddParameter("@username", username, DbType.String);
            qc.AddParameter("@now", DateTime.Now, DbType.DateTime);
            qc.CommandTimeout = 600;
            object obj = DataService.ExecuteScalar(qc);
            if (obj == DBNull.Value)
            {
                return 0;
            }
            return (int)obj;
        }

        public void DiscountUserDeleteOld(int eventId)
        {
            string sql = @" delete " + DiscountUser.Schema.TableName + @"
                            where " + DiscountUser.Columns.EventId + @" = @event_id
                            and " + DiscountUser.Columns.Status + @" = " + (int)DiscountUserStatus.Initial + @"
                            and " + DiscountUser.Columns.UserId + @" not in (SELECT " + DiscountUser.Columns.UserId + @" FROM " + DiscountUser.Schema.TableName + @" WITH(NOLOCK) WHERE " + DiscountUser.Columns.EventId + @" = @event_id and " + DiscountUser.Columns.Status + @" = " + (int)DiscountUserStatus.None + @")";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@event_id", eventId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void DiscountUserDeleteDuplicate(int eventId)
        {
            string sql = @" delete " + DiscountUser.Schema.TableName + @"
                            where " + DiscountUser.Columns.EventId + @" = @event_id
                            and " + DiscountUser.Columns.Status + @" = " + (int)DiscountUserStatus.None + @"
                            and " + DiscountUser.Columns.UserId + @" in (SELECT " + DiscountUser.Columns.UserId + @" FROM " + DiscountUser.Schema.TableName + @" WITH(NOLOCK) WHERE " + DiscountUser.Columns.EventId + @" = @event_id and " + DiscountUser.Columns.Status + @" <> " + (int)DiscountUserStatus.None + @")";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@event_id", eventId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void DiscountUserUpdateStatus(int eventId)
        {
            string sql = @" UPDATE " + DiscountUser.Schema.TableName + @"
                            SET " + DiscountUser.Columns.Status + @" = " + (int)DiscountUserStatus.Initial + @"
                            where " + DiscountUser.Columns.EventId + @" = @event_id
                            and " + DiscountUser.Columns.Status + @" = " + (int)DiscountUserStatus.None;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@event_id", eventId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public DiscountUserCollection DiscountUserGetList(int userId, int eventId)
        {
            return DB.SelectAllColumnsFrom<DiscountUser>()
                .Where(DiscountUser.Columns.EventId).IsEqualTo(eventId)
                .And(DiscountUser.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<DiscountUserCollection>();
        }

        public int DiscountUserGetCount(int eventId, DiscountUserStatus status = DiscountUserStatus.None)
        {
            string sql = "select count(1) from " + DiscountUser.Schema.Provider.DelimitDbName(DiscountUser.Schema.TableName) + " with(nolock) where " + DiscountUser.Columns.EventId + @" = @event_id ";
            if (status != DiscountUserStatus.None)
            {
                sql += " and " + DiscountUser.Columns.Status + "=" + (int)status;
            }
            QueryCommand qc = new QueryCommand(sql, DiscountUser.Schema.Provider.Name);
            qc.AddParameter("@event_id", eventId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public void DiscountUserUpdateStatus(int eventId, int userId, DiscountUserStatus status, string username)
        {
            DB.Update<DiscountUser>()
                .Set(DiscountUser.StatusColumn).EqualTo((int)status)
                .Set(DiscountUser.ModifyIdColumn).EqualTo(username)
                .Set(DiscountUser.ModifyTimeColumn).EqualTo(DateTime.Now)
                .Where(DiscountUser.UserIdColumn).IsEqualTo(userId)
                .And(DiscountUser.EventIdColumn).IsEqualTo(eventId)
                .Execute();
        }

        #endregion

        #region discount_event_campaign

        public bool DiscountEventCampaignDelete(int id)
        {
            DB.Destroy<DiscountEventCampaign>(DiscountEventCampaign.Columns.Id, id);
            return true;
        }

        public void DiscountEventCampaignSet(DiscountEventCampaign discountEventCampaign)
        {
            DB.Save<DiscountEventCampaign>(discountEventCampaign);
        }

        #endregion

        #region view_discount_event_campaign

        public ViewDiscountEventCampaignCollection ViewDiscountEventCampaignGetList(int eventId)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountEventCampaign>()
                .Where(ViewDiscountEventCampaign.Columns.EventId).IsEqualTo(eventId)
                .ExecuteAsCollection<ViewDiscountEventCampaignCollection>();
        }

        public ViewDiscountEventCampaignCollection ViewDiscountEventCampaignGetAddList(DateTime eventStart, DateTime eventEnd)
        {
            string sql = @"select * from " + ViewDiscountEventCampaign.Schema.TableName + @" with(nolock)
                    where " + ViewDiscountEventCampaign.Columns.EventId + @" is null
                    and " + ViewDiscountEventCampaign.Columns.StartTime + @" > @now
                    and " + ViewDiscountEventCampaign.Columns.StartTime + @" <= @dateS
                    and " + ViewDiscountEventCampaign.Columns.EndTime + @" >= @dateE
                    and " + ViewDiscountEventCampaign.Columns.CancelTime + @" is null
                    and flag & " + (int)DiscountCampaignUsedFlags.InstantGenerate + @" > 0 
                    and flag & " + (int)DiscountCampaignUsedFlags.ReferenceAndPay + @" = 0 
                    and flag & " + (int)DiscountCampaignUsedFlags.SingleSerialKey + @" = 0
                    and flag & " + (int)DiscountCampaignUsedFlags.FirstBuy + @" = 0
                    and " + ViewDiscountEventCampaign.Columns.ApplyTime + @" is not null ";

            QueryCommand qc = new QueryCommand(sql, ViewDiscountEventCampaign.Schema.Provider.Name);
            qc.AddParameter("now", DateTime.Now, DbType.DateTime);
            qc.AddParameter("dateS", eventStart, DbType.DateTime);
            qc.AddParameter("dateE", eventEnd, DbType.DateTime);

            ViewDiscountEventCampaignCollection data = new ViewDiscountEventCampaignCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region view_discount_user

        public ViewDiscountUserCollection ViewDiscountUserGetList(int userId, DateTime date)
        {
            string sql = @" select * from " + ViewDiscountUser.Schema.TableName + @" with(nolock)
                            where " + ViewDiscountUser.Columns.UserId + @" = @userId
                            and " + ViewDiscountUser.Columns.DiscountUserStatus + @" in (@initial, @shortage)
                            and " + ViewDiscountUser.Columns.Status + @" = @status
                            and " + ViewDiscountUser.Columns.EventStartTime + @" <= @date
                            and " + ViewDiscountUser.Columns.EventEndTime + @" > @date";

            QueryCommand qc = new QueryCommand(sql, ViewDiscountUser.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            qc.AddParameter("initial", (int)DiscountUserStatus.Initial, DbType.Int32);
            qc.AddParameter("shortage", (int)DiscountUserStatus.Shortage, DbType.Int32);
            qc.AddParameter("status", (int)DiscountEventStatus.Initial, DbType.Int32);
            qc.AddParameter("date", date, DbType.DateTime);

            ViewDiscountUserCollection data = new ViewDiscountUserCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region discount_store

        public DiscountStoreCollection DiscountStoreGetList(List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<DiscountStore>().Where(DiscountStore.Columns.CampaignId).In(campaignIds)
                .ExecuteAsCollection<DiscountStoreCollection>();
        }

        public DiscountStoreCollection DiscountStoreGetList(Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<DiscountStore>().Where(DiscountStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<DiscountStoreCollection>();
        }

        public void DiscountStoreCloneByTemplate(int templateId, int campaignId)
        {
            const string sql = @"insert into discount_store(campaign_id,store_guid) 
                            select @campaignId, store_guid from discount_template_store where template_id=@templateId";

            var qc = new QueryCommand(sql, DiscountStore.Schema.Provider.Name);
            qc.AddParameter("@campaignId", campaignId, DbType.Int32);
            qc.AddParameter("@templateId", templateId, DbType.Int32);

            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region discount_event_promo_link

        public void DiscountEventPromoLinkSet(DiscountEventPromoLink depl)
        {
            DB.Save<DiscountEventPromoLink>(depl);
        }

        public DiscountEventPromoLinkCollection DiscountEventPromoLinkGetList(int campaign_id)
        {
            return DB.SelectAllColumnsFrom<DiscountEventPromoLink>().NoLock().Where(DiscountEventPromoLink.Columns.CampaignId).IsEqualTo(campaign_id)
                .ExecuteAsCollection<DiscountEventPromoLinkCollection>();
        }

        public DiscountEventPromoLinkCollection DiscountEventPromoLinkGetListByCampaignIdList(List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<DiscountEventPromoLink>().NoLock().Where(DiscountEventPromoLink.Columns.CampaignId).In(campaignIds)
                .ExecuteAsCollection<DiscountEventPromoLinkCollection>();
        }

        public DiscountEventPromoLinkCollection DiscountEventIdGetList(int event_promo_id)
        {
            return DB.SelectAllColumnsFrom<DiscountEventPromoLink>().NoLock().Where(DiscountEventPromoLink.Columns.EventPromoId).IsEqualTo(event_promo_id)
                .ExecuteAsCollection<DiscountEventPromoLinkCollection>();
        }

        #endregion

        #region discount_brand_link
        public void DiscountBrandLinkSet(DiscountBrandLink dbl)
        {
            DB.Save<DiscountBrandLink>(dbl);
        }

        public DiscountBrandLinkCollection DiscountBrandLinkGetList(int campaign_id)
        {
            return DB.SelectAllColumnsFrom<DiscountBrandLink>().NoLock().Where(DiscountBrandLink.Columns.CampaignId).IsEqualTo(campaign_id)
                .ExecuteAsCollection<DiscountBrandLinkCollection>();
        }

        public DiscountBrandLinkCollection DiscountBrandLinkGetListByCampaignIdList(List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<DiscountBrandLink>().NoLock().Where(DiscountBrandLink.Columns.CampaignId).In(campaignIds)
                .ExecuteAsCollection<DiscountBrandLinkCollection>();
        }
        #endregion

        #region discount_category

        public DiscountCategoryCollection DiscountCategoryGetList(int campaignId)
        {
            return DB.SelectAllColumnsFrom<DiscountCategory>().NoLock().Where(DiscountCategory.Columns.CampaignId).IsEqualTo(campaignId)
                .ExecuteAsCollection<DiscountCategoryCollection>();
        }

        public DiscountCategoryCollection DiscountCategoryGetListByCampaignIds(List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<DiscountCategory>().NoLock().Where(DiscountCategory.Columns.CampaignId).In(campaignIds)
                .ExecuteAsCollection<DiscountCategoryCollection>();
        }

        public void DiscountCategorySet(DiscountCategory DiscountCategory)
        {
            DB.Save<DiscountCategory>(DiscountCategory);
        }

        #endregion

        #region discount_use_type

        public void DiscountUseTypeSet(DiscountUseType DiscountUseType)
        {
            DB.Save<DiscountUseType>(DiscountUseType);
        }
        public DiscountUseType DiscountUseTypeGet(Guid Bid)
        {
            return DB.Get<DiscountUseType>(Bid);
        }

        #endregion

        #region view_discount_main

        public ViewDiscountMainCollection ViewDiscountMainGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewDiscountMain.Columns.CreateTime + " desc";
            QueryCommand qc = GetDCWhere(ViewDiscountMain.Schema, filter);
            qc.CommandSql = "select * from " + ViewDiscountMain.Schema.Provider.DelimitDbName(ViewDiscountMain.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewDiscountMainCollection vfcCol = new ViewDiscountMainCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public int ViewDiscountMainGetCount(params string[] filter)
        {
            QueryCommand qc = GetDCWhere(ViewDiscountMain.Schema, filter);
            qc.CommandSql = "select count(*) from " + ViewDiscountMain.Schema.Provider.DelimitDbName(ViewDiscountMain.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetDCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        #endregion view_discount_main

        #region ViewDiscountDetail

        public ViewDiscountDetail ViewDiscountDetailCheckSingleSerialDuplicate(string customCode, DateTime startDate, DateTime endDate)
        {
            var sql = string.Format(@"select top 1 * from {0} with(nolock)
               where {1} = @code and (@startDate < {2} AND @endDate > {3})"
                       , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                       , ViewDiscountDetail.Columns.Code
                       , ViewDiscountDetail.Columns.EndTime
                       , ViewDiscountDetail.Columns.StartTime);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@code", customCode, DbType.String);
            qc.AddParameter("@startDate", startDate, DbType.DateTime);
            qc.AddParameter("@endDate", endDate, DbType.DateTime);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public ViewDiscountDetail ViewDiscountDetailByCustomCodeOnTime(string code)
        {
            var sql = string.Format(@"select top 1 * from {0}
                        where {1}=@code and getdate() between {2} and {3} and {4} is null and {5} is not null"
                                    , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                                    , ViewDiscountDetail.Columns.Code
                                    , ViewDiscountDetail.Columns.StartTime
                                    , ViewDiscountDetail.Columns.EndTime
                                    , ViewDiscountDetail.Columns.UseTime
                                    , ViewDiscountDetail.Columns.ApplyTime);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.String);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));

            if (!detail.IsLoaded)
            {
                sql = string.Format(@"select top 1 * from {0}
                        where {1}=@code and getdate() between {2} and {3} and {4} is not null"
                                        , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                                        , ViewDiscountDetail.Columns.Code
                                        , ViewDiscountDetail.Columns.StartTime
                                        , ViewDiscountDetail.Columns.EndTime
                                        , ViewDiscountDetail.Columns.ApplyTime);

                qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
                qc.AddParameter("@code", code, DbType.String);
                detail.LoadAndCloseReader(DataService.GetReader(qc));
            }

            return detail;
        }

        public ViewDiscountDetail ViewDiscountDetailByCustomCodeNextTime(string code)
        {
            var sql = string.Format(@"select top 1 * from {0}
                        where {1}=@code and getdate() > {2} and {3} is null and {4} is not null"
                        , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                        , ViewDiscountDetail.Columns.Code
                        , ViewDiscountDetail.Columns.EndTime
                        , ViewDiscountDetail.Columns.UseTime
                        , ViewDiscountDetail.Columns.ApplyTime);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.String);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public ViewDiscountDetailCollection ViewDiscountDetailByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountDetail>().NoLock().Where(ViewDiscountDetail.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewDiscountDetailCollection>();
        }

        public ViewDiscountDetail ViewDiscountDetailByCode(string code)
        {
            var sql = string.Format(@"select top 1 * from {0} with (nolock) where {1} = @code Order By {2} desc"
                             , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                             , ViewDiscountDetail.Columns.Code
                             , ViewDiscountDetail.Columns.StartTime);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.AnsiString);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public ViewDiscountDetail ViewDiscountDetailByCode(string code, int campaignId)
        {
            var sql = string.Format(@"select top 1 * from {0} with (nolock) where {1} = @code and {2} = @campaignId Order By {3} desc"
                             , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                             , ViewDiscountDetail.Columns.Code
                             , ViewDiscountDetail.Columns.Id
                             , ViewDiscountDetail.Columns.StartTime);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.AnsiString);
            qc.AddParameter("@campaignId", campaignId, DbType.Int32);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public ViewDiscountDetail ViewDiscountDetailByDiscountCodeId(int discountCodeId)
        {
            var sql = string.Format(@"select top 1 * from {0} with (nolock) where {1}=@discountCodeId"
                , ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName)
                , ViewDiscountDetail.Columns.CodeId);

            var qc = new QueryCommand(sql, ViewDiscountDetail.Schema.Provider.Name);
            qc.AddParameter("@discountCodeId", discountCodeId, DbType.Int32);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public ViewDiscountDetail ViewDiscountDetailByCodeWithoutUseTime(string code)
        {
            string sql = "select top 1 * from " + ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName) +
                         @" with (nolock) where " + ViewDiscountDetail.Columns.Code + " = @code and " + ViewDiscountDetail.Columns.UseTime + " is null";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.AnsiString);

            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(DataService.GetReader(qc));
            return detail;
        }

        public int ViewDiscountDetailCountByCampiagnIdUserId(int campiagn_id, int userId)
        {
            string sql = "select count(*) from " + ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName) +
                         @" with (nolock) where " + ViewDiscountDetail.Columns.Id + " = @id and " + ViewDiscountDetail.Columns.UseTime + " is not null and " +
                         ViewDiscountDetail.Columns.UseId + "=@userid";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@id", campiagn_id, DbType.Int32);
            qc.AddParameter("@userid", userId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewDiscountDetailCollection ViewDiscountDetailCollectionGetByOwner(int pageStart, int pageLength, string orderBy
            , int owner, DiscountCampaignType type = DiscountCampaignType.PponDiscountTicket)
        {
            string[] filters =
            {
                ViewDiscountDetail.Columns.Owner + "=" + owner,
                ViewDiscountDetail.Columns.StartTime + "<" + DateTime.Now,
                ViewDiscountDetail.Columns.Type + "=" + (int)type
            };
            return ViewDiscountDetailGetList(pageStart, pageLength, orderBy, filters);
        }

        public ViewDiscountDetailCollection ViewDiscountDetailGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewDiscountDetail.Columns.EndTime + " desc";
            QueryCommand qc = GetDCWhere(ViewDiscountDetail.Schema, filter);
            qc.CommandSql = "select * from " + ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewDiscountDetailCollection vscCol = new ViewDiscountDetailCollection();
            vscCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vscCol;
        }

        public int ViewDiscountDetailGetListCount(params string[] filter)
        {
            QueryCommand qc = GetDCWhere(ViewDiscountDetail.Schema, filter);
            qc.CommandSql = "select count(1) from " + ViewDiscountDetail.Schema.Provider.DelimitDbName(ViewDiscountDetail.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewDiscountDetailCollection ViewDiscountDetailGetList(int userId,
            DiscountCampaignType type, List<int> campaignIds)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountDetail>().Where(ViewDiscountDetail.Columns.Owner).IsEqualTo(userId)
                .And(ViewDiscountDetail.Columns.Type).IsEqualTo((int)type)
                .And(ViewDiscountDetail.Columns.Id).In(campaignIds)
                .ExecuteAsCollection<ViewDiscountDetailCollection>();
        }
        /// <summary>
        /// 取得會員還有哪些折價券可使用
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public ViewDiscountDetailCollection ViewDiscountDetailGetUnUsedList(int userId, DiscountCampaignType type)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountDetail>().Where(ViewDiscountDetail.Columns.Owner).IsEqualTo(userId)
                .And(ViewDiscountDetail.Columns.Type).IsEqualTo((int)type)
                .And(ViewDiscountDetail.Columns.UseTime).IsNull()
                .And(ViewDiscountDetail.Columns.CancelTime).IsNull()
                .And(ViewDiscountDetail.Columns.EndTime).IsGreaterThan(DateTime.Now)
                .OrderAsc(ViewDiscountDetail.Columns.EndTime)
                .ExecuteAsCollection<ViewDiscountDetailCollection>();
        }

        public ViewDiscountDetail ViewDiscountDetailGetByCodeId(int codeId)
        {
            var detail = new ViewDiscountDetail();
            detail.LoadAndCloseReader(
                DB.SelectAllColumnsFrom<ViewDiscountDetail>().Where(ViewDiscountDetail.Columns.CodeId)
                    .IsEqualTo(codeId).ExecuteReader());
            return detail;
        }

        public ViewDiscountDetailCollection ViewDiscountDetailGetListByCampaignId(int campaignId)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountDetail>().Where(ViewDiscountDetail.Columns.Id).IsEqualTo(campaignId)
                .ExecuteAsCollection<ViewDiscountDetailCollection>();
        }
        #endregion ViewDiscountDetail

        #region ViewDiscountOrder

        public ViewDiscountOrderCollection ViewDiscountOrderGetList(string column, object value, int discountCampaignType = (int)DiscountCampaignType.PponDiscountTicket)
        {
            return DB.SelectAllColumnsFrom<ViewDiscountOrder>().Where(column).IsEqualTo(value).And(ViewDiscountOrder.Columns.DiscountType).IsEqualTo(discountCampaignType)
                .ExecuteAsCollection<ViewDiscountOrderCollection>();
        }

        #endregion ViewDiscountOrder

        #region CT_ATM

        public CtAtmCollection CtAtmGetList(DateTime d)
        {
            string sql = "select * from " + CtAtm.Schema.TableName + " with(nolock) where " + CtAtm.Columns.CreateTime + " >@date";
            QueryCommand qc = new QueryCommand(sql, CtAtm.Schema.Provider.Name);
            qc.AddParameter("@date", d, DbType.DateTime);
            CtAtmCollection data = new CtAtmCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CtAtm CtAtmGetByOrderId(string orderId)
        {
            return DB.Get<CtAtm>(CtAtm.OrderIdColumn.ColumnName, orderId);
        }

        public CtAtm CtAtmGet(Guid orderGuid)
        {
            return DB.Get<CtAtm>(CtAtm.Columns.OrderGuid, orderGuid);
        }

        public CtAtm CtAtmGetByVirtualAccount(string acc)
        {
            return DB.Get<CtAtm>(CtAtm.VirtualAccountColumn.ColumnName, acc);
        }

        public int CtAtmGetMaxSi()
        {
            return DB.Select(Aggregate.Max(CtAtm.SiColumn.ColumnName)).From(CtAtm.Schema.TableName).ExecuteScalar<int>();
        }

        public int CtAtmGetTodayCount(DateTime date)
        {
            string sql = "select count(1) from " + CtAtm.Schema.TableName + " with(nolock) where create_time > " + date.ToShortDateString() + " and create_time < " + date.AddDays(1).ToShortDateString();
            QueryCommand qc = new QueryCommand(sql, CtAtm.Schema.Provider.Name);
            return (int)DataService.ExecuteScalar(qc);
        }

        public int CtAtmGetDealCount(Guid bid)
        {
            string sql = "select count(1) from " + CtAtm.Schema.TableName + " with(nolock) where business_guid = @bid and status <2";
            QueryCommand qc = new QueryCommand(sql, CtAtm.Schema.Provider.Name);
            qc.Parameters.Add("@bid", bid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public CtAtm CtAtmGetBySeqno(string seqno, int amount)
        {
            return DB.Select().From(CtAtm.Schema.TableName).Where(CtAtm.Columns.Senq).IsEqualTo(seqno).And(CtAtm.Columns.Amount).IsEqualTo(amount).ExecuteSingle<CtAtm>();
        }

        public int CtAtmGetDealAtmMax(Guid bid)
        {
            string sql = "select business_hour_atm_maximum from " + BusinessHour.Schema.TableName + " with(nolock) where [guid] = @bid";
            QueryCommand qc = new QueryCommand(sql, CtAtm.Schema.Provider.Name);
            qc.Parameters.Add("@bid", bid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public bool CtAtmSet(CtAtm ctatm)
        {
            if (!ctatm.IsDirty)
                return false;
            DB.Save<CtAtm>(ctatm);
            return true;
        }

        public void CtAtmUpdateStatus(int status, Guid oid)
        {
            string sql = "update " + CtAtm.Schema.TableName + " set " + CtAtm.Columns.Status + "=@status where " + CtAtm.Columns.OrderGuid + " = @oid";
            QueryCommand qc = new QueryCommand(sql, CtAtmRefund.Schema.Provider.Name);
            qc.AddParameter("@status", status, DbType.Int32);
            qc.AddParameter("@oid", oid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public CtAtmCompared CtAtmComparedGetByVirtualAccount(string account)
        {
            return
                DB.SelectAllColumnsFrom<CtAtmCompared>().Where(CtAtmCompared.Columns.VirtualAccount).IsEqualTo(account)
                .ExecuteSingle<CtAtmCompared>();
        }

        public CtAtmComparedCollection CtAtmComparedGetAtmTime(string atmTime)
        {
            return
                DB.SelectAllColumnsFrom<CtAtmCompared>().Where(CtAtmCompared.Columns.AtmTime).IsEqualTo(atmTime)
                .ExecuteAsCollection<CtAtmComparedCollection>();
        }

        public void CtAtmComparedSet(CtAtmCompared ct)
        {
            DB.Save<CtAtmCompared>(ct);
        }

        public string CtAtmMessageInformGetMaxSeqno()
        {
            string sql = "select top 1 " + CtAtmMessageInform.Columns.SEQNo + " from " + CtAtmMessageInform.Schema.TableName + " with(nolock) order by " + CtAtmMessageInform.Columns.SEQNo + " desc";
            QueryCommand qc = new QueryCommand(sql, CtAtmMessageInform.Schema.Provider.Name);
            return DataService.ExecuteScalar(qc).ToString();
        }

        public bool CtAtmLogSet(CtAtmMessageInform atmlog)
        {
            if (!atmlog.IsDirty)
                return false;
            DB.Save<CtAtmMessageInform>(atmlog);
            return true;
        }

        public bool CtAtmAp2ApLogCheck(string seqno)
        {
            string sql = "select count(1) from " + CtAtmMessageInform.Schema.TableName + " with(nolock) where SEQNo = '" + seqno + "' and ResponseStats='200'";
            QueryCommand qc = new QueryCommand(sql, CtAtm.Schema.Provider.Name);
            if ((int)DataService.ExecuteScalar(qc) == 0)
                return true;
            else
                return false;
        }

        public void CtAtmAchMarkRefundItem()
        {
            string sql = "update " + CtAtmRefund.Schema.TableName + " set [status]=1,refund_process_time=getdate() where [status] = 0 and amount>0";
            QueryCommand qc = new QueryCommand(sql, CtAtmRefund.Schema.Provider.Name);
            DataService.ExecuteScalar(qc);
        }

        public ViewCtAtmRefundCollection ViewCtAtmRefundGetList()
        {
            return
                DB.SelectAllColumnsFrom<ViewCtAtmRefund>().Where(ViewCtAtmRefund.Columns.Status).IsEqualTo(
                    AtmRefundStatus.AchProcess).ExecuteAsCollection<ViewCtAtmRefundCollection>();
        }

        public CtAtmRefundCollection CtAtmRefundGetList()
        {
            return DB.SelectAllColumnsFrom<CtAtmRefund>().Where(CtAtmRefund.Columns.Status).IsEqualTo(
                AtmRefundStatus.AchProcess).ExecuteAsCollection<CtAtmRefundCollection>();
        }

        public CtAtmRefundCollection CtAtmGetRefundList(DateTime d)
        {
            return
                DB.SelectAllColumnsFrom<CtAtmRefund>().Where(CtAtmRefund.Columns.CreateTime).IsGreaterThan(d).ExecuteAsCollection<CtAtmRefundCollection>();
        }

        public CtAtmRefundCollection CtAtmRefundGetList(string orderBy, params string[] filter)
        {
            string defOrderBy = CtAtmRefund.Columns.Id;
            QueryCommand qc = GetCtAtmRefundQC(filter);
            qc.CommandSql = "select * from " + CtAtmRefund.Schema.TableName + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
            }

            CtAtmRefundCollection view = new CtAtmRefundCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public CtAtmRefund CtAtmRefundGetLatest(Guid oid)
        {
            string sql = "select  top 1 * from " + CtAtmRefund.Schema.TableName + " with(nolock) where "
                + CtAtmRefund.Columns.OrderGuid + " =@orderguid order by " + CtAtmRefund.Columns.Si + " desc";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@orderguid", oid, DbType.Guid);
            CtAtmRefund data = new CtAtmRefund();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        private QueryCommand GetCtAtmRefundQC(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<CtAtmRefund>(filter);

            if (qc.Parameters.Count > 0)
            {
                int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) ";
            }
            return qc;
        }

        public CtAtmP1logCollection CtAtmGetP1LogList()
        {
            return
                DB.SelectAllColumnsFrom<CtAtmP1log>().OrderDesc(CtAtmP1log.Columns.CreateTime).ExecuteAsCollection
                    <CtAtmP1logCollection>();
        }

        public ViewCtAtmRefundCollection ViewCtAtmRefundGetListIn(int[] si)
        {
            return
                DB.SelectAllColumnsFrom<ViewCtAtmRefund>().Where(CtAtmRefund.Columns.Si).In(si).ExecuteAsCollection<ViewCtAtmRefundCollection>();
        }

        public CtAtmRefundCollection CtAtmGetRefundFailListIn(string[] si)
        {
            return
                DB.SelectAllColumnsFrom<CtAtmRefund>().Where(CtAtmRefund.Columns.Status).IsEqualTo(AtmRefundStatus.RefundFail).And(CtAtmRefund.Columns.FailReason).In(si).ExecuteAsCollection<CtAtmRefundCollection>();
        }

        public bool CtAtmAchP1LogSet(CtAtmP1log p1Log)
        {
            if (!p1Log.IsDirty)
                return false;
            DB.Save<CtAtmP1log>(p1Log);
            return true;
        }

        public bool CtAtmAchR1LogSet(CtAtmR1log r1Log)
        {
            if (!r1Log.IsDirty)
                return false;
            DB.Save<CtAtmR1log>(r1Log);
            return true;
        }

        public CtAtmRefund CtAtmRefundGetByOrderGuid(Guid orderGuid)
        {
            return DB.Get<CtAtmRefund>(CtAtmRefund.OrderGuidColumn.ColumnName, orderGuid);
        }

        public bool CtAtmRefundSet(CtAtmRefund refund)
        {
            if (!refund.IsDirty)
                return false;
            DB.Save<CtAtmRefund>(refund);
            return true;
        }

        public CtAtmRefund CtAtmRefundGetForAchReturn(string si, string bankCode, string userAccount, int amount, DateTime date)
        {
            return DB.Select().From(CtAtmRefund.Schema.TableName)
                .Where(CtAtmRefund.Columns.Si).IsEqualTo(si)
                .And(CtAtmRefund.Columns.BankCode).IsEqualTo(bankCode)
                .And(CtAtmRefund.Columns.AccountNumber).IsEqualTo(userAccount)
                .And(CtAtmRefund.Columns.Amount).IsEqualTo(amount)
                .And(CtAtmRefund.Columns.TargetDate).IsEqualTo(date).ExecuteSingle<CtAtmRefund>();
        }

        public CtAtmCollection CtAtmGetPast(DateTime date)
        {
            return DB.SelectAllColumnsFrom<CtAtm>()
                .Where(CtAtm.Columns.Status).IsEqualTo((int)AtmStatus.Initial)
                .And(CtAtm.Columns.CreateTime).IsLessThan(date).ExecuteAsCollection<CtAtmCollection>();
        }

        #endregion CT_ATM

        #region Bank Info

        public BankInfo BankInfoGetById(int id)
        {
            return DB.Get<BankInfo>(BankInfo.IdColumn.ColumnName, id);
        }

        public BankInfo BankInfoGet(string bankNo)
        {
            return DB.Get<BankInfo>(BankInfo.BankNoColumn.ColumnName, bankNo);
        }

        public BankInfo BankInfoGetByBranch(string bankNo, string branchNo)
        {
            return DB.SelectAllColumnsFrom<BankInfo>().Where(BankInfo.Columns.BankNo).IsEqualTo(bankNo)
                .And(BankInfo.Columns.BranchNo).IsEqualTo(branchNo).ExecuteSingle<BankInfo>();
        }

        public void BankInfoSet(BankInfo bankInfo)
        {
            DB.Save<BankInfo>(bankInfo);
        }

        public BankInfoCollection BankInfoGetMainList()
        {
            return DB.SelectAllColumnsFrom<BankInfo>().Where(BankInfo.BranchNameColumn.ColumnName).IsNull().OrderAsc(BankInfo.BankNoColumn.ColumnName).ExecuteAsCollection<BankInfoCollection>();
        }

        /// <summary>
        /// 依銀行代碼取得銀行相關資訊
        /// </summary>
        /// <param name="bankNo">銀行代碼</param>
        /// <returns>銀行相關資訊</returns>
        public BankInfo BankInfoGetMainByBankNo(string bankNo)
        {
            return DB.SelectAllColumnsFrom<BankInfo>()
                .Where(BankInfo.BranchNameColumn.ColumnName).IsNull()
                .And(BankInfo.BankNoColumn.ColumnName).IsEqualTo(bankNo)
                .OrderAsc(BankInfo.BankNoColumn.ColumnName).ExecuteAsCollection<BankInfoCollection>().FirstOrDefault();
        }

        public BankInfoCollection BankInfoGetList()
        {
            return DB.SelectAllColumnsFrom<BankInfo>().ExecuteAsCollection<BankInfoCollection>();
        }
        public BankInfoCollection BankInfoGetBranchList(string bankNo)
        {
            return DB.SelectAllColumnsFrom<BankInfo>().Where(BankInfo.BankNoColumn.ColumnName).IsEqualTo(bankNo)
                .And(BankInfo.BranchNameColumn.ColumnName).IsNotNull().OrderAsc(BankInfo.BranchNoColumn.ColumnName).ExecuteAsCollection<BankInfoCollection>();
        }

        public bool BankInfoCollectionSet(BankInfoCollection bankInfoList)
        {
            DB.SaveAll<BankInfo, BankInfoCollection>(bankInfoList);
            return true;
        }

        #endregion Bank Info

        #region weeklypay

        public WeeklyPayAccount GetWeeklyPayAccount(Guid bid, Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<WeeklyPayAccount>()
                .Where(WeeklyPayAccount.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(WeeklyPayAccount.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteSingle<WeeklyPayAccount>();
        }

        public WeeklyPayAccountCollection WeeklyPayAccountGetList(Guid bid)
        {
            return DB.SelectAllColumnsFrom<WeeklyPayAccount>()
                .Where(WeeklyPayAccount.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<WeeklyPayAccountCollection>();
        }

        public void SetWeeklyPayAccount(WeeklyPayAccount account)
        {
            DB.Save<WeeklyPayAccount>(account);
        }

        public void WeeklyPayAccountSetList(WeeklyPayAccountCollection accounts)
        {
            DB.SaveAll(accounts);
        }

        public WeeklyPayAccountCollection WeeklyPayAccountGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = (string.IsNullOrEmpty(orderBy)) ? WeeklyPayAccount.Columns.CreateDate : orderBy;
            QueryCommand qc = SSHelper.GetWhereQC<WeeklyPayAccount>(filter);
            qc.CommandSql = "select * from " + WeeklyPayAccount.Schema.TableName + " with(nolock) " + qc.CommandSql;
            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            WeeklyPayAccountCollection view = new WeeklyPayAccountCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public int SetWeeklyPayReport(WeeklyPayReport report)
        {
            return DB.Save<WeeklyPayReport>(report);
        }

        public WeeklyPayReportCollection WeeklyPayReportGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = (string.IsNullOrEmpty(orderBy)) ? WeeklyPayReport.Columns.Id : orderBy;
            QueryCommand qc = SSHelper.GetWhereQC<WeeklyPayReport>(filter);
            qc.CommandSql = "select * from " + WeeklyPayReport.Schema.TableName + " with(nolock) " + qc.CommandSql;
            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            WeeklyPayReportCollection view = new WeeklyPayReportCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ViewWeeklyPayReportCollection GetViewWeeklyPayReportByDate(DateTime d_start, DateTime d_end)
        {
            string sql = "select * from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.IntervalStart + " >=@dstart  and "
                   + ViewWeeklyPayReport.Columns.IntervalStart + "<@dend and " + ViewWeeklyPayReport.Columns.BusinessHourGuid + "=" + ViewWeeklyPayReport.Columns.ReportGuid;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@dstart", d_start, DbType.DateTime);
            qc.AddParameter("@dend", d_end, DbType.DateTime);
            ViewWeeklyPayReportCollection data = new ViewWeeklyPayReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewWeeklyPayReportCollection GetViewWeeklyPayReportByBid(Guid bid)
        {
            string sql = "select * from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.BusinessHourGuid + " =@bid ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            ViewWeeklyPayReportCollection data = new ViewWeeklyPayReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewWeeklyPayReportCollection GetViewWeeklyPayReportById(int id)
        {
            string sql = "select * from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.Id + " =@id ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            ViewWeeklyPayReportCollection data = new ViewWeeklyPayReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewWeeklyPayReportCollection GetViewWeeklyPayReportByRid(Guid rid)
        {
            string sql = "select * from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.ReportGuid + " =@rid and " +
                ViewWeeklyPayReport.Columns.ReportGuid + "<>" + ViewWeeklyPayReport.Columns.BusinessHourGuid + " order by " + ViewWeeklyPayReport.Columns.Id;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@rid", rid, DbType.Guid);
            ViewWeeklyPayReportCollection data = new ViewWeeklyPayReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewBalanceSheetBillListCollection GetViewBalanceSheetByGuId(string companyId)
        {
            string sql = "select * from " + ViewBalanceSheetBillList.Schema.TableName + " with(nolock) " +
                 " where 1 = 1 " +
                 " and " + ViewBalanceSheetBillList.Columns.CompanyID + "  = @companyId " +
                 " and " + ViewBalanceSheetBillList.Columns.IsConfirmedReadyToPay + "= 1" +
                 " and (" + ViewBalanceSheetBillList.Columns.IsTransferComplete + " IS null or " + ViewBalanceSheetBillList.Columns.IsTransferComplete + " =0) " +
                 " and generation_frequency in ( 2 , 3 , 4 ) " +
                 " order by " + ViewBalanceSheetBillList.Columns.Id;
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@companyId", companyId, DbType.String);
            ViewBalanceSheetBillListCollection data = new ViewBalanceSheetBillListCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewWeeklyPayReportCollection GetViewWeeklyPayReportByUniqueId(string uid)
        {
            string sql = "select * from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.UniqueId + " =@uid ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@uid", uid, DbType.String);
            ViewWeeklyPayReportCollection data = new ViewWeeklyPayReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int GetViewWeeklyPayReportSumByBidRid(Guid bid, Guid rid)
        {
            string strSQL = "select sum( coalesce( " + ViewWeeklyPayReport.Columns.TransferAmount + ","
                + ViewWeeklyPayReport.Columns.TotalSum + ")) from " + ViewWeeklyPayReport.Schema.TableName + " with(nolock) where " + ViewWeeklyPayReport.Columns.ReportGuid + " = '" + rid + "' and " +
                ViewWeeklyPayReport.Columns.BusinessHourGuid + " = '" + bid + "' and " + ViewWeeklyPayReport.Columns.ReportGuid + "<>" + ViewWeeklyPayReport.Columns.BusinessHourGuid +
                " group by " + ViewWeeklyPayReport.Columns.BusinessHourGuid;
            int odc = new InlineQuery(ViewWeeklyPayReport.Schema.Provider).ExecuteScalar<int>(strSQL);

            return odc;
        }

        public void UpdateBusinessHourStatusForWeeklyPay(bool isweeklypay, Guid bid)
        {
            string sql = "update " + BusinessHour.Schema.TableName + " set " + BusinessHour.Columns.BusinessHourStatus + "=case when " +
                          BusinessHour.Columns.BusinessHourStatus + "&@status" + ">0 then " + BusinessHour.Columns.BusinessHourStatus + (isweeklypay ? "|" : "^") +
                          "@status else " + BusinessHour.Columns.BusinessHourStatus + (isweeklypay ? "|@status" : string.Empty) + " end where " + BusinessHour.Columns.Guid + "=@bid ";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@status", (int)BusinessHourStatus.WeeklyPay, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public BusinessHour GetHourStatusBybid(Guid bid)
        {
            return DB.Get<BusinessHour>(BusinessHour.Columns.Guid, bid);
        }

        /// <summary>
        /// 取得WeeklyPayRport單一物件
        /// </summary>
        /// <param name="id">WeeklyPayReport的ID</param>
        /// <returns>WeeklyPayReport物件</returns>
        public WeeklyPayReport GetWeeklyPayReportById(int id)
        {
            return DB.Get<WeeklyPayReport>(WeeklyPayReport.Columns.Id, id);
        }
        public int WeeklyPayReportCollectionSet(WeeklyPayReportCollection wprCol)
        {
            return DB.SaveAll(wprCol);
        }

        public DataTable WeeklyPayMonitorJob(int deliveryType)  //for job
        {
            string sql = string.Format(@"
IF OBJECT_ID('tempdb..#BusinessGroup') is not null DROP TABLE #BusinessGroup
select p.unique_id, p.business_hour_guid, g.acc_business_group_id, g.acc_business_group_name, p.delivery_type
into #BusinessGroup from deal_property p with(nolock)
join acc_business_group g with(nolock) on p.deal_acc_business_group_id=g.acc_business_group_id

select b.[GUID], d.paytocompany, w.account_no,w.bank_no, w.branch_no,w.account_id,w.event_name, s.seller_name, d.sales_id,
    CONVERT(varchar(10),b.business_hour_order_time_s,120) as stime,
    CONVERT(varchar(10),b.business_hour_order_time_e,120) as etime,
    case bg.acc_business_group_id when 1 then city.city_name else N'全區' end as district
from business_hour b with(nolock)
join group_order g with(nolock) on g.business_hour_guid = b.guid
left join weekly_pay_account w with(nolock)
    on b.[guid] = w.business_hour_guid and w.store_guid = b.seller_GUID
left join deal_accounting d with(nolock) on b.[guid] = d.business_hour_guid
left join seller s with(nolock) on b.seller_GUID = s.GUID
left join city with(nolock) on city.id = d.acc_city
left join #BusinessGroup bg
    on b.[guid] = bg.business_hour_guid
JOIN dbo.item it ON it.business_hour_guid = b.GUID
where b.business_hour_order_time_s > Convert(date,dateadd(dd,-6,GETDATE()),120)
and b.business_hour_order_time_s < Convert(date,dateadd(dd,1,GETDATE()),120)
and it.item_price > 0
and bg.delivery_type = {0}
{1}
order by stime
", deliveryType
 , deliveryType == (int)DeliveryType.ToShop
    ? " and (b.business_hour_status & 512 > 0 or business_hour_status & 196608 = 131072) "
    : string.Empty);

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion weeklypay

        #region WeeklyPayReportWms
        public int SetWeeklyPayReportWms(WeeklyPayReportWm report)
        {
            return DB.Save<WeeklyPayReportWm>(report);
        }
        #endregion

        #region ViewPponCloseDownExpirationChange

        public ViewPponCloseDownExpirationChange ViewPponCloseDownExpirationChangeGet(Guid orderGuid)
        {
            ViewPponCloseDownExpirationChangeCollection changeCollection = DB.SelectAllColumnsFrom<ViewPponCloseDownExpirationChange>()
                    .WhereExpression(ViewPponCloseDownExpirationChange.Columns.OrderGuid).IsEqualTo(orderGuid).ExecuteAsCollection<ViewPponCloseDownExpirationChangeCollection>();
            //shitty subsonic .ExecuteSingle<ViewPponCloseDownExpirationChange>() returns incomplete data.
            if (changeCollection != null)
            {
                return changeCollection.FirstOrDefault(x => x.OrderGuid == orderGuid);
            }
            else
            {
                return null;
            }
        }

        public ViewPponCloseDownExpirationChange ViewPponCloseDownExpirationChangeGetByBid(Guid bid, Guid storeGuid)
        {
            ViewPponCloseDownExpirationChangeCollection changeCollection = DB.SelectAllColumnsFrom<ViewPponCloseDownExpirationChange>()
                    .WhereExpression(ViewPponCloseDownExpirationChange.Columns.Bid).IsEqualTo(bid)
                    .And(ViewPponCloseDownExpirationChange.Columns.StoreGuid).IsEqualTo(storeGuid).ExecuteAsCollection<ViewPponCloseDownExpirationChangeCollection>();
            //shitty subsonic .ExecuteSingle<ViewPponCloseDownExpirationChange>() returns incomplete data.
            if (changeCollection != null)
            {
                return changeCollection.FirstOrDefault(x => x.Bid == bid && x.StoreGuid == storeGuid);
            }
            else
            {
                return null;
            }
        }

        #endregion ViewPponCloseDownExpirationChange

        #region CreditcardOrder

        public CreditcardOrder CreditcardOrderGet(int id)
        {
            return DB.Get<CreditcardOrder>(id);
        }

        public CreditcardOrder CreditcardOrderGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<CreditcardOrder>().Where(CreditcardOrder.OrderGuidColumn).IsEqualTo(orderGuid).ExecuteSingle<CreditcardOrder>();
        }

        public bool SetCreditcardOrder(CreditcardOrder creditcardOrder)
        {
            if (!creditcardOrder.IsDirty)
            {
                return false;
            }

            DB.Save<CreditcardOrder>(creditcardOrder);
            return true;
        }

        public bool IsCreditcardOrderOverAmount
            (string strCreditcardInfo, int iTheAmountOfThisTime, DateTime dtTransactionTime, int iTime = 1, int iAmountLimit = 5000)
        {
            string creditcardInfo = GetEncryptedCreditcardInfo(strCreditcardInfo);

            int amount = DB.Select(Aggregate.Sum(CreditcardOrder.Columns.CreditcardAmount))
                .From(CreditcardOrder.Schema)
                .Where(CreditcardOrder.Columns.CreditcardInfo).IsEqualTo(creditcardInfo)
                .And(CreditcardOrder.Columns.CreateTime).IsBetweenAnd(dtTransactionTime.AddHours(-iTime), dtTransactionTime)
                .ExecuteScalar<int>();

            return (amount + iTheAmountOfThisTime) >= iAmountLimit ? true : false;
        }

        public CreditcardOrderCollection GetCreditcardOrderByInfo(string strCreditcardInfo)
        {
            string creditcardInfo = GetEncryptedCreditcardInfo(strCreditcardInfo);

            CreditcardOrderCollection list = DB.SelectAllColumnsFrom<CreditcardOrder>()
                .WhereExpression(CreditcardOrder.Columns.CreditcardInfo).IsEqualTo(creditcardInfo)
                .OrderDesc(CreditcardOrder.Columns.CreateTime)
                .Paged(1, 5)
                .ExecuteAsCollection<CreditcardOrderCollection>();
            return (list.Count > 0) ? list : null;
        }

        public CreditcardOrderCollection GetCreditcardOrderByOrderGuid(Guid guid)
        {
            CreditcardOrderCollection list = DB.SelectAllColumnsFrom<CreditcardOrder>()
                .WhereExpression(CreditcardOrder.Columns.OrderGuid).IsEqualTo(guid)
                .OrderDesc(CreditcardOrder.Columns.CreateTime)
                .Paged(1, 5)
                .ExecuteAsCollection<CreditcardOrderCollection>();
            return (list.Count > 0) ? list : null;
        }
        public CreditcardOrderCollection GetCreditcardOrderList(Dictionary<string, string> dicCondition = null)
        {
            string query = "";
            if (dicCondition.Any(x => x.Key == "allorders" && x.Value == "True"))
            {
                query = " WHERE order_guid != '00000000-0000-0000-0000-000000000000'  "; //所有訂單也包含交易正常
            }
            else
            {
                query = " WHERE order_guid != '00000000-0000-0000-0000-000000000000' AND result > 0 "; //0是交易正常
            }

            if (dicCondition != null && dicCondition.Count > 0)
            {
                string condition = GetQueryToSearchCreditcardOrderByWebservice(dicCondition);
                query += string.IsNullOrEmpty(condition) ? "" : " AND " + condition;
            }
            string sql = @"SELECT * FROM creditcard_order with(nolock)" + query;

            sql += " ORDER BY create_time DESC ";
            CreditcardOrderCollection list = new InlineQuery().ExecuteAsCollection<CreditcardOrderCollection>(sql);
            return list;
        }

        /// <summary>
        /// 將WebService送來條件，兜出CreditcardOrder之查詢語法
        /// </summary>
        /// <param name="dicCondition">查詢條件，以下是個key值說明<br />
        /// type: 主要查詢條件<br />
        /// </param>
        /// <returns></returns>
        private string GetQueryToSearchCreditcardOrderByWebservice(Dictionary<string, string> dicCondition)
        {
            string query = string.Empty;
            foreach (KeyValuePair<string, string> item in dicCondition)
            {
                switch (item.Key.ToLower())
                {
                    #region 主要查詢項目

                    case "creditcard":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        query += " creditcard_info = '" + item.Value + "' ";
                        break;

                    case "orderguid":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        query += " order_guid = '" + item.Value + "' ";
                        break;

                    case "memberid":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        query += " member_id = '" + item.Value + "' ";
                        break;

                    case "membername":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        query += " member_id in ('" + item.Value + ")' ";
                        break;

                    case "info":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        query += " creditcard_info = '" + item.Value + "' ";
                        break;

                    #endregion 主要查詢項目

                    #region time:時間查詢

                    case "time":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        if (dicCondition.ContainsKey("time2"))
                        {
                            query += " create_time BETWEEN '" + dicCondition["time"] + "' AND '" + dicCondition["time2"] + "'";
                        }
                        else
                        {
                            query += " create_time = '" + dicCondition["time"] + "'";
                        }
                        break;

                    #endregion time:時間查詢

                    #region result:狀態

                    case "result":
                        query += string.IsNullOrEmpty(query) ? "" : " AND ";
                        if (item.Value == "0")
                        {
                            query += " result > 0 ";
                        }
                        else
                        {
                            query += " result = " + item.Value;
                        }
                        break;

                    #endregion result:狀態

                    #region reference:是否照會銀行

                    case "reference":
                        if (bool.Parse(item.Value))
                        {
                            query += string.IsNullOrEmpty(query) ? "" : " AND ";
                            query += " is_reference = 1 ";
                        }
                        break;

                    #endregion reference:是否照會銀行

                    #region locked:是否為黑名單

                    case "locked":
                        if (bool.Parse(item.Value))
                        {
                            query += string.IsNullOrEmpty(query) ? "" : " AND ";
                            query += " is_locked = 1 ";
                        }
                        break;

                        #endregion locked:是否為黑名單
                }
            }
            return query;
        }

        public void UpdateCreditcardOrderByIdWithRefer(int id, string referenceId, bool isReference)
        {
            Update product = new Update(CreditcardOrder.Schema.TableName);

            product.From(CreditcardOrder.Schema.TableName);
            product.Where(CreditcardOrder.Columns.Id).IsEqualTo(id);
            product.Set(CreditcardOrder.Columns.IsReference).EqualTo(isReference);
            product.Set(CreditcardOrder.Columns.ReferenceId).EqualTo(referenceId);
            product.Set(CreditcardOrder.Columns.ReferenceTime).EqualTo(string.Format("{0:yyyy/MM/dd HH:mm}", DateTime.Now));
            product.Execute();
        }

        public CreditcardOrderCollection GetCreditcardOrderBlackList()
        {
            string sql = @"SELECT * FROM creditcard_order with(nolock) where is_locked = 1";
            CreditcardOrderCollection list = new InlineQuery()
                .ExecuteAsCollection<CreditcardOrderCollection>(sql);
            return list;
        }


        public void UpdateCreditcardOrderByInfoWithLocked(string strCreditcardInfo, string lockedId, bool isLock)
        {
            string now = string.Format("{0:yyyy/MM/dd HH:mm}", DateTime.Now);

            string creditcardInfo = GetEncryptedCreditcardInfo(strCreditcardInfo);

            Update product = new Update(CreditcardOrder.Schema.TableName);

            product.From(CreditcardOrder.Schema.TableName);
            product.Where(CreditcardOrder.Columns.CreditcardInfo).IsEqualTo(creditcardInfo);
            if (isLock)
            {
                product.Set(CreditcardOrder.Columns.IsLocked).EqualTo(true);
                product.Set(CreditcardOrder.Columns.LockerId).EqualTo(lockedId);
                product.Set(CreditcardOrder.Columns.LockedTime).EqualTo(now);
            }
            else
            {
                product.Set(CreditcardOrder.Columns.IsLocked).EqualTo(false);
                product.Set(CreditcardOrder.Columns.UnlockerId).EqualTo(lockedId);
                product.Set(CreditcardOrder.Columns.UnlockedTime).EqualTo(now);
            }

            product.Execute();
        }

        public void SetUserUnlockCreditcardToLock(int userId, string locker = "auto")
        {
            DateTime now = DateTime.Now;
            string sql = @"
                select * from creditcard_order co with(nolock) where co.member_id = @userId
";
            QueryCommand qc = new QueryCommand(sql, LunchKingSite.DataOrm.CreditcardOrder.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            CreditcardOrderCollection coCol = new CreditcardOrderCollection();
            coCol.LoadAndCloseReader(DataService.GetReader(qc));
            foreach(var co in coCol)
            {
                if (co.IsLocked)
                {
                    continue;
                }
                co.IsLocked = true;
                co.LockerId = "auto";
                co.LockedTime = now;
                SetCreditcardOrder(co);
            }
        }

        /// <summary>
        /// 取得一段時間會員信用卡刷失敗的信用卡數目
        /// 檢查是否疑盜刷
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public int GetUserCreditcardFailCardCount(int userId, DateTime startTime, DateTime endTime)
        {
            string sql = @"
                select count(distinct(creditcard_info)) from creditcard_order co with(nolock)
                where
                 co.member_id = @userId
                 and co.result <> 0
                and co.create_time >= @startTime and co.create_time < @endTime
";
            QueryCommand qc = new QueryCommand(sql, CreditcardOrder.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 取得一段時間會員使用盜刷卡的數目
        /// 檢查是否疑盜刷
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public int GetUserCreditcardStolenCardCount(int userId, DateTime startTime, DateTime endTime)
        {
            string sql = @"
                select count(distinct(creditcard_info)) from creditcard_order co with(nolock)
                where
                 co.member_id = @userId
                 and (co.result = 41 or co.result = 43 or co.result = 10000004 or co.is_locked = 1)
                and co.create_time >= @startTime and co.create_time < @endTime
";
            QueryCommand qc = new QueryCommand(sql, CreditcardOrder.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }
        /// <summary>
        /// 取得信用卡號編碼
        /// </summary>
        /// <param name="strCreditcardInfo">信用卡號</param>
        /// <returns></returns>
        private string GetEncryptedCreditcardInfo(string strCreditcardInfo)
        {
            Security sec = new Security(SymmetricCryptoServiceProvider.AES);
            return sec.Encrypt(strCreditcardInfo);
        }

        #endregion CreditcardOrder

        #region CreditcardBankInfo

        //目前是應用在判斷是否為國外卡
        public CreditcardBankInfoCollection CreditcardBankInfoGetList()
        {
            var list = DB.Select().From(CreditcardBankInfo.Schema.TableName)
                .Where(CreditcardBankInfo.Columns.Flag).IsEqualTo(0)          //目前0為預設值，還未有其他作用
                .ExecuteAsCollection<CreditcardBankInfoCollection>();
            return list;
        }

        /// <summary>
        /// 取得允許分期的信用卡號碼前6碼
        /// </summary>
        /// <returns></returns>
        public List<string> CreditcardBankInstallmentBinsGetList()
        {
            string sql = @"SELECT cbi.bin
  FROM [creditcard_bank_info] cbi with(nolock)
  inner join creditcard_bank bank with(nolock) on bank.id = cbi.bank_id
  where bank.is_installment = 1";
            QueryCommand qc = new QueryCommand(sql, CreditcardBank.Schema.Provider.Name);
            List<string> result = new List<string>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
            }
            return result;
        }

        /// <summary>
        /// 取得銀行信用卡號碼前6碼
        /// </summary>
        /// <returns></returns>
        public List<string> CreditcardBankBinsGetList(int bankId)
        {
            string sql = @"SELECT bin FROM [creditcard_bank_info] with(nolock) where bank_id = @bankId";

            QueryCommand qc = new QueryCommand(sql, CreditcardBankInfo.Schema.Provider.Name);
            qc.AddParameter("@bankId", bankId, DbType.Int32);

            List<string> result = new List<string>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
            }
            return result;
        }

        /// <summary>
        /// 從刷卡前6碼，取得刷卡銀行，無值回傳null
        /// </summary>
        /// <param name="bin"></param>
        /// <returns></returns>
        public CreditcardBankInfo CreditcardBankInfoGetByBin(string bin)
        {
            return DB.QueryOver<CreditcardBankInfo>().NoLock().FirstOrDefault(t => t.Bin == bin);
        }

        public void CreditcardBankInfoSet(CreditcardBankInfo bankInfo)
        {
            DB.Save<CreditcardBankInfo>(bankInfo);
        }

        /// <summary>
        /// 取得銀行客服電話
        /// </summary>
        /// <param name="bin"></param>
        /// <returns></returns>
        public string CreditcardBankGetById(int id)
        {
            var data = DB.QueryOver<CreditcardBank>().NoLock().FirstOrDefault(t => t.Id == id);
            if (data != null)
            {
                return data.ServicePhone;
            }
            else
            {
                return "";
            }

        }

        /// <summary>
        /// 取得銀行能分期的所有BankId
        public List<int> CreditcardBankByInstallment()
        {
            List<int> bankList = new List<int>();

            var data =DB.SelectAllColumnsFrom<CreditcardBank>().Where(CreditcardBank.Columns.IsInstallment).IsEqualTo(1).ExecuteAsCollection<CreditcardBankCollection>();

            return bankList=data.Select(p=>p.Id).ToList();

        }

        /// <summary>
        /// 取得銀行List
        /// </summary>
        /// 
        /// <returns></returns>
        public CreditcardBankCollection CreditcardBankGetList()
        {
            var list = DB.Select().From(CreditcardBank.Schema.TableName)
                .ExecuteAsCollection<CreditcardBankCollection>();
            return list;
        }

        public void CreditcardBankSet(CreditcardBank bank)
        {
            DB.Save<CreditcardBank>(bank);
        }

        #endregion CreditcardBankInfo

        #region MohistOrderInfo

        public bool MohistVerifyInfoSet(MohistVerifyInfo mohist)
        {
            DB.Save<MohistVerifyInfo>(mohist);
            return true;
        }

        public MohistVerifyInfo MohistVerifyInfoGet(string couponId, string SequenceNumber)
        {
            return DB.SelectAllColumnsFrom<MohistVerifyInfo>().Where(MohistVerifyInfo.Columns.CouponId).IsEqualTo(couponId).And(MohistVerifyInfo.Columns.SequenceNumber).IsEqualTo(SequenceNumber).ExecuteSingle<MohistVerifyInfo>();
        }

        public MohistVerifyInfo MohistVerifyInfoGet(Guid bid, string trustSequenceNumber)
        {
            int uid = pp.ViewPponDealGetByBusinessHourGuid(bid).UniqueId ?? 0;
            return DB.SelectAllColumnsFrom<MohistVerifyInfo>().Where(MohistVerifyInfo.Columns.MohistProductId).IsEqualTo(uid).And(MohistVerifyInfo.Columns.SequenceNumber).IsEqualTo(trustSequenceNumber).ExecuteSingle<MohistVerifyInfo>();
        }

        public MohistVerifyInfo MohistVerifyInfoGet(string mohistDealsId, string sequenceNumber, string couponCode)
        {
            return DB.SelectAllColumnsFrom<MohistVerifyInfo>().Where(MohistVerifyInfo.Columns.MohistDealsId).IsEqualTo(mohistDealsId)
                .And(MohistVerifyInfo.Columns.SequenceNumber).IsEqualTo(sequenceNumber)
                .And(MohistVerifyInfo.Columns.CouponCode).IsEqualTo(couponCode).ExecuteSingle<MohistVerifyInfo>();
        }

        public MohistVerifyInfoCollection MohistVerifyInfoGetByOrderDatePeriod(DateTime dateStart, DateTime dateEnd)
        {
            return DB.SelectAllColumnsFrom<MohistVerifyInfo>().Where(MohistVerifyInfo.Columns.OrderDate).IsBetweenAnd(dateStart, dateEnd).ExecuteAsCollection<MohistVerifyInfoCollection>();
        }

        public MohistVerifyInfoCollection MohistVerifyInfoGetByPeriod(DateTime dateStart, DateTime dateEnd)
        {
            return DB.SelectAllColumnsFrom<MohistVerifyInfo>().Where(MohistVerifyInfo.Columns.ModifyTime).IsBetweenAnd(dateStart, dateEnd).ExecuteAsCollection<MohistVerifyInfoCollection>();
        }

        public MohistVerifyInfoCollection MohistVerifyInfoGetByRedoFlag(int flag)
        {
            string sql = "select * from " + MohistVerifyInfo.Schema.TableName + " with(nolock) where " +
                MohistVerifyInfo.Columns.RedoFlag + " & @flag > 0";
            QueryCommand qc = new QueryCommand(sql, MohistVerifyInfo.Schema.Provider.Name);
            qc.AddParameter("@flag", flag, DbType.Int32);
            MohistVerifyInfoCollection data = new MohistVerifyInfoCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        #endregion MohistOrderInfo

        #region IChannelInfo

        public bool IChannelInfoSet(IchannelInfo iChannel)
        {
            DB.Save<IchannelInfo>(iChannel);
            return true;
        }

        public IchannelInfo IchannelInfoGetById(int id)
        {
            return DB.Get<IchannelInfo>(IchannelInfo.Columns.Id, id);
        }

        #endregion

        #region ShipCompany

        public ShipCompanyCollection ShipCompanyGetList(int? shipCompanyId)
        {
            if (shipCompanyId.HasValue)
                return DB.SelectAllColumnsFrom<ShipCompany>()
                    .Where(ShipCompany.Columns.Id).IsEqualTo(shipCompanyId)
                    .ExecuteAsCollection<ShipCompanyCollection>()
                    .OrderByAsc(ShipCompany.Columns.Sequence);
            else
                return DB.SelectAllColumnsFrom<ShipCompany>()
                    .ExecuteAsCollection<ShipCompanyCollection>()
                    .OrderByAsc(ShipCompany.Columns.Sequence);
        }

        public ShipCompanyCollection ShipCompanyGetList(string searchItem, string searchValue, bool showLkSeller,
            int pageStart, int pageLength, out int totalCount)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ShipCompany>();
            if (searchItem == ShipCompany.Columns.Id)
            {
                query.Where(ShipCompany.Columns.Id).IsEqualTo(searchValue);
            }
            else if (searchItem == ShipCompany.Columns.Sequence)
            {
                query.Where(ShipCompany.Columns.Sequence).IsEqualTo(searchValue);
            }
            else if (searchItem == ShipCompany.Columns.ShipCompanyName)
            {
                query.Where(ShipCompany.Columns.ShipCompanyName).Like("%" + searchValue + "%");
            }
            else if (searchItem == ShipCompany.Columns.ServiceTel)
            {
                query.Where(ShipCompany.Columns.ServiceTel).IsEqualTo(searchValue);
            }
            else if (searchItem == ShipCompany.Columns.Status)
            {
                query.Where(ShipCompany.Columns.Status).IsEqualTo(searchValue);
            }

            totalCount = query.GetRecordCount();
            query.OrderDesc(ShipCompany.Columns.Status);
            query.OrderBys.Add(ShipCompany.Columns.Sequence);
            query.OrderBys.Add(ShipCompany.Columns.CreateTime);
            query.Paged(pageStart, pageLength, ShipCompany.Columns.Id);
            return query.ExecuteAsCollection<ShipCompanyCollection>();

            //return DB.SelectAllColumnsFrom<ShipCompany>().ExecuteAsCollection<ShipCompanyCollection>();
        }



        public ShipCompany ShipCompanyGetByName(string comapnyName)
        {
            return DB.SelectAllColumnsFrom<ShipCompany>()
                .Where(ShipCompany.Columns.ShipCompanyName).Like("%" + comapnyName + "%")
                .ExecuteSingle<ShipCompany>();
        }


        public ShipCompany ShipCompanyGet(int Id)
        {
            return DB.Get<ShipCompany>(Id);
        }

        public bool ShipCompanySet(ShipCompany item)
        {
            var data = DB.Get<ShipCompany>(item.Id);

            data.Sequence = item.Sequence;
            data.ShipCompanyName = item.ShipCompanyName;
            data.ShipWebsite = item.ShipWebsite;
            data.ServiceTel = item.ServiceTel;
            data.Status = item.Status;

            if (data.IsLoaded)
            {
                DB.Update<ShipCompany>(data);
            }
            else
            {
                data.Creator = item.Creator;
                data.CreateTime = DateTime.Now;
                DB.Save<ShipCompany>(data);
            }


            return true;
        }

        #endregion ShipCompany

        #region OrderShip

        public OrderShip OrderShipGet(Guid orderGuid)
        {
            var osc = DB.SelectAllColumnsFrom<OrderShip>().NoLock().Where(OrderShip.Columns.OrderGuid)
                .IsEqualTo(orderGuid)
                .ExecuteAsCollection<OrderShipCollection>();

            return osc.OrderByDesc(OrderShip.Columns.Id).FirstOrDefault();
        }

        public OrderShip GetOrderShipById(int Id)
        {
            return DB.SelectAllColumnsFrom<OrderShip>().Where(OrderShip.Columns.Id).IsEqualTo(Id).ExecuteSingle<OrderShip>();
        }

        public OrderShipCollection OrderShipGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<OrderShip>()
                    .Where(OrderShip.Columns.OrderGuid).IsEqualTo(orderGuid)
                    .ExecuteAsCollection<OrderShipCollection>();
        }

        public OrderShipCollection OrderShipGetListByShipNo(string shipNo)
        {
            return DB.SelectAllColumnsFrom<OrderShip>().NoLock()
                    .Where(OrderShip.Columns.ShipNo).IsEqualTo(shipNo)
                    .ExecuteAsCollection<OrderShipCollection>();
        }

        public int OrderShipSet(OrderShip os)
        {
            return DB.Save<OrderShip>(os);
        }

        public bool OrderShipDelete(int Id)
        {
            bool result = true;
            try
            {
                DB.Update<OrderShip>().Set(OrderShip.ShipCompanyIdColumn).EqualTo(null).Set(OrderShip.ShipNoColumn).EqualTo(null)
                    .Set(OrderShip.ShipTimeColumn).EqualTo(null).Set(OrderShip.ShipMemoColumn).EqualTo(null).Where(OrderShip.IdColumn).IsEqualTo(Id).Execute();
            }
            catch (Exception ex)
            {
                log.Error("Update OrderShip Error", ex);
                result = false;
            }

            return result;
        }

        public bool OrderShipUpdate(OrderShip os)
        {
            bool result = true;
            try
            {
                DB.Update<OrderShip>()
                    .Set(OrderShip.ShipCompanyIdColumn).EqualTo(os.ShipCompanyId)
                    .Set(OrderShip.ShipNoColumn).EqualTo(os.ShipNo)
                    .Set(OrderShip.ShipTimeColumn).EqualTo(os.ShipTime)
                    .Set(OrderShip.ModifyIdColumn).EqualTo(os.ModifyId)
                    .Set(OrderShip.ModifyTimeColumn).EqualTo(os.ModifyTime)
                    .Set(OrderShip.ShipMemoColumn).EqualTo(os.ShipMemo)
                .Where(OrderShip.IdColumn).IsEqualTo(os.Id)
                .Execute();
            }
            catch (Exception ex)
            {
                log.Error("Update OrderShip Error", ex);
                result = false;
            }

            return result;
        }

        #endregion OrderShip

        #region OrderShipLog

        public bool OrderShipLogSet(OrderShipLog osl)
        {
            DB.Save<OrderShipLog>(osl);
            return true;
        }

        #endregion OrderShipLog

        #region ViewOrderShipList

        public ViewOrderShipListCollection ViewOrderShipListGetListByProductGuid(Guid productGuid, OrderClassification orderClassification, OrderShipType type = OrderShipType.Normal)
        {
            return DB.SelectAllColumnsFrom<ViewOrderShipList>()
                .Where(ViewOrderShipList.Columns.ProductGuid).IsEqualTo(productGuid)
                .And(ViewOrderShipList.Columns.OrderClassification).IsEqualTo((int)orderClassification)
                .AndExpression(ViewOrderShipList.Columns.Type).IsNull()
                .Or(ViewOrderShipList.Columns.Type).IsEqualTo((int)type)
                .CloseExpression()
                .ExecuteAsCollection<ViewOrderShipListCollection>() ?? new ViewOrderShipListCollection();
        }

        public int ViewOrderShipListGetCountByProductGuid(Guid productGuid, OrderClassification orderClassification)
        {
            return new Select(Aggregate.Count(ViewOrderShipList.Columns.OrderId)).From(ViewOrderShipList.Schema)
                .Where(ViewOrderShipList.Columns.ProductGuid).IsEqualTo(productGuid).And(ViewOrderShipList.Columns.ShipTime).IsNull()
                .And(ViewOrderShipList.Columns.OrderClassification).IsEqualTo((int)orderClassification)
                .AndExpression(ViewOrderShipList.Columns.Type).IsNull()
                .Or(ViewOrderShipList.Columns.Type).IsEqualTo((int)OrderShipType.Normal)
                .CloseExpression()
                .ExecuteScalar<int>();
        }

        public ViewOrderShipListCollection ViewOrderShipListGetListByOrderGuid(Guid orderGuid, OrderShipType type = OrderShipType.Normal)
        {
            return DB.SelectAllColumnsFrom<ViewOrderShipList>().Where(ViewOrderShipList.Columns.OrderGuid).IsEqualTo(orderGuid)
                .AndExpression(ViewOrderShipList.Columns.Type).IsNull()
                .Or(ViewOrderShipList.Columns.Type).IsEqualTo((int)type)
                .CloseExpression()
                .ExecuteAsCollection<ViewOrderShipListCollection>();
        }

        public ViewOrderShipListCollection ViewOrderShipListGetListByProductGuid(IEnumerable<Guid> productGuids, OrderShipType type = OrderShipType.Normal)
        {
            return DB.SelectAllColumnsFrom<ViewOrderShipList>()
                .Where(ViewOrderShipList.Columns.ProductGuid).In(productGuids)
                .AndExpression(ViewOrderShipList.Columns.Type).IsNull()
                .Or(ViewOrderShipList.Columns.Type).IsEqualTo((int)type)
                .CloseExpression()
                .ExecuteAsCollection<ViewOrderShipListCollection>() ?? new ViewOrderShipListCollection();
        }

        public DataTable GetProductGuidAndMaxShipTime(OrderClassification orderClassification, DateTime queryEndTime)
        {
            string sql = string.Empty;
            if (orderClassification.Equals(OrderClassification.LkSite))
            {
                sql = string.Format(@"SELECT pd.product_guid
	                    --, MAX(osl.ship_time) AS max_ship_time
	                    FROM (
		                     SELECT bh.guid AS product_guid
		                     , COUNT(CASE WHEN (ISNULL(os.ship_time,'') = '' or os.ship_time >= @queryDate) THEN 1 END) AS unship_count
		                     FROM {0} AS bh WITH(nolock)
		                     INNER JOIN {1} AS dp WITH(nolock) ON dp.business_hour_guid = bh.GUID
                             LEFT JOIN {2} AS dsi WITH (NOLOCK) ON dsi.business_hour_guid = bh.GUID 
		                     INNER JOIN {3} AS da WITH(nolock) ON da.business_hour_guid = bh.GUID
		                     INNER JOIN {4} AS ctl WITH(nolock) ON ctl.business_hour_guid = bh.guid
                             INNER JOIN [{5}] AS od WITH(nolock) ON od.guid = ctl.order_guid
		                     LEFT OUTER JOIN {6} AS os WITH(nolock) ON os.order_guid = ctl.order_guid and os.type = {9}
                             LEFT JOIN (SELECT business_hour_guid,
                                                Count(*) AS isp_count
                                        FROM   cash_trust_log WITH(NOLOCK)
                                        WHERE  status IN ( 6, 7 )
                                        GROUP  BY business_hour_guid) AS ctlisp
                                    ON ctlisp.business_hour_guid = bh.GUID
		                     WHERE da.vendor_billing_model = 1
		                     AND bh.business_hour_order_time_e < @queryDate AND Isnull(dsi.ordered_quantity, 0) + COALESCE(ctlisp.isp_count, 0) >= bh.business_hour_order_minimum
		                     AND da.shipped_date is null
		                     AND dp.delivery_type = 2
		                     AND ctl.order_classification = 1
		                     AND ctl.status in (0, 1, 2, 6 ,7)
                             AND od.order_status & 512=0
		                     AND ctl.order_guid not in (
			                    SELECT order_guid
			                    FROM {7} WITH(nolock)
			                    WHERE progress_status in ({10}))
                                AND ((od.order_status & 8 > 0) or (od.order_status & 2097152 > 0 or od.order_status & 4194304 > 0))
		                        GROUP BY bh.GUID 
		                    ) AS pd
	                    --INNER JOIN {8} AS osl WITH(nolock) ON osl.product_guid = pd.product_guid AND osl.order_classification = 1 and osl.type = {9} 
	                    WHERE pd.unship_count = 0
	                    GROUP BY pd.product_guid "
                             , BusinessHour.Schema.TableName
                             , DealProperty.Schema.TableName
                             , DealSalesInfo.Schema.TableName
                             , DealAccounting.Schema.TableName
                             , CashTrustLog.Schema.TableName
                             , Order.Schema.TableName
                             , OrderShip.Schema.TableName
                             , ReturnForm.Schema.TableName
                             , ViewOrderShipList.Schema.TableName
                             , (int)OrderShipType.Normal
                             , string.Format("{0}, {1}, {2}, {3}", (int)ProgressStatus.Processing, (int)ProgressStatus.AtmQueueing, (int)ProgressStatus.AtmQueueSucceeded, (int)ProgressStatus.AtmFailed));
            }
            else if (orderClassification.Equals(OrderClassification.HiDeal))
            {
                sql = string.Format(@"SELECT pd.product_id
	                    , MAX(osl.ship_time) AS max_ship_time
	                    FROM (
		                     SELECT hp.id AS product_id
		                     , hp.guid AS product_guid
		                     , COUNT(CASE WHEN (ISNULL(os.ship_time,'') = '' or os.ship_time >= @queryDate) THEN 1 END) AS unship_count
		                     FROM {0} AS hp WITH (nolock)
		                     INNER JOIN {1} AS hd WITH(nolock) ON hd.id = hp.deal_id
		                     INNER JOIN {2} AS hod WITH(nolock) ON hod.product_id = hp.id
		                     INNER JOIN {3} AS ctl WITH(nolock) ON ctl.order_guid = hod.hi_deal_order_guid
		                     LEFT OUTER JOIN {4} AS os WITH(nolock) ON os.order_guid = ctl.order_guid and os.type = {7}
		                     WHERE hp.vendor_billing_model = 1
		                     AND hd.deal_end_time < @queryDate
		                     AND hp.shipped_date is null
		                     AND hp.is_home_delivery = 1
		                     AND ctl.order_classification = 2
		                     AND ctl.status in (0, 1, 2)
		                     AND ctl.order_detail_guid not in (
			                    SELECT order_detail_guid
			                    FROM {5} WITH(nolock)
			                    WHERE returned_status = 1)
		                     GROUP BY hp.id, hp.guid
		                    ) AS pd
	                    INNER JOIN {6} AS osl WITH(nolock) ON osl.product_guid = pd.product_guid AND osl.order_classification = 2 and osl.type = {7} 
	                    WHERE pd.unship_count = 0
	                    GROUP BY pd.product_id "
                        , HiDealProduct.Schema.TableName
                        , HiDealDeal.Schema.TableName
                        , HiDealOrderDetail.Schema.TableName
                        , CashTrustLog.Schema.TableName
                        , OrderShip.Schema.TableName
                        , ViewHiDealReturned.Schema.TableName
                        , ViewOrderShipList.Schema.TableName
                        , (int)OrderShipType.Normal);
            }
            //DataTable dt = new DataTable();
            //if (!string.IsNullOrEmpty(sql))
            //{
            //    string queryDate = queryEndTime.ToString("yyyy-MM-dd HH:mm:ss.000");
            //    IDataReader idr = new SubSonic.InlineQuery().ExecuteReader(sql, queryDate);
            //    using (idr)
            //    {
            //        dt.Load(idr);
            //    }
            //}
            //return dt;


            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(sql))
            {
                QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
                qc.CommandTimeout = 240;
                qc.AddParameter("@queryDate", queryEndTime.ToString("yyyy-MM-dd HH:mm:ss.000"), DbType.DateTime);
                using (IDataReader idr = DataService.GetReader(qc))
                {
                    dt.Load(idr);
                }
            }

            return dt;
        }

        public DataTable GetProductMainComboGuidAndMaxShipTime(DateTime queryEndTime)
        {
            //child條件為使用商家系統、結案時間限定、子檔、shipped date有值、達最低門檻、宅配             
            //主檔的出貨日為NULL才選擇
            //childCount為shipped date有值，而且同一個母檔的總個數
            //subCount為各多檔次的母檔的有效子檔總數（使用商家系統、子檔、達最低門檻、宅配且過濾退貨處理中退貨份數後是否仍有出貨份數，但shipped date、結案時間限定不限）
            //當shipped_date有值時，而且有值的子檔個數與總子檔個數相同，則取max shipped_date 

            string conditinoTBL = string.Format(@"FROM {0} AS cd 
                                                JOIN {1} AS da WITH (NOLOCK) 
                                                  ON cd.BusinessHourGuid = da.business_hour_guid 
                                                JOIN {2} AS bh WITH (NOLOCK) 
                                                  ON da.business_hour_guid = bh.GUID 
                                                JOIN {3} AS dp WITH (NOLOCK) 
                                                  ON dp.business_hour_guid = bh.GUID 
                                                JOIN {4} AS god WITH (NOLOCK) 
                                                  ON god.business_hour_guid = bh.GUID 
                                                LEFT JOIN {5} AS dsi WITH (NOLOCK)
                                                  ON dsi.business_hour_guid = bh.GUID 
                                                LEFT JOIN (SELECT business_hour_guid, count(*) AS isp_count
												           FROM {6} AS ctl WITH(NOLOCK) INNER JOIN [{7}] AS o WITH(NOLOCK) ON ctl.order_guid=o.guid
												           WHERE status in (6,7) AND o.order_status&512=0
												           GROUP BY business_hour_guid) AS ctlisp
												ON ctlisp.business_hour_guid = bh.GUID
                                                ", ComboDeal.Schema.TableName, DealAccounting.Schema.TableName, BusinessHour.Schema.TableName, DealProperty.Schema.TableName, GroupOrder.Schema.TableName, DealSalesInfo.Schema.TableName,CashTrustLog.Schema.TableName, Order.Schema.TableName);

            string sql = "";
            sql = string.Format(@"
                                select product_guid,max_shipped_date from deal_accounting da join (
                                select product_guid,shipped_date as max_shipped_date from (
                                select main.MainBusinessHourGuid as product_guid,child.shipped_date 
                                ,row_number()over(partition by main.MainBusinessHourGuid order by child.shipped_date desc) as rn from ( 
                                SELECT
                                  da.business_hour_guid AS BID,da.shipped_date,cd.MainBusinessHourGuid
                                  ,count(1)  OVER(partition by MainBusinessHourGuid) as childCount 
                                {0}
                                WHERE da.vendor_billing_model = {1} 
                                AND bh.business_hour_order_time_e < @queryDate 
                                AND MainBusinessHourGuid <> BusinessHourGuid
                                AND da.shipped_date is not null 
                                AND ISNULL(dsi.ordered_quantity,0) + COALESCE(ctlisp.isp_count, 0) >= bh.business_hour_order_minimum 
                                AND dp.delivery_type = {2} 
                                ) child --子檔已完成筆數
                                join (
                                SELECT MainBusinessHourGuid,BusinessHourGuid,(COUNT(1) OVER (PARTITION BY MainBusinessHourGuid)) AS subCount
                                {0}
                                LEFT JOIN (SELECT product_guid, COUNT(trust_id) AS returnning_count
	                                FROM view_order_return_form_list as orf WITH(NOLOCK) 
	                                JOIN return_form_refund AS rf WITH(NOLOCK) ON orf.return_form_id = rf.return_form_id
	                                WHERE orf.progress_status in ({3},{4},{5},{6})
	                                AND orf.refund_type in ({7},{8},{9},{10})
	                                AND rf.is_freight = 0
	                                GROUP BY product_guid) AS rft ON rft.product_guid = bh.GUID
                                WHERE da.vendor_billing_model = {1}
                                AND ((bh.business_hour_order_time_e <= @queryDate AND ISNULL(dsi.ordered_quantity,0) + COALESCE(ctlisp.isp_count, 0) >= bh.business_hour_order_minimum) OR (bh.business_hour_order_time_e > @queryDate AND god.slug IS NULL))
                                AND MainBusinessHourGuid <> BusinessHourGuid
                                AND dp.delivery_type = {2}
                                AND ISNULL(dsi.ordered_quantity,0) + COALESCE(ctlisp.isp_count, 0) > coalesce(rft.returnning_count, 0)) main --子檔應完成筆數
                                on child.BID=main.BusinessHourGuid and child.childCount=subCount
                                ) resultTop where rn=1

                                ) isUpdate on isUpdate.product_guid=da.business_hour_guid
                                inner join business_hour mb on mb.guid=da.business_hour_guid
                                where da.shipped_date is null and mb.business_hour_order_time_e <= @queryDate", conditinoTBL, (int)VendorBillingModel.BalanceSheetSystem, (int)DeliveryType.ToHouse,
                                                              (int)ProgressStatus.Processing, (int)ProgressStatus.AtmFailed, (int)ProgressStatus.AtmQueueing, (int)ProgressStatus.AtmQueueSucceeded,
                                                              (int)RefundType.Scash, (int)RefundType.Cash, (int)RefundType.Atm, (int)RefundType.Tcash);

            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(sql))
            {
                string queryDate = queryEndTime.ToString("yyyy-MM-dd HH:mm:ss.000");
                using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryDate))
                {
                    dt.Load(idr);
                }
            }
            return dt;
        }

        public DataTable ProductProgressStatusGetTable(string queryStartTime, string queryEndTime, bool isCompeleted = true)
        {
            //2014/8/1為宅配檔新制計算起點
            //先收單據後付款檔次也需壓記退換貨完成日
            //壓記規則同其他付款方式檔次(截至今日00:00:00 出貨回覆日已過出貨緩衝期(鑑賞期)檔次)

            string sql = string.Format(@"
select bh.GUID,i.item_name,dp.unique_id,da.shipped_date,da.remittance_type,god.status & {1} as 'partialFail'
from business_hour bh WITH (NOLOCK)
join deal_accounting da WITH (NOLOCK) on bh.GUID = da.business_hour_guid
join item i WITH (NOLOCK) on bh.GUID = i.business_hour_guid
join deal_property dp WITH (NOLOCK) on bh.GUID = dp.business_hour_guid
join group_order god WITH (NOLOCK) on god.business_hour_guid = bh.GUID
join deal_sales_info dsi WITH (NOLOCK) on dsi.business_hour_guid = god.business_hour_guid
left join combo_deals cd WITH (NOLOCK) on cd.BusinessHourGuid = god.business_hour_guid
left join (
	select o.parent_order_id from [order] o WITH (NOLOCK) 
	left join return_form rf WITH (NOLOCK) on rf.order_guid = o.GUID
	left join order_return_list orl WITH (NOLOCK) on orl.order_guid = o.GUID
	where ((rf.progress_status <> 4 and rf.vendor_progress_status in (1,2,3,7)) or orl.status in (3,5,7,8))
    and o.order_status & {2} > 0
	group by o.parent_order_id
) fail on god.order_guid=fail.parent_order_id 
where fail.parent_order_id {0} 
AND bh.business_hour_order_time_e < @queryEndTime 
AND (cd.MainBusinessHourGuid <> cd.BusinessHourGuid or cd.MainBusinessHourGuid is null) 
AND da.final_balance_sheet_date is null and dp.delivery_type=2 
and da.vendor_billing_model = {4}
and bh.business_hour_order_time_s>='2014/8/1' 
AND god.slug >= bh.business_hour_order_minimum 
and 
(
    (
	    (
		    da.remittance_type=3 and 
                (
                    (da.partially_payment_date >= @queryStartTime and da.partially_payment_date < @queryEndTime) 
                    or (da.partially_payment_date is null and god.status&{1}>0)
                )
	    )
	    or 
        (
		    da.remittance_type in ({3}) and da.partially_payment_date is null 
            and da.shipped_date >= @queryStartTime 
            and da.shipped_date < @queryEndTime - @shipBuffer
        )
	)
	or
	(
		bh.business_hour_order_time_s >= '2014/08/01' 
		and da.remittance_type in ({3}, 3)
		and shipped_date is null
		and partially_payment_date is null
		and dsi.ordered_quantity = 0
	)
)
", isCompeleted ? "is null " : "is not null AND bh.business_hour_deliver_time_e < @queryEndTime "
 , (int)GroupOrderStatus.PartialFail
 , (int)OrderStatus.Complete
 , string.Format("{0}{1}", (int)RemittanceType.Others
                         , string.Format(", {0}, {1}, {2} ,{3}", (int)RemittanceType.Flexible, (int)RemittanceType.Monthly, (int)RemittanceType.Weekly,(int)RemittanceType.Fortnightly))
 , (int)VendorBillingModel.BalanceSheetSystem);

            DataTable dt = new DataTable();
            QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
            qc.AddParameter("@queryStartTime", queryStartTime, DbType.DateTime);
            qc.AddParameter("@queryEndTime", queryEndTime, DbType.DateTime);
            qc.AddParameter("@shipBuffer", config.ShippingBuffer, DbType.Int32);
            using (IDataReader idr = DataService.GetReader(qc))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable PaymentChangeGetTable(string queryDate)
        {
            //昨日暫付七成已有值而且有客訴紀錄

            string sql = string.Format(@"
select da.business_hour_guid,i.item_name,dp.unique_id from deal_accounting da WITH (NOLOCK)
join vendor_payment_change vpc WITH (NOLOCK) on da.business_hour_guid = vpc.business_hour_guid
join item i WITH (NOLOCK) on i.business_hour_guid = da.business_hour_guid
join deal_property dp WITH (NOLOCK) on dp.business_hour_guid=da.business_hour_guid
where '{0}' = cast(da.final_balance_sheet_date as date)
group by da.business_hour_guid,i.item_name,dp.unique_id
", queryDate);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetProductMainComboGuidAndFinalBalanceSheetDate(DateTime queryEndTime)
        {
            //child條件為使用商家系統、結案時間限定、子檔、shipped date有值、達最低門 檻、宅配             
            //主檔的出貨日為NULL才選擇
            //childCount為shipped date有值，而且同一個母檔的總個數
            //subCount為各多檔次的母檔的子檔總數
            //當shipped_date有值時，而且有值的子檔個數與總子檔個數相同，則取max shipped_date 

            string conditinoTBL = string.Format(@"FROM {0} AS cd 
                                                JOIN {1} AS da WITH (NOLOCK) 
                                                  ON cd.BusinessHourGuid = da.business_hour_guid 
                                                JOIN {2} AS bh WITH (NOLOCK) 
                                                  ON da.business_hour_guid = bh.GUID 
                                                JOIN {3} AS dp WITH (NOLOCK) 
                                                  ON dp.business_hour_guid = bh.GUID 
                                                JOIN {4} AS god WITH (NOLOCK) 
                                                  ON god.business_hour_guid = bh.GUID 
                                                ", ComboDeal.Schema.TableName, DealAccounting.Schema.TableName, BusinessHour.Schema.TableName, DealProperty.Schema.TableName, GroupOrder.Schema.TableName);

            string sql = "";
            sql = string.Format(@"
                                select product_guid,isUpdate.final_balance_sheet_date from deal_accounting da join (
                                select product_guid,final_balance_sheet_date from (
                                select main.MainBusinessHourGuid as product_guid,child.final_balance_sheet_date 
                                ,row_number()over(partition by main.MainBusinessHourGuid order by child.final_balance_sheet_date desc) as rn from ( 
                                SELECT
                                  da.business_hour_guid AS BID,da.final_balance_sheet_date,cd.MainBusinessHourGuid
                                  ,count(1)  OVER(partition by MainBusinessHourGuid) as childCount 
                                {0}
                                WHERE da.vendor_billing_model = 1 
                                AND bh.business_hour_order_time_e < @queryDate 
                                AND MainBusinessHourGuid <> BusinessHourGuid 
                                AND da.final_balance_sheet_date is not null 
                                AND god.slug >= bh.business_hour_order_minimum 
                                AND dp.delivery_type = 2 
                                ) child  --子檔已完成筆數
                                join ( 
                                select MainBusinessHourGuid,BusinessHourGuid,(count(1) over(partition by MainBusinessHourGuid)) as subCount 
                                {0}
                                WHERE da.vendor_billing_model = 1
                                AND ((bh.business_hour_order_time_e <= @queryDate AND god.slug >= bh.business_hour_order_minimum)OR (bh.business_hour_order_time_e > @queryDate AND god.slug IS NULL))
                                AND MainBusinessHourGuid <> BusinessHourGuid
                                AND dp.delivery_type = 2
                                ) main  --子檔應完成筆數
                                on child.BID=main.BusinessHourGuid and child.childCount=subCount 
                                ) resultTop where rn=1
                                ) isUpdate on isUpdate.product_guid=da.business_hour_guid
                                inner join business_hour mb on mb.guid=da.business_hour_guid
                                where da.final_balance_sheet_date is null and mb.business_hour_order_time_e <= @queryDate", conditinoTBL);

            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(sql))
            {
                string queryDate = queryEndTime.ToString("yyyy-MM-dd HH:mm:ss.000");
                using (IDataReader idr = new SubSonic.InlineQuery().ExecuteReader(sql, queryDate))
                {
                    dt.Load(idr);
                }
            }
            return dt;
        }

        public DataTable GetProductMainComboGuidAndBalanceSheetCreateDate(DateTime queryEndTime)
        {
            //child條件為使用商家系統、結案時間限定、子檔、shipped date有值、達最低門 檻、宅配             
            //主檔的出貨日為NULL才選擇
            //childCount為shipped date有值，而且同一個母檔的總個數
            //subCount為各多檔次的母檔的子檔總數
            //當shipped_date有值時，而且有值的子檔個數與總子檔個數相同，則取max shipped_date 

            string conditinoTBL = string.Format(@"FROM {0} AS cd 
JOIN {1} AS da WITH (NOLOCK) 
  ON cd.BusinessHourGuid = da.business_hour_guid 
JOIN {2} AS bh WITH (NOLOCK) 
  ON da.business_hour_guid = bh.GUID 
JOIN {3} AS dp WITH (NOLOCK) 
  ON dp.business_hour_guid = bh.GUID 
JOIN {4} AS god WITH (NOLOCK) 
  ON god.business_hour_guid = bh.GUID 
", ComboDeal.Schema.TableName, DealAccounting.Schema.TableName, BusinessHour.Schema.TableName, DealProperty.Schema.TableName, GroupOrder.Schema.TableName);

            string sql = "";
            sql = string.Format(@"
select product_guid,isUpdate.balance_sheet_create_date from deal_accounting da join (
select product_guid,balance_sheet_create_date from (
select main.MainBusinessHourGuid as product_guid,child.balance_sheet_create_date 
,row_number()over(partition by main.MainBusinessHourGuid order by child.balance_sheet_create_date desc) as rn from ( 
SELECT
  da.business_hour_guid AS BID,da.balance_sheet_create_date,cd.MainBusinessHourGuid
  ,count(1)  OVER(partition by MainBusinessHourGuid) as childCount 
{0}
WHERE da.vendor_billing_model = 1 
AND bh.business_hour_order_time_e < @queryDate 
AND MainBusinessHourGuid <> BusinessHourGuid 
AND da.balance_sheet_create_date is not null 
AND god.slug >= bh.business_hour_order_minimum 
AND dp.delivery_type = 2 
AND da.remittance_type in (0, 3) 
) child 
join ( 
select MainBusinessHourGuid,BusinessHourGuid,(count(1) over(partition by MainBusinessHourGuid)) as subCount 
{0}
WHERE da.vendor_billing_model = 1
AND ((bh.business_hour_order_time_e < @queryDate AND god.slug >= bh.business_hour_order_minimum) OR (bh.business_hour_order_time_e >= @queryDate AND god.slug IS NULL))
AND MainBusinessHourGuid <> BusinessHourGuid
AND dp.delivery_type = 2
AND da.remittance_type in (0, 3) 
) main  
on child.BID=main.BusinessHourGuid and child.childCount=subCount 
) resultTop where rn=1
) isUpdate on isUpdate.product_guid=da.business_hour_guid
where da.balance_sheet_create_date is null", conditinoTBL);

            DataTable dt = new DataTable();
            if (!string.IsNullOrEmpty(sql))
            {
                string queryDate = queryEndTime.ToString("yyyy-MM-dd HH:mm:ss.000");
                using (IDataReader idr = new SubSonic.InlineQuery().ExecuteReader(sql, queryDate))
                {
                    dt.Load(idr);
                }
            }
            return dt;
        }

        #endregion ViewOrderShipList

        #region view_ship_order_status

        public ViewShipOrderStatusCollection ViewShipOrderStatusCollectionGet(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewShipOrderStatus.Schema, filter);
            qc.CommandSql = "select * from " + ViewShipOrderStatus.Schema.Provider.DelimitDbName(ViewShipOrderStatus.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            ViewShipOrderStatusCollection vsosCol = new ViewShipOrderStatusCollection();

            vsosCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vsosCol;
        }

        public ViewVbsExchangeOrderListCollection ViewVbsExchangeOrderListCollectionGet(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewVbsExchangeOrderList.Schema, filter);
            qc.CommandSql = "select * from " + ViewVbsExchangeOrderList.Schema.Provider.DelimitDbName(ViewVbsExchangeOrderList.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            ViewVbsExchangeOrderListCollection vsosCol = new ViewVbsExchangeOrderListCollection();

            vsosCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vsosCol;
        }



        public ViewVbsReturnOrderListCollection ViewVbsReturnOrderListCollectionGet(DateTime? beginDate, DateTime? endDate, List<int> progressStatus, List<int> vendorProgressStatus, string accountId, string queryOption, string queryKeyword)
        {
            SqlQuery query = DB.Select().From(ViewVbsReturnOrderList.Schema);

            query.Where(ViewVbsReturnOrderList.Columns.OrderId).IsNotNull();

            if (beginDate.HasValue && beginDate != DateTime.MinValue)
            {
                query.And(ViewVbsReturnOrderList.Columns.CreateTime).IsGreaterThanOrEqualTo(beginDate);
            }

            if (endDate.HasValue && endDate != DateTime.MinValue)
            {
                query.And(ViewVbsReturnOrderList.Columns.CreateTime).IsLessThan(endDate.Value.AddDays(1));
            }

            if (progressStatus.Any())
            {
                query.And(ViewVbsReturnOrderList.Columns.ProgressStatus).In(progressStatus);
            }

            if (vendorProgressStatus.Any())
            {
                query.And(ViewVbsReturnOrderList.Columns.VendorProgressStatus).In(vendorProgressStatus);
            }

            if (!string.IsNullOrEmpty(accountId))
            {
                query.And(ViewVbsReturnOrderList.Columns.AccountId).IsEqualTo(accountId);
            }


            if (!string.IsNullOrEmpty(queryOption))
            {
                if (!string.IsNullOrEmpty(queryKeyword))
                {
                    switch (queryOption)
                    {
                        case "uniqueId":
                            query.And(ViewVbsReturnOrderList.Columns.UniqueId).IsEqualTo(queryKeyword);
                            break;
                        case "order_id":
                            query.And(ViewVbsReturnOrderList.Columns.OrderId).IsEqualTo(queryKeyword);
                            break;
                        case "member_name":
                            query.And(ViewVbsReturnOrderList.Columns.MemberName).IsEqualTo(queryKeyword);
                            break;
                        case "mobile_number":
                            query.And(ViewVbsReturnOrderList.Columns.MobileNumber).IsEqualTo(queryKeyword);
                            break;
                        default:
                            break;
                    }
                }
            }



            return query.ExecuteAsCollection<ViewVbsReturnOrderListCollection>();

        }
        #endregion

        #region ViewNewShipOrderToSeller

        public ViewNewShipOrderToSellerCollection ViewNewShipOrderToSellerCollectionGet(double days = 1)
        {
            return DB.SelectAllColumnsFrom<ViewNewShipOrderToSeller>().NoLock()
                .Where(ViewNewShipOrderToSeller.Columns.CreateTime).IsGreaterThanOrEqualTo(DateTime.Now.AddDays(-days))
                .ExecuteAsCollection<ViewNewShipOrderToSellerCollection>();
        }

        #endregion

        #region 逾期/即將逾期訂單

        public ViewExpiringOrderCollection ViewExpiringOrderCollectionGet()
        {
            return DB.SelectAllColumnsFrom<ViewExpiringOrder>().NoLock()
                .ExecuteAsCollection<ViewExpiringOrderCollection>();
        }

        public ViewOverdueOrderCollection ViewOverdueOrderCollectionGet()
        {
            return DB.SelectAllColumnsFrom<ViewOverdueOrder>().NoLock()
                .ExecuteAsCollection<ViewOverdueOrderCollection>();
        }

        public ViewSimpleOverdueOrder ViewSimpleOverdueOrderGet(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewSimpleOverdueOrder>().NoLock()
                .Where(ViewSimpleOverdueOrder.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteSingle<ViewSimpleOverdueOrder>();
        }

        public ViewOrderNotYetShippedCollection ViewOrderNotYetShippedCollectionGetByNoLastShipDate()
        {
            return DB.SelectAllColumnsFrom<ViewOrderNotYetShipped>().NoLock()
                .Where(ViewOrderNotYetShipped.Columns.OpdId).IsNull()
                .ExecuteAsCollection<ViewOrderNotYetShippedCollection>();
        }

        #endregion

        public ViewVendorShipLogCollection ViewVendorShipLogCollectionGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewVendorShipLog>()
                 .Where(ViewVendorShipLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                 .OrderDesc(ViewVendorShipLog.Columns.VendorProcessTime)
                    .ExecuteAsCollection<ViewVendorShipLogCollection>();
        }

        #region temporary_payment_date
        //計算購買數與退貨數
        //讓AP計算不讓SQL負擔太大
        public DataTable TempPaymentGetTable(string queryStartTime, string queryEndTime)
        {
            //計算上週一到週日（起始為星期一）結檔檔次
            //多檔次需要尋找母檔出貨回覆日在期限內
            //然後撈取所有子檔  子檔統一使用母檔出貨回覆日（因為母檔跟子檔時間可能差異很大）
            //母檔無銷售份數紀錄
            //是商家 宅配 七成 成檔
            //總數與退貨數
            string sql = string.Format(@"
select MainBusinessHourGuid as 'mid', businesshourguid as 'bid', cast(coalesce(god.slug, 0) as int) as slug
from combo_deals cd with(nolock)
join (
	select MainBusinessHourGuid 'mainbid', bh.business_hour_order_minimum
	from business_hour bh with(nolock)
	join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
	join combo_deals cd with (nolock) on cd.businesshourguid = bh.GUID
	where 
	da.vendor_billing_model = {0}
	and da.remittance_type = {1}
	and da.partially_payment_date is null
	and bh.business_hour_order_time_s >= '2014/8/1'
	and da.shipped_date >= @queryStartTime and da.shipped_date <= @queryEndTime
	and MainBusinessHourGuid=businesshourguid
) main on cd.mainbusinesshourguid = main.mainbid
join group_order god with(nolock) on god.business_hour_guid = cd.businesshourguid and god.slug >= main.business_hour_order_minimum
union all
select bh.guid, bh.guid, cast(coalesce(god.slug, 0) as int) as slug
from business_hour bh with(nolock)
join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
join group_order god with(nolock) on god.business_hour_guid = bh.GUID and god.slug >= bh.business_hour_order_minimum
left join combo_deals cd with (nolock) on cd.businesshourguid = bh.GUID
where 
da.vendor_billing_model = {0}
and da.remittance_type = {1}
and da.partially_payment_date is null
and bh.business_hour_order_time_s >= '2014/8/1'
and da.shipped_date >= @queryStartTime and da.shipped_date <= @queryEndTime
and cd.businesshourguid is null
", (int)VendorBillingModel.BalanceSheetSystem, (int)RemittanceType.ManualPartially);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryStartTime, queryEndTime))
            {
                dt.Load(idr);
            }

            return dt;
        }

        public DataTable ReturningCountGetTable(IEnumerable<Guid> Guids)
        {
            DataTable infos = new DataTable();
            DataTable result = new DataTable();
            var tempIds = Guids.Distinct().ToList();
            int idx = 0;
            var bids = tempIds.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;

                string sql = string.Format(@"
select bid, sum(returning_order) as returning_count 
from (
	select god.business_hour_guid 'bid', count(op.order_guid)/dp.combo_pack_count as returning_order
	from group_order as god with(nolock) 
	join [order] as od with(nolock) on od.parent_order_id = god.order_guid
	join order_product as op with(nolock) on op.order_guid = od.guid
	join deal_property as dp with(nolock) on dp.business_hour_guid = god.business_hour_guid
	where 
	god.business_hour_guid in ({0})
	and od.order_status & {1} > 0
	and op.is_returning = 1 
	group by god.business_hour_guid, op.order_guid, dp.combo_pack_count
) as t
group by bid "
 , string.Join(",", bids.Skip(idx).Take(batchLimit))
 , (int)OrderStatus.Complete);

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                using (IDataReader reader = DataService.GetReader(qc))
                {
                    infos.Load(reader);
                }

                result.Merge(infos);

                idx += batchLimit;
            }
            return result;
        }

        //暫付七成付款異常
        public DataTable TempPaymentAlertGetTable(string queryStartTime, string queryEndTime)
        {
            string sql = @"
select bh.guid,i.item_name,dp.unique_id from business_hour bh WITH (NOLOCK) 
join item i WITH (NOLOCK) on i.business_hour_guid = bh.GUID
join deal_property dp WITH (NOLOCK) on dp.business_hour_guid=bh.GUID
where bh.guid in (
select distinct ISNULL(cd.MainBusinessHourGuid,da.business_hour_guid) 'business_hour_guid' 
from deal_accounting da WITH (NOLOCK)
left join combo_deals cd WITH (NOLOCK) on da.business_hour_guid = cd.BusinessHourGuid
join group_order god WITH(NOLOCK) on god.business_hour_guid = da.business_hour_guid
where da.shipped_date>=@queryStartTime and da.shipped_date <= @queryEndTime
and partially_payment_date is null and god.status&33554432>0
and da.remittance_type =3 )
";
            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryStartTime, queryEndTime))
            {
                dt.Load(idr);
            }

            return dt;
        }
        //暫付七成提醒通知
        public DataTable NoticePartillyPaymentGetTable(string queryDate)
        {
            string sql = @"
select da.business_hour_guid,i.item_name,dp.unique_id,s.CompanyEmail 'mail'
,s.seller_name from deal_accounting da WITH (NOLOCK)
left join combo_deals cd WITH (NOLOCK) on da.business_hour_guid = cd.BusinessHourGuid
join item i WITH (NOLOCK) on i.business_hour_guid = da.business_hour_guid
join business_hour bh WITH (NOLOCK) on bh.GUID = da.business_hour_guid
join seller s WITH (NOLOCK) on s.GUID = bh.seller_GUID
join deal_property dp WITH (NOLOCK) on dp.business_hour_guid=da.business_hour_guid
where partially_payment_date=@queryDate
and (cd.BusinessHourGuid = cd.MainBusinessHourGuid or cd.MainBusinessHourGuid is null)
and da.remittance_type = 3
";
            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryDate))
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region OrderUserMemoList

        public OrderUserMemoListCollection OrderUserMemoListGetList(Guid orderGuid, VbsDealType type = VbsDealType.Ppon)
        {
            return DB.SelectAllColumnsFrom<OrderUserMemoList>()
                .Where(OrderUserMemoList.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(OrderUserMemoList.Columns.DealType).IsEqualTo((int)type)
                .ExecuteAsCollection<OrderUserMemoListCollection>();
        }

        public OrderUserMemoListCollection OrderUserMemoListGetList(IEnumerable<Guid> orderGuids, VbsDealType type = VbsDealType.Ppon)
        {
            OrderUserMemoListCollection infos = new OrderUserMemoListCollection();
            List<Guid> tempGuids = orderGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<OrderUserMemoList>()
                        .Where(OrderUserMemoList.Columns.OrderGuid).In(tempGuids.Skip(idx).Take(batchLimit))
                        .And(OrderUserMemoList.Columns.DealType).IsEqualTo((int)type)
                        .ExecuteAsCollection<OrderUserMemoListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public int OrderUserMemoListSet(OrderUserMemoList uml)
        {
            return DB.Save<OrderUserMemoList>(uml);
        }

        public bool OrderUserMemoListDelete(int orderUserMemoId)
        {
            DB.Destroy<OrderUserMemoList>(OrderUserMemoList.IdColumn.ColumnName, orderUserMemoId);
            return true;
        }

        #endregion OrderUserMemoList

        #region OrderProduct

        public int OrderProductSet(OrderProduct op)
        {
            return DB.Save<OrderProduct>(op);
        }

        public int OrderProductSetList(OrderProductCollection products)
        {
            return DB.SaveAll(products);
        }

        public OrderProductCollection OrderProductGetListByOrderGuid(Guid order_guid)
        {
            return DB.SelectAllColumnsFrom<OrderProduct>()
              .Where(OrderProduct.Columns.OrderGuid).IsEqualTo(order_guid)
              .ExecuteAsCollection<OrderProductCollection>() ?? new OrderProductCollection();
        }

        public OrderProductCollection OrderProductGetList(IEnumerable<int> orderProductIds)
        {
            OrderProductCollection infos = new OrderProductCollection();
            List<int> tempIds = orderProductIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<OrderProduct>().NoLock()
                        .Where(OrderProduct.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<OrderProductCollection>() ?? new OrderProductCollection();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public OrderProductCollection OrderProductGetList(IEnumerable<Guid> orderGuids)
        {
            OrderProductCollection infos = new OrderProductCollection();
            List<Guid> tempIds = orderGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<OrderProduct>()
                        .Where(OrderProduct.Columns.OrderGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<OrderProductCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        #endregion OrderProduct

        #region OrderProductOption

        public OrderProductOptionCollection OrderProductOptionGetListByOrderProductIds(IEnumerable<int> orderProductIds)
        {
            OrderProductOptionCollection infos = new OrderProductOptionCollection();
            List<int> tempIds = orderProductIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<OrderProductOption>()
                        .Where(OrderProductOption.Columns.OrderProductId).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<OrderProductOptionCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public bool OrderProductOptionSet(OrderProductOption opo)
        {
            DB.Save<OrderProductOption>(opo);
            return true;
        }

        public int OrderProductOptionSetList(OrderProductOptionCollection options)
        {
            return DB.SaveAll(options);
        }

        #endregion OrderProductOption

        #region ViewOrderProductOptionList

        public List<ViewOrderProductOptionList> ViewOrderProductOptionListGetIsCurrentList(int productId, VbsDealType dealType)
        {
            ViewOrderProductOptionListCollection infos = DB.SelectAllColumnsFrom<ViewOrderProductOptionList>()
                    .Where(ViewOrderProductOptionList.Columns.ProductId).IsEqualTo(productId)
                    .And(ViewOrderProductOptionList.Columns.DealType).IsEqualTo((int)dealType)
                    .And(ViewOrderProductOptionList.Columns.IsCurrent).IsEqualTo(true)
                    .ExecuteAsCollection<ViewOrderProductOptionListCollection>() ?? new ViewOrderProductOptionListCollection();

            return infos.ToList();
        }

        public List<ViewOrderProductOptionList> ViewOrderProductOptionListGetIsCurrentList(IEnumerable<int> productIds)
        {
            ViewOrderProductOptionListCollection infos = DB.SelectAllColumnsFrom<ViewOrderProductOptionList>().NoLock()
                    .Where(ViewOrderProductOptionList.Columns.ProductId).In(productIds)
                    .And(ViewOrderProductOptionList.Columns.IsCurrent).IsEqualTo(true)
                    .ExecuteAsCollection<ViewOrderProductOptionListCollection>() ?? new ViewOrderProductOptionListCollection();

            return infos.ToList();
        }

        public List<ViewOrderProductOptionList> ViewOrderProductOptionListGetReadyItems(IEnumerable<int> productIds)
        {
            var infos = new List<ViewOrderProductOptionList>();
            List<int> tempGuids = productIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;
                string sql = string.Format(@"
select vopol.* from {0} vopol
left join {1} vosl on vopol.order_guid = vosl.order_guid and vosl.type = {4}
left join {2} vorfl on vorfl.order_guid = vopol.order_guid
where vopol.product_id in ({3}) and (vosl.ship_time is null or vosl.ship_time>GETDATE()) 
and (vorfl.progress_status in (3,4) or vorfl.product_guid is null)
", ViewOrderProductOptionList.Schema.TableName
 , ViewOrderShipList.Schema.TableName
 , ViewOrderReturnFormList.Schema.TableName
 , string.Join(",", tempGuids.Skip(idx).Take(batchLimit))
 , (int)OrderShipType.Normal);

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                var data = new ViewOrderProductOptionListCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }

            return infos;
        }

        public Dictionary<Guid, string> GetOrderProductOptionInfo(List<ViewOrderProductOptionList> infos)
        {
            var optionLists = infos.GroupBy(x => new
            {
                x.OrderGuid,
                x.OrderProductId
            }) //合併同一order_product_id 多個選項為一個item
                        .Select(x => new
                        {
                            OrderGuid = x.Key.OrderGuid,
                            OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                            Quantity = 1
                        })
                        .GroupBy(x => new
                        {
                            OrderGuid = x.OrderGuid,
                            OptionDescription = x.OptionDescription
                        }) //合併相同品項名稱並統計quantity
                        .Select(x => new
                        {
                            OrderGuid = x.Key.OrderGuid,
                            OptionDescription = x.Key.OptionDescription,
                            Quantity = x.Sum(o => o.Quantity),
                        })
                        .GroupBy(x => x.OrderGuid)
                        .ToDictionary(x => x.Key,
                                      x => string.Join("\n", x.Select(item => (string.IsNullOrEmpty(item.OptionDescription)
                                                                                ? string.Empty
                                                                                : string.Format("({0})", item.OptionDescription)) + item.Quantity.ToString())));

            return optionLists;
        }

        public List<OrderProductOptionInfo> GetOrderProductOptionList(List<ViewOrderProductOptionList> infos)
        {
            return infos.GroupBy(x => new
            {
                OrderGuid = x.OrderGuid,
                OrderProductId = x.OrderProductId
            }) //合併同一order_product_id 多個選項為一個item
                        .Select(x => new
                        {
                            OrderGuid = x.Key.OrderGuid,
                            OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                            ItemNo = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => (o.ItemNo ?? string.Empty)))),
                            Quantity = 1
                        })
                        .GroupBy(x => new
                        {
                            OrderGuid = x.OrderGuid,
                            OptionDescription = x.OptionDescription,
                            ItemNo = x.ItemNo
                        }) //合併相同品項名稱並統計quantity
                        .Select(x => new OrderProductOptionInfo
                        {
                            OrderGuid = x.Key.OrderGuid,
                            OptionDescription = x.Key.OptionDescription,
                            ItemNo = x.Key.ItemNo,
                            Quantity = x.Sum(o => o.Quantity),
                        })
                        .ToList();
        }

        public List<OrderProductOptionInfo> GetOrderProductOptionListNotGroup(List<ViewOrderProductOptionList> infos)
        {
            return infos.GroupBy(x => new
            {
                OrderGuid = x.OrderGuid,
                OrderProductId = x.OrderProductId
            }) //合併同一order_product_id 多個選項為一個item
                        .Select(x => new OrderProductOptionInfo
                        {
                            OrderGuid = x.Key.OrderGuid,
                            OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                            ItemNo = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => (o.ItemNo ?? string.Empty)))),
                            Quantity = 1
                        })
                        .ToList();
        }

        public Dictionary<string, int> GetProductOptionStatistics(List<ViewOrderProductOptionList> infos)
        {
            var optionLists = infos.GroupBy(x => x.OrderProductId) //合併同一order_product_id 多個選項為一個item
                                   .Select(x => new
                                   {
                                       OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                                       Quantity = 1
                                   })
                                   .GroupBy(x => x.OptionDescription) //合併相同品項名稱並統計quantity
                                   .ToDictionary(x => x.Key,
                                                 x => x.Sum(o => o.Quantity));

            return optionLists;
        }

        #endregion ViewOrderProductOptionList

        #region ViewReturnFormProductOptionList

        public List<ViewReturnFormProductOptionList> ViewReturnFormProductOptionListGetList(IEnumerable<int> returnFormIds)
        {
            ViewReturnFormProductOptionListCollection infos = new ViewReturnFormProductOptionListCollection();
            List<int> tempIds = returnFormIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewReturnFormProductOptionList>()
                        .Where(ViewReturnFormProductOptionList.Columns.ReturnFormId).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewReturnFormProductOptionListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos.ToList();
        }

        public Dictionary<int, string> GetReturnOrderProductOptionDesc(IEnumerable<int> returnFormIds)
        {
            List<ViewReturnFormProductOptionList> infos = ViewReturnFormProductOptionListGetList(returnFormIds);

            var optionLists = infos.GroupBy(x => new
            {
                ReturnFormId = x.ReturnFormId,
                OrderProductId = x.OrderProductId
            }) //合併同一order_product_id 多個選項為一個item
                        .Select(x => new
                        {
                            ReturnFormId = x.Key.ReturnFormId,
                            OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                            Quantity = 1,
                        })
                        .GroupBy(x => new
                        {
                            ReturnFormId = x.ReturnFormId,
                            OptionDescription = x.OptionDescription
                        }) //合併相同品項名稱並統計quantity
                        .Select(x => new
                        {
                            ReturnFormId = x.Key.ReturnFormId,
                            OptionDescription = x.Key.OptionDescription,
                            Quantity = x.Sum(o => o.Quantity),
                        })
                        .GroupBy(x => x.ReturnFormId)
                        .ToDictionary(x => x.Key,
                                        x => string.Join("\n", x.Select(item => (string.IsNullOrEmpty(item.OptionDescription)
                                                                                    ? string.Empty
                                                                                    : string.Format("({0})", item.OptionDescription)) + item.Quantity.ToString())));

            return optionLists;
        }

        public List<OrderProductOptionInfo> GetReturnOrderProductOptionInfo(IEnumerable<int> returnFormIds, bool? isCollected = null)
        {
            List<ViewReturnFormProductOptionList> infos = ViewReturnFormProductOptionListGetList(returnFormIds);

            infos = isCollected != null ? infos.Where(x => x.IsCollected == isCollected).ToList() : infos;

            return infos.GroupBy(x => new
            {
                ReturnFormId = x.ReturnFormId,
                OrderProductId = x.OrderProductId
            }) //合併同一order_product_id 多個選項為一個item
                            .Select(x => new
                            {
                                ReturnFormId = x.Key.ReturnFormId,
                                OptionDescription = string.Format("{0}", string.Join(",", x.OrderBy(o => o.CatgSeq).Select(o => o.OptionName))),
                                Quantity = 1,
                                OrderProductId = x.Key.OrderProductId
                            })
                            .GroupBy(x => new
                            {
                                ReturnFormId = x.ReturnFormId,
                                OptionDescription = x.OptionDescription
                            }) //合併相同品項名稱並統計quantity
                            .Select(x => new OrderProductOptionInfo
                            {
                                ReturnFormId = x.Key.ReturnFormId,
                                OptionDescription = string.IsNullOrEmpty(x.Key.OptionDescription) ? string.Empty : string.Format("({0})", x.Key.OptionDescription),
                                Quantity = x.Sum(o => o.Quantity),
                                OrderProductIds = new List<int>(x.Select(o => o.OrderProductId))
                            })
                            .ToList();
        }

        #endregion ViewReturnFormProductOptionList

        #region ViewOrderReturnFormList

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(Guid productGuid)
        {
            return DB.SelectAllColumnsFrom<ViewOrderReturnFormList>()
                    .Where(ViewOrderReturnFormList.Columns.ProductGuid).IsEqualTo(productGuid)
                    .ExecuteAsCollection<ViewOrderReturnFormListCollection>() ?? new ViewOrderReturnFormListCollection();
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(IEnumerable<Guid> productGuids)
        {
            ViewOrderReturnFormListCollection infos = new ViewOrderReturnFormListCollection();
            List<Guid> tempGuids = productGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewOrderReturnFormList>().NoLock()
                    .Where(ViewOrderReturnFormList.Columns.ProductGuid).In(tempGuids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<ViewOrderReturnFormListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByDealGuid(IEnumerable<Guid> productGuids, IEnumerable<int> progressStatus, IEnumerable<int> vendorProgressStatus, IEnumerable<int> refundType)
        {
            var infos = new ViewOrderReturnFormListCollection();
            List<Guid> tempGuids = productGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;
                string sql = string.Format(@"
select * from {0} 
where {1} in ('{2}') ", ViewOrderReturnFormList.Schema.TableName, ViewOrderReturnFormList.Columns.ProductGuid, string.Join("','", tempGuids.Skip(idx).Take(batchLimit)));

                if (progressStatus.Any())
                {
                    sql += string.Format(" and {0} in ({1})", ViewOrderReturnFormList.Columns.ProgressStatus, string.Join(",", progressStatus));
                }

                if (vendorProgressStatus.Any())
                {
                    sql += string.Format(" and {0} in ({1})", ViewOrderReturnFormList.Columns.VendorProgressStatus, string.Join(",", vendorProgressStatus));
                }

                if (refundType.Any())
                {
                    sql += string.Format(" and {0} in ({1})", ViewOrderReturnFormList.Columns.RefundType, string.Join(",", refundType));
                }

                var qc = new QueryCommand(sql, ViewOrderReturnFormList.Schema.Provider.Name);
                var data = new ViewOrderReturnFormListCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewOrderReturnFormList>()
                    .Where(ViewOrderReturnFormList.Columns.OrderGuid).IsEqualTo(orderGuid)
                    .ExecuteAsCollection<ViewOrderReturnFormListCollection>() ?? new ViewOrderReturnFormListCollection();
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListByOrderGuid(IEnumerable<Guid> orderGuids)
        {
            ViewOrderReturnFormListCollection infos = new ViewOrderReturnFormListCollection();
            List<Guid> tempGuids = orderGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewOrderReturnFormList>()
                        .Where(ViewOrderReturnFormList.Columns.OrderGuid).In(tempGuids.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewOrderReturnFormListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewOrderReturnFormList ViewOrderReturnFormListGet(int returnFormId)
        {
            ViewOrderReturnFormList result = new ViewOrderReturnFormList();
            result.LoadAndCloseReader(ViewOrderReturnFormList.Query()
                                                             .WHERE(ViewOrderReturnFormList.Columns.ReturnFormId, returnFormId)
                                                             .ExecuteReader());

            return result;
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetList(
            DeliveryType? deliveryType, ProductDeliveryType? productDeliveryType, IEnumerable<int> progressStatusLists, IEnumerable<int> vendorProgressStatusLists,
            string queryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime, string signCompanyID, DateTime? processStartTime, DateTime? processEndTime, int allowanceOption)
        {
            SqlQuery query = DB.Select().From(ViewOrderReturnFormList.Schema);

            query.Where(ViewOrderReturnFormList.Columns.DealType).IsEqualTo((int)VbsDealType.Ppon);

            if (deliveryType.HasValue)
            {
                query.And(ViewOrderReturnFormList.Columns.DeliveryType).IsEqualTo((int)deliveryType);
            }

            if (productDeliveryType.HasValue)
            {
                query.And(ViewOrderReturnFormList.Columns.ProductDeliveryType).IsEqualTo((int)productDeliveryType);
            }

            if (progressStatusLists.Any())
            {
                query.And(ViewOrderReturnFormList.Columns.ProgressStatus).In(progressStatusLists);
            }

            if (vendorProgressStatusLists.Any())
            {
                query.And(ViewOrderReturnFormList.Columns.VendorProgressStatus).In(vendorProgressStatusLists);
            }

            if (!string.IsNullOrEmpty(queryOption))
            {
                if (!string.IsNullOrEmpty(queryKeyword))
                {
                    switch (queryOption)
                    {
                        case "OrderId":
                            query.And(ViewOrderReturnFormList.Columns.OrderId).IsEqualTo(queryKeyword);
                            break;
                        case "WmsOrderId":
                            query.And(ViewOrderReturnFormList.Columns.WmsOrderId).IsEqualTo(queryKeyword);
                            break;
                        case "DealName":
                            query.And(ViewOrderReturnFormList.Columns.DealName).Like("%" + queryKeyword + "%");
                            break;
                        case "DealId":
                            int id;
                            int.TryParse(queryKeyword, out id);
                            query.And(ViewOrderReturnFormList.Columns.ProductId).IsEqualTo(id);
                            break;
                        case "SalesName":
                            ViewEmployee emp = hp.ViewEmployeeGet(ViewEmployee.Columns.EmpName, queryKeyword);
                            int deSalesId = 0;
                            if (emp.IsLoaded)
                                deSalesId = emp.UserId;
                            query.And(ViewOrderReturnFormList.Columns.DeEmpName).IsEqualTo(deSalesId);
                            break;
                        case "SellerName":
                            query.And(ViewOrderReturnFormList.Columns.SellerName).Like("%" + queryKeyword + "%");
                            break;
                        default:
                            break;
                    }
                }
            }

            if (allowanceOption != 0)
            {
                if (allowanceOption == 1)
                {
                    query.And(ViewOrderReturnFormList.Columns.CreditNoteType).IsEqualTo((int)AllowanceStatus.None);
                }
                else if (allowanceOption == 2)
                {
                    query.And(ViewOrderReturnFormList.Columns.CreditNoteType).IsEqualTo((int)AllowanceStatus.PaperAllowance);
                    query.And(ViewOrderReturnFormList.Columns.IsCreditNoteReceived).IsEqualTo(false);
                }
                else if (allowanceOption == 3)
                {
                    query.And(ViewOrderReturnFormList.Columns.CreditNoteType).IsEqualTo((int)AllowanceStatus.PaperAllowance);
                    query.And(ViewOrderReturnFormList.Columns.IsCreditNoteReceived).IsEqualTo(true);
                }
            }
            if (queryStartTime != null && queryEndTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ReturnApplicationTime)
                    .IsLessThanOrEqualTo(queryEndTime.Value)
                    .And(ViewOrderReturnFormList.Columns.ReturnApplicationTime)
                    .IsGreaterThanOrEqualTo(queryStartTime.Value);
            }
            else if (queryStartTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ReturnApplicationTime)
                    .IsGreaterThanOrEqualTo(queryStartTime.Value);
            }
            else if (queryEndTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ReturnApplicationTime)
                    .IsLessThanOrEqualTo(queryEndTime.Value);
            }

            if (!string.IsNullOrEmpty(signCompanyID))
            {
                query.And(ViewOrderReturnFormList.Columns.SignCompanyID)
                    .IsEqualTo(signCompanyID);
            }

            if (processStartTime != null && processEndTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ModifyTime)
                    .IsLessThanOrEqualTo(processEndTime.Value)
                    .And(ViewOrderReturnFormList.Columns.ModifyTime)
                    .IsGreaterThanOrEqualTo(processStartTime.Value);
            }
            else if (processStartTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ModifyTime)
                    .IsGreaterThanOrEqualTo(processStartTime.Value);
            }
            else if (processEndTime != null)
            {
                query.And(ViewOrderReturnFormList.Columns.ModifyTime)
                    .IsLessThanOrEqualTo(processEndTime.Value);
            }

            return query.ExecuteAsCollection<ViewOrderReturnFormListCollection>();
        }

        public ViewOrderReturnFormListCollection ViewOrderReturnFormListGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewOrderReturnFormList.Columns.ReturnApplicationTime;
            QueryCommand qc = GetVpolWhereQC(ViewOrderReturnFormList.Schema, filter);
            qc.CommandSql = "select * from " + ViewOrderReturnFormList.Schema.Provider.DelimitDbName(ViewOrderReturnFormList.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewOrderReturnFormListCollection vpolCol = new ViewOrderReturnFormListCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Dictionary<int, string> GetToNotifyChannelReturnForm()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            string sql = " select r." + ReturnForm.Columns.Id + @", o." + OrderCorresponding.Columns.RelatedOrderId + " from " + OrderCorresponding.Schema.TableName + " o " +
                         " inner join " + ReturnForm.Schema.TableName + " r on o." + OrderCorresponding.Columns.OrderGuid + " = r." + ReturnForm.Columns.OrderGuid +
                         " where o." + OrderCorresponding.Columns.Type + " = " + (int)AgentChannel.PayEasyOrder +
                         " and r." + ReturnForm.Columns.ProgressStatus + " = " + (int)ProgressStatus.Completed + " and " + ReturnForm.Columns.NotifyChannel + " = " + (int)NotifyChannel.Init;
            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    int returnFormId = reader.GetInt32(0);
                    string relatedOrderId = reader.GetString(1);
                    if (result.ContainsKey(returnFormId) == false)
                    {
                        result.Add(returnFormId, relatedOrderId);
                    }
                }
            }
            return result;
        }

        #endregion

        #region CreditcardRefundRecord

        public bool CreditcardRefundRecordSet(CreditcardRefundRecord crr)
        {
            DB.Save<CreditcardRefundRecord>(crr);
            return true;
        }

        public CreditcardRefundRecord CreditcardRefundRecordGet(string transId)
        {
            return DB.Get<CreditcardRefundRecord>(CreditcardRefundRecord.Columns.TransId, transId);
        }

        #endregion CreditcardRefundRecord

        #region CompanyUserOrder

        /// <summary>
        /// 新增或修改CompanyUserOrder 資料
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool CompanyUserOrderSet(CompanyUserOrder order)
        {
            int count = DB.Save(order);
            return count > 0;
        }

        /// <summary>
        /// 修改訂單 WaitUpdate欄位的狀態
        /// </summary>
        /// <param name="orderGuidList">要異動的訂單編號列表</param>
        /// <param name="value">異動的值</param>
        /// <returns></returns>
        public int CompanyUserOrderUpdateWaitUpdate(List<Guid> orderGuidList, bool value)
        {
            return DB.Update<CompanyUserOrder>()
              .Set(CompanyUserOrder.WaitUpdateColumn)
              .EqualTo(value)
              .Where(CompanyUserOrder.OrderGuidColumn)
              .In(orderGuidList)
              .Execute();
        }

        public int CompanyUserOrderUpdateWaitUpdate(Guid orderGuid, bool value)
        {
            return DB.Update<CompanyUserOrder>()
              .Set(CompanyUserOrder.WaitUpdateColumn)
              .EqualTo(value)
              .Where(CompanyUserOrder.OrderGuidColumn)
              .IsEqualTo(orderGuid)
              .Execute();
        }

        /// <summary>
        /// 查詢companyUserOrder的資料
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public CompanyUserOrderCollection CompanyUserOrderGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CompanyUserOrder, CompanyUserOrderCollection>(1, -1, null, filter);
        }

        public CompanyUserOrder CompanyUserOrderGet(Guid orderGuid)
        {
            return DB.Get<CompanyUserOrder>(orderGuid);
        }

        #endregion CompanyUserOrder

        #region ViewCompanyUserOrder

        public ViewCompanyUserOrderCollection ViewCompanyUserOrderGetList(int pageNumber, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewCompanyUserOrder, ViewCompanyUserOrderCollection>(1, -1, null, filter);
        }

        /// <summary>
        /// 回傳符合條件時的資料筆數
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public int ViewCompanyUserOrderListGetCount(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewCompanyUserOrder.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewCompanyUserOrder.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select count(1) from " + ViewCompanyUserOrder.Schema.Provider.DelimitDbName(ViewCompanyUserOrder.Schema.TableName) + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion ViewCompanyUserOrder

        #region Corresponding Order

        public OrderCorrespondingCollection OrderCorrespondingGet(string relatedOrderId, int tokenClientId)
        {
            return DB.SelectAllColumnsFrom<OrderCorresponding>().NoLock()
                .Where(OrderCorresponding.Columns.TokenClientId).IsEqualTo(tokenClientId)
                .And(OrderCorresponding.Columns.RelatedOrderId).IsEqualTo(relatedOrderId)
                .ExecuteAsCollection<OrderCorrespondingCollection>();
        }

        public void OrderCorrespondingSet(OrderCorresponding oc)
        {
            DB.Save<OrderCorresponding>(oc);
        }

        public OrderCorrespondingCollection OrderCorrespondingListGet(string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<OrderCorresponding, OrderCorrespondingCollection>(1, -1, string.Empty, filter);
        }

        public OrderCorresponding OrderCorrespondingListGetByOrderGuid(Guid order_guid)
        {
            return DB.Get<OrderCorresponding>(OrderCorresponding.Columns.OrderGuid, order_guid);
        }

        public OrderCorrespondingCollection OrderCorrespondingGetByOrderGuid(Guid orderGuid, AgentChannel type)
        {
            return DB.SelectAllColumnsFrom<OrderCorresponding>().NoLock()
                .Where(OrderCorresponding.Columns.Type).IsEqualTo((int)type).And(OrderCorresponding.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<OrderCorrespondingCollection>();
        }

        public OrderCorrespondingCollection OrderCorrespondingCollectionListGetByOrderGuidList(List<Guid> orderGuids)
        {
            return DB.SelectAllColumnsFrom<OrderCorresponding>().Where(OrderCorresponding.Columns.OrderGuid).In(orderGuids)
                .ExecuteAsCollection<OrderCorrespondingCollection>();
        }

        public OrderCorrespondingCollection OrderCorrespondingCollectionListGetByOrderGuidListAndClassification(List<Guid> orderGuids, OrderClassification type)
        {
            return DB.SelectAllColumnsFrom<OrderCorresponding>().Where(OrderCorresponding.Columns.Type).IsEqualTo((int)type)
                .And(OrderCorresponding.Columns.OrderGuid).In(orderGuids).ExecuteAsCollection<OrderCorrespondingCollection>();
        }

        public OrderCorrespondingCollection OrderCorrespondingGetByOrderCheck(int type, int orderCheck)
        {
            return DB.SelectAllColumnsFrom<OrderCorresponding>().NoLock()
                .Where(OrderCorresponding.Columns.Type).IsEqualTo(type)
                .And(OrderCorresponding.Columns.OrderCheck).IsEqualTo(orderCheck)
                .And(OrderCorresponding.Columns.CreatedTime).IsLessThan(DateTime.Now.AddMinutes(-5))
                .ExecuteAsCollection<OrderCorrespondingCollection>();
        }

        public int ViewOrderCorrespondingSmsLogListGetCount(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewCompanyUserOrder.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewOrderCorrespondingSmsLog.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select count(1) from " + ViewOrderCorrespondingSmsLog.Schema.Provider.DelimitDbName(ViewOrderCorrespondingSmsLog.Schema.TableName) + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public int ViewOrderCorrespondingSmsLogListGetCount(Guid business_hour_guid, params string[] filter)
        {
            string sql = @"SELECT count(1)
                            FROM order_corresponding o with(nolock)
                            INNER JOIN cash_trust_log c with(nolock) ON o.order_guid=c.order_guid
                            LEFT JOIN (SELECT coupon_id,status,create_time FROM sms_log s with(nolock)
                            INNER JOIN (SELECT MAX(id) AS id
                            FROM sms_log with(nolock) where type=0 and business_hour_guid=@bid
                            GROUP BY coupon_id) ss ON s.id=ss.id where s.type=0 and  s.business_hour_guid=@bid) sm ON c.coupon_id=sm.coupon_id
                            where c.order_classification=1  ";
            if (filter.Length > 0)
            {
                sql += " and " + string.Join(" and ", filter);
            }
            QueryCommand qc = new QueryCommand(sql, ViewOrderCorrespondingSmsLog.Schema.Provider.Name);
            qc.AddParameter("@bid", business_hour_guid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetList(int pageNumber, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewOrderCorrespondingSmsLog, ViewOrderCorrespondingSmsLogCollection>(pageNumber, pageSize, orderBy, filter);
        }

        public ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetList(int pageNumber, int pageSize, string orderBy, Guid business_hour_guid, params string[] filter)
        {
            string sql = @"SELECT o.*,sm.status AS sms_status,sm.create_time AS sms_create_time ,c.status AS trust_status,i.item_name
                            FROM order_corresponding o with(nolock)
                            INNER JOIN cash_trust_log c with(nolock) ON o.order_guid=c.order_guid
                            INNER JOIN item i with(nolock) ON i.business_hour_guid=o.business_hour_guid
                            LEFT JOIN (SELECT coupon_id,status,create_time FROM sms_log s with(nolock)
                            INNER JOIN (SELECT MAX(id) AS id
                            FROM sms_log with(nolock) where type=0 and business_hour_guid=@bid
                            GROUP BY coupon_id) ss ON s.id=ss.id where s.type=0 and  s.business_hour_guid=@bid) sm ON c.coupon_id=sm.coupon_id
                            where c.order_classification=1  ";
            if (filter.Length > 0)
            {
                sql += " and " + string.Join(" and ", filter);
            }
            QueryCommand qc = new QueryCommand(sql, ViewOrderCorrespondingSmsLog.Schema.Provider.Name);
            qc.AddParameter("@bid", business_hour_guid, DbType.Guid);
            qc = SSHelper.MakePagable(qc, pageNumber, pageSize, "sm.create_time desc");
            ViewOrderCorrespondingSmsLogCollection data = new ViewOrderCorrespondingSmsLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewOrderCorrespondingSmsLogCollection ViewOrderCorrespondingSmsLogGetFailList()
        {
            string sql = @"SELECT o.*,sm.status AS sms_status,sm.create_time AS sms_create_time ,c.status AS trust_status,i.item_name
                            FROM order_corresponding o with(nolock)
                            INNER JOIN cash_trust_log c with(nolock) ON o.order_guid=c.order_guid
                            INNER JOIN item i with(nolock) ON i.business_hour_guid=o.business_hour_guid
                            LEFT JOIN (SELECT coupon_id,status,create_time FROM sms_log s with(nolock)
                            INNER JOIN (SELECT MAX(id) AS id
                            FROM sms_log with(nolock) where type=0 and status=1
                            GROUP BY coupon_id) ss ON s.id=ss.id where s.type=0 and  s.status=1) sm ON c.coupon_id=sm.coupon_id
                            where c.order_classification=1 and sm.status=1 and sm.create_time>=@date_start and sm.create_time<=@date_end order by sm.create_time desc";
            QueryCommand qc = new QueryCommand(sql, ViewOrderCorrespondingSmsLog.Schema.Provider.Name);
            qc.AddParameter("@date_start", DateTime.Today.AddDays(-3), DbType.DateTime);
            qc.AddParameter("@date_end", DateTime.Today.AddDays(1), DbType.DateTime);
            ViewOrderCorrespondingSmsLogCollection data = new ViewOrderCorrespondingSmsLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewLiontravelCouponListMainCollection ViewLionTravelCouponListMainGetList(int pageNumber, int pageSize, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewLiontravelCouponListMain, ViewLiontravelCouponListMainCollection>(pageNumber, pageSize, ViewLiontravelCouponListMain.Columns.Id, false, filter);
        }

        public int ViewLionTravelCouponListMainGetCount(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewLiontravelCouponListMain.Schema, filter);
            qc.CommandSql = "select count(1) from " + ViewLiontravelCouponListMain.Schema.TableName + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewOrderCorrespondingCouponCollection ViewOrderCorrespondingCouponGetList(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewOrderCorrespondingCoupon.Schema, filter);
            qc.CommandSql = "select * from " + ViewOrderCorrespondingCoupon.Schema.TableName + " with(nolock) " + qc.CommandSql;
            ViewOrderCorrespondingCouponCollection data = new ViewOrderCorrespondingCouponCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewLiontravelOrderReturnFormListCollection ViewLiontravelOrderReturnFormListGetList(int pageNumber, int pageSize, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewLiontravelOrderReturnFormList, ViewLiontravelOrderReturnFormListCollection>(pageNumber, pageSize, ViewLiontravelOrderReturnFormList.Columns.ReturnApplicationTime, false, filter);
        }

        public int ViewLiontravelOrderReturnFormListGetCount(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewLiontravelOrderReturnFormList.Schema, filter);
            qc.CommandSql = "select count(1) from " + ViewLiontravelOrderReturnFormList.Schema.TableName + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion

        #region MasterPass
        public void MasterPassAdd(MasterpassLog masterpass_log)
        {
            DB.Save<MasterpassLog>(masterpass_log);
        }
        public void MasterPassTempAdd(MasterpassLogTemp masterpassLogTemp)
        {
            DB.Save<MasterpassLogTemp>(masterpassLogTemp);
        }
        public bool MasterPassGetByOrderGuid(Guid order_guid)
        {
            List<Guid> orderGuid = DB.Select(MasterpassLog.Columns.OrderGuid)
                .From<MasterpassLog>().NoLock()
                .Where(MasterpassLog.Columns.OrderGuid)
                .IsEqualTo(order_guid)
                .ExecuteTypedList<Guid>() ?? new List<Guid>();
            return orderGuid.Any();
        }

        public MasterpassLogCollection MasterPassGetList(DateTime dateStart, DateTime dateEnd)
        {
            string sql = @" SELECT * FROM " + MasterpassLog.Schema.TableName + @" m WITH(NOLOCK)
                            JOIN [" + Order.Schema.TableName + @"] o WITH(NOLOCK) ON o.guid = m.order_guid
                            WHERE o." + Order.Columns.CreateTime + @" >= @dateStart
                            AND o." + Order.Columns.CreateTime + @" < @dateEnd
                            AND o." + Order.Columns.OrderStatus + @" & " + (int)OrderStatus.Complete + @" > 0
                            AND o." + Order.Columns.OrderStatus + @" & " + (int)OrderStatus.Cancel + @" = 0 ";

            QueryCommand qc = new QueryCommand(sql, MasterpassLog.Schema.Provider.Name);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            MasterpassLogCollection data = new MasterpassLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public MasterpassLogCollection MasterPassUnencryptGetList(int count)
        {
            string sql = @" SELECT Top (@count) * FROM " + MasterpassLog.Schema.TableName + @" WITH(NOLOCK)                           
                            WHERE len(" + MasterpassLog.Columns.AccountNumber + @") = 16
                            AND [id] not in (SELECT log_id FROM " + MasterpassLogTemp.Schema.TableName + @" WITH(NOLOCK))";

            QueryCommand qc = new QueryCommand(sql, MasterpassLog.Schema.Provider.Name);
            qc.AddParameter("@count", count, DbType.Int32);
            MasterpassLogCollection data = new MasterpassLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public MasterpassPreCheckoutToken MasterPassPreCheckoutTokenGet(int userId)
        {
            return DB.Get<MasterpassPreCheckoutToken>(userId);
        }

        public bool MasterPassPreCheckoutTokenUpdateUsed(int userId)
        {
            const string sql = @"UPDATE masterpass_pre_checkout_token SET is_used = 1 WHERE user_id = @user_id";
            QueryCommand qc = new QueryCommand(sql, MasterpassPreCheckoutToken.Schema.Provider.Name);
            qc.AddParameter("@user_id", userId, DbType.Int32);
            DataService.ExecuteScalar(qc);
            return true;
        }

        public bool MasterPassPreCheckoutTokenUpdateAccessToken(int userId, string accessToken, int spentSec)
        {
            const string sql = @"UPDATE masterpass_pre_checkout_token SET access_token = @access_token, is_used=0, modify_time=getdate(), spent_sec=@spentSec
                        WHERE user_id = @user_id";
            QueryCommand qc = new QueryCommand(sql, MasterpassPreCheckoutToken.Schema.Provider.Name);
            qc.AddParameter("@access_token", accessToken, DbType.String);
            qc.AddParameter("@user_id", userId, DbType.Int32);
            qc.AddParameter("@spentSec", spentSec, DbType.Int32);
            DataService.ExecuteScalar(qc);
            return true;
        }

        public bool MasterPassPreCheckoutTokenUpdateFailReason(int userId, string reason)
        {
            return DB.Update<MasterpassPreCheckoutToken>().Set(MasterpassPreCheckoutToken.FailReasonColumn)
                .EqualTo(reason).Set(MasterpassPreCheckoutToken.IsUsedColumn).EqualTo(true)
                .Where(MasterpassPreCheckoutToken.UserIdColumn).IsEqualTo(userId).Execute() > 0;
        }

        public bool MasterPassPreCheckoutTokenSet(MasterpassPreCheckoutToken token)
        {
            token.ModifyTime = DateTime.Now;
            return DB.Save(token) > 0;
        }

        public bool MasterpassCardInfoLogNew(MasterpassCardInfoLog cardInfo)
        {
            cardInfo.CreateTime = DateTime.Now;
            return DB.Save(cardInfo) > 0;
        }

        public bool MasterpassCardInfoLogSetUsed(int verifierCode)
        {
            return DB.Update<MasterpassCardInfoLog>().Set(MasterpassCardInfoLog.Columns.UsedTime)
                .EqualTo(DateTime.Now).Set(MasterpassCardInfoLog.Columns.IsUsed).EqualTo(true)
                .Where(MasterpassCardInfoLog.Columns.VerifierCode)
                .IsEqualTo(verifierCode).Execute() > 0;
        }

        public MasterpassCardInfoLog MasterpassCardInfoLogGet(int verifierCode, string authToken)
        {
            var cardInfo = DB.Get<MasterpassCardInfoLog>(verifierCode);
            if (cardInfo.IsLoaded && cardInfo.AuthToken == authToken)
            {
                return cardInfo;
            }
            return new MasterpassCardInfoLog();
        }

        #endregion

        #region shopping_cart_item_options
        public ShoppingCartItemOptionCollection ShoppingCartItemOptionGetByUid(int uid)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartItemOption>()
                .Where(ShoppingCartItemOption.IsRemoveColumn).IsEqualTo(false)
                .And(ShoppingCartItemOption.UserIdColumn).IsEqualTo(uid)
                .ExecuteAsCollection<ShoppingCartItemOptionCollection>();
        }
        public ShoppingCartItemOptionCollection ShoppingCartItemOptionGetByTicketid(string ticketId)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartItemOption>()
                .Where(ShoppingCartItemOption.IsRemoveColumn).IsEqualTo(false)
                .And(ShoppingCartItemOption.TicketIdColumn).IsEqualTo(ticketId)
                .ExecuteAsCollection<ShoppingCartItemOptionCollection>();
        }
        public bool ShoppingCartItemOptionSet(ShoppingCartItemOption item)
        {
            return DB.Save(item) > 0;
        }
        #endregion shopping_cart_item_options

        #region view_ichannel_info

        public ViewIchannelInfoCollection ViewIchannelInfoGetList(DateTime dateStart, DateTime dateEnd)
        {
            string sql = @" select * from " + ViewIchannelInfo.Schema.TableName + @" with(nolock)
                            where " + ViewIchannelInfo.Columns.OrderTime + @" >= @startTime
                            and " + ViewIchannelInfo.Columns.OrderTime + @" <= @endTime
                            and " + ViewIchannelInfo.Columns.OrderStatus + @" & 8 > 0
                            and ((first_rsrc is null and order_time not between '2016/5/16 00:00:00' and '2017/6/19 06:00:00') or 
	                            (order_time < '2018/09/01' and first_rsrc = 'Oeya_ichannels') or 
	                            (order_time >= '2018/09/01' and last_external_rsrc = 'Oeya_ichannels'))";
            //order_status & 8 > 0 完成單, send_type = 1為回傳成功, 20160516~20170619間若rsrc is null不列入輸出
            if (!config.EnableCommissionRule)
            {
                sql += @" and  ((" + ViewIchannelInfo.Columns.ItemName + @" not like N'%王品集團餐廳%') 
                            or (" + ViewIchannelInfo.Columns.ItemName + @" like N'%王品集團餐廳%' and " + ViewIchannelInfo.Columns.OrderTime + @" <='2015/12/04'))";
                //排除分類票券類及名稱有王品集團餐廳的12/4日之後的訂單，避免分潤"
            }

            QueryCommand qc = new QueryCommand(sql, ViewIchannelInfo.Schema.Provider.Name);
            qc.AddParameter("startTime", dateStart, DbType.DateTime);
            qc.AddParameter("endTime", dateEnd, DbType.DateTime);

            ViewIchannelInfoCollection data = new ViewIchannelInfoCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewIchannelInfoCollection ViewIchannelInfoGet(Guid guid)
        {
            string sql = @" select * from " + ViewIchannelInfo.Schema.TableName + @" with(nolock)
                            where " + ViewIchannelInfo.Columns.OrderGuid + @" = @guid";

            QueryCommand qc = new QueryCommand(sql, ViewIchannelInfo.Schema.Provider.Name);
            qc.AddParameter("guid", guid, DbType.Guid);


            ViewIchannelInfoCollection data = new ViewIchannelInfoCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region LinePay

        /// <summary>
        /// Line Pay退貨 Save
        /// </summary>
        /// <param name="refundLog"></param>
        /// <param name="userId">0:sys ~ user_id</param>
        /// <returns></returns>
        public LinePayRefundLog LinePayRefundLogSet(LinePayRefundLog refundLog, string userId = "sys")
        {
            refundLog.ModifyTime = DateTime.Now;
            refundLog.ModifyUserId = userId;
            DB.Save(refundLog);
            return refundLog;
        }

        public LinePayRefundLogCollection LinePayRefundLogGetRetryData()
        {
            var sql = string.Format(@"SELECT * FROM line_pay_refund_log
                    WHERE refund_status = @status
                    AND can_retry = 1
                    AND DATEADD(mi, 20, modify_time) < getdate()
                    AND retry_time<{0}", config.LinePayApiRetryTimes);

            QueryCommand qc = new QueryCommand(sql, LinePayRefundLog.Schema.Provider.Name);
            qc.AddParameter("@status", (byte)LinePayRefundStatus.RequestedFail, DbType.Byte);

            var data = new LinePayRefundLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public LinePayTransLog LinePayTransLogSet(LinePayTransLog trans)
        {
            trans.ModifyTime = DateTime.Now;
            DB.Save(trans);
            return trans;
        }

        public LinePayTransLog LinePayTransLogGet(int id)
        {
            return DB.Get<LinePayTransLog>(id);
        }

        public LinePayTransLog LinePayTransLogGet(string ticketId)
        {
            return DB.Get<LinePayTransLog>(LinePayTransLog.Columns.TicketId, ticketId);
        }

        public LinePayTransLog LinePayTransLogGetByTransId(string transId)
        {
            return DB.Get<LinePayTransLog>(LinePayTransLog.Columns.TransactionId, transId);
        }

        public int LinePayTransLogIdGetIdByTransId(string transId)
        {
            const string sql = "SELECT id FROM line_pay_trans_log WHERE transaction_id = @transId";
            var qc = new QueryCommand(sql, LinePayTransLog.Schema.Provider.Name);
            qc.AddParameter("@transId", transId, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public bool LinePayApiLogSet(LinePayApiLog data)
        {
            DB.Save(data);
            return true;
        }

        public LinePayTransLogCollection LinePayTransLogGetRetryData(LinePayTransStatus status, string checkColumn, int retryLimit)
        {
            var sql = string.Format(@"SELECT * FROM line_pay_trans_log
                    WHERE trans_status = @status
                    AND can_retry = 1
                    AND DATEADD(mi, 20, modify_time) < getdate()
                    AND {0}<@retryLimit", checkColumn);

            QueryCommand qc = new QueryCommand(sql, LinePayTransLog.Schema.Provider.Name);
            qc.AddParameter("@status", (byte)status, DbType.Byte);
            qc.AddParameter("@retryLimit", retryLimit, DbType.Int32);
            var data = new LinePayTransLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion LinePay

        #region ThirdPartyPay

        public ThirdPartyPayTransLog ThirdPartyPayTransLogGetById(int id)
        {
            return DB.Get<ThirdPartyPayTransLog>(id);
        }

        public ThirdPartyPayTransLog ThirdPartyPayTransLogGet(PaymentAPIProvider paymentOrg, int userId, string orderId)
        {
            return DB.SelectAllColumnsFrom<ThirdPartyPayTransLog>()
                .Where(ThirdPartyPayTransLog.PaymentOrgColumn).IsEqualTo((int)paymentOrg)
                .And(ThirdPartyPayTransLog.UserIdColumn).IsEqualTo(userId)
                .And(ThirdPartyPayTransLog.OrderIdColumn).IsEqualTo(orderId)
                .ExecuteSingle<ThirdPartyPayTransLog>();
        }

        public ThirdPartyPayTransLog ThirdPartyPayTransLogGetByOrgTransId(PaymentAPIProvider paymentOrg, string transId)
        {
            return DB.SelectAllColumnsFrom<ThirdPartyPayTransLog>()
                .Where(ThirdPartyPayTransLog.PaymentOrgColumn).IsEqualTo((int)paymentOrg)
                .And(ThirdPartyPayTransLog.TransIdColumn).IsEqualTo(transId)
                .ExecuteSingle<ThirdPartyPayTransLog>();
        }

        public ThirdPartyPayTransLog ThirdPartyPayTransLogGetByTransId(string transId)
        {
            return DB.QueryOver<ThirdPartyPayTransLog>()
                .FirstOrDefault(t => t.TransId == transId);
        }

        public int ThirdPartyPayTransLogSet(ThirdPartyPayTransLog data)
        {
            data.ModifyTime = DateTime.Now;
            return DB.Save(data);
        }

        public void ThirdPartyPayRefundLogSet(ThirdPartyPayRefundLog refundLog)
        {
            DB.Save(refundLog);
        }

        public void ThirdPartyPayApiLogSet(ThirdPartyPayApiLog apiLog)
        {
            apiLog.CreateTime = DateTime.Now;
            DB.Save(apiLog);
        }

        #endregion

        #region shopping_cart_group
        public ShoppingCartGroupCollection ShoppingCartGroupGetByUid(int uid)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartGroup>()
                .Where(ShoppingCartGroup.CreateIdColumn).IsEqualTo(uid)
                .ExecuteAsCollection<ShoppingCartGroupCollection>();
        }
        public void ShoppingCartGroupSet(ShoppingCartGroup group)
        {
            DB.Save(group);
        }
        #endregion

        #region shopping_cart_group_d
        public ShoppingCartGroupDCollection ShoppingCartGroupDGetByCartGuid(Guid cid)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartGroupD>()
                .Where(ShoppingCartGroupD.CartGuidColumn).IsEqualTo(cid)
                .ExecuteAsCollection<ShoppingCartGroupDCollection>();
        }
        public void ShoppingCartGroupDSet(ShoppingCartGroupD groupD)
        {
            DB.Save(groupD);
        }
        #endregion

        #region PreventingOversell

        public PreventingOversell AddPreventingOversell(PreventingOversell entity)
        {
            entity.CreateTime = entity.ModifyTime = DateTime.Now;
            using (var db = new OrderDbContext())
            {
                db.PreventingOversellEntities.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public int GetPreventingOversellProcessingQty(Guid businessHourGuid)
        {
            DateTime timeoutPoint = DateTime.Now.AddSeconds(-config.PreventingOversellTimeoutSec);

            using (var db = new OrderDbContext())
            {
                var data = (from t in db.PreventingOversellEntities
                            where t.Bid == businessHourGuid && t.Processing && t.CreateTime > timeoutPoint
                            select t).ToList();
                return data.Any() ? data.Sum(x => x.OrderQty) : 0;
            }
        }

        public bool SetPreventingOversellExpired(Guid bid, int qty, int userId)
        {
            var sql = @"UPDATE preventing_oversell SET Processing = 0, modify_time = GETDATE() WHERE
                        id in (SELECT TOP 1 id FROM preventing_oversell WHERE bid=@businessHourGuid AND user_id=@USERID AND order_qty=@QTY AND processing=1 order by id)";

            List<SqlParameter> para = new List<SqlParameter>();
            para.Add(new SqlParameter("@businessHourGuid", bid.ToString()));
            para.Add(new SqlParameter("@USERID", userId));
            para.Add(new SqlParameter("@QTY", qty));

            using (var db = new OrderDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, para.ToArray()) > 0;
            }
        }

        public bool SetPreventingOversellExpired(int pid)
        {
            if (pid == 0)
            {
                return false;
            }

            var sql = @"Update preventing_oversell set processing = 0, modify_time = getdate() where id = @pid";

            List<SqlParameter> para = new List<SqlParameter>();
            para.Add(new SqlParameter("@pid", pid));

            using (var db = new OrderDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, para.ToArray()) > 0;
            }
        }

        #endregion

        #region ExpiredVerifiedLog
        public void ExpiredVerifiedLogSet(ExpiredVerifiedLog log)
        {
            DB.Save(log);
        }
        #endregion ExpiredVerifiedLog

        #region blog_referrer_url
        public void BlogReferrerUrlSet(BlogReferrerUrl url)
        {
            DB.Save(url);
        }
        #endregion blog_referrer_url

        #region OrderLog

        public void OrderLogSet(OrderLog log)
        {
            DB.Save(log);
        }

        public OrderLog OrderLogGet(int id)
        {
            return DB.Get<OrderLog>(id);
        }

        #endregion OrderLog

        public IEnumerable<Guid> GetOldOrderWithNoUninvoiceAmount(int? count)
        {
            var sql = string.Format(@"SELECT DISTINCT {0} order_guid
FROM cash_trust_log AS ctl WITH(NOLOCK)
CROSS APPLY ( SELECT guid from [order] AS od WITH(NOLOCK) WHERE od.GUID = ctl.order_guid AND od.order_status & 8 > 0) AS od
WHERE credit_card +atm + tcash > 0
AND uninvoiced_amount = 0", string.Format("TOP {0}", count.GetValueOrDefault(100)));

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            var dt = DataService.GetDataSet(qc).Tables[0];
            var result = new List<Guid>();

            foreach (var dr in dt.AsEnumerable())
            {
                result.Add(dr.Field<Guid>("order_guid"));
            }

            return result;
        }

        #region TaishinEcPayLog
        public void TaishinEcPayLogSet(TaishinEcPayLog log)
        {
            DB.Save(log);
        }
        #endregion TaishinEcPayLog

        #region 超取 ViewIspMakeOrderInfoCollection
        public ViewIspMakeOrderInfoCollection GetViewIspMakeOrderInfo(List<Guid> orderGuidList)
        {
            return DB.SelectAllColumnsFrom<ViewIspMakeOrderInfo>()
                .Where(ViewIspMakeOrderInfo.Columns.OrderGuid).In(orderGuidList)
                .NoLock()
                .ExecuteAsCollection<ViewIspMakeOrderInfoCollection>();
        }

        public ViewIspMakeOrderInfoCollection GetViewIspMakeOrderInfoCollection()
        {
            return DB.SelectAllColumnsFrom<ViewIspMakeOrderInfo>()
                .NoLock()
                .ExecuteAsCollection<ViewIspMakeOrderInfoCollection>();
        }
        #endregion

        #region 疑似盜刷資料

        public void CreditcardFraudDataBulkInsert(CreditcardFraudDatumCollection cfdc)
        {
            DB.BulkInsert(cfdc);
        }

        public void CreditcardFraudDataSet(CreditcardFraudDatum cfd)
        {
            DB.Save(cfd);
        }

        /// <summary>
        /// 查詢疑似盜刷資料
        /// </summary>
        /// <param name="addressIsEmpty">地址空值是否也要比對</param>
        /// <param name="ip">ip</param>
        /// <param name="address">地址</param>
        /// <param name="cardNumber">卡號</param>
        /// <returns></returns>
        public CreditcardFraudDatum CreditcardFraudDataGet(bool addressIsEmpty, string ip, string address, string cardNumber, int? userId, string type)
        {
            string sql = "select * from " + CreditcardFraudDatum.Schema.TableName + " where 1=1";

            if (ip != "")
            {
                sql += " and " + CreditcardFraudDatum.Columns.Ip + "=@ip";
            }

            if (address != "" || addressIsEmpty)
            {
                sql += " and " + CreditcardFraudDatum.Columns.Address + "=@address";
            }

            if (cardNumber != "")
            {
                sql += " and " + CreditcardFraudDatum.Columns.CardNumber + "=@cardNumber";
            }

            if (userId != null)
            {
                sql += " and " + CreditcardFraudDatum.Columns.UserId + "=@userId";
            }

            List<string> typeList = type.Split(",").ToList();
            if (type != "")
            {
                sql += " and " + CreditcardFraudDatum.Columns.Type + " in (";
                for (int i = 0; i < typeList.Count(); i++)
                {
                    sql += "@type" + i.ToString() + ",";
                }
                sql = sql.TrimEnd(",") + ")";
            }


            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@ip", ip, DbType.String);
            qc.AddParameter("@address", address, DbType.String);
            qc.AddParameter("@cardNumber", cardNumber, DbType.String);
            qc.AddParameter("@userId", userId, DbType.Int32);
            for (int i = 0; i < typeList.Count(); i++)
            {
                qc.AddParameter("@type" + i.ToString(), typeList[i], DbType.String);
            }

            CreditcardFraudDatum cfd = new CreditcardFraudDatum();
            cfd.LoadAndCloseReader(DataService.GetReader(qc));
            return cfd;
        }

        public CreditcardFraudDatumCollection CreditcardFraudDataGet(CreditcardFraudDataType type)
        {
            return DB.SelectAllColumnsFrom<CreditcardFraudDatum>()
                .Where(CreditcardFraudDatum.Columns.Type).IsEqualTo(type)
                .ExecuteAsCollection<CreditcardFraudDatumCollection>();
        }

        public CreditcardFraudDatum CreditcardFraudDataGetById(string id)
        {
            return DB.SelectAllColumnsFrom<CreditcardFraudDatum>()
                .Where(CreditcardFraudDatum.Columns.Id).IsEqualTo(id)
                .ExecuteSingle<CreditcardFraudDatum>();
        }

        public CreditcardFraudDatumCollection CreditcardFraudDataGet(DateTime? sDataTime, DateTime? eDataTime)
        {
            if (sDataTime != null && eDataTime != null)
            {
                string sql = "select * from " + CreditcardFraudDatum.Schema.TableName + " where 1=1";

                sql += " and (" + CreditcardFraudDatum.Columns.RelateTime + ">@sDataTime and " + CreditcardFraudDatum.Columns.RelateTime + " < @eDataTime)";
                sql += " or " + CreditcardFraudDatum.Columns.RelateTime + " is null";


                QueryCommand qc = new QueryCommand(sql, CreditcardFraudDatum.Schema.Provider.Name);
                qc.AddParameter("@sDataTime", sDataTime, DbType.DateTime);
                qc.AddParameter("@eDataTime", eDataTime, DbType.DateTime);
                var data = new CreditcardFraudDatumCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));
                return data;
            }
            else
            {
                return DB.SelectAllColumnsFrom<CreditcardFraudDatum>()
                .ExecuteAsCollection<CreditcardFraudDatumCollection>();
            }

        }

        public CreditcardFraudDatumCollection CreditcardFraudDataGetUserId(DateTime sDataTime, DateTime eDataTime)
        {
            return DB.SelectAllColumnsFrom<CreditcardFraudDatum>()
                    .Where(CreditcardFraudDatum.Columns.CreateTime).IsGreaterThan(sDataTime)
                    .And(CreditcardFraudDatum.Columns.CreateTime).IsLessThan(eDataTime)
                    .And(CreditcardFraudDatum.Columns.UserId).IsNotNull()
                    .And(CreditcardFraudDatum.Columns.Type).IsEqualTo(CreditcardFraudDataType.CustomerCare)
                .ExecuteAsCollection<CreditcardFraudDatumCollection>();

        }

        public CreditcardFraudDatumCollection CreditcardFraudDataGetIp(DateTime sDataTime, DateTime eDataTime)
        {
            return DB.SelectAllColumnsFrom<CreditcardFraudDatum>()
                    .Where(CreditcardFraudDatum.Columns.CreateTime).IsGreaterThan(sDataTime)
                    .And(CreditcardFraudDatum.Columns.CreateTime).IsLessThan(eDataTime)
                    .And(CreditcardFraudDatum.Columns.Ip).IsNotNull()
                    .And(CreditcardFraudDatum.Columns.Ip).IsNotEqualTo("")
                    .And(CreditcardFraudDatum.Columns.Type).IsEqualTo(CreditcardFraudDataType.CustomerCare)
                .ExecuteAsCollection<CreditcardFraudDatumCollection>();

        }



        public DataTable CreditcardFraudDataGetByUserId(int userId)
        {
            string sql = string.Format(@"SELECT DISTINCT source_ip,
                                                        user_id,
                                                        Max(a.create_time) AS create_time,
                                                        m.status &" + (int)MemberStatusFlag.Disabled + @" AS 'black'
                                        FROM   account_audit a WITH(nolock) INNER JOIN member m WITH(nolock) ON a.user_id = m.unique_id
                                        WHERE  source_ip IN(SELECT source_ip
                                                            FROM   account_audit WITH(nolock)
                                                            WHERE  user_id = @userId AND source_ip <> '60.251.154.220')
                                        GROUP  BY source_ip, user_id, m.status ");

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, userId))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable CreditcardFraudDataGetByErrorCard(DateTime sDataTime, DateTime eDataTime)
        {
            string sql = string.Format(@"SELECT c1.id,
                                                c1.member_id,
                                                c1.consumer_ip,
                                                c1.create_time
                                        FROM   creditcard_order c1 WITH(nolock),
                                                (SELECT Max(id) AS minId,
                                                        member_id
                                                FROM   creditcard_order c3 WITH(nolock)
                                                WHERE  order_guid != '00000000-0000-0000-0000-000000000000'
                                                        AND (result =7 or result=12 or result=41 or result=43 or result=10000008 or result=10000009)
                                                        AND member_id != 0
                                                        AND c3.create_time > '" + sDataTime.ToString("yyyy-MM-dd") + "' AND c3.create_time < '" + eDataTime.ToString("yyyy-MM-dd") + @"'
                                                GROUP  BY member_id) c2,
                                                member  m WITH(nolock)
                                        WHERE  c1.id = c2.minId and c1.member_id=m.unique_id
                                        AND c1.consumer_ip!='60.251.154.220' 
                                        AND m.user_name NOT LIKE '%@17life.com%'
                                        AND m.status&" + (int)MemberStatusFlag.Disabled + @"= 0
                                        AND NOT EXISTS (SELECT user_id
                                                       FROM   creditcard_fraud_data c3 WITH(nolock)
                                                       WHERE  type in (3,4,5) and c1.member_id = c3.user_id)");


            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql))
            {
                dt.Load(idr);
            }
            return dt;

        }

        public DataTable CreditcardFraudDataGetByCreateIp(string ip)
        {
            string sql = string.Format(@"SELECT  distinct id,user_id,max(a.create_time) as create_time
                                        FROM   account_audit a WITH(nolock)  
										INNER JOIN member m WITH(nolock) on a.user_id=m.unique_id
                                        WHERE  memo LIKE N'%建立%'
                                               AND action IN ( 0, 1, 2, 13 ) AND source_ip = @ip 
                                        AND m.status&" + (int)MemberStatusFlag.Disabled + @"= 0 
                                        group by id,user_id");

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, ip))
            {
                dt.Load(idr);
            }
            return dt;

        }

        #endregion


        public CreditcardOtpWhitelistCollection CreditcardOTPWhiteList()
        {
            //白名單條件
            //1.近一年 信用卡交易付款成功訂單量(含退貨) 超過3張
            //2.歷史帳戶裡有10筆信用卡成功訂單
            //3.沒有使用過遺失卡(41)、被竊卡(43)、掛失卡(7)
            //4.會員狀態為正常

            string sql = @"
                            select co2.member_id as user_id,1 as status,GETDATE() as create_time,null as modify_time from creditcard_order co2 with(nolock)
                            inner join [order] o2 with(nolock) on co2.order_guid=o2.guid
                            inner join 
                            (
	                            select co.member_id from creditcard_order co with(nolock)
	                            inner join [order] o with(nolock) on co.order_guid=o.guid
	                            where  o.create_time>DATEADD(year,-1,getdate()) and o.order_status&8>0
	                            group by co.member_id
	                            having count(1)>3
                            ) as co3 on co3.member_id=co2.member_id
                            inner join member m with(nolock) on co2.member_id=m.unique_id
                            left join creditcard_fraud_data cfd with(nolock) on isnull(cfd.user_id,0)=m.unique_id and type=1
                            where  o2.order_status&8>0
                            and m.is_approved=1
                            and m.unique_id not in (select member_id from creditcard_order with(nolock) where result in (7,41,43) or is_locked=1)
                            and cfd.id is null 
                            group by co2.member_id
                            having  count(1)>10";


            QueryCommand qc = new QueryCommand(sql, CreditcardOtpWhitelist.Schema.Provider.Name);
            qc.CommandTimeout = 600;
            CreditcardOtpWhitelistCollection data = new CreditcardOtpWhitelistCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;

        }


        public void CreditcardOtpWhitelistBulkInsert(CreditcardOtpWhitelistCollection cowc)
        {
            DB.BulkInsert(cowc);
        }


        public int DeleteExcludeWhiteList()
        {
            string sql = @"delete creditcard_otp_whitelist where user_id in 
                            (
	                            select distinct o.user_id from [order] o with(nolock)
	                            where  exists (select * from creditcard_fraud_data cfd with(nolock) where cfd.address!='' and cfd.address =o.delivery_address)
	                            group by o.user_id

	                            union 
	                            select unique_id from member m with(nolock)
	                            where  exists (select * from creditcard_fraud_data cfd with(nolock) where cfd.address!='' and cfd.address =m.company_address)
                            )
                            SELECT @@ROWCOUNT";
            QueryCommand qc = new QueryCommand(sql, CreditcardOtpWhitelist.Schema.Provider.Name);
            qc.CommandTimeout = 600;
            return (int)DataService.ExecuteScalar(qc);
        }

        public void TruncateCreditcardOtpWhitelist()
        {
            string sql = "truncate table creditcard_otp_whitelist";
            var qc = new QueryCommand(sql, CreditcardOtpWhitelist.Schema.Provider.Name);
            DataService.ExecuteScalar(qc);
        }

        public CreditcardOtpWhitelist CreditcardOtpWhitelistGet(int userId)
        {
            return DB.SelectAllColumnsFrom<CreditcardOtpWhitelist>()
                .Where(CreditcardOtpWhitelist.Columns.UserId).IsEqualTo(userId)
                .ExecuteSingle<CreditcardOtpWhitelist>();
        }

        public void CreditcardOtpWhitelistSet(CreditcardOtpWhitelist cow)
        {
            DB.Save(cow);
        }

        #region OTPLog
        public void OtpLogSet(OtpLog log)
        {
            DB.Save(log);
        }

        public OtpLog OtpLogGet(int id)
        {
            return DB.Get<OtpLog>(id);
        }
        #endregion

        #region TempCreditcardOtp

        public TempCreditcardOtp TempCreditcardOtpGet(string transId)
        {
            return DB.SelectAllColumnsFrom<TempCreditcardOtp>()
                .Where(TempCreditcardOtp.Columns.TransId).IsEqualTo(transId)
                .ExecuteSingle<TempCreditcardOtp>();
        }

        public bool TempCreditcardOtpSet(TempCreditcardOtp otp)
        {
            DB.Save(otp);
            return true;
        }

        public bool TempCreditcardOtpDelete(int Id)
        {
            DB.Destroy<TempCreditcardOtp>(TempCreditcardOtp.Columns.Id, Id);
            return true;
        }
        #endregion

        public void CreditcardOtpSet(CreditcardOtp otp)
        {
            DB.Save(otp);
        }

        public CreditcardOtp CreditcardOtpGet(int userId,string creditcardInfo)
        {
            return DB.SelectAllColumnsFrom<CreditcardOtp>()
                .Where(CreditcardOtp.Columns.UserId).IsEqualTo(userId)
                .And(CreditcardOtp.Columns.CreditcardInfo).IsEqualTo(creditcardInfo)
                .ExecuteSingle<CreditcardOtp>();
        }

        /// <summary>
        /// 取得是否3個月內有驗證過OTP
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="creditcardInfo"></param>
        /// <returns></returns>
        public CreditcardOtp CreditcardOtpGetByTime(int userId, string creditcardInfo)
        {
            return DB.SelectAllColumnsFrom<CreditcardOtp>()
                .Where(CreditcardOtp.Columns.UserId).IsEqualTo(userId)
                .And(CreditcardOtp.Columns.CreditcardInfo).IsEqualTo(creditcardInfo)
                .And(CreditcardOtp.Columns.AuthTime).IsGreaterThan(DateTime.Now.AddMonths(-3))
                .ExecuteSingle<CreditcardOtp>();
        }

        #region Wms

        public void WmsOrderSet(WmsOrder wo)
        {
            DB.Save(wo);
        }

        #endregion


        #region 共用

        protected QueryCommand GetVpolWhereQC(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        public ShoppingCartGroup ShoppingCartGroupGet(Guid guid)
        {
            return DB.Get<ShoppingCartGroup>(ShoppingCartGroup.Columns.Guid, guid);
        }

        protected string GetEinvoiceAllowanceSqlWithStringFormatParm()
        {
            var sql = @"SELECT	m.id, m.trans_id, MAX(p.id) as payment_trans_id, MAX(coalesce(rf.modify_time, ctl.modify_time)) as trans_time, SUM(coalesce(rf.uninvoiced_amount, ctl.uninvoiced_amount)) as amount, 
		m.order_time, m.invoice_sum_amount, m.invoice_mode, m.invoice_number_time, m.order_amount, 
		m.invoice_number, m.order_is_pponitem, bh.business_hour_status, rf.return_form_id, 
        case m.invoice_mode2 when 1 then 1 else m.allowance_status end allowance_status,
        m.invoice_papered 
FROM	dbo.cash_trust_log AS ctl WITH(NOLOCK) 
INNER JOIN dbo.einvoice_main AS m WITH(NOLOCK) ON m.order_guid = ctl.order_guid AND m.coupon_id is null 
INNER JOIN dbo.[order] AS o with(nolock) on o.GUID = m.order_guid 
INNER JOIN dbo.group_order gpo with(nolock) on gpo.order_guid = o.parent_order_id 
INNER JOIN dbo.business_hour bh with(nolock) on bh.GUID = gpo.business_hour_guid 
OUTER APPLY
(select f.id as return_form_id, f.modify_time , ct.uninvoiced_amount
	from return_form as f with(nolock)
	inner join return_form_refund as r with(nolock) on r.return_form_id = f.id 
	inner join cash_trust_log as ct with(nolock) on ct.trust_id = r.trust_id
	where f.order_guid = m.order_guid
	and f.progress_status = 2 
	and r.is_refunded = 1 
	and ct.uninvoiced_amount > 0 
	and r.trust_id = ctl.trust_id 
) as rf
CROSS APPLY
(select Max(id) as id
	from payment_transaction with(nolock)
	where trans_id = m.trans_id 
	and (status & 15 = 3) and (trans_type = 2) 
	and ((ctl.status = 4 
	and (payment_type = 1 OR payment_type = 6 or payment_type = 10 or payment_type = 11 or payment_type = 12 or payment_type = 13)) 
	or (ctl.status = 3  
	and payment_type = 5)
	) 
	group by trans_id
) as p
OUTER APPLY
(select payment_trans_id 
	from dbo.einvoice_detail 
	where (payment_trans_id = p.id  
	or return_form_id = rf.return_form_id )
	and invoice_type IN (2, 3)
) as ed
WHERE ctl.status in (3,4) 
AND ctl.uninvoiced_amount > 0 
AND (m.invoice_status <> - 1)
AND m.invoice_number_time is not null 
{0} 
GROUP BY m.id, m.trans_id, m.order_time, m.invoice_sum_amount, m.invoice_mode, m.invoice_number_time, m.order_amount, 
		m.invoice_number, m.order_is_pponitem,bh.business_hour_status, rf.return_form_id, m.allowance_status, m.invoice_mode2,m.invoice_papered";
            return sql;
        }

        protected string GetEinvoiceAllowanceSqlWithStringFormatParmV2()
        {
            var sql = @"SELECT	m.id, m.trans_id, MAX(p.id) as payment_trans_id, MAX(coalesce(rf.modify_time, ctl.modify_time)) as trans_time, SUM(coalesce(rf.uninvoiced_amount, ctl.uninvoiced_amount)) as amount, 
		m.order_time, m.invoice_sum_amount, m.invoice_mode, m.invoice_number_time, m.order_amount, 
		m.invoice_number, m.order_is_pponitem, bh.business_hour_status, rf.return_form_id, 
        case m.invoice_mode2 when 1 then 1 else m.allowance_status end allowance_status,
        m.invoice_papered 
FROM	dbo.cash_trust_log AS ctl WITH(NOLOCK) 
INNER JOIN dbo.einvoice_main AS m WITH(NOLOCK) ON m.order_guid = ctl.order_guid AND m.coupon_id is null 
INNER JOIN dbo.[order] AS o with(nolock) on o.GUID = m.order_guid 
INNER JOIN dbo.group_order gpo with(nolock) on gpo.order_guid = o.parent_order_id 
INNER JOIN dbo.business_hour bh with(nolock) on bh.GUID = gpo.business_hour_guid 
OUTER APPLY
(select f.id as return_form_id, f.modify_time , ct.uninvoiced_amount
	from return_form as f with(nolock)
	inner join return_form_refund as r with(nolock) on r.return_form_id = f.id 
	inner join cash_trust_log as ct with(nolock) on ct.trust_id = r.trust_id
	where f.order_guid = m.order_guid
	and f.progress_status = 2 
	and r.is_refunded = 1 
	and ct.uninvoiced_amount > 0 
	and r.trust_id = ctl.trust_id 
) as rf
CROSS APPLY
(select Max(id) as id
	from payment_transaction with(nolock)
	where trans_id = m.trans_id 
	and (status & 15 = 3) and (trans_type = 2) 
	and ctl.status in (3,4) and payment_type = 5
	group by trans_id
) as p
OUTER APPLY
(select payment_trans_id 
	from dbo.einvoice_detail 
	where (payment_trans_id = p.id  
	or return_form_id = rf.return_form_id )
	and invoice_type IN (2, 3)
) as ed
WHERE ctl.status in (3,4) 
AND ctl.uninvoiced_amount > 0 
AND (m.invoice_status <> - 1)
AND m.invoice_number_time is not null 
{0} 
GROUP BY m.id, m.trans_id, m.order_time, m.invoice_sum_amount, m.invoice_mode, m.invoice_number_time, m.order_amount, 
		m.invoice_number, m.order_is_pponitem,bh.business_hour_status, rf.return_form_id, m.allowance_status, m.invoice_mode2,m.invoice_papered";
            return sql;
        }

        protected List<EinvoiceAllowanceInfo> GetEinvoiceAllowanceInfoFromQuery(QueryCommand qc)
        {
            return DataService.GetDataSet(qc).Tables[0].AsEnumerable()
                .Select(dr => new EinvoiceAllowanceInfo
                {
                    Id = dr.Field<int>("id"),
                    TransId = dr.Field<string>("trans_id"),
                    PaymentTransId = dr.Field<int>("payment_trans_id"),
                    TransTime = dr.Field<DateTime>("trans_time"),
                    Amount = dr.Field<int>("amount"),
                    OrderTime = dr.Field<DateTime>("order_time"),
                    InvoiceSumAmount = dr.Field<decimal>("invoice_sum_amount"),
                    InvoiceMode = dr.Field<int>("invoice_mode"),
                    InvoiceNumberTime = dr.Field<DateTime>("invoice_number_time"),
                    OrderAmount = dr.Field<decimal>("order_amount"),
                    InvoiceNumber = dr.Field<string>("invoice_number"),
                    OrderIsPponitem = dr.Field<bool>("order_is_pponitem"),
                    BusinessHourStatus = dr.Field<int>("business_hour_status"),
                    ReturnFormId = dr.Field<int?>("return_form_id"),
                    AllowanceStatus = dr.Field<int?>("allowance_status"),
                    InvoicePapered = dr.Field<bool>("invoice_papered")
                })
            .ToList();
        }

        #endregion 共用

        #region payeasy 導購

        public bool PezChannelSet(PezChannel payeasy)
        {
            DB.Save<PezChannel>(payeasy);
            return true;
        }

        const string payeasyRsrc = "Payeasy_intro";
        public ViewPezChannelCollection ViewPezChannelGetList(DateTime dateStart, DateTime dateEnd)
        {
            string sql = @" select * from " + ViewPezChannel.Schema.TableName + @" with(nolock)
                            where " + ViewPezChannel.Columns.OrderTime + @" >= @startTime
                            and " + ViewPezChannel.Columns.OrderTime + @" < @endTime
                            and " + ViewPezChannel.Columns.OrderStatus + @" & 8 > 0
                            and " + ViewPezChannel.Columns.LastExternalRsrc + @" = @payeasyRsrc";

            QueryCommand qc = new QueryCommand(sql, ViewPezChannel.Schema.Provider.Name);
            qc.AddParameter("startTime", dateStart, DbType.DateTime);
            qc.AddParameter("endTime", dateEnd, DbType.DateTime);
            qc.AddParameter("payeasyRsrc", payeasyRsrc, DbType.String);

            ViewPezChannelCollection data = new ViewPezChannelCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public PezChannel PezChannelGetById(int id)
        {
            return DB.Get<PezChannel>(PezChannel.Columns.Id, id);
        }

        #endregion

        #region 阿里巴巴
        public DataTable SKmOrderGetForAlibaba(Guid OrderId)
        {
            var sql =
                @"		select ed.sku_Id as entityId,1 as entityType,ed.product_code as code,edc.shop_code as warehouseCode
							,100 warehouseType ,od.item_quantity as quantity,o.order_id as bizSrcId,100 bizSrcType ,null as bizSrcSubId
							from (select Guid,order_id,parent_order_id from [order] WITH (nolock) where GUID=@OrderId) as o
							inner join order_detail as od WITH (nolock) on od.order_GUID=o.GUID
							inner join group_order as gp WITH (nolock) on gp.order_guid=o.parent_order_id
							inner join external_deal_combo as edc WITH (nolock) on edc.bid=gp.business_hour_guid
							inner join external_deal as ed WITH (nolock) on ed.Guid=edc.external_deal_guid						";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@OrderId", OrderId, DbType.Guid);
            var dt = DataService.GetDataSet(qc).Tables[0];
            return dt;
        }
        /// <summary>
        /// 取得逾時未取的訂單商品數量
        /// </summary>
        /// <returns></returns>
        public DataTable SKmOrderGetNotVerifiedQuantityForAlibaba()
        {
            var sql =
                @"		select ed.guid as externalDealGuid,ed.sku_Id as entityId,1 as entityType,ed.product_code as code,vs.shop_code as warehouseCode,
                100 warehouseType ,(vs.ordered_quantity-vs.verified_quantity-vs.canceled_quantity-vs.returned_quantity) as quantity,o.order_id as bizSrcId,100 bizSrcType ,null as bizSrcSubId 
                from (select GUID,product_code,skuId from external_deal WITH (nolock) where not_verified_flag=1 or not_verified_flag is null and skuId is not null) as ed
				 	inner join view_skm_deal_guid_quantity as vs WITH (nolock) on vs.external_deal_guid=ed.GUID
				 inner join business_hour as bh WITH (nolock) on bh.Guid=vs.bid			
                inner join group_order as g WITH (nolock) on g.business_hour_guid=bh.GUID
                inner join [order] as o  WITH (nolock) on o.parent_order_id=g.order_guid";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            var dt = DataService.GetDataSet(qc).Tables[0];
            return dt;
        }

      
        #endregion
    }
}
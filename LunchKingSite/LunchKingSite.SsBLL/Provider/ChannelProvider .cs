﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using LunchKingSite.Core.Component;
using LunchKingSite.SsBLL.DbContexts;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.SsBLL.Provider
{
    public class ChannelProvider : IChannelProvider
    {
        #region 代銷相關
        
        public ChannelClientProperty ChannelClientPropertyGet(string appId)
        {
            return DB.Select().From<ChannelClientProperty>()
                .Where(ChannelClientProperty.AppIdColumn).IsEqualTo(appId)
                .ExecuteSingle<ChannelClientProperty>();
        }

        public void ChannelClientPropertySet(ChannelClientProperty clientProperty)
        {
            DB.Save(clientProperty);
        }

        public ViewChannelCheckBillOrderCollection ViewChannelCheckBillOrderGet(int type, string selectedMonth)
        {
            return DB.Select().From<ViewChannelCheckBillOrder>().NoLock()
                .Where(ViewChannelCheckBillOrder.Columns.Type).IsEqualTo(type)
                .And(ViewChannelCheckBillOrder.Columns.BuyDate).IsBetweenAnd(Convert.ToDateTime(selectedMonth).GetFirstDayOfMonth().ToString("yyyy/MM/dd HH:mm:ss"), Convert.ToDateTime(selectedMonth).GetLastDayOfMonth().ToString("yyyy/MM/dd 23:59:59"))
                .ExecuteAsCollection<ViewChannelCheckBillOrderCollection>();
        }

        public ViewChannelCheckBillCancelVerificationCollection ViewChannelCheckBillCancelVerificationGet(int type, string selectedMonth)
        {
            return DB.Select().From<ViewChannelCheckBillCancelVerification>().NoLock()
                .Where(ViewChannelCheckBillCancelVerification.Columns.Type).IsEqualTo(type)
                .And(ViewChannelCheckBillCancelVerification.Columns.UsageVerifiedTime).IsBetweenAnd(Convert.ToDateTime(selectedMonth).GetFirstDayOfMonth().ToString("yyyy/MM/dd HH:mm:ss"), Convert.ToDateTime(selectedMonth).GetLastDayOfMonth().ToString("yyyy/MM/dd 23:59:59"))
                .ExecuteAsCollection<ViewChannelCheckBillCancelVerificationCollection>();
        }

        public ViewChannelCheckBillVerificationCollection ViewChannelCheckBillVerificationGet(int type, string selectedMonth)
        {
            return DB.Select().From<ViewChannelCheckBillVerification>().NoLock()
                .Where(ViewChannelCheckBillVerification.Columns.Type).IsEqualTo(type)
                .And(ViewChannelCheckBillVerification.Columns.VDate).IsBetweenAnd(Convert.ToDateTime(selectedMonth).GetFirstDayOfMonth().ToString("yyyy/MM/dd HH:mm:ss"), Convert.ToDateTime(selectedMonth).GetLastDayOfMonth().ToString("yyyy/MM/dd 23:59:59"))
                .ExecuteAsCollection<ViewChannelCheckBillVerificationCollection>();
        }
        public ViewChannelCheckBillReturnCollection ViewChannelCheckBillReturnGet(int type, string selectedMonth)
        {
            return DB.Select().From<ViewChannelCheckBillReturn>().NoLock()
                .Where(ViewChannelCheckBillReturn.Columns.Type).IsEqualTo(type)
                .And(ViewChannelCheckBillReturn.Columns.ReturnDate).IsBetweenAnd(Convert.ToDateTime(selectedMonth).GetFirstDayOfMonth().ToString("yyyy/MM/dd HH:mm:ss"), Convert.ToDateTime(selectedMonth).GetLastDayOfMonth().ToString("yyyy/MM/dd 23:59:59"))
                .ExecuteAsCollection<ViewChannelCheckBillReturnCollection>();
        }

        #endregion 代銷相關

        #region ODM

        public ChannelOdmCustomerCollection ChannelOdmCustomerGetAll()
        {
            return DB.Select().From<ChannelOdmCustomer>().NoLock()
                .Where(ChannelOdmCustomer.Columns.IsDisabled).IsEqualTo(false)
             .ExecuteAsCollection<ChannelOdmCustomerCollection>();
        }

        public ChannelOdmCustomer ChannelOdmCustomerGet(Guid sellerRootGuid)
        {
            return DB.Select().From<ChannelOdmCustomer>().NoLock()
                .Where(ChannelOdmCustomer.Columns.SellerRootGuid).IsEqualTo(sellerRootGuid)
                .ExecuteAsCollection<ChannelOdmCustomerCollection>().FirstOrDefault() ?? new ChannelOdmCustomer();
        }

        public ChannelOdmSellerRoleCollection ChannelOdmSellerRoleGetByUserId(int userId)
        {
            return DB.Select().From<ChannelOdmSellerRole>().NoLock()
                .Where(ChannelOdmSellerRole.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<ChannelOdmSellerRoleCollection>();
        }

        public List<Guid> ChannelOdmCustomerGetAllSellerRootGuid()
        {
            string sql = @"select seller_root_guid from channel_odm_customer with(nolock)
                    where is_disabled = 0
                ";
            List<Guid> result = new List<Guid>();
            var qc = new QueryCommand(sql, SellerTree.Schema.Provider.Name);
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
            return result;
        }

        public bool ChannelOdmSellerRoleCollectionSet(ChannelOdmSellerRoleCollection channelOdmSellerRoleCollection)
        {
            return DB.SaveAll(channelOdmSellerRoleCollection) > 0;
        }

        public void ChannelOdmSellerRoleDeleteByUserid(int userid)
        {
            DB.Delete<ChannelOdmSellerRole>(ChannelOdmSellerRole.Columns.UserId, userid);
        }

        #endregion ODM

        #region ViewMasterpassBankLog
        public ViewMasterpassBankLogCollection MasterpassBankLogCollectionGet(int? bankId)
        {
            if(bankId.HasValue && bankId!=0)
            {
                return DB.Select().From<ViewMasterpassBankLog>().Where(ViewMasterpassBankLog.Columns.BankId).IsEqualTo(bankId)
                                .ExecuteAsCollection<ViewMasterpassBankLogCollection>();
            } 
            else
            {
                return DB.Select().From<ViewMasterpassBankLog>().ExecuteAsCollection<ViewMasterpassBankLogCollection>();                
            }              
        }
        #endregion

        #region ChannelCommissionRule

        public List<ChannelCommissionRule> GetChannelCommissionRules(ChannelSource source)
        {
            using (var db = new ChannelDbContext())
            {
                using (db.NoLock())
                {
                    return (from ccr in db.ChannelCommissionRuleEntities
                        where ccr.ChannelSource == (byte) source && ccr.IsDeleted == false
                            select ccr).ToList();
                }
            }
        }

        public ChannelCommissionRule GetChannelCommissionRule(ChannelSource source, int dealType, string keyWord)
        {
            using (var db = new ChannelDbContext())
            {
                using (db.NoLock())
                {
                    return (from ccr in db.ChannelCommissionRuleEntities
                        where ccr.ChannelSource == (byte) source
                              && ccr.DealType == dealType
                              && ccr.KeyWord == keyWord.Trim()
                              && ccr.IsDeleted == false
                        select ccr).FirstOrDefault();
                }
            }
        }

        public ChannelCommissionRule GetChannelCommissionRule(int id)
        {
            using (var db = new ChannelDbContext())
            {
                using (db.NoLock())
                {
                    return db.ChannelCommissionRuleEntities.FirstOrDefault(x=>x.Id == id);
                }
            }
        }

        public bool DeleteChannelCommissionRule(int id)
        {
            using (var db = new ChannelDbContext())
            {
                var role = db.ChannelCommissionRuleEntities.FirstOrDefault(x=>x.Id == id);
                if (role == null) return true;
                role.IsDeleted = true;
                db.Entry(role).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOrUpdateChannelCommissionRule(ChannelSource source, int dealType, string keyWord, decimal grossMarginLimit, string returnCode, int ruleId = 0)
        {
            using (var db = new ChannelDbContext())
            {
                var rule = ruleId == 0
                    ? GetChannelCommissionRule(source, dealType, keyWord)
                    : GetChannelCommissionRule(ruleId);

                if (rule == null || rule.Id == 0)
                {
                    rule = new ChannelCommissionRule
                    {
                        ChannelSource = (byte) source,
                        CreateTime = DateTime.Now,
                        RuleType = (byte)CommissionRuleType.NormalCondition
                    };
                }

                rule.GrossMarginLimit = grossMarginLimit;
                rule.ReturnCode = returnCode;
                rule.ModifyTime = DateTime.Now;
                rule.DealType = dealType;
                rule.KeyWord = keyWord;
                rule.IsDeleted = false;

                db.Entry(rule).State = rule.Id == 0
                    ? System.Data.Entity.EntityState.Added
                    : System.Data.Entity.EntityState.Modified;
                
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOrUpdateChannelCommissionRule(List<ChannelCommissionRule> rules)
        {
            using (var db = new ChannelDbContext())
            {
                foreach (var r in rules)
                {
                    if (r.Id == 0)
                    {
                        r.CreateTime = r.ModifyTime = DateTime.Now;
                        db.Entry(r).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        r.ModifyTime = DateTime.Now;
                        db.Entry(r).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                return db.SaveChanges() > 0;
            }
        }

        #endregion


        #region ChannelCommissionList
        public ChannelCommissionListCollection ChannelCommissionListGet(int source)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(source)
                .ExecuteAsCollection<ChannelCommissionListCollection>();
        }

        public ChannelCommissionList ChannelCommissionListGetBySid(int channel, Guid sid)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.SellerGuid).IsEqualTo(sid)
                .And(ChannelCommissionList.Columns.MainBid).IsNull()
                .And(ChannelCommissionList.Columns.Bid).IsNull()
                .ExecuteSingle<ChannelCommissionList>();
        }

        public ChannelCommissionList ChannelCommissionListGetByMainBid(int channel, Guid mainBid)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.MainBid).IsEqualTo(mainBid)
                .And(ChannelCommissionList.Columns.Bid).IsNull()
                .ExecuteSingle<ChannelCommissionList>();
        }

        public ChannelCommissionList ChannelCommissionListGetByBid(int channel, Guid bid)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.Bid).IsEqualTo(bid)
                .ExecuteSingle<ChannelCommissionList>();
        }

        public ChannelCommissionListCollection ChannelCommissionListGetBySeller(int channel,int type)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.Type).IsEqualTo(type)
                .And(ChannelCommissionList.Columns.SellerGuid).IsNotNull()
                .And(ChannelCommissionList.Columns.MainBid).IsNull()
                .And(ChannelCommissionList.Columns.Bid).IsNull()
                .ExecuteAsCollection<ChannelCommissionListCollection>();
        }

        public ChannelCommissionListCollection ChannelCommissionListGetByMainDeal(int channel, int type)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.Type).IsEqualTo(type)
                .And(ChannelCommissionList.Columns.MainBid).IsNotNull()
                .And(ChannelCommissionList.Columns.Bid).IsNull()
                .ExecuteAsCollection<ChannelCommissionListCollection>();
        }

        public ChannelCommissionListCollection ChannelCommissionListGetBySubDeal(int channel, int type)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.Type).IsEqualTo(type)
                .And(ChannelCommissionList.Columns.MainBid).IsNotNull()
                .And(ChannelCommissionList.Columns.Bid).IsNotNull()
                .ExecuteAsCollection<ChannelCommissionListCollection>();
        }

        public ChannelCommissionListCollection ChannelCommissionListGetByDeal(int channel, int type)
        {
            return DB.SelectAllColumnsFrom<ChannelCommissionList>().NoLock()
                .Where(ChannelCommissionList.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelCommissionList.Columns.Type).IsEqualTo(type)
                .And(ChannelCommissionList.Columns.MainBid).IsNull()
                .And(ChannelCommissionList.Columns.Bid).IsNotNull()
                .ExecuteAsCollection<ChannelCommissionListCollection>();
        }

        public void ChannelCommissionListSet(ChannelCommissionList list)
        {
            DB.Save(list);
        }

        public bool ChannelCommissionListDelete(int id)
        {
            DB.Destroy<ChannelCommissionList>(ChannelCommissionList.IdColumn.ColumnName, id);
            return true;
        }

        #endregion

        #region ChannelSpecialCommission
        public ChannelSpecialCommissionCollection ChannelSpecialCommissionGetByChannel(int channel)
        {
            string sql = @"select * from channel_special_commission where channel_source=@channel order by isnull(modify_time,create_time) desc";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@channel", (byte)channel, DbType.Int32);
            ChannelSpecialCommissionCollection data = new ChannelSpecialCommissionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ChannelSpecialCommission ChannelCommissionListGetByChannelBid(int channel, Guid bid)
        {
            return DB.SelectAllColumnsFrom<ChannelSpecialCommission>().NoLock()
                .Where(ChannelSpecialCommission.Columns.ChannelSource).IsEqualTo(channel)
                .And(ChannelSpecialCommission.Columns.Bid).IsEqualTo(bid)
                .ExecuteSingle<ChannelSpecialCommission>();
        }

        public void ChannelSpecialCommissionSet(ChannelSpecialCommission special)
        {
            DB.Save(special);
        }

        public bool ChannelSpecialCommissionSaveAll(ChannelSpecialCommissionCollection specials)
        {
            DB.SaveAll(specials);
            return true;
        } 
        #endregion


        #region ChannelPostList
        public void ChannelPostListSet(ChannelPostListCollection list)
        {
            DB.BulkInsert(list);
        }

        public bool ChannelPostListDelete(int channel_source)
        {
            DB.Destroy<ChannelPostList>(ChannelPostList.Columns.ChannelSource, channel_source);
            return true;
        } 
        #endregion

        #region shopping_guide_channel & shopping_guide_channel_detail & shopping_guide_deal_category

        public ShoppingGuideChannel ShoppingGuideChannelGet(ShoppingGuideDealType type)
        {
            return DB.SelectAllColumnsFrom<ShoppingGuideChannel>().NoLock()
                .Where(ShoppingGuideChannel.Columns.ChannelId).IsEqualTo((byte) type)
                .ExecuteSingle<ShoppingGuideChannel>();
        }

        public ShoppingGuideChannel ShoppingGuideChannelSet(ShoppingGuideChannel channel)
        {
            DB.Save(channel);
            return channel;
        }

        public List<Guid> ShoppingGuideChannelDetailGet(ShoppingGuideDealType type, string batchId)
        {
            var sql = @"SELECT bid FROM shopping_guide_channel_detail WHERE channel_id=@channelId and batch_id=@batchId";

            QueryCommand qc = new QueryCommand(sql, ShoppingGuideChannelDetail.Schema.Provider.Name);
            qc.AddParameter("@channelId", (byte)type, DbType.Byte);
            qc.AddParameter("@batchId", batchId, DbType.String);

            List<Guid> result = new List<Guid>();

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }

        public ShoppingGuideChannel ShoppingGuideChannelSaveDetail(ShoppingGuideDealType type, List<Guid> bids)
        {
            Guid batchId = Guid.NewGuid();
            var detailCol = new ShoppingGuideChannelDetailCollection();
            detailCol.AddRange(bids.Select(x => new ShoppingGuideChannelDetail
            {
                ChannelId = (byte) type,
                Bid = x,
                BatchId = batchId.ToString()
            }).ToList());

            var channel = ShoppingGuideChannelGet(type) ?? new ShoppingGuideChannel();
            channel.ChannelId = (byte) type;
            channel.ModifyTime = DateTime.Now;
            channel.DealCount = bids.Count;
            channel.BatchId = batchId.ToString();
            
            using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
            {
                DB.Delete<ShoppingGuideChannelDetail>(ShoppingGuideChannelDetail.Columns.ChannelId, (byte)type);
                DB.BulkInsert(detailCol);
                ShoppingGuideChannelSet(channel);
                ts.Complete();
            }

            return channel;
        }

        public void ShoppingGuideCategorySave(Dictionary<Guid, string> dealCategoryCol)
        {
            Guid batchId = Guid.NewGuid();
            var categoryTemp = new ShoppingGuideCategoryTempCollection();
            categoryTemp.AddRange(dealCategoryCol.Select(x => new ShoppingGuideCategoryTemp
            {
                Bid = x.Key,
                DealCategoryJson = x.Value,
                BatchId = batchId.ToString()
            }).ToList());
            
            if (DB.BulkInsert(categoryTemp))
            {
                var iSql = @"INSERT INTO shopping_guide_deal_category 
                            SELECT t.bid, t.deal_category_json, GETDATE() FROM shopping_guide_category_temp t 
                                LEFT JOIN shopping_guide_deal_category c ON t.Bid = c.Bid
                                WHERE t.batch_id=@batchId AND c.Bid IS NULL";

                QueryCommand iqc = new QueryCommand(iSql, ShoppingGuideCategoryTemp.Schema.Provider.Name);
                iqc.AddParameter("@batchId", batchId.ToString(), DbType.String);
                DB.Execute(iqc);

                var uSql = @"UPDATE shopping_guide_deal_category SET deal_category_json=t.deal_category_json
                                    , modify_time=GETDATE() FROM shopping_guide_category_temp t WHERE shopping_guide_deal_category.bid=t.bid AND t.batch_id=@batchId";

                QueryCommand uqc = new QueryCommand(uSql, ShoppingGuideCategoryTemp.Schema.Provider.Name);
                uqc.AddParameter("@batchId", batchId.ToString(), DbType.String);
                DB.Execute(uqc);

                var dSql = @"DELETE FROM shopping_guide_category_temp WHERE batch_id=@batchId";
                QueryCommand dqc = new QueryCommand(dSql, ShoppingGuideCategoryTemp.Schema.Provider.Name);
                dqc.AddParameter("@batchId", batchId.ToString(), DbType.String);
                DB.Execute(dqc);
            }
        }

        public ShoppingGuideDealCategoryCollection ShoppingGuideDealCategoryGet(List<Guid> bids)
        {
            ShoppingGuideDealCategoryCollection col = new ShoppingGuideDealCategoryCollection();
            int idx = 0;
            int batchLimit = 1000;  // sql server cannot take more than 2100 parameters.
            while (idx <= bids.Count - 1)
            {
                var batchProd = DB.SelectAllColumnsFrom<ShoppingGuideDealCategory>()
                    .Where(ShoppingGuideDealCategory.Columns.Bid).In(bids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<ShoppingGuideDealCategoryCollection>();

                col.AddRange(batchProd);
                idx += batchLimit;
            }

            return col;
        }

        #endregion

        #region ShoppingGuideQuery

        public List<Guid> ShoppingGuideQueryCollectionGet(Guid queryToken, string appId)
        {
            var sql = @"SELECT bid FROM shopping_guide_query WITH(NOLOCK) WHERE query_token=@token and app_id=@appId";

            QueryCommand qc = new QueryCommand(sql, ShoppingGuideQuery.Schema.Provider.Name);
            qc.AddParameter("@token", queryToken, DbType.Guid);
            qc.AddParameter("@appId", appId, DbType.String);

            List<Guid> result = new List<Guid>();

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }

        public Guid ShoppingGuideQueryCollectionSet(string appId, List<Guid> bids)
        {
            ShoppingGuideQueryCollection col = new ShoppingGuideQueryCollection();
            var queryToken = Guid.NewGuid();
            col.AddRange(bids.Select(x => new ShoppingGuideQuery
            {
                QueryToken = queryToken,
                Bid = x,
                CreateTime = DateTime.Now,
                AppId = appId,
                ExpiredTime = DateTime.Now.AddHours(24)
            }));

            DB.BulkInsert(col);
            return queryToken;
        }

        public void ShoppingGuideQueryDelete(int keepDay)
        {
            DateTime keepTime = DateTime.Now.AddDays(-keepDay);

            var sql = @"DELETE FROM shopping_guide_query WHERE expired_time < @expiredTime";
            QueryCommand qc = new QueryCommand(sql, ShoppingGuideQuery.Schema.Provider.Name);
            qc.AddParameter("@expiredTime", keepTime, DbType.DateTime);
            DB.Execute(qc);
        }

        #endregion

        #region 低毛利

        public List<Guid> GetFilterLowGrossMarginDeal(int channelId, string batchId, decimal couponGrossMargin, decimal deliveryGrossMargin)
        {
            List<Guid> result = new List<Guid>();

            var sqla = @"select bid from shopping_guide_channel_detail d with(nolock) inner join business_hour b with(nolock)
                            on d.bid = b.GUID
                            inner join view_deal_base_gross_margin gm with(nolock)
                            on d.bid = gm.business_hour_guid
                            inner join deal_property dp with(nolock)
                            on d.bid = dp.business_hour_guid
                            where d.channel_id =@channelId and d.batch_id=@batchId and b.business_hour_status & 1024 = 0 and b.business_hour_status & 2048 = 0
                            and ((dp.delivery_type = 1 and gm.tax_gross_margin > @cgm) Or (dp.delivery_type = 2 and gm.tax_gross_margin > @dgm))
                            union
                            select bid from shopping_guide_channel_detail d with(nolock) inner join business_hour b with(nolock)
                            on d.bid = b.GUID
                            inner join deal_property dp with(nolock)
                            on d.bid = dp.business_hour_guid
                            where d.channel_id = @channelId and d.batch_id=@batchId and b.business_hour_status & 1024 > 0
                            and NOT EXISTS(
	                            select 1 from combo_deals cd with(nolock)
	                            inner join deal_property dp with(nolock)
	                            on cd.MainBusinessHourGuid = dp.business_hour_guid
	                            inner join view_deal_base_gross_margin gm with(nolock)
	                            On cd.BusinessHourGuid = gm.business_hour_guid
	                             where cd.MainBusinessHourGuid = d.bid AND cd.MainBusinessHourGuid <> cd.BusinessHourGuid
	                             and ((dp.delivery_type = 1 and gm.tax_gross_margin < @cgm) Or (dp.delivery_type = 2 and gm.tax_gross_margin < @dgm))
                            )";

            QueryCommand qca = new QueryCommand(sqla, ShoppingGuideChannelDetail.Schema.Provider.Name);
            qca.AddParameter("@channelId", channelId, DbType.Byte);
            qca.AddParameter("@batchId", batchId, DbType.String);
            qca.AddParameter("@cgm", couponGrossMargin, DbType.Decimal);
            qca.AddParameter("@dgm", deliveryGrossMargin, DbType.Decimal);
            
            using (var reader = DataService.GetReader(qca))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }

        #endregion


        #region PChome代銷 產品
        public bool PChomeChannelProdSetList(PchomeChannelProdCollection prods)
        {
            return DB.SaveAll(prods) > 0;
        }

        public PchomeChannelProd PChomeChannelProdGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                .Where(PchomeChannelProd.Columns.Bid).IsEqualTo(bid)
                .ExecuteSingle<PchomeChannelProd>();
        }

        public PchomeChannelProd PChomeChannelProdGetByProdId(string prodId)
        {
            return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                .Where(PchomeChannelProd.Columns.ProdId).IsEqualTo(prodId)
                .ExecuteSingle<PchomeChannelProd>();
        }

        public PchomeChannelProdCollection PchomeChannelProdGetByShelfVerify(int ? isVerify, int? isShelf)
        {
            if (isVerify != null && isShelf == null)
            {
                return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                   .Where(PchomeChannelProd.Columns.IsVerify).IsEqualTo(isVerify)
                   .ExecuteAsCollection<PchomeChannelProdCollection>();

            }
            else if (isVerify == null && isShelf != null)
            {
                return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                   .Where(PchomeChannelProd.Columns.IsShelf).IsEqualTo(isShelf)
                   .ExecuteAsCollection<PchomeChannelProdCollection>();

            }
            else if (isVerify != null && isShelf != null)
            {
                return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                   .Where(PchomeChannelProd.Columns.IsVerify).IsEqualTo(isVerify)
                   .And(PchomeChannelProd.Columns.IsShelf).IsEqualTo(isShelf)
                   .ExecuteAsCollection<PchomeChannelProdCollection>();

            }
            else
            {
                return DB.SelectAllColumnsFrom<PchomeChannelProd>().NoLock()
                   .ExecuteAsCollection<PchomeChannelProdCollection>();
            }

        }


        public void PchomeChannelProdSet(PchomeChannelProd prod)
        {
            DB.Save(prod);
        }

        #endregion

        #region PChome代銷 聯絡單客訴單
        public bool PChomeChannelContactSetList(PchomeChannelContactCollection contacts)
        {
            return DB.SaveAll(contacts) > 0;
        }

        public PchomeChannelContact PchomeChannelContactGetById(string contactId)
        {
            return DB.SelectAllColumnsFrom<PchomeChannelContact>().NoLock()
                .Where(PchomeChannelContact.Columns.ContactId).IsEqualTo(contactId)
                .ExecuteSingle<PchomeChannelContact>();
        }

        public void PchomeChannelContactSet(PchomeChannelContact contact)
        {
            DB.Save(contact);
        }

        public PchomeChannelContactCollection PchomeChannelContactGetToReply()
        {


            string sql = @"select pcc.* from pchome_channel_contact pcc 
	                      inner join  customer_service_message csm on pcc.service_no=csm.service_no
	                      where pcc.reply_time is null";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            PchomeChannelContactCollection data = new PchomeChannelContactCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }



        public PchomeChannelComplaintCollection PchomeChannelComplaintGetToReply()
        {
            string sql = @"select pcc.* from pchome_channel_complaint pcc 
                           inner join  customer_service_message csm on pcc.service_no=csm.service_no
                           where pcc.reply_time is null";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            PchomeChannelComplaintCollection data = new PchomeChannelComplaintCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public bool PchomeChannelComplaintSetList(PchomeChannelComplaintCollection complaints)
        {
            return DB.SaveAll(complaints) > 0;
        }

        public PchomeChannelComplaint PchomeChannelComplaintGetById(string complaintId)
        {
            return DB.SelectAllColumnsFrom<PchomeChannelComplaint>().NoLock()
                .Where(PchomeChannelComplaint.Columns.ComplaintId).IsEqualTo(complaintId)
                .OrderDesc(PchomeChannelComplaint.Columns.Id)
                .ExecuteSingle<PchomeChannelComplaint>();
        }

        public void PchomeChannelComplaintSet(PchomeChannelComplaint complaint)
        {
            DB.Save(complaint);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System.Data;
using System.Web;
using log4net;
using LunchKingSite.Core.Component;

namespace LunchKingSite.SsBLL
{
    public class SSMarketingProvider : IMarketingProvider
    {

        private static ILog logger = LogManager.GetLogger(typeof (SSMarketingProvider).Name);
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region CreditCardPremium

        public CreditCardPremiumCollection CreditCardPremiumGetListByActivityCodeAndEnabled(string activityCode)
        {
            return DB.SelectAllColumnsFrom<CreditCardPremium>().Where(CreditCardPremium.Columns.ActivityCode).IsEqualTo(activityCode).
                And(CreditCardPremium.Columns.Enabled).IsEqualTo(true).ExecuteAsCollection<CreditCardPremiumCollection>();
        }
        #endregion  CreditCardPremium

        #region MemberReferral
        public bool MemberReferralSet(MemberReferral data)
        {
            DB.Save(data);
            return true;
        }
        public MemberReferral MemberReferralGet(Guid id)
        {
            return DB.Get<MemberReferral>(id);
        }

        public MemberReferralCollection MemberReferralGetList()
        {
            return DB.SelectAllColumnsFrom<MemberReferral>().OrderAsc(MemberReferral.Columns.CreateTime).ExecuteAsCollection<MemberReferralCollection>();
        }
        #endregion MemberReferral

        #region ReferralCampaign

        public ReferralCampaign ReferralCampaignGet(Guid id)
        {
            return DB.Get<ReferralCampaign>(id);
        }

        public bool ReferralCampaignSet(ReferralCampaign data)
        {
            DB.Save(data);
            return true;
        }
        public ReferralCampaignCollection ReferralCampaignGetListByReferralGuid(Guid rGuid)
        {
            return DB.SelectAllColumnsFrom<ReferralCampaign>()
                    .Where(ReferralCampaign.Columns.ReferrerGuid)
                    .IsEqualTo(rGuid)
                    .OrderAsc(ReferralCampaign.Columns.CreateTime)
                    .ExecuteAsCollection<ReferralCampaignCollection>();
        }
        public ReferralCampaign ReferralCampaignGetByReferralGuidAndCode(Guid rGuid, string code)
        {
            return
                DB.SelectAllColumnsFrom<ReferralCampaign>().Where(ReferralCampaign.Columns.ReferrerGuid).IsEqualTo(rGuid)
                    .And(ReferralCampaign.Columns.Code).IsEqualTo(code).ExecuteSingle<ReferralCampaign>();
        }

        /// <summary>
        /// 無值回傳 null
        /// </summary>
        /// <param name="rsrc">譬如  IT_home  的格式</param>
        /// <returns></returns>
        public ReferralCampaign ReferralCampaignGetByRsrc(string rsrc)
        {
            string[] codes = rsrc.Split('_');
            if (codes.Length < 2)
            {
                if (config.RsrcDetailError)
                {
                    try
                    {
                        try
                        {
                            string routeUrl = ((System.Web.Routing.Route)(HttpContext.Current.Request.RequestContext.RouteData.Route)).Url;
                            logger.WarnFormat("提醒 rsrc code 不如預期為 [0]_[1] 的格式. rsrc = {0}, routeUrl={1}, rawUrl={2}, referrer={3}, agent={4}",
                                rsrc, routeUrl, HttpContext.Current.Request.RawUrl, HttpContext.Current.Request.UrlReferrer,
                                HttpContext.Current.Request.UserAgent);
                        }
                        catch
                        {
                            logger.WarnFormat("提醒 rsrc code 不如預期為 [0]_[1] 的格式. rsrc = {0}, rawUrl={1}, referrer={2}, agent={3}",
                                rsrc, HttpContext.Current.Request.RawUrl, HttpContext.Current.Request.UrlReferrer,
                                HttpContext.Current.Request.UserAgent);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.WarnFormat("提醒 rsrc code 不如預期為 [0]_[1] 的格式. rsrc = {0}, 獲取額外的訊息失敗. ex={1}", rsrc, ex);
                    }
                }
                return null;
            }
            string sql = string.Format(@"select top 1 rc.* from {0} rc
  inner join {1} mr on mr.Guid = rc.referrer_guid
  where rc.rsrc = @rsrc", ReferralCampaign.Schema.TableName, MemberReferral.Schema.TableName);
            QueryCommand qc = new QueryCommand(sql, ReferralCampaign.Schema.Provider.Name);
            qc.AddParameter("@rsrc", rsrc, DbType.String);

            ReferralCampaign rc = new ReferralCampaign();
            rc.LoadAndCloseReader(DataService.GetReader(qc));

            if (rc.IsLoaded == false)
            {
                return null;
            }
            return rc;
        }

        public CpaTracking CpaTrackingGetLast(string cpaKey)
        {
            return DB.QueryOver<CpaTracking>().Where(t => t.CpaKey == cpaKey)
                .OrderByDescending(t => t.CreateTime).FirstOrDefault();
        }

        public void CpaTrackingSet(CpaTracking tracking)
        {
            DB.Save(tracking);
        }

        public void CpaOrderSet(CpaOrder cpaOrder)
        {
            DB.Save(cpaOrder);
        }

        public List<CpaTracking> CpaTrackingGetHeadAndTail(string cpaKey)
        {
            List<CpaTracking> items = DB.QueryOver<CpaTracking>().Where(t => t.CpaKey == cpaKey)
                .OrderBy(t => t.Id).ToList();
            List<CpaTracking> result = new List<CpaTracking>();
            if (items.Count > 1)
            {
                result.Add(items[0]);
                result.Add(items[items.Count - 1]);
            }
            else if (items.Count == 1) //修正原有沒判斷只有單筆的錯誤
            {
                result.Add(items[0]);
                result.Add(items[0]);
            }
            return result;
        }
        public CpaTracking CpaTrackingGetListExternalByCpaKey(string cpaKey)
        {
            string sql = string.Format(@"select top 1 ct.* from {0} ct
            join {1} rc on ct.rsrc = rc.rsrc
            join {2} mr on mr.Guid = rc.referrer_guid
            where
            rc.is_external = 1 and
            ct.cpa_key = @cpaKey
            order by ct.id desc "
                ,CpaTracking.Schema.TableName
                ,ReferralCampaign.Schema.TableName
                ,MemberReferral.Schema.TableName);

            QueryCommand qc = new QueryCommand(sql, CpaTracking.Schema.Provider.Name);
            qc.AddParameter("@cpaKey", cpaKey, DbType.String);

            CpaTracking ct = new CpaTracking();
            ct.LoadAndCloseReader(DataService.GetReader(qc));

            if (ct.IsLoaded == false)
            {
                return null;
            }
            return ct;
        }

        public List<CpaTracking> CpaTrackingGetListByCpaKey(string[] cpaKeys)
        {
            var inList = "(" +
                         string.Join(", ", Enumerable.Range(0, cpaKeys.Length).Select(t=> "@t" + t)) +
                         ")";
            string sql = string.Format("select * from cpa_tracking where cpa_key in {0}", inList);
            QueryCommand qc = new QueryCommand(sql, CpaTracking.Schema.Provider.Name);
            for(int i=0; i< cpaKeys.Length; i++)
            {
                qc.Parameters.Add("@t" + i, cpaKeys[i], DbType.String);
            }
            CpaTrackingCollection result = new CpaTrackingCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result.ToList();
        }

        #endregion ReferralCampaign

        #region PezEdmUpload
        public void PezEdmUploadSet(PezEdmUpload uploadObject)
        {
            DB.Save<PezEdmUpload>(uploadObject);
        }

        public void PezEdmUploadDelete(int id)
        {
            DB.Delete<PezEdmUpload>(PezEdmUpload.Columns.Id, id);
        }

        public PezEdmUpload PezEdmUploadGet(int id)
        {
            return DB.Get<PezEdmUpload>(id);
        }

        public PezEdmUpload PezEdmUploadGet(string colName, object value)
        {
            return DB.Get<PezEdmUpload>(colName, value);
        }

        public PezEdmUploadCollection PezEdmUploadGetListTop50()
        {
            return DB.SelectAllColumnsFrom<PezEdmUpload>().Top("50").OrderDesc(PezEdmUpload.Columns.CreateTime).ExecuteAsCollection<PezEdmUploadCollection>();
        }
        #endregion

        #region ReferredAction
        public bool ReferredActionSet(ReferralAction data)
        {
            DB.Save(data);
            return true;
        }

        public ReferralActionCollection ReferralActionGetListByReferenceSourceId(string srcId)
        {
            return DB.SelectAllColumnsFrom<ReferralAction>()
                    .Where(ReferralAction.Columns.ReferrerSourceId).IsEqualTo(srcId)
                    .And(ReferralAction.Columns.CreateTime).IsGreaterThan(DateTime.Now.AddDays(-45))
                    .OrderAsc(ReferralAction.Columns.CreateTime)
                    .ExecuteAsCollection<ReferralActionCollection>();
        }
        public ReferralActionCollection ReferralActionGetListByReferenceSourceAndActionType(string srcId, ReferrerActionType atype)
        {
            return DB.SelectAllColumnsFrom<ReferralAction>()
                    .Where(ReferralAction.Columns.ReferrerSourceId).IsEqualTo(srcId)
                    .And(ReferralAction.Columns.ActionType).IsEqualTo((int)atype)
                    .And(ReferralAction.Columns.CreateTime).IsGreaterThan(DateTime.Now.AddDays(-45))
                    .OrderAsc(ReferralAction.Columns.CreateTime)
                    .ExecuteAsCollection<ReferralActionCollection>();
        }
        public ReferralActionCollection ReferralActionGetListByReferenceSourceAndTime(
            ReferrerActionType? actionType,
            string srcId, DateTime starttime, DateTime endtime)
        {
            if (actionType == null)
            {
                return DB.SelectAllColumnsFrom<ReferralAction>()
                    .Where(ReferralAction.Columns.ReferrerSourceId).IsEqualTo(srcId)
                    .And(ReferralAction.Columns.CreateTime).IsGreaterThan(starttime)
                    .And(ReferralAction.Columns.CreateTime).IsLessThanOrEqualTo(endtime)
                    .OrderAsc(ReferralAction.Columns.CreateTime)
                    .ExecuteAsCollection<ReferralActionCollection>();
            }
            else
            {
                return DB.SelectAllColumnsFrom<ReferralAction>()
                    .Where(ReferralAction.Columns.ReferrerSourceId).IsEqualTo(srcId)
                    .And(ReferralAction.Columns.ActionType).IsEqualTo((int)actionType)
                    .And(ReferralAction.Columns.CreateTime).IsGreaterThan(starttime)
                    .And(ReferralAction.Columns.CreateTime).IsLessThanOrEqualTo(endtime)
                    .OrderAsc(ReferralAction.Columns.CreateTime)
                    .ExecuteAsCollection<ReferralActionCollection>();
            }
        }
        #endregion

        #region GA

        public bool GaOverviewSet(GaOverviewCollection data)
        {
            return DB.SaveAll<GaOverview, GaOverviewCollection>(data) != 0;
        }

        public GaOverviewCollection GaOverviewGetList(DateTime s, DateTime e, int vid)
        {
            return DB.SelectAllColumnsFrom<GaOverview>()
                .Where(GaOverview.Columns.QueryDate).IsGreaterThanOrEqualTo(s)
                .And(GaOverview.Columns.QueryDate).IsLessThanOrEqualTo(e)
                .And(GaOverview.Columns.ViewId).IsEqualTo(vid)
                .ExecuteAsCollection<GaOverviewCollection>();
        }

        #endregion

        #region BiTraffic

       
        public bool BiTrafficCollectionSet(BiTrafficCollection BitrafficList)
        {
            DB.SaveAll<BiTraffic, BiTrafficCollection>(BitrafficList);
            return true;
        }

        #endregion 

        #region GaDeal
        public void GaDealSet(GaDealCollection data)
        {
            DB.SaveAll<GaDeal, GaDealCollection>(data);
        }

        public void GaDealSet(GaDeal data)
        {
            DB.Save(data);
        }

        public GaDealCollection GetGaDealCollectionBySiteDate(int gid, DateTime start_date, DateTime end_date)
        {
            return DB.SelectAllColumnsFrom<GaDeal>()
                   .Where(GaDeal.Columns.StartDate).IsGreaterThanOrEqualTo(start_date)
                   .And(GaDeal.Columns.EndDate).IsLessThanOrEqualTo(end_date)
                   .And(GaDeal.Columns.Gid).IsEqualTo(gid)
                   .ExecuteAsCollection<GaDealCollection>();
        }

        public GaDealCollection GetGaDealCollectionByGid(int gid)
        {
            return DB.SelectAllColumnsFrom<GaDeal>()
                      .Where(GaDeal.Columns.Gid).IsEqualTo(gid)
                      .ExecuteAsCollection<GaDealCollection>();
        }

        public void DeleteGaDeal(int gid)
        {
            DB.Delete<GaDeal>(GaDeal.Columns.Gid, gid);
        }
        #endregion

        #region GaDealList
        public void SetGaDealList(GaDealList data)
        {
            DB.Save<GaDealList>(data);
        }

        public void EnableGaDealList(int id, bool enabled)
        {
            DB.Update<GaDealList>().Set(GaDealList.Columns.Enabled).EqualTo(enabled).Where(GaDealList.Columns.Id).IsEqualTo(id).Execute();
        }

        public GaDealList GetGaDealListById(int id)
        {
            return DB.Get<GaDealList>(id);
        }

        public GaDealListCollection GetGaDealListByDate(DateTime date)
        {
            return DB.SelectAllColumnsFrom<GaDealList>()
                 .Where(GaDealList.Columns.StartDate).IsLessThanOrEqualTo(date)
                 .And(GaDealList.Columns.EndDate).IsGreaterThan(date)
                 .And(GaDealList.Columns.Enabled).IsEqualTo(true).And(GaDealList.Columns.Type).IsEqualTo((int)GaDealListType.BySchedule)
                 .ExecuteAsCollection<GaDealListCollection>();
        }

        public void DeleteGaDealList(int id)
        {
            DB.Delete<GaDealList>(GaDealList.Columns.Id, id);
        }
        #endregion

        #region ViewGaDealList
        public ViewGaDealListCollection GetViewGaDealList(Guid? bid, int type)
        {
            string sql = "select * from " + ViewGaDealList.Schema.TableName + " where " + ViewGaDealList.Columns.Type + "=@type ";
            if (bid.HasValue)
            {
                sql += " and " + ViewGaDealList.Columns.Bid + "=@bid ";
            }
            sql += " order by " + ViewGaDealList.Columns.Id + " desc"; 
            QueryCommand qc = new QueryCommand(sql, ViewGaDealList.Schema.Provider.Name);
            ViewGaDealListCollection vgac = new ViewGaDealListCollection();
            qc.AddParameter("@type", type, DbType.Int16);
            if (bid.HasValue)
            {
                qc.AddParameter("@bid", bid, DbType.Guid);
            }
            vgac.LoadAndCloseReader(DataService.GetReader(qc));
            return vgac;
            
        }
        public ViewGaDealList GetViewGaDealList(int id)
        {
            return DB.SelectAllColumnsFrom<ViewGaDealList>().Where(ViewGaDealList.Columns.Id).IsEqualTo(id).ExecuteSingle<ViewGaDealList>();

        }
        public ViewGaDealListCollection GetViewGaDealList(bool only_enabled)
        {
            string sql = "select * from " + ViewGaDealList.Schema.TableName;
            if (only_enabled)
            {
                sql += " where " + ViewGaDealList.Columns.Enabled + "=1";
            }
            sql += " order by " + ViewGaDealList.Columns.Id + " desc"; 
            QueryCommand qc = new QueryCommand(sql, ViewGaDealList.Schema.Provider.Name);
            ViewGaDealListCollection vgac = new ViewGaDealListCollection();
            vgac.LoadAndCloseReader(DataService.GetReader(qc));
            return vgac;
        }
        #endregion

        #region DiscountCategoryDeal

        public bool IsDiscountCategoryCampaignExist(Guid bid, int campaignId)
        {
            var sql = @"SELECT count(1) cnt
                        FROM dbo.discount_category AS dc WITH (nolock) INNER JOIN
                             dbo.category_deals AS cd WITH (nolock) ON dc.category_id = cd.cid
	                        where bid= @bid and campaign_id = @campaignId";

            var qc = new QueryCommand(sql, DiscountCategory.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@campaignId", campaignId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc) > 0;
        }

        public bool IsDiscountEventPromoLinkCampaignExist(Guid bid, List<int> mainIds)
        {
            if (!mainIds.Any()) return false;

            var sql = string.Format(@"select count(1) cnt from discount_event_promo_link l with(nolock)
                    inner join view_event_promo_item i with(nolock)
                    on l.event_promo_id = i.MainId
                    where MainId in ({0}) and business_hour_guid = @bid and item_type = 0"
                    , string.Join(",", mainIds));
            var qc = new QueryCommand(sql, DiscountEventPromoLink.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);            
            return (int)DataService.ExecuteScalar(qc) > 0;
        }

        public bool IsDiscountBrandLinkCampaignExist(Guid bid, List<int> brandIds)
        {
            if (!brandIds.Any()) return false;

            var sql = string.Format(@"select count(1) cnt from discount_brand_link l with(nolock)
                    inner join view_brand_item i with(nolock)
                    on l.brand_id = i.main_id
                    where brand_id in ({0}) and i.business_hour_guid = @bid"
                    , string.Join(",", brandIds));
            var qc = new QueryCommand(sql, DiscountBrandLink.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc) > 0;
        }

        #endregion DiscountCategoryDeal
    }
}

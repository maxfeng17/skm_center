﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class DealProvider : IDealProvider
    {
        #region GmcAvgPriceDeal

        public GmcAvgPriceDeal GetGmcAvgPriceDeal(Guid bid)
        {
            using (var db = new DealDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from g in db.GmcAvgPriceDealEntities
                        where g.Bid == bid
                                 select g).FirstOrDefault();
                    return item ?? new GmcAvgPriceDeal();
                }
            }
        }

        public List<Guid> GetGmcAvgPriceDealAllBid()
        {
            using (var db = new DealDbContext())
            {
                using (db.NoLock())
                {
                    var items = db.GmcAvgPriceDealEntities.Select(x => x.Bid).Distinct().ToList();
                    return items;
                }
            }
        }

        #endregion GmcAvgPriceDeal

        public List<Guid> GetOnlineGroupCouponBid()
        {
            var sql = @"SELECT          GUID AS 'business_hour_guid'
                        FROM              dbo.business_hour AS b WITH (nolock)
                        WHERE          (GETDATE() BETWEEN business_hour_order_time_s AND business_hour_order_time_e) AND 
                            (business_hour_status & 8388608 > 1) AND (business_hour_status & 1024 > 1) AND 
                            (business_hour_status & 2048 = 0) AND EXISTS
                                (SELECT          business_hour_GUID, city_id, sequence, effective_start, effective_end, status, is_in_turn, 
                                                              is_lock_seq, is_hot_deal
                                  FROM               dbo.deal_time_slot AS t WITH (nolock)
                                  WHERE           (business_hour_GUID = b.GUID) AND (GETDATE() BETWEEN effective_start AND effective_end) AND 
                                                              (status = 0))";

            using (var db = new OrderDbContext())
            {
                return db.Database.SqlQuery<Guid>(sql).ToList();
            }
        }
    }
}

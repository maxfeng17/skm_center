using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Text.RegularExpressions;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;
using SubSonic;
using log4net;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.SsBLL
{
    public class SSMemberProvider : IMemberProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly ILog log = LogManager.GetLogger(typeof(SSMemberProvider));

        #region Memeber
        public bool MemberSet(Member member)
        {
            if (member != null && (member.IsDirty || member.IsNew))
                DB.Save<Member>(member);
            return true;
        }

        public Member MemberGet(string memberId)
        {
            return DB.Get<Member>(memberId);
        }

        public Member MemberGet(int uniqueId)
        {
            return MemberGetbyUniqueId(uniqueId);
        }

        public Member MemberGetbyUniqueId(int uniqueId)
        {
            return DB.Get<Member>(Member.Columns.UniqueId, uniqueId);
        }

        public Member MemberGetByTrueEmail(string userName)
        {
            return DB.Get<Member>(Member.Columns.TrueEmail, userName);
        }

        public Member MemberGetByUserName(string userName)
        {
            return DB.Get<Member>(Member.Columns.UserName, userName);
        }

        public Member MemberGetByUserEmail(string userEmail)
        {
            return DB.Get<Member>(Member.Columns.UserEmail, userEmail);
        }

        public Member MemberGetByMobile(string mobile)
        {
            return DB.Get<Member>(Member.Columns.Mobile, mobile);
        }

        public Member MemberGetByAccessToken(string toekn)
        {
            string sql = string.Format(@" select m.* from member m with(nolock)
                 inner join oauth_token ot with(nolock)
                 on m.unique_id = ot.user_id and ot.is_valid = 1 and ot.is_token_refreshed = 0 and ot.expired_time > getdate()
                 where access_token = @accessToken");

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("@accessToken", toekn, DbType.String);

            MemberCollection memCol = new MemberCollection();
            memCol.LoadAndCloseReader(DataService.GetReader(qc));
            return memCol.Any() ? memCol.FirstOrDefault() : new Member();
        }

        public MemberCollection MemberGetByRoleGuid(Guid roleGuid)
        {
            string sql = string.Format(@" select * from member
                 where unique_id in (
                 select user_id from role_member
                 where role_GUID=@roleGuid )");

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("@roleGuid", roleGuid, DbType.Guid);

            MemberCollection memCol = new MemberCollection();
            memCol.LoadAndCloseReader(DataService.GetReader(qc));
            return memCol;
        }

        public string MemberUserNameGet(int userId)
        {
            string sql = string.Format(@"SELECT {0}
                            FROM {1} m
                            WHERE m.{2} = @userId", Member.Columns.UserName, Member.Schema.TableName, Member.Columns.UniqueId);
            return new InlineQuery().ExecuteScalar<string>(sql, userId);
        }

        public int MemberUniqueIdGet(string userName)
        {
            string sql = @"SELECT unique_id
                            FROM member m
                            WHERE m.user_name = @userName";
            return new InlineQuery().ExecuteScalar<int?>(sql, userName).GetValueOrDefault();
        }

        /// <summary>
        /// 更改使用者的Email(UserName)，不建議直接使用，請用 MemberUtility.ChangeUserEmail。
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="newUserName"></param>
        /// <param name="checkFirst">檢查使用者是否存在，要更新的user name有沒有被別人使用</param>
        /// <returns></returns>
        public bool MemberSetUserName(int uniqueId, string newUserName, bool checkFirst = true)
        {
            if (checkFirst)
            {
                Member mem = MemberGet(uniqueId);
                if (mem.IsLoaded == false)
                {
                    return false;
                }

                if (string.Equals(mem.UserName, newUserName, StringComparison.OrdinalIgnoreCase))
                {
                    //Same Email
                    return false;
                }

                Member chkMem = MemberGet(newUserName);
                if (chkMem.IsLoaded)
                {
                    return false;
                }
            }
            new Update(Member.Schema.TableName)
                .Set(Member.Columns.UserName).EqualTo(newUserName).Set(Member.Columns.UserEmail).EqualTo(newUserName)
                .Where(Member.Columns.UniqueId).IsEqualTo(uniqueId)
                .Execute();
            return true;
        }

        public MemberCollection MemberAuthorizeGet(int userId)
        {
            string sql = @" SELECT *
                            FROM member m
                            WHERE m.unique_id = @userId
	                            AND m.status & " + (int)MemberStatusFlag.Active17Life + @" > 0
	                            OR m.unique_id in 
                                (select ml.user_id from member_link ml 
                                    inner join member m2 on m2.unique_id = ml.user_id
                                    WHERE m2.unique_id = @userId AND ml.external_org <> " + (int)SingleSignOnSource.ContactDigitalIntegration + @")
                            ";
            return new InlineQuery().ExecuteAsCollection<MemberCollection>(sql, userId, userId);
        }

        public MemberCollection MemberGetList(string filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<Member, MemberCollection>(1, -1, null, filter);
        }

        public MemberCollection MemberGetList(string column, object value)
        {
            SqlQuery qry;
            if (string.Compare(column, Member.Columns.UserName, true) == 0)
                qry = DB.SelectAllColumnsFrom<Member>().Where(column).Like((string)value);
            else
                qry = DB.SelectAllColumnsFrom<Member>().Where(column).IsEqualTo(value);
            return qry.ExecuteAsCollection<MemberCollection>();
        }

        public MemberCollection MemberGetList(List<int> unique_id)
        {
            SqlQuery qry = DB.SelectAllColumnsFrom<Member>().Where(Member.Columns.UniqueId).In(unique_id);
            return qry.ExecuteAsCollection<MemberCollection>();
        }

        public MemberCollection MemberGetList(int pageNumber, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<Member, MemberCollection>(pageNumber, pageSize, orderBy, filter);
        }

        public MemberCollection MemberGetListByCity(MemberStatusFlag status, string[] cities, string[] buildings)
        {
            QueryCommand qc = new QueryCommand(@"select a.* from member a with(nolock) inner join building b with(nolock) on a.building_guid=b.guid where (a.status & @statusMask = @expectStatus) ", Member.Schema.Provider.Name);
            qc.AddParameter("@statusMask", (int)status, DbType.Int32);
            qc.AddParameter("@expectStatus", (int)status, DbType.Int32);

            string additionalFilter = string.Empty;
            if (cities != null && cities.Length > 0)
            {
                additionalFilter += " b.city_id in (@cid0";
                qc.AddParameter("@cid0", int.Parse(cities[0]), DbType.Int32);
                for (int i = 1; i < cities.Length; i++)
                {
                    string pn = "@cid" + i;
                    additionalFilter += string.Format("," + pn);
                    qc.AddParameter(pn, int.Parse(cities[i]), DbType.Int32);
                }
                additionalFilter += ")";
            }

            if (buildings != null && buildings.Length > 0)
            {
                if (additionalFilter != string.Empty)
                    additionalFilter += " or";
                additionalFilter += " b.guid in (@bid0";
                qc.AddParameter("@bid0", new Guid(buildings[0]), DbType.Guid);
                for (int i = 1; i < buildings.Length; i++)
                {
                    string pn = "@bid" + i;
                    additionalFilter += string.Format("," + pn);
                    qc.AddParameter(pn, new Guid(buildings[i]), DbType.Guid);
                }
                additionalFilter += ")";
            }

            qc.CommandSql += !string.IsNullOrEmpty(additionalFilter) ? "and (" + additionalFilter + ")" : "";
            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListByCityWithOutSubscription(MemberStatusFlag status, string[] cities, string[] buildings)
        {
            string sql =
                @"select a.* from member a with(nolock) 
inner join building b with(nolock) 
on a.building_guid=b.guid 
where (a.status & @statusMask = @expectStatus)
and not exists(select * from subscription s where s.email = a.user_name )
";
            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("@statusMask", (int)status, DbType.Int32);
            qc.AddParameter("@expectStatus", (int)status, DbType.Int32);

            string additionalFilter = string.Empty;
            if (cities != null && cities.Length > 0)
            {
                additionalFilter += " b.city_id in (@cid0";
                qc.AddParameter("@cid0", int.Parse(cities[0]), DbType.Int32);
                for (int i = 1; i < cities.Length; i++)
                {
                    string pn = "@cid" + i;
                    additionalFilter += string.Format("," + pn);
                    qc.AddParameter(pn, int.Parse(cities[i]), DbType.Int32);
                }
                additionalFilter += ")";
            }

            if (buildings != null && buildings.Length > 0)
            {
                if (additionalFilter != string.Empty)
                    additionalFilter += " or";
                additionalFilter += " b.guid in (@bid0";
                qc.AddParameter("@bid0", new Guid(buildings[0]), DbType.Guid);
                for (int i = 1; i < buildings.Length; i++)
                {
                    string pn = "@bid" + i;
                    additionalFilter += string.Format("," + pn);
                    qc.AddParameter(pn, new Guid(buildings[i]), DbType.Guid);
                }
                additionalFilter += ")";
            }

            qc.CommandSql += !string.IsNullOrEmpty(additionalFilter) ? "and (" + additionalFilter + ")" : "";
            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListDeliverableBySeller(MemberStatusFlag status, params Guid[] sellers)
        {
            QueryCommand qc = GetMemberStatusWhereQC("a.status", status, status, "seller_guid in (" + string.Join(",", (from x in sellers select x.ToString()).ToArray()) + ")");
            qc.CommandSql = qc.CommandSql.Replace("[seller_guid]", "[c].[seller_guid]");
            qc.CommandSql = @"select a.* from member a with(nolock) where a.building_guid in (select distinct b.building_guid from building_delivery b with(nolock) inner join business_hour c with(nolock) on b.business_hour_GUID=c.GUID" + qc.CommandSql + ")";
            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListDeliverableBySellerWithOutSubscription(MemberStatusFlag status, params Guid[] sellers)
        {
            QueryCommand qc = GetMemberStatusWhereQC("a.status", status, status, "seller_guid in (" + string.Join(",", (from x in sellers select x.ToString()).ToArray()) + ")");
            qc.CommandSql = qc.CommandSql.Replace("[seller_guid]", "[c].[seller_guid]");
            qc.CommandSql = @"select a.* from member a with(nolock) where a.building_guid in (select distinct b.building_guid from building_delivery b with(nolock) inner join business_hour c with(nolock) on b.business_hour_GUID=c.GUID" + qc.CommandSql + ") and  not exists(select * from subscription s with(nolock) where s.email = a.user_name )  ";
            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListByName(string name)
        {
            QueryCommand qc = new QueryCommand(@"SELECT * FROM member WITH(NOLOCK) WHERE last_name + first_name LIKE N'@name' ", Member.Schema.Provider.Name);
            qc.AddParameter("@name", name, DbType.String);
            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListByRole(string roleName)
        {
            string sql = @"
                select member.* from member with(nolock) 
                    inner join role_member with(nolock) on role_member.user_id = member.unique_id
                    inner join role with(nolock) on role.GUID = role_member.role_guid
                    where role.role_name = @roleName

            ";

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("roleName", roleName, DbType.String);

            MemberCollection memCol = new MemberCollection();
            memCol.LoadAndCloseReader(DataService.GetReader(qc));
            return memCol;
        }

        private QueryCommand GetMemberStatusWhereQC(string statusColumn, MemberStatusFlag statusMask, MemberStatusFlag expectStatus, params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<Member>(filter);

            if (statusMask != MemberStatusFlag.None)
            {
                if (qc.Parameters.Count == 0)
                    qc.CommandSql += SqlFragment.WHERE;
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += " (" + statusColumn + " & @statusMask ) = @expectStatus";
                qc.AddParameter("@statusMask", statusMask, DbType.Int32);
                qc.AddParameter("@expectStatus", expectStatus, DbType.Int32);
            }

            return qc;
        }

        public ViewMemberBuildingCity ViewMemberBuildingCityGet(string username)
        {
            return new ViewMemberBuildingCity(ViewMemberBuildingCity.Columns.UserName, username);
        }

        public ViewMemberBuildingCityCollection ViewMemberBuildingCityGetListPaging(int pageStart, int pageLength, string orderBy, string filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewMemberBuildingCity, ViewMemberBuildingCityCollection>(pageStart, pageLength, orderBy, filter);
        }

        public MemberCollection MemberGetListByStatus(MemberStatusFlag status, MemberStatusFlag expectStatus, string orderBy)
        {
            QueryCommand qc = GetMemberStatusWhereQC(Member.Columns.Status, status, expectStatus, null);
            qc.CommandSql = "select * from " + Member.Schema.Provider.DelimitDbName(Member.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
                qc.CommandSql += "order by " + orderBy;
            MemberCollection mcol = new MemberCollection();
            mcol.LoadAndCloseReader(DataService.GetReader(qc));
            return mcol;
        }

        public MemberCollection MemberGetListByStatusWithOutSubscription(MemberStatusFlag status, MemberStatusFlag expectStatus, string orderBy)
        {
            QueryCommand qc = GetMemberStatusWhereQC(Member.Columns.Status, status, expectStatus, null);
            qc.CommandSql = "select * from " + Member.Schema.Provider.DelimitDbName(Member.Schema.TableName) + " a " + " with(nolock) " + qc.CommandSql +
                " and not exists(select * from subscription s with(nolock) where s.email = a.user_name )  ";
            if (!string.IsNullOrEmpty(orderBy))
                qc.CommandSql += "order by " + orderBy;
            MemberCollection mcol = new MemberCollection();
            mcol.LoadAndCloseReader(DataService.GetReader(qc));
            return mcol;
        }

        public int MemberGetCount(string filter)
        {
            SubSonic.Query qry = ViewMemberBuildingCity.Query();
            if (!string.IsNullOrEmpty(filter))
                qry = qry.WHERE(filter);
            return qry.GetCount(ViewMemberBuildingCity.Columns.UserName);
        }

        public void MemberSetCollectDealExpireMessage(int memberUniqueId, bool isEnable)
        {
            DB.Update<Member>().Set(Member.CollectDealExpireMessageColumn).EqualTo(isEnable)
                .Where(Member.UniqueIdColumn).IsEqualTo(memberUniqueId).Execute();
        }

        public MemberCollection MemberGetListByCollectDealBid(Guid businessHourGuid, MemberCollectDealExpireLogSendType type)
        {
            string sql = @"select m.* from member m with(nolock) inner join member_collect_deal mcd with(nolock)
                On m.unique_id = mcd.member_unique_id
                left join member_collect_deal_expire_log el with(nolock)
                On mcd.member_unique_id = el.member_unique_id 
                AND mcd.business_hour_guid = el.business_hour_guid
                AND el.send_type = @sendType
                where m.collect_deal_expire_message = 1
                AND mcd.business_hour_guid = @bid
                AND mcd.collect_status = @CollectStatus
                AND el.member_unique_id is null
                AND m.user_email != ''";

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("@bid", businessHourGuid, DbType.Guid);
            qc.AddParameter("@sendType", (byte)type, DbType.Byte);
            qc.AddParameter("@CollectStatus", (int)MemberCollectDealStatus.Collected, DbType.Int16);

            MemberCollection mc = new MemberCollection();
            mc.LoadAndCloseReader(DataService.GetReader(qc));
            return mc;
        }

        public MemberCollection MemberGetListBySql(string sql)
        {
            var qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            var result = new MemberCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public void MemberPicSet(int userId, string pic)
        {
            DB.Update<Member>().Set(Member.PicColumn).EqualTo(pic)
                  .Where(Member.UniqueIdColumn).IsEqualTo(userId).Execute();
        }
        #endregion

        #region MemberProperty

        public MemberProperty MemberPropertyGetByUserId(int userId)
        {
            return DB.Get<MemberProperty>(MemberProperty.Columns.UserId, userId);
        }

        public void MemberPropertySet(MemberProperty memProp)
        {
            DB.Save(memProp);
        }

        public void MemberPropertyDelete(int userId)
        {
            try
            {
                var mp = MemberPropertyGetByUserId(userId);
                if (mp.IsLoaded)
                {
                    DB.Delete(mp);
                }
            }
            catch (Exception ex)
            {
                log.WarnFormat("MemberPropertyDelete error, userId={0}, ex={1}", userId, ex);
            }
        }

        #endregion

        #region einvoiceTriple



        public EinvoiceTriple EinvoiceTripleGetByUserid(int uniqueId)
        {
            return DB.Get<EinvoiceTriple>(EinvoiceTriple.Columns.UserId, uniqueId);
        }

        public bool SaveEinvoiceTriple(EinvoiceTriple tripleData)
        {
            DB.Save<EinvoiceTriple>(tripleData);
            return true;
        }

        public bool EinvoiceTripleDelete(int Id)
        {
            DB.Delete<EinvoiceTriple>(EinvoiceTriple.Columns.Id, Id);
            return true;
        }
        public EinvoiceWinner EinvoiceWinnerGetByUserid(int uniqueId)
        {
            return DB.Get<EinvoiceWinner>(EinvoiceTriple.Columns.UserId, uniqueId);
        }

        public bool SaveEinvoiceWinner(EinvoiceWinner winnerData)
        {
            DB.Save<EinvoiceWinner>(winnerData);
            return true;
        }

        #endregion

        #region Role

        public void RoleSet(Role role)
        {
            DB.Save(role);
        }

        public Role RoleGet(string roleName)
        {
            return DB.Get<Role>(Role.Columns.RoleName, roleName);
        }

        public void RoleDeleteByName(string roleName)
        {
            DB.Delete<Role>(Role.Columns.RoleName, roleName);
        }

        public RoleCollection RoleGetList()
        {
            RoleCollection roleCol = new Select().From(Role.Schema).OrderAsc(Role.Columns.RoleName)
                .ExecuteAsCollection<RoleCollection>();
            return roleCol;
        }

        public RoleCollection RoleGetList(string userName)
        {
            string sql = @"select role.role_name from role_member rm
inner join member on member.unique_id = rm.user_id
inner join role on role.guid = rm.role_guid
where member.user_name = @userName";

            QueryCommand qc = new QueryCommand(sql, Role.Schema.Provider.Name);
            qc.AddParameter("userName", userName, DbType.String);
            RoleCollection roleCol = new RoleCollection();
            roleCol.LoadAndCloseReader(DataService.GetReader(qc));
            return roleCol;
        }
        public RoleCollection RoleGetList(int userId)
        {
            string sql = @"select role.role_name from role_member rm
inner join member on member.unique_id = rm.user_id
inner join role on role.guid = rm.role_guid
where rm.user_id = @userId";

            QueryCommand qc = new QueryCommand(sql, Role.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            RoleCollection roleCol = new RoleCollection();
            roleCol.LoadAndCloseReader(DataService.GetReader(qc));
            return roleCol;
        }

        public void RoleMemberDelete(string userName, string[] roleNames)
        {
            string sql = @"delete from role_member where role_member.id in 
(select role_member.id from role_member
inner join member on member.unique_id = role_member.user_id
inner join role on role.guid = role_member.role_guid
where member.user_name = @userName 
	and role.role_name in ($roleNames)
)";
            //TODO it's has sql injection issue?
            sql = sql.Replace("$roleNames", "'" + string.Join("','", roleNames) + "'");
            QueryCommand qc = new QueryCommand(sql, RoleMember.Schema.Provider.Name);
            qc.AddParameter("userName", userName, DbType.String);
            DB.Execute(qc);
        }

        public void RoleMemberDelete(int userId, string[] roleNames)
        {
            string sql = string.Format(@"delete from role_member where role_member.id in 
(select role_member.id from role_member
inner join member on member.unique_id = role_member.user_id
inner join role on role.guid = role_member.role_guid
where role_member.user_id = @userId 
	and role.role_name in ({0})
)", "'" + string.Join("','", roleNames) + "'");

            QueryCommand qc = new QueryCommand(sql, RoleMember.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            DB.Execute(qc);
        }

        public void RoleMemberDelete(int userId)
        {
            string sql = @"delete from role_member where role_member.id in 
(select role_member.id from role_member
inner join member on member.unique_id = role_member.user_id
inner join role on role.guid = role_member.role_guid
where role_member.user_id = @userId)";

            QueryCommand qc = new QueryCommand(sql, RoleMember.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            DB.Execute(qc);
        }

        public void RoleMemberSet(RoleMember rm)
        {
            DB.Save(rm);
        }

        #endregion

        #region ViewRoleMember

        public ViewRoleMemberCollection ViewRoleMemberCollectionGet(string roleName)
        {
            return DB.SelectAllColumnsFrom<ViewRoleMember>().NoLock()
                .Where(ViewRoleMember.Columns.RoleName).IsEqualTo(roleName)
                .ExecuteAsCollection<ViewRoleMemberCollection>();
        }

        #endregion

        #region Member Auth Info

        public bool MemberAuthInfoSet(MemberAuthInfo memberAuthInfo)
        {
            if (memberAuthInfo != null && (memberAuthInfo.IsDirty || memberAuthInfo.IsNew))
                DB.Save<MemberAuthInfo>(memberAuthInfo);
            return true;
        }

        public MemberAuthInfo MemberAuthInfoGet(int uniqueId)
        {
            return DB.Get<MemberAuthInfo>(uniqueId);
        }

        public MemberAuthInfo MemberAuthInfoGetByEmail(string email)
        {
            return DB.Get<MemberAuthInfo>(MemberAuthInfo.Columns.Email, email);
        }

        public MemberAuthInfo MemberAuthInfoGetByKey(string authKey)
        {
            return DB.Get<MemberAuthInfo>(MemberAuthInfo.Columns.AuthKey, authKey);
        }

        public bool MemberAuthInfoDelete(int userId)
        {
            var mai = MemberAuthInfoGet(userId);
            if (mai.IsLoaded)
            {
                try
                {
                    DB.Delete(mai);
                    return true;
                }
                catch (Exception ex)
                {
                    log.WarnFormat("MemberAuthInfoDelete {0}, ex={1}", userId, ex);
                    return false;
                }
            }
            return false;
        }

        #endregion

        #region MemberDelivery
        public bool MemberDeliverySet(MemberDelivery memberdelivery)
        {
            if (memberdelivery.UserId == 0)
            {
                log.WarnFormat("操作 MemberDeliverySet 發生異常 id={0}",
                    memberdelivery.Id);
            }
            if (memberdelivery != null && (memberdelivery.IsDirty || memberdelivery.IsNew))
            {
                DB.Save<MemberDelivery>(memberdelivery);
            }
            return true;
        }
        public MemberDelivery SetMemberDelivery(MemberDelivery memberdelivery)
        {
            MemberDeliverySet(memberdelivery);
            return memberdelivery;
        }

        public MemberDelivery MemberDeliveryGet(int id)
        {
            return DB.Get<MemberDelivery>(id);
        }

        public MemberDeliveryCollection MemberDeliveryGetCollection(int memberId, bool IsDefault)
        {
            return new Select().From(MemberDelivery.Schema).Where(MemberDelivery.Columns.UserId)
                .IsEqualTo(memberId).And(MemberDelivery.IsDefaultColumn).IsEqualTo(IsDefault)
                .ExecuteAsCollection<MemberDeliveryCollection>();
        }

        public MemberDeliveryCollection MemberDeliveryGetCollection(int memberId)
        {
            return new Select().From(MemberDelivery.Schema).Where(MemberDelivery.Columns.UserId)
                .IsEqualTo(memberId).OrderDesc(MemberDelivery.Columns.Id).ExecuteAsCollection<MemberDeliveryCollection>();
        }

        public bool MemberDeliveryDelete(string id)
        {
            DB.Destroy<MemberDelivery>(MemberDelivery.Columns.Id, id);
            return true;
        }

        public bool MemberDeliveryDelete(MemberDelivery delivery)
        {
            DB.Delete(delivery);
            return true;
        }

        #endregion

        #region MemberCheckoutInfo

        public void MemberCheckoutInfoUpdateByStoreId(string storeId, ServiceChannel serverChannel)
        {
            string sql = string.Empty;
            switch (serverChannel)
            {
                case ServiceChannel.FamilyMart:

                    sql = " update " + MemberCheckoutInfo.Schema.TableName
                                 + " set " + MemberCheckoutInfo.FamilyStoreIdColumn + " = '', "
                                 + MemberCheckoutInfo.FamilyStoreNameColumn + " = '', "
                                 + MemberCheckoutInfo.FamilyStoreTelColumn + " = '',"
                                 + MemberCheckoutInfo.FamilyStoreAddrColumn + " = '' "
                                 + "where " + MemberCheckoutInfo.FamilyStoreIdColumn + " = @storeId ";
                    break;
                case ServiceChannel.SevenEleven:

                    sql = " update " + MemberCheckoutInfo.Schema.TableName
                                 + " set " + MemberCheckoutInfo.SevenStoreIdColumn + " = '', "
                                 + MemberCheckoutInfo.SevenStoreNameColumn + " = '', "
                                 + MemberCheckoutInfo.SevenStoreTelColumn + " = '',"
                                 + MemberCheckoutInfo.SevenStoreAddrColumn + " = '' "
                                 + "where " + MemberCheckoutInfo.SevenStoreIdColumn + " = @storeId ";
                    break;
            }
            QueryCommand qc = new QueryCommand(sql, MemberCheckoutInfo.Schema.Provider.Name);
            qc.AddParameter("@storeId", storeId, DbType.String);

            DataService.ExecuteScalar(qc);
        }

        public void MemberCheckoutInfoSet(MemberCheckoutInfo info)
        {
            DB.Save(info);
        }

        public MemberCheckoutInfo MemberCheckoutInfoGet(int userId)
        {
            return DB.SelectAllColumnsFrom<MemberCheckoutInfo>().NoLock()
                .Where(MemberCheckoutInfo.Columns.UserId).IsEqualTo(userId)
                .ExecuteSingle<MemberCheckoutInfo>();
        }

        public MemberCheckoutInfoCollection MemberCheckoutInfoCollectionGet()
        {
            return DB.SelectAllColumnsFrom<MemberCheckoutInfo>().NoLock()
                  .ExecuteAsCollection<MemberCheckoutInfoCollection>();
        }

        public MemberCheckoutInfo MemberCheckoutInfoGetByStoreId(string storeId, ServiceChannel serverChannel)
        {
            switch (serverChannel)
            {
                case ServiceChannel.FamilyMart:
                    return DB.SelectAllColumnsFrom<MemberCheckoutInfo>().NoLock()
                          .Where(MemberCheckoutInfo.FamilyStoreIdColumn).IsEqualTo(storeId)
                          .ExecuteSingle<MemberCheckoutInfo>();
                case ServiceChannel.SevenEleven:
                    return DB.SelectAllColumnsFrom<MemberCheckoutInfo>().NoLock()
                          .Where(MemberCheckoutInfo.SevenStoreIdColumn).IsEqualTo(storeId)
                          .ExecuteSingle<MemberCheckoutInfo>();
            }
            return new MemberCheckoutInfo();
        }


        #endregion 

        #region MemberLink
        public MemberLink MemberLinkGet(int userid, SingleSignOnSource sourceOrg)
        {
            return new Select().From(MemberLink.Schema.TableName).Where(MemberLink.UserIdColumn).IsEqualTo(userid)
                .And(MemberLink.ExternalOrgColumn).IsEqualTo((int)sourceOrg)
                .ExecuteSingle<MemberLink>();
        }

        public MemberLink MemberLinkGetByExternalId(string externId)
        {
            return DB.SelectAllColumnsFrom<MemberLink>().Where(MemberLink.ExternalUserIdColumn).IsEqualTo(externId)
                .ExecuteSingle<MemberLink>();
        }

        public MemberLink MemberLinkGetByExternalId(string externId, SingleSignOnSource sourceOrg)
        {
            return DB.SelectAllColumnsFrom<MemberLink>().Where(MemberLink.ExternalUserIdColumn).IsEqualTo(externId)
                .And(MemberLink.ExternalOrgColumn).IsEqualTo((int)sourceOrg)
                .ExecuteSingle<MemberLink>();
        }

        public MemberLinkCollection MemberLinkGetList(int userId)
        {
            return DB.SelectAllColumnsFrom<MemberLink>().Where(MemberLink.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<MemberLinkCollection>();
        }

        public MemberLinkCollection MemberLinkGetList(string userName)
        {
            string sql = "select * from member_link inner join member on member.unique_id = member_link.user_id where member.user_name = @userName";
            QueryCommand qc = new QueryCommand(sql, MemberLink.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);
            MemberLinkCollection mlCol = new MemberLinkCollection();
            mlCol.LoadAndCloseReader(DataService.GetReader(qc));
            return mlCol;
        }

        public bool MemberLinkSet(MemberLink link)
        {
            if (!link.IsDirty)
                return false;
            if (link.UserId == 0)
            {
                log.WarnFormat("儲存 MemberLinkSet 發生異常 extId={0}, soruce={1}",
                    link.ExternalUserId, (SingleSignOnSource)link.ExternalOrg);
            }

            if (string.IsNullOrEmpty(link.ExternalUserId) == false && link.ExternalUserId.Contains('\''))
            {
                log.Warn("儲存 MemberLinkSet 偵測到奇怪的符號", LogWith.LoginIdAndUrl);
            }
            DB.Save<MemberLink>(link);
            return true;
        }

        public void MemberLinkDelete(int userId, SingleSignOnSource singleSignOnSource)
        {
            DB.Delete().From<MemberLink>()
                .Where(MemberLink.Columns.UserId).IsEqualTo(userId)
                .And(MemberLink.Columns.ExternalOrg).IsEqualTo((int)singleSignOnSource)
                .Execute();
        }

        public void MemberLinkDelete(int userId)
        {
            DB.Delete().From<MemberLink>()
                .Where(MemberLink.Columns.UserId).IsEqualTo(userId)
                .Execute();
        }

        #endregion

        #region PcashMemberLink
        public bool PCashMemberLinkSet(PcashMemberLink link)
        {
            DB.Save<PcashMemberLink>(link);
            return true;
        }

        public PcashMemberLink PcashMemberLinkGet(int userid)
        {
            return new Select().From(PcashMemberLink.Schema.TableName)
                .Where(PcashMemberLink.UserIdColumn).IsEqualTo(userid)
                .ExecuteSingle<PcashMemberLink>();
        }

        public void UnbindPcashMemberLink(int userId)
        {
            PcashMemberLink ml = DB.Get<PcashMemberLink>(PcashMemberLink.Columns.UserId, userId);
            if (ml.IsLoaded)
            {
                DB.Delete(ml);
            }
        }
        #endregion

        #region 第三方支付 MemberToken

        public MemberToken MemberTokenGet(int userId, ThirdPartyPayment paymentOrg)
        {
            return DB.SelectAllColumnsFrom<MemberToken>()
                .Where(MemberToken.Columns.UserId).IsEqualTo(userId)
                .And(MemberToken.Columns.PaymentOrg).IsEqualTo((int)paymentOrg)
                .ExecuteSingle<MemberToken>();
        }

        public int MemberTokenSet(MemberToken data)
        {
            data.ModifyTime = DateTime.Now;
            return DB.Save(data);
        }

        public int MemberTokenDelete(int userId, ThirdPartyPayment paymentOrg)
        {
            return DB.Delete().From(MemberToken.Schema.TableName)
                .Where(MemberToken.Columns.UserId).IsEqualTo(userId)
                .And(MemberToken.Columns.PaymentOrg).IsEqualTo((int)paymentOrg)
                .Execute();
        }

        #endregion

        #region BonusTransaction

        public bool BonusTransactionSetToNewUser(string origUserName, string newUserName)
        {
            DB.Update<BonusTransaction>().Set(BonusTransaction.UserNameColumn).EqualTo(newUserName).Where(BonusTransaction.UserNameColumn)
               .IsEqualTo(origUserName).Execute();
            return true;
        }
        #endregion

        #region MemberPromotionDeposit
        public bool MemberPromotionDepositSet(MemberPromotionDeposit data)
        {
            if (data.UserId == 0)
            {
                log.WarnFormat("MemberPromotionDeposit 有問題.不預期會有 UserId 為 0 的操作. OrderGuid={0}, CreateId={1}, CreateTime={2} ",
                    data.OrderGuid, data.CreateId, data.CreateTime);
            }
            DB.Save<MemberPromotionDeposit>(data);
            return true;
        }

        public MemberPromotionDepositCollection MemberPromotionDepositGetListByPponDealOrderGuid(Guid pponDealOrderGuid)
        {
            string sql = " select * from " + MemberPromotionDeposit.Schema.TableName + " where " + MemberPromotionDeposit.OrderGuidColumn + " in " +
             " (select " + Order.GuidColumn + " from [" + Order.Schema.TableName + "] where " +
             Order.ParentOrderIdColumn + " = @parentOrderId ) and action = @action ";
            QueryCommand qc = new QueryCommand(sql, MemberPromotionDeposit.Schema.Provider.Name);
            qc.AddParameter("@parentOrderId", pponDealOrderGuid, DbType.Guid);
            qc.AddParameter("@action", I18N.Phrase.ReferenceBonusActionForReferencedUser, DbType.String);

            MemberPromotionDepositCollection data = new MemberPromotionDepositCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public MemberPromotionDepositCollection MemberPromotionDepositGetListByOrderGuid(Guid orderGuid)
        {
            return new Select().From(MemberPromotionDeposit.Schema).Where(MemberPromotionDeposit.OrderGuidColumn).
                    IsEqualTo(orderGuid).ExecuteAsCollection<MemberPromotionDepositCollection>();
        }


        public MemberPromotionDepositCollection MemberPromotionDepositGetListByVendorPaymentChangeIds(IEnumerable<int> vpcIds)
        {
            return new Select().From(MemberPromotionDeposit.Schema)
                               .Where(MemberPromotionDeposit.VendorPaymentChangeIdColumn).In(vpcIds)
                               .ExecuteAsCollection<MemberPromotionDepositCollection>();
        }

        public MemberNoneDepositCollection MemberNoneDepositGetListByVendorPaymentChangeIds(IEnumerable<int> vpcIds)
        {
            return new Select().From(MemberNoneDeposit.Schema)
                               .Where(MemberNoneDeposit.VendorPaymentChangeIdColumn).In(vpcIds)
                               .ExecuteAsCollection<MemberNoneDepositCollection>();
        }
        public MemberNoneDepositCollection MemberNoneDepositGetListByOids(Guid Oids)
        {
            return new Select().From(MemberNoneDeposit.Schema)
                               .Where(MemberNoneDeposit.OrderGuidColumn).In(Oids)
                               .ExecuteAsCollection<MemberNoneDepositCollection>();
        }

        public MemberPromotionDepositCollection MemberPromotionDepositGetListByOids(Guid Oids)
        {
            return new Select().From(MemberPromotionDeposit.Schema)
                               .Where(MemberPromotionDeposit.OrderGuidColumn).In(Oids)
                               .ExecuteAsCollection<MemberPromotionDepositCollection>();
        }

        public int MemberPromotionDepositSetList(MemberPromotionDepositCollection mpdc)
        {
            return DB.SaveAll3(mpdc);
        }

        #endregion

        #region MemberPromotionWithdrawal
        public bool MemberPromotionWithdrawalSet(MemberPromotionWithdrawal data)
        {
            DB.Save<MemberPromotionWithdrawal>(data);
            return true;
        }

        public bool MemberPromotionWithdrawalCollectionSet(MemberPromotionWithdrawalCollection data)
        {
            DB.SaveAll<MemberPromotionWithdrawal, MemberPromotionWithdrawalCollection>(data);
            return true;
        }

        public MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetListByOrderGuid(Guid orderGuid)
        {
            return new Select().From(MemberPromotionWithdrawal.Schema).Where(MemberPromotionWithdrawal.OrderGuidColumn).
                    IsEqualTo(orderGuid).ExecuteAsCollection<MemberPromotionWithdrawalCollection>();
        }

        public MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetListByPponDealOrderGuid(Guid pponDealOrderGuid)
        {
            string sql = " select * from " + MemberPromotionWithdrawal.Schema.TableName + " where " + MemberPromotionWithdrawal.OrderGuidColumn + " in " +
                         " (select " + Order.GuidColumn + " from [" + Order.Schema.TableName + "] where " +
                         Order.ParentOrderIdColumn + " = @parentOrderId )";
            QueryCommand qc = new QueryCommand(sql, MemberPromotionWithdrawal.Schema.Provider.Name);
            qc.AddParameter("@parentOrderId", pponDealOrderGuid, DbType.Guid);

            MemberPromotionWithdrawalCollection data = new MemberPromotionWithdrawalCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public MemberPromotionDepositCollection MemberPromotionDepositGetAvailableList(int userId)
        {
            string sql = @"
select mpd.*
 from member_promotion_deposit mpd
where mpd.user_id = @userId 
and mpd.type = 0
and getdate() > mpd.start_time
and getdate() < mpd.expire_time
order by mpd.expire_time asc
";
            MemberPromotionDepositCollection mpdCol = new MemberPromotionDepositCollection();
            QueryCommand qc = new QueryCommand(sql, MemberPromotionDeposit.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            mpdCol.LoadAndCloseReader(DataService.GetReader(qc));
            return mpdCol;
        }

        public MemberPromotionWithdrawalCollection MemberPromotionWithdrawalGetAvailableList(int userId)
        {
            string sql = @"
select mpw.* from
member_promotion_withdrawal mpw
inner join member_promotion_deposit mpd on mpd.id = mpw.deposit_ID
and getdate() > mpd.start_time
and getdate() < mpd.expire_time 
where mpd.user_id = @userId 
and mpd.type = 0 and mpw.type <> 5

";
            MemberPromotionWithdrawalCollection mpdCol = new MemberPromotionWithdrawalCollection();
            QueryCommand qc = new QueryCommand(sql, MemberPromotionDeposit.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            mpdCol.LoadAndCloseReader(DataService.GetReader(qc));
            return mpdCol;
        }

        public double MemberPromotionWithdralSumByDeposit(int depositId)
        {
            string sql2 = @"
select ISNULL(
(
select sum(mpw.promotion_value) from member_promotion_withdrawal mpw
inner join member_promotion_deposit mpd on mpd.id = mpw.deposit_ID
inner join member m on m.unique_id = mpd.user_id
where mpd.id = @depositId
and mpd.type = 0
and getdate() > mpd.start_time
and getdate() < mpd.expire_time
and mpw.type <> 5
), 0)
";
            QueryCommand qc2 = new QueryCommand(sql2, MemberPromotionDeposit.Schema.Provider.Name);
            qc2.AddParameter("@depositId", depositId, DbType.Int32);
            double withdralAmount = Convert.ToDouble(DataService.ExecuteScalar(qc2));
            return withdralAmount;
        }

        public int MemberBounsGetGivenTotal(Guid actionGroupGuid)
        {
            string sql = @"
            select isnull(sum(mpd.promotion_value),0)/10 from member_promotion_deposit mpd with(nolock)
                where mpd.action_group_guid = @actionGroupGuid
";
            QueryCommand qc = new QueryCommand(sql, MemberPromotionDeposit.Schema.Provider.Name);
            qc.AddParameter("@actionGroupGuid", actionGroupGuid, DbType.Guid);
            int result = Convert.ToInt32(DataService.ExecuteScalar(qc));
            return result;
        }

        public int MemberBounsGetUsedTotal(Guid actionGroupGuid)
        {
            string sql = @"
                select isnull(sum(mpw.promotion_value), 0)/10 from member_promotion_withdrawal mpw with(nolock)
                where mpw.deposit_ID in (
	                select mpd.id from member_promotion_deposit mpd with(nolock)
	                where mpd.action_group_guid = @actionGroupGuid
                )
";
            QueryCommand qc = new QueryCommand(sql, MemberPromotionDeposit.Schema.Provider.Name);
            qc.AddParameter("@actionGroupGuid", actionGroupGuid, DbType.Guid);
            int result = Convert.ToInt32(DataService.ExecuteScalar(qc));
            return result;
        }


        #endregion

        #region ViewMemberPromotionTransaction

        public decimal ViewMemberPromotionTransactionGetSum(string username, MemberPromotionType memberPromotionType)
        {
            string sql = @"
select sum(t.balance) from view_member_promotion_transaction t with(nolock)
where t.member_user_name = @userName
and t.deposit_or_withdrawal = '1' 
and t.promotion_type = @promotionType
and GETDATE() >= t.start_time  and GETDATE() <= t.expire_time
";
            QueryCommand qc = new QueryCommand(sql, ViewMemberPromotionTransaction.Schema.Provider.Name);
            qc.AddParameter("@userName", username, DbType.String);
            qc.AddParameter("@promotionType", memberPromotionType, DbType.Int32);

            object obj = DataService.ExecuteScalar(qc);
            if (obj == null || obj == DBNull.Value)
            {
                return 0;
            }
            return decimal.Parse(obj.ToString());
        }

        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetList(
            string username, string orderBy, 
            MemberPromotionType memberPromotionType = MemberPromotionType.Bouns, 
            bool orderDesc = false,  int? topNum = null)
        {
            ViewMemberPromotionTransactionCollection col = new ViewMemberPromotionTransactionCollection();
            //IDataReader rdr;
            if (string.IsNullOrEmpty(orderBy))
            {
                var qry = DB.SelectAllColumnsFrom<ViewMemberPromotionTransaction>();
                if (topNum != null)
                {
                    qry.Top(topNum.ToString());
                }
                col = qry.Where(ViewMemberPromotionTransaction.Columns.MemberUserName).IsEqualTo(username)
                        .And(ViewMemberPromotionTransaction.Columns.PromotionType).IsEqualTo((int)memberPromotionType)
                        .ExecuteAsCollection<ViewMemberPromotionTransactionCollection>();
            }
            else
            {
                var qry = DB.SelectAllColumnsFrom<ViewMemberPromotionTransaction>();
                if (topNum != null)
                {
                    qry.Top(topNum.ToString());                    
                }
                if (orderDesc == false)
                {
                    col = qry.Where(ViewMemberPromotionTransaction.Columns.MemberUserName).IsEqualTo(username)
                            .And(ViewMemberPromotionTransaction.Columns.PromotionType).IsEqualTo((int)memberPromotionType)
                            .OrderAsc(orderBy)
                            .ExecuteAsCollection<ViewMemberPromotionTransactionCollection>();
                } 
                else
                {
                    col = qry.Where(ViewMemberPromotionTransaction.Columns.MemberUserName).IsEqualTo(username)
                            .And(ViewMemberPromotionTransaction.Columns.PromotionType).IsEqualTo((int)memberPromotionType)
                            .OrderDesc(orderBy)
                            .ExecuteAsCollection<ViewMemberPromotionTransactionCollection>();
                }
            }

            return col;
        }

        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetList(string orderBy, params string[] filter)
        {
            ViewMemberPromotionTransactionCollection col = new ViewMemberPromotionTransactionCollection();

            Query qry = ViewMemberPromotionTransaction.Query();
            qry = SSHelper.GetQueryByFilterOrder(qry, orderBy, filter);
            qry.CommandTimeout = 1200;
            col.LoadAndCloseReader(qry.ExecuteReader());
            return col;
        }

        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListTop(string orderBy, params string[] filter)
        {
            ViewMemberPromotionTransactionCollection col = new ViewMemberPromotionTransactionCollection();

            Query qry = ViewMemberPromotionTransaction.Query();
            qry = SSHelper.GetQueryByFilterOrder(qry, orderBy, filter);
            qry.Top = "10";
            col.LoadAndCloseReader(qry.ExecuteReader());
            return col;
        }

        /// <summary>
        /// Get the last N records from ViewMemberPromotionTransaction, by specified order or not
        /// </summary>
        /// <param name="orderBy">The order by string.</param>
        /// <param name="recCount">The record count.</param>
        /// <returns></returns>
        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListLastN(string username, string orderBy, int recCount, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            string ord_pre = null;
            string ord_post = null;

            // check orderby existence
            if (!string.IsNullOrEmpty(orderBy))
            {
                // separate column & direction, so far only 1 order by column is supported
                Regex r = new Regex(@"^(\w+)\W*((asc)|(desc))*\W*$", RegexOptions.IgnoreCase);
                Match m = r.Match(orderBy);
                if (m.Success)
                {
                    // invert directions in inner query
                    string dir = m.Groups[2].Success ? (m.Groups[2].Value.ToUpper() == "ASC" ? "DESC" : "ASC") : "DESC";
                    ord_pre = ord_post = @" order by " + m.Groups[1].Value + @" ";
                    ord_pre += dir;
                    ord_post += dir == "ASC" ? "DESC" : "ASC";
                }
            }
            else
            {
                ord_pre = ord_post = @" order by id";
                ord_pre += " DESC";
            }

            string sql = @"select * from (select top " + recCount + @" * from view_member_promotion_transaction with(nolock) where " +
            ViewMemberPromotionTransaction.Columns.MemberUserName + @"=@id and " + ViewMemberPromotionTransaction.Columns.PromotionType + @"=@promotiontype " + ord_pre + @") as tbl" + ord_post;

            QueryCommand qc = new QueryCommand(sql, ViewMemberPromotionTransaction.Schema.Provider.Name);
            qc.AddParameter("@id", username, DbType.String);
            qc.AddParameter("@promotiontype", (int)memberPromotionType, DbType.Int32);
            ViewMemberPromotionTransactionCollection data = new ViewMemberPromotionTransactionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetListLastN(int userId, string orderBy, int recCount, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            string ord_pre = null;
            string ord_post = null;

            // check orderby existence
            if (!string.IsNullOrEmpty(orderBy))
            {
                // separate column & direction, so far only 1 order by column is supported
                Regex r = new Regex(@"^(\w+)\W*((asc)|(desc))*\W*$", RegexOptions.IgnoreCase);
                Match m = r.Match(orderBy);
                if (m.Success)
                {
                    // invert directions in inner query
                    string dir = m.Groups[2].Success ? (m.Groups[2].Value.ToUpper() == "ASC" ? "DESC" : "ASC") : "DESC";
                    ord_pre = ord_post = @" order by " + m.Groups[1].Value + @" ";
                    ord_pre += dir;
                    ord_post += dir == "ASC" ? "DESC" : "ASC";
                }
            }
            else
            {
                ord_pre = ord_post = @" order by id";
                ord_pre += " DESC";
            }

            string sql = @"select * from (select top " + recCount + @" * from view_member_promotion_transaction with(nolock) where " +
            ViewMemberPromotionTransaction.Columns.MemberUserId + @"=@id and " + ViewMemberPromotionTransaction.Columns.PromotionType + @"=@promotiontype " + ord_pre + @") as tbl" + ord_post;

            QueryCommand qc = new QueryCommand(sql, ViewMemberPromotionTransaction.Schema.Provider.Name);
            qc.AddParameter("@id", userId, DbType.Int32);
            qc.AddParameter("@promotiontype", (int)memberPromotionType, DbType.Int32);
            ViewMemberPromotionTransactionCollection data = new ViewMemberPromotionTransactionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewMemberPromotionTransactionCollection ViewMemberPromotionTransactionGetOrderGroupList(int pageStart, int pageLength, string username, string orderBy, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            string defOrderBy = ViewMemberPromotionTransaction.Columns.CreateTime;
            string groupByColumn = ViewMemberPromotionTransaction.Columns.CreateTime + "," +
                                   ViewMemberPromotionTransaction.Columns.OrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.Action + "," +
                                   ViewMemberPromotionTransaction.Columns.Type + "," +
                                   ViewMemberPromotionTransaction.Columns.WithdrawalOrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.RedeemOrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.StartTime + "," +
                                   ViewMemberPromotionTransaction.Columns.ExpireTime + "," +
                                   ViewMemberPromotionTransaction.Columns.RunSum + "," +
                                   ViewMemberPromotionTransaction.Columns.CouponUsage + "," +
                                   ViewMemberPromotionTransaction.Columns.CouponEventContentName;

            string sql = "select " + groupByColumn + ", SUM(" + ViewMemberPromotionTransaction.Columns.PromotionValue +
                         ") promotion_value " +
                         " from " +
                         ViewMemberPromotionTransaction.Schema.TableName +
                         " with(nolock) where " + ViewMemberPromotionTransaction.Columns.MemberUserName + " = @UserName " +
                         " AND " + ViewMemberPromotionTransaction.Columns.PromotionType + " = @promotiontype " +
                         " group by " + groupByColumn;
            QueryCommand qc = new QueryCommand(sql, ViewMemberPromotionTransaction.Schema.Provider.Name);
            qc.AddParameter("@UserName", username, DbType.String);
            qc.AddParameter("@promotiontype", (int)memberPromotionType, DbType.Int32);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy + " desc";
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }
            ViewMemberPromotionTransactionCollection data = new ViewMemberPromotionTransactionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        public int ViewMemberPromotionTransactionGetOrderGroupListCount(string username, MemberPromotionType memberPromotionType = MemberPromotionType.Bouns)
        {
            string defOrderBy = ViewMemberPromotionTransaction.Columns.CreateTime;
            string groupByColumn = ViewMemberPromotionTransaction.Columns.CreateTime + "," +
                                   ViewMemberPromotionTransaction.Columns.OrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.Action + "," +
                                   ViewMemberPromotionTransaction.Columns.Type + "," +
                                   ViewMemberPromotionTransaction.Columns.WithdrawalOrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.RedeemOrderGuid + "," +
                                   ViewMemberPromotionTransaction.Columns.RunSum;

            string sql = "select count(1) from " +
                         ViewMemberPromotionTransaction.Schema.TableName +
                         " with(nolock) where " + ViewMemberPromotionTransaction.Columns.MemberUserName + " = @UserName " +
                         " AND " + ViewMemberPromotionTransaction.Columns.PromotionType + " = @promotiontype ";
            //"+ group by " + groupByColumn;
            //"+ order by " + orderBy;
            QueryCommand qc = new QueryCommand(sql, ViewMemberPromotionTransaction.Schema.Provider.Name);
            qc.AddParameter("@UserName", username, DbType.String);
            qc.AddParameter("@promotiontype", (int)memberPromotionType, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion

        #region ViewMemberBuildingCity
        public ViewMemberDeliveryBuildingCityCollection ViewMemberDeliveryBuildingCityGetCollection(string memberId)
        {
            ViewMemberDeliveryBuildingCityCollection col = new ViewMemberDeliveryBuildingCityCollection();
            IDataReader rdr;

            rdr = ViewMemberDeliveryBuildingCity.FetchByParameter(ViewMemberDeliveryBuildingCity.Columns.UserName, memberId);

            col.LoadAndCloseReader(rdr);
            return col;
        }

        #endregion ViewMemberBuildingCity

        #region
        public int RedeemOrderSetToNewUser(string oldUserName, string newUserName)
        {
            List<SqlQuery> queries = new List<SqlQuery>();
            queries.Add(new Update(RedeemOrder.Schema).Set(RedeemOrder.MemberEmailColumn).EqualTo(newUserName).Where(RedeemOrder.MemberEmailColumn).IsEqualTo(oldUserName));
            queries.Add(new Update(RedeemOrderDetail.Schema).Set(RedeemOrderDetail.CreateIdColumn).EqualTo(newUserName).Where(RedeemOrderDetail.CreateIdColumn).IsEqualTo(oldUserName));
            foreach (var query in queries)
            {
                query.Execute();
            }
            //SqlQuery.ExecuteTransaction(queries);
            return 0;
        }
        #endregion

        #region ServiceMessage

        public void ServiceMessageSet(ServiceMessage sc, int? userId = null)
        {
            if (userId != null)
            {
                sc.UserId = userId;
            }
            if (sc.UserId == null)
            {
                if (string.IsNullOrEmpty(sc.Phone) == false)
                {
                    MobileMember mm = MobileMemberGet(sc.Phone);
                    if (mm.IsLoaded)
                    {
                        sc.UserId = mm.UserId;
                    }
                }
            }
            if (sc.UserId == null)
            {
                Member m = MemberGet(sc.Email);
                if (m.IsLoaded)
                {
                    sc.UserId = m.UniqueId;
                }
            }
            DB.Save<ServiceMessage>(sc);
        }

        public ServiceMessage ServiceMessageGet(int id)
        {
            return DB.Get<ServiceMessage>(id);
        }

        public ServiceMessageCollection ServiceMessageGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ServiceMessage.Columns.CreateTime + " desc";
            QueryCommand qc = new QueryCommand(" ", ServiceMessage.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ServiceMessage.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select * from " + ServiceMessage.Schema.Provider.DelimitDbName(ServiceMessage.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ServiceMessageCollection vfcCol = new ServiceMessageCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public int ServiceMessageGetCountForFilter(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ServiceMessage.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ServiceMessage.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select count(1) from " + ServiceMessage.Schema.Provider.DelimitDbName(ServiceMessage.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetSMCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        public ServiceMessageCategory ServiceMessageCategoryGetByCategoryId(int cateId)
        {
            return DB.SelectAllColumnsFrom<ServiceMessageCategory>().Where(ServiceMessageCategory.Columns.CategoryId).IsEqualTo(cateId).ExecuteSingle<ServiceMessageCategory>();
        }

        public ServiceMessageCategoryCollection ServiceMessageCategoryCollectionGetListByParentId(int? parentId)
        {
            return parentId == null ? DB.SelectAllColumnsFrom<ServiceMessageCategory>().Where(ServiceMessageCategory.Columns.ParentCategoryId).IsNull()
                .OrderAsc(ServiceMessageCategory.Columns.CategoryOrder).ExecuteAsCollection<ServiceMessageCategoryCollection>()
                : DB.SelectAllColumnsFrom<ServiceMessageCategory>().Where(ServiceMessageCategory.Columns.ParentCategoryId).IsEqualTo(parentId)
                .OrderAsc(ServiceMessageCategory.Columns.CategoryOrder).ExecuteAsCollection<ServiceMessageCategoryCollection>();
        }

        #endregion

        #region ViewServiceMessageDetail

        public int ViewServiceMessageDetailGetCountForFilter(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewServiceMessageDetail.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewServiceMessageDetail.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select count(1) from " + ViewServiceMessageDetail.Schema.Provider.DelimitDbName(ViewServiceMessageDetail.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewServiceMessageDetailCollection ViewServiceMessageDetailGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewServiceMessageDetail.Columns.CreateTime + " desc";
            QueryCommand qc = new QueryCommand(" ", ViewServiceMessageDetail.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewServiceMessageDetail.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select * from " + ViewServiceMessageDetail.Schema.Provider.DelimitDbName(ViewServiceMessageDetail.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewServiceMessageDetailCollection vsmdc = new ViewServiceMessageDetailCollection();
            vsmdc.LoadAndCloseReader(DataService.GetReader(qc));
            return vsmdc;
        }

        #endregion

        #region ServiceLog
        public void ServiceLogSet(ServiceLog sl)
        {
            DB.Save<ServiceLog>(sl);
        }

        public ServiceLogCollection ServiceLogGetList(string serviceMessageId)
        {
            SqlQuery qry;

            qry = DB.SelectAllColumnsFrom<ServiceLog>().Where(ServiceLog.Columns.RefX).IsEqualTo(serviceMessageId).OrderDesc(ServiceLog.Columns.CreateTime);

            return qry.ExecuteAsCollection<ServiceLogCollection>();
        }
        #endregion

        #region Cash Trust Log

        public CashTrustLog CashTrustLogGet(Guid businessHourGuid, string couponSequenceNumber)
        {
            return DB.Select().From(CashTrustLog.Schema.TableName).NoLock()
                .Where(CashTrustLog.Columns.BusinessHourGuid).IsEqualTo(businessHourGuid)
                .And(CashTrustLog.Columns.CouponSequenceNumber).IsEqualTo(couponSequenceNumber)
                .ExecuteSingle<CashTrustLog>();
        }

        public CashTrustLog CashTrustLogGetByCouponSequenceNumber(string couponSequenceNumber)
        {
            return DB.Select().From(CashTrustLog.Schema.TableName).NoLock()
                .And(CashTrustLog.Columns.CouponSequenceNumber).IsEqualTo(couponSequenceNumber)
                .ExecuteSingle<CashTrustLog>();
        }

        public CashTrustLog CashTrustLogGet(Guid trustId)
        {
            return DB.Get<CashTrustLog>(trustId);
        }

        public bool CashTrustLogUpdateStoreGuidByTrustId(Guid trustId, Guid storeGuid)
        {
            return DB.Update<CashTrustLog>().Set(CashTrustLog.Columns.VerifiedStoreGuid).EqualTo(storeGuid)
                .Where(CashTrustLog.Columns.TrustId).IsEqualTo(trustId).Execute() > 0;
        }

        public bool CashTrustLogIsSameStoreGuidByTrustId(List<Guid> trustIds, Guid storeGuid)
        {
            var ctls = DB.Select().From(CashTrustLog.Schema.TableName).NoLock()
                 .Where(CashTrustLog.Columns.TrustId).In(trustIds)
                 .And(CashTrustLog.Columns.VerifiedStoreGuid).IsEqualTo(storeGuid)
                 .ExecuteAsCollection<CashTrustLogCollection>();
            if (ctls.Count == trustIds.Count)
            {
                return true;
            }
            return false;
        }

        public CashTrustLogCollection CashTrustLogGetListByNoReceiptPaid()
        {
            int[] dcStatus = new int[] { (int)IspDcAcceptStatus.Default, (int)IspDcAcceptStatus.Success, (int)IspDcAcceptStatus.None };
            string sql = string.Format(@"select ctl.* from order_ship os with(nolock)
inner join [order] o with(nolock) on o.guid = os.order_guid
inner join cash_trust_log ctl with(nolock) on o.guid = ctl.order_guid
where os.is_receipt = 0 and ctl.status in (6,7)  and ctl.seven_isp + family_isp > 0
and o.order_status & 8 = 0 and o.order_status & 512 = 0
UNION ALL
select ctl.* from order_product_delivery opd with(Nolock)
inner join [order] o with(nolock) on o.guid = opd.order_guid
inner join cash_trust_log ctl with(nolock) on o.guid = ctl.order_guid
where opd.dc_accept_status not in ({0}) and ctl.status in (6,7)  and ctl.seven_isp + family_isp > 0
and o.order_status & 8 = 0 and o.order_status & 512 = 0", string.Join(",", dcStatus));
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);

            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogByTrustId(List<Guid> trustIds)
        {
            return DB.Select().From(CashTrustLog.Schema.TableName).NoLock()
                 .Where(CashTrustLog.Columns.TrustId).In(trustIds)
                 .ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLog CashTrustLogGetByCouponId(int couponId, OrderClassification orderClassification = OrderClassification.LkSite)
        {
            //return DB.Get<CashTrustLog>(CashTrustLog.CouponIdColumn.ColumnName, couponId);
            return
                DB.Select().From(CashTrustLog.Schema.TableName)
                    .Where(CashTrustLog.Columns.CouponId).IsEqualTo(couponId)
                    .And(CashTrustLog.Columns.OrderClassification).IsEqualTo((int)orderClassification)
                    .ExecuteSingle<CashTrustLog>();

        }

        public CashTrustLog CashTrustLogGetByOrderId(Guid orderGuid)
        {
            return
                DB.Select().From(CashTrustLog.Schema.TableName).Where(CashTrustLog.Columns.OrderGuid).IsEqualTo(orderGuid).ExecuteSingle<CashTrustLog>();

        }

        public CashTrustLogCollection CashTrustLogListGetByOrderId(Guid orderGuid)
        {
            return
                DB.Select().From(CashTrustLog.Schema.TableName).Where(CashTrustLog.Columns.OrderGuid).In(orderGuid).ExecuteAsCollection<CashTrustLogCollection>();

        }

        public OldCashTrustLogCollection GetOldCashTrustLogCollectionByOid(Guid order_guid)
        {
            return DB.SelectAllColumnsFrom<OldCashTrustLog>().Where(OldCashTrustLog.OrderGuidColumn.ColumnName).IsEqualTo(order_guid).ExecuteAsCollection<OldCashTrustLogCollection>();
        }

        public CashTrustLogCollection GetCashTrustLogCollectionByOid(IEnumerable<Guid> order_guid)
        {
            CashTrustLogCollection infos = new CashTrustLogCollection();
            List<Guid> tempIds = order_guid.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<CashTrustLog>()
                        .Where(CashTrustLog.Columns.OrderGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<CashTrustLogCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public OldCashTrustLog OldCashTrustLogGetByCouponId(int couponId)
        {
            return DB.Get<OldCashTrustLog>(OldCashTrustLog.CouponIdColumn.ColumnName, couponId);
        }

        public CashTrustLogCollection CashTrustLogGetByTrustSequenceNumber(string trustSequenceNumber, TrustProvider provider)
        {
            string sql = " select * from " + CashTrustLog.Schema.TableName + " with(nolock) " + " where " + CashTrustLog.TrustProviderColumn + " = @provider and " +
              CashTrustLog.TrustSequenceNumberColumn + "=@trustSequenceNumber and " + CashTrustLog.BankStatusColumn + "<>" + (int)TrustBankStatus.Initial +
              " and case " + CashTrustLog.CouponIdColumn + " when null then 0 else (" + CashTrustLog.CreditCardColumn + "+" + CashTrustLog.ScashColumn + "+" + CashTrustLog.AtmColumn + "+" + CashTrustLog.LcashColumn + ") end>0";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@provider", (int)provider, DbType.Int32);
            qc.AddParameter("@trustSequenceNumber", trustSequenceNumber, DbType.String);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CashTrustLog, CashTrustLogCollection>(0, 0, orderBy, filter);
        }

        public CashTrustLogCollection CashTrustLogGetList(IEnumerable<Guid> trustIds)
        {
            CashTrustLogCollection infos = new CashTrustLogCollection();
            List<Guid> tempIds = trustIds.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<CashTrustLog>()
                        .Where(CashTrustLog.Columns.TrustId).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<CashTrustLogCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public CashTrustLogCollection CashTrustLogGetListByOrderGuid(Guid orderGuid, OrderClassification classification)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().NoLock().Where(CashTrustLog.OrderGuidColumn.ColumnName).IsEqualTo(orderGuid)
                .And(CashTrustLog.OrderClassificationColumn).IsEqualTo((int)classification)
                .ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().NoLock().Where(CashTrustLog.OrderGuidColumn.ColumnName).IsEqualTo(orderGuid)
                .ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetCouponSequencesWithUnused(Guid orderGuid)
        {
            List<TrustStatus> statusList = new List<TrustStatus>();
            statusList.Add(TrustStatus.Initial);
            statusList.Add(TrustStatus.Trusted);

            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(CashTrustLog.Columns.Status).In(statusList).ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetCouponSequencesWithUnused(Guid orderGuid, int userId)
        {
            var statusList = new List<TrustStatus> { TrustStatus.Initial, TrustStatus.Trusted };

            return DB.SelectAllColumnsFrom<CashTrustLog>().NoLock()
                .Where(CashTrustLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(CashTrustLog.Columns.UserId).IsEqualTo(userId)
                .And(CashTrustLog.Columns.Status).In(statusList).ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetListByOrderDetailGuid(Guid orderDetailGuid, List<TrustStatus> statusList,
                                                                     OrderClassification classification =
                                                                         OrderClassification.LkSite)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid).And(
                    CashTrustLog.Columns.OrderClassification).IsEqualTo(classification).And(CashTrustLog.Columns.Status).In(statusList.Select(status => (int)status).ToList())
                    .ExecuteAsCollection<CashTrustLogCollection>();

        }

        public CashTrustLogCollection CashTrustLogGetListByOrderDetailGuid(Guid orderDetailGuid, OrderClassification classification = OrderClassification.LkSite)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().NoLock()
                .Where(CashTrustLog.OrderDetailGuidColumn.ColumnName)
                .IsEqualTo(orderDetailGuid)
                .And(CashTrustLog.Columns.OrderClassification).IsEqualTo((int)classification)
                .ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetListByOrderDetailGuids(IEnumerable<Guid> orderDetailGuids, OrderClassification classification = OrderClassification.LkSite)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.OrderDetailGuidColumn.ColumnName).In(orderDetailGuids)
                .And(CashTrustLog.Columns.OrderClassification).IsEqualTo((int)classification)
                .ExecuteAsCollection<CashTrustLogCollection>() ?? new CashTrustLogCollection();
        }

        public CashTrustLogCollection CashTrustLogGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.BusinessHourGuidColumn.ColumnName).IsEqualTo(bid)
                .And(CashTrustLog.Columns.OrderClassification).IsEqualTo((int)OrderClassification.LkSite)
                .ExecuteAsCollection<CashTrustLogCollection>() ?? new CashTrustLogCollection();
        }

        public CashTrustLogCollection CashTrustLogGetListByBid(IEnumerable<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.BusinessHourGuidColumn.ColumnName).In(bids)
                .And(CashTrustLog.Columns.OrderClassification).IsEqualTo((int)OrderClassification.LkSite)
                .ExecuteAsCollection<CashTrustLogCollection>() ?? new CashTrustLogCollection();
        }

        public CashTrustLogCollection CashTrustLogGetListByBidWhereOrderIsSuccessful(Guid bid)
        {
            string sql = @"SELECT 
                                          ctlog.* 
                                        FROM cash_trust_log ctlog 
                                        INNER JOIN [order] o 
                                          ON o.GUID = ctlog.order_GUID
                                        WHERE o.order_status = o.order_status | " + (int)OrderStatus.Complete + @"
                                          AND ctlog.business_hour_guid = @bid";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        public CashTrustLogCollection CashTrustLogGetListByBidWhereOrderIsSuccessful(Guid bid, DateTime genDate)
        {
            string sql = @"SELECT 
                                          ctlog.* 
                                        FROM cash_trust_log ctlog 
                                        INNER JOIN [order] o 
                                          ON o.GUID = ctlog.order_GUID
                                        WHERE o.order_status = o.order_status | " + (int)OrderStatus.Complete + @"
                                          AND ctlog.usage_verified_time > '" + genDate.AddMonths(-3).ToString("yyyy/MM/dd HH:mm:ss") + @"'
                                          AND ctlog.business_hour_guid = @bid";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        public CashTrustLogCollection PponRefundingCashTrustLogGetListByOrderGuid(Guid orderGuid)
        {
            string sql = string.Format(@"
select ctlog.* from [order] o with(nolock)
inner join cash_trust_log ctlog with(nolock)
  on ctlog.order_guid = o.guid
outer apply (select rfr.trust_id from return_form rf with(nolock)
  inner join return_form_refund rfr with(nolock)
    on rf.id = rfr.return_form_id
    and rf.progress_status not in ({0},{1})
  where ctlog.trust_id = rfr.trust_id) refund
where o.guid=@oid
and refund.trust_id is not null
", ((int)ProgressStatus.Canceled).ToString(), ((int)ProgressStatus.Unreturnable).ToString());
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@oid", orderGuid, DbType.Guid);
            var data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public bool CashTrustLogSet(CashTrustLog cashTrustLog)
        {
            if (!cashTrustLog.IsLoaded)
                return false;
            DB.Save(cashTrustLog);
            return true;
        }

        public bool CashTrustLogSetNew(CashTrustLog cashTrustLog)
        {
            if (!cashTrustLog.IsDirty)
                return false;
            DB.Save(cashTrustLog);
            return true;
        }

        /// <summary>
        /// for憑證核銷用
        /// </summary>
        /// <param name="cashTrustLog"></param>
        /// <returns></returns>
        public bool CashTrustLogColSetForVerifyCoupons(CashTrustLogCollection cashTrustLogs)
        {
            if (!cashTrustLogs.Any(x => x.IsDirty))
                return false;

            int c = 0;
            foreach (CashTrustLog cashTrustLog in cashTrustLogs)
            {
                Query q = new Query(CashTrustLog.Schema);
                q.QueryType = QueryType.Update;

                q.WHERE(CashTrustLog.Columns.ModifyTime, Comparison.LessOrEquals, cashTrustLog.ModifyTime);
                q.AND(CashTrustLog.Columns.TrustId, cashTrustLog.TrustId);

                foreach (var f in cashTrustLog.DirtyColumns)
                {
                    q.AddUpdateSetting(f.ColumnName, cashTrustLog.GetPropertyValue(f.PropertyName) ?? DBNull.Value);
                }

                //預期query更新的數量
                c += q.GetRecordCount();
                q.Execute();
            }

            return c == cashTrustLogs.Count;
        }

        public bool OldCashTrustLogSet(OldCashTrustLog oldCashTrustLog)
        {
            if (!oldCashTrustLog.IsLoaded)
                return false;
            DB.Save(oldCashTrustLog);
            return true;
        }

        public bool CashTrustLogCollectionSet(CashTrustLogCollection cashTrustLogCollection)
        {
            DB.SaveAll<CashTrustLog, CashTrustLogCollection>(cashTrustLogCollection);
            return true;
        }

        //new
        //取出所有總數
        //**************

        public CashTrustLogCollection CashTrustLogGetListForRemain(TrustProvider provider)
        {
            string sql = " select * from " + CashTrustLog.Schema.TableName + " with(nolock) where " + CashTrustLog.TrustProviderColumn + " = @provider and " +
                CashTrustLog.BankStatusColumn + "=@status and (" +
                CashTrustLog.CreditCardColumn + " + " + CashTrustLog.ScashColumn + "+" + CashTrustLog.AtmColumn + ")>0";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@provider", (int)provider, DbType.Int32);
            qc.AddParameter("@status", (int)TrustBankStatus.Initial | (int)TrustBankStatus.Trusted, DbType.Int32);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void CashTrustLogToUpdateFromWPF(Guid trust_id)
        {
            string sql = "update " + CashTrustLog.Schema.TableName + " set " + CashTrustLog.Columns.Status + "=@status_verified,"
                + CashTrustLog.Columns.ModifyTime + "=getdate() where " + CashTrustLog.Columns.TrustId + "=@trustid and " + CashTrustLog.Columns.Status + "<@status_verified;";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@status_verified", (int)TrustStatus.Verified, DbType.Int32);
            qc.AddParameter("@trustid", trust_id, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public CashTrustLogCollection CashTrustLogGetListByOrderCreateTime(DateTime dateStart, DateTime dateEnd, TrustProvider provider)
        {
            string sql = @" SELECT ctl.* from dbo.cash_trust_log ctl with(nolock)
                            WHERE ctl.order_guid in (
                                select o.GUID from dbo.[order] o with(nolock)
                                where o.create_time BETWEEN @dateS and @dateE)
                            and ctl.trust_provider = @trust_provider
                            order by ctl.create_time ";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@dateS", dateStart, DbType.DateTime);
            qc.AddParameter("@dateE", dateEnd, DbType.DateTime);
            qc.AddParameter("@trust_provider", (int)provider, DbType.Int32);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogGreaterThanStatusGetListByPeriod(DateTime dateStart, DateTime dateEnd, TrustProvider provider, TrustStatus status)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.Columns.Status).IsGreaterThanOrEqualTo((int)status).And(CashTrustLog.Columns.ModifyTime).IsBetweenAnd(dateStart, dateEnd).And(CashTrustLog.TrustProviderColumn.ColumnName).IsEqualTo(provider).ExecuteAsCollection<CashTrustLogCollection>();
        }

        public CashTrustLogCollection CashTrustLogGetListByHiDealProductId(int productId)
        {
            string sql = @"SELECT ctl.* 
                         FROM (SELECT id, product_id, order_detail_guid
                         FROM dbo.hi_deal_coupon
                         WHERE status > 0) AS c INNER JOIN
                         dbo.cash_trust_log AS ctl ON ctl.coupon_id = c.id AND ctl.order_detail_guid = c.order_detail_guid 
                         WHERE ctl.order_classification = 2 and c.product_id = @productId ";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@productId", productId, DbType.Int32);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogGetListByBalanceSheetId(int balance_sheet_id, BalanceSheetDetailStatus balance_sheet_detail_status)
        {
            string sql = " select * from " + CashTrustLog.Schema.TableName + " where " + CashTrustLog.Columns.TrustId
                       + " in(select " + BalanceSheetDetail.Columns.TrustId + " from " + BalanceSheetDetail.Schema.TableName
                       + " where " + BalanceSheetDetail.Columns.BalanceSheetId + "=@bsid and " + BalanceSheetDetail.Columns.Status + " =@bsd_status) ";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@bsid", balance_sheet_id, DbType.Int32);
            qc.AddParameter("@bsd_status", (int)balance_sheet_detail_status, DbType.Int32);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogGetAllListByHiDealProductId(int productId)
        {
            string sql = @"SELECT ctl.* 
                        FROM (SELECT distinct product_id, hi_deal_order_guid as order_guid, guid as order_detail_guid
                        FROM dbo.hi_deal_order_detail
                        WHERE product_type = 1) AS hod INNER JOIN
                        dbo.cash_trust_log AS ctl ON ctl.order_guid = hod.order_guid AND ctl.order_detail_guid = hod.order_detail_guid 
                        WHERE ctl.order_classification = 2 and hod.product_id = @productId ";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@productId", productId, DbType.Int32);
            CashTrustLogCollection data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void CashTrustLogToUpdateForAch(Guid report_guid, int balance_sheet_id)
        {
            string sql = " update " + CashTrustLog.Schema.TableName + " set " + CashTrustLog.Columns.StoreVerifiedGuid + " =@rid where " + CashTrustLog.Columns.TrustId
                       + " in(select " + BalanceSheetDetail.Columns.TrustId + " from " + BalanceSheetDetail.Schema.TableName
                       + " where " + BalanceSheetDetail.Columns.BalanceSheetId + "=@bsid and " + BalanceSheetDetail.Columns.Status + " = " + (int)BalanceSheetDetailStatus.Normal + "); "
                       + " update " + CashTrustLog.Schema.TableName + " set " + CashTrustLog.Columns.StoreVerifiedGuid + " =NULL where " + CashTrustLog.Columns.TrustId
                       + " in(select " + BalanceSheetDetail.Columns.TrustId + " from " + BalanceSheetDetail.Schema.TableName
                       + " where " + BalanceSheetDetail.Columns.BalanceSheetId + "=@bsid and " + BalanceSheetDetail.Columns.Status + " = " + (int)BalanceSheetDetailStatus.Deduction + ")";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@rid", report_guid, DbType.Guid);
            qc.AddParameter("@bsid", balance_sheet_id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public CashTrustLogCollection CashTrustLogGetUnShipOrderListByOrderClassification(OrderClassification classification, DateTime queryEndTime)
        {
            string sql = string.Empty;
            //deal:撈取選擇[新對帳方式]之宅配檔次 配送開始日期 <= 今天 且仍有未出貨訂單
            //order_ship:該檔次訂單填寫之出貨日期不為null且小於今天
            //cash_trust_log:為未出貨狀態(已出貨視為核銷)
            //現在時間超過配送截止日3個月，應該不會再有新訂單吧，我是這樣想的
            if (classification.Equals(OrderClassification.LkSite))
            {//P好康多加判斷:已結檔檔次須大於門檻 若未結檔須為隨買即用檔次
                sql = string.Format(@"select ctl.* 
                        from {0} as ctl with(nolock, index(0)) 
                        inner join {1} as os with(nolock, index(1)) on os.order_guid = ctl.order_guid and os.type = 1  
                        inner join {2} as bh with(nolock) on bh.guid = ctl.business_hour_guid
                        inner join {3} as god with(nolock) on god.business_hour_guid = ctl.business_hour_guid
                        inner join {4} as da with(nolock) on da.business_hour_guid = ctl.business_hour_guid
                        where ctl.order_classification = 1 
                        and ctl.status in (0, 1) 
                        and ctl.coupon_sequence_number is null 
                        and da.vendor_billing_model = 1
                        and bh.business_hour_deliver_time_s <= @queryEndTime
                        and @queryEndTime < dateadd(month, 3, bh.business_hour_deliver_time_e)
                        and ( god.slug >= bh.business_hour_order_minimum 
                            or  ( COALESCE(god.slug, 0) = 0
		                        and bh.business_hour_deliver_time_s < CONVERT(VARCHAR(10), bh.business_hour_order_time_e, 111)
		                        and bh.business_hour_deliver_time_s >= CONVERT(VARCHAR(10), bh.business_hour_order_time_s, 111)
		                        )
                            )
                        and COALESCE(os.ship_time, '') <> '' 
                        and os.ship_time < @queryEndTime  "
                    , CashTrustLog.Schema.TableName
                    , OrderShip.Schema.TableName
                    , BusinessHour.Schema.TableName
                    , GroupOrder.Schema.TableName
                    , DealAccounting.Schema.TableName);
            }
            else if (classification.Equals(OrderClassification.HiDeal))
            {
                sql = string.Format(@"select ctl.* 
                        from {0} as ctl with(nolock)                         
                        inner join {1} as os with(nolock) on os.order_guid = ctl.order_guid and os.type = 1 
                        inner join {2} as hod with(nolock) on hod.hi_deal_order_guid = ctl.order_guid
						inner join {3} as hp with(nolock) on hp.id = hod.product_id 
                        where ctl.order_classification = 2 
                        and ctl.status in (0, 1) 
                        and hp.vendor_billing_model = 1 
						and hp.use_start_time <= @queryEndTime              
                        and COALESCE(os.ship_time, '') <> '' 
                        and os.ship_time < @queryEndTime "
                    , CashTrustLog.Schema.TableName
                    , OrderShip.Schema.TableName
                    , HiDealOrderDetail.Schema.TableName
                    , HiDealProduct.Schema.TableName);
            }

            CashTrustLogCollection data = new CashTrustLogCollection();
            if (!string.IsNullOrEmpty(sql))
            {
                QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
                qc.CommandTimeout = 60 * 10;
                qc.AddParameter("@queryEndTime", queryEndTime, DbType.DateTime);
                data.LoadAndCloseReader(DataService.GetReader(qc));
            }

            return data;
        }

        public int CashTrustLogConsumingAmoutGetByPeriod(int userId, DateTime dateStart, DateTime dateEnd, int cid = 0)
        {
            string sql = @" select isnull(sum(credit_card + pcash + scash + atm), 0)
                            from " + CashTrustLog.Schema.TableName + @" WITH(NOLOCK)
                            where user_id = @userId 
                            and create_time BETWEEN @dateStart and @dateEnd";
            if (cid != 0)
            {
                sql += @" and business_hour_guid in (
                            SELECT bid FROM dbo.category_deals WITH(NOLOCK)
                            WHERE cid = @cid) ";
            }

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            qc.AddParameter("@cid", cid, DbType.Int32);

            return (int)DataService.ExecuteScalar(qc);
        }

        public int CashTrustLogGetFamiDealVerifiedCount(int userId, DateTime startTime, DateTime endTime)
        {
            var sql = @"select count(1) from cash_trust_log c with(nolock) inner join group_order g with(nolock)
                            on c.business_hour_guid = g.business_hour_guid
                            where c.user_id = @userId and c.amount = 0 and c.bank_status = 0 and c.status = 2
                            and usage_verified_time between @startTime and @endTime
                            and g.status & 4194304 > 0 and g.status & 2048 > 0";

            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        public void CashTrustLogUpdateRefundBySkm(Guid orderGuid, TrustStatus status)
        {
            string sql = " update " + CashTrustLog.Schema.TableName + " set " +
                         CashTrustLog.StatusColumn + " = @status, " +
                         CashTrustLog.ReturnedTimeColumn + " = getdate(), " +
                         CashTrustLog.ModifyTimeColumn + " = getdate() " +
                         "where " + CashTrustLog.OrderGuidColumn + " = @orderGuid and " +
                         CashTrustLog.OrderClassificationColumn + " = @orderClassification";
            QueryCommand qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            qc.AddParameter("@status", (int)status, DbType.Int32);
            qc.AddParameter("@orderClassification", (int)OrderClassification.Skm, DbType.Int32);

            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region User Cash Trust Log

        public UserCashTrustLogCollection UserCashTrustLogGetList(int userId)
        {
            return DB.SelectAllColumnsFrom<UserCashTrustLog>().Where(UserCashTrustLog.Columns.UserId).IsEqualTo(userId)
                .And(UserCashTrustLog.Columns.BankStatus).IsNotEqualTo((int)TrustBankStatus.Initial).ExecuteAsCollection<UserCashTrustLogCollection>();
        }

        //超過一年的購物金正項
        public UserCashTrustLogCollection UserCashTrustLogPlusOverYearGetList(int userId)
        {
            DateTime now = DateTime.Now.GetFirstDayOfMonth().AddYears(-1);
            return DB.SelectAllColumnsFrom<UserCashTrustLog>().Where(UserCashTrustLog.Columns.UserId).IsEqualTo(userId).And(UserCashTrustLog.Columns.BankStatus).IsNotEqualTo((int)TrustBankStatus.Initial).And(UserCashTrustLog.Columns.CreateTime).IsLessThan(now).And(UserCashTrustLog.Columns.Amount).IsGreaterThan(0).ExecuteAsCollection<UserCashTrustLogCollection>();
        }

        //超過一年的購物金負項
        public UserCashTrustLogCollection UserCashTrustLogNegativeOverYearGetList(int userId)
        {
            DateTime now = DateTime.Now.GetFirstDayOfMonth().AddYears(-1);
            return DB.SelectAllColumnsFrom<UserCashTrustLog>().Where(UserCashTrustLog.Columns.UserId).IsEqualTo(userId).And(UserCashTrustLog.Columns.BankStatus).IsNotEqualTo((int)TrustBankStatus.Initial).And(UserCashTrustLog.Columns.CreateTime).IsLessThan(now).And(UserCashTrustLog.Columns.Amount).IsLessThan(0).ExecuteAsCollection<UserCashTrustLogCollection>();
        }

        //一年以內的購物金
        public UserCashTrustLogCollection UserCashTrustLogWithinYearGetList(int userId)
        {
            DateTime now = DateTime.Now.GetFirstDayOfMonth().AddYears(-1);
            return DB.SelectAllColumnsFrom<UserCashTrustLog>().Where(UserCashTrustLog.Columns.UserId).IsEqualTo(userId).And(UserCashTrustLog.Columns.BankStatus).IsNotEqualTo((int)TrustBankStatus.Initial).And(UserCashTrustLog.Columns.CreateTime).IsGreaterThanOrEqualTo(now).ExecuteAsCollection<UserCashTrustLogCollection>();
        }

        public UserCashTrustLogCollection UserCashTrustLogGet(Guid tid)
        {
            return DB.SelectAllColumnsFrom<UserCashTrustLog>().Where(UserCashTrustLog.Columns.TrustId).IsEqualTo(tid).ExecuteAsCollection<UserCashTrustLogCollection>();
        }

        public void UserCashTrustSetToInitialFromAtm(Guid tid)
        {
            string sql = " update " + UserCashTrustLog.Schema.TableName + " set " + UserCashTrustLog.StatusColumn +
                         " = 0  where " + UserCashTrustLog.TrustIdColumn + " = @tid and " + UserCashTrustLog.StatusColumn + " = 5";
            QueryCommand qc = new QueryCommand(sql, UserCashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@tid", tid, DbType.Guid);

            DataService.ExecuteScalar(qc);
        }

        public UserCashTrustLogCollection UserCashTrustLogGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<UserCashTrustLog, UserCashTrustLogCollection>(0, 0, orderBy, filter);
        }

        //new 
        //取出剩餘購物金

        public bool UserCashTrustLogSet(UserCashTrustLog userCashTrustLog)
        {
            if (!userCashTrustLog.IsDirty)
                return false;
            DB.Save<UserCashTrustLog>(userCashTrustLog);
            return true;
        }

        public bool UserCashTrustLogCollectionSet(UserCashTrustLogCollection userCashTrustLogCollection)
        {
            DB.SaveAll<UserCashTrustLog, UserCashTrustLogCollection>(userCashTrustLogCollection);
            return true;
        }

        #endregion

        #region Cash Trust Status Log

        public CashTrustStatusLogCollection CashTrustStatusLogGetList(Guid trustId)
        {
            return DB.SelectAllColumnsFrom<CashTrustStatusLog>().Where(CashTrustStatusLog.TrustIdColumn.ColumnName).IsEqualTo(trustId).ExecuteAsCollection<CashTrustStatusLogCollection>();
        }

        public CashTrustStatusLogCollection CashTrustStatusLogGetList(IEnumerable<Guid> trustIds)
        {
            CashTrustStatusLogCollection result = new CashTrustStatusLogCollection();

            List<Guid> tempIds = trustIds.ToList();
            int idx = 0;

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<CashTrustStatusLog>()
                    .Where(CashTrustStatusLog.Columns.TrustId).In(tempIds.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<CashTrustStatusLogCollection>() ?? new CashTrustStatusLogCollection();

                result.AddRange(batchProd);
                idx += batchLimit;
            }

            return result;
        }

        public bool CashTrustStatusLogSet(CashTrustStatusLog trustLog)
        {
            if (!trustLog.IsDirty)
                return false;
            DB.Save<CashTrustStatusLog>(trustLog);
            return true;
        }

        public bool CashTrustStatusLogCollectionSet(CashTrustStatusLogCollection trustLogCollection)
        {
            DB.SaveAll<CashTrustStatusLog, CashTrustStatusLogCollection>(trustLogCollection);
            return true;
        }

        #endregion

        #region Trust Verification Report

        public TrustVerificationReport TrustVerificationReportGetLatest(TrustProvider provider, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash, bool isUseFlag = true, TrustFlag flag = TrustFlag.SendReport)
        {
            string sql = " select top 1 * from " + TrustVerificationReport.Schema.TableName + " where " +
                 TrustVerificationReport.Columns.TrustProvider + "=@provider " +
                 (isUseFlag ? " and " + TrustVerificationReport.Columns.Flag + " =@flag " : "") +
                 " and " + TrustVerificationReport.Columns.ReportType + " = " + (int)type +
                " order by id desc";
            QueryCommand qc = new QueryCommand(sql, UserCashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@provider", (int)provider, DbType.Int32);
            qc.AddParameter("@flag", (int)flag, DbType.Int32);
            TrustVerificationReport data = new TrustVerificationReport();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int TrustVerificationReportSet(TrustVerificationReport r)
        {
            return DB.Save<TrustVerificationReport>(r);
        }

        public TrustVerificationReport TrustVerificationReportGetByDate(TrustProvider provider, DateTime date, TrustFlag flag, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash)
        {
            string sql = " select * from " + TrustVerificationReport.Schema.TableName + " where " +
                 TrustVerificationReport.Columns.TrustProvider + "=@provider " +
                 " and " + TrustVerificationReport.Columns.Flag + " =@flag " +
                 " and " + TrustVerificationReport.Columns.ReportType + " = " + (int)type +
                 " and " + TrustVerificationReport.Columns.ReportDate + " between @begindate and @enddate order by id desc";
            QueryCommand qc = new QueryCommand(sql, UserCashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@provider", (int)provider, DbType.Int32);
            qc.AddParameter("@begindate", date.GetFirstDayOfMonth(), DbType.DateTime);
            qc.AddParameter("@enddate", date.GetLastDayOfMonth(), DbType.DateTime);
            qc.AddParameter("@flag", (int)flag, DbType.Int32);
            TrustVerificationReport data = new TrustVerificationReport();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public TrustVerificationReportCollection TrustVerificationReportCollectionGet(TrustProvider provider, TrustVerificationReportType type = TrustVerificationReportType.CouponAndCash)
        {
            string sql = " select * from " + TrustVerificationReport.Schema.TableName + " where " +
                 TrustVerificationReport.Columns.TrustProvider + "=@provider " +
                 " and " + TrustVerificationReport.Columns.ReportType + " = " + (int)type +
                " order by id desc";
            QueryCommand qc = new QueryCommand(sql, UserCashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@provider", (int)provider, DbType.Int32);
            TrustVerificationReportCollection data = new TrustVerificationReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public DataTable CashTrustLogGetListForRrport()
        {
            string sql = @"
    SELECT coupon_sequence_number,(atm+credit_card+scash+lcash)as amount,create_time ,trusted_bank_time 
    FROM cash_trust_log 
    where bank_status=1 and atm+credit_card+scash+lcash >0";

            DataTable dt = new DataTable();
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable UserCashTrustLogGetListForRrport()
        {
            string sql = @"
    	SELECT user_id,sum(amount) as amount from user_cash_trust_log  
	    where trust_provider = 1 and bank_status<>0 GROUP BY user_id HAVING sum(amount)>0";

            DataTable dt = new DataTable();
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }


        #endregion Trust Verification Report

        #region Verifcation Log
        public bool VerificationLogSet(VerificationLog vLog)
        {
            if (!vLog.IsDirty)
                return false;
            DB.Save<VerificationLog>(vLog);
            return true;
        }


        public VerificationLog VerificationLogVerifyCouponGet(int? couponId)
        {
            return DB.SelectAllColumnsFrom<VerificationLog>().Where(VerificationLog.CouponIdColumn).IsEqualTo(couponId)
                .And(VerificationLog.StatusColumn).IsEqualTo((int)VerificationStatus.CouponOk)
                .And(VerificationLog.UndoFlagColumn).IsEqualTo(0)
                .ExecuteSingle<VerificationLog>();
        }
        #endregion

        #region Cash Trust Coupon Change Log

        public bool CashTrustCouponChangeLogCollectionSet(CashTrustCouponChangeLogCollection ctcc)
        {
            DB.SaveAll<CashTrustCouponChangeLog, CashTrustCouponChangeLogCollection>(ctcc);
            return true;
        }

        #endregion

        #region CouponListMain 訂單

        /// <summary>
        /// [v3.0] 取得訂單主分類數量
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="mainFilter"></param>
        /// <returns></returns>
        public int GetMainFilterOrderCount(int? uniqueId, MemberOrderMainFilterType mainFilter)
        {
            int orderCount = 0;

            if (uniqueId != null && uniqueId != 0)
            {
                switch (mainFilter)
                {
                    case MemberOrderMainFilterType.All:
                        {
                            var sql = @"select COUNT(1) cnt from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                        and create_time >= '20130701' 
                                        and (status & 4194304 = 0 
                                            or status & 4194304 > 0 and ( --非全家
	                                        item_price > 0 or isnull(m.changed_expire_date, business_hour_deliver_time_e) > GETDATE() ))  --全家有價檔、全家未過期0元檔";
                            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                            qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                            orderCount = (int)DataService.ExecuteScalar(qc);
                        }
                        break;
                    case MemberOrderMainFilterType.Goods:
                        {
                            var sql = @"select COUNT(1) cnt from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                        and create_time >= '20130701' 
                                        and delivery_type = 2 ";
                            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                            qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                            orderCount = (int)DataService.ExecuteScalar(qc);
                        }
                        break;
                    case MemberOrderMainFilterType.Ppon:
                        {
                            var sql = @"select COUNT(1) cnt from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                        and create_time >= '20130701' 
                                        and delivery_type = 1 
                                        and (status & 4194304 = 0 --非全家
                                        	or status & 4194304 > 0 and (item_price > 0 or isnull(m.changed_expire_date, business_hour_deliver_time_e) > GETDATE() ))  --或全家有價檔、全家未過期0元檔";
                            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                            qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                            orderCount = (int)DataService.ExecuteScalar(qc);
                        }
                        break;
                    case MemberOrderMainFilterType.Coffee:
                        {
                            var sql = @"select COUNT(1) cnt from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                        and create_time >= '20130701' and is_deposit_coffee = 1";
                            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                            qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                            orderCount = (int)DataService.ExecuteScalar(qc);
                        }
                        break;
                }
            }
            return orderCount;
        }

        /// <summary>
        /// [v3.0] 查詢訂單列表 subFilter
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="subFilter"></param>
        /// <param name="pageLength"></param>
        /// <returns></returns>
        public ViewCouponListMainCollection GetCouponListMainColBySubFilter(int uniqueId, MemberOrderSubFilterType subFilter, int pageLength)
        {
            string sql;
            var mainCol = new ViewCouponListMainCollection();

            switch (subFilter)
            {
                #region 宅配
                case MemberOrderSubFilterType.GoodsReadyToShip:
                    {
                        if (config.EnableCancelPaidByIspOrder)
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2 
                                and (ship_time is null or ship_time >= SYSDATETIME()) and m.return_status in(-1, 4) --未出貨、未退貨
                                and order_status & 512 = 0 --非取消訂單 (超付訂單沒有return_status)
                                and is_canceling=0 --非取消中
                                and (order_status & 8 > 0 
	                                or (order_status & 8 = 0 and order_status & 1048576 > 0 and CAST(create_time as date) = CAST(GETDATE() as date)) --ATM未付款
	                                or (order_status & 8 = 0 and (order_status & 2097152 > 0 or order_status & 4194304 > 0)))  --超取超付
                                and m.status & 8388608 = 0 --排除公益檔 
                                order by create_time desc ";
                        }
                        else
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2 
                                and (ship_time is null or ship_time >= SYSDATETIME()) and m.return_status in(-1, 4) --未出貨、未退貨
                                and order_status & 512 = 0 --非取消訂單 (超付訂單沒有return_status)
                                and (order_status & 8 > 0 
	                                or (order_status & 8 = 0 and order_status & 1048576 > 0 and CAST(create_time as date) = CAST(GETDATE() as date)) --ATM未付款
	                                or (order_status & 8 = 0 and (order_status & 2097152 > 0 or order_status & 4194304 > 0)))  --超取超付
                                and m.status & 8388608 = 0 --排除公益檔 
                                order by create_time desc ";
                        }

                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                case MemberOrderSubFilterType.GoodsShipped:
                    {
                        if (config.EnableCancelPaidByIspOrder)
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2 
                                and ship_time is not null and ship_time < SYSDATETIME()  --已出貨
                                and m.return_status in(-1, 4) and m.status & 8388608 = 0  --未退貨、排除公益檔
                                and order_status & 512 = 0 --非取消訂單 (超付訂單沒有return_status)
                                and is_canceling=0 --非取消中
                                order by create_time desc";
                        }
                        else
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2 
                                and ship_time is not null and ship_time < SYSDATETIME()  --已出貨
                                and m.return_status in(-1, 4) and m.status & 8388608 = 0  --未退貨、排除公益檔 
                                order by create_time desc";
                        }

                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                case MemberOrderSubFilterType.GoodsCancel:
                    {
                        if (config.EnableCancelPaidByIspOrder)
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2
                                and (m.return_status in(1, 2, 3, 5, 6, 7, 8) --退貨處理中、退貨完成、退貨失敗
	                                or m.order_status & 512 > 0 --取消訂單 (超付訂單沒有return_status)
                                    or is_canceling=1 --取消中
	                                or order_status & 1048576 > 0 and CAST(create_time as date) < CAST(GETDATE() as date) ) --ATM逾期未付款
                                order by create_time desc";
                        }
                        else
                        {
                            sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2
                                and (m.return_status in(1, 2, 3, 5, 6, 7, 8) --退貨處理中、退貨完成、退貨失敗
	                                or m.order_status & 512 > 0 --取消訂單 (超付訂單沒有return_status)
	                                or order_status & 1048576 > 0 and CAST(create_time as date) < CAST(GETDATE() as date) ) --ATM逾期未付款
                                order by create_time desc";

                        }

                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                #endregion
                #region 電子票券
                case MemberOrderSubFilterType.PponCanUse:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 1 
                                and (order_status & 8 > 0 		
	                                and (m.remain_count - 			
			                                (select COUNT(1) from dbo.mgm_gift mg with(nolock) 
				                                left join mgm_gift_version mgv on mg.id = mgv.mgm_gift_id 
				                                left join cash_trust_log ctl on mg.trust_id = ctl.trust_id 
				                                where ctl.order_guid = m.GUID and is_active = 1 and accept in (1, 6) and is_used = 0) > 0) --實際可用憑證數(扣除贈禮的未使用憑證數)
	                                or (order_status & 8 = 0 and order_status & 1048576 > 0 and CAST(create_time as date) = CAST(GETDATE() as date) )) --ATM未付款
                                and m.return_status in(-1, 4) --未退貨
                                and (item_price >0 --有價憑證, 不論是否逾期
	                                or status & 4194304 = 0 and item_price = 0 and isnull(m.changed_expire_date, business_hour_deliver_time_e) > GETDATE() --未逾期0元檔
	                                or status & 4194304 > 0 and isnull(m.changed_expire_date, business_hour_deliver_time_e) >=  GETDATE()) --未逾期全家檔
                                order by create_time desc";
                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                        //SQL 僅能簡單判斷未逾期憑證，接到結果後需再加上程式未逾期判斷 !PponDealApiManager.IsPastFinalExpireDate(main)
                    }
                    break;
                case MemberOrderSubFilterType.PponUsed:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 1 
                                and m.return_status in(-1, 4) --未退貨
                                and order_status & 8 > 0 	
                                and (m.remain_count - 			
			                                (select COUNT(1) from dbo.mgm_gift mg with(nolock) 
				                                left join mgm_gift_version mgv on mg.id = mgv.mgm_gift_id 
				                                left join cash_trust_log ctl on mg.trust_id = ctl.trust_id 
				                                where ctl.order_guid = m.GUID and is_active = 1 and accept in (1, 6) and is_used = 0) = 0) --實際可用憑證數(扣除贈禮的未使用憑證數)
                                and (status & 4194304 = 0 --非全家已使用完憑證	
	                                or status & 4194304 > 0 and item_price > 0) --全家有價檔已使用完憑證
                                order by create_time desc";
                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                case MemberOrderSubFilterType.PponCancel:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 1 
                                and (m.return_status in(1, 2, 3, 5, 6, 7, 8)
	                                or status & 4194304 = 0 and item_price = 0 and remain_count > 0 and isnull(m.changed_expire_date, business_hour_deliver_time_e) < GETDATE() --逾期未使用0元檔
	                                or (order_status & 8 = 0 and order_status & 1048576 > 0 and CAST(create_time as date) < CAST(GETDATE() as date) )) --ATM未付款
                                order by create_time desc";
                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                #endregion
                #region 咖啡寄杯
                case MemberOrderSubFilterType.Coffee:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and m.is_deposit_coffee = 1 order by create_time desc	";
                        QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                        mainCol.LoadAndCloseReader(DataService.GetReader(qc));
                    }
                    break;
                    #endregion
            }

            return mainCol;
        }

        /// <summary>
        /// [v3.0] 查詢訂單列表 mainFilter
        /// </summary>
        /// <param name="uniqueId"></param>
        /// <param name="mainFilter"></param>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        public ViewCouponListMainCollection GetCouponListMainColByMainFilter(
            int uniqueId, MemberOrderMainFilterType mainFilter, int pageStart, int pageLength)
        {
            string sql = string.Empty;
            QueryCommand qc;
            var mainCol = new ViewCouponListMainCollection();
            switch (mainFilter)
            {
                case MemberOrderMainFilterType.All:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' 
                                and group_order_status & 67108864 = 0
                                and (status & 4194304 = 0
	                                or status & 4194304 > 0 and (  
                                        item_price > 0 or isnull(m.changed_expire_date, business_hour_deliver_time_e) > GETDATE() ))"; //或全家有價檔、全家未過期0元檔
                        qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                    }
                    break;
                case MemberOrderMainFilterType.Goods:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 2 and group_order_status & 67108864 = 0                                                                
                                and m.status & 8388608 = 0"; //排除公益檔
                        qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                    }
                    break;
                case MemberOrderMainFilterType.Ppon:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and delivery_type = 1 and group_order_status & 67108864 = 0                                                                
                                and (status & 4194304 = 0
	                                or status & 4194304 > 0 and ( 
                                        item_price > 0 or isnull(m.changed_expire_date, business_hour_deliver_time_e) > GETDATE() ))"; //或全家有價檔、全家未過期0元檔
                        qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                    }
                    break;
                case MemberOrderMainFilterType.Coffee:
                    {
                        sql = @"select * from view_coupon_list_main as m with(nolock) where unique_id = @UniqueId
                                and create_time >= '20130701' and m.is_deposit_coffee = 1 and group_order_status & 67108864 = 0";
                        qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                        qc.AddParameter("@UniqueId", uniqueId, DbType.Int32);
                    }
                    break;
                default:
                    qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
                    break;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, ViewCouponListMain.Columns.CreateTime + " desc");
            }

            mainCol.LoadAndCloseReader(DataService.GetReader(qc));

            return mainCol;
        }

        public int GetCouponListMainCount(int uniqueId, string filter)
        {
            string sql = @"select count(1) from " + ViewCouponListMain.Schema.TableName + " with(nolock) where "
                + ViewCouponListMain.Columns.UniqueId + "=@uniqueId "
                + filter;
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public int GetCouponListMainCount(int uniqueId, Guid sellerGuid, string filter)
        {
            string sql = @"select count(1) from " + ViewCouponListMain.Schema.TableName + " with(nolock) "
                        + " where " + ViewCouponListMain.Columns.UniqueId + "=@uniqueId "
                        + " and " + ViewCouponListMain.Columns.SellerGuid + "=@sellerGuid "
                        + filter;

            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);

            return (int)DataService.ExecuteScalar(qc);
        }

        public int GetCouponOnlyListMainCount(int uniqueId, string filter)
        {
            string sql = @"select count(1) from " + ViewCouponOnlyListMain.Schema.TableName + " with(nolock) where "
                + ViewCouponOnlyListMain.Columns.UniqueId + "=@uniqueId " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewCouponListMain GetCouponListMainByOid(Guid orderGuid)
        {
            string sql = "select TOP 1 * from " + ViewCouponListMain.Schema.TableName + " where " + ViewCouponListMain.Columns.Guid + "=@orderGuid";
            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            ViewCouponListMain couponListMain = new ViewCouponListMain();
            couponListMain.LoadAndCloseReader(DataService.GetReader(qc));
            return couponListMain;
        }

        public ViewCouponListSequence GetCouponListSequenceByOid(Guid orderGuid, int couponId)
        {
            string sql = @"SELECT TOP 1 * from " + ViewCouponListSequence.Schema.TableName +
                            " WHERE " + ViewCouponListSequence.Columns.Guid + "=@orderGuid " + " AND " + ViewCouponListSequence.Columns.Id + "=@couponId ";
            QueryCommand qc = new QueryCommand(sql, ViewCouponListSequence.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            qc.AddParameter("@couponId", couponId, DbType.Int32);
            ViewCouponListSequence couponListSequence = new ViewCouponListSequence();
            couponListSequence.LoadAndCloseReader(DataService.GetReader(qc));
            return couponListSequence;
        }

        public ViewCouponListMainCollection GetGiftOrderListByUser(int uniqueId)
        {
            var filter = " and " + ViewCouponListMain.Columns.RemainCount + ">0" +
                         " and " + ViewCouponListMain.Columns.ItemPrice + ">0 and " +
                         ViewCouponListMain.Columns.DeliveryType + " != " + (int)DeliveryType.ToHouse
                         + " and ( " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + "=0 "
                         + "or (" + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Cancel + " >0 "
                         + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.PartialCancel +
                         " >0 " + ")" + " ) "
                         + " and (" + ViewCouponListMain.Columns.BusinessHourDeliverTimeE + ">='"
                         + DateTime.Now.AddDays(config.MGMGiftLockDay + 1).ToString("yyyy-MM-dd") + "' or " // +1 是因為退禮物job時間差的 buffer
                         + ViewCouponListMain.Columns.ChangedExpireDate + ">='" + DateTime.Now.AddDays(config.MGMGiftLockDay + 1).ToString("yyyy-MM-dd") + "'"
                         + ") and " + ViewCouponListMain.Columns.ReturnStatus + " Not in (" +
                         (int)ProgressStatus.Processing + ","
                         + (int)ProgressStatus.AtmQueueing + "," + (int)ProgressStatus.AtmQueueSucceeded + ","
                         + (int)ProgressStatus.AtmFailed + "," + (int)ProgressStatus.Completed + ","
                         + (int)ProgressStatus.CompletedWithCreditCardQueued + ")"
                         + " and " + ViewCouponListMain.Columns.OrderStatus + " & " + (int)OrderStatus.Complete + " >0 ";

            return GetCouponListMainListByUser(0, 0, uniqueId, string.Empty, filter, true);
        }

        public ViewCouponListMainCollection GetCouponListMainListByUser(int pageStart, int pageLength, int uniqueId, string orderBy,
            string filter, bool withPiinlife)
        {
            string subSql = string.Empty;
            //排除品生活的檔次 (品生活改用好康結構後，由於APP尚未準備好處理，所以提供過濾的方式)
            if (!withPiinlife)
            {
                subSql = " and " + ViewCouponListMain.Columns.Guid + " not in ( select m." +
                         ViewCouponListMain.Columns.Guid + " from " + ViewCouponListMain.Schema.TableName + " m , " +
                         CategoryDeal.Schema.TableName + " s where  m." + ViewCouponListMain.Columns.UniqueId + "=@uniqueId " +
                         " and m." + ViewCouponListMain.Columns.BusinessHourGuid + " = s." + CategoryDeal.Columns.Bid +
                         " and s." + CategoryDeal.Columns.Cid + " = 148 ) ";
            }

            string defOrderBy = ViewCouponListMain.Columns.CreateTime;
            string sql = @"SELECT * from " + ViewCouponListMain.Schema.TableName + " with(nolock) WHERE "
                + ViewCouponListMain.Columns.UniqueId + "=@uniqueId " + filter + subSql;
            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);

            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy + " desc");
            }
            ViewCouponListMainCollection ocol = new ViewCouponListMainCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponListMainCollection GetCouponListMainListByUserAndSellerGuid(int pageStart, int pageLength, int uniqueId, Guid sellerGuid,
            string filter)
        {
            string sql = @"SELECT * from " + ViewCouponListMain.Schema.TableName + " with(nolock) "
                        + " WHERE " + ViewCouponListMain.Columns.UniqueId + "=@uniqueId"
                        + " and " + ViewCouponListMain.Columns.SellerGuid + "=@sellerGuid"
                        + filter;

            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);

            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + ViewCouponListMain.Columns.CreateTime;
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, ViewCouponListMain.Columns.CreateTime + " desc");
            }
            ViewCouponListMainCollection ocol = new ViewCouponListMainCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponListMainCollection GetCouponListMainListByOrderID(int uniqueId, string orderid)
        {
            string sql = @"SELECT * from " + ViewCouponListMain.Schema.TableName + " with(nolock) WHERE "
                + ViewCouponListMain.Columns.UniqueId + "=@uniqueId and " + ViewCouponListMain.Columns.OrderId + "=@orderid";
            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            qc.AddParameter("@orderid", orderid, DbType.AnsiString);
            ViewCouponListMainCollection vclmc = new ViewCouponListMainCollection();
            vclmc.LoadAndCloseReader(DataService.GetReader(qc));
            return vclmc;
        }

        public decimal GetCouponTotalSavings(string user_name)
        {
            string sql = @"SELECT isnull(SUM(item_orig_price)-SUM(item_price),0) FROM [view_ppon_coupon] with (nolock) " +
                                        "join view_ppon_deal vpd with (nolock) on vpd.business_hour_guid=view_ppon_coupon.business_hour_guid " +
                                        "WHERE " +
                                        "member_email='" + user_name + "' and " +
                                        ViewPponCoupon.Columns.OrderDetailStatus + "<>" +
                                       ((int)OrderDetailStatus.SystemEntry).ToString() +
                                        " and (vpd.ordered_quantity>=vpd.business_hour_order_minimum" +
                                        " or (vpd.ordered_quantity<=vpd.business_hour_order_minimum and vpd.group_order_status & 1<>1))"
                                        + " and order_detail_create_id<>'sys' and item_price<>0 and order_status&8>0";
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            return (decimal)DataService.ExecuteScalar(qc);
        }

        public ViewCouponListSequenceCollection GetCouponListSequenceListByOid(Guid guid, OrderClassification classification)
        {
            string sql = @"SELECT * from " + ViewCouponListSequence.Schema.TableName + " with(nolock) WHERE "
                + ViewCouponListSequence.Columns.Guid + "=@guid "
                + "AND " + ViewCouponListSequence.Columns.OrderClassification + "=@classification";
            QueryCommand qc = new QueryCommand(sql, ViewCouponListSequence.Schema.Provider.Name);
            qc.AddParameter("@guid", guid, DbType.Guid);
            qc.AddParameter("@classification", (int)classification, DbType.Int32);
            ViewCouponListSequenceCollection ocol = new ViewCouponListSequenceCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponListSequenceCollection GetCouponListSequenceListByOid(Guid guid)
        {
            string sql = @"SELECT * from " + ViewCouponListSequence.Schema.TableName + " with(nolock) WHERE " +
                         ViewCouponListSequence.Columns.Guid + "=@guid ";

            QueryCommand qc = new QueryCommand(sql, ViewCouponListSequence.Schema.Provider.Name);
            qc.AddParameter("@guid", guid, DbType.Guid);
            ViewCouponListSequenceCollection ocol = new ViewCouponListSequenceCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponListSequenceCollection GetCouponListSequenceListByBetweenTime(int tokenId, DateTime startTime, DateTime endTime)
        {
            string sql = @"SELECT * from  view_coupon_list_sequence with(nolock) WHERE guid in 
            (SELECT order_guid from order_corresponding with(nolock)where token_client_id = @tokenId and created_time between @startTime and @endTime )";

            QueryCommand qc = new QueryCommand(sql, ViewCouponListSequence.Schema.Provider.Name);
            qc.AddParameter("@tokenId", tokenId, DbType.Int16);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            ViewCouponListSequenceCollection ocol = new ViewCouponListSequenceCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponListSequenceCollection GetCouponListSequenceByOidList(List<Guid> oidList)
        {
            var vclsCol = new ViewCouponListSequenceCollection();
            var idx = 0;

            do
            {
                const int batchLimit = 2000;
                vclsCol.AddRange(
                    DB.SelectAllColumnsFrom<ViewCouponListSequence>()
                        .Where(ViewCouponListSequence.Columns.Guid).In(oidList.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewCouponListSequenceCollection>());
                idx += batchLimit;
            } while (idx <= oidList.Count - 1);


            return vclsCol;
        }

        public DataTable GetMemberListByDepartmentAndBusinessHourAndCouponUsageExcludeRefunding(DeliveryType theType, Guid theBusinessHourGuid,
            Guid storeGuid, int theRemainCount)
        {
            string strSql = @"SELECT vclm.member_email, o.member_name, 
                            od.item_name + s.seller_name as item_name
                            FROM view_coupon_list_main vclm 
                            inner join order_detail od on vclm.GUID = od.order_GUID 
                            inner join [order] o on vclm.GUID = o.GUID 
                            inner join view_ppon_close_down_expiration_change vxc on vxc.order_guid = od.order_GUID 
                            left join seller s on s.guid = od.store_guid
                            WHERE vclm.delivery_type = " + (int)theType +
                            "AND business_hour_guid = '" + theBusinessHourGuid + @"' 
                            AND vclm.remain_count > " + theRemainCount +
                            @" AND od.status&2 = 0 AND 
                            ( coalesce(vxc.bh_changed_expire_date, '2100-01-01') > vxc.bh_deliver_time_e 
                            AND coalesce(vxc.store_changed_expire_date, '2100-01-01') > vxc.bh_deliver_time_e
                            AND vxc.seller_close_down_date is null
                            AND vxc.store_close_down_date is null)
                            AND o.GUID not in(select order_guid from return_form where progress_status in(1,2,5,6,7))
                            AND o.GUID in (select order_guid from view_mgm_active_coupon where business_hour_guid= '" + theBusinessHourGuid + "')";

            if (storeGuid != Guid.Empty)
                strSql += " AND od.store_guid = '" + storeGuid + "'";

            IDataReader idr = new InlineQuery().ExecuteReader(strSql);
            DataTable dt = new DataTable();

            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetMemberListByDepartmentAndBusinessHourAndCouponForSendGift(Guid theBusinessHourGuid, Guid storeGuid)
        {
            string strSql = @"Select user_name, m.last_name+m.first_name as member_name,od.item_name,vmg.order_guid,count(vmg.order_guid) as count  
                            From view_mgm_gift vmg INNER JOIN member m
                            ON vmg.receiver_id = m.unique_id INNER JOIN order_detail od
                            ON vmg.order_guid=od.order_GUID
                            Where accept=1 and is_used=0
                            And vmg.bid='" + theBusinessHourGuid + "'";

            //為了測試所下的條件，到時候要移除
            strSql += " AND vmg.send_time >= '2017/03/21'";

            if (storeGuid != Guid.Empty)
            {
                strSql += " AND od.store_guid = '" + storeGuid + "'";
            }
            strSql += " Group By user_name,m.last_name,m.first_name,od.item_name,vmg.order_guid";


            IDataReader idr = new InlineQuery().ExecuteReader(strSql);
            DataTable dt = new DataTable();

            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public ViewCouponListCollection GetViewCouponListByOrderGuid(Guid orderGuid)
        {
            string sql = "select * from " + ViewCouponList.Schema.TableName + " where " + ViewCouponList.Columns.OrderGuid + "=@orderGuid";
            QueryCommand qc = new QueryCommand(sql, ViewCouponList.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            ViewCouponListCollection ocol = new ViewCouponListCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewCouponOnlyListMainCollection GetCouponOnlyListMainListByUser(int pageStart, int pageLength, int uniqueId, string orderBy,
            string filter, bool withPiinlife)
        {
            string subSql = string.Empty;
            //排除品生活的檔次 (品生活改用好康結構後，由於APP尚未準備好處理，所以提供過濾的方式)
            if (!withPiinlife)
            {
                subSql = " and " + ViewCouponOnlyListMain.Columns.Guid + " not in ( select m." +
                         ViewCouponOnlyListMain.Columns.Guid + " from " + ViewCouponOnlyListMain.Schema.TableName + " m , " +
                         CategoryDeal.Schema.TableName + " s where  m." + ViewCouponOnlyListMain.Columns.UniqueId + "=@uniqueId " +
                         " and m." + ViewCouponOnlyListMain.Columns.BusinessHourGuid + " = s." + CategoryDeal.Columns.Bid +
                         " and s." + CategoryDeal.Columns.Cid + " = 148 ) ";
            }

            string defOrderBy = ViewCouponOnlyListMain.Columns.CreateTime;
            string sql = @"SELECT * from " + ViewCouponOnlyListMain.Schema.TableName + " with(nolock) WHERE "
                + ViewCouponOnlyListMain.Columns.UniqueId + "=@uniqueId " + filter + subSql;
            QueryCommand qc = new QueryCommand(sql, ViewCouponOnlyListMain.Schema.Provider.Name);
            qc.AddParameter("@uniqueId", uniqueId, DbType.Int32);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy + " desc");
            }
            ViewCouponOnlyListMainCollection ocol = new ViewCouponOnlyListMainCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        #endregion

        #region Trust Fix Tool
        public DataTable GetTrustOverCoupon()
        {
            string sql = @"
select * into #tempB from coupon with(nolock) where order_detail_id in 
(select order_detail_guid from cash_trust_log with(nolock))

select distinct o.order_detail_guid,o.[count] as 'trust_count',p.item_quantity from 
(select order_detail_guid,COUNT(1) as 'COUNT' from cash_trust_log a with(nolock) where order_detail_guid in (select order_detail_id from #tempB) group by a.order_detail_guid) 
o join
(select order_detail_id,item_quantity from view_ppon_coupon with(nolock) where order_detail_id in (select order_detail_id from #tempB)) 
p on o.order_detail_guid=p.order_detail_id
where [COUNT]>item_quantity
";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetCouponOverTrust()
        {
            string sql = @"
select * into #tempB from coupon with(nolock) where order_detail_id in 
(select order_detail_guid from cash_trust_log with(nolock))

select distinct o.order_detail_guid,o.[count] as 'trust_count',p.item_quantity from 
(select order_detail_guid,COUNT(1) as 'COUNT' from cash_trust_log a with(nolock) where order_detail_guid in (select order_detail_id from #tempB) group by a.order_detail_guid) 
o join
(select order_detail_id,item_quantity from view_ppon_coupon with(nolock) where order_detail_id in (select order_detail_id from #tempB)  and order_detail_create_time>'2011-07-16 08:00') 
p on o.order_detail_guid=p.order_detail_id
where [COUNT]<item_quantity
";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetTrustCouponEmpty()
        {
            string sql = @"
select distinct a.trust_id,a.coupon_sequence_number,a.order_detail_guid,b.sequence_number,b.coupon_id from cash_trust_log a with(nolock), view_ppon_coupon b with(nolock) 
where b.order_detail_create_time>'2011-07-16 08:00' 
and a.order_detail_guid=b.order_detail_id and a.coupon_sequence_number is null
and not b.sequence_number is null
";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetTrustCouponEmptyOrderDetailByDays(int days)
        {
            string sql = string.Format(@"
SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

select distinct A.order_detail_guid 
  from coupon  B 
       left join cash_trust_log A
         on B.order_detail_id = A.order_detail_guid 
 where A.create_time > DATEADD(day, -{0}, GETDATE()) 
   and B.sequence_number is NOT null 
   and A.coupon_sequence_number is null
", days);
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }
        #endregion

        #region Edm Eliminate Email
        public EdmEliminateEmail EdmEliminateEmailGet(string column, object value)
        {
            return DB.Get<EdmEliminateEmail>(column, value);
        }

        public bool EdmEliminateEmailSet(EdmEliminateEmail data)
        {
            if (!data.IsDirty)
                return false;
            DB.Save<EdmEliminateEmail>(data);
            return true;
        }
        #endregion

        #region ViewMemberBuildingCityParentCity
        public ViewMemberBuildingCityParentCity ViewMemberBuildingCityParentCityGetByUserName(string userName)
        {
            ViewMemberBuildingCityParentCity view = new ViewMemberBuildingCityParentCity();
            view.LoadByParam(ViewMemberBuildingCityParentCity.Columns.UserName, userName);
            return view;
        }
        #endregion

        #region AccountAudit

        public void AccountAuditSet(int userId, string accountId, string ipAddress, AccountAuditAction action,
            bool isSuccess, string memo = null, OrderFromType? deviceType = null)
        {
            AccountAudit audit = new AccountAudit(true);
            if (userId == 0)
            {
                audit.UserId = null;
            }
            else
            {
                audit.UserId = userId;
            }
            audit.AccountId = accountId;
            audit.SourceIp = ipAddress;
            audit.Action = (int)action;
            audit.IsSuccess = isSuccess;
            if (HttpContext.Current != null)
            {
                audit.CreateId = HttpContext.Current.User.Identity.Name;
            }
            if (deviceType != null)
            {
                audit.DeviceType = (int)deviceType.Value;
            }
            audit.Host = Dns.GetHostName();
            if (!string.IsNullOrEmpty(memo))
            {
                audit.Memo = memo;
            }
            DB.Save<AccountAudit>(audit);
        }

        public void AccountAuditSet(string accountId, string ipAddress, AccountAuditAction action, bool isSuccess, string memo = null
            , OrderFromType? deviceType = null)
        {
            AccountAudit audit = new AccountAudit(true);

            Member m = MemberGet(accountId);
            if (m.IsLoaded)
            {
                audit.UserId = m.UniqueId;
            }
            AccountAuditSet(m.UniqueId, accountId, ipAddress, action, isSuccess, memo, deviceType);
        }

        public List<AccountAudit> AccountAuditGetList(int userId, AccountAuditAction action)
        {
            return DB.QueryOver<AccountAudit>()
                .Where(t => t.UserId == userId && t.Action == (int)action)
                .ToList();
        }

        #endregion

        #region LoginTicket

        public bool LoginTicketSet(LoginTicket ticket)
        {
            DB.Save(ticket);
            return true;
        }
        public LoginTicket LoginTicketGet(string ticketId, int userId, string driverId, LoginTicketType logintickettype = LoginTicketType.AppLogin)
        {
            return DB.SelectAllColumnsFrom<LoginTicket>().Where(LoginTicket.TicketIdColumn).IsEqualTo(ticketId)
                .And(LoginTicket.UserIdColumn).IsEqualTo(userId)
                .And(LoginTicket.DriverIdColumn).IsEqualTo(driverId)
                .And(LoginTicket.LoginTicketTypeColumn).IsEqualTo((int)logintickettype)
                .ExecuteSingle<LoginTicket>();
        }
        public LoginTicket LoginTicketGet(string ticketId)
        {
            return DB.SelectAllColumnsFrom<LoginTicket>().Where(LoginTicket.TicketIdColumn).IsEqualTo(ticketId)
                .ExecuteSingle<LoginTicket>();
        }

        public LoginTicket LoginTicketGetExpiration(string ticketId, int userId, string driverId,
            LoginTicketType logintickettype = LoginTicketType.AppLogin)
        {
            DateTime dateNow = DateTime.Now;
            Select query = DB.SelectAllColumnsFrom<LoginTicket>();
            query.Where(LoginTicket.UserIdColumn).IsEqualTo(userId)
                .AndExpression(LoginTicket.Columns.TicketId).IsEqualTo(ticketId)
                    .Or(LoginTicket.Columns.PrevTicketId).IsEqualTo(ticketId).CloseExpression()
                .And(LoginTicket.ExpirationTimeColumn).IsGreaterThan(dateNow)
                .And(LoginTicket.LoginTicketTypeColumn).IsEqualTo((int)logintickettype);
            if (driverId != null)
            {
                query.And(LoginTicket.Columns.DriverId).IsEqualTo(driverId);
            }
            return query.ExecuteSingle<LoginTicket>();
        }

        public void LoginTicketSetExpirationTimeAsNowByUser(int userId)
        {
            string sql = "update " + LoginTicket.Schema.TableName + " set " + LoginTicket.Columns.ExpirationTime +
                "= GETDATE() where " + LoginTicket.Columns.UserId + "=@userId";
            QueryCommand qc = new QueryCommand(sql, LoginTicket.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            DB.Execute(qc);
        }

        public LoginTicketCollection LoginTicketListGet(int userId, LoginTicketType logintickettype = LoginTicketType.AppLogin)
        {
            return DB.SelectAllColumnsFrom<LoginTicket>().Where(LoginTicket.Columns.UserId).IsEqualTo(userId)
                   .And(LoginTicket.LoginTicketTypeColumn).IsEqualTo((int)logintickettype)
                   .ExecuteAsCollection<LoginTicketCollection>();
        }

        #endregion LoginTicket

        #region VbsAccountAudit

        public void VbsAccountAuditSet(int? vbsMembershipId, string accountId, string ipAddress,
                                       VbsAccountAuditAction action, bool isSuccess, bool isTemporary, string memo = null)
        {
            var audit = new VbsAccountAudit(true);

            audit.VbsMembershipId = vbsMembershipId;
            audit.AccountId = accountId;
            audit.SourceIp = ipAddress;
            audit.Action = (int)action;
            audit.IsSuccess = isSuccess;
            if (!string.IsNullOrEmpty(memo))
            {
                audit.Memo = memo;
            }
            audit.CreateTime = DateTime.Now;

            if (HttpContext.Current != null)
            {
                audit.CreateId = HttpContext.Current.User.Identity.Name;
            }
            audit.Host = Dns.GetHostName();
            audit.IsTemporary = isTemporary;

            DB.Save<VbsAccountAudit>(audit);
        }

        #endregion VbsAccountAudit

        #region VbsShipfileImportLog
        public void VbsShipfileImportLogSet(VbsShipfileImportLog vsfi)
        {
            DB.Save<VbsShipfileImportLog>(vsfi);
        }
        #endregion

        #region VbsMembership

        public void VbsMembershipSet(VbsMembership mem)
        {
            DB.Save<VbsMembership>(mem);
        }

        public int VbsPrefixUseGetCount(string prefix, int prefixLength)
        {
            string sql = string.Format(@"
SELECT count(*) FROM {0}
WHERE substring({1}, 1, {2}) = @prefix
AND {3} = @accountType"
                , VbsMembership.Schema.TableName
                , VbsMembership.Columns.AccountId
                , prefixLength
                , VbsMembership.Columns.AccountType);
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("prefix", prefix, DbType.String);
            qc.AddParameter("accountType", (int)VbsMembershipAccountType.VendorAccount, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public VbsMembership VbsMembershipGetById(int id)
        {
            return DB.SelectAllColumnsFrom<VbsMembership>()
                    .Where(VbsMembership.IdColumn).IsEqualTo(id)
                    .ExecuteSingle<VbsMembership>();
        }

        public VbsMembership VbsMembershipGetByAccountId(string accountId)
        {
            return DB.SelectAllColumnsFrom<VbsMembership>()
                    .Where(VbsMembership.AccountIdColumn).IsEqualTo(accountId)
                    .ExecuteSingle<VbsMembership>();
        }

        /// <summary>
        /// 以member.unique_id取得對應的vbs_membership資料
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public VbsMembership VbsMembershipGetByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<VbsMembership>()
                .Where(VbsMembership.UserIdColumn).IsEqualTo(userId)
                .ExecuteSingle<VbsMembership>();
        }

        public VbsMembership VbsMembershipGetByAccountId(string accountId, List<VbsMembershipAccountType> accountTypes)
        {
            return DB.SelectAllColumnsFrom<VbsMembership>()
                    .Where(VbsMembership.AccountIdColumn).IsEqualTo(accountId)
                    .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                    .ExecuteSingle<VbsMembership>();
        }

        public VbsMembershipCollection VbsMembershipGetListLikeAccountId(string accountId, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false)
        {
            SqlQuery qc = DB.SelectAllColumnsFrom<VbsMembership>();

            if (fuzzySearch)
            {
                qc.Where(VbsMembership.AccountIdColumn).Like(string.Format("%{0}%", accountId))
                    .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type));
            }
            else
            {
                qc.Where(VbsMembership.AccountIdColumn).IsEqualTo(accountId)
                    .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type));
            }

            return qc.ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
        }

        public VbsMembershipCollection VbsMembershipGetListByBindAccount(string bindAccount)
        {
            string sql = @"
                select ms.* from vbs_membership as ms with(nolock)
                inner join vbs_membership_bind_account as ba with(nolock) on ba.vbs_account_id = ms.account_id
                inner join member as m with(nolock) on ba.user_id = m.unique_id
                where ba.is_delete = 0 ";

            if (!string.IsNullOrEmpty(bindAccount))
            {
                if (bindAccount.Contains("@"))
                {
                    sql += "and m.user_name = '" + bindAccount + "'";
                }
                else
                {
                    sql += "and m.mobile = '" + bindAccount + "'";
                }
            }

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            VbsMembershipCollection memCol = new VbsMembershipCollection();
            memCol.LoadAndCloseReader(DataService.GetReader(qc));
            return memCol;
        }

        public VbsMembershipCollection VbsMembershipGetListByAccountName(string name, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false)
        {
            if (fuzzySearch)
            {
                return DB.SelectAllColumnsFrom<VbsMembership>()
                       .Where(VbsMembership.NameColumn).Like(string.Format("%{0}%", name))
                       .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                       .ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
            }
            else
            {
                return DB.SelectAllColumnsFrom<VbsMembership>()
                       .Where(VbsMembership.NameColumn).IsEqualTo(name)
                       .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                       .ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
            }

        }

        public VbsMembershipCollection VbsMembershipGetListByEmail(string email, List<VbsMembershipAccountType> accountTypes, bool fuzzySearch = false)
        {
            if (fuzzySearch)
            {
                return DB.SelectAllColumnsFrom<VbsMembership>()
                           .Where(VbsMembership.EmailColumn).Like(string.Format("%{0}%", email))
                           .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                           .ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
            }
            else
            {
                return DB.SelectAllColumnsFrom<VbsMembership>()
                           .Where(VbsMembership.EmailColumn).IsEqualTo(email)
                           .And(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                           .ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
            }
        }

        public VbsMembershipCollection VbsMembershipGetList(List<VbsMembershipAccountType> accountTypes)
        {
            return DB.SelectAllColumnsFrom<VbsMembership>()
                .Where(VbsMembership.AccountTypeColumn).In(accountTypes.Select(type => (int)type))
                .ExecuteAsCollection<VbsMembershipCollection>() ?? new VbsMembershipCollection();
        }

        #endregion

        #region VbsRole
        public void VbsRoleSet(VbsRole vbsRole)
        {
            DB.Save(vbsRole);
        }
        public VbsRole VbsRoleGet(string vbsRoleName)
        {
            return DB.Get<VbsRole>(VbsRole.Columns.RoleName, vbsRoleName);
        }

        public VbsRoleCollection VbsRoleGetList(int userId)
        {
            string sql = @"select vbs_role.role_name from vbs_role_member vrm with (nolock)
inner join member on member.unique_id = vrm.user_id
inner join vbs_role on vbs_role.guid = vrm.role_guid
where vrm.user_id = @userId";

            QueryCommand qc = new QueryCommand(sql, Role.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            VbsRoleCollection collection = new VbsRoleCollection();
            collection.LoadAndCloseReader(DataService.GetReader(qc));
            return collection;
        }

        public void VbsRoleMemberSet(VbsRoleMember vrm)
        {
            DB.Save(vrm);
        }
        public void VbsRoleMemberDelete(int userId, string[] roleNames)
        {
            string sql = string.Format(@"delete from vbs_role_member where vbs_role_member.id in 
(select vbs_role_member.id from vbs_role_member
inner join member on member.unique_id = vbs_role_member.user_id
inner join vbs_role on vbs_role.guid = vbs_role_member.role_guid
where vbs_role_member.user_id = @userId 
	and vbs_role.role_name in ({0})
)", "'" + string.Join("','", roleNames) + "'");

            QueryCommand qc = new QueryCommand(sql, VbsRoleMember.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            DB.Execute(qc);
        }
        #endregion

        #region VbsAccountApplyPasswordLog
        public VbsAccountApplyPasswordLog VbsAccountApplyPasswordLogGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<VbsAccountApplyPasswordLog>().NoLock()
                    .Where(VbsAccountApplyPasswordLog.GuidColumn).IsEqualTo(guid)
                    .ExecuteSingle<VbsAccountApplyPasswordLog>();
        }
        public void VbsAccountApplyPasswordLogSet(VbsAccountApplyPasswordLog applyLog)
        {
            DB.Save<VbsAccountApplyPasswordLog>(applyLog);
        }
        #endregion VbsAccountApplyPasswordLog

        #region VbsMembershipBindAccount

        public void VbsMembershipBindAccountSet(VbsMembershipBindAccount newVbsmsba)
        {
            DB.Save<VbsMembershipBindAccount>(newVbsmsba);
        }
        public VbsMembershipBindAccount VbsMembershipBindAccountGetByVbsAccountName(string vbsAccountName)
        {
            string sql = @"
                select top 1 ba.* from vbs_membership_bind_account as ba with(nolock)
                inner join vbs_membership as ms with(nolock) on ms.account_id = ba.vbs_account_id
                where ba.is_delete = 0 and ms.name = @vbsAccountName";

            QueryCommand qc = new QueryCommand(sql, Member.Schema.Provider.Name);
            qc.AddParameter("@vbsAccountName", vbsAccountName, DbType.String);
            VbsMembershipBindAccountCollection memCol = new VbsMembershipBindAccountCollection();
            memCol.LoadAndCloseReader(DataService.GetReader(qc));
            return memCol.Any() ? memCol.FirstOrDefault() : new VbsMembershipBindAccount(); ;
        }

        public VbsMembershipBindAccount VbsMembershipBindAccountGetByVbsAccountId(string vbsAccountId)
        {
            return DB.SelectAllColumnsFrom<VbsMembershipBindAccount>()
                  .Where(VbsMembershipBindAccount.VbsAccountIdColumn).IsEqualTo(vbsAccountId)
                  .And(VbsMembershipBindAccount.IsDeleteColumn).IsEqualTo(false)
                  .ExecuteSingle<VbsMembershipBindAccount>();
        }
        public VbsMembershipBindAccount VbsMembershipBindAccountGetByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<VbsMembershipBindAccount>()
                  .Where(VbsMembershipBindAccount.UserIdColumn).IsEqualTo(userId)
                  .And(VbsMembershipBindAccount.IsDeleteColumn).IsEqualTo(false)
                  .ExecuteSingle<VbsMembershipBindAccount>();
        }
        public void VbsMemebershipBindAccountUpdateAllDelete(string vbsAccountId, int userId)
        {
            string sql =
                @"Update vbs_membership_bind_account 
                  set is_delete = 1 , modify_time = @nowdate
                  where vbs_account_id = @vbsAccountId and is_delete = 0 ";

            if (userId != 0)
            {
                sql += "and user_id = " + userId + "";
            }

            QueryCommand qc = new QueryCommand(sql, VbsMembershipBindAccount.Schema.Provider.Name);
            qc.AddParameter("@vbsAccountId", vbsAccountId, DbType.String);
            qc.AddParameter("@nowdate", DateTime.Now, DbType.DateTime);
            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region ResourceAcl

        public ResourceAcl ResourceAclGet(string accountId, ResourceAclAccountType accountType, Guid resourceGuid, ResourceType resourceType)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>().NoLock()
                .Where(ResourceAcl.AccountIdColumn).IsEqualTo(accountId)
                .And(ResourceAcl.AccountTypeColumn).IsEqualTo(accountType)
                .And(ResourceAcl.ResourceGuidColumn).IsEqualTo(resourceGuid)
                .And(ResourceAcl.ResourceTypeColumn).IsEqualTo(resourceType)
                .ExecuteSingle<ResourceAcl>();
        }

        public ResourceAcl ResourceAclGetByAccountIdAndResourceType(string accountId, int resourceType)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>()
                .Where(ResourceAcl.AccountIdColumn).IsEqualTo(accountId)
                .And(ResourceAcl.ResourceTypeColumn).IsEqualTo(resourceType)
                .ExecuteSingle<ResourceAcl>();
        }

        public ResourceAclCollection ResourceAclGetListByAccountId(string accountId)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>().NoLock()
                .Where(ResourceAcl.AccountIdColumn).IsEqualTo(accountId)
                .ExecuteAsCollection<ResourceAclCollection>();
        }

        public int ResourceAclSet(ResourceAcl acl)
        {
            return DB.Save(acl);
        }

        public ResourceAclCollection ResourceAclGetListByResourceGuidList(IEnumerable<Guid> resourceGuids)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>().NoLock()
                       .Where(ResourceAcl.ResourceGuidColumn).In(resourceGuids)
                       .ExecuteAsCollection<ResourceAclCollection>() ?? new ResourceAclCollection();
        }

        public ResourceAcl ResourceAclGetListByResourceGuid(Guid resourceGuid)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>().NoLock()
                       .Where(ResourceAcl.ResourceGuidColumn).IsEqualTo(resourceGuid)
                       .ExecuteSingle<ResourceAcl>();
        }

        public ResourceAclCollection ResourceAclGetListByIds(IEnumerable<int> resourceAclIds)
        {
            return DB.SelectAllColumnsFrom<ResourceAcl>().NoLock()
                       .Where(ResourceAcl.IdColumn).In(resourceAclIds)
                       .ExecuteAsCollection<ResourceAclCollection>() ?? new ResourceAclCollection();
        }

        public void ResourceAclDelete(ResourceAcl acl)
        {
            DB.Delete<ResourceAcl>(acl);
        }

        public DataTable ResourceAclGetDealStorePermissionInfoByAccountId(string accountId)
        {
            string sql = @"select ra.resource_guid, coalesce(dp.unique_id, hdp.id) as product_id, 
coalesce(cec.coupon_usage, hdd.name + ' ' + hdp.name) as product_name, 
coalesce(st.seller_name, hdpst.store_name) as store_name
from resource_acl as ra with(nolock)
left join ppon_store as ps with(nolock) on ps.resource_guid = ra.resource_guid
left join deal_property as dp with(nolock) on dp.business_hour_guid = ps.business_hour_guid
left join coupon_event_content as cec with(nolock) on cec.business_hour_guid = ps.business_hour_guid
left join seller as st with(nolock) on st.guid = ps.store_guid
left join hi_deal_product_store as hdps with(nolock) on hdps.resource_guid = ra.resource_guid
left join hi_deal_product as hdp with(nolock) on hdp.id = hdps.hi_deal_product_id
left join hi_deal_deal as hdd with(nolock) on hdd.id = hdp.deal_id
left join store as hdpst with(nolock) on hdpst.guid = hdps.store_guid
where ra.account_id =@accountId
and ra.resource_type in (5,6)
and (ps.resource_guid is not null or hdps.resource_guid is not null)";

            QueryCommand qc = new QueryCommand(sql, ResourceAcl.Schema.Provider.Name);
            qc.AddParameter("@accountId", accountId, DbType.String);

            IDataReader idr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region ResourceAclLog

        public int ResourceAclLogSet(ResourceAclLog log)
        {
            return DB.Save(log);
        }

        #endregion ResourceAclLog

        #region ViewVbsFlattenedAcl

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclCurrentPermissionsGetList(string accountId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .AndExpression(ViewVbsFlattenedAcl.Columns.SellerPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.StorePermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PponPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PiinPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.DealStorePermission).IsNotNull()
                .CloseExpression().ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .And(ViewVbsFlattenedAcl.Columns.MerchandiseGuid).IsNotNull()
                .AndExpression(ViewVbsFlattenedAcl.Columns.SellerPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.StorePermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PponPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PiinPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.DealStorePermission).IsNotNull()
                .CloseExpression().ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, VbsRightFlag vbsRight)
        {
            var vbsRightSql = string.Empty;
            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                vbsRightSql = string.Format(@" ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right > 0) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight);
            }
            else if (vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                vbsRightSql = string.Format(@" {1} & @vbs_right > 0 OR (({2} = 1 AND ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right_verifyShop > 0)) OR {3} = 1) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight
                                , ViewVbsFlattenedAcl.Columns.IsInStore
                                , ViewVbsFlattenedAcl.Columns.IsHomeDelivery);
            }
            else
            {
                vbsRightSql = string.Format(" {0} & @vbs_right > 0 ", ViewVbsFlattenedAcl.Columns.VbsRight);
            }

            var sql = string.Format(@"SELECT * FROM {0}
WHERE {1} = @account_id
AND {2} is not null
AND ({3})
AND ({4} is not null
  OR {5} is not null
  OR {6} is not null)",
                      ViewVbsFlattenedAcl.Schema.TableName,
                      ViewVbsFlattenedAcl.Columns.AccountId,
                      ViewVbsFlattenedAcl.Columns.MerchandiseGuid,
                      vbsRightSql,
                      ViewVbsFlattenedAcl.Columns.SellerPermission,
                      ViewVbsFlattenedAcl.Columns.StorePermission,
                      ViewVbsFlattenedAcl.Columns.DealStorePermission);
            var qc = new QueryCommand(sql, ViewVbsFlattenedAcl.Schema.Provider.Name);
            qc.AddParameter("@account_id", accountId, DbType.String);
            qc.AddParameter("@vbs_right", (int)vbsRight, DbType.Int32);

            if (vbsRight == VbsRightFlag.VerifyShop || vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                qc.AddParameter("@no_restricted_store", (int)BusinessHourStatus.NoRestrictedStore, DbType.Int32);
                qc.AddParameter("@vbs_right_verify", (int)VbsRightFlag.Verify, DbType.Int32);

                if (vbsRight == VbsRightFlag.ViewBalanceSheet)
                {
                    qc.AddParameter("@vbs_right_verifyShop", (int)VbsRightFlag.VerifyShop, DbType.Int32);
                }
            }

            var result = new ViewVbsFlattenedAclCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, Guid merchandiseGuid)
        {
            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .And(ViewVbsFlattenedAcl.Columns.MerchandiseGuid).IsEqualTo(merchandiseGuid)
                .AndExpression(ViewVbsFlattenedAcl.Columns.SellerPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.StorePermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PponPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.PiinPermission).IsNotNull()
                .Or(ViewVbsFlattenedAcl.Columns.DealStorePermission).IsNotNull()
                .CloseExpression().ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }
        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealLevelPermissionsGetList(string accountId, Guid merchandiseGuid, VbsRightFlag vbsRight)
        {
            var vbsRightSql = string.Empty;
            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                vbsRightSql = string.Format(@" ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right > 0) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight);
            }
            else if (vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                vbsRightSql = string.Format(@" {1} & @vbs_right > 0 OR (({2} = 1 AND ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right_verifyShop > 0)) OR {3} = 1) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight
                                , ViewVbsFlattenedAcl.Columns.IsInStore
                                , ViewVbsFlattenedAcl.Columns.IsHomeDelivery);
            }
            else
            {
                vbsRightSql = string.Format(" {0} & @vbs_right > 0 ", ViewVbsFlattenedAcl.Columns.VbsRight);
            }
            var sql = string.Format(@"SELECT * FROM {0} with(nolock)
WHERE {1} = @account_id
AND {2} = @merchandiseGuid
AND ({3})
AND ({4} is not null
  OR {5} is not null
  OR {6} is not null)",
          ViewVbsFlattenedAcl.Schema.TableName,
          ViewVbsFlattenedAcl.Columns.AccountId,
          ViewVbsFlattenedAcl.Columns.MerchandiseGuid,
          vbsRightSql,
          ViewVbsFlattenedAcl.Columns.SellerPermission,
          ViewVbsFlattenedAcl.Columns.StorePermission,
          ViewVbsFlattenedAcl.Columns.DealStorePermission);
            var qc = new QueryCommand(sql, ViewVbsFlattenedAcl.Schema.Provider.Name);
            qc.AddParameter("@account_id", accountId, DbType.String);
            qc.AddParameter("@merchandiseGuid", merchandiseGuid, DbType.Guid);
            qc.AddParameter("@vbs_right", (int)vbsRight, DbType.Int32);

            if (vbsRight == VbsRightFlag.VerifyShop || vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                qc.AddParameter("@no_restricted_store", (int)BusinessHourStatus.NoRestrictedStore, DbType.Int32);
                qc.AddParameter("@vbs_right_verify", (int)VbsRightFlag.Verify, DbType.Int32);

                if (vbsRight == VbsRightFlag.ViewBalanceSheet)
                {
                    qc.AddParameter("@vbs_right_verifyShop", (int)VbsRightFlag.VerifyShop, DbType.Int32);
                }
            }

            var result = new ViewVbsFlattenedAclCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclGetListBySid(string accountId, IEnumerable<Guid> sid)
        {
            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .And(ViewVbsFlattenedAcl.Columns.SellerGuid).In(sid)
                .ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }

        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclGetListLikeSellerName(string accountId, string sellerName)
        {
            if (string.IsNullOrWhiteSpace(sellerName))
            {
                return new ViewVbsFlattenedAclCollection();
            }

            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .And(ViewVbsFlattenedAcl.Columns.SellerName).Like(string.Format("%{0}%", sellerName))
                .ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }
        public ViewVbsFlattenedAclCollection ViewVbsFlattenedAclDealPermissionsGetList(string accountId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsFlattenedAcl>()
                .Where(ViewVbsFlattenedAcl.Columns.AccountId).IsEqualTo(accountId)
                .ExecuteAsCollection<ViewVbsFlattenedAclCollection>() ?? new ViewVbsFlattenedAclCollection();
        }

        public DataTable FnVbsFlattenedAclLiteGetList(string accountId)
        {
            string sql = @"select * from fn_vbs_flattened_acl_lite(@accountId)";
            QueryCommand qc = new QueryCommand(sql, ResourceAcl.Schema.Provider.Name);
            qc.AddParameter("@accountId", accountId, DbType.String);

            IDataReader idr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region VbsVendorAceLite

        public DataTable VbsVendorAceLiteFnGetDataTable(string accountId, VbsRightFlag? vbsRight = null)
        {
            var vbsRightSql = string.Empty;
            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                vbsRightSql = string.Format(@" ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right > 0) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight);
            }
            else if (vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                vbsRightSql = string.Format(@" {1} & @vbs_right > 0 OR (({2} = 1 AND ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right_verifyShop > 0)) OR {3} = 1) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight
                                , ViewVbsFlattenedAcl.Columns.IsInStore
                                , ViewVbsFlattenedAcl.Columns.IsHomeDelivery);
            }
            else if (vbsRight.HasValue)
            {
                vbsRight = vbsRight == VbsRightFlag.Verify ? vbsRight | VbsRightFlag.VerifyShop : vbsRight;
                vbsRightSql = string.Format(" {0} & @vbs_right > 0 ", ViewVbsFlattenedAcl.Columns.VbsRight);
            }

            string sql = @"select * from fn_vbs_flattened_acl_lite(@accountId)";

            if (vbsRight.HasValue)
            {
                sql += string.Format(" where ({0}) ", vbsRightSql);
            }
            //測試某檔可直皆篩選
            //sql += " and merchandise_guid in ('bid')";
            QueryCommand qc = new QueryCommand(sql, ResourceAcl.Schema.Provider.Name);
            qc.AddParameter("@accountId", accountId, DbType.String);

            if (vbsRight.HasValue)
            {
                qc.AddParameter("@vbs_right", (int)vbsRight, DbType.Int32);
            }

            if (vbsRight == VbsRightFlag.VerifyShop || vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                qc.AddParameter("@no_restricted_store", (int)BusinessHourStatus.NoRestrictedStore, DbType.Int32);
                qc.AddParameter("@vbs_right_verify", (int)VbsRightFlag.Verify, DbType.Int32);

                if (vbsRight == VbsRightFlag.ViewBalanceSheet)
                {
                    qc.AddParameter("@vbs_right_verifyShop", (int)VbsRightFlag.VerifyShop, DbType.Int32);
                }
            }

            IDataReader idr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable VbsVendorAceLiteFnGetDataTable(string accountId, Guid dealGuid, VbsRightFlag? vbsRight = null)
        {
            var vbsRightSql = string.Empty;
            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                vbsRightSql = string.Format(@" ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right > 0) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight);
            }
            else if (vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                vbsRightSql = string.Format(@" {1} & @vbs_right > 0 OR (({2} = 1 AND ({0} = 0 AND {1} & @vbs_right_verify > 0) or ({0} = 1 AND {1} & @vbs_right_verifyShop > 0)) OR {3} = 1) "
                                , ViewVbsFlattenedAcl.Columns.IsRestrictedStore
                                , ViewVbsFlattenedAcl.Columns.VbsRight
                                , ViewVbsFlattenedAcl.Columns.IsInStore
                                , ViewVbsFlattenedAcl.Columns.IsHomeDelivery);
            }
            else
            {
                vbsRightSql = string.Format(" {0} & @vbs_right > 0 ", ViewVbsFlattenedAcl.Columns.VbsRight);
            }

            string sql = @"select * from fn_vbs_flattened_acl_lite(@accountId) where merchandise_guid = @dealGuid";
            if (vbsRight.HasValue)
            {
                sql += string.Format(" and ({0}) ", vbsRightSql);
            }

            QueryCommand qc = new QueryCommand(sql, ResourceAcl.Schema.Provider.Name);
            qc.AddParameter("@accountId", accountId, DbType.String); ;
            qc.AddParameter("@dealGuid", dealGuid, DbType.Guid);

            if (vbsRight.HasValue)
            {
                qc.AddParameter("@vbs_right", (int)vbsRight, DbType.Int32);
            }

            if (vbsRight == VbsRightFlag.VerifyShop || vbsRight == VbsRightFlag.ViewBalanceSheet)
            {
                qc.AddParameter("@no_restricted_store", (int)BusinessHourStatus.NoRestrictedStore, DbType.Int32);
                qc.AddParameter("@vbs_right_verify", (int)VbsRightFlag.Verify, DbType.Int32);

                if (vbsRight == VbsRightFlag.ViewBalanceSheet)
                {
                    qc.AddParameter("@vbs_right_verifyShop", (int)VbsRightFlag.VerifyShop, DbType.Int32);
                }
            }

            IDataReader idr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region MemberCollectDeal

        /// <summary>
        /// 取得過期收藏檔次數量
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <returns></returns>
        public int MemberCollectDealGetOutOfDateDealCount(int memberUniqueId)
        {
            string sql = string.Format(@"select Count(1) from member_collect_deal mcd with(nolock)
                inner join business_hour bh with(nolock)
                On mcd.business_hour_guid = bh.GUID
                where mcd.member_unique_id = @memberUniqueId
                AND collect_status = {0}
                AND bh.business_hour_order_time_e < GETDATE()", (int)MemberCollectDealStatus.Collected);

            QueryCommand qc = new QueryCommand(sql, MemberCollectDeal.Schema.Provider.Name);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);

            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 移除已過期的收藏檔次
        /// </summary>
        /// <param name="memberUniqueId"></param>
        public void MemberCollectDealRemoveOutOfDateDeal(int memberUniqueId)
        {
            string sql = string.Format(@"Update member_collect_deal set collect_status = {0}
                    from member_collect_deal mcd with(nolock)
                    inner join business_hour bh with(nolock)
                    On mcd.business_hour_guid = bh.GUID
                    where mcd.member_unique_id = @memberUniqueId
                    AND collect_status = {1}
                    AND bh.business_hour_order_time_e < GETDATE()"
                , (int)MemberCollectDealStatus.CancelOutOfDateDeal, (int)MemberCollectDealStatus.Collected);
            QueryCommand qc = new QueryCommand(sql, MemberCollectDeal.Schema.Provider.Name);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 儲存/更新收藏檔次
        /// </summary>
        /// <param name="mcd"></param>
        public void MemberCollectDealSave(MemberCollectDeal mcd)
        {
            if (MemberCollectDealIsExist(mcd.MemberUniqueId, mcd.BusinessHourGuid))
            {
                DB.Update<MemberCollectDeal>().Set(MemberCollectDeal.CollectStatusColumn).EqualTo(mcd.CollectStatus)
                    .Set(MemberCollectDeal.CollectTimeColumn).EqualTo(mcd.CollectTime)
                    .Set(MemberCollectDeal.LastAccessTimeColumn).EqualTo(mcd.LastAccessTime)
                    .Set(MemberCollectDeal.CityIdColumn).EqualTo(mcd.CityId)
                    .Set(MemberCollectDeal.AppNoticeColumn).EqualTo(mcd.AppNotice)
                    .Set(MemberCollectDeal.CollectTypeColumn).EqualTo(mcd.CollectType)
                    .Where(MemberCollectDeal.MemberUniqueIdColumn).IsEqualTo(mcd.MemberUniqueId)
                    .And(MemberCollectDeal.BusinessHourGuidColumn).IsEqualTo(mcd.BusinessHourGuid).Execute();
            }
            else
            {
                DB.Save<MemberCollectDeal>(mcd);
            }
        }

        /// <summary>
        /// 設定收藏推撥時間
        /// </summary>
        /// <param name="MemberUniqueId"></param>
        /// <param name="BusinessHourGuid"></param>
        /// <param name="PushTime"></param>
        public void MemberCollectDealSetPushTime(int MemberUniqueId, Guid BusinessHourGuid, DateTime PushTime)
        {
            if (MemberCollectDealIsExist(MemberUniqueId, BusinessHourGuid))
            {
                DB.Update<MemberCollectDeal>()
                    .Set(MemberCollectDeal.PushTimeColumn).EqualTo(PushTime)
                    .Where(MemberCollectDeal.MemberUniqueIdColumn).IsEqualTo(MemberUniqueId)
                    .And(MemberCollectDeal.BusinessHourGuidColumn).IsEqualTo(BusinessHourGuid)
                    .Execute();
            }
        }

        /// <summary>
        /// 取消收藏
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        public void MemberCollectDealRemove(int memberUniqueId, Guid businessHourGuid)
        {
            DB.Update<MemberCollectDeal>().Set(MemberCollectDeal.CollectStatusColumn).EqualTo((byte)MemberCollectDealStatus.Removed)
                .Set(MemberCollectDeal.LastAccessTimeColumn).EqualTo(System.DateTime.Now)
                .Where(MemberCollectDeal.MemberUniqueIdColumn).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.BusinessHourGuidColumn).IsEqualTo(businessHourGuid).Execute();
        }

        public void MemberCollectDealRemove(int memberUniqueId, List<Guid> bidList)
        {
            DB.Update<MemberCollectDeal>().Set(MemberCollectDeal.CollectStatusColumn).EqualTo((byte)MemberCollectDealStatus.Removed)
                .Set(MemberCollectDeal.LastAccessTimeColumn).EqualTo(System.DateTime.Now)
                .Where(MemberCollectDeal.MemberUniqueIdColumn).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.BusinessHourGuidColumn).In(bidList).Execute();
        }
        /// <summary>
        /// 判斷資料是否已存在
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public bool MemberCollectDealIsExist(int memberUniqueId, Guid businessHourGuid)
        {
            MemberCollectDeal mcd = DB.Select().From(MemberCollectDeal.Schema.TableName)
                .Where(MemberCollectDeal.Columns.MemberUniqueId).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.Columns.BusinessHourGuid).IsEqualTo(businessHourGuid)
                .ExecuteSingle<MemberCollectDeal>();

            if (mcd.IsLoaded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 檔次是否已被收藏
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public bool MemberCollectDealIsCollected(int memberUniqueId, Guid businessHourGuid)
        {
            MemberCollectDeal mcd = DB.Select().From(MemberCollectDeal.Schema.TableName)
                .Where(MemberCollectDeal.Columns.MemberUniqueId).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.Columns.BusinessHourGuid).IsEqualTo(businessHourGuid)
                .And(MemberCollectDeal.Columns.CollectStatus).IsEqualTo((byte)MemberCollectDealStatus.Collected)
                .ExecuteSingle<MemberCollectDeal>();

            if (mcd.IsLoaded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Guid> MemberCollectDealGetOnlineDealGuids(int userId, DateTime sDate, DateTime eDate, int topNum = 1000)
        {
            string sql = @"
select top (@topNum) mcd.business_hour_guid from member_collect_deal mcd with(nolock)
where member_unique_id = @userId and mcd.collect_status = 0
and business_hour_guid in (
select distinct dts.business_hour_GUID from deal_time_slot dts with(nolock)
where dts.effective_start > @sDate and dts.effective_start < @eDate)
";
            QueryCommand qc = new QueryCommand(sql, MemberCollectDeal.Schema.Provider.Name);
            qc.Parameters.Add("@userId", userId, DbType.Int32);
            qc.Parameters.Add("@sDate", sDate, DbType.DateTime);
            qc.Parameters.Add("@eDate", eDate, DbType.DateTime);
            qc.Parameters.Add("@topNum", topNum, DbType.Int32);

            List<Guid> result = new List<Guid>();
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }
            return result;
        }

        /// <summary>
        /// 取得收藏的檔次資料
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="businessHourGuid"></param>
        /// <returns></returns>
        public MemberCollectDeal MemberCollectDealGet(int memberUniqueId, Guid businessHourGuid)
        {
            return DB.Select().From(MemberCollectDeal.Schema.TableName)
                .Where(MemberCollectDeal.Columns.MemberUniqueId).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.Columns.BusinessHourGuid).IsEqualTo(businessHourGuid)
                .And(MemberCollectDeal.Columns.CollectStatus).IsEqualTo((byte)MemberCollectDealStatus.Collected)
                .ExecuteSingle<MemberCollectDeal>();

        }


        /// <summary>
        /// 取得已收藏檔次(ALL)
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <returns></returns>
        public MemberCollectDealCollection MemberCollectDealGetByMemberUniqueId(int memberUniqueId)
        {
            return DB.SelectAllColumnsFrom<MemberCollectDeal>()
                .Where(MemberCollectDeal.MemberUniqueIdColumn).IsEqualTo(memberUniqueId)
                .And(MemberCollectDeal.CollectStatusColumn).IsEqualTo((byte)MemberCollectDealStatus.Collected)
                .ExecuteAsCollection<MemberCollectDealCollection>();
        }

        /// <summary>
        /// 取得該檔次的收藏者
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public MemberCollectDealCollection MemberCollectDealGetByBusinessHourGuid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<MemberCollectDeal>()
               .Where(MemberCollectDeal.BusinessHourGuidColumn).IsEqualTo(bid)
               .And(MemberCollectDeal.CollectStatusColumn).IsEqualTo((byte)MemberCollectDealStatus.Collected)
               .ExecuteAsCollection<MemberCollectDealCollection>();
        }

        /// <summary>
        /// 取得該檔次收藏次數
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="collectStatus"></param>
        /// <param name="collectType"></param>
        /// <returns></returns>
        public int MemberCollectDealCountByBidStatusType(Guid bid, MemberCollectDealStatus collectStatus, MemberCollectDealType collectType)
        {
            return DB.Select().From(MemberCollectDeal.Schema).Where(MemberCollectDeal.BusinessHourGuidColumn).IsEqualTo(bid)
                .And(MemberCollectDeal.CollectStatusColumn).IsEqualTo((byte)collectStatus)
                .And(MemberCollectDeal.CollectTypeColumn).IsEqualTo((byte)collectType).GetRecordCount();
        }

        #endregion MemberCollectDeal

        #region MemberCollectNotice
        /// <summary>
        /// 儲存要收到收藏通知的裝置
        /// </summary>
        /// <param name="notice"></param>
        /// <returns></returns>
        public int MemberCollectNoticeSet(MemberCollectNotice notice)
        {
            return DB.Save(notice);
        }
        /// <summary>
        /// 以使用者編號與裝置編號取得特定 要收到收藏推播裝置資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        public MemberCollectNotice MemberCollectNoticeGet(int userId, int deviceId)
        {
            return DB.SelectAllColumnsFrom<MemberCollectNotice>()
              .Where(MemberCollectNotice.Columns.UserId)
              .IsEqualTo(userId)
              .And(MemberCollectNotice.Columns.DeviceId)
              .IsEqualTo(deviceId)
              .ExecuteSingle<MemberCollectNotice>();
        }
        /// <summary>
        /// 以使用者編號與AccessToken編號取得特定 要收到收藏推播裝置資料
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="tokenId"></param>
        /// <returns></returns>
        public MemberCollectNotice MemberCollectNoticeGetByToken(int userId, int tokenId)
        {
            return DB.SelectAllColumnsFrom<MemberCollectNotice>()
              .Where(MemberCollectNotice.Columns.UserId)
              .IsEqualTo(userId)
              .And(MemberCollectNotice.Columns.TokenId)
              .IsEqualTo(tokenId)
              .ExecuteSingle<MemberCollectNotice>();
        }
        /// <summary>
        /// 查詢某裝置所有的設定紀錄
        /// </summary>
        /// <param name="deviceId">裝置編號</param>
        /// <returns></returns>
        public MemberCollectNoticeCollection MemberCollectNoticeGetList(int deviceId)
        {
            return DB.SelectAllColumnsFrom<MemberCollectNotice>()
                     .Where(MemberCollectNotice.Columns.DeviceId).IsEqualTo(deviceId)
                     .ExecuteAsCollection<MemberCollectNoticeCollection>();
        }

        /// <summary>
        /// 移除deviceId為傳入值的所有紀錄
        /// </summary>
        /// <param name="deviceId"></param>
        /// <returns></returns>
        public void MemberCollectNoticeRemove(int deviceId)
        {
            DB.Delete<MemberCollectNotice>(MemberCollectNotice.Columns.DeviceId, deviceId);
        }

        /// <summary>
        /// 移除使用者編號與裝置編號的關聯
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        public void MemberCollectNoticeRemove(int userId, int deviceId)
        {
            MemberCollectNotice mcn = MemberCollectNoticeGet(userId, deviceId);
            DB.Delete<MemberCollectNotice>(mcn);
        }

        #endregion MemberCollectNotice

        #region ViewMemberCollectDeal

        /// <summary>
        /// 取得收藏檔次數量
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <returns></returns>
        public int ViewMemberCollectDealGetDealCount(int memberUniqueId)
        {
            string sql = @"select Count(1) from view_member_collect_deal with(nolock)
                where member_unique_id = @memberUniqueId";

            QueryCommand qc = new QueryCommand(sql, ViewMemberCollectDeal.Schema.Provider.Name);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);

            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 取得已收藏檔次(分頁)
        /// </summary>
        /// <param name="memberUniqueId"></param>
        /// <param name="pageStart"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        public ViewMemberCollectDealCollection ViewMemberCollectionDealGetListByPager(int memberUniqueId, int pageStart, int pageSize, string orderBy)
        {
            string sql = @"select * from view_member_collect_deal with(nolock)
                where member_unique_id = @memberUniqueId";
            QueryCommand qc = new QueryCommand(sql, ViewMemberCollectDeal.Schema.Provider.Name);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);
            qc = SSHelper.MakePagable(qc, pageStart, pageSize, orderBy);

            ViewMemberCollectDealCollection mcdc = new ViewMemberCollectDealCollection();
            mcdc.LoadAndCloseReader(DataService.GetReader(qc));
            return mcdc;
        }

        public string ViewMemberCollectDealGetDealList(int uniqueid)
        {
            string bids;
            string[] bidarray;
            int i = 0;

            ViewMemberCollectDealCollection vmcdc = DB.SelectAllColumnsFrom<ViewMemberCollectDeal>().NoLock()
                .Where(ViewMemberCollectDeal.Columns.MemberUniqueId).IsEqualTo(uniqueid)
                .ExecuteAsCollection<ViewMemberCollectDealCollection>();
            bidarray = new string[vmcdc.Count];
            foreach (ViewMemberCollectDeal vmcd in vmcdc)
            {
                bidarray[i] = vmcd.BusinessHourGuid.ToString();
                i++;
            }
            bids = string.Join(",", bidarray);

            return bids;
        }

        #endregion ViewMemberCollectDeal

        #region ViewMemberCollectDealContent

        /// <summary>
        /// 查詢會員收藏的檔次資料
        /// </summary>
        /// <param name="pageNumber"></param>
        /// <param name="pageSize"></param>
        /// <param name="orderBy"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewMemberCollectDealContentCollection ViewMemberCollectDealContentGetList(int pageNumber, int pageSize,
                                                                                   string orderBy,
                                                                                   params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewMemberCollectDealContent, ViewMemberCollectDealContentCollection>(pageNumber, pageSize, orderBy, filter);
        }

        /// <summary>
        /// 取得會員收藏檔次(排除結檔超過N個月檔次)
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="months"></param>
        /// <param name="collectType"></param>
        /// <returns></returns>
        public ViewMemberCollectDealContentCollection ViewMemberCollectDealContentGetList(int userId, int months, MemberCollectDealType collectType = MemberCollectDealType.Coupon)
        {
            return DB.SelectAllColumnsFrom<ViewMemberCollectDealContent>().NoLock()
                .Where(ViewMemberCollectDealContent.Columns.UserId).IsEqualTo(userId)
                .And(ViewMemberCollectDealContent.Columns.CollectStatus).IsEqualTo((int)MemberCollectDealStatus.Collected)
                .And(ViewMemberCollectDealContent.Columns.CollectType).IsEqualTo((byte)collectType)
                .And(ViewMemberCollectDealContent.Columns.BusinessHourOrderTimeE).IsGreaterThan(DateTime.Now.AddMonths(-months))
                .OrderDesc(ViewMemberCollectDealContent.Columns.CollectTime)
                .ExecuteAsCollection<ViewMemberCollectDealContentCollection>();
        }

        #endregion ViewMemberCollectDealContent

        #region MemberCollectDealExpireLog

        public void MemberCollectDealExpireLogSave(MemberCollectDealExpireLog log)
        {
            DB.Save<MemberCollectDealExpireLog>(log);
        }

        #endregion MemberCollectDealExpireLog

        #region ViewMemberCollectDealPush
        /// <summary>
        /// 依照到期日取得收藏檔次
        /// </summary>
        /// <param name="due_date">到期日</param>
        /// <returns></returns>
        public ViewMemberCollectDealPushCollection ViewMemberCollectDealPushGetListByDate(DateTime due_date)
        {
            string sql = @"select * from view_member_collect_deal_push with(nolock)
                where (business_hour_order_time_e >= GETDATE()) AND (business_hour_order_time_e <= @due_date)";//AND ((DATEDIFF(DAY, @due_date, business_hour_order_time_e) = 0) AND (DATEDIFF(HOUR, @due_date, business_hour_order_time_e) <= 0))";
            QueryCommand qc = new QueryCommand(sql, ViewMemberCollectDeal.Schema.Provider.Name);
            qc.AddParameter("@due_date", due_date, DbType.DateTime);
            //qc = SSHelper.MakePagable(qc, pageStart, pageSize, orderBy);

            ViewMemberCollectDealPushCollection mcdc = new ViewMemberCollectDealPushCollection();
            mcdc.LoadAndCloseReader(DataService.GetReader(qc));
            return mcdc;
        }

        /// <summary>
        /// 依照日期間隔取收藏檔次
        /// </summary>
        /// <param name="interval"></param>
        /// <returns></returns>
        public ViewMemberCollectDealPushCollection ViewMemberCollectDealPushGetListByInterval(int interval)
        {
            DateTime due_date = DateTime.Today.AddDays(interval);

            return ViewMemberCollectDealPushGetListByDate(due_date);
        }
        #endregion

        #region Evaluate

        public int EvaluateGetMainID(Guid tid)
        {
            var result = DB.Select().From(UserEvaluateMain.Schema)
                        .Where(UserEvaluateMain.TrustIdColumn).IsEqualTo(tid)
                        .ExecuteAsCollection<UserEvaluateMainCollection>();
            return result.Count > 0 ? result[0].Id : 0;
        }

        public UserEvaluateDetailCollection EvaluateDetailGetByMainID(int mainid, int dataType)
        {
            return DB.SelectAllColumnsFrom<UserEvaluateDetail>()
                .Where(UserEvaluateDetail.MainIdColumn).IsEqualTo(mainid)
                .And(UserEvaluateDetail.DataTypeColumn).IsEqualTo(dataType)
                .OrderDesc(UserEvaluateDetail.Columns.Id)
                .ExecuteAsCollection<UserEvaluateDetailCollection>();
        }

        public UserEvaluateItemCollection UserEvaluateItemCollectionGet()
        {
            return DB.SelectAllColumnsFrom<UserEvaluateItem>().Where(UserEvaluateItem.Columns.IsEnable).IsEqualTo(true)
                .ExecuteAsCollection<UserEvaluateItemCollection>();
        }

        public int UserEvaluateDetailSetList(UserEvaluateDetailCollection d)
        {
            return DB.SaveAll<UserEvaluateDetail, UserEvaluateDetailCollection>(d);
        }

        public int UserEvaluateMainSet(UserEvaluateMain d)
        {
            return DB.Save<UserEvaluateMain>(d);
        }

        /// <summary>
        /// 依照日期取得使用檔次資料
        /// </summary>
        /// <param name="sendDate"></param>
        /// <returns></returns>
        public DataTable EvaluateUserGet()
        {
            string sql = @"select a.usage_verified_time,a.trust_id,a.coupon_sequence_number,a.business_hour_guid,d.user_name,case when b.is_hide_content=1 then b.name else b.content_name end as [name],c.item_name,(d.last_name + d.first_name) as UserName, a.order_guid from cash_trust_log a with (nolock)
inner join member d with (nolock) on d.unique_id = a.user_id
inner join coupon_event_content b with (nolock) on a.business_hour_guid = b.business_hour_guid
inner join item c with (nolock) on a.business_hour_guid = c.business_hour_guid
inner join deal_property e with (nolock) on a.business_hour_guid = e.business_hour_guid And e.delivery_type ='1'
where (select g.status from group_order g where g.business_hour_guid = a.business_hour_guid) & 4194304 = 0 and 
a.usage_verified_time >=(select CONVERT(varchar(12),getdate()- 1,111)) and a.usage_verified_time <=(select CONVERT(varchar(12),getdate(),111))";

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable EvaluateUserByNegativeComments()
        {
            string sql = string.Format(@"
select ctl.business_hour_guid,d.item_id,item.sequence,count(*) as TotalCommentsCount,sum(d.rating)/count(*) as AvgPoint,
(select count(*) from user_evaluate_main m2 with(NOLOCK)
inner join user_evaluate_detail d2 with(NOLOCK) on m2.id = d2.main_id and d2.data_type=0
inner join cash_trust_log ctl2 with(NOLOCK) on m2.trust_id = ctl2.trust_id
where ctl2.business_hour_guid = ctl.business_hour_guid and d2.rating <=2 and d2.item_id = d.item_id
) as NegativeCommentsCount,
(select count(*) from view_ppon_cash_trust_log vpctl with(NOLOCK)
where vpctl.bid = ctl.business_hour_guid and vpctl.cash_trust_log_status = 2
) as TotalVerifiedCount
into #tmpData
from user_evaluate_main m with(NOLOCK)
inner join user_evaluate_detail d with(NOLOCK) on m.id = d.main_id and d.data_type=0
inner join user_evaluate_item item with(NOLOCK) on item.id = d.item_id
inner join cash_trust_log ctl with(NOLOCK) on ctl.trust_id = m.trust_id
inner join business_hour b WITH(NOLOCK) on b.GUID = ctl.business_hour_guid
where d.item_id<>5 and item.is_enable = 1 and
b.business_hour_deliver_time_s < '9999/1/1' and 
b.business_hour_deliver_time_e < '9999/1/1' and
(
 (b.business_hour_deliver_time_e <= DATEADD(DAY,21,b.business_hour_deliver_time_s) and (CONVERT(char(10), b.business_hour_deliver_time_e,126) = CONVERT(char(10), GetDate(),126)) or (CONVERT(char(10), DATEADD(MONTH,1,b.business_hour_deliver_time_e),126) = CONVERT(char(10), GetDate(),126)))
 OR
 ((b.business_hour_deliver_time_e > DATEADD(DAY,21,b.business_hour_deliver_time_s) and b.business_hour_deliver_time_e <= DATEADD(DAY,45,b.business_hour_deliver_time_s)) and (CONVERT(char(10), DATEADD(DAY,21,b.business_hour_deliver_time_s),126) = CONVERT(char(10), GetDate(),126) or (CONVERT(char(10), b.business_hour_deliver_time_e,126) = CONVERT(char(10), GetDate(),126)) or CONVERT(char(10), DATEADD(MONTH,1,b.business_hour_deliver_time_e),126) = CONVERT(char(10), GetDate(),126)))
 OR
 (b.business_hour_deliver_time_e > DATEADD(DAY,45,b.business_hour_deliver_time_s) and (CONVERT(char(10), DATEADD(DAY,21,b.business_hour_deliver_time_s),126) = CONVERT(char(10), GetDate(),126) or CONVERT(char(10), DATEADD(DAY,45,b.business_hour_deliver_time_s),126) = CONVERT(char(10), GetDate(),126) or (CONVERT(char(10), b.business_hour_deliver_time_e,126) = CONVERT(char(10), GetDate(),126)) or CONVERT(char(10), DATEADD(MONTH,1,b.business_hour_deliver_time_e),126) = CONVERT(char(10), GetDate(),126)))
)
group by ctl.business_hour_guid,d.item_id,item.sequence


select vdp.coupon_usage,vdp.title,vdp.unique_id,vdp.business_hour_order_time_s,vdp.business_hour_order_time_e
,vdp.business_hour_deliver_time_s,vdp.business_hour_deliver_time_e,vdp.op_emp_name,m.user_email as sales_email,t.*
,da.dept_id,manager.user_name as manager_email
from #tmpData t
inner join view_deal_property_business_hour_content vdp with(NOLOCK) on vdp.business_hour_guid = t.business_hour_guid
inner join deal_accounting da with(NOLOCK) on da.business_hour_guid = t.business_hour_guid
inner join employee emp with(NOLOCK) on emp.emp_name = vdp.op_emp_name
inner join member m with(NOLOCK) on m.unique_id = emp.user_id
CROSS APPLY  (
select top 1 m.user_name from employee e with(NOLOCK)
inner join department d with(NOLOCK) on e.dept_id = d.dept_id
inner join role_member rm with(NOLOCK) on e.user_id = rm.user_id 
inner join member m with(NOLOCK) on m.unique_id = e.user_id
where rm.role_GUID =  (select guid from role with(NOLOCK) where role_name = 'SalesManager')
and d.dept_id = da.dept_id
) manager	
where t.TotalVerifiedCount > 0
order by t.business_hour_guid,sequence

drop table #tmpData");

            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        private string EvaluateStatisticsSQL(Guid? sid)
        {
            return string.Format(@"
select m.trust_id,d.item_id,d.rating,d.comment,m.create_time
,ueitem.evaluate_type,ueitem.item_name,ueitem.item_desc,ueitem.is_count,ueitem.sequence
,ctl.coupon_sequence_number,ctl.usage_verified_time 
,ctl.verified_store_guid as store_guid
,ctl.business_hour_guid
,isnull((select MainBusinessHourGuid from combo_deals where combo_deals.BusinessHourGuid = ctl.business_hour_guid), ctl.business_hour_guid) 'mid'
from cash_trust_log ctl with(NOLOCK)
join user_evaluate_main m with(NOLOCK) on m.trust_id = ctl.trust_id
join user_evaluate_detail d with(NOLOCK) on d.main_id=m.id and d.data_type=0
join user_evaluate_item ueitem with(NOLOCK) on ueitem.id = d.item_id
where ueitem.is_enable = 1 and {0} ", sid != null ? " ctl.verified_store_guid = '" + sid.ToString() + "' " : " 1=1");
        }

        public DataTable EvaluateStatisticsByBidGet(Guid? sid = null)
        {
            DataTable result = new DataTable();
            //string sql = EvaluateStatisticsSQL();
            var qc = new QueryCommand(EvaluateStatisticsSQL(sid), ComboDeal.Schema.Provider.Name);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }
            return result;
        }
        public DataTable EvaluateStatisticsByBidGet(IEnumerable<Guid> productGuids, Guid? sid = null)
        {
            DataTable infos = new DataTable();
            DataTable result = new DataTable();
            var tempIds = productGuids.Distinct().ToList();
            int idx = 0;
            var bids = tempIds.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;

                string sql = string.Format(@"select * from ({0})x where x.business_hour_guid in ({1}) order by x.sequence", EvaluateStatisticsSQL(sid), string.Join(",", bids.Skip(idx).Take(batchLimit)));

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                using (IDataReader reader = DataService.GetReader(qc))
                {
                    infos.Load(reader);
                }

                result.Merge(infos);

                idx += batchLimit;
            }
            return result;
        }

        public DataTable EvaluateStoreList()
        {
            DataTable result = new DataTable();
            string sql = string.Format(@"select ps.store_guid as guid,s.seller_name as store_name,v.business_hour_guid,v.seller_GUID as seller_guid,v.seller_id,v.seller_name,p.unique_id, v.item_name,v.business_hour_order_time_s,v.business_hour_order_time_e
              ,v.business_hour_deliver_time_s,v.business_hour_deliver_time_e
            from ppon_store ps with(NOLOCK)
            inner join deal_property p with(NOLOCK) on  ps.business_hour_guid = p.business_hour_guid
            inner join view_ppon_deal v with(NOLOCK) on ps.business_hour_guid = v.business_hour_guid
            inner join seller s with(NOLOCK) on ps.store_guid=s.guid
            ");

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);

            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }
            return result;
        }

        public DataTable EvaluateStoreListBySeller()
        {
            DataTable result = new DataTable();
            string sql = string.Format(@"select guid as seller_guid,seller_id,seller_name
,Null as business_hour_guid,Null as unique_id,Null as guid,
'' as store_name,'' as item_name,Null as business_hour_order_time_s,Null as business_hour_order_time_e,Null as business_hour_deliver_time_s,Null as business_hour_deliver_time_e
from seller");

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);

            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }
            return result;
        }

        public DataTable EvaluateStoreListForCustomer(string account_id, int condition)
        {
            DataTable result = new DataTable();
            string sql = string.Format(@"select s.store_name,m.trust_id,d.item_id,d.rating,d.comment,m.create_time ,ueitem.evaluate_type,ueitem.item_name,ueitem.item_desc,ueitem.is_count 
,ctl.coupon_sequence_number,ctl.usage_verified_time   ,s.guid As store_guid  ,
ISNULL(cd.BusinessHourGuid,ctl.business_hour_guid) 'business_hour_guid'  ,
ISNULL(cd.MainBusinessHourGuid,ctl.business_hour_guid) 'mid',s.seller_guid,
ctl.status
from store s with(NOLOCK)
left join cash_trust_log ctl with(NOLOCK) on ctl.store_guid=s.Guid
left join user_evaluate_main m with(NOLOCK) on ctl.trust_id = m.trust_id
left join user_evaluate_detail d with(NOLOCK) on d.main_id = m.id and d.data_type=0
left join user_evaluate_item ueitem with(NOLOCK) on ueitem.id = d.item_id  
left join combo_deals cd with(NOLOCK) on ctl.business_hour_guid=cd.BusinessHourGuid

where m.trust_id is not null and s.Guid in
(
select store_guid from view_vbs_flattened_acl
where account_id=@account_id and is_in_store=1
and store_guid is not null and 
(store_permission=1 or seller_permission=1)");

            switch (condition)
            {
                case (int)EvaluateShowStoreDataRange.Now:
                    sql += " and deal_start_time <=GETDATE() and deal_end_time>=GETDATE()";
                    break;
            }
            sql += ")";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@account_id", account_id, System.Data.DbType.String);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }
            return result;
        }

        public DataTable EvaluateStoreListForCustomer3(List<Guid> stores, int condition)
        {
            DataTable result = new DataTable();
            string sql = string.Format(@"
SELECT s.seller_name as store_name
	,m.trust_id	,d.item_id	,d.rating	,d.comment
	,m.create_time	,ueitem.evaluate_type	,ueitem.item_name	,ueitem.item_desc
	,ueitem.is_count,ueitem.is_enable	,ctl.coupon_sequence_number	,ctl.usage_verified_time
	,s.guid AS store_guid
	,ISNULL(sl.guid, s.guid) as seller_guid	,ctl.STATUS
	,ISNULL(cd.BusinessHourGuid, ctl.business_hour_guid) 'business_hour_guid'
	,ISNULL(cd.MainBusinessHourGuid, ctl.business_hour_guid) 'mid'	
FROM cash_trust_log ctl with(NOLOCK)
JOIN seller s with(NOLOCK) ON ctl.verified_store_guid = s.Guid
JOIN user_evaluate_main m with(NOLOCK) ON ctl.trust_id = m.trust_id
JOIN user_evaluate_detail d with(NOLOCK) ON d.main_id = m.id and d.data_type=0
JOIN user_evaluate_item ueitem with(NOLOCK) ON ueitem.id = d.item_id
LEFT JOIN combo_deals cd with(NOLOCK) ON ctl.business_hour_guid = cd.BusinessHourGuid
JOIN business_hour bh with(NOLOCK) on ctl.business_hour_guid = bh.GUID 
LEFT JOIN seller sl with(NOLOCK) on sl.GUID = (
    SELECT TOP 1 parent_seller_guid 
    FROM seller_tree 
    WHERE seller_guid = s.GUID)
WHERE ctl.status = 2 AND ctl.usage_verified_time is NOT NULL AND
s.Guid IN ('{0}') ", string.Join("','", stores));

            switch (condition)
            {
                case (int)EvaluateShowStoreDataRange.Now:
                    sql += " AND bh.business_hour_deliver_time_s<=GETDATE() AND GETDATE()<= bh.business_hour_deliver_time_e ";
                    break;
            }

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }
            return result;
        }
        public DataTable EvaluateDetailRatingGet(Guid bid, Guid sid)
        {
            DataTable result = new DataTable();

            string sql = string.Format(@"
select ctl.business_hour_guid,ctl.store_guid
,ctl.item_name,cec.name as event_name,s.seller_name as store_name,ctl.usage_verified_time
,ctl.coupon_sequence_number,ctl.trust_id
from cash_trust_log ctl with(NOLOCK)
join seller s with(NOLOCK) on ctl.verified_store_guid=s.Guid 
join coupon_event_content cec with(NOLOCK) on cec.business_hour_guid = ctl.business_hour_guid
where ctl.business_hour_guid=@bid and ctl.verified_store_guid=@sid");

            var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, System.Data.DbType.Guid);
            qc.AddParameter("@sid", sid, System.Data.DbType.Guid);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }

            return result;
        }

        public DataTable EvaluateDetailRatingGetTable(Guid bid, Guid sid)
        {
            DataTable result = new DataTable();
            string sql = @"
select 
    vue.*,
    ctl.item_name,
    ctl.usage_verified_time, 
    ctl.coupon_sequence_number, 
    ps.store_name 
from view_user_evaluate vue with(nolock)
inner join cash_trust_log ctl with(nolock) on ctl.trust_id = vue.trust_id
inner join view_ppon_store ps with(nolock) on ps.store_guid = ctl.verified_store_guid and ps.business_hour_guid = @bid
where ctl.business_hour_guid=@bid and ctl.verified_store_guid=@sid
order by vue.create_time desc
";

            var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@sid", sid, DbType.Guid);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                result.Load(reader);
            }

            return result;
        }

        public ViewUserEvaluateCollection ViewUserEvaluateGet()
        {
            return DB.SelectAllColumnsFrom<ViewUserEvaluate>()
                .ExecuteAsCollection<ViewUserEvaluateCollection>();
        }

        public int UserEvaluateLogSet(UserEvaluateLog ueLog)
        {
            return DB.Save<UserEvaluateLog>(ueLog);
        }

        public UserEvaluateLog UserEvaluateLogGetByOGuid(Guid oGuid)
        {
            return DB.Get<UserEvaluateLog>(UserEvaluateLog.Columns.OrderGuid, oGuid);
        }
        #endregion

        #region Mobile Member

        public MobileMember MobileMemberGet(int userId)
        {
            return DB.Get<MobileMember>(userId);
        }
        /// <summary>
        /// 依電話號碼取得手機會員
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public MobileMember MobileMemberGet(string number)
        {
            string sql = @"
                select mm.* from mobile_member mm with(nolock)                 
                where mm.mobile_number = @number order by status desc, create_time desc";
            QueryCommand qc = new QueryCommand(sql, MobileMember.Schema.Provider.Name);
            qc.AddParameter("@number", number, DbType.String);
            MobileMember result = new MobileMember();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public MobileMember MobileMemberGetByUserName(string username)
        {
            string sql = @"
                select mm.* from mobile_member mm with(nolock) 
                inner join member m with(nolock) on m.unique_id = mm.user_id 
                where m.user_name = @uerName";
            QueryCommand qc = new QueryCommand(sql, MobileMember.Schema.Provider.Name);
            qc.AddParameter("@uerName", username, DbType.String);
            MobileMember result = new MobileMember();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public void MobileMemberSet(MobileMember mobileMember)
        {
            DB.Save(mobileMember);
        }

        public void MobileMemberDelete(string mobileMember)
        {
            DB.Delete().From(MobileMember.Schema)
                .Where(MobileMember.Columns.MobileNumber).IsEqualTo(mobileMember)
                .Execute();
        }

        public void MobileMemberDelete(int userId)
        {
            DB.Delete().From(MobileMember.Schema)
                .Where(MobileMember.Columns.UserId).IsEqualTo(userId)
                .Execute();
        }

        public MobileAuthInfo MobileAuthInfoGet(int userId, string mobile)
        {
            string sql = "select top 1 * from mobile_auth_info where user_id=@userId and mobile=@mobile";
            QueryCommand qc = new QueryCommand(sql, MobileAuthInfo.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@mobile", mobile, DbType.String);
            MobileAuthInfo mai = new MobileAuthInfo();
            mai.LoadAndCloseReader(DataService.GetReader(qc));
            return mai;
        }

        public List<MobileAuthInfo> MobileAuthInfoGetList(string mobile)
        {
            return DB.QueryOver<MobileAuthInfo>().Where(t => t.Mobile == mobile).ToList();
        }

        public void MobileAuthInfoSet(MobileAuthInfo mai)
        {
            DB.Save(mai);
        }

        public void MobileAuthInfoDelete(int userId)
        {
            DB.Delete().From<MobileAuthInfo>()
                .Where(MobileAuthInfo.Columns.UserId).IsEqualTo(userId)
                .Execute();
        }

        #endregion

        #region MembershipCardLog

        public bool MembershipCardLogSet(MembershipCardLog membershipCardLog)
        {
            return DB.Save<MembershipCardLog>(membershipCardLog) == 1;
        }
        public bool MembershipCardLogSetCollection(MembershipCardLogCollection data)
        {
            return DB.SaveAll<MembershipCardLog, MembershipCardLogCollection>(data) != 0;
        }

        #endregion MembershipCardLog

        #region ViewMembershipCardLog

        public ViewMembershipCardLogCollection ViewMembershipCardLogGetList(int userId, string orderBy)
        {
            ViewMembershipCardLogCollection col = new ViewMembershipCardLogCollection();
            //IDataReader rdr;
            if (string.IsNullOrEmpty(orderBy))
            {
                col = DB.SelectAllColumnsFrom<ViewMembershipCardLog>().Where(ViewMembershipCardLog.Columns.UserId).IsEqualTo(userId)
                        .ExecuteAsCollection<ViewMembershipCardLogCollection>();
            }
            else
            {
                col = DB.SelectAllColumnsFrom<ViewMembershipCardLog>().Where(ViewMembershipCardLog.Columns.UserId).IsEqualTo(userId)
                        .OrderAsc(orderBy)
                        .ExecuteAsCollection<ViewMembershipCardLogCollection>();

            }

            return col;
        }

        public ViewMembershipCardLogCollection ViewMembershipCardLogGetListByCardId(int cardId, bool queryAfterCol = true)
        {
            ViewMembershipCardLogCollection result = new ViewMembershipCardLogCollection();

            var col = queryAfterCol ?
                ViewMembershipCardLog.Columns.MembershipCardIdAfter :
                ViewMembershipCardLog.Columns.MembershipCardIdBefore;

            result = DB.SelectAllColumnsFrom<ViewMembershipCardLog>()
                .Where(col).IsEqualTo(cardId)
                .OrderDesc(ViewMembershipCardLog.Columns.CreateTime)
                .ExecuteAsCollection<ViewMembershipCardLogCollection>();

            return result;
        }

        public ViewMembershipCardLogCollection ViewMembershipCardLogGetListByUserCardId(int userCardId,
            MembershipCardLogType type)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCardLog>()
                .Where(ViewMembershipCardLog.Columns.UserMembershipCardId)
                .IsEqualTo(userCardId)
                .And(ViewMembershipCardLog.Columns.LogType)
                .IsEqualTo((int)type)
                .OrderDesc(ViewMembershipCardLog.Columns.CreateTime)
                .ExecuteAsCollection<ViewMembershipCardLogCollection>();
        }

        #endregion ViewMembershipCardLog

        #region TrsutRecord

        public int TrustRecordSet(TrustRecordCollection records)
        {
            return DB.SaveAll<TrustRecord, TrustRecordCollection>(records) != 0 ? 1 : 0;
        }

        public void TrustRecordSetTrust(int userId, int provider, Guid guid)
        {
            DB.Update<TrustRecord>().Set(TrustRecord.BankStatusColumn).EqualTo((int)TrustBankStatus.Trusted)
                .Set(TrustRecord.TrustedReportGuidColumn).EqualTo(guid)
                .Set(TrustRecord.TrustProviderColumn).EqualTo(provider)
                .Set(TrustRecord.TrustedTimeColumn).EqualTo(DateTime.Now)
                .Where(TrustRecord.UserIdColumn).IsEqualTo(userId)
                .And(TrustRecord.BankStatusColumn).IsEqualTo((int)TrustBankStatus.Initial)
                .And(TrustRecord.TrustedReportGuidColumn).IsNull()
                .Execute();
        }

        public TrustRecordCollection TrustRecordGetUnTrusted()
        {
            return DB.Select().From<TrustRecord>()
                .Where(TrustRecord.Columns.BankStatus).IsEqualTo((int)TrustBankStatus.Initial)
                .ExecuteAsCollection<TrustRecordCollection>();
        }

        public TrustRecordCollection TrustRecordGetUserTotalAmount(List<int> userId, DateTime today)
        {
            var result = new TrustRecordCollection();
            var tempIds = userId.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.

                string sql = string.Format(@"
select user_id,SUM(amount) as amount from trust_record WITH(NOLOCK)
WHERE user_id in ({0}) and @today = CAST(create_time as DATE)
GROUP BY user_id
", string.Join(",", tempIds.Skip(idx).Take(batchLimit)));

                QueryCommand qc = new QueryCommand(sql, TrustRecord.Schema.Provider.Name);
                qc.AddParameter("@today", today, DbType.DateTime);

                var data = new TrustRecordCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));
                result.AddRange(data);

                idx += batchLimit;
            }

            return result;
        }

        public TrustRecordCollection TrustRecordGetAll()
        {
            string sql = string.Format(@"
select * from (
select user_id,SUM(amount) as amount from trust_record
WHERE bank_status <> 0
GROUP BY user_id
)x
where x.amount>0
");
            QueryCommand qc = new QueryCommand(sql, TrustRecord.Schema.Provider.Name);

            var result = new TrustRecordCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        #endregion

        #region TrsutUserBank

        public int TrustUserBankSet(TrustUserBankCollection users)
        {
            return DB.SaveAll<TrustUserBank, TrustUserBankCollection>(users) != 0 ? 1 : 0;
        }
        public TrustUserBankCollection TrustUserBankGetLastUser(List<int> userId)
        {
            var result = new TrustUserBankCollection();
            var tempIds = userId.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.

                string sql = string.Format(@"
SELECT * FROM(
select *,ROW_NUMBER() OVER(PARTITION BY user_id ORDER BY id DESC) as r from trust_user_bank WITH(NOLOCK)
WHERE user_id in ({0})
)x
where x.r=1
", string.Join(",", tempIds.Skip(idx).Take(batchLimit)));

                QueryCommand qc = new QueryCommand(sql, TrustRecord.Schema.Provider.Name);

                var data = new TrustUserBankCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));
                result.AddRange(data);

                idx += batchLimit;
            }

            return result;
        }

        #endregion

        #region MemberMessage

        public void MemberMessageSet(MemberMessage mm)
        {
            DB.Save(mm);
        }

        public List<MemberMessage> MemberMessageGetByList(int userId)
        {
            return DB.QueryOver<MemberMessage>()
                .Where(t => t.SenderId == userId).OrderByDescending(t => t.SendTime)
                .ToList();
        }

        #endregion

        #region   (說明：訊息中心-訊息事件)

        public ActionEventInfo ActionEventInfoGet(string ActionName)
        {
            return DB.SelectAllColumnsFrom<ActionEventInfo>()
                   .Where(ActionEventInfo.Columns.ActionStartTime).IsLessThanOrEqualTo(DateTime.Now)
                   .And(ActionEventInfo.Columns.ActionEndTime).IsGreaterThan(DateTime.Now)
                   .And(ActionEventInfo.Columns.ActionName).IsEqualTo(ActionName)
                   .ExecuteAsCollection<ActionEventInfoCollection>().FirstOrDefault();
        }

        public ActionEventInfoCollection ActionEventInfoListGet()
        {
            return DB.SelectAllColumnsFrom<ActionEventInfo>()
                       .Where(ActionEventInfo.Columns.ActionStartTime).IsLessThanOrEqualTo(DateTime.Now)
                       .And(ActionEventInfo.Columns.ActionEndTime).IsGreaterThan(DateTime.Now)
                       .ExecuteAsCollection<ActionEventInfoCollection>();
        }

        public ActionEventDeviceInfo ActionEventDeviceInfoGet(int actionEventInfoId, string beaconMacAddress)
        {
            var result = new ActionEventDeviceInfo();

            string sql = @"SELECT Top 1* FROM action_event_device_info WITH(NOLOCK) " +
                        "WHERE action_event_info_id = @infoId " +
                        "AND substring(electric_mac_address,10,7) = @electicMacAddress";

            var qc = new QueryCommand(sql, ActionEventDeviceInfo.Schema.Provider.Name);
            qc.AddParameter("@infoId", actionEventInfoId, DbType.Int32);
            qc.AddParameter("@electicMacAddress", beaconMacAddress, DbType.String);

            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ActionEventDeviceInfoCollection ActionEventDeviceInfoListGet(int actionEventInfoId)
        {
            return DB.SelectAllColumnsFrom<ActionEventDeviceInfo>().Where(ActionEventDeviceInfo.Columns.ActionEventInfoId).IsEqualTo(actionEventInfoId).ExecuteAsCollection<ActionEventDeviceInfoCollection>();
        }

        public ActionEventDeviceInfo ActionEventDeviceInfoByMajorMinorGet(int actionEventInfoId, int major, int minor)
        {

            return DB.SelectAllColumnsFrom<ActionEventDeviceInfo>().Where(ActionEventDeviceInfo.Columns.ActionEventInfoId).IsEqualTo(actionEventInfoId)
                                                                    .And(ActionEventDeviceInfo.Columns.Major).IsEqualTo(major)
                                                                    .And(ActionEventDeviceInfo.Columns.Minor).IsEqualTo(minor)
                                                                    .ExecuteAsCollection<ActionEventDeviceInfoCollection>().FirstOrDefault();
        }

        public bool ActionEventDeviceInfoSet(ActionEventDeviceInfo actionEventDeviceInfo)
        {
            DB.Save<ActionEventDeviceInfo>(actionEventDeviceInfo);
            return true;
        }

        public ActionEventDeviceInfoCollection ActionEventDeviceInfoGetByElePower(int actionEventInfoId, BeaconPowerLevel powerLevel, string shopCode = "")
        {
            var result = new ActionEventDeviceInfoCollection();

            string sql = @"SELECT * FROM action_event_device_info WITH(NOLOCK) WHERE action_event_info_id = @infoId";

            if (!string.IsNullOrEmpty(shopCode))
            {
                sql += " AND shop_code = @shopCode";
            }

            switch (powerLevel)
            {
                case BeaconPowerLevel.Full:
                    sql += " AND electric_power >= 2.6";
                    break;
                case BeaconPowerLevel.Medium:
                    sql += " AND electric_power between 2.4 and 2.5";
                    break;
                case BeaconPowerLevel.Low:
                    sql += " AND electric_power <= 2.3";
                    break;
            }

            var qc = new QueryCommand(sql, ActionEventDeviceInfo.Schema.Provider.Name);
            qc.AddParameter("@infoId", actionEventInfoId, DbType.Int32);
            if (!string.IsNullOrEmpty(shopCode))
            {
                qc.AddParameter("@shopCode", shopCode, DbType.String);
            }

            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ActionEventDeviceInfoCollection ActionEventDeviceInfoGetList(int pageStart, int pageLength,
            string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = ActionEventDeviceInfo.Columns.ElectricPower;
            QueryCommand qc = SSHelper.GetWhereQC<ActionEventDeviceInfo>(filter);
            qc.CommandSql = "select * from " + ActionEventDeviceInfo.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ActionEventDeviceInfoCollection view = new ActionEventDeviceInfoCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ActionEventPushMessage ActionEventPushMessageGetByFamiPortId(int famiportId)
        {
            return DB.Get<ActionEventPushMessage>(ActionEventPushMessage.Columns.FamportId, famiportId);
        }

        public ActionEventPushMessageCollection ActionEventPushMessageGetBySkmBeaconMessageId(int skmbeaconid)
        {
            return DB.SelectAllColumnsFrom<ActionEventPushMessage>()
                .Where(ActionEventPushMessage.Columns.SkmBeaconMessageId).IsEqualTo(skmbeaconid)
                .ExecuteAsCollection<ActionEventPushMessageCollection>();
        }

        public bool ActionEventPushMessageSet(ActionEventPushMessage actionEventPushMessage)
        {
            DB.Save<ActionEventPushMessage>(actionEventPushMessage);
            return true;
        }

        public ActionEventPushMessageCollection ActionEventPushMessageByFamiGetList()
        {
            string sql = @"select * from " + ActionEventPushMessage.Schema.TableName + " with(nolock) WHERE [event_type]=1 and famport_id in (select id from famiport where [type]=1) ";

            var qc = new QueryCommand(sql, ActionEventPushMessage.Schema.Provider.Name);

            var data = new ActionEventPushMessageCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ActionEventPushMessageCollection ActionEventPushMessageGetList(int pageStart, int pageLength,
            string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = ActionEventPushMessage.Columns.Id;
            QueryCommand qc = SSHelper.GetWhereQC<ActionEventPushMessage>(filter);
            qc.CommandSql = "select * from " + ActionEventPushMessage.Schema.TableName + " with(nolock) WHERE [event_type]=1 and famport_id in (select id from famiport where [type]=1) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }
            if (pageLength != -1)
            {
                SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }
            ActionEventPushMessageCollection view = new ActionEventPushMessageCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ActionEventPushMessageCollection ActionEventPushMessageListGet()
        {
            return DB.SelectAllColumnsFrom<ActionEventPushMessage>().ExecuteAsCollection<ActionEventPushMessageCollection>();
        }

        public ActionEventPushMessageCollection ActionEventPushMessageOfNowListGet(DateTime nowDateTime)
        {
            return DB.SelectAllColumnsFrom<ActionEventPushMessage>()
                .Where(ActionEventPushMessage.Columns.SendStartTime).IsLessThanOrEqualTo(nowDateTime)
                .And(ActionEventPushMessage.Columns.SendEndTime).IsGreaterThanOrEqualTo(nowDateTime)
                .And(ActionEventPushMessage.Columns.Status).IsEqualTo(true)
                .ExecuteAsCollection<ActionEventPushMessageCollection>();
        }
        public ActionEventPushMessage ActionEventPushMessageGetByActionEventPushMessageId(int actionEventPushMessageId)
        {
            return DB.Get<ActionEventPushMessage>(ActionEventPushMessage.Columns.Id, actionEventPushMessageId);
        }

        #endregion (說明：訊息中心-訊息事件)

        #region "MemberNoneDeposit"
        public void MemberNoneDepositSet(MemberNoneDeposit mnd)
        {
            DB.Save<MemberNoneDeposit>(mnd);
        }
        #endregion

        #region EmailDomain
        public EmailDomainCollection EmailDomainGetListByDomain(string domain)
        {
            return
                DB.SelectAllColumnsFrom<EmailDomain>().Where(EmailDomain.Columns.Domain).IsEqualTo(domain).OrderDesc(EmailDomain.Columns.Id).
                ExecuteAsCollection<EmailDomainCollection>();
        }

        public void EmailDomainSet(EmailDomain email_domain)
        {
            if (EmailDomainIsExist(email_domain.Domain))
            {
                DB.Update<EmailDomain>().Set(EmailDomain.StatusColumn).EqualTo(email_domain.Status)
                    .Set(EmailDomain.CreateTimeColumn).EqualTo(email_domain.CreateTime)
                    .Where(EmailDomain.DomainColumn).IsEqualTo(email_domain.Domain).Execute();
            }
            else
            {
                DB.Save<EmailDomain>(email_domain);
            }
        }

        /// <summary>
        /// 判斷資料是否已存在
        /// </summary>
        public bool EmailDomainIsExist(string domain)
        {
            EmailDomain ed = DB.Select().From(EmailDomain.Schema.TableName)
                .Where(EmailDomain.Columns.Domain).IsEqualTo(domain)
                .ExecuteSingle<EmailDomain>();

            if (ed.IsLoaded)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region MGM
        public bool MemberMsgSet(MemberMessage memberMsg)
        {
            return DB.Save(memberMsg) > 0;
        }

        public MemberFastInfo MemberFastInfoGet(int userId)
        {
            return DB.Get<MemberFastInfo>(MemberFastInfo.Columns.UserId, userId);
        }

        public bool MemberFastInfoSet(MemberFastInfo memberFastInf)
        {
            return DB.Save(memberFastInf) > 0;
        }

        public bool MemberContactSet(MemberContact memberContact)
        {
            return DB.Save(memberContact) > 0;
        }

        public bool MemberGroupSet(MemberGroup memberGroup)
        {
            return DB.Save(memberGroup) > 0;
        }

        public bool MemberContactGroupSet(MemberContactGroup memberContactGroup)
        {
            return DB.Save(memberContactGroup) > 0;
        }

        public ViewMgmContactGroupCollection ViewMgmContactGroupGetById(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewMgmContactGroup>()
               .Where(ViewMgmContactGroup.Columns.UserId).IsEqualTo(userId)
               .ExecuteAsCollection<ViewMgmContactGroupCollection>();
        }

        public MemberContact MemberContactGetByGuid(Guid guid)
        {
            return DB.SelectAllColumnsFrom<MemberContact>()
                .Where(MemberContact.Columns.Guid).IsEqualTo(guid)
                .ExecuteSingle<MemberContact>();
        }

        #endregion

        #region OneTimeEvent
        public DataTable GetOneTimeEventMailList()
        {
            string sql = @"select distinct m.user_name as member_mail,o.member_name from [order] as o with(nolock)
                            left join member m on o.user_id=m.unique_id
                            where o.create_time between '2016/07/01 00:00:00' and '2016/07/31 23:59:59'
                            and m.city_id in (
                            155,156,157,158,159,160,161,162,163,164,165,166,167,168,170,171,172,173,174,175,
                            176,177,178,179,180,181,182,183,184,185,186,188,189,190,192,193,194,195,196,197,198)
                          ";

            QueryCommand qc = new QueryCommand(sql, ResourceAcl.Schema.Provider.Name);

            IDataReader idr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }
        #endregion

        #region MemberCreditCard
        // Create MemberCreditCard
        public int MemberCreditCardSet(MemberCreditCard memberCreditCard)
        {
            return DB.Save(memberCreditCard);
        }

        // Read MemberCreditCard by Id
        public MemberCreditCard MemberCreditCardGet(int id)
        {
            return DB.Get<MemberCreditCard>(id);
        }

        public MemberCreditCard MemberCreditCardGet(Guid guid)
        {
            return DB.Get<MemberCreditCard>(MemberCreditCard.Columns.Guid, guid);
        }

        public MemberCreditCard MemberCreditCardGetByGuidUserId(Guid guid, int userId)
        {
            return DB.SelectAllColumnsFrom<MemberCreditCard>()
                .Where(MemberCreditCard.Columns.Guid).IsEqualTo(guid)
                .And(MemberCreditCard.Columns.UserId).IsEqualTo(userId)
                .ExecuteSingle<MemberCreditCard>();
        }

        public MemberCreditCardCollection MemberCreditCardGetBinNullList()
        {
            SqlQuery qry = DB.SelectAllColumnsFrom<MemberCreditCard>().Where(MemberCreditCard.Columns.Bin).IsNull();
            return qry.ExecuteAsCollection<MemberCreditCardCollection>();
        }

        // Read MemberCreditCard by user id
        public MemberCreditCardCollection MemberCreditCardGetList(int userid)
        {
            SqlQuery qry = DB.SelectAllColumnsFrom<MemberCreditCard>().Where(MemberCreditCard.Columns.UserId).IsEqualTo(userid);
            return qry.ExecuteAsCollection<MemberCreditCardCollection>();
        }

        public MemberCreditCardCollection MemberCreditCardGetCardList(int userid)
        {
            SqlQuery qry = DB.Select(MemberCreditCard.Columns.CreditCardName, MemberCreditCard.Columns.Guid)
                             .From<MemberCreditCard>()
                             .Where(MemberCreditCard.Columns.UserId).IsEqualTo(userid)
                             .OrderAsc(MemberCreditCard.Columns.CreateTime);
            return qry.ExecuteAsCollection<MemberCreditCardCollection>();
        }

        public int MemberCreditCardDeleteByGuidUserId(Guid guid, int userId)
        {
            return DB.Delete().From<MemberCreditCard>()
                    .Where(MemberCreditCard.Columns.Guid).IsEqualTo(guid).And(MemberCreditCard.Columns.UserId)
                    .IsEqualTo(userId).Execute();
        }

        public MemberCreditCard MemberCreditCardGetLastUsed(int userid)
        {
            return DB.Select().Top("1")
                     .From<MemberCreditCard>()
                     .Where(MemberCreditCard.Columns.UserId).IsEqualTo(userid)
                     .And(MemberCreditCard.Columns.LastUseTime).IsNotNull()
                     .OrderDesc("last_use_time")
                     .ExecuteSingle<MemberCreditCard>();
        }

        /// <summary>
        /// 更新最後使用時間
        /// </summary>
        /// <param name="Guid"></param>
        /// <param name="userid"></param>
        public int MemberCreditCardSetLastUsed(Guid Guid, int userid)
        {
            return DB.Update<MemberCreditCard>()
                   .Set(MemberCreditCard.LastUseTimeColumn).EqualTo(DateTime.Now)
                   .Where(MemberCreditCard.GuidColumn).IsEqualTo(Guid)
                   .And(MemberCreditCard.UserIdColumn).IsEqualTo(userid)
                   .Execute();
        }


        #endregion

        #region einvoice
        public EinvoiceMainCollection GetEinvoiceMainByAllowanceStatusAndOrderId(string orderId, int status)
        {
            return DB.SelectAllColumnsFrom<EinvoiceMain>().Where(EinvoiceMain.Columns.OrderId).IsEqualTo(orderId)
                                                          .And(EinvoiceMain.Columns.AllowanceStatus).IsNotEqualTo(status)
                                                          .Or(EinvoiceMain.Columns.OrderId).IsEqualTo(orderId)
                                                          .And(EinvoiceMain.Columns.AllowanceStatus).IsNull()
                                                          .ExecuteAsCollection<EinvoiceMainCollection>();
        }

        public void EinvoiceMainSet(EinvoiceMain Einvoice)
        {
            DB.Save(Einvoice);
        }
        #endregion
    }
}
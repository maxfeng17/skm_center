﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LunchKingSite.SsBLL.Provider
{
    public class SSHumanProvider : IHumanProvider
    {
        #region seller_Mapping_employee

        public ViewSellerMappingEmployee SellerMappingEmployeeGetBySeller(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<ViewSellerMappingEmployee>()
                .Where(ViewSellerMappingEmployee.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteSingle<ViewSellerMappingEmployee>();
        }

        public SellerMappingEmployee SellerMappingEmployeeGet(Guid seller_Guid)
        {
            return DB.SelectAllColumnsFrom<SellerMappingEmployee>().Where(SellerMappingEmployee.Columns.SellerGuid).IsEqualTo(seller_Guid)
                   .ExecuteSingle<SellerMappingEmployee>();
        }

        public SellerMappingEmployeeCollection SellerMappingEmployeeByEmpId(string empId)
        {
            return DB.SelectAllColumnsFrom<SellerMappingEmployee>().Where(SellerMappingEmployee.Columns.EmpId).IsEqualTo(empId)
                   .ExecuteAsCollection<SellerMappingEmployeeCollection>();
        }

        public bool SellerMappingEmployeeSet(SellerMappingEmployee entity)
        {
            DB.Save<SellerMappingEmployee>(entity);
            return true;
        }
        #endregion

        #region Employee & ViewEmployee
        public bool EmployeeSet(Employee emp)
        {
            DB.Save<Employee>(emp);
            return true;
        }
        public Employee EmployeeGet(string empNo)
        {
            return DB.Get<Employee>(empNo);
        }
        public Employee EmployeeGet(string column, object value)
        {
            return DB.Get<Employee>(column, value);
        }
        public Employee EmployeeGetByUserId(int userId)
        {
            return DB.Get<Employee>(Employee.Columns.UserId, userId);
        }
        public string EmployeeGetLastEmpId()
        {
            var emp = new Employee();
            var sql = "select top 1 * from " + Employee.Schema.TableName + " order by " + Employee.Columns.EmpId +
                         " desc";
            var qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            emp.LoadAndCloseReader(DataService.GetReader(qc));
            return emp.EmpId;
        }
        public EmployeeCollection EmployeeCollectionGetByFilter(string column, object value)
        {
            return Select.AllColumnsFrom<Employee>().Where(column).IsEqualTo(value).OrderDesc(Employee.Columns.CreateTime).ExecuteAsCollection<EmployeeCollection>();
        }
        public EmployeeCollection EmployeeCollectionGetByDepartment(EmployeeDept dept)
        {
            return Select.AllColumnsFrom<Employee>().Where(Employee.Columns.DeptId).In(DepartmentGetListByEnabled(true, dept).Select(x => x.DeptId)).OrderDesc(Employee.Columns.CreateTime).ExecuteAsCollection<EmployeeCollection>();
        }
        public ViewEmployee ViewEmployeeGet(string column, object value)
        {
            ViewEmployee view = new ViewEmployee();
            view.LoadByParam(column, value);
            if (string.IsNullOrEmpty(view.EmpNo))
            {
                view.IsLoaded = false;
            }
            return view;
        }

        public ViewEmployee ViewEmployeeByUserIdAndDept(int userId,string dept_id)
        {
            return DB.SelectAllColumnsFrom<ViewEmployee>().Where(ViewEmployee.Columns.UserId).IsEqualTo(userId)
                   .And(ViewEmployee.Columns.IsInvisible).IsEqualTo(0).And(ViewEmployee.Columns.DeptId).IsEqualTo(dept_id)
                   .ExecuteSingle<ViewEmployee>();
        }


        /// <summary>
        /// 取得員工資料
        /// </summary>
        /// <param name="column"></param>
        /// <param name="value"></param>
        /// <param name="isInvisible">是否需查詢離職員工(傳入null則搜尋全部資料)</param>
        /// <returns></returns>
        public ViewEmployeeCollection ViewEmployeeCollectionGetByFilter(string column, object value, bool? isInvisible = null)
        {
            SqlQuery query = Select.AllColumnsFrom<ViewEmployee>().Where(column).IsEqualTo(value);
            if (isInvisible != null)
            {
                query.And(ViewEmployee.Columns.IsInvisible).IsEqualTo(isInvisible);
            }
            return query.OrderDesc(ViewEmployee.Columns.CreateTime).ExecuteAsCollection<ViewEmployeeCollection>();
        }
        public ViewEmployeeCollection ViewEmployeeCollectionGetByDepartment(EmployeeDept dept)
        {
            return Select.AllColumnsFrom<ViewEmployee>()
                .Where(ViewEmployee.Columns.DeptId).In(DepartmentGetListByEnabled(true, dept)
                .Select(x => x.DeptId))
                .OrderDesc(ViewEmployee.Columns.CreateTime)
                .ExecuteAsCollection<ViewEmployeeCollection>();
        }
        public ViewEmployeeCollection ViewEmployeeCollectionGetAll()
        {
            return Select.AllColumnsFrom<ViewEmployee>()
                .OrderDesc(ViewEmployee.Columns.CreateTime)
                .ExecuteAsCollection<ViewEmployeeCollection>();
        }

        public ViewEmployeeCollection ViewEmployeeByLikeUserName(string username)
        {
            return Select.AllColumnsFrom<ViewEmployee>()
                .Where(ViewEmployee.Columns.Email).Like("%" + username + "%")
                .ExecuteAsCollection<ViewEmployeeCollection>();
        }

        public ViewEmployeeCollection ViewEmployeeByRoleName(string roleName)
        {
            string sql = @"select emp.* from view_employee emp with(NOLOCK)
inner join role_member rm with(nolock) on emp.user_id = rm.user_id
inner join [role] ro with(nolock) on ro.GUID = rm.role_GUID
where ro.role_name = @roleName";

            QueryCommand qc = new QueryCommand(sql, Role.Schema.Provider.Name);
            qc.AddParameter("roleName", roleName, DbType.String);
            ViewEmployeeCollection emp = new ViewEmployeeCollection();
            emp.LoadAndCloseReader(DataService.GetReader(qc));
            return emp;
        }

        public List<string> ViewEmployeeGetColumnListByLike(string column, string partValue)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(partValue))
            {
                string sql = @"SELECT {0}.{1} FROM {0} WITH (NOLOCK) WHERE {1} LIKE N'%' + @Value + '%'";
                sql = string.Format(sql, ViewEmployee.Schema.TableName, column);
                QueryCommand query = new QueryCommand(sql, ViewEmployee.Schema.Provider.Name);
                query.AddParameter("@Value", partValue, DbType.String);

                using (IDataReader reader = DataService.GetReader(query))
                {
                    while (reader.Read())
                    {
                        list.Add(reader.GetString(0));
                    }
                }
            }
            return list;
        }
        #endregion Employee

        #region Department
        public Department DepartmentGet(string deptId)
        {
            return DB.Get<Department>(Department.Columns.DeptId, deptId);
        }
        public DepartmentCollection DepartmentGetParentDeptListByEnabled(bool enabled)
        {
            return DB.SelectAllColumnsFrom<Department>().Where(Department.Columns.Enabled).IsEqualTo(enabled).And(Department.Columns.ParentDeptId).IsNull().OrderAsc(
                Department.Columns.DeptId).ExecuteAsCollection<DepartmentCollection>();
        }

        public DepartmentCollection DepartmentGetAllSubDepartmentsByEnabled(bool enabled)
        {
            return DB.SelectAllColumnsFrom<Department>().Where(Department.Columns.Enabled).IsEqualTo(enabled).And(Department.Columns.ParentDeptId).IsNotNull().OrderAsc(Department.Columns.DeptId).ExecuteAsCollection<DepartmentCollection>();
        }

        public DepartmentCollection DepartmentGetListByEnabled(bool enabled, EmployeeDept parentDeptId = EmployeeDept.S000)
        {
            return DB.SelectAllColumnsFrom<Department>().Where(Department.Columns.ParentDeptId).IsEqualTo(parentDeptId.ToString()).And(Department.Columns.Enabled).IsEqualTo(enabled).OrderAsc(
                Department.Columns.DeptId).ExecuteAsCollection<DepartmentCollection>();
        }
        /// <summary>
        /// 抓出業績歸屬的單位, 包含行銷和業務底下所有Enable的單位
        /// </summary>
        public DepartmentCollection DepartmentGetListBySalesAndMarketing(bool enabled)
        {
            return DB.SelectAllColumnsFrom<Department>().Where(Department.Columns.ParentDeptId).IsEqualTo(EmployeeDept.S000)
                .Or(Department.Columns.ParentDeptId).IsEqualTo(EmployeeDept.M000).And(Department.Columns.Enabled).IsEqualTo(enabled).OrderAsc(
                Department.Columns.DeptId).ExecuteAsCollection<DepartmentCollection>();
        }

        #endregion Department

        #region organization
        public void SetOrganization(Organization organization)
        {
            DB.Save(organization);
        }

        public void DeleteOrganization(string name)
        {
            DB.Delete<Organization>(Organization.Columns.Name, name);
        }

        public Organization GetOrganization(string name)
        {
            return DB.Get<Organization>(Organization.Columns.Name, name);
        }

        public OrganizationCollection GetOrganizationListByOrgNameList(List<string> orgNames)
        {
            return DB.SelectAllColumnsFrom<Organization>().Where(Organization.Columns.Name).In(orgNames).ExecuteAsCollection<OrganizationCollection>();
        }

        public OrganizationCollection GetOrganizationListByParentOrgName(string parentOrgName)
        {
            return DB.SelectAllColumnsFrom<Organization>().Where(Organization.Columns.ParentOrgName).IsEqualTo(parentOrgName).ExecuteAsCollection<OrganizationCollection>();
        }

        public OrganizationCollection GetOrganizationList(string orgName)
        {
            return DB.SelectAllColumnsFrom<Organization>().Where(Organization.Columns.DisplayName).ContainsString(orgName).OrderAsc(Organization.Columns.DisplayName).ExecuteAsCollection<OrganizationCollection>();
        }
        #endregion

        #region Privilege
        public void SetPrivilege(Privilege privilege)
        {
            DB.Save(privilege);
        }

        public void DeletePrivilege(int privilegeId)
        {
            DB.Delete<Privilege>(Privilege.Columns.Id, privilegeId);
        }

        public void PrivilegeDeleteByUser(int userId)
        {
            DB.Delete<Privilege>(Privilege.Columns.UserId, userId);
        }

        public Privilege GetPrivilege(string userName, int funcId)
        {
            string sql = @"select top 1 pri.* from privilege pri inner join member m on m.[unique_id] = pri.[user_id] 
                                where m.[user_name] = @userName
                                and pri.[func_id] = @funcId";
            sql = sql.Replace("[unique_id]", Member.Columns.UniqueId);
            sql = sql.Replace("[user_id]", Privilege.Columns.UserId);
            sql = sql.Replace("[user_name]", Member.Columns.UserName);
            sql = sql.Replace("[func_id]", Privilege.Columns.FuncId);

            var qc = new QueryCommand(sql, Privilege.Schema.Provider.Name);
            qc.AddParameter("@userName", userName, DbType.String);
            qc.AddParameter("@funcId", funcId, DbType.Int32);
            Privilege pri = new Privilege();
            pri.LoadAndCloseReader(DataService.GetReader(qc));
            return pri;
        }

        public PrivilegeCollection GetPrivilegeListByEmail(string email)
        {
            //return DB.SelectAllColumnsFrom<Privilege>().Where(Privilege.Columns.Email).IsEqualTo(email).ExecuteAsCollection<PrivilegeCollection>();
            string sql = @"select pri.* from privilege pri inner join member m on m.[unique_id] = pri.[user_id]
                                where m.[user_name] = @email";
            sql = sql.Replace("[unique_id]", Member.Columns.UniqueId);
            sql = sql.Replace("[user_id]", Privilege.Columns.UserId);
            sql = sql.Replace("[user_name]", Member.Columns.UserName);

            var qc = new QueryCommand(sql, Privilege.Schema.Provider.Name);
            qc.AddParameter("@email", email, DbType.String);
            PrivilegeCollection priCol = new PrivilegeCollection();
            priCol.LoadAndCloseReader(DataService.GetReader(qc));
            return priCol;
        }

        public PrivilegeCollection GetPrivilegeListByFuncId(int funcId)
        {
            return DB.SelectAllColumnsFrom<Privilege>().Where(Privilege.Columns.FuncId).IsEqualTo(funcId).ExecuteAsCollection<PrivilegeCollection>();
        }

        public ViewPrivilegeCollection ViewPrivilegeGetList(string userName)
        {
            return DB.SelectAllColumnsFrom<ViewPrivilege>()
              .Where(ViewPrivilege.Columns.UserName)
              .IsEqualTo(userName)
              .ExecuteAsCollection<ViewPrivilegeCollection>();
        }

        public ViewPrivilegeCollection ViewPrivilegeGetListByFuncId(int funcId)
        {
            return DB.SelectAllColumnsFrom<ViewPrivilege>()
                .Where(ViewPrivilege.Columns.FuncId).IsEqualTo(funcId)
                .ExecuteAsCollection<ViewPrivilegeCollection>();
        }

        public ViewPrivilegeCollection ViewPrivilegeGetListByFuncType(string link, SystemFunctionType type)
        {
            return DB.SelectAllColumnsFrom<ViewPrivilege>()
                .Where(ViewPrivilege.Columns.Link).IsEqualTo(link)
                .And(ViewPrivilege.Columns.FuncType).IsEqualTo(type.ToString())
                .ExecuteAsCollection<ViewPrivilegeCollection>();
        }

        public void PrivilegeSetList(PrivilegeCollection pc)
        {
            DB.SaveAll(pc);
        }

        public void PrivilegeSetListByOrgName(int userId, string createId, string orgName)
        {
            string sql = @" INSERT into privilege
                            SELECT op.func_id, @createId, GETDATE(), @userId
                            FROM org_privilege op
                            JOIN [role] r on r.role_name = op.org_name
                            WHERE r.role_name = @orgName
                            AND op.func_id NOT in (SELECT p.func_id from privilege p WHERE p.user_id = @userId)
                            order BY op.func_id ";
            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@createId", createId, DbType.String);
            qc.AddParameter("@orgName", orgName, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        public void PrivilegeSetListByOrgName(string orgName, List<int> funcIds, string createId)
        {
            funcIds.Add(0);
            string sql = @" INSERT INTO privilege
                            SELECT f.id, @createId, GETDATE(), users.user_id
                            FROM system_function f
                            JOIN (
                            SELECT rm.user_id FROM role_member rm
                            JOIN [role] r on rm.role_GUID = r.GUID
                            JOIN organization og ON og.name = r.role_name
                            WHERE og.name = @orgName
                            ) as users ON users.user_id = users.user_id
                            LEFT join privilege p ON p.user_id = users.user_id AND p.func_id = f.id
                            where f.id in (" + string.Join(",", funcIds.ToArray()) + @") AND p.id IS NULL ";
            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@createId", createId, DbType.String);
            qc.AddParameter("@orgName", orgName, DbType.String);
            DataService.ExecuteScalar(qc);
        }

        public void OrgPrivilegeDeleteList(string orgName, List<int> funcIds)
        {
            funcIds.Add(0);
            string sql = @" DELETE org_privilege
                            WHERE id in (
                                SELECT op.id FROM system_function f
                                join org_privilege op ON op.func_id = f.id
                                join organization o ON op.org_name = o.name
                                WHERE o.name = @orgName
                                AND op.func_id NOT in (" + string.Join(",", funcIds.ToArray()) + @")
                            ) ";

            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@orgName", orgName, DbType.String);
            DataService.ExecuteScalar(qc);
        }
        #endregion

        #region OrgPrivilege
        public void SetOrgPrivilege(OrgPrivilege orgPrivilege)
        {
            DB.Save(orgPrivilege);
        }

        public void DeleteOrgPrivilege(int id)
        {
            DB.Delete<OrgPrivilege>(OrgPrivilege.Columns.Id, id);
        }

        public void DeleteOrgPrivilegeByOrgName(string orgName)
        {
            DB.Delete<OrgPrivilege>(OrgPrivilege.Columns.OrgName, orgName);
        }

        public OrgPrivilege GetOrgPrivilege(string orgName, int funcId)
        {
            return DB.SelectAllColumnsFrom<OrgPrivilege>().Where(OrgPrivilege.Columns.OrgName).IsEqualTo(orgName).And(OrgPrivilege.Columns.FuncId).IsEqualTo(funcId).ExecuteSingle<OrgPrivilege>();
        }

        public OrgPrivilegeCollection GetOrgPrivilegeListByOrgName(string orgName)
        {
            return DB.SelectAllColumnsFrom<OrgPrivilege>().Where(OrgPrivilege.Columns.OrgName).IsEqualTo(orgName).ExecuteAsCollection<OrgPrivilegeCollection>();
        }

        public OrgPrivilegeCollection GetOrgPrivilegeListByFuncId(int funcId)
        {
            return DB.SelectAllColumnsFrom<OrgPrivilege>().Where(OrgPrivilege.Columns.FuncId).IsEqualTo(funcId).ExecuteAsCollection<OrgPrivilegeCollection>();
        }

        public void OrgPrivilegeSetListByOrgName(string orgName, List<int> funcIds, string createId)
        {
            funcIds.Add(0);
            string sql = @" INSERT into org_privilege
                            select @orgName, f.id, @createId, GETDATE()
                            from system_function f
                            where id in (" + string.Join(",", funcIds.ToArray()) + @")
                            AND id NOT IN (SELECT opp.func_id FROM org_privilege opp where opp.org_name = @orgName) ";
            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@orgName", orgName, DbType.String);
            qc.AddParameter("@createId", createId, DbType.String);
            DataService.ExecuteScalar(qc);
        }
        #endregion

        #region SalesVolumeMonth

        public SalesVolumeMonthCollection GetSalesVolumeMonthList(int userId)
        {
            return DB.SelectAllColumnsFrom<SalesVolumeMonth>().Where(SalesVolumeMonth.Columns.SalesId).In(userId).ExecuteAsCollection<SalesVolumeMonthCollection>();
        }

        public int SalesVolumeMonthSet(SalesVolumeMonth s)
        {
            return DB.Save<SalesVolumeMonth>(s);
        }

        public int SalesVolumeMonthUpdate(SalesVolumeMonth s)
        {
            return DB.Update<SalesVolumeMonth>(s);
        }
        #endregion
        /// <summary>
        /// 員工已過試用期狀態更新
        /// </summary>
        /// <param name="OfficialMonth"></param>
        /// <param name="isOfficial"></param>
        /// <returns></returns>
        public int updateEmployeeIsOfficial(int OfficialMonth, bool isOfficial)
        {
            int Official = 0;
            if (isOfficial) {
                Official = 1;
            }
            string sql = "UPDATE " + Employee.Schema.TableName + " SET is_Official=@Official, modify_time='" + DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + "',modify_id='sys' where '" 
                + DateTime.Now.ToString("yyyyMM") + "'>=substring(convert(varchar, DATEADD (MONTH , @OfficialMonth , " + Employee.Columns.CreateTime + " ), 112),1,6) and " 
                + Employee.Columns.IsOfficial + "=0";
            QueryCommand qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            qc.AddParameter("@Official", Official, DbType.Int32);
            qc.AddParameter("@OfficialMonth", OfficialMonth, DbType.Int32);
            int affectCount = DataService.ExecuteQuery(qc);
            return affectCount;
        }
    }
}

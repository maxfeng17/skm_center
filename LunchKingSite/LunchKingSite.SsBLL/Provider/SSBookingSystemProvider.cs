﻿using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace LunchKingSite.SsBLL
{
    public class SSBookingSystemProvider : IBookingSystemProvider
    {
        #region View

        #region view_booking_system_store_date

        /// <summary>
        /// 取出店家在查詢日期的開店資訊
        /// </summary>
        /// <param name="store">BookingSystemStore</param>
        /// <param name="date">Query Date</param>
        /// <returns></returns>
        public ViewBookingSystemStoreDateCollection ViewBookingSystemStoreDateGetByDate(BookingSystemStore store, DateTime date)
        {
            string sql = @"select * from dbo.view_booking_system_store_date 
                            Where booking_system_store_booking_id=@bookingSystemStoreBookingId AND (effective_date=@effectiveDate or effective_date is null)
                            Order By day_Type";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemStoreDate.Schema.Provider.Name);
            qc.AddParameter("@bookingSystemStoreBookingId", store.BookingId, DbType.Int32);
            qc.AddParameter("@effectiveDate", date.ToString("yyyy/MM/dd"), DbType.DateTime);
            ViewBookingSystemStoreDateCollection viewBookingSystemStoreDateCol = new ViewBookingSystemStoreDateCollection();
            viewBookingSystemStoreDateCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemStoreDateCol;
        }

        public DayType DayTypeParse(DateTime date)
        {
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return DayType.SUN;
                case DayOfWeek.Monday:
                    return DayType.MON;
                case DayOfWeek.Tuesday:
                    return DayType.TUE;
                case DayOfWeek.Wednesday:
                    return DayType.WED;
                case DayOfWeek.Thursday:
                    return DayType.THU;
                case DayOfWeek.Friday:
                    return DayType.FRI;
                case DayOfWeek.Saturday:
                    return DayType.SAT;
                default:
                    return DayType.DefaultSet;
            }
        }

        public ViewBookingSystemStoreDateCollection ViewBookingSystemStoreDateGetByBookingSystemStoreGuid(
            Guid storeGuid)
        {
            string sql = @"select * from dbo.view_booking_system_store_date 
                            Where booking_system_store_booking_id in (select booking_id from booking_system_store where store_guid =@storeGuid )";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemStoreDate.Schema.Provider.Name);
            qc.AddParameter("@storeGuid", storeGuid.ToString(), DbType.String);
            ViewBookingSystemStoreDateCollection viewBookingSystemStoreDateCol = new ViewBookingSystemStoreDateCollection();
            viewBookingSystemStoreDateCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemStoreDateCol;

        }


        #endregion

        #region view_booking_system_reservation_info_time

        /// <summary>
        /// 取得店家當天訂位時段已訂位人數, 訂位人數上限, 剩餘訂位人數
        /// </summary>
        /// <param name="store"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public ViewBookingSystemReservationInfoTimeCollection ViewBookingSystemReservationInfoTimeGetByDate(BookingSystemStore store, DateTime date)
        {
            string sql = @"select * from dbo.view_booking_system_reservation_info_time
                          where booking_system_store_booking_id=@bookingSystemStoreBookingId AND reservation_date = @reservation_date
                          order by time_slot";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationInfoTime.Schema.Provider.Name);
            qc.AddParameter("@bookingSystemStoreBookingId", store.BookingId, DbType.Int32);
            qc.AddParameter("@reservation_date", date.ToString("yyyy/MM/dd"), DbType.DateTime);
            ViewBookingSystemReservationInfoTimeCollection viewBookingSystemReservationInfoTimeCol = new ViewBookingSystemReservationInfoTimeCollection();
            viewBookingSystemReservationInfoTimeCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemReservationInfoTimeCol;
        }

        #endregion

        #region view_booking_system_reservation_info_day

        public ViewBookingSystemReservationInfoDayCollection ViewBookingSystemReservationInfoDayGetByDate(int bookingId, DateTime date)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemReservationInfoDay>()
                .Where(ViewBookingSystemReservationInfoDay.Columns.BookingSystemStoreBookingId).IsEqualTo(bookingId)
                .And(ViewBookingSystemReservationInfoDay.Columns.ReservationDate).IsEqualTo(date.ToString("yyyy/MM/dd"))
                .ExecuteAsCollection<ViewBookingSystemReservationInfoDayCollection>();
        }

        #endregion

        #region view_booking_system_store_reservation_count
        /// <summary>
        /// 查詢店家日期區間的已訂位人數總計
        /// </summary>
        /// <param name="store"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        public ViewBookingSystemStoreReservationCountCollection ViewBookingSystemReservationCountGet(BookingSystemStore store, DateTime dateStart, DateTime dateEnd)
        {
            string sql = @"select * from dbo.view_booking_system_store_reservation_count
                        where booking_system_store_booking_id=@bookingId AND reservation_date between @dateStart AND @dateEnd";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemStoreReservationCount.Schema.Provider.Name);
            qc.AddParameter("@bookingId", store.BookingId, DbType.Int32);
            qc.AddParameter("@dateStart", dateStart.ToString("yyyy/MM/dd") + " 00:00:00", DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd.ToString("yyyy/MM/dd") + " 23:59:59", DbType.DateTime);
            ViewBookingSystemStoreReservationCountCollection viewBookingSystemStoreReservationCountCol = new ViewBookingSystemStoreReservationCountCollection();
            viewBookingSystemStoreReservationCountCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemStoreReservationCountCol;
        }

        #endregion

        #region view_booking_system_reservation_list

        /// <summary>
        /// 查尋店家指定日期的已訂位明細列表
        /// </summary>
        /// <param name="store"></param>
        /// <param name="queryDate"></param>
        /// <returns></returns>
        public ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(BookingSystemStore store, DateTime queryDate)
        {
            string sql = @" select * from dbo.view_booking_system_reservation_list
                         where booking_system_store_booking_id = @bookingId
                         and reservation_date between @dateStart and @dateEnd";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationList.Schema.Provider.Name);
            qc.AddParameter("@bookingId", store.BookingId, DbType.Int32);
            qc.AddParameter("@dateStart", queryDate.ToString("yyyy/MM/dd") + " 00:00:00", DbType.DateTime);
            qc.AddParameter("@dateEnd", queryDate.ToString("yyyy/MM/dd") + " 23:59:59", DbType.DateTime);
            ViewBookingSystemReservationListCollection viewBookingSystemReservationListCol = new ViewBookingSystemReservationListCollection();
            viewBookingSystemReservationListCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemReservationListCol;
        }

        public ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(Guid storeGuid, DateTime queryDate)
        {
            string sql = @" select * from dbo.view_booking_system_reservation_list
                         where booking_system_store_booking_id in (select booking_id from booking_system_store where store_guid=@storeGuid)
                         and reservation_date between @dateStart and @dateEnd";
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationList.Schema.Provider.Name);
            qc.AddParameter("@storeGuid", storeGuid.ToString(), DbType.String);
            qc.AddParameter("@dateStart", queryDate.ToString("yyyy/MM/dd") + " 00:00:00", DbType.DateTime);
            qc.AddParameter("@dateEnd", queryDate.ToString("yyyy/MM/dd") + " 23:59:59", DbType.DateTime);
            ViewBookingSystemReservationListCollection viewBookingSystemReservationListCol = new ViewBookingSystemReservationListCollection();
            viewBookingSystemReservationListCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemReservationListCol;
        }

        public ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(DateTime queryDate)
        {
            string sql = @" select * from dbo.view_booking_system_reservation_list
                         where reservation_date between @dateStart and @dateEnd";
            var qc = new QueryCommand(sql, ViewBookingSystemReservationList.Schema.Provider.Name);
            qc.AddParameter("@dateStart", queryDate.ToString("yyyy/MM/dd") + " 00:00:00", DbType.DateTime);
            qc.AddParameter("@dateEnd", queryDate.ToString("yyyy/MM/dd") + " 23:59:59", DbType.DateTime);
            var viewBookingSystemReservationListCol = new ViewBookingSystemReservationListCollection();
            viewBookingSystemReservationListCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemReservationListCol;
        }


        public ViewBookingSystemReservationList ViewBookingSystemReservationListGet(int reservationId)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemReservationList>()
                .Where(ViewBookingSystemReservationList.Columns.BookingSystemReservationId).IsEqualTo(reservationId)
                .ExecuteAsCollection<ViewBookingSystemReservationListCollection>().FirstOrDefault();
        }

        public ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGet(Guid storeGuid, string contactName, string contactMobile, string couponSequence)
        {
            #region subSql

            string subSQL = string.Empty;

            if (!string.IsNullOrEmpty(contactName))
            {
                subSQL += " contact_name like '%' + @contactName + '%' ";
            }

            if (!string.IsNullOrEmpty(contactMobile))
            {
                if (string.IsNullOrEmpty(subSQL))
                {
                    subSQL += " contact_number like '%' + @contactMobile + '%' ";
                }
                else
                {
                    subSQL += " AND contact_number like '%' + @contactMobile + '%' ";
                }
            }

            if (!string.IsNullOrEmpty(couponSequence))
            {
                if (string.IsNullOrEmpty(subSQL))
                {
                    subSQL += " booking_system_reservation_id in (select reservation_id from booking_system_coupon where  ISNULL(CAST(prefix as varchar),'')+coupon_sequence_number=@couponSequence) ";
                }
                else
                {
                    subSQL += " AND booking_system_reservation_id in (select reservation_id from booking_system_coupon where  ISNULL(CAST(prefix as varchar),'')+coupon_sequence_number=@couponSequence) ";
                }
            }

            if (!string.IsNullOrEmpty(subSQL))
            {
                subSQL = "AND (" + subSQL + " )";
            }

            #endregion subSql

            string sql = @" select * from view_booking_system_reservation_list where booking_system_store_booking_id in (select booking_id from booking_system_store where store_guid=@storeGuid)" + subSQL;

            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationList.Schema.Provider.Name);
            qc.AddParameter("@storeGuid", storeGuid.ToString(), DbType.String);
            if (!string.IsNullOrEmpty(contactName))
            {
                qc.AddParameter("@contactName", contactName, DbType.String);
            }
            if (!string.IsNullOrEmpty(contactMobile))
            {
                qc.AddParameter("@contactMobile", contactMobile, DbType.String);
            }
            if (!string.IsNullOrEmpty(couponSequence))
            {
                qc.AddParameter("@couponSequence", couponSequence, DbType.String);
            }
            var viewBookingSystemReservationListCol = new ViewBookingSystemReservationListCollection();
            viewBookingSystemReservationListCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewBookingSystemReservationListCol;
        }

        public ViewBookingSystemReservationListCollection ViewBookingSystemReservationListGetList(int bookingId)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemReservationList>()
                .Where(ViewBookingSystemReservationList.Columns.BookingSystemStoreBookingId).IsEqualTo(bookingId)
                .ExecuteAsCollection<ViewBookingSystemReservationListCollection>();
        }

        #endregion

        #region view_booking_system_reservation_record

        /// <summary>
        /// 依 member key 回傳訂位明細
        /// </summary>
        /// <param name="memberKey"></param>
        /// <param name="IsOutOfDate">是否回傳過期訂位資料</param>
        /// <returns></returns>
        public ViewBookingSystemReservationRecordCollection ViewBookingSystemReservationRecordGetByMemberKey(string memberKey, bool IsOutOfDate = false)
        {
            string sql = string.Format(@"select * from dbo.view_booking_system_reservation_record
                where member_key = @memberKey
                and Convert(varchar(10), reservation_date) {0} Convert(varchar(10), getdate())", IsOutOfDate ? "<" : ">=");
            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationRecord.Schema.Provider.Name);
            qc.AddParameter("@memberKey", memberKey, DbType.String);
            ViewBookingSystemReservationRecordCollection reservationRecordCol = new ViewBookingSystemReservationRecordCollection();
            reservationRecordCol.LoadAndCloseReader(DataService.GetReader(qc));
            return reservationRecordCol;
        }

        #endregion

        #region view_booking_system_Max_number

        public ViewBookingSystemMaxNumberCollection ViewBookingSystemMaxNumberListGet(int storeBookingId)
        {
            string sql = @"select * from dbo.view_booking_system_Max_number 
                            Where booking_system_store_booking_id=@bookingId";


            QueryCommand qc = new QueryCommand(sql, BookingSystemDate.Schema.Provider.Name);
            qc.AddParameter("@bookingId", storeBookingId, DbType.Int32);

            ViewBookingSystemMaxNumberCollection cols = new ViewBookingSystemMaxNumberCollection();
            cols.LoadAndCloseReader(DataService.GetReader(qc));
            return cols;

        }

        #endregion view_booking_system_Max_number

        #region view_booking_system_coupon_reservation

        public ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(Guid orderDetailGuid, string sequenceNumber)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemCouponReservation>().NoLock()
                .Where(ViewBookingSystemCouponReservation.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid)
                .And(ViewBookingSystemCouponReservation.Columns.CouponSequenceNumber).IsEqualTo(sequenceNumber)
                .ExecuteAsCollection<ViewBookingSystemCouponReservationCollection>()
                .OrderByAsc(ViewBookingSystemCouponReservation.Columns.IsCancel)
                .OrderByDesc(ViewBookingSystemCouponReservation.Columns.CreateDatetime);
        }

        public ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(string orderKey, string sequenceNumber)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemCouponReservation>().NoLock()
                .Where(ViewBookingSystemCouponReservation.Columns.OrderKey).IsEqualTo(orderKey)
                .And(ViewBookingSystemCouponReservation.Columns.CouponSequenceNumber).IsEqualTo(sequenceNumber)
                .ExecuteAsCollection<ViewBookingSystemCouponReservationCollection>()
                .OrderByAsc(ViewBookingSystemCouponReservation.Columns.IsCancel)
                .OrderByDesc(ViewBookingSystemCouponReservation.Columns.CreateDatetime);
        }

        public ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetBySequenceNumber(string sequenceNumber)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemCouponReservation>().NoLock()
                .Where(ViewBookingSystemCouponReservation.Columns.CouponSequenceNumber).IsEqualTo(sequenceNumber)
                .ExecuteAsCollection<ViewBookingSystemCouponReservationCollection>()
                .OrderByAsc(ViewBookingSystemCouponReservation.Columns.IsCancel)
                .OrderByDesc(ViewBookingSystemCouponReservation.Columns.CreateDatetime);
        }


        public ViewBookingSystemCouponReservationCollection ViewBookingSystemCouponReservationGetByOrderKey(string orderKey)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemCouponReservation>().NoLock()
                .Where(ViewBookingSystemCouponReservation.Columns.OrderKey).IsEqualTo(orderKey)
                .And(ViewBookingSystemCouponReservation.Columns.IsCancel).IsEqualTo(0)
                .And(ViewBookingSystemCouponReservation.Columns.IsLock).IsEqualTo(1)
                .ExecuteAsCollection<ViewBookingSystemCouponReservationCollection>();
        }
        public DataTable ViewBookingSystemCouponReservationGetByOrderKey(IEnumerable<string> orderKeys)
        {
            DataTable infos = new DataTable();
            DataTable result = new DataTable();
            var tempIds = orderKeys.Distinct().ToList();
            int idx = 0;
            var orderGuids = tempIds.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;

                string sql = string.Format(@"select distinct {0}, {1} 
from {2}
where {3} in ({4})
and {1} = 0",
            ViewBookingSystemCouponReservation.Columns.CouponSequenceNumber,
            ViewBookingSystemCouponReservation.Columns.IsCancel,
            ViewBookingSystemCouponReservation.Schema.Name,
            ViewBookingSystemCouponReservation.Columns.OrderKey,
            string.Join(",", orderGuids.Skip(idx).Take(batchLimit)));

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                using (IDataReader reader = DataService.GetReader(qc))
                {
                    infos.Load(reader);
                }

                result.Merge(infos);

                idx += batchLimit;
            }
            return result;
        }

        #endregion

        #region view_booking_system_store

        public ViewBookingSystemStoreCollection ViewBookingSystemStoreGetBySellerGuid(Guid sellerGuid, BookingType bookingType)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemStore>()
                .Where(ViewBookingSystemStore.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewBookingSystemStore.Columns.BookingType).IsEqualTo((int)bookingType)
                .OrderAsc(ViewBookingSystemStore.Columns.TownshipId)
                .ExecuteAsCollection<ViewBookingSystemStoreCollection>();
        }

        public ViewBookingSystemStore ViewBookingSystemStoreGetByStoreGuid(Guid storeGuid, BookingType bookingType)
        {
            return DB.SelectAllColumnsFrom<ViewBookingSystemStore>()
                .Where(ViewBookingSystemStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(ViewBookingSystemStore.Columns.BookingType).IsEqualTo((int)bookingType)
                .ExecuteAsCollection<ViewBookingSystemStoreCollection>().FirstOrDefault();
        }

        #endregion

        #endregion View

        #region Table

        #region booking_system_time_slot

        /// <summary>
        /// 取得店家查尋日期的所有可訂位資訊
        /// </summary>
        /// <param name="bookingSystemDateId"></param>
        /// <returns></returns>
        public BookingSystemTimeSlotCollection BookingSystemTimeSlotGet(int bookingSystemDateId)
        {
            return DB.SelectAllColumnsFrom<BookingSystemTimeSlot>()
                     .Where(BookingSystemTimeSlot.Columns.BookingSystemDateId).IsEqualTo(bookingSystemDateId)
                     .ExecuteAsCollection<BookingSystemTimeSlotCollection>().OrderByAsc(BookingSystemTimeSlot.Columns.TimeSlot);
        }

        public BookingSystemTimeSlot BookingSystemTimeSlotGetById(int timeSlotId)
        {
            return DB.SelectAllColumnsFrom<BookingSystemTimeSlot>()
                .Where(BookingSystemTimeSlot.Columns.Id).IsEqualTo(timeSlotId)
                .ExecuteAsCollection<BookingSystemTimeSlotCollection>().FirstOrDefault();
        }

        public bool BookingSystemTimeSlotSet(BookingSystemTimeSlot bookingSystemTimeSlot)
        {
            DB.Save(bookingSystemTimeSlot);
            return true;
        }

        #endregion booking_system_time_slot

        #region booking_system_store

        public int BookingSystemStoreSet(BookingSystemStore bookingSystemStore)
        {
            DB.Save(bookingSystemStore);
            if (bookingSystemStore.IsLoaded)
            {
                return bookingSystemStore.BookingId;
            }
            else
            {
                return 0;
            }
        }

        public BookingSystemStore BookingSystemStoreGetByStoreGuid(Guid guid, BookingType bookingType)
        {
            string sql = @"select * from dbo.booking_system_store 
                where store_guid=@storeGuid AND booking_type=@bookingType";
            QueryCommand qc = new QueryCommand(sql, BookingSystemStore.Schema.Provider.Name);
            qc.AddParameter("@storeGuid", guid, DbType.Guid);
            qc.AddParameter("@bookingType", (int)bookingType, DbType.Int32);
            BookingSystemStore bookingSystemStore = new BookingSystemStore();
            bookingSystemStore.LoadAndCloseReader(DataService.GetReader(qc));
            return bookingSystemStore;
        }

        public BookingSystemStoreCollection BookingSystemStoreGetByStoreGuid(Guid storeGuid)
        {
            string sql = @"select * from dbo.booking_system_store 
                where store_guid=@storeGuid ";
            QueryCommand qc = new QueryCommand(sql, BookingSystemStore.Schema.Provider.Name);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);

            BookingSystemStoreCollection bookingSystemStores = new BookingSystemStoreCollection();
            bookingSystemStores.LoadAndCloseReader(DataService.GetReader(qc));
            return bookingSystemStores;
        }

        public BookingSystemStore BookingSystemStoreGetBookingId(int bookingId)
        {
            return DB.Get<BookingSystemStore>(bookingId);
        }

        #endregion booking_system_store

        #region booking_system_date

        public int BookingSystemDateSet(BookingSystemDate bookingSystemDate)
        {
            DB.Save(bookingSystemDate);
            if (bookingSystemDate.IsLoaded)
            {
                return bookingSystemDate.Id;
            }
            else
            {
                return 0;
            }
        }

        public BookingSystemDate BookingSystemDateGet(int storeBookingId, DayType dayType, DateTime? effectiveDate)
        {
            string sql = @"select * from dbo.booking_system_date 
                            Where booking_system_store_booking_id=@bookingId
                            AND day_type=@dayType";

            if (dayType == DayType.AssignDay)
            {
                sql += " AND effective_date=@effectiveDate";
            }

            QueryCommand qc = new QueryCommand(sql, BookingSystemDate.Schema.Provider.Name);
            qc.AddParameter("@bookingId", storeBookingId, DbType.Int32);
            qc.AddParameter("@dayType", (int)dayType, DbType.Int32);

            if (dayType == DayType.AssignDay)
            {
                qc.AddParameter("@effectiveDate", effectiveDate.Value.Date, DbType.DateTime);
            }

            BookingSystemDate bookingSystemDate = new BookingSystemDate();
            bookingSystemDate.LoadAndCloseReader(DataService.GetReader(qc));
            return bookingSystemDate;
        }

        public BookingSystemDate BookingSystemDateGetByStoreBookingId(int storeBookingId)
        {
            return DB.Get<BookingSystemDate>(BookingSystemDate.Columns.BookingSystemStoreBookingId, storeBookingId);
        }

        public BookingSystemDateCollection BookingSystemDateGetList(int storeBookingId)
        {
            return DB.SelectAllColumnsFrom<BookingSystemDate>()
                .Where(BookingSystemDate.Columns.BookingSystemStoreBookingId).IsEqualTo(storeBookingId)
                .ExecuteAsCollection<BookingSystemDateCollection>() ?? new BookingSystemDateCollection();
        }


        #endregion booking_system_date

        #region booking_system_reservation

        public BookingSystemReservation BookingSystemReservationSet(BookingSystemReservation bookingSystemReservation)
        {
            string sql = @"IF NOT EXISTS(SELECT 1 FROM dbo.view_booking_system_reservation_info_time WHERE id=@TimeSlotId AND reservation_date=@ReservationDate)
                        BEGIN
                            INSERT INTO dbo.booking_system_reservation 
                            SELECT @TimeSlotId, @ReservationDate, @NumberOfPeople, @Remark, @ContactName, @ContactGender, @ContactNumber,
                                    GETDATE(), 0, 0, @OrderKey, @MemberKey, @ApiId, @CouponInfo, @CouponLink, NULL, @IsLock,@ContactEmail,null
                            SELECT CONVERT(int, ISNULL(@@IDENTITY, 0)) AS Id;
                        END
                        ELSE BEGIN
                            INSERT INTO dbo.booking_system_reservation 
                            SELECT @TimeSlotId, @ReservationDate, @NumberOfPeople, @Remark, @ContactName, @ContactGender, @ContactNumber,
                                    GETDATE(), 0, 0, @OrderKey, @MemberKey, @ApiId, @CouponInfo, @CouponLink, NULL, @IsLock,@ContactEmail,null
                                    FROM dbo.view_booking_system_reservation_info_time WHERE id = @TimeSlotId AND reservation_date = @ReservationDate
                                    AND number_of_people + @NumberOfPeople <= max_number_of_people;select Convert(int, ISNULL(@@IDENTITY, 0)) as Id
                        END";

            QueryCommand qc = new QueryCommand(sql, BookingSystemReservation.Schema.Provider.Name);
            qc.AddParameter("@TimeSlotId", bookingSystemReservation.TimeSlotId, DbType.Int32);
            qc.AddParameter("@ReservationDate", bookingSystemReservation.ReservationDate, DbType.DateTime);
            qc.AddParameter("@NumberOfPeople", bookingSystemReservation.NumberOfPeople, DbType.Int32);
            qc.AddParameter("@Remark", bookingSystemReservation.Remark, DbType.String);
            qc.AddParameter("@ContactName", bookingSystemReservation.ContactName, DbType.String);
            qc.AddParameter("@ContactGender", bookingSystemReservation.ContactGender, DbType.Boolean);
            qc.AddParameter("@ContactNumber", bookingSystemReservation.ContactNumber, DbType.String);
            qc.AddParameter("@OrderKey", bookingSystemReservation.OrderKey, DbType.String);
            qc.AddParameter("@MemberKey", bookingSystemReservation.MemberKey, DbType.String);
            qc.AddParameter("@ApiId", bookingSystemReservation.ApiId, DbType.Int32);
            qc.AddParameter("@CouponInfo", bookingSystemReservation.CouponInfo, DbType.String);
            qc.AddParameter("@CouponLink", bookingSystemReservation.CouponLink, DbType.String);
            qc.AddParameter("@IsLock", bookingSystemReservation.IsLock, DbType.Boolean);
            qc.AddParameter("@ContactEmail", bookingSystemReservation.ContactEmail, DbType.String);
            bookingSystemReservation.Id = (int)DataService.ExecuteScalar(qc);
            return bookingSystemReservation;
        }

        public bool VbsBookingSystemReservationSet(BookingSystemReservation bookingSystemReservation)
        {
            DB.Save(bookingSystemReservation);
            return true;
        }


        public BookingSystemReservation BookingSystemReservationGet(int reservationId)
        {
            return DB.Get<BookingSystemReservation>(reservationId);
        }

        public BookingSystemReservationCollection BookingSystemReservationGetListByBookingSystemDateAndDay(int bookingSystemDateId, DateTime queryDate)
        {
            string sql = @"select * from dbo.booking_system_reservation 
                where time_slot_id in (select id from booking_system_time_slot where booking_system_date_id = @bookingSystemDateId)  AND ( reservation_date between @startDate AND @endDate )";
            QueryCommand qc = new QueryCommand(sql, BookingSystemReservation.Schema.Provider.Name);
            qc.AddParameter("@bookingSystemDateId", bookingSystemDateId, DbType.Int32);
            qc.AddParameter("@startDate", queryDate.ToString("yyyy/MM/dd") + " 00:00:00", DbType.DateTime);
            qc.AddParameter("@endDate", queryDate.ToString("yyyy/MM/dd") + " 23:59:59", DbType.DateTime);
            BookingSystemReservationCollection bookingSystemReservationCol = new BookingSystemReservationCollection();
            bookingSystemReservationCol.LoadAndCloseReader(DataService.GetReader(qc));
            return bookingSystemReservationCol;
        }


        public int ReservationPeopleOfNumberGetByDate(int timeSlotId, DateTime date)
        {
            //抓取某個時段的預約人數 光看time_slot_id不準確 需抓取所有booking_system_date_id的time_slot
            string sql = @"select Case When SUM(br.number_of_people) is null then 0 Else SUM(br.number_of_people) end as PeopleOfNumber 
                           from dbo.view_booking_system_reservation_list as br with(nolock)
                           join dbo.booking_system_time_slot as bt with(nolock) 
                            on bt.booking_system_date_id = br.booking_system_date_id 
                            and bt.time_slot = br.time_slot
						   where bt.id = @timeSlotId
                           and Convert(varchar, br.reservation_date, 111) = @queryDate
                           and br.is_cancel = 0 ";

            QueryCommand qc = new QueryCommand(sql, ViewBookingSystemReservationList.Schema.Provider.Name);
            qc.AddParameter("@timeSlotId", timeSlotId, DbType.Int32);
            qc.AddParameter("@queryDate", date.ToString("yyyy/MM/dd"), DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewBookingSystemReservationRecordCollection ReservationRecordGetByServiceNameAndMemberKey(string memberKey, BookingSystemServiceName serviceName, BookingSystemRecordType type)
        {
            DateTime now = System.DateTime.Now;

            if (type == BookingSystemRecordType.Unexpired)
            {
                return DB.SelectAllColumnsFrom<ViewBookingSystemReservationRecord>()
                         .Where(ViewBookingSystemReservationRecord.Columns.MemberKey).IsEqualTo(memberKey)
                         .And(ViewBookingSystemReservationRecord.Columns.ApiId).IsEqualTo((int)serviceName)
                         .And(ViewBookingSystemReservationRecord.Columns.ReservationDateTime).IsGreaterThan(now.AddDays(-1).ToString("yyyy/MM/dd 23:59:59"))
                         .OrderAsc(ViewBookingSystemReservationRecord.Columns.ReservationDateTime)
                         .ExecuteAsCollection<ViewBookingSystemReservationRecordCollection>();
            }
            else
            {
                return DB.SelectAllColumnsFrom<ViewBookingSystemReservationRecord>()
                         .Where(ViewBookingSystemReservationRecord.Columns.MemberKey).IsEqualTo(memberKey)
                         .And(ViewBookingSystemReservationRecord.Columns.ApiId).IsEqualTo((int)serviceName)
                         .And(ViewBookingSystemReservationRecord.Columns.ReservationDateTime).IsLessThan(now.ToString("yyyy/MM/dd 00:00:00"))
                         .OrderDesc(ViewBookingSystemReservationRecord.Columns.ReservationDateTime)
                         .ExecuteAsCollection<ViewBookingSystemReservationRecordCollection>();
            };
        }

        public void SetReservationRecordCancel(int reservationId, string cancelId = null)
        {
            DB.Update<BookingSystemReservation>().Set(BookingSystemReservation.Columns.IsCancel).EqualTo(1)
                .Set(BookingSystemReservation.Columns.CancelDate).EqualTo(System.DateTime.Now)
                .Set(BookingSystemReservation.Columns.CancelId).EqualTo(cancelId)
                .Where(BookingSystemReservation.Columns.Id).IsEqualTo(reservationId).Execute();
        }

        #endregion booking_system_reservation

        #region booking_system_coupon

        public bool BookingSystemCouponSetWithReservationId(BookingSystemCoupon bookingSystemCoupon, int reservationId)
        {
            bookingSystemCoupon.ReservationId = reservationId;
            DB.Save(bookingSystemCoupon);
            return true;
        }

        public BookingSystemCouponCollection BookingSystemCouponGetListByReservationId(int reservationId)
        {
            return DB.SelectAllColumnsFrom<BookingSystemCoupon>()
                 .Where(BookingSystemCoupon.Columns.ReservationId).IsEqualTo(reservationId)
                 .ExecuteAsCollection<BookingSystemCouponCollection>();
        }

        public bool BookingSystemCouponIsExistBySequenceOrderDetailGuid(string sequence, Guid orderDetailGuid)
        {
            string sql = @"select bsc.* from dbo.booking_system_coupon bsc inner join dbo.booking_system_reservation res
                        On bsc.reservation_id = res.id 
                        Where bsc.coupon_sequence_number = @sequence 
                        AND bsc.order_detail_guid = @orderDetailGuid 
                        AND res.is_cancel = 0";
            QueryCommand qc = new QueryCommand(sql, BookingSystemCoupon.Schema.Provider.Name);
            qc.AddParameter("@sequence", sequence, DbType.String);
            qc.AddParameter("@orderDetailGuid", orderDetailGuid, DbType.Guid);
            BookingSystemCouponCollection couponCol = new BookingSystemCouponCollection();
            couponCol.LoadAndCloseReader(DataService.GetReader(qc));
            bool result = couponCol.Any();
            couponCol.Clear();
            return result;
        }

        public BookingSystemCouponCollection BookingSystemCouponGetByOrderKey(string orderKey)
        {
            string sql = @"select bsc.* from dbo.booking_system_coupon bsc inner join dbo.booking_system_reservation res
                        On bsc.reservation_id = res.id 
                        Where res.order_key=@orderKey AND res.is_cancel = 0";
            QueryCommand qc = new QueryCommand(sql, BookingSystemCoupon.Schema.Provider.Name);
            qc.AddParameter("@orderKey", orderKey, DbType.String);
            BookingSystemCouponCollection couponCol = new BookingSystemCouponCollection();
            couponCol.LoadAndCloseReader(DataService.GetReader(qc));
            return couponCol;
        }

        #endregion booking_system_coupon

        #region booking_system_api

        public BookingSystemApi BookingSystemApiSet(BookingSystemApi api)
        {
            DB.Save(api);
            return api;
        }

        public bool BookingSystemApiCheckExistByServiceName(string serviceName, BookingSystemApiServiceType serviceType)
        {
            return (DB.SelectAllColumnsFrom<BookingSystemApi>()
                .Where(BookingSystemApi.Columns.ServiceName).IsEqualTo(serviceName)
                .And(BookingSystemApi.Columns.ServiceType).IsEqualTo((int)serviceType)
                .ExecuteAsCollection<BookingSystemApiCollection>().Any()) ? true : false;
        }

        public BookingSystemServiceName BookingSystemApiGetBookingSystemServiceName(string serviceName, BookingSystemApiServiceType serviceType)
        {
            BookingSystemApiCollection apiCol = DB.SelectAllColumnsFrom<BookingSystemApi>()
                .Where(BookingSystemApi.Columns.ServiceName).IsEqualTo(serviceName)
                .And(BookingSystemApi.Columns.ServiceType).IsEqualTo((int)serviceType)
                .ExecuteAsCollection<BookingSystemApiCollection>();

            if (apiCol.Any())
            {
                switch (apiCol.FirstOrDefault().ApiId)
                {
                    case (int)BookingSystemServiceName._17Life:
                    case (int)BookingSystemServiceName.HiDeal:
                        return (BookingSystemServiceName)apiCol.FirstOrDefault().ApiId;
                    default:
                        return BookingSystemServiceName.None;
                }
            }
            else
            {
                return BookingSystemServiceName.None;
            }
        }

        public string BookingSystemApiGetApiKeyById(int apiId)
        {
            var col = DB.SelectAllColumnsFrom<BookingSystemApi>()
              .Where(BookingSystemApi.Columns.ApiId).IsEqualTo(apiId)
              .ExecuteAsCollection<BookingSystemApiCollection>();
            return (col.Any()) ? col.First().ApiKey : string.Empty;
        }

        #endregion

        #region booking_system_reserve_lock_status_log

        public BookingSystemReserveLockStatusLogCollection BookingSystemReserveLockStatusLogListGet(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<BookingSystemReserveLockStatusLog, BookingSystemReserveLockStatusLogCollection>(0, -1, null, filter);
        }

        public bool BookingSystemReserveLockStatusLogSet(BookingSystemReserveLockStatusLog bookingSystemReserveLockStatusLog)
        {
            DB.Save(bookingSystemReserveLockStatusLog);
            return true;
        }

        public BookingSystemReserveLockStatusLogCollection BookingSystemReserveLockStatusLogGetByCouponId(int coupon_id, int lock_status)
        {
            return DB.SelectAllColumnsFrom<BookingSystemReserveLockStatusLog>()
                  .Where(BookingSystemReserveLockStatusLog.Columns.CouponId).IsEqualTo(coupon_id)
                  .And(BookingSystemReserveLockStatusLog.Columns.ReserveLockStatus).IsEqualTo(lock_status)
                  .ExecuteAsCollection<BookingSystemReserveLockStatusLogCollection>();
        }

        #endregion booking_system_reserve_lock_status_log

        #endregion Table

        public List<ReserveCouponData> GetReserveCouponWithPpon(string firstCouponSequence, string secoundCouponSequence)
        {
            string sql = @"SELECT distinct vpc.coupon_id, vpc.sequence_number, vpc.event_name, vpc.member_name, c.is_reservation_lock, vpc.order_status
                           , ctl.status, DP.is_reserve_lock, vpc.business_hour_guid, isnull(vpc.store_guid, '00000000-0000-0000-0000-000000000000') store_guid, dp.unique_id as deal_unique_id
                           , case when  osl.status is null then -1 else osl.status end,vpc.order_guid, vpc.coupon_usage
                           , business_hour_deliver_time_s, coalesce(bh_changed_expire_date, business_hour_deliver_time_e) as business_hour_deliver_time_e 
                           FROM view_ppon_coupon AS vpc LEFT JOIN coupon AS c ON vpc.coupon_id = c.id 
                           LEFT JOIN deal_property AS DP ON  vpc.business_hour_guid=DP.business_hour_guid                           
                           LEFT JOIN cash_trust_log AS ctl ON vpc.coupon_id=ctl.coupon_id 
                           LEFT JOIN 
                           (  SELECT T.order_guid,T.status from (
                                  select *,ROW_NUMBER() over (PARTITION BY order_Guid ORDER BY id DESC) as rn
                                  FROM   dbo.order_status_log ) as T where T.rn=1
                           ) as osl  on osl.order_guid = vpc.order_guid 
                           WHERE vpc.sequence_number = @firstCouponSequence + '-' + @secoundCouponSequence     
                            and ctl.status in (" + (int)TrustStatus.Initial + "," + (int)TrustStatus.Trusted + "," + (int)TrustStatus.Returned + "," + (int)TrustStatus.Refunded + ") " +
                            @" and group_order_status&" + (int)GroupOrderStatus.SKMDeal + "=0";


            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@firstCouponSequence", firstCouponSequence, DbType.String);
            qc.AddParameter("@secoundCouponSequence", secoundCouponSequence, DbType.String);
            var result = new List<ReserveCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new ReserveCouponData
                    {
                        CouponId = dr.GetInt32(0),
                        CouponSequence = dr.GetString(1),
                        EventName = dr.GetString(2),
                        MemberName = dr.GetString(3),
                        IsReservatoinLock = dr.GetBoolean(4),
                        DealType = BusinessModel.Ppon,
                        OrderStatus = dr.GetInt32(5),
                        CouponStatus = dr.GetInt32(6),
                        IsDealReserveLock = dr.GetBoolean(7),
                        DealGuid = dr.GetGuid(8),
                        StoreGuid = dr.GetGuid(9),
                        DealUniqueId = dr.GetInt32(10),
                        OrderStatusLog = dr.GetInt32(11),
                        OrderGuid = dr.GetGuid(12),
                        CouponUsage = dr.GetString(13),
                        ExchangePeriodStartTime = dr.GetDateTime(14),
                        ExchangePeriodEndTime = dr.GetDateTime(15)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<ReserveCouponData> GetReserveCouponWithPiinLife(string firstCouponSequence, string secoundCouponSequence)
        {
            string sql = @"SELECT distinct hdc.id as coupon_id,vhdc.prefix,vhdc.sequence,vhdc.promo_short_desc,vhdc.[user_id],hdc.is_reservation_lock,vhdc.order_status,ctl.status,hdp.is_reserve_lock,vhdc.product_guid,vhdc.store_guid,hdp.id as hi_deal_product_id
                            FROM  hi_deal_coupon AS hdc 
                            LEFT JOIN hi_deal_product AS hdp ON  hdc.product_id =hdp.id
                            LEFT JOIN view_hi_deal_coupon AS vhdc ON vhdc.coupon_id=hdc.id
                            LEFT JOIN cash_trust_log AS ctl ON hdc.id=ctl.coupon_id
                            LEFT JOIN hi_deal_order_show as hdos ON vhdc.hi_deal_order_guid =hdos.order_guid 
                            Where (hdc.prefix +hdc.sequence) = @firstCouponSequence + '-' + @secoundCouponSequence                            
                            and ctl.status in ( " + (int)TrustStatus.Initial + "," + (int)TrustStatus.Trusted + "," + (int)TrustStatus.Returned + "," + (int)TrustStatus.Refunded + @" ) and vhdc.order_status in(3,4) and vhdc.delivery_type=1 and product_type=1 
                            and hdos.order_show_status = 0  
                            and vhdc.store_guid is not null ";


            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@firstCouponSequence", firstCouponSequence, DbType.String);
            qc.AddParameter("@secoundCouponSequence", secoundCouponSequence, DbType.String);
            var result = new List<ReserveCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new ReserveCouponData
                    {
                        CouponId = unchecked((int)dr.GetInt64(0)),
                        CouponSequence = dr.GetString(1) + dr.GetString(2),
                        EventName = dr.GetString(3),
                        MemberName = dr.GetInt32(4).ToString(),
                        IsReservatoinLock = dr.GetBoolean(5),
                        DealType = BusinessModel.PiinLife,
                        OrderStatus = dr.GetInt32(6),
                        CouponStatus = dr.GetInt32(7),
                        IsDealReserveLock = dr.GetBoolean(8),
                        DealGuid = dr.GetGuid(9),
                        StoreGuid = dr.GetGuid(10),
                        DealUniqueId = dr.GetInt32(11),
                        OrderStatusLog = null,
                        OrderGuid = null,
                        CouponUsage = dr.GetString(3)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }


        public List<ReserveCouponData> GetReserveLockCouponWithPpon(int couponLockSearchType, DateTime searchStartDate, DateTime searchEndDate)
        {
            string sql = @"SELECT distinct vpc.coupon_id,vpc.sequence_number,vpc.event_name,vpc.member_name,c.is_reservation_lock,vpc.order_status,ctl.status,DP.is_reserve_lock,vpc.business_hour_guid,vpc.store_guid,dp.unique_id as deal_unique_id
                            , case when vblc.reserve_lock_status is null then 0 else vblc.reserve_lock_status end as reserve_lock_status
                            , vpc.order_guid 
                            , vblc.modify_time 
                            , coupon_reservation_date 
                            , vpc.coupon_usage
                            FROM view_ppon_coupon AS vpc LEFT JOIN coupon AS c ON vpc.coupon_id = c.id 
                           LEFT JOIN deal_property AS DP ON  vpc.business_hour_guid=DP.business_hour_guid                           
                           LEFT JOIN cash_trust_log AS ctl ON vpc.coupon_id=ctl.coupon_id 
                          LEFT JOIN 
                           (  SELECT T.order_guid,T.status from (
                                  select *,ROW_NUMBER() over (PARTITION BY order_Guid ORDER BY id DESC) as rn
                                  FROM   dbo.order_status_log ) as T where T.rn=1
                           ) as osl  on osl.order_guid = vpc.order_guid 
                           LEFT JOIN view_booking_system_reserve_lock_coupon as vblc on vpc.coupon_id = vblc.coupon_id                       
                           Where vblc.reserve_lock_status in (1,2) 
                           and ctl.status in (" + (int)TrustStatus.Initial + "," + (int)TrustStatus.Trusted + "," + (int)TrustStatus.Returned + "," + (int)TrustStatus.Refunded + ") ";

            if (couponLockSearchType == (int)CouponLockSearchType.ModifityDate)
            {
                sql += " and ( @searchStartTime <= vblc.modify_time and vblc.modify_time < @searchEndTime )";
            }
            else if (couponLockSearchType == (int)CouponLockSearchType.ReservationDate)
            {
                sql += " and ( vblc.coupon_reservation_date is null or (@searchStartTime <= vblc.coupon_reservation_date and vblc.coupon_reservation_date < @searchEndTime ) )";
            }

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@searchStartTime", searchStartDate, DbType.DateTime);
            qc.AddParameter("@searchEndTime", searchEndDate.AddDays(1), DbType.DateTime);
            var result = new List<ReserveCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new ReserveCouponData
                    {
                        CouponId = dr.GetInt32(0),
                        CouponSequence = dr.GetString(1),
                        EventName = dr.GetString(2),
                        MemberName = dr.GetString(3),
                        IsReservatoinLock = dr.GetBoolean(4),
                        DealType = BusinessModel.Ppon,
                        OrderStatus = dr.GetInt32(5),
                        CouponStatus = dr.GetInt32(6),
                        IsDealReserveLock = dr.GetBoolean(7),
                        DealGuid = dr.GetGuid(8),
                        StoreGuid = dr.IsDBNull(9) ? (Guid?)null : dr.GetGuid(9),
                        DealUniqueId = dr.GetInt32(10),
                        OrderStatusLog = dr.GetInt32(11),
                        OrderGuid = dr.GetGuid(12),
                        CouponUsage = dr.GetString(15)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<ReserveCouponData> GetReserveLockCouponWithPiinLife(int couponLockSearchType, DateTime searchStartDate, DateTime searchEndDate)
        {
            string sql = @"SELECT distinct hdc.id as coupon_id,vhdc.prefix,vhdc.sequence,vhdc.promo_short_desc,vhdc.[user_id],hdc.is_reservation_lock,vhdc.order_status,ctl.status,hdp.is_reserve_lock,vhdc.product_guid,vhdc.store_guid,hdp.id as hi_deal_product_id
                            , case when vblc.reserve_lock_status is null then 0 else vblc.reserve_lock_status end as reserve_lock_status
                            ,vblc.modify_time,coupon_reservation_date
                            FROM  hi_deal_coupon AS hdc 
                            LEFT JOIN hi_deal_product AS hdp ON  hdc.product_id =hdp.id
                            LEFT JOIN view_hi_deal_coupon AS vhdc ON vhdc.coupon_id=hdc.id
                            LEFT JOIN cash_trust_log AS ctl ON hdc.id=ctl.coupon_id
                            LEFT JOIN hi_deal_order_show as hdos ON vhdc.hi_deal_order_guid =hdos.order_guid 
                            LEFT JOIN view_booking_system_reserve_lock_coupon as vblc on  hdc.id = vblc.coupon_id  
                            Where ctl.status in (0,1)and vhdc.order_status in(3,4) and vhdc.delivery_type=1 and product_type=1 
                            and hdos.order_show_status=0 
                            and vhdc.store_guid is not null 
                            and vblc.reserve_lock_status in (1,2) ";
            if (couponLockSearchType == (int)CouponLockSearchType.ModifityDate)
            {
                sql += " and ( @searchStartTime <= vblc.modify_time and vblc.modify_time < @searchEndTime )";
            }
            else if (couponLockSearchType == (int)CouponLockSearchType.ReservationDate)
            {
                sql += " and ( @searchStartTime <= vblc.coupon_reservation_date and vblc.coupon_reservation_date < @searchEndTime )";
            }


            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@searchStartTime", searchStartDate, DbType.DateTime);
            qc.AddParameter("@searchEndTime", searchEndDate.AddDays(1), DbType.DateTime);
            var result = new List<ReserveCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new ReserveCouponData
                    {
                        CouponId = unchecked((int)dr.GetInt64(0)),
                        CouponSequence = dr.GetString(1) + dr.GetString(2),
                        EventName = dr.GetString(3),
                        MemberName = dr.GetInt32(4).ToString(),
                        IsReservatoinLock = dr.GetBoolean(5),
                        DealType = BusinessModel.PiinLife,
                        OrderStatus = dr.GetInt32(6),
                        CouponStatus = dr.GetInt32(7),
                        IsDealReserveLock = dr.GetBoolean(8),
                        DealGuid = dr.GetGuid(9),
                        StoreGuid = dr.GetGuid(10),
                        DealUniqueId = dr.GetInt32(11),
                        OrderStatusLog = null,
                        OrderGuid = null,
                        CouponUsage = dr.GetString(3)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }
    }
}

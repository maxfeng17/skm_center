﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;

namespace LunchKingSite.SsBLL.Provider
{
    public class VbsProvider : IVbsProvider
    {
        #region VbsMembershipPermission

        public bool VbsMembershipPermissionSet(VbsMembershipPermissionCollection vmpc)
        {
            return DB.SaveAll(vmpc) > 0;
        }

        public VbsMembershipPermissionCollection VbsMembershipPermissionGetList(int vbsMembershipId)
        {
            return DB.SelectAllColumnsFrom<VbsMembershipPermission>().NoLock()
                 .Where(VbsMembershipPermission.Columns.VbsMembershipId).IsEqualTo(vbsMembershipId)
                 .ExecuteAsCollection<VbsMembershipPermissionCollection>();
        }

        public VbsMembershipPermissionCollection VbsMembershipPermissionGetList(string vbsMembershipAccountId)
        {
            var vbs = DB.SelectAllColumnsFrom<VbsMembership>().NoLock()
                    .Where(VbsMembership.AccountIdColumn).IsEqualTo(vbsMembershipAccountId)
                    .ExecuteSingle<VbsMembership>();

            if (!vbs.IsLoaded)
            {
                return new VbsMembershipPermissionCollection();
            }

            return DB.SelectAllColumnsFrom<VbsMembershipPermission>().NoLock()
                 .Where(VbsMembershipPermission.Columns.VbsMembershipId).IsEqualTo(vbs.Id)
                 .ExecuteAsCollection<VbsMembershipPermissionCollection>();
        }

        public VbsMembershipPermission VbsMembershipPermissionGet(int vbsMembershipId, VbsMembershipPermissionScope scope)
        {
            return DB.SelectAllColumnsFrom<VbsMembershipPermission>().NoLock()
                 .Where(VbsMembershipPermission.Columns.VbsMembershipId).IsEqualTo(vbsMembershipId)
                 .And(VbsMembershipPermission.ScopeColumn).IsEqualTo((int)scope)
                 .ExecuteAsCollection<VbsMembershipPermissionCollection>().FirstOrDefault() ?? new VbsMembershipPermission();
        }

        public void VbsMembershipPermissionDelAll(int vbsMembershipId)
        {
            DB.Delete<VbsMembershipPermission>(VbsMembershipPermission.Columns.VbsMembershipId, vbsMembershipId);
        }

        public void VbsMembershipPermissionDelAll(string vbsMembershipAccountId)
        {
            var vbs = DB.SelectAllColumnsFrom<VbsMembership>().NoLock()
                .Where(VbsMembership.AccountIdColumn).IsEqualTo(vbsMembershipAccountId)
                .ExecuteSingle<VbsMembership>();

            if (vbs.IsLoaded)
            {
                DB.Delete<VbsMembershipPermission>(VbsMembershipPermission.Columns.VbsMembershipId, vbs.Id);
            }
        }

        #endregion VbsMembershipPermission

        #region vbs_document_category
        public VbsDocumentCategory VbsDocumentCategoryGet(int id)
        {
            return DB.SelectAllColumnsFrom<VbsDocumentCategory>().NoLock()
                 .Where(VbsDocumentCategory.Columns.Id).IsEqualTo(id)
                 .ExecuteSingle<VbsDocumentCategory>();
        }

        public VbsDocumentCategoryCollection VbsDocumentCategoryGetList()
        {
            return DB.SelectAllColumnsFrom<VbsDocumentCategory>().NoLock()
                 .ExecuteAsCollection<VbsDocumentCategoryCollection>();
        }

        public void VbsDocumentCategorySet(VbsDocumentCategory category)
        {
            DB.Save(category);
        }
        #endregion vbs_document_category

        #region vbs_document_file
        public VbsDocumentFile VbsDocumentFileGet(int id)
        {
            return DB.SelectAllColumnsFrom<VbsDocumentFile>().NoLock()
                 .Where(VbsDocumentFile.Columns.Id).IsEqualTo(id)
                 .ExecuteSingle<VbsDocumentFile>();
        }
        public VbsDocumentFileCollection VbsDocumentFileGetList()
        {
            return DB.SelectAllColumnsFrom<VbsDocumentFile>().NoLock()
                 .ExecuteAsCollection<VbsDocumentFileCollection>();
        }

        public void VbsDocumentFileSet(VbsDocumentFile file)
        {
            DB.Save(file);
        }
        #endregion vbs_document_file

        #region vbs_contact_info
        public VbsContactInfo VbsContactInfoGet(int id)
        {
            return DB.SelectAllColumnsFrom<VbsContactInfo>().NoLock()
                 .Where(VbsContactInfo.Columns.Id).IsEqualTo(id)
                 .ExecuteSingle<VbsContactInfo>();
        }

        public void VbsContactInfoSet(VbsContactInfo info)
        {
            DB.Save(info);
        }
        #endregion vbs_contact_info
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System.Linq;
using LunchKingSite.Core.Component;
using Microsoft.SqlServer.Types;

namespace LunchKingSite.SsBLL
{
    public class SSEventProvider : IEventProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region AppDownloadSMSLog

        public AppDownloadSmsLogCollection AppDownloadSmsLogGetList(string phoneNo, DateTime startDateTime, DateTime endDateTime)
        {
            return DB.Select().From(AppDownloadSmsLog.Schema.TableName)
                .Where(AppDownloadSmsLog.PhoneNoColumn).IsEqualTo(phoneNo)
                .And(AppDownloadSmsLog.CreateTimeColumn).IsBetweenAnd(startDateTime, endDateTime)
                .ExecuteAsCollection<AppDownloadSmsLogCollection>();
        }

        public AppDownloadSmsLogCollection AppDownloadSmsLogGetList(string phoneNo)
        {
            return DB.Select().From(AppDownloadSmsLog.Schema.TableName)
                .Where(AppDownloadSmsLog.PhoneNoColumn).IsEqualTo(phoneNo)
                .ExecuteAsCollection<AppDownloadSmsLogCollection>();
        }

        public bool AppDownloadSmsLogSet(AppDownloadSmsLog log)
        {
            if (DB.Save<AppDownloadSmsLog>(log) == 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region EventContent

        public bool EventContentSet(EventContent content)
        {
            try
            {
                DB.Save<EventContent>(content);
            }
            catch (Exception)
            {
                //throw ex;
                return false;
            }
            return true;
        }

        public EventContent EventContentGet(Guid eid)
        {
            return DB.SelectAllColumnsFrom<EventContent>().Where(EventContent.Columns.Guid).IsEqualTo(eid).ExecuteSingle<EventContent>();
        }

        public EventContent EventContentGet(string url)
        {
            return DB.SelectAllColumnsFrom<EventContent>().Top("1").Where(EventContent.Columns.Url).IsEqualTo(url).OrderDesc(EventContent.Columns.StartTime).ExecuteSingle<EventContent>();
        }

        public EventContentCollection EventContentGetList(int page, int size)
        {
            //bool success = false;
            //if (Helper.IsNumeric(page) && Helper.IsNumeric(size))
            //{
            //    if (page > 0)
            //        success = true;
            //}

            //if (success)
            //    return DB.Select().From(EventContent.Schema.TableName).Paged(page, size).OrderDesc(EventContent.Columns.StartTime).ExecuteAsCollection<EventContentCollection>();
            //else
            //    return DB.Select().Top(size.ToString()).From(EventContent.Schema.TableName).OrderDesc(EventContent.Columns.StartTime).ExecuteAsCollection<EventContentCollection>();

            return DB.Select().From(EventContent.Schema.TableName).OrderDesc(EventContent.Columns.StartTime).ExecuteAsCollection<EventContentCollection>();
        }

        #endregion

        #region EventEmail
        public DataTable EmailGetListFromEventEmailList(string eid)
        {
            //return DB.Select().From(EventEmailList.Schema.TableName).Where(EventEmailList.Columns.Eid).IsEqualTo(eid).ExecuteAsCollection<EventEmailListCollection>();
            //IDataReader rdr = new SubSonic.InlineQuery().ExecuteReader(sql, bid.ToString())
            IDataReader rdr = DB.Select().From(EventEmailList.Schema.TableName).Where(EventEmailList.Columns.Eid).IsEqualTo(eid).
                    ExecuteReader();
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        public bool EventEmailSet(EventEmailList el)
        {
            int count = DB.Select().From(EventEmailList.Schema.TableName)
                    .Where(EventEmailList.Columns.Eid).IsEqualTo(el.Eid).And(EventEmailList.Columns.Email).IsEqualTo(el.Email)
                    .GetRecordCount();
            if (count == 0)
            {
                DB.Save<EventEmailList>(el);
                return true;
            }
            return false;
        }
        #endregion

        #region ViewPponDealTimeSlot
        public ViewPponDealTimeSlotCollection GetDealFromCity(int ct)
        {
            try
            {
                return DB.SelectAllColumnsFrom<ViewPponDealTimeSlot>().Where(ViewPponDealTimeSlot.Columns.CityId).
                    IsEqualTo(ct).And(ViewPponDealTimeSlot.Columns.EffectiveStart).IsLessThan(DateTime.Now)
                    .And(ViewPponDealTimeSlot.Columns.EffectiveEnd).IsGreaterThanOrEqualTo(DateTime.Now)
                    .ExecuteAsCollection<ViewPponDealTimeSlotCollection>();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 活動頁設定活動主題 (EventActivity)
        public void EventActivitySet(EventActivity ea)
        {
            DB.Save<EventActivity>(ea);
        }

        public EventActivityCollection EventActivityGetList(DateTime dateStart, DateTime dateEnd)
        {
            return DB.SelectAllColumnsFrom<EventActivity>().Where(EventActivity.Columns.EndDate).IsBetweenAnd(dateStart, dateEnd).OrderDesc(EventActivity.Columns.Id).ExecuteAsCollection<EventActivityCollection>();
        }

        public EventActivity EventActivityGet(int id)
        {
            return DB.Get<EventActivity>(id);
        }

        public EventActivityCollection EventActivityGetByPageNameList(string pageName)
        {
            return DB.SelectAllColumnsFrom<EventActivity>()
                .Where(EventActivity.Columns.PageName).IsEqualTo(pageName)
                .ExecuteAsCollection<EventActivityCollection>();
        }

        public EventActivity EventActivityGetByPageName(string pageName)
        {
            return DB.SelectAllColumnsFrom<EventActivity>().Where(EventActivity.Columns.PageName).IsEqualTo(pageName).ExecuteSingle<EventActivity>();
        }

        public EventActivityCollection EventActivityEdmGetList(int exclude_id)
        {
            return DB.SelectAllColumnsFrom<EventActivity>().Where(EventActivity.Columns.EndDate).IsGreaterThanOrEqualTo(DateTime.Now)
                .And(EventActivity.Columns.Type).In((int)EventActivityType.PopUp, (int)EventActivityType.PopUpEdm)
                .And(EventActivity.Columns.Id).IsNotEqualTo(exclude_id).And(EventActivity.Columns.Status).IsEqualTo(true)
                .OrderDesc(EventActivity.Columns.Id).ExecuteAsCollection<EventActivityCollection>();
        }
        #endregion

        #region PromoItem
        public void PromoItemSet(PromoItem item)
        {
            DB.Save<PromoItem>(item);
        }

        public PromoItem PromoItemGet(int id)
        {
            return DB.Get<PromoItem>(id);
        }

        public PromoItem PromoItemGetByUserId(string userId)
        {
            return DB.SelectAllColumnsFrom<PromoItem>().Where(PromoItem.Columns.UserId).IsEqualTo(userId).ExecuteSingle<PromoItem>();
        }

        public PromoItem PromoItemGetByCode(string code)
        {
            return DB.SelectAllColumnsFrom<PromoItem>().Where(PromoItem.Columns.Code).IsEqualTo(code).ExecuteSingle<PromoItem>();
        }

        public PromoItem PromoItemGetByCode(int eventId, string code)
        {
            return DB.SelectAllColumnsFrom<PromoItem>().Where(PromoItem.Columns.EventActivityId).IsEqualTo(eventId).And(PromoItem.Columns.Code).IsEqualTo(code).ExecuteSingle<PromoItem>();
        }

        public PromoItemCollection PromoItemGetIsUsedListByEventyId(int eventId)
        {
            return DB.SelectAllColumnsFrom<PromoItem>().Where(PromoItem.Columns.EventActivityId).IsEqualTo(eventId).And(PromoItem.IsUsedColumn).IsEqualTo(true).ExecuteAsCollection<PromoItemCollection>();
        }

        public PromoItemCollection PromoItemGetListPaging(int pageStart, int pageLength, params string[] filter)
        {
            string sql = @"SELECT * FROM " + PromoItem.Schema.Provider.DelimitDbName(PromoItem.Schema.TableName) + @" with(nolock)";
            QueryCommand qc = new QueryCommand(sql, PromoItem.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(PromoItem.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, PromoItem.IdColumn.ColumnName);
            PromoItemCollection ocol = new PromoItemCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public int PromoItemGetListCount(params string[] filter)
        {
            string sql = @"SELECT count(*) FROM " + PromoItem.Schema.Provider.DelimitDbName(PromoItem.Schema.TableName) + @" with(nolock)";
            QueryCommand qc = new QueryCommand(sql, PromoItem.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(PromoItem.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion

        #region EventPremiumPromo

        public void EventPremiumPromoSet(EventPremiumPromo eventPremiumPromo)
        {
            DB.Save<EventPremiumPromo>(eventPremiumPromo);
        }

        public EventPremiumPromo EventPremiumPromoGet(int eventPremiumPromoId)
        {
            return DB.Get<EventPremiumPromo>(eventPremiumPromoId);
        }

        public EventPremiumPromo EventPremiumPromoGet(string url, EventPremiumPromoType type)
        {
            return DB.SelectAllColumnsFrom<EventPremiumPromo>()
                .Where(EventPremiumPromo.Columns.Url).IsEqualTo(url).And(EventPremiumPromo.Columns.TemplateType).IsEqualTo((int)type)
                .ExecuteSingle<EventPremiumPromo>();
        }

        public EventPremiumPromoCollection EventPremiumPromoGetListPaging(int pageStart, int pageLength, params string[] filter)
        {
            string sql = @"SELECT * FROM " + EventPremiumPromo.Schema.Provider.DelimitDbName(EventPremiumPromo.Schema.TableName) + @" with(nolock)";
            QueryCommand qc = new QueryCommand(sql, EventPremiumPromo.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(EventPremiumPromo.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, EventPremiumPromo.IdColumn.ColumnName);
            EventPremiumPromoCollection ocol = new EventPremiumPromoCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        #endregion EventPremiumPromo

        #region EventPremiumPromoItem

        public void EventPremiumPromoItemSet(EventPremiumPromoItem item)
        {
            DB.Save<EventPremiumPromoItem>(item);
        }

        public int EventPremiumPromoItemSet(int eventpremiumid, int premiumamount, string awardname, string awardmail, string awardaddress, string awardmobile, int userid)
        {
            string sql = @"IF (select COUNT(id) from event_premium_promo_item where event_premium_promo_id=@eventpremiumid)+1 <= @premiumamount
                           BEGIN
                             insert into event_premium_promo_item values(@eventpremiumid,@awardname,@awardemail,@awardaddress,@awardmobile,GETDATE(),@userid)  
                           END
                               ";
            var qc = new QueryCommand(sql, DiscountCode.Schema.Provider.Name);
            qc.AddParameter("@premiumamount", premiumamount, DbType.Int32);

            qc.AddParameter("@eventpremiumid", eventpremiumid, DbType.Int32);
            qc.AddParameter("@awardname", awardname, DbType.String);
            qc.AddParameter("@awardemail", awardmail, DbType.AnsiString);
            qc.AddParameter("@awardaddress", awardaddress, DbType.String);
            qc.AddParameter("@awardmobile", awardmobile, DbType.AnsiString);
            qc.AddParameter("@userid", userid, DbType.Int32);

            var a = DB.Execute(qc);
            return a;
        }

        public EventPremiumPromoItem EventPremiumPromoItemGet(int id)
        {
            return DB.Get<EventPremiumPromoItem>(id);
        }

        public EventPremiumPromoItem EventPremiumPromoItemGetByUserId(string userId)
        {
            return DB.SelectAllColumnsFrom<EventPremiumPromoItem>().Where(EventPremiumPromoItem.Columns.UserId).IsEqualTo(userId).ExecuteSingle<EventPremiumPromoItem>();
        }

        public EventPremiumPromoItemCollection EventPremiumPromoItemGetListByEventyId(int eventPremiumPromoId)
        {
            return DB.SelectAllColumnsFrom<EventPremiumPromoItem>().Where(EventPremiumPromoItem.Columns.EventPremiumPromoId).IsEqualTo(eventPremiumPromoId).ExecuteAsCollection<EventPremiumPromoItemCollection>();
        }

        public EventPremiumPromoItemCollection EventPremiumPromoItemGetListPaging(int pageStart, int pageLength, params string[] filter)
        {
            string sql = @"SELECT * FROM " + EventPremiumPromoItem.Schema.Provider.DelimitDbName(EventPremiumPromoItem.Schema.TableName) + @" with(nolock)";
            QueryCommand qc = new QueryCommand(sql, EventPremiumPromoItem.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(EventPremiumPromoItem.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, EventPremiumPromoItem.IdColumn.ColumnName);
            EventPremiumPromoItemCollection ocol = new EventPremiumPromoItemCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public int EventPremiumPromoItemGetListCount(params string[] filter)
        {
            string sql = @"SELECT count(*) FROM " + EventPremiumPromoItem.Schema.Provider.DelimitDbName(EventPremiumPromoItem.Schema.TableName) + @" with(nolock)";
            QueryCommand qc = new QueryCommand(sql, EventPremiumPromoItem.Schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(EventPremiumPromoItem.Schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion EvnetPremiumPromoItem


        #region 優惠券

        #region VourcherEvent
        public void VourcherEventSet(VourcherEvent vourcher_event)
        {
            DB.Save<VourcherEvent>(vourcher_event);
        }

        public VourcherEvent VourcherEventGetById(int id)
        {
            return DB.SelectAllColumnsFrom<VourcherEvent>().Where(VourcherEvent.Columns.Id).IsEqualTo(id).ExecuteSingle<VourcherEvent>();
        }

        public VourcherEventCollection VourcherEventGetBySellerGuid(Guid seller_guid)
        {
            return DB.SelectAllColumnsFrom<VourcherEvent>().Where(VourcherEvent.Columns.SellerGuid).IsEqualTo(seller_guid).ExecuteAsCollection<VourcherEventCollection>();
        }
        public VourcherEventCollection VourcherEventGetByUserCollect(int userId, VourcherCollectType? collectType, VourcherCollectStatus? collectStatus)
        {
            var subSelect =
                DB.Select(VourcherCollect.Columns.EventId).From<VourcherCollect>().Where(VourcherCollect.UniqueIdColumn)
                    .IsEqualTo(userId);
            if (collectType != null)
            {
                subSelect.And(VourcherCollect.TypeColumn).IsEqualTo((int)collectType);
            }
            if (collectStatus != null)
            {
                subSelect.And(VourcherCollect.StatusColumn).IsEqualTo((int)collectStatus);
            }
            return DB.SelectAllColumnsFrom<VourcherEvent>().Where(VourcherEvent.StatusColumn).IsEqualTo(
                    (int)VourcherEventStatus.EventChecked)
                    .And(VourcherEvent.IdColumn)
                    .In(subSelect).
                    ExecuteAsCollection<VourcherEventCollection>();
        }
        public void VourcherEventReturnApply(int event_id, string message)
        {
            DB.Update<VourcherEvent>().Set(VourcherEvent.StatusColumn).EqualTo((int)VourcherEventStatus.ReturnApply).Set(VourcherEvent.ReturnTimeColumn).EqualTo(DateTime.Now).Set(VourcherEvent.MessageColumn).EqualTo(message)
                .Where(VourcherEvent.IdColumn).IsEqualTo(event_id).Execute();
        }

        public void VourcherEventUpdatePageCount(int event_id, int newPageCount)
        {
            DB.Update<VourcherEvent>().Set(VourcherEvent.PageCountColumn).EqualTo(newPageCount).Where(VourcherEvent.IdColumn).IsEqualTo(event_id).Execute();
        }

        public void VourcherEventAddPageCount(int event_id, int addPageCount)
        {
            DB.Update<VourcherEvent>().SetExpression(VourcherEvent.Columns.PageCount).EqualTo(
                VourcherEvent.Columns.PageCount + "+" + addPageCount).Where(VourcherEvent.IdColumn).IsEqualTo(event_id).Execute();
        }

        public int VourcherEventGetPageCountSum()
        {
            return DB.Select(Aggregate.Sum(VourcherEvent.Columns.PageCount)).From(VourcherEvent.Schema)
                    .ExecuteScalar<int>();
        }

        public void VoucherPromoAddPageCount(int promo_id, int addPageCount)
        {
            DB.Update<VourcherPromo>().SetExpression(VourcherPromo.Columns.PageCount).EqualTo(
                 VourcherPromo.Columns.PageCount + "+" + addPageCount).Where(VourcherPromo.IdColumn).IsEqualTo(promo_id).Execute();
        }
        #endregion VourcherEvent

        #region VourcherStore
        public VourcherStoreCollection VourcherStoreCollectionGetByEventId(int event_id)
        {
            return DB.SelectAllColumnsFrom<VourcherStore>().Where(VourcherStore.Columns.VourcherEventId).IsEqualTo(event_id).ExecuteAsCollection<VourcherStoreCollection>();
        }
        public void VourcherStoreDeleteByEventId(int event_id)
        {
            DB.Delete<VourcherStore>(VourcherStore.Columns.VourcherEventId, event_id);
        }

        public void VourcherStoreDeleteByStoreGuid(Guid store_guid)
        {
            DB.Delete<VourcherStore>(VourcherStore.Columns.StoreGuid, store_guid);
        }

        public void VourcherStoresSet(VourcherStoreCollection vourcherstores)
        {
            DB.SaveAll<VourcherStore, VourcherStoreCollection>(vourcherstores);
        }

        public VourcherStoreCollection VourcherEventGetByStoreGuid(Guid store_guid)
        {
            return DB.SelectAllColumnsFrom<VourcherStore>().Where(VourcherStore.Columns.StoreGuid).IsEqualTo(store_guid).ExecuteAsCollection<VourcherStoreCollection>();
        }
        #endregion VourcherStore

        #region ViewVourcherSeller

        public ViewVourcherSellerCollection TodayVourcherEventSellerCollectionGet(DateTime todayDateTime)
        {
            var vcol = DB.SelectAllColumnsFrom<ViewVourcherSeller>().Where(ViewVourcherSeller.Columns.StartDate).IsLessThanOrEqualTo(todayDateTime)
                                                                   .And(ViewVourcherSeller.Columns.EndDate).IsGreaterThan(todayDateTime)
                                                                   .And(ViewVourcherSeller.Columns.Mode).IsEqualTo((int)VourcherEventMode.None)
                                                                   .And(ViewVourcherSeller.Columns.Enable).IsEqualTo(1)
                                                                   .And(ViewVourcherSeller.Columns.Status).IsEqualTo((int)VourcherEventStatus.EventChecked)
                                                                   .ExecuteAsCollection<ViewVourcherSellerCollection>();

            return vcol;

        }

        public ViewVourcherSellerCollection VourcherEventSellerCollectionGetBySearch(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string orderBy, int status = -1)
        {
            ViewVourcherSellerCollection vcol = new ViewVourcherSellerCollection();
            string defOrderBy = ViewVourcherSeller.Columns.Id + " desc";
            string sqlcommand = "select * from " + ViewVourcherSeller.Schema.TableName + ((!string.IsNullOrEmpty(search_keys.Value) || status != -1) ? " where " : string.Empty);
            if (!string.IsNullOrEmpty(search_keys.Value))
                sqlcommand += search_keys.Key + " like @" + search_keys.Key;
            if (status != -1)
                sqlcommand += (!string.IsNullOrEmpty(search_keys.Value) ? " and " : string.Empty) + ViewVourcherSeller.Columns.Status + "=@status";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(search_keys.Value))
                qc.Parameters.Add("@" + search_keys.Key, "%" + search_keys.Value + "%", DbType.String);
            if (status != -1)
                qc.Parameters.Add("@status", status, DbType.Int32);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public ViewVourcherSellerCollection VourcherEventSellerCollectionGetSellerId(int pageStart, int pageLength, string seller_id, string orderBy)
        {
            ViewVourcherSellerCollection vcol = new ViewVourcherSellerCollection();
            string defOrderBy = ViewVourcherSeller.Columns.Id + " desc";
            string sqlcommand = "select * from " + ViewVourcherSeller.Schema.TableName + " where " + ViewVourcherSeller.Columns.StartDate + "<=getdate() and " + ViewVourcherSeller.Columns.EndDate + ">getdate() and "
                + ViewVourcherSeller.Columns.Enable + "=1 and " + ViewVourcherSeller.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked + " and " + ViewVourcherSeller.Columns.SellerId + "=@sellerid";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@sellerid", seller_id, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public int VourcherEventSellerCollectionGetCount(KeyValuePair<string, string> search_keys, int status = -1)
        {
            string sqlcommand = "select count(1) from " + ViewVourcherSeller.Schema.TableName + ((!string.IsNullOrEmpty(search_keys.Value) || status != -1) ? " where " : string.Empty);
            if (!string.IsNullOrEmpty(search_keys.Value))
                sqlcommand += search_keys.Key + " like @" + search_keys.Key;
            if (status != -1)
                sqlcommand += (!string.IsNullOrEmpty(search_keys.Value) ? " and " : string.Empty) + ViewVourcherSeller.Columns.Status + "=@status";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(search_keys.Value))
                qc.Parameters.Add("@" + search_keys.Key, "%" + search_keys.Value + "%", DbType.String);
            if (status != -1)
                qc.Parameters.Add("@status", status, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public int VourcherEventSellerCollectionGetCount(string username, int status)
        {
            string sqlcommand = "select count(1) from " + ViewVourcherSeller.Schema.TableName + " where " + ViewVourcherSeller.Columns.Status + "=@status ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + ViewVourcherSeller.Columns.CreateId + "=@username";
            //sqlcommand += " order by " + ViewVourcherSeller.Columns.Id + " desc ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@status", status, DbType.Int32);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@username", username, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(string username, int status)
        {
            string sqlcommand = "select * from " + ViewVourcherSeller.Schema.TableName + " where " + ViewVourcherSeller.Columns.Status + "=@status ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + ViewVourcherSeller.Columns.CreateId + "=@username";
            sqlcommand += " order by " + ViewVourcherSeller.Columns.Id + " desc ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@status", status, DbType.Int32);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@username", username, DbType.String);
            ViewVourcherSellerCollection vcol = new ViewVourcherSellerCollection();
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public ViewVourcherSellerCollection VourcherEventSellerCollectionGetByStatus(int pageStart, int pageLength, string orderBy, string username, int status)
        {
            string defOrderBy = ViewVourcherSeller.Columns.Id + " desc";
            string sqlcommand = "select * from " + ViewVourcherSeller.Schema.TableName + " where " + ViewVourcherSeller.Columns.Status + "=@status ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + ViewVourcherSeller.Columns.CreateId + "=@username";
            sqlcommand += " order by " + ViewVourcherSeller.Columns.Id + " desc ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@status", status, DbType.Int32);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@username", username, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewVourcherSellerCollection vcol = new ViewVourcherSellerCollection();
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public ViewVourcherSellerCollection ViewVourcherSellerCollectionGetBySellerGuid(Guid seller_guid)
        {
            return DB.SelectAllColumnsFrom<ViewVourcherSeller>().Where(ViewVourcherSeller.Columns.SellerGuid).IsEqualTo(seller_guid).ExecuteAsCollection<ViewVourcherSellerCollection>();
        }

        public ViewVourcherSeller ViewVourcherSellerGetById(int id)
        {
            string sql = "select top 1 * from " + ViewVourcherSeller.Schema.Provider.DelimitDbName(ViewVourcherSeller.Schema.TableName) +
                        @" where " + ViewVourcherSeller.Columns.Id + " = @id ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);

            var vvs = new ViewVourcherSeller();
            vvs.LoadAndCloseReader(DataService.GetReader(qc));
            return vvs;
        }

        public int ViewVourcherSellerGetSellerCountBySampleCategory(SellerSampleCategory? category)
        {
            string categorySql = "";
            if (category != null)
            {
                categorySql = " and " + ViewVourcherSeller.Columns.SellerCategory + " = @category ";
            }
            string sql = " select COUNT(*) from ( select " + ViewVourcherSeller.Columns.SellerGuid + " from " +
                         ViewVourcherSeller.Schema.Provider.DelimitDbName(ViewVourcherSeller.Schema.TableName) +
                         " with(nolock) " +
                         " where " + ViewVourcherSeller.Columns.StartDate + " <= @nowDate " +
                         " and " + ViewVourcherSeller.Columns.EndDate + " > @nowDate " +
                         " and " + ViewVourcherSeller.Columns.Enable + " = @enabled " +
                         categorySql +
                         " group by " + ViewVourcherSeller.Columns.SellerGuid + " )selelr ";
            QueryCommand qc = new QueryCommand(sql, ViewVourcherSeller.Schema.Provider.Name);
            if (category != null)
            {
                qc.Parameters.Add("@category", (int)category.Value, DbType.Int32);
            }
            qc.Parameters.Add("@nowDate", DateTime.Now, DbType.DateTime);
            qc.Parameters.Add("@enabled", true, DbType.Boolean);
            return (int)DataService.ExecuteScalar(qc);

        }

        public int ViewVourcherSellerGetSellerCountBySampleCategoryAndCityId(SellerSampleCategory? category, int cityId)
        {
            string categorySql = "";
            if (category != null)
            {
                categorySql = " and " + ViewVourcherSeller.Columns.SellerCategory + " = @category ";
            }
            string sql = " select COUNT(*) from ( select " + ViewVourcherSeller.Columns.SellerGuid + " from " +
                         ViewVourcherSeller.Schema.Provider.DelimitDbName(ViewVourcherSeller.Schema.TableName) +
                         " with(nolock) " +
                         " where " + ViewVourcherSeller.Columns.StartDate + " <= @nowDate " +
                         " and " + ViewVourcherSeller.Columns.EndDate + " > @nowDate " +
                         " and " + ViewVourcherSeller.Columns.Enable + " = @enabled " +
                         " and " + ViewVourcherSeller.Columns.CityBit + "& " +
                         "( select SUM(" + CityGroup.Columns.BitNumber + ") from " +
                         CityGroup.Schema.Provider.DelimitDbName(CityGroup.Schema.TableName) +
                         " where " + CityGroup.Columns.RegionId + " = @cityId and " +
                         CityGroup.Columns.Type + " = " + (int)CityGroupType.Vourcher + " ) > 0 " +
                         categorySql +
                         " group by " + ViewVourcherSeller.Columns.SellerGuid + " )selelr ";
            QueryCommand qc = new QueryCommand(sql, ViewVourcherSeller.Schema.Provider.Name);
            if (category != null)
            {
                qc.Parameters.Add("@category", (int)category.Value, DbType.Int32);
            }
            qc.Parameters.Add("@nowDate", DateTime.Now, DbType.DateTime);
            qc.Parameters.Add("@enabled", true, DbType.Boolean);
            qc.Parameters.Add("@cityId", cityId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion ViewVourcherSeller

        #region ViewVourcherStoreFacade
        public ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGet(int pageStart, int pageLength, int regionId, int distance, SqlGeography geo, string orderBy)
        {
            ViewVourcherStoreFacadeCollection vcol = new ViewVourcherStoreFacadeCollection();
            string defOrderBy = ViewVourcherStoreFacade.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherStoreFacade.Schema.TableName + " where ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (regionId > 0)
            {
                qc.CommandSql += ViewVourcherStoreFacade.Columns.RegionId + "=@regionId";
                qc.Parameters.Add("@regionId", regionId, DbType.Int32);
            }
            else if (distance > 0)
            {
                string coorStr = "(Geography::Point(" + geo.Lat + ", " + geo.Long + ", 4326).STDistance(coordinate))";
                qc.CommandSql += coorStr + " < @distance ";
                qc.AddParameter("@distance", distance, DbType.Int32);
            }
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        /// <summary>
        /// 依 城市id 優惠券類型、地理區域 為條件查詢分店資訊，並依據排序欄位進行排序。
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市ID</param>
        /// <param name="category">優惠券類型，未傳入表示不區分</param>
        /// <param name="geography">地理資訊的條件，未傳入表示不做地理區間的限制</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="theType">資料庫查詢附載平衡的要求，預設為止允許查詢主要的DB</param>
        /// <returns></returns>
        public ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeCollectionGet(int pageStart, int pageLength, int regionId, SellerSampleCategory? category
           , GeographySearchParameter geography, string orderBy)
        {
            ViewVourcherStoreFacadeCollection vcol = new ViewVourcherStoreFacadeCollection();
            string defOrderBy = ViewVourcherStoreFacade.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherStoreFacade.Schema.TableName + " where (1=1)";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (regionId > 0)
            {
                qc.CommandSql += " and " + ViewVourcherStoreFacade.Columns.RegionId + "=@regionId";
                qc.Parameters.Add("@regionId", regionId, DbType.Int32);
            }
            if (category != null)
            {
                qc.CommandSql += " and " + ViewVourcherStoreFacade.Columns.SellerCategory + "=@category";
                qc.Parameters.Add("@category", (int)category.Value, DbType.Int32);
            }
            else
            {
                qc.CommandSql += " and " + ViewVourcherStoreFacade.Columns.SellerCategory + " is not null ";
            }

            if (geography != null)
            {
                string coorStr = " and (Geography::Point(" + geography.Geography.Lat + ", " + geography.Geography.Long + ", 4326).STDistance(coordinate))";
                qc.CommandSql += coorStr + " < @distance ";
                qc.AddParameter("@distance", geography.Distance, DbType.Int32);
            }
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }


        /// <summary>
        /// 依 優惠券類型、地理區域 為條件查詢分店資訊，並依據排序欄位進行排序。
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="category">優惠券類型，未傳入表示不區分</param>
        /// <param name="geography">地理資訊的條件，未傳入表示不做地理區間的限制</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="theType">資料庫查詢附載平衡的要求，預設為止允許查詢主要的DB</param>
        /// <returns></returns>
        public ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetList(int pageStart, int pageLength, SellerSampleCategory? category
           , GeographySearchParameter geography, string orderBy)
        {
            ViewVourcherStoreFacadeCollection vcol = new ViewVourcherStoreFacadeCollection();
            string defOrderBy = ViewVourcherStoreFacade.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherStoreFacade.Schema.TableName + " where (1=1)";

            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (category != null)
            {
                qc.CommandSql += " and " + ViewVourcherStoreFacade.Columns.SellerCategory + "=@category";
                qc.Parameters.Add("@category", (int)category.Value, DbType.Int32);
            }
            else
            {
                qc.CommandSql += " and " + ViewVourcherStoreFacade.Columns.SellerCategory + " is not null ";
            }

            if (geography != null)
            {
                string coorStr = " and (Geography::Point(" + geography.Geography.Lat + ", " + geography.Geography.Long + ", 4326).STDistance(coordinate))";
                qc.CommandSql += coorStr + " < @distance ";
                qc.AddParameter("@distance", geography.Distance, DbType.Int32);
            }
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public int ViewVourcherStoreFacadeCollectionGetCount(int regionId, int distance, SqlGeography geo)
        {
            string sqlcommand = "select count(1) from " + ViewVourcherStoreFacade.Schema.TableName + " where ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (regionId > 0)
            {
                qc.CommandSql += ViewVourcherStoreFacade.Columns.RegionId + "=@regionId";
                qc.Parameters.Add("@regionId", regionId, DbType.Int32);
            }
            else if (distance > 0)
            {
                string coorStr = "(Geography::Point(" + geo.Lat + ", " + geo.Long + ", 4326).STDistance(coordinate))";
                qc.CommandSql += coorStr + " < @distance ";
                qc.AddParameter("@distance", distance, DbType.Int32);
            }
            return (int)DataService.ExecuteScalar(qc);
        }
        /// <summary>
        /// 查詢優惠券商家資料
        /// </summary>
        /// <param name="pageStart">第幾頁</param>
        /// <param name="pageLength">每頁幾筆</param>
        /// <param name="orderBy">排序條件</param>
        /// <param name="filter">搜尋條件</param>
        /// <returns></returns>
        public ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewVourcherStoreFacade, ViewVourcherStoreFacadeCollection>(pageStart, pageLength, orderBy, filter);
        }


        public ViewVourcherStoreFacadeCollection ViewVourcherStoreFacadeGetListByEventPromoId(int eventId)
        {
            ViewVourcherStoreFacadeCollection vcol = new ViewVourcherStoreFacadeCollection();
            string sql = " select * from " + EventPromoItem.Schema.TableName + " i with(nolock), " +
                         ViewVourcherStoreFacade.Schema.TableName + " v with(nolock) " +
                         "where i.MainId = @mainId and i.item_type = @itemType and i.Status = @status " +
                         " and i.item_id = v.vourcher_event_id order by i.Seq ";
            QueryCommand qc = new QueryCommand(sql, ViewVourcherStoreFacade.Schema.Provider.Name);
            qc.AddParameter("@mainId", eventId, DbType.Int32);
            qc.AddParameter("@itemType", (int)EventPromoItemType.Vourcher, DbType.Int32);
            qc.AddParameter("@status", true, DbType.Boolean);

            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }
        #endregion ViewVourcherStoreFacade

        #region ViewVourcherStore

        public ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int event_id)
        {
            ViewVourcherStoreCollection vvscol = new ViewVourcherStoreCollection();
            string sqlcommand = "select * from " + ViewVourcherStore.Schema.TableName + " where " + ViewVourcherStore.Columns.VourcherEventId + "=@event_id";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@event_id", event_id, DbType.Int32);
            vvscol.LoadAndCloseReader(DataService.GetReader(qc));
            return vvscol;
        }

        public ViewVourcherStore ViewVourcherStoreGetById(int id)
        {
            ViewVourcherStore store = new ViewVourcherStore();
            string sql = "select top 1 * from " + ViewVourcherStore.Schema.TableName + " where " + ViewVourcherStore.Columns.Id + " =@id ";
            QueryCommand qc = new QueryCommand(sql, ViewVourcherStore.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            store.LoadAndCloseReader(DataService.GetReader(qc));
            return store;
        }

        public ViewVourcherStoreCollection ViewVourcherStoreCollectionGetByEventId(int pageStart, int pageLength, int event_id, string orderBy)
        {
            ViewVourcherStoreCollection vcol = new ViewVourcherStoreCollection();
            string defOrderBy = ViewVourcherStore.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherStore.Schema.TableName + " where " + ViewVourcherStore.Columns.VourcherEventId + "=@eventid";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@eventid", event_id, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }
        /// <summary>
        /// 依據任何條件搜尋優惠券的分店
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy">排序</param>
        /// <param name="theType">搜尋DB 複載平衡的狀況</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewVourcherStoreCollection ViewVourcherStoreGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            ViewVourcherStoreCollection vcol = new ViewVourcherStoreCollection();
            string defOrderBy = ViewVourcherStore.Columns.Id;
            QueryCommand qc = SSHelper.GetWhereQC<ViewVourcherStore>(filter);

            qc.CommandSql = "select * from " +
                            ViewVourcherStore.Schema.Provider.DelimitDbName(ViewVourcherStore.Schema.TableName) + " " +
                            qc.CommandSql;

            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }
        #endregion ViewVourcherStore

        #region VourcherOrder
        public void VourcherOrdertSet(VourcherOrder vourcherOrder)
        {
            DB.Save<VourcherOrder>(vourcherOrder);
        }
        public VourcherOrderCollection VourcherOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<VourcherOrder, VourcherOrderCollection>(pageStart, pageLength, orderBy, filter);
        }
        #endregion VourcherOrder

        #region VourcherCollect
        public void VourcherCollectSet(VourcherCollect vourcherCollect)
        {
            DB.Save<VourcherCollect>(vourcherCollect);
        }

        public VourcherCollect VourcherCollectGet(int uniqueId, int eventId)
        {
            return DB.SelectAllColumnsFrom<VourcherCollect>()
                .Where(VourcherCollect.Columns.UniqueId).IsEqualTo(uniqueId)
                .And(VourcherCollect.Columns.EventId).IsEqualTo(eventId)
                .ExecuteSingle<VourcherCollect>();
        }

        public VourcherCollectCollection VourcherCollectGetByType(int uniqueId, VourcherCollectType type)
        {
            return DB.SelectAllColumnsFrom<VourcherCollect>()
                .Where(VourcherCollect.Columns.UniqueId).IsEqualTo(uniqueId)
                .And(VourcherCollect.Columns.Type).IsEqualTo(type)
                .ExecuteAsCollection<VourcherCollectCollection>();
        }

        public int VourcherCollectSetDisabledForOverdueEvent(int uniqueId, VourcherCollectType type)
        {
            var subSelect = DB.Select(VourcherCollect.Columns.Id).From(VourcherCollect.Schema)
                .InnerJoin(VourcherEvent.IdColumn, VourcherCollect.EventIdColumn)
                .Where(VourcherCollect.UniqueIdColumn).IsEqualTo(uniqueId)
                .And(VourcherEvent.EndDateColumn).IsLessThan(DateTime.Now).And(VourcherCollect.TypeColumn).IsEqualTo(
                    (int)type);

            return DB.Update<VourcherCollect>().Set(VourcherCollect.Columns.Status).EqualTo(
                (int)VourcherCollectStatus.Disabled)
                .Where(VourcherCollect.IdColumn).In(subSelect).Execute();
        }
        #endregion VourcherCollect

        #region ViewVourcherSellerCollect
        public ViewVourcherSellerCollectCollection ViewVourcherSellerCollectCollectionGetByUniqueId(int pageStart, int pageLength, int uniqueId, string orderBy)
        {
            ViewVourcherSellerCollectCollection vcol = new ViewVourcherSellerCollectCollection();
            string defOrderBy = ViewVourcherSellerCollect.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherSellerCollect.Schema.TableName
                + " where " + ViewVourcherSellerCollect.Columns.UniqueId + "=@uniqueId"
                + " and " + ViewVourcherSellerCollect.Columns.CollectStatus + "=" + (int)VourcherCollectStatus.Initial
                + " and " + ViewVourcherSellerCollect.Columns.CollectType + "=" + (int)VourcherCollectType.Favorite;
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@uniqueId", uniqueId, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        public ViewVourcherSellerCollectCollection ViewVourcherSellerCollectGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewVourcherSellerCollect, ViewVourcherSellerCollectCollection>(pageStart, pageLength, orderBy, filter);
        }
        #endregion ViewVourcherSellerCollect

        #region ViewVourcherSellerOrder
        public ViewVourcherSellerOrderCollection ViewVourcherSellerOrderCollectionGetByUniqueId(int pageStart, int pageLength, int uniqueId, string orderBy)
        {
            ViewVourcherSellerOrderCollection vcol = new ViewVourcherSellerOrderCollection();
            string defOrderBy = ViewVourcherSellerOrder.Columns.Id;
            string sqlcommand = "select * from " + ViewVourcherSellerOrder.Schema.TableName + " where " + ViewVourcherSellerOrder.Columns.UniqueId + "=@uniqueId";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@uniqueId", uniqueId, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageStart > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            vcol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcol;
        }

        #endregion ViewVourcherSellerOrder

        #region ViewVourcherPromo
        public ViewVourcherPromoCollection ViewVourcherPromoCollectionGetAll(int promo_type)
        {
            ViewVourcherPromoCollection vvscol = new ViewVourcherPromoCollection();
            string sqlcommand = "select * from " + ViewVourcherPromo.Schema.TableName + " where " + ViewVourcherPromo.Columns.Type + "=@promo_type";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@promo_type", promo_type);
            vvscol.LoadAndCloseReader(DataService.GetReader(qc));
            return vvscol;
        }
        #endregion ViewVourcherPromo

        #region VourcherPromo
        public void VourcherPromoDelete(int id)
        {
            DB.Delete<VourcherPromo>(VourcherPromo.Columns.Id, id);
        }

        public void VourcherPromoSet(VourcherPromo promo)
        {
            DB.Save<VourcherPromo>(promo);
        }

        public VourcherPromo VourcherPromoGet(int id)
        {
            return DB.SelectAllColumnsFrom<VourcherPromo>().Where(VourcherPromo.Columns.Id).IsEqualTo(id).ExecuteSingle<VourcherPromo>();
        }

        public VourcherPromoCollection VourcherPromoGetList(int type)
        {
            return DB.SelectAllColumnsFrom<VourcherPromo>().Where(VourcherPromo.Columns.Type).IsEqualTo(type).OrderDesc(VourcherPromo.Columns.StartDate).ExecuteAsCollection<VourcherPromoCollection>();
        }
        #endregion VourcherPromo

        #region ViewVourcherPromoShow
        public ViewVourcherPromoShowCollection ViewVourcherPromoShowGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewVourcherPromoShow, ViewVourcherPromoShowCollection>(pageStart, pageLength, orderBy, filter);
        }
        #endregion ViewVourcherPromoShow

        #region ViewYahooVoucher

        public ViewYahooVoucherSellerCollection ViewYahooVoucherSellerGetAvailableList()
        {
            ViewYahooVoucherSellerCollection vycsc = new ViewYahooVoucherSellerCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherSeller.Schema.TableName + " where "
                + ViewYahooVoucherSeller.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked
                + " and " + ViewYahooVoucherSeller.Columns.Enable + "=1 and " + ViewYahooVoucherSeller.Columns.IsCloseDown + "=0 and " + ViewYahooVoucherSeller.Columns.EndDate + ">=getdate()";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherSeller.Schema.Provider.Name);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherSellerCollection ViewYahooVoucherSellerGetById(int id)
        {
            ViewYahooVoucherSellerCollection vycsc = new ViewYahooVoucherSellerCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherSeller.Schema.TableName + " where "
                + ViewYahooVoucherSeller.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked
                + " and " + ViewYahooVoucherSeller.Columns.Enable + "=1 and " + ViewYahooVoucherSeller.Columns.IsCloseDown + "=0 and " + ViewYahooVoucherSeller.Columns.EndDate + ">=getdate() and " + ViewYahooVoucherSeller.Columns.EventId + "=@id";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherSeller.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherStoreCollection ViewYahooVoucherStoreGetAvailableList()
        {
            ViewYahooVoucherStoreCollection vycsc = new ViewYahooVoucherStoreCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherStore.Schema.TableName + " where "
                + ViewYahooVoucherStore.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked
                + " and " + ViewYahooVoucherStore.Columns.Enable + "=1 and " + ViewYahooVoucherStore.Columns.IsCloseDown + "=0 and " + ViewYahooVoucherStore.Columns.EndDate + ">=getdate()";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherStore.Schema.Provider.Name);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherStoreCollection ViewYahooVoucherStoreGetByEventId(int eventid)
        {
            ViewYahooVoucherStoreCollection vycsc = new ViewYahooVoucherStoreCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherStore.Schema.TableName + " where "
                + ViewYahooVoucherStore.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked
                + " and " + ViewYahooVoucherStore.Columns.Enable + "=1 and " + ViewYahooVoucherStore.Columns.IsCloseDown + "=0 and " + ViewYahooVoucherStore.Columns.EndDate + ">=getdate() and " + ViewYahooVoucherStore.Columns.EventId + "=@eventid";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherStore.Schema.Provider.Name);
            qc.AddParameter("@eventid", eventid, DbType.Int32);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherCategoryCollection ViewYahooVoucherCategoryGetAvailableList()
        {
            ViewYahooVoucherCategoryCollection vycsc = new ViewYahooVoucherCategoryCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherCategory.Schema.TableName + " where "
                + ViewYahooVoucherCategory.Columns.Status + "=" + (int)VourcherEventStatus.EventChecked
                + " and " + ViewYahooVoucherCategory.Columns.Enable + "=1 and " + ViewYahooVoucherCategory.Columns.EndDate + ">=getdate()";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherCategory.Schema.Provider.Name);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherPromoCollection ViewYahooVoucherPromoGetAvailableList()
        {
            ViewYahooVoucherPromoCollection vycsc = new ViewYahooVoucherPromoCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherPromo.Schema.TableName + " where " +
              ViewYahooVoucherPromo.Columns.Type + " in (" + (int)VourcherPromoType.TagCategory + "," + (int)VourcherPromoType.VoucherBlock + "," + (int)VourcherPromoType.VoucherRanking + "," + (int)VourcherPromoType.VoucherTag + "," + (int)VourcherPromoType.Ad + ")";
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherPromo.Schema.Provider.Name);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }

        public ViewYahooVoucherPromoCollection ViewYahooVoucherPromoGetAvailableList(int type, bool exclued_expired)
        {
            ViewYahooVoucherPromoCollection vycsc = new ViewYahooVoucherPromoCollection();
            string sqlcommand = "select * from " + ViewYahooVoucherPromo.Schema.TableName + " where " +
              ViewYahooVoucherPromo.Columns.Type + "=@type ";
            if (exclued_expired)
            {
                sqlcommand += " and " + ViewYahooVoucherPromo.Columns.EndDate + ">=getdate()";
            }
            QueryCommand qc = new QueryCommand(sqlcommand, ViewYahooVoucherPromo.Schema.Provider.Name);
            qc.AddParameter("@type", type, DbType.Int32);
            vycsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vycsc;
        }
        #endregion
        #endregion

        #region SkmEventPromoInvoiceContent

        public void SkmEventPromoInvoiceContentSet(SkmEventPromoInvoiceContent invoiceContent)
        {
            DB.Save<SkmEventPromoInvoiceContent>(invoiceContent);
        }

        public SkmEventPromoInvoiceContentCollection SkmEventPromoInvoiceContentCollectionGet()
        {
            return DB.Select()
              .From(SkmEventPromoInvoiceContent.Schema.TableName)
              .ExecuteAsCollection<SkmEventPromoInvoiceContentCollection>();
        }

        #endregion

        #region SkmEventPrizeWinner

        public void SkmEventPrizeWinnerSet(SkmEventPrizeWinner skmEventPrizeWinner)
        {
            DB.Save<SkmEventPrizeWinner>(skmEventPrizeWinner);
        }

        public SkmEventPrizeWinnerCollection SkmEventPrizeWinnerCollectionGet()
        {
            return DB.Select()
              .From(SkmEventPrizeWinner.Schema.TableName)
              .ExecuteAsCollection<SkmEventPrizeWinnerCollection>();
        }


        #endregion SkmEventPrizeWinner

        #region EventPromoVote 投票

        public ViewEventPromoVote ViewEventPromoVoteGetByPromoItemId(int eventPromoItemId)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoVote>()
                .Where(ViewEventPromoVote.Columns.Id).IsEqualTo(eventPromoItemId)
                .ExecuteAsCollection<ViewEventPromoVoteCollection>().FirstOrDefault();
        }

        public ViewEventPromoVoteCollection ViewEventPromoVoteGet(int mainId)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoVote>()
                .Where(ViewEventPromoVote.Columns.MainId).IsEqualTo(mainId).ExecuteAsCollection<ViewEventPromoVoteCollection>();
        }

        public int EventPromoVoteSet(EventPromoVote vote)
        {
            return DB.Save(vote);
        }

        public EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, DateTime start, DateTime end, EventPromoItemType itemType)
        {
            const string sql = @"select v.* from event_promo_vote v inner join event_promo_item i
                On v.event_promo_item_id = i.Id
                inner join event_promo p
                On i.MainId = p.Id
                where MainId = @mainId
                and member_unique_id = @memberUniqueId
                and i.item_type = @itemType
                and vote_time between @startDate and @endDate";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@mainId", eventPromoId, DbType.Int32);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);
            qc.AddParameter("@itemType", (int)itemType, DbType.Int32);
            qc.AddParameter("@startDate", start, DbType.DateTime);
            qc.AddParameter("@endDate", end, DbType.DateTime);

            var voteCol = new EventPromoVoteCollection();
            voteCol.LoadAndCloseReader(DataService.GetReader(qc));

            return voteCol;
        }

        public EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, DateTime start, DateTime end)
        {
            const string sql = @"select v.* from event_promo_vote v inner join event_promo_item i
                On v.event_promo_item_id = i.Id
                where MainId = @mainId
                and member_unique_id = @memberUniqueId
                and vote_time between @startDate and @endDate";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@mainId", eventPromoId, DbType.Int32);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);
            qc.AddParameter("@startDate", start, DbType.DateTime);
            qc.AddParameter("@endDate", end, DbType.DateTime);

            var voteCol = new EventPromoVoteCollection();
            voteCol.LoadAndCloseReader(DataService.GetReader(qc));

            return voteCol;
        }

        public EventPromoVoteCollection EventPromoVoteGetList(int eventPromoId, int memberUniqueId, EventPromoItemType itemType)
        {
            const string sql = @"select v.* from event_promo_vote v inner join event_promo_item i
                On v.event_promo_item_id = i.Id
                where MainId = @mainId
                and member_unique_id = @memberUniqueId
                and i.item_type = @itemType";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@mainId", eventPromoId, DbType.Int32);
            qc.AddParameter("@memberUniqueId", memberUniqueId, DbType.Int32);
            qc.AddParameter("@itemType", (int)itemType, DbType.Int32);

            var voteCol = new EventPromoVoteCollection();
            voteCol.LoadAndCloseReader(DataService.GetReader(qc));

            return voteCol;
        }

        public ViewEventPromoVourcherCollectCollection ViewEventPromoVourcherCollectGet(int mainId, int memberUniqueId)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoVourcherCollect>()
                .Where(ViewEventPromoVourcherCollect.Columns.MainId).IsEqualTo(mainId)
                .And(ViewEventPromoVourcherCollect.Columns.UniqueId).IsEqualTo(memberUniqueId)
                .ExecuteAsCollection<ViewEventPromoVourcherCollectCollection>();
        }

        public ViewEventPromoVoteRecordCollection ViewEventPromoVoteRecordGet(int eventPromoId, int memberUniqueId)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoVoteRecord>()
                .Where(ViewEventPromoVoteRecord.Columns.EventPromoId).IsEqualTo(eventPromoId)
                .And(ViewEventPromoVoteRecord.Columns.MemberUniqueId).IsEqualTo(memberUniqueId)
                .ExecuteAsCollection<ViewEventPromoVoteRecordCollection>();
        }

        #endregion

        #region 2016中元節活動
        public DiscountCodeCollection CheckDiscountCodeByCondition(int uniqueId)
        {
            var data = DB.SelectAllColumnsFrom<DiscountCode>()
                .Where(DiscountCode.Columns.Owner).IsEqualTo(uniqueId)
                .And(DiscountCode.Columns.SendDate).IsGreaterThan(DateTime.Today)
                .AndExpression(DiscountCode.Columns.Amount).IsEqualTo("37").Or(DiscountCode.Columns.Amount).IsEqualTo("77").Or(DiscountCode.Columns.Amount).IsEqualTo("117").Or(DiscountCode.Columns.Amount).IsEqualTo("177")
                .ExecuteAsCollection<DiscountCodeCollection>();
            return data;
        }
        #endregion
    }
}

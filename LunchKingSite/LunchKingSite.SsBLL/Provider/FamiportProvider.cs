﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;
using System.Data.SqlClient;
using System.Transactions;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;
using Famiport = LunchKingSite.Core.Models.Entities.Famiport;
using Peztemp = LunchKingSite.Core.Models.Entities.Peztemp;

namespace LunchKingSite.SsBLL.Provider
{
    public class FamiportProvider : IFamiportProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region Cache

        private static readonly ConcurrentDictionary<Guid, int> FamilyNetPincodeTransId = new ConcurrentDictionary<Guid, int>();

        public static int GetFamilyNetPincodeTransIdByOrderGuid(Guid orderGuid)
        {
            var result = FamilyNetPincodeTransId.GetOrAdd(orderGuid, delegate
            {
                var sql = string.Format(@"select id from family_net_pincode_trans with(nolock) where order_guid = '{0}'", orderGuid);
                using (var db = new FamiportDbContext())
                {
                    return db.Database.SqlQuery<int>(sql).FirstOrDefault();
                }
            });

            return result;
        }

        #endregion

        #region FamiportPincodeTran

        public int AddFamilyNetPincodeTran(FamilyNetPincodeTran trans)
        {
            var ticket = GetFamilyNetOrderTicket(FamilyNetApi.PinOrder);
            trans.OrderTicket = ticket;
            trans.Status = (byte)FamilyNetPincodeStatus.Init;
            trans.CreateTime = DateTime.Now;
            trans.ReplyDesc = trans.ReplyCode = trans.PinExpiredate = trans.Pincode = string.Empty;
            using (var db = new FamiportDbContext())
            {
                db.FamilyNetPincodeTranEntities.Add(trans);
                db.SaveChanges();
                return trans.Id;
            }
        }

        public bool UpdateFamilyNetPincodeTran(FamilyNetPincodeTran trans)
        {
            using (var db = new FamiportDbContext())
            {
                db.Entry(trans).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        public FamilyNetPincodeTran GetFamilyNetPincodeTrans(int id)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from t in db.FamilyNetPincodeTranEntities
                        where t.Id == id
                        select t).FirstOrDefault();
                    return result;
                }
            }
        }

        public FamilyNetPincodeTran GetFamilyNetPincodeTrans(Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from t in db.FamilyNetPincodeTranEntities
                        where t.OrderGuid == orderGuid
                        select t).FirstOrDefault();
                    return result;
                }
            }
        }

        public List<FamilyNetPincodeTran> GetFamilyNetPincodeTransRetry()
        {
            DateTime checkTime = DateTime.Now.AddHours(-12);
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var tc = (from t in db.FamilyNetPincodeTranEntities
                              where t.CreateTime > checkTime && t.Status == (byte)FamilyNetPincodeStatus.Completed
                              && t.Qty > db.FamilyNetPincodeEntities.Count(x => x.PincodeTransId == t.Id)
                              select t).ToList();
                    return tc;
                }
            }
        }

        #endregion FamiportPincodeTranEntity

        #region Peztemp
        
        public Peztemp GetPeztemp(int pezId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.PeztempEntities
                        where p.Id == pezId
                        select p).FirstOrDefault();
                    return result;
                }
            }
        }

        public Peztemp GetPeztemp(string pezCode)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.PeztempEntities
                        where p.PezCode == pezCode && p.ServiceCode == (int) PeztempServiceCode.FamilynetPincode
                        select p).FirstOrDefault();

                    return result;
                }
            }
        }

        public bool UpdatePeztemp(Peztemp entity)
        {
            using (var db = new FamiportDbContext())
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        #endregion Peztemp

        #region Famiport

        public Famiport GetFamiport(Guid bid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamiportEntities
                        where f.BusinessHourGuid == bid
                        select f).FirstOrDefault();
                    return result;
                }
            }
        }

        #endregion Famiport

        #region FamilyNetOrderTicket

        public int GetFamilyNetOrderTicket(FamilyNetApi api)
        {
            var t = new FamilyNetOrderTicket
            {
                CreateTime = DateTime.Now,
                ApiName = api.ToString(),
                RequestType = (byte) api
            };

            using (var db = new FamiportDbContext())
            {
                db.FamilyNetOrderTicketEntities.Add(t);
                db.SaveChanges();
                return t.Id;
            }
        }

        #endregion

        #region FamilyNetEvent

        public FamilyNetEvent GetFamilyNetEventById(int? id)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.Id == id
                                  select f).FirstOrDefault();
                    return result;
                }
            }
        }
        public FamilyNetEvent GetFamilyNetEvent(Guid bid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                        where f.BusinessHourGuid == bid
                        select f).FirstOrDefault();
                    return result;
                }
            }
        }
        public List<FamilyNetEvent> GetFamilyNetEventMainList()
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.IsComboDeal == false || f.Id ==f.ComboDealMainId
                                  select f).ToList();
                    return result;
                }
            }
        }
        public List<FamilyNetEvent> GetFamilyNetEventDealById(int? id)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.Id==id
                                  select f).ToList();
                    return result;
                }
            }
        }
        public List<FamilyNetEvent> GetFamilyNetEventDealSonListByMainId(int comboDealMainId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.ComboDealMainId == comboDealMainId && f.Id != comboDealMainId
                                  select f).ToList();
                    return result;
                }
            }
        }
        public List<FamilyNetEvent> GetFamilyNetEventDealList(Guid bid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.MainBusinessHourGuid.Equals(bid)
                                  select f).ToList();
                    return result;
                }
            }
        }
        public List<FamilyNetEvent> GetFamilyNetEventDealAllListByMainId(int comboDealMainId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from f in db.FamilyNetEventEntities
                                  where f.ComboDealMainId == comboDealMainId
                                  select f).ToList();
                    return result;
                }
            }
        }
        public FamilyNetEvent AddFamilyNetEvent(FamilyNetEvent entity)
        {
            var netEvent = new FamilyNetEvent
            {
                MainBusinessHourGuid = entity.MainBusinessHourGuid,
                BusinessHourGuid = entity.BusinessHourGuid,
                ActCode = entity.ActCode,
                ItemCode = entity.ItemCode,
                ItemName = entity.ItemName,
                ItemOrigPrice = entity.ItemOrigPrice,
                ItemPrice = entity.ItemPrice,
                PurchasePrice = entity.PurchasePrice,
                MaxItemCount = entity.MaxItemCount,
                Buy = entity.Buy,
                Free = entity.Free,
                BusinessHourOrderTimeS = entity.BusinessHourOrderTimeS,
                BusinessHourOrderTimeE = entity.BusinessHourOrderTimeE,
                BusinessHourDeliverTimeS = entity.BusinessHourDeliverTimeS,
                BusinessHourDeliverTimeE = entity.BusinessHourDeliverTimeE,
                Remark = entity.Remark,
                CreateId = entity.CreateId,
                CreateTime = entity.CreateTime,
                ModifyTime = entity.ModifyTime,
                Status = (byte)FamilyNetEventStatus.Draft,
                IsComboDeal = entity.IsComboDeal,
                ComboDealMainId = entity.Id,
                Version = (int)FamilyBarcodeVersion.FamiSingleBarcode
            };

            using (var db = new FamiportDbContext())
            {
                db.FamilyNetEventEntities.Add(netEvent);
                db.SaveChanges();
                return netEvent;
            }
        }
        public bool UpdateFamilyNetEvent(FamilyNetEvent entity)
        {
            using (var db = new FamiportDbContext())
            {
                entity.Version = (int)FamilyBarcodeVersion.FamiSingleBarcode;
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }
        public void DeleteFamilyNetEventComboDealByItems(List<FamilyNetEvent> items)
        {
            if(items.Count>0)
            {
                using (var db = new FamiportDbContext())
                {
                    foreach(var item in items)
                    {
                        db.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                        var result = db.SaveChanges();
                    }
                }
            }
        }
        public void AddFamilyNetEventComboDealList(List<FamilyNetEvent> items)
        {
            if (items.Count > 0)
            {
                using (var db = new FamiportDbContext())
                {
                    foreach (var item in items)
                    {
                        item.Version = (int)FamilyBarcodeVersion.FamiSingleBarcode;
                        db.Entry(item).State = System.Data.Entity.EntityState.Added;
                        var result = db.SaveChanges();
                    }
                }
            }
        }
        public void SetFamilyNetEventComboDealList(List<FamilyNetEvent> items)
        {
            if (items.Count > 0)
            {
                using (var db = new FamiportDbContext())
                {
                    foreach (var item in items)
                    {
                        item.Version = (int)FamilyBarcodeVersion.FamiSingleBarcode;
                        db.Entry(item).State =item.Id==0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                        var result = db.SaveChanges();
                    }
                }
            }
        }
        #endregion

        #region FamilyNetPincode

        public FamilyNetPincode GetFamilyNetPincodeByCouponId(int couponId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                                  where p.CouponId == couponId
                                  select p).FirstOrDefault();
                    return result;
                }
            }
        }

        public FamilyNetPincode GetFamilyNetPincode(int orderNo)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                        where p.OrderNo == orderNo
                        select p).FirstOrDefault();
                    return result;
                }
            }
        }

        public FamilyNetPincode GetFamilyNetPincodeByPeztempId(int peztempId)
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from f in db.FamilyNetPincodeEntities
                              where f.PeztempId == peztempId
                              select f).FirstOrDefault();
                return result;
            }
        }
        public FamilyNetPincode GetVerifiedFamilyNetPincodeByPeztempId(int peztempId)
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from f in db.FamilyNetPincodeEntities
                              where f.PeztempId == peztempId
                              && f.IsVerified
                              select f).FirstOrDefault();
                return result;
            }
        }

        public FamilyNetPincode GetFamilyNetPincode(int peztempId, string actCode, Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from f in db.FamilyNetPincodeEntities
                                where f.PeztempId == peztempId && f.OrderGuid == orderGuid && f.ActCode == actCode
                                select f).FirstOrDefault();
                return result;
            }
        }

        public FamilyNetPincode GetFamilyNetPincode(int peztempId, string actCode)
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from f in db.FamilyNetPincodeEntities
                              where f.PeztempId == peztempId && f.ActCode == actCode
                              select f).FirstOrDefault();
                return result;
            }
        }

        public FamilyNetPincode AddFamilyNetPincode(Peztemp pez, FamilyNetEvent famiEvent, Guid orderGuid, int couponId)
        {
            if (famiEvent.Id == 0)
            {
                return null;
            }

            var transId = GetFamilyNetPincodeTransIdByOrderGuid(orderGuid);
            if (transId == 0)
            {
                return null;
            }

            using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
            {
                if (config.FamiOrderExgGate)
                {
                    //防址同秒內多次request
                    AddFamilyNetOrderExgGate(new FamilyNetOrderExgGate
                    {
                        PeztempId = pez.Id,
                        Processing = true
                    });

                    if (CheckFamilyNetOrderExgProcessing(pez.Id))
                    {
                        return null;
                    }
                }

                var ticketNo = GetFamilyNetOrderTicket(FamilyNetApi.OrderExg);

                var pincode = new FamilyNetPincode
                {
                    PeztempId = pez.Id,
                    OrderDate = DateTime.Now,
                    ActCode = famiEvent.ActCode,
                    Status = (byte)FamilyNetPincodeStatus.Init,
                    IsLock = false,
                    IsVerified = false,
                    ViewCount = 0,
                    ModifyDate = DateTime.Now,
                    OrderGuid = orderGuid,
                    CouponId = couponId,
                    PincodeTransId = (int)transId,
                    IsVerifying = false,
                    OrderTicket = ticketNo,
                    ReissuePin = false,
                    BarcodeReget = false
                };

                using (var db = new FamiportDbContext())
                {
                    db.FamilyNetPincodeEntities.Add(pincode);
                    db.SaveChanges();
                }
                ts.Complete();
                return pincode;
            }
        }

        public bool UpdateFamilyNetPincode(FamilyNetPincode entity)
        {
            using (var db = new FamiportDbContext())
            {
                entity.ModifyDate = DateTime.Now;
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        public List<FamilyNetPincode> GetFamilyNetPincodeRetryData(int perday, int retryLimit, int timespan)
        {
            DateTime checkPreDay = DateTime.Now.AddDays(-perday);
            DateTime checkTimespan = DateTime.Now.AddSeconds(-timespan);
            string replyCode = ((int) FamilyNetReplyCode.Code00).ToString().PadLeft(2, '0');
            List<string> excludeCode = new List<string> { ((int)FamilyNetReplyCode.Code12).ToString().PadLeft(2, '0') };

            using (var db = new FamiportDbContext())
            {
                var result = (from p in db.FamilyNetPincodeEntities
                    where p.OrderDate > checkPreDay &&
                          p.FailRetry < retryLimit &&
                          p.ModifyDate < checkTimespan &&
                          (p.ReplyCode == null ||
                           p.ReplyCode != replyCode) && !excludeCode.Contains(p.ReplyCode)
                    select p).ToList();
                return result;
            }
        }

        public List<FamilyNetPincode> GetFamilyNetPincodeRegetBarcode()
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from p in db.FamilyNetPincodeEntities
                    where p.BarcodeReget == true && p.Status == (byte)FamilyNetPincodeStatus.Completed 
                    && p.IsVerified == false && p.IsLock == false && p.IsVerifying == false 
                    && p.ReturnStatus == (byte)FamilyNetPincodeReturnStatus.Init
                    select p).ToList();
                return result;
            }
        }

        public List<FamilyNetPincode> GetFamilyNetPincodeReturning(DateTime returnTime)
        {
            using (var db = new FamiportDbContext())
            {
                var result = db.FamilyNetPincodeEntities.Where(x => x.ModifyDate < returnTime && x.ReturnStatus == (int)FamilyNetPincodeReturnStatus.Returning).ToList();
                return result;
            }
        }

        public List<FamilyNetPincode> GetFamilyNetListPincode(Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                return db.FamilyNetPincodeEntities.Where(x => x.OrderGuid == orderGuid).ToList();
            }
        }

        public int SetFamilyNetPincodeLock(Guid orderGuid, List<int> ids)
        {
            if (ids.Count == 0) return 0;

            StringBuilder sb = new StringBuilder("@cid0");
            if (ids.Count > 1)
            {
                for(int i=1; i<ids.Count; i++)
                {
                    sb.Append(",@cid" + i);
                }
            }

            var sql =
                string.Format(@"UPDATE family_net_pincode SET is_lock = 1, lock_time = getdate() WHERE 
                                    order_guid='{0}' AND coupon_id in ({1})", orderGuid.ToString(), sb.ToString());

            var parameterList = ids.Select((t, i) => new SqlParameter("@cid" + i, t)).ToList();

            using (var db = new FamiportDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, parameterList.ToArray());
            }
        }

        public int SetFamilyNetPincodeUnlock(int unlockSec)
        {
            var sql = string.Format(@"Update family_net_pincode Set is_lock = 0 WHERE is_lock = 1 
                            AND is_verifying = 0 AND DATEADD(ss, {0}, lock_time) < GETDATE()", unlockSec);

            using (var db = new FamiportDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql);
            }
        }

        public int UpdateFamilyNetPincodeViewCount(List<int> orderNoList)
        {
            if (orderNoList.Count == 0) return 0;

            StringBuilder sb = new StringBuilder("@oid0");
            if (orderNoList.Count > 1)
            {
                for (int i = 1; i < orderNoList.Count; i++)
                {
                    sb.Append(",@oid" + i);
                }
            }

            var sql =
                string.Format(@"UPDATE family_net_pincode SET view_count = view_count + 1, modify_date = getdate() WHERE 
                                    order_no in ({0})", sb.ToString());

            var parameterList = orderNoList.Select((t, i) => new SqlParameter("@oid" + i, t)).ToList();

            using (var db = new FamiportDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, parameterList.ToArray());
            }
        }

        public bool UpdateFamilyNetPincodeReturnStatus(List<FamilyNetPincode> list)
        {
            using (var db = new FamiportDbContext())
            {
                list.ForEach(x => {
                    var pincode = db.FamilyNetPincodeEntities.FirstOrDefault(y => y.OrderNo == x.OrderNo);
                    if (pincode != null)
                    {
                        pincode.ReturnStatus = x.ReturnStatus;
                        pincode.ModifyDate = DateTime.Now;
                    }
                    });
                db.SaveChanges();
            }
            return true;
        }

        public FamilyNetPincode GetFamilyNetPincodeByPezCode(string pezCode)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                                  join pez in db.PeztempEntities on p.PeztempId equals pez.Id
                                  where pez.PezCode == pezCode && pez.ServiceCode == (int)PeztempServiceCode.FamilynetPincode
                                  && p.Status == (byte)FamilyNetPincodeStatus.Completed
                                  && p.OrderGuid != Guid.Empty
                                  select p).FirstOrDefault();

                    return result;
                }
            }
        }

        #endregion FamilyNetPincode

        #region FamilyNetPincodeDetail

        public bool AddFamilyNetPincodeDetail(List<FamilyNetPincodeDetail> entities)
        {
            using (var db = new FamiportDbContext())
            {
                foreach (var entity in entities)
                {
                    var checkData = GetFamilyNetPincodeDetail(entity.PincodeOrderNo, entity.SerialNo);
                    if (checkData != null)
                    {
                        entity.Id = checkData.Id;
                        db.Entry(entity).State = EntityState.Modified;
                    }
                    else
                    {
                        db.Entry(entity).State = EntityState.Added;
                    }
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdateFamilyNetPincodeDetail(FamilyNetPincodeDetail entity)
        {
            using (var db = new FamiportDbContext())
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        public FamilyNetPincodeDetail GetFamilyNetPincodeDetail(int orderNo, int serialNo, string itemCode)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from d in db.FamilyNetPincodeDetailEntities
                        where d.PincodeOrderNo == orderNo && d.SerialNo == serialNo && d.ItemCode == itemCode
                        select d).FirstOrDefault();
                    return result;
                }
            }
        }


        public FamilyNetPincodeDetail GetFamilyNetPincodeDetail(int orderNo, int serialNo)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from d in db.FamilyNetPincodeDetailEntities
                                  where d.PincodeOrderNo == orderNo && d.SerialNo == serialNo
                                  select d).FirstOrDefault();
                    return result;
                }
            }
        }

        #endregion FamilyNetPincodeDetail

        #region FamilyNetVerifyLog

        public List<FamilyNetVerifyLog> GetFamilyNetVerifyLogRetryData(int preDays, int retryLimit, int timespan)
        {
            DateTime checkPreDay = DateTime.Now.AddDays(-preDays);
            DateTime checkTimespan = DateTime.Now.AddSeconds(timespan);

            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetVerifyLogEntities
                                    where p.Status == (int)FamilyNetVerifyLogStatus.Fail
                                        && p.CreateTime > checkPreDay
                                        && p.RetryCount < retryLimit 
                                        && p.ModifyTime < checkTimespan
                                    select p).ToList();
                    return result;
                }
            }
        }

        public FamilyNetVerifyLog AddFamilyNetVerifyLog(FamilyNetVerifyLog entity)
        {
            using (var db = new FamiportDbContext())
            {
                entity.ModifyTime = DateTime.Now;
                db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
                return entity;
            }
        }

        public bool UpdateFamilyNetVerifyLog(FamilyNetVerifyLog log)
        {
            using (var db = new FamiportDbContext())
            {
                log.ModifyTime = DateTime.Now;
                db.Entry(log).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        public bool UpdateFamilyNetVerifyLog(List<FamilyNetVerifyLog> logCol)
        {
            using (var db = new FamiportDbContext())
            {
                foreach (var log in logCol)
                {
                    log.ModifyTime = DateTime.Now;
                    db.Entry(log).State = EntityState.Modified;
                }
                
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        public FamilyNetVerifyLog GetFamilyNetVerifyLog(int id)
        {
            using (var db = new FamiportDbContext())
            {
                var log = db.FamilyNetVerifyLogEntities.Find(id);
                return log;
            }
        }

        #endregion

        #region FamilyStoreInfo


        public Dictionary<int, string> GetFamilyStoreInfoList(Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                                  join s in db.FamilyStoreInfoEntities on p.StoreCode equals s.StoreCode
                                  where p.OrderGuid == orderGuid
                                  select new
                                  {
                                      couponId = p.CouponId,
                                      storeName = s.StoreName,
                                      storeAddress = s.Address
                                  }).ToDictionary(p => p.couponId, p => p.storeName + "::" + p.storeAddress);

                    return result ?? new Dictionary<int, string>();
                }
            }
        }

        public List<FamilyStoreInfo> GetFamilyStoreInfoList()
        {
            using (var db = new FamiportDbContext())
            {
                var result = ( from d in db.FamilyStoreInfoEntities
                              select d).ToList();
                return result;
            }
        }

        public bool UpdateOrInsertFamilyStoreInfoList(List<FamilyStoreInfo> familyStoreInfoList)
        {
            using (var db = new FamiportDbContext())
            {

                foreach (var familyStoreInfo in familyStoreInfoList)
                {
                    var cols = db.FamilyStoreInfoEntities.Where(p => p.StoreCode== familyStoreInfo.StoreCode);
                    db.FamilyStoreInfoEntities.RemoveRange(cols);

                    if (familyStoreInfo.StoreCode == string.Empty) continue;
                    db.Entry(familyStoreInfo).State = familyStoreInfo.Id == 0 ?
                                             EntityState.Added :
                                             EntityState.Modified;
                    
                
                }
                return db.SaveChanges() > 0;
            }
        }

        public FamilyStoreInfo GetFamilyStoreInfoByStoreCode(string storeCode)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    return db.FamilyStoreInfoEntities.FirstOrDefault(s => s.StoreCode == storeCode);
                }
            }
        }

        #endregion

        #region FamilyNetReturnLog

        public FamilyNetReturnLog AddFamilyNetReturnLog(FamilyNetReturnLog entity)
        {
            using (var db = new FamiportDbContext())
            {

                db.Entry(entity).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
                return entity;
            }
        }

        public List<FamilyNetReturnLog> AddFamilyNetReturnLog(List<FamilyNetReturnLog> entity)
        {
            using (var db = new FamiportDbContext())
            {

                db.FamilyNetReturnLogEntities.AddRange(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool UpdateFamilyNetReturnLog(FamilyNetReturnLog entity)
        {
            using (var db = new FamiportDbContext())
            {
                db.Entry(entity).State = System.Data.Entity.EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }

        #endregion

        #region FamilyNetGetBarcodeLog

        public bool AddFamilyNetGetBarcodeLog(List<FamiBarcodeData> data, int userId, Guid orderGuid, int requestQty)
        {
            using (var db = new FamiportDbContext())
            {
                var batchId = Guid.NewGuid();
                var requestTime = DateTime.Now;

                foreach (var d in data)
                {
                    db.Entry(new FamilyNetGetBarcodeLog
                    {
                        BatchId = batchId,
                        OrderGuid = orderGuid,
                        RequestTime = requestTime,
                        RequestQty = requestQty,
                        UserId = userId,
                        OrderNo = d.OrderNo,
                        CouponId = d.CouponId,
                        Pezcode = d.PezCode,
                        Barcode1 = d.Barcode1,
                        Barcode2 = d.Barcode2,
                        Barcode3 = d.Barcode3,
                        Barcode4 = d.Barcode4                       
                    }).State = EntityState.Added;
                }

                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region FamilyNetLockLog

        public bool AddFamilyNetLockLog(List<int> couponIds, int userId, Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                var batchId = Guid.NewGuid();
                var lockTime = DateTime.Now;

                foreach (var d in couponIds)
                {
                    db.Entry(new FamilyNetLockLog
                    {
                        BatchId = batchId,
                        OrderGuid = orderGuid,
                        CouponId = d,
                        UserId = userId,
                        LockTime = lockTime
                    }).State = EntityState.Added;
                }

                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region FamilyNetOrderExgGate

        public FamilyNetOrderExgGate AddFamilyNetOrderExgGate(FamilyNetOrderExgGate entity)
        {
            entity.CreateTime = entity.ModifyTime = DateTime.Now;
            using (var db = new FamiportDbContext())
            {
                db.FamilyNetOrderExgGateEntities.Add(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public bool CheckFamilyNetOrderExgProcessing(int peztempId)
        {
            DateTime timeoutPoint = DateTime.Now.AddSeconds(-240);

            using (var db = new FamiportDbContext())
            {
                var result = (from t in db.FamilyNetOrderExgGateEntities
                              where t.PeztempId == peztempId && t.Processing && t.CreateTime > timeoutPoint
                              select t).Count();
                return result > 1;
            }
        }

        public bool SetFamilyNetOrderExgGateExpired(int peztempId)
        {
            var sql = @"UPDATE family_net_order_exg_gate SET Processing = 0, modify_time = GETDATE() WHERE
                        id in (SELECT TOP 1 id FROM family_net_order_exg_gate WHERE peztemp_id=@peztempId AND processing=1 order by id)";

            List<SqlParameter> para = new List<SqlParameter>();
            para.Add(new SqlParameter("@peztempId", peztempId));

            using (var db = new OrderDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, para.ToArray()) > 0;
            }
        }
        
        #endregion FamilyNetOrderExgGate

        #region 混合

        public FamiBarcodeData GetFamilyNetPincodeInfo(Guid orderGuid, int couponId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from fp in db.FamilyNetPincodeEntities
                                  join p in db.PeztempEntities on fp.PeztempId equals p.Id
                                  join fd in db.FamilyNetPincodeDetailEntities on fp.OrderNo equals fd.PincodeOrderNo
                                  where fp.OrderGuid == orderGuid && fp.CouponId == couponId
                                 select new FamiBarcodeData
                                 {
                                     OrderNo = fp.OrderNo,
                                     CouponId = fp.CouponId,
                                     PezCode = p.PezCode,
                                     Barcode1 = fd.Barcode1,
                                     Barcode2 = fd.Barcode2,
                                     Barcode3 = fd.Barcode3,
                                     Barcode4 = fd.Barcode4
                                 }).FirstOrDefault();
                    return result;
                }
            }
        }


        public List<FamiBarcodeData> GetFamilNetVerifyBarcodeByGift(Guid orderGuid,int cnt, List<int> couponIds)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    //濾出是禮物的
                    var result = (from fp in db.FamilyNetPincodeEntities
                                  join p in db.PeztempEntities
                                      on fp.PeztempId equals p.Id
                                  join fd in db.FamilyNetPincodeDetailEntities
                                      on fp.OrderNo equals fd.PincodeOrderNo
                                  where fp.OrderGuid == orderGuid
                                        && fp.IsLock == false && fp.IsVerified == false &&
                                        fp.Status == (int)FamilyNetPincodeStatus.Completed
                                        && couponIds.Contains(fp.CouponId)
                                  orderby fp.ViewCount ascending, fp.OrderNo ascending
                                  select new FamiBarcodeData
                                  {
                                      OrderNo = fp.OrderNo,
                                      CouponId = fp.CouponId,
                                      PezCode = p.PezCode,
                                      Barcode1 = fd.Barcode1,
                                      Barcode2 = fd.Barcode2,
                                      Barcode3 = fd.Barcode3,
                                      Barcode4 = fd.Barcode4
                                  }).Take(cnt).ToList();

                    if (result.Any())
                    {
                        //加ViewCount
                        UpdateFamilyNetPincodeViewCount(result.Select(x => x.OrderNo).ToList());
                    }

                    return result;
                }
            }
        }
     
        public List<FamiBarcodeData> GetFamilNetVerifyBarcode(Guid orderGuid, int cnt, List<int> excludeCouponId)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from fp in db.FamilyNetPincodeEntities
                        join p in db.PeztempEntities
                            on fp.PeztempId equals p.Id
                        join fd in db.FamilyNetPincodeDetailEntities
                            on fp.OrderNo equals fd.PincodeOrderNo
                        where fp.OrderGuid == orderGuid
                              && fp.IsLock == false && fp.IsVerified == false &&
                              fp.Status == (int) FamilyNetPincodeStatus.Completed
                              && !excludeCouponId.Contains(fp.CouponId)
                        orderby fp.ViewCount ascending, fp.OrderNo ascending
                        select new FamiBarcodeData
                        {
                            OrderNo = fp.OrderNo,
                            CouponId = fp.CouponId,
                            PezCode = p.PezCode,
                            Barcode1 = fd.Barcode1,
                            Barcode2 = fd.Barcode2,
                            Barcode3 = fd.Barcode3,
                            Barcode4 = fd.Barcode4
                        }).Take(cnt).ToList();

                    if (result.Any())
                    {
                        //加ViewCount
                        UpdateFamilyNetPincodeViewCount(result.Select(x => x.OrderNo).ToList());
                    }

                    return result;
                }
            }
        }

        public List<FamiUserCouponBarcodeInfo> GetUserCouponBarcodeInfoListByOrderGuid(Guid orderGuid)
        {
            var result = new List<FamiUserCouponBarcodeInfo>();
            var jsonS = new JsonSerializer();
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    result = (from p in db.FamilyNetPincodeEntities
                                  join d in db.FamilyNetPincodeDetailEntities on p.OrderNo equals d.PincodeOrderNo
                                  join t in db.PeztempEntities on p.PeztempId equals t.Id
                                  from s in db.FamilyStoreInfoEntities.Where(s=>s.StoreCode == d.StoreCode).DefaultIfEmpty() //left join
                                  where p.OrderGuid == orderGuid
                                  select new FamiUserCouponBarcodeInfo
                                  {
                                      CouponId = p.CouponId,
                                      Pincode = t.PezCode,
                                      Barcode1 = d.Barcode1,
                                      Barcode2 = d.Barcode2,
                                      Barcode3 = d.Barcode3,
                                      Barcode4 = d.Barcode4,
                                      Barcode5 = d.Barcode5,
                                      IsLock = p.IsLock,
                                      LockTime = p.LockTime,
                                      StoreCode = d.StoreCode,
                                      StoreName = s.StoreName,
                                      StoreAddress = s.Address
                                  }).ToList();
                    return result;
                }
            }
        }

        public int GetFamiCouponLockedCount(Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                                  where p.OrderGuid == orderGuid && p.IsLock == true
                                  select p.CouponId).Count();
                    return result;
                }
            }
        }

        public List<FamilyNetPincode> GetFamiCouponsByLocked(Guid orderGuid)
        {
            using (var db = new FamiportDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.FamilyNetPincodeEntities
                        where p.OrderGuid == orderGuid && p.IsLock == true
                        select p).ToList();
                    return result;
                }
            }
        }

        #endregion

        #region ViewFamilyNetVerifyRetryEntities

        public List<ViewFamilyNetVerifyRetry> GetViewFamilyNetVerifyRetry()
        {
            using (var db = new FamiportDbContext())
            {
                var result = (from p in db.ViewFamilyNetVerifyRetryEntities                              
                              select p).ToList();
                return result;
            }
        }

        List<FamiOrderExgParam> IFamiportProvider.GetFamiOrderExgParams(Guid orderGuid)
        {
             var sql = @"select fnpt.pincode PezCodeEncodes, od.order_GUID OrderGuid, c.id CouponId from peztemp p with(nolock) 
join coupon c with(nolock) on p.OrderDetailGuid = c.order_detail_id and c.code = p.PezCode
join order_detail od with(nolock) on p.OrderDetailGuid = od.GUID
join family_net_pincode_trans fnpt with(nolock) on od.order_GUID = fnpt.order_guid
where od.order_GUID = @orderGuid";

            List<SqlParameter> para = new List<SqlParameter>();
            para.Add(new SqlParameter("@orderGuid", orderGuid));

            using (var db = new FamiportDbContext())
            {
                return db.Database.SqlQuery<FamiOrderExgParam>(sql, para.ToArray()).ToList();
            }
        }

        #endregion
    }
}

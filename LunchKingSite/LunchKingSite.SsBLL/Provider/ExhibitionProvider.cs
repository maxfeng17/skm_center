﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Transactions;
using System.Web.Mvc;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL.DbContexts;
using SubSonic;

namespace LunchKingSite.SsBLL.Provider
{
    public class ExhibitionProvider : IExhibitionProvider
    {
        #region ExhibitionEvent

        public ExhibitionEvent GetExhibitionEvent(int id)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from ee in db.ExhibitionEventEntities
                               where ee.Id == id
                               select ee).FirstOrDefault() ?? new ExhibitionEvent();
                }
            }
        }

        public bool SetExhibitionEvent(ExhibitionEvent evt)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(evt).State = evt.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依館代號取得策展資料清單
        /// </summary>
        /// <param name="guid">館代號</param>
        /// <returns>策展資料清單</returns>
        public List<ExhibitionEvent> GetExhibitionEventsBySellerGuid(Guid guid)
        {
            using (var db = new ExhibitionDbContext())
            {
                List<ExhibitionEvent> result = db.ExhibitionEventEntities
                    .Join(db.ExhibitionStoreEntities, exhibitionEvent => exhibitionEvent.Id, exhibitionStore => exhibitionStore.EventId, (exhibitionEvent, exhibitionStore) => new { exhibitionEvent, exhibitionStore })
                    .Where(item => item.exhibitionStore.StoreGuid == guid)
                    .Select(item => item.exhibitionEvent).ToList();
                
                return result;
            }
        }

        /// <summary>
        /// 依館代號取得可顯示的策展資料清單
        /// </summary>
        /// <param name="guid">館代號</param>
        /// <returns>可顯示的策展資料清單</returns>
        public List<ExhibitionEvent> GetPublishExhibitionEventsBySellerGuid(Guid guid)
        {
            using (var db = new ExhibitionDbContext())
            {
                List<ExhibitionEvent> result = db.ExhibitionEventEntities
                    .Join(db.ExhibitionStoreEntities, exhibitionEvent => exhibitionEvent.Id, exhibitionStore => exhibitionStore.EventId, (exhibitionEvent, exhibitionStore) => new { exhibitionEvent, exhibitionStore })
                    .Where(item => item.exhibitionStore.StoreGuid == guid && 
                                   item.exhibitionEvent.Status == (byte)SkmExhibitionEventStatus.Published &&
                                   item.exhibitionEvent.EndDate > DateTime.Now)
                    .Select(item => item.exhibitionEvent).ToList();

                return result;
            }
        }
        /// <summary>
        /// 設定策展活動排序
        /// </summary>
        /// <param name="eventInfo">策展活動排序資料</param>
        /// <returns>是否成功</returns>
        public bool SetExhibitionEventStatus(Dictionary<int, int> eventSortInfo)
        {
            using (var db = new ExhibitionDbContext())
            {
                IEnumerable<ExhibitionEvent> exhibitionEvents = db.ExhibitionEventEntities
                    .Where(item => eventSortInfo.Keys.Contains(item.Id));
                foreach (ExhibitionEvent exhibitionEvent in exhibitionEvents)
                {
                    exhibitionEvent.Seq = eventSortInfo[exhibitionEvent.Id];
                }

                return db.SaveChanges() > 0;
            }
        }
        #endregion ExhibitionEvent

        #region ExhibitionCategory

        public List<ExhibitionCategory> GetExhibitionCategoryList(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from ec in db.ExhibitionCategoryEntities
                            where ec.EventId == eventId
                            orderby ec.Seq
                            select ec).ToList();
                }
            }
        }

        public ExhibitionCategory GetExhibitionCategory(int id)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from ec in db.ExhibitionCategoryEntities
                        where ec.Id == id
                        select ec).FirstOrDefault();
                }
            }
        }

        public bool SetExhibitionCategory(ExhibitionCategory exCate)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(exCate).State = exCate.Id == 0
                    ? System.Data.Entity.EntityState.Added
                    : System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool DelExhibitionCategory(int id)
        {
            using (var db = new ExhibitionDbContext())
            {
                var c = db.ExhibitionCategoryEntities.FirstOrDefault(x => x.Id == id);
                if (c == null || c.Id == 0) return false;
                db.ExhibitionCategoryEntities.Remove(c);
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依策展編號刪除策展分類資訊
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        public bool DelExhibitionCategoryByEventID(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var delCategorys = (from s in db.ExhibitionCategoryEntities
                                    where s.EventId == eventId
                                    select s).ToList();
                if (delCategorys == null || delCategorys.Count() <= 0) return false;

                db.ExhibitionCategoryEntities.RemoveRange(delCategorys);
                return db.SaveChanges() > 0;
            }
        }

        #endregion ExhibitionCategory

        #region ExhibitionDeal

        public List<ExhibitionDeal> GetExhibitionDeal(Guid externalGuid)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionDealEntities
                        where d.ExternalDealGuid == externalGuid
                        select d).ToList();
                }
            }
        }

        public ExhibitionDeal GetExhibitionDeal(int categoryId, Guid externalGuid)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionDealEntities
                        where d.CategoryId == categoryId
                              && d.ExternalDealGuid == externalGuid
                        select d).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// 依策展代號取得策展檔次資訊清單
        /// </summary>
        /// <param name="eventID"></param>
        /// <returns>策展檔次資訊清單</returns>
        public List<ExhibitionDeal> GetExhibitionDealListByEventId(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from ds in db.ExhibitionDealEntities
                            where ds.EventId == eventId
                            select ds).ToList();
                }
            }
        }

        public List<ExhibitionDeal> GetExhibitionDealList(int categoryId)
        {
            using (var db = new ExhibitionDbContext())
            {
                return (from ds in db.ExhibitionDealEntities
                        where ds.CategoryId == categoryId
                        select ds).ToList();
            }
        }

        public bool SetExhibitionDeal(ExhibitionDeal deal)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(deal).State = deal.Id == 0
                    ? System.Data.Entity.EntityState.Added
                    : System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool DelExhibitionDeal(int exDealId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var target = db.ExhibitionDealEntities.FirstOrDefault(x => x.Id == exDealId);

                if (target == null) return false;
                db.ExhibitionDealEntities.Remove(target);
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依策展編號刪除策展分類檔次資訊
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        public bool DelExhibitionDealByEventId(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var target = db.ExhibitionDealEntities.Where(x => x.EventId == eventId);

                if (target == null || target.Count() <= 0) return false;
                db.ExhibitionDealEntities.RemoveRange(target);
                return db.SaveChanges() > 0;
            }
        }

        public int GetExhibitionDealCount(int exhibitionEventId, Guid sellerGuid)
        {
            using (var db = new ExhibitionDbContext())
            {
                string sql = @"select count(1) from exhibition_deal ex with(nolock)
                                inner join external_deal ed with(nolock)
                                on ex.external_deal_guid = ed.Guid
                                inner join external_deal_relation_store rs with(nolock)
                                on ex.external_deal_guid = rs.deal_guid
                                inner join business_hour bh on rs.bid = bh.GUID
                                where ex.event_id = @exhId and (rs.seller_guid = @sellerGuid Or parent_seller_guid = @sellerGuid)
                                and bid is not null and status in (4,5)
                                and bh.business_hour_order_time_s <= getdate()
                                and bh.business_hour_order_time_e > getdate()";

                var dealCount = db.Database.SqlQuery<int>(sql
                    , new SqlParameter("@exhId", exhibitionEventId)
                    , new SqlParameter("@sellerGuid", sellerGuid)).FirstOrDefault();
                return dealCount;
            }
        }

        #endregion ExhibitionDeal

        #region ExhibitionStore

        public List<ExhibitionStore> GetExhibitionStore(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var stores = (from s in db.ExhibitionStoreEntities
                    where s.EventId == eventId
                    select s).ToList();
                return stores;
            }
        }

        public bool SetExhibitionStore(int eventId, List<ExhibitionStore> stores)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.ExhibitionStoreEntities.AddRange(stores);
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依策展編號刪除策展適用分店
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        public bool DelExhibitionStoreByEventId(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var delStores = (from s in db.ExhibitionStoreEntities
                                 where s.EventId == eventId
                                 select s).ToList();
                if (delStores == null || delStores.Count() <= 0) return false;

                db.ExhibitionStoreEntities.RemoveRange(delStores);
                return db.SaveChanges() > 0;
            }
        }

        #endregion ExhibitionStore

        #region ExhibitionLog

        public bool SetExhibitionLog(ExhibitionLog log)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(log).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        #endregion ExhibitionLog

        #region ExhibitionCode

        public ExhibitionCode GetExhibitionCode(string code)
        {
            long queryId;
            if(!long.TryParse(code, out queryId))
            {
                return null;
            }
        
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from ec in db.ExhibitionCodeEntities
                        where ec.Id == queryId
                            select ec).FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// 依seller guid 清單取得策展代號清單
        /// </summary>
        /// <param name="list">seller guid 清單</param>
        /// <returns>策展代號清單</returns>
        public List<ExhibitionCode> GetExhibitionCodeList(List<string> sellerGuids)
        {
            using (var db = new ExhibitionDbContext())
            {
                List<ExhibitionCode> result = db.ExhibitionCodeEntities.Where(item => sellerGuids.Contains(item.SellerGuid.ToString())).ToList();
                return result;
            }
        }
        /// <summary>
        /// 新增策展代號
        /// </summary>
        /// <param name="request">策展代號資料</param>
        /// <returns>是否新增成功</returns>
        public bool InsertExhibitionCode(ExhibitionCode exhibitionCode)
        {
            using (var db = new ExhibitionDbContext())
            {
                long newId = long.Parse(DateTime.Now.ToString("yyyyMMdd001"));
                var test = db.ExhibitionCodeEntities.Where(item => item.Id >= newId);
                long? todayMaxIdInDB = test.Max<ExhibitionCode, long?>(item => item.Id);
                if (todayMaxIdInDB.HasValue)
                {
                    newId = todayMaxIdInDB.Value + 1;
                }
                exhibitionCode.Id = newId;
                exhibitionCode.CreateTime = DateTime.Now;
                db.Entry(exhibitionCode).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 編輯策展代號
        /// </summary>
        /// <param name="request">策展代號資料</param>
        /// <returns>是否編輯成功</returns>
        public bool SetExhibitionCode(ExhibitionCode exhibitionCode)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(exhibitionCode).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 刪除策展資料
        /// </summary>
        /// <param name="exhibitionCode">策展代號資料</param>
        /// <returns>是否刪除成功</returns>
        public bool DeleteExhibitionCode(ExhibitionCode exhibitionCode)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(exhibitionCode).State = System.Data.Entity.EntityState.Deleted;
                return db.SaveChanges() > 0;
            }
        }
        #endregion

        #region ExhibitionDealTimeSlot

        public ExhibitionDealTimeSlot GetExhibitionDealTimeSlotById(int id)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionDealTimeSlotEntities
                        where d.Id == id
                        select d).FirstOrDefault();
                }
            }
        }

        public bool UpdateExhibitionDealTimeSlot(List<ExhibitionDealTimeSlot> data)
        {
            using (var db = new ExhibitionDbContext())
            {
                foreach (var d in data)
                {
                    db.Entry(d).State = System.Data.Entity.EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public List<ExhibitionDealTimeSlot> GetExhibitionDealTimeSlot(DateTime beginDate, Guid sellerGuid, int exhibitionId, int categoryId)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    var data = (from d in db.ExhibitionDealTimeSlotEntities
                        where d.ExhibitionId == exhibitionId && d.EffectiveDate == beginDate 
                                                             && d.SellerGuid == sellerGuid
                                                            && d.ExhibitionCategoryId == categoryId
                        orderby d.Sequence
                        select d).ToList();
                    return data;
                }
            }
        }

        public bool SaveExhibitionDealTimeSlot(Guid sellerGuid, int exhibitionEventId, int categoryId, Guid externalGuid, DateTime start, DateTime end)
        {
            start = start.SetTime(0, 0, 0);
            end = end.SetTime(0, 0, 0);

            using (var db = new ExhibitionDbContext())
            {
                var sql = @"DELETE FROM exhibition_deal_time_slot WHERE seller_guid=@sellerGuid
                                AND exhibition_id=@exhId AND exhibition_category_id=@catId AND external_guid=@extGuid";

                db.Database.ExecuteSqlCommand(sql
                    , new SqlParameter("@sellerGuid", sellerGuid)
                    , new SqlParameter("@exhId", exhibitionEventId)
                    , new SqlParameter("@catId", categoryId)
                    , new SqlParameter("@extGuid", externalGuid));

                for (var s = start; s <= end; s = s.AddDays(1))
                {
                    db.Entry(new ExhibitionDealTimeSlot
                    {
                        SellerGuid = sellerGuid,
                        ExhibitionId = exhibitionEventId,
                        ExhibitionCategoryId = categoryId,
                        ExternalGuid = externalGuid,
                        EffectiveDate = s,
                        IsHidden = false,
                        Sequence = 99999
                    }).State = System.Data.Entity.EntityState.Added;
                }
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依策展編號刪除策展商品排序資料
        /// </summary>
        /// <param name="exhibitionEventId">策展編號</param>
        /// <returns>是否刪除成功</returns>
        public bool DelExhibitionDealTimeSlotUnreferencedCategor(int exhibitionEventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                IEnumerable<int> categoryIds = db.ExhibitionCategoryEntities.Where(item => item.EventId == exhibitionEventId).Select(item => item.Id);
                IEnumerable<ExhibitionDealTimeSlot> exhibitionDealTimeSlots = db.ExhibitionDealTimeSlotEntities
                    .Where(item => item.ExhibitionId == exhibitionEventId && categoryIds.All(categoryId => categoryId != item.ExhibitionCategoryId));
                if (exhibitionDealTimeSlots.Any())
                {
                    db.ExhibitionDealTimeSlotEntities.RemoveRange(exhibitionDealTimeSlots);
                    return db.SaveChanges() > 0;
                }
                else
                {
                    return true;
                }
            }
        }

        #endregion ExhibitionDealTimeSlot

        #region ViewExternalDealRelationStoreExhibitionCode
        /// <summary>
        /// 依策展代號等訊息取得檔次資訊清單
        /// </summary>
        /// <param name="exhibitionCodeId">策展代號</param>
        /// <param name="startDate">檔次上架時間</param>
        /// <param name="endDate">檔次下架時間</param>
        /// <param name="storeGuids">館別代號清單</param>
        /// <returns>檔次資訊清單</returns>
        public List<ViewExternalDealRelationStoreExhibitionCode> GetExternalDealByExhibitionCode(long exhibitionCodeId, DateTime startDate, DateTime endDate, List<string> storeGuids)
        {
            using (var db = new ExhibitionDbContext())
            {
                var result = db.ViewExternalDealRelationStoreExhibitionCodeEntities
                               .Where(item => item.ExhibitionCodeId == exhibitionCodeId
                                    && ((item.StartDate < startDate && item.EndDate > startDate) ||
                                        (item.StartDate >= startDate && item.StartDate <= endDate))
                                    //&& item.EndDate > endDate
                                    && storeGuids.Contains(item.StoreGuid.ToString())
                                );
                return result.ToList();
            }
        }
        #endregion

        #region ExhibitionCardType
        /// <summary>
        /// 依策展編號儲存推薦卡別(先刪除再新增)
        /// </summary>
        /// <param name="exhibitionEventId">策展編號</param>
        /// <param name="recommendCreditCardLevels"></param>
        /// <returns>是否儲存成功</returns>
        public bool SetExhibitionCardType(int exhibitionEventId, SkmCardLevelType[] recommendCreditCardLevels)
        {
            using (var db = new ExhibitionDbContext())
            {
                IEnumerable<ExhibitionCardType> exhibitionCardTypes = recommendCreditCardLevels.Select(item =>
                {
                    return new ExhibitionCardType
                    {
                        ExhibitionEventId = exhibitionEventId,
                        CardType = (byte)item
                    };
                 });
                db.ExhibitionCardTypeEntities.RemoveRange(db.ExhibitionCardTypeEntities.Where(item =>item.ExhibitionEventId == exhibitionEventId));
                db.SaveChanges();

                db.ExhibitionCardTypeEntities.AddRange(exhibitionCardTypes);
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 依策展編號取得推薦卡別清單
        /// </summary>
        /// <param name="eventId">策展編號</param>
        /// <returns>推薦卡別清單</returns>
        public List<ExhibitionCardType> GetExhibitionCardTypesByEventId(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                List<ExhibitionCardType> result = db.ExhibitionCardTypeEntities.Where(item => item.ExhibitionEventId == eventId).ToList();
                return result;
            }
        }
        #endregion

        #region ViewExhibitionDealExternalDeal
        /// <summary>
        /// 依策展編號取得檔次基本資訊清單
        /// </summary>
        /// <param name="exhibitionEventId">策展編號</param>
        /// <returns>檔次基本資訊清單</returns>
        public List<ViewExhibitionDealExternalDeal> GetExternalDealsSimpleInfoByExhibitionEventID(int exhibitionEventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                List<ViewExhibitionDealExternalDeal> result = db.ViewExhibitionDealExternalDealEntities.Where(item => item.ExhibitionEventId == exhibitionEventId).ToList();
                return result;
            }
        }
        #endregion

        #region ViewExhibitionEvent

        public List<ViewExhibitionEvent> GetViewExhibitionEvent(Guid sellerGuid, SkmCardLevelType cardLevel, SkmExhibitionEventStatus status)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    DateTime today = DateTime.Now;
                    return db.ViewExhibitionEvents
                        .Where(x => x.StoreGuid == sellerGuid
                                    && x.CardType == (byte) cardLevel
                                    && x.StartDate <= today
                                    && x.EndDate > today
                                    && x.Status == (byte) status).ToList();
                }
            }
        }

        public List<ViewExhibitionEvent> GetViewExhibitionEvent(Guid sellerGuid, List<SkmCardLevelType> cardLevel, SkmExhibitionEventStatus status)
        {
            List<byte> cardTypeCol = Array.ConvertAll(cardLevel.ToArray(), value => (byte) value).ToList();
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    DateTime today = DateTime.Now;
                    var data = db.ViewExhibitionEvents
                        .Where(x => x.StoreGuid == sellerGuid
                                    && x.StartDate <= today
                                    && x.EndDate > today
                                    && x.Status == (byte)status
                                    && cardTypeCol.Contains(x.CardType)).ToList();
                    return data;
                }
            }
        }

        public ViewExhibitionEvent GetViewExhibitionEvent(Guid sellerGuid, int exhibitionEventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return db.ViewExhibitionEvents.FirstOrDefault(x => x.StoreGuid == sellerGuid
                                                                       && x.ExhibitionEventId == exhibitionEventId
                                                                       && x.Status == (byte) SkmExhibitionEventStatus
                                                                           .Published);
                }
            }
        }

        #endregion ViewExhibitionEvent

        #region ExhibitionActivity

        public List<ExhibitionActivity> GetExhibitionActivities(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionActivities
                            where d.ExhibitionEventId == eventId
                            orderby d.Seq
                        select d).ToList();
                }
            }
        }

        /// <summary>
        /// 設定策展多重活動資訊清單
        /// </summary>
        /// <param name="exhibitionActivities">策展多重活動資訊清單</param>
        /// <returns>是否設定成功</returns>
        public bool SetExhibitionActivity(IEnumerable<ExhibitionActivity> exhibitionActivities)
        {

            using (var db = new ExhibitionDbContext())
            {
                IEnumerable<int> exhibitionEventIds = exhibitionActivities.Select(item =>
                {
                    return item.ExhibitionEventId;
                }).Distinct();
                db.ExhibitionActivities.RemoveRange(db.ExhibitionActivities.Where(item => exhibitionEventIds.Contains(item.ExhibitionEventId)));
                db.SaveChanges();

                db.ExhibitionActivities.AddRange(exhibitionActivities);
                return db.SaveChanges() > 0;
            }
        }

        public int GetExhibitionActivityCount(int exhibitionEventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                string sql = @"select count(1) from exhibition_activity ex with(nolock)
                                where ex.exhibition_event_id = @exhId";

                var dealCount = db.Database.SqlQuery<int>(sql
                    , new SqlParameter("@exhId", exhibitionEventId)).FirstOrDefault();
                return dealCount;
            }
        }

        #endregion ExhibitionActivity

        #region ExhibitionUserGroup 

        public bool DelExhibitionUserGroup(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var delUserGroup = db.ExhibitionUserGroupEntities.FirstOrDefault(p => p.Id == eventId);
                if (delUserGroup != null)
                {
                    db.ExhibitionUserGroupEntities.Remove(delUserGroup);
                    return db.SaveChanges() > 0;
                }

                return false;
            }
        }

        public bool AddExhibitionUserGroup(ExhibitionUserGroup userGroup)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.ExhibitionUserGroupEntities.Add(userGroup);
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdateExhibitionUserGroup(ExhibitionUserGroup userGroup)
        {
            using (var db = new ExhibitionDbContext())
            {
                db.Entry(userGroup).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public ExhibitionUserGroup GetExhibitionUserGroup(int id)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return db.ExhibitionUserGroupEntities.FirstOrDefault(p => p.Id == id);

                }
            }
        }

        public List<SelectListItem> GetExhibitionUserGroupList(int eventId, List<string> sellerGuids, DateTime eventSd, DateTime eventEd)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    var cols = (from d in db.ExhibitionUserGroupEntities
                               where sellerGuids.Contains(d.SellerGuid.ToString())
                               orderby d.EndDate descending
                               select d).ToList();

                    if (eventSd != DateTime.MinValue)
                    {
                        cols = cols.Where(p => p.StartDate <= eventSd).ToList();
                    }

                    if (eventEd != DateTime.MinValue)
                    {
                        cols = cols.Where(p => p.EndDate >= eventEd).ToList();
                    }

                    return cols.Select(d => new SelectListItem()
                    {
                        Text = d.Name,
                        Value = d.Id.ToString()
                    }).ToList();
                }
            }
        }

        public List<ExhibitionUserGroup> GetExhibitionUserGroupList(string sellerGuid, out int dataCnt, int page = 1, int pageSize = 10)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    int skipCnt = (page - 1) * pageSize;
                    Guid seller = Guid.Parse(sellerGuid);
                    var cols = db.ExhibitionUserGroupEntities.Where(p => p.SellerGuid == seller);
                    dataCnt = cols.Count();
                    return cols.OrderByDescending(p => p.EndDate).ThenBy(p=>p.Id).Skip(skipCnt).Take(pageSize).ToList();
                }
            }
        }
        
        #endregion

        #region ExhibitionUserGroupMapping

        public bool DelExhibitionUserGroupMapping(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var delUserGroups = (from s in db.ExhibitionUserGroupMappingEntities
                    where s.ExhibitionEventId == eventId
                    select s).ToList();
                if (delUserGroups == null || delUserGroups.Count() <= 0) return false;

                db.ExhibitionUserGroupMappingEntities.RemoveRange(delUserGroups);
                return db.SaveChanges() > 0;
            }
        }

        public bool AddExhibitionUserGroupMapping(int eventId, List<string> userGroups)
        {
            using (var db = new ExhibitionDbContext())
            {
                var list = new List<ExhibitionUserGroupMapping>();
                foreach (var ug in userGroups)
                {
                    list.Add(new ExhibitionUserGroupMapping()
                    {
                        ExhibitionEventId = eventId,
                        ExhibitionUserGroupId = Convert.ToInt32(ug)
                    });
                }

                if (list.Any())
                {
                    db.ExhibitionUserGroupMappingEntities.AddRange(list);
                    return db.SaveChanges() > 0;
                }

                return false;
            }
        }

        public List<ExhibitionUserGroupMapping> GetExhibitionUserGroupMappings()
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionUserGroupMappingEntities
                        select d).ToList();
                }
            }
        }

        public List<int> GetHaveUserGroupExhibitionEventIds(List<int> eventIds)
        {
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    return (from d in db.ExhibitionUserGroupMappingEntities
                            where eventIds.Contains(d.ExhibitionEventId)
                            select d.ExhibitionEventId).ToList();
                }
            }
        }

        #endregion

        #region ExhibitionUserGroupMemberToken

        public bool BulkInsertExhibitionUserGroupMemberToken(List<ExhibitionUserGroupMemberToken> list, string dbName = "Default")
        {
            bool result = false;
            DataTable table = new DataTable();
            table.Columns.Add("id", typeof(int));
            table.Columns.Add("exhibition_user_group_id", typeof(int));
            table.Columns.Add("skm_member_token", typeof(string));

            foreach (var d in list)
            {
                DataRow dr = table.NewRow();
                //dr["id"] = 0;
                dr["exhibition_user_group_id"] = d.ExhibitionUserGroupId;
                dr["skm_member_token"] = d.SkmMemberToken;
                table.Rows.Add(dr);
            }

            TransactionOptions options = new TransactionOptions();
            options.IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted;
            using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew, options))
            {
                using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConfigurationManager.ConnectionStrings[dbName].ConnectionString, SqlBulkCopyOptions.CheckConstraints))
                {
                    bulkCopy.DestinationTableName = "dbo.exhibition_user_group_member_token";
                    bulkCopy.BulkCopyTimeout = 600;

                    try
                    {
                        bulkCopy.WriteToServer(table);
                        result = true;
                    }
                    catch (Exception ex)
                    {
                        result = false;
                    }
                }
                scope.Complete();
            }
            return result;
        }

        public void DelExhibitionUserGroupMemberToken(int eventId)
        {
            using (var db = new ExhibitionDbContext())
            {
                var delTokens = (from s in db.ExhibitionUserGroupMemberTokenEntities
                                 where s.ExhibitionUserGroupId == eventId
                                 select s).ToList();
                if (delTokens != null && delTokens.Any())
                {
                    db.ExhibitionUserGroupMemberTokenEntities.RemoveRange(delTokens);
                    db.SaveChanges();
                }
            }
        }

        #endregion

        #region ViewExhibitionEventUserGroup

        /// <summary>
        /// 綁定客群的策展
        /// </summary>
        /// <returns></returns>
        public List<ViewExhibitionEventUserGroup> GetViewExhibitionEventHaveUserGroupList(Guid sellerGuid, List<SkmCardLevelType> cardLevels, string memberToken)
        {
            List<byte> cardTypeCol = Array.ConvertAll(cardLevels.ToArray(), value => (byte)value).ToList();
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    DateTime today = DateTime.Now;
                    return db.ViewExhibitionEventUserGroupEntities
                        .Where(x => x.StoreGuid == sellerGuid
                                    && cardTypeCol.Contains(x.CardType)
                                    && x.StartDate <= today
                                    && x.EndDate > today
                                    && x.Status == (byte)SkmExhibitionEventStatus.Published
                                    && x.SkmMemberToken == memberToken
                        ).ToList();
                }
            }
        }

        /// <summary>
        /// 沒綁客群的策展
        /// </summary>
        /// <returns></returns>
        public List<ViewExhibitionEventUserGroup> GetViewExhibitionEventNoUserGroupList(Guid sellerGuid,
            List<SkmCardLevelType> cardLevels)
        {
            List<byte> cardTypeCol = Array.ConvertAll(cardLevels.ToArray(), value => (byte)value).ToList();
            using (var db = new ExhibitionDbContext())
            {
                using (db.NoLock())
                {
                    DateTime today = DateTime.Now;
                    return db.ViewExhibitionEventUserGroupEntities
                        .Where(x => x.StoreGuid == sellerGuid
                                    && cardTypeCol.Contains(x.CardType)
                                    && x.StartDate <= today
                                    && x.EndDate > today
                                    && x.Status == (byte)SkmExhibitionEventStatus.Published
                                    && x.ExhibitionUserGroupId == null
                        ).ToList();
                }
            }
        }

        

        #endregion

    }
}
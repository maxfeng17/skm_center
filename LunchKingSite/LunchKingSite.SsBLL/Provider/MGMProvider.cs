﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Linq;
using LunchKingSite.Core.Enumeration;
using System.Collections.Generic;
using LunchKingSite.Core.Component;

namespace LunchKingSite.SsBLL.Provider
{
    public class MGMProvider : IMGMProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public MemberFastInfo MemberFastInfoGetByUserId(int userid)
        {
            var data = DB.Get<MemberFastInfo>(MemberFastInfo.Columns.UserId, userid);
            return data;
        }

        public MgmGiftCollection GiftGetByAccessCode(string accessCode)
        {
            var data = DB.SelectAllColumnsFrom<MgmGift>().Where(MgmGift.Columns.AccessCode).IsEqualTo(accessCode).ExecuteAsCollection<MgmGiftCollection>();
            return data;
        }

        public int SendGiftCountGetByOrderGuid(Guid orderGuid)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmActiveCoupon>().Where(ViewMgmActiveCoupon.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewMgmActiveCouponCollection>().Count;
            return data;
        }

        public ViewMgmActiveCouponCollection ActiveGiftGetByOrderGuid(Guid orderGuid)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmActiveCoupon>().Where(ViewMgmActiveCoupon.Columns.OrderGuid).IsEqualTo(orderGuid).ExecuteAsCollection<ViewMgmActiveCouponCollection>();
            return data;
        }

        public MemberMessage GiftCardGetById(int msgId)
        {
            var data = DB.Get<MemberMessage>(MemberMessage.Columns.Id, msgId);
            return data;
        }
        public MemberMessage GiftMsgByAccessCode(string accessCode)
        {
            var data = DB.Get<MemberMessage>(MemberMessage.Columns.Message, accessCode);
            return data;
        }
        public MemberMessage ReplyMsgByMsgId(int msgId)
        {
            var data = DB.Get<MemberMessage>(MemberMessage.Columns.ReferMessageId, msgId);
            return data;
        }

        public MemberMessageCollection GiftCardListGetByUserId(int userId)
        {
            var data = DB.SelectAllColumnsFrom<MemberMessage>().Where(MemberMessage.Columns.ReceiverId).IsEqualTo(userId).Or(MemberMessage.Columns.SenderId).IsEqualTo(userId);
            return data.ExecuteAsCollection<MemberMessageCollection>();
        }

        public MemberMessageCollection GiftCardListGetBySenderId(int senderId)
        {
            var data = DB.SelectAllColumnsFrom<MemberMessage>().Where(MemberMessage.Columns.SenderId).IsEqualTo(senderId)
                .And(MemberMessage.Columns.MessageType).IsEqualTo(1); // type=mgm_send
            return data.ExecuteAsCollection<MemberMessageCollection>();
        }

        public MemberMessageCollection GiftAccessGetByUserId(int userId)
        {
            var data = DB.SelectAllColumnsFrom<MemberMessage>().Where(MemberMessage.Columns.ReceiverId).IsEqualTo(userId)
                .And(MemberMessage.Columns.MessageType).IsEqualTo((int)GiftMessageType.GiftCode)
                .And(MemberMessage.Columns.IsRead).IsEqualTo(0);
            return data.ExecuteAsCollection<MemberMessageCollection>();
        }

        public MemberMessageTemplate GiftCardTemplateGetById(int id)
        {
            var data = DB.Get<MemberMessageTemplate>(MemberMessageTemplate.Columns.Id, id);
            return data;
        }

        public MgmGiftVersion GiftVersionGetByGiftiId(int giftiId)
        {
            var data = DB.Get<MgmGiftVersion>(MgmGiftVersion.Columns.MgmGiftId, giftiId);
            return data;
        }

        public MgmGiftVersionCollection GiftVersionGetByMsgId(int msgId)
        {
            var data = DB.SelectAllColumnsFrom<MgmGiftVersion>().Where(MgmGiftVersion.Columns.SendMessageId)
                .IsEqualTo(msgId).ExecuteAsCollection<MgmGiftVersionCollection>();
            return data;
        }

        public ViewMgmGiftCollection GiftViewGetByAccessCode(string accessCode)
        {
            return DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.AccessCode).IsEqualTo(accessCode)
                     .ExecuteAsCollection<ViewMgmGiftCollection>();
        }

        public ViewMgmGift GiftGetByGiftId(int giftId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.Id).IsEqualTo(giftId).OrderDesc(ViewMgmGift.Columns.SendTime);
            return data.ExecuteSingle<ViewMgmGift>();
        }

        public ViewMgmGiftCollection GiftGetByReceiverIdAndSellerGuid(int userId, Guid sellerGuid)
        {
            string sql = "select * from " + ViewMgmGift.Schema.TableName + " with(nolock) " +
                         " where " + ViewMgmGift.Columns.ReceiverId + "=@userId " +
                         " and " + ViewMgmGift.Columns.SellerGuid + "=@sellerGuid " +
                         " and " + ViewMgmGift.Columns.Accept + "=" + (int)MgmAcceptGiftType.Accept +
                         " and (" + ViewMgmGift.Columns.BusinessHourDeliverTimeE + ">getdate() or " + ViewMgmGift.Columns.ChangedExpireDate + "> getdate())";

            QueryCommand qc = new QueryCommand(sql, ViewMgmGift.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);

            ViewMgmGiftCollection col = new ViewMgmGiftCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public ViewMgmGiftCollection GiftGetByReceiverId(int userId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.ReceiverId).IsEqualTo(userId).OrderDesc(ViewMgmGift.Columns.SendTime);
            return data.ExecuteAsCollection<ViewMgmGiftCollection>();
        }

        public ViewMgmGiftCollection GiftGetBySenderId(int userId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.SenderId).IsEqualTo(userId).OrderDesc(ViewMgmGift.Columns.SendTime);
            return data.ExecuteAsCollection<ViewMgmGiftCollection>();
        }

        public ViewMgmGiftCollection GiftGetByOid(Guid oid)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().NoLock().Where(ViewMgmGift.Columns.OrderGuid).IsEqualTo(oid);
            return data.ExecuteAsCollection<ViewMgmGiftCollection>();
        }
        public ViewMgmGiftCollection GiftGetByBidForSendMail(Guid bid)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.Bid).IsEqualTo(bid)
                       .And(ViewMgmGift.Columns.Accept).IsEqualTo(1).And(ViewMgmGift.Columns.IsUsed).IsEqualTo(0)
                       .Or(ViewMgmGift.Columns.Bid).IsEqualTo(bid).And(ViewMgmGift.Columns.Accept).IsEqualTo(6).And(ViewMgmGift.Columns.IsUsed).IsEqualTo(0);
            return data.ExecuteAsCollection<ViewMgmGiftCollection>();
        }

        public ViewMgmGift GiftGetByCouponId(int couponId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.CouponId).IsEqualTo(couponId)
                .And(ViewMgmGift.Columns.IsActive).IsEqualTo(true).ExecuteSingle<ViewMgmGift>();
            return data;
        }

        public ViewMgmGift GiftGetByCouponIdAndAsCode(int couponId,string asCode)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.CouponId).IsEqualTo(couponId)
                .And(ViewMgmGift.Columns.AccessCode).IsEqualTo(asCode).ExecuteSingle<ViewMgmGift>();
            return data;
        }

        public ViewMgmGiftCollection GiftViweGetByAsCode(string asCode, int userId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.AccessCode).IsEqualTo(asCode)
                .And(ViewMgmGift.Columns.ReceiverId).IsEqualTo(userId).And(ViewMgmGift.Columns.Accept).IsEqualTo(1).ExecuteAsCollection<ViewMgmGiftCollection>();
            return data;
        }

        public DataTable GetViewMgmGiftByStoreGuidAndBusinessHourSendGift(Guid bid, Guid storeGuid)
        {
            string strSql = @"Select dbo.mgm_gift_version.quick_info,dbo.order_detail.item_name,dbo.cash_trust_log.order_guid,count(dbo.cash_trust_log.order_guid) as count
                              From dbo.mgm_gift INNER JOIN dbo.mgm_gift_version ON dbo.mgm_gift.id = dbo.mgm_gift_version.mgm_gift_id 
                                                INNER JOIN dbo.cash_trust_log ON dbo.mgm_gift.trust_id = dbo.cash_trust_log.trust_id
                                                INNER JOIN dbo.order_detail ON dbo.cash_trust_log.order_guid = dbo.order_detail.order_GUID
                              Where is_used=0 and accept=6
                              And bid='"+ bid + "'";

            //為了測試加的時間條件
            strSql += " AND send_time>='2017/03/21'";

            if (storeGuid != Guid.Empty)
            {
                strSql += " AND dbo.order_detail.store_guid = '" + storeGuid + "'";
            }
            strSql += " Group By dbo.mgm_gift.bid,dbo.mgm_gift_version.quick_info,dbo.order_detail.item_name,dbo.cash_trust_log.order_guid";

            IDataReader idr = new InlineQuery().ExecuteReader(strSql);
            DataTable dt = new DataTable();

            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public List<int?> GiftViweGetByAsCodeWithNoUserId(string asCode)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.AccessCode).IsEqualTo(asCode)
                .ExecuteAsCollection<ViewMgmGiftCollection>().Select(p=>p.CouponId).ToList();
            return data;
        }

        public bool GetMGMGiftUsed(int couponId, string asCode)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>().Where(ViewMgmGift.Columns.AccessCode).IsEqualTo(asCode)
                       .And(ViewMgmGift.Columns.CouponId).IsEqualTo(couponId).ExecuteAsCollection<ViewMgmGiftCollection>().Select(p => p.IsUsed).FirstOrDefault();
            return data;
        }


        public ViewMgmGiftCollection GetUnacceptedExpiredGiftList(DateTime nowTime)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>()
                         .Where(ViewMgmGift.Columns.Accept).IsEqualTo(MgmAcceptGiftType.Unknown)
                         .And(ViewMgmGift.Columns.IsActive).IsEqualTo(1)
                         .And(ViewMgmGift.Columns.SendTime).IsLessThan(nowTime.AddDays(-config.MGMGiftLockDay))//執行當下時間往前推Lock天數
                         .ExecuteAsCollection<ViewMgmGiftCollection>();
            return data;
        }

        public ViewMgmGift GetActiveGiftByCouponId(Guid orderGuid, int couponId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMgmGift>()
                         .Where(ViewMgmGift.Columns.OrderGuid).IsEqualTo(orderGuid)
                         .And(ViewMgmGift.Columns.CouponId).IsEqualTo(couponId)
                         .And(ViewMgmGift.Columns.IsActive).IsEqualTo(1)
                         .And(ViewMgmGift.Columns.IsUsed).IsEqualTo(false)
                        .ExecuteSingle<ViewMgmGift>();
            return data;
        }

        public MemberMessageTemplateCollection MessageTemplateGet()
        {
            return DB.SelectAllColumnsFrom<MemberMessageTemplate>()
                .ExecuteAsCollection<MemberMessageTemplateCollection>();
        }

        public bool MgmGiftSet(MgmGift mgmGift)
        {
            return DB.Save(mgmGift) > 0;
        }

        public bool MemberMessageSet(MemberMessage msg)
        {
            return DB.Save(msg) > 0;
        }

        public bool MgmGiftMatchSet(MgmGiftMatch mgmGiftMatch)
        {
            return DB.Save(mgmGiftMatch) > 0;
        }

        public bool MgmGiftVersionSet(MgmGiftVersion mgmGiftVersion)
        {
            return DB.Save(mgmGiftVersion) > 0;
        }

        public bool MgmGiftVersionSet(MgmGiftVersionCollection collection)
        {
            return DB.SaveAll(collection) > 0;
        }

        public bool ReturnUnacceptedExpiredGiftByNowTime(DateTime nowTime)
        {
            return DB.Update<MgmGiftVersion>().Set(MgmGiftVersion.Columns.IsActive).EqualTo(false)
                     .Set(MgmGiftVersion.Columns.Accept).EqualTo((int)MgmAcceptGiftType.Expired)
                     .Where(MgmGiftVersion.Columns.Accept).IsEqualTo(MgmAcceptGiftType.Unknown)
                     .And(MgmGiftVersion.Columns.IsActive).IsEqualTo(1)
                     .And(MgmGiftVersion.Columns.SendTime).IsLessThan(nowTime.AddDays(-config.MGMGiftLockDay))
                     .Execute() > 0;
        }

        public int MgmGiftMatchIdGet(string matchValue)
        {
            return DB.Select().From<MgmGiftMatch>()
                .Where(MgmGiftMatch.Columns.MatchValue).IsEqualTo(matchValue)
                .ExecuteSingle<MgmGiftMatch>().Id;
        }

        public MemberMessage MemberMessageGetbyId(int id)
        {
            return DB.Select().From<MemberMessage>()
                .Where(MemberMessage.Columns.Id).IsEqualTo(id)
                .ExecuteSingle<MemberMessage>();
        }


        public ViewMgmGift GiftGetByGetByMatchId(int matchId)
        {
            return DB.Select().From<ViewMgmGift>()
                .Where(ViewMgmGift.Columns.MgmMatchId).IsEqualTo(matchId)
                .ExecuteSingle<ViewMgmGift>();
        }

        public int MatchIdGetByContainsRequestId(string requestId)
        {
            return DB.Select().From<MgmGiftMatch>()
                .Where(MgmGiftMatch.Columns.MatchValue).ContainsString(requestId)
                .ExecuteSingle<MgmGiftMatch>().Id;
        }

        public MgmGift GiftGetById(int giftId)
        {
            return DB.Select().From<MgmGift>()
                .Where(MgmGift.Columns.Id).IsEqualTo(giftId)
                .ExecuteSingle<MgmGift>();
        }

        public MgmGiftMatch GiftMatchGetById(int id)
        {
            return DB.Select().From<MgmGiftMatch>()
                .Where(MgmGiftMatch.Columns.Id).IsEqualTo(id)
                .ExecuteSingle<MgmGiftMatch>();
        }

        public bool MgmGiftExpiredLogSet(MgmGiftExpiredLog log)
        {
            return DB.Save(log) > 0;
        }

        public bool MgmGiftExpiredLogSet(MgmGiftExpiredLogCollection logs)
        {
            return DB.SaveAll(logs) > 0;
        }


        public bool MgmGiftHeaderSet(MgmGiftHeader mgmGiftHeader)
        {
            return DB.Save(mgmGiftHeader) > 0;
        }

        public MgmGiftHeaderCollection MgmGiftHeaderCollectionGet()
        {
            return DB.SelectAllColumnsFrom<MgmGiftHeader>()
                     .Where(MgmGiftHeader.Columns.OnLine).IsEqualTo(true) //目前應該只會有兩筆是線上
                     .ExecuteAsCollection<MgmGiftHeaderCollection>()
                     .OrderByAsc(MgmGiftHeader.Columns.ShowType);
        }

        public ViewMgmGiftBacklistCollection ViewMgmGiftBacklistGetByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewMgmGiftBacklist, ViewMgmGiftBacklistCollection>(page, pageSize, orderBy, false, filter);
        }

        public ViewMgmGiftCollection ViewMgmGiftGetAll(string sequenceNumber)
        {
            return DB.Select().From<ViewMgmGift>()
                .Where(ViewMgmGift.Columns.SequenceNumber).IsEqualTo(sequenceNumber)
                .ExecuteAsCollection<ViewMgmGiftCollection>();
        }

        public int ViewMgmGiftGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<ViewMgmGiftBacklist>(filter);
            qc.CommandSql = "select count(1) from " + ViewMgmGiftBacklist.Schema.Provider.DelimitDbName(ViewMgmGiftBacklist.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public MgmGiftHeaderCollection MgmGiftHeaderGetByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<MgmGiftHeader, MgmGiftHeaderCollection>(page, pageSize, orderBy, filter);
        }

        public int MgmGiftHeaderGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<MgmGiftHeader>(filter);
            qc.CommandSql = "select count(1) from " + MgmGiftHeader.Schema.Provider.DelimitDbName(MgmGiftHeader.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public MgmGiftHeader MgmGiftHeaderGet(int id)
        {
            return DB.SelectAllColumnsFrom<MgmGiftHeader>()
                .Where(MgmGiftHeader.Columns.Id).IsEqualTo(id).ExecuteSingle<MgmGiftHeader>();
        }

        public bool MgmGiftHeaderDelete(int id)
        {
            DB.Destroy<MgmGiftHeader>(MgmGiftHeader.Columns.Id, id);
            return true;
        }

        public MgmGiftHeader MgmGiftHeaderByShowType(int showType)
        {
            return DB.SelectAllColumnsFrom<MgmGiftHeader>()
                .Where(MgmGiftHeader.Columns.ShowType).IsEqualTo(showType).And(MgmGiftHeader.Columns.OnLine).IsEqualTo(true).ExecuteSingle<MgmGiftHeader>();
        }

        public int MgmNewGiftCountGetByReceiverId(int userId)
        {
            var qry = new Select(MgmGift.Columns.AccessCode).From(MgmGift.Schema)
                .InnerJoin(MgmGiftVersion.MgmGiftIdColumn, MgmGift.IdColumn)
                .Where(MgmGiftVersion.Columns.ReceiverId).IsEqualTo(userId)
                .And(MgmGiftVersion.Columns.Accept).IsEqualTo((int) MgmAcceptGiftType.Unknown)
                .Distinct().GetRecordCount();
          
            return qry;
        }

        //public bool MgmGiftVersionUpdateIdByTrustId(Guid trustId, Guid storeGuid)
        //{
        //    return DB.Update<CashTrustLog>().Set(CashTrustLog.Columns.VerifiedStoreGuid).EqualTo(storeGuid)
        //        .Where(CashTrustLog.Columns.TrustId).IsEqualTo(trustId).Execute() > 0;
        //}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL.DbContexts;
using SubSonic;

namespace LunchKingSite.SsBLL
{
    public class SSVerificationProvider : IVerificationProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private const string TimeFormat = @"{0:yyyy/MM/dd HH:mm}";
        static ILog logger = LogManager.GetLogger(typeof(SSVerificationProvider));
        #region cash_trust_log

        public CashTrustLogCollection GetCashTrustLogGetListByBidAndSeller(Guid bid, Guid sellerGuid)
        {
            const string sql = @"SELECT * " +
                               @"FROM cash_trust_log WITH(NOLOCK) " +
                               @"WHERE coupon_id in ( " +
                               @"	SELECT coupon_id " +
                               @"	FROM view_ppon_coupon WITH(NOLOCK) " +
                               @"	WHERE business_hour_guid = @businessHourGuid " +
                               @"     AND seller_guid = @sellerGuid " +
                               @"		AND coupon_id IS NOT NULL " +
                               @")";
            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@businessHourGuid", bid, DbType.Guid);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            var data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection GetCashTrustLogGetListByBidAndSellerAndStore(Guid bid, Guid sellerGuid, Guid storeGuid)
        {
            const string sql = @"SELECT * " +
                               @"FROM cash_trust_log WITH(NOLOCK) " +
                               @"WHERE coupon_id in ( " +
                               @"	SELECT coupon_id " +
                               @"	FROM view_ppon_coupon WITH(NOLOCK) " +
                               @"	WHERE business_hour_guid = @businessHourGuid " +
                               @"     AND seller_guid = @sellerGuid " +
                               @"     AND store_guid = @storeGuid " +
                               @"		AND coupon_id IS NOT NULL " +
                               @")";
            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@businessHourGuid", bid, DbType.Guid);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            var data = new CashTrustLogCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CashTrustLogCollection CashTrustLogGetListByStore(List<Guid> stores)
        {
            SqlQuery query = DB.Select().From(CashTrustLog.Schema)
                .Where(CashTrustLog.Columns.Status).IsEqualTo((int)TrustStatus.Verified)
                .And(CashTrustLog.Columns.UsageVerifiedTime).IsNotNull()
                .And(CashTrustLog.Columns.StoreGuid).In(stores);

            return query.ExecuteAsCollection<CashTrustLogCollection>();
        }

        public List<VendorDealSalesCount> GetPponVerificationSummary(IEnumerable<Guid> bids)
        {

            string sql = @"SELECT
ctl.business_hour_guid,
COUNT(CASE
		WHEN ctl.status in (0, 1)
		THEN 1
	END) AS unverified,
COUNT(	CASE
		WHEN (ctl.status = 2 AND ctl.special_status & 1 = 0)
		THEN 1
	END) AS verified,
COUNT(	CASE
		WHEN ctl.status in (3, 4) AND ctl.special_status & 1 = 0
		THEN 1
	END) AS returned,
COUNT(	CASE 
		WHEN ctl.modify_time < bh.business_hour_order_time_e AND ctl.status in (3, 4) AND ctl.special_status & 1 = 0
		THEN 1 
	END) AS before_deal_end_returned,
COUNT(	CASE
		WHEN (ctl.status = 2 AND ctl.special_status & 1 = 1)
		THEN 1
	END) AS Force_verified,
COUNT(	CASE
		WHEN ctl.status in (3, 4) AND ctl.special_status & 1 = 1
		THEN 1
	END) AS Force_returned
FROM cash_trust_log AS ctl WITH(NOLOCK)
INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
INNER JOIN [order] AS o WITH(NOLOCK) ON o.GUID = ctl.order_guid AND o.order_status & 8 > 0
WHERE ctl.business_hour_guid in ('" + string.Join("','", bids) + @"')
AND ctl.special_status & 16 = 0 
GROUP BY ctl.business_hour_guid";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        UnverifiedCount = dr.GetInt32(1),
                        VerifiedCount = dr.GetInt32(2),
                        ReturnedCount = dr.GetInt32(3),
                        BeforeDealEndReturnedCount = dr.GetInt32(4),
                        ForceVerifyCount = dr.GetInt32(5),
                        ForceReturnCount = dr.GetInt32(6)
                    };

                    result.Add(deal);
                }
            }
            catch (Exception ex)
            {
                logger.Error("GetPponVerificationSummary error, sql: " + sql, ex);
                throw new Exception("GetPponVerificationSummary error, sql: " + sql, ex);
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<VendorDealSalesCount> GetPponVerificationSummaryFromArchive(List<Guid> bids)
        {
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            using (var db = new PponDbContext())
            {
                var items = db.DealArchiveVerificationSummaryDbSet.Where(t => bids.Contains(t.Bid)).ToList();                
                foreach (var item in items)
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = item.Bid,
                        UnverifiedCount = item.Unverified,
                        VerifiedCount = item.Verified,
                        ReturnedCount =item.Returned,
                        BeforeDealEndReturnedCount =item.BeforeDealEndReturned,
                        ForceVerifyCount = item.ForceVerified,
                        ForceReturnCount = item.ForceReturned
                    };

                    result.Add(deal);
                }
            }            
            return result;
        }

        public List<VendorDealSalesCount> GetPponVerificationSummaryGroupByStore(IEnumerable<Guid> bids)
        {
            string sql = @"SELECT
ctl.business_hour_guid,
COALESCE(ctl.verified_store_guid, ctl.store_guid) as store_guid,
COUNT(CASE
		WHEN ctl.status in (0, 1)
		THEN 1
	END) AS unverified,
COUNT(	CASE
		WHEN (ctl.special_status & 1 = 1 OR ctl.status = 2)
		THEN 1
	END) AS verified,
COUNT(	CASE
		WHEN ctl.status in (3, 4) AND ctl.special_status & 1 = 0
		THEN 1
	END) AS returned,
COUNT(	CASE 
		WHEN ctl.modify_time < bh.business_hour_order_time_e AND ctl.status in (3, 4) AND ctl.special_status & 1 = 0
		THEN 1 
	END) AS before_deal_end_returned
FROM cash_trust_log AS ctl WITH(NOLOCK)
INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
INNER JOIN [order] AS o WITH(NOLOCK) ON o.GUID = ctl.order_guid AND o.order_status & 8 > 0
WHERE ctl.business_hour_guid in ('" + string.Join("','", bids) + @"')
AND ctl.special_status & 16 = 0 
GROUP BY ctl.business_hour_guid, COALESCE(ctl.verified_store_guid, ctl.store_guid)";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        UnverifiedCount = dr.GetInt32(2),
                        VerifiedCount = dr.GetInt32(3),
                        ReturnedCount = dr.GetInt32(4),
                        BeforeDealEndReturnedCount = dr.GetInt32(5)
                    };
                    if (!dr.IsDBNull(1))
                    {
                        deal.StoreGuid = dr.GetGuid(1);
                    }

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<VendorDealSalesCount> GetPponToHouseDealSummary(IEnumerable<Guid> bids)
        {
            string sql = @"SELECT
ctl.business_hour_guid,
COUNT(CASE
		WHEN ctl.status in (0, 1)
		THEN 1
	END) AS unverified,
COUNT(	CASE
		WHEN ctl.status = 2
		THEN 1
	END) AS verified,
COUNT(	CASE
		WHEN ctl.status in (3, 4)
		THEN 1
	END) AS returned,
COUNT(	CASE
		WHEN ctl.modify_time < bh.business_hour_order_time_e AND ctl.status in (3, 4)
		THEN 1
	END) AS before_deal_end_returned
FROM cash_trust_log AS ctl WITH(NOLOCK)
INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
INNER JOIN [order] AS o WITH(NOLOCK) ON o.GUID = ctl.order_guid AND o.order_status & 8 > 0
WHERE ctl.business_hour_guid in ('" + string.Join("','", bids) + @"')
AND ctl.special_status & 16 = 0 
AND ctl.order_classification = 1 
GROUP BY ctl.business_hour_guid";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        UnverifiedCount = dr.GetInt32(1),
                        VerifiedCount = dr.GetInt32(2),
                        ReturnedCount = dr.GetInt32(3),
                        BeforeDealEndReturnedCount = dr.GetInt32(4)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        #endregion cash_trust_log

        #region ViewVbsCashTrustLogStatusLogInfo

        public ViewVbsCashTrustLogStatusLogInfoCollection GetCashTrustLogGetListByProductGuidAndStore(Guid productGuid, OrderClassification orderClassification, 
            string couponNumber, VbsStoreFilter storeFilter, string storeGuidList)
        {
            SqlQuery query = DB.Select().From(ViewVbsCashTrustLogStatusLogInfo.Schema)
                .Where(ViewVbsCashTrustLogStatusLogInfo.Columns.ProductGuid).IsEqualTo(productGuid)
                .And(ViewVbsCashTrustLogStatusLogInfo.Columns.OrderClassification).IsEqualTo((int)orderClassification)
                .And(ViewVbsCashTrustLogStatusLogInfo.Columns.CouponSequenceNumber).IsNotNull();

            if (string.IsNullOrEmpty(couponNumber) == false)
            {
                query.And(ViewVbsCashTrustLogStatusLogInfo.Columns.CouponSequenceNumber).IsEqualTo(couponNumber);
            }
            if (storeFilter == VbsStoreFilter.HeaderOffice)
            {
                query.And(ViewVbsCashTrustLogStatusLogInfo.Columns.StoreGuid).IsNull();
            }
            else if (storeFilter == VbsStoreFilter.BranchStore)
            {
                var storeGuIds = (from t in storeGuidList.Split(',') select Guid.Parse(t)).ToArray();
                query.And(ViewVbsCashTrustLogStatusLogInfo.Columns.StoreGuid).In(storeGuIds);
            }
            else if (storeFilter == VbsStoreFilter.BranchStoreAndHeaderOffice)
            {
                var storeGuIds = (from t in storeGuidList.Split(',') select Guid.Parse(t)).ToArray();
                query.AndExpression(ViewVbsCashTrustLogStatusLogInfo.Columns.StoreGuid).IsNull()
                    .Or(ViewVbsCashTrustLogStatusLogInfo.Columns.StoreGuid).In(storeGuIds);
            }
            return query.ExecuteAsCollection<ViewVbsCashTrustLogStatusLogInfoCollection>();
        }

        public ViewVbsCashTrustLogStatusLogInfoCollection GetCashTrustLogGetListByProductGuid(Guid productGuid, OrderClassification orderClassification)
        {
            SqlQuery query = DB.Select().From(ViewVbsCashTrustLogStatusLogInfo.Schema).NoLock()
                .Where(ViewVbsCashTrustLogStatusLogInfo.Columns.ProductGuid).IsEqualTo(productGuid)
                .And(ViewVbsCashTrustLogStatusLogInfo.Columns.OrderClassification).IsEqualTo((int)orderClassification);

            return query.ExecuteAsCollection<ViewVbsCashTrustLogStatusLogInfoCollection>();
        }

        public ViewVbsCashTrustLogStatusLogInfo ViewVbsCashTrustLogStatusLogInfoGet(Guid trustId)
        {
            string sql = "select top 1 * from " + ViewVbsCashTrustLogStatusLogInfo.Schema.TableName +
                        @" where " + ViewVbsCashTrustLogStatusLogInfo.Columns.TrustId + " = @trustId ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@trustId", trustId, DbType.Guid);

            var result = new ViewVbsCashTrustLogStatusLogInfo();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion ViewVbsCashTrustLogStatusLogInfo

        #region store_acct_info

        public bool HaveLegalAcctInfoByAppInfo(string strAcct, string strPwd, string strImei, string strImsi, string strRegCode)
        {
            string acctCode = strAcct.Substring(0, 4);
            string acctNo = strAcct.Substring(4);

            var saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.AcctCodeColumn).IsEqualTo(acctCode)
                .And(StoreAcctInfo.AcctNoColumn).IsEqualTo(acctNo)
                .And(StoreAcctInfo.AcctPwColumn).IsEqualTo(strPwd)
                .And(StoreAcctInfo.ImeiCodeColumn).ContainsString(strImei)
                .And(StoreAcctInfo.SimCodeColumn).ContainsString(strImsi)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            return (saic.Count > 0);
        }
        public bool RegistAcctInfo(StoreAcctInfo sai)
        {
            bool flag = true;
            try
            {
                DB.Update<StoreAcctInfo>().Set(StoreAcctInfo.ImeiCodeColumn).EqualTo(sai.ImeiCode)
                .Set(StoreAcctInfo.SimCodeColumn).EqualTo(sai.SimCode)
                .Set(StoreAcctInfo.StatusColumn).EqualTo((int)VerificationAccountStatusDetail.Regist)
                .Where(StoreAcctInfo.IdColumn).IsEqualTo(sai.Id)
                .Execute();
            }
            catch
            {
                flag = false;
            }

            return flag;
        }

        public bool ResetRegistAcctInfo(StoreAcctInfo sai, string strNewRegCode)
        {
            bool flag = true;
            try
            {
                DB.Update<StoreAcctInfo>()
                    .Set(StoreAcctInfo.SimCodeColumn).EqualTo(sai.SimCode)
                    .Set(StoreAcctInfo.ImeiCodeColumn).EqualTo(sai.ImeiCode)
                    .Set(StoreAcctInfo.StatusColumn).EqualTo((int)VerificationAccountStatusDetail.Regist)
                    .Set(StoreAcctInfo.RegisterCodeColumn).EqualTo(sai.NewRegisterCode)
                    .Set(StoreAcctInfo.NewRegisterCodeColumn).EqualTo(strNewRegCode)
                    .Where(StoreAcctInfo.IdColumn).IsEqualTo(sai.Id)
                    .Execute();

            }
            catch
            {
                flag = false;
            }

            return flag;
        }

        public bool UndoRegistAcctInfo(StoreAcctInfo sai, string strRegCode1, string strRegCode2)
        {
            bool flag = true;
            try
            {
                DB.Update<StoreAcctInfo>()
                .Set(StoreAcctInfo.SimCodeColumn).EqualTo(sai.SimCode)
                .Set(StoreAcctInfo.ImeiCodeColumn).EqualTo(sai.ImeiCode)
                .Set(StoreAcctInfo.StatusColumn).EqualTo((int)VerificationAccountStatusDetail.Invalid)
                .Set(StoreAcctInfo.RegisterCodeColumn).EqualTo(strRegCode1)
                .Set(StoreAcctInfo.NewRegisterCodeColumn).EqualTo(strRegCode2)
                .Where(StoreAcctInfo.IdColumn).IsEqualTo(sai.Id)
                .Execute();
            }
            catch
            {
                flag = false;
            }

            return flag;
        }

        public StoreAcctInfo GetAcctInfoByAcct(string strAcct, string strRegCode)
        {
            var acctCode = strAcct.Substring(0, 4);
            var acctNo = strAcct.Substring(4);
            var sai = new StoreAcctInfo();
            var saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.AcctCodeColumn).IsEqualTo(acctCode)
                .And(StoreAcctInfo.AcctNoColumn).IsEqualTo(acctNo)
                .And(StoreAcctInfo.RegisterCodeColumn).IsEqualTo(strRegCode)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            if (saic.Count > 0)
            {
                foreach (StoreAcctInfo t in saic.Where(t => t.AcctCode == acctCode && t.AcctNo == acctNo))
                {
                    sai = t;
                }
            }
            return sai;
        }

        public StoreAcctInfo GetAcctInfoByAcctWithReRegist(string strAcct, string strOldRegCode, string strNewRegCode, string strImei, string strImsi)
        {
            var acctCode = strAcct.Substring(0, 4);
            var acctNo = strAcct.Substring(4);
            var sai = new StoreAcctInfo();
            var saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.AcctCodeColumn).IsEqualTo(acctCode)
                .And(StoreAcctInfo.AcctNoColumn).IsEqualTo(acctNo)
                .And(StoreAcctInfo.RegisterCodeColumn).IsEqualTo(strOldRegCode)
                .And(StoreAcctInfo.NewRegisterCodeColumn).IsEqualTo(strNewRegCode)
                .And(StoreAcctInfo.ImeiCodeColumn).IsEqualTo(strImei)
                .And(StoreAcctInfo.SimCodeColumn).IsEqualTo(strImsi)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            if (saic.Count > 0)
            {
                foreach (StoreAcctInfo t in saic.Where(t => t.AcctCode == acctCode && t.AcctNo == acctNo))
                {
                    sai = t;
                }
            }
            return sai;
        }

        public StoreAcctInfo GetAcctInfoByAcctWithUndoRegist(string strAcct, string strRegCode, string strImei, string strImsi)
        {
            string acctCode = strAcct.Substring(0, 4);
            string acctNo = strAcct.Substring(4);
            var sai = new StoreAcctInfo();
            var saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.AcctCodeColumn).IsEqualTo(acctCode)
                .And(StoreAcctInfo.AcctNoColumn).IsEqualTo(acctNo)
                .And(StoreAcctInfo.RegisterCodeColumn).IsEqualTo(strRegCode)
                .And(StoreAcctInfo.ImeiCodeColumn).ContainsString(strImei)
                .And(StoreAcctInfo.SimCodeColumn).ContainsString(strImsi)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            if (saic.Count > 0)
            {
                foreach (StoreAcctInfo t in saic.Where(t => t.AcctCode == acctCode && t.AcctNo == acctNo))
                {
                    sai = t;
                }
            }
            return sai;
        }

        public StoreAcctInfo GetAcctInfoByAcct(string strAcct, string strPwd, string strImei, string strImsi)
        {
            string acctCode = strAcct.Substring(0, 4);
            string acctNo = strAcct.Substring(4);
            var sai = new StoreAcctInfo();
            var saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.AcctCodeColumn).IsEqualTo(acctCode)
                .And(StoreAcctInfo.AcctNoColumn).IsEqualTo(acctNo)
                .And(StoreAcctInfo.AcctPwColumn).IsEqualTo(strPwd)
                .And(StoreAcctInfo.ImeiCodeColumn).ContainsString(strImei)
                .And(StoreAcctInfo.SimCodeColumn).ContainsString(strImsi)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            if (saic.Count > 0)
            {
                foreach (StoreAcctInfo t in saic.Where(t => t.AcctCode == acctCode && t.AcctNo == acctNo && t.AcctPw == strPwd))
                {
                    sai = t;
                    break;
                }
            }
            return sai;
        }

        #endregion
        #region store_acct_info_log

        public bool SetStoreAcctInfoLog(StoreAcctInfoLog sail)
        {
            DB.Save(sail);
            return true;
        }

        #endregion
        #region store_acct_special_info

        public StoreAcctSpecialInfo GetStoreAcctSepcialInfoByAcct(string strAcct, string strPwd)
        {
            var sasic = DB.Select().From(StoreAcctSpecialInfo.Schema)
                .Where(StoreAcctSpecialInfo.Columns.Acct).IsEqualTo(strAcct)
                .And(StoreAcctSpecialInfo.Columns.AcctPw).IsEqualTo(strPwd)
                .ExecuteAsCollection<StoreAcctSpecialInfoCollection>();
            return sasic.Count > 0 ? sasic[0] : new StoreAcctSpecialInfo();

        }

        #endregion
        #region view_ppon_coupon

        public ViewPponCouponCollection GetCouponInfoBySNoAndCodeAndSeller(string strSequence, string strCode, Guid sellerGuid)
        {
            const string sql = @" SELECT * " + "\n" +
                               @" FROM view_ppon_coupon WITH(NOLOCK) " + "\n" +
                               @" WHERE sequence_number LIKE @strSequence AND coupon_code = @strCode " + "\n" +
                               @" AND seller_guid = @sellerGuid " + "\n" +
                               @" AND DATEADD(DAY, -1, GETDATE()) BETWEEN DATEADD(DAY, -1, business_hour_deliver_time_s) AND DATEADD(MONTH, 1, business_hour_deliver_time_e) ";
            var qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@strSequence", "%" + strSequence, DbType.String);
            qc.AddParameter("@strCode", strCode, DbType.String);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            var data = new ViewPponCouponCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewPponCouponCollection GetCouponInfoBySNoAndCodeAndSellerAndStoreGuid(string strSequence, string strCode, Guid sellerGuid, Guid storeGuid)
        {
            const string sql = @" SELECT * " + "\n" +
                               @" FROM view_ppon_coupon WITH(NOLOCK) " + "\n" +
                               @" WHERE sequence_number LIKE @strSequence AND coupon_code = @strCode " + "\n" +
                               @" AND seller_guid = @sellerGuid " + "\n" +
                               @" AND store_guid = @storeGuid " + "\n" +
                               @" AND DATEADD(DAY, -1, GETDATE()) BETWEEN DATEADD(DAY, -1, business_hour_deliver_time_s) AND DATEADD(MONTH, 1, business_hour_deliver_time_e) ";
            var qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@strSequence", "%" + strSequence, DbType.String);
            qc.AddParameter("@strCode", strCode, DbType.String);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            var data = new ViewPponCouponCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion
        #region verification_log
        
		public bool UndoVerificationLog(int couponId, OrderClassification classification = OrderClassification.LkSite)
		{
			try
			{
				VerificationLog undoLog = GetVerificationLogByCouponIdAndStatus(couponId, (int)VerificationStatus.CouponOk, classification);
				undoLog.MarkOld();
				undoLog.UndoFlag = (int)UndoVerificationFlag.Undo;
				undoLog.UndoTime = DateTime.Now;

				return DB.Save(undoLog) == 1;
			}
			catch
			{
				return false;
			}
		}

		public VerificationLog GetVerificationLogByCouponIdAndStatus(int? intCouponId, int intStatus, OrderClassification classification = OrderClassification.LkSite)
		{
			var vlc = DB.Select().From(VerificationLog.Schema)
				.Where(VerificationLog.Columns.CouponId).IsEqualTo(intCouponId)
				.And(VerificationLog.Columns.Status).IsEqualTo(intStatus)
				.And(VerificationLog.Columns.IsHideal).IsEqualTo(classification == OrderClassification.HiDeal)
				.OrderDesc(VerificationLog.Columns.CreateTime)
				.ExecuteAsCollection<VerificationLogCollection>();
			return vlc.Count > 0 ? vlc[0] : null;
		}

        #endregion
        #region view_seller_store_bid_menu
        public ViewSellerStoreBidMenuCollection GetDealsBySeller(Guid sellerGuid)
        {
            return DB.Select().From(ViewSellerStoreBidMenu.Schema)
                .Where(ViewSellerStoreBidMenu.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewSellerStoreBidMenu.Columns.BusinessHourDeliverTimeS).IsLessThanOrEqualTo(DateTime.Today)
                .ExecuteAsCollection<ViewSellerStoreBidMenuCollection>();
        }
        public ViewSellerStoreBidMenuCollection GetDealsBySellerAndStoreGuid(Guid sellerGuid, Guid storeGuid)
        {
            return DB.Select().From(ViewSellerStoreBidMenu.Schema)
                .Where(ViewSellerStoreBidMenu.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewSellerStoreBidMenu.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(ViewSellerStoreBidMenu.Columns.BusinessHourDeliverTimeS).IsLessThanOrEqualTo(DateTime.Today)
                .ExecuteAsCollection<ViewSellerStoreBidMenuCollection>();
        }

        #endregion

        #region VbsBulletinBoard

        public VbsBulletinBoard VbsBulletinBoardGetById(int id) 
        {
            return DB.SelectAllColumnsFrom<VbsBulletinBoard>().Where(VbsBulletinBoard.Columns.Id).IsEqualTo(id).ExecuteSingle<VbsBulletinBoard>();
        }
        public VbsBulletinBoardCollection VbsBulletinBoardGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter) 
        {
            string defOrderBy = VbsBulletinBoard.Columns.CreateTime;
            QueryCommand qc = GetVpolWhereQC(VbsBulletinBoard.Schema, filter);
            qc.CommandSql = "select * from " + VbsBulletinBoard.Schema.Provider.DelimitDbName(VbsBulletinBoard.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            VbsBulletinBoardCollection boardCol = new VbsBulletinBoardCollection();
            boardCol.LoadAndCloseReader(DataService.GetReader(qc));
            return boardCol;
        }
        public bool VbsBulletinBoardSet(VbsBulletinBoard board) 
        {
            DB.Save<VbsBulletinBoard>(board);
            return true;
        }
        public bool VbsBulletinBoardDelete(int id) 
        {
            return DB.Update<VbsBulletinBoard>().Set(VbsBulletinBoard.Columns.PublishStatus).EqualTo((int)VbsBulletinBoardPublishStatus.Deleted)
                .Where(VbsBulletinBoard.Columns.Id).IsEqualTo(id).Execute() > 0;
        }

        #endregion

        #region HiDeal
        public bool HaveHiDealProd(string strAcct, string strPwd, Guid sellerGuid)
        {
            var acctCode = strAcct.Substring(0, 4);
            var acctNo = strAcct.Substring(4);

            //先判斷此帳號是否存在
            var sai = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.Columns.AcctCode).IsEqualTo(acctCode)
                .And(StoreAcctInfo.Columns.AcctPw).IsEqualTo(strPwd)
                .ExecuteAsCollection<StoreAcctInfoCollection>();

            if (sai.Count <= 0)
            {
                return false;
            }

            //再去找此帳號是否有檔次資料
            var hda = new ViewHiDealVerificationAcct();
            var hdac = DB.Select().From(ViewHiDealVerificationAcct.Schema)
                .Where(ViewHiDealVerificationAcct.Columns.AcctCode).IsEqualTo(acctCode)
                .And(ViewHiDealVerificationAcct.Columns.AcctPw).IsEqualTo(strPwd)
                .And(ViewHiDealVerificationAcct.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Now.AddDays(1))
                .AndExpression(ViewHiDealVerificationAcct.Columns.UseEndTime).IsGreaterThan(DateTime.Now.AddMonths(-1).AddDays(-1))
                    .Or(ViewHiDealVerificationAcct.Columns.ChangedExpireTime).IsGreaterThan(DateTime.Now.AddMonths(-1).AddDays(-1)).CloseExpression()
                .ExecuteAsCollection<ViewHiDealVerificationAcctCollection>();

            if (hdac.Count <= 0 && acctNo == "00000")
            {   //因為該檔次只有設定店家有資料，上面方式用總店帳號搜尋會找不到，所以要針對總店去找資料
                hdac = DB.Select().From(ViewHiDealVerificationAcct.Schema)
                    .Where(ViewHiDealVerificationAcct.Columns.AcctCode).IsEqualTo(acctCode)
                    .And(ViewHiDealVerificationAcct.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Now.AddDays(1))
                    .AndExpression(ViewHiDealVerificationAcct.Columns.UseEndTime).IsGreaterThan(DateTime.Now.AddMonths(-1).AddDays(-1))
                        .Or(ViewHiDealVerificationAcct.Columns.ChangedExpireTime).IsGreaterThan(DateTime.Now.AddMonths(-1).AddDays(-1)).CloseExpression()
                    .ExecuteAsCollection<ViewHiDealVerificationAcctCollection>();
            }

            if (hdac.Count > 0)
            {
                foreach (var t in hdac.Where(t => t.AcctCode == acctCode && t.AcctNo == acctNo && t.AcctPw == strPwd))
                {
                    hda = t;
                    break;
                }
                //針對總店又無檔次時的檢查
                if (acctNo == "00000" && hda.IsNew)
                {
                    foreach (var t in hdac.Where(t => t.Quantity > 0 && t.SellerGuid == sellerGuid))
                    {
                        hda = t;
                        break;
                    }
                }
            }
            return !hda.IsNew;
        }
        #region view_hideal_verification_info
        public ViewHiDealVerificationInfoCollection GetHiDealCouponInfoBySNoAndCodeAndSeller(string strSequence, string strCode, Guid sellerGuid)
        {
            return DB.Select().From(ViewHiDealVerificationInfo.Schema)
                .Where(ViewHiDealVerificationInfo.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewHiDealVerificationInfo.Columns.Sequence).Like('%' + strSequence)
                .And(ViewHiDealVerificationInfo.Columns.Code).IsEqualTo(strCode)
                .And(ViewHiDealVerificationInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today)
                .OrderDesc(ViewHiDealVerificationInfo.Columns.UseEndTime)
                .ExecuteAsCollection<ViewHiDealVerificationInfoCollection>();
        }

        public ViewHiDealVerificationInfoCollection GetHiDealCouponInfoBySNoAndCodeAndSellerAndStoreGuid(string strSequence, string strCode, Guid sellerGuid, Guid storeGuid)
        {
            return DB.Select().From(ViewHiDealVerificationInfo.Schema)
                .Where(ViewHiDealVerificationInfo.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewHiDealVerificationInfo.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(ViewHiDealVerificationInfo.Columns.Sequence).Like('%' + strSequence)
                .And(ViewHiDealVerificationInfo.Columns.Code).IsEqualTo(strCode)
                .And(ViewHiDealVerificationInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today)
                .OrderDesc(ViewHiDealVerificationInfo.Columns.UseEndTime)
                .ExecuteAsCollection<ViewHiDealVerificationInfoCollection>();
        }

        public ViewHiDealVerificationInfoCollection GetHiDealsBySeller(Guid sellerGuid)
        {
            return DB.Select().From(ViewHiDealVerificationInfo.Schema)
                .Where(ViewHiDealVerificationInfo.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewHiDealVerificationInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today.AddDays(1))//合法兌換開始前一天就能使用
                .ExecuteAsCollection<ViewHiDealVerificationInfoCollection>();
        }

        public ViewHiDealVerificationInfoCollection GetHiDealsBySellerAndStoreGuid(Guid sellerGuid, Guid storeGuid)
        {
            return DB.Select().From(ViewHiDealVerificationInfo.Schema)
                .Where(ViewHiDealVerificationInfo.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(ViewHiDealVerificationInfo.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(ViewHiDealVerificationInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today)
                .ExecuteAsCollection<ViewHiDealVerificationInfoCollection>();
        }
       
        public List<VendorDealSalesCount> GetHiDealVerificationSummary(IEnumerable<int> pids)
        {
            string sql =
                @"SELECT 
vi.product_guid, 
COUNT(CASE 
	WHEN vi.status in (0, 1)  
	THEN 1 
END) AS unverified, 
COUNT(CASE 
	WHEN (vi.status = 2 AND vi.special_status & 1 = 0) 
	THEN 1 
END) AS verified,
COUNT(CASE 
	WHEN (vi.status in (3, 4) AND vi.special_status & 1 = 0)
	THEN 1 
END) AS returned ,
COUNT(CASE 
	WHEN (hdr.completed_time < vi.deal_end_time AND vi.status in (3, 4) AND vi.special_status & 1 = 0)
	THEN 1 
END) AS before_deal_end_returned , 
COUNT(CASE 
	WHEN (vi.status = 2 AND vi.special_status & 1 = 1) 
	THEN 1 
END) AS Force_verified,
COUNT(CASE 
	WHEN (vi.status in (3, 4) AND vi.special_status & 1 = 1)
	THEN 1 
END) AS Force_returned
FROM view_hi_deal_verification_info AS vi WITH(NOLOCK) 
LEFT OUTER JOIN  dbo.hi_deal_returned AS hdr WITH(NOLOCK) 
INNER JOIN hi_deal_returned_detail AS hdrd WITH(NOLOCK) 
  ON hdrd.returned_id = hdr.id 
  AND hdr.status = 2  
ON vi.order_pk = hdr.order_pk
AND vi.order_status = 3
WHERE vi.product_id in (" + string.Join(",", pids) + @")
AND vi.special_status & 16 = 0 
GROUP BY vi.product_guid";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        UnverifiedCount = dr.GetInt32(1),
                        VerifiedCount = dr.GetInt32(2),
                        ReturnedCount = dr.GetInt32(3),
                        BeforeDealEndReturnedCount = dr.GetInt32(4),
                        ForceVerifyCount = dr.GetInt32(5),
                        ForceReturnCount = dr.GetInt32(6)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<VendorDealSalesCount> GetHiDealVerificationSummaryGroupByStore(IEnumerable<int> pids)
        {
            string sql =
                @"SELECT 
vi.product_guid, 
vi.store_guid,
COUNT(CASE 
	WHEN vi.status in (0, 1)  
	THEN 1 
END) AS unverified, 
COUNT(CASE 
	WHEN (vi.special_status & 1 = 1 OR vi.status = 2) 
	THEN 1 
END) AS verified,
COUNT(CASE 
	WHEN (vi.status in (3, 4) AND vi.special_status & 1 = 0)
	THEN 1 
END) AS returned ,
COUNT(CASE 
	WHEN (hdr.completed_time < vi.deal_end_time AND vi.status in (3, 4) AND vi.special_status & 1 = 0)
	THEN 1 
END) AS before_deal_end_returned 
FROM view_hi_deal_verification_info AS vi WITH(NOLOCK) 
LEFT OUTER JOIN  dbo.hi_deal_returned AS hdr WITH(NOLOCK) 
INNER JOIN hi_deal_returned_detail AS hdrd WITH(NOLOCK) 
  ON hdrd.returned_id = hdr.id 
  AND hdr.status = 2  
ON vi.order_pk = hdr.order_pk
AND vi.order_status = 3
WHERE vi.product_id in (" + string.Join(",", pids) + @")
AND vi.special_status & 16 = 0 
GROUP BY vi.product_guid, vi.store_guid";
            
            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        StoreGuid = dr.GetGuid(1),
                        UnverifiedCount = dr.GetInt32(2),
                        VerifiedCount = dr.GetInt32(3),
                        ReturnedCount = dr.GetInt32(4),
                        BeforeDealEndReturnedCount = dr.GetInt32(5)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<VendorDealSalesCount> GetHidealToHouseDealSummary(IEnumerable<int> pids)
        {
            string sql =
                @"SELECT 
vi.product_guid, 
COUNT(CASE 
	WHEN vi.status in (0, 1)  
	THEN 1 
END) AS unverified, 
COUNT(CASE 
	WHEN vi.status = 2 
	THEN 1 
END) AS verified,
COUNT(CASE 
	WHEN vi.status in (3, 4)
	THEN 1 
END) AS returned ,
COUNT(CASE 
	WHEN (hdr.completed_time < vi.deal_end_time AND vi.status in (3, 4))
	THEN 1 
END) AS before_deal_end_returned 
FROM view_hi_deal_verification_info AS vi WITH(NOLOCK) 
LEFT OUTER JOIN  dbo.hi_deal_returned AS hdr WITH(NOLOCK) 
INNER JOIN hi_deal_returned_detail AS hdrd WITH(NOLOCK) 
  ON hdrd.returned_id = hdr.id 
  AND hdr.status = 2  
ON vi.order_pk = hdr.order_pk
AND vi.order_status = 3
WHERE vi.product_id in (" + string.Join(",", pids) + @")
AND vi.special_status & 16 = 0 
GROUP BY vi.product_guid";

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            List<VendorDealSalesCount> result = new List<VendorDealSalesCount>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    VendorDealSalesCount deal = new VendorDealSalesCount
                    {
                        MerchandiseGuid = dr.GetGuid(0),
                        UnverifiedCount = dr.GetInt32(1),
                        VerifiedCount = dr.GetInt32(2),
                        ReturnedCount = dr.GetInt32(3),
                        BeforeDealEndReturnedCount = dr.GetInt32(4)
                    };

                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }
        #endregion
        #endregion

        #region 核銷列表
        public DataTable GetVerifiedListWithPpon(bool isStore, Guid bGuid, Guid sellerGuid, Guid storeGuid,
            DateTime startTime, DateTime endTime, VerificationQueryStatus enumQueryStatus
            )
        {
            const string sql = @"
SELECT info.trust_id, info.coupon_id, info.coupon_sequence_number, 
	CASE WHEN b.seller_GUID IS NULL THEN '00000000-0000-0000-0000-000000000000' ELSE b.seller_GUID END AS seller_guid,
	CASE WHEN od.store_guid IS NULL THEN '00000000-0000-0000-0000-000000000000' ELSE od.store_guid END AS store_guid, 
	info.status, vl.create_time AS verified_time, info.modify_time
FROM (SELECT * FROM cash_trust_log WITH(NOLOCK) 
	WHERE coupon_id IS NOT NULL AND order_classification = 1 AND business_hour_guid IS NOT NULL) AS info
	LEFT OUTER JOIN ( 
		SELECT GUID, seller_GUID FROM business_hour WITH(NOLOCK)
	) AS b ON b.GUID = info.business_hour_guid
	LEFT OUTER JOIN ( 
		SELECT GUID, store_guid FROM order_detail WITH(NOLOCK) 
	) AS od ON od.GUID = info.order_detail_guid
	LEFT OUTER JOIN (
		SELECT coupon_id, create_time, undo_flag FROM verification_log WITH(NOLOCK)  
		WHERE coupon_id IS NOT NULL AND is_hideal = 0 AND status = 2
	) AS vl ON vl.coupon_id = info.coupon_id ";

            var query = GetVerifiedListQuery(isStore, sellerGuid, storeGuid, bGuid, -1, -1, startTime, endTime, enumQueryStatus);
            const string order = @" ORDER BY verified_time DESC, modify_time DESC ";
            var rdr = new InlineQuery().ExecuteReader(sql + query + order);
            var dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;

        }

        public DataTable GetVerifiedListWithPiinlife(bool isStore, int dealId, int productId, Guid sellerGuid, Guid storeGuid,
            DateTime startTime, DateTime endTime, VerificationQueryStatus enumQueryStatus)
        {
            const string sql = @"
SELECT info.trust_id, info.cid AS coupon_id, info.sequence AS coupon_sequence_number, 
	info.seller_guid, info.store_guid, info.status,
	vl.create_time AS verified_time, info.modify_time
FROM view_hi_deal_verification_info AS info
	 LEFT OUTER JOIN(
		SELECT coupon_id, create_time, undo_flag FROM verification_log WITH(NOLOCK)  
		WHERE coupon_id IS NOT NULL AND is_hideal = 1 AND status = 2
	 ) AS vl ON vl.coupon_id = info.cid  ";

            var query = GetVerifiedListQuery(isStore, sellerGuid, storeGuid, Guid.Empty, dealId, productId, startTime, endTime, enumQueryStatus);
            const string order = @" ORDER BY verified_time DESC, modify_time DESC ";
            var rdr = new InlineQuery().ExecuteReader(sql + query + order);
            var dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        private static string GetVerifiedListQuery(bool isStore, Guid sellerGuid, Guid storeGuid,
            Guid bGuid, int dealId, int productId,
            DateTime startTime, DateTime endTime, VerificationQueryStatus enumQueryStatus)
        {
            string query = " WHERE seller_guid = '" + sellerGuid.ToString() + "' ";
            query += isStore ? " AND od.store_guid = '" + storeGuid.ToString() + "' " : "";
            if (bGuid == Guid.Empty)
            {
                query += " AND deal_id = '" + dealId.ToString(CultureInfo.InvariantCulture) + "' AND product_id = '" + productId.ToString(CultureInfo.InvariantCulture) + "'";
            }
            else
            {
                query += " AND business_hour_guid = '" + bGuid.ToString() + "'";
            }
            switch (enumQueryStatus)
            {
                case VerificationQueryStatus.Init:
                    query += " AND info.status in (" + (int)TrustStatus.Initial + "," + 
                        (int)TrustStatus.Trusted + "," + (int)TrustStatus.ATM + ") ";
                    break;
                case VerificationQueryStatus.Verified:
                    if (!startTime.Equals(DateTime.MinValue))
                    {
                        query += " AND vl.create_time BETWEEN '" +
                        string.Format(TimeFormat, startTime) + "' AND '" + string.Format(TimeFormat, endTime.AddDays(1)) + "'";
                    }
                    query += " AND ((status = " + (int)TrustStatus.Verified + " AND vl.undo_flag = 0)  OR (info.status in (" + (int)TrustStatus.Refunded + "," + (int)TrustStatus.Returned + ") AND special_status & " + (int)TrustSpecialStatus.ReturnForced + " > 0))";
                    break;
                case VerificationQueryStatus.Refund:
                    //因為使用 between 語法的原故，所以結束日要再加上一天到隔天的零點零分
                    if (!startTime.Equals(DateTime.MinValue))
                    {
                        query += " AND modify_time BETWEEN '" +
                            string.Format(TimeFormat, startTime) + "' AND '" + string.Format(TimeFormat, endTime.AddDays(1)) + "'";
                    }
                    query += " AND info.status in (" + (int)TrustStatus.Refunded + "," + (int)TrustStatus.Returned + ") AND special_status & " + (int)TrustSpecialStatus.ReturnForced + " = 0 ";
                    break;
            }
            return query;
        }

        public List<VerifiedCouponData> GetVerifiedCouponDataWithPpon(string couponSequence, string couponCode, Guid trustId,bool isPezTmep=false)
        {
            var sql = @"SELECT 
                ctl.trust_id, 
                ctl.coupon_sequence_number as coupon_sequence, 
                cp.code, ctl.status, ctl.special_status, ctl.modify_time, 
                o.member_name as member_name, bh.business_hour_deliver_time_s as use_start, 
                coalesce(ps.changed_expire_date, coalesce(bh.changed_expire_date, bh.business_hour_deliver_time_e)) as use_end, 
                bh.GUID as deal_id, cec.coupon_usage as deal_name, od.item_name , ctl.store_guid,
                bh.business_hour_status
                FROM cash_trust_log AS ctl with(nolock) 
                INNER JOIN coupon AS cp with(nolock) 
                   ON cp.id = ctl.coupon_Id 
                LEFT OUTER JOIN [order_detail] as od with(nolock) on cp.order_detail_id = od.GUID 
                INNER JOIN dbo.business_hour bh with(nolock) 
                   ON ctl.business_hour_guid = bh.GUID
                INNER JOIN coupon_event_content cec with(nolock) 
                  ON bh.GUID = cec.business_hour_guid
                INNER JOIN deal_property dp with(nolock) 
                  ON dp.business_hour_guid = bh.GUID   
                INNER JOIN [order] AS o with(nolock) 
                  ON o.guid = ctl.order_guid 
				LEFT OUTER JOIN ppon_store as ps with(nolock)
				  ON ps.business_hour_guid = ctl.business_hour_guid and ps.store_guid = ctl.store_guid
                WHERE dp.delivery_type = 1
                    AND o.order_status & 8 > 0 
                    AND (bh.business_hour_order_time_e < GETDATE() 
                    OR (bh.business_hour_order_time_e >= GETDATE() 
                        AND bh.business_hour_deliver_time_s <= CONVERT(VARCHAR(10), bh.business_hour_order_time_e, 111)
                        AND bh.business_hour_deliver_time_s >= CONVERT(VARCHAR(10), bh.business_hour_order_time_s, 111))
                    )";
            if (isPezTmep)
            {
                sql += " AND ctl.coupon_sequence_number =@couponSequence AND cp.code=@couponCode";
            }
            else if (trustId != Guid.Empty)
            {
                sql += string.Format(" AND trust_id = @trustId");
            }
            else if (Regex.IsMatch(couponSequence, @"\d{4}-\d{4}"))
            {
                sql += " AND ctl.coupon_sequence_number =@couponSequence AND cp.code=@couponCode";
            }
            else
            {
                sql += " AND substring(ctl.coupon_sequence_number, len(ctl.coupon_sequence_number) - 3, 4) = @couponSequence AND cp.code = @couponCode";
            }

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            if (trustId != Guid.Empty)
            {
                qc.AddParameter("@trustId", trustId, DbType.Guid);
            }
            else
            {
                qc.AddParameter("@couponSequence", couponSequence, DbType.AnsiString);
                qc.AddParameter("@couponCode", couponCode, DbType.AnsiString);
            }

            List<VerifiedCouponData> result = new List<VerifiedCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new VerifiedCouponData
                    {
                        TrustId = dr.GetGuid(0),
                        CouponSequence = dr.GetString(1),
                        CouponCode = dr.GetString(2),
                        Status = dr.GetInt32(3),
                        SpecialStatus = dr.GetInt32(4),
                        ModifyTime = dr.GetDateTime(5),
                        MemberName = dr.GetString(6),
                        UseStartTime = dr.GetDateTime(7),
                        UseEndTime = dr.GetDateTime(8),
                        DealGuid = dr.GetGuid(9),
                        DealName = dr.GetString(10),
                        ItemName = dr.GetString(11),
                        NoRestrictedStore = Helper.IsFlagSet(dr.GetInt32(13), BusinessHourStatus.NoRestrictedStore)
                    };
                    if (!dr.IsDBNull(12))
                    {
                        deal.StoreGuid = dr.GetGuid(12);
                    }
                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public List<VerifiedCouponData> GetVerifiedCouponDataWithPiinLife(string couponSequence, string couponCode, Guid trustId, bool isPezEvent =false)
        {
            string sql =
                @"SELECT ctl.trust_id, dc.prefix + ctl.coupon_sequence_number as coupon_sequence, dc.code, ctl.status, ctl.special_status, ctl.modify_time,
                        mb.last_name + mb.first_name as member_name, vhdps.use_start_time as use_start, vhdps.use_end_time as use_end, dc.deal_id, vhdps.product_name as deal_name, vhdps.product_guid as deal_guid, ctl.store_guid
                        ,vhdps.product_name + CASE WHEN hdod.option_name1 is not null THEN  '(' + hdod.option_name1 + ')' ELSE '' END 
                                            + CASE WHEN hdod.option_name2 is not null THEN  '(' + hdod.option_name2 + ')' ELSE '' END 
                                            + CASE WHEN hdod.option_name3 is not null THEN  '(' + hdod.option_name3 + ')' ELSE '' END 
				                            + CASE WHEN hdod.option_name4 is not null THEN  '(' + hdod.option_name4 + ')' ELSE '' END 
				                            + CASE WHEN hdod.option_name5 is not null THEN  '(' + hdod.option_name5 + ')' ELSE '' END 
                                            as item_name
                        FROM (
                        SELECT id, product_id, deal_id, store_guid, order_pk, order_detail_guid, prefix, sequence, code, status, create_date, bought_date, used_date, refund_date
                        FROM hi_deal_coupon with(nolock)
                        WHERE (status > 0)
                        ) AS dc INNER JOIN
                        cash_trust_log AS ctl with(nolock) ON ctl.coupon_id = dc.id AND ctl.order_detail_guid = dc.order_detail_guid INNER JOIN
                        (SELECT use_start_time, use_end_time, product_name, product_id, product_guid 
                        FROM view_hi_deal_product_seller_store with(nolock) 
                        WHERE is_in_store = 1 GROUP BY use_start_time,use_end_time,product_name, product_id, product_guid
                        ) AS vhdps ON vhdps.product_id = dc.product_id INNER JOIN
                        member AS mb with(nolock) on mb.unique_id = ctl.user_id
                        LEFT JOIN  hi_deal_order_detail as hdod on dc.order_detail_guid =hdod.guid
                        WHERE ctl.order_classification = 2";
            if (isPezEvent)
            {
                sql += " AND ctl.coupon_sequence_number =@couponSequence AND dc.code=@couponCode";
            }
            else if (trustId != Guid.Empty)
            {
                sql += string.Format("AND trust_id=@trustId");
            }
            else if (Regex.IsMatch(couponSequence, @"\d{4}-\d{4}"))
            {
                sql += "AND ctl.coupon_sequence_number =@couponSequence AND dc.code=@couponCode";
            }
            else
            {
                sql += "AND substring(ctl.coupon_sequence_number, len(ctl.coupon_sequence_number) - 3, 4)=@couponSequence AND dc.code=@couponCode";
            }

            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            if (trustId != Guid.Empty)
            {
                qc.AddParameter("@trustId", trustId, DbType.Guid);
            }
            else
            {
                qc.AddParameter("@couponSequence", couponSequence, DbType.AnsiString);
                qc.AddParameter("@couponCode", couponCode, DbType.AnsiString);
            }
            List<VerifiedCouponData> result = new List<VerifiedCouponData>();
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    var deal = new VerifiedCouponData
                    {
                        TrustId = dr.GetGuid(0),
                        CouponSequence = dr.GetString(1),
                        CouponCode = dr.GetString(2),
                        Status = dr.GetInt32(3),
                        SpecialStatus = dr.GetInt32(4),
                        ModifyTime = dr.GetDateTime(5),
                        MemberName = dr.GetString(6),
                        UseStartTime = dr.GetDateTime(7),
                        UseEndTime = dr.GetDateTime(8),
                        DealId = dr.GetInt32(9),
                        DealName = dr.GetString(10),
                        DealGuid = dr.GetGuid(11),
                        ItemName = dr.GetString(13),
                        NoRestrictedStore = false
                    };
                    if (!dr.IsDBNull(12))
                    {
                        deal.StoreGuid = dr.GetGuid(12);
                    }
                    result.Add(deal);
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }
        
        #endregion

        #region WPF AP
        public StoreAcctInfoCollection GetAcctInfoByAcctForWPF(string acctcode, string acctno, string password)
        {
            return DB.SelectAllColumnsFrom<StoreAcctInfo>().Where(StoreAcctInfo.Columns.AcctCode).IsEqualTo(acctcode)
                 .And(StoreAcctInfo.Columns.AcctNo).IsEqualTo(acctno)
                 .And(StoreAcctInfo.Columns.AcctPw).IsEqualTo(password)
                 .ExecuteAsCollection<StoreAcctInfoCollection>();
        }
        #endregion

        #region 成套商品

        public bool GroupCouponCheckExist(Guid orderGuid, string timestamp)
        {
            var checkTemp = DB.Select().From(GroupCouponLog.Schema).NoLock()
                .Where(GroupCouponLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(GroupCouponLog.Columns.Timestamp).IsEqualTo(timestamp)
                .ExecuteAsCollection<GroupCouponLogCollection>();

            return checkTemp.Any();
        }

        public bool GroupCouponLogSet(GroupCouponLog log)
        {
            var checkTemp = DB.Select().From(GroupCouponLog.Schema).NoLock()
                .Where(GroupCouponLog.Columns.OrderGuid).IsEqualTo(log.OrderGuid)
                .And(GroupCouponLog.Columns.Timestamp).IsEqualTo(log.Timestamp)
                .ExecuteAsCollection<GroupCouponLogCollection>();

            if (checkTemp.Any()) return false;
            DB.Save(log);
            return true;
        }

        #endregion

        /// <summary>
        /// 依商家帳號取得其擁有讀取權限的分店
        /// </summary>
        /// <param name="accountId">商家帳號</param>
        /// <returns></returns>
        public List<Guid> StoreGuidsGetListByAccountId(string accountId)
        {
            /*
             * {
             *   設定賣家權限，列出其擁有的分店
             *     union (加)
             *   設定分店權限，選取該分店
             *     union (加)
             *   設定品生活檔次權限, hi_deal_product_store該檔有設定的分店都會選取
             *     union (加)
             *   設定P好康檔次權限，ppon_store該檔有設定的分店都會選取
             * }
             * except (減掉)
             * {
             *   設定賣家權限為deny時，賣家擁有的分店
             *     union (加)
             *   設定分店權限為deny時，該分店  
             * }
             * 
             * 
             * permission_setting = 1 is allow 
             * permission_setting = 0 is deny
             */
            string sql =
                @"
(
	select distinct ps.store_guid from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join business_hour as bh on bh.seller_GUID = s.GUID
		inner join ppon_store as ps on ps.business_hour_guid = bh.GUID 
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 1
	union
		select st.GUID from resource_acl ra
		inner join seller st on st.Guid = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 2 and
			ra.permission_type = 1 and ra.permission_setting = 1
	union
        select hdps.store_guid as GUID from resource_acl ra
        inner join hi_deal_product hdp on hdp.guid = ra.resource_guid
        inner join hi_deal_product_store hdps on hdps.hi_deal_product_id = hdp.id
        where 
			ra.account_id = @account_id and
			ra.resource_type = 3 and
			ra.permission_type = 1 and ra.permission_setting = 1
	union
        select ps.store_guid as GUID from resource_acl ra
        inner join ppon_store ps on ps.business_hour_guid = ra.resource_guid
        where 
			ra.account_id = @account_id and
			ra.resource_type = 4 and
			ra.permission_type = 1 and ra.permission_setting = 1
)
EXCEPT 
(
	select distinct ps.store_guid from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join business_hour as bh on bh.seller_GUID = s.GUID
		inner join ppon_store as ps on ps.business_hour_guid = bh.GUID 
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 0
	union
		select st.GUID from resource_acl ra
		inner join seller st on st.Guid = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 2 and
			ra.permission_type = 1 and ra.permission_setting = 0
)
";

            List<Guid> result = new List<Guid>();
            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@account_id", accountId, DbType.String);
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        /// <summary>
        /// 依商家帳號取得其擁有讀取權限的檔次
        /// 
        /// *當某檔所設定的所有分店，在商家權限中的分店權限"都"被設成deny時，將被排除。
        /// 
        /// </summary>
        /// <param name="accountId"></param>
        /// <returns></returns>
        public List<VendorBillingProductModel> DealGuidsGetListByAccountId(string accountId)
        {         
            string sql = @"
(
	--P好康賣家權限
	select bh.GUID,1 as order_classification from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join business_hour bh on bh.seller_GUID = s.GUID
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 1
            and bh.business_hour_status & 1024 = 0
)
union
(
	--P好康分店權限
	select bh.GUID,1 as order_classification from resource_acl ra
		inner join ppon_store ps on ps.store_guid = ra.resource_guid
		inner join business_hour bh on bh.GUID = ps.business_hour_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 2 and
			ra.permission_type = 1 and ra.permission_setting = 1
            and bh.business_hour_status & 1024 = 0
)
union
(
	--P好康檔次權限
	select bh.GUID,1 as order_classification from resource_acl ra
		inner join business_hour bh on bh.GUID = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 4 and
			ra.permission_type = 1 and ra.permission_setting = 1
            and bh.business_hour_status & 1024 = 0
)
union
(
	--品生活賣家權限
	select hdp.guid,2 as order_classification from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join hi_deal_product hdp on hdp.seller_guid = s.GUID
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 1
)
union
(
	--品生活分店權限
	select hdp.guid,2 as order_classification from resource_acl ra
		inner join hi_deal_product_store hdps on hdps.store_guid = ra.resource_guid
		inner join hi_deal_product hdp on hdp.id = hdps.hi_deal_product_id
		where 
			ra.account_id = @account_id and
			ra.resource_type = 2 and
			ra.permission_type = 1 and	ra.permission_setting = 1
)
union
(
	--品生活檔次權限
	select hdp.guid,2 as order_classification from resource_acl ra
		inner join hi_deal_product hdp on hdp.guid = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 3 and
			ra.permission_type = 1 and ra.permission_setting = 1
)
except
(
	--P好康賣家權限(deny)
	select bh.GUID,1 as order_classification from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join business_hour bh on bh.seller_GUID = s.GUID
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 0
)
except
(
	--P好康分店權限(deny)
    select bh.GUID,1 as order_classification from business_hour bh
    where (  
	    (
		    select count(*) from ppon_store ps -- 設定的分店數
		    where ps.business_hour_guid = bh.GUID
	    )
    =
	    (
		    select count(*) from ppon_store ps -- 槽限設定deny的分店數
		    inner join resource_acl ra on 
				ra.account_id = @account_id and
			    ra.resource_guid = ps.store_guid and 
			    ra.resource_type = 2 and 
			    ra.permission_type = 1 and ra.permission_setting = 0		
		    where ps.business_hour_guid = bh.GUID
	    )	
    )
    and 
	    (
		    select count(*) from ppon_store ps 
		    where ps.business_hour_guid = bh.GUID
	    ) > 0
)
except
(
	--P好康檔次權限(deny)
	select bh.GUID,1 as order_classification from resource_acl ra
		inner join business_hour bh on bh.GUID = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 4 and
			ra.permission_type = 1 and ra.permission_setting = 0
)
except
(
	--品生活賣家權限(deny)
	select hdp.guid,2 as order_classification from resource_acl ra
		inner join seller s on s.GUID = ra.resource_guid
		inner join hi_deal_product hdp on hdp.seller_guid = s.GUID
		where 
			ra.account_id = @account_id and
			ra.resource_type = 1 and
			ra.permission_type = 1 and ra.permission_setting = 0
)
except
(
	--品生活分店權限(deny)
    select hdp.guid,2 as order_classification from hi_deal_product hdp
    where (  
	    (
		    select count(*) from hi_deal_product_store hdps -- 設定的分店數
		    where hdps.hi_deal_product_id = hdp.id
	    )
    =
	    (
		    select count(*) from hi_deal_product_store hdps -- 槽限設定deny的分店數
		    inner join resource_acl ra on 
				ra.account_id = @account_id and
			    ra.resource_guid = hdps.store_guid and 
			    ra.resource_type = 2 and 
			    ra.permission_type = 1 and ra.permission_setting = 0		
		    where hdps.hi_deal_product_id = hdp.id
	    )	
    )
    and 
	    (
		    select count(*) from hi_deal_product_store hdps
		    where hdps.hi_deal_product_id = hdp.id
	    ) > 0
)
except 
(
	--品生活檔次權限(deny)
	select hdp.guid,2 as order_classification from resource_acl ra
		inner join hi_deal_product hdp on hdp.guid = ra.resource_guid
		where 
			ra.account_id = @account_id and
			ra.resource_type = 3 and
			ra.permission_type = 1 and ra.permission_setting = 0
)
";

            List<VendorBillingProductModel> result = new List<VendorBillingProductModel>();
            var qc = new QueryCommand(sql, CashTrustLog.Schema.Provider.Name);
            qc.AddParameter("@account_id", accountId, DbType.String);
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    result.Add(new VendorBillingProductModel
                    {
                        Guid = dr.GetGuid(0),
                        OrderClassification = (OrderClassification)dr.GetInt32(1)
                    });
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }
        
        protected QueryCommand GetVpolWhereQC(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }
    }
}

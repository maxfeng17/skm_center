﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.SsBLL
{
    public class SSItemProvider : IItemProvider
    {

        public AccessoryGroupCollection AccessoryGroupGetListByItem(Guid[] guidItemList)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroup>()
                .Where(AccessoryGroup.GuidColumn)
                .In(new Select(ItemAccessoryGroupList.AccessoryGroupGuidColumn)
                        .From<ItemAccessoryGroupList>()
                        .Where(ItemAccessoryGroupList.ItemGuidColumn).In(guidItemList)).OrderAsc(AccessoryGroup.Columns.AccessoryGroupName)
                .ExecuteAsCollection<AccessoryGroupCollection>();
        }

        public AccessoryGroupCollection AccessoryGroupGetListByItem(Guid itemGuid)
        {
            string sql = string.Format(
                @"SELECT ag.*
                  FROM {0} il
                  left join accessory_group ag on ag.guid = il.accessory_group_guid
                  where il.item_guid=@ItemGuid
                  order by il.sequence ", ItemAccessoryGroupList.Schema.TableName);

            QueryCommand qc = new QueryCommand(sql, AccessoryGroup.Schema.Provider.Name);
            qc.Parameters.Add("@ItemGuid", itemGuid, DbType.Guid);
            AccessoryGroupCollection col = new AccessoryGroupCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public AccessoryGroupCollection AccessoryGroupGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroup>().Where(column).IsEqualTo(value).ExecuteAsCollection<AccessoryGroupCollection>();
        }

        public AccessoryGroupCollection AccessoryGroupGetListBySeller(Guid guidSeller)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroup>()
                .Where(AccessoryGroup.SellerGuidColumn).IsEqualTo(guidSeller)
                .ExecuteAsCollection<AccessoryGroupCollection>();
        }

        public AccessoryGroupCollection AccessoryGroupGetList(int pageStart, int pageLength, string orderby, params string[] filter)
        {
            Query qry = SSHelper.GetQueryByFilterOrder(new Query(AccessoryGroup.Schema.TableName, AccessoryGroup.Schema.Provider.Name), orderby, filter);
            qry.PageIndex = pageStart;
            qry.PageSize = pageLength;

            AccessoryGroupCollection view = new AccessoryGroupCollection();
            view.LoadAndCloseReader(qry.ExecuteReader());
            return view;
        }

        public AccessoryGroupCollection AccessoryGroupGetListNoItem(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroup>()
                .Where(AccessoryGroup.SellerGuidColumn).IsEqualTo(sellerGuid)
                .And(AccessoryGroup.GuidColumn).NotIn(new Select(ItemAccessoryGroupList.AccessoryGroupGuidColumn).From(ItemAccessoryGroupList.Schema.TableName))
                .ExecuteAsCollection<AccessoryGroupCollection>();
        }

        public AccessoryCategory AccessoryCategoryGetByAccessory(Guid guidAccessory)
        {
            return DB.Select().From<AccessoryCategory>()
                .InnerJoin<Accessory>().Where(Accessory.GuidColumn).IsEqualTo(guidAccessory)
                .ExecuteSingle<AccessoryCategory>();
        }

        public bool AccessorySet(Accessory a)
        {
            DB.Save<Accessory>(a);
            return true;
        }

        public bool AccessoryDelete(string column, object value)
        {
            DB.Destroy<Accessory>(column, value);
            return true;
        }

        public AccessoryCategoryCollection AccessoryCategoryGetList()
        {
            return DB.SelectAllColumnsFrom<AccessoryCategory>()
                .OrderDesc(AccessoryCategory.Columns.CreateTime)
                .ExecuteAsCollection<AccessoryCategoryCollection>();
        }

        public Accessory AccessoryGet(string column, object value)
        {
            return DB.Get<Accessory>(column, value);
        }

        public Accessory AccessoryGet(Guid g)
        {
            return DB.Get<Accessory>(g);
        }

        public bool AccessoryCategorySet(AccessoryCategory ac)
        {
            DB.Save<AccessoryCategory>(ac);
            return true;
        }

        public bool AccessoryCategoryDelete(string column, object value)
        {
            DB.Destroy<AccessoryCategory>(column, value);
            return true;
        }

        public AccessoryCategory AccessoryCategoryGet(Guid guid)
        {
            return DB.Get<AccessoryCategory>(guid);
        }

        public AccessoryCategory AccessoryCategoryGet(string name)
        {
            return DB.Get<AccessoryCategory>(AccessoryCategory.Columns.AccessoryCategoryName, name);
        }

        public AccessoryGroupMember AccessoryGroupMemberGet(Guid guidAccessoryGroupMember)
        {
            return DB.Get<AccessoryGroupMember>(guidAccessoryGroupMember);
        }

        public AccessoryGroupMemberCollection AccessoryGroupMemberGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroupMember>()
                .Where(column).IsEqualTo(value)
                .OrderAsc(AccessoryGroupMember.Columns.Sequence)
                .ExecuteAsCollection<AccessoryGroupMemberCollection>();
        }

        public AccessoryGroupMemberCollection AccessoryGroupMemberGetList(params Guid[] accGrpGuidList)
        {
            return DB.SelectAllColumnsFrom<AccessoryGroupMember>()
                .Where(AccessoryGroupMember.AccessoryGroupGuidColumn)
                .In(accGrpGuidList)
                .OrderAsc(AccessoryGroupMember.Columns.Sequence)
                .ExecuteAsCollection<AccessoryGroupMemberCollection>();
        }

        public AccessoryCollection AccessoryGetList(Guid guidAccessoryCategory)
        {
            return DB.SelectAllColumnsFrom<Accessory>()
                .Where(Accessory.AccessoryCategoryGuidColumn).IsEqualTo(guidAccessoryCategory)
                .ExecuteAsCollection<AccessoryCollection>();
        }

        public AccessoryCollection AccessoryGetList(params Guid[] accessoryGuids)
        {
            return DB.SelectAllColumnsFrom<Accessory>().Where(Accessory.GuidColumn).In(accessoryGuids).ExecuteAsCollection<AccessoryCollection>();
        }

        public AccessoryCollection AccessoryGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<Accessory>().Where(column).IsEqualTo(value).ExecuteAsCollection<AccessoryCollection>();
        }

        public bool AccessoryGroupSet(AccessoryGroup ag)
        {
            try
            {
                DB.Save<AccessoryGroup>(ag);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return true;
        }

        public bool AccessoryGroupDelete(string column, object value)
        {
            AccessoryGroupMemberDelete(AccessoryGroupMember.Columns.AccessoryGroupGuid, value);
            ItemAccessoryGroupListDelete(ItemAccessoryGroupList.Columns.AccessoryGroupGuid, value);
            DB.Destroy<AccessoryGroup>(column, value);
            return true;
        }

        public AccessoryGroup AccessoryGroupGet(Guid id)
        {
            return DB.Get<AccessoryGroup>(id);
        }

        public bool AccessoryGroupPurgeNotReferenced(Guid sellerGuid)
        {
            string sql = 
@"DELETE FROM accessory_group_member WHERE accessory_group_GUID IN (SELECT GUID FROM [accessory_group] WHERE seller_GUID=@sgid AND GUID NOT IN (SELECT accessory_group_GUID FROM item_accessory_group_list));
DELETE FROM [accessory_group] WHERE seller_GUID=@sgid AND GUID NOT IN (SELECT accessory_group_GUID FROM item_accessory_group_list)";
            QueryCommand qc = new QueryCommand(sql, AccessoryGroup.Schema.Provider.Name);
            qc.AddParameter("@sgid", sellerGuid, DbType.Guid);
            DataService.ExecuteScalar(qc);
            return true;
        }

        public bool AccessoryGroupPurgeReference(Guid sellerGuid)
        {
            string sql = @"Update [accessory_group] SET seller_GUID = @newsgid WHERE seller_GUID = @sgid AND GUID NOT IN (SELECT accessory_group_GUID FROM item_accessory_group_list)";
            QueryCommand qc = new QueryCommand(sql, AccessoryGroup.Schema.Provider.Name);
            qc.AddParameter("@sgid", sellerGuid, DbType.Guid);
            qc.AddParameter("@newsgid", Guid.Empty, DbType.Guid);
            DataService.ExecuteScalar(qc);
            return true;
        }

        public bool AccessoryGroupMemberSet(AccessoryGroupMember agm)
        {
            DB.Save<AccessoryGroupMember>(agm);
            return true;
        }

        public bool AccessoryGroupMemberDelete(string column, object value)
        {
            DB.Destroy<AccessoryGroupMember>(column, value);
            return true;
        }

        public ItemAccessoryGroupList ItemAccessoryGroupListGet(string column, object value)
        {
            return DB.Get<ItemAccessoryGroupList>(column, value);
        }

        public bool ItemAccessoryGroupListSet(ItemAccessoryGroupList i)
        {
            DB.Save<ItemAccessoryGroupList>(i);
            return true;
        }

        public bool ItemAccessoryGroupListDelete(string column, object value)
        {
            DB.Destroy<ItemAccessoryGroupList>(column, value);
            return true;
        }

        public bool ItemAccessoryGroupListDelete(string column, Guid itemGuid, Guid accessoryGroupGuid)
        {
            ItemAccessoryGroupListCollection iaglCol = new Select(ItemAccessoryGroupList.GuidColumn)
                                                            .From(ItemAccessoryGroupList.Schema)
                                                            .Where(ItemAccessoryGroupList.ItemGuidColumn).IsEqualTo(itemGuid)
                                                            .And(ItemAccessoryGroupList.AccessoryGroupGuidColumn).IsEqualTo(accessoryGroupGuid)
                                                            .ExecuteAsCollection<ItemAccessoryGroupListCollection>();
            foreach (ItemAccessoryGroupList iagl in iaglCol)
            {
                ItemAccessoryGroupListDelete(column, iagl.Guid);
            }
            return true;
        }

        public ItemAccessoryGroupListCollection ItemAccessoryGroupListGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<ItemAccessoryGroupList>()
                .Where(column).IsEqualTo(value)
                .OrderAsc(ItemAccessoryGroupList.Columns.Sequence)
                .ExecuteAsCollection<ItemAccessoryGroupListCollection>();
        }

        public ItemAccessoryGroupListCollection ItemAccessoryGroupListGetListByItem(params Guid[] itemGuidList)
        {
            return DB.SelectAllColumnsFrom<ItemAccessoryGroupList>()
                .Where(ItemAccessoryGroupList.ItemGuidColumn)
                .In(itemGuidList)
                .ExecuteAsCollection<ItemAccessoryGroupListCollection>();
        }

        public Item ItemGet(Guid g)
        {
            return DB.Get<Item>(g);
        }


        public Item ItemGetByBid(Guid bid)
        {
            return DB.Get<Item>(Item.Columns.BusinessHourGuid, bid);
        }

        public ItemCollection ItemGetByBid(IEnumerable<Guid> bid)
        {
            ItemCollection infos = DB.SelectAllColumnsFrom<Item>()
                    .Where(Item.Columns.BusinessHourGuid).In(bid)
                    .ExecuteAsCollection<ItemCollection>();

            return infos;
        }

        public int ItemGetCount(Guid menuGuid)
        {
            return DB.Select(Aggregate.Count(Item.GuidColumn)).From(Item.Schema.TableName)
                .Where(Item.MenuGuidColumn).IsEqualTo(menuGuid)
                .ExecuteScalar<int>();
        }

        public ItemCollection ItemGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<Item>()
                .Where(column).IsEqualTo(value)
                .OrderAsc(Item.Columns.Sequence)
                .ExecuteAsCollection<ItemCollection>();
        }
		
        public ItemCollection ItemGetListInMenu(params Guid[] menuGuidList)
        {
            return DB.SelectAllColumnsFrom<Item>()
                .Where(Item.MenuGuidColumn).In(menuGuidList)
                .ExecuteAsCollection<ItemCollection>();
        }
 
        public bool ItemSet(Item i)
        {
            DB.Save<Item>(i);
            return true;
        }

        public bool ItemDelete(Guid g)
        {
            DB.Destroy<Item>(Item.Columns.Guid, g);
            return true;
        }

        public ViewAccessoryGroupCategoryCollection ViewAccGrpCatGetList(string column, object value)
        {
            ViewAccessoryGroupCategoryCollection col = new ViewAccessoryGroupCategoryCollection();
            col.LoadAndCloseReader(ViewAccessoryGroupCategory.FetchByQuery(ViewAccessoryGroupCategory.Query().WHERE(column, value).ORDER_BY(ViewAccessoryGroupCategory.Columns.Sequence)));
            return col;
        }

        public ViewItemAccessoryGroupCollection ViewItemAccessoryGroupGetList(Guid itemGuid,Guid accessoryGroupGuid)
        {
            ViewItemAccessoryGroupCollection viagCol = new ViewItemAccessoryGroupCollection();
            viagCol.LoadAndCloseReader(ViewItemAccessoryGroup.FetchByQuery(
                ViewItemAccessoryGroup.Query().WHERE(ViewItemAccessoryGroup.Columns.ItemGuid, itemGuid)
                .AND(ViewItemAccessoryGroup.Columns.AccessoryGroupGuid,accessoryGroupGuid)
                .ORDER_BY(ViewItemAccessoryGroup.Columns.ItemAccessoryGroupListSequence)
                .ORDER_BY(ViewItemAccessoryGroup.Columns.AccessoryGroupMemberSequence)));
            return viagCol;
        }

        /// <summary>
        /// Query ViewItemAccessoryGroup to get its collection, result is sorterd according to ItemAccessoryGroupListSequence sequence and then AccessoryGroupMemberSequence
        /// </summary>
        /// <param name="itemGuid">The item GUID.</param>
        /// <returns></returns>
        public ViewItemAccessoryGroupCollection ViewItemAccessoryGroupGetList(Guid itemGuid)
        {
            ViewItemAccessoryGroupCollection viagCol = new ViewItemAccessoryGroupCollection();
            viagCol.LoadAndCloseReader(ViewItemAccessoryGroup.FetchByQuery(
                ViewItemAccessoryGroup.Query().WHERE(ViewItemAccessoryGroup.Columns.ItemGuid, itemGuid)
                .ORDER_BY(ViewItemAccessoryGroup.Columns.ItemAccessoryGroupListSequence)
                .ORDER_BY(ViewItemAccessoryGroup.Columns.AccessoryGroupMemberSequence)));
            return viagCol;
        }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.SsBLL.DbContexts;
using System.Data.Entity;
using System.Linq;

namespace LunchKingSite.SsBLL.Provider
{
    public class EntrepotProvider : IEntrepotProvider
    {
        public bool EntrepotMemberSet(EntrepotMember member)
        {
            using (var db = new EntrepotDbContext())
            {
                db.Entry(member).State = member.Id == 0 ? EntityState.Added : EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public EntrepotMember EntrepotMemberGet(string token, int oauthClientId)
        {
            using (var db = new EntrepotDbContext())
            {
                using (db.NoLock())
                {
                    var a = (from s in db.EntrepotMemberEntities
                             where s.EntrepotToken == token && s.OauthClientId == oauthClientId
                             select s).FirstOrDefault();
                    return a ?? new EntrepotMember();
                }
            }
        }
    }
}

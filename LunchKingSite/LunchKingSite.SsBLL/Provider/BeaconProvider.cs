﻿using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.SsBLL.DbContexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.SsBLL.Provider
{
    public class BeaconProvider : IBeaconProvider
    {
        #region BeaconDeviceEntity

        public bool InsertOrUpdateBeaconDevice(BeaconDevice beaconDevice)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconDevice).State = beaconDevice.DeviceId == 0 ?
                                                  EntityState.Added :
                                                  EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOrUpdateBeaconDeviceList(List<BeaconDevice> beaconDeviceList)
        {
            using (var db = new BeaconDbContext())
            {
                foreach (var beaconDevice in beaconDeviceList)
                {
                    db.Entry(beaconDevice).State = beaconDevice.DeviceId == 0
                        ? EntityState.Added
                        : EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconDevice(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var col = db.BeaconDeviceEntities.FirstOrDefault(x => x.DeviceId == deviceId);
                db.BeaconDeviceEntities.Remove(col);
                return db.SaveChanges() > 0;
            }
        }

        public BeaconDevice GetBeaconDeviceByMacAddress(string beaconMacAddress1, string beaconMacAddress2)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = (from b in db.BeaconDeviceEntities
                               join bgdl in db.BeaconGroupDeviceLinkEntities on b.DeviceId equals bgdl.DeviceId
                               join bg in db.BeaconGroupEntities on bgdl.GroupId equals bg.GroupId
                               join bta in db.BeaconTriggerAppEntities on bg.TriggerAppId equals bta.TriggerAppId
                               where bta.TriggerAppId == (int)BeaconTriggerAppFunctionName.skm && (b.ElectricMacAddress.Contains(beaconMacAddress1) || b.ElectricMacAddress.Contains(beaconMacAddress2))
                               select b).FirstOrDefault();

                    return one ?? new BeaconDevice();
                }
            }
        }

        public int GetBeaconDeviceId(int major, int minor)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    int deviceId = (from p in db.BeaconDeviceEntities
                                    where p.Major == major && p.Minor == minor
                                    select p.DeviceId).FirstOrDefault();

                    return deviceId;
                }
            }
        }

        public List<ViewBeaconDeviceGroup> GetBeaconDeviceGroupList(int triggerAppId, BeaconPowerLevel? beaconPowerLevel, string groupCode, string auxiliaryCode, string orderField, bool isDesc)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from v in db.ViewBeaconDeviceGroupEntities
                               where v.TriggerAppId == triggerAppId
                               select v;

                    //篩選條件 電量
                    switch (beaconPowerLevel)
                    {
                        case BeaconPowerLevel.Low:
                            cols = cols.Where(p => p.ElectricPower <= 2.3);
                            break;
                        case BeaconPowerLevel.Medium:
                            cols = cols.Where(p => p.ElectricPower > 2.3 && p.ElectricPower < 2.6);
                            break;
                        case BeaconPowerLevel.Full:
                            cols = cols.Where(p => p.ElectricPower >= 2.6);
                            break;
                    }

                    //篩選條件 auxiliaryCode
                    if (!string.IsNullOrEmpty(auxiliaryCode))
                    {
                        cols = cols.Where(p => p.AuxiliaryCode == auxiliaryCode);
                    }else
                    {
                        cols = cols.Where(p => p.AuxiliaryCode != "fullshop");
                    }

                    //篩選條件 groupCode
                    if (!string.IsNullOrEmpty(groupCode))
                    {
                        cols = cols.Where(p => p.GroupCode == groupCode);
                    }

                    //排序
                    switch (orderField)
                    {
                        case "Major":
                            cols = isDesc ? cols.OrderByDescending(p => p.Major) : cols.OrderBy(p => p.Major);
                            break;
                        case "Minor":
                            cols = isDesc ? cols.OrderByDescending(p => p.Minor) : cols.OrderBy(p => p.Minor);
                            break;
                        case "DeviceName":
                            cols = isDesc ? cols.OrderByDescending(p => p.DeviceName) : cols.OrderBy(p => p.DeviceName);
                            break;
                        case "Floor":
                            cols = isDesc ? cols.OrderByDescending(p => p.Floor) : cols.OrderBy(p => p.Floor);
                            break; 
                         case "Electric":
                            cols = isDesc ? cols.OrderByDescending(p => p.ElectricPower) : cols.OrderBy(p => p.ElectricPower);
                            break; 
                        case "LastUpdateTime":
                            cols = isDesc ? cols.OrderByDescending(p => p.LastUpdateTime) : cols.OrderBy(p => p.LastUpdateTime);
                            break;
                        default:
                            cols = isDesc ? cols.OrderByDescending(p => p.Floor) : cols.OrderBy(p => p.Floor);
                            break;
                    }

                    return cols.ToList();
                }
            }
        }
        #endregion

        #region BeaconGroup

        public List<BeaconGroup> GetBeaconGroupListByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from beaciGroup in db.BeaconGroupEntities
                               join beaconEventGroupLink in db.BeaconEventGroupLinkEntities on beaciGroup.GroupId equals beaconEventGroupLink.GroupId
                               where beaconEventGroupLink.EventId == eventId
                               select beaciGroup;

                    return cols.ToList();
                }
            }
        }
        
        public List<BeaconGroup> GetBeaconGroupList(int triggerAppId)
        {
            return GetBeaconGroupList(triggerAppId, null, null, null);
        }
        
        public List<BeaconGroup> GetBeaconGroupList(int triggerAppId, string groupCode)
        {
            return GetBeaconGroupList(triggerAppId, groupCode, null, null);
        }

        public List<BeaconGroup> GetBeaconGroupListByAuxiliaryCode(int triggerAppId, string auxiliaryCode)
        {
            return GetBeaconGroupList(triggerAppId, null, auxiliaryCode, null);
        }
        
        public List<BeaconGroup> GetBeaconGroupListNotContainAuxiliaryCode(int triggerAppId, string groupCode, string notContainAuxiliaryCode)
        {
            return GetBeaconGroupList(triggerAppId, groupCode, null, notContainAuxiliaryCode);
        }

        public List<BeaconGroup> GetBeaconGroupList(int triggerAppId, string groupCode, string auxiliaryCode)
        {
            return GetBeaconGroupList(triggerAppId, groupCode, auxiliaryCode, null);
        }

        public List<BeaconGroup> GetBeaconGroupList(int triggerAppId, string groupCode, string auxiliaryCode, string notContainAuxiliaryCode)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from p in db.BeaconGroupEntities
                               select p;

                    if (triggerAppId!=0)
                    {
                        cols = cols.Where(p=>p.TriggerAppId == triggerAppId);
                    }
                    if (!string.IsNullOrEmpty(groupCode))
                    {
                        cols = cols.Where(p => p.GroupCode == groupCode);
                    }
                    if (!string.IsNullOrEmpty(auxiliaryCode))
                    {
                        cols = cols.Where(p => p.AuxiliaryCode == auxiliaryCode);
                    }
                    if (!string.IsNullOrEmpty(notContainAuxiliaryCode))
                    {
                        cols = cols.Where(p => p.AuxiliaryCode != notContainAuxiliaryCode);
                    }
                    return cols.ToList() ;
                }
            }
        }
        
        public List<int> GetBeaconEventGroupIdList(int triggerAppId, int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               where groups.TriggerAppId == triggerAppId && groupDevices.DeviceId == deviceId
                               group groups by groups.GroupId into g
                               select g.FirstOrDefault();
                    return cols.Select(p=>p.GroupId).ToList();
                }
            }
        }

        public bool InsertOrUpdateBeaconGroupList(List<BeaconGroup> beaconGroups)
        {
            using (var db = new BeaconDbContext())
            {
                foreach (var beaconGroup in beaconGroups)
                {
                    db.Entry(beaconGroup).State = beaconGroup.GroupId == 0 ?
                                                    EntityState.Added :
                                                    EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconEventGroupIdList(int groupId)
        {
            using (var db = new BeaconDbContext())
            {
                var col = db.BeaconGroupEntities.FirstOrDefault(p => p.GroupId == groupId);
                db.BeaconGroupEntities.Remove(col);
                return db.SaveChanges() > 0;
            }
        }

        public List<BeaconGroupIdDeivceId> GetBeaconGroupIdList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               where groups.TriggerAppId == triggerAppId
                               select new BeaconGroupIdDeivceId
                               {
                                   GroupId = groups.GroupId,
                                   DeviceId = groupDevices.DeviceId
                               };

                    return cols.ToList();
                }
            }
        }

        public List<BeaconGroupDeviceLink> GetBeaconGroupDeviceLinkList()
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconGroupDeviceLinkEntities.ToList();
                }
            }
        }

        public List<BeaconChangeManageInfo> GetBeaconChangeManageInfoList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               join devices in db.BeaconDeviceEntities on groupDevices.DeviceId equals devices.DeviceId
                               where groups.TriggerAppId == triggerAppId && groups.AuxiliaryCode != "fullshop"
                               select new BeaconChangeManageInfo
                               {
                                   DeviceId = devices.DeviceId,
                                   DeviceName = devices.DeviceName,
                                   Major = devices.Major,
                                   Minor = devices.Minor,
                                   GroupRemark = groups.GroupRemark,
                                   GroupCode = groups.GroupCode,
                                   AuxiliaryCode = groups.AuxiliaryCode
                               };
                    return cols.ToList();
                }
            }
        }

        public List<BeaconGroup> GetBeaconEventGroupByEventId(int eventId,int linkType)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from beaconEventGroupLinks in db.BeaconEventGroupLinkEntities
                               join beaconGroups in db.BeaconGroupEntities on beaconEventGroupLinks.GroupId equals beaconGroups.GroupId
                               where beaconEventGroupLinks.EventId == eventId && beaconEventGroupLinks.LinkType == linkType
                               select beaconGroups;

                    return cols.ToList();
                }
            }
        }
        
        public BeaconGroup GetBeaconGroupByGroupId(int groupId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconGroupEntities.FirstOrDefault(x => x.GroupId == groupId);
                }
            }
        }

        public int GetBeaconEventDeviceLinkForDeviceId(int eventGroupLinkId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var beaconEventDeviceLink = db.BeaconEventDeviceLinkEntities.FirstOrDefault(x => x.EventGroupLinkId == eventGroupLinkId);
                    if (beaconEventDeviceLink != null)
                    {
                        return beaconEventDeviceLink.DeviceId;
                    }
                    return 0;
                }
            }
        }


        public List<BeaconDevice> GetBeaconEventDeviceLinkListByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from beaconEventDeviceLinks in db.BeaconEventDeviceLinkEntities
                               join beaconDevices in db.BeaconDeviceEntities on beaconEventDeviceLinks.DeviceId equals beaconDevices.DeviceId
                               where beaconEventDeviceLinks.EventId == eventId
                               select beaconDevices;

                    return cols.ToList();
                }
            }
        }

        public List<BeaconDevice> GetBeaconDeviceListByBeaconGroup(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               join devices in db.BeaconDeviceEntities on groupDevices.DeviceId equals devices.DeviceId
                               where groups.TriggerAppId == triggerAppId 
                               select devices;

                    return cols.ToList();
                }
            }
        }

        public List<BeaconDevice> GetBeaconDeviceListByBeaconGroup(int triggerAppId,string groupCode, string auxiliaryCode)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               join devices in db.BeaconDeviceEntities on groupDevices.DeviceId equals devices.DeviceId
                               where groups.TriggerAppId == triggerAppId && groups.GroupCode == groupCode && groups.AuxiliaryCode == auxiliaryCode
                               select devices;

                    return cols.ToList();
                }
            }
        }

        public List<BeaconDevice> GetBeaconDevice()
        {
            using (var db = new BeaconDbContext())
            {
                return db.BeaconDeviceEntities.ToList();
            }
        }

        public BeaconDevice GetBeaconDevice(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = db.BeaconDeviceEntities.FirstOrDefault(x => x.DeviceId == deviceId);
                    return one ?? new BeaconDevice();
                }
            }
        }

        public BeaconDevice GetBeaconDevice(int major, int minor)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconDeviceEntities.FirstOrDefault(x => x.Major == major && x.Minor == minor);
                }
            }
        }


        public List<BeaconDevice> GetBeaconDeviceList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from devices in db.BeaconDeviceEntities
                               join deviceFieldLink in db.BeaconFieldDeviceLinkEntities on devices.DeviceId equals deviceFieldLink.DeviceId
                               join deviceField in db.BeaconFieldEntities on deviceFieldLink.FieldId equals deviceField.FieldId
                               join deviceTriggerApp in db.BeaconTriggerAppFieldEntities on deviceFieldLink.FieldId equals deviceTriggerApp.FieldId
                               where deviceTriggerApp.TriggerAppId == triggerAppId 
                               select devices;

                    return cols.ToList();
                }
            }
        }

        public bool InsertOrUpdateBeaconFieldDeviceLink(int triggerAppId, List<int> deviceIds)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var fieldId = from deviceAppField in db.BeaconTriggerAppFieldEntities
                                  join deviceFieldLink in db.BeaconFieldEntities on deviceAppField.FieldId equals deviceFieldLink.FieldId
                                  where deviceAppField.TriggerAppId == triggerAppId
                                  select deviceFieldLink.FieldId;

                    foreach (var deviceId in deviceIds)
                    {
                        BeaconFieldDeviceLink beaconFieldDeviceLink = new BeaconFieldDeviceLink
                        {
                            FieldId = fieldId.FirstOrDefault(),
                            DeviceId = deviceId
                        };
                        db.Entry(beaconFieldDeviceLink).State = beaconFieldDeviceLink.Id == 0 ?
                                                                EntityState.Added :
                                                                EntityState.Modified;
                    }
                    return db.SaveChanges() > 0;
                }
            }
        }

        public bool DeleteBeaconFieldDeviceLink(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconFieldDeviceLinkEntities.Where(p => p.DeviceId == deviceId);
                db.BeaconFieldDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconFieldDeviceLink(int triggerAppId, int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = from deviceFieldLink in db.BeaconFieldDeviceLinkEntities
                           join deviceField in db.BeaconFieldEntities on deviceFieldLink.FieldId equals deviceField.FieldId
                           join deviceTriggerApp in db.BeaconTriggerAppFieldEntities on deviceFieldLink.FieldId equals deviceTriggerApp.FieldId
                           where deviceTriggerApp.TriggerAppId == triggerAppId && deviceFieldLink.DeviceId == deviceId
                           select deviceFieldLink;

                db.BeaconFieldDeviceLinkEntities.RemoveRange(cols);

                return db.SaveChanges() > 0;
            }
        }

        public List<BeaconDevice> GetSingleBeaconDeviceList(int triggerAppId, int beaconEventGroupId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from devices in db.BeaconDeviceEntities
                               join deviceLinks in db.BeaconEventDeviceLinkEntities on devices.DeviceId equals deviceLinks.DeviceId
                               join deviceFieldLink in db.BeaconFieldDeviceLinkEntities on devices.DeviceId equals deviceFieldLink.DeviceId
                               join deviceField in db.BeaconFieldEntities on deviceFieldLink.FieldId equals deviceField.FieldId
                               join deviceTriggerApp in db.BeaconTriggerAppFieldEntities on deviceFieldLink.FieldId equals deviceTriggerApp.FieldId
                               where deviceTriggerApp.TriggerAppId == triggerAppId && deviceLinks.EventGroupLinkId == beaconEventGroupId
                               select devices;

                    return cols.ToList();
                }
            }
        }

        public List<int> GetEffectiveBeaconEventIds(DateTime dt, int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                string sql = @"SELECT DISTINCT events.event_id FROM beacon_event as events with(nolock)"
                + " where events.effective_date_start <= '" + dt.ToString("yyyy/MM/dd") + "' and events.effective_date_end  > '" + dt.ToString("yyyy/MM/dd") + "'"
                + " and events.effective_time_start <= convert(varchar, '" + dt.ToString("HH:mm") + "', 108) and events.effective_time_end > convert(varchar, '" + dt.ToString("HH:mm") + "', 108) "
                + " and events.trigger_app_id = " + triggerAppId + " and events.enabled = 1";

                var eventIds = db.Database.SqlQuery<int>(sql).ToList();
                return eventIds;
            }
        }

        public bool InsertOUpdateBeaconEventGroupLink(BeaconEventGroupLink beaconEventGroupLink)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconEventGroupLink).State = beaconEventGroupLink.Id == 0 ?
                                        EntityState.Added :
                                        EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOUpdateBeaconEventGroupLinkList(List<BeaconEventGroupLink> beaconEventGroupLinkList)
        {
            using (var db = new BeaconDbContext())
            {
                foreach (var beaconEventGroupLink in beaconEventGroupLinkList)
                {
                    db.Entry(beaconEventGroupLink).State = beaconEventGroupLink.Id == 0 ?
                                       EntityState.Added :
                                       EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconEventGroupLink(int groupId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventGroupLinkEntities.Where(p => p.GroupId == groupId);
                db.BeaconEventGroupLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconEventGroupLinkByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventGroupLinkEntities.Where(p => p.EventId == eventId);
                db.BeaconEventGroupLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOUpdateBeaconGroupDeviceLink(BeaconGroupDeviceLink beaconGroupDeviceLink)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconGroupDeviceLink).State = beaconGroupDeviceLink.Id == 0 ?
                                       EntityState.Added :
                                       EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOUpdateBeaconGroupDeviceLinkList(List<BeaconGroupDeviceLink> beaconGroupDeviceLinkList)
        {
            using (var db = new BeaconDbContext())
            {
                foreach (var beaconGroupDeviceLink in beaconGroupDeviceLinkList)
                {
                    db.Entry(beaconGroupDeviceLink).State = beaconGroupDeviceLink.Id == 0 ?
                                    EntityState.Added :
                                    EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconGroupDeviceLink(int groupId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconGroupDeviceLinkEntities.Where(x => x.GroupId == groupId).ToList();
                db.BeaconGroupDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconGroupDeviceLinkByDeviceId(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconGroupDeviceLinkEntities.Where(x => x.DeviceId == deviceId).ToList();
                db.BeaconGroupDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconGroupDeviceLinkByDeviceId(int triggerAppid, int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = from beaconGroup in db.BeaconGroupEntities
                           join deviceLinks in db.BeaconGroupDeviceLinkEntities on beaconGroup.GroupId equals deviceLinks.GroupId
                           where beaconGroup.TriggerAppId == triggerAppid && deviceLinks.DeviceId == deviceId
                           select deviceLinks;

                db.BeaconGroupDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public List<int> GetBeaconEventIdsByGroupIds(List<int> groupIds)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = (from g in db.BeaconGroupEntities
                                join l in db.BeaconEventGroupLinkEntities on g.GroupId equals l.GroupId
                                where groupIds.Contains(g.GroupId)
                                group l by l.EventId).Select(p => p.Key);

                    return cols.ToList();
                }
            }
        }

        public List<int> GetBeaconEventIdsByGroupIds(List<int> groupIds, LinkType linkType)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    int lintType = (int)linkType;
                    var cols = from groupLink in db.BeaconEventGroupLinkEntities
                               where groupIds.Contains(groupLink.GroupId) && groupLink.LinkType == lintType
                               select groupLink.EventId;

                    return cols.ToList();
                }
            }
        }

        public List<int> GetBeaconEventIdsByGroup(List<int> eventIds, int triggerAppId, int beaconDeviceId, LinkType linkType)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    int lintType = (int)linkType;
                    var cols = (from g in db.BeaconGroupEntities
                                join eg in db.BeaconEventGroupLinkEntities on g.GroupId equals eg.GroupId
                                join egd in db.BeaconGroupDeviceLinkEntities on eg.GroupId equals egd.GroupId
                                where eg.LinkType == lintType && egd.DeviceId == beaconDeviceId && g.TriggerAppId == triggerAppId &&
                                      eventIds.Contains(eg.EventId)
                                group eg by eg.EventId).Select(p => p.Key);

                    return cols.ToList();
                }
            }
        }

        public List<int> GetBeaconEventIdsByDeviceId(List<int> eventIds, int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from p in db.BeaconEventDeviceLinkEntities
                                   //指定 + 群組指定
                               where p.DeviceId == deviceId && eventIds.Contains(p.EventId)
                               select p.EventId;

                    return cols.ToList();
                }
            }
        }

        #endregion

        #region BeaconTriggerAppEntity

        public BeaconTriggerApp GetBeaconTriggerApp(string oauthAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = (from app in db.BeaconTriggerAppEntities
                               join appOauth in db.BeaconOauthEntities on app.TriggerAppId equals appOauth.TriggerAppId
                               where appOauth.OauthAppId == oauthAppId
                               select app).FirstOrDefault();

                    return one ?? new BeaconTriggerApp();
                }
            }
        }

        public BeaconDevice GetBeaconDevice(int major, int minor, int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = (from app in db.BeaconTriggerAppEntities
                               join appField in db.BeaconTriggerAppFieldEntities on app.TriggerAppId equals appField.TriggerAppId
                               join fieldDevic in db.BeaconFieldDeviceLinkEntities on appField.FieldId equals fieldDevic.FieldId
                               join device in db.BeaconDeviceEntities on fieldDevic.DeviceId equals device.DeviceId
                               where device.Major == major && device.Minor == minor && app.TriggerAppId == triggerAppId
                               select device).FirstOrDefault();

                    return one ?? new BeaconDevice();
                }
            }
        }

        public BeaconTriggerApp GetBeaconTriggerApp(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconTriggerAppEntities.FirstOrDefault(p => p.TriggerAppId == triggerAppId);
                }
            }
        }

        public BeaconTriggerApp GetBeaconTriggerAppByFunctionName(string functionName)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconTriggerAppEntities.FirstOrDefault(p => p.FunctionName == functionName);
                }
            }
        }

        public bool InsertOrUpdateBeaconEventEntity(BeaconEvent beaconEvent)
        {
            using (var db = new BeaconDbContext())
            {
                if (beaconEvent.EventId == 0)
                {
                    //自動取號
                    var id = GetBeaconEventIdTicker(BeaconTableName.Event);
                    beaconEvent.EventId = id;
                    db.Entry(beaconEvent).State = EntityState.Added;
                }
                else
                {
                    db.Entry(beaconEvent).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region BeaconEventIdTicker

        public int GetBeaconEventIdTicker(BeaconTableName tableName)
        {
            using (var db = new BeaconDbContext())
            {
                BeaconEventIdTicker beaconEventIdTicker = new BeaconEventIdTicker();
                beaconEventIdTicker.EventType = (byte)tableName;
                beaconEventIdTicker.CreateTime = DateTime.Now;
                db.Entry(beaconEventIdTicker).State = EntityState.Added;
                db.SaveChanges();
                return beaconEventIdTicker.Id;
            }
        }
        #endregion

        #region BeaconSubeventEntity

        public List<int> GetBeaconSubeventIds(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var collection = from sub in db.BeaconSubeventEntities
                                     where sub.EventId == eventId
                                     select sub.SubeventId;
                    return collection.ToList();
                }

            }
        }


        public BeaconSubevent GetBeaconSubeventListBySubEventId(int subEventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = (from p in db.BeaconSubeventEntities
                               where p.SubeventId == subEventId
                               select p).FirstOrDefault();

                    return one ?? new BeaconSubevent();
                }
            }
        }

        public List<BeaconSubevent> GetBeaconSubeventList(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconSubeventEntities.Where(x => x.EventId == eventId).ToList();
                }
            }
        }

        public bool InsertOrUpdateBeaconSubeventEntity(BeaconSubevent beaconSubevent)
        {
            using (var db = new BeaconDbContext())
            {
                if (beaconSubevent.SubeventId == 0)
                {   //自動取號
                    var id = GetBeaconEventIdTicker(BeaconTableName.SubEvent);
                    beaconSubevent.SubeventId = id;
                    db.Entry(beaconSubevent).State = EntityState.Added;
                }
                else
                {
                    db.Entry(beaconSubevent).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteBeaconSubeventEntity(List<int> subEventIds)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconSubeventEntities.Where(p => subEventIds.Contains(p.SubeventId));
                db.BeaconSubeventEntities.RemoveRange(cols);

                return db.SaveChanges() > 0;
            }
        }
        #endregion
        
        #region BeaconEvent

        public BeaconEvent GetBeaconEvent(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = (from p in db.BeaconEventEntities
                               where p.EventId == eventId
                               select p).FirstOrDefault();

                    return one ?? new BeaconEvent();
                }
            }
        }
        public BeaconEventTimeSlot GetEventTimeSlotBytimeSlotId(int timeSlotId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconEventTimeSlotEntities.FirstOrDefault(x => x.TimeSlotId == timeSlotId);
                }
            }
        }



        public List<BeaconEventTimeSlot> GetEventTimeSlotByFloor(List<int> eventIds, List<int> groupIds, DateTime dt, string floor)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    if (floor == null) floor = string.Empty;

                    var cols = from timeSlot in db.BeaconEventTimeSlotEntities
                               where groupIds.Contains(timeSlot.GroupId)
                               && eventIds.Contains(timeSlot.EventId)
                               && DbFunctions.TruncateTime(timeSlot.EffectiveDate) == dt.Date
                               && timeSlot.Floor == floor
                               && timeSlot.Status == 1
                               group timeSlot by new { timeSlot.EventId, timeSlot.Sequence, timeSlot.Floor } into g
                               select g.FirstOrDefault();
                    
                    return cols.OrderBy(p => p.Sequence).ThenByDescending(p => p.EventId).ToList();
                }
            }
        }


        public List<BeaconEventTimeSlot> GetEventTimeSlotByToDay(List<int> eventIds, List<int> groupIds, DateTime dt, string floor)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    // join beaconGroupDeviceLink 是為了不要與其他全館群組內非同樓層的一起排序
                    var cols = from timeSlot in db.BeaconEventTimeSlotEntities
                        
                               where groupIds.Contains(timeSlot.GroupId)
                               && eventIds.Contains(timeSlot.EventId)
                               && DbFunctions.TruncateTime(timeSlot.EffectiveDate) == dt.Date
                               && timeSlot.Floor == floor
                               && timeSlot.Status == 1
                               group timeSlot by new { timeSlot.EventId, timeSlot.Sequence } into g
                               select g.FirstOrDefault();

                    return cols.OrderBy(p => p.Sequence).ThenByDescending(p => p.EventId).ToList();
                }
            }
        }

        public List<BeaconEventTimeSlot> GetEventTimeSlotByToDay(int evnetId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from t in db.BeaconEventTimeSlotEntities
                               where t.EventId == evnetId && t.Status == 1
                               orderby t.Sequence, t.EventId // 若沒有排序則取決event誰先建立
                               select t;

                    return cols.ToList();
                }
            }
        }


        public List<ViewBeaconEventTimeSlot> GetEventTimeSlotByToDay(DateTime startDate, DateTime endDate, int fullshopGroupId, int groupId, string floor)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from t in db.ViewBeaconEventTimeSlotEntities
                               where DbFunctions.TruncateTime(t.EffectiveDate) >= startDate && DbFunctions.TruncateTime(t.EffectiveDate) <= endDate && t.Status == 1 && (t.GroupId == groupId || t.GroupId == fullshopGroupId)
                               //orderby t.Sequence, t.EventId // 多這行會有convert-iqueryable-to-iorderedqueryable的問題
                               select t;

                    if (!string.IsNullOrEmpty(floor))
                    {
                        cols = cols.Where(p => p.Floor == floor);
                    }

                    return cols.OrderBy(p => p.Sequence).ThenByDescending(p => p.EventId).ToList();
                }
            }
        }

        public bool InsertOrUpdateEventTimeSlot(BeaconEventTimeSlot beaconEventTimeSlot)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconEventTimeSlot).State = beaconEventTimeSlot.TimeSlotId == 0 ?
                                             EntityState.Added :
                                             EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteEventTimeSlotByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventTimeSlotEntities.Where(p => p.EventId == eventId);
                db.BeaconEventTimeSlotEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public List<BeaconEvent> GetBeaconEventList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from beaconEvent in db.BeaconEventEntities
                               where beaconEvent.TriggerAppId == triggerAppId
                               select beaconEvent;
                    return cols.ToList();
                }
            }
        }

        public List<BeaconEvent> GetBeaconEventList(int triggerAppId, string groupCode)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = from beaconEvent in db.BeaconEventEntities
                              join beaconEventGroupLink in db.BeaconEventGroupLinkEntities on beaconEvent.EventId equals beaconEventGroupLink.EventId
                              join beaconGroup in db.BeaconGroupEntities on beaconEventGroupLink.GroupId equals beaconGroup.GroupId
                              where beaconGroup.GroupCode == groupCode && beaconGroup.TriggerAppId == triggerAppId
                              select beaconEvent;
                    return one.Distinct().ToList();
                }
            }
        }

        public List<BeaconEvent> GetBeaconEventList(int triggerAppId, string groupCode, string auxillaryCode)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = from beaconEvent in db.BeaconEventEntities
                              join beaconEventGroupLink in db.BeaconEventGroupLinkEntities on beaconEvent.EventId equals beaconEventGroupLink.EventId
                              join beaconGroup in db.BeaconGroupEntities on beaconEventGroupLink.GroupId equals beaconGroup.GroupId
                              where beaconGroup.GroupCode == groupCode && beaconGroup.AuxiliaryCode == auxillaryCode && beaconGroup.TriggerAppId == triggerAppId
                              select beaconEvent;
                    return one.Distinct().ToList();
                }
            }
        }

        public List<BeaconEvent> GetBeaconEventList(int triggerAppId, string groupCode, List<string> auxillaryCodes)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var one = from beaconEvent in db.BeaconEventEntities
                              join beaconEventGroupLink in db.BeaconEventGroupLinkEntities on beaconEvent.EventId equals beaconEventGroupLink.EventId
                              join beaconGroup in db.BeaconGroupEntities on beaconEventGroupLink.GroupId equals beaconGroup.GroupId
                              where beaconGroup.GroupCode == groupCode && auxillaryCodes.Contains(beaconGroup.AuxiliaryCode) && beaconGroup.TriggerAppId == triggerAppId
                              select beaconEvent;
                    return one.Distinct().ToList();
                }
            }
        }


        #endregion

        #region BeaconEventTargetEntity
        public BeaconEventTarget GetBeaconEventTarget(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var col = from target in db.BeaconEventTargetEntities
                               where eventId == target.EventId
                               select target;

                    return col.FirstOrDefault();
                }
            }
        }

        public List<BeaconEventTarget> GetBeaconEventTargetList(List<int> eventIds)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from target in db.BeaconEventTargetEntities
                               where eventIds.Contains(target.EventId)
                               select target;

                    return cols.ToList();
                }
            }
        }

        public int GetBeaconEventTargetCount(List<int> eventIds)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var totalCount = (from target in db.BeaconEventTargetEntities
                               where eventIds.Contains(target.EventId)
                               select target).Count();

                    return totalCount;
                }
            }
        }


        public bool DeleteBeaconEventTargetByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventTargetEntities.Where(p => p.EventId == eventId);
                db.BeaconEventTargetEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertOrUpdateBeaconEventTarget(BeaconEventTarget beaconEventTarget)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconEventTarget).State = beaconEventTarget.TargetId == 0 ?
                                             EntityState.Added :
                                             EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }
        #endregion

        #region BeaconLog
        public List<BeaconLog> GetBeaconLogList(int triggerAppId, int userId, int? appDeviceId, DateTime queryDate)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    DateTime startDate = queryDate.Date;
                    DateTime endDate = queryDate.Date.AddDays(1);

                    var cols = from log in db.BeaconLogEntities
                               where log.TriggerAppId == triggerAppId
                               && log.UserId == userId
                               && log.CreateDate >= startDate
                               && log.CreateDate < endDate
                               select log;
                
                    if (appDeviceId.HasValue)
                    {
                        cols = cols.Where(p => p.AppDeviceId == appDeviceId.Value);
                    }
                  
                    return cols.ToList();
                }
            }
        }

        public List<BeaconLog> GetBeaconLogListByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from log in db.BeaconLogEntities
                               where log.EventId == eventId
                               select log;

                    return cols.ToList();
                }
            }
        }


        public List<BeaconLog> GetBeaconLogList(int triggerAppId,int groupId, DateTime beginDate, DateTime endDate, int eventId, int userId, int deviceId, string orderByCol, bool orderByDesc)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var beaconLogList = from log in db.BeaconLogEntities
                                        where log.TriggerAppId == triggerAppId && log.GroupId == groupId && (log.CreateDate >= beginDate && log.CreateDate <= endDate)
                                        select log;

                    #region 資料篩選排序

                    if (eventId != 0)
                    {
                        beaconLogList = beaconLogList.Where(x => x.EventId == eventId);
                    }
                    if (userId != 0)
                    {
                        beaconLogList = beaconLogList.Where(x => x.UserId == userId);
                    }
                    if (deviceId != 0)
                    {
                        beaconLogList = beaconLogList.Where(x => x.BeaconDeviceId == deviceId);
                    }

                    switch (orderByCol)
                    {
                        case "rowDateEventTriggerDate":
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.CreateDate) : beaconLogList.OrderByDescending(x => x.CreateDate);
                            break;
                        case "rowDateEventSubject":
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.EventId) : beaconLogList.OrderByDescending(x => x.EventId);
                            break;
                        case "rowDateEventMember":
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.UserId) : beaconLogList.OrderByDescending(x => x.UserId);
                            break;
                        case "rowDateDeviceName":
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.BeaconDeviceId) : beaconLogList.OrderByDescending(x => x.BeaconDeviceId);
                            break;
                        case "rowDateTriggerAddress":
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.GroupId) : beaconLogList.OrderByDescending(x => x.GroupId);
                            break;
                        default:
                            beaconLogList = orderByDesc ? beaconLogList.OrderBy(x => x.Id) : beaconLogList.OrderByDescending(x => x.Id);
                            break;

                    }
                    #endregion
                    return beaconLogList.ToList();
                }
            }
        }

        public List<BeaconTriggerLogInfo> GetTriggerLogInfo(int triggerAppId, List<int> groups, string groupremark, DateTime beginDate,
            DateTime endDate, int eventId, int userId, int deviceId, BeaconIsMember beaconIsMember, string orderByCol, bool orderByDesc)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var beaconTriggerLogInfo = from log in db.BeaconLogEntities
                                               join beaconEvent in db.BeaconEventEntities on log.EventId equals beaconEvent.EventId
                                               join beaconDevice in db.BeaconDeviceEntities on log.BeaconDeviceId equals beaconDevice.DeviceId
                                               where log.TriggerAppId == triggerAppId && groups.Contains(log.GroupId) && (log.CreateDate >= beginDate && log.CreateDate <= endDate)
                                               select new BeaconTriggerLogInfo
                                               {
                                                   EventId = log.EventId,
                                                   GroupId = log.GroupId,
                                                   DeviceId = log.BeaconDeviceId,
                                                   DeviceName = beaconDevice.DeviceName,
                                                   UserId = log.UserId,
                                                   EventSubject = beaconEvent.Subject,
                                                   GroupRemark = groupremark,
                                                   CreateDate = log.CreateDate
                                               };

                    #region 資料篩選排序

                    if (eventId != 0)
                    {
                        beaconTriggerLogInfo = beaconTriggerLogInfo.Where(x => x.EventId == eventId);
                    }
                    if (userId != 0)
                    {
                        beaconTriggerLogInfo = beaconTriggerLogInfo.Where(x => x.UserId == userId);
                    }
                    if (deviceId != 0)
                    {
                        beaconTriggerLogInfo = beaconTriggerLogInfo.Where(x => x.DeviceId == deviceId);
                    }

                    switch (beaconIsMember)
                    {
                        case BeaconIsMember.IsMember:
                            beaconTriggerLogInfo = beaconTriggerLogInfo.Where(x => x.UserId != 0);
                            break;
                        case BeaconIsMember.IsNotMember:
                            beaconTriggerLogInfo = beaconTriggerLogInfo.Where(x => x.UserId == 0);
                            break;
                        case BeaconIsMember.all:
                        default:
                            break;
                    }

                    switch (orderByCol)
                    {
                        case "rowDateEventTriggerDate":
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.CreateDate) : beaconTriggerLogInfo.OrderByDescending(x => x.CreateDate);
                            break;
                        case "rowDateEventSubject":
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.EventId) : beaconTriggerLogInfo.OrderByDescending(x => x.EventId);
                            break;
                        case "rowDateEventMember":
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.UserId) : beaconTriggerLogInfo.OrderByDescending(x => x.UserId);
                            break;
                        case "rowDateDeviceName":
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.DeviceId) : beaconTriggerLogInfo.OrderByDescending(x => x.DeviceId);
                            break;
                        case "rowDateTriggerAddress":
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.GroupId) : beaconTriggerLogInfo.OrderByDescending(x => x.GroupId);
                            break;
                        default:
                            beaconTriggerLogInfo = orderByDesc ? beaconTriggerLogInfo.OrderBy(x => x.DeviceId) : beaconTriggerLogInfo.OrderByDescending(x => x.DeviceId);
                            break;

                    }
                    #endregion
                    
                    return beaconTriggerLogInfo.ToList();
                }
            }
        }

        public bool InsertOrUpdateBeaconLog(BeaconLog beaconLog)
        {
            using (var db = new BeaconDbContext())
            {
                try
                {
                    db.Entry(beaconLog).State = beaconLog.Id == 0 ?
                                           EntityState.Added :
                                           EntityState.Modified;
                    return db.SaveChanges() > 0;
                }
                catch (Exception e)
                {
                    return false;
                }
            }
        }
        #endregion

        #region Beacon Property
        public bool InsertOrUpdateSkmBeaconProperty(SkmBeaconProperty skmBeaconProperty)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(skmBeaconProperty).State = skmBeaconProperty.Id == 0 ?
                                             EntityState.Added :
                                             EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteSkmBeaconPropertyByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.SkmBeaconPropertyEntities.Where(p => p.EventId == eventId);
                db.SkmBeaconPropertyEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public SkmBeaconProperty GetSkmBeaconPropertyByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.SkmBeaconPropertyEntities.FirstOrDefault(x => x.EventId == eventId) ?? new SkmBeaconProperty();
                }
            }
        }
        #endregion

        #region Beacon UUID

        public List<BeaconTriggerAppUuid> GetBeaconTriggerAppUuidList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from u in db.BeaconTriggerAppUuidEntities
                               where u.TriggerAppId == triggerAppId
                               select u;
                    return cols.ToList();
                }
            }
        }

        #endregion

        #region BeaconGroupDeviceLink
        public List<BeaconGroupDeviceLink> GetBeaconGroupDeviceLinkList(int triggerAppId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               where groups.TriggerAppId == triggerAppId
                               select groupDevices;
                    return cols.ToList();
                }
            }
        }

        public List<BeaconGroupDeviceLink> GetBeaconGroupDeviceLinkList(int triggerAppId, int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from groups in db.BeaconGroupEntities
                               join groupDevices in db.BeaconGroupDeviceLinkEntities on groups.GroupId equals groupDevices.GroupId
                               where groups.TriggerAppId == triggerAppId && groupDevices.DeviceId == deviceId
                               select groupDevices;
                    return cols.ToList();
                }
            }
        }
        
        #endregion

        #region BeaconEventGroupLink

        public List<int> GetBeaconEventGroupListByGroupId(List<int> groupIds)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var col = from p in db.BeaconEventGroupLinkEntities
                              join e in db.BeaconEventEntities on p.EventId equals e.EventId
                              where groupIds.Contains(p.GroupId) && e.Enabled
                              select p.EventId;

                    return col.ToList();
                }
            }
        }

        public List<BeaconEventGroupLink> GetBeaconEventGroupList(int eventId, int groupId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    return db.BeaconEventGroupLinkEntities.Where(x => x.EventId == eventId && x.GroupId == groupId).ToList();
                }
            }
        }

        #endregion

        #region BeaconEventDeviceLink

        public List<int> GetBeaconEventDeviceLinkByDeviceId(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var col = from p in db.BeaconEventDeviceLinkEntities
                              join e in db.BeaconEventEntities on p.EventId equals e.EventId
                              where p.DeviceId == deviceId && e.Enabled 
                              select p.Id;

                    return col.ToList();
                }
            }
        }

        public List<BeaconEventDeviceLink> GetBeaconEventDeviceLink(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var col = from p in db.BeaconEventDeviceLinkEntities
                              join e in db.BeaconEventEntities on p.EventId equals e.EventId
                              where p.DeviceId == deviceId && e.Enabled
                              select p;

                    return col.ToList();
                }
            }
        }

        public bool InsertOUpdateBeaconEventDeviceLink(BeaconEventDeviceLink beaconEventDeviceLink)
        {
            using (var db = new BeaconDbContext())
            {
                db.Entry(beaconEventDeviceLink).State = beaconEventDeviceLink.Id == 0 ?
                                       EntityState.Added :
                                       EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }
        

        public bool DeleteBeaconEventDeviceLink(int deviceId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventDeviceLinkEntities.Where(p => p.DeviceId == deviceId);
                db.BeaconEventDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }


        public bool DeleteBeaconEventDeviceLinkByEventId(int eventId)
        {
            using (var db = new BeaconDbContext())
            {
                var cols = db.BeaconEventDeviceLinkEntities.Where(p => p.EventId == eventId);
                db.BeaconEventDeviceLinkEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region 混合
        public List<BeaconMessageModel> GetBeaconAppSubeventList(int eventId, int memeberId)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var data = from sev in db.BeaconSubeventEntities
                               join dp in db.DevicePushRecordEntities on sev.SubeventId equals dp.ActionId
                               where sev.EventId == eventId && dp.MemberId == memeberId && dp.IsRemove == false
                               select new BeaconMessageModel
                               {
                                   Id = dp.Id,
                                   Subject = sev.Subject,
                                   Content = sev.Content,
                                   ActionUrl = sev.ActionUrl,
                                   ActionBid = sev.ActionBid,
                                   CreateTime = dp.CreateTime,
                                   EventType = sev.EventType,
                                   IsRead = dp.IsRead,
                                   IsRemove = dp.IsRemove,
                                   MemberId = dp.MemberId.Value,
                               };


                    return data.ToList();
                }
            }
        }

        public List<BeaconMessageModel> GetBeaconAppEventList(int memeberId,DateTime dueDate)
        {
            using (var db = new BeaconDbContext())
            {
                using (db.NoLock())
                {
                    var data = from dp in db.DevicePushRecordEntities
                               join ev in db.BeaconEventEntities on dp.ActionId equals ev.EventId into evd
                               from leftEvd in evd.DefaultIfEmpty()
                               join sev in db.BeaconSubeventEntities on dp.ActionId equals sev.SubeventId into sevd
                               from leftSevd in sevd.DefaultIfEmpty()
                               where dp.MemberId == memeberId && dp.CreateTime >= dueDate && !(leftEvd == null && leftSevd == null)  && dp.IsRemove == false
                               select new BeaconMessageModel
                               {
                                   Id = dp.Id,
                                   Subject = (leftSevd == null ? leftEvd.Subject : leftSevd.Subject),
                                   Content = leftSevd == null ? leftEvd.Content : leftSevd.Content,
                                   ActionUrl = leftSevd == null ? leftEvd.ActionUrl : leftSevd.ActionUrl,
                                   ActionBid = leftSevd == null ? leftEvd.ActionBid : leftSevd.ActionBid,
                                   CreateTime = dp.CreateTime,
                                   EventType = leftSevd == null ? leftEvd.EventType : leftSevd.EventType,
                                   IsRead = dp.IsRead,
                                   IsRemove = dp.IsRemove,
                                   MemberId = dp.MemberId.Value ,
                               };


                    return data.ToList();
                }
            }
        }
        #endregion
    }
}

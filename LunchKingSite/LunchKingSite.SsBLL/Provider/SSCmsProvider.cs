using System;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System.Data;
using LunchKingSite.Core.Component;

namespace LunchKingSite.SsBLL
{
    public class SSCmsProvider : ICmsProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public CmsContent CmsContentGet(string name)
        {
            return DB.Get<CmsContent>(CmsContent.Columns.ContentName, name);
        }

        public CmsContent CmsContentGet(int id)
        {
            return DB.Get<CmsContent>(CmsContent.Columns.ContentId, id);
        }

        public void CmsContentDelete(int id)
        {
            DB.Delete<CmsContent>(CmsContent.Columns.ContentId, id);
        }

        public void CmsContentSet(CmsContent content)
        {
            DB.Save<CmsContent>(content);
        }

        public CmsContentCollection CmsContentGetList(int pageStart, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CmsContent, CmsContentCollection>(pageStart, pageSize, orderBy, filter);
        }

        public bool AuditSet(Audit record)
        {
            DB.Save<Audit>(record);
            return true;
        }

        public Audit AuditGet(int id)
        {
            return DB.Get<Audit>(id);
        }

        public AuditCollection AuditGetList(string referenceId, bool showAll)
        {
            SqlQuery qry = Select.AllColumnsFrom<Audit>().Where(Audit.ReferenceIdColumn).IsEqualTo(referenceId).OrderDesc(Audit.Columns.CreateTime);
            if (!showAll)
                qry = qry.And(Audit.CommonVisibleColumn).IsEqualTo(true);
            return qry.ExecuteAsCollection<AuditCollection>();
        }

        public AuditCollection AuditGetListByIdAndType(string referenceId, AuditType auditType)
        {
            return DB.SelectAllColumnsFrom<Audit>().NoLock()
                .Where(Audit.ReferenceIdColumn).IsEqualTo(referenceId)
                .And(Audit.ReferenceTypeColumn).IsEqualTo((int)auditType)
                .ExecuteAsCollection<AuditCollection>();
        }

        #region Schema
        public string SchemaGetVersion()
        {
            return
                new InlineQuery(DB.Repository.Provider).ExecuteScalar<string>(
                    "SELECT ISNULL(value,'') FROM fn_listextendedproperty(@name, default, default, default, default, default, default)",
                    "Version");
        }

        public int SchemaGetBranchVersion()
        {
            int? result = new InlineQuery(DB.Repository.Provider).ExecuteScalar<int?>(
                "SELECT ISNULL(value,'') FROM fn_listextendedproperty(@name, default, default, default, default, default, default)",
                "branch_version");
            return result.GetValueOrDefault();
        }
        #endregion

        #region UserTrack
        public bool UserTrackSet(UserTrack track)
        {
            DB.Save(track);
            return true;
        }

        public bool UserTrackSet(string sessionId, string displayName, string url, string queryString, string context)
        {
            UserTrack userTrack = new UserTrack { SessionId = sessionId, DisplayName = displayName, Url = url, Querystring = queryString, Context = context, CreateDate = DateTime.Now };
            return UserTrackSet(userTrack);
        }
        #endregion

        #region CmsRadomContent
        public void CmsRandomContentDelete(int id)
        {
            DB.Update<CmsRandomContent>().Set(CmsRandomContent.StatusColumn).EqualTo(0).Where(CmsRandomContent.IdColumn)
                            .IsEqualTo(id).Execute();
        }
        public void CmsRandomContentSet(CmsRandomContent content)
        {
            DB.Save<CmsRandomContent>(content);
        }
        public void CmsRandomCitySet(CmsRandomCity city)
        {
            DB.Save<CmsRandomCity>(city);
        }
        public void CmsRandomCityDelete(int id)
        {
            DB.Delete<CmsRandomCity>(CmsRandomCity.Columns.Id, id);
        }
        public void UpdateCmsRandomContentStatus(int brandId, bool status)
        {
            DB.Update<CmsRandomContent>().Set(CmsRandomContent.StatusColumn).EqualTo(status).Where(CmsRandomContent.BrandIdColumn)
                            .IsEqualTo(brandId).Execute();
        }
        public DataTable CmsRandomContentGetTitleNId(string contentname, RandomCmsType type)
        {
            string strSQL = "select " + CmsRandomContent.IdColumn + "," + CmsRandomContent.TitleColumn + " from " + CmsRandomContent.Schema.TableName + " where "
                + CmsRandomContent.ContentNameColumn + "=@contentname and isnull(" + CmsRandomContent.ModifiedByColumn + ",'')<>N'KILLER' and " + CmsRandomContent.Columns.Type + "=@type";
            IDataReader idr = new InlineQuery().ExecuteReader(strSQL, contentname, (int)type);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }
        public ViewCmsRandomCollection GetViewCmsRandomCollection(string contentname, RandomCmsType type)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " with(nolock) where " + ViewCmsRandom.Columns.BannerStatus + "=1 and " +
                ViewCmsRandom.Columns.Status + "=1 and getdate()<=" + ViewCmsRandom.Columns.EndTime + " and getdate()>=" + ViewCmsRandom.Columns.StartTime +
                " and " + ViewCmsRandom.Columns.ContentName + "=@contentname and " + ViewCmsRandom.Columns.Type + "=@type";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@contentname", contentname, DbType.String);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        /// <summary>
        /// 預先抓之後addminute對應的CmsRandom
        /// </summary>
        /// <param name="contentname"></param>
        /// <param name="type"></param>
        /// <param name="addminute"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        public ViewCmsRandomCollection GetViewCmsRandomCollection(string contentname, RandomCmsType type ,int addminute)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " with(nolock) where " + ViewCmsRandom.Columns.BannerStatus + "=1 "
                + " and " +ViewCmsRandom.Columns.Status + "=1 and " + ViewCmsRandom.Columns.Type + "=@type and " + ViewCmsRandom.Columns.ContentName + "=@contentname "
                + " and ((getdate()<=" + ViewCmsRandom.Columns.EndTime + " and getdate()>=" + ViewCmsRandom.Columns.StartTime +") or ("
                + " DATEADD(minute," + addminute + ",GETDATE())<=" + ViewCmsRandom.Columns.EndTime + " and DATEADD(minute," + addminute + ",GETDATE())>=" + ViewCmsRandom.Columns.StartTime + "))";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@contentname", contentname, DbType.String);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public ViewCmsRandomCollection GetViewCmsRandomCollection(RandomCmsType type)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.BannerStatus + "=1 and " +
                ViewCmsRandom.Columns.Status + "=1 and getdate()<=" + ViewCmsRandom.Columns.EndTime + " and " + ViewCmsRandom.Columns.Type + "=@type";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        /// <summary>
        /// 撈出符合type的全部資料
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public ViewCmsRandomCollection GetViewCmsRandomCollectionAll(RandomCmsType type)
        {
            if ((int)type < (int)RandomCmsType.VBO)
            {
                throw new Exception("給 VBO 使用.");
            }
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.Type + "=@type";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public void CmsRandomContentUpdate(CmsRandomContent content)
        {
            DB.Update<CmsRandomContent>().Set(CmsRandomContent.TitleColumn).EqualTo(content.Title)
                .Set(CmsRandomContent.ContentTitleColumn).EqualTo(content.ContentTitle)
                .Set(CmsRandomContent.BodyColumn).EqualTo(content.Body)
                .Set(CmsRandomContent.ModifiedByColumn).EqualTo(content.ModifiedBy)
                .Set(CmsRandomContent.ModifiedOnColumn).EqualTo(content.ModifiedOn)
                .Set(CmsRandomContent.StatusColumn).EqualTo(content.Status)
                .Where(CmsRandomContent.IdColumn).IsEqualTo(content.Id).Execute();
        }
        public void CmsRandomCityUpdate(CmsRandomCity city)
        {
            DB.Update<CmsRandomCity>().Set(CmsRandomCity.RatioColumn).EqualTo(city.Ratio)
                   .Set(CmsRandomCity.StatusColumn).EqualTo(city.Status)
                   .Set(CmsRandomCity.StartTimeColumn).EqualTo(city.StartTime)
                   .Set(CmsRandomCity.EndTimeColumn).EqualTo(city.EndTime)
                   .Where(CmsRandomCity.IdColumn).IsEqualTo(city.Id).Execute();
        }
        public void CmsRandomSortUpdate(CmsRandomCity city)
        {
            DB.Update<CmsRandomCity>().Set(CmsRandomCity.SeqColumn).EqualTo(city.Seq)
                   .Where(CmsRandomCity.IdColumn).IsEqualTo(city.Id).Execute();
        }
        public CmsRandomContent CmsRandomContentGetById(int id)
        {
            return DB.Get<CmsRandomContent>(id);
        }
        public CmsRandomCity CmsRandomCityGetById(int id)
        {
            return DB.Get<CmsRandomCity>(id);
        }
        public CmsRandomContent CmsRandomContentGetByEventPromoId(int eventId)
        {
            return DB.Get<CmsRandomContent>(CmsRandomContent.Columns.EventPromoId, eventId);
        }
        public CmsRandomContent CmsRandomContentGetByBrandId(int brandId)
        {
            return DB.Get<CmsRandomContent>(CmsRandomContent.Columns.BrandId, brandId);
        }
        public CmsRandomCityCollection CmsRandomCityGetByPid(int pid, RandomCmsType type)
        {
            string sql = " select * from " + CmsRandomCity.Schema.TableName + " where " + CmsRandomCity.PidColumn + "=@pid and " + CmsRandomCity.TypeColumn + "=@type";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@pid", pid, System.Data.DbType.Int32);
            qc.AddParameter("@type", (int)type, System.Data.DbType.Int32);
            CmsRandomCityCollection data = new CmsRandomCityCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        /// <summary>
        /// 只取第一筆，給Type = 3(VBO) 使用
        /// 因為 VBO 裡，random_content 跟 random_city 是1對1關係
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public CmsRandomCity CmsRandomCityGetByPidNType(int pid, int type)
        {
            string sql = " select * from " + CmsRandomCity.Schema.TableName + " where " +
                CmsRandomCity.PidColumn + "=@pid and " + CmsRandomContent.Columns.Type + "=@type";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@pid", pid, DbType.Int32);
            qc.AddParameter("@type", type, DbType.Int32);
            CmsRandomCityCollection data = new CmsRandomCityCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data[0];
        }
        public void CmsRandomCityDelete(int pid, int type)
        {
            DB.Delete().From(CmsRandomCity.Schema.TableName).Where(CmsRandomCity.Columns.Pid).IsEqualTo(pid).And(CmsRandomCity.Columns.Type).IsEqualTo((int)type).Execute();
        }
        public ViewCmsRandomCollection GetViewCmsRandomCollectionByTitleAndDate(string title, DateTime? startdate, DateTime? enddate, RandomCmsType type)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.Type + "=@type and " + ViewCmsRandom.Columns.Status + "=@status ";
            //if (!string.IsNullOrEmpty(title))
            sql += " and " + ViewCmsRandom.Columns.Title + " like @title ";
            if (startdate != null && enddate != null)
                sql += " and " + ViewCmsRandom.Columns.EndTime + ">=@startdate and " + ViewCmsRandom.Columns.EndTime + "<=@enddate ";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@status", true, DbType.Boolean);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            //if (!string.IsNullOrEmpty(title))
            qc.AddParameter("@title", "%" + title + "%", DbType.String);
            if (startdate != null && enddate != null)
            {
                qc.AddParameter("@startdate", startdate.Value, DbType.DateTime);
                qc.AddParameter("@enddate", enddate.Value, DbType.DateTime);
            }
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public ViewCmsRandomCollection GetViewCmsRandomCollectionByPid(int pid, RandomCmsType type)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.Type + "=@type and " + ViewCmsRandom.Columns.Pid + "=@pid";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            qc.AddParameter("@pid", pid, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public ViewCmsRandomCollection GetViewCmsRandomCollectionByCityIdDate(DateTime deliverydate, int cityid, RandomCmsType type)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.Type + "=@type  and " +
                ViewCmsRandom.Columns.EndTime + ">@deliverydate and " + ViewCmsRandom.Columns.StartTime + "<=@deliverydate and " + ViewCmsRandom.Columns.CityId + "=@cityid";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@deliverydate", deliverydate, DbType.DateTime);
            qc.AddParameter("@cityid", cityid, DbType.Int32);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewCmsRandomCollection GetViewCmsRandomCollectionByCityIdDateNoPast(string contentname, DateTime deliverydate, int cityid, RandomCmsType type)
        {  

            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.Type + "=@type  and " +
                ViewCmsRandom.Columns.EndTime + ">@deliverydate and (" + ViewCmsRandom.Columns.StartTime + "<=@deliverydate or " + ViewCmsRandom.Columns.StartTime + ">=@deliverydate)  and " +
                ViewCmsRandom.Columns.CityId + "=@cityid and " + CmsRandomContent.ContentNameColumn + "=@contentname";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@deliverydate", deliverydate, DbType.DateTime);
            qc.AddParameter("@cityid", cityid, DbType.Int32);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            qc.AddParameter("@contentname", contentname, DbType.String);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewCmsRandomCollection ViewCmsRandomGetCollectionByEventPromoId(int eventPromoId)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.EventPromoId + "=@eventPromoId";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);           
            qc.AddParameter("@eventPromoId", eventPromoId, DbType.Int32);           
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewCmsRandomCollection ViewCmsRandomGetCollectionByBrandId(int brandId)
        {
            string sql = " select * from " + ViewCmsRandom.Schema.TableName + " where " + ViewCmsRandom.Columns.BrandId + "=@brandId";
            QueryCommand qc = new QueryCommand(sql, CmsRandomCity.Schema.Provider.Name);
            qc.AddParameter("@brandId", brandId, DbType.Int32);
            ViewCmsRandomCollection data = new ViewCmsRandomCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public List<int> GetBrandListBySearch(string searchWord, string searchCityId, RandomCmsType type)
        {
            int cityId = 0;
            string sql = "";
            QueryCommand qc;
            if (int.TryParse(searchCityId, out cityId) && cityId > 0) //查單一頻道
            {
                sql = " select distinct brand_id from " + ViewCmsRandom.Schema.TableName + " with(nolock) where " + ViewCmsRandom.Columns.Type + "=@type ";
                if (!string.IsNullOrWhiteSpace(searchWord))
                    sql += " and (" + ViewCmsRandom.Columns.Title + " like @searchTitle or " + ViewCmsRandom.Columns.BrandId + " = @brandId ) ";
                if (int.TryParse(searchCityId, out cityId) && cityId > 0)
                    sql += " and " + ViewCmsRandom.Columns.CityId + " =@cityId";

                int bid = 0;
                int.TryParse(searchWord, out bid);

                qc = new QueryCommand(sql, ViewCmsRandom.Schema.Provider.Name);
                qc.AddParameter("@type", (int)type, DbType.Int32);
                if (!string.IsNullOrWhiteSpace(searchWord))
                {
                    qc.AddParameter("@searchTitle", "%" + searchWord + "%", DbType.String);
                    qc.AddParameter("@brandId", bid, DbType.Int32);
                }
                if (int.TryParse(searchCityId, out cityId) && cityId > 0)
                    qc.AddParameter("@cityId", cityId, DbType.Int32);
            }
            else //查全頻道
            {
                sql = " select brand_id from " + CmsRandomContent.Schema.TableName + " with(nolock) where " + CmsRandomContent.Columns.Type + "=@type ";
                if (!string.IsNullOrWhiteSpace(searchWord)) {
                    sql += " and (" + CmsRandomContent.Columns.Title + " like @searchTitle or " + CmsRandomContent.Columns.BrandId + " = @brandId ) ";
                }

                int bid = 0;
                int.TryParse(searchWord, out bid);

                qc = new QueryCommand(sql, CmsRandomContent.Schema.Provider.Name);
                qc.AddParameter("@type", (int)type, DbType.Int32);
                if (!string.IsNullOrWhiteSpace(searchWord))
                {
                    qc.AddParameter("@searchTitle", "%" + searchWord + "%", DbType.String);
                    qc.AddParameter("@brandId", bid, DbType.Int32);
                }
            }

            List<int> result = new List<int>();
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                int brandId = 0;
                int.TryParse(row["brand_id"].ToString(), out brandId);

                if (brandId > 0)
                    result.Add(brandId);
            }
            return result;
        }
        #endregion
    }
}

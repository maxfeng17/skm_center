﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class PosProvider : IPosProvider
    {
        public ChannelPosApiLog SavePosApiLog(ChannelPosApiLog log)
        {
            using (var db = new ChannelDbContext())
            {
                db.Entry(log).State = log.Id == 0 ? System.Data.Entity.EntityState.Added : System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
                return log;
            }
        }
    }
}

using System;
using System.Linq;
using System.Collections;
using System.Data;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using System.Web.UI.WebControls;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using Microsoft.SqlServer.Types;
using System.Data.SqlTypes;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.SsBLL
{
    public class SSLocationProvider : ILocationProvider
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SSLocationProvider));


        public bool BuildingDeliveryDelete(string column, object value)
        {
            DB.Destroy<BuildingDelivery>(column, value);
            return true;
        }
        public bool BuildingDeliveryDelete(int id)
        {
            try
            {
                DB.Destroy<BuildingDelivery>(BuildingDelivery.Columns.Id, id);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public BuildingDelivery BuildingDeliveryGet(Guid bizHourGuid, Guid buildingGuid)
        {
            return DB.SelectAllColumnsFrom<BuildingDelivery>()
                .Where(BuildingDelivery.BusinessHourGuidColumn).IsEqualTo(bizHourGuid)
                .And(BuildingDelivery.BuildingGuidColumn).IsEqualTo(buildingGuid)
                .ExecuteSingle<BuildingDelivery>();
        }

        public BuildingDeliveryCollection BuildingDeliveryGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<BuildingDelivery>().Where(column).IsEqualTo(value).ExecuteAsCollection<BuildingDeliveryCollection>();
        }

        public bool BuildingDeliverySet(BuildingDelivery bd)
        {
            bool ret = true;
            try
            {
                DB.Save<BuildingDelivery>(bd);
            }
            catch
            {
                ret = false;
            }
            return ret;
        }

        public bool BuildingSet(Building bd)
        {
            DB.Save<Building>(bd);
            return true;
        }

        public bool BuildingDelete(Guid g)
        {
            try
            {
                DB.Destroy<Building>(Building.Columns.Guid, g);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public BuildingCollection BuildingGetList()
        {
            return DB.SelectAllColumnsFrom<Building>().ExecuteAsCollection<BuildingCollection>();
        }

        public BuildingCollection BuildingGetListByArea(int area_id)
        {
            return DB.SelectAllColumnsFrom<Building>()
                .Where(Building.CityIdColumn).IsEqualTo(area_id)
                .OrderAsc(Building.Columns.BuildingRank)
                .ExecuteAsCollection<BuildingCollection>();
        }

        public BuildingCollection BuildingGetListByName(string buildingName)
        {
            return DB.SelectAllColumnsFrom<Building>()
                .Where(Building.BuildingNameColumn).IsEqualTo(buildingName)
                .OrderAsc(Building.Columns.BuildingRank)
                .ExecuteAsCollection<BuildingCollection>();
        }

        public Building BuildingGet(Guid g)
        {
            return DB.Get<Building>(g);
        }

        public Building BuildingGet(string column, object value)
        {
            return DB.Get<Building>(column, value);
        }

        public bool CitySet(City c)
        {
            DB.Save<City>(c);
            return true;
        }

        public bool CityDelete(int id)
        {
            try
            {
                DB.Destroy<City>(City.Columns.Id, id);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public City CityGet(int id)
        {
            return CityGet(id, false);
        }

        public City CityGet(int id, bool appendParentName)
        {
            City ret = DB.Get<City>(id);
            if (ret.IsLoaded && appendParentName && (ret.ParentId != null))
            {
                City city = DB.Get<City>(ret.ParentId);

                if (city.IsLoaded)
                    ret.CityName = city.CityName + ret.CityName;
            }

            return ret;
        }

        public City CityGet(string code)
        {
            return DB.Get<City>(code);
        }

        public City CityGetByCodeWithParentIsNull(string code)
        {
            SqlQuery s = new Select().From<City>().Where(City.ParentIdColumn).IsNull().And(City.CodeColumn).IsEqualTo(code);
            CityCollection citys = s.ExecuteAsCollection<CityCollection>();
            return (citys.Count > 0) ? citys[0] : new City();
        }

        public CityCollection CityGetList(int parentId)
        {
            return CityGetList(parentId, City.Columns.Rank);
        }

        public CityCollection CityGetList(int parentId, string orderBy)
        {
            SqlQuery s = new Select().From<City>().Where(City.ParentIdColumn).IsEqualTo(parentId);
            s.OrderBys.Add(orderBy);
            return s.ExecuteAsCollection<CityCollection>();
        }

        public CityCollection CityGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<City, CityCollection>(1, -1, null, filter);
        }

        public CityCollection CityGetListSetOrder(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<City, CityCollection>(1, -1, orderBy, filter);
        }

        public CityCollection CityGetListTopLevel()
        {
            return DB.SelectAllColumnsFrom<City>()
                .Where(City.ParentIdColumn).IsNull().OrderAsc(City.Columns.Rank)
                .ExecuteAsCollection<CityCollection>();
        }

        public CityCollection CityGetListTopLevel(CityStatus status)
        {
            return CityGetListTopLevel(status, -1);
        }

        public CityCollection CityGetListTopLevel(CityStatus status, int topCount)
        {
            Select s = DB.SelectAllColumnsFrom<City>();
            if (topCount >= 0)
                s = s.Top(topCount.ToString());
            var q = s.Where(City.ParentIdColumn).IsNull();
            if (status == CityStatus.Nothing)
                q = q.AndExpression(City.StatusColumn.ColumnName).IsNull().Or(City.StatusColumn).IsEqualTo((int)status);
            else
                q = q.And(City.StatusColumn).IsEqualTo((int)status);
            return q.OrderAsc(City.Columns.Rank).ExecuteAsCollection<CityCollection>();
        }

        public CityCollection CityGetListTopLevelForCompany(CityStatus status, int topCount)
        {
            Select s = DB.SelectAllColumnsFrom<City>();
            if (topCount >= 0)
                s = s.Top(topCount.ToString());
            var q = s.Where(City.StatusColumn).IsEqualTo((int)status);
            return q.OrderDesc(City.Columns.Code).ExecuteAsCollection<CityCollection>();
        }

        /// <summary>
        /// 查詢城市列表，依據目前上架中的熟客卡可用分店判斷
        /// </summary>
        /// <returns></returns>
        public CityCollection CityGetListForMembershipCardStore()
        {
            string sql = @"select * from city with (nolock) where id in (
select distinct c.parent_id city_id from (
select * from membership_card_group_store  with (nolock) where card_group_id in 
(select distinct card_group_id from membership_card with (nolock) where status = 1
and enabled = 1)
) ms
join seller s with (nolock)
on ms.store_guid = s.guid
join city c with (nolock) on c.id = s.store_township_id
)
order by rank ";

            var qc = new QueryCommand(sql, City.Schema.Provider.Name);

            var data = new CityCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #region BuildingGetListInZoneAndPrefix
        public ListItemCollection BuildingGetListInZoneAndPrefix(int city_id, string prefix)
        {

            ListItemCollection llCol = new ListItemCollection();
            ViewBuildingCityCollection bCol;

            if (prefix != null) prefix = prefix.Trim();

            bCol = BuildingController.GetBuildingWithinZone(city_id, prefix);
            llCol = new ListItemCollection();
            foreach (ViewBuildingCity building in bCol)
            {
                string text = building.BuildingName;
                if (!string.IsNullOrEmpty(building.BuildingAddressNumber))
                    text += " (" + building.BuildingAddressNumber + ")";
                llCol.Add(new ListItem(text, building.Guid.ToString()));
            }
            return llCol;
        }
        #endregion

        #region CityGetListFromAvailBuilding
        public CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName, bool includeTop, CityStatus status)
        {
            string sql = @"select c.* from city as c where id in (select city_id from building as b) {0} order by c.rank";
            CityCollection cCol;
            if (status != CityStatus.Nothing)
                cCol = new InlineQuery().ExecuteAsCollection<CityCollection>(string.Format(sql, @"and status=@st"), (int)status);
            else
                cCol = new InlineQuery().ExecuteAsCollection<CityCollection>(string.Format(sql, @"and (status is null or status=@st)"), (int)status);

            if (cCol.Count > 0 && appendParentName != AppendMode.None)
            {
                CityCollection topCities = CityGetListTopLevel();
                foreach (City c in cCol)
                    if (appendParentName == AppendMode.All || (c.Status == null || c.Status != (int)CityStatus.Virtual)) // append parent name for physical districts or all if mode = all
                        c.CityName = topCities[topCities.Find("Id", c.ParentId)].CityName + c.CityName;
            }
            if (includeTop)
                cCol.AddRange(CityGetListTopLevel());

            return cCol;

        }

        public CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName, bool includeTop)
        {
            return CityGetListFromAvailBuilding(appendParentName, includeTop, CityStatus.Nothing);
        }

        public CityCollection CityGetListFromAvailBuilding(AppendMode appendParentName)
        {
            return CityGetListFromAvailBuilding(appendParentName, false);
        }

        #endregion

        public ViewBuildingCity ViewBuildingCityGet(Guid g)
        {
            ViewBuildingCity view = new ViewBuildingCity();
            view.LoadByParam(ViewBuildingCity.Columns.Guid, g);
            return view;
        }

        public ViewBuildingCityCollection ViewBuildingCityGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewBuildingCity, ViewBuildingCityCollection>(pageStart, pageLength, orderBy, filter);
        }

        public ViewBuildingCityCollection ViewBuildingCityGetList(string column, object value, string orderBy)
        {
            ViewBuildingCityCollection view = new ViewBuildingCityCollection();
            view.LoadAndCloseReader(ViewBuildingCity.Query().WHERE(column, value).ORDER_BY(orderBy).ExecuteReader());
            return view;
        }

        public ViewBuildingCityCollection ViewBuildingCityGetListByBusinessHour(int pageStart, int pageLength, string orderBy, Guid bizHourGuid, ShowListMode mode, SqlGeography geo, int distance, params string[] filter)
        {
            if (mode == ShowListMode.All)
                return ViewBuildingCityGetListPaging(pageStart, pageLength, orderBy, filter);

            QueryCommand qc = SSHelper.GetWhereQC<ViewBuildingCity>(filter);
            string sqlCond = (mode == ShowListMode.Inclusive) ? "IN" : "NOT IN";
            qc.CommandSql = qc.CommandSql + " and (" + ViewBuildingCity.Columns.Guid + " " + sqlCond + " (SELECT building_GUID FROM building_delivery WHERE (business_hour_GUID = @bhGuid))) ";
            qc.CommandSql = "select " + ViewBuildingCity.Schema.GetDelimitedColumnList(",", false, false) + " from " + ViewBuildingCity.Schema.Provider.DelimitDbName(ViewBuildingCity.Schema.TableName) + qc.CommandSql;

            if (distance > 0)
            {
                // we'll piggyback distance into building_rank, bad design i know but we are using subsonic...
                string coorStr = "(Geography::Point(" + geo.Lat + ", " + geo.Long + ", 4326).STDistance(coordinate))";
                qc.CommandSql = qc.CommandSql.Replace(",building_rank,", "," + coorStr + " AS building_rank,");
                qc.CommandSql += " and " + coorStr + " < @distance ";
                orderBy = coorStr;
                qc.AddParameter("@distance", distance, DbType.Int32);
            }

            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
            qc.AddParameter("@bhGuid", bizHourGuid, DbType.Guid);




            //temporarilly add sql this way for calculating distance, should be refined at a later time.
            //if (distance > 0 && geo.Lat != SqlDouble.Null)


            qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderBy);
            ViewBuildingCityCollection vbcc = new ViewBuildingCityCollection();
            vbcc.Load(DataService.GetReader(qc));
            return vbcc;
        }

        public ViewBuildingSellerCountCollection ViewBuildingSellerCountGetList(string column, object value, string orderBy)
        {
            ViewBuildingSellerCountCollection view = new ViewBuildingSellerCountCollection();
            view.LoadAndCloseReader(ViewBuildingSellerCount.Query().WHERE(column, value).ORDER_BY(orderBy).ExecuteReader());
            return view;
        }

        public string CityNameGetWithSubscription(int cityId)
        {
            City city = CityGet(cityId);
            string rtnCityName = string.Empty;

            switch (city.Code)
            {
                case "TP":
                case "TA":
                    rtnCityName = I18N.Phrase.PponCityTaipeiCity;
                    break;
                case "TY":
                    rtnCityName = I18N.Phrase.PponCityTaoyuan;
                    break;
                case "XZ":
                    rtnCityName = I18N.Phrase.PponCityHsinchu;
                    break;
                case "TC":
                    rtnCityName = I18N.Phrase.PponCityTaichung;
                    break;
                case "TN":
                    rtnCityName = I18N.Phrase.PponCityTainan;
                    break;
                case "GX":
                    rtnCityName = I18N.Phrase.PponCityKaohsiung;
                    break;
                case "ALL":
                    rtnCityName = I18N.Phrase.PponCityAllCountry;
                    break;
                case "TRA":
                    rtnCityName = I18N.Phrase.PponCityTravel;
                    break;
                case "PBU":
                    rtnCityName = I18N.Phrase.PponCityPeauty;
                    break;
            }

            return rtnCityName;
        }

        #region WeChat
        public void WeChatLocationSet(WechatLocation wechat_location)
        {
            DB.Save<WechatLocation>(wechat_location);
        }

        public WechatLocation WeChatLocationGet(string open_id)
        {
            return DB.Get<WechatLocation>(WechatLocation.Columns.OpenId, open_id);
        }
        #endregion

        #region DeviceIdentifierInfo

        public int? DeviceIdentyfierInfoGetCurrentDeviceId(string identifierCode)
        {
            var sql = @"select Top 1 id from device_identyfier_info with(nolock) 
                where identifier_code = @code order by id desc";
            var qc = new QueryCommand(sql, DeviceIdentyfierInfo.Schema.Provider.Name);
            qc.AddParameter("@code", identifierCode, DbType.AnsiString);
            object result = DataService.ExecuteScalar(qc);
            if (result == null || result == DBNull.Value)
            {
                return null;
            }
            return (int)result;
        }

        public DeviceIdentyfierInfo DeviceIdentyfierInfoGet(string identifierCode)
        {
            return DB.SelectAllColumnsFrom<DeviceIdentyfierInfo>()
                   .Where(DeviceIdentyfierInfo.Columns.IdentifierCode).IsEqualTo(identifierCode)
                   .ExecuteSingle<DeviceIdentyfierInfo>();
        }

        public DeviceIdentyfierInfoCollection DeviceIdentyfierInfoListGet(string identifierCode)
        {
            string sql = string.Format("select * from {0} with(nolock) where {1}=@identifierCode",
                DeviceIdentyfierInfo.Schema.TableName, 
                DeviceIdentyfierInfo.Columns.IdentifierCode);
            QueryCommand qc = new QueryCommand(sql, DeviceIdentyfierInfo.Schema.Provider.Name);
            qc.AddParameter("@identifierCode", identifierCode, DbType.AnsiString);
            var result = new DeviceIdentyfierInfoCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public bool DeviceIdentyfierInfoSet(DeviceIdentyfierInfo deviceIdentyfierInfo)
        {
            DB.Save<DeviceIdentyfierInfo>(deviceIdentyfierInfo);
            return true;
        }

        public DevicePushRecordCollection DevicePushRecordListGet(int deviceid, int? memberUniqueId)
        {
            if (memberUniqueId.GetValueOrDefault() > 0)
            {
                return DB.SelectAllColumnsFrom<DevicePushRecord>()
                    .Where(DevicePushRecord.Columns.MemberId).IsEqualTo(memberUniqueId.GetValueOrDefault())
                    .Or(DevicePushRecord.Columns.DeviceId).IsEqualTo(deviceid)
                    .Or(DevicePushRecord.Columns.DeviceId).IsEqualTo(0)
                    .ExecuteAsCollection<DevicePushRecordCollection>();
            }
            else
            {
                return DB.SelectAllColumnsFrom<DevicePushRecord>().Where(DevicePushRecord.Columns.DeviceId).IsEqualTo(deviceid)
                    .ExecuteAsCollection<DevicePushRecordCollection>();
            }

            //return new DevicePushRecordCollection();
        }

        public DevicePushRecordCollection DevicePushRecordGetListByDeviceId(int deviceid)
        {
            return DB.SelectAllColumnsFrom<DevicePushRecord>().Where(DevicePushRecord.Columns.DeviceId).IsEqualTo(deviceid)
                    .ExecuteAsCollection<DevicePushRecordCollection>();
        }

        public ViewPushRecordMessageCollection ViewPushRecordMessageGetListByDeviceAndNoOwner(int deviceid, int days)
        {
            return DB.SelectAllColumnsFrom<ViewPushRecordMessage>().NoLock()
                .Where(ViewPushRecordMessage.Columns.DeviceId).IsEqualTo(deviceid)
                .And(ViewPushRecordMessage.Columns.PushTime).IsGreaterThan(DateTime.Now.AddDays(-days))
                .And(ViewPushRecordMessage.Columns.UserId).IsNull()
                .And(ViewPushRecordMessage.Columns.IsRemove).IsEqualTo(false)
                .ExecuteAsCollection<ViewPushRecordMessageCollection>();
        }

        public ViewPushRecordMessageCollection ViewPushRecordMessageGetListByMember(int userId, int days)
        {
            return DB.SelectAllColumnsFrom<ViewPushRecordMessage>().NoLock()
                .And(ViewPushRecordMessage.Columns.UserId).IsEqualTo(userId)
                .And(ViewPushRecordMessage.Columns.PushTime).IsGreaterThan(DateTime.Now.AddDays(-days))                
                .And(ViewPushRecordMessage.Columns.IsRemove).IsEqualTo(false)
                .ExecuteAsCollection<ViewPushRecordMessageCollection>();
        }

        public void DevicePushRecordSetRemove(int devicePushRecordId)
        {
            DB.Update<DevicePushRecord>()
                .Set(DevicePushRecord.Columns.IsRemove).EqualTo(true)
                .Set(DevicePushRecord.Columns.RemoveTime).EqualTo(DateTime.Now)
                .Where(DevicePushRecord.Columns.Id).IsEqualTo(devicePushRecordId).Execute();
        }

        public int DevicePushRecordSetMemberIfNoOwner(int deviceId, int userId)
        {
            return DB.Update<DevicePushRecord>().Set(DevicePushRecord.Columns.MemberId).EqualTo(userId)
                .Where(DevicePushRecord.Columns.DeviceId).IsEqualTo(deviceId)
                .And(DevicePushRecord.Columns.MemberId).IsNull().Execute();
        }

        public DevicePushRecordCollection SkmDevicePushRecordListGet(int memberUniqueId)
        {
                return DB.SelectAllColumnsFrom<DevicePushRecord>()
                .Where(DevicePushRecord.Columns.MemberId).IsEqualTo(memberUniqueId)
                .ExecuteAsCollection<DevicePushRecordCollection>();
        }


        public DevicePushRecordCollection DevicePushRecordGetListByUserId(int memberUniqueId)
        {
            return DB.SelectAllColumnsFrom<DevicePushRecord>().Where(DevicePushRecord.Columns.MemberId).IsEqualTo(memberUniqueId)
            .ExecuteAsCollection<DevicePushRecordCollection>();
        }

        public DevicePushRecord DevicePushRecordGet(int devicePushRecordId)
        {
            return DB.Get<DevicePushRecord>(DevicePushRecord.Columns.Id, devicePushRecordId);
        }

        public int DevicePushRecordSetNotExist(DevicePushRecord record)
        {
            string dt = string.Format("{0:yyyy-MM-dd HH:mm:ss:fff}", DateTime.Now);

            string sql = string.Format("Insert into device_push_record (device_id,member_id,identifier_type,action_id,create_time,trigger_type,is_deal) " +
                        "SELECT top 1 {0}, {1}, {2}, {3}, '{4}', {5}, {6} " +
                        "FROM device_push_record t1 " +
                        "WHERE NOT EXISTS(SELECT 1 FROM device_push_record t2 WHERE t2.device_id = {7} and t2.member_id = {8} and t2.identifier_type = {9} and t2.action_id = {10} )" +
                        "SELECT @@IDENTITY as id ",
                        record.DeviceId, record.MemberId, record.IdentifierType, record.ActionId, dt, record.TriggerType, (record.IsDeal ? 1 : 0), record.DeviceId, record.MemberId, record.IdentifierType, record.ActionId);
        
            QueryCommand qc = new QueryCommand(sql, RoleMember.Schema.Provider.Name);
            var newId = qc.Provider.ExecuteScalar(qc);

            return (newId == DBNull.Value) ? 0 : Convert.ToInt32(newId);
        }

        public bool DevicePushRecordSet(DevicePushRecord devicePushRecord)
        {
            DB.Save<DevicePushRecord>(devicePushRecord);
            return true;
        }
        public int DevicePushRecordCollectionSet(DevicePushRecordCollection devicePushRecordCollection)
        {
            return DB.SaveAll(devicePushRecordCollection);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="deviceId"></param>
        /// <param name="hourVal"></param>
        /// <returns>PushRecordId, ActionId, PushTime, ReadTime</returns>
        public List<DevicePushRecordReadModel> DevicePushRecordGetReadRecordWithInHour(int userId, int deviceId,
            int hourVal)
        {
            //            string sql = @"
            //select 
            //dpr.id, 
            //dpr.action_id, 
            //pa.id as push_app_id,
            //dpr.create_time, 
            //dpr.read_time
            //    from device_push_record dpr with(nolock) 							
            //							inner join push_app pa on pa.action_event_push_message_id = dpr.action_id
            //                            where 
            //							member_id = @userId and device_id = @deviceId 
            //							and read_time > DATEADD(HH, @hour, GETDATE()) 
            //                            order by read_time
            //";
            string sql = @"
select 
dpr.id, 
dpr.action_id, 
pa.id as push_app_id,
dpr.create_time, 
dpr.read_time
    from device_push_record dpr with(nolock) 							
							inner join push_app pa on pa.action_event_push_message_id = dpr.action_id
                            where 
							member_id = @userId  
							and read_time > DATEADD(HH, @hour, GETDATE()) 
                            order by read_time
";
            var qc = new QueryCommand(sql, DevicePushRecord.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            //qc.AddParameter("@deviceId", deviceId, DbType.Int32);
            qc.AddParameter("@hour", -hourVal, DbType.Int32);

            var result = new List<DevicePushRecordReadModel>();

            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(new DevicePushRecordReadModel
                    {
                        RecordId = dr.GetInt32(0),
                        ActionId = dr.GetInt32(1),
                        PushAppId = dr.GetInt32(2),
                        CreateTime = dr.GetDateTime(3),
                        ReadTime = dr.GetDateTime(4),
                    });
                }
            }

            return result;
        }

        #endregion DeviceIdentifierInfo

        #region DevicePushOrder

        public bool DevicePushOrderSet(DevicePushOrder order)
        {
            order.CreateTime = DateTime.Now;
            return DB.Save(order) > 0;
        }

        public bool TryGetDevicePushOrderInfo(int psuhAppId, out int orderCount, out int turnover)
        {
            //            string sql = @"
            //select 
            //	count(o.GUID) as order_count, ISNULL(sum(o.turnover),0) as turnover 
            //from [device_push_order] dpr with(nolock)
            //	inner join [order] o with(nolock) on o.GUID = dpr.order_guid
            //where 
            //	dpr.last_push_app_id = @pushAppId
            //	and o.order_status & 8 = 8 
            //	and o.order_status & 512 = 0 
            //";

            //修改營業額
            string sql = @"
select 
	count(o.GUID) as order_count,  ISNULL(sum(ctl.amount - ctl.discount_amount), 0) as turnover 
from [device_push_order] dpr with(nolock)
	inner join [order] o with(nolock) on o.GUID = dpr.order_guid
    inner join cash_trust_log ctl with(nolock) on o.GUID = ctl.order_guid 
where 
	dpr.last_push_app_id = @pushAppId
	and o.order_status & 8 = 8 
	and o.order_status & 512 = 0 
";

            QueryCommand qc = new QueryCommand(sql, DevicePushOrder.Schema.Provider.Name);
            qc.AddParameter("@pushAppId", psuhAppId, DbType.Int32);

            using (var dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    orderCount = dr.GetInt32(0);
                    turnover = dr.GetInt32(1);
                    return true;
                }
            }
            orderCount = 0;
            turnover = 0;
            return false;
        }

        #endregion DevicePushOrder

        #region mrt_location_info (MRT)

        public MrtLocationInfoCollection MrtLocationInfoCollectionGetNearByLocation(double latitude, double longitude)
        {
            var result = new MrtLocationInfoCollection();

            string sql = string.Format(@"
              select * from mrt_location_info where 1 > ([coordinate].STDistance (geography::STGeomFromText('POINT ({0} {1})', 4326)))/1000 ", longitude, latitude);

            sql += string.Format("order by [coordinate].STDistance (geography::STGeomFromText('POINT ({0} {1})', 4326))"
                , longitude, latitude);

            QueryCommand qc = new QueryCommand(sql, MrtLocationInfo.Schema.Provider.Name);

            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;

        }

        #endregion mrt_location_info (MRT)
    }
}

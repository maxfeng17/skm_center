﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EntityFramework.BulkInsert.Extensions;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class SkmEfProvider : ISkmEfProvider
    {
        #region SkmMember

        public int SaveSkmTokenTracking(SkmTokenTracking skmTokenTracking)
        {
            skmTokenTracking.CreateTime = DateTime.Now;
            using (var db = new SkmDbContext())
            {
                db.Entry(skmTokenTracking).State = skmTokenTracking.Id == 0
                    ? System.Data.Entity.EntityState.Added
                    : System.Data.Entity.EntityState.Modified;

                return db.SaveChanges();
            }
        }

        #endregion

        #region SkmActivivy

        public SkmActivity GetSkmActivity(string token)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    DateTime dt = DateTime.Now;
                    var a = (from s in db.SkmActivityEntities
                             where s.Token == token && s.StartDate <= dt && s.EndDate > dt
                             select s).FirstOrDefault();
                    return a ?? new SkmActivity();
                }
            }
        }

        public SkmActivity GetSkmActivityWithJoinDate(string token)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    DateTime dt = DateTime.Now;
                    var a = (from s in db.SkmActivityEntities
                             where s.Token == token && s.JoinStartDate <= dt && s.JoinEndDate > dt
                             select s).FirstOrDefault();
                    return a ?? new SkmActivity();
                }
            }
        }

        #endregion SkmActivivy

        #region SkmActivityItem

        public List<SkmActivityItem> GetSkmActivityItem(int activityId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.SkmActivityItemEntities
                                 where i.ActivityId == activityId
                                 select i).ToList();
                    return items;
                }
            }
        }

        public SkmActivityItem GetSkmActivityItem(int activityId, Guid bid)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from i in db.SkmActivityItemEntities
                                where i.ActivityId == activityId && i.Bid == bid
                                select i).FirstOrDefault();
                    return item ?? new SkmActivityItem();
                }
            }
        }

        #endregion SkmActivityItem

        #region SkmActivityDateRange

        public List<SkmActivityDateRange> GetSkmActivityDateRange(int activityId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.SkmActivityDateRangeEntities
                                 where i.ActivityId == activityId
                                 select i).OrderBy(x => x.Id).ToList();
                    return items;
                }
            }
        }

        public SkmActivityDateRange GetSkmActivityDateRangeNow(int activityId)
        {
            DateTime dt = DateTime.Now;
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from i in db.SkmActivityDateRangeEntities
                                where i.ActivityId == activityId && dt >= i.DateRangeStart && dt <= i.DateRangeEnd
                                select i).FirstOrDefault() ?? new SkmActivityDateRange();
                    return item;
                }
            }
        }

        public List<int> GetSkmActivityDateShowRangeNow(int activityId)
        {
            DateTime dt = DateTime.Now;
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from i in db.SkmActivityDateRangeEntities
                                where i.ActivityId == activityId && dt >= i.DateRangeStart && dt <= i.DateRangeEnd
                                select i.Id).ToList();
                    return item;
                }
            }
        }

        #endregion SkmActivityDateRange

        #region SkmActivityLog

        public List<SkmActivityLog> GetSkmActivityLog(int activityId, int userId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.SkmActivityLogEntities
                                 where i.ActivityId == activityId && i.UserId == userId
                                 select i).ToList();
                    return items;
                }
            }
        }

        public List<SkmActivityLog> GetSkmActivityLogByUserId(int userId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.SkmActivityLogEntities
                                 where i.UserId == userId
                                 select i).ToList();
                    return items;
                }
            }
        }

        public bool SaveSkmActivityLog(SkmActivityLog log)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmActivityLogEntities.Add(log);
                return db.SaveChanges() > 0;
            }
        }

        public List<ViewSkmActivityLog> GetViewSkmActivityLog(int activityId, int userId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.ViewSkmActivityLog
                                 where i.ActivityId == activityId && i.UserId == userId
                                 select i).ToList();
                    return items;
                }
            }
        }

        public List<ViewSkmActivityLog> GetViewSkmActivityLog(int activityId, int userId, DateTime minDate, DateTime maxDate)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from i in db.ViewSkmActivityLog
                                 where i.ActivityId == activityId && i.UserId == userId
                                 && i.CreateTime >= minDate && i.CreateTime <= maxDate
                                 select i).ToList();
                    return items;
                }
            }
        }

        #endregion SkmActivityLog

        #region skm_pay_banner

        public List<SkmPayBanner> GetSkmPayBannerList()
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.SkmPayBanner
                            orderby b.Id descending
                            select b).ToList();
                }
            }
        }

        public SkmPayBanner GetSkmPayBanner(int id)
        {
            using (var db = new SkmDbContext())
            {
                return (from b in db.SkmPayBanner
                        where b.Id == id
                        select b).SingleOrDefault();
            }
        }

        public bool SetSkmPayBanner(SkmPayBanner banner, string userName)
        {
            banner.ModifyTime = DateTime.Now;
            banner.ModifyUser = userName;
            using (var db = new SkmDbContext())
            {
                db.Entry(banner).State = banner.Id == 0
                    ? System.Data.Entity.EntityState.Added
                    : System.Data.Entity.EntityState.Modified;

                return db.SaveChanges() > 0;
            }
        }

        public bool DelSkmPayBanner(int id)
        {
            using (var db = new SkmDbContext())
            {
                var banner = GetSkmPayBanner(id);
                if (banner == null)
                {
                    return true;
                }

                db.SkmPayBanner.Remove(banner);
                return db.SaveChanges() > 0;
            }
        }

        public List<SkmPayBanner> GetSkmPayBannerSortData()
        {
            DateTime now = DateTime.Now;
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.SkmPayBanner
                            where b.EndTime > now
                            orderby b.Seq
                            select b).ToList();
                }
            }
        }

        /// <summary>
        /// 取得今日SKM Pay Banner
        /// </summary>
        /// <returns></returns>
        public List<SkmPayBanner> GetSkmPayBannerToday()
        {
            DateTime now = DateTime.Now;
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.SkmPayBanner
                            where b.IsHidden == false && !string.IsNullOrEmpty(b.BannerPath)
                                                      && b.StartTime < now && b.EndTime > now
                            orderby b.Seq
                            select b).ToList();
                }
            }
        }

        #endregion skm_pay_banner

        #region Skm_pay_order
        /// <summary>
        /// 設定skm pay訂單資料
        /// </summary>
        /// <param name="skmPayOrder">要設定的skm pay訂單資料</param>
        /// <returns>設定後的skm pay訂單資料</returns>
        public bool SetSkmPayOrder(SkmPayOrder skmPayOrder)
        {
            using (var db = new SkmDbContext())
            {
                if (skmPayOrder.Id == 0)
                {
                    db.Entry(skmPayOrder).State = System.Data.Entity.EntityState.Added;
                    skmPayOrder.CreateDate = DateTime.Now;
                }
                else
                {
                    db.Entry(skmPayOrder).State = System.Data.Entity.EntityState.Modified;
                    skmPayOrder.LastModifyDate = DateTime.Now;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(skmPayOrder).Reload();
                return returnValue;
            }
        }

        public bool SetSkmPayOrders(List<SkmPayOrder> skmPayOrders)
        {
            using (var db = new SkmDbContext())
            {
                bool returnValue = true;
                foreach (var skmPayOrder in skmPayOrders)
                {
                    if (skmPayOrder.Id == 0)
                    {
                        db.Entry(skmPayOrder).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        db.Entry(skmPayOrder).State = System.Data.Entity.EntityState.Modified;
                    }
                    bool isSuccess = db.SaveChanges() > 0;
                    if (isSuccess)
                        db.Entry(skmPayOrder).Reload();
                    returnValue = returnValue & isSuccess;
                }
                return returnValue;
            }        
        }

        public SkmPayOrder GetSkmPayOrderByOrderNo(string orderNo)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.SkmPayOrder.FirstOrDefault(x => x.OrderNo == orderNo);
                }
            }
        }

        public ViewSkmPayOrder GetViewSkmPayOrderByOrderNo(string orderNo, string shopCode)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.ViewSkmPayOrder.FirstOrDefault(x => x.OrderNo == orderNo && x.ShopCode == shopCode);
                }
            }
        }

        public ViewSkmPayOrder GetViewSkmPayOrderByTradeNo(string tradeNo, string shopCode)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.ViewSkmPayOrder.FirstOrDefault(
                        x => (x.SkmTradeNo == tradeNo || x.SkmRefundTradeNo == tradeNo) && x.ShopCode == shopCode);
                }
            }
        }

        public SkmPayOrder GetSkmPayOrderByPreTransNo(string preTransNo)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayOrder.FirstOrDefault(x => x.PreTransNo == preTransNo.ToString());
            }
        }

        public SkmPayOrder GetSkmPayOrderByOrderGuid(Guid orderGuid)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.SkmPayOrder.FirstOrDefault(x => x.OrderGuid == orderGuid);
                }
            }
        }

        public bool SetSkmPayOrderTransLog(SkmPayOrderTransLog transLog)
        {
            using (var db = new SkmDbContext())
            {
                if (transLog.Id == 0)
                {
                    db.Entry(transLog).State = System.Data.Entity.EntityState.Added;
                    transLog.CreateTime = DateTime.Now;
                }
                else
                {
                    db.Entry(transLog).State = System.Data.Entity.EntityState.Modified;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(transLog).Reload();
                return returnValue;
            }
        }

        /// <summary>
        /// 依TrustId取得Skm Pay訂單資訊
        /// </summary>
        /// <param name="trustId">TrustId</param>
        /// <returns>Skm Pay訂單資訊</returns>
        public SkmPayOrder GetSkmPayOrderByTrustId(Guid trustId)
        {
            using (var db = new SkmDbContext())
            {
                var detail = GetskmPayOrderDetailByTrustId(trustId);
                if (detail == null || detail.Id == 0)
                {
                    return new SkmPayOrder();
                }

                using (db.NoLock())
                {
                    var data = (from s in db.SkmPayOrder
                                where s.Id == detail.SkmPayOrderId
                                select s).FirstOrDefault();
                    return data ?? new SkmPayOrder();
                }
            }
        }

        /// <summary>
        /// 依交易TOKEN取得SkmPay訂單資訊
        /// </summary>
        /// <param name="paymentToken">交易TOKEN</param>
        /// <returns>SkmPay訂單資訊</returns>
        public SkmPayOrder GetSkmPayOrderByPaymentToken(string paymentToken)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var data = (from s in db.SkmPayOrder
                                where s.PaymentToken == paymentToken
                                select s).FirstOrDefault();
                    return data ?? new SkmPayOrder();
                }
            }
        }

        public ViewSkmPayOrderQueryUmallGift GetViewSkmPayOrderQueryUmallGift(string orderNo)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.ViewSkmPayOrderQueryUmallGift.FirstOrDefault(x => x.OrderNo == orderNo);
                }
            }
        }

        /// <summary>
        /// 依訂單狀態清單取得訂單資料清單
        /// </summary>
        /// <param name="skmPayOrderFaildNeedRetryStatus">訂單狀態清單</param>
        /// <returns>訂單資料清單</returns>
        public List<SkmPayOrder> GetSkmPayOrdersByOrderStatus(List<SkmPayOrderStatus> skmPayOrderStatus)
        {
            using (var db = new SkmDbContext())
            {
                List<int> statusList = skmPayOrderStatus.Select(item => (int)item).ToList();
                return db.SkmPayOrder.Where(item => statusList.Any(status => status == item.Status)).ToList();
            }
        }

        /// <summary>
        /// 取得OTP逾期訂單
        /// </summary>
        /// <param name="timeoutSeconds"></param>
        /// <returns></returns>
        public List<SkmPayOrder> GetSkmPayOrdersByOtpTimeout(int timeoutSeconds)
        {
            using (var db = new SkmDbContext())
            {
                var date = DateTime.Now.AddSeconds(-timeoutSeconds);
                return db.SkmPayOrder
                    .Where(x => x.Status == (int)SkmPayOrderStatus.WaitingOTPFinished && x.CreateDate < date)
                    .ToList();
            }
        }
        #endregion

        #region skm_pay_parking_order 停車

        public bool SetSkmPayParkingOrder(SkmPayParkingOrder parkingOrder)
        {
            using (var db = new SkmDbContext())
            {
                if (parkingOrder.Id == 0)
                {
                    db.Entry(parkingOrder).State = System.Data.Entity.EntityState.Added;
                    parkingOrder.CreateDate = DateTime.Now;
                }
                else
                {
                    db.Entry(parkingOrder).State = System.Data.Entity.EntityState.Modified;
                    parkingOrder.LastModifyDate = DateTime.Now;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(parkingOrder).Reload();
                return returnValue;
            }
        }

        /// <summary>
        /// 依交易TOKEN取得SkmPay訂單資訊
        /// </summary>
        /// <param name="paymentToken">交易TOKEN</param>
        /// <returns>SkmPay訂單資訊</returns>
        public SkmPayParkingOrder GetSkmParkingOrderByPaymentToken(string paymentToken)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var data = (from s in db.SkmPayParkingOrder
                                where s.WalletPaymentToken == paymentToken
                                select s).FirstOrDefault();
                    return data ?? new SkmPayParkingOrder();
                }
            }
        }

        public SkmPayParkingOrder GetSkmParkingOrderByPreTransNo(string preTransNo)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayParkingOrder.FirstOrDefault(x => x.WalletPreTransNo == preTransNo.ToString());
            }
        }

        public SkmPayParkingOrder GetSkmParkingOrder(string orderNo, string parkingTicketNo)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayParkingOrder.FirstOrDefault(x => x.OrderNo == orderNo
                                                                 && x.ParkingTicketNo == parkingTicketNo);
            }
        }


        public List<SkmPayParkingOrder> GetSkmParkingOrderByIds(List<int> orderIds)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayParkingOrder.Where(x => orderIds.Contains(x.Id)).ToList();
            }
        }

        /// <summary>
        /// 依訂單狀態清單取得訂單資料清單
        /// </summary>
        /// <param name="skmPayOrderFaildNeedRetryStatus">訂單狀態清單</param>
        /// <returns>訂單資料清單</returns>
        public List<SkmPayParkingOrder> GetSkmPayParkingOrdersByOrderStatus(List<SkmPayOrderStatus> orderStatusList
        , string memberCardNo = null
        , string shopCode = null
        , string parkingTicketNo = null
        , string parkingToken = null
        , string sd = null
        , string ed = null)
        {
            using (var db = new SkmDbContext())
            {
                List<int> statusList = orderStatusList.Select(item => (int)item).ToList();
                var dbData = db.SkmPayParkingOrder.Where(item => statusList.Any(status => status == item.Status));
                if (!string.IsNullOrEmpty(memberCardNo))
                {
                    dbData = dbData.Where(p => p.MemberCardNo == memberCardNo);
                }
                if (!string.IsNullOrEmpty(shopCode))
                {
                    dbData = dbData.Where(p => p.ShopCode == shopCode);
                }
                if (!string.IsNullOrEmpty(parkingTicketNo))
                {
                    dbData = dbData.Where(p => p.ParkingTicketNo == parkingTicketNo);
                }
                if (!string.IsNullOrEmpty(parkingToken))
                {
                    dbData = dbData.Where(p => p.ParkingToken == parkingToken);
                }
                if (!string.IsNullOrEmpty(sd))
                {
                    DateTime sdate = DateTime.Parse(sd);
                    dbData = dbData.Where(p => p.CreateDate >= sdate);
                }
                if (!string.IsNullOrEmpty(ed))
                {
                    DateTime edate = DateTime.Parse(ed);
                    dbData = dbData.Where(p => p.CreateDate <= edate);
                }

                return dbData.ToList();
            }
        }


        #endregion

        #region  skm_pay_parking_tran_log


        public bool SetSkmPayParkingOrderTransLog(SkmPayParkingOrderTransLog transLog)
        {
            using (var db = new SkmDbContext())
            {
                if (transLog.Id == 0)
                {
                    db.Entry(transLog).State = System.Data.Entity.EntityState.Added;
                    transLog.CreateTime = DateTime.Now;
                }
                else
                {
                    db.Entry(transLog).State = System.Data.Entity.EntityState.Modified;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(transLog).Reload();
                return returnValue;
            }
        }

        public SkmPayParkingOrderTransLog GetSkmPayParkingOrderTransLog(int orderId, int userId, string action)
        {
            using (var db = new SkmDbContext())
            {
                //針對最新的一筆log去修正
                return db.SkmPayParkingOrderTransLog.OrderByDescending(p => p.Id)
                    .FirstOrDefault(p => p.OrderId == orderId && p.UserId == userId && p.Action == action);
            }
        }

        public List<SkmPayParkingOrderTransLog> GetSkmPayParkingOrderTransLogList(string action, bool isSuccess)
        {
            using (var db = new SkmDbContext())
            {
                //針對最新的一筆log去修正
                return db.SkmPayParkingOrderTransLog
                    .Where(p => p.IsSuccess == isSuccess && p.Action.Contains(action)).ToList();
            }
        }

        #endregion

        #region Skm_Pay_Order_Detail
        /// <summary> 設定skm pay訂單細項資料清單 </summary>
        /// <param name="skmPayOrderDetails">skm pay訂單細項資料清單</param>
        public bool SetSkmPayOrderDetails(IEnumerable<SkmPayOrderDetail> skmPayOrderDetails)
        {
            using (var db = new SkmDbContext())
            {
                bool returnValue = true;
                foreach (SkmPayOrderDetail skmPayOrderDetail in skmPayOrderDetails)
                {
                    if (skmPayOrderDetail.Id == 0)
                    {
                        db.Entry(skmPayOrderDetail).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        db.Entry(skmPayOrderDetail).State = System.Data.Entity.EntityState.Modified;
                    }
                    bool isSuccess = db.SaveChanges() > 0;
                    if (isSuccess)
                        db.Entry(skmPayOrderDetail).Reload();
                    returnValue = returnValue & isSuccess;
                }
                return returnValue;
            }
        }

        public SkmPayOrderDetail GetskmPayOrderDetailByTrustId(Guid trustId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    var data = (from i in db.SkmPayOrderDteailEntities
                                where i.TrustId == trustId
                                select i).FirstOrDefault();
                    return data ?? new SkmPayOrderDetail();
                }
            }
        }

        #endregion

        #region skm_pay_invoice
        /// <summary>
        /// 設定skm pay發票資料
        /// </summary>
        /// <param name="skmPayInvoice">要設定的skm pay發票資料</param>
        /// <returns>設定後的skm pay發票資料</returns>
        public bool SetSkmPayInvoice(SkmPayInvoice skmPayInvoice)
        {
            using (var db = new SkmDbContext())
            {
                if (skmPayInvoice.Id == 0)
                {
                    db.Entry(skmPayInvoice).State = System.Data.Entity.EntityState.Added;
                    skmPayInvoice.CreateDate = DateTime.Now;
                }
                else
                {
                    db.Entry(skmPayInvoice).State = System.Data.Entity.EntityState.Modified;
                    skmPayInvoice.LastModifyDate = DateTime.Now;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(skmPayInvoice).Reload();
                return returnValue;
            }
        }

        public List<SkmPayInvoice> GetSkmPayInvoiceList(List<int> ids, string platFormId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from i in db.SkmPayInvoice
                            where ids.Contains(i.SkmPayOrderId)
                            && i.PlatFormId == platFormId
                            select i).ToList();
                }
            }
        }

        public List<SkmPayInvoice> GetSkmPayInvoice(int skmPayOrderId, string platFormId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from i in db.SkmPayInvoice
                            where i.SkmPayOrderId == skmPayOrderId
                                  && i.PlatFormId == platFormId
                            select i).ToList();
                }
            }
        }

        /// <summary>
        /// 取得銷售日期為傳入日期且傳送成功發票資訊清單(預售&退貨)(精選優惠)
        /// PlatFormId&SellTranstype和停車不同
        /// </summary>
        /// <param name="sellStrtDate">要取得的開始日期</param>
        /// <param name="sellEndDate">要取得的結束日期</param>
        /// <returns>發票資訊清單</returns>
        public List<ViewSkmPayClearPos> GetSentSuccessSkmPayInvoiceBySellDate(DateTime sellStrtDate, DateTime sellEndDate)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmPayClearPosEntities.Where(item =>
                item.ReturnCode == "1" &&    //執行成功
                item.PlatFormId == "7" &&
                ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Sale && item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                 item.SellTranstype == (int)TurnCloudSellTransType.Refund) &&  //預售or退貨
                item.SellDay >= sellStrtDate && item.SellDay < sellEndDate).ToList();
            }
        }

        /// <summary>
        /// 取得銷售日期為傳入日期且傳送成功發票資訊清單(預售&退貨)(停車)
        /// PlatFormId&SellTranstype和精選優惠不同
        /// </summary>
        /// <param name="sellStrtDate">要取得的開始日期</param>
        /// <param name="sellEndDate">要取得的結束日期</param>
        /// <returns>發票資訊清單</returns>
        public List<ViewSkmPayClearPos> GetSentSuccessSkmPayParkingInvoiceBySellDate(DateTime sellStrtDate, DateTime sellEndDate)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmPayClearPosEntities.Where(item =>
                    item.ReturnCode == "1" &&   //執行成功
                    item.PlatFormId == "P" &&
                    ((item.PreSalesStatus == (int)TurnCloudPreDalesStatus.Default && item.SellTranstype == (int)TurnCloudSellTransType.Sell) ||
                     item.SellTranstype == (int)TurnCloudSellTransType.Refund) &&  //預售or退貨
                    item.SellDay >= sellStrtDate && item.SellDay < sellEndDate).ToList();
            }
        }
        #endregion

        #region skm_pay_invoice_sell_detail
        /// <summary>
        /// 新增/修改發票銷售明細
        /// </summary>
        /// <param name="skmPayInvoiceSellDetail">發票銷售明細</param>
        /// <returns>是否新增/修改成功</returns>
        public bool SetSkmPayInvoiceSellDetail(SkmPayInvoiceSellDetail skmPayInvoiceSellDetail)
        {
            using (var db = new SkmDbContext())
            {
                if (skmPayInvoiceSellDetail.Id == 0)
                {
                    db.Entry(skmPayInvoiceSellDetail).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    db.Entry(skmPayInvoiceSellDetail).State = System.Data.Entity.EntityState.Modified;
                }
                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(skmPayInvoiceSellDetail).Reload();
                return returnValue;
            }
        }

        /// <summary>
        /// 依照發票流水號取得發票銷售明細清單
        /// </summary>
        /// <param name="invoiceId">發票流水號</param>
        /// <returns>發票銷售明細清單</returns>
        public List<SkmPayInvoiceSellDetail> GetSkmPayInvoiceSellDetailsByInvoiceId(int invoiceId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayInvoiceSellDetail.Where(item => item.SkmPayInvoiceId == invoiceId).ToList();
            }
        }

        #endregion

        #region skm_pay_invoice_tender_detail
        /// <summary>
        /// 新增/修改發票付款明細清單
        /// </summary>
        /// <param name="skmPayInvoiceTenderDetails">發票付款明細清單</param>
        /// <returns>是否新增/修改成功</returns>
        public bool SetSkmPayInvoiceTenderDetail(IEnumerable<SkmPayInvoiceTenderDetail> skmPayInvoiceTenderDetails)
        {
            using (var db = new SkmDbContext())
            {
                bool returnValue = true;
                foreach (SkmPayInvoiceTenderDetail skmPayInvoiceTenderDetail in skmPayInvoiceTenderDetails)
                {
                    if (skmPayInvoiceTenderDetail.Id == 0)
                    {
                        db.Entry(skmPayInvoiceTenderDetail).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        db.Entry(skmPayInvoiceTenderDetail).State = System.Data.Entity.EntityState.Modified;
                    }
                    bool isSuccess = db.SaveChanges() > 0;
                    if (isSuccess)
                        db.Entry(skmPayInvoiceTenderDetail).Reload();
                    returnValue = returnValue & isSuccess;
                }
                return returnValue;
            }
        }

        /// <summary>
        /// 依照發票流水號取得發票付款明細清單
        /// </summary>
        /// <param name="invoiceId">發票流水號</param>
        /// <returns>發票付款明細清單</returns>

        public List<SkmPayInvoiceTenderDetail> GetSkmPayInvoiceTenderDetailsByInvoiceId(int invoiceId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayInvoiceTenderDetail.Where(item => item.SkmPayInvoiceId == invoiceId).ToList();
            }
        }
        #endregion

        #region skm_pay_clear_pos_trans_log
        /// <summary>
        /// 儲存 當日銷售總額 電文傳送紀錄
        /// </summary>
        /// <param name="skmPayClearPosTransLog">當日銷售總額 電文傳送紀錄</param>
        /// <returns>是否儲存成功</returns>
        public bool SetSkmPayClearPosTransLog(SkmPayClearPosTransLog skmPayClearPosTransLog)
        {
            using (var db = new SkmDbContext())
            {
                if (skmPayClearPosTransLog.Id == 0)
                {
                    db.Entry(skmPayClearPosTransLog).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    db.Entry(skmPayClearPosTransLog).State = System.Data.Entity.EntityState.Modified;
                }

                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(skmPayClearPosTransLog).Reload();
                return returnValue;
            }
        }
        
        /// <summary>
        /// 依銷售日期取得失敗的上傳當日銷售總額清單
        /// </summary>
        /// <param name="startDate">銷售日期起始</param>
        /// <param name="endDate">銷售日期結束</param>
        /// <param name="platFormId">哪個類型資料, 7=精選優惠 P=停車</param>
        /// <returns>上傳當日銷售總額清單</returns>
        public List<SkmPayClearPosTransLog> GetFailedSkmPayClearPosBySellDay(DateTime startDate, DateTime endDate, string platFormId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmPayClearPosTransLogEntities
                       .Where(item => !item.IsSuccess && !item.IsRetry
                                    && item.SellDay >= startDate && item.SellDay < endDate
                                    && item.PlatFormId == platFormId).ToList();
            }
        }
        
        /// <summary>
        /// 依銷售日期判斷是否有執行過上傳當日銷售總額
        /// </summary>
        /// <param name="sellDate">銷售日期</param>
        /// <returns></returns>
        public bool IsPostSkmPayClearPos(DateTime sellDate, string platFormId)
        {
            using (var db = new SkmDbContext())
            {
                DateTime startDate = sellDate;
                DateTime endDate = sellDate.AddDays(1);
                return db.SkmPayClearPosTransLogEntities
                       .Any(item => !item.IsRetry && item.SellDay >= startDate && item.SellDay < endDate && item.PlatFormId == platFormId);
            }
        }
        #endregion

        #region ExternalDealCombo

        public List<ExternalDealCombo> GetExternalDealComboByMainBids(List<Guid> bids)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.ExternalDealCombo
                            where bids.Contains(b.MainBid.Value) && b.IsDel == false
                            select b).ToList();
                }
            }
        }

        public bool SetExternalDealCombo(List<ExternalDealCombo> deals, bool isUpdate)
        {
            using (var db = new SkmDbContext())
            {
                foreach (var d in deals)
                {
                    db.Entry(d).State = isUpdate
                        ? System.Data.Entity.EntityState.Modified
                        : System.Data.Entity.EntityState.Added;
                }

                return db.SaveChanges() > 0;
            }
        }

        public List<ExternalDealCombo> GetExternalDealCombo(Guid externalGuid)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.ExternalDealCombo
                            where b.ExternalDealGuid == externalGuid
                            select b).ToList();
                }
            }
        }

        public ExternalDealCombo GetExternalDealComboByBid(Guid bid)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.ExternalDealCombo
                            where b.Bid == bid && b.IsDel == false
                            select b).FirstOrDefault();
                }
            }
        }

        public List<ExternalDealCombo> GetExternalDealComboByMainBid(Guid bid)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.ExternalDealCombo
                            where b.MainBid == bid && b.IsDel == false
                            select b).ToList();
                }
            }
        }

        /// <summary>
        /// 取得SKM檔次子項資料
        /// </summary>
        /// <param name="externalDealGuid">SKM檔次編號</param>
        /// <param name="shopCode">館號</param>
        /// <returns>SKM檔次子項資料</returns>
        public ExternalDealCombo GetExternalDealComboByGuidShopCode(Guid externalDealGuid, string shopCode)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from b in db.ExternalDealCombo
                            where b.ExternalDealGuid == externalDealGuid
                            && b.ShopCode == shopCode
                            && b.IsDel == false
                            select b).First();
                }
            }
        }

        /// <summary>
        /// 更新子檔次憑證數量
        /// </summary>
        /// <param name="externalDealGuid">檔次編號</param>
        /// <param name="shopCode">館別代號</param>
        /// <param name="orderTotalLimit">憑證數量</param>
        /// <returns>是否更新成功</returns>
        public bool SetExternalDealComboQty(Guid externalDealGuid, string shopCode, int orderTotalLimit)
        {
            using (var db = new SkmDbContext())
            {
                var externalDealCombo = db.ExternalDealCombo.Where(item => item.ExternalDealGuid == externalDealGuid && item.ShopCode == shopCode);
                externalDealCombo.ForEach(item =>
                {
                    item.Qty = orderTotalLimit;
                    db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                });
                return db.SaveChanges() > 0;
            }
        }
        #endregion ExternalDealCombo

        #region Skm_Pay_OTP
        /// <summary>
        /// 設定Skm Pay OTP資料
        /// </summary>
        /// <param name="skmPayOtp">Skm Pay OTP資料</param>
        /// <returns>是否設定成功</returns>
        public bool SetSkmPayOtp(SkmPayOtp skmPayOtp)
        {
            bool returnValue = false;
            using (var db = new SkmDbContext())
            {
                if (skmPayOtp.Id <= 0)
                {
                    db.Entry(skmPayOtp).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    db.Entry(skmPayOtp).State = System.Data.Entity.EntityState.Modified;
                }
                returnValue = db.SaveChanges() > 0;
            }
            return returnValue;
        }
        /// <summary>
        /// 依SkmPay訂單流水號取得Skm Pay Otp資料
        /// </summary>
        /// <param name="skmPayOrderId">SkmPay訂單流水號</param>
        /// <returns>Skm Pay Otp資料</returns>
        public SkmPayOtp GetSkmPayOtpBySkmPayOrderId(int skmPayOrderId)
        {
            using (var db = new SkmDbContext())
            {
                SkmPayOtp returnValue = db.SkmPayOtpEntities.FirstOrDefault(item => item.SkmPayOrderId == skmPayOrderId);
                return returnValue;
            }
        }
        #endregion

        #region skm_store_display_name
        /// <summary>
        /// 取得APP顯示館位名稱資料清單
        /// </summary>
        /// <returns>顯示館位名稱資料清單</returns>
        public List<ViewSkmStoreDisplayName> GetViewSkmStoreDisplayNames()
        {
            using (var db = new SkmDbContext())
            {
                List<ViewSkmStoreDisplayName> returnValue = db.ViewSkmStoreDisplayNameEntities.ToList();
                return returnValue;
            }
        }

        #endregion

        #region ViewExternalDealStoreDisplayName

        /// <summary>
        /// 依照檔次編號查詢APP顯示館名清單
        /// </summary>
        /// <param name="dealGuid">檔次編號</param>
        /// <returns>APP顯示館名清單</returns>
        public List<ViewExternalDealStoreDisplayName> GetViewExternalDealStoreDisplayNameByDealGuid(Guid dealGuid)
        {
            using (var db = new SkmDbContext())
            {
                List<ViewExternalDealStoreDisplayName> returnValue = db.ViewExternalDealStoreDisplayNameEntities.Where(item => item.ExternalDealGuid == dealGuid).ToList();
                return returnValue;
            }
        }

        #endregion

        #region skm_in_app_message

        public bool SetSkmInAppMessage(SkmInAppMessage msg)
        {
            using (var db = new SkmDbContext())
            {
                if (msg.Id == 0)
                {
                    msg.ModifyTime = msg.SendTime = DateTime.Now;
                    db.Entry(msg).State = System.Data.Entity.EntityState.Added;
                }
                else
                {
                    db.Entry(msg).State = System.Data.Entity.EntityState.Modified;
                }

                msg.ModifyTime = DateTime.Now;

                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(msg).Reload();
                return returnValue;
            }
        }

        public bool BulkInsertSkmInAppMessage(List<SkmInAppMessage> msgs)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmInAppMessage.AddRange(msgs);

                bool returnValue = db.SaveChanges() > 0;

                return returnValue;
            }
        }

        public List<SkmInAppMessage> GetSkmInAppMessage(int userId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return (from m in db.SkmInAppMessage
                            where m.UserId == userId && m.IsDel == false
                            orderby m.SendTime descending
                            select m).ToList();
                }
            }
        }

        public bool SetInAppMessageRead(int id)
        {
            using (var db = new SkmDbContext())
            {
                var msg = db.SkmInAppMessage.FirstOrDefault(x => x.Id == id);
                if (msg != null)
                {
                    msg.IsRead = true;
                    msg.ReadTime = DateTime.Now;

                    return SetSkmInAppMessage(msg);
                }
            }

            return false;
        }

        public bool SetInAppMessageDel(int id)
        {
            using (var db = new SkmDbContext())
            {
                var msg = db.SkmInAppMessage.FirstOrDefault(x => x.Id == id);
                if (msg != null)
                {
                    msg.IsDel = true;
                    msg.ModifyTime = DateTime.Now;
                    return SetSkmInAppMessage(msg);
                }
            }

            return false;
        }

        #endregion skm_in_app_message

        #region view_skm_ppon_order

        public List<ViewSkmPponOrder> GetViewSkmPponOrder(int userId, int expiredMonth)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    //排除檔次過期N個月
                    DateTime expiredDate = DateTime.Now.AddMonths(-expiredMonth);

                    var spOrderGuids = (from spo in db.SkmPayOrder
                                        where spo.UserId == userId
                                        select spo.OrderGuid).Distinct();

                    List<ViewSkmPponOrder> data = (from vpo in db.ViewSkmPponOrder
                                                   where vpo.UserId == userId && vpo.BusinessHourOrderTimeE >= expiredDate
                                                         && !spOrderGuids.Contains(vpo.Guid)
                                                   select vpo).ToList();

                    return data;
                }
            }
        }

        #endregion view_skm_ppon_order

        # region view_skm_pay_ppon_order

        public List<ViewSkmPayPponOrder> GetViewSkmPayPponOrder(int userId, int expiredMonth)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    //排除檔次過期N個月
                    DateTime expiredDate = DateTime.Now.AddMonths(-expiredMonth);

                    List<ViewSkmPayPponOrder> data = (from vpo in db.ViewSkmPayPponOrder
                                                      where vpo.UserId == userId
                                                          && vpo.CreateTime >= expiredDate
                                                      select vpo).ToList();

                    return data;
                }
            }
        }

        #endregion view_skm_pay_ppon_order

        #region skm_serial_number

        public SkmSerialNumber GetSkmSerialNumber(string storeFixcode)
        {
            using (var db = new SkmDbContext())
            {
                var sqlQuery = "sp_getSkmSerialNumber @storeFixcode";
                var sqlParams = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@storeFixcode",  Value = storeFixcode , Direction = System.Data.ParameterDirection.Input}
                };

                return db.Database.SqlQuery<SkmSerialNumber>(sqlQuery, sqlParams).SingleOrDefault();
            }
        }

        /// <summary>
        /// 依Skm Trade No取得還在交易中的Skm Serial Number info
        /// </summary>
        /// <param name="skmTradeNo">Skm Trade No</param>
        /// <returns>Skm Serial Number info</returns>
        public SkmSerialNumber GetProcessingSkmSerialNumberBySkmTradNo(string skmTradeNo)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmSerialNumber.Where(item => item.SkmTradeNo == skmTradeNo && item.Processing == true).FirstOrDefault();
            }
        }

        /// <summary>
        /// 依Skm Trade No取得Skm Serial Number info
        /// </summary>
        /// <param name="skmTradeNo">Skm Trade No</param>
        /// <returns>Skm Serial Number info</returns>
        public SkmSerialNumber GetSkmSerialNumberBySkmTradNo(string skmTradeNo)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmSerialNumber.Where(item => item.SkmTradeNo == skmTradeNo).FirstOrDefault();
            }
        }

        public bool UpdateSkmSerialNumber(int id, bool isProcessing, bool isTradeComplete)
        {
            using (var db = new SkmDbContext())
            {
                var sql = "Update skm_serial_number set processing = @isProcessing, is_trade_complete=@isTradeComplete, modify_time=getdate() Where id=@id";
                List<SqlParameter> para = new List<SqlParameter>();
                para.Add(new SqlParameter("@isProcessing", isProcessing ? 1 : 0));
                para.Add(new SqlParameter("@isTradeComplete", isTradeComplete ? 1 : 0));
                para.Add(new SqlParameter("@id", id));

                return db.Database.ExecuteSqlCommand(sql, para.ToArray()) > 0;
            }
        }

        /// <summary>
        /// 更新Skm Serial Number info
        /// </summary>
        /// <param name="skmSerialNumber">Skm Serial Number info</param>
        /// <returns>是否更新成功</returns>
        public bool UpdateSkmSerialNumber(SkmSerialNumber skmSerialNumber)
        {
            using (var db = new SkmDbContext())
            {
                db.Entry(skmSerialNumber).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool SetSkmSerialNumberLog(SkmSerialNumberLog log)
        {
            using (var db = new SkmDbContext())
            {
                if (log.Id == 0)
                {
                    db.Entry(log).State = System.Data.Entity.EntityState.Added;
                    log.RequestTime = DateTime.Now;
                }
                else
                {
                    db.Entry(log).State = System.Data.Entity.EntityState.Modified;
                    log.ResponseTime = DateTime.Now;
                }

                bool returnValue = db.SaveChanges() > 0;
                if (returnValue)
                    db.Entry(log).Reload();
                return returnValue;
            }
        }

        #endregion

        #region skm_ad_board

        public SkmAdBoard GetSkmAdBoard()
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    DateTime now = DateTime.Now;
                    var cols = (from p in db.SkmAdBoardEntities
                                where now >= p.StartDate
                                      && now < p.EndDate
                                      && p.IsEnable
                                select p).FirstOrDefault();
                    return cols;
                }
            }
        }

        public List<SkmAdBoard> GetSkmAdBoardList()
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.SkmAdBoardEntities.ToList();
                }
            }
        }

        /// <summary>
        /// 檢查此時間區間是否已有廣告看板資料
        /// </summary>
        /// <param name="startDate">開始時間</param>
        /// <param name="endDate">結束時間</param>
        /// <param name="nowAdBoardId">目前的廣告看板編號(若是新增填0)</param>
        /// <returns>是否已存在</returns>
        public bool CheckAdBoardIsExistByDate(DateTime startDate, DateTime endDate, int nowAdBoardId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmAdBoardEntities
                         .Any(item => item.Id != nowAdBoardId && item.IsEnable && (
                                    (item.StartDate <= startDate && item.EndDate > startDate) ||
                                    (item.StartDate < endDate && item.EndDate >= endDate) ||
                                    (item.StartDate >= startDate && item.EndDate <= endDate)));
            }
        }

        public bool DeleteAdBoards(int id)
        {
            using (var db = new SkmDbContext())
            {
                var cols = db.SkmAdBoardEntities.Where(p => p.Id == id);
                db.SkmAdBoardEntities.RemoveRange(cols);
                return db.SaveChanges() > 0;
            }
        }

        public bool InsertAdBoard(SkmAdBoard data)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmAdBoardEntities.Add(data);
                return db.SaveChanges() > 0;
            }
        }


        /// <summary>
        /// 更新廣告看版狀態(顯示/隱藏)
        /// </summary>
        /// <param name="id">廣告看版編號</param>
        /// <param name="isEnable">是否顯示</param>
        /// <returns>更新後的廣告看版資訊</returns>
        public SkmAdBoard UpdateAdBoardIsEnable(int id, bool isEnable)
        {
            using (var db = new SkmDbContext())
            {
                SkmAdBoard adBoard = new SkmAdBoard() { Id = id, IsEnable = isEnable };
                db.SkmAdBoardEntities.Attach(adBoard);
                db.Entry(adBoard).Property(item => item.IsEnable).IsModified = true;
                if (db.SaveChanges() > 0) return adBoard;
                else return null;
            }
        }

        /// <summary>
        /// 依廣告看板編號取得廣告看板資訊
        /// </summary>
        /// <param name="id">廣告看板編號</param>
        /// <returns></returns>
        public SkmAdBoard GetSkmAdBoardById(int id)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmAdBoardEntities.FirstOrDefault(item => item.Id == id);
            }
        }

        /// <summary>
        /// 更新廣告看板資料
        /// </summary>
        /// <param name="skmAdBoard">要更新的廣告看板資料</param>
        /// <returns>是否更新成功</returns>
        public bool UpdatAdBoard(SkmAdBoard skmAdBoard)
        {
            using (var db = new SkmDbContext())
            {
                db.Entry(skmAdBoard).State = System.Data.Entity.EntityState.Modified;
                db.Entry(skmAdBoard).Property(item => item.CreateUser).IsModified = false;
                db.Entry(skmAdBoard).Property(item => item.CreateDate).IsModified = false;
                return db.SaveChanges() > 0;
            }
        }
        #endregion
        
        #region view_skm_burning_event_quantity

        /// <summary>
        /// 依贈品編號查詢各檔次的庫存量清單
        /// </summary>
        /// <param name="exchangeItemId">贈品編號</param>
        /// <returns>各檔次的庫存量清單</returns>
        public List<ViewSkmDealQuantity> GetDealQuantityByExchangeItemId(string exchangeItemId)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {
                    return db.ViewSkmDealQuantityEntities.Where(item => item.ExchangeItemId == exchangeItemId).ToList();
                }
            }
        }
        /// <summary>
        /// 依BusinessHourGuid取得庫存量資訊
        /// </summary>
        /// <param name="value">依BusinessHourGuid取得庫存量資訊</param>
        /// <returns>庫存量資訊</returns>
        public ViewSkmDealQuantity DealQuantityByBid(Guid bid)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmDealQuantityEntities.First(item => item.Bid == bid);
            }
        }
        #endregion

        #region view_skm_deal_guid_quantity
        /// <summary>
        /// 依檔次編號取得檔次數量相關資訊清單
        /// </summary>
        /// <param name="guid">檔次編號</param>
        /// <returns>檔次數量相關資訊清單</returns>
        public List<ViewSkmDealGuidQuantity> GetExternalQuantityByGuid(Guid guid)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmDealGuidQuantityEntities.Where(item => item.ExternalDealGuid == guid).ToList();
            }
        }
        #endregion

        #region ExternalDealInventoryLog
        /// <summary>
        /// 新增更新贈品庫存紀錄
        /// </summary>
        /// <param name="skmSetExternalDealInventoryLog">更新贈品庫存資料</param>
        /// <returns>是否更新成功</returns>
        public bool InsertSkmSetExternalDealInventoryLog(SkmSetExternalDealInventoryLog skmSetExternalDealInventoryLog)
        {
            using (var db = new SkmDbContext())
            {
                db.Entry(skmSetExternalDealInventoryLog).State = System.Data.Entity.EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 更新更新贈品庫存紀錄
        /// </summary>
        /// <param name="skmSetExternalDealInventoryLog">更新贈品庫存資料</param>
        /// <returns>是否更新成功</returns>
        public bool UpdateSkmSetExternalDealInventoryLog(SkmSetExternalDealInventoryLog skmSetExternalDealInventoryLog)
        {
            using (var db = new SkmDbContext())
            {
                db.Entry(skmSetExternalDealInventoryLog).State = System.Data.Entity.EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }
        #endregion

        #region SkmCustomizedBoard
        /// <summary>
        /// 取得自訂版位資訊清單
        /// </summary>
        /// <param name="storeGuid">店編號</param>
        /// <returns>自訂版位基本資訊清單</returns>
        public List<SkmCustomizedBoard> GetSkmCustomizedBoardList(Guid storeGuid)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmCustomizedBoardEntities.Where(item => item.SellerGuid == storeGuid).ToList();
            }
        }

        /// <summary>
        /// 依編號取得自訂版位資訊
        /// </summary>
        /// <param name="id">編號</param>
        /// <returns>自訂版位資訊</returns>
        public SkmCustomizedBoard GetSkmCustomizedBoardById(int id)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmCustomizedBoardEntities.FirstOrDefault(item => item.Id == id);
            }
        }

        /// <summary>
        /// 更新自訂版位資訊
        /// </summary>
        /// <param name="skmCustomizedBoard">自訂版位資訊</param>
        /// <returns>是否更新成功</returns>
        public bool UpdateSkmCustomizedBoard(SkmCustomizedBoard skmCustomizedBoard)
        {
            using (var db = new SkmDbContext())
            {
                db.Entry(skmCustomizedBoard).State = System.Data.Entity.EntityState.Modified;
                db.Entry(skmCustomizedBoard).Property(item => item.CreateUser).IsModified = false;
                db.Entry(skmCustomizedBoard).Property(item => item.CreateDate).IsModified = false;
                return db.SaveChanges() > 0;
            }
        }
        #endregion

        #region ViewSkmCashTrustLogNotVerified
        /// <summary>
        /// 取得新光未核銷且未取消訂單憑證
        /// </summary>
        /// <param name="days">幾天後</param>
        /// <param name="type">StartDate or EndDate</param>
        /// <returns>取得相關資料</returns>
        public List<ViewSkmCashTrustLogNotVerified> GetSkmCashTrustLogNotVerifiedByDay(int days, SkmCashTrustLogNotVerifiedType type)
        {
            using (var db = new SkmDbContext())
            {
                using (db.NoLock())
                {

                    var date = DateTime.Now.AddDays(days);
                    //讓時分秒變成00000000
                    date = Convert.ToDateTime(date.ToShortDateString());
                    var dateAddOneDay = date.AddDays(1);

                    var data = new List<ViewSkmCashTrustLogNotVerified>();
                    switch (type)
                    {
                        case SkmCashTrustLogNotVerifiedType.PreOrder:
                            data = db.ViewSkmCashTrustLogNotVerifiedEntities
                                .Where(p => p.BusinessHourDeliverTimeS >= date && p.BusinessHourDeliverTimeS < dateAddOneDay)
                                .ToList();
                            break;
                        case SkmCashTrustLogNotVerifiedType.ExpiringOrder:
                            data = db.ViewSkmCashTrustLogNotVerifiedEntities
                                .Where(p => p.BusinessHourDeliverTimeE >= date && p.BusinessHourDeliverTimeE < dateAddOneDay)
                                .ToList();
                            break;
                    }

                    return data;
                }
            }

        }
        #endregion

        #region SkmInstallment
        /// <summary>
        /// 依活動名稱模糊搜尋分期活動相關資訊清單
        /// </summary>
        /// <param name="eventTitle">活動名稱</param>
        /// <returns>活動相關資訊清單</returns>
        public List<ViewSkmInstallEventList> GetViewSkmInstallEventListByEventTitle(string eventTitle)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmInstallEventList.Where(x => x.Title.Contains(eventTitle)).ToList();
            }
        }
        /// <summary>
        /// 取得分期活動列表
        /// </summary>
        /// <returns>分期活動列表</returns>
        public List<SkmInstallEvent> GetSkmInstallEventList()
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmInstallEvent.ToList();
            }
        }
        /// <summary>
        /// 取得分期活動By活動編號
        /// </summary>
        /// <param name="id">活動編號</param>
        /// <returns>分期活動</returns>
        public SkmInstallEvent GetSkmInstallEventById(int id)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmInstallEvent.SingleOrDefault(x => x.Id == id);
            }
        }
        /// <summary>
        /// 新增/編輯(更新) 分期活動
        /// </summary>
        /// <param name="installEvent"></param>
        /// <returns>是否成功</returns>
        public bool AddOrUpdateSkmInstallEvent(SkmInstallEvent installEvent)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmInstallEvent.AddOrUpdate(installEvent);
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 新增/編輯(更新) 某分期活動下的銀行
        /// </summary>
        /// <param name="installEventBank"></param>
        /// <returns>是否成功</returns>
        public bool AddOrUpdateSkmInstallEventBank(SkmInstallEventBank installEventBank)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmInstallEventBank.AddOrUpdate(installEventBank);
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 依時間取得銀行分期相關資訊清單
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns>銀行分期相關資訊清單</returns>
        public List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByDateTime(DateTime dateTime)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmInstallEventBank
                    .Where(x => dateTime >= x.EventStartDate && dateTime <= x.EventEndDate && x.EventIsAvailable && !(x.BankIsDel ?? true) && (x.PeriodIsAvailable ?? false))
                    .ToList();
            }
        }
        /// <summary>
        /// 依活動編號取得銀行分期相關資訊清單
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns>銀行分期相關資訊清單</returns>
        public List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByEventId(int eventId)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmInstallEventBank.Where(x => x.EventId == eventId && x.BankIsDel == false).ToList();
            }
        }
        /// <summary>
        /// 依活動銀行編號取得銀行分期相關資訊清單
        /// </summary>
        /// <param name="eventBankId"></param>
        /// <returns>銀行分期相關資訊清單</returns>
        public List<ViewSkmInstallEventBank> GetViewSkmInstallEventBankByEventBankId(int eventBankId)
        {
            using (var db = new SkmDbContext())
            {
                return db.ViewSkmInstallEventBank.Where(x => x.EventBankId == eventBankId).ToList();
            }
        }
        /// <summary>
        /// 依活動銀行編號取得銀行分期資訊
        /// </summary>
        /// <param name="eventBankId"></param>
        /// <returns>銀行分期資訊</returns>
        public List<SkmInstallEventPeriod> GetSkmInstallEventPeriodsByEventBankId(int eventBankId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmInstallEventPeriod.Where(x => x.EventBankId == eventBankId).ToList();
            }
        }
        /// <summary>
        /// 依活動編號取得銀行列表
        /// </summary>
        /// <param name="eventId"></param>
        /// <returns>銀行列表</returns>
        public List<SkmInstallEventBank> GetSkmInstallEventBankByEventId(int eventId)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmInstallEventBank.Where(x => x.EventId == eventId && x.IsDel == false).ToList();
            }
        }
        /// <summary>
        /// 依Id取得活動銀行
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SkmInstallEventBank GetSkmInstallEventBankById(int id)
        {
            using (var db = new SkmDbContext())
            {
                return db.SkmInstallEventBank.FirstOrDefault(x => x.Id == id);
            }
        }
        /// <summary>
        /// 批量刪除某活動銀行下的分期資訊
        /// </summary>
        /// <param name="eventBankId"></param>
        /// <returns>是否成功</returns>
        public bool DeleteSkmInstallEventPeriodsByEventBankId(int eventBankId)
        {
            using (var db = new SkmDbContext())
            {
                var data = db.SkmInstallEventPeriod.Where(x => x.EventBankId == eventBankId);

                if (data.Any())
                {
                    db.SkmInstallEventPeriod.RemoveRange(data);
                    return db.SaveChanges() > 0;
                }

                return true;
            }
        }
        /// <summary>
        /// 批量新增分期資訊
        /// </summary>
        /// <param name="data"></param>
        /// <returns>是否成功</returns>
        public bool BulkInsertSkmInstallEventPeriods(List<SkmInstallEventPeriod> data)
        {
            using (var db = new SkmDbContext())
            {
                db.SkmInstallEventPeriod.AddRange(data);
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region SKM 總部查詢資料

        /// <summary>
        /// 取得skmpay訂單交易明細
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public List<SkmpayOrderInfo> ExecFnGetSkmpayOrderInfo(DateTime startDate, DateTime endDate)
        {
            using (var ctx = new SkmDbContext())
            {
                return ctx.FnGetSkmpayOrderInfo(startDate, endDate).ToList();
            }
        }

        public List<SkmOnlineDeals> ExecFnGetSkmOnlineDeals(DateTime startDate, DateTime endDate)
        {
            using (var ctx = new SkmDbContext())
            {
                return ctx.FnGetSkmOnlineDeals(startDate, endDate).ToList();
            }
        }

        public List<SkmCurationEvents> ExecFnGetSkmCurationEvents(DateTime startDate, DateTime endDate)
        {
            using (var ctx = new SkmDbContext())
            {
                return ctx.FnGetFnGetSkmCurationEvents(startDate, endDate).ToList();
            }
        }

        public List<SkmCurationDeals> ExecFnGetSkmCurationDeals(DateTime startDate, DateTime endDate)
        {
            using (var ctx = new SkmDbContext())
            {
                return ctx.FnGetFnGetSkmCurationDeals(startDate, endDate).ToList();
            }
        }

        #endregion

        #region 預防超抽獎

        public bool CheckPrizeOverdraw(int itemId, int qty, int userId, int timeoutSec, int prizeLimit, out int preventId, out string msg)
        {
            preventId = 0;
            msg = string.Empty;
            using (var db = new SkmDbContext())
            {
                var sqlQuery = "sp_prizeOverdraw @itemId, @qty, @userId, @timeoutSec, @prizeTotalLimit, @pid OUTPUT, @checkResult OUTPUT, @msg OUTPUT";
                var sqlParams = new SqlParameter[]
                {
                    new SqlParameter { ParameterName = "@itemId",  Value = itemId , Direction = System.Data.ParameterDirection.Input},
                    new SqlParameter { ParameterName = "@qty",  Value = qty , Direction = System.Data.ParameterDirection.Input},
                    new SqlParameter { ParameterName = "@userId",  Value = userId , Direction = System.Data.ParameterDirection.Input},
                    new SqlParameter { ParameterName = "@timeoutSec",  Value = timeoutSec , Direction = System.Data.ParameterDirection.Input},
                    new SqlParameter { ParameterName = "@prizeTotalLimit",  Value = prizeLimit , Direction = System.Data.ParameterDirection.Input},
                    new SqlParameter { ParameterName = "@pid",  DbType = System.Data.DbType.Int32, Direction = System.Data.ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@checkResult",  DbType = System.Data.DbType.Byte, Direction = System.Data.ParameterDirection.Output },
                    new SqlParameter { ParameterName = "@msg",  DbType = System.Data.DbType.String, Size = 100, Direction = System.Data.ParameterDirection.Output }
                };

                db.Database.ExecuteSqlCommand(sqlQuery, sqlParams);
                var result = false;

                foreach (var p in sqlParams)
                {
                    if (p.Direction != System.Data.ParameterDirection.Output)
                    {
                        continue;
                    }

                    if (p.ParameterName == "@pid")
                    {
                        preventId = Convert.ToInt32(p.Value);
                    }
                    else if (p.ParameterName == "@checkResult")
                    {
                        result = Convert.ToInt32(p.Value) == 1;
                    }
                    else if (p.ParameterName == "@msg")
                    {
                        msg = p.Value.ToString();
                    }
                }

                return result;
            }
        }

        #endregion
    }
}

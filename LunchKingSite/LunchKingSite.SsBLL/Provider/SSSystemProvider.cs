﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using log4net;
using System;
using Newtonsoft.Json;

namespace LunchKingSite.SsBLL
{
    public class SSSystemProvider : ISystemProvider
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SSSystemProvider));

        #region ManagerResettime
        /// <summary>
        /// 取出執行中的ManagerResettime設定值。
        /// </summary>
        /// <returns></returns>
        public DataManagerResettimeCollection DataManagerResettimeGetListWithEnabled()
        {
            return
                DB.SelectAllColumnsFrom<DataManagerResettime>().Where(DataManagerResettime.Columns.Enabled).IsEqualTo(true).
                    ExecuteAsCollection<DataManagerResettimeCollection>();
        }
        /// <summary>
        /// 儲存DataManagerResettime的collection
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool DataManagerResettimeSetList(DataManagerResettimeCollection data)
        {
            DB.SaveAll(data);
            return true;
        }
        /// <summary>
        /// 取得特定條件的ManagerResettime設定值。
        /// </summary>
        /// <param name="job_type"></param>
        /// <param name="enabled"></param>
        /// <param name="nextRunningTime"></param>
        /// <returns></returns>
        public DataManagerResettimeCollection DataManagerResettimeGetWithJobType(int job_type, bool enabled, DateTime? nextRunningTime)
        {
            if (nextRunningTime == null)
            {
                return
                    DB.SelectAllColumnsFrom<DataManagerResettime>()
                    .Where(DataManagerResettime.Columns.Enabled).IsEqualTo(enabled)
                    .And(DataManagerResettime.Columns.JobType).IsEqualTo(job_type)
                    .ExecuteAsCollection<DataManagerResettimeCollection>();
            }
            else
            {
                return
                    DB.SelectAllColumnsFrom<DataManagerResettime>()
                    .Where(DataManagerResettime.Columns.Enabled).IsEqualTo(enabled)
                    .And(DataManagerResettime.Columns.JobType).IsEqualTo(job_type)
                    .And(DataManagerResettime.Columns.NextRunningTime).IsEqualTo(nextRunningTime)
                    .ExecuteAsCollection<DataManagerResettimeCollection>();
            }
        }

        /// <summary>
        /// 取得特定條件的ManagerResettime設定值。
        /// </summary>
        /// <param name="job_type"></param>
        /// <param name="enabled"></param>
        /// <param name="nextRunningTime"></param>
        /// <param name="serverName"></param>
        /// <returns></returns>
        public DataManagerResettimeCollection DataManagerResettimeGetWithJobType(int job_type, bool enabled, DateTime? nextRunningTime, string serverName)
        {
            if (nextRunningTime == null)
            {
                return
                    DB.SelectAllColumnsFrom<DataManagerResettime>()
                    .Where(DataManagerResettime.Columns.Enabled).IsEqualTo(enabled)
                    .And(DataManagerResettime.Columns.JobType).IsEqualTo(job_type)
                    .And(DataManagerResettime.Columns.ServerName).IsEqualTo(serverName)
                    .ExecuteAsCollection<DataManagerResettimeCollection>();
            }
            else
            {
                return
                    DB.SelectAllColumnsFrom<DataManagerResettime>()
                    .Where(DataManagerResettime.Columns.Enabled).IsEqualTo(enabled)
                    .And(DataManagerResettime.Columns.JobType).IsEqualTo(job_type)
                    .And(DataManagerResettime.Columns.NextRunningTime).IsEqualTo(nextRunningTime)
                    .And(DataManagerResettime.Columns.ServerName).IsEqualTo(serverName)
                    .ExecuteAsCollection<DataManagerResettimeCollection>();
            }
        }

        /// <summary>
        /// 寫入ManagerResettime資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public bool DataManagerResettimeSetData(DataManagerResettime data)
        {
            try
            {
                if (!data.Enabled)//單純保留記錄
                {
                    DB.Save<DataManagerResettime>(data);
                }
                else
                {
                    var a = DB.SelectAllColumnsFrom<DataManagerResettime>()
                        .Where(DataManagerResettime.Columns.Enabled).IsEqualTo(data.Enabled)
                        .And(DataManagerResettime.Columns.JobType).IsEqualTo(data.JobType)
                        //.And(DataManagerResettime.Columns.NextRunningTime).IsEqualTo(data.NextRunningTime)
                        .And(DataManagerResettime.Columns.ManagerName).IsEqualTo(data.ManagerName)
                        .ExecuteSingle<DataManagerResettime>();

                    if (a.IsLoaded)
                    {
                        a.NextRunningTime = data.NextRunningTime;
                        a.LastRunningTime = null;
                        DB.Update<DataManagerResettime>(a);
                    }
                    else
                    {
                        DB.Save<DataManagerResettime>(data);
                    }
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        #endregion
        #region SystemCode
        public SystemCodeCollection SystemCodeGetListByCodeGroup(string codeGroupId)
        {
            return
                DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.CodeGroup).IsEqualTo(codeGroupId).
                    OrderAsc(SystemCode.Columns.CodeId).ExecuteAsCollection<SystemCodeCollection>();
        }
        public SystemCodeCollection SystemCodeGetListWithEnabled()
        {
            return DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.Enabled).IsEqualTo(true).ExecuteAsCollection<SystemCodeCollection>();
        }
        public SystemCodeCollection SystemCodeHiDealCategoryGetList()
        {
            return
                DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.CodeGroup).IsEqualTo("HiDealCatg").And(SystemCode.CodeIdColumn).IsGreaterThan(0).
                    OrderAsc(SystemCode.Columns.CodeId).ExecuteAsCollection<SystemCodeCollection>() ?? new SystemCodeCollection();
        }

        public string SystemCodeGetName(string codegroup, int id)
        {
            var system_codes = DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.CodeGroup).IsEqualTo(codegroup).And(SystemCode.Columns.CodeId).IsEqualTo(id).ExecuteAsCollection<SystemCodeCollection>();
            if (system_codes.Count > 0)
                return system_codes[0].CodeName;
            else
                return string.Empty;
        }

        public SystemCode SystemCodeGetByCodeGroupId(string codegroup, int id)
        {
            return DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.CodeGroup).IsEqualTo(codegroup).And(SystemCode.Columns.CodeId).IsEqualTo(id).ExecuteSingle<SystemCode>();
        }

        /// <summary>
        /// 傳入類別種類跟類別Id, 傳回其母類別
        /// </summary>
        public SystemCode ParentSystemCodeGetByCodeGroupId(string codeGroup, int id)
        {
            SystemCode sc = SystemCodeGetByCodeGroupId(codeGroup, id);
            return DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.Columns.CodeGroup).IsEqualTo(sc.CodeGroup).And(SystemCode.Columns.CodeId).IsEqualTo(sc.ParentCodeId).ExecuteSingle<SystemCode>();
        }

        public SystemCodeCollection SystemCodeGetListByParentId(int parentid)
        {
            return DB.SelectAllColumnsFrom<SystemCode>().Where(SystemCode.ParentCodeIdColumn).IsEqualTo(parentid).OrderAsc(SystemCode.Columns.Seq).ExecuteAsCollection<SystemCodeCollection>()
                ?? new SystemCodeCollection();
        }

        public SystemCodeCollection SystemCodeGetListByParentId(string codeGroup, int parentid)
        {
            return DB.SelectAllColumnsFrom<SystemCode>().NoLock()
                .Where(SystemCode.Columns.CodeGroup).IsEqualTo(codeGroup)
                .And(SystemCode.Columns.ParentCodeId).IsEqualTo(parentid)
                .ExecuteAsCollection<SystemCodeCollection>();                
        }

        public int SystemCodeSet(SystemCode system_code)
        {
            return DB.Save<SystemCode>(system_code);
        }
        /// <summary>
        /// 取得指定群組的下一個編號
        /// </summary>
        /// <param name="codeGroup"></param>
        /// <returns></returns>
        public int GetNextCodeId(string codeGroup)
        {
            string sql = @"
select isnull(max(code_id), 0) +1 from system_code
where  code_group = @codeGroup
";
            QueryCommand qc = new QueryCommand(sql, SystemCode.Schema.Provider.Name);
            qc.AddParameter("@codeGroup", codeGroup, DbType.AnsiString);
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion SystemCode
        #region AccBusinessGroup

        public AccBusinessGroupCollection AccBusinessGroupGetList()
        {
            return
                DB.SelectAllColumnsFrom<AccBusinessGroup>().Where(AccBusinessGroup.Columns.Status).IsNotEqualTo(AccBusinessGroupStatus.Disable)
                .OrderAsc(AccBusinessGroup.Columns.Seq).ExecuteAsCollection<AccBusinessGroupCollection>();
        }

        public AccBusinessGroup AccBusinessGroupGet(int accBusinessGroupId)
        {
            return DB.Get<AccBusinessGroup>(AccBusinessGroup.Columns.AccBusinessGroupId, accBusinessGroupId);
        }

        #endregion
        #region ApiUser

        public ApiUserCollection ApiUserGetList(bool isEnabled)
        {
            return DB.SelectAllColumnsFrom<ApiUser>().Where(ApiUser.Columns.Enabled).IsEqualTo(isEnabled).
                    ExecuteAsCollection<ApiUserCollection>();
        }
        #endregion ApiUser

        #region AppVersion
        /// <summary>
        /// 取出所有的AppVersion資訊
        /// </summary>
        /// <returns></returns>
        public AppVersionCollection AppVersionGetList()
        {
            return DB.SelectAllColumnsFrom<AppVersion>().ExecuteAsCollection<AppVersionCollection>();
        }

        public void UpdateAppVersion(AppVersion version)
        {
            DB.Update<AppVersion>().Set(AppVersion.LatestVersionColumn).EqualTo(version.LatestVersion)
                .Set(AppVersion.MinimumVersionColumn).EqualTo(version.MinimumVersion)
                .Set(AppVersion.OsVersionColumn).EqualTo(version.OsVersion)
                .Set(AppVersion.OsMinimumVersionColumn).EqualTo(version.OsMinimumVersion)
                .Set(AppVersion.AppConfigColumn).EqualTo(version.AppConfig)
                .Where(AppVersion.AppNameColumn)
                           .IsEqualTo(version.AppName).Execute();
        }
        #endregion AppVersion

        #region JobsStatus
        public void JobsStatusSet(JobsStatus jobs)
        {
            jobs.ModifyTime = DateTime.Now;
            DB.Save<JobsStatus>(jobs);
        }

        public JobsStatus JobsStatusGet(string className)
        {
            return DB.Get<JobsStatus>(className);
        }
        #endregion

        #region SystemFunction
        public void SetSystemFunction(SystemFunction systemFunction)
        {
            DB.Save(systemFunction);
        }

        public void DeleteSystemFunction(int id)
        {
            DB.Delete<SystemFunction>(SystemFunction.Columns.Id, id);
        }

        public SystemFunction GetSystemFunction(int id)
        {
            return DB.Get<SystemFunction>(SystemFunction.Columns.Id, id);
        }

        public SystemFunction GetSystemFunction(string link, SystemFunctionType functype)
        {
            return DB.SelectAllColumnsFrom<SystemFunction>()
                .Where(SystemFunction.Columns.Link).IsEqualTo(link)
                .And(SystemFunction.Columns.FuncType).IsEqualTo(functype.ToString()).ExecuteSingle<SystemFunction>();
        }

        public SystemFunctionCollection GetSystemFunctionList(int pageNumber, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<SystemFunction, SystemFunctionCollection>(pageNumber, pageSize, orderBy, filter);
        }

        public int GetSystemFunctionListCount(params string[] filter)
        {
            SubSonic.Query qry = SystemFunction.CreateQuery();
            if (filter != null && filter.Length > 0)
            {
                string query = string.Empty;
                foreach (string item in filter)
                {
                    query += " AND " + item;
                }
                qry = qry.WHERE(query.TrimStart(" AND ".ToCharArray()));
            }
            return qry.GetCount(SystemFunction.Columns.Id);
        }
        #endregion

        #region OauthToken
        public OauthToken OauthTokenGet(string accessToken)
        {
            var token = DB.SelectAllColumnsFrom<OauthToken>().NoLock()
                .Where(OauthToken.Columns.AccessToken).IsEqualTo(accessToken)
                .ExecuteSingle<OauthToken>();

            return token;
        }

        public OauthToken OauthTokenGet(int userId)
        {
            var query = new Select()
             .Top("1")
             .From(OauthToken.Schema).NoLock()
             .Where(OauthToken.Columns.UserId).IsEqualTo(userId)
             .OrderDesc(OauthToken.Columns.ExpiredTime);

            return query.ExecuteSingle<OauthToken>();
        }
        #endregion

        #region OauthTokenPermission
        public OauthTokenPermissionCollection OauthTokenPermissionGetList(int token_id)
        {
            return DB.SelectAllColumnsFrom<OauthTokenPermission>().NoLock()
                .Where(OauthTokenPermission.Columns.TokenId).IsEqualTo(token_id)
                .ExecuteAsCollection<OauthTokenPermissionCollection>();
        }
        #endregion

        #region SystemData
        public void SystemDataSet(SystemData data)
        {
            DB.Save<SystemData>(data);
        }
        public SystemData SystemDataGet(string name)
        {
            return DB.Get<SystemData>(SystemData.Columns.Name, name);
        }

        public void SystemDataDelete(string name)
        {
            DB.Delete<SystemData>(SystemData.Columns.Name, name);
        }
        #endregion

        #region sys.extended_properties
        /// <summary>
        /// 依據name取得資料庫中的ExtendedProperties參數之值，查無資料回傳空字串
        /// </summary>
        /// <param name="name"></param>
        public string DBExtendedPropertiesGet(string name)
        {
            string sql = @"select value from sys.extended_properties where class_desc = 'DATABASE' and name = @name";

            QueryCommand qc = new QueryCommand(sql, AppVersion.Schema.Provider.Name);
            qc.AddParameter("@name", name, DbType.String);
            object data = DataService.ExecuteScalar(qc);
            if (data == null)
            {
                return string.Empty;
            }
            else
            {
                return (string)data;
            }
        }

        public void DbExtendedPropertyAdd(string name, string value)
        {
            const string sql = @"EXEC sys.[sp_addextendedproperty] @name, @value";
            var qc = new QueryCommand(sql, AppVersion.Schema.Provider.Name);
            qc.AddParameter("@name", name, DbType.String);
            qc.AddParameter("@value", value, DbType.String);

            DataService.ExecuteScalar(qc);
        }

        public void DbExtendedPropertyUpdate(string name, string value)
        {
            const string sql = @"EXEC sys.[sp_updateextendedproperty] @name, @value";
            var qc = new QueryCommand(sql, AppVersion.Schema.Provider.Name);
            qc.AddParameter("@name", name, DbType.String);
            qc.AddParameter("@value", value, DbType.String);

            DataService.ExecuteScalar(qc);
        }

        #endregion sys.extended_properties

        #region HitLog

        public void HitLogSet(HitLog log)
        {
            DB.Save(log);
        }

        #region serial_number

        public int SerialNumberGetNext(string serialName, int start = 1)
        {
            string sql = @"
Begin tran

	if (EXISTS(select 1 from serial_number where serial_name = @serialName))
	Begin
		update serial_number set last_value = last_value+1 
		where serial_name = @serialName; 
	End
	Else
	Begin
		Insert into serial_number(serial_name, last_value) values(@serialName, @start)
	End
	select last_value from serial_number where serial_name = @serialName

Commit";
            QueryCommand qc = new QueryCommand(sql, SerialNumber.Schema.Provider.Name);
            qc.AddParameter("@serialName", serialName, DbType.String);
            qc.AddParameter("@start", start, DbType.Int32);
            object obj = DataService.ExecuteScalar(qc);
            if (obj == null)
            {
                throw new Exception("在 serial_number 找不到 " + serialName + "，取得流水號失敗");
            }
            return (int)obj;
        }

        #endregion

        #endregion
    }

    [Serializable]
    public class DataManagerResetViewModel
    {
        [JsonProperty(Required = Required.Always)]
        public string NameSpace { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string ClassName { get; set; }

        [JsonProperty(Required = Required.Always)]
        public string MethodName { get; set; }

        [JsonProperty(Required = Required.AllowNull)]
        public object[] Parameters { get; set; }
    }
}

﻿using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.SsBLL
{
    public class SkmProvider : ISkmProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region SkmMember

        public bool InsertSkmMember(SkmMember member)
        {
            int count = 0;
            try
            {
                var sql = @"Insert into skm_member(skm_token, user_id, create_time, is_enable, is_shared)
                            select @skmToken, @userId, getdate(), @isEnable, @isShared 
                            Where Not EXISTS 
                                    (   SELECT  1
                                        FROM    skm_member 
                                        WHERE   skm_token = @skmToken 
                                    );";
                QueryCommand qc = new QueryCommand(sql, SkmMember.Schema.Provider.Name);
                qc.AddParameter("@skmToken", member.SkmToken, DbType.String);
                qc.AddParameter("@userId", member.UserId, DbType.Int32);
                qc.AddParameter("@isEnable", member.IsEnable, DbType.Boolean);
                qc.AddParameter("@isShared", member.IsShared, DbType.Boolean);
                count = DB.Execute(qc);
            }
            catch (SqlException ex)
            {
                if (ex.Number == 2601)
                {
                    return false;
                }
            }
            return count == 1;
        }

        public bool MemberSet(SkmMember member)
        {
            return DB.Save(member) > 0;
        }

        public SkmMember MemberGetBySkmToken(string token, bool isShared)
        {
            var skmMember = DB.Get<SkmMember>(SkmMember.Columns.SkmToken, token);
            //若會員尚未成為共同會員，且本次同意共同會員則更新，忽略不同意者
            if (skmMember.IsLoaded && isShared && !skmMember.IsShared)
            {
                skmMember.IsShared = true;
                DB.Save(skmMember);//紀錄 isShared Change Log
                SkmLogSet(new SkmLog() { SkmToken = token, UserId = skmMember.UserId, LogType = (int)SkmLogType.IsSharedChange, InputValue = (isShared) ? "IsShared=True" : "IsShared=False" });
            }
            
            return skmMember;
        }

        public SkmMember MemberGetBySkmToken(string token)
        {
            return DB.Get<SkmMember>(SkmMember.Columns.SkmToken, token);
        }

        public SkmMember MemberGetByUserId(int userId)
        {
            return DB.Get<SkmMember>(SkmMember.Columns.UserId, userId);
        }
        public SkmMemberCollection MemberListGet()
        {
               return DB.SelectAllColumnsFrom<SkmMember>().NoLock()
                .ExecuteAsCollection<SkmMemberCollection>();
        }

        public SkmMember MemberIsSharedGetByUserid(int userId)
        {
            return DB.Get<SkmMember>(SkmMember.Columns.IsShared, userId);
        }

        public void MemberDeleteBySkmToken(string token)
        {
            DB.Delete<SkmMember>(SkmMember.Columns.SkmToken, token);
        }

        public bool MemberCancelSharedByUserId(int userId, bool isShared)
        {
            return DB.Update<SkmMember>().Set(SkmMember.Columns.IsShared).EqualTo(isShared)
                .Where(SkmMember.Columns.UserId).IsEqualTo(userId).Execute() > 0;
        }

        #endregion

        #region SkmVerifyLog

        public bool VerifyLogSet(SkmVerifyLog verifyLog)
        {
            return DB.Save(verifyLog) > 0;
        }

        #endregion

        #region SkmVerifyReply

        public SkmVerifyReply SkmVerifyReplySet(SkmVerifyReply reply)
        {
            DB.Save(reply);
            return reply;
        }

        public SkmVerifyReply SkmVerifyReplyGet(Guid trustId)
        {
            var reply = DB.Get<SkmVerifyReply>(SkmVerifyReply.Columns.TrustId, trustId);
            return reply ?? new SkmVerifyReply();
        }

        public SkmVerifyReplyCollection SkmVerifyReplyGetRetry(DateTime start, DateTime end)
        {
            return DB.SelectAllColumnsFrom<SkmVerifyReply>().NoLock()
                .Where(SkmVerifyReply.Columns.CreateTime).IsBetweenAnd(start, end)
                .And(SkmVerifyReply.Columns.ReplyStatus).In(new[] { (byte)SkmVerifyReplyStatus.Ready, (byte)SkmVerifyReplyStatus.ReplyFail })
                .ExecuteAsCollection<SkmVerifyReplyCollection>();
        }

        public SkmVerifyReplyHistory SkmVerifyReplyHistorySet(SkmVerifyReplyHistory history)
        {
            history.CreateTime = DateTime.Now;
            DB.Save(history);
            return history;
        }

        #endregion

        #region SkmExchange
        public SkmExchangeCollection SkmExchangeGet()
        {
            var skmExchange = DB.SelectAllColumnsFrom<SkmExchange>()
                .ExecuteAsCollection<SkmExchangeCollection>();
            return skmExchange;
        }

        public bool SkmExchangeSet(SkmExchange skmExchange)
        {
            return DB.Save(skmExchange) > 0;
        }
        #endregion SkmExchange

        #region SkmShoppe

        public SkmShoppeCollection SkmShoppeGetAllShop()
        {
            return DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                .Where(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteAsCollection<SkmShoppeCollection>();
        }

        public SkmShoppeCollection SkmShoppeGetAll()
        {
            var skmData = DB.SelectAllColumnsFrom<SkmShoppe>()
                .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                .And(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                .ExecuteAsCollection<SkmShoppeCollection>();
            return skmData;
        }

        public SkmShoppeCollection SkmShoppeGetByShopeCdoe(string shopCode)
        {
            var skmData = DB.SelectAllColumnsFrom<SkmShoppe>()
                 .Where(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode)
                .ExecuteAsCollection<SkmShoppeCollection>();
            return skmData;
        }

        public SkmShoppe SkmShoppeGetFirstByShopeCdoe(string shopCode)
        {
            var skmData = DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                .Where(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode)
                .And(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteSingle<SkmShoppe>();
            return skmData;
        }

        public List<string> SkmShoppeGetAllShopCodeList()
        {
            var sql = @"select distinct shop_code from skm_shoppe with(nolock) where is_shoppe = 0 and is_available = 1";
            List<string> result = new List<string>();
            var qc = new QueryCommand(sql, SkmShoppe.Schema.Provider.Name);
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    result.Add(dr.GetString(0));
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                    dr.Dispose();
                }
            }
            return result;
        }

        public List<string> SkmShoppeGetShopCodeBySellerGuid(Guid sellerGuid)
        {
            var sql =
                @"select * from skm_shoppe where seller_guid = @sellerGuid and is_shoppe = 0 and is_available = 1
                    union
                    select * from skm_shoppe where store_guid = @sellerGuid and is_shoppe = 0 and is_available = 1";

            QueryCommand qc = new QueryCommand(sql, SkmShoppe.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            SkmShoppeCollection ss = new SkmShoppeCollection();
            ss.LoadAndCloseReader(DataService.GetReader(qc));
            return ss.Any() ? ss.Select(x=>x.ShopCode).ToList() : new List<string>();
        }

        public SkmShoppe SkmShoppeGet(Guid storeGuid,bool isContainsNotAvailableShoppe = false)
        {
            var skmData = isContainsNotAvailableShoppe ? DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                .Where(SkmShoppe.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<SkmShoppeCollection>()
                : DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                    .Where(SkmShoppe.Columns.StoreGuid).IsEqualTo(storeGuid)
                    .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                    .ExecuteAsCollection<SkmShoppeCollection>();
            return skmData.FirstOrDefault() ?? new SkmShoppe();
        }

        public SkmShoppe SkmShoppeGet(string shopCode, string brandCounterCode)
        {
            var data = DB.SelectAllColumnsFrom<SkmShoppe>().Where(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode)
                .And(SkmShoppe.Columns.BrandCounterCode).IsEqualTo(brandCounterCode)
                .ExecuteAsCollection<SkmShoppeCollection>();

            return data.FirstOrDefault() ?? new SkmShoppe();
        }

        public bool ShoppeSet(SkmShoppe shoppe)
        {
            return DB.Save(shoppe) > 0;
        }

        /// <summary>
        /// 取得seller下所有分店
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public SkmShoppeCollection SkmShoppeGetAllShoppe(Guid sellerGuid, string shopCode)
        {
            return DB.Select().From<SkmShoppe>().Where(SkmShoppe.Columns.IsShoppe).IsEqualTo(true)
                .And(SkmShoppe.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode)
                .ExecuteAsCollection<SkmShoppeCollection>();
        }

        public SkmShoppeCollection SkmShoppeGetAllShoppe(Guid sellerGuid)
        {
            return DB.Select().From<SkmShoppe>().Where(SkmShoppe.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<SkmShoppeCollection>();
        }
        
        /// <summary>
        /// 取得分館的 seller guid
        /// </summary>
        /// <param name="shopCode"></param>
        /// <returns></returns>
        public SkmShoppe ShoppeGetSellerByShopeCode(string shopCode)
        {
            return DB.Select()
                .From<SkmShoppe>()
                .Where(SkmShoppe.Columns.ShopCode)
                .IsEqualTo(shopCode)
                .And(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                .And(SkmShoppe.Columns.SellerGuid).IsNotNull()
                .ExecuteAsCollection<SkmShoppeCollection>().FirstOrDefault() ?? new SkmShoppe();
        }

        /// <summary>
        /// 分店是否存在
        /// </summary>
        /// <param name="shopCode"></param>
        /// <param name="brandCounterCode"></param>
        /// <returns></returns>
        public bool IsExistSkmShoppe(string shopCode, string brandCounterCode)
        {
            return DB.Select().From<SkmShoppe>().Where(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode)
                .And(SkmShoppe.Columns.BrandCounterCode).IsEqualTo(brandCounterCode)
                .ExecuteAsCollection<SkmShoppeCollection>().Any();
        }

        public bool ShoppeCollectionSet(SkmShoppeCollection shoppes)
        {
            return DB.SaveAll(shoppes) > 0;
        }

        #endregion SkmShoppe

        #region SkmShoppeChangeLog

        public bool SkmShoppeChangeLogAdd(SkmShoppeChangeLog log)
        {
            log.CreateTime = DateTime.Now;
            return DB.Save(log) > 0;
        }

        #endregion SkmShoppeChangeLog

        #region Skm_App_Style
        public SkmAppStyle SkmAppStyleGet(int id)
        {
            return DB.Get<SkmAppStyle>(SkmAppStyle.Columns.Id, id);
        }
        public SkmAppStyle SkmAppStyleGet(Guid guid)
        {
            return DB.Select().From<SkmAppStyle>().NoLock()
                .Where(SkmAppStyle.Columns.Guid).IsEqualTo(guid)
                .And(SkmAppStyle.Columns.Status).IsEqualTo((int)SkmAppStyleStatus.Normal)
                .ExecuteAsCollection<SkmAppStyleCollection>().FirstOrDefault();
        }
        public SkmAppStyleCollection SkmAppStyleGetBySeller(Guid SellerGuid)
        {
            return DB.Select().From<SkmAppStyle>().NoLock()
                .Where(SkmAppStyle.Columns.SellerGuid).IsEqualTo(SellerGuid)
                .And(SkmAppStyle.Columns.Status).IsEqualTo((int)SkmAppStyleStatus.Normal)
                .ExecuteAsCollection<SkmAppStyleCollection>();
        }
        public bool SkmAppStyleSet(SkmAppStyle skmAppStyle)
        {
            return DB.Save(skmAppStyle) > 0;
        }
        #endregion

        #region Skm_App_Style_Setup
        public SkmAppStyleSetup SkmAppStyleSetupGet(Guid gid)
        {
            return DB.Get<SkmAppStyleSetup>(SkmAppStyleSetup.Columns.Guid, gid);
        }
        public SkmAppStyleSetupCollection SkmAppStyleSetupGetBySid(Guid sid)
        {
            return DB.Select().From<SkmAppStyleSetup>().NoLock()
                .Where(SkmAppStyleSetup.Columns.StyleGuid).IsEqualTo(sid)
                .And(SkmAppStyleSetup.Columns.Status).IsNotEqualTo((int)SkmAppStyleStatus.Delete)
                .ExecuteAsCollection<SkmAppStyleSetupCollection>();
        }
        public bool SkmAppStyleSetupSet(SkmAppStyleSetup skmAppStyleSetup)
        {
            return DB.Save(skmAppStyleSetup) > 0;
        }

        public SkmLatestActivityCollection SkmLatestActivityGetBySid(Guid sid)
        {
            return DB.Select().From<SkmLatestActivity>().NoLock()
                .Where(SkmLatestActivity.Columns.SellerGuid).IsEqualTo(sid)
                .OrderDesc()
                .ExecuteAsCollection<SkmLatestActivityCollection>();
        }
        public SkmLatestActivity SkmLatestActivityGet(Guid guid)
        {
            return DB.Get<SkmLatestActivity>(SkmLatestActivity.Columns.Guid, guid);
        }

        public void SkmLatestActivitySet(SkmLatestActivity act)
        {
            DB.Save<SkmLatestActivity>(act);
        }

        public SkmLatestSellerDealCollection SkmLatestSellerDealGetBySid(Guid sid)
        {
            return DB.Select().From<SkmLatestSellerDeal>().NoLock()
                .Where(SkmLatestSellerDeal.Columns.SellerGuid).IsEqualTo(sid)
                .OrderDesc()
                .ExecuteAsCollection<SkmLatestSellerDealCollection>();
        }
        public void SkmLatestSellerDealSet(SkmLatestSellerDeal sdeal)
        {
            DB.Save<SkmLatestSellerDeal>(sdeal);
        }


        #endregion

        #region Skm_Deal_Time_Slot
        public SkmDealTimeSlot SkmDealTimeSlotGet(int id)
        {
            return DB.Get<SkmDealTimeSlot>(SkmDealTimeSlot.Columns.Id, id);
        }
        public SkmDealTimeSlotCollection SkmDealTimeSlotGetByBid(Guid bid)
        {
            return DB.Select().From<SkmDealTimeSlot>().NoLock()
                .Where(SkmDealTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<SkmDealTimeSlotCollection>();
        }
        
        public SkmDealTimeSlotCollection SkmDealTimeSlotGetByDate(DateTime effectiveSdate)
        {
            SkmDealTimeSlotCollection sdtsc = new SkmDealTimeSlotCollection();
            string sql = @"select ts.* from skm_deal_time_slot ts with(nolock) inner join
                            business_hour bh with(nolock) On ts.business_hour_guid = bh.GUID
                            where effective_start = @effectiveSdate and bh.business_hour_order_time_s < getdate()
                            ";
            QueryCommand qc = new QueryCommand(sql, SkmDealTimeSlot.Schema.Provider.Name);
            qc.AddParameter("@effectiveSdate", effectiveSdate, DbType.DateTime);
            sdtsc.LoadAndCloseReader(DataService.GetReader(qc));
            return sdtsc;
        }

        /// <summary>
        /// 依時間取得檔次列表
        /// </summary>
        /// <param name="effectiveSdate">要取得的時間</param>
        /// <returns>檔次列表</returns>
        public SkmDealTimeSlotCollection SkmDealTimeSlotGetAllByDate(DateTime effectiveSdate)
        {
            SkmDealTimeSlotCollection sdtsc = new SkmDealTimeSlotCollection();
            string sql = @"select ts.* from skm_deal_time_slot ts with(nolock) inner join
                            business_hour bh with(nolock) On ts.business_hour_guid = bh.GUID
                            where effective_start = @effectiveSdate
                            ";
            QueryCommand qc = new QueryCommand(sql, SkmDealTimeSlot.Schema.Provider.Name);
            qc.AddParameter("@effectiveSdate", effectiveSdate, DbType.DateTime);
            sdtsc.LoadAndCloseReader(DataService.GetReader(qc));
            return sdtsc;
        }

        public SkmDealTimeSlot SkmDealTimeSlotGetByBidAndDate(Guid bid, DateTime effective_sdate, string CategoryType, string DealType)
        {
            return DB.Select().From<SkmDealTimeSlot>().NoLock()
                .Where(SkmDealTimeSlot.Columns.EffectiveStart).IsEqualTo(effective_sdate)
                .And(SkmDealTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(SkmDealTimeSlot.Columns.CategoryType).IsEqualTo(CategoryType)
                .And(SkmDealTimeSlot.Columns.DealType).IsEqualTo(DealType)
                .ExecuteAsCollection<SkmDealTimeSlotCollection>().FirstOrDefault();
        }

        public SkmDealTimeSlot SkmDealTimeSlotGetByBidAndDate(Guid bid, DateTime effective_sdate)
        {
            return DB.Select().From<SkmDealTimeSlot>().NoLock()
                .Where(SkmDealTimeSlot.Columns.EffectiveStart).IsEqualTo(effective_sdate)
                .And(SkmDealTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<SkmDealTimeSlotCollection>().FirstOrDefault();
        }
        public SkmDealTimeSlotCollection SkmDealTimeSlotGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", SkmDealTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(SkmDealTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select * from " + SkmDealTimeSlot.Schema.Provider.DelimitDbName(SkmDealTimeSlot.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            SkmDealTimeSlotCollection rtnCol = new SkmDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public SkmDealTimeSlotCollection SkmDealTimeSlotGetList(DateTime startDate, DateTime endDate, int cityId, List<Guid> sellerGuids)
        {
            string sql = string.Format(@"select ts.* from [skm_deal_time_slot] ts with(nolock) 
                            inner join business_hour bh with(nolock) 
                            On ts.business_hour_guid = bh.GUID
                            WHERE bh.seller_GUID in ({0})
                            AND ts.[effective_start] >= @sdate
                            AND ts.[effective_start] <= @edate 
                            AND ts.[city_id] = @cityId", string.Join(",", sellerGuids.Select(x => string.Format("'{0}'", x)).ToList()));

            SkmDealTimeSlotCollection sdtsc = new SkmDealTimeSlotCollection();
            QueryCommand qc = new QueryCommand(sql, SkmDealTimeSlot.Schema.Provider.Name);
            qc.AddParameter("@sdate", startDate, DbType.DateTime);
            qc.AddParameter("@edate", endDate, DbType.DateTime);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            sdtsc.LoadAndCloseReader(DataService.GetReader(qc));
            return sdtsc;
        }

        public bool SkmDealTimeSlotSet(SkmDealTimeSlot skmDealTimeSlot)
        {
            return DB.Save(skmDealTimeSlot) > 0;
        }

        public bool SkmDealTimeSlotSaveAll(SkmDealTimeSlotCollection skmDealTimeSlots)
        {
            return DB.SaveAll(skmDealTimeSlots) > 0;
        }

        public bool UpdateSkmDealTimeSlotSetStatus(int status, List<Guid?> bids)
        {
            return DB.Update<SkmDealTimeSlot>().Set(SkmDealTimeSlot.Columns.Status).EqualTo(status)
                .Where(SkmDealTimeSlot.Columns.BusinessHourGuid).In(bids).Execute() > 0;
        }

        public void SkmDealTimeSlotDelete(int id)
        {
            DB.Delete<SkmDealTimeSlot>(SkmDealTimeSlot.Columns.Id, id);
        }

        public void SkmDealTimeSlotDeleteAll(List<int> ids)
        {
            DB.Delete().From(SkmDealTimeSlot.Schema.TableName).Where(SkmDealTimeSlot.Columns.Id).In(ids).Execute();
        }
        #endregion

        #region Skm_Beacon_Message
        public SkmBeaconMessage SkmBeaconMessageGet(int id)
        {
            return DB.Get<SkmBeaconMessage>(SkmBeaconMessage.Columns.Id, id);
        }
        public SkmBeaconMessage SkmBeaconMessageGetByBid(Guid bid, string shop_code, string floor)
        {
            List<string> fl = new List<string>();
            fl.Add(string.Format("{0}", floor));
            if (floor.Length == 3)
            {
                if (floor.Substring(0, 1).ToLower() == "b")
                {
                    fl.Add(string.Format("{0}", floor.Replace("F", "")));
                }
                else if (floor.Substring(0, 1) == "0")
                {
                    fl.Add(string.Format("{0}", floor.Substring(1, floor.Length - 1)));
                }
            }
            else if (floor.Length == 2)
            {
                if (floor.Substring(0, 1).ToLower() == "b")
                {
                    fl.Add(string.Format("{0}F", floor));
                }
                else if (floor.ToLower().EndsWith("f"))
                {
                    fl.Add(string.Format("{0}", floor.PadLeft(3, '0')));
                }
            }
            
            return DB.Select().From<SkmBeaconMessage>().NoLock()
                .Where(SkmBeaconMessage.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(SkmBeaconMessage.Columns.ShopCode).IsEqualTo(shop_code)
                .And(SkmBeaconMessage.Columns.Floor).In(fl)
                .ExecuteAsCollection<SkmBeaconMessageCollection>().FirstOrDefault();
        }
        public SkmBeaconMessage SkmBeaconMessageGetByBid(Guid bid, string shop_code)
        {
            return DB.Select().From<SkmBeaconMessage>().NoLock()
                .Where(SkmBeaconMessage.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(SkmBeaconMessage.Columns.ShopCode).IsEqualTo(shop_code)
                .ExecuteAsCollection<SkmBeaconMessageCollection>().FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="skmBeaconChecked">測試期間不綁定特定Beacon</param>
        /// <param name="date">日期</param>
        /// <param name="time">時間</param>
        /// <param name="beaconId">Major+Minor</param>
        /// <returns></returns>
        public SkmBeaconMessageCollection SkmBeaconMessageGetListByTimeByBeaconID(bool skmBeaconChecked, string date, string time, string beaconId)
        {
            string sql = @"SELECT msg.* 
            FROM skm_beacon_message msg  with(nolock) 
            inner join skm_beacon_message_info info  with(nolock) on msg.guid = info.guid 
            where msg.effective_start <= @ddate and msg.effective_end  > @ddate 
            and msg.time_start <= convert(varchar, @ttime, 108) and msg.time_end > convert(varchar, @ttime, 108) 
            and msg.beacon is not null ";

            if (skmBeaconChecked)
            {
                sql += "and info.beacon_id = @beaconId";
            }

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@ddate", date, DbType.DateTime);
            qc.AddParameter("@ttime", time, DbType.String);
            qc.AddParameter("@beaconId", beaconId, DbType.String);

            SkmBeaconMessageCollection rtnCol = new SkmBeaconMessageCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public SkmBeaconMessageCollection SkmBeaconMessageGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", SkmBeaconMessage.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(SkmBeaconMessage.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select * from " + SkmBeaconMessage.Schema.Provider.DelimitDbName(SkmBeaconMessage.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            SkmBeaconMessageCollection rtnCol = new SkmBeaconMessageCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }
        public bool SkmBeaconMessageSet(SkmBeaconMessage skmBeaconMessage)
        {
            return DB.Save(skmBeaconMessage) > 0;
        }
        public void SkmBeaconMessageDelete(int id)
        {
            DB.Delete<SkmBeaconMessage>(SkmBeaconMessage.Columns.Id, id);
        }
        #endregion Skm_Beacon_Message

        #region Skm_Beacon_Message_Info

        public SkmBeaconMessageInfoCollection SkmBeaconMessageInfoAll()
        {
            return DB.Select().From<SkmBeaconMessageInfo>().NoLock()
                .ExecuteAsCollection<SkmBeaconMessageInfoCollection>();
        }

        public SkmBeaconMessageInfoCollection SkmBeaconMessageInfoGet(Guid guid)
        {
            return DB.Select().From<SkmBeaconMessageInfo>().NoLock()
                .Where(SkmBeaconMessageInfo.Columns.Guid).IsEqualTo(guid)
                .ExecuteAsCollection<SkmBeaconMessageInfoCollection>();
        }
        public bool SkmBeaconMessageInfoSet(SkmBeaconMessageInfo skmBeaconMessageInfo)
        {
            return DB.Save(skmBeaconMessageInfo) > 0;
        }
        public void SkmBeaconMessageInfoDelete(Guid guid)
        {
            DB.Delete<SkmBeaconMessageInfo>(SkmBeaconMessageInfo.Columns.Guid, guid);
        }
        #endregion Skm_Beacon_Message_Info

        #region Skm_Beacon_Message_Device_Info_Link

        public void SkmBeaconMessageDeviceInfoLinkDeleteBySkmBeaconMessageId(int skmBeaconMessageId)
        {
            DB.Delete().From(SkmBeaconMessageDeviceInfoLink.Schema.TableName).Where(SkmBeaconMessageDeviceInfoLink.Columns.SkmBeaconMessageId).IsEqualTo(skmBeaconMessageId).Execute();
        }
   
        public void SkmBeaconMessageDeviceInfoLinkSet(SkmBeaconMessageDeviceInfoLink skmBeaconMessageDeviceInfoLink)
        {
            DB.Save(skmBeaconMessageDeviceInfoLink);
        }
        #endregion Skm_Beacon_Message_Device_Info_Link

        #region ViewSkmBeaconMessagePushRecord

        public ViewSkmBeaconMessagePushRecordCollection GetBeaconMessagePushRecordByUserId(int userId)
        {
            return DB.Select().From<ViewSkmBeaconMessagePushRecord>().NoLock()
                .Where(ViewSkmBeaconMessagePushRecord.Columns.MemberId).IsEqualTo(userId)
                .ExecuteAsCollection<ViewSkmBeaconMessagePushRecordCollection>();
        }

        #endregion 

        #region Skm_Beacon_Message_time_slot
        public SkmBeaconMessageTimeSlot SkmBeaconMessageTimeSlotGet(int id)
        {
            return DB.Get<SkmBeaconMessageTimeSlot>(SkmBeaconMessageTimeSlot.Columns.Id, id);
        }
        public SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetByBid(Guid bid, int beaconType)
        {
            return DB.Select().From<SkmBeaconMessageTimeSlot>().NoLock()
                .Where(SkmBeaconMessageTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(SkmBeaconMessageTimeSlot.Columns.BeaconType).IsEqualTo(beaconType)
                .ExecuteAsCollection<SkmBeaconMessageTimeSlotCollection>();
        }

        public bool UpdateSkmBeaconMessageTimeSlotStatus(int status, List<Guid?> bids, int beaconType)
        {
            return DB.Update<SkmBeaconMessageTimeSlot>().Set(SkmBeaconMessageTimeSlot.Columns.Status).EqualTo(status)
                .Where(SkmBeaconMessageTimeSlot.Columns.BusinessHourGuid).In(bids)
                .And(SkmBeaconMessageTimeSlot.Columns.BeaconType).IsEqualTo(beaconType)
                .Execute() > 0;
        }

        public SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", SkmBeaconMessageTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(SkmBeaconMessageTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select * from " + SkmBeaconMessageTimeSlot.Schema.Provider.DelimitDbName(SkmBeaconMessageTimeSlot.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            SkmBeaconMessageTimeSlotCollection rtnCol = new SkmBeaconMessageTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }
        
        public SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetListByDate(string orderBy, string startDate, string endDate, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", SkmBeaconMessageTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(SkmBeaconMessageTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            string sql = "select * from " + SkmBeaconMessageTimeSlot.Schema.Provider.DelimitDbName(SkmBeaconMessageTimeSlot.Schema.TableName) + " with(nolock) ";
            sql += qc.CommandSql;

            sql += " and CONVERT(CHAR(10),effective_start, 111) >= '" + startDate + "'";
            sql += " and CONVERT(CHAR(10),effective_end, 111) <= '" + endDate + "'";

            qc.CommandSql = sql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            SkmBeaconMessageTimeSlotCollection rtnCol = new SkmBeaconMessageTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public SkmBeaconMessageTimeSlotCollection SkmBeaconMessageTimeSlotGetListByDateV1(string orderBy, string startDate, string endDate, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", SkmBeaconMessageTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(SkmBeaconMessageTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            string sql = "select * from " + SkmBeaconMessageTimeSlot.Schema.Provider.DelimitDbName(SkmBeaconMessageTimeSlot.Schema.TableName) + " with(nolock) ";
            sql += qc.CommandSql;

            sql += " and effective_start >= @effective_start and effective_end <= @effective_end";
            qc.AddParameter("@effective_start", startDate, DbType.DateTime);
            qc.AddParameter("@effective_end", endDate, DbType.DateTime);

            qc.CommandSql = sql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            SkmBeaconMessageTimeSlotCollection rtnCol = new SkmBeaconMessageTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public bool SkmBeaconMessageTimeSlotSet(SkmBeaconMessageTimeSlot skmBeaconMessageTimeSlot)
        {
            return DB.Save(skmBeaconMessageTimeSlot) > 0;
        }
        public void SkmBeaconMessageTimeSlotDelete(int id)
        {
            DB.Delete<SkmBeaconMessageTimeSlot>(SkmBeaconMessageTimeSlot.Columns.Id, id);
        }
        #endregion Skm_Beacon_Message_time_slot

        #region skmBeaconGroup
        public SkmBeaconGroupCollection SkmBeaconGroupGetByGuid(Guid gid)
        {
            return DB.Select().From<SkmBeaconGroup>().NoLock()
                .Where(SkmBeaconGroup.Columns.Guid).IsEqualTo(gid)
                .ExecuteAsCollection<SkmBeaconGroupCollection>();
        }
        public int SkmBeaconGroupGetCountByGuid(Guid gid)
        {
            string sql = @"select count(*) from skm_beacon_groups with(nolock) where guid=@oid";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@oid", gid, DbType.Guid);
            
            return (int)DataService.ExecuteScalar(qc);
        }
        public SkmBeaconGroupCollection SkmBeaconGroupGetByGuid(int pageStart, int pageLength, string orderBy, Guid gid)
        {
            return SkmBeaconGroupGetList(pageStart, pageLength, orderBy, gid);
        }
        public SkmBeaconGroupCollection SkmBeaconGroupGetList(int pageStart, int pageLengh, string orderBy, Guid gid)
        {
            string sql = @"select * from skm_beacon_groups with(nolock) where guid=@oid order by id desc ";

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@oid", gid, DbType.Guid);

            qc = SSHelper.MakePagable(qc, pageStart, pageLengh, GetSkmBeaconGroupSql(orderBy, null));
            SkmBeaconGroupCollection data = new SkmBeaconGroupCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        private string GetSkmBeaconGroupSql(string orderBy, string tableAlias)
        {
            if (string.IsNullOrEmpty(tableAlias))
                tableAlias = OrderDetail.Schema.TableName;
            return "(" + tableAlias + ".status&" + (int)OrderDetailStatus.SystemEntry + ")" +
                   (string.IsNullOrEmpty(orderBy) ? "" : "," + orderBy);
        }

        public void SkmBeaconGroupDelete(Guid gid)
        {
            DB.Delete<SkmBeaconGroup>(SkmBeaconGroup.Columns.Guid, gid);
        }
        public bool SkmBeaconGroupSet(SkmBeaconGroupCollection groups)
        {
            DB.BulkInsert(groups);
            return true;
        }
        #endregion

        #region skmlog

        public bool SkmLogSet(SkmLog skmLog)
        {
            skmLog.CreateTime = DateTime.Now;
            return DB.Save(skmLog) > 0;
        }

        #endregion

        #region skm_burning_event

        public bool SkmBurningEventSet(SkmBurningEvent e)
        {
            e.ModifyTime = DateTime.Now;
            return DB.Save(e) > 0;
        }

        public List<SkmBurningEvent> SkmBurningEventListGet(List<Guid> externalGuidList, bool checkStatus)
        {
            SkmBurningEventCollection col = new SkmBurningEventCollection();
            for (int i = 0; i < externalGuidList.Count; i += 2000)
            {
                col.AddRange(DB.SelectAllColumnsFrom<SkmBurningEvent>().NoLock()
                    .Where(SkmBurningEvent.Columns.ExternalGuid)
                    .In(externalGuidList.Skip(i).Take(2000))
                    .ExecuteAsCollection<SkmBurningEventCollection>());
            }

            if (col.Any())
            {
                if (checkStatus)
                {
                    return col.Where(y => y.Status == (byte)SkmBurningEventStatus.Create).OrderByDescending(x => x.Id).ToList();
                }

                return col.OrderByDescending(x => x.Id).ToList();
            }

            return new List<SkmBurningEvent>();
        }
        public SkmBurningEvent SkmBurningEventGet(Guid externalGuid, string storeCode, bool checkStatus)
        {
            var col = string.IsNullOrEmpty(storeCode) ? DB.SelectAllColumnsFrom<SkmBurningEvent>().NoLock()
                .Where(SkmBurningEvent.Columns.ExternalGuid)
                .IsEqualTo(externalGuid)
                .ExecuteAsCollection<SkmBurningEventCollection>() :
                DB.SelectAllColumnsFrom<SkmBurningEvent>().NoLock()
                    .Where(SkmBurningEvent.Columns.ExternalGuid)
                    .IsEqualTo(externalGuid)
                    .And(SkmBurningEvent.Columns.StoreCode).IsEqualTo(storeCode)
                    .ExecuteAsCollection<SkmBurningEventCollection>();

            if (col.Any())
            {
                if (checkStatus)
                {
                    return col.Where(y => y.Status == (byte)SkmBurningEventStatus.Create).OrderByDescending(x => x.Id).FirstOrDefault() ?? new SkmBurningEvent();
                }

                return col.OrderByDescending(x => x.Id).FirstOrDefault() ?? new SkmBurningEvent();
            }

            return new SkmBurningEvent();
        }

        public List<SkmBurningEvent> SkmBurningEventGet(Guid externalGuid, bool checkStatus)
        {
            var col = DB.SelectAllColumnsFrom<SkmBurningEvent>().NoLock()
                .Where(SkmBurningEvent.Columns.ExternalGuid)
                .IsEqualTo(externalGuid)
                .ExecuteAsCollection<SkmBurningEventCollection>();

            if (col.Any())
            {
                if (checkStatus)
                {
                    return col.Where(y => y.Status == (byte)SkmBurningEventStatus.Create).OrderByDescending(x => x.Id).ToList();
                }

                return col.OrderByDescending(x => x.Id).ToList();
            }

            return new List<SkmBurningEvent>();
        }

        public int SkmBurningEventGetMonthCountByDate(DateTime eventDate)
        {
            var sdate = new DateTime(eventDate.Year, eventDate.Month, 1).SetTime(0, 0, 0);
            var edate = new DateTime(eventDate.AddMonths(1).Year, DateTime.Now.AddMonths(1).Month, 1).AddDays(-1).SetTime(23,59,59);

            string sql = @"SELECT COUNT(*) FROM skm_burning_event WHERE create_time BETWEEN @startDate AND @endDate";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@startDate", sdate, DbType.DateTime);
            qc.AddParameter("@endDate", edate, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        public int SkmBurningEventGetYearCountByDate(DateTime eventDate)
        {
            var sdate = new DateTime(eventDate.Year, 1, 1).SetTime(0, 0, 0);
            var edate = new DateTime(eventDate.Year, 12, 31).SetTime(23, 59, 59);

            string sql = @"SELECT COUNT(*) FROM skm_burning_event WHERE create_time BETWEEN @startDate AND @endDate";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@startDate", sdate, DbType.DateTime);
            qc.AddParameter("@endDate", edate, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion

        #region skmCostCenterInfo

        public SkmCostCenterInfoCollection SkmCostCenterInfoGetAll()
        {
            return DB.SelectAllColumnsFrom<SkmCostCenterInfo>().ExecuteAsCollection<SkmCostCenterInfoCollection>();
        }

        public SkmCostCenterInfoCollection SkmCostCenterInfoGetByStoreCode(string storeCode)
        {
            return DB.SelectAllColumnsFrom<SkmCostCenterInfo>().NoLock()
                       .Where(SkmCostCenterInfo.Columns.StoreCode).IsEqualTo(storeCode)
                       .ExecuteAsCollection<SkmCostCenterInfoCollection>() ?? new SkmCostCenterInfoCollection();
        }

        #endregion

        #region skm_burning_cost_center

        public bool SkmBurningCostCenterSet(SkmBurningCostCenterCollection centerCol)
        {
            return DB.SaveAll(centerCol) > 0;
        }

        public SkmBurningCostCenterCollection SkmBurningCostCenterGet(int burningEventId)
        {
            return DB.SelectAllColumnsFrom<SkmBurningCostCenter>().NoLock()
                .Where(SkmBurningCostCenter.Columns.BurningEventId).IsEqualTo(burningEventId)
                .ExecuteAsCollection<SkmBurningCostCenterCollection>() ?? new SkmBurningCostCenterCollection();
        }

        public SkmBurningCostCenterCollection SkmBurningCostCenterGet(List<int> burningEventIds)
        {
            return DB.SelectAllColumnsFrom<SkmBurningCostCenter>().NoLock()
                       .Where(SkmBurningCostCenter.Columns.BurningEventId).In(burningEventIds)
                       .ExecuteAsCollection<SkmBurningCostCenterCollection>() ?? new SkmBurningCostCenterCollection();
        }

        public void SkmBurningCostCenterDel(int eventId)
        {
            DB.Delete<SkmBurningCostCenter>(SkmBurningCostCenter.Columns.BurningEventId, eventId);
        }

        #endregion

        #region skm_burning_order_log

        public bool SkmBurningOrderLogSet(SkmBurningOrderLog log)
        {
            log.ModifyTime = DateTime.Now;
            return DB.Save(log) > 0;
        }

        public int SkmBurningOrderCountByEventId(int eventId)
        {
            string sql = @"select count(*) from skm_burning_order_log where event_id=@eid AND status in (@status1, @status2)";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@eid", eventId, DbType.Int32);
            qc.AddParameter("@status1", SkmBurningOrderStatus.Create, DbType.Byte);
            qc.AddParameter("@status2", SkmBurningOrderStatus.Fail, DbType.Byte);
            return (int)DataService.ExecuteScalar(qc);
        }

        public SkmBurningOrderLog SkmBurningOrderLogGet(int userId, Guid orderGuid, int couponId)
        {
            var col =
                DB.SelectAllColumnsFrom<SkmBurningOrderLog>().Where(SkmBurningOrderLog.Columns.UserId).IsEqualTo(userId)
                    .And(SkmBurningOrderLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                    .And(SkmBurningOrderLog.Columns.CouponId).IsEqualTo(couponId)
                    .ExecuteAsCollection<SkmBurningOrderLogCollection>();
            return col.FirstOrDefault(x => x.Status == (byte) SkmBurningOrderStatus.Create) ?? new SkmBurningOrderLog();
        }

        #endregion

        #region skm_system_log

        public void SkmSystemLogSet(SkmSystemLogType type, int logId, string content)
        {
            var log = new SkmSystemLog
            {
                LogType = (byte) type,
                LogId = logId,
                LogContent = content,
                CreateTime = DateTime.Now
            };

            DB.Save(log);
        }

        #endregion

        #region skm_member_ph
        public SkmMemberPh SkmMemberPhGetBySkmToken(string skmToken)
        {
            return DB.SelectAllColumnsFrom<SkmMemberPh>().NoLock().Where(SkmMemberPh.Columns.SkmToken).IsEqualTo(skmToken)
             .ExecuteSingle<SkmMemberPh>();
        }

        public bool SkmMemberPhCollectionBulkInsert(SkmMemberPhCollection smpCol)
        {
            return DB.BulkInsert(smpCol);
        }

        public bool SkmMemberPhCollectionSet(SkmMemberPhCollection smpCol)
        {
            return DB.SaveAll(smpCol) > 0;
        }
        #endregion
    }
}

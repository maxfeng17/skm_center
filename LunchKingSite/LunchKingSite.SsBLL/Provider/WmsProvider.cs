﻿using System;
using System.Data.Entity;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.SsBLL.DbContexts;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using SubSonic;
using System.Data;

namespace LunchKingSite.SsBLL.Provider
{
    public class WmsProvider : IWmsProvider
    {
        public WmsContact WmsContactGet(string accountId)
        {
            using (var dbx = new PponDbContext())
            {
                return dbx.WmsContactDbSet.FirstOrDefault(t => t.AccountId == accountId);
            }
        }

        public void WmsContactSave(WmsContact contact, string createId)
        {
            if (contact == null || string.IsNullOrEmpty(contact.AccountId))
            {
                throw new Exception("PchomeContactSave fail, contact 為 null 或 未填寫 AccountId.");
            }
            using (var db = new PponDbContext())
            {
                if (contact.Id == 0)
                {
                    contact.CreateId = createId;
                    contact.CreateTime = DateTime.Now;
                    db.Entry(contact).State = EntityState.Added;
                }
                else
                {
                    contact.ModifyId = createId;
                    contact.ModifyTime = DateTime.Now;
                    db.Entry(contact).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public WmsContact WmsContactGetBySellerGuid(Guid sellerGuid)
        {
            using (var db = new PponDbContext())
            {
                string sql = string.Format(@"
                select w.id as Id,w.account_id as AccountId,w.returner_name as ReturnerName,w.returner_tel as ReturnerTel,w.returner_mobile as ReturnerMobile,w.returner_email as ReturnerEmail,
				w.returner_zip as ReturnerZip,w.returner_address as ReturnerAddress,w.warehousing_name as WarehousingName,w.warehousing_tel as WarehousingTel,w.warehousing_mobile as  WarehousingMobile,
				w.warehousing_email as  WarehousingEmail,w.warehousing_zip as WarehousingZip,w.warehousing_address as WarehousingAddress,w.create_id as CreateId,w.create_time as CreateTime,
				w.modify_id as ModifyId,w.modify_time as ModifyTime from resource_acl r 
                inner join vbs_membership v on r.account_id=v.account_id
                inner join wms_contact w on w.account_id=v.account_id
                where  resource_guid='{0}' and resource_type=" + (int)ResourceType.Seller + @" and view_wms_right=1
                ", sellerGuid);


                var result = db.Database.SqlQuery<WmsContact>(sql).FirstOrDefault();
                return result;
            }
        }

        public ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrderListGetByItemGuids(List<Guid> itemGuids)
        {
            var result = new ViewWmsPurchaseOrderCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                result.AddRange(DB.SelectAllColumnsFrom<ViewWmsPurchaseOrder>()
                .Where(ViewWmsPurchaseOrder.Columns.ItemGuid).In(itemGuids.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewWmsPurchaseOrderCollection>());
                idx += batchLimit;
            } while (idx <= itemGuids.Count - 1);

            return result;
        }

        public ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrderListGetByPurchaseOrderGuid(List<Guid> purchaseOrderGuid)
        {
            var result = new ViewWmsPurchaseOrderCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                result.AddRange(DB.SelectAllColumnsFrom<ViewWmsPurchaseOrder>()
                .Where(ViewWmsPurchaseOrder.Columns.PurchaseOrderGuid).In(purchaseOrderGuid.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewWmsPurchaseOrderCollection>());
                idx += batchLimit;
            } while (idx <= purchaseOrderGuid.Count - 1);

            return result;
        }

        public ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrdermList()
        {
            return DB.SelectAllColumnsFrom<ViewWmsPurchaseOrder>().NoLock()
                        .ExecuteAsCollection<ViewWmsPurchaseOrderCollection>();
        }

        public ViewWmsPurchaseOrderCollection ViewWmsPurchaseOrdermListGetBySellerGuidList(List<Guid> sellerGuids)
        {
            return DB.SelectAllColumnsFrom<ViewWmsPurchaseOrder>().NoLock()
                        .Where(ViewWmsPurchaseOrder.Columns.SellerGuid).In(sellerGuids)
                        .ExecuteAsCollection<ViewWmsPurchaseOrderCollection>();
        }


        public void WmsPurchaseOrderSet(WmsPurchaseOrderCollection order)
        {
            DB.SaveAll(order);
        }

        public void WmsPurchaseOrderSet(WmsPurchaseOrder wpo)
        {
            DB.Save(wpo);
        }

        public WmsPurchaseOrder WmsPurchaseOrderGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<WmsPurchaseOrder>().NoLock()
                        .Where(WmsPurchaseOrder.Columns.Guid).In(guid)
                        .ExecuteSingle<WmsPurchaseOrder>();
        }

        public WmsPurchaseOrder WmsPurchaseOrderGetWithUpdateLock(Guid guid)
        {
            return DB.SelectAllColumnsFrom<WmsPurchaseOrder>().UpdateLock()
                        .Where(WmsPurchaseOrder.Columns.Guid).In(guid)
                        .ExecuteSingle<WmsPurchaseOrder>();
        }

        public void UpdtaeWmsPurchaseOrderStatus(List<Guid> guid, int status, string userName)
        {

            string sql = "update wms_purchase_order set status=@status,modify_id=@userName,modify_time=getdate() where guid in (";
            int count = 1;
            foreach (Guid g in guid)
            {
                sql += "@guid" + count.ToString() + ",";
                count++;
            }
            sql = sql.TrimEnd(",") + ")";


            QueryCommand qc = new QueryCommand(sql, WmsPurchaseOrder.Schema.Provider.Name);

            count = 1;
            foreach (Guid g in guid)
            {
                qc.AddParameter("@guid" + count, g, DbType.Guid);
                count++;
            }

            qc.AddParameter("@userName", userName, DbType.String);
            qc.AddParameter("@status", status, DbType.Int32);
            DataService.ExecuteScalar(qc);

        }
        /// <summary>
        /// 取得需要跟PChome倉儲，確認狀態的進貨資料列表
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public WmsPurchaseOrderCollection WmsPurchaseOrderGetListForWmsCheck()
        {
            string sql = string.Format(@"
                select * from wms_purchase_order where status in ({0},{1}) and pchome_purchase_order_id is not null
                and (invalidation = 0 or invalidation is null)",
                (int)PurchaseOrderStatus.Submit, (int)PurchaseOrderStatus.WmsArrived);

            QueryCommand qc = new QueryCommand(sql, WmsPurchaseOrder.Schema.Provider.Name);
            WmsPurchaseOrderCollection list = new WmsPurchaseOrderCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        public IList<ViewWmsOrder> GetSuccessfulViewWmsOrdersBySendStatus(WmsOrderSendStatus status)
        {
            string sql = @"
                select * from view_wms_order vwo with(nolock) where vwo.status = @status 
                    and order_status & 8 > 0 and order_status & 512 = 0 and is_canceling=0
                    and create_time < DATEADD (minute , -1 , getdate()) ";
            QueryCommand qc = new QueryCommand(sql, ViewWmsOrder.Schema.Provider.Name);
            qc.AddParameter("@status", (int)status, DbType.Int32);


            ViewWmsOrderCollection vwoCol = new ViewWmsOrderCollection();
            vwoCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vwoCol;
        }

        public IList<ViewWmsOrder> GetSuccessfulViewWmsOrdersByOrderGuid(Guid oid)
        {
            string sql = @"
                select * from view_wms_order vwo with(nolock) where vwo.status = @status 
                    and order_status & 512 = 0
                    and order_guid=@order_guid
                    ";
            QueryCommand qc = new QueryCommand(sql, ViewWmsOrder.Schema.Provider.Name);
            qc.AddParameter("@status", (int)WmsOrderSendStatus.Init, DbType.Int32);
            qc.AddParameter("@order_guid", oid, DbType.Guid);


            ViewWmsOrderCollection vwoCol = new ViewWmsOrderCollection();
            vwoCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vwoCol;
        }

        public void WmsOrderSet(WmsOrder wmsOrder)
        {
            DB.Save(wmsOrder);
        }

        public WmsOrder WmsOrderGet(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<WmsOrder>().NoLock()
                        .Where(WmsOrder.Columns.OrderGuid).In(orderGuid)
                        .ExecuteSingle<WmsOrder>();
        }

        public WmsOrder WmsOrderGetByPchomeId(string pchomeOrderId)
        {
            return DB.Get<WmsOrder>(WmsOrder.Columns.WmsOrderId, pchomeOrderId);
        }

        public WmsOrderCollection GetReadySyncShipStatusOfWmsOrders()
        {
            return DB.SelectAllColumnsFrom<WmsOrder>().NoLock()
                        .Where(WmsOrder.Columns.Status).IsEqualTo((int)WmsOrderSendStatus.Success)
                        .And(WmsOrder.Columns.ShipStatus).IsLessThan((int)WmsDeliveryStatus.Arrived) //已送達是最終狀態
                        .ExecuteAsCollection<WmsOrderCollection>();
        }

        public WmsOrderCollection WmsOrderGetFailure(DateTime sDate, DateTime eDate)
        {
            return DB.SelectAllColumnsFrom<WmsOrder>().NoLock()
                        .Where(WmsOrder.Columns.Status).IsEqualTo((int)WmsOrderSendStatus.failure)
                        .And(WmsOrder.Columns.CreateTime).IsGreaterThanOrEqualTo(sDate)
                        .And(WmsOrder.Columns.CreateTime).IsLessThanOrEqualTo(eDate)
                        .ExecuteAsCollection<WmsOrderCollection>();
        }

        public WmsOrderCollection GetReadySyncShipStatusOfWmsOrders(DateTime dateTime)
        {
            string sql = @"
                select * from wms_order wo with(nolock) where wo.status = @status 
                    and ship_status < @ship_status and (ship_date >= @ship_date or ship_date is null)";

            QueryCommand qc = new QueryCommand(sql, WmsOrder.Schema.Provider.Name);
            qc.AddParameter("@status", (int)WmsOrderSendStatus.Success, DbType.Int32);
            qc.AddParameter("@ship_status", (int)WmsDeliveryStatus.Arrived, DbType.Int32); //已送達是最終狀態
            qc.AddParameter("@ship_date", dateTime, DbType.DateTime);

            WmsOrderCollection woCol = new WmsOrderCollection();
            woCol.LoadAndCloseReader(DataService.GetReader(qc));
            return woCol;
        }

        public OrderShip OrderShipGet(Guid orderGuid, string pickId)
        {
            return DB.SelectAllColumnsFrom<OrderShip>().NoLock()
                       .Where(OrderShip.Columns.OrderGuid).IsEqualTo(orderGuid)
                       .And(OrderShip.Columns.PickId).IsEqualTo(pickId)
                      .ExecuteSingle<OrderShip>();
        }

        public OrderShip OrderShipGetByshipNo(Guid orderGuid, string pickId, string shipNo)
        {
            return DB.SelectAllColumnsFrom<OrderShip>().NoLock()
                       .Where(OrderShip.Columns.OrderGuid).IsEqualTo(orderGuid)
                       .And(OrderShip.Columns.PickId).IsEqualTo(pickId)
                       .And(OrderShip.Columns.ShipNo).IsEqualTo(shipNo)
                      .ExecuteSingle<OrderShip>();
        }

        public void OrderShipSet(OrderShip wmsOrderShip)
        {
            DB.Save<OrderShip>(wmsOrderShip);
        }

        public void OrderShipSave(OrderShip wmsOrderShip)
        {
            DB.Update<OrderShip>().Set(OrderShip.ProgressStatusColumn).EqualTo(wmsOrderShip.ProgressStatus)
                .Set(OrderShip.ProgressDateColumn).EqualTo(wmsOrderShip.ProgressDate)
                .Set(OrderShip.ShipTimeColumn).EqualTo(wmsOrderShip.ShipTime)
                .Set(OrderShip.ShipCompanyIdColumn).EqualTo(wmsOrderShip.ShipCompanyId)
                .Set(OrderShip.ShipNoColumn).EqualTo(wmsOrderShip.ShipNo)
                .Set(OrderShip.IsReceiptColumn).EqualTo(wmsOrderShip.IsReceipt)
                .Set(OrderShip.ReceiptTimeColumn).EqualTo(wmsOrderShip.ReceiptTime)
                .Set(OrderShip.ModifyIdColumn).EqualTo(wmsOrderShip.ModifyId)
                .Set(OrderShip.ModifyTimeColumn).EqualTo(wmsOrderShip.ModifyTime)
                .Where(OrderShip.Columns.Id).IsEqualTo(wmsOrderShip.Id)
                .Execute();
        }
        #region view_wms_return_order
        public ViewWmsReturnOrderCollection ViewWmsReturnOrderListGetByReturnOrderGuid(List<Guid> returnOrderGuid)
        {
            var result = new ViewWmsReturnOrderCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                result.AddRange(DB.SelectAllColumnsFrom<ViewWmsReturnOrder>()
                .Where(ViewWmsReturnOrder.Columns.ReturnOrderGuid).In(returnOrderGuid.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewWmsReturnOrderCollection>());
                idx += batchLimit;
            } while (idx <= returnOrderGuid.Count - 1);

            return result;
        }
        public ViewWmsReturnOrderCollection ViewWmsReturnOrdermListGetBySellerGuidList(
            int pageStart, int pageLength, List<Guid> sellerGuids, string productNo,
            string productBrandName, string productName, string pchomeProdId, string specs, string productCode,
            string sDate, string eDate, string rtnStatus, string source, bool invalidation)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewWmsReturnOrder>()
                .Where(ViewWmsReturnOrder.Columns.SellerGuid).In(sellerGuids);

            if (!string.IsNullOrEmpty(productNo))
            {
                int proNo = 0;
                int.TryParse(productNo, out proNo);
                query.And(ViewWmsReturnOrder.Columns.ProductNo).IsEqualTo(proNo);
            }
            if (!string.IsNullOrEmpty(productBrandName))
            {
                query.And(ViewWmsReturnOrder.Columns.ProductBrandName).ContainsString(productBrandName);
            }
            if (!string.IsNullOrEmpty(productName))
            {
                query.And(ViewWmsReturnOrder.Columns.ProductName).ContainsString(productName);
            }
            if (!string.IsNullOrEmpty(pchomeProdId))
            {
                query.And(ViewWmsReturnOrder.Columns.PchomeProdId).IsEqualTo(pchomeProdId);
            }
            if (!string.IsNullOrEmpty(specs))
            {
                query.And(ViewWmsReturnOrder.Columns.SpecName).ContainsString(specs);
            }
            if (!string.IsNullOrEmpty(productCode))
            {
                query.And(ViewWmsReturnOrder.Columns.ProductCode).IsEqualTo(productCode);
            }
            if (!string.IsNullOrEmpty(sDate))
            {
                query.And(ViewWmsReturnOrder.Columns.CreateTime).IsGreaterThanOrEqualTo(sDate);
            }
            if (!string.IsNullOrEmpty(eDate))
            {
                query.And(ViewWmsReturnOrder.Columns.CreateTime).IsLessThanOrEqualTo(eDate);
            }
            if (!string.IsNullOrEmpty(rtnStatus))
            {
                query.And(ViewWmsReturnOrder.Columns.Status).IsEqualTo(rtnStatus);
            }
            if (!string.IsNullOrEmpty(source))
            {
                query.And(ViewWmsReturnOrder.Columns.Source).IsEqualTo(source);
            }
            if (invalidation)
            {
                query.And(ViewWmsReturnOrder.Columns.Invalidation).IsNotEqualTo((int)WmsInvalidation.Yes);
            }
            return query.ExecuteAsCollection<ViewWmsReturnOrderCollection>() ?? new ViewWmsReturnOrderCollection();
        }

        public ViewWmsReturnOrder ViewWmsReturnOrderGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<ViewWmsReturnOrder>().NoLock()
                        .Where(ViewWmsReturnOrder.Columns.ReturnOrderGuid).In(guid)
                        .ExecuteSingle<ViewWmsReturnOrder>();
        }
        public ViewWmsReturnOrderCollection ViewWmsReturnOrdermList()
        {
            return DB.SelectAllColumnsFrom<ViewWmsReturnOrder>().NoLock()
                        .ExecuteAsCollection<ViewWmsReturnOrderCollection>();
        }
        public ViewWmsReturnOrderCollection ViewWmsReturnOrdermListByStatus(int returnOrderStatus, int invalidation)
        {
            return DB.SelectAllColumnsFrom<ViewWmsReturnOrder>().NoLock()
                     .Where(ViewWmsReturnOrder.Columns.Status).IsEqualTo(returnOrderStatus).And(ViewWmsReturnOrder.Columns.Invalidation).IsEqualTo(invalidation)
                        .ExecuteAsCollection<ViewWmsReturnOrderCollection>();
        }

        public ViewWmsReturnOrder ViewWmsReturnOrderListGetByPchomeProdId(string pchomeProdId)
        {
            return DB.SelectAllColumnsFrom<ViewWmsReturnOrder>().NoLock()
                     .Where(ViewWmsReturnOrder.Columns.PchomeProdId).IsEqualTo(pchomeProdId)
                     .ExecuteSingle<ViewWmsReturnOrder>();
        }

        #endregion

        #region wms_return_order
        public void WmsReturnOrderSet(WmsReturnOrder wmsReturnOrder)
        {
            DB.Save<WmsReturnOrder>(wmsReturnOrder);
        }
        public void WmsReturnOrderSet(WmsReturnOrderCollection returnOrders)
        {
            DB.SaveAll(returnOrders);
        }
        public WmsReturnOrder WmsReturnOrderGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<WmsReturnOrder>().NoLock()
                        .Where(WmsReturnOrder.Columns.Guid).In(guid)
                        .ExecuteSingle<WmsReturnOrder>();
        }
        public WmsReturnOrderCollection WmsReturnOrderByPchomeReturnId(string pchomeReturnId)
        {
            return DB.SelectAllColumnsFrom<WmsReturnOrder>().NoLock()
                        .Where(WmsReturnOrder.Columns.PchomeReturnOrderId).IsEqualTo(pchomeReturnId)
                        .ExecuteAsCollection<WmsReturnOrderCollection>();
        }
        public void UpdtaeWmsReturnOrderStatus(List<Guid> guid, int status, string userName)
        {

            string sql = "update wms_return_order set status=@status,invalidation=@invalidation,modify_id=@userName,modify_time=getdate() where guid in (";
            int count = 1;
            foreach (Guid g in guid)
            {
                sql += "@guid" + count.ToString() + ",";
                count++;
            }
            sql = sql.TrimEnd(",") + ")";


            QueryCommand qc = new QueryCommand(sql, WmsPurchaseOrder.Schema.Provider.Name);

            count = 1;
            foreach (Guid g in guid)
            {
                qc.AddParameter("@guid" + count, g, DbType.Guid);
                count++;
            }

            qc.AddParameter("@userName", userName, DbType.String);
            if (status == (int)WmsReturnOrderStatus.Fail)
            {
                qc.AddParameter("@invalidation", (int)WmsInvalidation.Yes, DbType.Int32);
            }
            else
            {
                qc.AddParameter("@invalidation", (int)WmsInvalidation.No, DbType.Int32);
            }
            qc.AddParameter("@status", status, DbType.Int32);
            DataService.ExecuteScalar(qc);

        }
        public void UpdtaeWmsReturnOrderInvalidationStatus(Guid returnOrderGuid, bool invalidationStatus, string userName)
        {
            DB.Update<WmsReturnOrder>().Set(WmsReturnOrder.InvalidationColumn).EqualTo(Convert.ToInt32(invalidationStatus))
                .Set(WmsReturnOrder.ModifyIdColumn).EqualTo(userName)
                .Set(WmsReturnOrder.ModifyTimeColumn).EqualTo(DateTime.Now)
                .Where(WmsReturnOrder.Columns.Guid).IsEqualTo(returnOrderGuid)
                .Execute();
        }
        #endregion

        #region wms_return_order_log
        public void WmsReturnOrderLogSet(WmsReturnOrderLogCollection orderLogs)
        {
            DB.SaveAll(orderLogs);
        }
        #endregion

        #region 24H 查詢訂單配送與物流
        public ViewWmsProposalOrderDeliveryCollection GetViewWmsProposalOrderDeliveryByVbs(string orderBy, string sellerGuids, bool isCompleteOrder, params string[] filter)
        {
            string defOrderBy = ViewWmsProposalOrderDelivery.Columns.Id;
            QueryCommand qc = GetVpodWhereQC(sellerGuids, filter);
            qc.CommandSql = "select view_wms_proposal_order_delivery.* from " + ViewWmsProposalOrderDelivery.Schema.Provider.DelimitDbName(ViewWmsProposalOrderDelivery.Schema.TableName) + " with(nolock) "
                        + qc.CommandSql;
            if (isCompleteOrder)
            {
                qc.CommandSql += " and " + ViewWmsProposalOrderDelivery.Columns.OrderStatus + " & " + (int)OrderStatus.Complete + " > 0 ";
            }

            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy + " desc ";
                defOrderBy = orderBy;
            }

            ViewWmsProposalOrderDeliveryCollection vcslCol = new ViewWmsProposalOrderDeliveryCollection();
            vcslCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcslCol;
        }

        protected QueryCommand GetVpodWhereQC(string sellerGuids, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewWmsProposalOrderDelivery.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewWmsProposalOrderDelivery.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                qc.CommandSql += string.Format(" and {0} IN ({1}) ", ViewWmsProposalOrderDelivery.Columns.SellerGuid, sellerGuids);
                DataProvider.AddWhereParameters(qc, qry);
            }
            else
            {
                qc.CommandSql += string.Format(" where {0} IN ({1}) ", ViewWmsProposalOrderDelivery.Columns.SellerGuid, sellerGuids);
            }
            return qc;
        }
        #endregion

        #region wms_reunf_order

        public WmsRefundOrder WmsRefundOrderGetByReturnFormId(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<WmsRefundOrder>().NoLock()
                       .Where(WmsRefundOrder.Columns.ReturnFormId).IsEqualTo(returnFormId)
                      .ExecuteSingle<WmsRefundOrder>();
        }


        #endregion

        #region WmsFee PCHOME倉儲費
        public List<Guid> GetWmsProductItemForBalanceSheetSeller()
        {
            List<Guid> result = new List<Guid>();

            string sql = @"select seller_guid from view_product_item where pchome_prod_id is not null group by seller_guid";

            QueryCommand qca = new QueryCommand(sql, ViewProductItem.Schema.Provider.Name);


            using (var reader = DataService.GetReader(qca))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }

            return result;
        }

        public ProductItemCollection GetViewProductItemBySellerProdId(Guid sellerGuid)
        {
            string sql = @"SELECT item.*
                          FROM product_item AS item
                          LEFT OUTER JOIN product_info AS info ON info.guid = item.info_guid
                          WHERE info.seller_guid = @sellerGuid  and item.pchome_prod_id is not null";

            QueryCommand qc = new QueryCommand(sql, ViewProductItem.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);

            ProductItemCollection list = new ProductItemCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }



        public void WmsPurchaseServFeeSet(WmsPurchaseServFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsStockFeeSet(WmsStockFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsReturnServFeeSet(WmsReturnServFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsReturnShipFeeSet(WmsReturnShipFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsReturnPackFeeSet(WmsReturnPackFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsOrderServFeeSet(WmsOrderServFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsShipFeeSet(WmsShipFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsPackFeeSet(WmsPackFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public void WmsRefundShipFeeSet(WmsRefundShipFeeCollection fee)
        {
            DB.SaveAll(fee);
        }

        public int GetWmsPurchaseServFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsPurchaseServFee>().NoLock()
                .Where(WmsPurchaseServFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsStockFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsStockFee>().NoLock()
                .Where(WmsStockFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsReturnServFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsReturnServFee>().NoLock()
                .Where(WmsReturnServFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsReturnShipFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsReturnShipFee>().NoLock()
                .Where(WmsReturnShipFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsReturnPackFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsReturnPackFee>().NoLock()
                .Where(WmsReturnPackFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsOrderServFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsOrderServFee>().NoLock()
                .Where(WmsOrderServFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsShipFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsShipFee>().NoLock()
                .Where(WmsShipFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsPackFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsPackFee>().NoLock()
                .Where(WmsPackFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public int GetWmsRefundShipFeeByAccountingDate(DateTime accountingDate)
        {
            return DB.SelectAllColumnsFrom<WmsRefundShipFee>().NoLock()
                .Where(WmsRefundShipFee.Columns.AccountingDate).IsEqualTo(accountingDate).GetRecordCount();
        }

        public WmsPurchaseServFeeCollection GetWmsPurchaseServFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime)
        {
            return DB.SelectAllColumnsFrom<WmsPurchaseServFee>().NoLock()
                       .Where(WmsPurchaseServFee.Columns.BalanceSheetWmsId).IsNull()
                       .And(WmsPurchaseServFee.Columns.ProdId).In(pchomeProdId)
                       .And(WmsPurchaseServFee.Columns.AccountingDate).IsLessThan(endTime)
                      .ExecuteAsCollection<WmsPurchaseServFeeCollection>();
        }

        public WmsStockFeeCollection GetWmsStockFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime)
        {
            return DB.SelectAllColumnsFrom<WmsStockFee>().NoLock()
                       .Where(WmsStockFee.Columns.BalanceSheetWmsId).IsNull()
                       .And(WmsStockFee.Columns.ProdId).In(pchomeProdId)
                       .And(WmsStockFee.Columns.AccountingDate).IsLessThan(endTime)
                      .ExecuteAsCollection<WmsStockFeeCollection>();
        }

        public WmsReturnServFeeCollection GetWmsReturnServFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime)
        {
            return DB.SelectAllColumnsFrom<WmsReturnServFee>().NoLock()
                       .Where(WmsReturnServFee.Columns.BalanceSheetWmsId).IsNull()
                       .And(WmsPurchaseServFee.Columns.ProdId).In(pchomeProdId)
                       .And(WmsPurchaseServFee.Columns.AccountingDate).IsLessThan(endTime)
                      .ExecuteAsCollection<WmsReturnServFeeCollection>();
        }

        public WmsReturnShipFeeCollection GetWmsReturnShipFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime)
        {
            return DB.SelectAllColumnsFrom<WmsReturnShipFee>().NoLock()
                       .Where(WmsReturnShipFee.Columns.BalanceSheetWmsId).IsNull()
                       .And(WmsReturnShipFee.Columns.ProdId).In(pchomeProdId)
                       .And(WmsReturnShipFee.Columns.AccountingDate).IsLessThan(endTime)
                      .ExecuteAsCollection<WmsReturnShipFeeCollection>();
        }

        public WmsReturnPackFeeCollection GetWmsReturnPackFeeByNoBalanceSheet(List<string> pchomeProdId, DateTime endTime)
        {
            return DB.SelectAllColumnsFrom<WmsReturnPackFee>().NoLock()
                       .Where(WmsReturnPackFee.Columns.BalanceSheetWmsId).IsNull()
                       .And(WmsReturnPackFee.Columns.ProdId).In(pchomeProdId)
                       .And(WmsReturnPackFee.Columns.AccountingDate).IsLessThan(endTime)
                      .ExecuteAsCollection<WmsReturnPackFeeCollection>();
        }

        public WmsPurchaseServFeeCollection GetWmsPurchaseServFeeByBalanceSheetWmsId(int balanceSheetWmsId)
        {
            return DB.SelectAllColumnsFrom<WmsPurchaseServFee>().NoLock()
                       .Where(WmsPurchaseServFee.Columns.BalanceSheetWmsId).IsEqualTo(balanceSheetWmsId)
                      .ExecuteAsCollection<WmsPurchaseServFeeCollection>();
        }

        public WmsStockFeeCollection GetWmsStockFeeByBalanceSheetWmsId(int balanceSheetWmsId)
        {
            return DB.SelectAllColumnsFrom<WmsStockFee>().NoLock()
                       .Where(WmsStockFee.Columns.BalanceSheetWmsId).IsEqualTo(balanceSheetWmsId)
                      .ExecuteAsCollection<WmsStockFeeCollection>();
        }

        public WmsReturnServFeeCollection GetWmsReturnServFeeByBalanceSheetWmsId(int balanceSheetWmsId)
        {
            return DB.SelectAllColumnsFrom<WmsReturnServFee>().NoLock()
                       .Where(WmsReturnServFee.Columns.BalanceSheetWmsId).IsEqualTo(balanceSheetWmsId)
                      .ExecuteAsCollection<WmsReturnServFeeCollection>();
        }

        public WmsReturnShipFeeCollection GetWmsReturnShipFeeByBalanceSheetWmsId(int balanceSheetWmsId)
        {
            return DB.SelectAllColumnsFrom<WmsReturnShipFee>().NoLock()
                       .Where(WmsReturnShipFee.Columns.BalanceSheetWmsId).IsEqualTo(balanceSheetWmsId)
                      .ExecuteAsCollection<WmsReturnShipFeeCollection>();
        }

        public WmsReturnPackFeeCollection GetWmsReturnPackFeeByBalanceSheetWmsId(int balanceSheetWmsId)
        {
            return DB.SelectAllColumnsFrom<WmsReturnPackFee>().NoLock()
                       .Where(WmsReturnPackFee.Columns.BalanceSheetWmsId).IsEqualTo(balanceSheetWmsId)
                      .ExecuteAsCollection<WmsReturnPackFeeCollection>();
        }
        public WmsOrderServFeeCollection GetWmsOrderServFeeByBalanceSheetId(int bsId)
        {
            return DB.SelectAllColumnsFrom<WmsOrderServFee>().NoLock()
                     .Where(WmsOrderServFee.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<WmsOrderServFeeCollection>() ?? new WmsOrderServFeeCollection();
        }

        public WmsShipFeeCollection GetWmsShipFeeByBalanceSheetId(int bsId)
        {
            return DB.SelectAllColumnsFrom<WmsShipFee>().NoLock()
                     .Where(WmsShipFee.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<WmsShipFeeCollection>() ?? new WmsShipFeeCollection();

        }

        public WmsPackFeeCollection GetWmsPackFeeByBalanceSheetId(int bsId)
        {
            return DB.SelectAllColumnsFrom<WmsPackFee>().NoLock()
                     .Where(WmsPackFee.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<WmsPackFeeCollection>() ?? new WmsPackFeeCollection();

        }

        public WmsRefundShipFeeCollection GetWmsRefundShipFeeByBalanceSheetId(int bsId)
        {
            return DB.SelectAllColumnsFrom<WmsRefundShipFee>().NoLock()
                     .Where(WmsRefundShipFee.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<WmsRefundShipFeeCollection>() ?? new WmsRefundShipFeeCollection();

        }

        public WmsOrderServFeeCollection WmsOrderServFeeGetByOrder(Guid productGuid, DateTime endTime)
        {
            string sql = string.Format(@"
                        select wosf.* from group_order gp with(nolock)
                        inner join [order] o with(nolock) on o.parent_order_id=gp.order_guid
                        inner join wms_order_serv_fee wosf with(nolock) on wosf.order_guid=o.guid
                        where business_hour_guid=@productGuid
                        and accounting_date<@endTime
                        and balance_sheet_id is null",
                        productGuid, endTime);

            QueryCommand qc = new QueryCommand(sql, WmsOrderServFee.Schema.Provider.Name);
            qc.AddParameter("@productGuid", productGuid, DbType.Guid);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            WmsOrderServFeeCollection list = new WmsOrderServFeeCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        public WmsShipFeeCollection WmsShipFeeGetListGetByOrder(Guid productGuid, DateTime endTime)
        {
            string sql = string.Format(@"
                        select wsf.* from group_order gp with(nolock) 
                        inner join [order] o with(nolock) on o.parent_order_id=gp.order_guid
                        inner join wms_ship_fee wsf with(nolock) on wsf.order_guid=o.guid
                        where business_hour_guid=@productGuid
                        and accounting_date<@endTime
                        and balance_sheet_id is null",
                        productGuid, endTime);

            QueryCommand qc = new QueryCommand(sql, WmsShipFee.Schema.Provider.Name);
            qc.AddParameter("@productGuid", productGuid, DbType.Guid);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            WmsShipFeeCollection list = new WmsShipFeeCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        public WmsPackFeeCollection WmsPackFeeGetByOrder(Guid productGuid, DateTime endTime)
        {
            string sql = string.Format(@"
                        select wpf.* from group_order gp with(nolock) 
                        inner join [order] o with(nolock) on o.parent_order_id=gp.order_guid
                        inner join wms_pack_fee wpf with(nolock) on wpf.order_guid=o.guid
                        where business_hour_guid=@productGuid
                        and accounting_date<@endTime
                        and balance_sheet_id is null",
                        productGuid, endTime);

            QueryCommand qc = new QueryCommand(sql, WmsPackFee.Schema.Provider.Name);
            qc.AddParameter("@productGuid", productGuid, DbType.Guid);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            WmsPackFeeCollection list = new WmsPackFeeCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        public WmsRefundShipFeeCollection WmsRefundShipFeeGetByOrder(Guid productGuid, DateTime endTime)
        {
            string sql = string.Format(@"
                        select wrsf.* from group_order gp with(nolock) 
                        inner join [order] o with(nolock) on o.parent_order_id=gp.order_guid
                        inner join wms_refund_ship_fee wrsf with(nolock) on wrsf.order_guid=o.guid
                        where business_hour_guid=@productGuid
                        and accounting_date<@endTime
                        and balance_sheet_id is null",
                        productGuid, endTime);

            QueryCommand qc = new QueryCommand(sql, WmsRefundShipFee.Schema.Provider.Name);
            qc.AddParameter("@productGuid", productGuid, DbType.Guid);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            WmsRefundShipFeeCollection list = new WmsRefundShipFeeCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        public int WmsOrderFeeSumByBalanceSheet(int balanceSheetWmsId)
        {
            string sql = @"select ISNULL(sum(temp.fee),0) from (
	                        select sum(fee) as fee from wms_order_serv_fee with(nolock) where balance_sheet_id=@bswid
	                        union all
	                        select sum(fee) as fee from wms_ship_fee with(nolock) where balance_sheet_id=@bswid
	                        union all
	                        select sum(fee) as fee from wms_pack_fee with(nolock) where balance_sheet_id=@bswid
	                        union all
	                        select sum(fee) as fee from wms_refund_ship_fee with(nolock) where balance_sheet_id=@bswid
                        ) temp";

            QueryCommand qc = new QueryCommand(sql, ViewOrderCorrespondingSmsLog.Schema.Provider.Name);
            qc.AddParameter("@bswid", balanceSheetWmsId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        public DataTable WmsOrderFeeDetailByBalanceSheet(int balanceSheetId)
        {
            string sql = @"select dp.unique_id,o.order_id,i.item_name,os.ship_time,'order' as type,fee from wms_order_serv_fee wosf with(nolock) 
                            inner join [order] o with(nolock) on wosf.order_guid=o.guid
                            inner join group_order g with(nolock) on g.order_guid=o.parent_order_id
                            inner join item i with(nolock) on i.business_hour_guid=g.business_hour_guid
                            inner join deal_property dp with(nolock) on dp.business_hour_guid=i.business_hour_guid
                            OUTER Apply (select top 1 * from order_ship with(nolock) where order_guid=o.guid and type = 1) os
                            where balance_sheet_id=@balanceSheetId

                            union all

                            select dp.unique_id,o.order_id,i.item_name,os.ship_time,'ship' as type,fee from wms_ship_fee wsf with(nolock) 
                            inner join [order] o with(nolock) on wsf.order_guid=o.guid
                            inner join group_order g with(nolock) on g.order_guid=o.parent_order_id
                            inner join item i with(nolock) on i.business_hour_guid=g.business_hour_guid
                            inner join deal_property dp with(nolock) on dp.business_hour_guid=i.business_hour_guid
                            OUTER Apply (select top 1 * from order_ship with(nolock) where order_guid=o.guid and type = 1) os
                            where balance_sheet_id=@balanceSheetId

                            union all

                            select dp.unique_id,o.order_id,i.item_name,os.ship_time,'pack' as type,fee from wms_pack_fee wpf with(nolock) 
                            inner join [order] o with(nolock) on wpf.order_guid=o.guid
                            inner join group_order g with(nolock) on g.order_guid=o.parent_order_id
                            inner join item i with(nolock) on i.business_hour_guid=g.business_hour_guid
                            inner join deal_property dp with(nolock) on dp.business_hour_guid=i.business_hour_guid
                            OUTER Apply (select top 1 * from order_ship with(nolock) where order_guid=o.guid and type = 1) os
                            where balance_sheet_id=@balanceSheetId

                            union all

                            select dp.unique_id,o.order_id,i.item_name,os.ship_time,'refund' as type,fee from wms_refund_ship_fee wrs with(nolock) 
                            inner join [order] o with(nolock) on wrs.order_guid=o.guid
                            inner join group_order g with(nolock) on g.order_guid=o.parent_order_id
                            inner join item i with(nolock) on i.business_hour_guid=g.business_hour_guid
                            inner join deal_property dp with(nolock) on dp.business_hour_guid=i.business_hour_guid
                            OUTER Apply (select top 1 * from order_ship with(nolock) where order_guid=o.guid and type = 1) os
                             where balance_sheet_id=@balanceSheetId";
            IDataReader idr = new InlineQuery().ExecuteReader(sql, balanceSheetId);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }
        #endregion

        #region product_item_wms_inventory

        public WmsProductItemInventory WmsProductItemInventoryGet(Guid itemGuid)
        {
            return DB.Get<WmsProductItemInventory>(WmsProductItemInventory.Columns.ItemGuid, itemGuid);
        }
        public void WmsProductItemInventorySet(WmsProductItemInventory inventory)
        {
            if (inventory.Id == 0)
            {
                inventory.CreateTime = DateTime.Now;
                inventory.ModifyTime = DateTime.Now;
                DB.Save(inventory);
            }
            else
            {
                inventory.ModifyTime = DateTime.Now;
                DB.Save(inventory);
            }
        }

        #endregion
        public void WmsPurchaseOrderLogSet(WmsPurchaseOrderLogCollection orderLogs)
        {
            DB.SaveAll(orderLogs);
        }

        public void UpdtaeWmsPurchaseOrderInvalidationStatus(Guid purchaseOrderGuid, bool invalidationStatus, string userName)
        {
            DB.Update<WmsPurchaseOrder>().Set(WmsPurchaseOrder.InvalidationColumn).EqualTo(Convert.ToInt32(invalidationStatus))
                .Set(WmsPurchaseOrder.ModifyIdColumn).EqualTo(userName)
                .Set(WmsPurchaseOrder.ModifyTimeColumn).EqualTo(DateTime.Now)
                .Where(WmsPurchaseOrder.Columns.Guid).IsEqualTo(purchaseOrderGuid)
                .Execute();
        }

        public void UpdtaeWmsPurchaseOrderStatusWithInvalidationStatus(List<Guid> guid, int status, string userName, bool invalidationStatus)
        {

            string sql = "update wms_purchase_order set status=@status,modify_id=@userName,modify_time=getdate(), invalidation=@invalidationStatus where guid in (";
            int count = 1;
            foreach (Guid g in guid)
            {
                sql += "@guid" + count.ToString() + ",";
                count++;
            }
            sql = sql.TrimEnd(",") + ")";


            QueryCommand qc = new QueryCommand(sql, WmsPurchaseOrder.Schema.Provider.Name);

            count = 1;
            foreach (Guid g in guid)
            {
                qc.AddParameter("@guid" + count, g, DbType.Guid);
                count++;
            }

            qc.AddParameter("@userName", userName, DbType.String);
            qc.AddParameter("@status", status, DbType.Int32);
            qc.AddParameter("@invalidationStatus", Convert.ToInt32(invalidationStatus), DbType.Int32);
            DataService.ExecuteScalar(qc);

        }

        #region WmsRefundOrder
        public void WmsRefundOrderSet(WmsRefundOrder order)
        {
            DB.Save(order);
        }

        public WmsRefundOrder WmsRefundOrderGet(int returnFormId)
        {
            return DB.SelectAllColumnsFrom<WmsRefundOrder>().NoLock()
                        .Where(WmsRefundOrder.Columns.ReturnFormId).IsEqualTo(returnFormId)
                        .And(WmsRefundOrder.Columns.Status).NotIn(WmsRefundStatus.Fail, WmsRefundStatus.NotRefund)
                        .ExecuteSingle<WmsRefundOrder>();
        }

        public WmsRefundOrderCollection WmsRefundOrderGetByUpdate()
        {            return DB.SelectAllColumnsFrom<WmsRefundOrder>()
                     .InnerJoin(ReturnForm.IdColumn, WmsRefundOrder.ReturnFormIdColumn)
                     .Where(WmsRefundOrder.Columns.Status).In((int)WmsRefundStatus.Initial, (int)WmsRefundStatus.Examine, (int)WmsRefundStatus.Processing, (int)WmsRefundStatus.Delivering)
                     .And(ReturnForm.Columns.ProgressStatus).In((int)ProgressStatus.Retrieving, (int)ProgressStatus.RetrieveToPC, (int)ProgressStatus.RetrieveToCustomer)
                     .And(ReturnForm.Columns.VendorProgressStatus).IsEqualTo((int)VendorProgressStatus.Retrieving)
                     .ExecuteAsCollection<WmsRefundOrderCollection>();
        }

        public ReturnFormCollection WmsRefundOrderGetByArrivaled(int type)
        {
            string sql = string.Format(@"select * from return_form rf 
                                        inner join wms_refund_order wro on rf.id=wro.return_form_id" + 
                                        " and wro.status in (" + (int)WmsRefundStatus.Initial + "," + (int)WmsRefundStatus.Examine + "," + (int)WmsRefundStatus.Processing + "," + (int)WmsRefundStatus.Delivering) + "," + (int)WmsRefundStatus.Arrived + ")";

            if (type == 1)//取貨三天內已配達->廠商待確認
            {
                sql += " and rf.progress_status in ( " + (int)ProgressStatus.Retrieving + "," + (int)ProgressStatus.RetrieveToPC + "," + (int)ProgressStatus.RetrieveToCustomer + ")"; 
                sql += " and rf.vendor_progress_status = " + (int)VendorProgressStatus.Retrieving;
                sql += " and dbo.WorkingDayGet(convert(varchar(10),has_pickup_time,111),convert(varchar(10),getdate(),111))<=3 and arrival_time is not null";
            } 
            else if (type == 2)//取貨三天後未配達,經客服處理後取得送達日->廠商待確認
            {
                sql += " and rf.progress_status in ( " + (int)ProgressStatus.Retrieving + "," + (int)ProgressStatus.RetrieveToPC + "," + (int)ProgressStatus.RetrieveToCustomer + ")";
                sql += " and rf.vendor_progress_status = " + (int)VendorProgressStatus.Retrieving;
                sql += " and dbo.WorkingDayGet(convert(varchar(10),has_pickup_time,111),convert(varchar(10),getdate(),111))>3 and arrival_time is not null";
            }
            else if (type == 3)//取貨三天後未配達->客服待確認
            {
                sql += " and rf.progress_status in ( " + (int)ProgressStatus.Retrieving + "," + (int)ProgressStatus.RetrieveToPC + "," + (int)ProgressStatus.RetrieveToCustomer + ")";
                sql += " and rf.vendor_progress_status = " + (int)VendorProgressStatus.Retrieving;
                sql += " and dbo.WorkingDayGet(convert(varchar(10),ISNULL(cs_confirmed_time,has_pickup_time),111),convert(varchar(10),getdate(),111))>3 and arrival_time is null";
            } 
            else if (type == 4)//送達三天廠商尚未回壓->自動退貨
            {
                sql += " and rf.progress_status in ( " + (int)ProgressStatus.Retrieving + "," + (int)ProgressStatus.RetrieveToPC + "," + (int)ProgressStatus.RetrieveToCustomer + "," + (int)ProgressStatus.ConfirmedForVendor + ")";
                sql += " and rf.vendor_progress_status in( " + (int)VendorProgressStatus.Retrieving + "," + (int)VendorProgressStatus.ConfirmedForVendor + ")";
                sql += " and dbo.WorkingDayGet(convert(varchar(10),arrival_time,111),convert(varchar(10),getdate(),111))>3";
            }
            else if (type == 5)//送達三天廠商尚未回壓->自動退貨
            {
                sql += " and rf.progress_status in ( " + (int)ProgressStatus.RetrieveToPC + ")";
                sql += " and rf.vendor_progress_status in( " + (int)VendorProgressStatus.Retrieving + ")";
                sql += " and wro.status=" + (int)WmsRefundStatus.Arrived + " and has_pickup_time is null and arrival_time is null";
            }
            QueryCommand qc = new QueryCommand(sql, ReturnForm.Schema.Provider.Name);

            ReturnFormCollection list = new ReturnFormCollection();
            list.LoadAndCloseReader(DataService.GetReader(qc));
            return list;
        }

        #endregion

        public void WmsRefundOrderStatusLogSet(WmsRefundOrderStatusLog log)
        {
            DB.Save(log);
        }

        public Dictionary<Guid, Guid> WmsAmtOrderGet()
        {
            Dictionary<Guid, Guid> oids = new Dictionary<Guid, Guid>();
            string sql = string.Format(@"
                    select distinct o.guid,ctl.business_hour_guid
                    from cash_trust_log ctl with(nolock)
                    left join [order] o with(nolock) on o.guid=ctl.order_guid
                    left join order_product_delivery opd with(nolock) on opd.order_guid=o.GUID
                    where ctl.create_time >='{0}'
                    and ctl.atm>0
                    and o.order_status&{1}=0
                    and o.order_status&{2}=0
                    and o.order_status&{3}>0
                    and ctl.delivery_type={4}
                    and opd.product_delivery_type={5}
                    ",
                    DateTime.Today.ToString("yyyy/MM/dd"),
                    (int)OrderStatus.Complete,
                    (int)OrderStatus.Cancel,
                    (int)OrderStatus.ATMOrder,
                    (int)DeliveryType.ToHouse,
                    (int)ProductDeliveryType.Wms);

            QueryCommand qc = new QueryCommand(sql, WmsOrder.Schema.Provider.Name);

            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    oids.Add(reader.GetGuid(0), reader.GetGuid(1));
                }
            }

            return oids;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class MailLogProvider : IMailLogProvider
    {
        public void MailLogSet(string sender, List<string> receivers, string subject, string content, bool result, 
            int category, string lastError)
        {
            DateTime now = DateTime.Now;
            using (var tran = TransactionScopeBuilder.CreateReadCommitted(TimeSpan.FromSeconds(10)))
            {
                using (var db = new MailLogDbContext())
                {
                    MailContentLog contentLog = new MailContentLog() {Content = content};
                    db.MailContentLogEntities.Add(contentLog);
                    if (subject != null && subject.Length > 200)
                    {
                        subject = subject.Substring(0, 200);
                    }
                    if (lastError != null && lastError.Length > 200)
                    {
                        lastError = lastError.Substring(0, 200);
                    }
                    db.SaveChanges();
                    foreach (string receiver in receivers)
                    {
                        if (subject != null && subject.Length > 200)
                        {
                            subject = subject.Substring(0, 200);
                        }
                        MailLog log = new MailLog()
                        {
                            Subject = subject,
                            Sender = sender,
                            Category = category,
                            Receiver = receiver,
                            CreateTime = now,
                            Result = result,
                            LastError = lastError,
                            ResendTime = null,
                            MailContentLogId = contentLog.Id
                        };
                        db.MailLogEntities.Add(log);
                    }
                    db.SaveChanges();
                }
                tran.Complete();
            }
        }

        public void MailLogResendSet(int mailLogId, bool result, string lastError)
        {
            using (var db = new MailLogDbContext())
            {
                var mailLog = db.MailLogEntities.FirstOrDefault(t => t.Id == mailLogId);
                if (mailLog != null)
                {
                    mailLog.Result = result;
                    mailLog.LastError = lastError;
                    mailLog.ResendTime = DateTime.Now;
                    db.SaveChanges();
                }
            }
        }

    }
}

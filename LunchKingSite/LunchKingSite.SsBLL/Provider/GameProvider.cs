﻿using System.Data;
using System;
using System.Linq;
using System.Collections.Generic;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.GameEntities;
using LunchKingSite.SsBLL.DbContexts;
using System.Data.Entity;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using log4net;

namespace LunchKingSite.SsBLL.Provider
{
    public class GameProvider : IGameProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ILog logger = LogManager.GetLogger(typeof(GameProvider));
        public void SaveGameCampaign(GameCampaign campaign, string modifyId)
        {
            if (string.IsNullOrEmpty(campaign.Name))
            {
                throw new Exception("SaveGameCampaign fail, Name沒有設定");
            }
            using (var db = new GameDbContext())
            {
                if (campaign.Id == 0)
                {
                    campaign.Guid = Guid.NewGuid();
                    campaign.PreviewCode = Guid.NewGuid().ToString().Substring(0, 4);
                    campaign.CreateId = modifyId;
                    campaign.CreateTime = DateTime.Now;
                    db.Entry(campaign).State = EntityState.Added;
                }
                else
                {
                    campaign.ModifyId = modifyId;
                    campaign.ModifyTime = DateTime.Now;
                    db.Entry(campaign).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public GameCampaign GetGameCampaign(int campaignId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameCampaignSet.FirstOrDefault(t => t.Id == campaignId);
            }
        }

        public GameCampaign GetGameCampaign(string name)
        {
            using (var db = new GameDbContext())
            {
                return db.GameCampaignSet.FirstOrDefault(t => t.Name == name);
            }
        }

        public int GetCampaignIdFromRoundGuid(Guid roundGuid)
        {
            string sql = @"
select ga.campaign_id from game_round gr with(nolock)
inner join game_activity ga with(nolock) on ga.id = gr.activity_id
where gr.guid = @roundGuid
";

            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@roundGuid", SqlDbType.UniqueIdentifier);
                para1.Value = roundGuid;
                return db.Database.SqlQuery<int>(sql, para1).First();
            }
        }

        public List<GameActivity> GetGameActivityByCampaignId(int campaignId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameActivitySet.Where(t => t.CampaignId == campaignId).ToList();
            }
        }

        public void SaveGameActivity(GameActivity activity, string modifyId)
        {
            using (var db = new GameDbContext())
            {
                if (activity.Id == 0)
                {
                    activity.Guid = Guid.NewGuid();
                    activity.CreateId = modifyId;
                    activity.CreateTime = DateTime.Now;
                    db.Entry(activity).State = EntityState.Added;
                }
                else
                {
                    activity.ModifyId = modifyId;
                    activity.ModifyTime = DateTime.Now;
                    db.Entry(activity).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public GameActivity GetGameActivity(Guid guid)
        {
            using (var db = new GameDbContext())
            {
                return db.GameActivitySet.FirstOrDefault(t => t.Guid == guid);
            }
        }

        public GameActivity GetGameActivityBycampaignId(Guid bid, int campaignId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameActivitySet.FirstOrDefault(t => t.ReferenceId == bid && t.CampaignId == campaignId);
            }
        }

        public GameActivity GetGameActivity(int id)
        {
            using (var db = new GameDbContext())
            {
                return db.GameActivitySet.FirstOrDefault(t => t.Id == id);
            }
        }

        public void SaveGameGame(GameGame game, string modifyId)
        {
            using (var db = new GameDbContext())
            {
                if (game.Id == 0)
                {
                    game.Guid = Guid.NewGuid();
                    game.CreateId = modifyId;
                    game.CreateTime = DateTime.Now;
                    db.Entry(game).State = EntityState.Added;
                }
                else
                {
                    game.ModifyId = modifyId;
                    game.ModifyTime = DateTime.Now;
                    db.Entry(game).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public List<GameGame> GetGamesByActivityId(int activityId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameGameSet.Where(t => t.ActivityId == activityId).ToList();
            }
        }

        public GameGame GetGame(int gameId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameGameSet.FirstOrDefault(t => t.Id == gameId);
            }
        }

        public List<TopGameView> GetTopGameActivityViewList(int campaignId)
        {
            string sql = @"
select top 20 * from 
(
select ga.guid as Guid,  
ga.title as Title, 
gg.price as Price, 
ga.image as Image,
gg.rule_level as RuleLevel,
gg.rule_hours as RuleHours,
gg.rule_people as RulePeople,
case when (ga.run_out = 1 or ga.closed = 1 or getdate() >= ga.end_time or gg.run_out = 1) 
	then cast(1 as bit) else cast(0 as bit) end as RunOut,
(select count(*) from game_round where game_round.game_id =  gg.id) as  game_round_count
 from game_game gg
 inner join game_activity ga on ga.id = gg.activity_id
 where ga.campaign_id = @campaignId and ga.closed = 0
 ) as t
 where t.game_round_count > 0 
 order by t.game_round_count desc, RunOut asc
";
            List<TopGameView> result = new List<TopGameView>();
            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@campaignId", SqlDbType.Int);
                para1.Value = campaignId;
                var items = db.Database.SqlQuery<TopGameView>(sql, para1).ToList();
                result.AddRange(items);                
            }
            return result;
        }

        public GameRoundView GetGameRoundView(Guid roundGuid)
        {
            string sql = @"
select 
gr.id as Id,
gr.guid as Guid,
gr.game_id as GameId,
ga.id as ActivityId,
ga.title as Title, 
ga.sub_title as SubTitle,
gr.expired_time as ExpiredTime,
ga.image as Image,
gr.status as Status,
(case when gr.status = 1 and gdo.order_guid is not null then 1 
 when gr.status = 1 and gdp.expired_time is not null and getdate() > gdp.expired_time then -1
 else 0 end) as RewardStatus
 from game_round gr with(nolock)
 inner join game_game gg with(nolock) on gg.id = gr.game_id
 inner join game_activity ga with(nolock) on ga.id = gr.activity_id
 left join game_deal_permission gdp with(nolock) on gdp.round_id = gr.id
 left join game_deal_order gdo with(nolock) on gdo.permission_id = gdp.id
where gr.guid = @roundGuid
";

            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@roundGuid", SqlDbType.UniqueIdentifier);
                para1.Value = roundGuid;
                var item = db.Database.SqlQuery<GameRoundView>(sql, para1).FirstOrDefault();
                return item;
            }
        }

        public List<GameRoundView> GetGameRoundViewList(int userId, GameCampaign campaign)
        {
            string sql = @"
select 
gr.id as Id,
gr.guid as Guid,
gr.game_id as GameId,
ga.id as ActivityId,
ga.title as Title, 
ga.sub_title as SubTitle,
gr.expired_time as ExpiredTime,
ga.image as Image,
gr.status as Status,
(case when gr.status = 1 and gdo.order_guid is not null then 1 
 when gr.status = 1 and gdp.expired_time is not null and getdate() > gdp.expired_time then -1
 else 0 end) as RewardStatus
 from game_round gr with(nolock)
 inner join game_game gg with(nolock) on gg.id = gr.game_id
 inner join game_activity ga with(nolock) on ga.id = gr.activity_id and ga.campaign_id = @campaignId
 left join game_deal_permission gdp with(nolock) on gdp.round_id = gr.id
 left join game_deal_order gdo with(nolock) on gdo.permission_id = gdp.id
where gr.owner_id = @userId 
order by gr.id desc
";

            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@userId", SqlDbType.Int);
                para1.Value = userId;
                var para2 = new System.Data.SqlClient.SqlParameter("@campaignId", SqlDbType.Int);
                para2.Value = campaign.Id;
                var items = db.Database.SqlQuery<GameRoundView>(sql, para1, para2).ToList();
                return items;
            }
        }

        public List<GameActivityView> GetGameActivityViewByCampaignId(int campaignId)
        {
             string sql = @"
select 
	t.Guid, 
    t.HigestOriginalPrice,
	t.LowestPrice,
    t.RunOut as RunOut,
	gg.rule_level as RuleLevel,
	gg.rule_people as RulePeople,
	gg.rule_hours as RuleHours,
	t.Title, 
	t.Image, 
	t.SubTitle ,
	t.CreateTime as CreateTime
		from (
			select ga.guid as Guid, 
			ga.title as Title, 
			ga.sub_title as SubTitle, 
			ga.image as Image,
			ga.create_time as CreateTime,
            case when (ga.run_out = 1 or ga.closed = 1 or getdate() >= ga.end_time) then cast(1 as bit) else cast(0 as bit) end as RunOut,
			(select max(gg.id) from game_game gg with(nolock) where gg.activity_id = ga.id)  as LowestGameId,
            (select max(original_price) from game_game gg with(nolock) where gg.activity_id = ga.id)  as HigestOriginalPrice,
            (select min(price) from game_game gg with(nolock) where gg.activity_id = ga.id)  as LowestPrice,
			ga.seqence
			from game_activity ga with(nolock)
			where ga.campaign_id = @campaignId and ga.closed = 0			
	) as t
	inner join game_game gg with(nolock) on gg.id = t.LowestGameId
	order by t.seqence asc, t.CreateTime desc
";

            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@campaignId", SqlDbType.Int);
                para1.Value = campaignId;
                var items = db.Database.SqlQuery<GameActivityView>(sql, para1).ToList();
                return items;
            }
        }

        public void SaveGameRule(GameRule rule, string modifyId)
        {
            using (var db = new GameDbContext())
            {
                if (rule.Id == 0)
                {
                    rule.CreateId = modifyId;
                    rule.CreateTime = DateTime.Now;
                    db.Entry(rule).State = EntityState.Added;
                }
                else
                {
                    rule.ModifyId = modifyId;
                    rule.ModifyTime = DateTime.Now;
                    db.Entry(rule).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public void SaveGameRound(GameRound round, string modifyId)
        {
            if (round.Id  == 0)
            {
                throw new Exception("UpdateGameRound can't do insert new data.");
            }
            using (var db = new GameDbContext())
            {
                round.ModifyId = modifyId;
                round.ModifyTime = DateTime.Now;
                db.Entry(round).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public GameRound InsertGameRoundWhenNoOtherRunningOne(int userId, GameGame game, DateTime expiredTime, string modifyId)
        {
            using (var db = new GameDbContext())
            {                
                using (var tx = db.Database.BeginTransaction())
                {
                    //如果有任何一個正在遊戲中的，就不建立
                    GameRound gameRound = db.GameRoundSet.FirstOrDefault(
                        t => t.GameId == game.Id && t.OwnerId == userId && t.Status == GameRoundStatus.Running);
                    if (gameRound == null)
                    {
                        gameRound = new GameRound
                        {
                            Guid = Guid.NewGuid(),
                            OwnerId = userId,
                            GameId = game.Id,
                            ActivityId = game.ActivityId,
                            ExpiredTime = expiredTime,
                            Status = GameRoundStatus.Running,
                            CreateId = modifyId,
                            CreateTime = DateTime.Now
                        };
                        db.Entry(gameRound).State = EntityState.Added;
                        db.SaveChanges();
                        tx.Commit();
                    }
                    return gameRound;
                }
            }
        }

        public GameRound GetGameRound(int id)
        {
            using (var db = new GameDbContext())
            {
                return db.GameRoundSet.FirstOrDefault(
                    t => t.Id == id);
            }
        }

        public GameRound GetGameRound(Guid roundGuid)
        {
            using (var db = new GameDbContext())
            {
                return db.GameRoundSet.FirstOrDefault(
                    t => t.Guid == roundGuid);
            }
        }

        public GameRound GetGameRoundByActivityId(int activityId, int userId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameRoundSet.FirstOrDefault(
                    t => t.ActivityId == activityId && t.OwnerId == userId);
            }
        }

        public GameRound GetGameRoundByActivityId(int activityId, int userId, GameRoundStatus status)
        {
            using (var db = new GameDbContext())
            {
                return db.GameRoundSet.FirstOrDefault(
                    t => t.ActivityId == activityId && t.OwnerId == userId && t.Status == status);
            }
        }

        public List<GameRound> GetGameRoundByGameId(int gameId, GameRoundStatus status)
        {
            using (var db = new GameDbContext())
            {
                return db.GameRoundSet.Where(t => t.GameId == gameId && t.Status == status).ToList();
            }
        }

        public GameCampaign GetCurrentGameCampaign()
        {
            DateTime now = DateTime.Now;
            using (var db = new GameDbContext())
            {
                return db.GameCampaignSet.FirstOrDefault(t =>
                    now >= t.StartTime && now <= t.EndTime);
            }
        }

        public List<GameCampaign> GetAllGameCampaign()
        {
            using (var db = new GameDbContext())
            {
                return db.GameCampaignSet.ToList();
            }
        }

        public List<GameRule> GetAllGameRules()
        {
            DateTime now = DateTime.Now;
            using (var db = new GameDbContext())
            {
                return db.GameRuleSet.OrderBy(t => t.Level).ToList();
            }
        }

        public bool HasGameDealPermissionByMainOrSubDeal(int userId, Guid dealId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameDealPermissionSet.FirstOrDefault(
                    t => t.UserId == userId && (t.MainDealId == dealId || t.DealId == dealId)) != null;
            }
        }

        public GameDealPermission GetUnexpiredGameDealPermission(int userId, Guid dealId)
        {
            DateTime now = DateTime.Now;
            using (var db = new GameDbContext())
            {
                //"儘量"回傳尚標註使用的 permission
                var items = db.GameDealPermissionSet.Where(
                    t => t.UserId == userId && t.DealId == dealId && t.ExpiredTime > now)
                    .OrderBy(t => t.Used).ToList();
                return items.FirstOrDefault();
            }
        }

        public GameDealPermission GetAvailableGameDealPermission(int userId, Guid dealId)
        {
            DateTime now = DateTime.Now;
            using (var db = new GameDbContext())
            {
                return db.GameDealPermissionSet.FirstOrDefault(
                    t => t.UserId == userId && t.DealId == dealId && t.Used == false && t.ExpiredTime > now);
            }
        }

        public void SaveGameDealPermission(GameDealPermission permission)
        {
            using (var db = new GameDbContext())
            {
                if (permission.Id == 0)
                {
                    db.Entry(permission).State = EntityState.Added;
                    permission.CreateTime = DateTime.Now;
                }
                else
                {
                    db.Entry(permission).State = EntityState.Modified;
                    permission.ModifyTime = DateTime.Now;

                }
                db.SaveChanges();
            }
        }

        public List<Guid> GetEligibleDealsDealIds(int topNum)
        {
            string sql = @"
select top(@topnum)
	t.business_hour_guid
	from (
select 
((select count(*) from combo_deals where combo_deals.MainBusinessHourGuid = vpd.business_hour_guid) - 1) as SubDealsCount,
vpd.* 
from  
view_ppon_deal vpd where vpd.business_hour_guid in 
(select distinct dts.business_hour_GUID from deal_time_slot dts where getdate() between dts.effective_start and dts.effective_end)
	and vpd.business_hour_status & 1024 > 0 
	and (vpd.business_hour_status & 67108864)= 0
	and vpd.business_hour_order_time_s <= '2018-10-01'
	and vpd.business_hour_order_time_e >= '2018-11-01'
    and vpd.app_deal_pic <> ''
) as t where t.SubDealsCount = 3
";

            using (var db = new GameDbContext())
            {
                System.Data.SqlClient.SqlParameter para1 = new System.Data.SqlClient.SqlParameter("@topnum", SqlDbType.Int);
                para1.Value = topNum;
                return db.Database.SqlQuery<Guid>(sql, para1).ToList();
            }
        }

        public bool GetGameActivitiesAndGamesByCampaignId(int campaignId, 
            out List<GameActivity> activities, out List<List<GameGame>> activityGames)
        {
            activities = new List<GameActivity>();
            activityGames = new List<List<GameGame>>();
            try
            {
                using (var db = new GameDbContext())
                {
                    //db.
                    db.NoLock();
                    var q = (from gg in db.GameGameSet
                             from ga in db.GameActivitySet
                             where gg.ActivityId == ga.Id && ga.CampaignId == campaignId
                             select gg);
                    Dictionary<int, List<GameGame>> games = q.GroupBy(t => t.ActivityId).ToDictionary(t => t.Key, t => t.ToList());

                    activities = db.GameActivitySet.Where(t => t.CampaignId == campaignId).OrderBy(x => x.Seqence).ThenByDescending(x => x.Id).ToList().ToList();

                    foreach (var activity in activities)
                    {
                        if (games.ContainsKey(activity.Id) == false)
                        {
                            activityGames.Add(new List<GameGame>());
                        }
                        else
                        {
                            activityGames.Add(games[activity.Id]);
                        }
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.Warn("GetGameActivitiesAndGamesByCampaignId error", ex);
                return false;
            }
        }

        public GameBuddy GetGameBuddy(int gameBuddyId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameBuddySet.FirstOrDefault(t => t.Id == gameBuddyId);
            }
        }
        /// <summary>
        /// TODO 需要索引 
        /// </summary>
        /// <param name="roundGuid"></param>
        /// <param name="source"></param>
        /// <param name="externalId"></param>
        /// <returns></returns>
        public GameBuddy GetGameBuddy(Guid roundGuid, SingleSignOnSource source, string externalId)
        {
            using (var db = new GameDbContext())
            {
                var q = from gb in db.GameBuddySet
                        from gr in db.GameRoundSet
                        where
                            gb.RoundId == gr.Id &&
                            gr.Guid == roundGuid &&
                            gb.Source == (int)source &&
                            gb.ExternalId == externalId
                        select gb;
                return q.FirstOrDefault();
            }
        }
        /// <summary>
        /// 取得目前使用者建立的遊戲(GameRound)，有多少人幫助
        /// </summary>
        /// <param name="roundId"></param>
        /// <returns></returns>
        public List<GameBuddy> GetGameBuddyList(int roundId)
        {
            using (var db = new GameDbContext())
            {
                return db.GameBuddySet.Where(t => t.RoundId == roundId).ToList();
            }
        }

        public void SaveGameBuddy(GameBuddy buddy)
        {
            using (var db = new GameDbContext())
            {
                if (buddy.Id == 0)
                {
                    db.Entry(buddy).State = EntityState.Added;
                }
                else
                {
                    db.Entry(buddy).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public bool InsertGameBuddyReward(GameBuddyReward reward)
        {
            if (reward == null || reward.Id != 0)
            {
                throw new Exception("InsertGameBuddyReward fail, reward.Id can't be not 0");
            }
            using (var db = new GameDbContext())
            {
                using (var tx = db.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    var oldReward = db.GameBuddyRewardSet
                        .FirstOrDefault(t => t.UserId == reward.UserId && t.TheDate == reward.TheDate);
                    if (oldReward != null)
                    {
                        return false;
                    }
                    reward.CreateTime = DateTime.Now;
                    db.Entry(reward).State = EntityState.Added;
                    db.SaveChanges();
                    tx.Commit();
                }
            }
            return true;
        }

        public void UpdateGameBuddyReward(GameBuddyReward reward)
        {
            if (reward == null || reward.Id == 0)
            {
                throw new Exception("UpdateGameBuddyReward fail, reward.Id can't be 0");
            }
            using (var db = new GameDbContext())
            {
                db.Entry(reward).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public GameBuddyReward GetGameBuddyReward(int userId, DateTime today)
        {
            using (var db = new GameDbContext())
            {
                using (var tx = db.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    var reward = db.GameBuddyRewardSet
                        .FirstOrDefault(t => t.UserId == userId && t.TheDate == today);
                    return reward;
                }
            }
        }
        public GameBuddyReward GetGameBuddyReward(int buddyId)
        {
            using (var db = new GameDbContext())
            {
                using (var tx = db.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                {
                    var reward = db.GameBuddyRewardSet
                        .FirstOrDefault(t => t.BuddyId == buddyId);
                    return reward;
                }
            }
        }

        public GameActivity GetGameActivityByRoundGuid(Guid roundGuid)
        {
            using (var db = new GameDbContext())
            {
                using (var tx = db.Database.BeginTransaction(IsolationLevel.ReadUncommitted))
                {
                    var query = from gr in db.GameRoundSet
                                from ga in db.GameActivitySet
                                where gr.ActivityId == ga.Id && gr.Guid == roundGuid
                                select ga;
                    return query.FirstOrDefault();
                }
            }
        }

        /// <summary>
        /// 取得超過時間的遊戲，且狀態仍然在進行中
        /// </summary>
        public List<GameRound> GetEpiredRunningGameRounds()
        {
            string sql = @"
SELECT [id] as Id
      ,[guid] as Guid
      ,[owner_id] as OwnerId
      ,[game_id] as GameId
      ,[activity_id] as ActivityId
      ,[expired_time] as ExpiredTime
      ,[status] as Status
      ,[completed_time] as CompletedTime
      ,[create_id] as CreateId
      ,[create_time] as CreateTime
      ,[modify_id] as ModifyId
      ,[modify_time] as ModifyTime
  FROM [dbo].[game_round] with(nolock) where expired_time < getdate() and status=@status";
            DateTime now = DateTime.Now;
            using (var db = new GameDbContext())
            {
                var para1 = new System.Data.SqlClient.SqlParameter("@status", (int)SqlDbType.Int);
                para1.Value = (int)GameRoundStatus.Running;
                return db.Database.SqlQuery<GameRound>(sql, para1).ToList();
            }
        }

        public bool ExecuteGameRoundCompletePlan(GameRound round, string modifyId, out bool runOut)
        {
            runOut = false;
            DateTime now = DateTime.Now;
            try
            {
                using (var db = new GameDbContext())
                {
                    using (var tx = db.Database.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        GameGame game = db.GameGameSet.First(t => t.Id == round.GameId);
                        GameActivity activity = db.GameActivitySet.First(t => t.Id == game.ActivityId);
                        if (game.RunOut)
                        {
                            runOut = true;
                            return false;
                        }
                        var count = db.GameRoundSet.Count(t => t.GameId == game.Id && t.Status == GameRoundStatus.Completed);
                        if (count >= game.Maximum)
                        {
                            runOut = true;
                            return false;
                        }
                        count++;
                        if (count >= game.Maximum)
                        {
                            runOut = true;
                        }
                        //完成關卡，建立 GameDealPermission
                        db.Entry(new GameDealPermission
                        {
                            UserId = round.OwnerId,
                            RoundId = round.Id,
                            DealId = game.ReferenceId,
                            MainDealId = activity.ReferenceId,
                            ExpiredTime = now.AddHours(48),
                            CreateTime = now,                            
                        }).State = EntityState.Added;
                        //設定round狀態為完成
                        round.Status = GameRoundStatus.Completed;
                        round.CompletedTime = DateTime.Now;
                        round.ModifyTime = now;
                        round.ModifyId = modifyId;
                        db.Entry(round).State = EntityState.Modified;
                        game.ModifyId = modifyId;
                        game.ModifyTime = now;
                        game.RunOut = runOut;
                        
                        db.Entry(game).State = EntityState.Modified;
                        db.SaveChanges();
                        tx.Commit();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Warn("ExecuteGameRoundCompletePlan fail. ", ex);
                return false;
            }
        }
        public void SaveGameDealOrder(GameDealOrder gameDealOrder)
        {
            using (var db = new GameDbContext())
            {
                if (gameDealOrder.Id == 0)
                {
                    gameDealOrder.CreateTime = DateTime.Now;
                    db.Entry(gameDealOrder).State = EntityState.Added;
                }
                else
                {
                    db.Entry(gameDealOrder).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public GameDealOrder GetGameDearOrder(Guid orderGuid)
        {
            if (orderGuid == Guid.Empty)
            {
                return null;
            }
            using (var db = new GameDbContext())
            {
                return db.GameDealOrderSet.FirstOrDefault(t => t.OrderGuid == orderGuid);
            }
        }
    }
}

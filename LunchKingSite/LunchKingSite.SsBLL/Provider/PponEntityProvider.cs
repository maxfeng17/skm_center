﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using EntityFramework.BulkInsert.Extensions;
using log4net;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.SsBLL.DbContexts;
using Newtonsoft.Json;

namespace LunchKingSite.SsBLL.Provider
{
    public class PponEntityProvider : IPponEntityProvider
    {
        static ILog logger = LogManager.GetLogger(typeof(PponEntityProvider));
        public void GoogleShoppingProductDeleteAll()
        {
            using (var db = new PponDbContext())
            {
                db.Database.ExecuteSqlCommand("delete from google_shopping_product");
            }
        }

        public void GoogleShoppingProductSet(List<GoogleShoppingProduct> items)
        {
            using (var db = new PponDbContext())
            {
                db.BulkInsert(items);
                db.SaveChanges();
            }
        }

        public Dictionary<Guid, DealGoogleMetadata> DealGoogleMetadataGetDict()
        {
            using (var db = new PponDbContext())
            {
                return db.DealGoogleMetadataDbSet.ToDictionary(t => t.Bid, t => t);
            }
        }

        public DealGoogleMetadata DealGoogleMetadataGet(Guid bid)
        {
            using (var db = new PponDbContext())
            {
                return db.DealGoogleMetadataDbSet.FirstOrDefault(t => t.Bid == bid);
            }
        }

        public void DealGoogleMetadataSet(DealGoogleMetadata metadata)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(metadata).State = metadata.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
        }

        public List<DealDiscountProperty> GeOnlineDealDiscountProperties()
        {
            using (var db = new PponDbContext())
            {
                string sql = string.Format(@"
select {0}
from deal_discount_property ddp with(nolock) 
inner join business_hour bh on bh.GUID = ddp.bid
where ddp.bid in 
(select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= dateadd(day, -1, cast(getdate() as date))
						and effective_start < dateadd(day, 1, cast(getdate() as date)))
and getdate() between bh.business_hour_order_time_s and bh.business_hour_order_time_e
", db.GetModelSelectColumns<DealDiscountProperty>());
                var result = db.DealDiscountPropertyDbSet.SqlQuery(sql).ToList();
                return result;
            }
        }

        #region

        public DealDiscountProperty GetDealDiscountProperty(Guid bid)
        {
            using (var db = new PponDbContext())
            {
                return db.DealDiscountPropertyDbSet.FirstOrDefault(t => t.Bid == bid);
            }
        }

        public void SetDealDiscountProperty(DealDiscountProperty item)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(item).State = item.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void DeleteDealDiscountProperty(Guid bid)
        {
            using (var db = new PponDbContext())
            {
                string sql = @"DELETE FROM deal_discount_property
                                 WHERE bid = @bid";

                SqlParameter para1 = new SqlParameter("@bid", System.Data.SqlDbType.UniqueIdentifier)
                {
                    Value = bid
                };

                db.Database.ExecuteSqlCommand(sql, para1);
            }
        }

        public List<Guid> GetDealPageViewBidsTop10(int userId)
        {
            List<Guid> result;
            using (var db = new PponDbContext())
            {
                result = db.DealPageViewDbSet.Where(t => t.UserId == userId).Select(t => t.DealId).ToList()
                    .Distinct().Take(10).ToList();
            }

            return result;
        }

        public List<DealPageView> GetDealPageViews(DateTime startTime, DateTime endTime)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                return db.DealPageViewDbSet.Where(t => t.StartTime >= startTime && t.StartTime < endTime).ToList();
            }
        }

        #endregion

        #region DealDiscountPriceBlacklist

        public List<DealDiscountPriceBlacklist> GetAllDealDiscountPriceBlacklist()
        {
            using (var db = new PponDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from qe in db.DealDiscountPriceBlacklistDbSet
                               select qe;
                    return cols.ToList();
                }
            }
        }

        public DealDiscountPriceBlacklist GetDealDiscountPriceBlacklist(Guid bid)
        {
            using (var db = new PponDbContext())
            {
                return db.DealDiscountPriceBlacklistDbSet.FirstOrDefault(t => t.Bid == bid);
            }
        }

        public bool InsertDealDiscountPriceBlacklist(DealDiscountPriceBlacklist dealDiscountPriceBlacklist)
        {
            using (var db = new PponDbContext())
            {
                if (!db.DealDiscountPriceBlacklistDbSet.Any(t => t.Bid == dealDiscountPriceBlacklist.Bid))
                {
                    db.Entry(dealDiscountPriceBlacklist).State = EntityState.Added;
                    return db.SaveChanges() > 0;
                }

                return true;
            }
        }

        public void DeleteDealDiscountPriceBlacklist(Guid bid)
        {
            using (var db = new PponDbContext())
            {
                string sql = @"DELETE FROM deal_discount_price_blacklist
                                 WHERE bid = @bid";

                SqlParameter para1 = new SqlParameter("@bid", System.Data.SqlDbType.UniqueIdentifier)
                {
                    Value = bid
                };

                db.Database.ExecuteSqlCommand(sql, para1);
            }
        }

        #endregion

        #region 成效報表

        public List<FrontDealsDailyStat> GetFrontDealsDailyStatListByRange(DateTime start, DateTime end)
        {
            using (var db = new PponDbContext())
            {
                using (db.NoLock())
                {
                    var result = from qe in db.FrontDealsDailyStatsDbSet
                                 where (qe.Date >= start && qe.Date < end)
                                 select qe;
                    return result.ToList();
                }
            }
        }

        public List<FrontDealOrder> GetFrontDealOrderByRange(DateTime start, DateTime end)
        {
            using (var db = new PponDbContext())
            {
                var result = db.FrontDealOrderDbSet.SqlQuery(
                         @"select id, UserId, CreateTime, Amount, DiscountAmount, IsNull(bc.base_cost, 0) BaseCost from (
                            select 
                                ctl.order_guid id, 
                                g.business_hour_guid, 
                                o.user_id UserId, 
                                o.create_time CreateTime, 
                                sum(ctl.amount) Amount, 
                                sum(ctl.discount_amount) DiscountAmount 
                            from [order] o with(nolock) 
                        inner join cash_trust_log ctl with(nolock) on o.GUID = ctl.order_guid 
                        inner join group_order g with(nolock) on g.order_guid = o.parent_order_id
                        where (o.order_status & 8 ) = 8 and order_from_type in (3, 2) 
                        and o.create_time >= @start and o.create_time < @end
                        group by ctl.order_guid, g.business_hour_guid, o.user_id, o.create_time
                        ) sumAmount
                        inner join view_deal_base_cost bc with(nolock) ON sumAmount.business_hour_guid = bc.business_hour_guid"
                         , new SqlParameter("start", start)
                         , new SqlParameter("end", end))
                     .ToList();

                return result;
            }
        }

        public void ImportFrontDealsDailyState(DateTime startDateTime, DateTime endDateTime,
            List<FrontDealsDailyStat> frontDealsDailyStats)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                using (var db = new PponDbContext())
                {
                    #region Delete

                    string sql = @"DELETE FROM front_deals_daily_stat
                                  WHERE date >= @start and date < @end";

                    SqlParameter pStart = new SqlParameter("@start", System.Data.SqlDbType.DateTime)
                    {
                        Value = startDateTime
                    };

                    SqlParameter pEnd = new SqlParameter("@end", System.Data.SqlDbType.DateTime)
                    {
                        Value = endDateTime
                    };

                    db.Database.ExecuteSqlCommand(sql, pStart, pEnd);

                    #endregion

                    #region Insert

                    db.BulkInsert(frontDealsDailyStats);
                    db.SaveChanges();

                    #endregion
                }

                scope.Complete();
            }
        }

        public bool InsertFrontViewCount(FrontViewCount frontViewCount)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(frontViewCount).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        public List<FrontViewCount> GetFrontViewCountByRange(DateTime start, DateTime end)
        {
            using (var db = new PponDbContext())
            {
                using (db.NoLock())
                {
                    var result = from qe in db.FrontViewCountDbSet
                                 where (qe.Date >= start && qe.Date < end)
                                 select qe;
                    return result.ToList();
                }
            }
        }

        #endregion

        #region Solo Edm

        /// <summary>
        /// 新增大量 SoleEdmMain 要發送給哪些Email
        /// </summary>
        /// <param name="sendEmails"></param>
        public void SaveSoloEdmSendEmails(List<SoloEdmSendEmail> sendEmails)
        {
            using (var db = new PponDbContext())
            {
                db.BulkInsert(sendEmails);
            }
        }
        /// <summary>
        /// 依據 mainId 刪除設定發送的Email
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        public void DeleteSoleEdmSendEmailsByMainId(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.Database.CommandTimeout = 120;
                string sql = "delete from solo_edm_send_email where main_id = @mainId";
                db.Database.ExecuteSqlCommand(sql,
                    new SqlParameter("@mainId", mainId));
            }
        }
        /// <summary>
        /// 取得特定SoloEdm發送名單數目
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        /// <returns></returns>
        public int GetSoloEdmSendEmailCount(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();

                int count = db.SoloEdmSendEmailDbSet.Count(t => t.MainId == mainId);
                return count;
            }
        }
        /// <summary>
        /// 取得特定SoloEdm發送名單
        /// </summary>
        /// <param name="mainId">SoloEdmMain的Id</param>
        /// <returns>回傳 emails</returns>
        public List<string> GetSoloEdmSendEmails(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();

                var result = db.SoloEdmSendEmailDbSet.Where(t => t.MainId == mainId).Select(t => t.Email).ToList();
                return result;
            }
        }

        public void DeleteDealDiscountPropertyExpired()
        {
            using (var db = new PponDbContext())
            {
                string sql = @"
            delete from deal_discount_property 
            where modified_time < dateadd(hour, -2, getdate())
";
                db.Database.ExecuteSqlCommand(sql);
            }
        }

        #endregion

        #region 限時優惠

        public List<LimitedTimeSelectionDeal> GetEffectiveLimitedTimeSelectionDeals(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                var result = db.LimitedTimeSelectionDealDbSet.Where(t => t.MainId == mainId && t.Enabled)
                    .OrderBy(t => t.Sequence).ToList();
                return result;
            }
        }
        /// <summary>
        /// 取得 限時專區 日期主題設定與檔次設定數
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public Dictionary<DateTime, int> GetLimitedTimeSelectionMainSummary(DateTime startDate, DateTime endDate)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                string sql = @"
                    select main.the_date, count(deal.id) as record_count from limited_time_selection_main main
                    left join limited_time_selection_deals deal on main.id = deal.main_id and deal.enabled = 1
                    group by main.the_date";
                var result = db.QueryGroupByCountRawSql<DateTime, int>(sql);
                return result;
            }
        }

        public List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionAllDeals(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                var result = db.LimitedTimeSelectionDealDbSet.Where(t => t.MainId == mainId)
                    .OrderBy(t => t.Sequence).ToList();
                return result;
            }
        }

        public List<LimitedTimeSelectionDeal> GetLimitedTimeSelectionValidDeals(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                var result = db.LimitedTimeSelectionDealDbSet
                    .Where(t => t.MainId == mainId && t.Enabled == true)
                    .OrderBy(t => t.Sequence).ToList();
                return result;
            }
        }

        public LimitedTimeSelectionMain GetLimitedTimeSelectionMain(int mainId)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                return db.LimitedTimeSelectionMainDbSet.FirstOrDefault(t => t.Id == mainId);
            }
        }

        public LimitedTimeSelectionMain GetLimitedTimeSelectionMain(DateTime theDate)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                return db.LimitedTimeSelectionMainDbSet.FirstOrDefault(t => t.TheDate == theDate);
            }
        }

        public void SaveLimitedTimeSelectionMain(LimitedTimeSelectionMain main)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(main).State = main.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
        }

        public int SaveLimitedTimeSelectionDeal(LimitedTimeSelectionDeal deal)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(deal).State = deal.Id == 0 ? EntityState.Added : EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public int SetLimitedTimeSelectionDealDisabled(int mainId, Guid bid, string userId, DateTime now)
        {
            using (var db = new PponDbContext())
            {
                var selection = db.LimitedTimeSelectionDealDbSet.FirstOrDefault(t => t.MainId == mainId && t.Bid == bid);
                if (selection == null)
                {
                    return 0;
                }
                selection.Enabled = false;
                selection.ModifyId = userId;
                selection.ModifyTime = now;
                db.Entry(selection).State = EntityState.Modified;
                return db.SaveChanges();
            }
        }

        public List<LimitedTimeSelectionLog> GetLimitedTimeSelectionLogs(int mainId)
        {
            using (var db = new PponDbContext())
            {
                return db.LimitedTimeSelectionLogDbSet.Where(t => t.MainId == mainId)
                    .OrderByDescending(t => t.Id)
                    .ToList();
            }
        }
        public void SaveLimitedTimeSelectionLog(LimitedTimeSelectionLog log)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(log).State = log.Id == 0 ? EntityState.Added : EntityState.Modified;
                db.SaveChanges();
            }
        }

        #endregion

        #region 檔次已訂購數量

        public int OrderedQuantityGet(Guid bid)
        {
            string sql = @"
                        SELECT 	
		                        (CASE WHEN n.ordered_quantity IS NULL THEN 0 ELSE n.ordered_quantity END) AS ordered_quantity		
                        FROM         dbo.business_hour AS a 
                                              LEFT OUTER JOIN
                                              dbo.group_order AS j ON j.business_hour_guid = a.GUID LEFT OUTER JOIN
                                                  (SELECT     o.parent_order_id AS order_guid, SUM(od.item_quantity) AS ordered_quantity
                                                    FROM          dbo.[order] AS o INNER JOIN
                                                                           dbo.order_detail AS od ON od.order_GUID = o.GUID
                                                    WHERE      (o.order_status & 8 > 0) AND (o.order_status & 512 = 0) AND (od.status <> 2)
                                                    GROUP BY o.parent_order_id) AS n ON n.order_guid = j.order_guid 

                        where business_hour_guid = @bid";

            List<SqlParameter> para = new List<SqlParameter>();
            para.Add(new SqlParameter("@bid", bid.ToString()));

            using (var db = new OrderDbContext())
            {
                return db.Database.SqlQuery<int>(sql, para.ToArray()).FirstOrDefault();
            }
        }

        #endregion

        #region 每日頁面瀏覽數/銷售數資料

        public void DeleteDealDailySalesInfo(DateTime theDate)
        {
            using (var db = new PponDbContext())
            {
                string sql = @"
            delete from deal_daily_sales_info 
            where the_date =  @theDate
";
                db.Database.ExecuteSqlCommand(sql, new SqlParameter("@theDate", theDate));
            }
        }

        public int GetDealDailySalesInfoCount(DateTime theDate)
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                return db.DealDailySalesInfoDbSet.Count(t => t.TheDate == theDate);
            }
        }

        public Dictionary<Guid, List<DealDailySalesInfo>> GetPriorDealDailySalesInfos(DateTime startDate, DateTime endDate)
        {
            using (var db = new PponDbContext())
            {
                DateTime endNextDate = endDate.AddDays(1);
                db.NoLock();
                return db.DealDailySalesInfoDbSet.Where(t => t.TheDate >= startDate && t.TheDate < endNextDate)
                    .ToList()
                    .GroupBy(t => t.MainDealId)
                    .ToDictionary(t => t.Key, t => t.ToList());
            }
        }

        public void BulkInsertDealDailySalesInfo(List<DealDailySalesInfo> dealDailySalesInfos)
        {
            using (var db = new PponDbContext())
            {
                db.BulkInsert(dealDailySalesInfos);
                db.SaveChanges();
            }
        }

        public Dictionary<Guid, decimal> GetDealWeeklyConverationRateDict(DateTime theDate)
        {
            using (var db = new PponDbContext())
            {
                var result = db.DealDailySalesInfoDbSet
                    .Where(t => t.TheDate == theDate)
                    .Select(t => new
                    {
                        MainDealId = t.MainDealId,
                        Rate = t.WeeklyConversionRate
                    }).ToDictionary(t => t.MainDealId, t => t.Rate);
                return result;
            }
        }

        #endregion

        #region Google ADS Log

        public void SaveADSLog(AdsLog adsLog)
        {
            using (var db = new PponDbContext())
            {
                db.Entry(adsLog).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        #endregion


        #region 推播

        public List<QueuedMemberPushMessage> GetQueuedMemberPushMessagePendingList()
        {
            using (var db = new PponDbContext())
            {
                db.NoLock();
                var items = db.QueuedMemberPushMessageSet.Where(t => t.Status == 0).ToList();
                return items;
            }
        }

        public void QueuedMemberPushMessageSet(QueuedMemberPushMessage entity)
        {
            using (var db = new PponDbContext())
            {
                if (entity.Id == 0)
                {
                    db.Entry(entity).State = EntityState.Added;
                    if (entity.CreateTime == default(DateTime))
                    {
                        entity.CreateTime = DateTime.Now;
                    }
                }
                else
                {
                    db.Entry(entity).State = EntityState.Modified;
                    if (entity.ModifyTime == null)
                    {
                        entity.ModifyTime = DateTime.Now;
                    }
                }
                db.SaveChanges();
            }
        }

        #endregion

        #region Line

        public void LineUserPageViewSet(LineUserPageView entity)
        {
            using (var db = new PponDbContext())
            {
                if (entity.Id == 0)
                {
                    db.Entry(entity).State = EntityState.Added;
                    if (entity.CreateTime == default(DateTime))
                    {
                        entity.CreateTime = DateTime.Now;
                    }
                }
                else
                {
                    db.Entry(entity).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        #endregion

        #region HomepageBlock

        public List<HomepageBlock> GetLatestHomepageBlocks()
        {
            using (var db = new PponDbContext())
            {
                var items = db.HomepageBlockDbSet.Where(t => t.Latest).OrderBy(t => t.Sequence).ToList();
                return items;
            }
        }

        public bool SaveHomepageBlocks(HomepageBlock newBlock)
        {
            bool result = false;
            if (newBlock.Id != 0)
            {
                throw new Exception("SaveHomepageBlocks 只允許新增，Id不應該有值.");
            }
            try
            {
                using (var db = new PponDbContext())
                {
                    using (var tx = db.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
                    {
                        var prevBlocks =
                            db.HomepageBlockDbSet.Where(t => t.DocGuid == newBlock.DocGuid && t.Latest).ToList();
                        string oldContent = null;
                        bool? oldEnabled = null;
                        foreach (var block in prevBlocks)
                        {
                            oldContent = block.Content;
                            oldEnabled = block.Enabled;
                        }
                        bool changed = oldContent != newBlock.Content || oldEnabled != newBlock.Enabled;
                        if (oldContent == null || changed)
                        {
                            foreach (var block in prevBlocks)
                            {
                                block.Latest = false;
                                db.Entry(block).State = EntityState.Modified;
                            }
                            db.Entry(newBlock).State = EntityState.Added;
                        }
                        result = db.SaveChanges() > 0;
                        tx.Commit();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Warn("SaveHomepageBlocks fail.", ex);
            }
            return result;
        }

        #endregion

        #region EnterpriseMember 企業帳號購物金記錄

        public List<EnterpriseMember> GetEnterpriseMembers()
        {
            using (var db = new PponDbContext())
            {
                return db.EnterpriseMemberSet.OrderBy(t => t.UserId).ToList();
            }
        }

        public EnterpriseMember GetEnterpriseMember(int eemId)
        {
            using (var db = new PponDbContext())
            {
                return db.EnterpriseMemberSet.FirstOrDefault(t => t.Id == eemId);
            }
        }
        public EnterpriseMember GetEnterpriseMemberByUserId(int userId)
        {
            using (var db = new PponDbContext())
            {
                return db.EnterpriseMemberSet.FirstOrDefault(t => t.UserId == userId);
            }
        }

        public bool SaveEnterpriseMember(EnterpriseMember member)
        {
            using (var db = new PponDbContext())
            {
                if (member.Id == 0)
                {
                    db.Entry(member).State = EntityState.Added;
                    member.CreateTime = DateTime.Now;
                }
                else
                {
                    db.Entry(member).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public void MarkEnterpriseMemberDeleted(int eemId, string deletedBy)
        {
            using (var db = new PponDbContext())
            {
                var member = db.EnterpriseMemberSet.FirstOrDefault(t => t.Id == eemId);
                if (member.IsDeleted == false)
                {
                    member.IsDeleted = true;
                    member.DeletedTime = DateTime.Now;
                    member.DeletedBy = deletedBy;
                }
                db.SaveChanges();
            }
        }

        public void SaveEnterpriseDepisitOrder(int eemId, int withdrawalAmount, int userId, string createId)
        {
            using (var db = new PponDbContext())
            {
                var depositOrder = new EnterpriseDepositOrder
                {
                    Amount = withdrawalAmount,
                    CreateId = createId,
                    Guid = Guid.NewGuid(),
                    CreateTime = DateTime.Now,
                    EnterpriseMemberId = eemId,
                    UserId = userId
                };
                db.Entry(depositOrder).State = EntityState.Added;
                db.SaveChanges();
            }
        }

        public void SaveEnterpriseWithdrawalOrder(int eemId, int withdralwlAmount, int userId, string createId)
        {
            using (var db = new PponDbContext())
            {
                var deposits = db.EnterpriseDepositOrderSet.Where(t => t.EnterpriseMemberId == eemId).ToList();
                var withdrawals = (from w in db.EnterpriseWithdrawalOrderSet
                                   from d in db.EnterpriseDepositOrderSet
                                   where w.DepositId == d.Id && d.EnterpriseMemberId == eemId
                                   select w).ToList();

                foreach (var deposit in deposits)
                {
                    int depositAmount = deposit.Amount ?? 0;
                    int usedAmount = withdrawals
                        .Where(t => t.DepositId == deposit.Id)
                        .Select(t => t.Amount)
                        .Sum().GetValueOrDefault();
                    int availableAmount = depositAmount - usedAmount;
                    if (availableAmount == 0)
                    {
                        continue;
                    }
                    if (withdralwlAmount > availableAmount)
                    {
                        db.Entry(new EnterpriseWithdrawalOrder
                        {
                            Amount = availableAmount,
                            DepositId = deposit.Id,
                            CreateId = createId,
                            CreateTime = DateTime.Now
                        }).State = EntityState.Added;
                        db.SaveChanges();
                        withdralwlAmount = withdralwlAmount - availableAmount;
                    }
                    else
                    {
                        db.Entry(new EnterpriseWithdrawalOrder
                        {
                            Amount = withdralwlAmount,
                            DepositId = deposit.Id,
                            CreateId = createId,
                            CreateTime = DateTime.Now
                        }).State = EntityState.Added;
                        db.SaveChanges();
                        withdralwlAmount = 0;
                        break;
                    }
                }
            }
        }

        public List<EnterpriseDepositOrder> GetEnterpriseDepisitOrders(int eemId)
        {
            using (var db = new PponDbContext())
            {
                var depositLogs = db.EnterpriseDepositOrderSet
                    .Where(t => t.EnterpriseMemberId == eemId)
                    .OrderBy(t => t.Id).ToList();
                return depositLogs;
            }
        }

        public List<EnterpriseWithdrawalOrder> GetEnterpriseWithdrawalOrders(int depositId)
        {
            using (var db = new PponDbContext())
            {
                var withdrawalOrders = db.EnterpriseWithdrawalOrderSet
                    .Where(t => t.DepositId == depositId)
                    .OrderByDescending(t => t.Id).ToList();
                return withdrawalOrders;
            }
        }

        #endregion

    }
}
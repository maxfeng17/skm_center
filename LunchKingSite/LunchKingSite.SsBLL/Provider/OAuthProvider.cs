﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System;

namespace LunchKingSite.SsBLL.Provider
{
    public class OAuthProvider : IOAuthProvider
    {
        public OauthClient ClientGet(string appId)
        {
            return DB.Select().From<OauthClient>()
                .Where(OauthClient.AppIdColumn).IsEqualTo(appId)
                .ExecuteSingle<OauthClient>();
        }

        public OauthClient ClientGetByName(string name)
        {
            return DB.Select().From<OauthClient>()
                .Where(OauthClient.NameColumn).IsEqualTo(name)
                .ExecuteSingle<OauthClient>();
        }

        public OauthClientCollection ClientGetList()
        {
            return DB.SelectAllColumnsFrom<OauthClient>()
                .Where(OauthClient.Columns.Enabled).IsEqualTo(true)
                .OrderDesc(OauthClient.Columns.CreatedTime)
                .ExecuteAsCollection<OauthClientCollection>();
        }

        public void ClientSet(OauthClient client)
        {
            DB.Save(client);
        }

        public bool ClientHasUser(int clientId, int userId)
        {
            return DB.Select().From<OauthClientUser>()
                .Where(OauthClientUser.ClientIdColumn).IsEqualTo(clientId)
                .And(OauthClientUser.UserIdColumn).IsEqualTo(userId)
                .ExecuteSingle<OauthClientUser>()
                .IsLoaded;
        }

        public void ClientRemoveUser(int clientId, int userId)
        {
            DB.Delete().From<OauthClientUser>()
                .Where(OauthClientUser.ClientIdColumn).IsEqualTo(clientId)
                .And(OauthClientUser.UserIdColumn).IsEqualTo(userId)
                .Execute();
        }

        public OauthClientUser ClientUserGet(int clientId, int userId)
        {
            return DB.Select().From<OauthClientUser>()
                .Where(OauthClientUser.ClientIdColumn).IsEqualTo(clientId)
                .And(OauthClientUser.UserIdColumn).IsEqualTo(userId)
                .ExecuteSingle<OauthClientUser>();
        }

        public void ClientUserSet(int clientId, int userId)
        {
            OauthClientUser clientUser = new OauthClientUser();
            clientUser.ClientId = clientId;
            clientUser.UserId = userId;
            DB.Insert(clientUser);
        }

        public OauthClientPermissionCollection ClientPermissionGetList(int clientId)
        {
            return DB.SelectAllColumnsFrom<OauthClientPermission>()
                .Where(OauthClientPermission.Columns.ClientId).IsEqualTo(clientId)
                .ExecuteAsCollection<OauthClientPermissionCollection>();
        }

        public void ClientPermissionDeleteByClientId(int clientId)
        {
            DB.Delete().From<OauthClientPermission>().Where(OauthClientPermission.Columns.ClientId)
                .IsEqualTo(clientId).Execute();
        }

        public void ClientPermissionSetList(int clientId, OauthClientPermissionCollection oauthClientPermissionCollection)
        {
            DB.SaveAll(oauthClientPermissionCollection);
        }


        public OauthTokenCollection OAuthTokenGetList(string appId)
        {
            var sql = @"SELECT * FROM oauth_token with(nolock) WHERE app_id=@appId
                            AND expired_time > GETDATE()
                            AND DATEDIFF(dd, created_time, expired_time) > 3600";
            var data = new OauthTokenCollection();

            QueryCommand qc = new QueryCommand(sql, OauthToken.Schema.Provider.Name);
            qc.AddParameter("appId", appId, DbType.String);
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void TokenSetList(OauthTokenCollection tokens)
        {
            DB.SaveAll(tokens);
        }

        public void TokenSet(OauthToken token)
        {
            DB.Save(token);
        }

        public void TokenPermissionSetList(OauthTokenPermissionCollection tokenPerms)
        {
            DB.SaveAll(tokenPerms);
        }

        public OauthToken TokenGetByAccessToken(string accessToken)
        {
            return DB.Select().From<OauthToken>()
                .Where(OauthToken.Columns.AccessToken).IsEqualTo(accessToken)
                .ExecuteSingle<OauthToken>();
        }

        public OauthToken TokenGetByCode(string code)
        {
            return DB.Select().From<OauthToken>()
                .Where(OauthToken.Columns.Code).IsEqualTo(code)
                .ExecuteSingle<OauthToken>();
        }

        public OauthTokenPermissionCollection TokenPermissionGetList(int tokenId)
        {
            return DB.SelectAllColumnsFrom<OauthTokenPermission>()
                .Where(OauthTokenPermission.Columns.TokenId).IsEqualTo(tokenId)
                .ExecuteAsCollection<OauthTokenPermissionCollection>();
        }


        public void TokenPermissionDeleteByTokenId(int tokenId)
        {
            DB.Delete().From<OauthTokenPermission>().Where(OauthTokenPermission.Columns.TokenId)
                .IsEqualTo(tokenId).Execute();
        }


        public OauthToken TokenGetByRefreshToken(string refreshToken)
        {
            return DB.Select().From<OauthToken>()
                .Where(OauthToken.Columns.RefreshToken).IsEqualTo(refreshToken)
                .ExecuteSingle<OauthToken>();
        }

        public OauthClientCollection ClientGetListByUser(int userId)
        {
            string sql = @"
                SELECT c.*
                  FROM [oauth_client] c
                  inner join oauth_client_user cu on cu.client_id = c.id
                  where cu.user_id = @userId
            ";

            QueryCommand qc = new QueryCommand(sql, OauthClient.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);

            OauthClientCollection result = new OauthClientCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public void ClientUserDelete(int id)
        {
            DB.Delete<OauthClientUser>(OauthClientUser.Columns.Id, id);
        }

        public void TokenSetInvalid(int clientId, int userId)
        {
            string sql = @"
                update oauth_token
                set oauth_token.is_valid = 0
                from oauth_token t
                inner join oauth_client c on c.app_id = t.app_id
                where c.id = @clientId and t.user_id = @userId
            ";

            QueryCommand qc = new QueryCommand(sql, OauthToken.Schema.PropertyName);
            qc.AddParameter("clientId", clientId, DbType.Int32);
            qc.AddParameter("userId", userId, DbType.Int32);

            DB.Execute(qc);

        }
        /// <summary>
        /// 取得逾期的Token集合
        /// </summary>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        public OauthTokenCollection OauthTokenGetExpiredList(DateTime expiredDate)
        {
            return DB.SelectAllColumnsFrom<OauthToken>()
                .Where(OauthToken.Columns.IsValid).IsEqualTo(true)
                .And(OauthToken.Columns.AccessToken).IsNotNull()
                .And(OauthToken.Columns.ExpiredTime).IsLessThan(expiredDate)
                .ExecuteAsCollection<OauthTokenCollection>();
        }

        public OauthClientCollection OauthClientGetListByChannelClient()
        {
            string sql = @"
                select oc.* from oauth_client oc with(nolock) inner join channel_client_property cp with(nolock)
                on oc.app_id = cp.app_id
				where cp.general_commission IS NOT NULL and cp.general_gross_margin IS NOT NULL 
				and cp.min_gross_margin IS NOT NULL and cp.min_commission IS NOT NULL 
            ";

            QueryCommand qc = new QueryCommand(sql, OauthClient.Schema.Provider.Name);
            OauthClientCollection result = new OauthClientCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public OauthClientCollection OauthClientGetByAccessToken(string accessToken)
        {
            string sql = @"select oc.* from oauth_client oc with(nolock)
                    inner join oauth_token ot with(nolock)
                    on oc.app_id = ot.app_id
                    where access_token = @token
                    and is_valid = 1 and expired_time > getdate()";

            QueryCommand qc = new QueryCommand(sql, OauthClient.Schema.Provider.Name);
            qc.AddParameter("@token", accessToken, DbType.String);
            OauthClientCollection result = new OauthClientCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }
    }
}

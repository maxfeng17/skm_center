using System;
using System.Data;
using System.Linq;
using System.Collections.Generic;
using LunchKingSite.Core;
using LunchKingSite.Core.Constant;
using LunchKingSite.DataOrm;
using SubSonic;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelCustom;
using log4net;
using System.Text;
using System.Text.RegularExpressions;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.SsBLL
{
    public class SSSellerProvider : ISellerProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        ILog logger = LogManager.GetLogger(typeof(Seller).Name);
        #region SellerCategory
        public bool SellerCategorySave(SellerCategory s)
        {
            DB.Save<SellerCategory>(s);
            return true;
        }

        public bool SellerCategoryDelete(int id)
        {
            DB.Destroy<SellerCategory>(SellerCategory.Columns.Id, id);
            return true;
        }

        public SellerCategoryCollection SellerCategoryGetList(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerCategory>().Where(SellerCategory.SellerGuidColumn).IsEqualTo(sellerGuid).OrderAsc(SellerCategory.Columns.CategoryId).ExecuteAsCollection<SellerCategoryCollection>();
        }
        #endregion

        #region Methods for Seller
        public int SellerGetCount()
        {
            SubSonic.Query qry = new SubSonic.Query(Seller.Schema);
            return qry.GetCount(Seller.Columns.Guid);
        }

        public int SellerGetCountByPrefix(string vbsPrefix)
        {
            string sql = string.Format(@"
SELECT count(*) FROM {0}
WHERE {1} = @vbsPrefix"
                , Seller.Schema.TableName
                , Seller.Columns.VbsPrefix);
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("vbsPrefix", vbsPrefix, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public SellerCollection SellerGetByPrefix(string vbsPrefix)
        {
            return DB.SelectAllColumnsFrom<Seller>().NoLock()
                .Where(Seller.Columns.VbsPrefix).IsEqualTo(vbsPrefix)
                .ExecuteAsCollection<SellerCollection>();
        }

        public Seller SellerGet(Guid g)
        {
            return DB.Get<Seller>(g);
        }

        public Seller SellerGet(string column, object value)
        {
            return DB.Get<Seller>(column, value);
        }

        public Seller SellerGetByBid(Guid bid)
        {
            QueryCommand qc = new QueryCommand(@"select * from seller where [guid]=(select seller_GUID from business_hour where [guid]=@bid)", Seller.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);

            Seller col = new Seller();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            return col;
        }

        public Seller SellerGetByCompany(string SellerName, string SignCompanyID)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.SellerName).IsEqualTo(SellerName)
                .And(Seller.Columns.SignCompanyID).IsEqualTo(SignCompanyID)
                .ExecuteSingle<Seller>();
        }

        public bool SellerSet(Seller s)
        {
            DB.Save<Seller>(s);
            return true;
        }

        public bool SellerSaveAll(SellerCollection s)
        {
            DB.SaveAll(s);
            return true;
        }

        public SellerCollection SellerGetList()
        {
            return DB.SelectAllColumnsFrom<Seller>().ExecuteAsCollection<SellerCollection>();
        }

        public SellerCollection SellerGetList(List<Guid> guids)
        {
            //return DB.SelectAllColumnsFrom<Seller>()
            //    .Where(Seller.Columns.Guid)
            //    .In(guids)
            //    .ExecuteAsCollection<SellerCollection>();

            SellerCollection result = new SellerCollection();

            List<Guid> tempSellers = guids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempSellers.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<Seller>()
                    .Where(Seller.Columns.Guid).In(tempSellers.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<SellerCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }

        public SellerCollection SellerGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(column).IsEqualTo(value)
                .ExecuteAsCollection<SellerCollection>();
        }

        public List<string> SellerGetColumnListByLike(string column, string partValue)
        {
            List<string> list = new List<string>();
            if (!string.IsNullOrWhiteSpace(partValue))
            {
                string sql = @"SELECT {0}.{1} FROM {0} WITH (NOLOCK) WHERE {1} LIKE N'%' + @Value + '%'";
                sql = string.Format(sql, Seller.Schema.TableName, column);
                QueryCommand query = new QueryCommand(sql, Seller.Schema.Provider.Name);
                query.AddParameter("@Value", partValue, DbType.String);

                using (IDataReader reader = DataService.GetReader(query))
                {
                    while (reader.Read())
                    {
                        list.Add(reader.GetString(0));
                    }
                }
            }
            return list;
        }

        public SellerCollection SellerGetByCompanyList(string SellerName, string SignCompanyID)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.SellerName).IsEqualTo(SellerName)
                .And(Seller.Columns.SignCompanyID).IsEqualTo(SignCompanyID)
                .ExecuteAsCollection<SellerCollection>();
        }

        public Seller SellerGetBySellerGuid(string CompanyName, string SellerGuid)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.SellerName).IsEqualTo(CompanyName)
                .And(Seller.Columns.Guid).IsEqualTo(SellerGuid)
                .ExecuteSingle<Seller>();
        }

        public SellerCollection SellerGetRepeatList(string seller_name, string signcompany_id, string company_boss_name)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.SellerName).IsEqualTo(seller_name)
                .And(Seller.Columns.SignCompanyID).IsEqualTo(signcompany_id).And(Seller.Columns.CompanyBossName).IsEqualTo(company_boss_name)
                .ExecuteAsCollection<SellerCollection>();
        }

        public SellerCollection SellerGetListBySellerGuidList(List<Guid> sellerGuidList)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.Guid).In(sellerGuidList).ExecuteAsCollection<SellerCollection>();
        }

        public int SellerGetTempCount(KeyValuePair<string, string> search_keys, string user_name)
        {
            string sqlcommand = "select count(1) from " + Seller.Schema.TableName + " where "
                + search_keys.Key + " like @" + search_keys.Key;
            if (!string.IsNullOrEmpty(user_name))
                sqlcommand += " and " + Seller.Columns.ApplyId + "=@username";
            sqlcommand += " and " + Seller.Columns.Department + "=3 and " + Seller.Columns.TempStatus + "<>" + (int)SellerTempStatus.Returned;
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@" + search_keys.Key, "%" + search_keys.Value + "%", DbType.String);
            if (!string.IsNullOrEmpty(user_name))
                qc.Parameters.Add("@username", user_name, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }

        public SellerCollection SellerGetTempList(int pageStart, int pageLength, KeyValuePair<string, string> search_keys, string user_name, string orderBy)
        {
            string defOrderBy = Seller.Columns.SellerId;
            string sqlcommand = "select * from " + Seller.Schema.TableName + " where "
                + search_keys.Key + " like @" + search_keys.Key;
            if (!string.IsNullOrEmpty(user_name))
                sqlcommand += " and " + Seller.Columns.ApplyId + "=@username";
            sqlcommand += " and " + Seller.Columns.Department + "=3 and " + Seller.Columns.TempStatus + "<>" + (int)SellerTempStatus.Returned;
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@" + search_keys.Key, "%" + search_keys.Value + "%", DbType.String);
            if (!string.IsNullOrEmpty(user_name))
                qc.Parameters.Add("@username", user_name, DbType.String);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            SellerCollection scol = new SellerCollection();
            scol.LoadAndCloseReader(DataService.GetReader(qc));
            return scol;
        }

        public SellerCollection SellerCollectionGetReturnCase(string username, SellerTempStatus temp_status)
        {
            string sqlcommand = "select * from " + Seller.Schema.TableName + " where " + Seller.Columns.TempStatus + "=@tempstatus ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + Seller.Columns.ApplyId + "=@applyid ";
            sqlcommand += " order by " + Seller.Columns.ApplyTime + " desc ";
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@applyid", username, DbType.String);
            qc.Parameters.Add("@tempstatus", (int)temp_status, DbType.Int32);

            SellerCollection scol = new SellerCollection();
            scol.LoadAndCloseReader(DataService.GetReader(qc));
            return scol;
        }

        protected QueryCommand GetSellerWhereQC(SellerStatusFlag statusMask, SellerStatusFlag expectStatus, string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<Seller>(filter);

            if (statusMask != 0)
            {
                if (qc.Parameters.Count == 0)
                    qc.CommandSql += SqlFragment.WHERE;
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += " (" + Seller.Columns.SellerStatus + " & @statusMask ) = @expectStatus";
                qc.AddParameter("@statusMask", statusMask, DbType.Int32);
                qc.AddParameter("@expectStatus", expectStatus, DbType.Int32);
            }
            return qc;
        }

        public SellerCollection SellerGetList(int pageStart, int pageLength, string orderBy, string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<Seller, SellerCollection>(pageStart, pageLength, orderBy, filter);
        }
        
        /// <summary>
        /// 商家管理查詢
        /// </summary>
        /// <param name="currentPage">起始頁</param>
        /// <param name="pageSize">每頁幾筆</param>
        /// <param name="orderColumn">排序</param>
        /// <param name="seller">其他篩選</param>
        /// <param name="userName">登入者email</param>
        /// <param name="isPrivate">自己的</param>
        /// <param name="isPublic">公池</param>
        /// <param name="EmpUserId">登入者userId</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <param name="Dept">業務部門</param>
        /// <param name="CityId">商家地區</param>
        /// <param name="SellerLevelList">商家規模</param>
        /// <param name="SellerPorperty">商家類型</param>
        /// <param name="SellerManagLogNumber">商家聯繫狀況(數量)</param>
        /// <param name="SellerManagLogDatepart">商家聯繫狀況(單位)</param>
        /// <param name="SalesUserId">業務</param>
        /// <param name="RowCount"></param>
        /// <returns></returns>
        public List<Seller> SellerGetList(int currentPage, int pageSize, string orderColumn, Seller seller,
                                          string userName, bool isPrivate, bool isPublic, int EmpUserId, string CrossDeptTeam,
                                          string Dept, int CityId, string SellerLevelList, string SellerPorperty, int SellerManagLogNumber,
                                          string SellerManagLogDatepart, int? SalesId, ref int RowCount)
        {
            List<Seller> sellerList = new List<Seller>();

            List<Guid> SellerPorpertyList = null;
            List<Guid> SellerManageLogList = null;
            if (!string.IsNullOrEmpty(SellerPorperty))
            {
                SellerPorpertyList = GetSellerPorperty(SellerPorperty);
            }
            if (SellerManagLogNumber != 0)
            {
                SellerManageLogList = GetSellerManageLog(SellerManagLogNumber, SellerManagLogDatepart);
            }



            int idx = 0;
            if (SellerPorpertyList != null || SellerManageLogList != null)
            {
                if (SellerPorpertyList != null && SellerPorpertyList.Count > 0)
                {
                    while (idx <= SellerPorpertyList.Count - 1)
                    {
                        var query = GetSellerQueryByCondition(seller, userName, isPrivate, isPublic, EmpUserId,
                                    CrossDeptTeam, Dept, CityId, SellerLevelList,
                                    null,
                                    null,
                                    SalesId);

                        int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                        var batchBh = query
                            .And(Seller.Columns.Guid).In(SellerPorpertyList.Skip(idx).Take(batchLimit))
                            .ExecuteAsCollection<SellerCollection>();
                        sellerList.AddRange(batchBh);
                        idx += batchLimit;

                        RowCount += sellerList.Count();
                    }
                }
                if (SellerManageLogList != null && SellerManageLogList.Count > 0)
                {
                    if (sellerList != null && sellerList.Count() > 0)
                    {
                        //前面已撈過
                        sellerList = sellerList.Where(x => SellerManageLogList.Contains(x.Guid)).ToList();

                        RowCount += sellerList.Count();
                    }
                    else
                    {
                        while (idx <= SellerManageLogList.Count - 1)
                        {
                            var query = GetSellerQueryByCondition(seller, userName, isPrivate, isPublic, EmpUserId,
                                    CrossDeptTeam, Dept, CityId, SellerLevelList,
                                    null,
                                    null,
                                    SalesId);
                            int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                            var batchBh = query
                                .And(Seller.Columns.Guid).In(SellerManageLogList.Skip(idx).Take(batchLimit))
                                .ExecuteAsCollection<SellerCollection>();
                            sellerList.AddRange(batchBh);
                            idx += batchLimit;

                            RowCount += sellerList.Count();
                        }
                    }
                }
                sellerList = sellerList.Skip((currentPage - 1) * pageSize).Take(pageSize).ToList();
            }
            else
            {
                var query = GetSellerQueryByCondition(seller, userName, isPrivate, isPublic, EmpUserId,
                                    CrossDeptTeam, Dept, CityId, SellerLevelList,
                                    null,
                                    null,
                                    SalesId);
                query.OrderDesc(string.Format("{0}.{1}", Seller.Schema.TableName, orderColumn)).Paged(currentPage, pageSize, Seller.Columns.Guid);
                sellerList = query.ExecuteAsCollection<SellerCollection>().ToList();
            }

            return sellerList;//sellerList 必須再用Linq指定欄位再排序一次，因為SubSonic的Paged的有問題
        }


        public int SellerGetCount(Seller seller, string userName, bool isPrivate, bool isPublic, int EmpUserId,
                                  string CrossDeptTeam, string Dept, int CityId, string SellerLevelList, string SellerPorperty,
                                  int SellerManagLogNumber, string SellerManagLogDatepart, int RowCount, int? SalesId)
        {
            int dataCount = 0;

            List<Guid> SellerPorpertyList = null;
            List<Guid> SellerManageLogList = null;
            if (!string.IsNullOrEmpty(SellerPorperty))
            {
                SellerPorpertyList = GetSellerPorperty(SellerPorperty);
            }
            if (SellerManagLogNumber != 0)
            {
                SellerManageLogList = GetSellerManageLog(SellerManagLogNumber, SellerManagLogDatepart);
            }

            var query = GetSellerQueryByCondition(seller, userName, isPrivate, isPublic, EmpUserId, CrossDeptTeam, Dept,
                CityId, SellerLevelList,
                null,
                null,
                SalesId);

            if (SellerPorpertyList != null || SellerManageLogList != null)
            {
                //直接讀取上次取得的筆數
                dataCount = RowCount;
            }
            else
            {
                //原本的寫法，不讀取上次取得的筆數，因為他只有10筆
                dataCount = query.GetRecordCount();
            }

            return dataCount;
        }

        private SqlQuery GetSellerQueryByCondition(Seller seller, string userName, bool isPrivate, bool isPublic, int EmpUserId,
                                                   string CrossDeptTeam, string Dept, int CityId, string SellerLevelList, List<Guid> SellerPorperty,
                                                   List<Guid> SellerManageLog, int? SalesId)
        {
            var query = DB.SelectAllColumnsFrom<Seller>().NoLock().LeftOuterJoin(Member.UniqueIdColumn, Seller.SalesIdColumn).LeftOuterJoin(Employee.UserIdColumn, Seller.SalesIdColumn);

            if (query != null)
            {
                if (!string.IsNullOrWhiteSpace(seller.SellerName))
                {
                    query.And(Seller.Columns.SellerName).Like(string.Format("%{0}%", seller.SellerName));
                }
                if (!string.IsNullOrWhiteSpace(seller.SellerBossName))
                {
                    query.And(Seller.Columns.SellerBossName).Like(string.Format("%{0}%", seller.SellerBossName));
                }
                if (!string.IsNullOrWhiteSpace(seller.CompanyName))
                {
                    query.And(Seller.Columns.CompanyName).Like(string.Format("%{0}%", seller.CompanyName));
                }
                if (!string.IsNullOrWhiteSpace(seller.SignCompanyID))
                {
                    query.And(Seller.Columns.SignCompanyID).IsEqualTo(seller.SignCompanyID);
                }
                if (seller.TempStatus != -1)
                {
                    query.And(Seller.Columns.TempStatus).IsEqualTo(seller.TempStatus);
                }

                //if (EmpUserId != 0)
                //{
                //    query.AndExpression(Seller.Columns.SalesId).IsEqualTo(EmpUserId);
                //    query.Or(Seller.Columns.SalesId).IsNull();//公池不檢查
                //}

                ////跨區組
                //if (!string.IsNullOrEmpty(CrossDeptTeam))
                //{
                //    int i = 1;
                //    foreach (var c in CrossDeptTeam.Split('/'))
                //    {
                //        if (i == 1)
                //        {
                //            if (c.Contains("["))
                //            {
                //                string a = query.SQLCommand;
                //                query.AndExpression(Employee.Columns.DeptId).IsEqualTo(c.Substring(0, c.IndexOf("[")));
                //                query.And(Employee.Columns.TeamNo).In((c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1)).Split(','));
                //            }
                //            else
                //            {
                //                query.AndExpression(Employee.Columns.DeptId).IsEqualTo(c);
                //            }
                //        }
                //        else
                //        {
                //            if (c.Contains("["))
                //            {
                //                string a = query.SQLCommand;
                //                query.Or(Employee.Columns.DeptId).IsEqualTo(c.Substring(0, c.IndexOf("[")));
                //                query.And(Employee.Columns.TeamNo).In((c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1)).Split(','));
                //            }
                //            else
                //            {
                //                query.Or(Employee.Columns.DeptId).IsEqualTo(c);
                //            }
                //        }
                //        i++;

                //    }

                //    query.Or(Seller.Columns.SalesId).IsNull();//公池不檢查

                //    query.CloseExpression();


                //}

                //if (!string.IsNullOrWhiteSpace(seller.SellerSales) || isPrivate || isPublic)
                //{
                //    query.AndExpression(Seller.Columns.Guid).IsNull();

                //    if (!string.IsNullOrWhiteSpace(seller.SellerSales))
                //    {
                //        query.Or(Member.Columns.UserEmail).Like(string.Format("%{0}%", seller.SellerSales));
                //    }
                //    if (isPrivate)
                //    {
                //        query.Or(Member.Columns.UserEmail).IsEqualTo(userEmail);
                //    }
                //    if (isPublic)
                //    {
                //        query.Or(Seller.Columns.SellerSales).IsNull();
                //        query.Or(Seller.Columns.SellerSales).IsEqualTo(string.Empty);
                //    }


                //    query.CloseExpression();
                //}

                if (!string.IsNullOrWhiteSpace(Dept))
                {
                    query.And(Employee.Columns.DeptId).IsEqualTo(Dept);
                }

                if (CityId != 0)
                {
                    query.And(Seller.Columns.CityId).IsEqualTo(CityId);
                }

                //in 要用陣列才讀的到
                if (!string.IsNullOrWhiteSpace(SellerLevelList))
                {
                    query.And(Seller.Columns.SellerLevel).In(SellerLevelList.Split(','));
                }

                if (SellerPorperty != null)
                {
                    if (SellerPorperty.Count() > 0)
                    {
                        query.AndExpression(Seller.Columns.Guid).IsEqualTo(SellerPorperty[0]);
                        for (int i = 1; i < SellerPorperty.Count(); i++)
                        {
                            query.Or(Seller.Columns.Guid).IsEqualTo(SellerPorperty[i]);
                        }
                    }
                    else
                        query.AndExpression(Seller.Columns.Guid).IsNull();
                }

                if (SellerManageLog != null)
                {
                    if (SellerManageLog.Count() > 0)
                    {
                        query.AndExpression(Seller.Columns.Guid).IsEqualTo(SellerManageLog[0]);
                        for (int i = 1; i < SellerManageLog.Count(); i++)
                        {
                            query.Or(Seller.Columns.Guid).IsEqualTo(SellerManageLog[i]);
                        }
                    }
                    else
                        query.AndExpression(Seller.Columns.Guid).IsNull();
                }
            }



            //new
            var query2 = DB.Select(Seller.Columns.Guid).From<Seller>().NoLock().LeftOuterJoin(SellerSale.SellerGuidColumn, Seller.GuidColumn).LeftOuterJoin(Employee.UserIdColumn, SellerSale.SellerSalesIdColumn).Distinct();
            if (EmpUserId != 0)
            {
                query2.AndExpression(SellerSale.Columns.SellerSalesId).IsEqualTo(EmpUserId);
                query2.Or(SellerSale.Columns.SellerSalesId).IsNull();//公池不檢查
            }

            if (SalesId != null)
                query2.AndExpression(SellerSale.Columns.SellerSalesId).IsEqualTo(SalesId);

            //跨區組
            if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                int i = 1;
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (i == 1)
                    {
                        if (c.Contains("["))
                        {
                            query2.AndExpression(Employee.Columns.DeptId).IsEqualTo(c.Substring(0, c.IndexOf("[")));
                            query2.And(Employee.Columns.TeamNo).In((c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1)).Split(','));
                        }
                        else
                        {
                            query2.AndExpression(Employee.Columns.DeptId).IsEqualTo(c);
                        }
                    }
                    else
                    {
                        if (c.Contains("["))
                        {
                            query2.Or(Employee.Columns.DeptId).IsEqualTo(c.Substring(0, c.IndexOf("[")));
                            query2.And(Employee.Columns.TeamNo).In((c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1)).Split(','));
                        }
                        else
                        {
                            query2.Or(Employee.Columns.DeptId).IsEqualTo(c);
                        }
                    }
                    i++;

                }

                query2.Or(SellerSale.Columns.SellerSalesId).IsNull();//公池不檢查

                query2.CloseExpression();


            }

            if (!string.IsNullOrWhiteSpace(seller.SellerSales) || isPrivate || isPublic)
            {
                query2.AndExpression(Seller.Columns.Guid).IsNull();

                if (!string.IsNullOrWhiteSpace(seller.SellerSales))
                {
                    query2.Or(Member.Columns.UserEmail).Like(string.Format("%{0}%", seller.SellerSales));
                }
                if (isPrivate)
                {
                    query2.Or(Member.Columns.UserEmail).IsEqualTo(userName);
                }
                if (isPublic)
                {
                    query2.Or(SellerSale.Columns.SellerSalesId).IsNull();
                }


                query2.CloseExpression();
            }

            query.AndExpression(Seller.Columns.Guid).In(query2);



            return query;
        }

        /// <summary>
        /// 取得商家類型
        /// </summary>
        /// <param name="SellerPorperty"></param>
        /// <returns></returns>
        private List<Guid> GetSellerPorperty(string SellerPorperty)
        {
            string sql = @"--select sum(num) from (";
            for (int i = 0; i < SellerPorperty.Split("/").Length; i++)
            {
                string Porperty = SellerPorperty.Split("/")[i].ToString();
                string PorpertyName = Porperty.Substring(0, Porperty.IndexOf("["));
                string PorpertyNum = Porperty.Substring(Porperty.IndexOf("[") + 1, Porperty.IndexOf("]") - Porperty.IndexOf("[") - 1);
                if (PorpertyName == "OnDeal")
                {
                    sql += @"--on檔
                                            SELECT s.guid
		                                    FROM   seller s
				                                   LEFT JOIN business_hour b
						                                 ON s.guid = b.seller_GUID
		                                    WHERE  Getdate() BETWEEN business_hour_order_time_s AND business_hour_order_time_e
				                                   AND (";
                    for (int j = 0; j < PorpertyNum.Split(",").Length; j++)
                    {
                        sql += "s.seller_porperty like '%" + PorpertyNum.Split(",")[j] + "%' or ";
                    }
                    sql = sql.TrimEnd("or ") + ")" + " union all ";
                }
                else if (PorpertyName == "HaveCooperation")
                {
                    sql += @"--過去或未來有檔
                                            SELECT seller_guid
                                            FROM   seller s
                                                   LEFT JOIN business_hour b
                                                          ON s.guid = b.seller_GUID
                                            WHERE  CONVERT(VARCHAR(10), business_hour_order_time_e, 111) != '9999/12/31'
                                                   AND Getdate() NOT BETWEEN business_hour_order_time_s AND business_hour_order_time_e
                                                   AND seller_guid NOT IN (SELECT DISTINCT seller_guid FROM business_hour WHERE getdate() BETWEEN business_hour_order_time_s AND business_hour_order_time_e )
                                                   AND (";
                    for (int j = 0; j < PorpertyNum.Split(",").Length; j++)
                    {
                        sql += "s.seller_porperty like '%" + PorpertyNum.Split(",")[j] + "%' or ";
                    }
                    sql = sql.TrimEnd("or ") + ") GROUP  BY seller_guid ";
                    sql += " HAVING Max(business_hour_order_time_e) > Dateadd(year, -1, CONVERT(VARCHAR(10), Getdate(), 111))   union all ";
                }
                else if (PorpertyName == "YearNoCooperation")
                {
                    sql += @"--超過一年沒檔
                                            SELECT seller_guid
                                            FROM   seller s
                                                   LEFT JOIN business_hour b
                                                          ON s.guid = b.seller_GUID
                                            WHERE  CONVERT(VARCHAR(10), business_hour_order_time_e, 111) != '9999/12/31'
                                                   AND (";
                    for (int j = 0; j < PorpertyNum.Split(",").Length; j++)
                    {
                        sql += "s.seller_porperty like '%" + PorpertyNum.Split(",")[j] + "%' or ";
                    }
                    sql = sql.TrimEnd("or ") + ") GROUP  BY seller_guid ";
                    sql += @" HAVING Max(business_hour_order_time_e) < Dateadd(year, -1, CONVERT(VARCHAR(10), Getdate(), 111))    union all ";
                }
                else if (PorpertyName == "NotCooperation")
                {
                    sql += @"--沒有任何檔次
                                            SELECT s.guid
			                                FROM   seller s
				                                   LEFT JOIN business_hour b
						                                  ON s.guid = b.seller_GUID
			                                WHERE  b.guid IS NULL
				                                   AND (";
                    for (int j = 0; j < PorpertyNum.Split(",").Length; j++)
                    {
                        sql += "s.seller_porperty like '%" + PorpertyNum.Split(",")[j] + "%' or ";
                    }
                    sql = sql.TrimEnd("or ") + ")";
                }


            }
            sql = sql.TrimEnd("union all ") + "--) as a";

            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            List<Guid> GuidList = new List<Guid>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    GuidList.Add(reader.GetGuid(0));
                }
            }

            return GuidList;
        }

        /// <summary>
        /// 取得商家聯繫狀況
        /// </summary>
        /// <param name="Number"></param>
        /// <param name="Datepart"></param>
        /// <returns></returns>
        private List<Guid> GetSellerManageLog(int Number, string Datepart)
        {
            string sql = @"SELECT s.GUID
                           FROM   seller s
                                   LEFT JOIN seller_manage_log sml
                                          ON s.guid = sml.seller_guid
                           WHERE  sml.id IS NOT NULL
                           GROUP  BY s.GUID ";

            sql += "HAVING Dateadd(" + Datepart + ",-" + Number.ToString() + ", CONVERT(VARCHAR(10), Getdate(), 111)) >= CONVERT(VARCHAR(10), Max(sml.create_time), 111) ";
            //負號是往前算


            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            List<Guid> GuidList = new List<Guid>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    GuidList.Add(reader.GetGuid(0));
                }
            }

            return GuidList;
        }
        public SellerCollection SellerGetParentList(params string[] filter)
        {
            QueryCommand qc = GetDCWhere(Seller.Schema, filter);
            qc.CommandSql = "select * from " + Seller.Schema.TableName + " with(nolock) " + qc.CommandSql + (filter.Length > 0 ? " and" : "where") + " guid not in (select seller_guid from seller_tree)";
            SellerCollection col = new SellerCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public SellerCollection SellerGetSkmParentList()
        {
            const string sql = @"SELECT s.* FROM seller s WITH(nolock) inner join seller_tree st with(nolock)
                On s.GUID = st.seller_guid
                WHERE st.root_seller_guid = @skmRootGuid
                AND guid in (SELECT seller_guid FROM skm_shoppe WHERE is_shoppe = 0 AND is_available = 1 group by seller_guid)";
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("@skmRootGuid", config.SkmRootSellerGuid, DbType.Guid);
            SellerCollection col = new SellerCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public SellerCollection SellerGetListByNameWithActiveTransport(string sellerName)
        {
            QueryCommand qc = new QueryCommand(@"select * from seller with(nolock) where seller_name like @sn and (seller_status & @onlineMask = @on) and (seller_status & @tranMask > 0)", Seller.Schema.Provider.Name);
            qc.AddParameter("@sn", sellerName, DbType.String);
            qc.AddParameter("@onlineMask", SellerStatusFlag.OnlineMask, DbType.Int32);
            qc.AddParameter("@on", (int)SellerOnlineType.Online << (int)SellerStatusBitShift.OnlineType, DbType.Int32);
            qc.AddParameter("@tranMask", SellerStatusFlag.TransportConfidenceMask, DbType.Int32);
            SellerCollection col = new SellerCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
            //return
            //    new InlineQuery().ExecuteAsCollection<SellerCollection>(
            //        @"select * from seller where seller_name like @sn and (seller_status & @onlineMask = @on) and (seller_status & @tranMask > 0)",
            //        sellerName, (int)SellerStatusFlag.OnlineMask, (int)SellerOnlineType.Online << (int)SellerStatusBitShift.OnlineType, (int)SellerStatusFlag.TransportConfidenceMask);
        }

        /// <summary>
        /// 商家管理搜尋(舊)
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="SalesId"></param>
        /// <param name="CrossDeptTeam"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public SellerCollection SellerGetList(int pageStart, int pageLength, string orderBy, int SalesId, string CrossDeptTeam, string[] filter)
        {
            string defOrderBy = Seller.Columns.CreateTime + " desc";
            QueryCommand qc = GetDCWhere(Seller.Schema, filter);
            qc.CommandSql = "select " + Seller.Schema.TableName + ".* from " + Seller.Schema.Provider.DelimitDbName(Seller.Schema.TableName) + " with(nolock) inner join " + Employee.Schema.Provider.DelimitDbName(Employee.Schema.TableName) + " e " +
                            " on seller.sales_id =e.user_id " + qc.CommandSql;
            qc.CommandSql = SellerGetData(SalesId, CrossDeptTeam, qc);

            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);


            SellerCollection vfcCol = new SellerCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }
        public SellerCollection SellerGetListByAppId(string OauthClientAppId)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.Columns.OauthClientAppId).IsEqualTo(OauthClientAppId)
                .ExecuteAsCollection<SellerCollection>();
        }
        public int SellerGetCount(int SalesId, string CrossDeptTeam, params string[] filter)
        {
            QueryCommand qc = GetDCWhere(Seller.Schema, filter);
            qc.CommandSql = "select count(*) from " + Seller.Schema.Provider.DelimitDbName(Seller.Schema.TableName) + " with(nolock) inner join " + Employee.Schema.Provider.DelimitDbName(Employee.Schema.TableName) + " e " +
                            " on seller.sales_id = e.user_id  " + qc.CommandSql;
            qc.CommandSql = SellerGetData(SalesId, CrossDeptTeam, qc);


            return (int)DataService.ExecuteScalar(qc);
        }


        private string SellerGetData(int SalesId, string CrossDeptTeam, QueryCommand qc)
        {
            #region SalesId
            if (SalesId != 0)
            {
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += Seller.Columns.SalesId + " = @salesid";
                qc.AddParameter("@salesid", SalesId, DbType.Int32);
            }
            #endregion

            #region CrossDeptTeam
            if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                string tmpsql = "";
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        tmpsql += "(dept_id='" + c.Substring(0, c.IndexOf("[")) + "' and team_no in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                    }
                    else
                    {
                        tmpsql += " (dept_id='" + c + "') or ";
                    }


                }
                tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                qc.CommandSql += tmpsql;


            }

            #endregion

            return qc.CommandSql;
        }

        /// <summary>
        /// 依 sellerName 參數 查詢相似名稱的 seller
        /// </summary>
        /// <param name="sellerName">大概的 seller 名稱</param>
        /// <returns></returns>
        public SellerCollection SellerGetListByLikelyName(string sellerName)
        {
            string sql = @"
                SELECT * 
                FROM SELLER
                WHERE seller_name like  N'%' + @sellerName + '%'
            ";
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("@sellerName", sellerName, DbType.String);
            SellerCollection sellers = new SellerCollection();
            sellers.LoadAndCloseReader(DataService.GetReader(qc));
            return sellers;
        }

        public SellerCollection SellerGetList(IEnumerable<Guid> sellerGuids)
        {
            var result = new SellerCollection();

            List<Guid> tempBids = sellerGuids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempBids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<Seller>()
                    .Where(Seller.Columns.Guid).In(tempBids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<SellerCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }

        public string SellerGetLastSellerId()
        {
            string sql = @"select top 1 seller_id from seller
                where create_time > '20160101' 
                order by seller_id desc";
            object obj = DataService.ExecuteScalar(new QueryCommand(sql, Seller.Schema.Provider.Name));
            return obj as string;
        }

        public SellerCollection SellerGetListBySalesName(string salesName, bool isPool)
        {
            string sql = @"
                SELECT * 
                FROM Seller
                WHERE Seller_Sales like  N'%' + @salesName + '%'";

            if (isPool)
            {
                sql += " Or isNull(Seller_Sales,'') = '' ";
            }

            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("@salesName", salesName, DbType.String);
            SellerCollection sellers = new SellerCollection();
            sellers.LoadAndCloseReader(DataService.GetReader(qc));
            return sellers;
        }

        /// <summary>
        /// 取得商家聯絡資訊 by 在地/宅配
        /// (注意此view為一次性使用沒有下索引)
        /// </summary>
        /// <param name="deliveryType"></param>
        /// <returns></returns>
        public ViewSellerContactCollection GetSellerContactsByDeliveryType(DeliveryType deliveryType)
        {
            return DB.SelectAllColumnsFrom<ViewSellerContact>()
                .Where(ViewSellerContact.Columns.DeliveryType).IsEqualTo((int)deliveryType)
                .ExecuteAsCollection<ViewSellerContactCollection>();
        }

        #endregion

        #region SellerSales
        public SellerSaleCollection SellerSaleGetBySellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerSale>().Where(SellerSale.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<SellerSaleCollection>();
        }

        public SellerSale SellerSaleGetBySellerGuid(Guid sellerGuid, int salesGroup, int SalesType)
        {
            return DB.SelectAllColumnsFrom<SellerSale>().Where(SellerSale.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(SellerSale.Columns.SalesGroup).IsEqualTo(salesGroup)
                .And(SellerSale.Columns.SalesType).IsEqualTo(SalesType)
                .ExecuteSingle<SellerSale>();

        }

        public bool SellerSaleSet(SellerSale sellerSales)
        {
            DB.Save(sellerSales);
            return true;
        }

        public void SellerSaleDelete(Guid sellerGuid)
        {
            DB.Delete().From<SellerSale>().Where(SellerSale.SellerGuidColumn).IsEqualTo(sellerGuid).Execute();
        }

        public void SellerSaleDelete(Guid sellerGuid, int salesGroup)
        {
            DB.Delete().From<SellerSale>().Where(SellerSale.SellerGuidColumn).IsEqualTo(sellerGuid)
                .And(SellerSale.Columns.SalesGroup).IsEqualTo(salesGroup).Execute();
        }
        public void SellerSaleDelete(Guid sellerGuid, int salesGroup, int salesType)
        {
            DB.Delete().From<SellerSale>().Where(SellerSale.SellerGuidColumn).IsEqualTo(sellerGuid)
                .And(SellerSale.Columns.SalesGroup).IsEqualTo(salesGroup)
                .And(SellerSale.Columns.SalesType).IsEqualTo(salesType).Execute();
        }
        #endregion

        #region SellerContractFiles
        public SellerContractFile SellerContractFileGetByGuid(Guid guid)
        {
            return DB.Get<SellerContractFile>(SellerContractFile.Columns.Guid, guid);
        }
        public SellerContractFileCollection SellerContractFileGetListBySellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerContractFile>().Where(SellerContractFile.Columns.SellerGuid).IsEqualTo(sellerGuid).And(SellerContractFile.Columns.Status).IsNotEqualTo(-1)
                .OrderAsc(SellerContractFile.Columns.CreateTime)
                .ExecuteAsCollection<SellerContractFileCollection>();
        }
        public bool SellerContractFileSet(SellerContractFile s)
        {
            DB.Save<SellerContractFile>(s);
            return true;
        }
        #endregion

        #region Methods for BizHour
        public BusinessHour BusinessHourGet(Guid guid)
        {
            return DB.Get<BusinessHour>(guid);
        }

        public bool BusinessHourDelete(Guid value)
        {
            DB.Destroy<BusinessHour>(BusinessHour.Columns.Guid, value);
            return true;
        }

        public bool BusinessHourSet(BusinessHour bh)
        {
            DB.Save<BusinessHour>(bh);
            return true;
        }

        public BusinessHourCollection BusinessHourGetListBySeller(Guid guid)
        {
            return BusinessHourGetList(BusinessHour.Columns.SellerGuid, guid);
        }

        public BusinessHourCollection BusinessHourGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<BusinessHour>().Where(column).IsEqualTo(value).ExecuteAsCollection<BusinessHourCollection>();
        }

        public BusinessHourCollection BusinessHourGetList(IEnumerable<Guid> bids)
        {
            BusinessHourCollection result = new BusinessHourCollection();

            List<Guid> tempBids = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempBids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<BusinessHour>()
                    .Where(BusinessHour.Columns.Guid).In(tempBids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<BusinessHourCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }

        public BusinessHourCollection BusinessHourGetListBySettlementTime(DateTime date_start, DateTime date_end)
        {
            return DB.SelectAllColumnsFrom<BusinessHour>().NoLock()
                .Where(BusinessHour.Columns.SettlementTime).IsBetweenAnd(date_start, date_end)
                .AndExpression(BusinessHour.Columns.BusinessHourDeliverTimeE).IsGreaterThan(date_end)
                .And(BusinessHour.Columns.ChangedExpireDate).IsNull().Or(BusinessHour.Columns.ChangedExpireDate).IsGreaterThan(date_end)
                .And(BusinessHour.Columns.ChangedExpireDate).IsNotNull().CloseExpression()
                .OrExpression(BusinessHour.Columns.BusinessHourDeliverTimeE).IsGreaterThan(date_end)
                .And(BusinessHour.Columns.ChangedExpireDate).IsNull().And(BusinessHour.Columns.SettlementTime).IsLessThan(date_start)
                .Or(BusinessHour.Columns.ChangedExpireDate).IsGreaterThan(date_end)
                .And(BusinessHour.Columns.ChangedExpireDate).IsNotNull().And(BusinessHour.Columns.SettlementTime).IsLessThan(date_start).CloseExpression()
                .ExecuteAsCollection<BusinessHourCollection>();
        }

        public Dictionary<Guid, Guid> BusinessHourGetFamiExpiredDeal(int precheckDays)
        {
            var sql = string.Format(@"SELECT fami.order_guid,b.guid as bid FROM business_hour b WITH(NOLOCK) 
INNER JOIN group_order g WITH(NOLOCK) ON b.GUID = g.business_hour_guid
INNER JOIN peztemp pez with(nolock) ON pez.Bid = b.GUID
INNER JOIN family_net_pincode fami with(nolock) on fami.peztemp_id = pez.Id
WHERE 
(
(b.changed_expire_date is NULL and b.business_hour_deliver_time_e BETWEEN DATEADD(dd, @precheckDays, GETDATE()) and GETDATE())
or 
(b.changed_expire_date is not null and b.changed_expire_date BETWEEN DATEADD(dd, @precheckDays, GETDATE()) and GETDATE())
)
AND g.status & {0} > 0 AND g.status & {1} > 0
AND b.business_hour_status & {2} > 0
AND fami.is_lock =0 and fami.is_verified = 0 and fami.is_verifying = 0 and fami.return_status = 0 
group by fami.order_guid,b.guid"
                        , (int)GroupOrderStatus.PEZevent
                        , (int)GroupOrderStatus.FamiDeal
                        , (int)BusinessHourStatus.GroupCoupon);

            QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
            qc.AddParameter("@precheckDays", -precheckDays, DbType.Int32);

            Dictionary<Guid, Guid> result = new Dictionary<Guid, Guid>();

            using (IDataReader reader = DataService.GetReader(qc))
            {
                using (DataTable dt = new DataTable())
                {
                    dt.Load(reader);
                    foreach (var row in dt.AsEnumerable())
                    {
                        result.Add(Guid.Parse(row["order_guid"].ToString()), Guid.Parse(row["bid"].ToString()));
                    }
                }
            }
            return result;
        }

        #endregion

        #region ViewBuildingSellerDeliveryHour

        public ViewBuildingSellerDeliveryHourCollection ViewBuildingSellerDeliveryHourGetList(Guid sellerGuid, Guid buildingGuid)
        {
            ViewBuildingSellerDeliveryHourCollection col = new ViewBuildingSellerDeliveryHourCollection();
            col.LoadAndCloseReader(
                    ViewBuildingSellerDeliveryHour.Query()
                    .WHERE(ViewBuildingSellerDeliveryHour.Columns.SellerGuid, sellerGuid)
                    .AND(ViewBuildingSellerDeliveryHour.Columns.BuildingGuid, buildingGuid).ExecuteReader());
            return col;
        }

        #endregion

        #region ViewSellerCityBizHour
        public ViewSellerCityBizHour ViewSellerCityBizHourGet(string column, object value)
        {
            ViewSellerCityBizHour view = new ViewSellerCityBizHour();
            view.LoadAndCloseReader(ViewSellerCityBizHour.FetchByParameter(column, value));
            return view;
        }

        public ViewSellerCityBizHourCollection ViewSellerCityBizHourGetList(int pageStart, int pageLength, string orderby, params string[] filter)
        {
            return
                SSHelper.GetQueryResultByFilterOrder<ViewSellerCityBizHour, ViewSellerCityBizHourCollection>(pageStart,
                                                                                                             pageLength,
                                                                                                             orderby,
                                                                                                             filter);
        }

        public ViewSellerCityBizHourCollection ViewSellerCityBizHourGetListByBuilding(int pageStart, int pageLength, string orderby, Guid buildingGuid, params string[] filter)
        {
            string defOrderBy = ViewSellerCityBizHour.Columns.SellerGuid;
            QueryCommand qc = SSHelper.GetWhereQC<ViewSellerCityBizHour>(filter);
            if (qc.CommandSql == " ")
                qc.CommandSql += " WHERE ";
            else
                qc.CommandSql += " AND ";


            qc.CommandSql += "(" + ViewSellerCityBizHour.Columns.BusinessHourGuid + " IN (SELECT " + BuildingDelivery.Columns.BusinessHourGuid + " FROM " + BuildingDelivery.Schema.Provider.DelimitDbName(BuildingDelivery.Schema.TableName) + " WHERE (" + BuildingDelivery.Columns.BuildingGuid + " = @bid)))";
            qc.CommandSql = "SELECT * FROM " + ViewSellerCityBizHour.Schema.Provider.DelimitDbName(ViewSellerCityBizHour.Schema.TableName) + qc.CommandSql;
            qc.AddParameter("@bid", buildingGuid, DbType.Guid);

            if (!string.IsNullOrEmpty(orderby))
            {
                qc.CommandSql += " ORDER BY " + orderby;
                defOrderBy = orderby;
            }

            qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            ViewSellerCityBizHourCollection view = new ViewSellerCityBizHourCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));

            return view;
        }

        /// <summary>
        /// Gets view_seller_city_biz_hour count list by building_Guid.
        /// </summary>
        /// <param name="buildingGuid">The building GUID.</param>
        /// <param name="bizHourType">Type of the biz hour.</param>
        /// <param name="onlineOnly">if set to <c>true</c> [online only].</param>
        /// <returns></returns>
        /// <remarks>IMPORTANT: Department types are : ZERO for "meal" , ONE for "cosmetics" , TWO for "delicacies"</remarks>

        public int ViewSellerCityBizHourGetCountByBuilding(Guid buildingGuid, params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<ViewSellerCityBizHour>(filter);

            if (qc.CommandSql == " ")
                qc.CommandSql += " WHERE ";
            else
                qc.CommandSql += " AND ";

            qc.CommandSql += "(" + ViewSellerCityBizHour.Columns.BusinessHourGuid + " IN (SELECT " + BuildingDelivery.Columns.BusinessHourGuid + " FROM " + BuildingDelivery.Schema.Provider.DelimitDbName(BuildingDelivery.Schema.TableName) + " WHERE (" + BuildingDelivery.Columns.BuildingGuid + " = @bid)))";
            qc.AddParameter("@bid", buildingGuid, DbType.Guid);
            qc.CommandSql = "SELECT * FROM " + ViewSellerCityBizHour.Schema.Provider.DelimitDbName(ViewSellerCityBizHour.Schema.TableName) + qc.CommandSql;

            ViewSellerCityBizHourCollection view = new ViewSellerCityBizHourCollection();
            view.LoadAndCloseReader(DataService.GetReader(qc));

            return view.Count;
        }
        #endregion

        #region Category

        public Dictionary<int, string> CategoriesNameGet()
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            var sql = @"select id, name from category with(nolock) group by id, name";
            QueryCommand query = new QueryCommand(sql, Category.Schema.Provider.Name);

            using (IDataReader reader = DataService.GetReader(query))
            {
                using (DataTable dt = new DataTable())
                {
                    dt.Load(reader);
                    foreach (var row in dt.AsEnumerable())
                    {
                        result.Add(int.Parse(row["id"].ToString()), row["name"].ToString());
                    }
                }
            }

            return result;
        }

        public Category CategoryGet(int categoryId)
        {
            return DB.Get<Category>(Category.Columns.Id, categoryId);
        }

        public Category CategoryGetAlibaba(string AlibabaId) {
            return DB.Get<Category>(Category.Columns.AlibabaId , AlibabaId);
        }

        public CategoryDependencyCollection GatCategoryParent (int categoryId) {
            return DB.SelectAllColumnsFrom<CategoryDependency>().Where(CategoryDependency.Columns.CategoryId).IsEqualTo(categoryId).ExecuteAsCollection<CategoryDependencyCollection>();
            //return DB.Get<CategoryDependency>(CategoryDependency.Columns.CategoryId , categoryId);
        }

        public void CategoryDelete(Category category)
        {
            DB.Delete().From<Category>().Where(Category.IdColumn).IsEqualTo(category.Id).Execute();
        }

        /// <summary>
        /// 取得需要排DealTimeSlot活動時程表的Category
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public CategoryDealTimeSlotCollection CategoryDealTimeSlotGetList(int type = 0)
        {
            return DB.SelectAllColumnsFrom<CategoryDealTimeSlot>().Where(CategoryDealTimeSlot.Columns.Type).IsEqualTo(type).OrderAsc(CategoryDealTimeSlot.Columns.CategoryId).OrderAsc(CategoryDealTimeSlot.Columns.AreaId).ExecuteAsCollection<CategoryDealTimeSlotCollection>();
        }

        public int GetCategoryMaxCode()
        {
            return new Select(Aggregate.Max(Category.CodeColumn)).From(Category.Schema).ExecuteScalar<int>();
        }

        public int GetCategoryMaxId()
        {
            return new Select(Aggregate.Max(Category.IdColumn)).From(Category.Schema).ExecuteScalar<int>();
        }

        public CategoryCollection CategoryGetAll(string orderBy)
        {
            Query qry = new Query(Category.Schema.TableName);
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }
            CategoryCollection coll = new CategoryCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }

        public CategoryCollection CategoryGetList(int type = 0)
        {
            return DB.SelectAllColumnsFrom<Category>().NoLock().Where(Category.Columns.Type).IsEqualTo(type).OrderAsc(Category.Columns.Rank).ExecuteAsCollection<CategoryCollection>();
        }

        public CategoryCollection CategoryGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<Category, CategoryCollection>(pageStart, pageLength, orderBy, filter);
        }

        public CategoryCollection CategoryGetList(List<int> categoryId)
        {
            return DB.SelectAllColumnsFrom<Category>().Where(Category.Columns.Id).In(categoryId).ExecuteAsCollection<CategoryCollection>();
        }

        public Dictionary<int, Category> CategoryGetDictionary()
        {
            string sql = "";
            sql = "select * from Category where status=1";
            QueryCommand qc = new QueryCommand(sql, Category.Schema.Provider.Name);
            CategoryCollection items = new CategoryCollection();
            items.LoadAndCloseReader(DataService.GetReader(qc));


            Dictionary<int, Category> result = new Dictionary<int, Category>();
            foreach (var item in items)
            {
                if (result.ContainsKey(item.Id) == false)
                {
                    result.Add(item.Id, item);
                }
            }
            return result;
        }

        /// <summary>
        /// 根據公司類別(館別)來取得類別
        /// </summary>
        /// <param name="deptType">公司類別(館別)~</param>
        /// <returns>類別集合</returns>
        public CategoryCollection CategoryGetListByDepartment(DepartmentTypes deptType)
        {
            string sql = "";
            sql = "Select DISTINCT c.* from category c " +
                  "inner join seller_category sc on sc.category_id=c.id " +
                  "inner join seller s on s.GUID=sc.seller_guid " +
                  "where s.department=@DeptID ";
            if (deptType == DepartmentTypes.Delicacies)
                sql += "AND c.code>1000 ";
            sql += "order by c.rank";
            QueryCommand qc = new QueryCommand(sql, Category.Schema.Provider.Name);
            qc.AddParameter("@DeptID", (int)deptType, DbType.Int32);
            CategoryCollection col = new CategoryCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        /// <summary>
        /// 由新光業務中台的id取得我們對應的類別
        /// </summary>
        /// <param name="AlibabaId">業務中台的類別id</param>
        /// <returns>類別集合</returns>
        public CategoryCollection CategoryGetListByAlibabaId(long AlibabaId)
        {
            string sql = "";
            sql = "Select DISTINCT c.* from category c " +
                  "where c.alibaba_id=@AlibabaId ";
            QueryCommand qc = new QueryCommand(sql, Category.Schema.Provider.Name);
            qc.AddParameter("@AlibabaId", AlibabaId, DbType.Int64);
            CategoryCollection col = new CategoryCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public int CategorySetList(CategoryCollection cc)
        {
            return DB.SaveAll(cc);
        }

        public int CategorySet(Category category)
        {
            int result = DB.Save(category);
            return result;
        }

        public string GetCategoryNameByCid(int cid)
        {
            var data = DB.SelectAllColumnsFrom<Category>()
                .Where(Category.Columns.Id).IsEqualTo(cid)
                .ExecuteSingle<Category>();
            return data.Name;
        }
        #endregion

        #region CategoryDependency

        public void CategoryDependencyDelete(CategoryDependencyCollection categoryDependencyCollection)
        {
            foreach (var item in categoryDependencyCollection)
            {
                DB.Delete().From<CategoryDependency>().Where(CategoryDependency.ParentIdColumn).IsEqualTo(item.ParentId)
                    .And(CategoryDependency.CategoryIdColumn).IsEqualTo(item.CategoryId)
                    .Execute();
            }
        }

        public CategoryDependencyCollection CategoryDependencyGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CategoryDependency, CategoryDependencyCollection>(1, -1, orderBy, filter);
        }

        public CategoryDependencyCollection CategoryDependencyGetAll(string orderBy)
        {
            Query qry = new Query(CategoryDependency.Schema.TableName);
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }
            CategoryDependencyCollection coll = new CategoryDependencyCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }

        public CategoryDependencyCollection CategoryDependencyByRegionGet(int regionId)
        {
            return DB.SelectAllColumnsFrom<CategoryDependency>().Where(CategoryDependency.Columns.ParentId).IsEqualTo(regionId).
                OrderDesc(CategoryDependency.Columns.Seq).ExecuteAsCollection<CategoryDependencyCollection>();
        }

        public CategoryDependencyCollection CategoryDependencyGet(int parentId, int categoryId)
        {
            return DB.SelectAllColumnsFrom<CategoryDependency>().Where(CategoryDependency.Columns.ParentId).IsEqualTo(parentId)
                .And(CategoryDependency.Columns.CategoryId).IsEqualTo(categoryId)
                .OrderDesc(CategoryDependency.Columns.Seq).ExecuteAsCollection<CategoryDependencyCollection>();
        }

        public int CategoryDependencySetList(CategoryDependencyCollection cdc)
        {
            return DB.SaveAll(cdc);
        }

        public int CategoryDependencySet(CategoryDependency categoryDependency)
        {
            int result = DB.Save(categoryDependency);
            return result;
        }

        #endregion CategoryDependency

        #region CategoryRegionDisplayRule

        public CategoryRegionDisplayRuleCollection CategoryRegionDisplayRuleGetList()
        {
            return DB.SelectAllColumnsFrom<CategoryRegionDisplayRule>()
                        .OrderAsc(CategoryRegionDisplayRule.Columns.CategoryId)
                        .ExecuteAsCollection<CategoryRegionDisplayRuleCollection>();
        }

        #endregion

        #region ViewCategoryDependency

        public ViewCategoryDependencyCollection ViewCategoryDependencyGetAll(string orderBy)
        {
            Query qry = new Query(ViewCategoryDependency.Schema.TableName)
                .AddWhere(ViewCategoryDependency.Columns.CategoryStatus, Comparison.Equals, (int)CategoryStatus.Enabled);
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }
            ViewCategoryDependencyCollection coll = new ViewCategoryDependencyCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }

        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByParentId(int parentId)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.ParentId).IsEqualTo(parentId)
                    .OrderAsc(ViewCategoryDependency.Columns.CategoryId).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }


        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByParentType(int parent_type)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.ParentType).IsEqualTo(parent_type)
                .And(ViewCategoryDependency.Columns.CategoryStatus).IsEqualTo(true)
                .OrderAsc(ViewCategoryDependency.Columns.CategoryId).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }

        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryId(int category_type)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.CategoryType).IsEqualTo(category_type)
                .OrderAsc(ViewCategoryDependency.Columns.CategoryId).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }
        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByCid(int categoryId)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.CategoryId).IsEqualTo(categoryId)
                .OrderAsc(ViewCategoryDependency.Columns.CategoryId).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }

        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryIdList(List<int> categoryid_list)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.CategoryId).In(categoryid_list).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }

        public ViewCategoryDependencyCollection ViewCategoryDependencyGetByCategoryCodeList(List<int> categoryCode_list)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDependency>().Where(ViewCategoryDependency.Columns.CategoryCode).In(categoryCode_list).ExecuteAsCollection<ViewCategoryDependencyCollection>();
        }

        #endregion ViewCategoryDependency

        #region ViewSellerCategoryCity
        public ViewSellerCategoryCityCollection ViewSellerCategoryCityGetList(string column, object value, bool onlyOnline, string orderBy)
        {
            ViewSellerCategoryCityCollection view = new ViewSellerCategoryCityCollection();
            Query qry = ViewSellerCategoryCity.Query().AddWhere(column, value);
            QueryCommand qc = qry.BuildSelectCommand();
            if (onlyOnline)
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " and (" + ViewSellerCategoryCity.Columns.SellerStatus + " & " + (int)SellerStatusFlag.OnlineMask + " = " + ((int)SellerOnlineType.Online << (int)SellerStatusBitShift.OnlineType) + ") ";
            }
            qc.CommandSql += " ORDER BY " + ViewSellerCategoryCity.Columns.CityName;

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }
        #endregion

        #region Store

        public bool StoreSet(Store store)
        {
            DB.Save(store);
            return true;
        }
        public Store StoreGet(Guid storeGuid)
        {
            return DB.Get<Store>(storeGuid);
        }
        public StoreCollection StoreGetListBySellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<Store>().Where(Store.Columns.SellerGuid).IsEqualTo(sellerGuid).ExecuteAsCollection<StoreCollection>();
        }
        public SellerCollection StoreGetListBySellerGuidAndStoreCode(List<Guid> sellerGuid, string storeCode)
        {
            return DB.SelectAllColumnsFrom<Seller>().NoLock().Where(Seller.Columns.Guid).In(sellerGuid)
                .And(Seller.Columns.StoreRelationCode).IsEqualTo(storeCode).ExecuteAsCollection<SellerCollection>();
        }
        public StoreCollection StoreGetListBySellerName(string sellerName, bool fuzzySearch = false)
        {
            string seller = fuzzySearch ? string.Format("%{0}%", sellerName) : sellerName;
            return DB.SelectAllColumnsFrom<Store>().Where(Store.Columns.SellerGuid)
                .In(new Select(Seller.Columns.Guid).From<Seller>()
                        .Where(Seller.Columns.SellerName).Like(seller))
                .ExecuteAsCollection<StoreCollection>() ?? new StoreCollection();
        }
        public StoreCollection StoreGetListByStoreGuidList(List<Guid> storeGuidList)
        {
            return DB.SelectAllColumnsFrom<Store>().Where(Store.Columns.Guid).In(storeGuidList).ExecuteAsCollection<StoreCollection>();
        }
        public StoreCollection StoreGetListBySellerGuidList(List<Guid> sellerGuidList)
        {
            var result = new StoreCollection();

            var tempIds = sellerGuidList.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<Store>()
                    .Where(Store.Columns.SellerGuid).In(tempIds.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<StoreCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }
        public SellerCollection StoreGetListByPCPmemberCard(string cardGroupId)
        {
            string sql = @"
                select * from seller s WITH (NOLOCK)
                JOIN membership_card_group_store m WITH (NOLOCK) ON s.Guid = m.store_guid
                WHERE m.card_group_id=@cardGroupId 
                and s.store_status = 0
                ";

            var qc = new QueryCommand(sql, Store.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);

            var data = new SellerCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public StoreCollection StoreGetListByBid(Guid bid, bool isBidSellStoresOnly = false)
        {
            string joinString = isBidSellStoresOnly ? string.Empty : " left ";
            string sql = string.Format(@"
            select t.* from 
	            (select * from store s with(nolock)
		            where seller_guid = 
		            (select seller_GUID from business_hour bh with(nolock) 
			            where bh.GUID = @bid)
	            ) as t
            {0} join ppon_store ps with(nolock) on ps.store_guid = t.Guid
            and ps.business_hour_guid = @bid
            order by isnumeric(ps.sort_order) desc, ps.sort_order
            ", joinString);
            QueryCommand qc = new QueryCommand(sql, Store.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            StoreCollection data = new StoreCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        /// <summary>
        /// HiDealSetup 專用
        /// </summary>
        /// <param name="sellerGuid"></param>
        /// <returns></returns>
        public DataTable GetInitialSellerBranchStores(Guid sellerGuid)
        {
            string sql = @"SELECT 
                                            NULL as Sequence,
                                            s.Guid as StoreGuid, 
                                            s.store_name as StoreName, 
                                            '' as UseTime, 
                                            city.city_name + zone.city_name + s.address_string as StoreAddress
                                        FROM seller 
                                        INNER JOIN store s
                                            ON seller.GUID = s.seller_guid
                                        left JOIN city as zone
                                        left JOIN city
                                            ON zone.parent_id = city.id
                                            ON s.township_id = zone.id
                                        WHERE seller.GUID=@sellerGuid";

            IDataReader reader = new InlineQuery().ExecuteReader(sql, sellerGuid.ToString());
            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        public StoreCollection StoreCollectionGetReturnCase(string username, SellerTempStatus temp_status)
        {
            string sqlcommand = "select * from " + Store.Schema.TableName + " where " + Store.Columns.TempStatus + "=@tempstatus ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + Store.Columns.ApplyId + "=@applyid  ";
            sqlcommand += " order by " + Store.Columns.ApplyTime;
            QueryCommand qc = new QueryCommand(sqlcommand, Seller.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@applyid", username, DbType.String);
            qc.Parameters.Add("@tempstatus", (int)temp_status, DbType.Int32);
            StoreCollection scol = new StoreCollection();
            scol.LoadAndCloseReader(DataService.GetReader(qc));
            return scol;
        }
        public ViewSellerStoreCollection ViewSellerStoreCollectionGetReturnCase(string username, SellerTempStatus temp_status)
        {
            string sqlcommand = "select * from " + ViewSellerStore.Schema.TableName + " where " + ViewSellerStore.Columns.TempStatus + "=@tempstatus ";
            if (!string.IsNullOrEmpty(username))
                sqlcommand += " and " + ViewSellerStore.Columns.ApplyId + "=@applyid  ";
            sqlcommand += " order by " + ViewSellerStore.Columns.ApplyTime;
            QueryCommand qc = new QueryCommand(sqlcommand, ViewSellerStore.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(username))
                qc.Parameters.Add("@applyid", username, DbType.String);
            qc.Parameters.Add("@tempstatus", (int)temp_status, DbType.Int32);
            ViewSellerStoreCollection scol = new ViewSellerStoreCollection();
            scol.LoadAndCloseReader(DataService.GetReader(qc));
            return scol;
        }

        public ViewSellerStoreCollection ViewSellerStoreCollectionGetSellerList(List<Guid> list)
        {
            return DB.SelectAllColumnsFrom<ViewSellerStore>().Where(Store.Columns.SellerGuid).In(list).ExecuteAsCollection<ViewSellerStoreCollection>();
        }

        public StoreCollection StoreGetListByPponDeal()
        {
            var sql = @"
select s.* from store as s with(nolock)
outer apply (select count(trust_id) as order_count 
from cash_trust_log with(nolock) 
where delivery_type = 1 and store_guid = s.guid
group by store_guid) as ctl
where s.status = 1
and ctl.order_count > 0
            ";
            var qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            var stores = new StoreCollection();
            stores.LoadAndCloseReader(DataService.GetReader(qc));
            return stores;
        }

        #endregion Store

        #region SkmShoppe

        public SkmShoppe SkmShoppeGet(Guid storeGuid)
        {
            return DB.Get<SkmShoppe>(SkmShoppe.Columns.StoreGuid, storeGuid);
        }

        public SkmShoppeCollection SkmShoppeCollectionGetStore()
        {
            return
                DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                    .Where(SkmShoppe.Columns.IsShoppe)
                    .IsEqualTo(false)
                    .And(SkmShoppe.Columns.IsAvailable)
                    .IsEqualTo(true)
                    .ExecuteAsCollection<SkmShoppeCollection>();
        }

        public SkmShoppeCollection SkmShoppeCollectionGetAll(bool isShoppe = true,bool isContainsNotAvailableShoppe = false)
        {

            if (isShoppe)
            {
                return isContainsNotAvailableShoppe
                    ? DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.IsShoppe).IsEqualTo(true)
                        .ExecuteAsCollection<SkmShoppeCollection>()
                    : DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.IsShoppe).IsEqualTo(true)
                        .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true).ExecuteAsCollection<SkmShoppeCollection>();
            }
            else
            {
                return isContainsNotAvailableShoppe
                    ? DB.SelectAllColumnsFrom<SkmShoppe>().ExecuteAsCollection<SkmShoppeCollection>()
                    : DB.SelectAllColumnsFrom<SkmShoppe>().And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                        .ExecuteAsCollection<SkmShoppeCollection>();
            }
        }

        public SkmShoppeCollection SkmShoppeCollectionGetAllSeller(bool isAvailable)
        {
            return DB.SelectAllColumnsFrom<SkmShoppe>().NoLock()
                .Where(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(isAvailable)
                .ExecuteAsCollection<SkmShoppeCollection>();
        }

        public Dictionary<string, string> SkmShoppeGetShopCodeNameList()
        {
            var ss =
                DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.IsAvailable).IsEqualTo(true)
                    .And(SkmShoppe.Columns.IsShoppe).IsEqualTo(false)
                    .ExecuteAsCollection<SkmShoppeCollection>();
            return ss.Select(x => new { x.ShopCode, x.ShopName }).ToDictionary(d => d.ShopCode, d => d.ShopName);
        }

        public SkmShoppeCollection SkmShoppeCollectionGetBySeller(Guid sellerGuid, bool isShoppe)
        {
            return DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.SellerGuid).In(sellerGuid)
                    .And(SkmShoppe.Columns.IsShoppe).IsEqualTo(isShoppe)
                    .And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true).ExecuteAsCollection<SkmShoppeCollection>();
        }

        public SkmShoppeCollection SkmShoppeCollectionGetByShopCode(string shopCode)
        {
            return DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.ShopCode).IsEqualTo(shopCode).And(SkmShoppe.Columns.IsShoppe).IsEqualTo(false).And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true).ExecuteAsCollection<SkmShoppeCollection>();
        }
        public SkmShoppeCollection SkmShoppeCollectionGetBySellerList(List<Guid> lists)
        {
            return DB.SelectAllColumnsFrom<SkmShoppe>().NoLock().Where(SkmShoppe.Columns.SellerGuid).In(lists).And(SkmShoppe.Columns.IsAvailable).IsEqualTo(true).ExecuteAsCollection<SkmShoppeCollection>();
        }

        public ViewPponStoreSkmCollection ViewPponStoreSkmGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewPponStoreSkm>().NoLock()
                .Where(ViewPponStoreSkm.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<ViewPponStoreSkmCollection>();
        }

        public ViewPponStoreSkmCollection ViewPponStoreSkmByBidStoreGuid(Guid bid, Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<ViewPponStoreSkm>().NoLock()
                .Where(ViewPponStoreSkm.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(ViewPponStoreSkm.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<ViewPponStoreSkmCollection>();
        }

        #endregion SkmShoppe

        #region StoreAccountInfo

        public bool IsStoreAcctInfoHave(Guid sellerGuid, Guid storeGuid)
        {
            StoreAcctInfoCollection saic =
                (storeGuid == Guid.Empty)
                    ? DB.Select().From(StoreAcctInfo.Schema)
                          .Where(StoreAcctInfo.SellerGuidColumn).IsEqualTo(sellerGuid)
                          .And(StoreAcctInfo.StoreGuidColumn).IsNull()
                          .ExecuteAsCollection<StoreAcctInfoCollection>()
                    : DB.Select().From(StoreAcctInfo.Schema)
                          .Where(StoreAcctInfo.SellerGuidColumn).IsEqualTo(sellerGuid)
                          .And(StoreAcctInfo.StoreGuidColumn).IsEqualTo(storeGuid)
                          .ExecuteAsCollection<StoreAcctInfoCollection>();
            return (saic.Count > 0) ? true : false;
        }

        public bool StoreAcctSet(StoreAcctInfo sai)
        {
            DB.Save(sai);
            return true;
        }
        public bool StoreAcctLogSet(StoreAcctInfoLog sail)
        {
            DB.Save(sail);
            return true;
        }

        public StoreAcctInfoCollection StoreAcctInfoGetAllList()
        {
            return DB.SelectAllColumnsFrom<StoreAcctInfo>().ExecuteAsCollection<StoreAcctInfoCollection>();
        }

        public string GetStoreAcctCodeBySeller(Guid sellerGuid)
        {
            StoreAcctInfoCollection saic =
                DB.Select().From(StoreAcctInfo.Schema).Where(StoreAcctInfo.SellerGuidColumn)
                    .IsEqualTo(sellerGuid).And(StoreAcctInfo.StoreGuidColumn).IsNull().ExecuteAsCollection<StoreAcctInfoCollection>();
            return (saic.Count > 0) ? saic[0].AcctCode : string.Empty;
        }

        public int GetStoreAcctNoLastestBySeller(Guid sellerGuid)
        {
            return
                DB.Select(StoreAcctInfo.AcctNoColumn.ToString()).From(StoreAcctInfo.Schema).Where(
                    StoreAcctInfo.SellerGuidColumn)
                    .IsEqualTo(sellerGuid).And(StoreAcctInfo.SellerGuidColumn).IsNotNull().OrderDesc(
                        StoreAcctInfo.AcctNoColumn.ToString()).ExecuteScalar<int>();
        }

        public StoreAcctInfo GetStoreAcctByGuids(Guid sellerGuid, Guid storeGuid)
        {
            StoreAcctInfoCollection saic = DB.Select().From(StoreAcctInfo.Schema)
                .Where(StoreAcctInfo.StoreGuidColumn).IsEqualTo(sellerGuid)
                .And(StoreAcctInfo.StoreGuidColumn).IsEqualTo(storeGuid)
                .ExecuteAsCollection<StoreAcctInfoCollection>();
            return (saic.Count > 0) ? saic[0] : new StoreAcctInfo();
        }

        public SellerCollection GetSellerAll()
        {
            return DB.SelectAllColumnsFrom<Seller>().ExecuteAsCollection<SellerCollection>();
        }

        public SellerCollection GetSellerByGuid(Guid guid)
        {
            return DB.SelectAllColumnsFrom<Seller>().Where(Seller.GuidColumn).IsEqualTo(guid).ExecuteAsCollection<SellerCollection>();
        }

        public StoreCollection GetStoreAll()
        {
            return DB.SelectAllColumnsFrom<Store>().ExecuteAsCollection<StoreCollection>();
        }

        #endregion

        #region ViewCouponStore
        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid sellerid, Guid storeid)
        {
            ViewCouponStoreCollection data = new ViewCouponStoreCollection();
            Query qry = ViewCouponStore.Query().AddWhere(ViewCouponStore.Columns.Bid, bid).AddWhere(ViewCouponStore.Columns.SellerGuid, sellerid);
            if (storeid != Guid.Empty)
                qry.AddWhere(ViewCouponStore.Columns.StoreGuid, storeid);
            QueryCommand qc = qry.BuildSelectCommand();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid bid, Guid sellerid, Guid storeid, string sequencenumber, string code)
        {
            ViewCouponStoreCollection data = new ViewCouponStoreCollection();
            Query qry = ViewCouponStore.Query().AddWhere(ViewCouponStore.Columns.Bid, bid)
                .AddWhere(ViewCouponStore.Columns.SellerGuid, sellerid)
                .AddWhere(ViewCouponStore.Columns.CouponSequenceNumber, sequencenumber)
                .AddWhere(ViewCouponStore.Columns.Code, code);
            if (storeid != Guid.Empty)
                qry.AddWhere(ViewCouponStore.Columns.StoreGuid, storeid);
            QueryCommand qc = qry.BuildSelectCommand();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;

        }
        public ViewCouponStoreCollection ViewCouponStoreCollectionGet(Guid sellerid, Guid storeid, string sequencenumber, string code)
        {
            ViewCouponStoreCollection data = new ViewCouponStoreCollection();
            Query qry = ViewCouponStore.Query()
                .AddWhere(ViewCouponStore.Columns.SellerGuid, sellerid)
                .AddWhere(ViewCouponStore.Columns.CouponSequenceNumber, sequencenumber)
                .AddWhere(ViewCouponStore.Columns.Code, code);
            if (storeid != Guid.Empty)
                qry.AddWhere(ViewCouponStore.Columns.StoreGuid, storeid);
            QueryCommand qc = qry.BuildSelectCommand();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;

        }
        public PponStoreCollection PponStoreGetListByStoreGuid(Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<PponStore>()
                .Where(PponStore.Columns.StoreGuid).IsEqualTo(storeGuid).ExecuteAsCollection<PponStoreCollection>();
        }

        #endregion

        public DealLabelCollection GetDealLabelCollection()
        {
            return DB.SelectAllColumnsFrom<DealLabel>().ExecuteAsCollection<DealLabelCollection>();
        }

        #region VbsUserManualCategory

        public VbsUserManualCategoryCollection VbsUserManualCategoryGetEnabledList()
        {
            return DB.SelectAllColumnsFrom<VbsUserManualCategory>()
                .Where(VbsUserManualCategory.Columns.IsDisabled).IsEqualTo(false)
                .OrderAsc(VbsUserManualCategory.Columns.Seq)
                .ExecuteAsCollection<VbsUserManualCategoryCollection>();
        }

        #endregion VbsUserManualCategory

        #region VbsUserManualFile

        public VbsUserManualFileCollection VbsUserManualFileGetEnabledList()
        {
            return DB.SelectAllColumnsFrom<VbsUserManualFile>()
                .Where(VbsUserManualFile.Columns.IsDisabled).IsEqualTo(false)
                .OrderAsc(VbsUserManualFile.Columns.CategoryId, VbsUserManualFile.Columns.Seq)
                .ExecuteAsCollection<VbsUserManualFileCollection>();
        }

        #endregion VbsUserManualFile

        #region view_seller_deal

        public ViewSellerDealCollection ViewSellerDealGetList(string searchItem, string searchValue, bool showLkSeller,
            int pageStart, int pageLength, out int totalCount)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewSellerDeal>();
            if (searchItem == ViewSellerDeal.Columns.SellerName)
            {
                query.Where(ViewSellerDeal.Columns.SellerName).Like("%" + searchValue + "%");
            }
            else if (searchItem == ViewSellerDeal.Columns.SellerId)
            {
                query.Where(ViewSellerDeal.Columns.SellerId).IsEqualTo(searchValue);
            }
            else if (searchItem == ViewSellerDeal.Columns.DealName)
            {
                query.Where(ViewSellerDeal.Columns.DealName).Like("%" + searchValue + "%");
            }
            else if (searchItem == ViewSellerDeal.Columns.DealId)
            {
                query.Where(ViewSellerDeal.Columns.DealId).IsEqualTo(searchValue);
            }
            else if (searchItem == ViewSellerDeal.Columns.CompanyName)
            {
                query.Where(ViewSellerDeal.Columns.CompanyName).Like("%" + searchValue + "%");
            }
            else if (searchItem == ViewSellerDeal.Columns.SignCompanyID)
            {
                query.Where(ViewSellerDeal.Columns.SignCompanyID).IsEqualTo(searchValue);
            }
            if (showLkSeller == false)
            {
                if (query.HasWhere)
                {
                    query.And(ViewSellerDeal.Columns.Department).IsNotEqualTo(0);
                }
                else
                {
                    query.Where(ViewSellerDeal.Columns.Department).IsNotEqualTo(0);
                }
            }
            totalCount = query.GetRecordCount();

            query.OrderBys.Add(ViewSellerDeal.Columns.DealStartTime + " desc");
            return query.ExecuteAsCollection<ViewSellerDealCollection>();
        }

        #endregion

        #region view_vbs_Member_Detail

        public ViewVbsMemberDetailCollection ViewVbsMemberDetailGet(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsMemberDetail>().Where(ViewVbsMemberDetail.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<ViewVbsMemberDetailCollection>();
        }

        #endregion

        #region vbs_company_detail

        public bool VbsCompanyDetailSet(VbsCompanyDetail vcd)
        {
            return DB.Save(vcd) > 0;
        }

        public VbsCompanyDetail VbsCompanyDetailGet(Guid sellerGuid)
        {
            return DB.Get<VbsCompanyDetail>(VbsCompanyDetail.Columns.SellerGuid, sellerGuid);
        }
        #endregion

        #region view_vbs_seller_member

        /// <summary>
        /// 取得會員數
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public int ViewVbsSellerMemberCount(int sellerUserId, SellerMemberType type)
        {
            var sql = @"select count(1) from view_vbs_seller_member where seller_user_id=@sellerUserId";
            switch (type)
            {
                case SellerMemberType.SiteMember:
                    sql += " and user_id is not null";
                    break;
                case SellerMemberType.SellerMember:
                    sql += " and seller_member_id is not null";
                    break;
            }

            var qc = new QueryCommand(sql, ViewVbsSellerMember.Schema.Provider.Name);
            qc.AddParameter("@sellerUserId", sellerUserId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 取得會員數(有篩選條件)
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public int ViewVbsSellerMemberCount(int sellerUserId, SellerMemberType type, string criteria, params string[] filter)
        {
            List<string> filterList = filter.ToList();
            filterList.Insert(0, ViewVbsSellerMember.Columns.SellerUserId + " = " + sellerUserId);
            QueryCommand qc = SSHelper.GetWhereQC<ViewVbsSellerMember>(filterList.ToArray());

            var sql = @"select count(1) from view_vbs_seller_member " + qc.CommandSql;
            switch (type)
            {
                case SellerMemberType.SiteMember:
                    sql += " and user_id is not null";
                    break;
                case SellerMemberType.SellerMember:
                    sql += " and seller_member_id is not null";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(criteria))
            {
                sql += " and (card_no = @criteria Or (IsNull(seller_member_last_name,'') + IsNull(seller_member_first_name, '')) like @criteria + '%')";
                qc.AddParameter("@criteria", criteria, DbType.String);
            }

            qc.CommandSql = sql;
            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 取回商家會員資料
        /// </summary>
        /// <param name="sellerUserId"></param>
        /// <param name="type"></param>
        /// <param name="criteria">查詢條件;string;針對姓名或會員卡編號進行查詢，若不查詢回傳空字串</param>
        /// <param name="startIndex">資料起始數 (1起始)</param>
        /// <param name="size">起始數往後捉幾筆</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewVbsSellerMemberCollection ViewVbsSellerMemberGet(int sellerUserId, SellerMemberType type, string criteria, int startIndex, int size, params string[] filter)
        {
            if (startIndex == 0) startIndex++;

            List<string> filterList = filter.ToList();
            filterList.Insert(0, ViewVbsSellerMember.Columns.SellerUserId + " = " + sellerUserId);

            QueryCommand qc = SSHelper.GetWhereQC<ViewVbsSellerMember>(filterList.ToArray());
            qc.CommandSql = "SELECT * FROM " + ViewVbsSellerMember.Schema.Provider.DelimitDbName(ViewVbsSellerMember.Schema.TableName) + qc.CommandSql;
            switch (type)
            {
                case SellerMemberType.SiteMember:
                    qc.CommandSql += " and user_id is not null";
                    break;
                case SellerMemberType.SellerMember:
                    qc.CommandSql += " and seller_member_id is not null";
                    break;
            }

            if (!string.IsNullOrWhiteSpace(criteria))
            {
                qc.CommandSql +=
                    " and (card_no = @criteria Or (IsNull(seller_member_last_name,'') + IsNull(seller_member_first_name, '')) like @criteria + '%')";
            }

            qc.CommandSql = string.Format(@"select * from (
                select ROW_NUMBER() OVER (ORDER BY user_membership_card_id) AS row_num, mem.* 
                    from ( {0} )mem
                )rowData", qc.CommandSql);

            if (size > 0)
            {
                qc.CommandSql += " where row_num between @start and @end";
            }
            else
            {
                qc.CommandSql += " where row_num > @start";
            }

            if (!string.IsNullOrEmpty(criteria))
            {
                qc.AddParameter("@criteria", criteria, DbType.String);
            }
            qc.AddParameter("@start", startIndex, DbType.Int32);
            if (size > 0)
            {
                qc.AddParameter("@end", startIndex + size - 1, DbType.Int32);
            }

            var col = new ViewVbsSellerMemberCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public ViewVbsSellerMember ViewVbsSellerMemberGet(int? userId, int? sellerMemberId, int sellerUserId)
        {
            if (userId == null && sellerMemberId == null) return new ViewVbsSellerMember();

            var sql = @"select * from view_vbs_seller_member where user_id = @userId AND seller_user_id = @sellerUserId";
            if (userId == null)
            {
                sql = @"select * from view_vbs_seller_member where seller_member_id = @sellerMemberId AND seller_user_id = @sellerUserId";
            }

            var qc = new QueryCommand(sql, ViewVbsSellerMember.Schema.Provider.Name);
            qc.AddParameter("@sellerUserId", sellerUserId, DbType.Int32);
            if (userId != null)
            {
                qc.AddParameter("@userId", userId, DbType.Int32);
            }
            else
            {
                qc.AddParameter("@sellerMemberId", sellerMemberId, DbType.Int32);
            }

            var member = new ViewVbsSellerMember();
            member.LoadAndCloseReader(DataService.GetReader(qc));
            return member;
        }

        public ViewVbsSellerMember ViewVbsSellerMemberGet(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsSellerMember>()
               .Where(ViewVbsSellerMember.Columns.UserId).IsEqualTo(userId)
               .And(ViewVbsSellerMember.Columns.CardGroupId).IsEqualTo(cardGroupId)
               .ExecuteSingle<ViewVbsSellerMember>();
        }

        #endregion

        #region view_vbs_seller_member_consumption

        public int ViewVbsSellerMemberConsumptionCollectionGetCount(int cardGroupId, string search)
        {
            int mobileNumber = 0;
            bool isMobile = int.TryParse(search, out mobileNumber);

            string sql = @"select count(1) from view_vbs_seller_member_consumption with(nolock) where card_group_id = @cardGroupId ";
            if (!string.IsNullOrEmpty(search))
            {
                if (Regex.IsMatch(search, @"^[\u4e00-\u9fa5_a-zA-Z0-9]+$"))
                {
                    sql += "and (";
                    sql += "(" + ViewVbsSellerMemberConsumption.Columns.SellerMemberFirstName + "+" + ViewVbsSellerMemberConsumption.Columns.SellerMemberLastName + ") like N'%" + search + "%' ";
                    sql += "or (" + ViewVbsSellerMemberConsumption.Columns.SellerMemberLastName + "+" + ViewVbsSellerMemberConsumption.Columns.SellerMemberFirstName + ") like N'%" + search + "%' ";
                    if (isMobile)
                        sql += "or " + ViewVbsSellerMemberConsumption.Columns.SellerMemberMobile + " = @mobileNumber ";
                    sql += " )";
                }
            }

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);

            if (isMobile)
                qc.AddParameter("@mobileNumber", mobileNumber, DbType.Int32);

            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewVbsSellerMemberConsumptionCollection ViewVbsSellerMemberConsumptionCollectionGet(int cardGroupId, string search, int page, int pageSize, int OrderByField)
        {
            int mobileNumber = 0;
            bool isMobile = int.TryParse(search, out mobileNumber);

            string sql = @"select * from view_vbs_seller_member_consumption with(nolock) where card_group_id = @cardGroupId ";
            if (!string.IsNullOrEmpty(search))
            {
                if (Regex.IsMatch(search, @"^[\u4e00-\u9fa5_a-zA-Z0-9]+$"))
                {
                    sql += "and (";
                    sql += "(" + ViewVbsSellerMemberConsumption.Columns.SellerMemberFirstName + "+"+ ViewVbsSellerMemberConsumption.Columns.SellerMemberLastName+") like N'%" + search + "%' ";
                    sql += "or (" + ViewVbsSellerMemberConsumption.Columns.SellerMemberLastName + "+" + ViewVbsSellerMemberConsumption.Columns.SellerMemberFirstName + ") like N'%" + search + "%' ";
                    if (isMobile)
                        sql += "or " + ViewVbsSellerMemberConsumption.Columns.SellerMemberMobile + " = @mobileNumber ";
                    sql += " )";
                }
            }

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);
            if (isMobile)
                qc.AddParameter("@mobileNumber", mobileNumber, DbType.Int32);

            #region 處理排序
            switch (OrderByField)
            {
                case (int)MembersOrderByField.AmountOfConsumptionDesc:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.TotalConsumption +" desc");
                    break;
                case (int)MembersOrderByField.CumulativePoints:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.TotalPoints + " desc");
                    break;
                case (int)MembersOrderByField.DistanceDesc:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.LastUseTime + " desc");
                    break;
                case (int)MembersOrderByField.NumberOfCupsDesc:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.TotalDepositCoups + " desc");
                    break;
                case (int)MembersOrderByField.UsageCountDesc:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.TotalOrderCount + " desc");
                    break;
                default:
                    qc = SSHelper.MakePagable(qc, page, pageSize, ViewVbsSellerMemberConsumption.Columns.LastUseTime + " desc");
                    break;
            }
            #endregion
            
            ViewVbsSellerMemberConsumptionCollection data = new ViewVbsSellerMemberConsumptionCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewVbsSellerMemberConsumption ViewVbsSellerMemberConsumptionGet(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsSellerMemberConsumption>()
               .Where(ViewVbsSellerMemberConsumption.Columns.UserId).IsEqualTo(userId)
               .And(ViewVbsSellerMemberConsumption.Columns.CardGroupId).IsEqualTo(cardGroupId)
               .ExecuteSingle<ViewVbsSellerMemberConsumption>();
        }

        #endregion

        #region vbs

        public List<int> VbsRelationshipGet(int userId)
        {
            var result = new List<int>();

            var sql = @"
SELECT DISTINCT VM.user_id FROM vbs_membership AS VM
JOIN resource_acl AS RA ON RA.account_id = VM.account_id
WHERE RA.resource_guid IN (

SELECT ST.Guid FROM vbs_membership AS VM
JOIN resource_acl AS RA ON RA.account_id = VM.account_id
JOIN seller AS ST ON ST.Guid=RA.resource_guid AND ST.store_status=0
WHERE ra.resource_type = 2 AND VM.user_id=@userId
UNION ALL
SELECT STE.parent_seller_guid as seller_guid FROM vbs_membership AS VM
JOIN resource_acl AS RA ON RA.account_id = VM.account_id
JOIN seller AS ST ON ST.Guid=RA.resource_guid AND ST.store_status=0
JOIN seller_tree AS STE ON STE.seller_guid = ST.GUID 
WHERE ra.resource_type = 2 AND VM.user_id=@userId
UNION ALL
SELECT ST.Guid FROM vbs_membership AS VM
JOIN resource_acl AS RA ON RA.account_id = VM.account_id
JOIN seller_tree AS STE ON STE.parent_seller_guid=RA.resource_guid 
JOIN seller AS ST ON ST.GUID=STE.seller_guid AND ST.store_status=0
WHERE ra.resource_type = 1 AND VM.user_id=@userId
UNION ALL
SELECT ra.resource_guid as seller_guid FROM vbs_membership AS VM
JOIN resource_acl AS RA ON RA.account_id = VM.account_id
WHERE ra.resource_type = 1 AND VM.user_id=@userId

)

";

            IDataReader idr = new InlineQuery().ExecuteReader(sql, userId);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            foreach (var row in dt.AsEnumerable())
            {
                result.Add(Convert.ToInt32(row["user_id"]));
            }

            return result;
        }

        //        public StoreCollection VbsSellerGuidGet(int userId)
        //        {

        //            var sql = @"
        //SELECT s.* FROM store s
        //WHERE s.status=0
        //AND s.Guid IN (
        //	SELECT RA.resource_guid FROM vbs_membership AS VM
        //	JOIN resource_acl AS RA ON RA.account_id = VM.account_id
        //	WHERE vm.user_id=@userId
        //)
        //UNION ALL
        //SELECT s.* FROM store s
        //WHERE s.status=0
        //AND s.seller_guid IN (
        //	SELECT RA.resource_guid FROM vbs_membership AS VM
        //	JOIN resource_acl AS RA ON RA.account_id = VM.account_id
        //	WHERE vm.user_id=@userId
        //)
        //";
        //            var result = new StoreCollection();
        //            QueryCommand qc = new QueryCommand(sql, ViewMembershipCardStore.Schema.Provider.Name);
        //            qc.AddParameter("@userId", userId, DbType.Int32);
        //            result.LoadAndCloseReader(DataService.GetReader(qc));
        //            return result;
        //        }
        public SellerCollection VbsSellerGuidGet(int userId)
        {

            var sql = @"
SELECT s.* FROM seller s
WHERE s.store_status=0
AND s.guid IN (
	SELECT RA.resource_guid FROM vbs_membership AS VM
	JOIN resource_acl AS RA ON RA.account_id = VM.account_id
	WHERE vm.user_id=@userId
)
";
            var result = new SellerCollection();
            QueryCommand qc = new QueryCommand(sql, ViewMembershipCardStore.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion


        #region vbsISP

        public VbsInstorePickupCollection VbsInstorePickupGet(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<VbsInstorePickup>().NoLock()
                .Where(VbsInstorePickup.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<VbsInstorePickupCollection>();
        }

        public VbsInstorePickup VbsInstorePickupGet(Guid sellerId, int channel)
        {
            return DB.SelectAllColumnsFrom<VbsInstorePickup>().NoLock()
                .Where(VbsInstorePickup.Columns.SellerGuid).IsEqualTo(sellerId)
                .And(VbsInstorePickup.Columns.ServiceChannel).IsEqualTo(channel)
                .ExecuteSingle<VbsInstorePickup>();
        }

        public VbsInstorePickupCollection GetISPStoreStauts()
        {
            return DB.SelectAllColumnsFrom<VbsInstorePickup>().NoLock()
                .Where(VbsInstorePickup.Columns.Status).IsEqualTo((int)ISPStatus.StoreCreate)
                .Or(VbsInstorePickup.Columns.Status).IsEqualTo((int)ISPStatus.Checking)
                .Or(VbsInstorePickup.Columns.Status).IsEqualTo((int)ISPStatus.Failed)
                .ExecuteAsCollection<VbsInstorePickupCollection>();
        }

        public void VbsInstorePickupSet(VbsInstorePickup vip)
        {
            DB.Save(vip);
        }

        public int VbsInstorePickupSetList(VbsInstorePickupCollection vipc)
        {
            return DB.SaveAll(vipc);
        }

        public ViewVbsInstorePickupCollection ViewVbsInstorePickupCollectionGet(Guid sellerId)
        {
            return DB.SelectAllColumnsFrom<ViewVbsInstorePickup>().NoLock()
                .Where(ViewVbsInstorePickup.Columns.SellerGuid).IsEqualTo(sellerId)
                .ExecuteAsCollection<ViewVbsInstorePickupCollection>();
        }


        public ViewVbsInstorePickupCollection ViewVbsInstorePickupCollectionGet(int pageStart, int pageLength, params string[] filter)
        {
            string defOrderBy = ViewVbsInstorePickup.Columns.CreateTime + " desc";

            QueryCommand qc = GetDCWhere(ViewVbsInstorePickup.Schema, filter);
            qc.CommandSql = "select * from " + ViewVbsInstorePickup.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            ViewVbsInstorePickupCollection col = new ViewVbsInstorePickupCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public int ViewVbsInstorePickupCollectionGetCount(params string[] filter)
        {
            int rowCnt = 0;
            try
            {
                QueryCommand qc = GetDCWhere(ViewVbsInstorePickup.Schema, filter);
                qc.CommandSql = "select count(*) from " + ViewVbsInstorePickup.Schema.TableName + " with(nolock) " + qc.CommandSql;
                rowCnt = (int)DataService.ExecuteScalar(qc);
            }
            catch (Exception ex)
            {
                
            }
            return rowCnt;
        }

        public ViewVbsInstorePickup ViewVbsInstorePickupGet(Guid sellerId, int channel)
        {
            return DB.SelectAllColumnsFrom<ViewVbsInstorePickup>().NoLock()
                .Where(ViewVbsInstorePickup.Columns.SellerGuid).IsEqualTo(sellerId)
                .And(ViewVbsInstorePickup.Columns.ServiceChannel).IsEqualTo(channel)
                .ExecuteSingle<ViewVbsInstorePickup>();
        }

        #endregion vbsISP

        #region proposal

        public Proposal ProposalGet(int id)
        {
            return DB.SelectAllColumnsFrom<Proposal>()
                .Where(Proposal.Columns.Id).IsEqualTo(id).ExecuteSingle<Proposal>();
        }
        public Proposal ProposalGet(Guid bid)
        {
            return DB.SelectAllColumnsFrom<Proposal>()
                .Where(Proposal.Columns.BusinessHourGuid).IsEqualTo(bid).ExecuteSingle<Proposal>();
        }

        public void ProposalSet(Proposal Proposal)
        {
            DB.Save<Proposal>(Proposal);
        }

        public Proposal ProposalGet(string column, object value)
        {
            return DB.Get<Proposal>(column, value);
        }
        public ProposalCollection ProposalGetListByHouse()
        {
            return DB.SelectAllColumnsFrom<Proposal>()
                .Where(Proposal.Columns.ProposalSourceType).IsEqualTo((int)ProposalSourceType.House)
                .OrderAsc(Proposal.Columns.Id)
                .ExecuteAsCollection<ProposalCollection>();
        }
        public ProposalCollection ProposalGetByBids(IEnumerable<Guid> bids)
        {
            ProposalCollection pros = new ProposalCollection();
            List<Guid> tempIds = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<Proposal>()
                        .Where(Proposal.Columns.BusinessHourGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ProposalCollection>();

                pros.AddRange(batchProd);

                idx += batchLimit;
            }

            return pros;
        }


        public ProposalCollection ProposalGetByOrderTimeS()
        {
            return DB.SelectAllColumnsFrom<Proposal>()
                .Where(Proposal.Columns.OrderTimeS).IsNotEqualTo(DateTime.MaxValue.ToString("yyyy/MM/dd"))
                .And(Proposal.Columns.OrderTimeS).IsGreaterThanOrEqualTo(DateTime.Now.ToString("yyyy/MM/dd"))
                .OrderAsc(Proposal.Columns.Id)
                .ExecuteAsCollection<ProposalCollection>();
        }

        public ProposalCollection ProposalGetListBySellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<Proposal>().Where(Proposal.Columns.SellerGuid).IsEqualTo(sellerGuid).ExecuteAsCollection<ProposalCollection>();
        }
        public ProposalCollection ProposalGetListByProduction()
        {
            return DB.SelectAllColumnsFrom<Proposal>().Where(Proposal.Columns.IsProduction)
                .IsEqualTo(true).ExecuteAsCollection<ProposalCollection>();
        }

        public ProposalCollection ProposalGetListByRestriction(int dealType2)
        {
            return DB.SelectAllColumnsFrom<Proposal>().NoLock()
                .Where(Proposal.Columns.ProposalSourceType).IsEqualTo((int)ProposalSourceType.House)
                .And(Proposal.Columns.BusinessFlag).IsEqualTo((int)ProposalBusinessFlag.Initial)
                .And(Proposal.Columns.ApproveFlag).IsEqualTo((int)ProposalApproveFlag.QCcheck)
                .And(Proposal.Columns.IsProduction).IsEqualTo(false)
                .And(Proposal.Columns.DealType2).IsEqualTo(dealType2)
                .ExecuteAsCollection<ProposalCollection>();
        }

        public bool ProposalDelete(int id)
        {
            DB.Destroy<Proposal>(Proposal.Columns.Id, id);
            return true;
        }

        public ProposalContractFile ProposalContractFileGetByGuid(Guid guid)
        {
            return DB.Get<ProposalContractFile>(ProposalContractFile.Columns.Guid, guid);
        }

        public ProposalContractFileCollection ProposalContractFileGetListBypid(int ProposalId)
        {
            return DB.SelectAllColumnsFrom<ProposalContractFile>()
                .Where(ProposalContractFile.Columns.ProposalId).IsEqualTo(ProposalId)
                .And(ProposalContractFile.Columns.Status).IsNotEqualTo(-1)
                .OrderDesc(ProposalContractFile.Columns.CreateTime)
                .ExecuteAsCollection<ProposalContractFileCollection>();
        }

        public bool ProposalContractFileSet(ProposalContractFile s)
        {
            DB.Save<ProposalContractFile>(s);
            return true;
        }

        public ProposalContractFileCollection ProposalContractFileGetByType(int proposal_id, int Type)
        {
            return DB.SelectAllColumnsFrom<ProposalContractFile>()
                .Where(ProposalContractFile.Columns.ProposalId).IsEqualTo(proposal_id)
                .And(ProposalContractFile.Columns.ContractType).IsEqualTo((int)Type)
                .And(ProposalContractFile.Columns.Status).IsNotEqualTo(-1)
                .ExecuteAsCollection<ProposalContractFileCollection>();
        }

        public SellerContractFileCollection SellerContractFileGetByType(Guid seller_guid, int Type)
        {
            return DB.SelectAllColumnsFrom<SellerContractFile>()
                .Where(SellerContractFile.Columns.SellerGuid).IsEqualTo(seller_guid)
                .And(SellerContractFile.Columns.ContractType).IsEqualTo((int)Type)
                .And(SellerContractFile.Columns.Status).IsNotEqualTo(-1)
                .ExecuteAsCollection<SellerContractFileCollection>();

        }

        public ProposalCollection ProposalGetPhotographer()
        {
            string strSQL = " select * from " + Proposal.Schema.Provider.DelimitDbName(Proposal.Schema.TableName) + " with(nolock) " +
                            " where " + Proposal.Columns.SpecialAppointFlagText + " is not null and " + Proposal.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " = 0 ";

            QueryCommand qc = new QueryCommand(strSQL, ViewProposalSeller.Schema.Provider.Name);


            ProposalCollection vfcCol = new ProposalCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;

        }

        public List<int> ProposalMultiDealGetByBrandName(string brandName)
        {
            return DB.Select(Proposal.Columns.Id)
                .From<Proposal>().NoLock()
                .Where(Proposal.Columns.BrandName).Like("%" + brandName + "%")
                .Or(Proposal.Columns.DealName).Like("%" + brandName + "%")
                .ExecuteTypedList<int>();
        }

        #endregion


        #region view_proposal_seller

        public ViewProposalSeller ViewProposalSellerGet(string column, object value)
        {
            ViewProposalSeller view = new ViewProposalSeller();
            view.LoadAndCloseReader(ViewProposalSeller.Query().WHERE(column, value).ExecuteReader());
            return view;
        }


        /// <summary>
        /// 進度條查詢
        /// </summary>
        /// <param name="ProposalApplyFlag">提案申請狀態</param>
        /// <param name="EmpUserId">登入者userId</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <returns></returns>
        public ViewProposalSellerCollection ViewProposalSellerGetListByPeriod(int ProposalApplyFlag, int EmpUserId, string CrossDeptTeam, int month)
        {
            string sql = @" SELECT * from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) + @" with(nolock) where " +
                         ViewProposalSeller.Columns.ApplyFlag + @" = @ProposalApplyFlag and " + ViewProposalSeller.Columns.CreateTime + @" >= @dateEnd ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);

            qc.AddParameter("@ProposalApplyFlag", ProposalApplyFlag, DbType.Int32);
            qc.AddParameter("@dateEnd", DateTime.Now.AddMonths(0- month), DbType.DateTime);

            #region EmpUserId
            if (EmpUserId != 0)
            {
                //僅瀏覽權限
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += "(" + ViewProposalSeller.Columns.DevelopeSalesId + " = @EmpUserId or " + ViewProposalSeller.Columns.OperationSalesId + " = @EmpUserId)";
                qc.AddParameter("@EmpUserId", EmpUserId, DbType.Int32);
            }
            else if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                //跨區
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                string tmpsql = "";
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        //組
                        tmpsql += "(" + ViewProposalSeller.Columns.DevelopeDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewProposalSeller.Columns.DevelopeTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                        tmpsql += "(" + ViewProposalSeller.Columns.OperationDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewProposalSeller.Columns.OperationTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                    }
                    else
                    {
                        //區
                        tmpsql += " (" + ViewProposalSeller.Columns.DevelopeDeptId + "='" + c + "') or ";
                        tmpsql += " (" + ViewProposalSeller.Columns.OperationDeptId + "='" + c + "') or ";
                    }


                }
                tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                qc.CommandSql += tmpsql;


            }

            #endregion

            var vpolCol = new ViewProposalSellerCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }


        /// <summary>
        /// 提案單管理查詢
        /// </summary>
        /// <param name="pageStart">起始頁</param>
        /// <param name="pageLength">每頁幾筆</param>
        /// <param name="orderBy">排序</param>
        /// <param name="status">提案單狀態</param>
        /// <param name="flag">提案單狀態flag</param>
        /// <param name="specialFlag">特殊flag</param>
        /// <param name="showChecked">是否完成項目</param>
        /// <param name="SalesId">業務</param>
        /// <param name="StatusFlag">進階搜尋</param>
        /// <param name="DeptId">單位</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <param name="PhotographerFlag">約拍狀態</param>
        /// <param name="PhotoType">照片來源</param>
        /// <param name="EmpUserId">登入者userId</param>
        /// <param name="sSQL"></param>
        /// <param name="filter">其他篩選</param>
        /// <returns></returns>
        public ViewProposalSellerCollection ViewProposalSellerGetList(int pageStart, int pageLength, string orderBy, ProposalStatus status,
            int flag, int specialFlag, bool showChecked, int? SalesId, string StatusFlag, string DeptId,
            string CrossDeptTeam, int PhotographerFlag, int PhotoType,int EmpUserId, string dealStatus, List<int> pids, System.Text.StringBuilder sSQL, params string[] filter)
        {
            try
            {
                string defOrderBy = ViewProposalSeller.Columns.CreateTime + " desc";
                QueryCommand qc = GetDCWhere(ViewProposalSeller.Schema, filter);
                qc.CommandSql = "select * from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) + " with(nolock) " + qc.CommandSql;

                qc.CommandSql = ViewProposalSellerGetData(status, flag, specialFlag, showChecked, SalesId, StatusFlag, DeptId, CrossDeptTeam, PhotographerFlag, PhotoType, EmpUserId, dealStatus, pids, qc);

                if (!string.IsNullOrEmpty(orderBy))
                {
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                    defOrderBy = orderBy;
                }
                if (pageLength > 0)
                    qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

                sSQL.AppendLine(qc.CommandSql);
                sSQL.AppendLine(new JsonSerializer().Serialize(qc.Parameters));

                ViewProposalSellerCollection vfcCol = new ViewProposalSellerCollection();
                vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
                return vfcCol;
            }
            catch
            {
                try
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("【pageStart】=" + pageStart);
                    builder.AppendLine("【pageLength】=" + pageLength);
                    builder.AppendLine("【orderBy】=" + orderBy);
                    builder.AppendLine("【status】=" + status);
                    builder.AppendLine("【flag】=" + flag);
                    builder.AppendLine("【specialFlag】=" + specialFlag);
                    builder.AppendLine("【showChecked】=" + showChecked);
                    builder.AppendLine("【SalesEmail】=" + SalesId);
                    builder.AppendLine("【StatusFlag】=" + StatusFlag);
                    builder.AppendLine("【DeptId】=" + DeptId);
                    builder.AppendLine("【CrossDeptTeam】=" + CrossDeptTeam);
                    builder.AppendLine("【PhotographerFlag】=" + PhotographerFlag);
                    logger.Warn("【ViewProposalSellerGetList Error】=" + builder);
                }
                catch { }

                throw;

            }
        }



        public int ViewProposalSellerGetCount(ProposalStatus status, int flag, int specialFlag,
            bool showChecked, int? SalesId, string StatusFlag, string DeptId, string CrossDeptTeam, int PhotographerFlag,
            int PhotoType, int EmpUserId, string dealStatus, List<int> pids, ref string sSQL, params string[] filter)
        {
            int rowCnt = 0;
            try
            {
                QueryCommand qc = GetDCWhere(ViewProposalSeller.Schema, filter);
                qc.CommandSql = "select count(*) from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) + " with(nolock) " + qc.CommandSql;
                qc.CommandSql = ViewProposalSellerGetData(status, flag, specialFlag, showChecked, SalesId, StatusFlag, DeptId, CrossDeptTeam, PhotographerFlag, PhotoType, EmpUserId, dealStatus, pids, qc);

                sSQL = qc.CommandSql;
                rowCnt = (int)DataService.ExecuteScalar(qc);
            }
            catch
            {
                StringBuilder builder = new StringBuilder();
                builder.AppendLine("【status】=" + status);
                builder.AppendLine("【flag】=" + flag);
                builder.AppendLine("【specialFlag】=" + specialFlag);
                builder.AppendLine("【showChecked】=" + showChecked);
                builder.AppendLine("【SalesEmail】=" + SalesId);
                builder.AppendLine("【StatusFlag】=" + StatusFlag);
                builder.AppendLine("【showChecked】=" + showChecked);
                builder.AppendLine("【StatusFlag】=" + StatusFlag);
                builder.AppendLine("【DeptId】=" + DeptId);
                builder.AppendLine("【CrossDeptTeam】=" + CrossDeptTeam);
                builder.AppendLine("【PhotographerFlag】=" + PhotographerFlag);
                builder.AppendLine("【PhotoType】=" + PhotoType);
                logger.Warn("【ViewProposalSellerGetCount Error】=" + builder.ToString());

                throw;
            }
            return rowCnt;
        }

        private string ViewProposalSellerGetData(ProposalStatus status, int flag, int specialFlag, bool showChecked, int? SalesId, string StatusFlag, string DeptId, string CrossDeptTeam, int PhotographerFlag, int PhotoType, int EmpUserId, string dealStatus, List<int> pids, QueryCommand qc)
        {
            try
            {
                #region flag
                if (flag != 0 && status != ProposalStatus.Initial)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }

                    #region switch flags

                    string flagCol = string.Empty;
                    string statusCol = ViewProposalSeller.Columns.Status;
                    switch (status)
                    {
                        case ProposalStatus.Initial:    //未申請
                            break;
                        case ProposalStatus.Apply:      //提案申請
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.ApplyFlag;
                            break;
                        case ProposalStatus.Approve:    //申請核准
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.ApproveFlag;
                            break;
                        case ProposalStatus.Created:    //建檔檢核
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.BusinessCreateFlag;
                            break;
                        case ProposalStatus.BusinessCheck:  //檔次檢核
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.BusinessFlag;
                            break;
                        case ProposalStatus.Editor:     //編輯檢核
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.EditFlag;
                            break;
                        case ProposalStatus.Listing:    //上架檢核
                            statusCol = statusCol + "=" + (int)status;
                            flagCol = ViewProposalSeller.Columns.ListingFlag;
                            break;
                        default:
                            break;
                    }

                    #endregion
                    if (!string.IsNullOrEmpty(flagCol))
                    {
                        if (showChecked)
                        {
                            //查已完成項目
                            qc.CommandSql += statusCol + " And (" + flagCol + " & @flag )>0";
                        }
                        else
                        {
                            //查尚未完成項目
                            qc.CommandSql += " (" + flagCol + " & @flag )=0";
                        }
                        qc.AddParameter("@flag", flag, DbType.Int32);
                    }

                }
                #endregion

                #region specialFlag
                if (specialFlag != 0)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }
                    if(specialFlag > 0)
                    {
                        qc.CommandSql += " (" + ViewProposalSeller.Columns.SpecialFlag + " & @special )>0";
                        qc.AddParameter("@special", specialFlag, DbType.Int32);
                    }
                    else if (specialFlag == -10)
                    {
                        //超取
                        qc.CommandSql += " " + ViewProposalSeller.Columns.Id + " IN (select pid from proposal_multi_deals where isnull(box_quantity_limit,0) > 0) ";
                    }
                    else if (specialFlag == -11)
                    {
                        //活動遊戲檔
                        qc.CommandSql += " " + ViewProposalSeller.Columns.IsGame + "=1";
                    }
                    else if (specialFlag == -12)
                    {
                        //一般宅配 (自出含快速出貨)
                        qc.CommandSql += " " + ViewProposalSeller.Columns.IsWms + "=0";
                    }
                    else if (specialFlag == -13)
                    {
                        //24H到貨 (倉儲+物流)
                        qc.CommandSql += " " + ViewProposalSeller.Columns.IsWms + "=1";
                    }

                }
                #endregion


                #region StatusFlag
                if (StatusFlag != string.Empty)
                {

                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }

                    string tmpsql = "";
                    for (int i = 0; i < StatusFlag.Split('|').Count(); i++)
                    {
                        if (StatusFlag.Split('|')[i].ToString() != string.Empty)
                        {
                            string strStatusFlag = StatusFlag.Split('|')[i];
                            int intStatus = Convert.ToInt32(strStatusFlag.Split(':')[0].ToString());
                            int intFlag = Convert.ToInt32(strStatusFlag.Split(':')[1].ToString());


                            switch ((ProposalStatus)intStatus)
                            {
                                case ProposalStatus.Initial:
                                    break;
                                case ProposalStatus.Apply:
                                    tmpsql += Proposal.Columns.ApplyFlag + " = " + "@ApplyFlag";
                                    qc.AddParameter("@ApplyFlag", intFlag, DbType.Int32);
                                    break;
                                case ProposalStatus.Approve:
                                    tmpsql += Proposal.Columns.ApproveFlag + " = " + "@ApproveFlag";
                                    qc.AddParameter("@ApproveFlag", intFlag, DbType.Int32);
                                    break;
                                case ProposalStatus.Created:
                                    tmpsql += Proposal.Columns.BusinessCreateFlag + " = " + "@BusinessCreateFlag";
                                    qc.AddParameter("@BusinessCreateFlag", intFlag, DbType.Int32);
                                    break;
                                case ProposalStatus.BusinessCheck:
                                    tmpsql += Proposal.Columns.BusinessFlag + " = " + "@BusinessFlag";
                                    qc.AddParameter("@BusinessFlag", intFlag, DbType.Int32);
                                    break;
                                case ProposalStatus.Editor:
                                    tmpsql += Proposal.Columns.EditFlag + " = " + "@EditFlag";
                                    qc.AddParameter("@EditFlag", intFlag, DbType.Int32);
                                    break;
                                case ProposalStatus.Listing:
                                    tmpsql += Proposal.Columns.ListingFlag + " = " + "@ListingFlag";
                                    qc.AddParameter("@ListingFlag", intFlag, DbType.Int32);
                                    break;
                                default:
                                    break;
                            }

                            tmpsql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                        }
                    }

                    tmpsql = "(" + tmpsql.Substring(0, tmpsql.Length - 7) + ")";

                    qc.CommandSql += tmpsql;

                }
                #endregion

                if(pids.Count > 0)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE + " 1=1 ";
                    }
                    qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                    qc.CommandSql += ViewProposalSeller.Columns.Id + " IN (" + string.Join(",", pids) + ")";
                }

                #region CrossDeptTeam
                if (EmpUserId != 0)
                {
                    //僅瀏覽權限
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }

                    qc.CommandSql += "(" + ViewProposalSeller.Columns.DevelopeSalesId + " = " + "@EmpUserId or " + ViewProposalSeller.Columns.OperationSalesId + " = " + "@EmpUserId)";
                    qc.AddParameter("@EmpUserId", EmpUserId, DbType.String);
                }
                else if (!string.IsNullOrEmpty(CrossDeptTeam))
                {
                    //跨區
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }


                    string tmpsql = "";
                    string salesuseridtmpsql2 = "";
                    string salesuseridtmpsql3 = "";
                    if (SalesId != null)
                    {
                        //多判斷搜尋業務是否於跨區內
                        salesuseridtmpsql2 = " and " + ViewProposalSeller.Columns.DevelopeSalesId + " = " + "@SalesId ";
                        salesuseridtmpsql3 = " and " + ViewProposalSeller.Columns.OperationSalesId + " = " + "@SalesId";
                        qc.AddParameter("@SalesId", SalesId, DbType.Int32);
                    }
                    foreach (var c in CrossDeptTeam.Split('/'))
                    {
                        if (c.Contains("["))
                        {
                            //組
                            tmpsql += "(" + ViewProposalSeller.Columns.DevelopeDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                            ViewProposalSeller.Columns.DevelopeTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql2 + " ) or ";
                            tmpsql += "(" + ViewProposalSeller.Columns.OperationDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                            ViewProposalSeller.Columns.OperationTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql3 + ") or ";
                        }
                        else
                        {
                            //區
                            tmpsql += " (" + ViewProposalSeller.Columns.DevelopeDeptId + "='" + c + "'" + salesuseridtmpsql2 + ") or ";
                            tmpsql += " (" + ViewProposalSeller.Columns.OperationDeptId + "='" + c + "'" + salesuseridtmpsql3 + ") or ";
                        }


                    }
                    tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                    qc.CommandSql += tmpsql;


                }
                else
                {
                    //全區有設定單位
                    if (!string.IsNullOrEmpty(DeptId))
                    {
                        if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                        {
                            qc.CommandSql += SqlFragment.WHERE;
                        }
                        else
                        {
                            int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                            qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                        }


                        qc.CommandSql += "(" + ViewProposalSeller.Columns.DevelopeDeptId + " = " + "@DeptId or " + ViewProposalSeller.Columns.OperationDeptId + " = @DeptId)";
                        qc.AddParameter("@DeptId", DeptId, DbType.String);
                    }


                    if (SalesId != null)
                    {
                        if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                        {
                            qc.CommandSql += SqlFragment.WHERE;
                        }
                        else
                        {
                            int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                            qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                        }

                        qc.CommandSql += "(" + ViewProposalSeller.Columns.DevelopeSalesId + " = " + "@SalesId or " + ViewProposalSeller.Columns.OperationSalesId + " = " + "@SalesId)";
                        qc.AddParameter("@SalesId", SalesId, DbType.Int32);
                    }


                }
                #endregion

                #region PhotographerFlag
                if (PhotographerFlag != 0)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }
                    if (PhotographerFlag == (int)ProposalPhotographer.PhotographerAppointClose)
                        qc.CommandSql += " (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " ) > 0";
                    else if (PhotographerFlag == (int)ProposalPhotographer.PhotographerAppointCancel)
                    {
                        qc.CommandSql += " (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCancel + " ) > 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " ) = 0";
                    }
                    else if (PhotographerFlag == (int)ProposalPhotographer.PhotographerAppointCheck)
                    {
                        qc.CommandSql += " (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCheck + " ) > 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCancel + " ) = 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " ) = 0";
                    }
                    else if (PhotographerFlag == (int)ProposalPhotographer.PhotographerAppoint)
                    {
                        qc.CommandSql += " (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppoint + " ) > 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCheck + " ) = 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCancel + " ) = 0";
                        qc.CommandSql += " and (" + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " ) = 0";
                    }
                }
                #endregion

                #region PhotoType
                if (PhotoType != 0)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }
                    qc.CommandSql += " (" + ViewProposalSeller.Columns.SpecialFlag + " & @phototype ) = @phototype";
                    qc.AddParameter("@phototype", PhotoType, DbType.Int32);
                }
                #endregion

                #region 增加判斷業務不可先查到商家提案單
                string tmpsql2 = "";
                if (!qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    tmpsql2 += SqlFragment.WHERE;
                }
                else
                {
                    tmpsql2 += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                }
                qc.CommandSql += tmpsql2 + "(" + Proposal.Columns.ProposalCreatedType + "=" + (int)ProposalCreatedType.Sales + SqlFragment.SPACE + SqlFragment.OR + "(" + Proposal.Columns.ProposalCreatedType + "=" + (int)ProposalCreatedType.Seller + SqlFragment.SPACE + SqlFragment.AND + Proposal.Columns.SellerProposalFlag + ">=" + (int)SellerProposalFlag.Send + "))";
                #endregion

                if(!string.IsNullOrEmpty(dealStatus) && dealStatus != "0")
                {
                    switch (dealStatus)
                    {
                        case "O":
                            //上架銷售
                        case "S":
                            //暫停銷暫
                            string clsSQL1 = @"select business_hour_guid
                                                from group_order  with(nolock)
                                                where slug is null
                                                and business_hour_guid in(
                                                    select distinct MainBusinessHourGuid
                                                    from combo_deals  with(nolock)
                                                    union
                                                    select bh.guid
                                                    from business_hour bh with(nolock)
                                                    where bh.business_hour_status & 1024 = 0 and bh.business_hour_status & 2048 = 0
                                                )";
                            qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                            qc.CommandSql += ViewProposalSeller.Columns.BusinessHourGuid + " IN (" + clsSQL1 + ")";


                            if (dealStatus == "O")
                            {
                                string slotSQL1 = @"select distinct business_hour_GUID
                                                    from deal_time_slot with(nolock)
                                                    where effective_start<=getdate() and effective_end>=getdate()
                                                    and status=0";
                                qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                                qc.CommandSql += ViewProposalSeller.Columns.BusinessHourGuid + " IN (" + slotSQL1 + ")";
                            }
                            else if (dealStatus == "S")
                            {
                                string slotSQL2 = @"select distinct business_hour_GUID
                                                    from deal_time_slot with(nolock)
                                                    where effective_start<=getdate() and effective_end>=getdate()
                                                    and status=1";
                                qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                                qc.CommandSql += ViewProposalSeller.Columns.BusinessHourGuid + " IN (" + slotSQL2 + ")";
                            }
                            qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                            qc.CommandSql += ViewProposalSeller.Columns.OrderTimeS + " IS NOT NULL ";
                            break;
                        case "C":
                            //已結檔
                            string clsSQL2 = @"select business_hour_guid
                                                from group_order
                                                where slug is not null
                                                and business_hour_guid in(
                                                    select distinct MainBusinessHourGuid
                                                    from combo_deals
                                                    union
                                                    select bh.guid
                                                    from business_hour bh
                                                    where bh.business_hour_status & 1024 = 0 and bh.business_hour_status & 2048 = 0
                                                )";
                            qc.CommandSql += SqlFragment.SPACE + SqlFragment.AND + SqlFragment.SPACE;
                            qc.CommandSql += ViewProposalSeller.Columns.BusinessHourGuid + " IN (" + clsSQL2 + ")";
                            break;
                    }
                }
            }
            catch
            {
                try
                {
                    StringBuilder builder = new StringBuilder();
                    builder.AppendLine("【status】=" + status);
                    builder.AppendLine("【flag】=" + flag);
                    builder.AppendLine("【specialFlag】=" + specialFlag);
                    builder.AppendLine("【showChecked】=" + showChecked);
                    builder.AppendLine("【SalesEmail】=" + SalesId);
                    builder.AppendLine("【StatusFlag】=" + StatusFlag);
                    builder.AppendLine("【DeptId】=" + DeptId);
                    builder.AppendLine("【CrossDeptTeam】=" + CrossDeptTeam);
                    builder.AppendLine("【PhotographerFlag】=" + PhotographerFlag);
                    logger.Warn("【ViewProposalSellerGetData Error】=" + builder);
                }
                catch { }

                throw;
            }

            return qc.CommandSql;
        }

        public ProposalCollection SellerProposalSellerGetList(List<Guid> sellerGuid, int pid, Guid? BusinessHourGuid, string BrandName, string Deals, string OrderTimeS, string OrderTimeE,
            string SalesName, string isWms)
        {
            string sids = string.Join("','", sellerGuid.ToArray());
            string sql = @" SELECT * from " + Proposal.Schema.Provider.DelimitDbName(Proposal.Schema.TableName) + @" with(nolock) ";
            sql += SqlFragment.WHERE + " " + Proposal.Columns.SellerGuid + " In ('" + sids + "')";

            if (pid != 0)
            {
                sql += " and " + Proposal.Columns.Id + "=@Pid";
            }
            if (BusinessHourGuid != null && BusinessHourGuid != Guid.Empty)
            {
                sql += " and " + Proposal.Columns.BusinessHourGuid + "=@BusinessHourGuid";
            }
            if (!string.IsNullOrEmpty(BrandName))
            {
                sql += " and (" + Proposal.Columns.BrandName + " like N'%' + @BrandName + '%' or " + Proposal.Columns.DealName + " like N'%' + @BrandName + '%' or " + 
                      Proposal.Columns.Id + " in (select " + ProposalCouponEventContent.Columns.ProposalId + " from " + ProposalCouponEventContent.Schema.Provider.DelimitDbName(ProposalCouponEventContent.Schema.TableName) + " where " + ProposalCouponEventContent.Columns.AppTitle + " like N'%' + @BrandName + '%'))";
            }
            if (!string.IsNullOrEmpty(Deals))
            {
                List<int> pids = ProposalMultiDealGetByItemName(Deals).Select(x => x.Pid).Distinct().ToList();
                string pidList = string.Join(",", pids);
                if (!string.IsNullOrEmpty(pidList))
                    sql += " and " + Proposal.Columns.Id + " In (" + pidList + ")";
                else
                    sql += " and 1 < 0";//代表沒有符合的
            }
            if (!string.IsNullOrEmpty(OrderTimeS))
            {
                sql += " and " + Proposal.Columns.OrderTimeS + ">=@OrderTimeS";
            }
            if (!string.IsNullOrEmpty(OrderTimeE))
            {
                sql += " and " + Proposal.Columns.OrderTimeS + "<=@OrderTimeE";
            }
            if (!string.IsNullOrEmpty(isWms))
            {
                if (isWms == "1")
                {
                    sql += " and " + Proposal.Columns.IsWms + "=0";
                }
                else if (isWms == "2")
                {
                    sql += " and " + Proposal.Columns.IsWms + "=1";
                }
            }
            

            //if (!string.IsNullOrEmpty(SalesName))
            //{               
            //    sql += " and " + Proposal.Columns.SalesId + "=@SalesName";
            //}

            var qc = new QueryCommand(sql, Proposal.Schema.Provider.Name);

            qc.AddParameter("@Pid", pid, DbType.Int16);
            qc.AddParameter("@BusinessHourGuid", BusinessHourGuid, DbType.Guid);
            qc.AddParameter("@BrandName", BrandName, DbType.String);
            qc.AddParameter("@Deals", Deals, DbType.String);
            qc.AddParameter("@OrderTimeS", OrderTimeS, DbType.String);
            qc.AddParameter("@OrderTimeE", OrderTimeE, DbType.String);
            qc.AddParameter("@SalesName", SalesName, DbType.String);
            var vpolCol = new ProposalCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewProposalSellerCollection ViewProposalSellerGetListForPhotohrapher(DateTime beginTime, DateTime endTime)
        {
            string sql = @" SELECT * from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) + " WHERE " + ViewProposalSeller.Columns.CreateTime + " BETWEEN " + "@beginTime" + " AND " + "@endTime " +
                          "AND " + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppoint + " > 0 " +
                          "AND " + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCheck + " = 0 " +
                          "AND " + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointCancel + " = 0 " +
                          "AND " + ViewProposalSeller.Columns.PhotographerAppointFlag + " & " + (int)ProposalPhotographer.PhotographerAppointClose + " = 0 " +
                          "AND " + ViewProposalSeller.Columns.SpecialFlag + " & " + (int)ProposalSpecialFlag.Photographers + " > 0 " +
                          "AND " + ViewProposalSeller.Columns.BusinessFlag + " & " + (int)ProposalBusinessFlag.PhotographerCheck + " = 0";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@beginTime", beginTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            var vpolCol = new ViewProposalSellerCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        /// <summary>
        /// 創意部查詢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="orderBy"></param>
        /// <param name="city"></param>
        /// <param name="DateType"></param>
        /// <param name="DateStart"></param>
        /// <param name="DateEnd"></param>
        /// <param name="StatusType"></param>
        /// <param name="Status"></param>
        /// <param name="Emp"></param>
        /// <param name="DealProperty"></param>
        /// <param name="PicType"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        public ViewProposalSellerCollection ViewProposalSellerGetListForProduction(int pageStart, int pageLength, string orderBy, string city, int dateType,
                                                                                   DateTime dateStart, DateTime dateEnd, int statusType, int status,
                                                                                   string emp, int dealProperty, int picType, int dealType, int dealSubType,
                                                                                   bool marketingResource, bool chkProduction, ref string sSQL)
        {
            string defOrderBy = ViewProposalSeller.Columns.BusinessHourGuid;
            string strSQL = " select vps.* from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) +
                           " vps with(nolock) ";

            QueryCommand qc = new QueryCommand(strSQL, ViewProposalSeller.Schema.Provider.Name);
            qc.CommandSql = ViewProposalSellerGetData(city, dateType, dateStart, dateEnd, statusType, status, emp, dealProperty, picType, dealType, dealSubType, marketingResource, chkProduction, qc);


            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewProposalSellerCollection vpsc = new ViewProposalSellerCollection();
            DateTime start = DateTime.Now;
            try
            {
                vpsc.LoadAndCloseReader(DataService.GetReader(qc));
            }
            catch (Exception ex)
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("pageStart=" + pageStart);
                sb.AppendLine("pageLength=" + pageLength);
                sb.AppendLine("orderBy=" + orderBy);
                sb.AppendLine("city=" + city);
                sb.AppendLine("DateType=" + dateType);
                sb.AppendLine("DateStart=" + dateStart);
                sb.AppendLine("DateEnd=" + dateEnd);
                sb.AppendLine("StatusType=" + statusType);
                sb.AppendLine("Status=" + status);
                sb.AppendLine("Emp=" + emp);
                sb.AppendLine("DealProperty=" + dealProperty);
                sb.AppendLine("PicType=" + picType);
                sb.AppendLine("DealType=" + dealType);
                sb.AppendLine("DealSubType=" + dealSubType);
                sb.AppendLine("marketingResource=" + marketingResource.ToString());
                sb.AppendLine("chkProduction=" + chkProduction.ToString());
                logger.Warn("ViewProposalSellerGetListForProduction 發生錯誤, sql=" + qc.CommandSql + ", 參數, " + sb.ToString(), ex);
                throw;
            }
            DateTime end = DateTime.Now;

            TimeSpan sp = end - start;
            sSQL += "  " + "Start：" + start.ToString("yyyy-MM-dd hh:mm:ss fff") + " End：" + end.ToString("yyyy-MM-dd hh:mm:ss fff") + " Time：" + sp.Milliseconds + "毫秒 " + vpsc.Count() + "筆" + 
                qc.CommandSql + "==param==" + new JsonSerializer().Serialize(qc.Parameters.Select(x => x.ParameterName + x.ParameterValue)) +
                "SERVER：" + System.Net.Dns.GetHostName();


            return vpsc;

        }

        public int ViewProposalSellerGetCountForProduction(string city, int dateType, DateTime dateStart, DateTime dateEnd, int statusType,
                                                           int status, string emp, int dealProperty, int picType, int dealType,
                                                           int dealSubType, bool marketingResource, bool chkProduction)
        {
            string strSQL = " select count(*) from " + ViewProposalSeller.Schema.Provider.DelimitDbName(ViewProposalSeller.Schema.TableName) +
                           " vps with(nolock) ";

            QueryCommand qc = new QueryCommand(strSQL, ViewProposalSeller.Schema.Provider.Name);
            qc.CommandSql = ViewProposalSellerGetData(city, dateType, dateStart, dateEnd, statusType, status, emp, dealProperty, picType, dealType, dealSubType, marketingResource, chkProduction, qc);

            return (int)DataService.ExecuteScalar(qc);
        }

        private string ViewProposalSellerGetData(string city, int dateType, DateTime dateStart, DateTime dateEnd, int statusType,
                                                 int status, string emp, int dealProperty, int picType, int dealType,
                                                 int dealSubType, bool marketingResource, bool chkProduction, QueryCommand qc)
        {
            string sqlEmp = string.Empty;
            string sqlPaLCombineDate = string.Empty;
            string sqlPaLCombineMax = string.Empty;
            string sqlAssign = string.Empty;


            #region 先抓有哪些flag文字
            if (status == 0)
            {
                sqlAssign = "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.Setting) + "'," +
                            "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.CopyWriter) + "'," +
                            "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.ImageDesign) + "'," +
                            "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.ART) + "'," +
                            "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.Listing) + "'";
            }
            else
            {
                if (Helper.IsFlagSet(status, ProposalEditorFlag.Setting))
                {
                    sqlAssign += "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.Setting) + "',";
                }
                if (Helper.IsFlagSet(status, ProposalEditorFlag.CopyWriter))
                {
                    sqlAssign += "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.CopyWriter) + "',";
                }
                if (Helper.IsFlagSet(status, ProposalEditorFlag.ImageDesign))
                {
                    sqlAssign += "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.ImageDesign) + "',";
                }
                if (Helper.IsFlagSet(status, ProposalEditorFlag.ART))
                {
                    sqlAssign += "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.ART) + "',";
                }
                if (Helper.IsFlagSet(status, ProposalEditorFlag.Listing))
                {
                    sqlAssign += "N'" + Helper.GetLocalizedEnum(I18N.Phrase.ResourceManager, ProposalEditorFlag.Listing) + "',";
                }
            }
            
            #endregion

            #region 上檔頻道
            if (!string.IsNullOrEmpty(city))
            {
                string cityParameter = "";
                for (int i = 0; i < city.Split(',').Count(); i++)
                {
                    qc.AddParameter("@city" + i, city.Split(',')[i], DbType.String);
                    cityParameter += "@city" + i.ToString() + ",";
                }
                qc.CommandSql += " inner join (select bid from category_deals with(nolock) where cid in (" + cityParameter.TrimEnd(',') + ") group by bid) cd on vps.business_hour_guid = cd.bid";
            }
            #endregion

            #region 日期
            if (dateType != 0)
            {
                if (dateType == 2)
                {
                    //工作完成日
                    qc.CommandSql += " inner join (" +
                                     " SELECT " + ProposalLog.Columns.ProposalId + ", MAX(" + ProposalLog.Columns.CreateTime + ") as maxtime FROM " +
                                     ProposalLog.Schema.Provider.DelimitDbName(ProposalLog.Schema.TableName) +
                                     " with(nolock) where replace(SUBSTRING(change_log,1,5),N'(','') IN ( " + sqlAssign.TrimEnd(',') + ")" +
                                     " GROUP BY " + ProposalLog.Columns.ProposalId +
                                     " having Max(create_time) >= @DateStart AND Max(create_time) < @DateEnd) as pl " +
                                     " on vps.id = pl.proposal_id";
                }
                else if (dateType == 3)
                {
                    //指派日
                    if (statusType == 2 || statusType == 3)
                    {
                        //ProposalAssignLog不要重複join
                        sqlPaLCombineDate = " AND " + ProposalAssignLog.Columns.CreateTime + "  >= @DateStart and  " + ProposalAssignLog.Columns.CreateTime + " < @DateEnd";
                        sqlPaLCombineMax = ",max(create_time) as maxtime";
                    }
                    else
                    {
                        qc.CommandSql += " inner join (select " + ProposalAssignLog.Columns.Pid + " ,max(create_time) as maxtime " +
                                    " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                    " with(nolock) where " + ProposalAssignLog.Columns.CreateTime + "  >= @DateStart and  " + ProposalAssignLog.Columns.CreateTime + " < @DateEnd GROUP BY " + ProposalAssignLog.Columns.Pid + ") pal on vps.id = pal.pid";
                    }
                    
                }
                else if (dateType == 4)
                {
                    //執行排檔日
                    qc.CommandSql += " inner join (" +
                                     " SELECT " + ProposalLog.Columns.ProposalId + ", MAX(" + ProposalLog.Columns.CreateTime + ") as maxtime FROM " +
                                     ProposalLog.Schema.Provider.DelimitDbName(ProposalLog.Schema.TableName) +
                                     " with(nolock) where change_log LIKE N'\\[系統\\] 排檔於%' ESCAPE '\\'" +
                                     " GROUP BY " + ProposalLog.Columns.ProposalId +
                                     " having Max(create_time) >= @DateStart AND Max(create_time) < @DateEnd) as pl " +
                                     " on vps.id = pl.proposal_id";
                }

                qc.AddParameter("@DateStart", dateStart, DbType.DateTime);
                qc.AddParameter("@DateEnd", dateEnd.AddDays(1), DbType.DateTime);
            }
            #endregion

            #region 檔次/工作狀態
            if ((statusType == 2 || statusType == 3) && status != 0)
            {
                //已指派未完成 and 已指派已完成

                #region 看有哪些flag
                string flag = Convert.ToString(status, 2);

                int count = 0;
                List<string> list = new List<string>();


                int len = flag.Length - 1;
                while (len >= 0)
                {

                    string num = flag.Substring(len, 1);
                    int numm = Convert.ToInt32(num);
                    if (numm == 1)
                    {
                        list.Add(Math.Pow(2, count).ToString());
                    }

                    len--;
                    count++;
                } 
                #endregion

                //人員
                if (emp != null)
                {
                    sqlEmp = " and " + ProposalAssignLog.Columns.AssignEmail + " = @Emp ";
                }

                qc.CommandSql += " inner JOIN (Select " + ProposalAssignLog.Columns.Pid + sqlPaLCombineMax + " FROM " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                              "  with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " in (" + string.Join(",", list) + ")" + sqlEmp + sqlPaLCombineDate + " GROUP BY " + ProposalAssignLog.Columns.Pid +
                              " having count(*) >=" + list.Count().ToString() + " ) pal ON vps.id = pal.pid";


            }

            if ((statusType == 2 || statusType == 3) && chkProduction)
            {
                List<string> list = new List<string>();
                foreach (var item in Enum.GetValues(typeof(ProposalEditorFlag)))
                {
                    list.Add(((int)item).ToString());
                }
                qc.CommandSql += " inner JOIN (Select " + ProposalAssignLog.Columns.Pid + sqlPaLCombineMax + " FROM " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                            "  with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " in (" + string.Join(",", list) + ")" + sqlEmp + sqlPaLCombineDate + " GROUP BY " + ProposalAssignLog.Columns.Pid +
                            " ) pal ON vps.id = pal.pid";
            }
            #endregion

                #region 工作人員
                if (emp != null)
            {
                if (statusType != 2 && statusType != 3)
                {
                    //直接在檔次工作狀態篩選
                    qc.CommandSql += " inner join (select pl." + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) + " pl " +
                                     " with(nolock) where " + ProposalAssignLog.Columns.AssignEmail + " = @Emp group by " + ProposalAssignLog.Columns.Pid +
                                     ") plUser on vps.id = plUser.pid";
                }
            }

            #endregion

            


            qc.CommandSql += " where 1=1 ";


            #region 日期
            if (dateType != 0)
            {
                //上檔日
                if (dateType == 1)
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.OrderTimeS + " >= @DateStart and " + ViewProposalSeller.Columns.OrderTimeS + " < @DateEnd";
                else if (dateType == 4)
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.OrderTimeS + " IS NOT NULL";
            }
            #endregion

            #region 檔次/工作狀態
            if (statusType != 0 && chkProduction == false)
            {
                //未指派
                if (statusType == 1)
                {
                    if (Helper.IsFlagSet(status, ProposalEditorFlag.Setting))
                        qc.CommandSql += " and vps." + ViewProposalSeller.Columns.Id +
                                  " not in (select " + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                  " with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " = " + (int)ProposalEditorFlag.Setting + ")";
                    if (Helper.IsFlagSet(status, ProposalEditorFlag.CopyWriter))
                        qc.CommandSql += " and vps." + ViewProposalSeller.Columns.Id +
                                  " not in (select " + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                  " with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " = " + (int)ProposalEditorFlag.CopyWriter + ")";
                    if (Helper.IsFlagSet(status, ProposalEditorFlag.ImageDesign))
                        qc.CommandSql += " and vps." + ViewProposalSeller.Columns.Id +
                                  " not in (select " + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                  " with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " = " + (int)ProposalEditorFlag.ImageDesign + ")";
                    if (Helper.IsFlagSet(status, ProposalEditorFlag.ART))
                        qc.CommandSql += " and vps." + ViewProposalSeller.Columns.Id +
                                  " not in (select " + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                  " with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " = " + (int)ProposalEditorFlag.ART + ")";
                    if (Helper.IsFlagSet(status, ProposalEditorFlag.Listing))
                        qc.CommandSql += " and vps." + ViewProposalSeller.Columns.Id +
                                  " not in (select " + ProposalAssignLog.Columns.Pid + " from " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) +
                                  " with(nolock) where " + ProposalAssignLog.Columns.AssignFlag + " = " + (int)ProposalEditorFlag.Listing + ")";

                    //排除以前資料會有未指派已完成的狀況
                    qc.CommandSql += " and (vps.edit_flag & " + status.ToString() + " !=" + status.ToString() + " and vps.edit_pass_flag & " + status.ToString() + " !=" + status.ToString() + ")";

                }
                //指派未完成
                else if (statusType == 2)
                    qc.CommandSql += " and (vps.edit_flag | vps.edit_pass_flag & " + status.ToString() + " !=" + status.ToString() + ")";
                else if (statusType == 3)
                    qc.CommandSql += " and (vps.edit_flag & " + status.ToString() + " > 0" + " or vps.edit_pass_flag & " + status.ToString() + " > 0 )";

            }
            #endregion

            #region 工作人員
            if (emp != null)
            {
                qc.AddParameter("@Emp", emp, DbType.String);
            }
            #endregion

            #region 檔次類型
            if (dealProperty != 0)
            {
                if (dealProperty == 1)
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.SpecialFlag + " & " + (int)ProposalSpecialFlag.Urgent + " = 0 and " + ViewProposalSeller.Columns.CopyType + " =0";
                else if (dealProperty == 2)
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.SpecialFlag + " & " + (int)ProposalSpecialFlag.Urgent + " > 0";
                else if (dealProperty == 3)
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.CopyType + " !=0";

            }
            #endregion

            #region 圖片類型
            if (picType != 0)
            {
                qc.CommandSql += " and " + ViewProposalSeller.Columns.SpecialFlag + " & @PicType = @PicType and " + ViewProposalSeller.Columns.SpecialFlag + " & " + ((int)ProposalSpecialFlag.Photographers + (int)ProposalSpecialFlag.SellerPhoto + (int)ProposalSpecialFlag.FTPPhoto - picType) + "=0";
                qc.AddParameter("@PicType", picType, DbType.Int32);
            }
            #endregion

            #region 提案類型

            if (dealType != 0)
            {
                qc.CommandSql += " and " + ViewProposalSeller.Columns.DealType + "  = @DealType";
                qc.AddParameter("@DealType", dealType, DbType.Int32);
            }

            if (dealSubType != 0)
            {
                qc.CommandSql += " and  " + ViewProposalSeller.Columns.DealSubType + " = @DealSubType";
                qc.AddParameter("@DealSubType", dealSubType, DbType.Int32);
            }

            #endregion

            #region 提案類型
            if (marketingResource)
            {
                qc.CommandSql += " and ISNULL(" + ViewProposalSeller.Columns.MarketingResource + ",'') !=''";
            }
            #endregion

            #region 製檔
            if (chkProduction)
            {
                if(statusType == 1)
                {
                    //未指派
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.BusinessFlag + "& " + (int)ProposalBusinessFlag.ProposalAudit + " > 0 and (ISNULL(" + ViewProposalSeller.Columns.IsProduction + ",0) = 1 and listing_flag&" + (int)ProposalListingFlag.PageCheck + "=0) ";
                    qc.CommandSql += " and " + ViewProposalSeller.Columns.Id + " NOT IN ( Select distinct pid FROM " + ProposalAssignLog.Schema.Provider.DelimitDbName(ProposalAssignLog.Schema.TableName) + "  with(nolock) ) ";
                }
                else if (statusType == 2)
                {
                    //已指派未完成
                    qc.CommandSql += " and (ISNULL(" + ViewProposalSeller.Columns.IsProduction + ",0) = 1 and listing_flag&" + (int)ProposalListingFlag.PageCheck + "=0) ";
                }
                else if (statusType == 3)
                {
                    //已指派已完成
                    qc.CommandSql += " and (ISNULL(" + ViewProposalSeller.Columns.IsProduction + ",0) = 1 and listing_flag&" + (int)ProposalListingFlag.PageCheck + ">0) ";
                }
            }            
            #endregion 製檔

            return qc.CommandSql;




        }


        protected QueryCommand GetDCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }
        #endregion

        #region ProposalLog

        public void ProposalLogSet(ProposalLog log)
        {
            DB.Save<ProposalLog>(log);
        }

        public ProposalLog ProposalLogSaveThenSelect(ProposalLog log)
        {
            DB.Save<ProposalLog>(log);
            return log;
        }

        public ProposalLogCollection ProposalLogGetList(int proposalId, ProposalLogType type)
        {
            return DB.SelectAllColumnsFrom<ProposalLog>().NoLock()
                .Where(ProposalLog.Columns.ProposalId).IsEqualTo(proposalId)
                .And(ProposalLog.Columns.Type).IsEqualTo((int)type)
                .ExecuteAsCollection<ProposalLogCollection>();
        }

        public ProposalLogCollection ProposalLogGetList(int proposalId, List<int> types)
        {
            return DB.SelectAllColumnsFrom<ProposalLog>().NoLock()
                .Where(ProposalLog.Columns.ProposalId).IsEqualTo(proposalId)
                .And(ProposalLog.Columns.Type).In(types)
                .ExecuteAsCollection<ProposalLogCollection>();
        }

        public void ProposalLogDeleteList(int pid)
        {
            DB.Delete<ProposalLog>(ProposalLog.Columns.ProposalId, pid);
        }

        #endregion

        #region ProposalAssignLog

        public void ProposalAssignLogSet(ProposalAssignLog log)
        {
            DB.Save<ProposalAssignLog>(log);
        }

        public ProposalAssignLogCollection ProposalAssignLogGetList(int proposalId)
        {
            return DB.SelectAllColumnsFrom<ProposalAssignLog>()
                .Where(ProposalAssignLog.Columns.Pid).IsEqualTo(proposalId)
                .ExecuteAsCollection<ProposalAssignLogCollection>();
        }

        public ProposalAssignLogCollection ProposalAssignLogGetList(List<int> proposalIds)
        {
            return DB.SelectAllColumnsFrom<ProposalAssignLog>()
                .Where(ProposalAssignLog.Columns.Pid).In(proposalIds)
                .ExecuteAsCollection<ProposalAssignLogCollection>();
        }


        public bool ProposalAssignLogDelete(int pid)
        {
            DB.Destroy<ProposalAssignLog>(ProposalAssignLog.Columns.Pid, pid);
            return true;
        }
        #endregion

        #region ProposalReturnDetail
        public void ProposalReturnDetailSave(ProposalReturnDetail returnDetail)
        {
            DB.Save<ProposalReturnDetail>(returnDetail);
        }
        #endregion

        #region ProposalMultiDeal
        public ProposalMultiDealCollection ProposalMultiDealGetByPid(int pid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Pid).IsEqualTo(pid)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .OrderAsc(ProposalMultiDeal.Columns.Sort)
                .ExecuteAsCollection<ProposalMultiDealCollection>();
        }
        public ProposalMultiDealCollection ProposalMultiDealGetListByPid(List<int> pids)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Pid).In(pids)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteAsCollection<ProposalMultiDealCollection>();
        }
        public ProposalMultiDeal ProposalMultiDealGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Bid).IsEqualTo(bid)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteSingle<ProposalMultiDeal>();
        }
        public ProposalMultiDeal ProposalMultiDealGetAllByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Bid).IsEqualTo(bid)
                .ExecuteSingle<ProposalMultiDeal>();
        }
        public ProposalMultiDealCollection ProposalMultiDealGetByItemName(string itemName)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.ItemName).Like("%" + itemName + "%")
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteAsCollection<ProposalMultiDealCollection>();
        }
        public ProposalMultiDealCollection ProposalMultiDealGetListByBid(List<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Bid).In(bids)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteAsCollection<ProposalMultiDealCollection>();
        }
        public ProposalMultiDeal ProposalMultiDealGetById(int id)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Id).IsEqualTo(id)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteSingle<ProposalMultiDeal>();
        }
        public ProposalMultiDealCollection ProposalMultiDealGetListById(List<int> id)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Id).In(id)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteAsCollection<ProposalMultiDealCollection>();
        }

        public List<int> ProposalMultiDealGetPidListById(List<int> id)
        {
            return DB.Select(ProposalMultiDeal.Columns.Pid)
                .From<ProposalMultiDeal>().NoLock()
                .Where(ProposalMultiDeal.Columns.Id).In(id)
                .And(ProposalMultiDeal.Columns.Status).IsEqualTo((int)ProposalMultiDealStatus.Init)
                .ExecuteTypedList<int>();
        }

        public Dictionary<Guid, string> ProposalMultiDealGetOptionsDictByOnlineDeals()
        {
            string sql = @"
                select pmd.bid, pmd.options from proposal_multi_deals pmd with(nolock)
                where pmd.bid in 
			    (
				    (select distinct dts.business_hour_GUID from deal_time_slot dts 
					    where getdate() between dts.effective_start and dts.effective_end)
				    union
				    (select cd.BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid in 
					    (
						    select distinct dts.business_hour_GUID from deal_time_slot dts 
						    where getdate() between dts.effective_start and dts.effective_end
					    )
				    )			
			    )
	            and pmd.status = 0";
            QueryCommand qc = new QueryCommand(sql, ProposalMultiDeal.Schema.Provider.Name);
            Dictionary<Guid, string> result = new Dictionary<Guid, string>();

            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0), dr.GetString(1));
                }
            }
            return result;
        }

        public bool ProposalMultiDealDelete(ProposalMultiDeal deal)
        {
            DB.Delete<ProposalMultiDeal>(deal);
            return true;
        }

        public bool ProposalMultiDealDeleteByPid(int pid)
        {
            DB.Delete().From<ProposalMultiDeal>().Where(ProposalMultiDeal.PidColumn).IsEqualTo(pid).Execute();
            return true;
        }
        //public ProposalMultiDealCollection ProposalMultiDealGetByOptions(string searchValue)
        //{
        //    SqlQuery query = DB.SelectAllColumnsFrom<ProposalMultiDeal>();
        //    query.Where(ProposalMultiDeal.Columns.Options).Like("%" + searchValue + "%");

        //    return query.ExecuteAsCollection<ProposalMultiDealCollection>();
        //}

        public bool ProposalMultiDealSetList(ProposalMultiDealCollection multiDeals)
        {
            DB.SaveAll(multiDeals);
            return true;
        }

        public bool ProposalMultiDealsSet(ProposalMultiDeal deal)
        {
            DB.Save<ProposalMultiDeal>(deal);
            return true;
        }


        public bool ProposalMultiDealsDelete(int pid)
        {
            DB.Destroy<ProposalMultiDeal>(ProposalMultiDeal.Columns.Pid, pid);
            return true;
        }
        #endregion ProposalMultiDeal

        #region ProposalMultiOptionSpec
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByMid(int mid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.MultiOptionId).IsEqualTo(mid).OrderAsc(ProposalMultiOptionSpec.Columns.Sort)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByMids(List<int> mids)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.MultiOptionId).In(mids)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByProduct(Guid gid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.ProductGuid).IsEqualTo(gid)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByItem(Guid gid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.ItemGuid).IsEqualTo(gid)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public ProposalMultiOptionSpecCollection ProposalMultiOptionSpecGetByProductItem(Guid pid, Guid itemGuid)
        {
            return DB.SelectAllColumnsFrom<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.ProductGuid).IsEqualTo(pid)
                .And(ProposalMultiOptionSpec.Columns.ItemGuid).IsEqualTo(itemGuid)
                .ExecuteAsCollection<ProposalMultiOptionSpecCollection>();
        }
        public List<Guid> ProposalMultiOptionSpecGetItemGuidByBid(Guid bid)
        {
            return DB.Select(ProposalMultiOptionSpec.Columns.ItemGuid)
                .From<ProposalMultiOptionSpec>().NoLock()
                .Where(ProposalMultiOptionSpec.Columns.BusinessHourGuid).IsEqualTo(bid)
                .OrderAsc(ProposalMultiOptionSpec.Columns.GroupId)
                .OrderAsc(ProposalMultiOptionSpec.Columns.Sort)
                .ExecuteTypedList<Guid>();
        }
        public bool ProposalMultiOptionSpecSet(ProposalMultiOptionSpec spec)
        {
            DB.Save<ProposalMultiOptionSpec>(spec);
            return true;
        }
        public bool ProposalMultiOptionSpecDelete(int mid, Guid pid, Guid iid)
        {
            string sql = @" delete from " + ProposalMultiOptionSpec.Schema.TableName + @"
                            where " + ProposalMultiOptionSpec.Columns.MultiOptionId + @" = @mid
                            and " + ProposalMultiOptionSpec.Columns.ProductGuid + @"=@pid
                            and " + ProposalMultiOptionSpec.Columns.ItemGuid + @"=@iid";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@mid", mid, DbType.Int32);
            qc.AddParameter("@pid", pid, DbType.Guid);
            qc.AddParameter("@iid", iid, DbType.Guid);
            DB.Execute(qc);
            return true;
        }


        public bool ProposalMultiOptionSpecDelete(Guid bid)
        {
            DB.Destroy<ProposalMultiOptionSpec>(ProposalMultiOptionSpec.Columns.BusinessHourGuid, bid);
            return true;
        }
        #endregion ProposalMultiOptionSpec

        #region ProposalRestriction
        public bool ProposalRestrictionSet(ProposalRestriction res)
        {
            DB.Save<ProposalRestriction>(res);
            return true;
        }

        public ProposalRestrictionCollection ProposalRestrictionGet()
        {
            return DB.SelectAllColumnsFrom<ProposalRestriction>()
                .OrderAsc(ProposalRestriction.Columns.ParentCodeId)
                .OrderAsc(ProposalRestriction.Columns.CodeId)
                .ExecuteAsCollection<ProposalRestrictionCollection>();
        }

        public ProposalRestriction ProposalRestrictionGetById(int id)
        {
            return DB.SelectAllColumnsFrom<ProposalRestriction>()
                .Where(ProposalRestriction.Columns.Id).IsEqualTo(id).ExecuteSingle<ProposalRestriction>();
        }

        public ProposalRestriction ProposalRestrictionGetByCodeId(int codeId)
        {
            return DB.SelectAllColumnsFrom<ProposalRestriction>()
                .Where(ProposalRestriction.Columns.CodeId).IsEqualTo(codeId)
                .And(ProposalRestriction.Columns.Enable).IsEqualTo(true).ExecuteSingle<ProposalRestriction>();
        }
        #endregion

        #region ProposalRelatedDeal
        public ProposalRelatedDealCollection ProposalRelatedDealGetByPid(int pid)
        {
            return DB.SelectAllColumnsFrom<ProposalRelatedDeal>().NoLock()
                .Where(ProposalRelatedDeal.Columns.ProposalId).IsEqualTo(pid)
                .OrderAsc(ProposalRelatedDeal.Columns.Id)
                .ExecuteAsCollection<ProposalRelatedDealCollection>();
        }

        public bool ProposalRelatedDealSet(ProposalRelatedDeal deal)
        {
            DB.Save<ProposalRelatedDeal>(deal);
            return true;
        }

        public bool ProposalRelatedDealDelete(ProposalRelatedDeal deal)
        {
            DB.Delete<ProposalRelatedDeal>(deal);
            return true;
        }
        #endregion ProposalRelatedDeal


        #region BusinessChangeLog

        public void BusinessChangeLogSet(BusinessChangeLog log)
        {
            DB.Save<BusinessChangeLog>(log);
        }

        public BusinessChangeLogCollection BusinessChangeLogGetList(Guid bid)
        {
            return DB.SelectAllColumnsFrom<BusinessChangeLog>()
                .Where(BusinessChangeLog.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<BusinessChangeLogCollection>();
        }

        #endregion

        #region SellerChangeLog

        public void SellerChangeLogSet(SellerChangeLog log)
        {
            DB.Save<SellerChangeLog>(log);
        }

        public SellerChangeLogCollection SellerChangeLogGetList(Guid sid)
        {
            return DB.SelectAllColumnsFrom<SellerChangeLog>().Where(SellerChangeLog.Columns.SellerGuid).IsEqualTo(sid).ExecuteAsCollection<SellerChangeLogCollection>();
        }

        #endregion

        #region SellerManageLog

        public void SellerManageLogSet(SellerManageLog log)
        {
            DB.Save<SellerManageLog>(log);
        }

        public SellerManageLogCollection SellerManageLogGetList(Guid sid)
        {
            return DB.SelectAllColumnsFrom<SellerManageLog>().Where(SellerManageLog.Columns.SellerGuid).IsEqualTo(sid).ExecuteAsCollection<SellerManageLogCollection>();
        }

        #endregion

        #region StoreChangeLog

        public void StoreChangeLogSet(StoreChangeLog log)
        {
            DB.Save<StoreChangeLog>(log);
        }

        public StoreChangeLogCollection StoreChangeLogGetList(Guid sid)
        {
            return DB.SelectAllColumnsFrom<StoreChangeLog>().Where(StoreChangeLog.Columns.StoreGuid).IsEqualTo(sid).ExecuteAsCollection<StoreChangeLogCollection>();
        }

        #endregion

        #region store_category

        public StoreCategoryCollection StoreCategoryCollectionGetByStore(Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<StoreCategory>()
                .Where(StoreCategory.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(StoreCategory.Columns.Valid).IsEqualTo(1)
                .ExecuteAsCollection<StoreCategoryCollection>();
        }
        public ViewStoreCategoryCollection ViewStoreCategoryCollectionGetByStoreList(List<Guid> list)
        {
            return DB.SelectAllColumnsFrom<ViewStoreCategory>().Where(ViewStoreCategory.Columns.StoreGuid).In(list).ExecuteAsCollection<ViewStoreCategoryCollection>();
        }
        public bool StoreCategorySet(StoreCategory storeCategory)
        {
            return DB.Save<StoreCategory>(storeCategory) > 0;
        }

        #endregion store_category

        #region view_store_category

        public ViewStoreCategoryCollection ViewStoreCategoryCollectionGetByStore(Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<ViewStoreCategory>()
                .Where(ViewStoreCategory.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<ViewStoreCategoryCollection>();
        }

        #endregion view_store_category

        #region ProposalCategoryDeal

        public ProposalCategoryDealCollection ProposalCategoryDealsGetList(int pid)
        {
            return DB.SelectAllColumnsFrom<ProposalCategoryDeal>().Where(ProposalCategoryDeal.Columns.Pid).IsEqualTo(pid).ExecuteAsCollection<ProposalCategoryDealCollection>();
        }

        public bool ProposalCategoryDealSetList(ProposalCategoryDealCollection categories)
        {
            DB.SaveAll(categories);
            return true;
        }

        public void DeleteProposalCategoryDealsByCategoryType(int pid, CategoryType type)
        {
            string sql = @" delete from " + ProposalCategoryDeal.Schema.TableName + @"
                            where " + ProposalCategoryDeal.Columns.Pid + @" = @pid
                            and " + ProposalCategoryDeal.Columns.Cid + @" in (SELECT c." + Category.Columns.Id + @" FROM " + Category.Schema.TableName + @" c where " + Category.Columns.Type + @" = @type )";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@pid", pid, DbType.Int32);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            DB.Execute(qc);
        }

        public void DeleteProposalCategoryDealsByCategoryType(int pid, int cid)
        {
            string sql = @" delete from " + ProposalCategoryDeal.Schema.TableName + @"
                            where " + ProposalCategoryDeal.Columns.Pid + @" = @pid
                            and " + ProposalCategoryDeal.Columns.Cid + @" = @cid";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@pid", pid, DbType.Int32);
            qc.AddParameter("@cid", cid, DbType.Int32);
            DB.Execute(qc);
        }

        public bool ProposalCategoryDealsDelete(int pid)
        {
            DB.Destroy<ProposalCategoryDeal>(ProposalCategoryDeal.Columns.Pid, pid);
            return true;
        }



        #endregion

        #region ProposalSellerFiles
        public ProposalSellerFile ProposalSellerFileGet(int id)
        {
            return DB.SelectAllColumnsFrom<ProposalSellerFile>()
                .Where(ProposalSellerFile.Columns.Id).IsEqualTo(id).ExecuteSingle<ProposalSellerFile>();
        }
        public ProposalSellerFileCollection ProposalSellerFileListGet(int pid)
        {
            return DB.SelectAllColumnsFrom<ProposalSellerFile>()
                .Where(ProposalSellerFile.Columns.ProposalId).IsEqualTo(pid)
                .And(ProposalSellerFile.Columns.FileStatus).IsEqualTo((int)ProposalFileStatus.Nomal)
                .ExecuteAsCollection<ProposalSellerFileCollection>();
        }
        public bool ProposalSellerFileSet(ProposalSellerFile files)
        {
            DB.Save<ProposalSellerFile>(files);
            return true;
        }
        #endregion

        #region seller_tree

        public SellerTreeCollection SellerTreeGetListByParentSellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerTree>()
                .Where(SellerTree.Columns.ParentSellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<SellerTreeCollection>();
        }

        public IEnumerable<Guid> SellerTreeGetSellerGuidListByParentSellerGuid(Guid sellerGuid)
        {
            return DB.Select(SellerTree.Columns.SellerGuid).From<SellerTree>()
                .Where(SellerTree.Columns.ParentSellerGuid).IsEqualTo(sellerGuid)
                .ExecuteTypedList<Guid>();
        }

        public SellerTreeCollection SellerTreeGetListBySellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerTree>()
                .Where(SellerTree.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<SellerTreeCollection>();
        }

        public SellerTreeCollection SellerTreeGetListByRootSellerGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<SellerTree>()
                .Where(SellerTree.Columns.RootSellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<SellerTreeCollection>();
        }

        public Guid? SellerTreeGetRootSellerGuidBySellerGuid(Guid sellerGuid)
        {
            string strSql = "SELECT Top 1 " + SellerTree.Columns.RootSellerGuid + " FROM " + SellerTree.Schema.TableName + " with(nolock) " +
                            "where " + SellerTree.Columns.SellerGuid + " = @sellerGuid";
            QueryCommand qc = new QueryCommand(strSql, SellerTree.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            return (Guid?)DataService.ExecuteScalar(qc);
        }

        public IEnumerable<Guid> SellerTreeGetSellerGuidListByRootSellerGuid(Guid sellerGuid)
        {
            var col = DB.SelectAllColumnsFrom<SellerTree>().Where(SellerTree.Columns.RootSellerGuid).IsEqualTo(sellerGuid).ExecuteAsCollection<SellerTreeCollection>();
            return col.Any() ? col.Select(x => x.SellerGuid).ToList() : new List<Guid>();

            //return DB.Select(SellerTree.Columns.SellerGuid).From<SellerTree>()
            //    .Where(SellerTree.Columns.RootSellerGuid).IsEqualTo(sellerGuid)
            //    .ExecuteTypedList<Guid>();
        }

        public SellerTreeCollection SellerTreeGetListBySellerGuid(IEnumerable<Guid> sellerGuids)
        {
            var result = new SellerTreeCollection();

            var tempIds = sellerGuids.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<SellerTree>()
                    .Where(SellerTree.Columns.SellerGuid).In(tempIds.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<SellerTreeCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }

        public SellerTreeCollection SellerTreeGetListByParentSellerGuid(IEnumerable<Guid> sellerGuids)
        {
            var result = new SellerTreeCollection();

            var tempIds = sellerGuids.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchBh = DB.SelectAllColumnsFrom<SellerTree>()
                    .Where(SellerTree.Columns.ParentSellerGuid).In(tempIds.Skip(idx).Take(batchLimit))
                    .Or(SellerTree.Columns.SellerGuid).In(tempIds.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<SellerTreeCollection>();
                result.AddRange(batchBh);
                idx += batchLimit;
            }

            return result;
        }

        public bool SellerTreeSet(SellerTree sellerTree)
        {
            DB.Save<SellerTree>(sellerTree);
            return true;
        }

        public void SellerTreeDelete(SellerTree sellerTree)
        {
            DB.Delete<SellerTree>(sellerTree);
        }

        public IEnumerable<SellerTreeModel> SellerTreeGetListByRootGuid(Guid rootGuid)
        {
            string sql = @"select seller_guid, parent_seller_guid, root_seller_guid, sort from seller_tree st with(nolock)
                INNER JOIN seller s ON st.seller_guid = s.GUID
                 where root_seller_guid = @rootGuid
                 and s.store_status = 0
                ";
            List<SellerTreeModel> result = new List<SellerTreeModel>();
            var qc = new QueryCommand(sql, SellerTree.Schema.Provider.Name);
            qc.AddParameter("@rootGuid", rootGuid, DbType.Guid);
            IDataReader dr = null;
            try
            {
                dr = DataService.GetReader(qc);
                while (dr.Read())
                {
                    result.Add(new SellerTreeModel
                    {
                        SellerGuid = dr.GetGuid(0),
                        ParentSellerGuid = dr.GetGuid(1),
                        RootSellerGuid = dr.GetGuid(2),
                        Sort = dr.GetInt32(3)
                    });
                }
            }
            finally
            {
                if (dr != null && dr.IsClosed == false)
                {
                    dr.Close();
                }
            }
            return result;
        }

        public IEnumerable<Guid> SellerTreeMiddleGetSellerGuid(List<Guid> paretnSellerGuid, Guid? sellerGuid)
        {
            string strSql = "select s1.* from " + SellerTree.Schema.TableName + " s1 inner join " + SellerTree.Schema.TableName + " s2 on s1." + SellerTree.Columns.ParentSellerGuid + "=s2." + SellerTree.Columns.SellerGuid +
                            " where 1=1 ";
            if (sellerGuid != null)
            {
                strSql += " and s1." + SellerTree.Columns.SellerGuid + "=@sellerGuid";
            }

            strSql += " and s1." + SellerTree.Columns.ParentSellerGuid + " in (@p0";
            for (int i=1;i< paretnSellerGuid.Count();i++)
            {
                strSql += ",@p" + i.ToString();
            }
            strSql += ")";


            QueryCommand qc = new QueryCommand(strSql, SellerTree.Schema.Provider.Name);
            if (sellerGuid != null)
            {
                qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            }

            qc.AddParameter("@p0", paretnSellerGuid[0], DbType.Guid);
            for (int i = 1; i < paretnSellerGuid.Count(); i++)
            {
                qc.AddParameter("@p" + i.ToString(), paretnSellerGuid[i], DbType.Guid);
            }

            var dt = DataService.GetDataSet(qc).Tables[0];
            var result = new List<Guid>();

            foreach (var dr in dt.AsEnumerable())
            {
                result.Add(dr.Field<Guid>("seller_guid"));
            }

            return result;
        }

        #endregion seller_tree

        #region seller_tree_log

        public bool SellerTreeLogSet(SellerTreeLog sellerTreeLog)
        {
            DB.Save<SellerTreeLog>(sellerTreeLog);
            return true;
        }

        #endregion seller_tree_log

        #region proposal_performance_log
        public void ProposalPerformanceLogSet(ProposalPerformanceLog log)
        {
            DB.Save<ProposalPerformanceLog>(log);
        }
        #endregion

        #region proposal_file_oauth
        public ProposalFileOauth ProposalFileOauthGetByTicketId(string TicketId)
        {
            return DB.SelectAllColumnsFrom<ProposalFileOauth>()
                .Where(ProposalFileOauth.Columns.TicketId).IsEqualTo(TicketId)
                .ExecuteSingle<ProposalFileOauth>();
        }
        public void ProposalFileOauthSet(ProposalFileOauth log)
        {
            DB.Save<ProposalFileOauth>(log);
        }
        #endregion


        #region shopping_cart_freights
        public ShoppingCartFreight ShoppingCartFreightsGet(int id)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreight>()
                .Where(ShoppingCartFreight.Columns.Id).IsEqualTo(id).ExecuteSingle<ShoppingCartFreight>();
        }
        public ShoppingCartFreightCollection ShoppingCartFreightsGet(List<int> ids)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreight>()
                .Where(ShoppingCartFreight.Columns.Id).In(ids)
                .ExecuteAsCollection<ShoppingCartFreightCollection>();
        }
        public ShoppingCartFreightCollection ShoppingCartFreightsGetBySellerGuid(List<Guid> sids)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreight>()
                .Where(ShoppingCartFreight.Columns.SellerGuid).In(sids)
                .ExecuteAsCollection<ShoppingCartFreightCollection>();
        }
        public ShoppingCartFreightCollection ShoppingCartFreightsGetByUniqueId(List<string> fids)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreight>()
                .Where(ShoppingCartFreight.Columns.UniqueId).In(fids)
                .ExecuteAsCollection<ShoppingCartFreightCollection>();
        }
        public void ShoppingCartFreightsSet(ShoppingCartFreight freight)
        {
            DB.Save<ShoppingCartFreight>(freight);
        }

        public void ShoppingCartFreightsDelete(ShoppingCartFreight freight)
        {
            DB.Delete<ShoppingCartFreight>(freight);
        }
        public ShoppingCartFreightCollection ShoppingCartFreightsGetByStatus(ShoppingCartFreightStatus status)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreight>()
                .Where(ShoppingCartFreight.Columns.FreightsStatus).IsEqualTo(status)
                .ExecuteAsCollection<ShoppingCartFreightCollection>();
        }
        #endregion shopping_cart_freights

        #region shopping_cart_freights_item
        public ShoppingCartFreightsItem ShoppingCartFreightsItemGet(int id)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreightsItem>()
                .Where(ShoppingCartFreightsItem.Columns.Id).IsEqualTo(id).ExecuteSingle<ShoppingCartFreightsItem>();
        }
        public ShoppingCartFreightsItem ShoppingCartFreightsItemGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreightsItem>()
                .Where(ShoppingCartFreightsItem.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteSingle<ShoppingCartFreightsItem>();
        }
        public ShoppingCartFreightsItemCollection ShoppingCartFreightsItemGetByBids(List<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreightsItem>()
                .Where(ShoppingCartFreightsItem.Columns.BusinessHourGuid).In(bids)
                .ExecuteAsCollection<ShoppingCartFreightsItemCollection>();
        }
        public ShoppingCartFreightsItemCollection ShoppingCartFreightsItemGetByFid(int fid)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreightsItem>()
                .Where(ShoppingCartFreightsItem.Columns.FreightsId).IsEqualTo(fid).ExecuteAsCollection<ShoppingCartFreightsItemCollection>();
        }
        public void ShoppingCartFreightsItemSet(ShoppingCartFreightsItem freight)
        {
            DB.Save<ShoppingCartFreightsItem>(freight);
        }
        public bool ShoppingCartFreightsItemSetBulkInsert(List<ShoppingCartFreightsItem> items)
        {
            QueryCommandCollection qcCol = new QueryCommandCollection();
            foreach (var item in items)
            {
                qcCol.AddOrNot(SSHelper.GetQryCmd(item));
            }

            if (qcCol.Count > 0)
            {
                DataService.ExecuteTransaction(qcCol);
            }

            return true;
        }
        public bool ShoppingCartFreightsItemDelete(List<Guid> bids)
        {
            var queries = new List<SqlQuery>();
            queries.Add(
                new Delete().From(ShoppingCartFreightsItem.Schema).Where(ShoppingCartFreightsItem.BusinessHourGuidColumn).In(
                    bids));
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }
        //public bool ShoppingCartFreightsItemDelete(List<int> ids)
        //{
        //    var queries = new List<SqlQuery>();
        //    queries.Add(
        //        new Delete().From(ShoppingCartFreightsItem.Schema).Where(ShoppingCartFreightsItem.FreightsIdColumn).In(
        //            ids));
        //    SqlQuery.ExecuteTransaction(queries);
        //    return true;
        //}
        #endregion shopping_cart_freights_item

        #region shopping_cart_freights_log
        public ShoppingCartFreightsLogCollection ShoppingCartFreightsLogGetByIds(List<int> ids)
        {
            return DB.SelectAllColumnsFrom<ShoppingCartFreightsLog>()
                .Where(ShoppingCartFreightsLog.Columns.Id).In(ids).ExecuteAsCollection<ShoppingCartFreightsLogCollection>();
        }
        public void ShoppingCartFreightsLogSet(ShoppingCartFreightsLog log)
        {
            DB.Save<ShoppingCartFreightsLog>(log);
        }
        #endregion shopping_cart_freights_log

        #region wms_contract_log
        public WmsContractLogCollection GetWmsContractLogByAccountid(string accountId)
        {
            return DB.SelectAllColumnsFrom<WmsContractLog>()
                .Where(WmsContractLog.Columns.AccountId).IsEqualTo(accountId).ExecuteAsCollection<WmsContractLogCollection>();
        }
        public void WmsContractLogSet(WmsContractLog log)
        {
            DB.Save<WmsContractLog>(log);
        }
        #endregion Wms_contract_log



        public DataTable SellerTreeGetDataTableByRoot(Guid rootGuid)
        {
            string sql = @"
select st.parent_seller_guid, st.sort ,sl.GUID as seller_guid, 
sl.seller_id,seller_name, sl.temp_status, sl.store_status 
from seller_tree st with(nolock)
inner join seller sl with(nolock) on sl.GUID = st.seller_guid
where st.root_seller_guid = @root_seller_guid";
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("@root_seller_guid", rootGuid, DbType.Guid);
            DataTable dt = new DataTable();
            IDataReader reader = DataService.GetReader(qc);
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        public bool SellerSellerInfoIsExistsGet(Guid sellerGuid)
        {
            string sql = @"
            select ISNULL(len(seller_info), 0) from seller where guid = @sellerGuid";
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.Parameters.Add("@sellerGuid", sellerGuid, DbType.Guid);
            object obj = DataService.ExecuteScalar(qc);
            if (obj == null)
            {
                return false;
            }
            long result = Convert.ToInt64(obj);
            return result > 0;
        }

        public ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionBeforeBaseDate(DateTime baseDate)
        {
            return DB.SelectAllColumnsFrom<ViewDealSellerRelationshipPre>()
                .Where(ViewDealSellerRelationshipPre.Columns.SellerGuid).IsNotEqualTo(Guid.Empty)
                .And(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS).IsLessThan(baseDate.ToShortDateString())
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.SellerGuid)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeE)
                .ExecuteAsCollection<ViewDealSellerRelationshipPreCollection>();
        }

        public ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionByOneDays(DateTime baseDate)
        {
            var startDate = baseDate.ToShortDateString();
            var endDate = baseDate.AddDays(1).ToShortDateString();

            return DB.SelectAllColumnsFrom<ViewDealSellerRelationshipPre>()
                .Where(ViewDealSellerRelationshipPre.Columns.SellerGuid).IsNotEqualTo(Guid.Empty)
                .And(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS).IsGreaterThanOrEqualTo(startDate)
                .And(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS).IsLessThan(endDate)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.SellerGuid)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeE)
                .ExecuteAsCollection<ViewDealSellerRelationshipPreCollection>();
        }

        public ViewDealSellerRelationshipPreCollection GetAllViewDealSellerRelationshipPreCollectionBySeller(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<ViewDealSellerRelationshipPre>()
                .Where(ViewDealSellerRelationshipPre.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeS)
                .OrderAsc(ViewDealSellerRelationshipPre.Columns.BusinessHourOrderTimeE)
                .ExecuteAsCollection<ViewDealSellerRelationshipPreCollection>();
        }

        public bool TruncateAndBulkInsertBulkInsertDealSellerRelationshipCol(DealSellerRelationshipCollection col)
        {
            string sql = string.Format(@"truncate table " + DealSellerRelationship.Schema.TableName);
            QueryCommand qc = new QueryCommand(sql, "");
            DataService.ExecuteScalar(qc);

            return DB.BulkInsert(col);
        }

        public bool BulkInsertBulkInsertDealSellerRelationshipCol(DealSellerRelationshipCollection col)
        {
            return DB.BulkInsert(col);
        }


        #region vbs_confirm_notice_log
        public VbsConfirmNoticeLogCollection VbsConfirmNoticeLogGet(string accountId)
        {
            return DB.SelectAllColumnsFrom<VbsConfirmNoticeLog>().NoLock()
                 .Where(VbsConfirmNoticeLog.Columns.AccountId).IsEqualTo(accountId)
                 .OrderDesc(VbsConfirmNoticeLog.Columns.CreateTime)
                 .ExecuteAsCollection<VbsConfirmNoticeLogCollection>();
        }

        public void VbsConfirmNoticeLogSet(VbsConfirmNoticeLog info)
        {
            DB.Save(info);
        }
        #endregion vbs_confirm_notice_log
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;
using System.Web;


namespace LunchKingSite.SsBLL
{
    public class SSElmahProvider : IElmahProvider
    {
        public ElmahError ElmahGet(DateTime startedate, DateTime enddate)
        {
            string sql = "select Top 1 * from ELMAH_Error where TimeUtc between @startdate and @enddate order by TimeUtc desc";

            QueryCommand qc = new QueryCommand(sql, ElmahError.Schema.Provider.Name);
            ElmahError elmah = new ElmahError();
            qc.Parameters.Add("@startdate", startedate, System.Data.DbType.DateTime);
            qc.Parameters.Add("@enddate", enddate, System.Data.DbType.DateTime);
            elmah.LoadAndCloseReader(DataService.GetReader(qc));
            return elmah;

        }

        public ElmahErrorCollection ElmahGetLogBySource(string source, DateTime? startdate, DateTime? enddate, int pageStart, int pageLengh)
        {
            string sql = "select * from " + ElmahError.Schema.TableName + " with(nolock) where " + ElmahError.Columns.Source + " like @source and " + ElmahError.Columns.Type + "='Log' ";
            if (startdate.HasValue && enddate.HasValue)
            {
                sql += " and " + ElmahError.Columns.TimeUtc + ">=@startdate and " + ElmahError.Columns.TimeUtc + "<@enddate ";
            }
            QueryCommand qc = new QueryCommand(sql, ElmahError.Schema.Provider.Name);
            qc = SSHelper.MakePagable(qc, pageStart, pageLengh, ElmahError.Columns.TimeUtc + " desc");
            if (startdate.HasValue && enddate.HasValue)
            {
                qc.Parameters.Add("@startdate", startdate.Value, System.Data.DbType.DateTime);
                qc.Parameters.Add("@enddate", enddate.Value, System.Data.DbType.DateTime);
            }
            qc.Parameters.Add("@source", "%" + source + "%", System.Data.DbType.String);
            ElmahErrorCollection data = new ElmahErrorCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public int ElmahGetLogCountBySource(string source, DateTime? startdate, DateTime? enddate)
        {
            string sql = "select count(1) from " + ElmahError.Schema.TableName + " with(nolock) where " + ElmahError.Columns.Source + " like @source and " + ElmahError.Columns.Type + "='Log' ";
            if (startdate.HasValue && enddate.HasValue)
            {
                sql += " and " + ElmahError.Columns.TimeUtc + ">=@startdate and " + ElmahError.Columns.TimeUtc + "<@enddate ";
            }
            QueryCommand qc = new QueryCommand(sql, ElmahError.Schema.Provider.Name);
            if (startdate.HasValue && enddate.HasValue)
            {
                qc.Parameters.Add("@startdate", startdate.Value, System.Data.DbType.DateTime);
                qc.Parameters.Add("@enddate", enddate.Value, System.Data.DbType.DateTime);
            }
            qc.Parameters.Add("@source", "%" + source + "%", System.Data.DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }
    }
}

﻿using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;
using LunchKingSite.Core.ModelCustom;
using PaymentType = LunchKingSite.Core.PaymentType;

namespace LunchKingSite.SsBLL
{
    public class SSPponProvider : IPponProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly ILog log = LogManager.GetLogger(typeof(SSPponProvider));
        private static ISkmEfProvider skep = ProviderFactory.Instance().GetProvider<ISkmEfProvider>();

        #region BusinessHour

        public BusinessHour BusinessHourGet(Guid businessHourGuid)
        {
            return DB.Get<BusinessHour>(businessHourGuid);
        }

        public void BusinessHourSet(BusinessHour bh)
        {
            DB.Save(bh);
        }

        public List<Guid> BusinessHourGetBidsBySeller(Guid sellerGuid)
        {
            string sql = @"select " + BusinessHour.Columns.Guid + " from " + BusinessHour.Schema.TableName + " with(nolock) " +
                            " where " + BusinessHour.Columns.SellerGuid + "= @sellerGuid";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);

            List<Guid> result = new List<Guid>();
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }

            return result;
        }

        public DataTable BusinessHourGetExpireRedirectUrl(DateTime sDate, DateTime eDate)
        {
            string sql = @"SELECT bh.guid business_hour_guid,bh.business_hour_order_time_e,bh.expire_redirect_display,bh.expire_redirect_url,"
                        + " bh.use_expire_redirect_url_as_canonical "
                        + " FROM " + BusinessHour.Schema.TableName + " bh WITH(nolock) "
                        + " LEFT JOIN " + GroupOrder.Schema.TableName + " go WITH(nolock) ON go.business_hour_guid=bh.guid "
                        + " WHERE bh.business_hour_status&@BusinessHourStatus=0 "
                        + " and go.slug is not null"
                        + " and bh.business_hour_order_time_e>=@sDate "
                        + " and bh.business_hour_order_time_e<=@eDate "
                        + " and bh.expire_redirect_manual<>1 "
                        + " and (bh.expire_redirect_change_date is null or bh.expire_redirect_change_date<=@Date24H )"
                        + " order by bh.business_hour_order_time_e desc "
                        ;
            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("@BusinessHourStatus", (int)BusinessHourStatus.ComboDealSub, DbType.Int32);
            qc.AddParameter("@sDate", sDate.ToString("yyyy/MM/dd"), DbType.String);
            qc.AddParameter("@eDate", eDate.ToString("yyyy/MM/dd"), DbType.String);
            qc.AddParameter("@Date24H", DateTime.Now.AddDays(-1).ToString("yyyy/MM/dd"), DbType.String);

            IDataReader rdr = DataService.GetReader(qc);
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        #endregion BusinessHour

        #region Coupon

        public Coupon CouponGet(string column, object value)
        {
            return DB.Get<Coupon>(column, value);
        }

        public Coupon CouponGet(int id)
        {
            return DB.Get<Coupon>(id);
        }

        public CouponCollection CouponGet(List<Guid> trustIds)
        {
            CouponCollection cols = new CouponCollection();

            string sql = "select * from " + Coupon.Schema.TableName +
                " where " + Coupon.Columns.Id + " in " +
                "( select " + CashTrustLog.Columns.CouponId + " from " + CashTrustLog.Schema.TableName
                + " where " + CashTrustLog.Columns.TrustId + " in ('" + string.Join("','", trustIds) + " ') )";

            QueryCommand qc = new QueryCommand(sql, Coupon.Schema.Provider.Name);
            cols.LoadAndCloseReader(DataService.GetReader(qc));
            return cols;
        }

        public CouponCollection CouponGetList(Guid orderDetailId)
        {
            return DB.SelectAllColumnsFrom<Coupon>().Where(Coupon.OrderDetailIdColumn).IsEqualTo(orderDetailId).ExecuteAsCollection<CouponCollection>();
        }

        public CouponCollection CouponGetListWithNolock(Guid orderDetailId)
        {
            return DB.SelectAllColumnsFrom<Coupon>().NoLock().Where(Coupon.OrderDetailIdColumn).IsEqualTo(orderDetailId).ExecuteAsCollection<CouponCollection>();
        }

        public bool CouponSet(Coupon coupon)
        {
            DB.Save(coupon);

            return true;
        }

        public bool CouponSetBulk(CouponCollection couponCol)
        {
            QueryCommandCollection qcCol = new QueryCommandCollection();
            foreach (var c in couponCol)
            {
                qcCol.AddOrNot(SSHelper.GetQryCmd(c));
            }

            if (qcCol.Count > 0)
            {
                DataService.ExecuteTransaction(qcCol);
            }

            return true;
        }

        public string CouponGetMaxSequence(string bid)
        {
            QueryCommand qc = new QueryCommand("select max(convert(int,substring(" + ViewPponCoupon.Columns.SequenceNumber +
                ",0,(CHARINDEX('-'," + ViewPponCoupon.Columns.SequenceNumber + ",0))))) from " + ViewPponCoupon.Schema.TableName +
                " with(nolock) where business_hour_guid=@bid", ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.String);
            return DataService.ExecuteScalar(qc).ToString();
        }

        #region peztemp_batch_log

        public PeztempBatchLog PeztempBatchLogSave(PeztempBatchLog log)
        {
            DB.Save<PeztempBatchLog>(log);
            return log;
        }

        public int PeztempBatchLogSave(PeztempBatchLogCollection logs)
        {
            int result = 0;
            foreach (PeztempBatchLog log in logs)
            {
                result += DB.Save<PeztempBatchLog>(log);
            }
            return result;
        }

        public PeztempBatchLogCollection PeztempBatchLogGet(Guid bid, bool isBackupCode)
        {
            return DB.SelectAllColumnsFrom<PeztempBatchLog>().Where(PeztempBatchLog.Columns.Bid).IsEqualTo(bid.ToString())
                .And(PeztempBatchLog.Columns.IsBackup).IsEqualTo(isBackupCode)
                .ExecuteAsCollection<PeztempBatchLogCollection>();
        }

        public bool PeztempBatchLogDelete(int peztempBatchLogId)
        {
            DB.Delete<PeztempBatchLog>(PeztempBatchLog.Columns.Id, peztempBatchLogId);
            return true;
        }

        #endregion

        #region PezTemp

        public Peztemp PeztempGet(Guid orderDetailGuid, Guid businessHourGuid)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid)
                .And(Peztemp.Columns.Bid).IsEqualTo(businessHourGuid).ExecuteAsCollection<PeztempCollection>().FirstOrDefault();
        }
        public PeztempCollection PeztempGetByGid(Guid bid)
        {
            PeztempCollection pez = new PeztempCollection();
            string sql = "select * from " + Peztemp.Schema.TableName +
                " where " + Peztemp.Columns.Bid + "=@bid" +
                " and " + Peztemp.Columns.IsTest + "=0" +
                " and " + Peztemp.Columns.IsBackupCode + "=0" +
                " and " + Peztemp.Columns.OrderGuid + " is not null" +
                " order by " + Peztemp.IdColumn;
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            pez.LoadAndCloseReader(DataService.GetReader(qc));
            return pez;
        }

        //PEZ Event
        public Peztemp PeztempGet(Guid bid)
        {
            Peztemp pez = new Peztemp();
            string sql = "select top 1 * from " + Peztemp.Schema.TableName +
                " where " + Peztemp.Columns.Bid + "=@bid" +
                " and " + Peztemp.Columns.IsTest + "=0" +
                " and " + Peztemp.Columns.IsBackupCode + "=0" +
                " and " + Peztemp.Columns.OrderDetailGuid + " is null" +
                " order by " + Peztemp.IdColumn;
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            pez.LoadAndCloseReader(DataService.GetReader(qc));
            return pez;
        }

        public Peztemp PeztempGetByOrderDetailGuid(Guid order_detail_guid)
        {
            Peztemp pez = new Peztemp();
            string sql = "select top 1 * from " + Peztemp.Schema.TableName +
                " where " + Peztemp.Columns.OrderDetailGuid + "=@order_detail_guid" +
                " and " + Peztemp.Columns.IsTest + "=0" +
                " and " + Peztemp.Columns.IsBackupCode + "=0" +
                " order by " + Peztemp.IdColumn;
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@order_detail_guid", order_detail_guid, DbType.Guid);
            pez.LoadAndCloseReader(DataService.GetReader(qc));
            return pez;
        }

        //PEZ Event
        public Peztemp PeztempGet(Guid bid, string userid)
        {
            Peztemp pez = new Peztemp();
            string sql = "select top 1 * from " + Peztemp.Schema.TableName +
                " where " + Peztemp.Columns.Bid + "=@bid" +
                " and " + Peztemp.Columns.IsTest + "=0" +
                " and " + Peztemp.Columns.IsBackupCode + "=0" +
                " and " + Peztemp.Columns.OrderDetailGuid + " is null" +
                " and " + Peztemp.Columns.UserId + "=@userid" +
                " order by " + Peztemp.IdColumn;
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@userid", userid, DbType.String);
            pez.LoadAndCloseReader(DataService.GetReader(qc));
            return pez;
        }

        public Peztemp PeztempGet(string pezCode)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.PezCode).IsEqualTo(pezCode)
                .ExecuteAsCollection<PeztempCollection>().FirstOrDefault();
        }

        public Peztemp PeztempGetByCode(Guid orderDetailGuid, string pezCode)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().NoLock().Where(Peztemp.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid)
                .And(Peztemp.Columns.PezCode).IsEqualTo(pezCode)
                .ExecuteAsCollection<PeztempCollection>().FirstOrDefault() ?? new Peztemp();
        }

        public Peztemp PeztempGet(int peztempId)
        {
            return DB.Get<Peztemp>(peztempId);
        }

        public Peztemp PeztempPincodeGet(string pezCode, PeztempServiceCode serviceCode)
        {
            var pez = DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.ServiceCode).IsEqualTo((int)serviceCode)
                .And(Peztemp.Columns.PezCode).IsEqualTo(pezCode).ExecuteAsCollection<PeztempCollection>().FirstOrDefault();
            return pez ?? new Peztemp();
        }

        public PeztempCollection PeztempPincodeColGet(string pezCode, PeztempServiceCode serviceCode)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.ServiceCode).IsEqualTo((int)serviceCode)
                .And(Peztemp.Columns.PezCode).IsEqualTo(pezCode).ExecuteAsCollection<PeztempCollection>();
        }

        public Peztemp PeztempMakeTestCode(Guid bid)
        {
            string sql = @"Declare @key int;
                            Update dbo.peztemp set is_test = 1, @Key=[id]
                            where id = (Select top 1 id From peztemp where Bid=@bid
                            and OrderDetailGuid is null AND is_used = 0 AND is_test = 0 AND is_backup_code = 0);
                            Select @Key as 'peztempId';";

            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            int peztempId = (int)DataService.ExecuteScalar(qc);

            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.Id).IsEqualTo(peztempId)
                .ExecuteAsCollection<PeztempCollection>().FirstOrDefault();
        }

        /// <summary>
        /// 將特定筆數Peztemp更新為備用憑證
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public PeztempCollection PeztempMakeBackupCode(Guid bid, int batchId, int codeQty)
        {
            string sql = string.Format(@"Update dbo.peztemp set is_backup_code = 1 
                Where id in (Select top {0} id From dbo.peztemp 
                                where Bid=@bid And batch_id=@batchId AND OrderDetailGuid is null 
                                        AND is_test = 0 AND is_backup_code = 0)", codeQty);
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@batchId", batchId, DbType.Int32);
            DataService.ExecuteScalar(qc);

            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.Bid).IsEqualTo(bid.ToString())
                .And(Peztemp.Columns.BatchId).IsEqualTo(batchId)
                .And(Peztemp.Columns.IsBackupCode).IsEqualTo(true)
                .ExecuteAsCollection<PeztempCollection>();
        }

        /// <summary>
        /// 取得備用憑證
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="batchId"></param>
        /// <returns></returns>
        public PeztempCollection PeztempGetBackupCode(Guid bid, int batchId)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.Bid).IsEqualTo(bid.ToString())
                .And(Peztemp.Columns.BatchId).IsEqualTo(batchId)
                .And(Peztemp.Columns.IsBackupCode).IsEqualTo(true)
                .ExecuteAsCollection<PeztempCollection>();
        }

        public Peztemp PeztempGetTest(Guid bid)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.Columns.Bid).IsEqualTo(bid.ToString())
                .And(Peztemp.Columns.IsTest).IsEqualTo(true).And(Peztemp.Columns.IsUsed).IsEqualTo(false)
                .ExecuteAsCollection<PeztempCollection>().FirstOrDefault();
        }

        public PeztempCollection PeztempGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<Peztemp>().Where(Peztemp.BidColumn).IsEqualTo(bid).ExecuteAsCollection<PeztempCollection>();
        }

        public bool PeztempSet(Peztemp pez)
        {
            DB.Save(pez);
            return true;
        }

        public int PeztempGetCount(Guid bid, bool isused)
        {
            string sql = "select count(1) from " + Peztemp.Schema.TableName + " where " + Peztemp.Columns.Bid + " =@bid";
            if (isused)
            {
                sql += " and " + Peztemp.Columns.OrderDetailGuid + " is not null ";
            }

            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public int CvsStoreGet(string cvsstoreid)
        {
            string sql = "select id from " + CvsStore.Schema.TableName + " where " + CvsStore.Columns.CvsStoreId + " =@cvsstoreid";
            QueryCommand qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@cvsstoreid", cvsstoreid, DbType.String);
            var result = DataService.ExecuteScalar(qc);
            if (result == null)
            {
                return 0;
            }
            return (int)result;
        }

        public void PeztempUpdatePrintTime(string pezcode, DateTime time)
        {
            DB.Update<Peztemp>().Set(Peztemp.IsUsedColumn).EqualTo(true).Set(Peztemp.UsedTimeColumn).EqualTo(time)
              .Where(Peztemp.PezCodeColumn).IsEqualTo(pezcode).And(Peztemp.IsUsedColumn).IsEqualTo(false).And(Peztemp.OrderDetailGuidColumn).IsNotNull().Execute();
        }

        public void PeztempUpdateCvsId(string pezcode, int cvsid)
        {
            DB.Update<Peztemp>().Set(Peztemp.CvsIdColumn).EqualTo(cvsid)
              .Where(Peztemp.PezCodeColumn).IsEqualTo(pezcode).And(Peztemp.OrderDetailGuidColumn).IsNotNull().Execute();
        }

        public void PezErrorLogInsert(string pezcode, string cvsstoreid, int status)
        {
            string sql = @"INSERT INTO [peztemp_error_log]
           ([peztemp_id]
           ,[error_cvs_store_id]
           ,[status])
             select [id], @cvsstoreid, @status from [peztemp]
             where [pezcode] = @pezcode";
            QueryCommand qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@cvsstoreid", cvsstoreid, DbType.String);
            qc.AddParameter("@pezcode", pezcode, DbType.String);
            qc.AddParameter("@status", status, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public bool PezTempValidator(string pezcode)
        {
            string sql = @"SELECT count(0)
            FROM [peztemp]
            where pezcode = @pezcode and orderdetailguid is not null";
            QueryCommand qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@pezcode", pezcode, DbType.String);
            int result = (int)DataService.ExecuteScalar(qc);
            if (result > 0)
            {
                return true;
            }
            return false;
        }

        public void UpdateSellerCouponListExportLog(Guid bid, string username)
        {
            string sql = @"UPDATE [order] SET export_log=isnull(export_log,'')+@log WHERE
                                GUID IN (SELECT o.GUID from coupon c
                                join order_detail od with(nolock) on od.GUID=c.order_detail_id
                                join [order] o with(nolock) on o.GUID=od.order_GUID
                                join group_order gpo with(nolock) on gpo.order_guid=o.parent_order_id
                                join member m with(nolock) on m.unique_id=o.user_id
                                join coupon_event_content cec with(nolock) on cec.business_hour_guid=gpo.business_hour_guid
                                where cec.business_hour_guid=@bid and o.order_status & 512=0 and o.order_status & 8>0 and od.[status] & 2=0)";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@log", DateTime.Now.ToString("MM/dd HH:mm:ss_") + username + ",", DbType.String);
            qc.AddParameter("@bid", bid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public void PeztempCouponSet(PeztempCoupon peztempCoupon)
        {
            DB.Save<PeztempCoupon>(peztempCoupon);
        }

        public void PeztempCouponDelete(string pezCode, Guid businessHourGuid)
        {
            DB.Delete().From(PeztempCoupon.Schema.TableName).Where(PeztempCoupon.Columns.PezCode).IsEqualTo(pezCode)
                .And(PeztempCoupon.Columns.BusinessHourGuid).IsEqualTo(businessHourGuid.ToString()).Execute();
        }

        /// <summary>
        /// 批次新增Peztemp code, 上限50000, 如不明原因錯誤迴圈最多跑5萬次
        /// </summary>
        /// <param name="batchId"></param>
        /// <param name="bid"></param>
        /// <param name="serviceCode"></param>
        /// <param name="qty"></param>
        /// <param name="isBackupCode"></param>
        /// <returns>依 batch_id 回傳DB新增筆數</returns>
        public int PeztempSetWithVerification(int batchId, Guid bid, int serviceCode, int qty, bool isBackupCode = false)
        {
            const string sql = @"Declare @pezCnt int, @maxCnt int, @PezCode varchar(10), @peztempId int, @executeTime int, @idx int;
                            Select @pezCnt = 0, @maxCnt = @qty, @peztempId = 0, @executeTime = 0;
                            while @pezCnt < @maxCnt begin
	                            Set @PezCode = Right('0000000000' + convert(varchar, Convert(decimal , Round(RAND() * 9999999999, 0) + 1)), 10)
	                            INSERT INTO peztemp (PezCode, Bid, is_used, batch_id, service_code, is_backup_code)
		                            SELECT @PezCode, @Bid, 0, @batch_id, @service_code, @isBackupCode
		                            WHERE NOT EXISTS (SELECT 1 FROM peztemp WHERE PezCode = @PezCode
		                            AND status <> @PeztempStatus
		                            AND service_code = @service_code);
                                SELECT @idx=SCOPE_IDENTITY()
		
	                            if(@peztempId != @idx) begin
		                            Set @pezCnt = @pezCnt + 1
		                            SELECT @peztempId = @idx
	                            end
	                            Set @executeTime = @executeTime + 1
	                            if @executeTime > 50000 begin
		                            break;
	                            end
                            end;
                            Update dbo.peztemp_batch_log set complete_time = getdate() where id = @batch_id;
                            Select COUNT(1) from peztemp where batch_id = @batch_id And service_code = @service_code;
                            ";

            var qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@Bid", bid, DbType.Guid);
            qc.AddParameter("@batch_id", batchId, DbType.Int32);
            qc.AddParameter("@service_code", serviceCode, DbType.Int32);
            qc.AddParameter("@qty", qty, DbType.Int32);
            qc.AddParameter("@PeztempStatus", (int)PeztempStatus.Expired, DbType.Int32);
            qc.AddParameter("@isBackupCode", isBackupCode, DbType.Boolean);
            return (int)DataService.ExecuteScalar(qc);
        }

        public Peztemp PeztempGetWithDate(string pezCode, int serviceCode, DateTime deliverTimedate)
        {
            //被標記為測試序號的 peztemp 則不需檢查 business_hour_deliver_time_s 是否小於今天
            string sql = @" SELECT * FROM peztemp
                            WHERE PezCode = @PezCode
                            AND service_code = @serviceCode
                            AND status <> @status
                            AND ((is_test = 1 AND (
	                            SELECT DATEADD(dd, 1, bh.business_hour_deliver_time_s) FROM business_hour bh WHERE bh.GUID = peztemp.Bid) > @date
                            ) Or (Bid IN (
	                            SELECT b.GUID FROM business_hour b
	                            WHERE b.business_hour_deliver_time_s < @date
	                            AND b.business_hour_deliver_time_e >= @date))) ";
            QueryCommand qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@PezCode", pezCode, DbType.String);
            qc.AddParameter("@serviceCode", serviceCode, DbType.Int32);
            qc.AddParameter("@date", deliverTimedate, DbType.DateTime);
            qc.AddParameter("@status", (int)PeztempStatus.Expired, DbType.Int32);
            Peztemp pez = new Peztemp();
            pez.LoadAndCloseReader(DataService.GetReader(qc));
            return pez;
        }

        public void SetPeztempStatusExpiredWithDeliverTimeEndForFami(int service_code)
        {
            string sql = @" 
WHILE(1=1)
BEGIN

update top (10000) peztemp
        SET status = @status
        WHERE status <> @status
        AND Bid IN (
            SELECT b.GUID FROM business_hour b WITH(NOLOCK)
            JOIN group_order g WITH(NOLOCK) ON g.business_hour_guid = b.GUID
            WHERE g.status & " + (int)GroupOrderStatus.FamiDeal + @" > 0
            AND b.business_hour_deliver_time_e < @date
        ) AND service_code = @service_code 

  IF(@@rowCount = 0)
  BEGIN
    BREAK
  END

END

";
            QueryCommand qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.CommandTimeout = 600; //10分鍾
            qc.AddParameter("@status", (int)PeztempStatus.Expired, DbType.Int32);
            qc.AddParameter("@service_code", service_code, DbType.Int32);
            qc.AddParameter("@date", DateTime.Now.AddDays(-1), DbType.DateTime);
            DataService.ExecuteScalar(qc);
        }

        public int PeztempGetUsingMemberUserId(Peztemp pez)
        {
            string sql = @"select m.unique_id from peztemp p with(nolock)
                inner join [Order] o with(nolock)
                On p.order_guid = o.GUID
                inner join member m with(nolock)
                On m.unique_id = o.user_id
                where p.id = @peztempId";

            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@peztempId", pez.Id, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion

        public DataTable GetSellerCouponList(Guid bid)
        {
            string sql = @"select m.user_name, stuff(o.member_name,2,1,'*') as 姓名
                , cec.coupon_usage as 好康名稱
                , isnull(s.seller_name, '') as 分店名稱
                , REPLACE( REPLACE(od.item_name,convert(nvarchar(300),cec.coupon_usage),'') , '&nbsp', '') as 選項
                , c.sequence_number as 憑證編號
                , c.code as 使用者編號
                , Right('0000' + convert(varchar(10), c.store_sequence), 4) as 序號
                , o.order_id as 訂單編號 
                , s.Guid as 分店代碼
                , ct.status
                , ct.special_status
                , ct.modify_time
                from coupon c with(nolock)
                join order_detail od with(nolock) on od.GUID=c.order_detail_id
                join [order] o with(nolock) on o.GUID=od.order_GUID
                join group_order gpo with(nolock) on gpo.order_guid=o.parent_order_id
                join member m with(nolock) on m.unique_id=o.user_id
                join coupon_event_content cec with(nolock) on cec.business_hour_guid=gpo.business_hour_guid
                join cash_trust_log ct with(nolock) on ct.coupon_id=c.id
                left join seller s on s.guid = COALESCE(ct.verified_store_guid, ct.store_guid) 
                where cec.business_hour_guid=@bid and ct.status in(0,1,2,3,4) and od.[status]&2=0
                order by c.sequence_number,od.item_name";

            IDataReader rdr = new SubSonic.InlineQuery().ExecuteReader(sql, bid.ToString());
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        public DataTable GetSellerInvoiceList(Guid pid)
        {
            string sql = @"select invoice_no as '發票號碼',1 as '發票列次',
CONVERT(varchar(12),business_hour_order_time_e,111) as '發票日期',
case (CONVERT(nvarchar(max),(select value from SplitToTable(order_memo, '|') where id=2))) when '3'
	then (select value from SplitToTable(order_memo, '|') where id=4)
	else m.last_name+m.first_name end as '買受人',
case (CONVERT(nvarchar(max),(select value from SplitToTable(order_memo, '|') where id=2))) when '3'
	then (select value from SplitToTable(order_memo, '|') where id=3)
	else '' end as '統一編號',
'' as '產品編號', event_name as '產品品名',od.item_quantity as '產品數量','' as '單價', N'應稅' as '稅別',
case (CONVERT(nvarchar(max),(select value from SplitToTable(order_memo, '|') where id=2))) when '3'
	then round(amount/1.05,0)
	else amount end as '銷售金額',
case (CONVERT(nvarchar(max),(select value from SplitToTable(order_memo, '|') where id=2))) when '3'
	then round(amount/1.05*0.05,0)
	else 0 end as '營業稅',
amount as '總計金額', p.company_address as '地址',order_id as '訂單編號',
(select value from SplitToTable(order_memo, '|') where id=1) as '發票捐贈',
'' as '備註三', '' as '備註四','' as '備註五','' as '備註六', N'系統' as '登錄人',
m.last_name+m.first_name as '收件人', m.zip_code as '郵遞區號' from view_order_member_payment p
join view_ppon_deal d on p.parent_order_id=d.order_guid
join view_member_building_city m on m.user_name=p.user_name
join(
	select order_GUID, sum(item_quantity) as item_quantity from order_detail
	where status&2<>2 group by order_GUID
)od on od.order_GUID=p.order_guid
where p.order_status&576=0 and parent_order_id=@pid and payment_type=@ptype and trans_type=@ttype and payment_status&7=3
order by CONVERT(int, CONVERT(nvarchar(max),(select value from SplitToTable(order_memo, '|') where id=1))) desc, order_id";

            IDataReader rdr = new SubSonic.InlineQuery().ExecuteReader(sql, pid.ToString(), (int)PaymentType.Creditcard, (int)PayTransType.Charging);
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        public void UpdateGetSellerItemListExportLog(Guid bid, string orderTimeStart, string orderTimeEnd, string username)
        {
            string sql = @"UPDATE [order] SET export_log=isnull(export_log,'')+@log WHERE
                                GUID IN (SELECT o.GUID
                                from  order_detail od
                                join [order] o on o.GUID=od.order_GUID
                                join group_order gpo on gpo.order_guid=o.parent_order_id
                                join member m on m.unique_id=o.user_id
                                join coupon_event_content cec on cec.business_hour_guid=gpo.business_hour_guid
                                where cec.business_hour_guid=@bid and o.order_status & 584=8 and od.status & 2<>2 ";
            if (!string.IsNullOrEmpty(orderTimeStart) && !string.IsNullOrEmpty(orderTimeEnd))
            {
                sql += @"AND o.create_time BETWEEN @orderTimeStart AND @orderTimeEnd ";
            }

            sql += ")";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@log", DateTime.Now.ToString("MM/dd HH:mm:ss_") + username + ",", DbType.String);
            qc.AddParameter("@bid", bid, DbType.Guid);
            if (!string.IsNullOrEmpty(orderTimeStart) && !string.IsNullOrEmpty(orderTimeEnd))
            {
                qc.AddParameter("@orderTimeStart", orderTimeStart, DbType.DateTime);
                qc.AddParameter("@orderTimeEnd", orderTimeEnd, DbType.DateTime);
            }
            DataService.ExecuteScalar(qc);
        }

        public DataTable GetSellerItemList(Guid bid, string orderTimeStart, string orderTimeEnd)
        {
            string sql = @" SELECT ROW_NUMBER() OVER (ORDER BY 訂單編號) as 序號, *
                            FROM    (
                                select o.order_id as 訂單編號
                                     , replace(replace( REPLACE(od.item_name,convert(nvarchar(300),cec.coupon_usage),''),N'',''),'&nbsp','') as 選項
                                     , m.last_name+m.first_name as 訂購人
                                     , o.member_name as 收件人
                                     , o.mobile_number as 收件人電話
                                     , od.item_quantity as 數量
, STUFF((SELECT ',' + options+'*'+cast(COUNT(options) as VARCHAR(5))
            FROM   order_detail_item where order_detail_guid=od.GUID
            group by options
            FOR XML PATH('')),1,1,'') as 數量統計
                                     , o.delivery_address as 配送地址
                                     , o.user_memo as 備註
                                     , '' as 出貨日期
                                     , '' as 貨運編號
                                     , '' as 物流公司
                                from  order_detail od
                                join [order] o on o.GUID=od.order_GUID
                                join group_order gpo on gpo.order_guid=o.parent_order_id
                                join member m on m.unique_id=o.user_id
                                join coupon_event_content cec on cec.business_hour_guid=gpo.business_hour_guid
                                where cec.business_hour_guid=@bid and o.order_status & 584=8 and od.status & 2<>2 ";

            if (!string.IsNullOrEmpty(orderTimeStart) && !string.IsNullOrEmpty(orderTimeEnd))
            {
                sql += @"AND o.create_time BETWEEN @orderTimeStart AND @orderTimeEnd ";
            }

            sql += @" ) A order by 訂單編號";

            DataTable dt = new DataTable();
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);

            if (!string.IsNullOrEmpty(orderTimeStart) && !string.IsNullOrEmpty(orderTimeEnd))
            {
                qc.AddParameter("@orderTimeStart", orderTimeStart, DbType.DateTime);
                qc.AddParameter("@orderTimeEnd", orderTimeEnd, DbType.DateTime);
            }

            using (IDataReader reader = DataService.GetReader(qc))
            {
                dt.Load(reader);
            }
            return dt;
        }

        public DataTable GetComReport(Guid bid)
        {
            string sql = @"select
m.last_name+m.first_name as 訂購人
,m.user_name as 訂購人email
,o.member_name as 收件人
,o.delivery_address as 收件地址
,od.item_quantity as 數量
,o.total as 價格
,case when
(select COUNT(1) from member_link ml with(nolock) where ml.user_name=m.user_name)>1
then
	(select ml.external_user_id from member_link ml with(nolock) where ml.user_id=m.unique_id and ml.external_org=0)
else
	case when
	 (select ml.external_org from member_link ml with(nolock) where ml.user_id=m.unique_id)=1
	 then 'FB' else (select ml.external_user_id from member_link ml with(nolock) where ml.user_id=m.unique_id and ml.external_org=0) end
end
 AS '串接編號'
--,case when pt.payment_type =1 then 'CreditCard' else 'PCash' end as 付款方式
,odB.total as 紅利金額
,(select pt_p.amount from payment_transaction pt_p with(nolock) where pt_p.payment_type=2 and pt_p.order_guid=o.GUID and pt_p.trans_type=0) as Pcash金額
,(select pt_c.amount from payment_transaction pt_c with(nolock) where pt_c.payment_type=1 and pt_c.order_guid=o.GUID and pt_c.trans_type=0) as CC金額
from order_detail od
join [order] o with(nolock) on o.GUID=od.order_GUID
join [order] op with(nolock) on op.GUID=o.parent_order_id
join group_order gpo with(nolock) on gpo.order_guid=op.GUID
join member m with(nolock) on m.unique_id=o.user_id
join building b with(nolock) on b.GUID=m.building_GUID
join city c with(nolock) on c.id=b.city_id
--join payment_transaction pt on pt.order_guid=o.GUID
left join order_detail odB with(nolock) on odB.order_GUID=o.GUID and odB.item_name like N'%紅利%'
where gpo.business_hour_guid=@bid and o.order_status & 584=8
 and od.status & 2=0";

            IDataReader rdr = new SubSonic.InlineQuery().ExecuteReader(sql, bid.ToString());
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        //後台取P完美清單 2010/11/12 40
        public DataTable GetPeautySellerItemList(Guid bid, string itemName)
        {
            string sql = @"select case when
(select COUNT(1) from member_link ml with(nolock) where ml.user_id=m.unique_id)>1
then 'PEZ' else case when (select ml.external_org from member_link ml with(nolock) where ml.user_id=m.unique_id)=1
then 'FB' else 'PEZ' end end AS '串接類型',
case when (select COUNT(1) from member_link ml with(nolock) where ml.user_id=m.unique_id)>1
then (select ml.external_user_id from member_link ml with(nolock) where ml.user_id=m.unique_id and ml.external_org=0)
else case when (select ml.external_org from member_link ml with(nolock) where ml.user_id=m.unique_id)=1
then 'FB' else (select ml.external_user_id from member_link ml with(nolock) where ml.user_id=m.unique_id and ml.external_org=0) end
end AS '串接編號',o_b.member_name as '收件人',od.item_quantity as '數量',od.item_name as '訂購品項',
REPLACE(od.item_name,@itemName,'') as '商品選項',o_b.phone_number as '電話',m.mobile as '手機',c.zip_code as '郵遞區號',
o_b.delivery_address as  '配送地址',o_b.order_id from order_detail od
join [order] o_b with(nolock) on o_b.GUID=od.order_GUID
join [order] op_b with(nolock) on o_b.parent_order_id=op_b.GUID
join group_order gpo_b with(nolock) on gpo_b.order_guid=op_b.GUID
join member m with(nolock) on m.unique_id=o_b.user_id
join building b with(nolock) on b.GUID=m.building_GUID
join city c with(nolock) on c.id=b.city_id
where od.status & 2<>2 and gpo_b.business_hour_guid=@bid
and o_b.order_status & 584=8 ";
            DataTable dt = new DataTable();
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@itemName", itemName, DbType.String);
            qc.AddParameter("@bid", bid, DbType.Guid);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                dt.Load(reader);
            }
            return dt;
        }

        public bool UnlockCouponStatusByCouponIds(List<int> couponIds)
        {
            return DB.Update<Coupon>().Set(Coupon.Columns.Status).EqualTo(CouponStatus.None)
                     .Where(Coupon.Columns.Status).IsEqualTo(CouponStatus.Locked)
                     .And(Coupon.Columns.Id).In(couponIds)
                     .Execute() > 0;
        }
        #endregion Coupon

        #region CouponEventContent

        public bool CouponEventContentSet(CouponEventContent cec)
        {
            cec.ModifyTime = DateTime.Now;
            DB.Save(cec);

            return true;
        }

        public CouponEventContent CouponEventContentGet(string column, object value)
        {
            return DB.Get<CouponEventContent>(column, value);
        }

        public CouponEventContent CouponEventContentGetByBid(Guid bid)
        {
            return DB.Get<CouponEventContent>(CouponEventContent.Columns.BusinessHourGuid, bid);
        }

        public CouponEventContent CouponEventContentGetBySubDealBid(Guid subDealBid)
        {
            SqlQuery sub = new Select(ComboDeal.Columns.MainBusinessHourGuid).From<ComboDeal>()
                                                                           .Where(ComboDeal.Columns.BusinessHourGuid)
                                                                           .IsEqualTo(subDealBid);

            return DB.SelectAllColumnsFrom<CouponEventContent>()
                     .Where(CouponEventContent.BusinessHourGuidColumn)
                     .In(sub)
                     .ExecuteSingle<CouponEventContent>();
        }

        public Item ItemGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<Item>().Where(Item.BusinessHourGuidColumn).In(bid)
                   .ExecuteSingle<Item>();
        }
        public Item ItemGetBySubDealBid(Guid subDealBid)
        {
            SqlQuery sub = new Select(ComboDeal.Columns.MainBusinessHourGuid).From<ComboDeal>()
                                                                           .Where(ComboDeal.Columns.BusinessHourGuid)
                                                                           .IsEqualTo(subDealBid);

            return DB.SelectAllColumnsFrom<Item>()
                     .Where(Item.BusinessHourGuidColumn)
                     .In(sub)
                     .ExecuteSingle<Item>();
        }

        public void CouponEventContentUpdatePic(string image_path, string special_image_path, string travel_edm_special_image_path, Guid bid)
        {
            string sql = "update " + CouponEventContent.Schema.TableName + " set " + CouponEventContent.Columns.ImagePath + "=@image_path,"
                + CouponEventContent.Columns.SpecialImagePath + "=@special_image_path,"
                + CouponEventContent.Columns.TravelEdmSpecialImagePath + "=@travel_edm_special_image_path where "
                + CouponEventContent.Columns.BusinessHourGuid + "=@bid";
            QueryCommand qc = new QueryCommand(sql, CouponEventContent.Schema.Provider.Name);
            qc.AddParameter("@image_path", image_path, DbType.String);
            qc.AddParameter("@special_image_path", special_image_path, DbType.String);
            qc.AddParameter("@travel_edm_special_image_path", travel_edm_special_image_path, DbType.String);
            qc.AddParameter("@bid", bid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public void CouponEventContentUpdateDescription(string description, int id)
        {
            DB.Update<CouponEventContent>().Set(CouponEventContent.Columns.Description).EqualTo(description).Where(CouponEventContent.Columns.Id).IsEqualTo(id).Execute();
        }

        public void CouponEventContentUpdateRemark(string remark, int id)
        {
            DB.Update<CouponEventContent>().Set(CouponEventContent.Columns.Remark).EqualTo(remark).Where(CouponEventContent.Columns.Id).IsEqualTo(id).Execute();
        }

        #endregion CouponEventContent

        #region ProposalCouponEventContent
        public ProposalCouponEventContent ProposalCouponEventContentGetByPid(int pid)
        {
            return DB.Get<ProposalCouponEventContent>(ProposalCouponEventContent.Columns.ProposalId, pid);
        }

        public List<int> ProposalCouponEventContentGetByAppTitle(string appTitle)
        {
            return DB.Select(ProposalCouponEventContent.Columns.ProposalId)
                .From<ProposalCouponEventContent>().NoLock()
                .Where(ProposalCouponEventContent.Columns.AppTitle).Like("%" + appTitle + "%")
                .ExecuteTypedList<int>();
        }

        public bool ProposalCouponEventContentSet(ProposalCouponEventContent pcec)
        {
            DB.Save(pcec);

            return true;
        }

        public bool ProposalCouponEventContentDelete(int pid)
        {
            DB.Destroy<ProposalCouponEventContent>(ProposalCouponEventContent.Columns.ProposalId, pid);
            return true;
        }
        #endregion

        #region CouponFreight

        public bool CouponFreightSetList(CouponFreightCollection couponFreightList)
        {
            DB.SaveAll<CouponFreight, CouponFreightCollection>(couponFreightList);
            return true;
        }

        public bool CouponFreightDeleteListByBid(Guid businessHourGuid)
        {
            var queries = new List<SqlQuery>();
            queries.Add(
                new Delete().From(CouponFreight.Schema).Where(CouponFreight.BusinessHourGuidColumn).IsEqualTo(
                    businessHourGuid));
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }

        public CouponFreightCollection CouponFreightGetList(Guid businessHourGuid)
        {
            return DB.SelectAllColumnsFrom<CouponFreight>()
                .Where(CouponFreight.BusinessHourGuidColumn).IsEqualTo(businessHourGuid).ExecuteAsCollection
                <CouponFreightCollection>();
        }

        public CouponFreightCollection CouponFreightGetList(Guid businessHourGuid, CouponFreightType type)
        {
            return DB.SelectAllColumnsFrom<CouponFreight>()
                .Where(CouponFreight.BusinessHourGuidColumn).IsEqualTo(businessHourGuid)
                .And(CouponFreight.FreightTypeColumn).IsEqualTo(type)
                .ExecuteAsCollection<CouponFreightCollection>();
        }

        #endregion CouponFreight

        #region DealProperty

        public bool DealPropertySet(DealProperty dealProperty)
        {
            DB.Save<DealProperty>(dealProperty);
            return true;
        }

        public DealProperty DealPropertyGet(Guid bid)
        {
            return DB.Get<DealProperty>(bid);
        }

        public DealPropertyCollection DealPropertyListGet(List<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<DealProperty>().Where(DealProperty.BusinessHourGuidColumn.ColumnName).In(bids).ExecuteAsCollection<DealPropertyCollection>();
        }

        public DealProperty DealPropertyGet(int uniqueId)
        {
            return DB.Get<DealProperty>(DealProperty.Columns.UniqueId, uniqueId);
        }
        public DealPropertyCollection DealPropertyGet(IEnumerable<int> uniqueId)
        {
            return DB.SelectAllColumnsFrom<DealProperty>().Where(DealProperty.UniqueIdColumn.ColumnName).In(uniqueId).ExecuteAsCollection<DealPropertyCollection>();
        }

        public DealProperty DealPropertyGetByOrderGuid(Guid orderGuid)
        {
            string strSQL = @"select d.* from deal_property d
            inner join group_order go on go.business_hour_guid = d.business_hour_guid
            inner join [order] o on o.parent_order_id = go.order_guid
            where o.GUID = @orderGuid";
            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("orderGuid", orderGuid, DbType.Guid);

            DealPropertyCollection dpCol = new DealPropertyCollection();
            dpCol.LoadAndCloseReader(DataService.GetReader(qc));
            return dpCol.FirstOrDefault();
        }

        public List<Guid> ConsignmentDealPropertyGet(bool IsConsignment)
        {
            string strSQL = @"select " + DealProperty.Columns.BusinessHourGuid + " from " + DealProperty.Schema.TableName + " with(nolock) where " + DealProperty.Columns.Consignment + "= @IsConsignment";
            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("IsConsignment", IsConsignment, DbType.Boolean);

            List<Guid> result = new List<Guid>();
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }

            return result;
        }

        public DealPropertyCollection DealPropertyGetSubDealIsPackSet(Guid mainDealGuid)
        {
            //檔次資料ComboPackCount欄位若大於1且為購物車結構，表示此檔次為成套販售的檔次
            DealPropertyCollection items = new Select().From<DealProperty>()
       .Where(DealProperty.Columns.BusinessHourGuid).In(new Select(ComboDeal.Columns.BusinessHourGuid).From<ComboDeal>().Where(ComboDeal.Columns.MainBusinessHourGuid).IsEqualTo(mainDealGuid))
       .And(DealProperty.Columns.ShoppingCart).IsEqualTo(true)
       .And(DealProperty.Columns.ComboPackCount).IsGreaterThan(1)
       .ExecuteAsCollection<DealPropertyCollection>();
            return items;
        }

        /// <summary>
        /// 取得接續某檔的所有檔次資料
        /// </summary>
        /// <param name="bid">被接續的檔次 bid</param>
        /// <returns></returns>
        public DealPropertyCollection DealPropertyGetByAncestor(Guid bid)
        {
            string strSQL = " select * from " + DealProperty.Schema.Provider.DelimitDbName(DealProperty.Schema.TableName) +
                            " with(nolock) where " + DealProperty.Columns.AncestorBusinessHourGuid + " = @Bid ";

            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("Bid", bid, DbType.Guid);

            DealPropertyCollection dpCol = new DealPropertyCollection();
            dpCol.LoadAndCloseReader(DataService.GetReader(qc));
            return dpCol;
        }

        public DealPropertyCollection DealPropertyGetByDate(DateTime dateStart, DateTime dateEnd)
        {
            string sql = @"
                select * from deal_property dp with(nolock)
                where dp.business_hour_guid in 
	                (select distinct business_hour_guid from deal_time_slot with(nolock) where 
		            effective_start >= @dateStart and effective_start < @dateEnd)
            ";

            QueryCommand qc = new QueryCommand(sql, DealProperty.Schema.Provider.Name);
            qc.AddParameter("dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("dateEnd", dateEnd, DbType.DateTime);
            DealPropertyCollection dpCol = new DealPropertyCollection();
            dpCol.LoadAndCloseReader(DataService.GetReader(qc));
            return dpCol;
        }

        public List<int> RecentOrderDealTypeGetByUserId(int userId,Guid orderGuid)
        {
            string sql = @"select dp.deal_type from [order] o 
                          inner join group_order g on o.parent_order_id = g.order_guid
                          inner join deal_property dp on dp.business_hour_guid = g.business_hour_guid
                          where user_id = @userId and o.guid!=@orderGuid 
                          and o.create_time between DATEADD(hour, -1, getdate()) and getdate() ";
            QueryCommand qc = new QueryCommand(sql, DealProperty.Schema.Provider.Name);
            qc.AddParameter("@userId", userId,DbType.Int32);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);

            List<int> result = new List<int>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while(reader.Read())
                {
                    result.Add(reader.GetInt32(0));
                }
            }

            return result;

        }



        #endregion DealProperty

        #region DealPayment
        public bool DealPaymentSet(DealPayment dealPayment)
        {
            DB.Save<DealPayment>(dealPayment);
            return true;
        }
        public DealPayment DealPaymentGet(Guid bid)
        {
            return DB.Get<DealPayment>(DealPayment.Columns.BusinessHourGuid, bid);
        }
        public DealPaymentCollection DealPaymentsGet(IEnumerable<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<DealPayment>().NoLock()
                .Where(DealPayment.Columns.BusinessHourGuid)
                .In(bids).ExecuteAsCollection<DealPaymentCollection>();
        }
        #endregion

        #region ImgPool
        public bool ImgPoolSet(ImgPool imgPool)
        {
            DB.Save<ImgPool>(imgPool);
            return true;
        }

        public ImgPool ImgPoolGet(Guid bid, string path)
        {
            var data = DB.SelectAllColumnsFrom<ImgPool>().Where(ImgPool.Columns.Bid).IsEqualTo(bid).And(ImgPool.Columns.Path).IsEqualTo(path);
            return data.ExecuteSingle<ImgPool>();
        }

        public void ImgPoolDeleteByBid(Guid bid)
        {
            string sql = "delete from " + ImgPool.Schema.TableName + " where " + ImgPool.Columns.Bid + "=@bid";
            QueryCommand qc = new QueryCommand(sql, ImgPool.Schema.Provider.Name);
            qc.Parameters.Add("@bid", bid, DbType.Guid);
            DataService.ExecuteQuery(qc);
        }

        public ImgPoolCollection ImgPoolGetAllBybid(Guid bid)
        {
            var data = DB.SelectAllColumnsFrom<ImgPool>().NoLock()
                .Where(ImgPool.Columns.Bid).IsEqualTo(bid);
            return data.ExecuteAsCollection<ImgPoolCollection>();
        }

        public ImgPoolCollection ImgPoolGetListByDayWithNoLock(DateTime sDate, DateTime eDate)
        {
            string sql = @"
                    select * from img_pool with(nolock)
                    where bid
                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    union
	                    select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
	                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    )
	                    and BusinessHourGuid <> MainBusinessHourGuid
                    )";

            var qc = new QueryCommand(sql, ImgPool.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            var ImgPoolCol = new ImgPoolCollection();
            ImgPoolCol.LoadAndCloseReader(DataService.GetReader(qc));

            return ImgPoolCol;
        }

        public int ImgPoolDelete(int id)
        {
            return DB.Delete().From(ImgPool.Schema.TableName).Where(ImgPool.Columns.Id).IsEqualTo(id).Execute();
        }
        #endregion

        #region PponDeal

        //TODO 或許該移到 PponFacade
        public PponDeal PponDealGet(Guid bid)
        {
            PponDeal result = new PponDeal();
            BusinessHour deal = BusinessHourGet(bid);
            if (deal.IsLoaded)
            {
                result.Deal = deal;
                result.Store = ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGetByBid(bid);
                result.ItemDetail = ProviderFactory.Instance().GetProvider<IItemProvider>().ItemGetByBid(bid);
                result.DealContent = CouponEventContentGetByBid(bid);
                result.Property = DealPropertyGet(bid);
                result.Payment = DealPaymentGet(bid);

                //DEAL的上檔時程資料
                result.TimeSlotCollection = DealTimeSlotGetList("", DealTimeSlot.Columns.BusinessHourGuid + "=" + bid);

                IItemProvider ip = ProviderFactory.Instance().GetProvider<IItemProvider>();
                result.AccessoryGroups = ip.AccessoryGroupGetListBySeller(result.Store.Guid);
                result.ItemAccessory = ip.ItemAccessoryGroupListGetListByItem(result.ItemDetail.Guid);
            }
            return result;
        }

        public bool PponDealSet(PponDeal data, bool syncOrderableWithin, DealTimeSlotCollection origDtsCol = null)
        {
            QueryCommandCollection qcCol = new QueryCommandCollection();

            if (data.Store != null && (data.Store.IsLoaded || data.Store.IsNew))
            {
                //we hack informally here for column "coordinate" and should be normalized at later time.
                QueryCommand qc = SSHelper.GetQryCmd(data.Store);
                if (qc != null)
                {
                    int idx = qc.Parameters.FindIndex(x => x.ParameterName == Seller.CoordinateColumn.ParameterName);
                    if (idx > 0)
                    {
                        qc.Parameters[idx].ParameterValue = qc.Parameters[idx].ParameterValue == DBNull.Value
                                                            ? "POINT EMPTY"
                                                            : qc.Parameters[idx].ParameterValue.ToString();
                    }

                    qcCol.Add(qc);
                }
            }

            if (data.Deal == null)
            {
                return false;
            }

            qcCol.AddOrNot(SSHelper.GetQryCmd(data.Deal));

            if (data.DealContent != null)
            {
                qcCol.AddOrNot(SSHelper.GetQryCmd(data.DealContent));
            }

            if (data.ItemDetail != null)
            {
                qcCol.AddOrNot(SSHelper.GetQryCmd(data.ItemDetail));
            }

            DealTimeSlotCollection insertDealTimeSlotCol = new DealTimeSlotCollection();

            if (syncOrderableWithin && data.TimeSlotCollection != null)
            {
                if (origDtsCol == null) origDtsCol = new DealTimeSlotCollection();
                insertDealTimeSlotCol.AddRange(data.TimeSlotCollection.Except(origDtsCol));
                List<DealTimeSlot> deleteCol = origDtsCol.Except(data.TimeSlotCollection).ToList();

                foreach (var del in deleteCol)
                {
                    var qcDel = new QueryCommand(@"DELETE FROM deal_time_slot 
                        WHERE business_hour_guid=@bhGuid AND city_id=@cityId AND effective_start=@effectiveStart",
                              DealTimeSlot.Schema.Provider.Name);
                    qcDel.AddParameter("@bhGuid", del.BusinessHourGuid, DbType.Guid);
                    qcDel.AddParameter("@cityId", del.CityId, DbType.Int32);
                    qcDel.AddParameter("@effectiveStart", del.EffectiveStart, DbType.DateTime);
                    qcCol.Add(qcDel);
                }
            }

            bool ret = true;
            try
            {
                using (TransactionScope scope = TransactionScopeBuilder.CreateReadCommitted())
                {
                    foreach (QueryCommand cmd in qcCol)
                    {
                        DB.Execute(cmd);
                    }

                    if (config.AWSAutoSync)
                    {
                        DealCloudStorage cloudStorage = this.DealCloudStorageGet(data.Deal.Guid);
                        cloudStorage.BusinessHourGuid = data.Deal.Guid;
                        cloudStorage.S3MarkSync = true;
                        cloudStorage.ModifyId = string.Empty;
                        if (data.DealContent != null)
                        {
                            cloudStorage.ModifyTime = data.DealContent.ModifyTime;
                        }
                        this.DealCloudStorageSet(cloudStorage);
                    }

                    if (insertDealTimeSlotCol.Any())
                    {
                        DB.BulkInsert(insertDealTimeSlotCol);
                    }
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                log.Error("Exception occured on executing PponDealSet transaction:\n" + data, ex);
                ret = false;
            }

            return ret;
        }

        [Obsolete]
        public bool PponDealDelete(Guid bizHour)
        {
            throw new NotSupportedException("PponDealDelete 暫時不提供刪除功能, 如有疑問請洽IT");
            /*
            string sql = @"
delete a from coupon a inner join order_detail b on a.order_detail_id=b.guid inner join [order] c on b.order_guid=c.guid inner join group_order d on c.parent_order_id=d.order_guid where d.business_hour_guid=@bhGuid;
delete from order_detail where order_guid in (select a.guid from [order] a inner join group_order b on a.parent_order_id=b.order_guid where b.business_hour_guid=@bhGuid);
delete from payment_transaction where order_guid in (select a.GUID from [order] a inner join group_order b on a.parent_order_id=b.order_guid where b.business_hour_guid=@bhGuid);
delete from payment_transaction where order_guid in (select a.GUID from [order] a inner join group_order b on a.guid=b.order_guid where b.business_hour_guid=@bhGuid);
delete a from [order] a inner join group_order b on a.parent_order_id=b.order_guid where b.business_hour_guid=@bhGuid;
delete a from order_detail a inner join group_order b on a.order_guid=b.order_guid where b.business_hour_guid=@bhGuid;
declare @oid uniqueidentifier; select @oid=order_guid from group_order where business_hour_guid=@bhGuid;
delete from group_order where business_hour_guid=@bhGuid;
delete from [order] where guid=@oid;
delete from coupon_event_content where business_hour_guid=@bhGuid;
delete a from item_quantity a inner join item b on a.item_guid=b.guid where b.menu_guid in (select menu_GUID from business_hour_menu_list where business_hour_guid=@bhGuid);
delete a from shopping_cart a inner join item b on a.item_guid=b.guid where b.menu_guid in (select menu_GUID from business_hour_menu_list where business_hour_guid=@bhGuid);
delete from item where menu_GUID in (select menu_GUID from business_hour_menu_list where business_hour_guid=@bhGuid);
select menu_guid into #temp2 from business_hour_menu_list where business_hour_guid=@bhGuid;
delete from business_hour_menu_list where menu_guid in (select menu_guid from #temp2)
delete from itemmenu where GUID in (select menu_guid from #temp2);
drop table #temp2;
delete deal_property where business_hour_guid=@bhGuid
delete from business_hour where GUID=@bhGuid;
";
            QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
            qc.AddParameter("@bhGuid", bizHour, DbType.Guid);
            DataService.ExecuteTransaction(new QueryCommandCollection() { qc });

            return true;
            */
        }

        public bool PponDealExtendAfterClose(Guid bid)
        {
            ViewPponDeal vpd = ViewPponDealGet(ViewPponDeal.Columns.BusinessHourGuid + "=" + bid);
            int statusFlags = (int)(GroupOrderStatus.PponProcessed | GroupOrderStatus.Completed);

            if (!vpd.GroupOrderStatus.HasValue)
            {
                return false;
            }

            if (((vpd.GroupOrderStatus.Value & statusFlags) != statusFlags))
            {
                return false;
            }

            if ((int.Parse(vpd.Slug) >= (int)vpd.BusinessHourOrderMinimum)
                || Helper.IsFlagSet(vpd.BusinessHourStatus, BusinessHourStatus.ComboDealMain)
                || int.Parse(vpd.Slug) == 0)
            {

                var status = Helper.SetFlag(false, vpd.GroupOrderStatus.Value, statusFlags);

                new Update(GroupOrder.Schema).Set(GroupOrder.StatusColumn).EqualTo(status).Set(GroupOrder.SlugColumn).EqualTo(null)
                    .Where(GroupOrder.BusinessHourGuidColumn).IsEqualTo(bid).Execute();

                new Update(DealAccounting.Schema).Set(DealAccounting.ShippedDateColumn).EqualTo(null)
                    .Where(DealAccounting.BusinessHourGuidColumn).IsEqualTo(bid).Execute();

                new Update(DealAccounting.Schema).Set(DealAccounting.FinalBalanceSheetDateColumn).EqualTo(null)
                    .Where(DealAccounting.BusinessHourGuidColumn).IsEqualTo(bid).Execute();

                return true;
            }

            return false;
        }

        #region EDM用

        public DataTable RemittanceTypeCheck()
        {
            string sql = @"IF OBJECT_ID('tempdb..#Cost') is not null DROP TABLE #Cost
select business_hour_guid, cost, quantity
into #Cost from deal_cost with(nolock)
where id in(
	select MIN(id) from deal_cost with(nolock)
	group by business_hour_guid
)

select (select case when count(*)>0 then '*' else '' end from combo_deals WITH(NOLOCK) where MainBusinessHourGuid = d.business_hour_guid) as IsMainDeal,
isnull((select MainBusinessHourGuid from combo_deals WITH(NOLOCK) where BusinessHourGuid = d.business_hour_guid),d.business_hour_guid) as MainBid,
d.seller_id ,business_hour_order_time_s ,d.business_hour_guid,CASE WHEN (select count(*) from deal_cost WITH(NOLOCK) where business_hour_guid = d.business_hour_guid) = 1 THEN 0 ELSE c.quantity END as quantity,
p.unique_id ,'' as BusinessOrderId, a.sales_id ,
case when p.delivery_type=1 then N'憑證'
	  when p.delivery_type=2 then N'宅配'
 end as DeliveryType,g.acc_business_group_name ,dp.dept_name as AuthorizationGroup,
case when a.paytocompany = 0 then N'匯至各分店匯款資料' else N'統一匯至賣家匯款資料' end as paytocompany,
case when vendor_billing_model=0 then N'舊版'
when vendor_billing_model=1 then N'新版'
when vendor_billing_model=2 then N'墨攻'
end as VendorBillingModel,
case when remittance_type=0 then N'其他付款方式' 
  	  when remittance_type=1 then N'ACH每周付款'
	  when remittance_type=2 then N'ACH每月付款'
	  when remittance_type=3 then N'暫付七成'
	  when remittance_type=4 then N'人工每周付款'
	  when remittance_type=5 then N'人工每月付款'
	  when remittance_type=6 then N'彈性選擇出帳'
	  when remittance_type=7 then N'新每週出帳'
	  when remittance_type=8 then N'新每月出帳'  
 end as RemittanceType,
 case when a.vendor_receipt_type =0 then N'其他'
 when a.vendor_receipt_type =1 then N'免用統一發票 (開收據)'
 when a.vendor_receipt_type =2 then N'統一發票 (開發票)' end as VendorReceiptType,
item_name ,d.item_orig_price ,d.item_price ,dc.cost ,
CASE WHEN d.item_price=0 THEN N'單價為零' ELSE convert(varchar(10),1-(c.cost/d.item_price)) END as GrossMargin,
d.order_total_limit ,d.max_item_count , d.business_hour_atm_maximum 
 from view_ppon_deal d WITH(NOLOCK)
join deal_accounting a WITH(NOLOCK) on a.business_hour_guid=d.business_hour_guid
join deal_property p WITH(NOLOCK) on p.business_hour_guid=d.business_hour_guid
join acc_business_group g WITH(NOLOCK) on g.acc_business_group_id=p.deal_acc_business_group_id
join #Cost c on c.business_hour_guid=d.business_hour_guid
join (select MAX(cost) as cost,business_hour_guid from deal_cost group by business_hour_guid) dc on d.business_hour_guid = dc.business_hour_guid
join department dp WITH(NOLOCK) on dp.dept_id = a.dept_id
where 1=1
and ( 
(DATEDIFF(DAY, '17530101', DATEADD(DD,1,GETDATE())) % 7 / 5 = 0 and business_hour_order_time_s between DATEADD(d,DATEDIFF(d,0,getdate()),1) and DATEADD(SECOND,-1,DATEADD(d,DATEDIFF(d,0,getdate()),2)))
or
(DATEDIFF(DAY, '17530101', DATEADD(DD,1,GETDATE())) % 7 / 5 = 1 and business_hour_order_time_s between DATEADD(d,DATEDIFF(d,0,getdate()),1) and DATEADD(SECOND,-1,DATEADD(d,DATEDIFF(d,0,getdate()),4)))
)
order by p.delivery_type,remittance_type,p.unique_id
";
            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            DataTable dt = new DataTable();
            IDataReader idr = DataService.GetReader(qc);
            using (idr)
                dt.Load(idr);

            return dt;
        }

        public bool SetSubjectNameByBid(string bizHour, string subjectName)
        {
            bool ret = true;
            try
            {
                DB.Update<CouponEventContent>().Set(CouponEventContent.SubjectNameColumn).EqualTo(subjectName)
                .Where(CouponEventContent.BusinessHourGuidColumn).IsEqualTo(bizHour).Execute();
            }
            catch (Exception ex)
            {
                log.Error("EDM Subject Name's Error", ex);
                ret = false;
            }

            return ret;
        }

        #endregion EDM用

        #endregion PponDeal

        #region appPponDeal

        public AppDefaultPageDealsSettingCollection GetAllAppDefaultPageDealsSetting()
        {
            return DB.SelectAllColumnsFrom<AppDefaultPageDealsSetting>()
                    .ExecuteAsCollection<AppDefaultPageDealsSettingCollection>();
        }

        public void SaveAllAppDefaultPageDealsSetting(AppDefaultPageDealsSettingCollection data)
        {
            DB.SaveAll(data);
        }

        #endregion

        #region PponOrder

        public ViewPponOrder ViewPponOrderGet(Guid orderGuid)
        {
            string sql = string.Format(@"select * from {0} where {1} = @orderGuid"
                , ViewPponOrder.Schema.TableName
                , ViewPponOrder.Columns.Guid);

            QueryCommand qc = new QueryCommand(sql, ViewPponOrder.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);

            var result = new ViewPponOrderCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result.FirstOrDefault();
        }

        public ViewPponOrderCollection ViewPponOrderGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponOrder.Columns.Guid;
            QueryCommand qc = GetVPOWhereQC(ViewPponOrder.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponOrder.Schema.Provider.DelimitDbName(ViewPponOrder.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewPponOrderCollection vfcCol = new ViewPponOrderCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public ViewPponOrderCollection ViewPponOrderGetList(Guid dealGuid)
        {
            return DB.SelectAllColumnsFrom<ViewPponOrder>()
                        .Where(ViewPponOrder.Columns.BusinessHourGuid).IsEqualTo(dealGuid)
                        .ExecuteAsCollection<ViewPponOrderCollection>();
        }

        protected QueryCommand GetVPOWhereQC(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        public int ViewPponOrderGetCount(params string[] filter)
        {
            QueryCommand qc = GetVPOWhereQC(ViewPponOrder.Schema, filter);
            qc.CommandSql = "select count(1) from " + ViewPponOrder.Schema.Provider.DelimitDbName(ViewPponOrder.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public int RushBuyGetCompleteOrderCount(Guid bid, DateTime start, DateTime end)
        {
            string sql = @"select count(1)
            from cpa_order co with(nolock) 
            join [order] o with(nolock) ON co.order_guid = o.GUID 
            join [order] op with(nolock) ON op.GUID = o.parent_order_id 
            join group_order gpo with(nolock) ON gpo.order_guid = op.GUID 
            where o.order_status & 8 > 0 and co.last_rsrc = '17_rushbuy' and co.create_time between @start and @end and gpo.business_hour_guid=@bid";

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@start", start, DbType.DateTime);
            qc.AddParameter("@end", end, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        public bool ViewPponOrderUpdateShipStatus(OrderStatusLog ol, int orderstatus)
        {
            string setexpression = Order.Columns.OrderMemo + "=@ordermemo," + Order.Columns.OrderStatus + "=@order_status";
            var sql = @"update [" + Order.Schema.TableName + "] set " + setexpression + " where " + Order.Columns.Guid + "=@orderGuid";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@ordermemo", ol.Message, DbType.String);
            qc.AddParameter("@order_status", orderstatus, DbType.Int32);
            qc.AddParameter("@orderGuid", ol.OrderGuid, DbType.Guid);
            DataService.ExecuteScalar(qc);
            ol.Message = "更新出貨狀態";
            DB.Save<OrderStatusLog>(ol);
            return true;
        }

        public OrderCollection PponOrderGetByBid(Guid bid)
        {
            string strSQL = @"select o.* from [order] o with(nolock)
join [order] op with(nolock) on o.parent_order_id=op.GUID
join group_order gpo with(nolock) on gpo.order_guid=op.GUID
where gpo.business_hour_guid=@bid and o.order_status & 584=8";

            OrderCollection oc = new InlineQuery().ExecuteAsCollection<OrderCollection>(strSQL, bid.ToString());

            return oc;
        }

        public OrderCollection PponOrderGetByOrderId(string oid)
        {
            return DB.QueryOver<Order>()
                    .Where(t => t.OrderId == oid)
                    .ToCollection<OrderCollection>();
        }

        public ViewPponOrderCollection ViewPponOrderGetListByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewPponOrder>().Where(ViewPponOrder.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<ViewPponOrderCollection>() ?? new ViewPponOrderCollection();
        }

        #endregion PponOrder

        #region ViewPponOrderDetail

        public ViewPponOrderDetailCollection ViewPponOrderDetailGetList(string column, object value, string orderBy)
        {
            ViewPponOrderDetailCollection view = new ViewPponOrderDetailCollection();
            Query qry = ViewPponOrderDetail.Query().WHERE(column, value);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }

            view.LoadAndCloseReader(qry.ExecuteReader());
            return view;
        }

        public ViewPponOrderDetail ViewPponOrderDetailGet(Guid orderDetailGuid)
        {
            return DB.SelectAllColumnsFrom<ViewPponOrderDetail>().Where(ViewPponOrderDetail.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid).ExecuteSingle<ViewPponOrderDetail>();
        }

        public ViewPponOrderDetailCollection ViewPponOrderDetailGetListByOrderGuid(Guid orderGuid)
        {
            ViewPponOrderDetailCollection vpod = DB.SelectAllColumnsFrom<ViewPponOrderDetail>()
                .Where(ViewPponOrderDetail.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewPponOrderDetailCollection>();
            return vpod ?? new ViewPponOrderDetailCollection();
        }

        public int ViewPponOrderDetailGetCountByUser(string user_name, Guid bid, bool isDailyRstriction = false)
        {
            string strSql = "select case when SUM(od.item_quantity) is null then 0 else SUM(od.item_quantity) end from order_detail od with(nolock) " +
                    "join [order] o with(nolock) on o.GUID=od.order_GUID " +
                    "join [order] op with(nolock) on op.GUID=o.parent_order_id " +
                    "join group_order gpo with(nolock) on gpo.order_guid=op.GUID " +
                    "inner join member m with(nolock) on m.unique_id = o.user_id " +
                    "where gpo.business_hour_guid=@bid " +
                    "and m.user_name=@user_name and od.status<>2 and o.order_status & 8 > 0 and o.order_status & 512 = 0";

            if (isDailyRstriction)
            {
                strSql += " AND year(o.create_time)= year(getdate()) AND month(o.create_time) = month(getdate()) AND day(o.create_time) = day(getdate()) ";
            }

            int odc = new InlineQuery(OrderDetail.Schema.Provider).ExecuteScalar<int>(strSql, bid.ToString(), user_name);

            return odc;
        }

        public int ViewPponOrderDetailGetCountByUser(int userId, Guid bid, bool isDailyRstriction)
        {
            string sql = @"select case when SUM(od.item_quantity) is null then 0 else SUM(od.item_quantity) end from order_detail od with(nolock) 
                    join [order] o with(nolock) on o.GUID=od.order_GUID 
                    join [order] op with(nolock) on op.GUID=o.parent_order_id 
                    join group_order gpo with(nolock) on gpo.order_guid=op.GUID 
                    inner join member m with(nolock) on m.unique_id = o.user_id 
                    where gpo.business_hour_guid=@bid 
                    and m.unique_id=@userId and od.status<>2 and o.order_status & 8 > 0 and o.order_status & 512 = 0";

            if (isDailyRstriction)
            {
                sql += " AND o.create_time > cast(getdate() as date) and o.create_time < dateadd(day, 1, cast(getdate() as date))";
            }

            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@userId", userId, DbType.Int32);

            int odc = (int)DataService.ExecuteScalar(qc);
            return odc;
        }

        public DataTable PponGoman(string user_name)
        {
            string strSQL = @"
select cec.name ,od.item_name,
(select SUM(s_od.item_quantity) from order_detail s_od join [order] s_o on s_o.GUID=s_od.order_GUID where s_o.parent_order_id=gpo.[order_guid] and (s_od.status&2=0)) as od_qty
,bh.order_total_limit,bh.business_hour_order_minimum,od.item_quantity,bh.business_hour_order_time_e,cec.image_path,o.parent_order_id
,od.item_name,o.delivery_address
from order_detail od with(nolock)
join [order] o with(nolock) on o.GUID=od.order_GUID
inner join member m on m.unique_id = o.user_id
join group_order gpo with(nolock) on gpo.order_guid=o.parent_order_id
join coupon_event_content cec with(nolock) on cec.business_hour_guid=gpo.business_hour_guid
join business_hour bh with(nolock) on bh.GUID=gpo.business_hour_guid
where m.user_name=@user_name
and o.order_status& @status1 <> @stauts2
and gpo.status& @gpoStatus1 <> @gpoStatus2
and od.status<>2
";

            IDataReader rdr = new SubSonic.InlineQuery().ExecuteReader(strSQL, user_name, (int)OrderStatus.Cancel, (int)OrderStatus.Cancel, (int)GroupOrderStatus.Completed, (int)GroupOrderStatus.Completed);
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        protected QueryCommand GetVPODWhereQC(string prividerName, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", prividerName);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewPponOrderDetail.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        #endregion ViewPponOrderDetail

        #region ViewPponCoupon

        public ViewPponCouponCollection ViewPponCouponGetList(string column, object value)
        {
            ViewPponCouponCollection view = new ViewPponCouponCollection();
            view.LoadAndCloseReader(ViewPponCoupon.Query().WHERE(column, value).ExecuteReader());
            return view;
        }

        public ViewPponCouponCollection ViewPponCouponGetList(string column, object[] values)
        {
            ViewPponCouponCollection view = new ViewPponCouponCollection();
            view.LoadAndCloseReader(ViewPponCoupon.Query().IN(column, values).ExecuteReader());
            return view;
        }

        public ViewPponCouponCollection ViewPponCouponGetListRegularOnly(string column, object value)
        {
            ViewPponCouponCollection view = new ViewPponCouponCollection();
            view.LoadAndCloseReader(ViewPponCoupon.Query().WHERE(column, value).AND(ViewPponCoupon.Columns.SequenceNumber, SubSonic.Comparison.IsNot, null).ExecuteReader());
            return view;
        }

        public ViewPponCouponCollection ViewPponCouponGetListByOrderGuid(Guid orderGuid)
        {
            return DB.QueryOver<ViewPponCoupon>()
                    .Where(t => t.OrderGuid == orderGuid && t.OrderDetailStatus == (int)OrderDetailStatus.None)
                    .ToCollection<ViewPponCouponCollection>();
        }

        public ViewPponCouponCollection ViewPponCouponGetListByUserName(int pageStart, int pageLength, string user_name, string orderBy, string filter)
        {
            string defOrderBy = ViewPponCoupon.Columns.OrderDetailCreateTime;
            string sql = @"SELECT view_ppon_coupon.* FROM [view_ppon_coupon] with(nolock) " +
                             "join view_ppon_deal vpd with(nolock) on vpd.business_hour_guid=view_ppon_coupon.business_hour_guid " +
                             "WHERE " +
                             "member_email='" + user_name + "' and " +
                             ViewPponCoupon.Columns.OrderDetailStatus + "<>" +
                            ((int)OrderDetailStatus.SystemEntry).ToString() +
                              " and order_detail_create_id<>'sys' " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }
            ViewPponCouponCollection ocol = new ViewPponCouponCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewPponCouponCollection ViewPponCouponGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponCoupon.Columns.BusinessHourGuid;
            QueryCommand qc = GetVpolWhereQC(ViewPponCoupon.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponCoupon.Schema.Provider.DelimitDbName(ViewPponCoupon.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewPponCouponCollection vpolCol = new ViewPponCouponCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponCouponCollection ViewPponCouponGetList(int pageStart, int pageLength, Guid business_hour_guid, string orderBy)
        {
            string defOrderBy = ViewPponCoupon.Columns.OrderDetailCreateTime;
            string sql = @" SELECT * FROM " + ViewPponCoupon.Schema.TableName + @" WITH(NOLOCK) " +
                         @" WHERE " + ViewPponCoupon.Columns.BusinessHourGuid + @" = @businessHourGuid " +
                         @" AND " + ViewPponCoupon.Columns.OrderDetailStatus + @" & " + (int)OrderDetailStatus.SystemEntry + @" = 0 " +
                         @" AND " + ViewPponCoupon.Columns.OrderStatus + @" & " + (int)OrderStatus.Complete + @" > 0 " +
                         @" AND " + ViewPponCoupon.Columns.OrderGuid + @" NOT IN (select " + Order.Columns.Guid + @" from [" + Order.Schema.TableName + @"] WITH(NOLOCK) where " + Order.Columns.OrderStatus + @" & " + (int)OrderStatus.CancelBeforeClosed + @" > 0)" +
                         @" AND " + ViewPponCoupon.Columns.CouponId + @" is not null ";

            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@businessHourGuid", business_hour_guid, DbType.Guid);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }
            ViewPponCouponCollection ocol = new ViewPponCouponCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }

        public ViewPponCouponCollection ViewPponCouponGetNoCouponList(string column, object value)
        {
            ViewPponCouponCollection view = new ViewPponCouponCollection();
            string sql =
                @"SELECT distinct a.business_hour_status,b.order_detail_guid as order_detail_id,b.Order_Detail_Status,a.parent_order_id,a.business_hour_guid,a.member_email,a.item_quantity,a.item_name,a.delivery_address,a.member_name,a.business_hour_deliver_time_s,a.business_hour_deliver_time_e,a.group_order_status , a.store_guid " +
                " FROM [view_ppon_coupon] a with(nolock) join view_ppon_order_detail b with(nolock) on b.business_hour_guid=a.business_hour_guid and a.order_GUID=b.order_GUID " +
                " WHERE a." + column + "='" + value + "' and a.coupon_code is null " +
                " and a.order_status&512=0 and a.order_status&262144=0 and a.item_name=b.item_name " +
                " and a.order_detail_status&2=0 and b.order_detail_status&2=0";
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ViewPponCouponCollection GetNoCouponList()   //for job
        {
            ViewPponCouponCollection view = new ViewPponCouponCollection();
            string sql =
@"declare @endtime datetime;
declare @starttime datetime;
set @endtime=DATEADD(hh,-36,GETDATE())
set @starttime=DATEADD(mi,-30,GETDATE())
select distinct a.[guid] as 'business_guid',a.business_hour_order_minimum as 'minimun',b.order_guid as 'group_order_id' into #tmpJobA
from [dbo].[business_hour] a with(nolock), [dbo].[group_order] b with(nolock) ,[dbo].[order_detail] c with(nolock),[dbo].[order] d with(nolock),[dbo].[deal_property] e with(nolock)
where c.[create_time]>@endtime and c.[create_time]<@starttime and a.[GUID]=e.[business_hour_guid] and e.delivery_type=1
and c.[order_GUID]=d.[guid] and d.[parent_order_id]=b.[order_guid] and b.[business_hour_guid]=a.[GUID]
select b.business_guid,COUNT(1) as 'count' into #tmpJobB from [dbo].[order] a with(nolock), #tmpJobA b
where a.parent_order_id=b.group_order_id and a.order_status&512=0 and a.order_status&262144=0
group by b.business_guid,b.minimun having COUNT(1)>b.minimun
SELECT b.[GUID] as order_detail_id,b.[status] as 'Order_Detail_Status',a.parent_order_id,a.business_hour_guid,a.member_email,case when dp.sale_multiple_base > 0 then a.item_quantity * dp.sale_multiple_base else a.item_quantity end as item_quantity,a.item_name,a.delivery_address,a.member_name,a.business_hour_deliver_time_s,a.business_hour_deliver_time_e,a.group_order_status
FROM [view_ppon_coupon] a with(nolock) 
inner join deal_property dp with(nolock) on dp.business_hour_guid = a.business_hour_guid
left join order_detail b with(nolock) on a.order_GUID=b.order_GUID
where a.order_detail_create_time<@starttime and a.order_detail_create_time>@endtime
and a.business_hour_guid in (select business_guid from #tmpJobB)
and a.coupon_code is null
and a.order_status&512=0 and a.order_status&262144=0 and a.order_status&8>0
and a.order_detail_status&2=0 and b.[status]&2=0
UNION ALL
---補成套有產coupon 但數量不一致的狀況---
SELECT b.[GUID] as order_detail_id,b.[status] as 'Order_Detail_Status',a.parent_order_id,a.business_hour_guid,a.member_email,(a.item_quantity * dp.sale_multiple_base - count(*)) as item_quantity,a.item_name,a.delivery_address,a.member_name,a.business_hour_deliver_time_s,a.business_hour_deliver_time_e,a.group_order_status
FROM [view_ppon_coupon] a with(nolock) 
inner join deal_property dp with(nolock) on dp.business_hour_guid = a.business_hour_guid
left join order_detail b with(nolock) on a.order_GUID=b.order_GUID
where a.order_detail_create_time<@starttime and a.order_detail_create_time>@endtime
and a.business_hour_guid in (select business_guid from #tmpJobB)
and a.order_status&512=0 and a.order_status&262144=0 and a.order_status&8>0
and a.order_detail_status&2=0 and b.[status]&2=0
and dp.sale_multiple_base > 0
and a.coupon_id is not null
group by b.[GUID] ,b.[status] ,a.parent_order_id,a.business_hour_guid,a.member_email,dp.sale_multiple_base ,a.item_quantity 
,a.item_name,a.delivery_address,a.member_name,a.business_hour_deliver_time_s,a.business_hour_deliver_time_e,a.group_order_status
HAVING a.item_quantity * dp.sale_multiple_base - count(*) > 0;
drop table #tmpJobA;
drop table #tmpJobb;";

            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public DataTable ViewPponCouponGetIsReservationLockCouponList(Guid bid)
        {
            var result = new ViewPponCouponCollection();
            string sql = string.Format(@"select distinct {0}, {1} 
from {2}
where  {3} = @bid
and {0} is not null",
            ViewPponCoupon.Columns.SequenceNumber,
            ViewPponCoupon.Columns.IsReservationLock,
            ViewPponCoupon.Schema.Name,
            ViewPponCoupon.Columns.BusinessHourGuid
);

            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            var dt = DataService.GetDataSet(qc).Tables[0];

            return dt;
        }

        public int CheckCouponDetail(string orderDetailId)  //for backend tool
        {
            string sql =
                @"select (select case when dp.sale_multiple_base > 0 then dp.sale_multiple_base else vpod.item_quantity end from [view_ppon_order_detail] vpod with(nolock) inner join deal_property dp with(nolock) on dp.business_hour_guid = vpod.business_hour_guid " +
                "where vpod.order_detail_guid='" + orderDetailId + "') - " +
                "isnull((select COUNT(order_detail_id) from [view_ppon_coupon] with(nolock) " +
                "where order_detail_id='" + orderDetailId + "' group by order_detail_id),0)";
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            return (int)DataService.ExecuteScalar(qc);
        }

        public List<Guid> CouponlessOrderDetailGuidGetList()
        {
            string sql = @"
                ;with A as (
                select o.order_id, o.GUID as o_guid, count(od.GUID) as sod, SUM(od.item_quantity) as item_quan
                from [order] o
                inner join group_order goo
                  on goo.order_guid =o.parent_order_id
                inner join order_detail OD
                  ON od.order_GUID = o.GUID
                where goo.business_hour_guid in
                   (select guid
	                from business_hour bh
	                inner join deal_property dp
	                  on bh.GUID = dp.business_hour_guid
	                where bh.business_hour_order_time_e between GETDATE() - 30 and GETDATE() + 30
	                and dp.delivery_type = 1)   --最近的憑證型檔次
                  and od.status in ( 0,1) and o.order_status & 520 = 8
                group by    o.order_id, o.GUID
                )

                select
                    od.GUID
                from A
                outer apply (select COUNT(*) coupon_count from coupon c
                  inner join order_detail cod
                    on c.order_detail_id = cod.GUID
                  where cod.order_GUID = A.o_guid and cod.status in (0,1)
                  ) c
                outer apply (select COUNT(*) ctlog_count, COUNT(ctlog.coupon_id) as complete_ctlog_count
                  from cash_trust_log ctlog
                  inner join order_detail ctod
                    on ctod.guid = ctlog.order_detail_guid
                  where ctlog.order_GUID = A.o_guid and ctod.status in (0,1)
                  ) ctlog
                left join order_detail od
                  on A.o_guid = od.order_GUID
                where
                  item_quan <> coupon_count  --缺 coupon
                order by A.order_id
            ";
            List<Guid> list = new List<Guid>();
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            foreach (DataRow row in dt.Rows)
            {
                list.Add(new Guid(row[0].ToString()));
            }

            return list;
        }

        public int ViewPponCouponByBidAndOrderDetailStatusCount(Guid businessHourGuid)
        {
            string sql = @"SELECT count(1) FROM [view_ppon_coupon] WITH(NOLOCK) " +
                         @"WHERE business_hour_guid = @businessHourGuid AND Order_Detail_Status & 2 = 0";

            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@businessHourGuid", businessHourGuid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public OrderCollection OverchargedMonitorJob()  //for job
        {
            OrderCollection odrCol = new OrderCollection();

            string sql = string.Format(@"
select * from [dbo].[order] with(nolock)
where order_status&(1048576|8|512|2097152|4194304)=0 and subtotal>0
and guid in(
select order_guid from payment_transaction with(nolock) 
where trans_time between DATEADD(DAY,{0},GETDATE()) and DATEADD(MI,-30,GETDATE()) 
and trans_type=0 and status&15=3
)", config.NotOkOrderQueryDays);
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            odrCol.LoadAndCloseReader(DataService.GetReader(qc));
            return odrCol;
        }

        public PaymentTransactionCollection OverchargedWithoutOrderMonitorJob()
        {
            PaymentTransactionCollection pts = new PaymentTransactionCollection();

            //目前只用在檢查applepay訂單
            string sql = string.Format(@"
select pt.* from payment_transaction pt with(nolock) 
left join cashpoint_order co with(nolock) on co.id = pt.order_guid
where trans_time between DATEADD(DAY,{0},GETDATE()) and DATEADD(MI,-30,GETDATE()) 
and pt.trans_type=0 and pt.status&15=3 
and pt.api_provider = 38 
and co.id is null", config.NotOkOrderQueryDays);
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            pts.LoadAndCloseReader(DataService.GetReader(qc));
            return pts;
        }


        public PaymentTransactionCollection GetOtpNotPostBack()
        {
            PaymentTransactionCollection pts = new PaymentTransactionCollection();

            string sql = string.Format(@"select p.* from payment_transaction p with(nolock) inner join [order] o with(nolock) on p.order_guid=o.guid
                           where payment_type=" + (int)PaymentType.Creditcard + " and trans_type=" + (int)PayTransType.Authorization +
                           " and result=0 and status=48 and amount>0 and api_provider=" + (int)PaymentAPIProvider.TaishinPaymnetOTPGateway + " and  order_status&8=0" +
                           " and p.trans_time between DATEADD(DAY,{0}, GETDATE()) and DATEADD(MI,-30, GETDATE()) ", config.NotOkOrderQueryDays);
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            pts.LoadAndCloseReader(DataService.GetReader(qc));
            return pts;
        }


        public List<string> GetCreditcatrInconsistent()
        {
            string sql = string.Format(@"SELECT pt.trans_id FROM   [order] o INNER JOIN payment_transaction pt 
                                        ON pt.trans_id = (SELECT TOP 1 trans_id FROM   payment_transaction pt WHERE  pt.payment_type = 5
                                                          AND pt.trans_type = 0
                                                          AND pt.order_guid = o.GUID)
                                        AND pt.payment_type = 1
                                        AND pt.trans_type = 0
                                        WHERE  o.order_status & 8 > 0
                                        AND o.order_status & 512 = 0
                                        AND pt.status & 15 <> 3
                                        AND o.create_time BETWEEN Dateadd(DAY, -3, Getdate()) AND Dateadd(MI, -30, Getdate()) ", config.NotOkOrderQueryDays);

            QueryCommand qc = new QueryCommand(sql, PaymentTransaction.Schema.Provider.Name);
            List<string> result = new List<string>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetString(0));
                }
            }
            return result;
        }

        public DataTable GetEinvoiceMain2To3()  //for job
        {
            string sql = @"select order_id,invoice_number from einvoice_main with(nolock)
where message = N'二聯改三聯' and verified_time >= CONVERT(varchar(100), GETDATE()-1, 111) 
and verified_time < CONVERT(varchar(100), GETDATE(), 111) and invoice_number is not null";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }


        public DataTable ChargingFailureMonitorJob()  //for job
        {
            string sql = @"
DECLARE @time datetime;	SET @time=GETDATE();
select trans_id,result from [dbo].[payment_transaction] with(nolock)
where trans_time between DATEADD(d,-7,CONVERT(date ,@time)) and @time
and trans_type=1 and status&15<>3
";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable CheckShippingDate()  //for job
        {
            string sql = @"
IF OBJECT_ID('tempdb..#ProductEndtime') is not null DROP TABLE #ProductEndtime
select od.product_id, COUNT(1) as quantity into #ProductEndtime from cash_trust_log c with(nolock)
join hi_deal_order o with(nolock) on o.guid=c.order_guid
join hi_deal_order_detail od with(nolock) on o.guid=od.hi_deal_order_guid
join hi_deal_deal d with(nolock) on o.hi_deal_id=d.id
where o.order_status=3 and c.special_status&16=0
and (status in(0,1,2) or (status in(3,4) and c.modify_time>d.deal_end_time))
group by od.product_id

IF OBJECT_ID('tempdb..#PponQuantity') is not null DROP TABLE #PponQuantity
select business_hour_guid, COUNT(1) as quantity into #PponQuantity from cash_trust_log c with(nolock)
where status in (0,1,2)
group by business_hour_guid

select order_time_s as '銷售開始', order_time_e as '銷售結束', deliver_time_s as'配送開始', deliver_time_e as '配送結束',
	site as '平台', deal_id as '檔號or品編', product_name as '商品名稱', event_name as '優惠內容', sales as '業務' from(
(
select
convert(varchar(10),business_hour_order_time_s,120) as order_time_s,
convert(varchar(10),business_hour_order_time_e,120) as order_time_e,
convert(varchar(10),business_hour_deliver_time_s,120) as deliver_time_s,
convert(varchar(10),business_hour_deliver_time_e,120) as deliver_time_e,
N'P好康' as site, unique_id as deal_id, item_name as product_name, event_name, sales_id as sales
from view_ppon_deal d with(nolock)
join deal_accounting a with(nolock) on a.business_hour_guid=d.business_hour_guid
join #PponQuantity q on q.business_hour_guid=d.business_hour_guid
left join combo_deals c with(nolock) on c.BusinessHourGuid=d.business_hour_guid
where business_hour_order_time_e>'20120701' and business_hour_deliver_time_e<GETDATE()
and delivery_type=2 and slug>=business_hour_order_minimum
and shipped_date is null and c.Title<>N'主檔'
and q.quantity>0
)union(
select
convert(varchar(10),deal_start_time,120) as order_time_s,
convert(varchar(10),deal_end_time,120) as order_time_e,
convert(varchar(10),use_start_time,120) as deliver_time_s,
CONVERT(varchar(10), use_end_time,120) as deliver_time_e,
N'品生活' as site, p.id, p.name as product_name , promo_long_desc as event_name, sales from hi_deal_product p with(nolock)
join hi_deal_deal d with(nolock) on d.id=p.deal_id
join #ProductEndtime t on t.product_id=p.id
where deal_end_time>'20120701' and use_end_time<GETDATE()
and shipped_date is null and t.quantity>0 and p.is_home_delivery=1
)
)x order by deliver_time_e
";
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public int CheckCouponCount(Guid odid)
        {
            return DB.Select().From(Coupon.Schema.TableName).Where(Coupon.Columns.OrderDetailId).IsEqualTo(odid).GetRecordCount();
        }

        public int ClearCouponByCouponID(int id)
        {
            return DB.Delete().From(Coupon.Schema.TableName).Where(Coupon.Columns.Id).IsEqualTo(id).OrderAsc(Coupon.Columns.SequenceNumber).Execute();
        }

        public QueryCommand GetInsertCouponCmd(Coupon c)    //died modify
        {
            QueryCommand qc = new QueryCommand("insert into " + Coupon.Schema.TableName + " ( " +
                                               Coupon.Columns.OrderDetailId + " , " +
                                               Coupon.Columns.SequenceNumber + " , " +
                                               Coupon.Columns.Code + " , " +
                                               Coupon.Columns.Description + " , " +
                                               Coupon.Columns.Status + "," + Coupon.Columns.StoreSequence + " ) values(@oid,@sn,@code,@desc,@status,@StoreSequence);" +
                                               "SELECT SCOPE_IDENTITY() AS newID;", Coupon.Schema.Provider.Name);
            qc.AddParameter("@oid", c.OrderDetailId.ToString(), DbType.String);
            qc.AddParameter("@sn", c.SequenceNumber, DbType.String);
            qc.AddParameter("@code", c.Code, DbType.String);
            qc.AddParameter("@desc", c.Description, DbType.String);
            qc.AddParameter("@status", c.Status, DbType.Int16);
            qc.AddParameter("@StoreSequence", c.StoreSequence, DbType.String);
            return qc;
        }

        public int ViewPponCouponGetCount(string user_name, string filter)
        {
            string sql = @"SELECT count(1) FROM [view_ppon_coupon] with (nolock) " +
                                 "join view_ppon_deal vpd with (nolock) on vpd.business_hour_guid=view_ppon_coupon.business_hour_guid " +
                                 "WHERE " +
                                 "member_email='" + user_name + "' and " +
                                 ViewPponCoupon.Columns.OrderDetailStatus + "<>" +
                                ((int)OrderDetailStatus.SystemEntry).ToString() +
                                 " and vpd.department<>" + ((int)DepartmentTypes.Peauty).ToString()
                                 + " and order_detail_create_id<>'sys' " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            return (int)DataService.ExecuteScalar(qc);
        }

        public decimal ViewPponCouponGetSum(string user_name)
        {
            //died modify for null result 11.03.18
            string sql = @"SELECT isnull(SUM(item_orig_price)-SUM(item_price),0) FROM [view_ppon_coupon] with (nolock) " +
                                   "join view_ppon_deal vpd with (nolock) on vpd.business_hour_guid=view_ppon_coupon.business_hour_guid " +
                                   "WHERE " +
                                   "member_email='" + user_name + "' and " +
                                   ViewPponCoupon.Columns.OrderDetailStatus + "<>" +
                                  ((int)OrderDetailStatus.SystemEntry).ToString() +
                                   " and (vpd.ordered_quantity>=vpd.business_hour_order_minimum" +
                                   " or (vpd.ordered_quantity<=vpd.business_hour_order_minimum and vpd.group_order_status & 1<>1))" +
                                   " and vpd.department<>" + ((int)DepartmentTypes.Peauty).ToString()
                                   + " and order_detail_create_id<>'sys' and item_price<>0";
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            return (decimal)DataService.ExecuteScalar(qc); ;
        }

        public ViewPponCoupon ViewPponCouponGet(int id)
        {
            ViewPponCoupon view = new ViewPponCoupon();
            view.LoadByParam(ViewPponCoupon.Columns.CouponId, id);
            return view;
        }

        public ViewPponCoupon ViewPponCouponGet(Guid orderDetailGuid, string sequenceNumber)
        {
            var c = DB.SelectAllColumnsFrom<ViewPponCoupon>().NoLock()
                .Where(ViewPponCoupon.Columns.OrderDetailGuid).IsEqualTo(orderDetailGuid)
                .And(ViewPponCoupon.Columns.SequenceNumber).IsEqualTo(sequenceNumber)
                .ExecuteAsCollection<ViewPponCouponCollection>();
            return c.FirstOrDefault() ?? new ViewPponCoupon();
        }

        public void SetAtmCouponToQueue(Guid orderGuid)
        {
            var sql = "update [" + SmsLog.Schema.TableName + "] set status=2 " +
            " where coupon_id in (select coupon_id from view_ppon_coupon with(nolock) where order_GUID = @orderGuid and order_detail_status!=2 and order_status&512=0 and not coupon_id is null) " +
            " and status=4";
            QueryCommand qc = new QueryCommand(sql, SmsLog.Schema.Provider.Name);
            qc.AddParameter("@orderGuid", orderGuid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public ViewPponCoupon ViewPponCouponGetCouponEventContent(string column, object value)
        {
            string sql = "select * from " + ViewPponCoupon.Schema.Provider.DelimitDbName(ViewPponCoupon.Schema.TableName) + @" with(nolock)
                         where " + ViewPponCoupon.Columns.BusinessHourGuid + " = @value";
            var qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@value", value, DbType.Guid);
            var vpc = new ViewPponCoupon();
            vpc.LoadAndCloseReader(DataService.GetReader(qc));
            return vpc;
        }

        public ViewPponCoupon ViewPponCouponGetBySeqCodeCreateTime(int couponId, string sequenceNumber, string couponCode, string orderDetailCreateTime)
        {
            var vpc = new ViewPponCoupon();

            string sql = "select top 1 * from " + ViewPponCoupon.Schema.Provider.DelimitDbName(ViewPponCoupon.Schema.TableName) + @" with(nolock)
                         where " + ViewPponCoupon.Columns.CouponId + " = @couponId" +
                         " and " + ViewPponCoupon.Columns.SequenceNumber + " = @sequenceNumber" +
                         " and " + ViewPponCoupon.Columns.CouponCode + " = @CouponCode" +
                         " and " + ViewPponCoupon.Columns.OrderDetailCreateTime + " = @orderDetailCreateTime";

            var qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@couponId", couponId, DbType.Int32);
            qc.AddParameter("@sequenceNumber", sequenceNumber, DbType.AnsiString);
            qc.AddParameter("@couponCode", couponCode, DbType.AnsiString);
            qc.AddParameter("@orderDetailCreateTime", orderDetailCreateTime, DbType.DateTime);

            try
            {
                vpc.LoadAndCloseReader(DataService.GetReader(qc));
            }
            catch
            {
                // ignored
            }

            return vpc;
        }

        public ViewPponCouponCollection ViewPponCouponGetListUnusedBySellerGuid(Guid sellerGuid)
        {
            string sqlStr = string.Format(
            @"SELECT c.* from {0} c WITH(NOLOCK) join {1} ctl WITH(NOLOCK) on ctl.coupon_id=c.coupon_id where c.seller_GUID = @sellerGuid and ctl.status in({2},{3})"
            , ViewPponCoupon.Schema.TableName, CashTrustLog.Schema.TableName, (int)TrustStatus.Initial, (int)TrustStatus.Trusted);
            QueryCommand qc = new QueryCommand(sqlStr, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            ViewPponCouponCollection vpc = new ViewPponCouponCollection();
            vpc.LoadAndCloseReader(DataService.GetReader(qc));
            return vpc;
        }

        public ViewPponCouponCollection ViewPponCouponGetListUnusedBySellerStoreGuid(Guid sellerGuid, Guid storeGuid)
        {
            string sqlStr = string.Format(
            @"SELECT c.* from {0} c WITH(NOLOCK) join {1} ctl WITH(NOLOCK) on ctl.coupon_id=c.coupon_id where c.seller_GUID = @sellerGuid and c.store_guid = @storeGuid and ctl.status in({2},{3})"
            , ViewPponCoupon.Schema.TableName, CashTrustLog.Schema.TableName, (int)TrustStatus.Initial, (int)TrustStatus.Trusted);
            QueryCommand qc = new QueryCommand(sqlStr, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            ViewPponCouponCollection vpc = new ViewPponCouponCollection();
            vpc.LoadAndCloseReader(DataService.GetReader(qc));
            return vpc;
        }

        public ViewPponCouponCollection ViewPponCouponGetListUnusedByDeal(Guid bid)
        {
            string sqlStr = string.Format(
            @"SELECT c.* from {0} c WITH(NOLOCK) join {1} ctl WITH(NOLOCK) on ctl.coupon_id=c.coupon_id where c.business_hour_guid = @bid and ctl.status in({2},{3})"
            , ViewPponCoupon.Schema.TableName, CashTrustLog.Schema.TableName, (int)TrustStatus.Initial, (int)TrustStatus.Trusted);
            QueryCommand qc = new QueryCommand(sqlStr, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            ViewPponCouponCollection vpc = new ViewPponCouponCollection();
            vpc.LoadAndCloseReader(DataService.GetReader(qc));
            return vpc;
        }

        public ViewPponCouponCollection ViewPponCouponGetListUnusedByDealStoreGuid(Guid bid, Guid storeGuid)
        {
            string sqlStr = string.Format(
            @"SELECT c.* from {0} c WITH(NOLOCK) join {1} ctl WITH(NOLOCK) on ctl.coupon_id=c.coupon_id where c.business_hour_guid = @bid and c.store_guid = @storeGuid and ctl.status in({2},{3})"
            , ViewPponCoupon.Schema.TableName, CashTrustLog.Schema.TableName, (int)TrustStatus.Initial, (int)TrustStatus.Trusted);
            QueryCommand qc = new QueryCommand(sqlStr, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            ViewPponCouponCollection vpc = new ViewPponCouponCollection();
            vpc.LoadAndCloseReader(DataService.GetReader(qc));
            return vpc;
        }

        public int ViewPponCouponGetFirstBusinessHourStatus(Guid bid, Guid storeGuid, Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewPponCoupon>().Where(ViewPponCoupon.Columns.BusinessHourGuid).IsEqualTo(bid).And(ViewPponCoupon.Columns.StoreGuid).IsEqualTo(storeGuid)
                     .And(ViewPponCoupon.Columns.OrderGuid).IsEqualTo(orderGuid)
                     .ExecuteAsCollection<ViewPponCouponCollection>().FirstOrDefault().BusinessHourStatus;

        }

        #endregion ViewPponCoupon

        #region ViewPponDeal

        public ViewPponDealCollection ViewPponDealGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewPponDeal.Columns.BusinessHourGuid;
            QueryCommand qc = GetVpolWhereQC(ViewPponDeal.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCalendar ViewPponDealCalendarGet(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewPponDealCalendar>().Where(ViewPponDealCalendar.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteSingle<ViewPponDealCalendar>();
        }


        /// <summary>
        /// 檔次管理查詢
        /// </summary>
        /// <param name="pageStart">起始頁</param>
        /// <param name="pageLength">每頁幾筆</param>
        /// <param name="orderBy">排序</param>
        /// <param name="EmpUserId">登入者userId</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <param name="SalesId">業務</param>
        /// <param name="filter">其他篩選</param>
        /// <returns></returns>
        public ViewPponDealCalendarCollection ViewPponDealGetListPagingNoSub(int pageStart, int pageLength, string orderBy, int EmpUserId, string CrossDeptTeam, int? SalesId, params string[] filter)
        {
            string defOrderBy = ViewPponDealCalendar.Columns.BusinessHourGuid;
            QueryCommand qc = GetVpolWhereQC(ViewPponDealCalendar.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            qc.CommandSql += " and " + ViewPponDealCalendar.Columns.BusinessHourStatus + @" & " + (int)BusinessHourStatus.ComboDealSub + @"=0 ";
            qc.CommandSql = ViewPponDealGetDataPagingNoSub(EmpUserId, CrossDeptTeam, SalesId, qc);


            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewPponDealCalendarCollection vpolCol = new ViewPponDealCalendarCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public int ViewPponDealNoSubGetCount(int EmpUserId, string CrossDeptTeam, int? SalesId, params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponDealCalendar.Schema, filter);

            qc.CommandSql = "select count(1) from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            qc.CommandSql += " and " + ViewPponDealCalendar.Columns.BusinessHourStatus + @" & " + (int)BusinessHourStatus.ComboDealSub + @"=0 ";
            qc.CommandSql = ViewPponDealGetDataPagingNoSub(EmpUserId, CrossDeptTeam, SalesId, qc);
            return (int)DataService.ExecuteScalar(qc);
        }

        private string ViewPponDealGetDataPagingNoSub(int EmpUserId, string CrossDeptTeam, int? SalesId, QueryCommand qc)
        {
            #region CrossDeptTeam
            if (EmpUserId != 0)
            {
                //僅瀏覽權限
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                qc.CommandSql += "(" + ViewPponDealCalendar.Columns.DevelopeSalesId + " = @EmpUserId or " + ViewPponDealCalendar.Columns.OperationSalesId + " = @EmpUserId)";
                qc.AddParameter("@EmpUserId", EmpUserId, DbType.Int32);
            }
            else if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                //跨區
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                string tmpsql = "";
                string salesuseridtmpsql2 = "";
                string salesuseridtmpsql3 = "";
                if (SalesId != null)
                {
                    //多判斷搜尋業務是否於跨區內
                    salesuseridtmpsql2 = " and " + ViewPponDealCalendar.Columns.DevelopeSalesId + " = " + "@SalesId ";
                    salesuseridtmpsql3 = " and " + ViewPponDealCalendar.Columns.OperationSalesId + " = " + "@SalesId";
                    qc.AddParameter("@SalesId", SalesId, DbType.Int32);
                }
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        //組
                        tmpsql += "(" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.DevelopeTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql2 + " ) or ";
                        tmpsql += "(" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.OperationTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql3 + " ) or ";
                    }
                    else
                    {
                        //區
                        tmpsql += " (" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c + "'" + salesuseridtmpsql2 + ") or ";
                        tmpsql += " (" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c + "'" + salesuseridtmpsql3 + ") or ";
                    }


                }
                tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                qc.CommandSql += tmpsql;


            }
            else
            {
                if (SalesId != null)
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }

                    qc.CommandSql += "(" + ViewPponDealCalendar.Columns.DevelopeSalesId + " = " + "@SalesId or " + ViewPponDealCalendar.Columns.OperationSalesId + " = " + "@SalesId)";
                    qc.AddParameter("@SalesId", SalesId, DbType.Int32);
                }
            }

            #endregion

            return qc.CommandSql;
        }

        /// <summary>
        /// Yahoo大團購指定檔次(非0元、子檔、全家、公益)
        /// </summary>
        /// <param name="pageStart">pageStart</param>
        /// <param name="pageLength">pageLength</param>
        /// <param name="dateStart">檔次起始時間</param>
        /// <param name="dateEnd">檔次結束時間</param>
        /// <param name="bid">bid(傳入Guid.Empty不查詢)</param>
        /// <param name="dealNname">檔次名稱(傳入String.Empty不查詢)</param>
        /// <returns></returns>
        public ViewPponDealCollection ViewPponDealGetListPagingForYahoo(int pageStart, int pageLength, DateTime dateStart, DateTime dateEnd, Guid bid, string dealNname)
        {
            string defOrderBy = ViewPponDeal.Columns.BusinessHourOrderTimeS;
            string strSQL =
                @" select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" with(nolock)
                   where " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" < @dateS 
                   and " + ViewPponDeal.Columns.BusinessHourOrderTimeE + @" > @dateE 
                   and " + ViewPponDeal.Columns.ItemPrice + @" > 0 
                   and " + ViewPponDeal.Columns.BusinessHourStatus + @" & " + (int)BusinessHourStatus.ComboDealSub + @"=0 
                   and " + ViewPponDeal.Columns.GroupOrderStatus + @" & " + (int)GroupOrderStatus.FamiDeal + @"=0 
                   and " + ViewPponDeal.Columns.GroupOrderStatus + @" & " + (int)GroupOrderStatus.KindDeal + @"=0 ";

            if (!Guid.Equals(bid, Guid.Empty))
            {
                strSQL += @" and " + ViewPponDeal.Columns.BusinessHourGuid + @" = @bid ";
            }

            if (!string.IsNullOrWhiteSpace(dealNname))
            {
                strSQL += @" and " + ViewPponDeal.Columns.EventName + @" like @event_name ";
            }

            QueryCommand qc = new QueryCommand(strSQL, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@dateS", dateStart, DbType.DateTime);
            qc.AddParameter("@dateE", dateEnd, DbType.DateTime);

            if (!Guid.Equals(bid, Guid.Empty))
            {
                qc.AddParameter("@bid", bid, DbType.Guid);
            }

            if (!string.IsNullOrWhiteSpace(dealNname))
            {
                qc.AddParameter("@event_name", "%" + dealNname.Trim('%') + "%", DbType.String);
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public int ViewPponDealGetCountForYahoo(DateTime dateStart, DateTime dateEnd, Guid bid, string dealNname)
        {
            string strSQL =
                @" select count(1) from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" with(nolock)
                   where " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" < @dateS 
                   and " + ViewPponDeal.Columns.BusinessHourOrderTimeE + @" > @dateE 
                   and " + ViewPponDeal.Columns.ItemPrice + @" > 0 
                   and " + ViewPponDeal.Columns.BusinessHourStatus + @" & " + (int)BusinessHourStatus.ComboDealSub + @"=0 
                   and " + ViewPponDeal.Columns.GroupOrderStatus + @" & " + (int)GroupOrderStatus.FamiDeal + @"=0 
                   and " + ViewPponDeal.Columns.GroupOrderStatus + @" & " + (int)GroupOrderStatus.KindDeal + @"=0 ";

            if (!Guid.Equals(bid, Guid.Empty))
            {
                strSQL += @" and " + ViewPponDeal.Columns.BusinessHourGuid + @" = @bid ";
            }

            if (!string.IsNullOrWhiteSpace(dealNname))
            {
                strSQL += @" and " + ViewPponDeal.Columns.EventName + @" like @event_name ";
            }

            QueryCommand qc = new QueryCommand(strSQL, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@dateS", dateStart, DbType.DateTime);
            qc.AddParameter("@dateE", dateEnd, DbType.DateTime);

            if (!Guid.Equals(bid, Guid.Empty))
            {
                qc.AddParameter("@bid", bid, DbType.Guid);
            }

            if (!string.IsNullOrWhiteSpace(dealNname))
            {
                qc.AddParameter("@event_name", "%" + dealNname.Trim('%') + "%", DbType.String);
            }
            return (int)DataService.ExecuteScalar(qc);
        }


        /// <summary>
        /// 過往好康查詢
        /// </summary>
        /// <param name="pageStart">起始頁</param>
        /// <param name="pageLength">每頁幾筆</param>
        /// <param name="orderBy">排序</param>
        /// <param name="categoryId">上檔頻道</param>
        /// <param name="dealtype1">銷售分析類別(母)</param>
        /// <param name="dealtype2">銷售分析類別(子)</param>
        /// <param name="businessOrderTimeS">上檔時間(起)</param>
        /// <param name="businessOrderTimeE">上檔時間(訖)</param>
        /// <param name="businessDeliverTimeS">配送時間(起)</param>
        /// <param name="businessDeliverTimeE">配送時間(訖)</param>
        /// <param name="sellerName">店家名稱</param>
        /// <param name="dealName">優惠內容</param>
        /// <param name="isCloseDeal">是否已結檔</param>
        /// <param name="SalesId">業務</param>
        /// <param name="UniqueId">檔號</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <param name="EmpUserId">登入者userId</param>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public ViewPponDealCalendarCollection ViewPponDealGetListPagingForCategory(int pageStart, int pageLength, string orderBy, int categoryId, int? dealtype1, int? dealtype2, DateTime? businessOrderTimeS, DateTime? businessOrderTimeE, DateTime? businessDeliverTimeS, DateTime? businessDeliverTimeE, string sellerName, string dealName, bool isCloseDeal, bool isOnDeal, int? SalesId, int UniqueId, string CrossDeptTeam, string businessHourGuid, int EmpUserId,int Wms, ref string sSQL)
        {
            string sql = @" select * from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + @" WITH(NOLOCK) ";

            QueryCommand qc = GetViewPponDealForCategoryQueryCommand(sql, categoryId, dealtype1, dealtype2, businessOrderTimeS, businessOrderTimeE, businessDeliverTimeS, businessDeliverTimeE, sellerName, dealName, isCloseDeal, isOnDeal, SalesId, UniqueId, businessHourGuid, CrossDeptTeam, EmpUserId, Wms);
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, orderBy);
            }
            var vpolCol = new ViewPponDealCalendarCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            sSQL = qc.CommandSql + "==params==" + new JsonSerializer().Serialize(qc.Parameters);

            return vpolCol;
        }

        public int ViewPponDealGetCountForCategory(int categoryId, int? dealtype1, int? dealtype2, DateTime? businessOrderTimeS, DateTime? businessOrderTimeE, DateTime? businessDeliverTimeS, DateTime? businessDeliverTimeE, string sellerName, string dealName, bool isCloseDeal, bool isOnDeal, int? SalesId, int UniqueId, string businessHourGuid, string CrossDeptTeam, int EmpUserId, int Wms)
        {
            string sql = @" select count(1) from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + @" WITH(NOLOCK) ";

            QueryCommand qc = GetViewPponDealForCategoryQueryCommand(sql, categoryId, dealtype1, dealtype2, businessOrderTimeS, businessOrderTimeE, businessDeliverTimeS, businessDeliverTimeE, sellerName, dealName, isCloseDeal, isOnDeal, SalesId, UniqueId, businessHourGuid, CrossDeptTeam, EmpUserId, Wms);
            return (int)DataService.ExecuteScalar(qc); ;
        }

        protected QueryCommand GetViewPponDealForCategoryQueryCommand(string sql, int categoryId, int? dealtype1, int? dealtype2, DateTime? businessOrderTimeS, DateTime? businessOrderTimeE, DateTime? businessDeliverTimeS, DateTime? businessDeliverTimeE, string sellerName, string dealName, bool isCloseDeal, bool isOnDeal, int? SalesId, int UniqueId, string businessHourGuid, string CrossDeptTeam, int EmpUserId, int Wms)
        {
            // 不可查詢未開檔
            sql += @" WHERE " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " < GETDATE()";

            // 只搜尋母檔(子檔另外撈取)
            sql += @" and " + ViewPponDealCalendar.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealSub + "=0";

            // 分類
            if (categoryId != 0)
            {
                sql += " and " + ViewPponDealCalendar.Columns.BusinessHourGuid + " IN (SELECT cd.bid from " + CategoryDeal.Schema.Provider.DelimitDbName(CategoryDeal.Schema.TableName) + " cd WITH(NOLOCK) WHERE cd.cid = @cid) ";
            }
            // 銷售分析類別
            if (dealtype1 != null)
            {
                if (dealtype1 != 0)
                {
                    if (dealtype2 == 0)
                    {
                        //只選擇大類
                        sql += " and " + ViewPponDealCalendar.Columns.DealType + " IN (SELECT code_id from " + SystemCode.Schema.Provider.DelimitDbName(SystemCode.Schema.TableName) + " WITH(NOLOCK) WHERE enabled=1 AND parent_code_id = @dealtype1) ";
                    }
                    else
                    {
                        //選擇大類+小類
                        sql += " and " + ViewPponDealCalendar.Columns.DealType + " = @dealtype2";
                    }
                }
            }
            // 上檔時間
            if (businessOrderTimeS != null && businessOrderTimeE != null)
            {
                sql += " and " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " > @dateS AND " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeE + " <= @dateE ";
            }
            // 兌換/配送期間
            if (businessDeliverTimeS != null)
            {
                sql += " and " + ViewPponDealCalendar.Columns.BusinessHourDeliverTimeS + " >= @dateDS ";
            }
            if (businessDeliverTimeE != null)
            {
                sql += " and " + ViewPponDealCalendar.Columns.BusinessHourDeliverTimeE + " <= @dateDE ";
            }
            // 查詢已結檔
            if (isCloseDeal)
            {
                sql += " and " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeE + " < GETDATE() ";
            }
            // 查詢在線檔次
            if (isOnDeal)
            {
                sql += " and GETDATE() between " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + " and " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeE;
            }
            // 賣家名稱
            if (!string.IsNullOrWhiteSpace(sellerName))
            {
                sql += " and " + ViewPponDealCalendar.Columns.SellerName + " LIKE @sellerName ";

            }
            // 檔次名稱
            if (!string.IsNullOrWhiteSpace(dealName))
            {
                for (int i = 0; i < dealName.Split(' ').Count(); i++)
                {
                    sql += " and (" + ViewPponDealCalendar.Columns.EventName + " LIKE @dealName" + i + " or " + ViewPponDealCalendar.Columns.ContentName + " LIKE @dealName" + i + ")";
                }
            }


            //檔號
            if (UniqueId != 0)
            {
                sql += " and (" + ViewPponDealCalendar.Columns.BusinessHourGuid + " = (select MainBusinessHourGuid from view_combo_deals where unique_id=@UniqueId and MainBusinessHourGuid is not null) or " + ViewPponDealCalendar.Columns.UniqueId + "=@UniqueId) ";
            }
            if (!string.IsNullOrEmpty(businessHourGuid))
            {
                sql += " and (" + ViewPponDealCalendar.Columns.BusinessHourGuid + " = (select MainBusinessHourGuid from combo_deals with(nolock) where BusinessHourGuid=@BusinessHourGuid and MainBusinessHourGuid is not null) or " + ViewPponDealCalendar.Columns.BusinessHourGuid + "=@BusinessHourGuid) ";
            }

            if (EmpUserId != 0)
            {
                //僅瀏覽權限(只有在查店家名稱時)
                sql += " and (" + ViewPponDealCalendar.Columns.DevelopeSalesId + " = @EmpUserId or " + ViewPponDealCalendar.Columns.OperationSalesId + " = @EmpUserId)";
            }
            else if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                //跨區(只有在查店家名稱時)
                string tmpsql = "";
                string salesuseridtmpsql2 = "";
                string salesuseridtmpsql3 = "";
                if (SalesId != null)
                {
                    //多判斷搜尋業務是否於跨區內
                    salesuseridtmpsql2 = " and " + ViewPponDealCalendar.Columns.DevelopeSalesId + " = " + "@SalesId ";
                    salesuseridtmpsql3 = " and " + ViewPponDealCalendar.Columns.OperationSalesId + " = " + "@SalesId";

                }
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        //組
                        tmpsql += "(" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.DevelopeTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql2 + ") or ";
                        tmpsql += "(" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.OperationTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")" + salesuseridtmpsql3 + ") or ";
                    }
                    else
                    {
                        //區
                        tmpsql += " (" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c + "'" + salesuseridtmpsql2 + ") or ";
                        tmpsql += " (" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c + "'" + salesuseridtmpsql3 + ") or ";
                    }


                }
                tmpsql = " and (" + tmpsql.TrimEnd("or ") + ")";
                sql += tmpsql;
            }

            if (SalesId != null)
            {
                sql += " and (" + ViewPponDealCalendar.Columns.DevelopeSalesId + " = @SalesId or " + ViewPponDealCalendar.Columns.OperationSalesId + " = @SalesId)";
            }

            if (Wms != -1)
            {
                sql += " and (" + ViewPponDealCalendar.Columns.IsWms + " = @Wms and " + ViewPponDealCalendar.Columns.DeliveryType + "=" + (int)DeliveryType.ToHouse + " )";
            }


            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@cid", categoryId, DbType.Int32);
            qc.AddParameter("@dealtype1", dealtype1, DbType.Int32);
            qc.AddParameter("@dealtype2", dealtype2, DbType.Int32);
            qc.AddParameter("@dateS", businessOrderTimeS, DbType.DateTime);
            qc.AddParameter("@dateE", businessOrderTimeE, DbType.DateTime);
            qc.AddParameter("@dateDS", businessDeliverTimeS, DbType.DateTime);
            qc.AddParameter("@dateDE", businessDeliverTimeE, DbType.DateTime);
            qc.AddParameter("@sellerName", "%" + sellerName + "%", DbType.String);
            qc.AddParameter("@SalesId", SalesId, DbType.Int32);
            qc.AddParameter("@UniqueId", UniqueId, DbType.Int32);
            qc.AddParameter("@BusinessHourGuid", businessHourGuid, DbType.String);
            qc.AddParameter("@EmpUserId", EmpUserId, DbType.Int32);
            qc.AddParameter("@Wms", Wms, DbType.Int32);
            if (!string.IsNullOrWhiteSpace(dealName))
            {
                for (int i = 0; i < dealName.Split(' ').Count(); i++)
                {
                    qc.AddParameter("@dealName" + i, "%" + dealName.Split(' ')[i] + "%", DbType.String);
                }
            }
            return qc;
        }

        public string GetViewPponDealForCategorySummaryPriceCommand(string type)
        {
            string sql = "";
            if (type.Equals("business_price"))
            {
                //營業額
                sql = " case when (SELECT count(*) from " + ComboDeal.Schema.Provider.DelimitDbName(ComboDeal.Schema.TableName) + " Where MainBusinessHourGuid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid) =0 then " +
                       "       ( " +
                       "       select ISNull(SUM(( " +
                       "          case when vpdi.business_hour_order_time_e > getdate() then " +
                       "               vpdi.ordered_quantity " +
                       "          else " +
                       "              case when vpdi.slug is not null then " +
                       "                   vpdi.slug " +
                       "              else " +
                       "                   vpdi.ordered_quantity " +
                       "              end " +
                       "           end)*item_price),0) " +
                       "       from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " vpdi " +
                       "       where vpdi.business_hour_guid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid " +
                       "       ) " +
                       "    else " +
                       "       ( " +
                       "       select ISNull(SUM((case when vcd.business_hour_order_time_e > getdate() then vcd.ordered_quantity else case when vcd.slug is not null then vcd.slug else vcd.ordered_quantity end end)*vcd.item_price),0) " +
                       "       from " + ViewComboDeal.Schema.Provider.DelimitDbName(ViewComboDeal.Schema.TableName) + " vcd with(nolock) " +
                       "       where vcd.MainBusinessHourGuid =" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid  " +
                       "       and MainBusinessHourGuid <> BusinessHourGuid " +
                       "       )" +
                       "    end ";
            }
            else if (type.Equals("continued_quantity"))
            {
                //前台數量
                sql = " case when (SELECT count(*) from " + ComboDeal.Schema.Provider.DelimitDbName(ComboDeal.Schema.TableName) + " Where MainBusinessHourGuid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid) =0 then " +
                      "       ( " +
                      "       select ISNull(SUM((" +
                      "             case when vpdi.is_quantity_multiplier =1 and vpdi.quantity_multiplier is not null then " +
                      "                 isNull(" +
                      "                    (case when vpdi.slug is not null then " +
                      "                           vpdi.slug " +
                      "                    else " +
                      "                           vpdi.ordered_quantity" +
                      "                    end) " +
                      "                 ,0) * isNull(vpdi.quantity_multiplier,0) + isNull(vpdi.continued_quantity,0) " +
                      "             else " +
                      "                 isNull( " +
                      "                    (case when vpdi.slug is not null then " +
                      "                           vpdi.slug " +
                      "                    else " +
                      "                           vpdi.ordered_quantity" +
                      "                    end) " +
                      "                ,0) + isNull(vpdi.continued_quantity,0) " +
                      "             end" +
                      "       )),0) " +
                      "       from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " vpdi " +
                      "       where vpdi.business_hour_guid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid " +
                      "       ) " +
                      "    else " +
                      "       ( " +
                      "       select ISNull(SUM((case when vcd.is_quantity_multiplier=1 and vcd.quantity_multiplier is not null then vcd.quantity_multiplier else 1 end) * " +
                      "                                                                                        (case when slug is not null then slug else ordered_quantity end) " +
                      "              ),0) + (select isnull(sum(continued_quantity),0) as s1 from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " vvpd where vvpd.business_hour_guid=vcd.MainBusinessHourGuid) " +
                      "       from " + ViewComboDeal.Schema.Provider.DelimitDbName(ViewComboDeal.Schema.TableName) + " vcd with(nolock) " +
                      "       where vcd.MainBusinessHourGuid =" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid  " +
                      "       and MainBusinessHourGuid <> BusinessHourGuid " +
                      "       group by vcd.MainBusinessHourGuid " +
                      "       )" +
                      "    end ";
            }
            else if (type.Equals("quantity"))
            {
                //實際銷售
                sql = " case when (SELECT count(*) from " + ComboDeal.Schema.Provider.DelimitDbName(ComboDeal.Schema.TableName) + " Where MainBusinessHourGuid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid) =0 then " +
                       "       ( " +
                       "       select ISNull(SUM((case when vpdi.business_hour_order_time_e > getdate() then vpdi.ordered_quantity else case when vpdi.slug is not null then vpdi.slug else vpdi.ordered_quantity end end)),0) " +
                       "       from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + " vpdi " +
                       "       where vpdi.business_hour_guid=" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid " +
                       "       ) " +
                       "    else " +
                       "       ( " +
                       "       select ISNull(SUM((case when vcd.business_hour_order_time_e > getdate() then vcd.ordered_quantity else case when vcd.slug is not null then vcd.slug else vcd.ordered_quantity end end)),0) " +
                       "       from " + ViewComboDeal.Schema.Provider.DelimitDbName(ViewComboDeal.Schema.TableName) + " vcd with(nolock) " +
                       "       where vcd.MainBusinessHourGuid =" + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + ".business_hour_guid  " +
                       "       and MainBusinessHourGuid <> BusinessHourGuid " +
                       "       )" +
                       "    end ";
            }


            return sql;
        }



        public ViewPponDealCollection ViewPponDealGetListByMainBid(Guid mainBid)
        {
            //[過往好康] 查詢的子檔排序要跟後台一致
            string strSql = @" select vpd.* from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " vpd with(nolock) " +
                            @" left join [combo_deals] cd with(nolock) on vpd.business_hour_guid=cd.BusinessHourGuid " +
                            @" where cd.MainBusinessHourGuid = @mainBid AND cd.MainBusinessHourGuid <> cd.BusinessHourGuid " +
                            @" order by cd.Sequence";

            QueryCommand qc = new QueryCommand(strSql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@mainBid", mainBid, DbType.Guid);

            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetList(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponDeal.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealStoreCollection ViewPponDealStoreGetListDeliverTime(DateTime date, Guid sellerguid, Guid storeguid)
        {
            string strSQL = @"SELECT * FROM [view_ppon_deal_store] where seller_guid = @sellerguid";
            if (storeguid != Guid.Empty)
            {
                strSQL += " and store_guid = @storeguid";
            }
            strSQL = string.Format("SELECT * FROM ({0}) A WHERE '{1}' BETWEEN A.use_start_time AND A.use_end_time OR '{1}' < A.use_start_time", strSQL, date.ToString("yyyy/MM/dd"));
            QueryCommand qc = new QueryCommand(strSQL, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sellerguid", sellerguid, DbType.Guid);
            if (storeguid != Guid.Empty)
                qc.AddParameter("@storeguid", storeguid, DbType.Guid);
            ViewPponDealStoreCollection vpdsCol = new ViewPponDealStoreCollection();
            vpdsCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpdsCol;
        }

        /// <summary>
        /// 帳務連絡人信取憑證4/1號前,商品檔結取檔當月月份+前兩個月
        /// </summary>
        /// <returns></returns>
        public ViewPponDealStoreCollection ViewPponDealStoreGetList(Guid sellerguid, Guid storeguid)
        {
            string sql = string.Format(@"SELECT *
                           FROM [view_ppon_deal_store] with(nolock)
                           where delivery_type = 1 and deal_end_time < cast(YEAR(getdate()) as nvarchar) + '/04/01' and seller_guid = @sellerguid {0}
                           union all
                           SELECT *
                           FROM [view_ppon_deal_store] with(nolock)
                           where delivery_type = 2 and deal_end_time between DATEADD(MONTH,-2 ,DATEADD(MONTH, DATEDIFF(MONTH, 0, getdate() ), 0)) and DATEADD(MONTH, DATEDIFF(MONTH, -1, getdate() ), -1) and seller_guid = @sellerguid {0}"
                           , storeguid == Guid.Empty ? "" : "and store_guid = @storeguid");
            QueryCommand qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sellerguid", sellerguid, DbType.Guid);
            if (storeguid != Guid.Empty)
            {
                qc.AddParameter("@storeguid", storeguid, DbType.Guid);
            }
            ViewPponDealStoreCollection vpdsCol = new ViewPponDealStoreCollection();
            vpdsCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpdsCol;
        }

        public int ViewPponDealGetCount(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponDeal.Schema, filter);

            qc.CommandSql = "select count(1) from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewPponDealCollection ViewPponDealGetListOnShowBySellerGuid(Guid sellerGuid)
        {
            string sql =
                @"select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) +
                @" with(nolock) where seller_GUID = @sellerGuid
and (group_order_status & @mask = @expect)
order by business_hour_order_time_s ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            qc.AddParameter("@mask", (int)GroupOrderStatus.Hidden, DbType.Int32);
            qc.AddParameter("@expect", 0, DbType.Int32);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListOnUseBySellerGuid(Guid sellerGuid)
        {
            string sql = @" select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @"
                            where " + ViewPponDeal.Columns.SellerGuid + @" = @sid
                            and " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" < GETDATE() AND " + ViewPponDeal.Columns.BusinessHourDeliverTimeE + @" > GETDATE()
                            and " + ViewPponDeal.Columns.GroupOrderStatus + @" & @mask = 0 order by " + ViewPponDeal.Columns.BusinessHourOrderTimeS;
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sid", sellerGuid, DbType.Guid);
            qc.AddParameter("@mask", (int)GroupOrderStatus.Hidden, DbType.Int32);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }


        public List<Guid> OnlineDealGuidGetList(DateTime sDate, DateTime eDate)
        {
            string sql = @"
	            select distinct business_hour_guid from deal_time_slot with(nolock)
	                where effective_start >= @sDate and effective_start < @eDate
	            union
	                select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
	                in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                )  and BusinessHourGuid <> MainBusinessHourGuid                    
";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);


            List<Guid> result = new List<Guid>();
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }
            return result;
        }
        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的ViewPponDeal資料。
        /// 使用NoLock關鍵字，所以資料也許不是最正確的。
        /// </summary>
        /// <param name="sDate">排程日期大於等於起始日</param>
        /// <param name="eDate">排程日期小於起始日</param>
        /// <returns></returns>
        public ViewPponDealCollection ViewPponDealGetListByDayWithNoLock(DateTime sDate, DateTime eDate)
        {
            string sql = @"
                    select * from view_ppon_deal with(nolock)
                    where business_hour_guid
                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    union
	                    select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
	                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    )
	                    and BusinessHourGuid <> MainBusinessHourGuid
                    )
";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            int defaultTimeout = qc.CommandTimeout;
            qc.CommandTimeout = 600;
            log.InfoFormat("設定 ViewPponDealGetListByDayWithNoLock timeout 調整 {0} 為 {1}", defaultTimeout, qc.CommandTimeout);

            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vpolCol;
        }
        public ViewPponDealCollection ViewPponDealGetListByDayWithNoLock()
        {
            DateTime startTime = DateTime.Now.AddMinutes(30);
            DateTime endTime = DateTime.Now;
            string sql = @"
                select * from view_ppon_deal with(nolock) 
                where 
	                business_hour_order_time_s < @startTime
                and 
	                @endTime < business_hour_deliver_time_e
";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListByDayWithNoLock(DateTime sDate, DateTime eDate, PponDealSpecialCityType type, bool onlyDealShowInDefault)
        {
            var citySql = string.Empty;

            switch (type)
            {
                case PponDealSpecialCityType.WithOutSpecialCity:
                    citySql = " and city_id not in (select id from city where code in ('com','TML','PIN','SLT','SKM' )) ";
                    break;
                case PponDealSpecialCityType.Com:
                    citySql = " and city_id in (select id from city where code = 'com') ";
                    break;
                case PponDealSpecialCityType.Piinlife:
                    citySql = " and city_id in (select id from city where code = 'PIN') ";
                    break;
                case PponDealSpecialCityType.PponSelect:
                    citySql = " and city_id in (select id from city where code = 'SLT') ";
                    break;
                case PponDealSpecialCityType.Tmall:
                    citySql = " and city_id in (select id from city where code = 'TML') ";
                    break;
                case PponDealSpecialCityType.PponAndPiinlife:
                    citySql = " and city_id not in (select id from city where code in ('com','TML','SLT','SKM' )) ";
                    break;
            }

            var sql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" deal  with(nolock)
                         where business_hour_guid
in (
select distinct " + DealTimeSlot.Columns.BusinessHourGuid + " from " + DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + @" with(nolock)
where effective_start >= @sDate and effective_start < @eDate " + citySql + " ) and business_hour_status&2048=0";

            if (onlyDealShowInDefault)
            {
                sql += string.Format(@" and not exists (
                    select * from  [deal_time_slot]t with(nolock)  
			            where deal.business_hour_guid = t.business_hour_GUID
			            and ((effective_start >= @sDate and effective_start < @eDate)
			            Or (effective_end >= @sDate and effective_end < @eDate))
			            and status = {0})", (int)DealTimeSlotStatus.NotShowInPponDefault);
            }
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vpolCol;
        }

        public List<Guid> ViewPponDealGetBidListByDayWithNoLock(DateTime sDate, DateTime eDate, PponDealSpecialCityType type, bool onlyDealShowInDefault)
        {
            var citySql = string.Empty;

            switch (type)
            {
                case PponDealSpecialCityType.WithOutSpecialCity:
                    citySql = " and city_id not in (select id from city where code in ('com','TML','PIN','SLT','SKM' )) ";
                    break;
                case PponDealSpecialCityType.Com:
                    citySql = " and city_id in (select id from city where code = 'com') ";
                    break;
                case PponDealSpecialCityType.Piinlife:
                    citySql = " and city_id in (select id from city where code = 'PIN') ";
                    break;
                case PponDealSpecialCityType.PponSelect:
                    citySql = " and city_id in (select id from city where code = 'SLT') ";
                    break;
                case PponDealSpecialCityType.Tmall:
                    citySql = " and city_id in (select id from city where code = 'TML') ";
                    break;
                case PponDealSpecialCityType.PponAndPiinlife:
                    citySql = " and city_id not in (select id from city where code in ('com','TML','SLT','SKM' )) ";
                    break;
            }

            var sql = "select distinct " + DealTimeSlot.Columns.BusinessHourGuid + " from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" deal  with(nolock)
                                 where business_hour_guid
                                    in (
                                    select distinct " + DealTimeSlot.Columns.BusinessHourGuid + " from " + DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + @" with(nolock)
                                    where effective_start >= @sDate and effective_start < @eDate " + citySql + " ) and business_hour_status&2048=0";

            if (onlyDealShowInDefault)
            {
                sql += string.Format(@" and not exists (
                            select * from  [deal_time_slot]t with(nolock)  
        			            where deal.business_hour_guid = t.business_hour_GUID
        			            and ((effective_start >= @sDate and effective_start < @eDate)
        			            Or (effective_end >= @sDate and effective_end < @eDate))
        			            and status = {0})", (int)DealTimeSlotStatus.NotShowInPponDefault);
            }
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            List<Guid> result = new List<Guid>();
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }

            return result;
        }

        public ViewPponDealCollection ViewPponDealGetListByCityIdOrderBySlotSeq(int cityId, DeliveryType[] deliverytype)
        {
            string sql = @"
select a.* from view_ppon_deal a with(nolock)
inner join deal_time_slot b with(nolock) on a.business_hour_guid = b.business_hour_GUID
where  b.city_id = @cityId and @theDate between b.effective_start and b.effective_end and a.business_hour_status&2048=0
and b.status = @status

{0}
order by b.sequence asc
";

            string[] deptParam = null;
            if (deliverytype != null && deliverytype.Length > 0)
            {
                deptParam = deliverytype.Select((s, i) => "@dp" + i.ToString()).ToArray();
            }

            QueryCommand qc = new QueryCommand(string.Format(sql, deptParam == null ? "" : string.Format(@"and {0} in ({1}) ", "a.delivery_type", string.Join(",", deptParam))), ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            qc.AddParameter("@theDate", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);
            qc.AddParameter("@status", (int)DealTimeSlotStatus.Default, DbType.Int32);

            if (deptParam != null)
            {
                for (int i = 0; i < deptParam.Length; i++)
                {
                    qc.AddParameter(deptParam[i], (int)deliverytype[i], DbType.Int32);
                }
            }

            ViewPponDealCollection rtnCol = new ViewPponDealCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public List<Guid> ViewPponDealGetBidListByDay(DateTime sDate, DateTime eDate)
        {
            string sql = @"
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    union
	                    select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
	                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    )
	                    and BusinessHourGuid <> MainBusinessHourGuid
";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            List<Guid> result = new List<Guid>();
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }

            return result;
        }

        public List<Guid> BusinessHourGuidGetListByCityIdOrderBySlotSeq(int cityId, DeliveryType[] deliverytype)
        {
            string sql = @"
select a.GUID from business_hour a with(nolock)
inner join deal_time_slot b with(nolock) on a.GUID = b.business_hour_GUID
where  b.city_id = @cityId and @theDate between b.effective_start and b.effective_end and a.business_hour_status&2048=0
and b.status = @status
order by b.sequence asc
";

            QueryCommand qc = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            qc.AddParameter("@theDate", DateTime.Now, DbType.DateTime);
            qc.AddParameter("@status", (int)DealTimeSlotStatus.Default, DbType.Int32);

            List<Guid> result = new List<Guid>();
            using (IDataReader dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result.Add(dr.GetGuid(0));
                }
            }

            return result;
        }

        protected QueryCommand GetVpolWhereQC(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        public ViewPponDeal ViewPponDealGet(Guid orderGuid)
        {
            ViewPponDeal view = new ViewPponDeal();

            QueryCommand qc = new QueryCommand("select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock) where " + ViewPponDeal.Columns.OrderGuid + "=@oid", ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("oid", orderGuid.ToString(), DbType.Guid);
            view.LoadAndCloseReader(DataService.GetReader(qc));

            return view;
        }

        public ViewPponDeal ViewPponDealGetByBusinessHourGuid(Guid bzGuid)
        {
            ViewPponDeal view = new ViewPponDeal();
            QueryCommand qc = new QueryCommand("select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock) where " + ViewPponDeal.Columns.BusinessHourGuid + "=@bid", ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("bid", bzGuid.ToString(), DbType.Guid);
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ViewPponDealCollection ViewPponDealListGetByMainbid(Guid mainBid)
        {
            var list = new ViewPponDealCollection();

            var result = DB.SelectAllColumnsFrom<ViewPponDeal>()
                .Where(ViewPponDeal.Columns.MainBid)
                .IsEqualTo(mainBid)
                .ExecuteAsCollection<ViewPponDealCollection>();

            if (result.Count > 0)
            {
                list = result;
            }
            return list;
        }

        public ViewPponDealCollection ViewPponDealGetByBusinessHourGuidList(List<Guid> bzGuid)
        {

            var data = new ViewPponDealCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                data.AddRange(DB.SelectAllColumnsFrom<ViewPponDeal>()
                .Where(ViewPponDeal.Columns.BusinessHourGuid).In(bzGuid.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewPponDealCollection>());
                idx += batchLimit;
            } while (idx <= bzGuid.Count - 1);

            return data;
        }

        public ViewPponDeal ViewPponDealGetByUniqueId(int uid)
        {
            ViewPponDeal view = new ViewPponDeal();
            QueryCommand qc = new QueryCommand("select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock) where " + ViewPponDeal.Columns.UniqueId + "=@uid", ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("uid", uid, DbType.Int32);
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public ViewPponDeal ViewPponDealGet(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponDeal.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            ViewPponDeal view = new ViewPponDeal();
            view.LoadAndCloseReader(DataService.GetReader(qc));

            return view;
        }

        /// <summary>
        /// 取出今日全部好康
        /// </summary>
        /// <returns></returns>
        public ViewPponDealCollection ViewPponDealsToday()
        {
            string strSql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) +
                           " with(nolock) where GETDATE() between business_hour_order_time_s and business_hour_order_time_e";

            QueryCommand qc = new QueryCommand(strSql, ViewPponDeal.Schema.Provider.Name);

            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealsToday(string filter_string)
        {
            string strSql = "select * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) +
                           " with(nolock) where GETDATE() between business_hour_order_time_s and business_hour_order_time_e " + filter_string;

            QueryCommand qc = new QueryCommand(strSql, ViewPponDeal.Schema.Provider.Name);

            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection GetRandomPponDealByCity(int num, int cityId)
        {
            string strSql = @"  SELECT *
                                FROM " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" WITH(NOLOCK)
                                WHERE business_hour_guid IN (
	                                SELECT TOP " + num.ToString() + @" b.GUID
	                                FROM " + BusinessHour.Schema.Provider.DelimitDbName(BusinessHour.Schema.TableName) + @" b WITH(NOLOCK)
	                                WHERE b.GUID IN (
		                                SELECT business_hour_guid
		                                FROM " + DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + @" d WITH(NOLOCK) 
		                                WHERE d.status = 0
		                                AND d.city_id = " + cityId.ToString() + @"
		                                AND getdate() < d.effective_end
		                                AND getdate() > d.effective_start)
	                                AND b.business_hour_status & " + (int)BusinessHourStatus.ComboDealSub + @" = 0
	                                ORDER BY newid()
                                )";

            QueryCommand qc = new QueryCommand(strSql, ViewPponDeal.Schema.Provider.Name);
            ViewPponDealCollection vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection DealTimeSlotGetToday(int cid)
        {
            string sql = "select * from deal_time_slot with(nolock) join view_ppon_deal with(nolock) on deal_time_slot.business_hour_GUID=view_ppon_deal.business_hour_GUID  where GETDATE() between effective_start and effective_end and city_id=@cid and status=0 and business_hour_status&2048=0 order by sequence ";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.Parameters.Add("cid", cid);
            ViewPponDealCollection rtnCol = new ViewPponDealCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public ViewPponDealCollection ViewPponDealGetBySellerGuid(Guid sellerGuid)
        {
            string sql = @"SELECT * " +
             @"FROM view_ppon_deal WITH(NOLOCK) " +
             @"WHERE seller_GUID = @seller_GUID " +
             @"	and business_hour_status & 256 = 0 " +
             @"ORDER BY business_hour_deliver_time_e DESC ";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@seller_GUID", sellerGuid, DbType.Guid);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetBySellerGuid(Guid sellerGuid, Guid businessHourGuid)
        {
            string sql = @"SELECT * " +
             @"FROM view_ppon_deal WITH(NOLOCK) " +
             @"WHERE seller_GUID = @seller_GUID " +
             @"	and group_order_status   & @group_order_status = 0 " +
             @"	and business_hour_status & @business_hour_status1 = 0 " +
             @"	and business_hour_status & @business_hour_status2 = 0 " +
             @"	and business_hour_guid != @bid";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@seller_GUID", sellerGuid, DbType.Guid);
            qc.AddParameter("@group_order_status", (int)GroupOrderStatus.Completed, DbType.Int32);
            qc.AddParameter("@business_hour_status1", (int)BusinessHourStatus.ComboDealMain, DbType.Int32);
            qc.AddParameter("@business_hour_status2", (int)BusinessHourStatus.ComboDealSub, DbType.Int32);
            qc.AddParameter("@bid", businessHourGuid, DbType.Guid);

            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetBySellerGuidWithDealDone(Guid sellerGuid)
        {
            string sql = @" SELECT * " + "\n" +
                         @" FROM view_ppon_deal" + "\n" +
                         @" WHERE seller_GUID = @seller_GUID" + "\n" +
                         @"   AND ordered_quantity >= business_hour_order_minimum" + "\n" +
                         @"	  AND business_hour_status & 256 = 0 " +
                         @" ORDER BY business_hour_deliver_time_e DESC ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@seller_GUID", sellerGuid, DbType.Guid);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListByDeliverTimeStart(DateTime sDate)
        {
            string sql = @" SELECT * " + "\n" +
                         @" FROM view_ppon_deal WITH(NOLOCK) " + "\n" +
                         @" WHERE @sDate Between business_hour_deliver_time_s AND business_hour_deliver_time_e " +
                         @"       AND ordered_quantity >= business_hour_order_minimum " + "\n" +
                         @"       AND delivery_type = @deliveryType";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@deliveryType", DeliveryType.ToShop, DbType.Int32);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListByDeliverTimeStartForSendGift(DateTime sDate)
        {
            string sql = @" SELECT * " + "\n" +
                         @" FROM view_ppon_deal WITH(NOLOCK) " + "\n" +
                         @" WHERE @sDate Between business_hour_deliver_time_s AND business_hour_deliver_time_e " +
                         @"       AND ordered_quantity >= business_hour_order_minimum " + "\n" +
                         @"       AND delivery_type = @deliveryType" +
                         @"       AND business_hour_guid in (" +
                         @"       select bid from view_mgm_gift" +
                         @"       where is_used=0 and accept in ('1','6') and bid in ('43B2CC6E-862A-458E-8A6D-3AB85D9CE74F','053a35bd-9fbb-44e0-b5a1-c2d210f3ad31'))";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@deliveryType", DeliveryType.ToShop, DbType.Int32);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        /// <summary>
        /// 攝影檔期表
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="deptId"></param>
        /// <returns></returns>
        public ViewPponDealCalendarCollection ViewPponDealCalendarGetListByPeriod(DateTime dateStart, DateTime dateEnd, string deptId)
        {
            string sql = @" SELECT * from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + @" with(nolock)
                            where " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + @" >= @dateStart and " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + @" < @dateEnd
                            and " + ViewPponDealCalendar.Columns.BusinessHourStatus + @" & @mask = 0 and " + ViewPponDealCalendar.Columns.GroupOrderStatus + " & @famideal = 0";
            if (deptId != "")
            {
                sql += " and (" + ViewPponDealCalendar.Columns.DevelopeDeptId + "=@deptId or " + ViewPponDealCalendar.Columns.OperationDeptId + "=@deptId)";
            }

            var qc = new QueryCommand(sql, ViewPponDealCalendar.Schema.Provider.Name);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            qc.AddParameter("@deptId", deptId, DbType.String);
            qc.AddParameter("@mask", (int)BusinessHourStatus.ComboDealSub, DbType.Int32);
            qc.AddParameter("@famideal", (int)GroupOrderStatus.FamiDeal, DbType.Int32);
            var vpolCol = new ViewPponDealCalendarCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        /// <summary>
        /// 檔期表
        /// </summary>
        /// <param name="dateStart">開始日期</param>
        /// <param name="dateEnd">結束日期</param>
        /// <param name="deptId">設定單位</param>
        /// <param name="CrossDeptTeam">跨區設定</param>
        /// <param name="sSQL"></param>
        /// <returns></returns>
        public ViewPponDealCalendarCollection ViewPponDealCalendarGetListByPeriod(DateTime dateStart, DateTime dateEnd, string deptId, string CrossDeptTeam, ref string sSQL)
        {
            string sql = @" SELECT * from " + ViewPponDealCalendar.Schema.Provider.DelimitDbName(ViewPponDealCalendar.Schema.TableName) + @" with(nolock)
                            where " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + @" >= @dateStart and " + ViewPponDealCalendar.Columns.BusinessHourOrderTimeS + @" < @dateEnd
                            and " + ViewPponDealCalendar.Columns.BusinessHourStatus + @" & @mask = 0 and " + ViewPponDealCalendar.Columns.GroupOrderStatus + " & @famideal = 0";


            var qc = new QueryCommand(sql, ViewPponDealCalendar.Schema.Provider.Name);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            qc.AddParameter("@mask", (int)BusinessHourStatus.ComboDealSub, DbType.Int32);
            qc.AddParameter("@famideal", (int)GroupOrderStatus.FamiDeal, DbType.Int32);


            #region CrossDeptTeam
            if (!string.IsNullOrEmpty(CrossDeptTeam))
            {
                //跨區
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                string tmpsql = "";
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        //組
                        tmpsql += "(" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.DevelopeTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                        tmpsql += "(" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c.Substring(0, c.IndexOf("[")) + "' and " +
                                        ViewPponDealCalendar.Columns.OperationTeamNo + " in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                    }
                    else
                    {
                        //區
                        tmpsql += " (" + ViewPponDealCalendar.Columns.DevelopeDeptId + "='" + c + "') or ";
                        tmpsql += " (" + ViewPponDealCalendar.Columns.OperationDeptId + "='" + c + "') or ";
                    }


                }
                tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                qc.CommandSql += tmpsql;


            }
            else
            {
                //全區有設定單位
                if (!string.IsNullOrEmpty(deptId))
                {
                    if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                    {
                        qc.CommandSql += SqlFragment.WHERE;
                    }
                    else
                    {
                        int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                        qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                        qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                    }


                    qc.CommandSql += "(" + ViewPponDealCalendar.Columns.DevelopeDeptId + " = " + "@DeptId or " + ViewPponDealCalendar.Columns.OperationDeptId + " = @DeptId)";
                    qc.AddParameter("@DeptId", deptId, DbType.String);
                }


            }
            #endregion


            sSQL = qc.CommandSql + "==param==" + new JsonSerializer().Serialize(qc.Parameters);
            var vpolCol = new ViewPponDealCalendarCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        /// <summary>
        /// 數量管理查詢
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <returns></returns>
        public ViewPponDealCollection ViewPponDealGetListByPeriod(Guid bid, DateTime dateStart, DateTime dateEnd)
        {
            string sql = @" SELECT * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" with(nolock)
                            where " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" > @dateStart and " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" < @dateEnd
                            and " + ViewPponDeal.Columns.BusinessHourGuid + @" = @bid
                            and " + ViewPponDeal.Columns.DealEmpName + @" in ( ";
            sql += " SELECT " + Employee.Columns.EmpName + @" FROM " + Employee.Schema.Provider.DelimitDbName(Employee.Schema.TableName) + @" with(nolock) ";
            sql += " where 1=1 ";

            sql += ") ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            qc.AddParameter("@bid", bid, DbType.Guid);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListBySellerGuid(DateTime dateStart, DateTime dateEnd, Guid sellerGuid)
        {
            string sql = @" SELECT * from " + ViewPponDeal.Schema.Provider.DelimitDbName(ViewPponDeal.Schema.TableName) + @" with(nolock)
                            where (" + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" between @dateStart and @dateEnd or " + ViewPponDeal.Columns.BusinessHourOrderTimeS + @" between @dateStart and @dateEnd)
                            and " + ViewPponDeal.Columns.BusinessHourStatus + @" & @mask = 0 
                            and " + ViewPponDeal.Columns.SellerGuid + "=@sellerGuid";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@dateStart", dateStart, DbType.DateTime);
            qc.AddParameter("@dateEnd", dateEnd, DbType.DateTime);
            qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            qc.AddParameter("@mask", (int)BusinessHourStatus.ComboDealSub, DbType.Int32);
            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListByPeriod(DateTime dateStart, DateTime dateEnd)
        {
            return DB.SelectAllColumnsFrom<ViewPponDeal>().Where(ViewPponDeal.Columns.BusinessHourOrderTimeS).IsGreaterThanOrEqualTo(dateStart).And(ViewPponDeal.Columns.BusinessHourOrderTimeS).IsLessThan(dateEnd).ExecuteAsCollection<ViewPponDealCollection>();
        }

        public ViewPponDealCollection ViewPponDealGetListByNoReturn(DateTime theDay)
        {
            return DB.SelectAllColumnsFrom<ViewPponDeal>()
                .Where(ViewPponDeal.Columns.BusinessHourOrderTimeE).IsBetweenAnd(theDay.AddDays(-1), theDay.AddSeconds(-1))
                .ExecuteAsCollection<ViewPponDealCollection>();
        }

        public ViewPponDealCollection ViewPponDealGetListByExpireNoReturn(DateTime theDay)
        {
            return DB.SelectAllColumnsFrom<ViewPponDeal>()
                .Where(ViewPponDeal.Columns.BusinessHourDeliverTimeE).IsBetweenAnd(theDay.AddDays(-1), theDay.AddSeconds(-1))
                .ExecuteAsCollection<ViewPponDealCollection>();
        }

        public ViewPponDealCollection ViewPponDealGetListByVerifyInform(DateTime theDay)
        {
            string sql = @" SELECT * " +
             @" FROM view_ppon_deal WITH(NOLOCK) " +
             @" WHERE business_hour_deliver_time_e Between @startDate AND @endDate " +
             @" AND item_price > 0 " +
             @" AND (group_order_status & @noRefund > 0 OR group_order_status & @expireNoRefund > 0 OR group_order_status & @daysNoRefund > 0 Or group_order_status & @noRefundBeforeDays > 0 )";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@startDate", theDay.AddDays(-1), DbType.DateTime);
            qc.AddParameter("@endDate", theDay.AddSeconds(-1), DbType.DateTime);
            qc.AddParameter("@noRefund", (int)GroupOrderStatus.NoRefund, DbType.Int32);
            qc.AddParameter("@expireNoRefund", (int)GroupOrderStatus.ExpireNoRefund, DbType.Int32);
            qc.AddParameter("@daysNoRefund", (int)GroupOrderStatus.DaysNoRefund, DbType.Int32);
            qc.AddParameter("@noRefundBeforeDays", (int)GroupOrderStatus.NoRefundBeforeDays, DbType.Int32);

            var vpolCol = new ViewPponDealCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        public ViewPponDealCollection ViewPponDealGetListByRecentDeal()
        {
            return null;
        }

        /// <summary>
        /// 取得需要發送核銷通知信的對象清單 (憑證類才要核銷)
        /// </summary>
        public ViewPponDealCollection ViewPponDealGetVerificationNoticeList()
        {
            int days = config.SendVerificationNoticeEmailBeforeDays;
            DateTime startDateTime = DateTime.Now.Date.AddDays(days);
            DateTime endDateTime = startDateTime.Add(new TimeSpan(23, 59, 59));

            ViewPponDealCollection list = DB.SelectAllColumnsFrom<ViewPponDeal>()
                .WhereExpression(ViewPponDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop)
                        .And(ViewPponDeal.Columns.ChangedExpireDate).IsBetweenAnd(startDateTime, endDateTime)
                .OrExpression(ViewPponDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop)
                        .And(ViewPponDeal.Columns.ChangedExpireDate).IsNull()
                        .And(ViewPponDeal.Columns.BusinessHourDeliverTimeE).IsBetweenAnd(startDateTime, endDateTime)
                .OrderAsc(ViewPponDeal.Columns.BusinessHourDeliverTimeS).ExecuteAsCollection<ViewPponDealCollection>();
            return list;
        }

        #endregion ViewPponDeal

        #region subscription

        public void SubscriptionStatusSleepUpdate(int count, int minId, int maxId)
        {
            string strSql = @"UPDATE " + Subscription.Schema.TableName + @" SET status = 0 where id in(" +
                            "select s.[id] from " + Subscription.Schema.TableName + " as s with(nolock) " +
                            "LEFT JOIN (select account_id,max(create_time)as last_date from " + AccountAudit.Schema.TableName + @" with(nolock) where action in (0,1,2) group by account_id) a on s.[email] = a.[account_id] " +
                            "LEFT JOIN " + Member.Schema.TableName + " as m on s.email = m.user_email " +
                            " where s.status = 1 and last_date < DATEADD(MONTH, @count, GETDATE()) and unique_id is not null" +
                            " AND id>=@min_id and id<@max_id )";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);
            qc.AddParameter("@count", count, DbType.Int32);
            qc.AddParameter("@min_id", minId, DbType.Int32);
            qc.AddParameter("@max_id", maxId, DbType.Int32);
            DB.Execute(qc);
        }

        public void SubscriptionStatusDefaultUpdate(int count, int minId, int maxId)
        {
            string strSql = @"UPDATE " + Subscription.Schema.TableName + @" SET status = 1 where id in(" +
                            "select s.[id] from " + Subscription.Schema.TableName + " as s with(nolock) " +
                            "LEFT JOIN (select account_id,max(create_time)as last_date from " + AccountAudit.Schema.TableName + @" with(nolock) where action in (0,1,2,13,14) group by account_id) a on s.[email] = a.[account_id] " +
                            "LEFT JOIN " + Member.Schema.TableName + " as m on s.email = m.user_email " +
                            " where s.status = 0 and last_date > DATEADD(MONTH, @count, GETDATE()) and unique_id is not null" +
                            " AND id>=@min_id and id<@max_id )";
            strSql = @"
                UPDATE subscription set status = 1 where id in(
                    select s.id from subscription as s with(nolock)     
                        where s.status = 0 and 
		                s.email in 
		                (select user_email from member where unique_id in 
			                (select account_audit.user_id from account_audit with(nolock) where action in (0,1,2,13,14) and create_time > DATEADD(MONTH, -12, GETDATE()))
		                )
                        AND id>=@min_id and id<@max_id )
                ";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);
            qc.AddParameter("@count", count, DbType.Int32);
            qc.AddParameter("@min_id", minId, DbType.Int32);
            qc.AddParameter("@max_id", maxId, DbType.Int32);
            DB.Execute(qc);
        }

        public void SubscriptionDelete(int id)
        {
            DB.Delete<Subscription>(Subscription.Columns.Id, id);
        }

        public SubscriptionCollection SubscriptionGetList(string email)
        {
            return DB.SelectAllColumnsFrom<Subscription>()
                .Where(Subscription.Columns.Email).IsEqualTo(email)
                .ExecuteAsCollection<SubscriptionCollection>();
        }

        /// <summary>
        /// 藉由email以及cityid來取出訂閱電子報的所有資料欄位
        /// </summary>
        /// <param name="email">電子郵件</param>
        /// <param name="categoryId">分類編號</param>
        /// <returns></returns>
        public Subscription SubscriptionGet(string email, int categoryId)
        {
            return DB.SelectAllColumnsFrom<Subscription>().NoLock()
                .Where(Subscription.EmailColumn).IsEqualTo(email).And(Subscription.CategoryIdColumn).IsEqualTo(categoryId)
                .ExecuteSingle<Subscription>();
        }

        public void SubscriptionSet(Subscription ss)
        {
            DB.Save<Subscription>(ss);
        }

        /// <summary>
        /// 修改Subscription的狀態值
        /// </summary>
        /// <param name="id">訂閱信箱的id</param>
        /// <param name="iStats">狀態值，請參考SubscribeConfig.SubscribeType</param>
        /// <remarks>[2011/05] Ahdaa       新增</remarks>
        public void SubscriptionEditStatus(int id, int iStats)
        {
            var ses = new Update(Subscription.Schema.TableName, Subscription.Schema.Provider.Name);
            ses.Set(Subscription.Columns.Status).EqualTo(iStats)
                .Where(Subscription.IdColumn).IsEqualTo(id)
                .Execute();
        }

        /// <summary>
        /// 重新訂閱
        /// </summary>
        /// <param name="id">訂閱信箱的id</param>
        /// <remarks>[2011/05] Ahdaa       新增</remarks>
        public void SubscriptionReSet(int id)
        {
            var srs = new Update(Subscription.Schema.TableName, Subscription.Schema.Provider.Name);
            srs.Set(Subscription.Columns.ResubscriptionTime).EqualTo(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000"))
                .Where(Subscription.IdColumn).IsEqualTo(id)
                .Execute();
            SubscriptionEditStatus(id, (SubscriptionGetStatus(id) ^ Convert.ToInt32(SubscribeType.Unsubscribe)));
        }

        /// <summary>
        /// 取消訂閱
        /// </summary>
        /// <param name="id">訂閱信箱的id</param>
        /// <remarks>[2011/05] Ahdaa       新增</remarks>
        public void SubscriptionCancel(int id)
        {
            var sc = new Update(Subscription.Schema.TableName, Subscription.Schema.Provider.Name);

            sc.Set(Subscription.Columns.UnsubscriptionTime).EqualTo(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.000"))
                .Where(Subscription.IdColumn).IsEqualTo(id)
                .Execute();
            SubscriptionEditStatus(id, (SubscriptionGetStatus(id) | Convert.ToInt32(SubscribeType.Unsubscribe)));
        }

        /// <summary>
        /// 取得Subscription的狀態值
        /// </summary>
        /// <param name="id">訂閱信箱的id</param>
        /// <returns>[2011/06] Ahdaa       新增</returns>
        public int SubscriptionGetStatus(int id)
        {
            string strSql = "SELECT status FROM subscription WHERE id = @Id";
            QueryCommand qc = new QueryCommand(strSql, "");
            qc.AddParameter("Id", id, DbType.Int32);
            Subscription statusCol = new Subscription();
            statusCol.LoadAndCloseReader(DataService.GetReader(qc));

            return (int)statusCol.Status;
        }

        public int SubscriptionGetLatestId()
        {
            string strSql = "SELECT Top 1 " + Subscription.Columns.Id + " FROM " + Subscription.Schema.TableName + " with(nolock) Order By " + Subscription.Columns.Id + " desc";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);
            return (int)DataService.ExecuteScalar(qc);
        }

        public SubscriptionCollection SubscriptionGetListByCategoryList(params int[] categoryIdList)
        {
            if (categoryIdList.Length == 0)
            {
                return new SubscriptionCollection();
            }
            string sTmpName = "@Sid";
            string sIdList = string.Empty;
            int i = 0;
            foreach (int iId in categoryIdList)
            {
                sIdList += "," + sTmpName + i.ToString();
                i++;
            }
            sIdList = sIdList.Substring(1, sIdList.Length - 1);
            string strSql = " select distinct email from subscription with(nolock) " +
                            " where subscription." + Subscription.Columns.CategoryId +
                            " in (" + sIdList + ") AND subscription.status & @SubStatus = 0 ";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);

            i = 0;
            foreach (int iId in categoryIdList)
            {
                qc.AddParameter(sTmpName + i.ToString(), iId, DbType.Int32);
                i++;
            }
            qc.AddParameter("SubStatus", SubscribeType.Unsubscribe, DbType.Int32);

            SubscriptionCollection mailCol = new SubscriptionCollection();
            mailCol.LoadAndCloseReader(DataService.GetReader(qc));

            return mailCol;
        }

        public SubscriptionCollection SubscriptionGetListByCategoryId(int categoryId, int min_id, int max_id)
        {
            string strSql = "select * from subscription with(nolock) " +
                " where " + Subscription.Columns.CategoryId + " = @CategoryId AND status & @SubStatus = 0" +
                " AND (reliable=1 OR reliable is null) and id>=@min_id and id<@max_id";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);
            qc.AddParameter("@CategoryId", categoryId, DbType.Int32);
            qc.AddParameter("@SubStatus", SubscribeType.Unsubscribe, DbType.Int32);
            qc.AddParameter("@min_id", min_id, DbType.Int32);
            qc.AddParameter("@max_id", max_id, DbType.Int32);
            SubscriptionCollection mailCol = new SubscriptionCollection();
            mailCol.LoadAndCloseReader(DataService.GetReader(qc));
            return mailCol;
        }

        public List<SubscriptionModel> SubscriptionGetListByCategoryId(int categoryId)
        {
            List<SubscriptionModel> result = new List<SubscriptionModel>();

            //status & 4 = 0 參考 SubscribeType.Unsubscribe ，取消訂閱
            string strSql = "select id,email,status,reliable from subscription with(nolock) " +
                " where category_id= @categoryId AND status & 4 = 0" +
                " AND (reliable=1 OR reliable is null)";
            QueryCommand qc = new QueryCommand(strSql, Subscription.Schema.Provider.Name);
            qc.AddParameter("@categoryId", categoryId, DbType.Int32);
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    if (reader.IsDBNull(1))
                    {
                        continue;
                    }
                    int id = reader.GetInt32(0);
                    string email = reader.GetString(1);
                    int? status = reader.IsDBNull(2) ? (int?)null : reader.GetInt32(2);
                    bool? reliable = reader.IsDBNull(3) ? (bool?)null : reader.GetBoolean(3);

                    result.Add(new SubscriptionModel
                    {
                        Id = id,
                        Email = email,
                        Status = status,
                        Reliable = reliable
                    });
                }
            }
            return result;
        }

        public void SubscriptionUpdateReliable(string email, bool reliable)
        {
            var srs = new Update(Subscription.Schema.TableName, Subscription.Schema.Provider.Name);
            srs.Set(Subscription.Columns.Reliable).EqualTo(reliable)
                .Where(Subscription.EmailColumn).IsEqualTo(email)
                .Execute();
        }

        public bool SubscriptionSetToNewUser(string origUserName, string newUserName)
        {
            DB.Update<Subscription>().Set(Subscription.EmailColumn).EqualTo(newUserName).Where(Subscription.EmailColumn)
                .IsEqualTo(origUserName).Execute();
            return true;
        }

        #endregion subscription

        #region SubscriptionNotice

        public int SubscriptionNoticeSet(SubscriptionNotice subscription)
        {
            return DB.Save(subscription);
        }

        /// <summary>
        /// 依據裝置與訂閱的頻道區域，取得訂閱紀錄。
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="channelId"></param>
        /// <param name="subCategoryId"></param>
        /// <returns></returns>
        public SubscriptionNotice SubscriptionNoticeGet(int deviceId, int channelId, int? subCategoryId)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<SubscriptionNotice>()
                               .Where(SubscriptionNotice.Columns.DeviceId)
                               .IsEqualTo(deviceId)
                               .And(SubscriptionNotice.Columns.ChannelCategoryId)
                               .IsEqualTo(channelId);
            if (subCategoryId == null)
            {
                query.And(SubscriptionNotice.Columns.SubcategoryId).IsNull();
            }
            else
            {
                query.And(SubscriptionNotice.Columns.SubcategoryId).IsEqualTo(subCategoryId);
            }

            return query.ExecuteSingle<SubscriptionNotice>();
        }
        /// <summary>
        /// 修改訂閱推播的狀態，將某個裝置的訂閱資料enabled值全部設為某個數值
        /// </summary>
        /// <param name="deviceId"></param>
        /// <param name="enabled"></param>
        /// <returns></returns>
        public int SubscriptionNoticeSetEnabled(int deviceId, bool enabled)
        {
            return DB.Update<SubscriptionNotice>().Set(SubscriptionNotice.Columns.Enabled).EqualTo(enabled)
                .Where(SubscriptionNotice.Columns.DeviceId).IsEqualTo(deviceId).Execute();
        }
        #endregion SubscriptionNotice

        #region TempSession

        public void TempSessionSetForUpdate(TempSession ts)
        {
            TempSessionCollection tsc = TempSessionGetList(ts.SessionId);
            if (tsc.Count > 0)
            {
                tsc[0].Name = ts.Name;
                tsc[0].ValueX = ts.ValueX;
                ts.CreateTime = DateTime.Now;
                TempSessionSet(tsc[0]);
            }
            else
            {
                TempSession sts = new TempSession();
                sts.SessionId = ts.SessionId;
                sts.Name = ts.Name;
                sts.ValueX = ts.ValueX;
                sts.CreateTime = DateTime.Now;
                TempSessionSet(sts);
            }
        }

        public void TempSessionSet(TempSession ts)
        {
            ts.CreateTime = DateTime.Now;
            DB.Save<TempSession>(ts);
        }

        public void TempSessionDelete(int id)
        {
            DB.Delete<TempSession>(TempSession.Columns.Id, id);
        }

        public TempSessionCollection TempSessionGetList(string sessionId)
        {
            return DB.SelectAllColumnsFrom<TempSession>()
                           .Where(TempSession.Columns.SessionId).IsEqualTo(sessionId)
                           .ExecuteAsCollection<TempSessionCollection>();
        }

        public void TempSessionDelete(string sessionId)
        {
            Query delete = new Query(TempSession.Schema);
            delete.QueryType = QueryType.Delete;
            delete.WHERE(TempSession.Columns.SessionId, sessionId);
            delete.Execute();
        }

        public TempSession TempSessionGet(string sessionId, string name)
        {
            return DB.QueryOver<TempSession>().NoLock().Where(t => t.SessionId == sessionId && t.Name == name)
                .FirstOrDefault();
        }

        #endregion TempSession

        #region HotDeal

        public HotDealConfigCollection HotDealConfigGetAll()
        {
            return DB.SelectAllColumnsFrom<HotDealConfig>().ExecuteAsCollection<HotDealConfigCollection>();
        }

        public HotDealConfig HotDealConfigGet(int cityId)
        {
            return DB.Get<HotDealConfig>(cityId);
        }

        public bool HotDealConfigSet(HotDealConfig hdc)
        {
            DB.Save<HotDealConfig>(hdc);
            return true;
        }

        public DealTimeSlotHotDealOrderedTotalCollection HotDealOrderedTotalGetAll()
        {
            return DB.SelectAllColumnsFrom<DealTimeSlotHotDealOrderedTotal>()
                .ExecuteAsCollection<DealTimeSlotHotDealOrderedTotalCollection>();
        }

        //清除DealTimeSlot所有熱銷註記
        public int ClearHotDealFlag(DateTime workDate)
        {
            return DB.Update<DealTimeSlot>().Set(DealTimeSlot.Columns.IsHotDeal).EqualTo(false)
                .Where(DealTimeSlot.Columns.EffectiveStart).IsGreaterThanOrEqualTo(workDate)
                .And(DealTimeSlot.Columns.EffectiveStart).IsLessThan(workDate.AddDays(1))
                .And(DealTimeSlot.Columns.IsHotDeal).IsEqualTo(true)
                .Execute();
        }

        //依頻道清除DealTimeSlot熱銷註記
        public int ClearHotDealFlagByCityId(int cityId, DateTime workDate)
        {
            return DB.Update<DealTimeSlot>().Set(DealTimeSlot.Columns.IsHotDeal).EqualTo(false)
                .Where(DealTimeSlot.Columns.CityId).IsEqualTo(cityId)
                .And(DealTimeSlot.Columns.EffectiveStart).IsGreaterThanOrEqualTo(workDate)
                .And(DealTimeSlot.Columns.EffectiveStart).IsLessThan(workDate.AddDays(1))
                .And(DealTimeSlot.Columns.IsHotDeal).IsEqualTo(true)
                .Execute();
        }

        //清除熱銷暫存表所有資料
        public void ClearDealTimeSlotHotDealOrderedTotal()
        {
            string sql = "TRUNCATE TABLE dbo.deal_time_slot_hot_deal_ordered_total";
            QueryCommand qc = new QueryCommand(sql, "");
            DataService.ExecuteScalar(qc);
        }

        //依頻道清除清除熱銷暫存表
        public int ClearDealTimeSlotHotDealOrderedTotalByCityId(int cityId)
        {
            return DB.Delete().From(DealTimeSlotHotDealOrderedTotal.Schema.TableName)
            .Where(DealTimeSlotHotDealOrderedTotal.Columns.CityId).IsEqualTo(cityId)
            .Execute();
        }

        //取得最近n前n檔熱銷檔次放入熱銷暫存表GetHotDealToTempTable
        public void GetHotDealToTempTable(int topN, int lastNday, int cityId, DateTime workDate)
        {
            var lastDate = DateTime.Now.AddDays(-lastNday);
            var startDate = workDate;
            var endDate = workDate.AddDays(1);

            string sql = string.Format(@"
                    INSERT INTO dbo.deal_time_slot_hot_deal_ordered_total
                    select top {0} city_id, main_bid, effective_start, sum(ordered_total) ordered_total,
                            (case when sell_days > {1} then SUM(ordered_total*1.0)/{1} else SUM(ordered_total)/sell_days end) avg_total, sell_days
                      from dbo.view_ppon_hot_deal_preparation
                     where city_id = @cityId 
                       and sales_date > @lastDate
                       and effective_start >= @startDate and effective_start < @endDate
                     group by city_id, main_bid, effective_start, sell_days
                     order by (case when sell_days > {1} then SUM(ordered_total*1.0)/{1} else SUM(ordered_total)/sell_days end) desc"
                    , topN, lastNday);

            QueryCommand qc = new QueryCommand(sql, "");
            qc.Parameters.Add("@cityId", cityId, DbType.Int32);
            qc.Parameters.Add("@lastDate", lastDate, DbType.DateTime);
            qc.Parameters.Add("@startDate", startDate, DbType.DateTime);
            qc.Parameters.Add("@endDate", endDate, DbType.DateTime);
            DataService.ExecuteScalar(qc);
        }

        //註記所有熱銷
        public void SetDealTimeSlotHotDealFlag()
        {
            string sql = @"update deal_time_slot set deal_time_slot.is_hot_deal = 1
                             from dbo.deal_time_slot_hot_deal_ordered_total t with(nolock)
                            where t.city_id = deal_time_slot.city_id
                              and t.business_hour_guid = deal_time_slot.business_hour_GUID
                              and t.effective_start = deal_time_slot.effective_start";
            QueryCommand qc = new QueryCommand(sql, "");
            DataService.ExecuteScalar(qc);
        }
        //依頻道註記熱銷
        public void SetDealTimeSlotHotDealFlagByCity(int cityId)
        {
            string sql = @"update deal_time_slot set deal_time_slot.is_hot_deal = 1
                             from dbo.deal_time_slot_hot_deal_ordered_total t with(nolock)
                            where t.city_id = deal_time_slot.city_id and t.city_id = @cityId
                              and t.business_hour_guid = deal_time_slot.business_hour_GUID
                              and t.effective_start = deal_time_slot.effective_start";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.Parameters.Add("@cityId", cityId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region TempDealTimeSlotSeq

        public bool TruncateAndBulkInsertTempDealTimeSlotSeqCol(TempDealTimeSlotSeqCollection data)
        {
            string sql = string.Format(@"truncate table " + TempDealTimeSlotSeq.Schema.TableName);
            QueryCommand qc = new QueryCommand(sql, "");
            DataService.ExecuteScalar(qc);

            return DB.BulkInsert(data);
        }

        public int UpdateDealTimeSlotSeqFromTemp()
        {
            string sql = @"update deal_time_slot set deal_time_slot.sequence = t.sequence
                           from [dbo].[temp_deal_time_slot_seq] t
                           where deal_time_slot.business_hour_GUID = t.business_hour_GUID
	                       and deal_time_slot.city_id = t.city_id
	                       and deal_time_slot.effective_start = t.effective_start
                           select @@rowCount as reowCount";

            QueryCommand qc = new QueryCommand(sql, "");
            var rowCount = (int)DataService.ExecuteScalar(qc);

            return rowCount;
        }

        #endregion

        #region DealTimeSlot

        public bool DealTimeSlotUpdate(DealTimeSlot data)
        {
            DB.Update<DealTimeSlot>(data);
            return true;
        }

        public bool DealTimeSlotUpdateStatusWithTransaction(List<DealTimeSlot> dtsList, int status)
        {
            var queries = dtsList.Select(dts => new Update(DealTimeSlot.Schema).Set(DealTimeSlot.StatusColumn).EqualTo(status)
                .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(dts.BusinessHourGuid)
                .And(DealTimeSlot.CityIdColumn).IsEqualTo(dts.CityId)
                .And(DealTimeSlot.EffectiveStartColumn).IsEqualTo(dts.EffectiveStart)).ToList();
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }
        /// <summary>
        /// 異動指定檔次、頻道，且時間在 effectStart 之後的顯示狀態
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="cityId"></param>
        /// <param name="effectStart"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public bool DealTimeSlotUpdateStatusWithTransaction(Guid bid, int cityId, DateTime effectStart, int status)
        {
            using (var tran = TransactionScopeBuilder.CreateReadCommitted())
            {
                new Update(DealTimeSlot.Schema).Set(DealTimeSlot.StatusColumn).EqualTo(status)
                    .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(bid)
                    .And(DealTimeSlot.CityIdColumn).IsEqualTo(cityId)
                    .And(DealTimeSlot.EffectiveStartColumn).IsGreaterThanOrEqualTo(effectStart).Execute();
                tran.Complete();
            }
            return true;
        }

        public bool DealTimeSlotUpdateSeqWithTransaction(List<ViewPponDealTimeSlot> vpdtsList)
        {
            var queries = new List<SqlQuery>();
            foreach (var vpdts in vpdtsList)
            {
                queries.Add(
                    new Update(DealTimeSlot.Schema).Set(DealTimeSlot.SequenceColumn).EqualTo(vpdts.Sequence)
                        .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(vpdts.BusinessHourGuid)
                        .And(DealTimeSlot.CityIdColumn).IsEqualTo(vpdts.CityId)
                        .And(DealTimeSlot.EffectiveStartColumn).IsEqualTo(vpdts.EffectiveStart)
                );
            }
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }

        public bool DealTimeSlotUpdateSeqAddOne(int cityId, DateTime effectiveStartDate)
        {
            string setexpression = DealTimeSlot.Columns.Sequence + " = " + DealTimeSlot.Columns.Sequence + " + 1 ";
            var sql = "update [" + DealTimeSlot.Schema.TableName + "] set " + setexpression +
                 " where " + DealTimeSlot.Columns.CityId + " = @cityId and " +
                         DealTimeSlot.Columns.EffectiveStart + " >= @effDateS " +
                         " and " + DealTimeSlot.Columns.EffectiveStart + " < @effDateE ";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int16);
            qc.AddParameter("@effDateS", effectiveStartDate.ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);
            qc.AddParameter("@effDateE", effectiveStartDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);
            DataService.ExecuteScalar(qc);
            return true;
        }

        public void SortDealTimeSlotBulkInsert(SortDealTimeSlotCollection data)
        {
            DB.BulkInsert(data);
        }

        public void UpdateDealTimeSlotFromSortDealTimeSlotCol()
        {
            string sql = @"update deal_time_slot set sequence = ss.seq
                            from sort_deal_time_slot ss
                            where business_hour_GUID = ss.bid
                                and city_id = cityid
                                and effective_start = ss.effstart";
            var qc = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            DataService.ExecuteScalar(qc);
        }

        public void TruncateSortDealTimeSlot()
        {
            string sql = "truncate table sort_deal_time_slot";
            var qc = new QueryCommand(sql, SortDealTimeSlot.Schema.Provider.Name);
            DataService.ExecuteScalar(qc);
        }

        public DealTimeSlot DealTimeSlotGet(Guid guid, int cityId, DateTime effectiveStart)
        {
            return DB.SelectAllColumnsFrom<DealTimeSlot>().NoLock()
                .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(guid)
                .And(DealTimeSlot.CityIdColumn).IsEqualTo(cityId)
                .And(DealTimeSlot.EffectiveStartColumn).IsEqualTo(effectiveStart.ToString("yyyy/MM/dd HH:mm:ss"))
                .ExecuteSingle<DealTimeSlot>();
        }

        public DealTimeSlotCollection DealTimeSlotGetCol(Guid guid)
        {
            return DB.Select().From(DealTimeSlot.Schema.TableName).NoLock()
                .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(guid)
                .And(DealTimeSlot.EffectiveStartColumn).IsLessThanOrEqualTo(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                .And(DealTimeSlot.EffectiveEndColumn).IsGreaterThanOrEqualTo(DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"))
                .ExecuteAsCollection<DealTimeSlotCollection>();
        }

        public DealTimeSlotCollection DealTimeSlotGetAllCol(Guid guid)
        {
            return DB.Select().From(DealTimeSlot.Schema.TableName).NoLock()
                .Where(DealTimeSlot.BusinessHourGuidColumn).IsEqualTo(guid)
                .ExecuteAsCollection<DealTimeSlotCollection>();
        }

        public DealTimeSlot DealTimeSlotGetMainDeal(int cityId, DateTime showTime, DepartmentTypes[] departments)
        {
            string deptSql = string.Empty;
            string[] paramNames = null;
            if (departments != null && departments.Length > 0)
            {
                paramNames = departments.Select((s, i) => "@dp" + i.ToString()).ToArray();
                deptSql = string.Format(@"and {0} in ({1}) ", "c.department", string.Join(",", paramNames));
            }

            string sql = "select top 1 * from " +
                         DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + " a with(nolock) " +
                         "inner join business_hour b with(nolock) on a.business_hour_guid = b.guid inner join seller c on b.seller_guid = c.guid " +
                         " where a." + DealTimeSlot.Columns.EffectiveStart + "<= @effStart " +
                         " and a." + DealTimeSlot.Columns.EffectiveEnd + "> @effEnd " +
                         " and a." + DealTimeSlot.Columns.CityId + " = @cityId " +
                         " and a." + DealTimeSlot.Columns.Status + " = @status " +
                         deptSql +
                         " order by a." + DealTimeSlot.Columns.Sequence + " asc";
            QueryCommand qc = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            qc.Parameters.Add("@effStart", showTime.ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);
            qc.Parameters.Add("@effEnd", showTime.ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);
            qc.Parameters.Add("@cityId", cityId, DbType.Int32);
            qc.Parameters.Add("@status", (int)DealTimeSlotStatus.Default, DbType.Int32);

            if (paramNames != null)
            {
                for (int i = 0; i < paramNames.Length; i++)
                {
                    qc.Parameters.Add(paramNames[i], (int)departments[i], DbType.Int32);
                }
            }

            DealTimeSlotCollection rtnCol = new DealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            if (rtnCol.Count == 0)
            {
                return null;
            }
            return rtnCol[0];
        }

        public DealTimeSlotCollection DealTimeSlotGetMainDealOfAllCities(DateTime showTime)
        {
            string sql = @"
select a.* from
(select city_id, min(sequence) as sequence from deal_time_slot b with(nolock) where @showTime between b.effective_start and b.effective_end and (b.status & @mask) = @value group by city_id) c
inner join deal_time_slot a on a.city_id=c.city_id and a.sequence=c.sequence
where @showTime between a.effective_start and a.effective_end and (a.status & @mask) = @value;";
            var q = new QueryCommand(sql, DealTimeSlot.Schema.Provider.Name);
            q.AddParameter("@showTime", showTime, DbType.DateTime);
            q.AddParameter("@mask", (int)DealTimeSlotStatus.NotShowInPponDefault, DbType.Int32);
            q.AddParameter("@value", 0, DbType.Int32);
            var view = new DealTimeSlotCollection();
            view.LoadAndCloseReader(DataService.GetReader(q));
            return view;
        }

        public DealTimeSlotCollection DealTimeSlotGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", DealTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(DealTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select * from " + DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql += " order by " + orderBy;
            }

            DealTimeSlotCollection rtnCol = new DealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public DealTimeSlotCollection DealTimeSlotGetToday()
        {
            string sql = "select * from deal_time_slot with(nolock) where status=0 and GETDATE() between effective_start and effective_end";
            QueryCommand qc = new QueryCommand(sql, "");
            DealTimeSlotCollection rtnCol = new DealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public DataTable DealTomeSlotGetTodayCityId()
        {
            string strSQL = "select city_id from deal_time_slot with(nolock) where status=0 and GETDATE() between effective_start and effective_end group by city_id";
            IDataReader idr = new InlineQuery().ExecuteReader(strSQL);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public int DealTimeSlotGetMaxSeqInCityAndDay(int cityId, DateTime effectiveStartDate)
        {
            string sql = " select isnull(max(" + DealTimeSlot.Columns.Sequence + "),0) from " +
                         DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) +
                         " with(nolock) where " + DealTimeSlot.Columns.CityId + " = @cityId and " +
                         DealTimeSlot.Columns.EffectiveStart + " >= @effDateS " +
                         " and " + DealTimeSlot.Columns.EffectiveStart + " < @effDateE";

            int maxSeq = new InlineQuery(DealTimeSlot.Schema.Provider).ExecuteScalar<int>(sql, cityId,
                                                                                          effectiveStartDate.ToString("yyyy/MM/dd HH:mm:ss"),
                                                                                          effectiveStartDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"));
            return maxSeq;
        }

        public DataTable DealTimeSlotGetCityListBySeller(Guid sellerGuid)
        {
            var strSql = "select city_id from deal_time_slot with(nolock) where business_hour_GUID in (select guid from business_hour with(nolock) where seller_GUID = '" + sellerGuid.ToString() + "' ) group by city_id";
            QueryCommand qc = new QueryCommand(strSql, DealTimeSlot.Schema.Provider.Name);
            var dt = DataService.GetDataSet(qc).Tables[0];
            return dt;
        }

        public void DealTimeSlotDeleteList(Guid bid)
        {
            DB.Delete<DealTimeSlot>(DealTimeSlot.Columns.BusinessHourGuid, bid);
        }

        public void DealTimeSlotDeleteByToday(Guid bid)
        {
            DB.Delete().From(DealTimeSlot.Schema.TableName).Where(DealTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(DealTimeSlot.Columns.EffectiveStart).IsGreaterThan(DateTime.Today).Execute();
        }

        public bool DealTimeSlotSet(DealTimeSlot DealTimeSlot)
        {
            return DB.Save(DealTimeSlot) > 0;
        }

        public void DealTimeSlotDeleteAfterNewEndTime(Guid bid,DateTime newEndTime)
        {
            DB.Delete().From(DealTimeSlot.Schema.TableName).Where(DealTimeSlot.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(DealTimeSlot.Columns.EffectiveStart).IsGreaterThan(newEndTime).Execute();
        }

        #endregion DealTimeSlot

        #region ViewDealTimeSlot

        /// <summary>
        /// 取得當前時段需排序的DealTimeSlot
        /// </summary>
        /// <param name="cityIds"></param>
        /// <param name="runTime"></param>
        /// <returns></returns>
        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotToSortList(string cityIds, DateTime runTime)
        {
            return null;
        }

        /// <summary>
        /// 取得當前時段尚不需排序的DealTimeSlot(未開賣及已結束)
        /// </summary>
        /// <param name="cityIds"></param>
        /// <returns></returns>
        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotNotToSortList(string cityIds)
        {
            return null;
        }

        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotCollectionByCityAndStartTime(DateTime startTime, int cityId)
        {
            string sql = @"select * from view_ppon_Deal_time_slot 
                where 
                    city_id = @cityId and
                    effective_start >= @startTime and
                    effective_start < @endTime and
                    (business_hour_status & 2048) = 0 
                    order by sequence";

            QueryCommand qc = new QueryCommand(sql, ViewPponDealTimeSlot.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", startTime.AddDays(1), DbType.DateTime);

            ViewPponDealTimeSlotCollection col = new ViewPponDealTimeSlotCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponDealTimeSlot.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewPponDealTimeSlot.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            qc.CommandSql = "select * from " +
                            ViewPponDealTimeSlot.Schema.Provider.DelimitDbName(ViewPponDealTimeSlot.Schema.TableName) + " with(nolock)" +
                            qc.CommandSql;
            if (!orderBy.Equals(string.Empty))
            {
                qc.CommandSql += " Order by " + orderBy;
            }

            ViewPponDealTimeSlotCollection rtnCol = new ViewPponDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        /// <summary>
        /// 取得EDM使用檔次，排除隱藏檔
        /// </summary>
        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetListByCityDate(int city_id, DateTime effectdate)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponDealTimeSlot.Schema.Provider.Name);
            qc.CommandSql = "select * from " + ViewPponDealTimeSlot.Schema.Provider.DelimitDbName(ViewPponDealTimeSlot.Schema.TableName) + " with(nolock) where " +
                          ViewPponDealTimeSlot.Columns.CityId + "=@cityid and " + ViewPponDealTimeSlot.Columns.EffectiveStart + "<=@effectdate and @effectdate<" + ViewPponDealTimeSlot.Columns.EffectiveEnd +
                          " and " + ViewPponDealTimeSlot.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.ComboDealSub + "=0 " + " and " + ViewPponDealTimeSlot.Columns.Status + "=0 " +
                          " order by " + ViewPponDealTimeSlot.Columns.Sequence;
            qc.AddParameter("@cityid", city_id, DbType.Int32);
            qc.AddParameter("@effectdate", effectdate, DbType.DateTime);
            ViewPponDealTimeSlotCollection rtnCol = new ViewPponDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotGetWmsListByCityDate(DateTime effectdate)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponDealTimeSlot.Schema.Provider.Name);
            qc.CommandSql = "select * from " + ViewPponDealTimeSlot.Schema.Provider.DelimitDbName(ViewPponDealTimeSlot.Schema.TableName) + " with(nolock) where " +
                          ViewPponDealTimeSlot.Columns.IsWms + "=1 and " + ViewPponDealTimeSlot.Columns.EffectiveStart + "<=@effectdate and @effectdate<" + ViewPponDealTimeSlot.Columns.EffectiveEnd +
                          " and " + ViewPponDealTimeSlot.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.ComboDealSub + "=0 " + " and " + ViewPponDealTimeSlot.Columns.Status + "=0 " +
                          " order by " + ViewPponDealTimeSlot.Columns.Sequence;
            qc.AddParameter("@effectdate", effectdate, DbType.DateTime);
            ViewPponDealTimeSlotCollection rtnCol = new ViewPponDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }


        /// <summary>
        /// 取得EDM宅配24小時檔次，排除隱藏檔
        /// </summary>
        public ViewPponDealTimeSlotCollection ViewPponDealTimeSlotByFastShip(int city_id, DateTime effectdate, string notContainBid)
        {
            QueryCommand qc = new QueryCommand(" ", ViewPponDealTimeSlot.Schema.Provider.Name);
            qc.CommandSql = "select d.* from " + ViewPponDealTimeSlot.Schema.Provider.DelimitDbName(ViewPponDealTimeSlot.Schema.TableName) + " d with(nolock) " +
                           " inner join " + DealProperty.Schema.Provider.DelimitDbName(DealProperty.Schema.TableName) + " dp  with(nolock)  on d." + ViewPponDealTimeSlot.Columns.BusinessHourGuid + "=dp." + DealProperty.Columns.BusinessHourGuid +
                           " where " + ViewPponDealTimeSlot.Columns.CityId + "=@cityid and " + DealProperty.Columns.ShipType + " = " + (int)DealShipType.Ship72Hrs + " and @effectdate between effective_start and effective_end" +
                           " and " + ViewPponDealTimeSlot.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.ComboDealSub + "=0 " + " and " + ViewPponDealTimeSlot.Columns.Status + "=0 ";
            if (!string.IsNullOrEmpty(notContainBid))
                qc.CommandSql += " and d." + ViewPponDealTimeSlot.Columns.BusinessHourGuid + " not in (" + notContainBid + ")";

            qc.CommandSql += " order by " + ViewPponDealTimeSlot.Columns.Sequence;
            qc.AddParameter("@cityid", city_id, DbType.Int32);
            qc.AddParameter("@effectdate", effectdate, DbType.DateTime);
            ViewPponDealTimeSlotCollection rtnCol = new ViewPponDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            return rtnCol;
        }

        public List<int> CategoryDealTimeSlotGetCategoryIdList()
        {
            string sql = "select * from " + CategoryDealTimeSlot.Schema.Provider.DelimitDbName(CategoryDealTimeSlot.Schema.TableName) + " with(nolock) ";
            QueryCommand qc = new QueryCommand(sql, "");
            CategoryDealTimeSlotCollection rtnCol = new CategoryDealTimeSlotCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));

            return rtnCol.Select(c => c.CategoryId).ToList<int>();
        }
        #endregion ViewDealTimeSlot

        #region Evaluate 檔次評價
        /*
         *  企劃 http://prototype.17life.com/wireframe/201605_%E8%A9%95%E5%83%B9%E9%A1%AF%E7%A4%BA%E5%89%8D%E5%8F%B0/#p=企劃需求
         *  目前只有憑證(不含宅配、全家、公益、新光)可進行評價
         *  目前前台所顯示評價並非以檔次為主角所做的評價，而是以分店為主角的評分，相關詳細規則請參照企劃需求
         */

        /// <summary>
        /// 分店評價資料(已停用)
        /// </summary>
        public void ResetVerifiedStoreEvaluateDetail()
        {
            string sql = string.Format(@"
                    --truncate table verified_store_evaluate_detail
                    --insert into verified_store_evaluate_detail
                    select verified_store_guid, evaluate_main_id, star, GETDATE() from view_verified_store_evaluate_detail");

            QueryCommand qc = new QueryCommand(sql, "");
            DataService.ExecuteScalar(qc);
        }

        public bool HaveVerifiedStoreEvaluateBidByModifyDate(DateTime modifyDate)
        {
            var sql = @"select count(1) cnt from " + VerifiedStoreEvaluateBid.Schema.Provider.DelimitDbName(VerifiedStoreEvaluateBid.Schema.TableName) + @" with(nolock)
                    where " + VerifiedStoreEvaluateBid.Columns.ModifyDate + " = @modifyDate";
            var qc = new QueryCommand(sql, VerifiedStoreEvaluateBid.Schema.Provider.Name);
            qc.AddParameter("@modifyDate", modifyDate, DbType.DateTime);
            return (int)DataService.ExecuteScalar(qc) > 0;
        }

        public int SetVerifiedStoreEvaluateBid(DateTime modifyDate)
        {
            var workdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            var startDate = workdate.AddDays(-1);
            var endDate = workdate.AddDays(1);

            string sql = @"                                        
                    insert into verified_store_evaluate_bid
                    select business_hour_GUID, store_guid, @startDate, @endDate, @modifyDate 
                    from fn_verified_store_evaluate_bid (@startDate, @endDate)
                    select @@ROWCOUNT
                    ";

            QueryCommand qc = new QueryCommand(sql, "");
            qc.Parameters.Add("@startDate", startDate, DbType.DateTime);
            qc.Parameters.Add("@endDate", endDate, DbType.DateTime);
            qc.Parameters.Add("@modifyDate", modifyDate, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        public void DeleteVerifiedStoreEvaluateBidByModifyDate(DateTime modifyDate)
        {
            DB.Delete().From(VerifiedStoreEvaluateBid.Schema.TableName)
                .Where(VerifiedStoreEvaluateBid.Columns.ModifyDate).IsEqualTo(modifyDate)
                .Execute();
        }

        /// <summary>
        /// 檔次評價資料(依目前分店加總平均)
        /// </summary>
        /// <returns></returns>
        public ViewPponDealEvaluateStarCollection ViewPponDealEvaluateStarCollectionGet()
        {
            return DB.SelectAllColumnsFrom<ViewPponDealEvaluateStar>().NoLock()
                .ExecuteAsCollection<ViewPponDealEvaluateStarCollection>();
        }

        public int SetPponDealEvaluateStar()
        {
            int batchNum = int.Parse(DateTime.Now.ToString("yyyyMMddHH"));

            string sql = @"                                        
                    INSERT INTO ppon_deal_evaluate_star
                    select s.bid, s.cnt, s.star, @batchNum
                    from view_ppon_deal_evaluate_star s
                    select @@ROWCOUNT
                    ";

            QueryCommand qc = new QueryCommand(sql, PponDealEvaluateStar.Schema.Provider.Name);
            qc.Parameters.Add("@batchNum", batchNum, DbType.Int32);

            int rowcount = (int)DataService.ExecuteScalar(qc);

            //刪除2小時前資料
            int deleteTime = int.Parse(DateTime.Now.AddHours(-2).ToString("yyyyMMddHH"));
            DB.Delete().From(PponDealEvaluateStar.Schema.TableName)
                .Where(PponDealEvaluateStar.Columns.BatchNum).IsLessThanOrEqualTo(deleteTime)
                .Execute();

            return rowcount;
        }

        public PponDealEvaluateStarCollection PponDealEvaluateStarCollectionGet()
        {
            int maxBatchNum = DB.Select(Aggregate.Max(PponDealEvaluateStar.BatchNumColumn))
                .From(PponDealEvaluateStar.Schema.TableName)
                .ExecuteScalar<int>();

            return DB.SelectAllColumnsFrom<PponDealEvaluateStar>().NoLock()
                .Where(PponDealEvaluateStar.Columns.BatchNum).IsEqualTo(maxBatchNum)
                .ExecuteAsCollection<PponDealEvaluateStarCollection>();
        }


        #endregion

        #region SmsLog

        public void SMSLogSet(SmsLog sms)
        {
            DB.Save<SmsLog>(sms);
        }

        public int SMSLogGetCountByPpon(string userid, string couponId)
        {
            return SMSLogGetCountByPpon(userid, couponId, SmsType.Ppon);
        }

        public int SMSLogGetCountByPpon(string userid, string couponId, SmsType type)
        {
            string strSQL = "select count(1) from sms_log with(nolock) where create_id=@user_id and [status]=0 and " + SmsLog.Columns.CouponId + " = @cpid and " + SmsLog.Columns.Type + " = @type ";

            int log = new InlineQuery(DataService.Provider).ExecuteScalar<int>(strSQL, userid, couponId, (int)type);

            return log;
        }

        public int SMSLogGetcountByCouponIdList(List<int> couponIdList)
        {
            string idList = string.Join(",", couponIdList);
            const string strSQL = " select  isnull(min(send_count),0) from (select coupon_id , count(1) send_count from sms_log with(nolock) where coupon_id in (@idList) group by coupon_id)t ";
            int log = new InlineQuery(DataService.Provider).ExecuteScalar<int>(strSQL, idList);
            return log;
        }

        public SmsLogCollection SMSLogGetQueue()
        {
            return DB.SelectAllColumnsFrom<SmsLog>().Top("200").Where(SmsLog.Columns.Status).IsEqualTo(SmsStatus.Queue).OrderAsc(SmsLog.Columns.CreateTime).ExecuteAsCollection<SmsLogCollection>();
        }

        public void SmsLogSetToQueue(Guid bid)
        {
            string sql = string.Format("update {0} set [status]={1} where business_hour_guid=@businessHourGuid and [status]={2}",
                SmsLog.Schema.TableName, (int)SmsStatus.Queue, (int)SmsStatus.Ppon);
            QueryCommand qc = new QueryCommand(sql, SmsLog.Schema.Provider.Name);
            qc.AddParameter("@businessHourGuid", bid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public bool SmsLogSetStatus(string smsOrderId, string phoneNumber, SmsStatus status, SmsFailReason reason, DateTime resultTime)
        {
            string sql = string.Format(@"update {0} set [status]=@status,reason=@rason, result_time=@resultTime 
                     where sms_order_id=@smsOrderId and mobile=@mobile",
                SmsLog.Schema.TableName);
            QueryCommand qc = new QueryCommand(sql, SmsLog.Schema.Provider.Name);
            qc.AddParameter("@status", (int)status, DbType.Int32);
            qc.AddParameter("@resultTime", resultTime, DbType.DateTime);
            qc.AddParameter("@smsOrderId", smsOrderId, DbType.String);
            qc.AddParameter("@mobile", phoneNumber, DbType.String);
            return DB.Execute(qc) > 0;
        }

        public SmsLog SmsLogGet(string smsOrderId, string phoneNumber)
        {
            return DB.QueryOver<SmsLog>().FirstOrDefault(t => t.SmsOrderId == smsOrderId && t.Mobile == phoneNumber);
        }

        public List<SmsLog> SmsLogGetListByTaskId(Guid taskId)
        {
            return DB.QueryOver<SmsLog>().Where(t => t.TaskId == taskId).ToList();
        }

        #endregion SmsLog

        #region FETSmsLog

        public void FetSmsLogSet(FetSmsLog fsl)
        {
            DB.Save<FetSmsLog>(fsl);
        }

        #endregion FETSmsLog

        #region S2tSmsLog

        public void S2tSmsLogSet(S2tSmsLog stl)
        {
            DB.Save<S2tSmsLog>(stl);
        }

        #endregion S2tSmsLog

        #region HinetSmsLog

        public void HinetSmsLogSet(HinetSmsLog hsl)
        {
            DB.Save<HinetSmsLog>(hsl);
        }

        #endregion HinetSmsLog

        #region SMSContent

        public void SMSContentSet(SmsContent sms)
        {
            DB.Save<SmsContent>(sms);
        }

        public SmsContent SMSContentGet(Guid bid)
        {
            return DB.Get<SmsContent>(bid);
        }

        #endregion SMSContent

        #region City

        public CityCollection CityGetListByTop(string code)
        {
            string sql = "select * from city with(nolock) where parent_id is null and code=@code";
            QueryCommand qc = new QueryCommand(sql, City.Schema.Provider.Name);
            qc.Parameters.Add("@code", code);
            CityCollection rtnCol = new CityCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));
            if (rtnCol.Count == 0)
            {
                return null;
            }
            return rtnCol;
        }

        public int CityGetTheCityId(string city)
        {
            string sql = "select * from city with(nolock) where parent_id is null and code=@code";
            QueryCommand qc = new QueryCommand(sql, City.Schema.Provider.Name);
            qc.Parameters.Add("@code", city);

            CityCollection rtnCol = new CityCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));

            return (rtnCol.Count == 0) ? -1 : rtnCol[0].Id;
        }

        public string CityGetTheCityCode(int cityId)
        {
            string sql =
                "SELECT code FROM city with(nolock) WHERE id = @CityId";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("CityId", cityId, DbType.Int16);

            City theCity = new City();
            theCity.LoadAndCloseReader(DataService.GetReader(qc));
            return (theCity.Code != null) ? theCity.Code : "";
        }

        public List<int> CityGetDealTimeSlotListByDate(DateTime theDate)
        {
            List<int> cityIdList = new List<int>();
            string sql =
                "SELECT DISTINCT city_id FROM deal_time_slot with(nolock) WHERE @TheDate BETWEEN effective_start AND effective_end AND status = 0";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("TheDate", theDate.AddDays(1).ToString("yyyy/MM/dd HH:mm:ss"), DbType.DateTime);

            DealTimeSlotCollection dts = new DealTimeSlotCollection();
            dts.LoadAndCloseReader(DataService.GetReader(qc));
            if (dts.Count > 0)
                cityIdList = dts.Select(x => x.CityId).ToList<int>();
            return cityIdList;
        }

        #endregion City

        #region CityGroup

        public CityGroupCollection CityGroupGetByCityId(int cityId, CityGroupType type)
        {
            return DB.SelectAllColumnsFrom<CityGroup>().Where(CityGroup.Columns.CityId).IsEqualTo(cityId).And(CityGroup.Columns.Type).IsEqualTo((int)type).ExecuteAsCollection<CityGroupCollection>();
        }

        public CityGroupCollection CityGroupGetListByType(CityGroupType type)
        {
            return DB.SelectAllColumnsFrom<CityGroup>().Where(CityGroup.Columns.Type).IsEqualTo((int)type).ExecuteAsCollection<CityGroupCollection>();
        }

        public CityGroupCollection CityGroupGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CityGroup, CityGroupCollection>(pageStart, pageLength, orderBy, filter);
        }

        #endregion CityGroup

        #region DealAccounting

        public void DealAccountingSet(DealAccounting dAccounting)
        {
            DB.Save<DealAccounting>(dAccounting);
        }

        public DealAccounting DealAccountingGet(Guid businessHourGuid)
        {
            return DB.Get<DealAccounting>(businessHourGuid);
        }

        public DealAccountingCollection DealAccountingGet(IEnumerable<Guid> businessHourGuids)
        {
            var infos = new DealAccountingCollection();
            var tempIds = businessHourGuids.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<DealAccounting>()
                        .Where(DealAccounting.Columns.BusinessHourGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<DealAccountingCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public bool DealAccountingSetFlag(Guid businessHourGuid, AccountingFlag flag)
        {
            DealAccounting da = DealAccountingGet(businessHourGuid);
            da.Flag = da.Flag | (int)flag;
            DealAccountingSet(da);

            return true;
        }

        #endregion DealAccounting

        #region DealSalesInfo & DAILY

        public void DealSalesInfoSave(DealSalesInfo dsi)
        {
            DB.Save<DealSalesInfo>(dsi);
            //DB.Update<DealSalesInfo>().Set(DealSalesInfo.IsMoreThirtyFailColumn).EqualTo(dsi.IsMoreThirtyFail)
            //    .Where(DealSalesInfo.BusinessHourGuidColumn).IsEqualTo(dsi.BusinessHourGuid).Execute();
        }

        public DealSalesInfoCollection DealSalesInfoGet(IEnumerable<Guid> BusinessHourGuids)
        {
            return DB.SelectAllColumnsFrom<DealSalesInfo>()
                    .Where(DealSalesInfo.BusinessHourGuidColumn)
                    .In(BusinessHourGuids)
                    .ExecuteAsCollection<DealSalesInfoCollection>() ?? new DealSalesInfoCollection(); ;
        }

        public void DealSalesInfoAddTotalAndQuantity(Guid bid, int quantity, decimal total)
        {
            string sql = string.Format(@"
update {0} 
    set {1}={1}+@addQuantity, 
        {2}={2}+@addTotal,
        {3}={3}+@addQuantity,
        {4}={4}+@addTotal
    where {5}=@bid
IF @@ROWCOUNT=0
    INSERT INTO {0} VALUES (@bid,@addQuantity, @addTotal, @addQuantity, @addTotal)
",
                DealSalesInfo.Schema.TableName,
                DealSalesInfo.Columns.OrderedQuantity,
                DealSalesInfo.Columns.OrderedTotal,
                DealSalesInfo.Columns.OrderedIncludeRefundQuantity,
                DealSalesInfo.Columns.OrderedIncludeRefundTotal,
                DealSalesInfo.Columns.BusinessHourGuid);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@addQuantity", quantity, DbType.Int32);
            qc.AddParameter("@addTotal", total, DbType.Decimal);
            DB.Execute(qc);
        }

        public void DealSalesInfoMinusTotalAndQuantity(Guid bid, int quantity, decimal total)
        {
            string sql = string.Format(@"
update {0} set {1}={1}-@addQuantity, {2}={2}-@addTotal where {3}=@bid
IF @@ROWCOUNT=0
    INSERT INTO {0} VALUES (@bid,@addQuantity, @addTotal, @addQuantity, @addTotal)
",
                DealSalesInfo.Schema.TableName,
                DealSalesInfo.Columns.OrderedQuantity,
                DealSalesInfo.Columns.OrderedTotal,
                DealSalesInfo.Columns.BusinessHourGuid);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@addQuantity", quantity, DbType.Int32);
            qc.AddParameter("@addTotal", total, DbType.Decimal);
            DB.Execute(qc);
        }

        public void DealSalesInfoRefresh(Guid bid)
        {
            string sql = string.Format(@"
            IF OBJECT_ID('tempdb..#temp_dsi') IS NOT NULL DROP TABLE #temp_dsi

            SELECT * INTO #temp_dsi FROM (
	            SELECT ctl.business_hour_guid
	            , COUNT(CASE WHEN ctl.status in (0, 1) THEN 1 END)
	            + COUNT(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN 1 END) AS quantity
	            , SUM(CASE WHEN ctl.status in (0, 1) THEN ctl.amount ELSE 0 END)
	            + SUM(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN ctl.amount ELSE 0 END) AS amount
	            FROM [order] AS o WITH(NOLOCK)
	            INNER JOIN cash_trust_log AS ctl WITH(NOLOCK) ON ctl.order_guid = o.guid 
	            AND ctl.special_status & 16 = 0 
	            INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
	            WHERE o.order_status & 8 > 0
                AND ctl.{1} = @bid
	            GROUP BY business_hour_guid
            ) AS t

            DECLARE @count int;
            SET @count = (Select count(*) from {0} where {1} = @bid);

            IF @count > 0
            BEGIN
	            UPDATE {0}
	            set ordered_quantity = t.quantity, 
	            ordered_total = t.amount
	            from 
	            (	
		            select * from #temp_dsi
	            ) as t
	            where t.{1} = {0}.{1} and deal_sales_info.{1} = @bid
            END
            ELSE
            BEGIN
	            INSERT INTO {0}
	            SELECT business_hour_guid, quantity, amount, quantity AS quantity_ir, amount AS amount_ir FROM #temp_dsi
            END
            ",
                DealSalesInfo.Schema.TableName,
                DealSalesInfo.Columns.BusinessHourGuid);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            DB.Execute(qc);
        }

        public void DealSalesInfoRefreshForSkm(Guid bid)
        {
            string sql = string.Format(@"
            IF OBJECT_ID('tempdb..#temp_dsi') IS NOT NULL DROP TABLE #temp_dsi

            SELECT * INTO #temp_dsi FROM (
	            SELECT ctl.business_hour_guid
	            , COUNT(CASE WHEN ctl.status in (0, 1) THEN 1 END)
	            + COUNT(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN 1 END) AS quantity
	            , SUM(CASE WHEN ctl.status in (0, 1) THEN ctl.amount ELSE 0 END)
	            + SUM(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN ctl.amount ELSE 0 END) AS amount
	            FROM [order] AS o WITH(NOLOCK)
	            INNER JOIN cash_trust_log AS ctl WITH(NOLOCK) ON ctl.order_guid = o.guid 
	            AND ctl.special_status & 16 = 0 
	            INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
	            WHERE o.order_status & 8 > 0
                AND ctl.{1} = @bid
	            GROUP BY business_hour_guid
            ) AS t

            DECLARE @count int;
            DECLARE @tempCnt int;
            SET @count = (Select count(*) from {0} where {1} = @bid);
            SET @tempCnt = (Select count(*) from #temp_dsi where {1} = @bid);

            IF @tempCnt = 0
			BEGIN
				INSERT INTO #temp_dsi SELECT @bid, 0, 0
			END

            IF @count > 0
            BEGIN
	            UPDATE {0}
	            set ordered_quantity = t.quantity, 
	            ordered_total = t.amount
	            from 
	            (	
		            select * from #temp_dsi
	            ) as t
	            where t.{1} = {0}.{1} and deal_sales_info.{1} = @bid
            END
            ELSE
            BEGIN
	            INSERT INTO {0}
	            SELECT business_hour_guid, quantity, amount, quantity AS quantity_ir, amount AS amount_ir FROM #temp_dsi
            END
            ",
                DealSalesInfo.Schema.TableName,
                DealSalesInfo.Columns.BusinessHourGuid);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            DB.Execute(qc);
        }

        public void ExecSpRefreshDealSalesInfoForSkm(Guid bid)
        {
            string sql = @"EXEC sp_RefreshDealSalesInfoForSkm @bid";
            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            DB.Execute(qc);
        }

        /// <summary>
        /// 更新上檔檔次的銷售數字
        /// </summary>
        public void DealSalesInfoRefresh()
        {
            string sql = @"
IF OBJECT_ID('tempdb..#temp_dsi') IS NOT NULL DROP TABLE #temp_dsi

select * into #temp_dsi from (
select vpd.business_hour_guid 
	,vpd.ordered_quantity,
	vpd.ordered_total,      	
	ISNULL(n.quantity,0) as true_quantity,
	ISNULL(n.amount,0) as true_total
from view_ppon_deal vpd with(nolock)

LEFT OUTER JOIN
	dbo.group_order AS j with(nolock) ON j.business_hour_guid = vpd.business_hour_guid
LEFT OUTER JOIN
  (
SELECT o.parent_order_id as order_guid
	, COUNT(CASE WHEN ctl.status in (0, 1) THEN 1 END)
	+ COUNT(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN 1 END) AS quantity
	, SUM(CASE WHEN ctl.status in (0, 1) THEN ctl.amount ELSE 0 END)
	+ SUM(CASE WHEN ctl.special_status & 1 = 1 OR ctl.status = 2 THEN ctl.amount ELSE 0 END) AS amount
	FROM [order] AS o WITH(NOLOCK)
	INNER JOIN cash_trust_log AS ctl WITH(NOLOCK) ON ctl.order_guid = o.guid 
	AND ctl.special_status & 16 = 0 
	INNER JOIN business_hour AS bh WITH(NOLOCK) ON bh.GUID = ctl.business_hour_guid
	WHERE o.order_status & 8 > 0 
	GROUP BY business_hour_guid,o.parent_order_id
) AS n ON n.order_guid = j.order_guid 
where vpd.business_hour_guid in 
	(select distinct business_hour_guid from deal_time_slot dts with(nolock)
	where GETDATE() between dts.effective_start and dts.effective_end)
) as t
where t.true_quantity <> t.ordered_quantity


update deal_sales_info
set 
	ordered_quantity = t.true_quantity,
	ordered_total = t.true_total
from
(
	select * from #temp_dsi
) as t
where t.business_hour_guid = deal_sales_info.business_hour_guid
        ";
            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            DB.Execute(qc);
        }

        /// <summary>
        /// 更新每日上檔營業額
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="total"></param>
        public void DealSalesInfoDailyAddTotal(Guid bid, int total)
        {
            var today = DateTime.Now.ToShortDateString();

            string sql = string.Format(@"
            update {0} 
                set {1}={1}+@addTotal 
                where {2}=@bid and sales_date = @today
            IF @@ROWCOUNT=0
                INSERT INTO {0} VALUES ('{3}', @bid, @addTotal)
            ",
                DealSalesInfoDaily.Schema.TableName,
                DealSalesInfoDaily.Columns.OrderedTotal,
                DealSalesInfoDaily.Columns.BusinessHourGuid,
                today);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@today", today, DbType.DateTime);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@addTotal", total, DbType.Int32);
            DB.Execute(qc);
        }

        /// <summary>
        /// 更新每日上檔營業額及銷量 
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="total"></param>
        /// <param name="quantity"></param>
        public void DealSalesInfoDailyAddTotalAndQuantity(Guid bid, int quantity, int total)
        {
            var today = DateTime.Now.ToShortDateString();

            string sql = string.Format(@"
            update {0} 
                set {1} = {1} + @addTotal, {2} = {2} + @addQuantity
                where {3} = @bid and sales_date = @today
            IF @@ROWCOUNT=0
                INSERT INTO {0} VALUES (@today, @bid, @addTotal, @addQuantity)",
                DealSalesInfoDaily.Schema.TableName,
                DealSalesInfoDaily.Columns.OrderedTotal,
                DealSalesInfoDaily.Columns.Quantity,
                DealSalesInfoDaily.Columns.BusinessHourGuid);

            QueryCommand qc = new QueryCommand(sql, DealSalesInfo.Schema.Provider.Name);
            qc.AddParameter("@today", today, DbType.DateTime);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@addTotal", total, DbType.Int32);
            qc.AddParameter("@addQuantity", quantity, DbType.Int32);
            DB.Execute(qc);
        }

        #endregion

        #region DealCost

        public void DealCostSet(DealCost dCost)
        {
            DB.Save<DealCost>(dCost);
        }

        public void DealCostSetList(DealCostCollection dCostList)
        {
            DB.SaveAll(dCostList);
        }

        public DealCost DealCostGet(int id)
        {
            return DB.Get<DealCost>(id);
        }

        public DealCostCollection DealCostGetList(Guid businessHourGuid)
        {
            return DB.SelectAllColumnsFrom<DealCost>().Where(DealCost.BusinessHourGuidColumn).IsEqualTo(businessHourGuid).ExecuteAsCollection<DealCostCollection>();
        }


        public DealCostCollection DealCostGetList(IEnumerable<Guid> businessHourGuid)
        {
            DealCostCollection infos = DB.SelectAllColumnsFrom<DealCost>()
                    .Where(DealCost.Columns.BusinessHourGuid).In(businessHourGuid)
                    .ExecuteAsCollection<DealCostCollection>();

            return infos;
        }

        public void DealCostDelete(int id)
        {
            DB.Delete<DealCost>(DealCost.IdColumn.ColumnName, id);
        }

        public void DealCostDeleteListByBid(Guid bid)
        {
            DB.Delete<DealCost>(DealCost.Columns.BusinessHourGuid, bid);
        }

        public decimal DealCostGetByQty(Guid bid, int qty)
        {
            string sql = @"select * from deal_cost with(nolock)
where business_hour_guid=@bid
and @qty between lower_cumulative_quantity and cumulative_quantity";

            DealCostCollection dcc = new InlineQuery().ExecuteAsCollection<DealCostCollection>(sql, bid.ToString(), qty);

            if (dcc.Count == 0)
            {
                sql = @"select top 1 * from deal_cost with(nolock)
where business_hour_guid=@bid
order by cumulative_quantity desc";
                dcc = new InlineQuery().ExecuteAsCollection<DealCostCollection>(sql, bid.ToString());
            }

            if (dcc.Count == 0)
            {
                return 0;
            }
            else
            {
                return dcc[0].Cost.Value;
            }
        }

        #endregion DealCost

        #region Faq

        public FaqCollection FaqCollectionGet()
        {
            return DB.SelectAllColumnsFrom<Faq>()
                .Where(Faq.Columns.Status).IsEqualTo(true)
                .ExecuteAsCollection<FaqCollection>();
        }

        public void FaqSort(int id, int sequence)
        {
            var q = new Update(Faq.Schema.TableName, Faq.Schema.Provider.Name);
            q.Set(Faq.SequenceColumn).EqualTo(sequence).Where(Faq.IdColumn).IsEqualTo(id).Execute();
        }

        public void FaqSet(Faq faq)
        {
            DB.Save<Faq>(faq);
        }

        public Faq FaqGet(int id)
        {
            return DB.Get<Faq>(id);
        }

        public void FaqDelete(int id)
        {
            var q = new Update(Faq.Schema.TableName, Faq.Schema.Provider.Name);
            q.Set(Faq.StatusColumn).EqualTo(false).Where(Faq.IdColumn).IsEqualTo(id).Execute();
        }

        public FaqCollection FaqCollectionGetByParentId(int pid)
        {
            return DB.SelectAllColumnsFrom<Faq>()
                .Where(Faq.Columns.Status).IsEqualTo(true)
                .And(Faq.Columns.Pid).IsEqualTo(pid)
                .ExecuteAsCollection<FaqCollection>();
        }

        #endregion Faq

        #region ViewHamiDealProperty

        public ViewHamiDealProperty ViewHamiDealPropertyGet(Guid bid)
        {
            var vhdpc = DB.SelectAllColumnsFrom<ViewHamiDealProperty>().Where(ViewHamiDealProperty.Columns.BusinessHourGuid).
                IsEqualTo(bid).ExecuteAsCollection<ViewHamiDealPropertyCollection>();
            if (vhdpc.Count > 0)
            {
                return vhdpc[0];
            }
            else
            {
                return null;
            }
        }

        #endregion ViewHamiDealProperty

        #region ViewPponStore

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的Deal的ViewPponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        public ViewPponStoreCollection ViewPponStoreGetListByDayWithNoLock(DateTime sDate, DateTime eDate)
        {
            string sql = @"
                    select * from view_ppon_store with(nolock)
                    where business_hour_guid
                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    union
	                    select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
	                    in (
	                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                    where effective_start >= @sDate and effective_start < @eDate
	                    )
	                    and BusinessHourGuid <> MainBusinessHourGuid
                    )
";
            var qc = new QueryCommand(sql, ViewPponStore.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            var vpolCol = new ViewPponStoreCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));

            return vpolCol;
        }

        /// <summary>
        /// 依據BusinessHourGuid與StoreGuid查詢ViewPponStore的資料
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="storeGuid"></param>
        /// <returns></returns>
        public ViewPponStore ViewPponStoreGetByBidAndStoreGuid(Guid bid, Guid storeGuid)
        {
            string sql = "select * from " + ViewPponStore.Schema.Provider.DelimitDbName(ViewPponStore.Schema.TableName) + @" with(nolock)
                         where " + ViewPponStore.Columns.BusinessHourGuid + " = @bid And " + ViewPponStore.Columns.StoreGuid + " = @sid ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@sid", storeGuid, DbType.Guid);

            var col = new ViewPponStoreCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            if (col.Count > 0)
            {
                return col.First();
            }
            return new ViewPponStore();
        }

        /// <summary>
        /// 依據傳入的bid，取得ViewPponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public ViewPponStoreCollection ViewPponStoreGetListByBidWithNoLock(Guid bid)
        {
            string sql = "select * from " + ViewPponStore.Schema.Provider.DelimitDbName(ViewPponStore.Schema.TableName) + @" with(nolock)
                         where " + ViewPponStore.Columns.BusinessHourGuid + " = @bid ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);

            var col = new ViewPponStoreCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            return col;
        }

        /// <summary>
        /// 依據傳入的bid，取得ViewPponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="vbsRight"></param>
        /// <param name="excludeSeller"></param>
        /// <returns></returns>
        public ViewPponStoreCollection ViewPponStoreGetListByBidWithNoLock(Guid bid, VbsRightFlag vbsRight)
        {
            string sql = "select * from " + ViewPponStore.Schema.Provider.DelimitDbName(ViewPponStore.Schema.TableName) + @" with(nolock)
                         where " + ViewPponStore.Columns.BusinessHourGuid + " = @bid and (" + ViewPponStore.Columns.VbsRight + " & @vbsRight) = @vbsRight";

            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                sql += " and (" + ViewPponStore.Columns.VbsRight + " & @hideVerifyShop) = 0";
            }

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@vbsRight", (int)vbsRight, DbType.Int32);

            if (vbsRight == VbsRightFlag.VerifyShop)
            {
                qc.AddParameter("@hideVerifyShop", (int)VbsRightFlag.HideFromVerifyShop, DbType.Int32);
            }

            var col = new ViewPponStoreCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            return col;
        }

        public ViewPponStoreCollection ViewPponStoreGetListByBidListWithNoLock(List<Guid> bidList)
        {

            var data = new ViewPponStoreCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                data.AddRange(DB.SelectAllColumnsFrom<ViewPponStore>()
                .Where(ViewPponStore.Columns.BusinessHourGuid).In(bidList.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewPponStoreCollection>());
                idx += batchLimit;
            } while (idx <= bidList.Count - 1);

            return data;
        }

        public ViewPponStoreCollection ViewPponStoreGetListWithNoLock(params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponStore.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponStore.Schema.TableName + " with(nolock) " + qc.CommandSql;



            ViewPponStoreCollection data = new ViewPponStoreCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }


        public ViewPponStoreCollection ViewPponStoreGetStoreGuidWithSales(Guid storeGuid)
        {
            return DB.Select().From(ViewPponStore.Schema)
                .Where(ViewPponStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .And(ViewPponStore.Columns.OrderedQuantity).IsGreaterThan(0)
                .ExecuteAsCollection<ViewPponStoreCollection>();
        }

        public DataTable ProductDetailGet(IEnumerable<Guid> bids, int range)
        {
            DataTable infos = new DataTable();
            DataTable result = new DataTable();
            List<Guid> tempIds = bids.Distinct().ToList();
            int idx = 0;
            var bidList = tempIds.Select(x => "'" + x.ToString() + "'").ToList();

            string condition = "";
            switch (range)
            {
                case (int)EvaluateShowDataRange.Now:
                    condition = " business_hour_deliver_time_e >= GETDATE()";
                    break;
                case (int)EvaluateShowDataRange.History:
                    condition = " business_hour_deliver_time_e <= GETDATE()";
                    break;
                default:
                    condition = " 1=1 ";
                    break;
            }

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;

                string sql = string.Format(@"
                SELECT dp.unique_id,cec.name AS event_name,i.item_name,s.seller_name AS store_name,
                       bh.GUID,bh.business_hour_deliver_time_s,coalesce(bh.changed_expire_date, bh.business_hour_deliver_time_e) as business_hour_deliver_time_e,
                       s.guid AS sid
                FROM   business_hour bh JOIN deal_property dp ON bh.guid = dp.business_hour_guid
                       JOIN ppon_store ps ON dp.business_hour_guid = ps.business_hour_guid
                       JOIN seller s ON ps.store_guid = s.Guid
                       JOIN item i ON dp.business_hour_guid = i.business_hour_guid
                       JOIN coupon_event_content cec ON cec.business_hour_guid = bh.GUID
                WHERE  dp.business_hour_guid IN ({0}) AND {1} "
               , string.Join(",", bidList.Skip(idx).Take(batchLimit)), condition
               );

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                using (IDataReader reader = DataService.GetReader(qc))
                {
                    infos.Load(reader);
                }

                result.Merge(infos);
                idx += batchLimit;
            }
            return result;
        }

        #endregion ViewPponStore

        #region ViewOrderStore

        /// <summary>
        /// 依據傳入的日期區間，取得排程資料(DealTimeSolt)中，起始日期位於此區間中的Deal的PponStore資料。
        /// 使用NoLock關鍵字，所以資料也許不是最新的。
        /// </summary>
        /// <param name="sDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        public ViewOrderStoreQuantityCollection ViewOrderStoreQuantityGetListByDayWithNoLock(DateTime sDate, DateTime eDate)
        {
            string sql = "select * from " + ViewOrderStoreQuantity.Schema.Provider.DelimitDbName(ViewOrderStoreQuantity.Schema.TableName) + @" with(nolock)
                         where business_hour_guid
in (
select distinct " + DealTimeSlot.Columns.BusinessHourGuid + " from " + DealTimeSlot.Schema.Provider.DelimitDbName(DealTimeSlot.Schema.TableName) + @" with(nolock)
where effective_start >= @sDate and effective_start < @eDate
) ";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@sDate", sDate, DbType.DateTime);
            qc.AddParameter("@eDate", eDate, DbType.DateTime);

            var col = new ViewOrderStoreQuantityCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            return col;
        }

        public ViewOrderStoreQuantityTodayCollection ViewOrderStoreQuantityGetListByToday()
        {
            string sql = @"select * from " +
                ViewOrderStoreQuantityToday.Schema.Provider.DelimitDbName(ViewOrderStoreQuantityToday.Schema.TableName) + @" with(nolock)";

            var qc = new QueryCommand(sql, "");
            var col = new ViewOrderStoreQuantityTodayCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        #endregion ViewOrderStore


        #region ProposalStore
        public ProposalStoreCollection ProposalStoreGetListByProposalId(int pid)
        {
            return
                DB.SelectAllColumnsFrom<ProposalStore>().Where(ProposalStore.Columns.ProposalId).IsEqualTo(pid).
                    ExecuteAsCollection<ProposalStoreCollection>();
        }

        public ProposalStore ProposalStoreGet(int proposalId, Guid storeGuid)
        {
            return
                DB.SelectAllColumnsFrom<ProposalStore>()
                .Where(ProposalStore.Columns.ProposalId).IsEqualTo(proposalId)
                .And(ProposalStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                    .ExecuteSingle<ProposalStore>();
        }

        public bool ProposalStoreSet(ProposalStore store)
        {
            DB.Save(store);
            return true;
        }

        public bool ProposalStoreSetList(ProposalStoreCollection stores)
        {
            DB.SaveAll(stores);
            return true;
        }

        public bool ProposalStoreDeleteList(ProposalStoreCollection stores)
        {
            var queries = new List<SqlQuery>();
            foreach (var store in stores)
            {
                queries.Add(
                    new Delete().From(ProposalStore.Schema).Where(ProposalStore.ProposalIdColumn).IsEqualTo(
                        store.ProposalId).And(ProposalStore.StoreGuidColumn).IsEqualTo(store.StoreGuid));
            }
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }

        public bool ProposalStoreDelete(int pid)
        {
            DB.Destroy<ProposalStore>(ProposalStore.Columns.ProposalId, pid);
            return true;
        }
        #endregion

        #region PponStore

        public PponStore PponStoreGet(Guid bid, Guid storeGuid)
        {
            return
                DB.SelectAllColumnsFrom<PponStore>().NoLock()
                .Where(PponStore.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(PponStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteSingle<PponStore>();
        }

        public PponStore PponStoreGet(Guid bid, Guid storeGuid, VbsRightFlag vbsRight)
        {
            var psCol = DB.SelectAllColumnsFrom<PponStore>().Where(PponStore.Columns.BusinessHourGuid).IsEqualTo(bid)
                .And(PponStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<PponStoreCollection>();
            return psCol.FirstOrDefault(x => Helper.IsFlagSet(x.VbsRight, VbsRightFlag.VerifyShop) == true) ?? new PponStore();
        }

        public PponStoreCollection PponStoreGetListByBusinessHourGuid(Guid bid)
        {
            return
                DB.SelectAllColumnsFrom<PponStore>().Where(PponStore.Columns.BusinessHourGuid).IsEqualTo(bid).
                    ExecuteAsCollection<PponStoreCollection>();
        }

        public PponStoreCollection PponStoreGetListByResourceGuids(IEnumerable<Guid> resourceGuids)
        {
            return
                DB.SelectAllColumnsFrom<PponStore>().Where(PponStore.Columns.ResourceGuid).In(resourceGuids).
                    ExecuteAsCollection<PponStoreCollection>();
        }

        public bool PponStoreSet(PponStore store)
        {
            DB.Save(store);
            return true;
        }

        public void PponStoreUpdateOrderQuantity(Guid bid, Guid storeGuid, int orderQuantity, bool isResetOrderQuantity = false)
        {
            string sql;

            if (isResetOrderQuantity)
            {
                sql = string.Format(
                @"UPDATE {0}
                SET {1} = @orderQuantity
                WHERE {2} = @bid AND {3} =@storeGuid",
                PponStore.Schema.TableName,
                PponStore.Columns.OrderedQuantity,
                PponStore.Columns.BusinessHourGuid,
                PponStore.Columns.StoreGuid);
            }
            else
            {
                sql = string.Format(
                @"UPDATE {0}
                SET {1} = COALESCE( {1}, 0 ) + @orderQuantity
                WHERE {2} = @bid AND {3} =@storeGuid",
                PponStore.Schema.TableName,
                PponStore.Columns.OrderedQuantity,
                PponStore.Columns.BusinessHourGuid,
                PponStore.Columns.StoreGuid);
            }

            QueryCommand qc = new QueryCommand(sql, PponStore.Schema.Provider.Name);
            qc.AddParameter("orderQuantity", orderQuantity, DbType.Int32);
            qc.AddParameter("bid", bid, DbType.Guid);
            qc.AddParameter("storeGuid", storeGuid, DbType.Guid);

            DB.Execute(qc);
        }

        public void PponStoreUpdateOrderQuantity(Guid bid, Guid storeGuid, int orderQuantity, int vbsRight)
        {
            string sql = string.Format(
@"UPDATE {0}
SET {1} = COALESCE( {1}, 0 ) + @orderQuantity
WHERE {2} = @bid
AND {3} =@storeGuid
AND {4} =@vbsRight",
                PponStore.Schema.TableName,
                PponStore.Columns.OrderedQuantity,
                PponStore.Columns.BusinessHourGuid,
                PponStore.Columns.StoreGuid,
                PponStore.Columns.VbsRight
            );
            QueryCommand qc = new QueryCommand(sql, PponStore.Schema.Provider.Name);
            qc.AddParameter("orderQuantity", orderQuantity, DbType.Int32);
            qc.AddParameter("bid", bid, DbType.Guid);
            qc.AddParameter("storeGuid", storeGuid, DbType.Guid);
            qc.AddParameter("vbsRight", vbsRight, DbType.Int32);

            DB.Execute(qc);
        }

        public bool PponStoreSetList(PponStoreCollection stores)
        {
            DB.SaveAll(stores);
            return true;
        }

        public bool PponStoreDeleteList(PponStoreCollection stores)
        {
            var queries = new List<SqlQuery>();
            foreach (var store in stores)
            {
                queries.Add(
                    new Delete().From(PponStore.Schema).Where(PponStore.BusinessHourGuidColumn).IsEqualTo(
                        store.BusinessHourGuid).And(PponStore.StoreGuidColumn).IsEqualTo(store.StoreGuid));
            }
            SqlQuery.ExecuteTransaction(queries);
            return true;
        }

        public void PponStoreDeleteByBid(Guid bid)
        {
            DB.Delete<PponStore>(PponStore.Columns.BusinessHourGuid, bid);
        }
        #endregion PponStore

        #region ViewPponCashTrustLog

        public ViewPponCashTrustLogCollection ViewPponCashTrustLogGetList(Guid bid)
        {
            ViewPponCashTrustLogCollection cashTrustLogs =
                new Select().From<ViewPponCashTrustLog>().Where(ViewPponCashTrustLog.Columns.Bid).IsEqualTo(
                    bid).ExecuteAsCollection<ViewPponCashTrustLogCollection>();
            return cashTrustLogs ?? new ViewPponCashTrustLogCollection();
        }

        public ViewPponCashTrustLogCollection ViewPponCashTrustLogGetListByOrderGuid(Guid oid)
        {
            ViewPponCashTrustLogCollection cashTrustLogs =
                new Select().From<ViewPponCashTrustLog>().Where(ViewPponCashTrustLog.Columns.Oid).IsEqualTo(
                    oid).ExecuteAsCollection<ViewPponCashTrustLogCollection>();
            return cashTrustLogs ?? new ViewPponCashTrustLogCollection();
        }

        public ViewPponCashTrustLogCollection ViewPponCashTrustLogGetByCode(string codeNumber)
        {
            ViewPponCashTrustLogCollection cashTrustLogs =
                new Select().From<ViewPponCashTrustLog>().Where(ViewPponCashTrustLog.Columns.CouponCode).
                    IsEqualTo(codeNumber).ExecuteAsCollection<ViewPponCashTrustLogCollection>();
            return cashTrustLogs;
        }

        public ViewPponCashTrustLogCollection ViewPponCashTrustLogGet(string couponSequenceNumber)
        {
            ViewPponCashTrustLogCollection cashTrustLogs =
                new Select().From<ViewPponCashTrustLog>().Where(ViewPponCashTrustLog.Columns.CouponSequenceNumber).
                    IsEqualTo(couponSequenceNumber).ExecuteAsCollection<ViewPponCashTrustLogCollection>();
            return cashTrustLogs;
        }

        public ViewPponCashTrustLogCollection ViewPponCashTrustLogGetTmall(string couponSequenceNumber)
        {
            string strSQL = " select * from " + ViewPponCashTrustLog.Schema.TableName +
                          " with(nolock) where " + ViewPponCashTrustLog.Columns.CouponSequenceNumber + " = @couponSequenceNumber and "
                          + ViewPponCashTrustLog.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.TmallDeal + ">0";

            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("couponSequenceNumber", couponSequenceNumber, DbType.String);

            ViewPponCashTrustLogCollection cashTrustLogs = new ViewPponCashTrustLogCollection();
            cashTrustLogs.LoadAndCloseReader(DataService.GetReader(qc));

            return cashTrustLogs;
        }

        public ViewPponCashTrustLog ViewPponCashTrustLogGet(Guid trustId)
        {
            string strSQL = " select * from " + ViewPponCashTrustLog.Schema.TableName +
                            " with(nolock) where " + ViewPponCashTrustLog.Columns.TrustId + " = @trustId ";

            QueryCommand qc = new QueryCommand(strSQL, ViewPponCashTrustLog.Schema.Provider.Name);
            qc.AddParameter("trustId", trustId, DbType.Guid);

            ViewPponCashTrustLogCollection cashTrustLogs = new ViewPponCashTrustLogCollection();
            cashTrustLogs.LoadAndCloseReader(DataService.GetReader(qc));
            ViewPponCashTrustLog cashTrustLog = cashTrustLogs.FirstOrDefault();

            return cashTrustLog;
        }

        public List<OrderDiscount> OrderDiscountListGetByReferenceIdAndTime(string srcId, DateTime starttime, DateTime endtime)
        {
            string sql = @" select o.order_id, SUM(ctl.amount - ctl.discount_amount) after_discount_amount
                            from [order] o with(nolock)
                                join cash_trust_log ctl with(nolock) on o.guid = ctl.order_guid
                                join cpa_order ra with(nolock) on o.guid = ra.order_guid
                            where ra.first_rsrc = @srcId
                                and o.create_time >= @starttime
                                and o.create_time < @endtime
                            group by o.order_id";

            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("@srcId", srcId, DbType.String);
            qc.AddParameter("@starttime", starttime, DbType.DateTime);
            qc.AddParameter("@endtime", endtime, DbType.DateTime);

            DataTable dt = new DataTable();
            dt.Load(DataService.GetReader(qc));

            return dt.Select().Select(row =>
                new OrderDiscount
                {
                    OrderId = row["order_id"].ToString(),
                    AfterDiscountAmount = (int)row["after_discount_amount"]
                }).ToList();
        }

        #endregion ViewPponCashTrustLog

        #region VerificationStatisticsLog

        public int VerificationStatisticsLogInsert(VerificationStatisticsLog vslog, Guid bid, string createId)
        {
            vslog.BusinessHourGuid = bid;
            vslog.CreateId = createId;
            vslog.ModifyId = createId;
            vslog.CreateTime = DateTime.Now;
            vslog.ModifyTime = DateTime.Now;
            int id = DB.Save(vslog);
            return id;
        }

        public VerificationStatisticsLog VerificationStatisticsLogGetById(int id)
        {
            return DB.Get<VerificationStatisticsLog>(id);
        }

        public VerificationStatisticsLog VerificationStatisticsLogGetRecordByGuid(Guid bid, VerificationStatisticsLogStatus status)
        {
            string sql = string.Format(@"SELECT *
                                        FROM [verification_statistics_log]
                                        WHERE [business_hour_guid] = @business_hour_guid
                                            AND [status]  = [status] | {0}
                                        ORDER BY id ASC", ((int)status));
            VerificationStatisticsLogCollection collection =
                new InlineQuery().ExecuteAsCollection<VerificationStatisticsLogCollection>(sql, bid.ToString());
            if (collection.Count > 0)
            {
                VerificationStatisticsLog lockedRecord = collection[0];
                return lockedRecord;
            }

            return null;
        }

        /// 商家後臺_對帳查詢:根據輸入之對帳單編號抓取對帳單對應之累計核銷統計資料
        /// <param name="balanceSheetId"></param>
        /// <returns></returns>
        public VerificationStatisticsLog VerificationStatisticsLogGetListByBalanceSheetId(int balanceSheetId)
        {
            VerificationStatisticsLog vsLog = DB.SelectAllColumnsFrom<VerificationStatisticsLog>()
                .Where(VerificationStatisticsLog.Columns.Id)
                .In(new Select(BalanceSheet.Columns.VerificationStatisticsLogId).From<BalanceSheet>()
                        .Where(BalanceSheet.Columns.Id).IsEqualTo(balanceSheetId))
                .ExecuteSingle<VerificationStatisticsLog>();
            return vsLog ?? new VerificationStatisticsLog();
        }

        public bool VerificationStatisticsLogDelete(int vslId)
        {
            DB.Destroy<VerificationStatisticsLog>(VerificationStatisticsLog.IdColumn.ColumnName, vslId);
            return true;
        }

        #endregion VerificationStatisticsLog

        #region ComboDeals
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bid"></param>
        /// <param name="ismain">false 只撈子檔</param>
        /// <param name="theType"></param>
        /// <returns></returns>
        public ComboDealCollection GetComboDealByBid(Guid bid, bool ismain = true)
        {
            string sql = "select * from " + ComboDeal.Schema.TableName + " with(nolock) where " +
                ComboDeal.Columns.MainBusinessHourGuid + "=@bid " +
                " and " + ComboDeal.Columns.BusinessHourGuid + (ismain ? "=" : "<>") + ComboDeal.Columns.MainBusinessHourGuid + " order by " + ComboDeal.Columns.Sequence;
            var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            var data = new ComboDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ComboDealCollection GetComboDealByBid(IEnumerable<Guid> bids)
        {
            var infos = new ComboDealCollection();
            List<Guid> tempGuids = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempGuids.Count - 1)
            {
                int batchLimit = 2000;

                string sql = "select * from " + ComboDeal.Schema.TableName + " with(nolock)" +
                             " where " + ComboDeal.Columns.MainBusinessHourGuid +
                             " in ( select " + ComboDeal.Columns.MainBusinessHourGuid + " from " + ComboDeal.Schema.TableName +
                             " where " + ComboDeal.Columns.BusinessHourGuid + string.Format(" in ('{0}') ", string.Join("','", tempGuids.Skip(idx).Take(batchLimit))) + ")" +
                             " order by " + ComboDeal.Columns.Sequence;
                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                var data = new ComboDealCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }

            return infos;
        }

        public ComboDealCollection GetComboDealToday(bool ismain)
        {
            var today = DateTime.Today.SetTime(12, 0, 0);

            var sql = string.Format(@"select * from {0} with(nolock) where {1} in 
                (
                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                where effective_start >= @sdate and effective_start < @edate
                ) and {2} {3} {1}"
            , ComboDeal.Schema.TableName
            , ComboDeal.Columns.MainBusinessHourGuid
            , ComboDeal.Columns.BusinessHourGuid
            , (ismain ? "=" : "<>"));

            var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
            qc.AddParameter("@sdate", today.AddDays(-1), DbType.DateTime);
            qc.AddParameter("@edate", today.AddDays(1), DbType.DateTime);

            var data = new ComboDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewComboDealCollection GetViewComboDealToday()
        {
            var today = DateTime.Today.SetTime(12, 0, 0);

            var sql = string.Format(@"select * from {0} with(nolock) where {1} in 
                (
                    select distinct business_hour_guid from deal_time_slot with(nolock)
	                where effective_start >= @sdate and effective_start < @edate
                ) and {2} <> {1} order by {3}"
            , ViewComboDeal.Schema.TableName
            , ViewComboDeal.Columns.MainBusinessHourGuid
            , ViewComboDeal.Columns.BusinessHourGuid
            , ViewComboDeal.Columns.Sequence);

            var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
            qc.AddParameter("@sdate", today.AddDays(-1), DbType.DateTime);
            qc.AddParameter("@edate", today.AddDays(1), DbType.DateTime);

            var data = new ViewComboDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewComboDealCollection GetViewComboDealByBid(Guid bid, bool ismain = true)
        {
            string sql = "select * from " + ViewComboDeal.Schema.TableName + " with(nolock) where " + ViewComboDeal.Columns.MainBusinessHourGuid + "=@bid " +
                " and " + ViewComboDeal.Columns.BusinessHourGuid + (ismain ? "=" : "<>") + ViewComboDeal.Columns.MainBusinessHourGuid + " order by " + ViewComboDeal.Columns.Sequence;
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            var data = new ViewComboDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewComboDealCollection GetViewComboDealAllByBid(Guid bid)
        {
            string sql = "select * from " + ViewComboDeal.Schema.TableName + " where " + ViewComboDeal.Columns.MainBusinessHourGuid +
                " in (select " + ViewComboDeal.Columns.MainBusinessHourGuid + " from " + ViewComboDeal.Schema.TableName + " where " + ViewComboDeal.Columns.BusinessHourGuid + "=@bid) order by " + ViewComboDeal.Columns.Sequence;
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            var data = new ViewComboDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ComboDeal GetComboDeal(Guid bid)
        {
            return DB.Get<ComboDeal>(ComboDeal.Columns.BusinessHourGuid, bid);
        }

        /// <summary>
        /// 刪代表檔子檔的關聯，且更新BusinessHourStatus狀態
        /// </summary>
        /// <param name="id"></param>
        /// <param name="bid"></param>
        public void DeleteComboDeal(int id, Guid bid)
        {
            DB.Delete<ComboDeal>(ComboDeal.Columns.Id, id);
            string sql2 = "update " + BusinessHour.Schema.TableName + " set " + BusinessHour.Columns.BusinessHourStatus + "= " +
           BusinessHour.Columns.BusinessHourStatus + "^" + (int)BusinessHourStatus.ComboDealSub +
            " where " + BusinessHour.Columns.Guid + "=@subguid and " + BusinessHour.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.ComboDealSub + ">0";
            QueryCommand qc2 = new QueryCommand(sql2, BusinessHour.Schema.Provider.Name);
            qc2.AddParameter("@subguid", bid, DbType.Guid);
            DataService.ExecuteScalar(qc2);
        }

        /// <summary>
        /// 刪代表檔主檔的關聯，且更新BusinessHourStatus狀態
        /// </summary>
        /// <param name="mainbid"></param>
        public void DeleteComboDeal(Guid mainbid)
        {
            DB.Delete<ComboDeal>(ComboDeal.Columns.MainBusinessHourGuid, mainbid);
            string sql = "update " + BusinessHour.Schema.TableName + " set " + BusinessHour.Columns.BusinessHourStatus + "= " +
            BusinessHour.Columns.BusinessHourStatus + "^" + (int)BusinessHourStatus.ComboDealMain +
          " where " + BusinessHour.Columns.Guid + "=@mainguid and " + BusinessHour.Columns.BusinessHourStatus + "&" + (int)BusinessHourStatus.ComboDealMain + ">0";
            QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
            qc.AddParameter("@mainguid", mainbid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        public void AddComboDeal(ComboDeal deal)
        {
            DB.Save<ComboDeal>(deal);
            if (deal.BusinessHourGuid == deal.MainBusinessHourGuid)
            {
                string sql = "update " + BusinessHour.Schema.TableName + " set " + BusinessHour.Columns.BusinessHourStatus + "= " +
                    BusinessHour.Columns.BusinessHourStatus + "|" + (int)BusinessHourStatus.ComboDealMain + " where " + BusinessHour.Columns.Guid + "=@mainguid";
                QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
                qc.AddParameter("@mainguid", deal.MainBusinessHourGuid, DbType.Guid);
                DataService.ExecuteScalar(qc);
            }
            else
            {
                string sql2 = "update " + BusinessHour.Schema.TableName + " set " + BusinessHour.Columns.BusinessHourStatus + "= " +
                BusinessHour.Columns.BusinessHourStatus + "|" + (int)BusinessHourStatus.ComboDealSub + " where " + BusinessHour.Columns.Guid + "=@subguid ";
                QueryCommand qc2 = new QueryCommand(sql2, BusinessHour.Schema.Provider.Name);
                qc2.AddParameter("@subguid", deal.BusinessHourGuid, DbType.Guid);
                DataService.ExecuteScalar(qc2);
            }
        }

        #endregion ComboDeals

        #region EventPromo

        public void SavePromoSeoKeyword(PromoSeoKeyword seoKeyword)
        {
            DB.Save<PromoSeoKeyword>(seoKeyword);
        }

        public void DeletePromoSeoKeyword(PromoSeoKeyword seoKeyword)
        {
            DB.Delete(seoKeyword);
        }

        public PromoSeoKeyword GetPromoSeoKeyword(int promoType, int activityId)
        {
            return DB.SelectAllColumnsFrom<PromoSeoKeyword>()
                .Where(PromoSeoKeyword.Columns.PromoType).IsEqualTo(promoType)
                .And(PromoSeoKeyword.Columns.ActivityId).IsEqualTo(activityId)
                .ExecuteSingle<PromoSeoKeyword>();
        }

        public int SaveEventPromo(EventPromo promo)
        {
            DB.Save<EventPromo>(promo);
            return promo.Id;
        }



        public EventPromo GetEventPromo(int id)
        {
            return DB.Get<EventPromo>(id);
        }



        public EventPromo GetEventPromo(string url, EventPromoType type)
        {
            return DB.SelectAllColumnsFrom<EventPromo>()
                .Where(EventPromo.Columns.Url).IsEqualTo(url).And(EventPromo.Columns.Type).IsEqualTo((int)type)
                .ExecuteSingle<EventPromo>();
        }

        public EventPromo GetEventPromo(string url, EventPromoTemplateType type)
        {
            return DB.SelectAllColumnsFrom<EventPromo>()
                .Where(EventPromo.Columns.Url).IsEqualTo(url).And(EventPromo.Columns.TemplateType).IsEqualTo((int)type)
                .ExecuteSingle<EventPromo>();
        }

        public EventPromoCollection GetEventPromoList(EventPromoType type)
        {
            SqlQuery query =
             DB.SelectAllColumnsFrom<EventPromo>().Where(EventPromo.Columns.Type).IsEqualTo(type);
            var data = query.OrderDesc(EventPromo.Columns.Id)
                      .ExecuteAsCollection<EventPromoCollection>();
            return data;
        }

        public EventPromoCollection EventPromoGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<EventPromo, EventPromoCollection>(0, -1, null, filter);
        }

        public EventPromoCollection GetEventPromoList(EventPromoTemplateType type)
        {
            return DB.SelectAllColumnsFrom<EventPromo>()
                .Where(EventPromo.Columns.TemplateType).IsEqualTo((int)type)
                .And(EventPromo.Columns.Type).IsEqualTo((int)EventPromoType.Ppon).ExecuteAsCollection<EventPromoCollection>();
        }

        /// <summary>
        /// 取得正在進行中的商品主題活動
        /// </summary>
        /// <param name="isBindCategory">是否檢查有綁定類別的商品主題活動</param>
        /// <param name="checkIsShowInWeb">是否檢查顯示於web的商品主題活動</param>
        /// <param name="checkIsShowInApp">是否檢查顯示於app的商品主題活動</param>
        /// <returns></returns>
        public EventPromoCollection EventPromoOnGoingList(bool isBindCategory, bool checkIsShowInWeb, bool checkIsShowInApp)
        {
            string sql = string.Format(@"select * from event_promo ep with(nolock)
                                where GETDATE() between ep.StartDate and DATEADD(dd, 1, EndDate) 
                                AND ep.Status = 1{0}{1}{2}", (isBindCategory) ? " AND ISNULL(ep.bind_category_list, '') <> ''" : string.Empty
                                                           , (checkIsShowInWeb) ? " AND show_in_web=1" : string.Empty
                                                           , (checkIsShowInApp) ? " AND show_in_app=1" : string.Empty);

            QueryCommand qc = new QueryCommand(sql, EventPromo.Schema.Provider.Name);
            EventPromoCollection eventPromoCol = new EventPromoCollection();
            eventPromoCol.LoadAndCloseReader(DataService.GetReader(qc));
            return eventPromoCol;
        }

        public EventPromoCollection EventPromoGetListByDiscountCampaignId(int campaignId)
        {
            string sql = @"select ep.* from dbo.event_promo ep inner join 
	                    dbo.discount_event_promo_link depl On ep.Id = depl.event_promo_id
	                    where depl.campaign_id = @campaignId";

            QueryCommand qc = new QueryCommand(sql, EventPromo.Schema.Provider.Name);
            qc.AddParameter("@campaignId", campaignId, DbType.Int32);
            EventPromoCollection eventPromoCol = new EventPromoCollection();
            eventPromoCol.LoadAndCloseReader(DataService.GetReader(qc));
            return eventPromoCol;
        }

        public void UpdateEventPromoStatus(int id)
        {
            string sql = "update " + EventPromo.Schema.TableName + " set " + EventPromo.Columns.Status + "= " +
          " case " + EventPromo.Columns.Status +
          " when 1  then 0 else 1  end " +
          " where " + EventPromo.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EventPromo.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void UpdateEventPromoItemStatus(int id)
        {
            string sql = "update " + EventPromoItem.Schema.TableName + " set " + EventPromoItem.Columns.Status + "= " +
          " case " + EventPromo.Columns.Status +
          " when 1  then 0 else 1  end " +
          " where " + EventPromoItem.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, EventPromoItem.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public EventPromoCollection GetEventPromoListByDiscountCampaignId(int discountCampaignId)
        {
            string sql = @"select ep.* from event_promo ep inner join discount_event_promo_link dl
                            On ep.Id = dl.event_promo_id
                            Where dl.campaign_id = @campaignId";
            QueryCommand qc = new QueryCommand(sql, EventPromo.Schema.Provider.Name);
            qc.AddParameter("@campaignId", discountCampaignId, DbType.Int32);
            EventPromoCollection eventPromoCol = new EventPromoCollection();
            eventPromoCol.LoadAndCloseReader(DataService.GetReader(qc));
            return eventPromoCol;
        }

        #endregion EventPromo

        #region MerchantEventPromo
        public MerchantEventPromo MerchantEventPromoGet(int id)
        {
            return DB.Get<MerchantEventPromo>(id);
        }
        public MerchantEventPromoCollection MerchantEventPromoGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<MerchantEventPromo>().NoLock()
                .Where(MerchantEventPromo.Columns.Guid).IsEqualTo(guid)
                .ExecuteAsCollection<MerchantEventPromoCollection>();
        }
        public MerchantEventPromoCollection MerchantEventPromoListGet(MerchantEventType type)
        {
            return DB.SelectAllColumnsFrom<MerchantEventPromo>().NoLock()
                .Where(MerchantEventPromo.Columns.Type).IsEqualTo((int)type)
                .ExecuteAsCollection<MerchantEventPromoCollection>();
        }
        public MerchantEventPromoCollection MerchantEventPromoListGetToday(MerchantEventType type)
        {
            DateTime today = DateTime.Now;
            return DB.SelectAllColumnsFrom<MerchantEventPromo>().NoLock()
                .Where(MerchantEventPromo.Columns.Type).IsEqualTo((int)type)
                .And(MerchantEventPromo.Columns.StartDate).IsLessThanOrEqualTo(today)
                .And(MerchantEventPromo.Columns.EndDate).IsGreaterThan(today)
                .ExecuteAsCollection<MerchantEventPromoCollection>();
        }
        public MerchantEventPromoCollection MerchantEventPromoListGet(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = string.Format("{0} {1}", MerchantEventPromo.Columns.Sort, "desc");
            QueryCommand qc = SSHelper.GetWhereQC<MerchantEventPromo>(filter);
            qc.CommandSql = "select * from " + MerchantEventPromo.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            MerchantEventPromoCollection view = new MerchantEventPromoCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }
        public void MerchantEventPromoSet(MerchantEventPromo promo)
        {
            DB.Save<MerchantEventPromo>(promo);
        }
        public void MerchantEventPromoDelete(int id)
        {
            DB.Delete<MerchantEventPromo>(MerchantEventPromo.Columns.Id, id);
        }
        #endregion MerchantEventPromo

        #region ViewEventPromo

        public ViewEventPromoCollection GetViewEventPromoList(string url, bool? status)
        {
            SqlQuery query =
                DB.SelectAllColumnsFrom<ViewEventPromo>().Where(ViewEventPromo.Columns.Url).IsEqualTo(url);
            if (status != null)
            {
                query = query.And(ViewEventPromo.Columns.Status).IsEqualTo(status);
            }
            var data = query.OrderAsc(ViewEventPromo.Columns.Id)
                 .OrderAsc(ViewEventPromo.Columns.EventId, ViewEventPromo.Columns.Seq)
                 .ExecuteAsCollection<ViewEventPromoCollection>();
            return data;
        }

        public ViewEventPromoCollection ViewEventPromoGetList(List<string> urls, bool? status)
        {
            SqlQuery query =
                DB.SelectAllColumnsFrom<ViewEventPromo>().Where(ViewEventPromo.Columns.Url).In(urls);
            if (status != null)
            {
                query = query.And(ViewEventPromo.Columns.Status).IsEqualTo(status);
            }
            var data = query.OrderAsc(ViewEventPromo.Columns.Id)
                 .OrderAsc(ViewEventPromo.Columns.Seq)
                 .ExecuteAsCollection<ViewEventPromoCollection>();
            return data;
        }

        public ViewEventPromoCollection ViewEventPromoGetList(int pageStart, int pageLength, string orderBy,
                                                              params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewEventPromo, ViewEventPromoCollection>(pageStart, pageLength, orderBy, filter);
        }

        public ViewEventPromoCollection ViewEventPromoGetList(int eventId, bool? status = null)
        {
            SqlQuery query =
                DB.SelectAllColumnsFrom<ViewEventPromo>().Where(ViewEventPromo.Columns.EventId).IsEqualTo(eventId).NoLock();
            if (status != null)
            {
                query.And(ViewEventPromo.Columns.Status).IsEqualTo(status.Value);
            }
            return query.ExecuteAsCollection<ViewEventPromoCollection>();
        }

        #endregion ViewEventPromo


        #region ViewEventPromoItem

        public ViewEventPromoItemCollection GetViewEventPromoItemList(int mainid, EventPromoItemType type)
        {
            ViewEventPromoItemCollection items = new Select().From<ViewEventPromoItem>()
                   .Where(ViewEventPromoItem.Columns.MainId).IsEqualTo(mainid)
                   .And(ViewEventPromoItem.Columns.ItemType).IsEqualTo((int)type)
                   .OrderAsc(ViewEventPromoItem.Columns.Seq)
                   .ExecuteAsCollection<ViewEventPromoItemCollection>();
            return items;
        }

        public ViewEventPromoItemCollection GetViewEventPromoItemListByBid(Guid bid)
        {
            ViewEventPromoItemCollection items = new Select().From<ViewEventPromoItem>()
                   .Where(ViewEventPromoItem.Columns.BusinessHourGuid).IsEqualTo(bid)
                   .ExecuteAsCollection<ViewEventPromoItemCollection>();
            return items;
        }

        #endregion ViewEventPromoItem

        #region EventPromoItem
        public void SaveEventPromoItem(EventPromoItem item)
        {
            DB.Save<EventPromoItem>(item);
        }

        public void SaveEventPromoItemList(EventPromoItemCollection items)
        {
            DB.SaveAll<EventPromoItem, EventPromoItemCollection>(items);
        }

        public EventPromoItem GetEventPromoItem(int id)
        {
            return DB.Get<EventPromoItem>(id);
        }

        public EventPromoItem GetEventPromoItem(int mainId, int itemId)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>()
                    .Where(EventPromoItem.Columns.MainId).IsEqualTo(mainId)
                    .And(EventPromoItem.Columns.ItemId).IsEqualTo(itemId)
                    .ExecuteAsCollection<EventPromoItemCollection>().FirstOrDefault();
        }

        public EventPromoItem GetEventPromoItem(int mainId, int itemId, string category, string subCategory)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>()
                .Where(EventPromoItem.Columns.MainId).IsEqualTo(mainId)
                .And(EventPromoItem.Columns.ItemId).IsEqualTo(itemId)
                .And(EventPromoItem.Columns.Category).IsEqualTo(category)
                .And(EventPromoItem.Columns.SubCategory).IsEqualTo(subCategory)
                .ExecuteAsCollection<EventPromoItemCollection>().FirstOrDefault();
        }

        public EventPromoItemCollection GetEventPromoItemList(int mainId)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>()
                .Where(EventPromoItem.Columns.MainId).IsEqualTo(mainId).OrderAsc(EventPromoItem.Columns.Seq).ExecuteAsCollection<EventPromoItemCollection>();
        }

        public EventPromoItemCollection GetEventPromoItemList(int mainId, EventPromoItemType type)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>()
                .Where(EventPromoItem.Columns.MainId).IsEqualTo(mainId)
                .And(EventPromoItem.Columns.ItemType).IsEqualTo((int)type).ExecuteAsCollection<EventPromoItemCollection>();
        }

        public EventPromoItemCollection EventPromoItemGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<EventPromoItem, EventPromoItemCollection>(0, -1, null, filter);
        }

        public EventPromoItemCollection EventPromoItemGetListByMainUrls(List<string> urls, DateTime itemStartDate, DateTime itemEndDate)
        {
            SqlQuery sub = new Select(EventPromo.Columns.Id).From<EventPromo>().Where(EventPromo.Columns.Url).In(urls).And(EventPromo.Columns.Type).IsEqualTo((int)EventPromoType.Ppon);

            return DB.SelectAllColumnsFrom<EventPromoItem>()
                     .Where(EventPromoItem.MainIdColumn)
                     .In(sub)
                     .And(EventPromoItem.ItemStartDateColumn).IsLessThan(itemEndDate)
                     .And(EventPromoItem.ItemEndDateColumn).IsGreaterThan(itemStartDate)
                     .ExecuteAsCollection<EventPromoItemCollection>();
        }

        public int EventPromoItemGetListCount(int eventId, EventPromoItemType itemType)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>().Where(EventPromoItem.Columns.MainId).IsEqualTo(eventId)
                .And(EventPromoItem.Columns.ItemType).IsEqualTo((int)itemType)
                .GetRecordCount();
        }

        public int EventPromoItemGetListCount(int mainId)
        {
            return DB.SelectAllColumnsFrom<EventPromoItem>().Where(EventPromoItem.Columns.MainId).IsEqualTo(mainId).GetRecordCount();
        }

        public void DeleteEventPromoItem(EventPromoItem item)
        {
            DB.Delete(item);

            //所有Seq大於item.Seq的EventPromoItem物件，Seq要跟著-1
            EventPromoItemCollection items = DB.SelectAllColumnsFrom<EventPromoItem>()
                                               .Where(EventPromoItem.Columns.MainId).IsEqualTo(item.MainId)
                                               .And(EventPromoItem.Columns.Seq).IsGreaterThan(item.Seq).ExecuteAsCollection<EventPromoItemCollection>();
            foreach (EventPromoItem row in items)
            {
                row.Seq = row.Seq - 1;
            }
            SaveEventPromoItemList(items);
        }

        #endregion EventPromoItem

        #region ViewEventPromoItemPicVote

        public ViewEventPromoItemPicVoteCollection ViewEventPromoItemPicVoteGet(int mainId)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoItemPicVote>()
                .Where(ViewEventPromoItemPicVote.Columns.MainId)
                .IsEqualTo(mainId)
                .ExecuteAsCollection<ViewEventPromoItemPicVoteCollection>();
        }

        #endregion ViewEventPromoItemPicVote

        #region ViewEventPromoVourcher

        /// <summary>
        /// 以商品主題活動的Url取出參加該活動的優惠券資料
        /// </summary>
        /// <param name="url">活動URL字串(EventPromo.Url)</param>
        /// <param name="status"></param>
        /// <returns></returns>
        public ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(string url, bool? status)
        {
            string sql = "select * from " + ViewEventPromoVourcher.Schema.TableName + " where " +
                ViewEventPromoVourcher.Columns.Url + "=@url " +
                (status == null ? string.Empty : (" and " + ViewEventPromoVourcher.Columns.PromoStatus + "=@status ")) +
               " order by " + ViewEventPromoVourcher.Columns.Id + "," + ViewEventPromoVourcher.Columns.Seq;
            var qc = new QueryCommand(sql, ViewEventPromoVourcher.Schema.Provider.Name);
            qc.AddParameter("@url", url, DbType.String);
            if (status != null)
            {
                qc.AddParameter("@status", status.Value, DbType.Boolean);
            }

            var data = new ViewEventPromoVourcherCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(List<string> urls, bool? status)
        {
            //SqlQuery sub = new Select(EventPromo.Columns.Id).From<EventPromo>().Where(EventPromo.Columns.Url).In(urls);
            SqlQuery query =
                DB.SelectAllColumnsFrom<ViewEventPromoVourcher>().Where(ViewEventPromoVourcher.Columns.Url).In(urls);
            if (status != null)
            {
                query = query.And(ViewEventPromoVourcher.Columns.PromoStatus).IsEqualTo(status);
            }
            var data = query.OrderAsc(ViewEventPromoVourcher.Columns.Id)
                 .OrderAsc(ViewEventPromoVourcher.Columns.Seq)
                 .ExecuteAsCollection<ViewEventPromoVourcherCollection>();
            return data;
        }

        public ViewEventPromoVourcherCollection ViewEventPromoVourcherGetList(int pageStart, int pageLength,
                                                                              string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewEventPromoVourcher, ViewEventPromoVourcherCollection>(pageStart, pageLength, orderBy, filter);
        }

        #endregion ViewEventPromoVourcher

        #region ViewEventPromoCategory

        public ViewEventPromoCategoryCollection ViewEventPromoCategoryGetList(List<int> categoryIds)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoCategory>().Where(ViewEventPromoCategory.Columns.Cid).In(categoryIds)
                .OrderAsc(ViewEventPromoCategory.Columns.Seq).ExecuteAsCollection<ViewEventPromoCategoryCollection>();
        }

        #endregion

        #region ViewEventPromoItemDealId

        public ViewEventPromoItemDealIdCollection ViewEventPromoItemDealIdGetListByEventPromoId(List<int> eventPromoId, string dealKey)
        {
            return DB.SelectAllColumnsFrom<ViewEventPromoItemDealId>().Where(ViewEventPromoItemDealId.Columns.EventPromoId)
            .In(eventPromoId).And(ViewEventPromoItemDealId.Columns.Bid).IsEqualTo(dealKey)
            .ExecuteAsCollection<ViewEventPromoItemDealIdCollection>();
        }

        #endregion

        #region Brand
        public BrandCollection BrandGetList()
        {
            return DB.SelectAllColumnsFrom<Brand>().ExecuteAsCollection<BrandCollection>();
        }

        public BrandCollection BrandGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<Brand, BrandCollection>(0, -1, null, filter);
        }

        public BrandCollection BrandGetByBrandIdList(List<int> brandIdlist)
        {

            return DB.SelectAllColumnsFrom<Brand>()
                .Where(Brand.Columns.Id).In(brandIdlist)
                .ExecuteAsCollection<BrandCollection>();
        }

        public Brand GetBrand(int id)
        {
            return DB.Get<Brand>(id);
        }

        public Brand GetBrand(string url)
        {
            return DB.SelectAllColumnsFrom<Brand>()
                .Where(Brand.Columns.Url).IsEqualTo(url)
                .ExecuteSingle<Brand>();
        }

        public int SaveBrand(Brand brand)
        {
            DB.Save<Brand>(brand);
            return brand.Id;
        }

        public void UpdateBrandStatus(int id)
        {
            string sql = "update " + Brand.Schema.TableName + " set " + Brand.Columns.Status + "= " +
                          " case " + Brand.Columns.Status +
                          " when 1  then 0 else 1  end " +
                          " where " + Brand.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, Brand.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public BrandItem GetBrandItem(int id)
        {
            return DB.Get<BrandItem>(id);
        }

        public BrandItem GetBrandItem(int mainId, int itemId)
        {
            return DB.SelectAllColumnsFrom<BrandItem>()
                    .Where(BrandItem.Columns.MainId).IsEqualTo(mainId)
                    .And(BrandItem.Columns.ItemId).IsEqualTo(itemId)
                    .ExecuteAsCollection<BrandItemCollection>().FirstOrDefault();
        }

        public void SaveBrandItemList(BrandItemCollection items)
        {
            DB.SaveAll<BrandItem, BrandItemCollection>(items);
        }

        public void UpdateBrandItemStatus(int id)
        {
            string sql = "update " + BrandItem.Schema.TableName + " set " + BrandItem.Columns.Status + "= " +
          " case " + BrandItem.Columns.Status +
          " when 1  then 0 else 1  end " +
          " where " + BrandItem.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, BrandItem.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }
        public void UpdateSeqStatus(int id)
        {
            string sql = "update " + BrandItem.Schema.TableName + " set " + BrandItem.Columns.SeqStatus + "= " +
                         " case " + BrandItem.Columns.SeqStatus +
                         " when 1  then 0 else 1  end " +
                         " where " + BrandItem.Columns.Id + "=@id";
            QueryCommand qc = new QueryCommand(sql, BrandItem.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void DeleteBrandItem(BrandItem item)
        {
            DB.Delete(item);

            //所有Seq大於item.Seq的EventPromoItem物件，Seq要跟著-1
            BrandItemCollection items = DB.SelectAllColumnsFrom<BrandItem>()
                                               .Where(BrandItem.Columns.MainId).IsEqualTo(item.MainId)
                                               .And(BrandItem.Columns.Seq).IsGreaterThan(item.Seq).ExecuteAsCollection<BrandItemCollection>();
            foreach (BrandItem row in items)
            {
                row.Seq = row.Seq - 1;
            }
            SaveBrandItemList(items);
        }

        public BrandItemCollection GetBrandItemList(int mainId)
        {
            return DB.SelectAllColumnsFrom<BrandItem>()
                .Where(BrandItem.Columns.MainId).IsEqualTo(mainId).ExecuteAsCollection<BrandItemCollection>();
        }

        public void SaveBrandItemCategory(BrandItemCategory brandItemCategory)
        {
            DB.Save<BrandItemCategory>(brandItemCategory);
        }

        public void SaveBrandItemCategoryList(BrandItemCategoryCollection itemCategorys)
        {
            DB.SaveAll<BrandItemCategory, BrandItemCategoryCollection>(itemCategorys);
        }

        public int BrandItemCategoryGetMaxSequenceByBrandId(int brandId)
        {
            return DB.Select(Aggregate.Max(BrandItemCategory.SeqColumn)).From(BrandItemCategory.Schema.TableName)
                .Where(BrandItemCategory.BrandIdColumn).IsEqualTo(brandId).ExecuteScalar<int>();
        }

        public BrandItemCategoryCollection GetBrandItemCategoryByName(string bicName, int brandId)
        {
            return DB.SelectAllColumnsFrom<BrandItemCategory>()
                .Where(BrandItemCategory.Columns.BrandId).IsEqualTo(brandId).And(BrandItemCategory.Columns.Name).IsEqualTo(bicName).ExecuteAsCollection<BrandItemCategoryCollection>();
        }

        public BrandItemCategoryCollection GetBrandItemCategoryByBrandId(int brandId)
        {
            return DB.SelectAllColumnsFrom<BrandItemCategory>()
                .Where(BrandItemCategory.Columns.BrandId).IsEqualTo(brandId).ExecuteAsCollection<BrandItemCategoryCollection>();
        }

        public void DeleteBrandItemCategoryByBrandId(int brandId)
        {
            DB.Delete<BrandItemCategory>(BrandItemCategory.Columns.BrandId, brandId);
        }

        public void DeleteBrandItemCategoryByCategoryId(int brandId, int categoryId)
        {
            DB.Delete<BrandItemCategory>(BrandItemCategory.Columns.Id, categoryId);
            DB.Delete<BrandItemCategoryDependency>(BrandItemCategoryDependency.Columns.BrandItemCategoryId, categoryId);
        }

        public void DeleteBrandItemCategoryDependency(int brandItemId)
        {
            DB.Delete<BrandItemCategoryDependency>(BrandItemCategoryDependency.Columns.BrandItemId, brandItemId);
        }

        public void DeleteBrandItemCategoryDependencyBybicId(int bicId, int brandItemId)
        {
            string sql = @" DELETE " + BrandItemCategoryDependency.Schema.TableName + @"
                            WHERE " + BrandItemCategoryDependency.Columns.BrandItemId + @" = @brandItemId
                            And " + BrandItemCategoryDependency.Columns.BrandItemCategoryId + @" = @bicId";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@brandItemId", brandItemId, DbType.Int32);
            qc.AddParameter("@bicId", bicId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void SaveBrandItemCategoryDependency(BrandItemCategoryDependency bicd)
        {
            DB.Save<BrandItemCategoryDependency>(bicd);
        }

        public ViewBrandCategoryCollection GetViewBrandCategoryByBrandItemId(int brandItemId)
        {
            return DB.SelectAllColumnsFrom<ViewBrandCategory>()
                .Where(ViewBrandCategory.Columns.BrandItemId).IsEqualTo(brandItemId).ExecuteAsCollection<ViewBrandCategoryCollection>();
        }
        public ViewBrandCategoryCollection GetViewBrandCategoryByBrandId(int brandId)
        {
            return DB.SelectAllColumnsFrom<ViewBrandCategory>()
                .Where(ViewBrandCategory.Columns.BrandId).IsEqualTo(brandId).ExecuteAsCollection<ViewBrandCategoryCollection>();
        }

        public ViewBrandCollection ViewBrandCollectionByBrandId(int brandId)
        {
            return DB.SelectAllColumnsFrom<ViewBrand>().NoLock()
            .Where(ViewBrand.Columns.BrandId).IsEqualTo(brandId)
            .ExecuteAsCollection<ViewBrandCollection>();
        }

        public BrandCollection GetBrandListByDiscountCampaignId(int discountCampaignId)
        {
            string sql = @"select b.* from brand b inner join discount_brand_link dbl
                            On b.Id = dbl.brand_id
                            Where dbl.campaign_id = @campaignId";
            QueryCommand qc = new QueryCommand(sql, Brand.Schema.Provider.Name);
            qc.AddParameter("@campaignId", discountCampaignId, DbType.Int32);
            BrandCollection brandCol = new BrandCollection();
            brandCol.LoadAndCloseReader(DataService.GetReader(qc));
            return brandCol;
        }
        #endregion Brand

        #region 策展列表

        public ThemeCurationMain GetThemeCurationMainById(int id)
        {
            return DB.Get<ThemeCurationMain>(id);
        }
        public ThemeCurationMainCollection GetAllThemeCurationMainCollection()
        {
            return DB.SelectAllColumnsFrom<ThemeCurationMain>().NoLock().ExecuteAsCollection<ThemeCurationMainCollection>();
        }
        public int SaveThemeCurationMain(ThemeCurationMain data)
        {
            return DB.Save(data);
        }
        public ThemeCurationMainCollection GetNowThemeCurationMainCollection()
        {
            return DB.SelectAllColumnsFrom<ThemeCurationMain>().NoLock()
                 .Where(ThemeCurationMain.Columns.StartDate).IsLessThanOrEqualTo(DateTime.Now)
                 .And(ThemeCurationMain.Columns.EndTime).IsGreaterThan(DateTime.Now)
                 .ExecuteAsCollection<ThemeCurationMainCollection>();
        }

        public ThemeCurationGroup GetThemeCurationGroupByGroupId(int groupId)
        {
            return DB.Get<ThemeCurationGroup>(groupId);
        }
        public ThemeCurationGroupCollection GetThemeCurationGroupCollectionByMainId(int mainId)
        {
            return DB.SelectAllColumnsFrom<ThemeCurationGroup>().NoLock()
                .Where(ThemeCurationGroup.Columns.MainId).IsEqualTo(mainId)
                .ExecuteAsCollection<ThemeCurationGroupCollection>();
        }
        public int SaveThemeCurationGroup(ThemeCurationGroup data)
        {
            return DB.Save(data);
        }
        public int SaveAllThemeCurationGroupCollection(ThemeCurationGroupCollection data)
        {
            return DB.SaveAll(data);
        }
        public void DeleteThemeCurationGroupByGroupId(int groupId)
        {
            DB.Delete<ThemeCurationGroup>(ThemeCurationGroup.Columns.Id, groupId);
        }
        public int GetThemeCurationGroupNewSeq(int mainId)
        {
            var sql = @"select max(seq) from theme_curation_group with(nolock) where main_id = @mainId and is_top_group=0"; //非置頂策展
            QueryCommand qc = new QueryCommand(sql, ThemeCurationGroup.Schema.Provider.Name);
            qc.AddParameter("@mainId", mainId, DbType.Int32);
            var result = DataService.ExecuteScalar(qc);

            return (result == System.DBNull.Value) ? 1 : (int)result + 1;
        }

        public ThemeCurationItemCollection GetThemeCurationItemCollectionByGroupId(int groupId)
        {
            return DB.SelectAllColumnsFrom<ThemeCurationItem>().NoLock()
                .Where(ThemeCurationItem.Columns.GroupId).IsEqualTo(groupId)
                .ExecuteAsCollection<ThemeCurationItemCollection>();
        }
        public ThemeCurationItem GetThemeCurationItemByItemId(int itemId)
        {
            return DB.Get<ThemeCurationItem>(itemId);
        }
        public int SaveThemeCurationItem(ThemeCurationItem data)
        {
            return DB.Save(data);
        }
        public int SaveAllThemeCurationItemCollection(ThemeCurationItemCollection data)
        {
            return DB.SaveAll(data);
        }
        public void DeleteThemeCurationItemByItemId(int itemId)
        {
            DB.Delete<ThemeCurationItem>(ThemeCurationItem.Columns.Id, itemId);
        }
        public void DeleteThemeCurationItemByGroupId(int groupId)
        {
            DB.Delete<ThemeCurationItem>(ThemeCurationItem.Columns.GroupId, groupId);
        }

        public ViewThemeCurationCollection GetViewThemeCurationByMainId(int mainId)
        {
            return DB.SelectAllColumnsFrom<ViewThemeCuration>().NoLock()
                .Where(ViewThemeCuration.Columns.MainId).IsEqualTo(mainId)
                .ExecuteAsCollection<ViewThemeCurationCollection>();
        }

        public ViewThemeCurationCollection GetViewThemeCurationByCurationId(int brandId)
        {
            return DB.SelectAllColumnsFrom<ViewThemeCuration>().NoLock()
                .Where(ViewThemeCuration.Columns.CurationId).IsEqualTo(brandId)
                .ExecuteAsCollection<ViewThemeCurationCollection>();
        }
        #endregion

        #region ViewBrandItem
        public ViewBrandItemCollection GetViewBrandItemList(int mainid)
        {
            return DB.SelectAllColumnsFrom<ViewBrandItem>().NoLock()
                .Where(ViewBrandItem.Columns.MainId).IsEqualTo(mainid)
                .ExecuteAsCollection<ViewBrandItemCollection>();
        }
        public ViewBrandItemCollection GetViewBrandItemListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewBrandItem>().NoLock()
                .Where(ViewBrandItem.Columns.BusinessHourGuid).IsEqualTo(bid)
                .OrderDesc(ViewBrandItem.Columns.Id)
                .ExecuteAsCollection<ViewBrandItemCollection>();
        }
        public ViewBrandItemCollection GetViewBrandItemListbySearch(int brandId, string itemName, int categoryId)
        {
            string sql = " select * from " + ViewBrandItem.Schema.TableName + " with(nolock) where main_id =@bid ";

            if (!string.IsNullOrEmpty(itemName))
            {
                sql += " AND " + ViewBrandItem.Columns.ItemName + " LIKE N'%" + itemName + "%'";
            }

            if (categoryId > 0)
            {
                sql += " AND " + ViewBrandItem.Columns.Id + " in (select " + ViewBrandCategory.Columns.BrandItemId + " from " + ViewBrandCategory.Schema.TableName + " where  " + ViewBrandCategory.Columns.BrandItemCategoryId + "=" + categoryId + ")";
            }

            QueryCommand qc = new QueryCommand(sql, ViewBrandItem.Schema.Provider.Name);
            qc.AddParameter("bid", brandId, DbType.Int32);

            ViewBrandItemCollection data = new ViewBrandItemCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        #endregion ViewBrandItem

        #region CategoryDeals
        public CategoryDealCollection CategoryDealsGetList(Guid bid)
        {
            return DB.SelectAllColumnsFrom<CategoryDeal>().NoLock().Where(CategoryDeal.Columns.Bid).IsEqualTo(bid).ExecuteAsCollection<CategoryDealCollection>();
        }

        public Dictionary<Guid, List<int>> CategoryDealsGetListInEffectiveTime()
        {
            string sql = @"
select 
bh.GUID, cd.cid 
from business_hour bh with(nolock)
inner join category_deals cd with(nolock) on cd.bid = bh.GUID
inner join category c with(nolock) on c.id = cd.cid
where getdate() between bh.business_hour_order_time_s and bh.business_hour_order_time_e
and c.type in (-1,4,5,6) and c.status = 1
";
            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            Dictionary<Guid, List<int>> result = new Dictionary<Guid, List<int>>();
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    Guid bid = reader.GetGuid(0);
                    int cid = reader.GetInt32(1);
                    if (result.ContainsKey(bid) == false)
                    {
                        result.Add(bid, new List<int>());
                    }
                    result[bid].Add(cid);
                }
            }
            return result;
        }

        public CategoryCollection CategoryDealsTypeGetList(Guid bid)
        {
            string sql = string.Format(" select c.* from " + CategoryDeal.Schema.TableName + " cd with(nolock) " +
                         " left join " + Category.Schema.TableName + " c with(nolock) " +
                         " on c.id = cd.cid " +
                         " where c.[create_id]<>'sys' " +
                         " and cd.[bid] =@bid" +
                         " and c.[type] in ({0})", "" + (int)CategoryType.PponChannelArea + "," + (int)CategoryType.DealCategory + "");

            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            qc.AddParameter("bid", bid, DbType.Guid);


            CategoryCollection data = new CategoryCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public List<int> CategoryIdGetListByBid(Guid bid)
        {
            string sql = @"
select cd.cid from category_deals cd with(nolock) 
inner join category c with(nolock) on c.id = cd.cid
where cd.bid = @bid
and c.type in (4,5,6) and c.status = 1
";
            QueryCommand qc = new QueryCommand(sql, Category.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);

            List<int> result = new List<int>();

            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetInt32(0));
                }
            }
            return result;
        }

        public CategoryDealCollection CategoryDealsGetList(List<Guid> bids)
        {
            CategoryDealCollection col = new CategoryDealCollection();

            for (int i = 0; i < bids.Count; i += 2000)
            {
                col.AddRange(DB.SelectAllColumnsFrom<CategoryDeal>()
                    .Where(CategoryDeal.Columns.Bid).In(bids.Skip(i).Take(2000))
                    .ExecuteAsCollection<CategoryDealCollection>());
            }

            return col;
        }

        /// <summary>
        /// 依據檔次上檔區間，查詢此區間內的CategoryDeal資料
        /// </summary>
        /// <param name="dateStart">檔次起始時間</param>
        /// <param name="dateEnd">檔次截止時間</param>
        /// <param name="theType">查詢附載平衡設定</param>
        /// <returns></returns>
        public CategoryDealCollection CategoryDealsGetList(DateTime dateStart, DateTime dateEnd)
        {
            string sql = " select * from " + CategoryDeal.Schema.TableName + " with(nolock) " +
                         " where bid in (select b.GUID from " + BusinessHour.Schema.TableName +
                         " b with(nolock) where b.business_hour_order_time_s < @dateE" +
                         " and b.business_hour_order_time_e > @dateS    )";

            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            qc.AddParameter("dateS", dateStart, DbType.DateTime);
            qc.AddParameter("dateE", dateEnd, DbType.DateTime);

            CategoryDealCollection data = new CategoryDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public CategoryDealCollection CategoryDealsGetList(DateTime dateStart, DateTime dateEnd, CategoryType type)
        {
            string sql = " select * from " + CategoryDeal.Schema.TableName + " with(nolock) " +
                         " where bid in (select b.GUID from " + BusinessHour.Schema.TableName + " b with(nolock) where b.business_hour_order_time_s between @dateS and @dateE)" +
                         " and cid IN (select id from " + Category.Schema.TableName + " with(nolock) where type = @type) ";

            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            qc.AddParameter("dateS", dateStart, DbType.DateTime);
            qc.AddParameter("dateE", dateEnd, DbType.DateTime);
            qc.AddParameter("type", (int)type, DbType.Int32);

            CategoryDealCollection data = new CategoryDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void CategoryDealSave(CategoryDealCollection cds, Guid bid)
        {
            DB.Delete<CategoryDeal>(CategoryDeal.Columns.Bid, bid);
            DB.SaveAll<CategoryDeal, CategoryDealCollection>(cds);
        }

        public bool CategoryDealSetList(CategoryDealCollection categories)
        {
            DB.SaveAll(categories);
            return true;
        }

        public ViewCategoryDealCollection ViewCategoryDealGetList(CategoryType type)
        {
            string sql = "select * from " + ViewCategoryDeal.Schema.TableName + " with(nolock) where type=@type";
            QueryCommand qc = new QueryCommand(sql, ViewCategoryDeal.Schema.Provider.Name);
            qc.AddParameter("type", (int)type, DbType.Int32);
            ViewCategoryDealCollection data = new ViewCategoryDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewCategoryDealCollection ViewCategoryDealGetCidListByBid(int cityId, CategoryType type, Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDeal>()
                .Where(ViewCategoryDeal.Columns.Bid).IsEqualTo(bid)
                .And(ViewCategoryDeal.Columns.Type).IsEqualTo((int)type)
                .And(ViewCategoryDeal.Columns.CityId).IsEqualTo(cityId)
                .ExecuteAsCollection<ViewCategoryDealCollection>();
        }

        public void DeleteCategoryDealsByCategoryType(Guid bid, CategoryType type)
        {
            string sql = @" delete from " + CategoryDeal.Schema.TableName + @"
                            where " + CategoryDeal.Columns.Bid + @" = @bid
                            and " + CategoryDeal.Columns.Cid + @" in (SELECT c." + Category.Columns.Id + @" FROM " + Category.Schema.TableName + @" c where " + Category.Columns.Type + @" = @type )";
            QueryCommand qc = new QueryCommand(sql, ShoppingCart.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            qc.AddParameter("@type", (int)type, DbType.Int32);
            DB.Execute(qc);
        }

        public void UpdateCategoryDealsWithCategoryList()
        {
            string sql = @"           
                Declare @sDate datetime,@eDate datetime

                set @sDate = convert(datetime, convert(varchar(10), DATEADD(dd, -1, getdate()), 111) + ' 00:00:00')
                set @eDate = @sDate+2

                update dbo.deal_property 
                set category_list = isnull(apple2.category_list,'[]')
	            from deal_property as dp2,
	            (
		            select business_hour_guid,'[' + SUBSTRING((apple1.category_list ),0,len(apple1.category_list)) +  ']' as category_list from
		            (
			            select  business_hour_guid,
			            (
				            select  DISTINCT cast(+ cid as varchar) + ',' from category_deals with(nolock) where category_deals.bid = dp1.business_hour_guid for xml path('') 
			            ) as category_list from 
			            (
				            select distinct business_hour_guid from deal_time_slot with(nolock)
				            where effective_start >= @sDate and effective_start < @eDate
					            union
	                        select BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid 
				            in (
					            select distinct business_hour_guid from deal_time_slot with(nolock)
					            where effective_start >= @sDate and effective_start < @eDate
				            )
			            ) as dp1   
		            )as apple1
	            ) as apple2 where dp2.business_hour_guid = apple2.business_hour_guid";

            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            DB.Execute(qc);
        }

        public int GetCategoryDealCountByBidAndCid(Guid bid, int cid)
        {
            var data = DB.SelectAllColumnsFrom<CategoryDeal>().NoLock()
                .Where(CategoryDeal.Columns.Bid).IsEqualTo(bid)
                .And(CategoryDeal.Columns.Cid).IsEqualTo(cid)
                .ExecuteAsCollection<CategoryDealCollection>();
            return data.Count();
        }

        public int GetCategoryDealCountByBidAndCidString(Guid bid, string cidstring)
        {
            var cids = (from t in cidstring.Split(',') select int.Parse(t)).ToArray();
            var data = DB.SelectAllColumnsFrom<CategoryDeal>().NoLock()
                .Where(CategoryDeal.Columns.Bid).IsEqualTo(bid)
                .And(CategoryDeal.Columns.Cid).In(cids)
                .ExecuteAsCollection<CategoryDealCollection>();
            return data.Count();
        }

        public void SaveCategoryDeal(CategoryDealCollection cdc)
        {
            DB.SaveAll(cdc);
        }

        public void RemoveCategoryDeal(CategoryDealCollection cdc)
        {
            cdc.ForEach(x => DB.Delete().From<CategoryDeal>()
                .Where(CategoryDeal.Columns.Bid).IsEqualTo(x.Bid)
                .And(CategoryDeal.Columns.Cid).IsEqualTo(x.Cid)
                .Execute());
        }

        public Dictionary<Guid, List<int>> DealCategoriesDictGetAll()
        {
            Dictionary<Guid, List<int>> result = new Dictionary<Guid, List<int>>();
            string sql = @"
select cd.bid, cd.cid from category_deals cd
where cd.cid in 
(
select id from category where code in ('5001', '5002', '5003', '5004','5005', '5008')
)";
            QueryCommand qc = new QueryCommand(sql, CategoryDeal.Schema.Provider.Name);
            using (var reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    Guid bid = reader.GetGuid(0);
                    int cid = reader.GetInt32(1);
                    if (result.ContainsKey(bid) == false)
                    {
                        result.Add(bid, new List<int>());
                    }
                    result[bid].Add(cid);
                }
            }
            return result;
        }

        #endregion CategoryDeals

        #region ViewCategoryDeals

        public ViewCategoryDealCollection ViewCategoryDealGetList()
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDeal>().NoLock()
                     .ExecuteAsCollection<ViewCategoryDealCollection>();
        }
        #endregion

        #region ViewCategoryDealBase
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dateStart"></param>
        /// <param name="dateEnd"></param>
        /// <param name="theType"></param>
        /// <returns></returns>
        public ViewCategoryDealBaseCollection ViewCategoryDealBaseGetList(DateTime dateStart, DateTime dateEnd)
        {
            string sql = " select * from " + ViewCategoryDealBase.Schema.TableName + " with(nolock) " +
                         " where bid in (select b.GUID from " + BusinessHour.Schema.TableName +
                         " b with(nolock) where b.business_hour_order_time_s < @dateE" +
                         " and b.business_hour_order_time_e > @dateS    )";

            QueryCommand qc = new QueryCommand(sql, ViewCategoryDealBase.Schema.Provider.Name);
            qc.AddParameter("dateS", dateStart, DbType.DateTime);
            qc.AddParameter("dateE", dateEnd, DbType.DateTime);

            ViewCategoryDealBaseCollection data = new ViewCategoryDealBaseCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewCategoryDealBaseCollection ViewCategoryDealBaseGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDealBase>()
                .Where(ViewCategoryDealBase.Columns.Bid).IsEqualTo(bid)
                .ExecuteAsCollection<ViewCategoryDealBaseCollection>();
        }

        public ViewCategoryDealBaseCollection ViewCategoryDealBaseGetListByBids(List<Guid> bids)
        {
            return DB.SelectAllColumnsFrom<ViewCategoryDealBase>()
                .Where(ViewCategoryDealBase.Columns.Bid).In(bids)
                .ExecuteAsCollection<ViewCategoryDealBaseCollection>();
        }
        #endregion

        #region CategoryDealList

        public ViewCategoryDealListCollection GetCategoryDealListByCidAndOrderTime(int cid, string dateS, string dateE)
        {
            var data = DB.SelectAllColumnsFrom<ViewCategoryDealList>().NoLock()
                .Where(ViewCategoryDealList.Columns.Cid).IsEqualTo(cid);
            if (dateE == null)
            {
                data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeS).IsLessThanOrEqualTo(DateTime.MaxValue);
            }
            else
            {
                data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeS).IsLessThanOrEqualTo(dateE);
            }
            data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeE).IsGreaterThanOrEqualTo(dateS)
            .OrderAsc(ViewCategoryDealList.Columns.Outdate)
            .OrderDesc(ViewCategoryDealList.Columns.Id);

            return data.ExecuteAsCollection<ViewCategoryDealListCollection>();
        }

        public ViewCategoryDealListCollection GetCategoryDealListByBidAndCidAndOrderTime(string bidstring, int cid, string dateS, string dateE)
        {
            var bids = (from t in bidstring.Split(',') select Guid.Parse(t)).ToArray();
            var data = DB.SelectAllColumnsFrom<ViewCategoryDealList>().NoLock()
                .Where(ViewCategoryDealList.Columns.Cid).IsEqualTo(cid)
                .And(ViewCategoryDealList.Columns.Bid).In(bids);
            if (dateE == "")
            {
                data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeS).IsLessThanOrEqualTo(DateTime.MaxValue);
            }
            else
            {
                data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeS).IsLessThanOrEqualTo(dateE);
            }
            data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeE).IsGreaterThanOrEqualTo(dateS)
                .OrderAsc(ViewCategoryDealList.Columns.Outdate)
                .OrderDesc(ViewCategoryDealList.Columns.Id);

            return data.ExecuteAsCollection<ViewCategoryDealListCollection>();
        }

        public ViewCategoryDealListCollection GetCategoryDealListByBidAndCidOutsideOfOrdertime(string bidstring, int cid, string dateS, string dateE)
        {
            var bids = (from t in bidstring.Split(',') select Guid.Parse(t)).ToArray();
            var data = DB.SelectAllColumnsFrom<ViewCategoryDealList>().NoLock()
                .Where(ViewCategoryDealList.Columns.Cid).IsEqualTo(cid)
                .And(ViewCategoryDealList.Columns.Bid).In(bids);
            if (dateE == "")
            {
                data.And(ViewCategoryDealList.Columns.BusinessHourOrderTimeE).IsLessThan(dateS);
            }
            else
            {
                data.AndExpression(ViewCategoryDealList.Columns.BusinessHourOrderTimeS).IsGreaterThan(dateE)
                    .Or(ViewCategoryDealList.Columns.BusinessHourOrderTimeE).IsLessThan(dateS).CloseExpression();
            }

            return data.ExecuteAsCollection<ViewCategoryDealListCollection>();
        }
        #endregion

        #region CategoryDependency
        public CategoryDependencyCollection GetCategoryDependencyByParentId(int parentid)
        {
            var data = DB.SelectAllColumnsFrom<CategoryDependency>().NoLock()
                .Where(CategoryDependency.Columns.ParentId).IsEqualTo(parentid)
                .ExecuteAsCollection<CategoryDependencyCollection>();
            return data;
        }

        public int GetCategoryDependencyParentIdByCategoryId(int categoryid)
        {
            var data = DB.SelectAllColumnsFrom<CategoryDependency>().NoLock()
                .Where(CategoryDependency.Columns.CategoryId).IsEqualTo(categoryid)
                .ExecuteSingle<CategoryDependency>();
            return data.ParentId;
        }

        public bool CategoryFinalNodeSet(int cid)
        {
            //因category的is_final欄位不準才需使用此function
            Category c = DB.Select(Category.Columns.Id).From<Category>().NoLock()
                .Where(Category.Columns.Id).In(
                    new Select(CategoryDependency.Columns.CategoryId).From<CategoryDependency>().Distinct().NoLock()
                    .Where(CategoryDependency.Columns.CategoryId).NotIn(
                        new Select(CategoryDependency.Columns.ParentId).From<CategoryDependency>().Distinct().NoLock()
                    )
                )
                .And(Category.Columns.Id).IsEqualTo(cid)
                .ExecuteSingle<Category>();

            if (c.Id != 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public int GetCategoryDependencyParentIdByCidAndBid(int categoryid, Guid bid)
        {
            var data = DB.SelectAllColumnsFrom<CategoryDependency>().NoLock()
                .Where(CategoryDependency.Columns.CategoryId).IsEqualTo(categoryid)
                .And(CategoryDependency.Columns.ParentId).In(
                new Select(CategoryDeal.Columns.Cid).From(CategoryDeal.Schema)
                .Where(CategoryDeal.Columns.Bid).IsEqualTo(bid)
                )
                .ExecuteSingle<CategoryDependency>();
            return data.ParentId;
        }

        public int[] GetCategoryDependencyPidByCid(int cid)
        {
            CategoryDependencyCollection cdc = DB.Select(CategoryDependency.Columns.ParentId).From<CategoryDependency>().NoLock()
                .Where(CategoryDependency.Columns.CategoryId).IsEqualTo(cid)
                .ExecuteAsCollection<CategoryDependencyCollection>();

            int[] pids = new int[cdc.Count];
            int i = 0;

            foreach (CategoryDependency cd in cdc)
            {
                pids[i] = cd.ParentId;
                i++;
            }

            return pids;
        }

        public IList<CategoryDependency> GetAllCategoryDependencies()
        {
            CategoryDependencyCollection cdc = DB.SelectAllColumnsFrom<CategoryDependency>().NoLock()
                .ExecuteAsCollection<CategoryDependencyCollection>();

            return cdc.ToList();
        }


        #endregion

        #region ViewAllDealCategory

        public ViewAllDealCategoryCollection ViewAllDealCategoryGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewAllDealCategory>().NoLock()
                .Where(ViewAllDealCategory.Columns.Bid).IsEqualTo(bid).ExecuteAsCollection<ViewAllDealCategoryCollection>();
        }

        public ViewAllDealCategoryCollection ViewAllDealCategoryGetListByBidList(List<Guid> bids)
        {
            ViewAllDealCategoryCollection vadc = new ViewAllDealCategoryCollection();
            int idx = 0;
            while (idx <= bids.Count - 1)
            {
                int batchLimit = 200;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewAllDealCategory>()
                        .Where(ViewAllDealCategory.Columns.Bid).In(bids.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewAllDealCategoryCollection>();

                vadc.AddRange(batchProd);

                idx += batchLimit;
            }

            return vadc;
        }
        #endregion ViewAllDealCategory

        #region CategoryVourcher

        public CategoryVourcherCollection CategoryVourcherGetList(int vourcherId)
        {
            return DB.SelectAllColumnsFrom<CategoryVourcher>().Where(CategoryVourcher.Columns.VourcherId).IsEqualTo(vourcherId).ExecuteAsCollection<CategoryVourcherCollection>();
        }

        public CategoryVourcherCollection CategoryVourcherGetList(DateTime dateStart, DateTime dateEnd, CategoryType type)
        {
            string sql = " select * from " + CategoryVourcher.Schema.TableName + " with(nolock) " +
                         " where vourcher_id in (select v.id from " + VourcherEvent.Schema.TableName + " v with(nolock) where v.start_date between @dateS and @dateE)" +
                         " and cid IN (select id from " + Category.Schema.TableName + " with(nolock) where type = @type) ";

            QueryCommand qc = new QueryCommand(sql, CategoryVourcher.Schema.Provider.Name);
            qc.AddParameter("dateS", dateStart, DbType.DateTime);
            qc.AddParameter("dateE", dateEnd, DbType.DateTime);
            qc.AddParameter("type", (int)type, DbType.Int32);

            CategoryVourcherCollection data = new CategoryVourcherCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void CategoryVourcherSave(CategoryVourcherCollection cvs, int vourcherId)
        {
            DB.Delete<CategoryVourcher>(CategoryVourcher.Columns.VourcherId, vourcherId);
            DB.SaveAll<CategoryVourcher, CategoryVourcherCollection>(cvs);
        }

        #endregion CategoryVourcher

        #region ViewPponBusinessHourGuidGetList

        public ViewPponBusinessHourGuidCollection ViewPponBusinessHourGuidGetList(DateTime theDate)
        {
            string dateBegin = theDate.ToString("yyyy/MM/dd 00:00:00.000");
            string strSQL = " select * from " + ViewPponBusinessHourGuid.Schema.Provider.DelimitDbName(ViewPponBusinessHourGuid.Schema.TableName) +
                            " with(nolock) where " + ViewPponBusinessHourGuid.Columns.BusinessHourOrderTimeE + " <= @BeginDate ";

            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("BeginDate", dateBegin, DbType.DateTime);

            ViewPponBusinessHourGuidCollection vcbht = new ViewPponBusinessHourGuidCollection();
            vcbht.LoadAndCloseReader(DataService.GetReader(qc));
            return vcbht;
        }

        /// <summary>
        /// 取得某個區間內開檔的檔次
        /// </summary>
        /// <param name="startDate">起始時間</param>
        /// <param name="endDate">結束時間</param>
        /// <returns>ViewPponBusinessHourGuidCollection</returns>
        public ViewPponBusinessHourGuidCollection ViewPponBusinessHourGuidGetList(DateTime startDate, DateTime endDate)
        {
            string dateBegin = startDate.ToString("yyyy/MM/dd 00:00:00.000");
            string dateEnd = endDate.ToString("yyyy/MM/dd 00:00:00.000");
            string strSQL = string.Format(" select * from {0} with(nolock) where {1} between @BeginDate and @EndDate ",
                ViewPponBusinessHourGuid.Schema.Provider.DelimitDbName(ViewPponBusinessHourGuid.Schema.TableName),
                ViewPponBusinessHourGuid.Columns.BusinessHourOrderTimeS);

            QueryCommand qc = new QueryCommand(strSQL, "");
            qc.AddParameter("BeginDate", dateBegin, DbType.DateTime);
            qc.AddParameter("EndDate", dateEnd, DbType.DateTime);

            ViewPponBusinessHourGuidCollection vcbht = new ViewPponBusinessHourGuidCollection();
            vcbht.LoadAndCloseReader(DataService.GetReader(qc));
            return vcbht;
        }

        #endregion ViewPponBusinessHourGuidGetList

        #region ViewPponDealStore

        /// <summary>
        /// 商家後臺_核銷查詢:根據輸入之查詢條件取出已成檔之憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="storeGuidList">具檢視權限之分店Guid list 以,連結多筆資料</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewPponDealStoreCollection ViewPponDealStoreGetToShopDealList(string storeGuidList, params string[] filter)
        {
            QueryCommand qc = GetVpolWhereQC(ViewPponDealStore.Schema, filter);
            qc.CommandSql = "select * from " + ViewPponDealStore.Schema.TableName + " with(nolock) " + qc.CommandSql;
            qc.CommandSql = qc.CommandSql.TrimEnd(';') +
                string.Format(@" and {0} = 1 
                  and ( {1} < GETDATE() and isnull({2}, 0) >= cast({3} as int) 
                    or ({1} >= GETDATE() and isnull({2}, 0) = 0 
                        and GETDATE() >= {4})
                    ) and {5} & {6} = 0"
                 , ViewPponDealStore.Columns.DeliveryType
                 , ViewPponDealStore.Columns.DealEndTime
                 , ViewPponDealStore.Columns.Slug
                 , ViewPponDealStore.Columns.OrderMinimum
                 , ViewPponDealStore.Columns.DealStartTime
                 , ViewPponDealStore.Columns.BusinessHourStatus
                 , (int)BusinessHourStatus.ComboDealMain);

            if (!string.IsNullOrEmpty(storeGuidList))
            {
                qc.CommandSql = qc.CommandSql + string.Format(" and {0} in ('{1}')"
                                                , ViewPponDealStore.Columns.StoreGuid
                                                , storeGuidList.Replace(",", "','"));
            }

            ViewPponDealStoreCollection data = new ViewPponDealStoreCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion ViewPponDealStore

        #region ViewVbsToShopDeal

        public ViewVbsToShopDealCollection ViewVbsToShopDealGetList(
                string queryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime)
        {
            if (string.IsNullOrEmpty(queryOption) ||
                (string.IsNullOrEmpty(queryKeyword) && queryStartTime == null && queryEndTime == null))
            {
                return new ViewVbsToShopDealCollection();
            }
            if (queryEndTime != null)
            {
                queryEndTime = Helper.GetFinalTime(queryEndTime.Value);
            }

            SqlQuery query = DB.Select().From(ViewVbsToShopDeal.Schema);

            if (queryOption == "productName" && string.IsNullOrEmpty(queryKeyword) == false)
            {
                query.Where(ViewVbsToShopDeal.Columns.ProductName)
                    .Like("%" + queryKeyword + "%");
            }
            else if (queryOption == "sellerName" && string.IsNullOrEmpty(queryKeyword) == false)
            {
                query.Where(ViewVbsToShopDeal.Columns.SellerName)
                    .Like("%" + queryKeyword + "%");
            }
            else if (queryOption == "uniqueId" && string.IsNullOrEmpty(queryKeyword) == false)
            {
                int id;
                int.TryParse(queryKeyword, out id);
                query.Where(ViewVbsToShopDeal.Columns.ProductId)
                        .IsEqualTo(id);
            }
            else if (queryOption == "useStartTime")
            {
                if (queryStartTime != null && queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseStartTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value)
                        .And(ViewVbsToShopDeal.Columns.UseStartTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryStartTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseStartTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseStartTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value);
                }
            }
            else if (queryOption == "useEndTime")
            {
                if (queryStartTime != null && queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseEndTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value)
                        .And(ViewVbsToShopDeal.Columns.UseEndTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryStartTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseEndTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.UseEndTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value);
                }
            }
            else if (queryOption == "dealStartTime")
            {
                if (queryStartTime != null && queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealStartTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value)
                        .And(ViewVbsToShopDeal.Columns.DealStartTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryStartTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealStartTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealStartTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value);
                }
            }
            else if (queryOption == "dealEndTime")
            {
                if (queryStartTime != null && queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealEndTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value)
                        .And(ViewVbsToShopDeal.Columns.DealEndTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryStartTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealEndTime)
                        .IsGreaterThanOrEqualTo(queryStartTime.Value);
                }
                else if (queryEndTime != null)
                {
                    query.Where(ViewVbsToShopDeal.Columns.DealEndTime)
                        .IsLessThanOrEqualTo(queryEndTime.Value);
                }
            }

            return query.ExecuteAsCollection<ViewVbsToShopDealCollection>();
        }

        #endregion ViewVbsToShopDeal

        #region ViewVbsToHouseDeal

        public ViewVbsToHouseDealCollection ViewVbsToHouseDealGetList(
            string queryOption, string queryKeyword, DateTime? queryStartTime, DateTime? queryEndTime)
        {
            if (string.IsNullOrEmpty(queryOption) ||
                (string.IsNullOrEmpty(queryKeyword) && queryStartTime == null && queryEndTime == null))
            {
                return new ViewVbsToHouseDealCollection();
            }
            if (queryEndTime != null)
            {
                queryEndTime = Helper.GetFinalTime(queryEndTime.Value);
            }
            SqlQuery query = DB.Select().From(ViewVbsToHouseDeal.Schema);
            //只抓取對帳方式選擇[新版核銷對帳系統]的宅配檔次
            query.Where(ViewVbsToHouseDeal.Columns.VendorBillingModel).IsEqualTo(
                (int)VendorBillingModel.BalanceSheetSystem);

            if (!string.IsNullOrEmpty(queryOption))
            {
                if (queryOption == "productGuid" && !string.IsNullOrEmpty(queryKeyword))
                {
                    Guid productGuid;
                    Guid.TryParse(queryKeyword, out productGuid);
                    //若為多檔次可以子檔bid搜尋到母檔
                    var productGuids = GetViewComboDealAllByBid(productGuid)
                                          .Select(x => x.BusinessHourGuid)
                                          .ToList();
                    if (productGuids.Any())
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductGuid).In(productGuids);
                    }
                    else
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductGuid).IsEqualTo(productGuid);
                    }
                }
                else if (queryOption == "productName" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    query.And(ViewVbsToHouseDeal.Columns.ProductName).Like("%" + queryKeyword + "%");
                }
                else if (queryOption == "sellerName" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    query.And(ViewVbsToHouseDeal.Columns.SellerName).Like("%" + queryKeyword + "%");
                }
                else if (queryOption == "uniqueId" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    int id;
                    int.TryParse(queryKeyword, out id);
                    var bid = DealPropertyGet(id).BusinessHourGuid;
                    //若為多檔次可以子檔unique_id搜尋到母檔
                    var productIds = GetViewComboDealAllByBid(bid)
                                        .Select(x => x.UniqueId)
                                        .ToList();
                    if (productIds.Any())
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductId).In(productIds);
                    }
                    else
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductId).IsEqualTo(id);
                    }
                }
                else if (queryOption == "useStartTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
                else if (queryOption == "useEndTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
                else if (queryOption == "dealStartTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
                else if (queryOption == "dealEndTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
            }
            return query.ExecuteAsCollection<ViewVbsToHouseDealCollection>();
        }

        public ViewVbsToHouseDealCollection ViewVbsToHouseDealGetList(
           string queryOption, string queryKeyword, string queryTimeOption, DateTime? queryStartTime, DateTime? queryEndTime)
        {
            if (string.IsNullOrEmpty(queryKeyword) && queryStartTime == null && queryEndTime == null)
            {
                return new ViewVbsToHouseDealCollection();
            }
            if (queryEndTime != null)
            {
                queryEndTime = Helper.GetFinalTime(queryEndTime.Value);
            }
            SqlQuery query = DB.Select().From(ViewVbsToHouseDeal.Schema);
            //只抓取對帳方式選擇[新版核銷對帳系統]的宅配檔次
            query.Where(ViewVbsToHouseDeal.Columns.VendorBillingModel).IsEqualTo(
                (int)VendorBillingModel.BalanceSheetSystem);

            if (!string.IsNullOrEmpty(queryOption))
            {
                if (queryOption == "productGuid" && !string.IsNullOrEmpty(queryKeyword))
                {
                    Guid productGuid;
                    Guid.TryParse(queryKeyword, out productGuid);
                    var productGuids = GetViewComboDealAllByBid(productGuid)
                                          .Select(x => x.BusinessHourGuid)
                                          .ToList();
                    if (productGuids.Any())
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductGuid).In(productGuids);
                    }
                    else
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductGuid).IsEqualTo(productGuid);
                    }
                }
                else if (queryOption == "productName" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    query.And(ViewVbsToHouseDeal.Columns.ProductName).Like("%" + queryKeyword + "%");
                }
                else if (queryOption == "sellerName" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    query.And(ViewVbsToHouseDeal.Columns.SellerName).Like("%" + queryKeyword + "%");
                }
                else if (queryOption == "uniqueId" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    int id;
                    int.TryParse(queryKeyword, out id);
                    var bid = DealPropertyGet(id).BusinessHourGuid;
                    var productIds = GetViewComboDealAllByBid(bid)
                                        .Select(x => x.UniqueId)
                                        .ToList();
                    if (productIds.Any())
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductId).In(productIds);
                    }
                    else
                    {
                        query.And(ViewVbsToHouseDeal.Columns.ProductId).IsEqualTo(id);
                    }
                }
                else if (queryOption == "dealId" && string.IsNullOrEmpty(queryKeyword) == false)
                {
                    int id;
                    int.TryParse(queryKeyword, out id);
                    query.And(ViewVbsToHouseDeal.Columns.DealId)
                            .IsEqualTo(id);
                }
            }

            if (!string.IsNullOrEmpty(queryTimeOption))
            {
                if (queryTimeOption == "useTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.UseEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
                else if (queryTimeOption == "orderTime")
                {
                    if (queryStartTime != null && queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value)
                            .And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryStartTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealStartTime)
                            .IsGreaterThanOrEqualTo(queryStartTime.Value);
                    }
                    else if (queryEndTime != null)
                    {
                        query.And(ViewVbsToHouseDeal.Columns.DealEndTime)
                            .IsLessThanOrEqualTo(queryEndTime.Value);
                    }
                }
            }
            return query.ExecuteAsCollection<ViewVbsToHouseDealCollection>();
        }

        public ViewVbsToHouseDealCollection ViewVbsToHouseDealGetListByProductGuid(IEnumerable<Guid> productGuids)
        {
            return DB.SelectAllColumnsFrom<ViewVbsToHouseDeal>()
                        .Where(ViewVbsToHouseDeal.Columns.ProductGuid).In(productGuids)
                        .ExecuteAsCollection<ViewVbsToHouseDealCollection>() ?? new ViewVbsToHouseDealCollection();
        }

        #endregion ViewVbsToHouseDeal

        public DataTable BalanceSheetDeliverSubInfoGetTable(IEnumerable<Guid> productGuids)
        {
            //計算對帳單運費筆數以及運費單價
            //還有客訴補償
            DataTable infos = new DataTable();
            DataTable result = new DataTable();
            var tempIds = productGuids.Distinct().ToList();
            int idx = 0;
            //暫付七成付款日有值轉對帳單格式
            var bids = productGuids.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 999;

                string sql = string.Format(@"select bh.GUID, dc.cost, i.item_name, dp.unique_id, da.is_input_tax_required as is_tax
, da.partially_payment_date, isnull(bsDetail.fareamount, 0) as fareAmount, isnull(bsDetail.totalCount, 0) as totalCount
, da.remittance_type, vpc.debitAllowanceAmount, vpc.debitCaseAmount, da.balance_sheet_create_date, god.status, cast(isnull(god.slug, 0) as int) as slug 
from {0} bh
join {1} dc with(nolock) on dc.business_hour_guid = bh.GUID
join {2} i with(nolock) on i.business_hour_guid = bh.GUID
join {3} dp with(nolock) on dp.business_hour_guid = bh.GUID
join {4} da with(nolock) on da.business_hour_guid = bh.GUID
join {5} god with(nolock) on god.business_hour_guid = bh.GUID
left join (
    select ctl.business_hour_guid as bid, sum(case when special_status & {11} = 0 then 1 else 0 end) as totalCount
    , sum(case when special_status & {11} > 0 then amount else 0 end) as fareamount 
    from {6} ctl 
    join [{7}] o on ctl.order_guid = o.guid
    join {10} bsd on bsd.trust_id = ctl.trust_id
    where ctl.business_hour_guid in ({9})
    and o.order_status & {12} > 0 
    group by ctl.business_hour_guid
) bsDetail on bsDetail.bid = bh.GUID 
left join (
    select business_hour_guid, sum(case allowance_amount when 0 then amount else allowance_amount end) as debitCaseAmount
    , sum(allowance_amount) as debitAllowanceAmount 
    from {8} group by business_hour_guid 
) vpc on vpc.business_hour_guid = bh.GUID
where bh.GUID in ({9})
and god.slug >= bh.business_hour_order_minimum",
            BusinessHour.Schema.TableName, DealCost.Schema.TableName, Item.Schema.TableName,
            DealProperty.Schema.TableName, DealAccounting.Schema.TableName, GroupOrder.Schema.TableName,
            CashTrustLog.Schema.TableName, Order.Schema.TableName, VendorPaymentChange.Schema.TableName,
            string.Join(",", bids.Skip(idx).Take(batchLimit)), BalanceSheetDetail.Schema.TableName,
            (int)TrustSpecialStatus.Freight, (int)OrderStatus.Complete);

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                using (IDataReader reader = DataService.GetReader(qc))
                {
                    infos.Load(reader);
                }

                result.Merge(infos);

                idx += batchLimit;
            }
            return result;
        }

        #region New EDM

        public EdmMainCollection GetEdmMainByDate(DateTime deliverydate, EdmMainType edmtype, bool status)
        {
            EdmMainCollection edmmain =
                new Select().From<EdmMain>().Where(EdmMain.Columns.DeliveryDate).IsGreaterThanOrEqualTo(deliverydate).And(EdmMain.Columns.DeliveryDate).IsLessThan(deliverydate.AddDays(1))
                    .And(EdmMain.Columns.Type).IsEqualTo((int)edmtype).And(EdmMain.Columns.Status).IsEqualTo(status).ExecuteAsCollection<EdmMainCollection>();
            return edmmain;
        }

        public EdmMain GetEdmMainByCityDate(int cityid, DateTime deliverydate, EdmMainType edmtype, bool status)
        {
            EdmMain edmmain =
               new Select().From<EdmMain>().Where(EdmMain.Columns.CityId)
                   .IsEqualTo(cityid).And(EdmMain.Columns.DeliveryDate).IsEqualTo(deliverydate)
                   .And(EdmMain.Columns.Type).IsEqualTo((int)edmtype).And(EdmMain.Columns.Status).IsEqualTo(status).ExecuteSingle<EdmMain>();
            return edmmain;
        }

        public EdmMainCollection GetEdmMainByCityDates(int cityid, DateTime deliverydateS, DateTime deliverydateE, EdmMainType edmtype, bool status)
        {
            EdmMainCollection edmmain =
               new Select().From<EdmMain>().Where(EdmMain.Columns.CityId)
                   .IsEqualTo(cityid).And(EdmMain.Columns.DeliveryDate).IsLessThanOrEqualTo(deliverydateE)
                   .And(EdmMain.Columns.DeliveryDate).IsGreaterThanOrEqualTo(deliverydateS)
                   .And(EdmMain.Columns.Type).IsEqualTo((int)edmtype).And(EdmMain.Columns.Status).IsEqualTo(status).ExecuteAsCollection<EdmMainCollection>();
            return edmmain;
        }

        /// <summary>
        /// 取得未上傳的當日Edm主檔
        /// </summary>
        /// <param name="deliverydate"></param>
        /// <param name="edmtype"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public List<EdmMain> GetNewEdmMainListByDateWasNotUploaded(DateTime deliverydate, EdmMainType edmtype, bool status)
        {
            string sql = @"
select * from edm_main with(nolock) 
    where delivery_date >= @startTime and delivery_date < @endTime
        and [type] = @type
        and status = @status
        and upload_date is null
";
            QueryCommand qc = new QueryCommand(sql, EdmMain.Schema.Provider.Name);
            qc.AddParameter("@startTime", deliverydate.Date, DbType.DateTime);
            qc.AddParameter("@endTime", deliverydate.Date.AddDays(1), DbType.DateTime);
            qc.AddParameter("@type", edmtype, DbType.Int32);
            qc.AddParameter("@status", status, DbType.Boolean);

            EdmMainCollection edmCols = new EdmMainCollection();
            edmCols.LoadAndCloseReader(DataService.GetReader(qc));
            return edmCols.ToList();
        }

        public EdmMainCollection GetEdmMainByCity(int cityid, EdmMainType edmtype, bool status)
        {
            EdmMainCollection edmmain =
               new Select().From<EdmMain>().Where(EdmMain.Columns.CityId)
                   .IsEqualTo(cityid)
                   .And(EdmMain.Columns.Type).IsEqualTo((int)edmtype).And(EdmMain.Columns.Status).IsEqualTo(status).ExecuteAsCollection<EdmMainCollection>();
            return edmmain;
        }

        public EdmMain GetEdmMainById(int id)
        {
            EdmMain edmmain =
                  new Select().From<EdmMain>().Where(EdmMain.Columns.Id).
                      IsEqualTo(id).ExecuteSingle<EdmMain>();
            return edmmain;
        }

        public EdmMain GetEdmMainByCityId(int cid)
        {
            DateTime now = DateTime.Now;
            DateTime basedate = now >= DateTime.Today.AddHours(12) ? DateTime.Today : DateTime.Today.AddDays(-1);
            EdmMain edmmain =
                 new Select().From<EdmMain>().Where(EdmMain.Columns.DeliveryDate).IsGreaterThan(basedate)
                 .And(EdmMain.Columns.DeliveryDate).IsLessThan(basedate.AddDays(1))
                 .And(EdmMain.Columns.CityId).IsEqualTo(cid).And(EdmMain.Columns.Status).IsEqualTo(true).And(EdmMain.Columns.Type).IsEqualTo((int)EdmMainType.Daily).ExecuteSingle<EdmMain>();
            return edmmain;
        }

        public void SaveEdmMain(EdmMain edmmain)
        {
            DB.Save<EdmMain>(edmmain);
        }

        public EdmAreaDetailCollection GetEdmAreaCollectionByPid(int pid)
        {
            EdmAreaDetailCollection edmmain =
                new Select().From<EdmAreaDetail>().Where(EdmAreaDetail.Columns.Pid).IsEqualTo(pid)
                    .ExecuteAsCollection<EdmAreaDetailCollection>();
            return edmmain;
        }

        public EdmDetailCollection GetEdmDetailCollectionByPid(int pid)
        {
            EdmDetailCollection items = new Select().From<EdmDetail>()
                      .Where(EdmDetail.Columns.Pid).IsEqualTo(pid)
                      .ExecuteAsCollection<EdmDetailCollection>();
            return items;
        }

        public void SaveEdmDetail(EdmDetail edmdetail)
        {
            DB.Save<EdmDetail>(edmdetail);
        }

        public void SaveEdmAreaDetail(EdmAreaDetail edmdetail)
        {
            DB.Save<EdmAreaDetail>(edmdetail);
        }

        public void DeleteEdmDetailByPid(int pid)
        {
            DB.Delete<EdmDetail>(EdmDetail.Columns.Pid, pid);
        }
        public void DeleteEdmAreaDetailByPid(int pid)
        {
            DB.Delete<EdmAreaDetail>(EdmDetail.Columns.Pid, pid);
        }

        public void DisableEdmMain(int id, bool enable)
        {
            string sql = "update " + EdmMain.Schema.TableName + " set " + EdmMain.Columns.Status + "=@status  where " + EdmMain.Columns.Id + " = @id";
            QueryCommand qc = new QueryCommand(sql, CtAtmRefund.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            qc.AddParameter("@status", enable, DbType.Boolean);
            DataService.ExecuteScalar(qc);
        }

        #endregion New EDM

        #region Solo EDM
        public SoloEdmMainCollection GetSoloEdmMainByType(int type)
        {
            SoloEdmMainCollection edmmain =
                new Select().From<SoloEdmMain>().Where(SoloEdmMain.Columns.Type).IsEqualTo(type)
                    .ExecuteAsCollection<SoloEdmMainCollection>();
            return edmmain;
        }

        /// <summary>
        /// 只取目前運作中的EDM，且時間在sendDate一天內的區間
        /// </summary>
        /// <param name="sendDate"></param>
        /// <returns></returns>
        public SoloEdmMainCollection GetSoloEdmMainByDate(DateTime sendDate)
        {
            var tmpDate = sendDate.AddDays(-1);

            SoloEdmMainCollection edmmain = new Select().From<SoloEdmMain>().Where(SoloEdmMain.Columns.SendDate)
                .IsLessThanOrEqualTo(sendDate).And(SoloEdmMain.Columns.SendDate).IsGreaterThan(tmpDate)
                .And(SoloEdmMain.TypeColumn).IsEqualTo(0)
                .ExecuteAsCollection<SoloEdmMainCollection>();

            return edmmain;
        }

        public SoloEdmMain GetSoloEdmMainById(int eid)
        {
            SoloEdmMain edmmain =
                new Select().From<SoloEdmMain>().Where(SoloEdmMain.Columns.Id)
                    .IsEqualTo(eid).ExecuteSingle<SoloEdmMain>();
            return edmmain;
        }

        public SoloEdmBrandCollection GetSoloEdmBrandById(int eid)
        {
            SoloEdmBrandCollection edmmain =
                new Select().From<SoloEdmBrand>().Where(SoloEdmBrand.Columns.MainId).IsEqualTo(eid).ExecuteAsCollection<SoloEdmBrandCollection>();
            return edmmain;
        }

        public SoloEdmPponCollection GetSoloEdmPponById(int eid)
        {
            SoloEdmPponCollection edmmain =
                new Select().From<SoloEdmPpon>().Where(SoloEdmPpon.Columns.MainId).IsEqualTo(eid).ExecuteAsCollection<SoloEdmPponCollection>();
            return edmmain;
        }

        public SoloEdmSendUserCollection GetSoloEdmSendUserById(int eid)
        {
            SoloEdmSendUserCollection userCollection =
                new Select().From<SoloEdmSendUser>().Where(SoloEdmSendUser.Columns.MainId).IsEqualTo(eid).ExecuteAsCollection<SoloEdmSendUserCollection>();
            return userCollection;
        }

        public void SaveSoloEdmMain(SoloEdmMain soloedm)
        {
            DB.Save<SoloEdmMain>(soloedm);
        }

        public void SaveSoloEdmBrand(SoloEdmBrand soloEdmBrand)
        {
            DB.Save<SoloEdmBrand>(soloEdmBrand);
        }
        public void SaveSoloEdmPpon(SoloEdmPpon soloEdmPpon)
        {
            DB.Save<SoloEdmPpon>(soloEdmPpon);
        }

        public void SaveSoloEdmSendUser(SoloEdmSendUser sendUser)
        {
            DB.Save<SoloEdmSendUser>(sendUser);
        }
        public int SoloEdmSendCountGetByMainId(int mainId)
        {
            var sql = @"select count(1) cnt from " + SoloEdmSendUser.Schema.Provider.DelimitDbName(SoloEdmSendUser.Schema.TableName) + @" with(nolock)
                    where " + SoloEdmSendUser.Columns.MainId + " = @mainId ";
            var qc = new QueryCommand(sql, SoloEdmSendUser.Schema.Provider.Name);
            qc.AddParameter("@mainId", mainId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }
        public void SoloEdmSetAllMembers(int MainId)
        {
            string sql = @" INSERT into " + SoloEdmSendUser.Schema.TableName + @"
                            SELECT @mainId," + Member.Columns.UniqueId +
                            @" from " + Member.Schema.TableName + @" with(NOLOCK)";
            QueryCommand qc = new QueryCommand(sql, EinvoiceMain.Schema.Provider.Name);
            qc.AddParameter("@mainId", MainId, DbType.Int32);
            qc.CommandTimeout = 600;
            DataService.ExecuteScalar(qc);
        }

        public void DeleteSoloEdmBrandByMainId(int MainId)
        {
            DB.Delete<SoloEdmBrand>(SoloEdmBrand.Columns.MainId, MainId);
        }
        public void DeleteSoloEdmPponByMainId(int MainId)
        {
            DB.Delete<SoloEdmPpon>(SoloEdmPpon.Columns.MainId, MainId);
        }
        public void DeleteSoloEdmSendUserByMainId(int mainId)
        {
            DB.Delete<SoloEdmSendUser>(SoloEdmSendUser.Columns.MainId, mainId);
        }


        #endregion

        #region edm_hot_deal
        public void EdmHotDealSet(EdmHotDeal edmHotDeal)
        {
            DB.Save<EdmHotDeal>(edmHotDeal);
        }

        public void EdmHotDealCollectionSet(EdmHotDealCollection edmHotDeals)
        {
            DB.SaveAll(edmHotDeals);
        }

        public EdmHotDealCollection EdmHotDealGet()
        {
            return DB.SelectAllColumnsFrom<EdmHotDeal>()
                .Where(EdmHotDeal.Columns.Status).IsEqualTo(1)
                .OrderDesc(EdmHotDeal.Columns.CreateTime)
                .ExecuteAsCollection<EdmHotDealCollection>();
        }

        public EdmHotDealCollection EdmHotDealGetByBid(Guid bid, DateTime startDate)
        {
            return DB.SelectAllColumnsFrom<EdmHotDeal>()
                .Where(EdmHotDeal.Columns.Bid).IsEqualTo(bid)
                .And(EdmHotDeal.Columns.StartDate).IsLessThanOrEqualTo(startDate)
                .And(EdmHotDeal.Columns.EndDate).IsGreaterThan(startDate)
                .And(EdmHotDeal.Columns.Status).IsEqualTo(1)
                .ExecuteAsCollection<EdmHotDealCollection>();
        }

        public EdmHotDeal EdmHotDealGetById(int id)
        {
            return DB.SelectAllColumnsFrom<EdmHotDeal>().Where(EdmHotDeal.Columns.Id).IsEqualTo(id).ExecuteSingle<EdmHotDeal>();
        }

        public DataTable EdmHotDealByCityIdDate(int cityid, string startDate)
        {
            string sql = @"select * from get_edm_hot_deal(@cityid,@startDate)";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@cityid", cityid, DbType.Int32);
            qc.AddParameter("@startDate", startDate, DbType.String);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, cityid, startDate))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable EdmPriorityDealByCityIdDate(int cityid, string startDate)
        {
            string sql = @"select * from get_edm_priority_deal(@cityid,@startDate)";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@cityid", cityid, DbType.Int32);
            qc.AddParameter("@startDate", startDate, DbType.String);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, cityid, startDate))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable EdmPriorityDealByCityIdDate24(int cityid, string startDate)
        {
            string sql = @"select * from get_edm_priority_deal_pc_24H(@cityid,@startDate)";
            var qc = new QueryCommand(sql, ViewPponDeal.Schema.Provider.Name);
            qc.AddParameter("@cityid", cityid, DbType.Int32);
            qc.AddParameter("@startDate", startDate, DbType.String);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, cityid, startDate))
            {
                dt.Load(idr);
            }
            return dt;
        }
        #endregion

        #region EDM Log
        public EdmOpenLog GetEdmLogByEmail(int id)
        {
            EdmOpenLog edmmain =
                new Select().From<EdmOpenLog>().Where(EdmOpenLog.Columns.Id)
                    .IsEqualTo(id).ExecuteSingle<EdmOpenLog>();
            return edmmain;
        }

        public void SaveEdmOprnLog(EdmOpenLog edmOpenLog)
        {
            DB.Save<EdmOpenLog>(edmOpenLog);
        }

        #endregion

        #region NewCpa

        public void CpaMainSet(CpaMain main)
        {
            DB.Save<CpaMain>(main);
        }

        public void CpaDetailSet(CpaDetail detail)
        {
            DB.Save<CpaDetail>(detail);
        }

        public CpaMain CpaMainGetById(int id)
        {
            return DB.SelectAllColumnsFrom<CpaMain>().Where(CpaMain.Columns.Id).IsEqualTo(id)
                     .ExecuteSingle<CpaMain>();
        }

        public CpaMainCollection CpaMainGetListByDate(DateTime startdate, DateTime enddate)
        {
            return DB.SelectAllColumnsFrom<CpaMain>().Where(CpaMain.Columns.StartDate).IsGreaterThanOrEqualTo(startdate)
                          .And(CpaMain.Columns.EndDate).IsLessThanOrEqualTo(enddate).ExecuteAsCollection<CpaMainCollection>();
        }

        public CpaMainCollection CpaMainGetListByCodeName(string codename)
        {
            return DB.SelectAllColumnsFrom<CpaMain>().Where(CpaMain.Columns.CpaCode).Like("%" + codename + "%")
                       .Or(CpaMain.Columns.CpaName).Like("%" + codename + "%").ExecuteAsCollection<CpaMainCollection>();
        }

        public CpaMainCollection CpaMainGetListByCurrent(DateTime now)
        {
            return DB.SelectAllColumnsFrom<CpaMain>().NoLock().Where(CpaMain.Columns.EndDate).IsGreaterThanOrEqualTo(now)
                       .And(CpaMain.Columns.StartDate).IsLessThan(now).And(CpaMain.Columns.Enabled).IsEqualTo(true).ExecuteAsCollection<CpaMainCollection>();
        }

        #endregion NewCpa

        #region ChangeLog

        public int ChangeLogInsert(string tableName, string keyValue, string content, bool enabled = true)
        {
            ChangeLog changeLog = new ChangeLog();
            changeLog.ChangeObject = tableName;
            changeLog.KeyVal = keyValue;
            changeLog.Content = content;
            changeLog.ModifyUser = HttpContext.Current == null ? "sys" : HttpContext.Current.User.Identity.Name;
            changeLog.ModifyTime = DateTime.Now;
            changeLog.Enabled = enabled;
            return DB.Save(changeLog);
        }

        public ChangeLog ChangeLogGetLatest(string change_object_name, string key_val, bool enabled)
        {
            ChangeLog changelog = new ChangeLog();
            string sql = "select top 1 * from " + ChangeLog.Schema.TableName + " where " + ChangeLog.Columns.ChangeObject + " =@changeobject and " + ChangeLog.Columns.KeyVal +
                " =@keyval and " + ChangeLog.Columns.Enabled + "=@enabled order by " + ChangeLog.IdColumn + " desc";
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@changeobject", change_object_name, DbType.String);
            qc.AddParameter("@keyval", key_val, DbType.String);
            qc.AddParameter("@enabled", enabled, DbType.Boolean);
            changelog.LoadAndCloseReader(DataService.GetReader(qc));
            return changelog;
        }

        public void ChangeLogUpdateEnabled(string change_object_name, string key_val, bool enabled)
        {
            DB.Update<ChangeLog>().Set(ChangeLog.Columns.Enabled).EqualTo(enabled)
                .Where(ChangeLog.Columns.ChangeObject).IsEqualTo(change_object_name)
                .And(ChangeLog.Columns.KeyVal).IsEqualTo(key_val).Execute();
        }

        public ChangeLogCollection ChangeLogCollectionGetAll(string change_object_name, string key_val)
        {
            ChangeLogCollection changelogs = new ChangeLogCollection();
            string sql = "select  * from " + ChangeLog.Schema.TableName + " where " + ChangeLog.Columns.ChangeObject + " =@changeobject and " + ChangeLog.Columns.KeyVal +
                " =@keyval  order by " + ChangeLog.IdColumn + " desc";
            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@changeobject", change_object_name, DbType.String);
            qc.AddParameter("@keyval", key_val, DbType.String);

            changelogs.LoadAndCloseReader(DataService.GetReader(qc));
            return changelogs;
        }

        #endregion ChangeLog

        #region hami_category

        public HamiCategoryCollection HamiCategoryGetList()
        {
            return DB.SelectAllColumnsFrom<HamiCategory>().ExecuteAsCollection<HamiCategoryCollection>();
        }

        #endregion hami_category

        #region vacation

        public void SaveVacation(Vacation vacation)
        {
            DB.Save<Vacation>(vacation);
        }

        public void DeleteVacation(int id)
        {
            DB.Delete<Vacation>(Vacation.Columns.Id, id);
        }

        public Vacation GetTodayVacation()
        {
            const string sql = "select * from vacation where DATEDIFF(DAY, holiday, GETDATE()) = 0";
            var qc = new QueryCommand(sql, Vacation.Schema.Provider.Name);
            var data = new Vacation();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public Vacation GetVacationByDate(DateTime date)
        {
            return DB.SelectAllColumnsFrom<Vacation>().Where(Vacation.Columns.Holiday).IsEqualTo(date).ExecuteSingle<Vacation>();
        }

        public VacationCollection GetVacationListByPeriod(DateTime dateTimeStart, DateTime dateTimeEnd)
        {
            VacationCollection items = new Select().From<Vacation>()
                   .Where(Vacation.Columns.Holiday).IsBetweenAnd(dateTimeStart, dateTimeEnd)
                   .OrderAsc(Vacation.Columns.Holiday)
                   .ExecuteAsCollection<VacationCollection>();
            return items;
        }

        #endregion vacation

        #region ViewPponExpiration

        public ViewPponExpirationCollection ViewPponExpirationGetList(Guid bid)
        {
            string sql = string.Format(@"
				SELECT *
				FROM {0} with (nolock)
				WHERE {1} = @bid"
                , ViewPponExpiration.Schema.TableName
                , ViewPponExpiration.Columns.Bid);

            QueryCommand qc = new QueryCommand(sql, ViewPponExpiration.Schema.Provider.Name);
            qc.AddParameter("bid", bid, DbType.Guid);
            ViewPponExpirationCollection vpe = new ViewPponExpirationCollection();
            vpe.LoadAndCloseReader(DataService.GetReader(qc));
            return vpe;
        }

        public ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime, int endBuffer = 0)
        {
            if (usageTime == DateTime.MinValue)
            {
                return new ViewPponExpirationCollection();
            }

            /*
             * ... WHERE datetimeColumn + endBuffer >= @usageTime  ...
             * there are values '9999-12-31 23:59:59.997' inside our database,
             * causing overflow exception when adding more days into it.
             */

            string sql = string.Format(@"
				SELECT *
				FROM {0} with (nolock)
				WHERE (
					{1} >= @usageTime {7}
					OR {2}  >= @usageTime {7}
					OR {3}  >= @usageTime {7}
					OR {4}  >= @usageTime {7}
					OR {5}  >= @usageTime {7}
				) AND {6} < @usageTime
                AND {8} = {9}"
                , ViewPponExpiration.Schema.TableName
                , ViewPponExpiration.Columns.BhDeliverTimeEnd
                , ViewPponExpiration.Columns.BhChangedExpireDate
                , ViewPponExpiration.Columns.StoreChangedExpireDate
                , ViewPponExpiration.Columns.SellerCloseDownDate
                , ViewPponExpiration.Columns.StoreCloseDownDate
                , ViewPponExpiration.Columns.BhDeliverTimeStart
                , endBuffer == 0 ? string.Empty : string.Format(" - {0} ", endBuffer)
                , ViewPponExpiration.Columns.DeliveryType
                , (int)DeliveryType.ToShop
                );

            QueryCommand qc = new QueryCommand(sql, ViewPponExpiration.Schema.Provider.Name);
            qc.AddParameter("usageTime", usageTime, DbType.DateTime);

            ViewPponExpirationCollection result = new ViewPponExpirationCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime)
        {
            if (usageTime == DateTime.MinValue)
            {
                return new ViewPponExpirationCollection();
            }

            string sql = string.Format(@"
				SELECT *
				FROM {0} with (nolock)
				WHERE
				    {1}  < @usageTime  
				    AND (({2}  >= @usageTime - 3 
				    AND {2} < @usageTime)
                    OR ({3} > @usageTime - 3 
				    AND {3} < @usageTime))
                    AND {4} = {5}
                    AND {6} IN ({7}, {8})
	                AND {9} Not IN (
                        SELECT {10} 
                        FROM {11}
                        WHERE {12} = {13})
                    AND {14} > '2014-08-01'"
                , ViewPponExpiration.Schema.TableName
                , ViewPponExpiration.Columns.BhDeliverTimeStart
                , ViewPponExpiration.Columns.FinalBalanceSheetDate
                , ViewPponExpiration.Columns.BalanceSheetCreateDate
                , ViewPponExpiration.Columns.DeliveryType
                , (int)DeliveryType.ToHouse
                , ViewPponExpiration.Columns.RemittanceType
                , (int)RemittanceType.Others
                , (int)RemittanceType.ManualPartially
                , ViewPponExpiration.Columns.Bid
                , BalanceSheet.Columns.ProductGuid
                , BalanceSheet.Schema.TableName
                , BalanceSheet.Columns.GenerationFrequency
                , (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet
                , ViewPponExpiration.Columns.BhOrderTimeStart
                );

            QueryCommand qc = new QueryCommand(sql, ViewPponExpiration.Schema.Provider.Name);
            qc.AddParameter("usageTime", usageTime, DbType.DateTime);

            ViewPponExpirationCollection result = new ViewPponExpirationCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewPponExpirationCollection ViewPponExpirationGetList(DateTime usageTime, int deliveryBuffer, int endBuffer)
        {
            if (usageTime == DateTime.MinValue)
            {
                return new ViewPponExpirationCollection();
            }
            //撈取配送完成後六個月內完成退換貨檔次之宅配檔次 以產出對帳單 超出期限之檔次由財務人工產出對帳單
            //
            string sql = string.Format(@"
				SELECT bh.GUID                         AS bid,
                       bh.seller_GUID                  AS seller_guid,
                       NULL                            AS store_guid,
                       bh.business_hour_deliver_time_s AS bh_deliver_time_start,
                       bh.business_hour_deliver_time_e AS bh_deliver_time_end,
                       bh.changed_expire_date          AS bh_changed_expire_date,
                       NULL                            AS store_changed_expire_date,
                       s.close_down_date               AS seller_close_down_date,
                       NULL                            AS store_close_down_date,
                       bh.business_hour_order_time_s   AS bh_order_time_start,
                       bh.business_hour_order_time_e   AS bh_order_time_end,
                       da.vendor_billing_model,
                       da.remittance_type,
                       da.final_balance_sheet_date,
                       da.balance_sheet_create_date,
                       dp.delivery_type
                FROM {0} as bh with(nolock)
                INNER JOIN {1} as s with(nolock) on s.GUID = bh.seller_GUID
                INNER JOIN {2} as god with(nolock) on god.business_hour_guid = bh.GUID
                INNER JOIN {3} as da with(nolock) on da.business_hour_guid = bh.GUID
                INNER JOIN {4} as dp with(nolock) on dp.business_hour_guid = bh.GUID
                LEFT JOIN (SELECT business_hour_guid, Count(*) AS vpocount
                            FROM   {10} WITH(NOLOCK)
                            WHERE  balance_sheet_id IS NULL
                            GROUP  BY business_hour_guid) AS vpo
                        ON vpo.business_hour_guid = bh.GUID
                LEFT JOIN (SELECT business_hour_guid, Count(*) AS vpccount
                            FROM   {11} WITH(NOLOCK)
                            WHERE  balance_sheet_id IS NULL
                            GROUP  BY business_hour_guid) AS vpc
                        ON vpc.business_hour_guid = bh.GUID
                WHERE  bh.business_hour_deliver_time_s < @usageTime
				AND coalesce(da.shipped_date, bh.business_hour_deliver_time_e) >= DATEADD(MONTH, -6, @usageTime)
                AND bh.business_hour_status & {5} = 0
                AND dp.delivery_type = {6}
                AND (da.final_balance_sheet_date is null OR da.final_balance_sheet_date >= @usageTime {9})
                AND da.vendor_billing_model = {7}
                AND (
                        ((bh.business_hour_order_time_e < GETDATE() AND god.slug >= bh.business_hour_order_minimum) or vpocount is not null or vpccount is not null )
                        OR 
                        (bh.business_hour_order_time_e > GETDATE() AND god.slug is null)
                    )
                AND da.remittance_type in ({8})"
                , BusinessHour.Schema.TableName
                , Seller.Schema.TableName
                , GroupOrder.Schema.TableName
                , DealAccounting.Schema.TableName
                , DealProperty.Schema.TableName
                , (int)BusinessHourStatus.ComboDealMain
                , (int)DeliveryType.ToHouse
                , (int)VendorBillingModel.BalanceSheetSystem
                , string.Format("{0},{1},{2},{3}", (int)RemittanceType.Flexible, (int)RemittanceType.Weekly, (int)RemittanceType.Monthly, (int)RemittanceType.Fortnightly)
                , endBuffer == 0 ? string.Empty : string.Format(" - {0} ", endBuffer)
                , VendorPaymentOverdue.Schema.TableName
                , VendorPaymentChange.Schema.TableName);
            //測試某檔可直皆篩選
            //and bh.guid='D8E6F060-2095-4142-AC8F-69584B97AC05'
            DataTable dt = new DataTable();
            QueryCommand qc = new QueryCommand(sql, ViewPponExpiration.Schema.Provider.Name);
            qc.AddParameter("usageTime", usageTime, DbType.DateTime);

            using (IDataReader reader = DataService.GetReader(qc))
            {
                dt.Load(reader);
            }

            ViewPponExpirationCollection result = new ViewPponExpirationCollection();
            foreach (var r in dt.AsEnumerable())
            {
                var row = new ViewPponExpiration
                {
                    Bid = r.Field<Guid>("bid"),
                    SellerGuid = r.Field<Guid>("seller_guid"),
                    StoreGuid = r.Field<Guid?>("store_guid"),
                    BhDeliverTimeStart = r.Field<DateTime>("bh_deliver_time_start"),
                    BhDeliverTimeEnd = r.Field<DateTime>("bh_deliver_time_end"),
                    BhChangedExpireDate = r.Field<DateTime?>("bh_changed_expire_date"),
                    StoreChangedExpireDate = r.Field<DateTime?>("store_changed_expire_date"),
                    SellerCloseDownDate = r.Field<DateTime?>("seller_close_down_date"),
                    StoreCloseDownDate = r.Field<DateTime?>("store_close_down_date"),
                    BhOrderTimeStart = r.Field<DateTime>("bh_order_time_start"),
                    BhOrderTimeEnd = r.Field<DateTime>("bh_order_time_end"),
                    VendorBillingModel = r.Field<int>("vendor_billing_model"),
                    RemittanceType = r.Field<int>("remittance_type"),
                    FinalBalanceSheetDate = r.Field<DateTime?>("final_balance_sheet_date"),
                    BalanceSheetCreateDate = r.Field<DateTime?>("balance_sheet_create_date"),
                    DeliveryType = r.Field<int>("delivery_type")
                };
                result.Add(row);
            }

            return result;
        }

        #endregion ViewPponExpiration

        #region ViewDealPropertyBusinessHourContent

        public ViewDealPropertyBusinessHourContent ViewDealPropertyBusinessHourContentGetByBid(Guid bid)
        {
            string strSQL = " select * from " +
                            ViewDealPropertyBusinessHourContent.Schema.Provider.DelimitDbName(
                                ViewDealPropertyBusinessHourContent.Schema.TableName) +
                            " with(nolock) where " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourGuid +
                            " = @guid ";

            QueryCommand qc = new QueryCommand(strSQL, string.Empty);
            qc.AddParameter("guid", bid, DbType.Guid);

            ViewDealPropertyBusinessHourContent rtn = new ViewDealPropertyBusinessHourContent();
            rtn.LoadAndCloseReader(DataService.GetReader(qc));
            return rtn;
        }

        public ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetList(IEnumerable<Guid> bids)
        {
            ViewDealPropertyBusinessHourContentCollection infos = new ViewDealPropertyBusinessHourContentCollection();
            List<Guid> tempIds = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewDealPropertyBusinessHourContent>()
                        .Where(ViewDealPropertyBusinessHourContent.Columns.BusinessHourGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewDealPropertyBusinessHourContentCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetListPaging(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewDealPropertyBusinessHourContent.Columns.BusinessHourGuid;
            QueryCommand qc = GetVpolWhereQC(ViewDealPropertyBusinessHourContent.Schema, filter);
            qc.CommandSql = "select * from " + ViewDealPropertyBusinessHourContent.Schema.Provider.DelimitDbName(ViewDealPropertyBusinessHourContent
                .Schema.TableName) + " with(nolock)" + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }

            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewDealPropertyBusinessHourContentCollection vdpbCol = new ViewDealPropertyBusinessHourContentCollection();
            vdpbCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vdpbCol;
        }

        public ViewDealPropertyBusinessHourContentCollection ViewDealPropertyBusinessHourContentGetToHouseDealList(string queryOption, string queryKeyword,
            string queryDateOption, DateTime? queryStartTime, DateTime? queryEndTime,
            RemittanceType? remittanceType, IEnumerable<string> columnNull, bool hasVendorPaymentChange)
        {
            string sql = "SELECT * FROM " + ViewDealPropertyBusinessHourContent.Schema.TableName + " WITH(NOLOCK) " +
                         "WHERE " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourOrderTimeS + " < GETDATE() " +
                         "AND " + ViewDealPropertyBusinessHourContent.Columns.DeliveryType + " = " + (int)DeliveryType.ToHouse;

            if (!string.IsNullOrEmpty(queryOption))
            {
                if (!string.IsNullOrEmpty(queryKeyword))
                {
                    switch (queryOption)
                    {
                        case "UniqueId":
                            int id;
                            int.TryParse(queryKeyword, out id);
                            var bid = DealPropertyGet(id).BusinessHourGuid;
                            //若為多檔次可以母檔unique_id搜尋到子檔
                            var productIds = GetViewComboDealAllByBid(bid)
                                                .Select(x => x.UniqueId)
                                                .ToList();
                            if (productIds.Any())
                            {
                                sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.UniqueId + " IN (" + string.Join(",", productIds) + ")";
                            }
                            else
                            {
                                sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.UniqueId + " = " + queryKeyword;
                            }
                            break;
                        case "DealName":
                            sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.CouponUsage + " LIKE N'%" + queryKeyword + "%'";
                            break;
                        case "SignCompanyName":
                            sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.CompanyName + " LIKE N'%" + queryKeyword + "%'";
                            break;
                        case "SalesName":
                            sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.DeEmpName + " LIKE N'%" + queryKeyword + "%'";
                            break;
                    }
                }
            }

            if (!string.IsNullOrEmpty(queryDateOption))
            {
                var queryDateColumn = string.Empty;
                switch (queryDateOption)
                {
                    case "DealCloseDate":
                        queryDateColumn = ViewDealPropertyBusinessHourContent.Columns.BusinessHourOrderTimeE;
                        break;
                    case "ShippedDate":
                        queryDateColumn = ViewDealPropertyBusinessHourContent.Columns.MainDealShippedDate;
                        break;
                    case "PartiallyPaymentDate":
                        queryDateColumn = ViewDealPropertyBusinessHourContent.Columns.PartiallyPaymentDate;
                        break;
                    case "BalanceSheetCreateDate":
                        queryDateColumn = ViewDealPropertyBusinessHourContent.Columns.BalanceSheetCreateDate;
                        break;
                    case "BillGetDate":
                        queryDateColumn = ViewDealPropertyBusinessHourContent.Columns.FinanceGetDate;
                        break;
                }
                if (queryStartTime != null && queryEndTime != null)
                {
                    if (queryDateColumn == ViewDealPropertyBusinessHourContent.Columns.MainDealShippedDate)
                    {
                        sql += " AND (" + queryDateColumn + " <= @queryEndTime" +
                               " AND " + queryDateColumn + " >= @queryStartTime" +
                               " OR (" + queryDateColumn + " IS NULL" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealMain + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealSub + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " <= @queryEndTime" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " >= @queryStartTime" +
                               " ))";
                    }
                    else
                    {
                        sql += " AND " + queryDateColumn + " <= @queryEndTime" +
                               " AND " + queryDateColumn + " >= @queryStartTime";
                    }
                }
                else if (queryStartTime != null)
                {
                    if (queryDateColumn == ViewDealPropertyBusinessHourContent.Columns.MainDealShippedDate)
                    {
                        sql += " AND " + queryDateColumn + " >= @queryStartTime" +
                               " OR (" + queryDateColumn + " IS NULL" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealMain + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealSub + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " >= @queryStartTime" +
                               " )";
                    }
                    else
                    {
                        sql += " AND " + queryDateColumn + " >= @queryStartTime";
                    }
                }
                else if (queryEndTime != null)
                {
                    if (queryDateColumn == ViewDealPropertyBusinessHourContent.Columns.MainDealShippedDate)
                    {
                        sql += " AND " + queryDateColumn + " <= @queryEndTime" +
                               " OR (" + queryDateColumn + " IS NULL" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealMain + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.BusinessHourStatus + " & " + (int)BusinessHourStatus.ComboDealSub + " = 0" +
                               " AND " + ViewDealPropertyBusinessHourContent.Columns.ShippedDate + " <= @queryEndTime" +
                               " )";
                    }
                    else
                    {
                        sql += " AND " + queryDateColumn + " <= @queryEndTime";
                    }
                }
            }

            if (remittanceType.HasValue)
            {
                sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.RemittanceType + " = " +
                       (int)remittanceType.Value;
            }
            else
            {
                sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.RemittanceType + " IN (" +
                    string.Join(",", new List<int> { (int)RemittanceType.ManualPartially, (int)RemittanceType.Others, (int)RemittanceType.Flexible, (int)RemittanceType.Monthly, (int)RemittanceType.Weekly, (int)RemittanceType.Fortnightly }) + ")";
            }

            if (columnNull.Any())
            {
                foreach (var column in columnNull)
                {
                    sql += " AND " + column + " IS NULL";
                    if (column == ViewDealPropertyBusinessHourContent.Columns.BalanceSheetCreateDate)
                    {
                        sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.RemittanceType + " NOT IN (" +
                            string.Join(",", new List<int> { (int)RemittanceType.Flexible, (int)RemittanceType.Monthly, (int)RemittanceType.Weekly, (int)RemittanceType.Fortnightly }) + ")";
                    }
                }
            }

            if (hasVendorPaymentChange)
            {
                sql += " AND " + ViewDealPropertyBusinessHourContent.Columns.HasVendorPaymentChange + " = 1";
            }

            QueryCommand qc = new QueryCommand(sql, string.Empty);
            if (queryStartTime != null)
            {
                qc.AddParameter("@queryStartTime", queryStartTime, DbType.DateTime);
            }
            if (queryEndTime != null)
            {
                qc.AddParameter("@queryEndTime", queryEndTime, DbType.DateTime);
            }

            var rtn = new ViewDealPropertyBusinessHourContentCollection();
            rtn.LoadAndCloseReader(DataService.GetReader(qc));
            return rtn;
        }

        #endregion ViewDealPropertyBusinessHourContent

        #region FamiPort

        public void FamiportUpdate(Famiport fp)
        {
            fp.LatestUpdateTime = System.DateTime.Now;
            DB.Update<Famiport>(fp);
        }

        public void FamiportUpdateEventId(int famiportId, PeztempServiceCode serviceCode)
        {
            string eventIdHead = Convert.ToString((int)serviceCode).PadLeft(3, '0') + System.DateTime.Now.Year.ToString().Substring(2, 2);

            if (System.DateTime.Now.Month < 10)
            {
                eventIdHead += System.DateTime.Now.Month.ToString();
            }
            else if (System.DateTime.Now.Month == 10)
            {
                eventIdHead += "A";
            }
            else if (System.DateTime.Now.Month == 11)
            {
                eventIdHead += "B";
            }
            else if (System.DateTime.Now.Month == 12)
            {
                eventIdHead += "C";
            }

            string sql = string.Format(@"Declare @eventId varchar(10);
                            Select @eventId = '{0}' + Right('0000' + Convert(varchar, COUNT(1)) , 4) From dbo.famiport where is_out_of_date = 0
                            and Year(create_time) = @Year and MONTH(create_time) = @Month;

                            Update dbo.famiport Set event_id = @eventId
                            where Id = @famiportId AND NOT EXISTS (SELECT 1 FROM famiport WHERE event_id = @eventId)", eventIdHead);
            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@famiportId", famiportId, DbType.Int32);
            qc.AddParameter("@Year", System.DateTime.Now.Year, DbType.Int32);
            qc.AddParameter("@Month", System.DateTime.Now.Month, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public int FamiportUpdateBid(int famiportId, Guid guid)
        {
            return DB.Update<Famiport>().Set(Famiport.Columns.BusinessHourGuid).EqualTo(guid.ToString()).Where(Famiport.Columns.Id).IsEqualTo(famiportId).Execute();
        }

        public void FamiportSet(Famiport fp)
        {
            DB.Save<Famiport>(fp);
        }

        public Famiport FamiportGet(int id)
        {
            return DB.Get<Famiport>(id);
        }

        public void FamiportDelete(int id)
        {
            DB.Delete<Famiport>(Famiport.Columns.Id, id);
        }

        public Famiport FamiportGet(string colName, object obj_value)
        {
            return DB.Get<Famiport>(colName, obj_value);
        }

        public FamiportCollection FamiportGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter)
        {
            string defOrderBy = Famiport.Columns.Id;
            QueryCommand qc = SSHelper.GetWhereQC<Famiport>(filter);
            qc.CommandSql = "select * from " + Famiport.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderByDesc))
            {
                qc.CommandSql += " order by " + orderByDesc + " desc";
                defOrderBy = orderByDesc + " desc";
            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            FamiportCollection view = new FamiportCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public FamiportCollection FamiportGetListByDate(DateTime date)
        {
            FamiportCollection view = new FamiportCollection();
            string sql = "select * from " + Famiport.Schema.TableName + " fp with(nolock)" +
                " left join " + BusinessHour.Schema.TableName + " bh with(nolock)" +
                " on fp." + Famiport.Columns.BusinessHourGuid + "=bh." + BusinessHour.Columns.Guid +
                " where bh." + BusinessHour.Columns.BusinessHourOrderTimeS + "<=@date" +
                " and bh." + BusinessHour.Columns.BusinessHourOrderTimeE + ">=@date" +
                " order by " + Famiport.IdColumn;
            QueryCommand qc = new QueryCommand(sql, Famiport.Schema.Provider.Name);
            qc.AddParameter("@date", date, DbType.DateTime);
            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public FamiportCollection FamiportGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = Famiport.Columns.Id;
            QueryCommand qc = SSHelper.GetWhereQC<Famiport>(filter);
            qc.CommandSql = "select * from " + Famiport.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            FamiportCollection view = new FamiportCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        public int FamiportGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<Famiport>(filter);
            qc.CommandSql = "select count(1) from " + Famiport.Schema.Provider.DelimitDbName(Famiport.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion FamiPort

        #region ExternalDeal
        public void ExternalDealSet(ExternalDeal deal)
        {
            DB.Save<ExternalDeal>(deal);
        }
        public void ExternalDealCollectionSet(ExternalDealCollection deals)
        {
            DB.SaveAll(deals);
        }
        public ExternalDeal ExternalDealGet(Guid guid)
        {
            return DB.SelectAllColumnsFrom<ExternalDeal>().Where(ExternalDeal.Columns.Guid).IsEqualTo(guid).ExecuteSingle<ExternalDeal>();
        }

        public ExternalDeal ExternalDealGet(string skuid)
        {
            return DB.SelectAllColumnsFrom<ExternalDeal>().Where(ExternalDeal.Columns.Skuid).IsEqualTo(skuid).ExecuteAsCollection<ExternalDealCollection>().OrderByDescending(o=>o.CreateTime).FirstOrDefault();
        }

        public ViewExternalDealCollection ViewExternalDealGetListBySkuId (string skuid)
        {
            return DB.SelectAllColumnsFrom<ViewExternalDeal>().Where(ViewExternalDeal.Columns.Skuid).IsEqualTo(skuid).ExecuteAsCollection<ViewExternalDealCollection>();
        }

        public ExternalDealCollection ExternalDealGetList(int pageStart, int pageLength, string orderByDesc, params string[] filter)
        {
            return DB.SelectAllColumnsFrom<ExternalDeal>()
             //.Where(MerchantEventPromo.Columns.Type).IsEqualTo((int)type)
             .ExecuteAsCollection<ExternalDealCollection>();
        }

        public ExternalDealCollection ExternalDealGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            ExternalDealCollection edc = new ExternalDealCollection();
            return edc;
        }
        public ExternalDealCollection ExternalDealGetListByDate(DateTime date)
        {
            ExternalDealCollection edc = new ExternalDealCollection();
            return edc;
        }
        public ExternalDealRelationStoreCollection ExternalDealRelationStoreGetListByDeal(Guid guid)
        {
            return DB.SelectAllColumnsFrom<ExternalDealRelationStore>()
                .Where(ExternalDealRelationStore.Columns.DealGuid).IsEqualTo(guid)
                .ExecuteAsCollection<ExternalDealRelationStoreCollection>();
        }
        public ViewExternalDealCollection ViewExternalDealGetListBySellerGuid(Guid parentSellerGuid, int? shopCode, string searchTitle)
        {
            return shopCode != null ? DB.SelectAllColumnsFrom<ViewExternalDeal>()
                .Where(ViewExternalDeal.Columns.ParentSellerGuid).IsEqualTo(parentSellerGuid)
                .And(ViewExternalDeal.Columns.ShopCode).IsEqualTo(shopCode)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .And(ViewExternalDeal.Columns.Title).ContainsString(searchTitle)
                .ExecuteAsCollection<ViewExternalDealCollection>() : DB.SelectAllColumnsFrom<ViewExternalDeal>()
                .Where(ViewExternalDeal.Columns.ParentSellerGuid).IsEqualTo(parentSellerGuid)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .And(ViewExternalDeal.Columns.Title).ContainsString(searchTitle)
                .ExecuteAsCollection<ViewExternalDealCollection>();
        }

        public ViewExternalDealCollection ViewExternalDealGetListByBid(List<Guid> bids)
        {
            List<Core.Models.Entities.ExternalDealCombo> comboList = new List<Core.Models.Entities.ExternalDealCombo>();
            ViewExternalDealCollection vedColDB = new ViewExternalDealCollection();
            for (int i = 0; i < bids.Count; i += 2000)
            {
                comboList.AddRange(skep.GetExternalDealComboByMainBids(bids.Skip(i).Take(2000).ToList()));
                vedColDB.AddRange(DB.SelectAllColumnsFrom<ViewExternalDeal>().NoLock()
                    .Where(ViewExternalDeal.Columns.Bid).In(bids.Skip(i).Take(2000))
                    .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                    .ExecuteAsCollection<ViewExternalDealCollection>());
            }

            Dictionary<Guid, Core.Models.Entities.ExternalDealCombo> comboDict = new Dictionary<Guid, Core.Models.Entities.ExternalDealCombo>();
            foreach (var combo in comboList)
            {
                if (combo.Bid == null)
                {
                    continue;
                }
                if (comboDict.ContainsKey(combo.Bid.Value) == false)
                {
                    comboDict.Add(combo.Bid.Value, combo);
                }
            }

            Dictionary<Guid, List<ViewExternalDeal>> vedDict =
                vedColDB.GroupBy(t => (Guid)t.Bid).ToDictionary(t => t.Key, t => t.ToList());

            var resultVedCol = new ViewExternalDealCollection();
            foreach (var bid in bids)
            {
                string shopCode = string.Empty;
                Core.Models.Entities.ExternalDealCombo combo = null;
                if (comboDict.ContainsKey(bid))
                {
                    combo = comboDict[bid];
                }
                if (combo != null && combo.MainBid.HasValue)
                {
                    shopCode = combo.ShopCode;
                }
                if (vedDict.ContainsKey(bid))
                {
                    var data = string.IsNullOrEmpty(shopCode) ? vedDict[bid].FirstOrDefault()
                        : vedDict[bid].FirstOrDefault(x => x.ShopCode == shopCode);
                    resultVedCol.Add(data ?? new ViewExternalDeal());
                }
            }

            return resultVedCol;
        }

        public ViewExternalDealCollection ViewExternalDealGetListByPrizeDeal(string searchTitle)
        {
            return DB.SelectAllColumnsFrom<ViewExternalDeal>().NoLock()
                .Where(ViewExternalDeal.Columns.IsPrizeDeal).IsEqualTo(true)
                .And(ViewExternalDeal.Columns.IsHqDeal).IsEqualTo(true)
                .And(ViewExternalDeal.Columns.Title).ContainsString(searchTitle)
                .OrderDesc(ViewExternalDeal.Columns.CreateTime)
                .ExecuteAsCollection<ViewExternalDealCollection>();
        }

        public ViewExternalDealCollection ViewExternalDealGetList(string storeCode)
        {

            var cols = DB.SelectAllColumnsFrom<ViewExternalDeal>()
              .Where(ViewExternalDeal.Columns.ShopCode).IsEqualTo(storeCode)
              .ExecuteAsCollection<ViewExternalDealCollection>();
            return cols;

        }
        public ViewExternalDealCollection ViewExternalDealGetListByDealrGuid(Guid dealGuid)
        {
            return DB.SelectAllColumnsFrom<ViewExternalDeal>()
                .Where(ViewExternalDeal.Columns.Guid).IsEqualTo(dealGuid)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteAsCollection<ViewExternalDealCollection>();
        }

        public ViewExternalDeal ViewExternalDealGetListByDealGuid(Guid dealGuid, Guid sellerGuid)
        {
            var data = DB.SelectAllColumnsFrom<ViewExternalDeal>()
                .Where(ViewExternalDeal.Columns.Guid).IsEqualTo(dealGuid)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteAsCollection<ViewExternalDealCollection>().ToList();

            return data.FirstOrDefault(x => x.SellerGuid == sellerGuid || x.ParentSellerGuid == sellerGuid);
        }

        public ViewExternalDeal ViewExternalDealGetByBid(Guid bid, bool isAvailable = true)
        {
            Guid selectBid = bid;
            string shopCode = string.Empty;
            ViewExternalDealCollection vedCol;
            var combo = skep.GetExternalDealComboByBid(bid);
            if (combo != null && combo.MainBid.HasValue)
            {
                selectBid = combo.MainBid.Value;
                shopCode = combo.ShopCode;
            }
            if (isAvailable == false)
            {
                vedCol = DB.SelectAllColumnsFrom<ViewExternalDeal>().NoLock()
                .Where(ViewExternalDeal.Columns.Bid).IsEqualTo(selectBid)
                .ExecuteAsCollection<ViewExternalDealCollection>();
            }
            else
            {
                vedCol = DB.SelectAllColumnsFrom<ViewExternalDeal>().NoLock()
                .Where(ViewExternalDeal.Columns.Bid).IsEqualTo(selectBid)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteAsCollection<ViewExternalDealCollection>();
            }

            if (vedCol.Any())
            {
                var data = string.IsNullOrEmpty(shopCode) ? vedCol.FirstOrDefault() : vedCol.FirstOrDefault(x => x.ShopCode == shopCode);
                return data ?? new ViewExternalDeal();
            }

            return new ViewExternalDeal();
        }
        public ViewExternalDealCollection ViewExternalDealGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<ViewExternalDeal>()
                .Where(ViewExternalDeal.Columns.Bid).IsEqualTo(bid)
                .And(ViewExternalDeal.Columns.IsAvailable).IsEqualTo(true)
                .ExecuteAsCollection<ViewExternalDealCollection>();
        }

        public ViewExternalDealCollection ViewExternalDealGetListByNotExpired()
        {
            string sql = @"
select deal.* from view_external_deal deal
where 
	deal.bid is not null 
    --資料為快取用，故多撈20分鍾後的上檔資料
	and DATEADD(MINUTE, 20, GETDATE()) > deal.business_hour_order_time_s
	and GETDATE() < deal.business_hour_order_time_e
order by deal.business_hour_order_time_s";
            QueryCommand qc = new QueryCommand(sql, ViewExternalDeal.Schema.Provider.Name);
            ViewExternalDealCollection result = new ViewExternalDealCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public void ExternalDealRelationStoreDeleteByDeal(Guid guid)
        {
            DB.Delete<ExternalDealRelationStore>(ExternalDealRelationStore.Columns.DealGuid, guid);
        }
        public void ExternalDealRelationStoreDeleteByStores(ExternalDealRelationStoreCollection stores)
        {
            stores.ForEach(x => DB.Delete<ExternalDealRelationStore>(x));
        }
        public void ExternalDealRelationStoreCollectionSet(ExternalDealRelationStoreCollection stores)
        {
            DB.SaveAll(stores);
        }
        public ExternalDealRelationItemCollection ExternalDealItemGetByBid(Guid bid)
        {
            string sql = @"SELECT *
              FROM [dbo].[external_deal_relation_item] with(nolock)
              where external_guid in (SELECT deal_guid
              FROM [dbo].[external_deal_relation_store] with(nolock)
              where bid=@bid)";
            QueryCommand qc = new QueryCommand(sql, ExternalDealRelationItem.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            ExternalDealRelationItemCollection item = new ExternalDealRelationItemCollection();
            item.LoadAndCloseReader(DataService.GetReader(qc));
            return item;
        }
        public ExternalDealRelationItemCollection ExternalDealRelationItemGetListByDealGuid(Guid dealGuid)
        {
            return DB.SelectAllColumnsFrom<ExternalDealRelationItem>()
                .Where(ExternalDealRelationItem.Columns.ExternalGuid).IsEqualTo(dealGuid)
                .ExecuteAsCollection<ExternalDealRelationItemCollection>();
        }
        public void ExternalDealRelationItemDeleteByItems(ExternalDealRelationItemCollection items)
        {
            items.ForEach(x => DB.Delete<ExternalDealRelationItem>(x));
        }
        public void ExternalDealRelationItemCollectionSet(ExternalDealRelationItemCollection items)
        {
            DB.SaveAll(items);
        }

        #endregion

        #region DailyFamiportItemTriggerReport

        public void DailyFamiportItemTriggerReportSet(DailyFamiportItemTriggerReport dailyFamiportItemTriggerReport)
        {
            DB.Save<DailyFamiportItemTriggerReport>(dailyFamiportItemTriggerReport);
        }

        public DailyFamiportItemTriggerReportCollection DailyFamiportItemTriggerReportGetListByDate(bool isPager, int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = DailyFamiportItemTriggerReport.Columns.DailyStartTime;
            QueryCommand qc = SSHelper.GetWhereQC<DailyFamiportItemTriggerReport>(filter);
            qc.CommandSql = "select * from " + DailyFamiportItemTriggerReport.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            if (isPager)
            {
                SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            DailyFamiportItemTriggerReportCollection resultCollection = new DailyFamiportItemTriggerReportCollection();

            resultCollection.LoadAndCloseReader(DataService.GetReader(qc));
            return resultCollection;
        }

        #endregion DailyFamiportItemTriggerReport

        #region DailyBeaconStoreEffectReport

        public void DailyBeaconStoreEffectReportSet(DailyBeaconStoreEffectReport dailyBeaconStoreEffectReport)
        {
            DB.Save<DailyBeaconStoreEffectReport>(dailyBeaconStoreEffectReport);
        }

        public DailyBeaconStoreEffectReportCollection DailyBeaconStoreEffectReportGetListByDate(bool isPager, int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = DailyBeaconStoreEffectReport.Columns.ReportStartDate;
            QueryCommand qc = SSHelper.GetWhereQC<DailyBeaconStoreEffectReport>(filter);
            qc.CommandSql = "select * from " + DailyBeaconStoreEffectReport.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            if (isPager)
            {
                SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            DailyBeaconStoreEffectReportCollection resultCollection = new DailyBeaconStoreEffectReportCollection();

            resultCollection.LoadAndCloseReader(DataService.GetReader(qc));
            return resultCollection;
        }

        #endregion DailyBeaconStoreEffectReport

        #region FamiApiLog

        public void FamiApiLogSet(FamiApiLog apiLog)
        {
            DB.Save(apiLog);
        }

        #endregion

        #region Famipos

        public bool FamiposBarcodeSet(FamiposBarcode fami)
        {
            fami.ModifyDatetime = DateTime.Now;
            DB.Save(fami);
            return true;
        }

        /// <summary>
        /// 依交易序號取回 Famipos Barcode
        /// </summary>
        /// <param name="tranNo"></param>
        /// <returns></returns>
        public FamiposBarcode FamiposBarcodeGet(string tranNo)
        {
            return DB.Get<FamiposBarcode>(tranNo);
        }

        /// <summary>
        /// 依 Peztemp.Id 取回 Famipos Barcode
        /// </summary>
        /// <param name="peztempId"></param>
        /// <returns></returns>
        public FamiposBarcode FamiposBarcodeGet(int peztempId)
        {
            return DB.Get<FamiposBarcode>(FamiposBarcode.Columns.PeztempId, peztempId);
        }

        /// <summary>
        /// 依Peztemp Id取得一筆 Famipos Barcode，取不到則新增一筆
        /// </summary>
        /// <param name="peztempId">Peztemp.Id</param>
        /// <param name="vpd"></param>
        /// <returns></returns>
        public FamiposBarcode FamiposBarcodeGetCreate(string tranNoHeader, Guid orderDetailGuid, Guid businessHourGuid, IViewPponDeal vpd)
        {
            Peztemp peztemp = this.PeztempGet(orderDetailGuid, businessHourGuid);

            if (!peztemp.IsLoaded)
            {
                return new FamiposBarcode();
            }

            FamiposBarcode famipos = DB.SelectAllColumnsFrom<FamiposBarcode>()
                .Where(FamiposBarcode.Columns.PeztempId).IsEqualTo(peztemp.Id)
                .ExecuteAsCollection<FamiposBarcodeCollection>().FirstOrDefault();

            if (famipos != null)
            {
                return famipos;
            }
            else
            {
                famipos = new FamiposBarcode();
                Famiport famiport = this.FamiportGet(Famiport.Columns.BusinessHourGuid, peztemp.Bid);

                if (famiport == null || !famiport.IsLoaded)
                {
                    return new FamiposBarcode();
                }

                string sql = @"Declare @runCnt int, @famiposId int, @executeTime int, @tranNo varchar(11)
                    select @runCnt = 0, @famiposId = 0, @executeTime = 0;
                    while @runCnt < 1 begin
	                    Set @tranNo = @header + Right('0000000' + convert(varchar, Convert(decimal , Round(RAND() * 9999999, 0) + 1)), 7)
	                    INSERT INTO dbo.famipos_barcode (tran_no, famiport_id, peztemp_id)
		                    SELECT @tranNo, @famiportId, @peztempId
		                    WHERE NOT EXISTS (SELECT 1 FROM dbo.famipos_barcode WHERE tran_no = @tranNo)
	                    if(@famiposId != @@IDENTITY) begin
		                    Set @runCnt = @runCnt + 1
		                    SELECT @famiposId = @@IDENTITY
	                    end
	                    Set @executeTime = @executeTime + 1
	                    if(@executeTime > 500) begin
		                    break;
	                    end
                    end
                    select * from dbo.famipos_barcode where id = @@IDENTITY";

                QueryCommand qc = new QueryCommand(sql, FamiposBarcode.Schema.Provider.Name);
                qc.AddParameter("@famiportId", famiport.Id, DbType.Int32);
                qc.AddParameter("@peztempId", peztemp.Id, DbType.Int32);
                qc.AddParameter("@header", tranNoHeader, DbType.String);
                famipos.LoadAndCloseReader(DataService.GetReader(qc));

                if (!famipos.IsLoaded)
                {
                    return famipos;
                }

                #region create 3 barcode

                //兌換期限5碼(西元末一碼)YMMDD+條碼數量+交易序號
                string barcode1 = string.Format(@"{0}3{1}"
                    , ((System.DateTime)vpd.BusinessHourDeliverTimeE).ToString("yyyyMMdd").Substring(3, 5)
                    , famipos.TranNo);

                //兌換種類(4促銷券)+全家商品貨號(7碼)+項次碼(1)+付款金額(5)+MMK服務代號(3)+檢碼(1)
                string barcode2 = string.Format(@"4{0}1{1}{2}"
                    , famiport.ItemCode.PadLeft(7, '0').Substring(0, 7)
                    , ((int)vpd.ItemPrice).ToString().PadLeft(5, '0')
                    , config.FamiPosMMKId.ToString().PadLeft(3, '0'));

                barcode2 += FamiGetCheckValidationCode(barcode1 + barcode2).ToString();

                //famiport.eventid(7)+peztemp.pezcode(10)+檢碼(1)
                //2017.5.4 全家要求第三段條碼第17位元於無紙下(APP)需為1
                string barcode3 = string.Format("{0}0000000001"
                    , (famiport.EventId.Length > 7) ? famiport.EventId.Substring(3, 7) : famiport.EventId.PadLeft(7, '0'));

                barcode3 += FamiGetCheckValidationCode(barcode1 + barcode2 + barcode3).ToString();

                famipos.Barcode1 = string.Format(@"{0}3{1}"
                    , ((System.DateTime)vpd.BusinessHourDeliverTimeE).ToString("yyyyMMdd").Substring(3, 5)
                    , famipos.TranNo);

                famipos.Barcode1 = barcode1;
                famipos.Barcode2 = barcode2;
                famipos.Barcode3 = barcode3;

                DB.Save<FamiposBarcode>(famipos);

                return famipos;

                #endregion create 3 barcode
            }
        }

        private int FamiGetCheckValidationCode(string fullCode)
        {
            int oddSum = 0, evenSum = 0;

            for (int i = 0; i < fullCode.Length; i++)
            {
                if (i % 2 == 0)
                {
                    evenSum += NumberComparison(fullCode.Substring(i, 1));
                }
                else
                {
                    oddSum += NumberComparison(fullCode.Substring(i, 1));
                }
            }

            return (((oddSum % 10) + (evenSum % 10)) % 10);
        }

        private int NumberComparison(string c)
        {
            if (string.IsNullOrEmpty(c))
            {
                return 0;
            }

            c = c.Substring(0, 1);
            int result = 0;

            if (int.TryParse(c, out result))
            {
                return (result < 10) ? result : 0;
            }
            else
            {
                switch (c)
                {
                    case "A":
                        return 10;
                    case "B":
                        return 11;
                    case "C":
                        return 12;
                    case "D":
                        return 13;
                    case "E":
                        return 14;
                    case "F":
                        return 15;
                    case "G":
                        return 16;
                    case "H":
                        return 17;
                    case "I":
                        return 18;
                    case "J":
                        return 19;
                    case "K":
                        return 20;
                    case "L":
                        return 21;
                    case "M":
                        return 22;
                    case "N":
                        return 23;
                    case "O":
                        return 24;
                    case "P":
                        return 25;
                    case "Q":
                        return 26;
                    case "R":
                        return 27;
                    case "S":
                        return 28;
                    case "T":
                        return 29;
                    case "U":
                        return 30;
                    case "V":
                        return 31;
                    default:
                        return 0;
                }
            }
        }

        #endregion Famipos

        #region ViewFamiportPinRollbackLog

        public int ViewFamiportPinRollbackLogCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<ViewFamiportPinRollbackLog>(filter);
            qc.CommandSql = "select count(1) from " + ViewFamiportPinRollbackLog.Schema.Provider.DelimitDbName(ViewFamiportPinRollbackLog.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewFamiportPinRollbackLogCollection ViewFamiportPinRollBackLogGetList(int pageStart, int pageLength, string orderColumn, bool isOrderByASC, params string[] filter)
        {
            string defOrderBy = string.Format("{0} {1}", ViewFamiportPinRollbackLog.Columns.CreateTime, "desc");
            QueryCommand qc = SSHelper.GetWhereQC<ViewFamiportPinRollbackLog>(filter);
            qc.CommandSql = "select * from " + ViewFamiportPinRollbackLog.Schema.TableName + " with(nolock) " + qc.CommandSql;

            if (!string.IsNullOrEmpty(orderColumn))
            {
                if (!isOrderByASC)
                {
                    orderColumn += " desc";
                }

                qc.CommandSql += " order by " + orderColumn;
                defOrderBy = orderColumn;

            }

            SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewFamiportPinRollbackLogCollection view = new ViewFamiportPinRollbackLogCollection();

            view.LoadAndCloseReader(DataService.GetReader(qc));
            return view;
        }

        #endregion

        #region YahooProperty
        public void YahooPropertySet(YahooProperty yp)
        {
            DB.Save<YahooProperty>(yp);
        }

        public int YahooPropertySetList(YahooPropertyCollection yps)
        {
            return DB.SaveAll(yps);
        }

        public YahooProperty YahooPropertyGet(int id)
        {
            return DB.Get<YahooProperty>(id);
        }

        public YahooProperty YahooPropertyGet(string column, object value)
        {
            return DB.SelectAllColumnsFrom<YahooProperty>().Where(column).IsEqualTo(value).ExecuteSingle<YahooProperty>();
        }

        public void YahooPropertyDelete(int id)
        {
            DB.Delete<YahooProperty>(YahooProperty.Columns.Id, id);
        }

        /// <summary>
        /// Yahoo大團購API需要更新的資料(僅異動非No、Remove狀態的資料)
        /// </summary>
        /// <returns></returns>
        public YahooPropertyCollection YahooPropertyGetList()
        {
            YahooPropertyCollection items = new Select().From<YahooProperty>()
                   .Where(YahooProperty.Columns.Action).IsNotEqualTo((int)YahooPropertyAction.Ordinary)
                   .And(YahooProperty.Columns.Action).IsNotEqualTo((int)YahooPropertyAction.Remove)
                   .OrderAsc(YahooProperty.Columns.Id)
                   .ExecuteAsCollection<YahooPropertyCollection>();
            return items;
        }

        /// <summary>
        /// 取得 YahooProperty by bids
        /// </summary>
        /// <returns></returns>
        public YahooPropertyCollection YahooPropertyGetListByBids(List<Guid> bids)
        {
            var data = new YahooPropertyCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                data.AddRange(DB.SelectAllColumnsFrom<YahooProperty>()
                    .Where(YahooProperty.Columns.Bid).In(bids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<YahooPropertyCollection>());
                idx += batchLimit;
            } while (idx <= bids.Count - 1);

            return data;
        }

        public void YahooPropertyActionUpdate(int id, string user_name, DateTime modify_time)
        {
            string sql = @" UPDATE " + YahooProperty.Schema.Provider.DelimitDbName(YahooProperty.Schema.TableName) + @"
                            SET " + YahooProperty.Columns.Action + @"= (
                                CASE " + YahooProperty.Columns.Action + @"
                                WHEN " + (int)YahooPropertyAction.New + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Update + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Delete + @" THEN " + (int)YahooPropertyAction.Remove + @"
                                ELSE " + YahooProperty.Columns.Action + @" END),
                                " + YahooProperty.Columns.ModifyId + @" = @user_name,
                                " + YahooProperty.Columns.ModifyTime + @" = @modify_time
                            WHERE " + YahooProperty.Columns.Id + @" = @id
                            AND action IN (" + (int)YahooPropertyAction.New + @", " + (int)YahooPropertyAction.Update + @", " + (int)YahooPropertyAction.Delete + @")
                            AND " + YahooProperty.Columns.RequestTime + @" IS NOT NULL ";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@id", id, DbType.Int32);
            qc.AddParameter("@user_name", user_name, DbType.AnsiString);
            qc.AddParameter("@modify_time", modify_time, DbType.DateTime);

            DataService.ExecuteScalar(qc);
        }
        public void YahooPropertyActionListUpdate(List<int> ids, string user_name, DateTime modify_time)
        {
            string sql = @" UPDATE " + YahooProperty.Schema.Provider.DelimitDbName(YahooProperty.Schema.TableName) + @"
                            SET " + YahooProperty.Columns.Action + @"= (
                                CASE " + YahooProperty.Columns.Action + @"
                                WHEN " + (int)YahooPropertyAction.New + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Update + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Delete + @" THEN " + (int)YahooPropertyAction.Remove + @"
                                ELSE " + YahooProperty.Columns.Action + @" END),
                                " + YahooProperty.Columns.ModifyId + @" = @user_name,
                                " + YahooProperty.Columns.ModifyTime + @" = @modify_time
                            WHERE " + YahooProperty.Columns.Id + @" in (" + string.Join(",", ids.ToArray()) + @") 
                            AND action IN (" + (int)YahooPropertyAction.New + @", " + (int)YahooPropertyAction.Update + @", " + (int)YahooPropertyAction.Delete + @")
                            AND " + YahooProperty.Columns.RequestTime + @" IS NOT NULL ";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@user_name", user_name, DbType.AnsiString);
            qc.AddParameter("@modify_time", modify_time, DbType.DateTime);

            DataService.ExecuteScalar(qc);
        }


        public void YahooPropertyActionUpdateAll(string user_name, DateTime modify_time)
        {
            string sql = @" UPDATE " + YahooProperty.Schema.Provider.DelimitDbName(YahooProperty.Schema.TableName) + @"
                            SET " + YahooProperty.Columns.Action + @"= (
                                CASE " + YahooProperty.Columns.Action + @"
                                WHEN " + (int)YahooPropertyAction.New + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Update + @" THEN " + (int)YahooPropertyAction.Ordinary + @"
                                WHEN " + (int)YahooPropertyAction.Delete + @" THEN " + (int)YahooPropertyAction.Remove + @"
                                ELSE " + YahooProperty.Columns.Action + @" END),
                                " + YahooProperty.Columns.ModifyId + @" = @user_name,
                                " + YahooProperty.Columns.ModifyTime + @" = @modify_time
                            WHERE action IN (" + (int)YahooPropertyAction.New + @", " + (int)YahooPropertyAction.Update + @", " + (int)YahooPropertyAction.Delete + @")
                            AND " + YahooProperty.Columns.RequestTime + @" IS NOT NULL ";
            QueryCommand qc = new QueryCommand(sql, Order.Schema.Provider.Name);
            qc.AddParameter("@user_name", user_name, DbType.AnsiString);
            qc.AddParameter("@modify_time", modify_time, DbType.DateTime);

            DataService.ExecuteScalar(qc);
        }

        public void YahooPropertyUpdate(int id, int? sortN, int? sortC, int? sortS)
        {
            DB.Update<YahooProperty>().Set(YahooProperty.Columns.SortN).EqualTo(sortN)
                .Set(YahooProperty.Columns.SortC).EqualTo(sortC)
                .Set(YahooProperty.Columns.SortS).EqualTo(sortS)
                .Where(YahooProperty.Columns.Id).IsEqualTo(id).Execute();
        }
        #endregion

        #region PponOption

        public PponOption PponOptionGet(int id)
        {
            return DB.Get<PponOption>(id);
        }
        public string PponOptionGetIdByGuid(Guid gid)
        {
            QueryCommand qc = new QueryCommand("select id from " + PponOption.Schema.TableName +
                " with(nolock) where is_disabled=0 and guid=@gid", PponOption.Schema.Provider.Name);
            qc.AddParameter("@gid", gid, DbType.Guid);
            return DataService.ExecuteScalar(qc).ToString();
        }

        public PponOption PponOptionGetByGuid(Guid gid)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<PponOption>()
                .Where(PponOption.Columns.Guid).IsEqualTo(gid);

            return query.ExecuteSingle<PponOption>();
        }

        public List<Guid> PponOptionGetItemGuidListByBids(List<Guid> bids)
        {
            return DB.Select(PponOption.Columns.ItemGuid).From(PponOption.Schema.TableName)
                .Where(PponOption.Columns.BusinessHourGuid).In(bids)
                .ExecuteTypedList<Guid>();
        }

        public List<Guid> PponOptionGetItemGuidListByBid(Guid bid)
        {
            return DB.Select(PponOption.Columns.ItemGuid).From(PponOption.Schema.TableName)
                .Where(PponOption.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteTypedList<Guid>();
        }

        public Dictionary<Guid, List<Guid>> PponOptionGetItemGuidDictByOnlineDeals()
        {
            string sql = @"
    select * from ppon_option po with(nolock)
    where po.business_hour_guid in 
			(
				(select distinct dts.business_hour_GUID from deal_time_slot dts 
					where getdate() between dts.effective_start and dts.effective_end)
				union
				(select cd.BusinessHourGuid from combo_deals cd with(nolock) where cd.MainBusinessHourGuid in 
					(
						select distinct dts.business_hour_GUID from deal_time_slot dts 
						where getdate() between dts.effective_start and dts.effective_end
					)
				)			
			)
	and po.item_guid is not null
";
            QueryCommand qc = new QueryCommand(sql, PponOption.Schema.Provider.Name);
            PponOptionCollection col = new PponOptionCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            var result = col.GroupBy(t => t.BusinessHourGuid).ToDictionary(t => t.Key, t => t.Select(p => p.ItemGuid.GetValueOrDefault()).ToList());
            return result;
        }

        public int PponOptionSet(PponOption option)
        {
            return DB.Save(option);
        }

        public PponOptionCollection PponOptionGetList(Guid bid)
        {
            return PponOptionGetList(bid, true);
        }

        public PponOptionCollection PponOptionGetList(Guid bid, bool enabledOnly)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<PponOption>()
                .Where(PponOption.Columns.BusinessHourGuid).IsEqualTo(bid);

            if (enabledOnly)
            {
                query.And(PponOption.Columns.IsDisabled).IsEqualTo(false);
            }

            return query.ExecuteAsCollection<PponOptionCollection>() ?? new PponOptionCollection();
        }

        public int PponOptionSetList(PponOptionCollection options)
        {
            return DB.SaveAll(options);
        }

        public int? PponOptionGetIdByAccessory(Guid acc_grp_mem_guid)
        {
            string sql = @"select opt.id
from business_hour bh
inner join item
  on item.business_hour_guid = bh.GUID
left join item_accessory_group_list iag
  on iag.item_GUID = item.GUID
left join accessory_group ag
  on iag.accessory_group_GUID = ag.GUID
left join accessory_group_member agmem
  on ag.GUID = agmem.accessory_group_GUID
left join accessory acc
  on agmem.accessory_GUID = acc.GUID
left join ppon_option opt
  on bh.GUID = opt.business_hour_guid
  and ag.accessory_group_name = opt.catg_name
  and acc.accessory_name = opt.option_name
where agmem.guid = @agmem_guid";
            QueryCommand qc = new QueryCommand(sql, PponOption.Schema.Provider.Name);
            qc.AddParameter("agmem_guid", acc_grp_mem_guid, DbType.Guid);
            object id = DataService.ExecuteScalar(qc);

            if (id == null)
            {
                return null;
            }

            int result;
            if (int.TryParse(id.ToString(), out result))
            {
                return result;
            }
            return null;
        }

        public int? PponOptionGetIdByAccessoryAndBid(Guid acc_grp_mem_guid, Guid bid)
        {
            string sql = @"select opt.id
from accessory_group ag
inner join accessory_group_member agmem
  on ag.GUID = agmem.accessory_group_GUID
inner join accessory acc
  on agmem.accessory_GUID = acc.GUID
inner join ppon_option opt
  on agmem.ppon_bid = opt.business_hour_guid
  and ag.accessory_group_name = opt.catg_name
  and acc.accessory_name = opt.option_name
where agmem.guid = @agmem_guid
and agmem.ppon_bid = @bid";
            QueryCommand qc = new QueryCommand(sql, PponOption.Schema.Provider.Name);
            qc.AddParameter("agmem_guid", acc_grp_mem_guid, DbType.Guid);
            qc.AddParameter("bid", bid, DbType.Guid);
            object id = DataService.ExecuteScalar(qc);

            if (id == null)
            {
                return null;
            }

            int result;
            if (int.TryParse(id.ToString(), out result))
            {
                return result;
            }
            return null;
        }

        public List<Guid> PponOptionGetOverGroup()
        {

            string sql = @"select business_hour_guid from (
                           select business_hour_guid, catg_name from ppon_option --where business_hour_guid = '558ad206-32e7-4bfa-a27c-62eb44764dd5'
                                                                 where is_disabled = 0
                                                                 group by business_hour_guid,catg_name) as a
                           inner join business_hour b on a.business_hour_guid = b.guid
                           where getdate() between b.business_hour_order_time_s and business_hour_order_time_e
                           group by business_hour_guid
                           having count(*) > 1";

            List<Guid> result = new List<Guid>();
            QueryCommand qc = new QueryCommand(sql, PponOption.Schema.Provider.Name);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }
            return result;
        }

        #endregion

        #region PponOptionLog
        public PponOptionLog PponOptionLogGet(int id)
        {
            return DB.Get<PponOptionLog>(id);
        }
        public PponOptionLogCollection PponOptionLogGet(Guid bid)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<PponOptionLog>()
                .Where(PponOptionLog.Columns.BusinessHourGuid).IsEqualTo(bid);

            return query.ExecuteAsCollection<PponOptionLogCollection>() ?? new PponOptionLogCollection();
        }
        public int PponOptionLogSetList(PponOptionLogCollection options)
        {
            return DB.SaveAll(options);
        }
        #endregion

        #region ViewFamiDealDeliverTime

        public ViewFamiDealDeliverTimeCollection GetViewFamiDealDeliverTimeByBeforeDay(int dayNumber)
        {
            string sql = @"select * from dbo.view_fami_deal_deliver_time
                            where DATEDIFF(dd, GETDATE(), business_hour_deliver_time_e)=@dayNumber";
            QueryCommand qc = new QueryCommand(sql, ViewFamiDealDeliverTime.Schema.Provider.Name);
            qc.AddParameter("@dayNumber", dayNumber, DbType.Int32);
            ViewFamiDealDeliverTimeCollection viewFamiDealDeliverTimeCol = new ViewFamiDealDeliverTimeCollection();
            viewFamiDealDeliverTimeCol.LoadAndCloseReader(DataService.GetReader(qc));
            return viewFamiDealDeliverTimeCol;
        }

        #endregion

        #region FamiportPeztempLink

        public int FamiportPeztempLinkSet(FamiportPeztempLink fpl)
        {
            fpl.ModifyTime = DateTime.Now;
            return DB.Save<FamiportPeztempLink>(fpl);
        }

        public FamiportPeztempLink FamiportPeztempLinkGetByTranNo(string tranNo)
        {
            return DB.Get<FamiportPeztempLink>(FamiportPeztempLink.Columns.TranNo, tranNo);
        }

        public FamiportPeztempLink FamiportPeztempLinkGetByPeztempId(int peztempId)
        {
            FamiportPeztempLinkCollection fplc = new FamiportPeztempLinkCollection();
            fplc = DB.SelectAllColumnsFrom<FamiportPeztempLink>().Where(FamiportPeztempLink.Columns.PeztempId).IsEqualTo(peztempId)
                .ExecuteAsCollection<FamiportPeztempLinkCollection>();

            if (fplc.Any())
            {
                return fplc.OrderByDesc(FamiportPeztempLink.Columns.PayDatetime).FirstOrDefault();
            }
            else
            {
                return new FamiportPeztempLink();
            }
        }

        public void FamiportPeztempLinkUpdate(FamiportPeztempLink fpl)
        {
            DB.Update<FamiportPeztempLink>(fpl);
        }

        public void FamiportPeztempLinkDelete(FamiportPeztempLink fpl)
        {
            DB.Delete<FamiportPeztempLink>(fpl);
        }

        #endregion

        #region FamiportPinRollbackLog

        public int FamiportPinRollbackLogSet(FamiportPinRollbackLog log)
        {
            return DB.Save<FamiportPinRollbackLog>(log);
        }

        #endregion

        #region View_Fami_Pay_log

        public List<Tuple<string, int>> FamiUsageQuery(DateTime queryStart, DateTime queryEnd, bool day, bool hour)
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();
            string sql = string.Format(@"
                        select 
                        YEAR(fpl.pay_datetime) 'log_year'
                        , MONTH(fpl.pay_datetime) 'log_month'{0}{1}
                        , COUNT(1) as pay_count
                         from view_fami_pay_log fpl with(nolock)
                         where fpl.pay_datetime between @startDate and @endDate
                        GROUP BY YEAR(fpl.pay_datetime), MONTH(fpl.pay_datetime){2}{3}
                        ORDER BY YEAR(fpl.pay_datetime), MONTH(fpl.pay_datetime){4}{5}"
                , day ? ", DAY(fpl.pay_datetime) 'log_day'" : string.Empty
                , hour ? ", datepart(hh, fpl.pay_datetime) 'log_hour'" : string.Empty
                , day ? ", DAY(fpl.pay_datetime)" : string.Empty
                , hour ? ", datepart(hh, fpl.pay_datetime)" : string.Empty
                , day ? ", DAY(fpl.pay_datetime)" : string.Empty
                , hour ? ", datepart(hh, fpl.pay_datetime)" : string.Empty);

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@startDate", queryStart, DbType.DateTime);
            qc.AddParameter("@endDate", queryEnd, DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var strItem = new DateTime(int.Parse(row["log_year"].ToString())
                    , int.Parse(row["log_month"].ToString()), 1).ToString("yyyy/MM");

                if (day)
                {
                    strItem = new DateTime(int.Parse(row["log_year"].ToString())
                        , int.Parse(row["log_month"].ToString())
                        , int.Parse(row["log_day"].ToString())).ToString("yyyy/MM/dd");
                }

                if (hour)
                {
                    strItem = new DateTime(int.Parse(row["log_year"].ToString())
                        , int.Parse(row["log_month"].ToString())
                        , int.Parse(row["log_day"].ToString())
                        , int.Parse(row["log_hour"].ToString()), 0, 0).ToString("yyyy/MM/dd hh");
                }

                Tuple<string, int> item = new Tuple<string, int>(strItem, (int)row["pay_count"]);
                result.Add(item);
            }
            return result;
        }

        #endregion

        /// <summary>
        /// 取得檔次已定購數量 (為求準確性不加 with(nolock))
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public int OrderedQuantityGet(Guid bid)
        {
            string sql = @"
SELECT 	
		(CASE WHEN n.ordered_quantity IS NULL THEN 0 ELSE n.ordered_quantity END) AS ordered_quantity		
FROM         dbo.business_hour AS a 
                      LEFT OUTER JOIN
                      dbo.group_order AS j ON j.business_hour_guid = a.GUID LEFT OUTER JOIN
                          (SELECT     o.parent_order_id AS order_guid, SUM(od.item_quantity) AS ordered_quantity
                            FROM          dbo.[order] AS o INNER JOIN
                                                   dbo.order_detail AS od ON od.order_GUID = o.GUID
                            WHERE      (o.order_status & 8 > 0) AND (o.order_status & 512 = 0) AND (od.status <> 2)
                            GROUP BY o.parent_order_id) AS n ON n.order_guid = j.order_guid 

where business_hour_guid = @bid";

            QueryCommand qc = new QueryCommand(sql, BusinessHour.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            object obj = DataService.Provider.ExecuteScalar(qc);
            if (obj == null)
            {
                return 0;
            }
            return (int)obj;
        }

        #region IdeasStation
        public void SaveIdeasStationItem(IdeasStation item)
        {
            var data = DB.Get<IdeasStation>(item.Id);

            data.Title = item.Title;
            data.CouponDealBroadcastMinutes = item.CouponDealBroadcastMinutes;
            data.CouponEventUrl = item.CouponEventUrl;
            data.DeliveryDealBroadcastMinutes = item.DeliveryDealBroadcastMinutes;
            data.DeliveryEventUrl = item.DeliveryEventUrl;
            data.SpecialDealBroadcastTimeEndTimeFormat1 = item.SpecialDealBroadcastTimeEndTimeFormat1;
            data.SpecialDealBroadcastTimeEndTimeFormat2 = item.SpecialDealBroadcastTimeEndTimeFormat2;
            data.SpecialDealBroadcastTimeStartTimeFormat1 = item.SpecialDealBroadcastTimeStartTimeFormat1;
            data.SpecialDealBroadcastTimeStartTimeFormat2 = item.SpecialDealBroadcastTimeStartTimeFormat2;
            data.SpecialEventUrl = item.SpecialEventUrl;
            data.VourcherEventUrl = item.VourcherEventUrl;
            data.VourcherStoreBroadcastMinutes = item.VourcherStoreBroadcastMinutes;
            data.Status = item.Status;


            if (data.IsLoaded)
            {
                data.ModifyDate = DateTime.Now;

                DB.Update<IdeasStation>(data);
            }
            else
            {
                data.Id = item.Id;
                data.CreateDate = DateTime.Now;
                data.Creator = item.Creator;

                DB.Save<IdeasStation>(item);
            }

            if (!this.GetEventPromo(data.CouponEventUrl, EventPromoType.Ppon).IsLoaded)
            {
                this.SaveEventPromo(new EventPromo()
                {
                    Title = string.Format("資策會-{0}團購", data.Title),
                    Url = data.CouponEventUrl,
                    Cpa = "cpa",
                    StartDate = DateTime.Today,
                    EndDate = DateTime.Today.AddYears(2),
                    MainPic = "<br />",
                    BgColor = "FF9999",
                    Status = true,
                    Creator = "sys",
                    Cdt = DateTime.Now,
                    ShowInWeb = false,
                    ShowInApp = false,
                    ShowInIdeas = true
                });
            }
            if (!this.GetEventPromo(data.DeliveryEventUrl, EventPromoType.Ppon).IsLoaded)
            {
                this.SaveEventPromo(new EventPromo()
                {
                    Title = string.Format("資策會-{0}宅配", data.Title),
                    Url = data.DeliveryEventUrl,
                    Cpa = "cpa",
                    StartDate = DateTime.Today,
                    EndDate = DateTime.Today.AddYears(2),
                    MainPic = "<br />",
                    BgColor = "FF9999",
                    Status = true,
                    Creator = "sys",
                    Cdt = DateTime.Now,
                    ShowInWeb = false,
                    ShowInApp = false,
                    ShowInIdeas = true
                });
            }
            if (!this.GetEventPromo(data.SpecialEventUrl, EventPromoType.Ppon).IsLoaded)
            {
                this.SaveEventPromo(new EventPromo()
                {
                    Title = string.Format("資策會-{0}強打", data.Title),
                    Url = data.SpecialEventUrl,
                    Cpa = "cpa",
                    StartDate = DateTime.Today,
                    EndDate = DateTime.Today.AddYears(2),
                    MainPic = "<br />",
                    BgColor = "FF9999",
                    Status = true,
                    Creator = "sys",
                    Cdt = DateTime.Now,
                    ShowInWeb = false,
                    ShowInApp = false,
                    ShowInIdeas = true
                });
            }

            if (!this.GetEventPromo(data.VourcherEventUrl, EventPromoType.Ppon).IsLoaded)
            {
                this.SaveEventPromo(new EventPromo()
                {
                    Title = string.Format("資策會-{0}優惠券", data.Title),
                    Url = data.VourcherEventUrl,
                    Cpa = "cpa",
                    StartDate = DateTime.Today,
                    EndDate = DateTime.Today.AddYears(2),
                    MainPic = "<br />",
                    BgColor = "FF9999",
                    Status = true,
                    Creator = "sys",
                    Cdt = DateTime.Now,
                    ShowInWeb = false,
                    ShowInApp = false,
                    ShowInIdeas = true
                });
            }
        }

        public IdeasStation GetIdeasStationItem(int Id)
        {
            return DB.Get<IdeasStation>(Id);
        }

        public IdeasStationCollection GetIdeasStationList()
        {
            return DB.Select()
                .From(IdeasStation.Schema)
                .Where(IdeasStation.Columns.Status)
                .IsEqualTo(true)
                .ExecuteAsCollection<IdeasStationCollection>();
        }

        #endregion

        #region 延遲出貨的檔次
        public DataTable GetDelayBidTable(string queryEndTime)
        {
            //尚未回填母檔出貨回覆日的檔次，即今日>開始配送日期(迄)
            //
            //條件規則
            //宅配 成檔 商家系統
            //母當出貨回覆日未填

            //避免檔次全部退光還發延遲出貨信
            //所以多檔次要先統計在來比對
            string sql = string.Format(@"
select it.item_name 'Title',bh.guid as bid,dp.unique_id
from business_hour bh
join deal_property dp with(nolock) on bh.GUID=dp.business_hour_guid
join coupon_event_content cec with(nolock) on bh.GUID = cec.business_hour_guid
join deal_accounting da with(nolock) on bh.GUID = da.business_hour_guid
JOIN item it with(nolock) ON it.business_hour_guid = bh.GUID
left join (
	select isnull(cd.BusinessHourGuid,dsi.business_hour_guid) bid,cd.MainBusinessHourGuid mid,dsi.ordered_quantity,ComboQuantity 
	from combo_deals cd
	left join deal_sales_info dsi on dsi.business_hour_guid=cd.BusinessHourGuid
	left join (
		select icd.MainBusinessHourGuid mid,sum(idsi.ordered_quantity) as ComboQuantity
		from deal_sales_info idsi
		left join combo_deals icd on icd.BusinessHourGuid=idsi.business_hour_guid
		group by MainBusinessHourGuid
	)cdQuantity on cdQuantity.mid = cd.MainBusinessHourGuid
)cd on cd.bid = bh.GUID
where dp.delivery_type=2
and shipped_date is null and da.vendor_billing_model = 1
and da.remittance_type in (0,3)
and bh.business_hour_deliver_time_e=@queryEndTime
and (
(cd.mid is null and cd.ordered_quantity >= bh.business_hour_order_minimum)
or
(cd.bid = cd.mid and cd.ComboQuantity >= bh.business_hour_order_minimum)
)
");

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryEndTime))
            {
                dt.Load(idr);
            }
            return dt;
        }
        #endregion

        #region 延遲出貨罰款的檔次
        public DataTable DelayBidAmercementGetTable(string queryEndTime)
        {
            //已填母檔出貨回覆日，但母檔出貨回覆日>開始配送日(迄)的檔次
            //宅配檔
            //母當出貨回覆日已填
            //選擇主檔的檔次（exists....)
            //單檔檔次（not exists...)
            //昨天的母檔出貨

            string sql = string.Format(@"
select it.item_name 'Title',bh.guid as bid,dp.unique_id
from business_hour bh WITH (NOLOCK)
join deal_property dp with(nolock) on bh.GUID=dp.business_hour_guid
join coupon_event_content cec with(nolock) on bh.GUID = cec.business_hour_guid
join deal_accounting da with(nolock) on bh.GUID = da.business_hour_guid
JOIN item it with(nolock) ON it.business_hour_guid = bh.GUID
left join combo_deals cd WITH (NOLOCK) on cd.BusinessHourGuid = bh.GUID
left join deal_sales_info dsi WITH (NOLOCK) ON dsi.business_hour_guid = bh.GUID 
left join group_order god WITH (NOLOCK) on god.business_hour_guid = bh.GUID
where dp.delivery_type=2 and da.vendor_billing_model = 1
and da.remittance_type in (0,3)
and (dsi.ordered_quantity >= bh.business_hour_order_minimum or god.slug>=bh.business_hour_order_minimum)
and (cd.BusinessHourGuid = cd.MainBusinessHourGuid or cd.MainBusinessHourGuid is null)
and shipped_date=@queryEndTime
and bh.business_hour_deliver_time_e<@queryEndTime
");

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryEndTime))
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable GetDelayBidAmercements(string queryEndTime)
        {
            DataTable dt = new DataTable();
            string sql = string.Format(@"SELECT distinct bh.guid AS business_hour_guid,od.guid order_guid,od.order_id,os.ship_time,ctl.trust_id,ctl.amount
                        FROM Business_Hour AS bh WITH(nolock)
                            INNER JOIN {0} AS dp WITH(nolock) ON dp.business_hour_guid = bh.GUID
                            INNER JOIN {1} AS dsi WITH (NOLOCK) ON dsi.business_hour_guid = bh.GUID
                            INNER JOIN {2} AS da WITH(nolock) ON da.business_hour_guid = bh.GUID
                            INNER JOIN {3} AS ctl WITH(nolock) ON ctl.business_hour_guid = bh.GUID
                            INNER JOIN [{4}] AS od WITH(nolock) ON od.guid = ctl.order_guid
                            LEFT OUTER JOIN {8} AS os WITH(nolock) ON os.order_guid = ctl.order_guid
                        WHERE da.vendor_billing_model = 1
                            AND dsi.ordered_quantity >= bh.business_hour_order_minimum
                            AND DateDiff(day,bh.business_hour_deliver_time_e,@queryDate)=0                          --配送日期截止日是昨天
                            AND (os.ship_time is null or os.ship_time > bh.business_hour_deliver_time_e)            --出貨日期晚於配送日期截止日 或是 Null
                            AND da.Remittance_Type in({5})                                                          --先收單據後付款的資料
                            AND dp.delivery_type = 2	                                                            --宅配
                            AND ctl.status in (0, 1, 2)
                            AND ctl.order_guid not in (
                                SELECT order_guid
                                FROM {6} WITH(nolock)                                                               --排除退貨的資料
                                WHERE progress_status in ({7})
                            )
                            AND od.order_status & 8 > 0",
                        DealProperty.Schema.TableName,
                        DealSalesInfo.Schema.TableName,
                        DealAccounting.Schema.TableName,
                        CashTrustLog.Schema.TableName,
                        Order.Schema.TableName,
                        string.Format("{0}, {1}, {2}", (int)RemittanceType.Weekly, (int)RemittanceType.Monthly, (int)RemittanceType.Flexible),
                        ReturnForm.Schema.TableName,
                        string.Format("{0}, {1}, {2}, {3}", (int)ProgressStatus.Processing, (int)ProgressStatus.AtmQueueing, (int)ProgressStatus.AtmQueueSucceeded, (int)ProgressStatus.AtmFailed),
                        OrderShip.Schema.TableName
                        );
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql, queryEndTime))
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region 檔次質量相關 view_ppon_deal_operating_info

        public ViewPponDealOperatingInfoCollection ViewPponDealOperatingInfoGetInBids(List<Guid> bids)
        {
            var vpdoiCol = new ViewPponDealOperatingInfoCollection();
            var idx = 0;

            do
            {
                int batchLimit = config.TakeViewPponDealOperatingInfoCount; //至多2100，但容易Timeout
                vpdoiCol.AddRange(DB.SelectAllColumnsFrom<ViewPponDealOperatingInfo>()
                    .Where(ViewPponDealOperatingInfo.Columns.BusinessHourGuid).In(bids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<ViewPponDealOperatingInfoCollection>());

                idx += batchLimit;
            } while (idx <= bids.Count - 1);

            return vpdoiCol;
        }

        public DealTimeSlotSortElementCollection DealTimeSlotSortElementGet()
        {
            return DB.SelectAllColumnsFrom<DealTimeSlotSortElement>()
                .ExecuteAsCollection<DealTimeSlotSortElementCollection>();
        }

        public DealTimeSlotSortElementRankCollection DealTimeSlotSortElementRankGet()
        {
            return DB.SelectAllColumnsFrom<DealTimeSlotSortElementRank>().ExecuteAsCollection<DealTimeSlotSortElementRankCollection>();
        }

        public bool DealTimeSlotSortElementUpdate(DealTimeSlotSortElement dtsse)
        {
            DB.Update<DealTimeSlotSortElement>()
                .Set(DealTimeSlotSortElement.ElementWeightColumn)
                .EqualTo(dtsse.ElementWeight)
                .Where(DealTimeSlotSortElement.ElementIdColumn).IsEqualTo(dtsse.ElementId)
                .And(DealTimeSlotSortElement.CityIdColumn).IsEqualTo(dtsse.CityId)
                .Execute();
            return true;
        }

        public bool DealTimeSlotSortElementInsert(DealTimeSlotSortElementCollection dtsseList)
        {
            DB.SaveAll(dtsseList);

            return true;
        }

        #endregion

        #region Keyword

        public Keyword KeywordGet(int id)
        {
            return DB.Get<Keyword>(id);
        }

        public Keyword KeywordGet(string word)
        {
            return DB.SelectAllColumnsFrom<Keyword>().Where(Keyword.Columns.Word).IsEqualTo(word).ExecuteSingle<Keyword>();
        }

        public void KeywordSet(Keyword keyword)
        {
            DB.Save<Keyword>(keyword);
        }

        public void KeywordDelete(int id)
        {
            DB.Delete<Keyword>(Keyword.Columns.Id, id);
        }

        public KeywordCollection KeywordGetAllList()
        {
            return DB.SelectAllColumnsFrom<Keyword>().ExecuteAsCollection<KeywordCollection>();
        }

        public KeywordCollection KeywordGetList(string search)
        {
            return DB.SelectAllColumnsFrom<Keyword>().Where(Keyword.Columns.Word).ContainsString(search).ExecuteAsCollection<KeywordCollection>();
        }

        public KeywordCollection KeywordGetPromptList(string word)
        {
            return DB.SelectAllColumnsFrom<Keyword>().Where(Keyword.Columns.Word).StartsWith(word).ExecuteAsCollection<KeywordCollection>();
        }

        public void KeywordInsertByPponSearchLog(int count)
        {
            string sql = @" INSERT INTO " + Keyword.Schema.TableName + @"
                            select " + PponSearchLog.Columns.Word + @", 0, 'sys@17life.com', getdate()
                            from " + PponSearchLog.Schema.TableName + @" with(nolock)
                            where " + PponSearchLog.Columns.Word + @" not in (select k." + Keyword.Columns.Word + @" from " + Keyword.Schema.TableName + @" k with(nolock))
                            group by " + Keyword.Columns.Word + @"
                            HAVING COUNT(1) > @count
                            order by COUNT(1) desc";

            var qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@count", count, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        #endregion

        #region ViewDealBaseGrossMargin 毛利率/基礎成本

        public ViewDealBaseGrossMargin ViewDealBaseGrossMarginGet(Guid bid)
        {
            var data = DB.SelectAllColumnsFrom<ViewDealBaseGrossMargin>().NoLock()
                .Where(ViewDealBaseGrossMargin.Columns.BusinessHourGuid).IsEqualTo(bid)
                .ExecuteAsCollection<ViewDealBaseGrossMarginCollection>();

            return data.Any() ? data.FirstOrDefault() : new ViewDealBaseGrossMargin();
        }

        public ViewDealBaseGrossMarginCollection ViewDealBaseGrossMarginGetTodayDeal()
        {
            var today = DateTime.Today.SetTime(12, 0, 0);

            const string sql = @"select * from view_deal_base_gross_margin with(nolock)
                    where business_hour_guid in
                    (
                    select business_hour_guid from business_hour bh with(nolock)
                    where business_hour_order_time_s >= @sdate and business_hour_order_time_e < @edate
                    )";
            var qc = new QueryCommand(sql, ViewDealBaseGrossMargin.Schema.Provider.Name);
            qc.AddParameter("@sdate", today.AddDays(-1), DbType.DateTime);
            qc.AddParameter("@edate", today.AddDays(1), DbType.DateTime);

            var data = new ViewDealBaseGrossMarginCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        /// <summary>
        /// 取得檔次毛利
        /// 如為單檔，取該檔毛利
        /// 如果子檔，取該檔毛利
        /// 如果母檔且結檔，回傳最低子檔毛利
        /// 如果母檔未結檔，回傳未結檔子檔的最低毛利
        /// </summary>
        /// <param name="bid"></param>
        /// <returns></returns>
        public decimal DealMinGrossMarginGet(Guid bid)
        {
            string sql = @"
select isnull(
    (select isnull(
        (select min(vdbgm.gross_margin) as single_deal_min_gross_margin from view_deal_base_gross_margin vdbgm with(nolock)
			where 
			vdbgm.MainBusinessHourGuid is null and vdbgm.business_hour_guid = @bid)
        ,
        (select min(vdbgm.gross_margin) as combdeal_min_gross_margin from view_deal_base_gross_margin vdbgm with(nolock)
			where 
			vdbgm.business_hour_guid = @bid and vdbgm.MainBusinessHourGuid <> vdbgm.business_hour_guid)        
    ))
,
	(select min(vdbgm.gross_margin) as exclude_closed_combdeals_min_gross_margin from view_deal_base_gross_margin vdbgm with(nolock)
        inner join group_order gpo_main with(nolock) on gpo_main.business_hour_guid = vdbgm.MainBusinessHourGuid
        inner join group_order gpo with(nolock) on gpo.business_hour_guid = vdbgm.business_hour_guid
        where 
        (vdbgm.MainBusinessHourGuid = @bid and vdbgm.MainBusinessHourGuid <> vdbgm.business_hour_guid)
        and ((gpo_main.status & 1 = 0 and gpo.status & 1 = 0) or gpo_main.status & 1 = 1))
)

";
            var qc = new QueryCommand(sql, ViewDealBaseGrossMargin.Schema.Provider.Name);
            qc.AddParameter("@bid", bid, DbType.Guid);
            object result = DataService.ExecuteScalar(qc);
            if (result is DBNull)
            {
                return 0;
            }
            return (decimal)result;
        }

        #endregion

        #region PponSearchLog

        public void PponSearchLogSet(PponSearchLog pponSearchLog)
        {
            DB.Save(pponSearchLog);
        }

        public PponSearchLog PponSearchLogGet(string sessionId, string word)
        {
            return DB.Select().From<PponSearchLog>().Where(PponSearchLog.Columns.SessionId).IsEqualTo(sessionId)
                .And(PponSearchLog.Columns.Word).IsEqualTo(word).ExecuteSingle<PponSearchLog>();
        }

        /// <summary>
        /// 抓最近幾定個月的pponsearch使用次數統計
        /// 如果 monthNum 設12，通常會有13個月(12個完整月份+1個進行中的月份)的資料
        /// </summary>
        /// <returns></returns>
        public List<Tuple<string, int>> SearchUsageQueryByMonth(DateTime queryStart, DateTime queryEnd)
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();
            string sql = @"
                declare @startDate datetime, @endDate datetime
                set @startDate = DATEADD(month, DATEDIFF(month, 0, @queryDateStart), 0)
                set @endDate = DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, @queryDateEnd) + 1, 0))

                select 
	                year(psl.create_time) as log_year, 
	                MONTH(psl.create_time) as log_month ,
	                COUNT(*) as hits
	                from ppon_search_log  psl
                where psl.create_time >  @startDate and psl.create_time < @endDate
                group by year(psl.create_time), MONTH(psl.create_time)
                order by year(psl.create_time), MONTH(psl.create_time)
            ";

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@queryDateStart", queryStart, DbType.DateTime);
            qc.AddParameter("@queryDateEnd", queryEnd, DbType.DateTime);

            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Tuple<string, int> item = new Tuple<string, int>(
                    string.Format("{0}-{1}", row["log_year"], row["log_month"]),
                    (int)row["hits"]
                );
                result.Add(item);
            }
            return result;
        }

        /// <summary>
        /// 依月份往回推, 取每日搜尋次數
        /// </summary>
        /// <returns></returns>
        public List<Tuple<string, int>> SearchUsageQueryByDay(DateTime queryStart, DateTime queryEnd)
        {
            List<Tuple<string, int>> result = new List<Tuple<string, int>>();
            string sql = @"
                select
	                year(psl.create_time) as log_year, 
	                MONTH(psl.create_time) as log_month ,
					DAY(psl.create_time) as log_day ,
	                COUNT(*) as hits
	                from ppon_search_log  psl
                where psl.create_time >  @startDate and psl.create_time < @endDate
                group by year(psl.create_time), MONTH(psl.create_time), DAY(psl.create_time)
                order by year(psl.create_time), MONTH(psl.create_time), DAY(psl.create_time)
            ";

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@startDate", queryStart, DbType.DateTime);
            qc.AddParameter("@endDate", queryEnd, DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Tuple<string, int> item = new Tuple<string, int>(
                    new DateTime(int.Parse(row["log_year"].ToString())
                        , int.Parse(row["log_month"].ToString())
                        , int.Parse(row["log_day"].ToString())).ToString("yyyy/MM/dd"),
                    (int)row["hits"]
                );
                result.Add(item);
            }
            return result;
        }

        public List<Tuple<string, string, int>> SearchTopKeywordsByMonth(DateTime queryStart, DateTime queryEnd, int topKeywords = 20)
        {
            List<Tuple<string, string, int>> result = new List<Tuple<string, string, int>>();
            string sql = @"
                declare @startDate datetime, @endDate datetime
                set @startDate = DATEADD(month, DATEDIFF(month, 0, @queryDateStart), 0)
                set @endDate = DATEADD(d, -1, DATEADD(m, DATEDIFF(m, 0, @queryDateEnd) + 1, 0))

select * from (
                select 
	                year(psl.create_time) as log_year, 
	                MONTH(psl.create_time) as log_month ,
	                psl.word,
	                COUNT(*) as hits,
	                ROW_NUMBER() OVER(PARTITION BY year(psl.create_time), month(psl.create_time)
                    ORDER BY COUNT(*) DESC) AS SeqNo
                    
	                from ppon_search_log  psl
                where psl.create_time >  @startDate and psl.create_time < @endDate
                group by year(psl.create_time), MONTH(psl.create_time), word
                
) as t where t.SeqNo <= @top_keywords
order by t.log_year, t.log_month, hits desc
            ";

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@top_keywords", topKeywords, DbType.Int32);
            qc.AddParameter("@queryDateStart", queryStart, DbType.DateTime);
            qc.AddParameter("@queryDateEnd", queryEnd, DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Tuple<string, string, int> item = new Tuple<string, string, int>(
                    string.Format("{0}-{1:00}", row["log_year"], (int)row["log_month"]),
                    row["word"].ToString(),
                    (int)row["hits"]
                );
                result.Add(item);
            }
            return result;
        }

        public List<Tuple<string, string, int>> SearchTopKeywordsByWord(string word, DateTime queryStart, DateTime queryEnd)
        {
            List<Tuple<string, string, int>> result = new List<Tuple<string, string, int>>();
            string sql = @"
                select * from (
                                select 
	                                year(psl.create_time) as log_year, 
	                                MONTH(psl.create_time) as log_month ,
					                DAY(psl.create_time) as log_day,
	                                psl.word,
	                                COUNT(*) as hits
	                                from ppon_search_log  psl
                                where psl.create_time >  @startDate and psl.create_time < @endDate and word = @word
                                group by year(psl.create_time), MONTH(psl.create_time), DAY(psl.create_time), word
                
                ) as t
                order by t.log_year, t.log_month, t.log_day
            ";

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@word", word, DbType.String);
            qc.AddParameter("@startDate", queryStart, DbType.DateTime);
            qc.AddParameter("@endDate", queryEnd, DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Tuple<string, string, int> item = new Tuple<string, string, int>(
                    string.Format("{0}-{1:00}-{2:00}", row["log_year"], (int)row["log_month"], (int)row["log_day"]),
                    row["word"].ToString(),
                    (int)row["hits"]
                );
                result.Add(item);
            }
            return result;
        }

        public List<Tuple<string, string, int>> SearchTopKeywordsByDay(DateTime queryStart, DateTime queryEnd, int topKeywords = 20)
        {
            List<Tuple<string, string, int>> result = new List<Tuple<string, string, int>>();
            string sql = @"
select * from (
                select 
	                year(psl.create_time) as log_year, 
	                MONTH(psl.create_time) as log_month ,
					DAY(psl.create_time) as log_day,
	                psl.word,
	                COUNT(*) as hits,
	                ROW_NUMBER() OVER(PARTITION BY year(psl.create_time), month(psl.create_time), day(psl.create_time)
                    ORDER BY COUNT(*) DESC) AS SeqNo
                    
	                from ppon_search_log  psl
                where psl.create_time >  @startDate and psl.create_time < @endDate
                group by year(psl.create_time), MONTH(psl.create_time), DAY(psl.create_time), word
                
) as t where t.SeqNo <= @top_keywords
order by t.log_year, t.log_month, t.log_day desc, hits desc
            ";

            QueryCommand qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@top_keywords", topKeywords, DbType.Int32);
            qc.AddParameter("@startDate", queryStart, DbType.DateTime);
            qc.AddParameter("@endDate", queryEnd, DbType.DateTime);
            DataSet ds = DataService.GetDataSet(qc);
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                Tuple<string, string, int> item = new Tuple<string, string, int>(
                    string.Format("{0}-{1:00}-{2:00}", row["log_year"], (int)row["log_month"], (int)row["log_day"]),
                    row["word"].ToString(),
                    (int)row["hits"]
                );
                result.Add(item);
            }
            return result;
        }

        public List<Tuple<int, string, int>> SearchHopeData(DateTime queryStart, DateTime queryEnd)
        {
            var result = new List<Tuple<int, string, int>>();
            const string sql = @"select row_number() OVER(ORDER BY count(word) DESC) AS row, word, count(word) as 'hit'
            from ppon_search_log with(nolock) where result_count = 0 and create_time between @queryDateStart and @queryDateEnd
            group by word having count(word) >= 10
            and word not in (select distinct word from ppon_search_log where result_count > 0)";

            var qc = new QueryCommand(sql, PponSearchLog.Schema.Provider.Name);
            qc.AddParameter("@queryDateStart", queryStart, DbType.DateTime);
            qc.AddParameter("@queryDateEnd", queryEnd, DbType.DateTime);
            using (var ds = DataService.GetDataSet(qc))
            {
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var item = new Tuple<int, string, int>(int.Parse(row["row"].ToString()), row["word"].ToString(), (int)row["hit"]);
                    result.Add(item);
                }
            }
            return result;
        }

        #endregion

        #region PromoSearchKey


        public PromoSearchKeyCollection PromoSearchKeyGetColl(PromoSearchKeyUseMode useMode, int takeCnt = 0)
        {
            if (takeCnt > 0)
            {
                return DB.SelectAllColumnsFrom<PromoSearchKey>().Top(takeCnt.ToString())
                    .Where(PromoSearchKey.Columns.IsEnable).IsEqualTo(true)
                    .And(PromoSearchKey.Columns.UseMode).IsEqualTo(useMode)
                    .OrderAsc(PromoSearchKey.Columns.Seq)
                    .ExecuteAsCollection<PromoSearchKeyCollection>();
            }

            return DB.SelectAllColumnsFrom<PromoSearchKey>().Where(PromoSearchKey.Columns.IsEnable).IsEqualTo(true)
                .And(PromoSearchKey.Columns.UseMode).IsEqualTo(useMode)
                .OrderAsc(PromoSearchKey.Columns.Seq)
                .ExecuteAsCollection<PromoSearchKeyCollection>();
        }

        public int PromoSearchKeyRecordSet(PromoSearchKeyRecord data)
        {
            return DB.Save(data);
        }

        public PromoSearchKeyCollection PromoSearchKeyGetAllColl(PromoSearchKeyUseMode useMode)
        {
            return DB.SelectAllColumnsFrom<PromoSearchKey>().Where(PromoSearchKey.Columns.UseMode).IsEqualTo(useMode).OrderAsc(PromoSearchKey.Columns.Seq).ExecuteAsCollection<PromoSearchKeyCollection>();
        }

        public PromoSearchKey PromoSearchKeyGet(int id)
        {
            return DB.Get<PromoSearchKey>(id);
        }

        public PromoSearchKey PromoSearchKeyGet(string keyWord, PromoSearchKeyUseMode useMode)
        {
            return DB.SelectAllColumnsFrom<PromoSearchKey>().Where(PromoSearchKey.Columns.KeyWord).IsEqualTo(keyWord).And(PromoSearchKey.Columns.UseMode).IsEqualTo(useMode).ExecuteSingle<PromoSearchKey>();
        }


        public void PromoSearchKeydSet(PromoSearchKey promoSearchKey)
        {
            DB.Save<PromoSearchKey>(promoSearchKey);
        }

        public void PromoSearchKeydSetColl(PromoSearchKeyCollection promoSearchKeyList)
        {
            DB.SaveAll<PromoSearchKey, PromoSearchKeyCollection>(promoSearchKeyList);
        }

        public void PromoSearchKeyDelete(int id)
        {
            DB.Delete<PromoSearchKey>(PromoSearchKey.Columns.Id, id);
        }

        public int PromoSearchKeyMaxSeq(PromoSearchKeyUseMode useMode)
        {
            var sql = @"select max(seq) from promo_search_key where use_mode=@usemode";

            QueryCommand qc = new QueryCommand(sql, PromoSearchKey.Schema.Provider.Name);
            qc.AddParameter("@usemode", useMode, DbType.Int32);
            var result = DataService.ExecuteScalar(qc);
            return (result == System.DBNull.Value) ? 0 : (int)result;
        }

        public PromoSearchKeyCollection PromoSearchKeyGetSort(int id)
        {
            return DB.SelectAllColumnsFrom<PromoSearchKey>().Where(PromoSearchKey.Columns.Id).IsEqualTo(id)
                .OrderDesc(PromoSearchKey.Columns.Seq).ExecuteAsCollection<PromoSearchKeyCollection>();
        }

        #endregion

        #region RelatedSystemPartner
        public RelatedSystemPartner RelatedSystemPartnerGet(int id)
        {
            return DB.Get<RelatedSystemPartner>(id);
        }

        public void RelatedSystemPartnerSet(RelatedSystemPartner partner)
        {
            DB.Save<RelatedSystemPartner>(partner);
        }

        public int RelatedSystemPartnerGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<RelatedSystemPartner>(filter);
            qc.CommandSql = "select count(1) from " + RelatedSystemPartner.Schema.Provider.DelimitDbName(RelatedSystemPartner.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public RelatedSystemPartnerCollection RelatedSystemPartnerGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<RelatedSystemPartner, RelatedSystemPartnerCollection>(0, -1, null, filter);
        }

        public RelatedSystemPartnerCollection RelatedSystemPartnerGetByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<RelatedSystemPartner, RelatedSystemPartnerCollection>(page, pageSize, orderBy, false, filter);
        }

        public void RelatedSystemPartnerLogSet(RelatedSystemPartnerLog partnerlog)
        {
            DB.Save<RelatedSystemPartnerLog>(partnerlog);
        }
        #endregion

        #region FreewifiUseLog
        public void FreewifiUseLogSet(FreewifiUseLog wifiUseLog)
        {
            DB.Save<FreewifiUseLog>(wifiUseLog);
        }
        #endregion

        #region BackendRecentDealsLog
        public void BackendRecentDealsLogSet(BackendRecentDealsLog log)
        {
            DB.Save<BackendRecentDealsLog>(log);
        }
        #endregion

        #region DealCloudStorage

        public DealCloudStorage DealCloudStorageGet(int id)
        {
            return DB.Get<DealCloudStorage>(id);
        }

        public DealCloudStorage DealCloudStorageGet(Guid bid)
        {
            return DB.Get<DealCloudStorage>(DealCloudStorage.Columns.BusinessHourGuid, bid);
        }

        public void DealCloudStorageSet(DealCloudStorage cloudStorage)
        {
            DB.Save(cloudStorage);
        }

        public List<DealCloudStorage> DealCloudStorageGetListByMarkSync()
        {
            var items = DB.QueryOver<DealCloudStorage>().Where(t => t.S3MarkSync).ToList();
            return items;
        }

        public List<DealCloudStorage> DealCloudStorageGetListByMarkDelete()
        {
            var items = DB.QueryOver<DealCloudStorage>().Where(t => t.S3MarkDelete).ToList();
            return items;
        }

        public DealCloudStorageCollection DealCloudStorageGetListByDayWithNoLock(DateTime startTime, DateTime endTime)
        {
            string sql = @"select * from deal_cloud_storage where 
business_hour_GUID in (
select business_hour_GUID from deal_time_slot
where effective_start > @startTime and effective_start < @endTime
)";
            QueryCommand qc = new QueryCommand(sql, DealCloudStorage.Schema.Provider.Name);
            qc.Parameters.Add("@startTime", startTime, DbType.DateTime);
            qc.Parameters.Add("@endTime", endTime, DbType.DateTime);
            DealCloudStorageCollection col = new DealCloudStorageCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public List<DealCloudImageReference> DealCloudImageReferenceGetList(Guid bid)
        {
            return DB.QueryOver<DealCloudImageReference>().Where(t => t.BusinessHourGuid == bid).ToList();
        }

        public void DealCloudImageReferenceSet(DealCloudImageReference imgref)
        {
            DB.Save(imgref);
        }

        public void DealCloudImageReferenceDelete(DealCloudImageReference imgref)
        {
            DB.Delete(imgref);
        }

        public void DealCloudImageReferenceDeleteByBid(Guid bid)
        {
            string sql = @"delete from " + DealCloudImageReference.Schema.TableName
                + " where " + DealCloudImageReference.Columns.BusinessHourGuid + " = @bid";
            QueryCommand qc = new QueryCommand(sql, DealCloudStorage.Schema.Provider.Name);
            qc.Parameters.Add("@bid", bid, DbType.Guid);
            DataService.ExecuteQuery(qc);
        }

        public int DealCloudImageReferenceGetCount(string imgkey)
        {
            string sql = @"select count(*) from deal_cloud_image_reference where image_key = @image_key";
            QueryCommand qc = new QueryCommand(sql, DealCloudStorage.Schema.Provider.Name);
            qc.Parameters.Add("@image_key", imgkey, DbType.String);
            object result = DataService.ExecuteScalar(qc);
            if (result == null)
            {
                return 0;
            }
            return (int)result;
        }

        #endregion

        #region SalesCalendar
        public SalesCalendarEvent SalesCalendarEventGet(int id)
        {
            return DB.Get<SalesCalendarEvent>(id);
        }
        public SalesCalendarEventCollection SalesCalendarEventGet(int pageStart, int pageLength, params string[] filter)
        {
            QueryCommand qc = GetVPOWhereQC(SalesCalendarEvent.Schema, filter);
            qc.CommandSql = "select * from " + SalesCalendarEvent.Schema.Provider.DelimitDbName(SalesCalendarEvent.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + SalesCalendarEvent.Columns.CreateTime + " desc";
            qc = SSHelper.MakePagable(qc, pageStart, pageLength, "");
            SalesCalendarEventCollection vfcCol = new SalesCalendarEventCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }
        public int SalesCalendarEventGetCount(params string[] filter)
        {
            QueryCommand qc = GetVPOWhereQC(SalesCalendarEvent.Schema, filter);
            qc.CommandSql = "select count(1) from " + SalesCalendarEvent.Schema.TableName + " with(nolock)" + qc.CommandSql;

            return (int)DataService.ExecuteScalar(qc);
        }
        public SalesCalendarEvent SalesCalendarEventGetByGid(string gid)
        {
            return DB.Get<SalesCalendarEvent>(SalesCalendarEvent.Columns.Gid, gid);
        }
        public void SalesCalendarEventSet(SalesCalendarEvent calendar)
        {
            DB.Save(calendar);
        }
        public bool SalesCalendarEventDelete(int id)
        {
            DB.Delete<SalesCalendarEvent>(SalesCalendarEvent.Columns.Id, id);
            return true;
        }
        #endregion

        #region provision_description
        public ProvisionDescriptionCollection ProvisionDescriptionGetAll()
        {
            QueryCommand qc = GetVPOWhereQC(ProvisionDescription.Schema, null);
            qc.CommandSql = "select * from " + ProvisionDescription.Schema.Provider.DelimitDbName(ProvisionDescription.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            ProvisionDescriptionCollection vfcCol = new ProvisionDescriptionCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }
        public ProvisionDescriptionCollection ProvisionDescriptionGetByRootId(string rootId)
        {
            return DB.SelectAllColumnsFrom<ProvisionDescription>()
                        .Where(ProvisionDescription.Columns.RootId).IsEqualTo(rootId)
                        .ExecuteAsCollection<ProvisionDescriptionCollection>();
        }
        public ProvisionDescription ProvisionDescriptionGet(string id)
        {
            return DB.Get<ProvisionDescription>(id);
        }
        public void ProvisionDescriptionSet(ProvisionDescription provision)
        {
            DB.Save(provision);
        }
        public void ProvisionDescriptionDelete(ProvisionDescription provision)
        {
            DB.Delete(provision);
        }
        #endregion provision_description

        #region product_info
        public ProductInfo ProductInfoGet(Guid pid)
        {
            return DB.Get<ProductInfo>(pid);
        }
        public ProductInfoCollection ProductInfoListGet(List<Guid> pid)
        {
            var infos = new ProductInfoCollection();
            var tempIds = pid.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ProductInfo>().NoLock()
                        .Where(ProductInfo.Columns.Guid).In(pid.Skip(idx).Take(batchLimit))
                        .And(ProductInfo.Columns.Status).IsNotEqualTo((int)ProductItemStatus.Deleted)
                        .ExecuteAsCollection<ProductInfoCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;

            //return DB.SelectAllColumnsFrom<ProductInfo>().NoLock()
            //            .Where(ProductInfo.Columns.Guid).In(pid)
            //            .And(ProductInfo.Columns.Status).IsNotEqualTo((int)ProductItemStatus.Deleted)
            //            .ExecuteAsCollection<ProductInfoCollection>();
        }
        public ProductInfoCollection ProductInfoListGetByName(string pbName, string pName)
        {
            return DB.SelectAllColumnsFrom<ProductInfo>().NoLock()
                        .Where(ProductInfo.Columns.ProductBrandName).IsEqualTo(pbName)
                        .And(ProductInfo.Columns.ProductName).IsEqualTo(pName)
                        .And(ProductInfo.Columns.Status).IsNotEqualTo((int)ProductItemStatus.Deleted)
                        .ExecuteAsCollection<ProductInfoCollection>();
        }
        public ProductInfoCollection ProductInfoListGetSellerGuid(Guid sid)
        {
            return DB.SelectAllColumnsFrom<ProductInfo>().NoLock()
                        .Where(ProductInfo.Columns.SellerGuid).IsEqualTo(sid)
                        .And(ProductInfo.Columns.Status).IsEqualTo((int)ProductItemStatus.Normal)
                        .ExecuteAsCollection<ProductInfoCollection>();
        }
        public void ProductInfoSet(ProductInfo pi)
        {
            DB.Save(pi);
        }
        #endregion product_info

        #region product_detail_info
        public ProductItem ProductItemGet(Guid did)
        {
            return DB.Get<ProductItem>(did);
        }
        public ProductItem ProductItemGetByNo(int pro_no)
        {
            return DB.SelectAllColumnsFrom<ProductItem>().NoLock()
                        .Where(ProductItem.Columns.ProductNo).In(pro_no)
                        .ExecuteSingle<ProductItem>();
        }
        public ProductItemCollection ProductItemGetList(List<Guid> did)
        {
            return DB.SelectAllColumnsFrom<ProductItem>().NoLock()
                        .Where(ProductItem.Columns.Guid).In(did)
                        .ExecuteAsCollection<ProductItemCollection>();
        }
        public ProductItemCollection ProductItemListGetByProductGuid(Guid pid)
        {
            return DB.SelectAllColumnsFrom<ProductItem>().NoLock()
                        .Where(ProductItem.Columns.InfoGuid).IsEqualTo(pid)
                        .ExecuteAsCollection<ProductItemCollection>();
        }
        public ProductItemCollection ProductItemGetListByShortage()
        {
            QueryCommand qc = GetVPOWhereQC(ProvisionDescription.Schema, null);
            qc.CommandSql = "select * from " + ProductItem.Schema.Provider.DelimitDbName(ProductItem.Schema.TableName) + " with(nolock) "
                        + " where " + ProductItem.Columns.Stock + "<" + ProductItem.Columns.SafetyStock
                        + " and " + ProductItem.Columns.SafetyStock + "<> 0";
            ProductItemCollection vfcCol = new ProductItemCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }
        public void ProductItemSet(ProductItem di)
        {
            DB.Save(di);
        }

        public void ProductItemStockIncrease(Guid itemGuid, int incrQty, string modifyId)
        {
            string sql = @"
                            update product_item 
                            set 
                                stock = stock + @incrQty,
                                modify_id = @modifyId,
                                modify_time = getdate()
                            where guid = @itemGuid";

            QueryCommand qc = new QueryCommand(sql, Peztemp.Schema.Provider.Name);
            qc.AddParameter("@incrQty", incrQty, DbType.Int32);
            qc.AddParameter("@modifyId", modifyId, DbType.AnsiString);
            qc.AddParameter("@itemGuid", itemGuid, DbType.Guid);
            DataService.ExecuteScalar(qc);
        }

        #endregion product_detail_info

        #region product_item
        public ProductItem GetProductItemByProdId(string prodId)
        {
            return DB.SelectAllColumnsFrom<ProductItem>().NoLock()
                    .Where(ProductItem.Columns.PchomeProdId).IsEqualTo(prodId)
                    .ExecuteSingle<ProductItem>();
        }
        public ProductSpec ProductSpecGet(Guid pdi)
        {
            return DB.Get<ProductSpec>(pdi);
        }
        public ProductSpecCollection ProductSpecListGetByItemGuid(Guid itemGuid)
        {
            return DB.SelectAllColumnsFrom<ProductSpec>().NoLock()
                        .Where(ProductSpec.Columns.ItemGuid).IsEqualTo(itemGuid)
                        .ExecuteAsCollection<ProductSpecCollection>();
        }
        public ProductSpecCollection ProductSpecListGetByItemGuid(List<Guid> itemGuids)
        {
            var infos = new ProductSpecCollection();
            var tempIds = itemGuids.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ProductSpec>().NoLock()
                        .Where(ProductSpec.Columns.ItemGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .And(ProductSpec.Columns.SpecStatus).IsNotEqualTo((int)ProductItemStatus.Deleted)
                        .ExecuteAsCollection<ProductSpecCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;

            //return DB.SelectAllColumnsFrom<ProductSpec>().NoLock()
            //            .Where(ProductSpec.Columns.ItemGuid).In(itemGuids)
            //            .And(ProductSpec.Columns.SpecStatus).IsNotEqualTo((int)ProductItemStatus.Deleted)
            //            .ExecuteAsCollection<ProductSpecCollection>();
        }
        public void ProductSpecSet(ProductSpec spec)
        {
            DB.Save(spec);
        }
        #endregion product_item

        #region product_item_stock
        public ProductItemStock ProductItemStockGet(Guid pdi)
        {
            return DB.Get<ProductItemStock>(pdi);
        }
        public ProductItemStockCollection ProductItemStockListGetByProduct(Guid pid, Guid did)
        {
            return DB.SelectAllColumnsFrom<ProductItemStock>().NoLock()
                        .Where(ProductItemStock.Columns.InfoGuid).IsEqualTo(pid)
                        .And(ProductItemStock.Columns.ItemGuid).IsEqualTo(did)
                        .ExecuteAsCollection<ProductItemStockCollection>();
        }
        public void ProductItemStockSet(ProductItemStock stock)
        {
            DB.Save(stock);
        }
        #endregion product_item_stock

        #region product_info_import_log
        public ProductInfoImportLogCollection ProductInfoImportLogListGet(Guid gid)
        {
            return DB.SelectAllColumnsFrom<ProductInfoImportLog>().NoLock()
                        .Where(ProductInfoImportLog.Columns.Guid).IsEqualTo(gid)
                        .ExecuteAsCollection<ProductInfoImportLogCollection>();
        }
        public void ProductInfoImportLogSet(ProductInfoImportLog log)
        {
            DB.Save(log);
        }
        #endregion product_info_import_log

        #region product_log
        public ProductLogCollection ProductLogGetListByPid(Guid pid)
        {
            return DB.SelectAllColumnsFrom<ProductLog>().NoLock()
                        .Where(ProductLog.Columns.ProductGuid).IsEqualTo(pid)
                        .ExecuteAsCollection<ProductLogCollection>();
        }
        public void ProductLogSet(ProductLog log)
        {
            DB.Save(log);
        }
        #endregion product_log

        #region view_product_item
        public ViewProductItemCollection ViewProductItemListGetBySellerGuid(Guid sid)
        {
            return DB.SelectAllColumnsFrom<ViewProductItem>().NoLock()
                        .Where(ViewProductItem.Columns.SellerGuid).In(sid)
                        .ExecuteAsCollection<ViewProductItemCollection>();
        }

        public ViewProductItemCollection ViewProductItemListGetBySellerGuidList(List<Guid> sellerGuids)
        {
            return DB.SelectAllColumnsFrom<ViewProductItem>().NoLock()
                        .Where(ViewProductItem.Columns.SellerGuid).In(sellerGuids)
                        .ExecuteAsCollection<ViewProductItemCollection>();
        }

        public List<ViewProductItem> ViewProductItemListGet(int pageStart, int pageLength, List<Guid> sellerGuids, Guid? infoGuid, int productNo,
            string productBrandName, string productName,string gtin, string productCode, string items, int stockStatus, int warehouseType, bool hideDisabled, bool isSimilarProductNo = false)
        {
            ViewProductItemCollection vpis =  DB.SelectAllColumnsFrom<ViewProductItem>().NoLock()
                        .Where(ViewProductItem.Columns.SellerGuid).In(sellerGuids)
                        .ExecuteAsCollection<ViewProductItemCollection>();

            ProductSpecCollection pits = ProductSpecListGetByItemGuid(vpis.Select(x => x.ItemGuid).ToList());

            foreach (ViewProductItem vpi in vpis)
            {
                var ppits = pits.Where(x => x.ItemGuid == vpi.ItemGuid);
                vpi.ItemName = string.Join("_", ppits.OrderBy(t => t.Sort).Select(x => x.SpecName).ToList());
            }

            List<ViewProductItem> vpiList = vpis.ToList();

            if(infoGuid != null && infoGuid != Guid.Empty)
            {
                vpiList = vpiList.Where(x => x.InfoGuid == infoGuid).ToList();
            }

            //商品編號
            if (productNo != 0)
            {
                vpiList = vpiList.Where(x => x.ProductNo == productNo).ToList();
            }
            //商品品牌名稱
            if (!string.IsNullOrEmpty(productBrandName))
            {
                vpiList = vpiList.Where(x => x.ProductBrandName.Contains(productBrandName)).ToList();
            }
            //商品名稱
            if (!string.IsNullOrEmpty(productName))
            {
                vpiList = vpiList.Where(x => x.ProductName.Contains(productName)).ToList();
            }
            //原廠編號(GTINs)
            if (!string.IsNullOrEmpty(gtin))
            {
                vpiList = vpiList.Where(x => x.Gtins == gtin).ToList();
            }
            //貨號資料
            if (isSimilarProductNo)
            {
                if (!string.IsNullOrEmpty(productCode))
                {
                    var infogid = vpiList.Where(x => x.ProductCode.Contains(productCode)).Select(y => y.InfoGuid).FirstOrDefault();
                    vpiList = vpiList.Where(x => x.InfoGuid.Equals(infogid)).ToList();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(productCode))
                {
                    vpiList = vpiList.Where(x => x.ProductCode.Contains(productCode)).ToList();
                }
            }

            //規格
            if (!string.IsNullOrEmpty(items))
            {
                string[] itemList = items.Split(",");
                foreach(string it in itemList)
                {
                    vpiList = vpiList.Where(x => x.ItemName.Contains(it)).ToList();
                }
            }
            //庫存量
            if (stockStatus != 0)
            {
                if (stockStatus == (int)ProductStockFilter.Normal)
                {
                    vpiList = vpiList.Where(x => x.Stock > 0).ToList();
                }
                else if (stockStatus == (int)ProductStockFilter.LessThanBase)
                {
                    vpiList = vpiList.Where(x => x.Stock <= x.SafetyStock && x.Stock > 0).ToList();
                }
                else if (stockStatus == (int)ProductStockFilter.Zero)
                {
                    vpiList = vpiList.Where(x => x.Stock <= 0).ToList();
                }
            }

            //隱藏停用商品
            if (hideDisabled)
            {
                vpiList = vpiList.Where(x => x.ItemStatus == (int)ProductItemStatus.Normal).ToList();
            }

            //是否wms
            vpiList = vpiList.Where(x => x.WarehouseType == warehouseType).ToList();

            return vpiList;
        }


        public ViewProductItemCollection ViewProductItemListGet(
            int pageStart, int pageLength,
            List<Guid> sellerGuids, string productNo,
            string productBrandName, string productName,
            string pchomeProdId, string specs, string productCode)
        {
            //ViewProductItemCollection vpis = DB.SelectAllColumnsFrom<ViewProductItem>().NoLock()
            //            .Where(ViewProductItem.Columns.SellerGuid).In(sellerGuids)
            //            .ExecuteAsCollection<ViewProductItemCollection>();

            SqlQuery query = DB.SelectAllColumnsFrom<ViewProductItem>()
                .Where(ViewProductItem.Columns.SellerGuid).In(sellerGuids)
                .And(ViewProductItem.Columns.WarehouseType).IsEqualTo((int)WarehouseType.Wms);

            if (!string.IsNullOrEmpty(productNo))
            {
                int proNo = 0;
                int.TryParse(productNo, out proNo);
                query.And(ViewProductItem.Columns.ProductNo).IsEqualTo(proNo);
            }
            if (!string.IsNullOrEmpty(productBrandName))
            {
                query.And(ViewProductItem.Columns.ProductBrandName).ContainsString(productBrandName);
            }
            if (!string.IsNullOrEmpty(productName))
            {
                query.And(ViewProductItem.Columns.ProductName).ContainsString(productName);
            }
            if (!string.IsNullOrEmpty(pchomeProdId))
            {
                query.And(ViewProductItem.Columns.PchomeProdId).IsEqualTo(pchomeProdId);
            }
            if (!string.IsNullOrEmpty(specs))
            {
                query.And(ViewProductItem.Columns.ItemName).ContainsString(specs);
            }
            if (!string.IsNullOrEmpty(productCode))
            {
                query.And(ViewProductItem.Columns.ProductCode).IsEqualTo(productCode);
            }

            return query.ExecuteAsCollection<ViewProductItemCollection>() ?? new ViewProductItemCollection();
        }

        public ViewProductItemCollection ViewProductItemListGetByItemGuids(List<Guid> itemGuids)
        {
            var result = new ViewProductItemCollection();
            var idx = 0;
            do
            {
                const int batchLimit = 2000;
                result.AddRange(DB.SelectAllColumnsFrom<ViewProductItem>()
                .Where(ViewProductItem.Columns.ItemGuid).In(itemGuids.Skip(idx).Take(batchLimit))
                .ExecuteAsCollection<ViewProductItemCollection>());
                idx += batchLimit;
            } while (idx <= itemGuids.Count - 1);

            return result;
        }

        public Dictionary<Guid, ViewProductItem> ViewProductItemListGetByWithMpnOrGtin()
        {
            string sql = @"
                select * from view_product_item
                where isnull(gtins, '') != '' 
                or isnull(mpn, '') != '' 
";
            QueryCommand qc = new QueryCommand(sql, ViewProductItem.Schema.Provider.Name);
            ViewProductItemCollection col = new ViewProductItemCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));

            var result = col.ToDictionary(t => t.ItemGuid, t => t);
            return result;
        }

        public ViewProductItem ViewProductItemGet(Guid itemGuid)
        {
            return DB.QueryOver<ViewProductItem>().FirstOrDefault(t => t.ItemGuid == itemGuid);
        }

        #endregion view_product_item

        #region deal_product_delivery
        public DealProductDelivery DealProductDeliveryGetByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<DealProductDelivery>().NoLock()
                        .Where(DealProductDelivery.Columns.BusinessHourGuid).In(bid)
                        .ExecuteSingle<DealProductDelivery>();
        }

        public DealProductDeliveryCollection DealProductDeliveryGetByBid(IEnumerable<Guid> businessHourGuids)
        {
            var infos = new DealProductDeliveryCollection();
            var tempIds = businessHourGuids.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<DealProductDelivery>()
                        .Where(DealProductDelivery.Columns.BusinessHourGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<DealProductDeliveryCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }
        public void DealProductDeliverySet(DealProductDelivery dpDelivery)
        {
            DB.Save(dpDelivery);
        }
        #endregion deal_product_delivery

        #region product_mapping_others
        public List<Guid> GetGuidListByMainBid(Guid mainBid)
        {
            string sql = "select top 12 * from " + ProductMappingOther.Schema.Provider.DelimitDbName(ProductMappingOther.Schema.TableName) + " with(nolock) Where main_business_hour_id=@mainBid order by weight desc";
            QueryCommand qc = new QueryCommand(sql, "");
            qc.AddParameter("@mainBid", mainBid, DbType.Guid);
            ProductMappingOtherCollection rtnCol = new ProductMappingOtherCollection();
            rtnCol.LoadAndCloseReader(DataService.GetReader(qc));

            return rtnCol.Select(c => c.AssociationBusinessHourId).ToList<Guid>();
        }
        #endregion

        /// <summary>
        /// 取得美食/玩美/旅遊熱銷檔次，條件為近6小時銷售最多
        /// </summary>
        /// <returns></returns>
        public List<Guid> GetFoodBeautyTravelHotDealsGuidList()
        {
            string sql = @"
declare @check_time datetime
set @check_time = dateadd(hour , -6, getdate())

select top 17 t2.main_deal_guid from (
select 
(case when cd.MainBusinessHourGuid is null then t1.business_hour_guid else 
cd.MainBusinessHourGuid end) as main_deal_guid, 
t1.business_hour_guid,
t1.sold_num
from (

select ctl.business_hour_guid, count(*) as sold_num from cash_trust_log ctl with(nolock)
inner join deal_property dp with(nolock) on dp.business_hour_guid =ctl.business_hour_guid
where ctl.create_time > @check_time and dp.delivery_type = 1
group by ctl.business_hour_guid

) as t1
left join combo_deals cd with(nolock) on cd.BusinessHourGuid = t1.business_hour_guid
) as t2
where t2.main_deal_guid in 
(
 select cd.bid from category_deals cd with(nolock) where cd.cid in (87,89,90)
)
and t2.main_deal_guid not in 
(
	select cd.bid from category_deals cd with(nolock) where cd.cid = 186
)
group by t2.main_deal_guid
order by sum(t2.sold_num) desc
";
            List<Guid> result = new List<Guid>();
            QueryCommand qc = new QueryCommand(sql, DealProperty.Schema.Provider.Name);
            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    result.Add(reader.GetGuid(0));
                }
            }
            return result;
        }
    }
}
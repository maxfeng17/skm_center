﻿using System.Data.Entity;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class EventEntityProvider : IEventEntityProvider
    {
        public void SaveEventActivityHitLog(EventActivityHitLog log)
        {
            using (var db = new PponDbContext())
            {
                if (log.Id == 0)
                {
                    db.Entry(log).State = EntityState.Added;
                }
                else
                {
                    db.Entry(log).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }
    }
}
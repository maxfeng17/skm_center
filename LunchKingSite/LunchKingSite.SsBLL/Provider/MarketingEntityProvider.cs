﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.MarketingEntities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class MarketingEntityProvider : IMarketingEntityProvider
    {
        public RushbuyTracking RushBuyTrackingGetLast(string cpaKey, string bid)
        {
            using (var db = new MarketingDbContext())
            {
                return db.RushBuyTrackingDbSet.FirstOrDefault(t => t.CpaKey == cpaKey && t.Bid == new Guid(bid));
            }
        }

        public IList<RushbuyTracking> RushBuyTrackingsByDateTime(DateTime start, DateTime end)
        {
            using (var db = new MarketingDbContext())
            {
                return db.RushBuyTrackingDbSet.Where(x => x.CreateTime >= start && x.CreateTime <= end).ToList();
            }
        }

        public void SaveRushBuyTracking(RushbuyTracking rushBuyTracking)
        {
            using (var db = new MarketingDbContext())
            {
                db.Entry(rushBuyTracking).State = EntityState.Added;
                db.SaveChanges();
            }
        }
    }
}

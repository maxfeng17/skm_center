﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.SsBLL
{
    public class SSHiDealProvider : IHiDealProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(typeof(SSHiDealProvider));

        #region HiDealOrder

        public bool HiDealOrderSet(HiDealOrder order)
        {
            if (order.UserId == 0)
            {
                log.WarnFormat("HiDealOrder 新增有問題.不預期會有 UserId 為 0 的操作. time={0}, user={1} .",
                    order.CreateTime, order.CreateId);
            }
            DB.Save<HiDealOrder>(order);
            return true;
        }
        public HiDealOrder HiDealOrderGet(int orderPk)
        {
            return DB.Get<HiDealOrder>(orderPk);
        }
        public HiDealOrder HiDealOrderGet(Guid orderGuid)
        {
            return DB.Get<HiDealOrder>(HiDealOrder.Columns.Guid, orderGuid);
        }
        public HiDealOrder HiDealOrderGet(string column, object value)
        {
            return DB.Get<HiDealOrder>(column, value);
        }
        public HiDealOrder HiDealOrderGetByOrderId(string orderId, int userId)
        {
            return DB.SelectAllColumnsFrom<HiDealOrder>().Where(HiDealOrder.Columns.OrderId).IsEqualTo(orderId).And(
                HiDealOrder.Columns.UserId).IsEqualTo(userId).ExecuteSingle<HiDealOrder>();
        }
        public HiDealOrderCollection HiDealOrderGetListAtConfirm(DateTime beforeCreateTime)
        {
            return
                Select.AllColumnsFrom<HiDealOrder>().Where(HiDealOrder.Columns.OrderStatus).IsEqualTo(HiDealOrderStatus.Confirm)
                .And(HiDealOrder.Columns.CreateTime).IsLessThan(beforeCreateTime)
                .ExecuteAsCollection<HiDealOrderCollection>();
        }
        #endregion HiDealOrder

        #region HiDealOrderDetail

        public bool HiDealOrderDetailSetList(HiDealOrderDetailCollection orderDetails)
        {
            DB.SaveAll(orderDetails);
            return true;
        }
        public HiDealOrderDetail HiDealOrderDetailGet(int orderDetailId)
        {
            return DB.Get<HiDealOrderDetail>(orderDetailId);
        }

        public HiDealOrderDetail HiDealOrderDetailGet(Guid orderDetailGuid)
        {
            return DB.SelectAllColumnsFrom<HiDealOrderDetail>()
                .Where(HiDealOrderDetail.Columns.Guid).IsEqualTo(orderDetailGuid)
                .ExecuteSingle<HiDealOrderDetail>();
        }

        public HiDealOrderDetailCollection HiDealOrderDetailGetListByOrderGuid(Guid orderGuid, HiDealProductType productType)
        {
            return DB.SelectAllColumnsFrom<HiDealOrderDetail>().Where(HiDealOrderDetail.Columns.HiDealOrderGuid).IsEqualTo(orderGuid)
                .And(HiDealOrderDetail.Columns.ProductType).IsEqualTo((int)productType).ExecuteAsCollection<HiDealOrderDetailCollection>();
        }
        public HiDealOrderDetailCollection HiDealOrderDetailGetListByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<HiDealOrderDetail>().Where(HiDealOrderDetail.Columns.HiDealOrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<HiDealOrderDetailCollection>();
        }

        public HiDealOrderDetailCollection HiDealOrderDetailGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<HiDealOrderDetail>().Where(HiDealOrderDetail.Columns.ProductId).IsEqualTo(productId)
                .ExecuteAsCollection<HiDealOrderDetailCollection>();
        }


        #endregion HiDealOrderDetail

        #region HiDealOrderShow

        public bool HiDealOrderShowSet(HiDealOrderShow order)
        {
            if (order.UserId == 0)
            {
                log.WarnFormat("HiDealOrderShow 有問題.不預期會有 UserId 為 0 的操作. OrderGuid={0}, CreateTime={1}.",
                    order.OrderGuid, order.OrderCreateTime);
            }
            DB.Save<HiDealOrderShow>(order);
            return true;
        }
        public HiDealOrderShow HiDealOrderShowGet(Guid orderGuid)
        {
            return DB.Get<HiDealOrderShow>(orderGuid);
        }
        public HiDealOrderShow HiDealOrderShowGetByOrderPk(int orderPk)
        {
            return DB.Get<HiDealOrderShow>(HiDealOrderShow.Columns.OrderPk, orderPk);
        }
        public HiDealOrderShowCollection GetHiDealOrderShowListByUser(int pageStart, int pageLength, int userId, string orderBy, string filter)
        {
            string defOrderBy = HiDealOrderShow.Columns.OrderCreateTime;
            string sql = @"SELECT * from " + HiDealOrderShow.Schema.TableName + " WHERE " + HiDealOrderShow.Columns.UserId + "=@userId " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
            {
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy + " desc");
            }
            HiDealOrderShowCollection hdosc = new HiDealOrderShowCollection();
            hdosc.LoadAndCloseReader(DataService.GetReader(qc));
            return hdosc;
        }
        public int GetHiDealOrderShowListCount(int userId, string filter)
        {
            string sql = @"select count(1) from " + HiDealOrderShow.Schema.TableName + " where " + HiDealOrderShow.Columns.UserId + "=@userId " + filter;
            QueryCommand qc = new QueryCommand(sql, ViewPponCoupon.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.String);
            return (int)DataService.ExecuteScalar(qc);
        }
        public int HidealRemainCouponCountGet(Guid hidealOrderGuid)
        {
            return DB.SelectAllColumnsFrom<CashTrustLog>().Where(CashTrustLog.Columns.OrderGuid).IsEqualTo(hidealOrderGuid)
                    .And(CashTrustLog.Columns.Status).In(TrustStatus.Initial, TrustStatus.Trusted).GetRecordCount();
        }
        #endregion HiDealOrderShow

        #region HiDealFreight

        public HiDealFreightCollection HiDealFreightGetListByProductId(int productId, HiDealFreightType type)
        {
            return
                DB.SelectAllColumnsFrom<HiDealFreight>().Where(HiDealFreight.Columns.ProductId).IsEqualTo(productId).And
                    (HiDealFreight.Columns.FreightType).IsEqualTo((int)type).
                    OrderAsc(HiDealFreight.Columns.StartAmount).ExecuteAsCollection<HiDealFreightCollection>();
        }

        public void HiDealFreightDelete(HiDealFreight freight)
        {
            DB.Delete(freight);
        }

        public int HiDealFreightSet(HiDealFreight deal)
        {
            return DB.Save<HiDealFreight>(deal);
        }

        public HiDealFreight HiDealFreightGetLatest(int productId, HiDealFreightType type)
        {
            return
                DB.SelectAllColumnsFrom<HiDealFreight>().Top("1").Where(HiDealFreight.ProductIdColumn).IsEqualTo(productId).
                    And(HiDealFreight.FreightTypeColumn).IsEqualTo(type).OrderDesc(HiDealFreight.Columns.EndAmount).
                    ExecuteSingle<HiDealFreight>();
        }

        #endregion HiDealFreight

        #region HiDealCity
        public void HiDealCitySet(HiDealCity city)
        {
            DB.Save<HiDealCity>(city);
        }

        public int? HiDealSpecialIdGet(int dealId)
        {
            string sql =
                "select top 1 code_id from hi_deal_special_discount where hi_deal_special_discount.hi_deal_id = @dealId";
            int? result = new InlineQuery().ExecuteScalar<int?>(sql, dealId);
            return result;
        }

        public HiDealCityCollection HiDealCityGetListByDealId(int dealId)
        {
            HiDealCityCollection cityCollection = DB.Select().From<HiDealCity>().Where(HiDealCity.HiDealIdColumn).IsEqualTo(dealId).ExecuteAsCollection
                <HiDealCityCollection>();
            return cityCollection ?? new HiDealCityCollection();
        }

        public void HiDealCityDealTimeSet(int dealId, DateTime startTime, DateTime endTime, List<string> regionNames, bool IsAlwaysMain)
        {

            #region 查詢 system_code, 先找出 code_id, code_name. 要用來 insert 到 hi_deal_city.

            Dictionary<int, string> regions = new Dictionary<int, string>();
            DataSet ds = DB.Select(SystemCode.Columns.CodeId, SystemCode.Columns.CodeName)
                .From<SystemCode>().Where(SystemCode.CodeNameColumn).In(regionNames)
                .And(SystemCode.CodeGroupColumn).IsEqualTo("HiDealRegion").ExecuteDataSet();
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                regions.Add(int.Parse(row[0].ToString()), row[1].ToString());
            }

            #endregion

            #region 查目前 hi_deal_city 所有的區域 id (更新前的區域)

            List<int> dbRegionIdList = new List<int>();
            Query queryRegionId = new Query(HiDealCity.Schema);
            queryRegionId.DISTINCT();
            queryRegionId.SelectList = HiDealCity.Columns.CityCodeId;
            queryRegionId.WHERE(HiDealCity.Columns.HiDealId, dealId);
            queryRegionId.ORDER_BY(HiDealCity.Columns.CityCodeId);
            DataSet regionIdDataSet = queryRegionId.ExecuteDataSet();
            foreach (DataRow row in regionIdDataSet.Tables[0].Rows)
            {
                dbRegionIdList.Add((int)row[HiDealCity.Columns.CityCodeId]);
            }

            #endregion

            #region setup timeRanges 算出所有的 time slots

            Dictionary<DateTime, DateTime> timeRanges = new Dictionary<DateTime, DateTime>();
            TimeSpan tsStart = new TimeSpan(startTime.Ticks);
            TimeSpan tsEnd = new TimeSpan(endTime.Ticks);
            if ((tsEnd - tsStart).TotalMinutes <= 1440)
            {
                timeRanges.Add(startTime, endTime);  //extreme case: start hour >= 12 && end hour <= 12 && (end date - start date == 1) => one hi_deal_city record only
            }
            else
            {
                //additional start slot
                if (startTime.Hour < 12)
                {
                    DateTime addtionalStartSlotTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, 12, 0, 0);
                    timeRanges.Add(startTime, addtionalStartSlotTime);
                }

                //add standard slots
                DateTime standardTime = new DateTime(startTime.Year, startTime.Month, startTime.Day, 12, 0, 0);
                while (standardTime.AddDays(1) <= endTime)
                {
                    DateTime plusOne = standardTime.AddDays(1);
                    timeRanges.Add(standardTime, plusOne);
                    standardTime = standardTime.AddDays(1);
                }

                //modify first standard slot if start time > 12:00
                if (startTime.Hour > 12 || (startTime.Hour == 12 && startTime.Minute > 0))
                {
                    timeRanges.Remove(new DateTime(startTime.Year, startTime.Month, startTime.Day, 12, 0, 0));
                    timeRanges.Add(startTime, new DateTime(startTime.Year, startTime.Month, startTime.Day + 1, 12, 0, 0));
                }

                //add last standard slot if end time < 12:00
                if (endTime.Hour < 12)
                {
                    DateTime minusOne = new DateTime((endTime.AddDays(-1)).Year, (endTime.AddDays(-1)).Month, (endTime.AddDays(-1)).Day, 12, 0, 0);
                    timeRanges.Add(minusOne, endTime);
                }

                //additional end slot
                if (endTime.Hour > 12 || (endTime.Hour == 12 && endTime.Minute > 0))
                {
                    DateTime addtionalEndSlotTime = new DateTime(endTime.Year, endTime.Month, endTime.Day, 12, 0, 0);
                    timeRanges.Add(addtionalEndSlotTime, endTime);
                }
            }

            #endregion

            DateTime lastStartTime = DateTime.MinValue;
            foreach (KeyValuePair<DateTime, DateTime> pair in timeRanges)
            {
                if (pair.Value == endTime)
                    lastStartTime = pair.Key;
            }

            #region delete out of range / region data

            string regKeys = string.Join(",", regions.Keys.ToArray());
            if (string.IsNullOrEmpty(regKeys))
                regKeys = "''";
            string delSql = string.Format(@"DELETE FROM hi_deal_city 
																		  WHERE hi_deal_id = @dealId 
																			  AND (
																				  (start_time < @firstSTime AND end_time != @firstETime)
																				  OR (start_time > @lastSTime AND end_time != @lastETime)
																				  OR city_code_id NOT IN ({0})
																			  )", regKeys);
            new InlineQuery().Execute(delSql, dealId, startTime.ToString("yyyy-MM-dd HH:mm:ss"), timeRanges[startTime].ToString("yyyy-MM-dd HH:mm:ss"), lastStartTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"));

            #endregion

            #region modify database 舊資料的 start_time / end_time 更新

            string modStartTimeSql = @"UPDATE hi_deal_city
																	SET start_time = 
																			(CASE
																				WHEN (end_time = @firstETime) THEN @firstSTime
																				ELSE DATEADD(hh, 12 - DATEPART(hh,start_time), start_time)
																			 END
																			)
																	WHERE hi_deal_id = @dealId 
																	AND ( (end_time = @firstETime AND start_time != @firstSTime) OR DATEPART(hh,start_time) != 12)";
            new InlineQuery().Execute(modStartTimeSql, timeRanges[startTime].ToString("yyyy-MM-dd HH:mm:ss"), startTime.ToString("yyyy-MM-dd HH:mm:ss"),
                dealId, timeRanges[startTime].ToString("yyyy-MM-dd HH:mm:ss"), startTime.ToString("yyyy-MM-dd HH:mm:ss"));

            string modEndTimeSql = @"UPDATE hi_deal_city
																  SET end_time = 
																		  (CASE
																			  WHEN (start_time = @lastSTime) THEN @lastETime
																			  ELSE DATEADD(hh, 12 - DATEPART(hh,end_time), end_time)
																			  END
																		  )
																  WHERE hi_deal_id = @dealId 
																  AND ((start_time = @lastSTime AND end_time != @lastETime) OR DATEPART(hh,end_time) != 12)";
            new InlineQuery().Execute(modEndTimeSql, lastStartTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"),
                dealId, lastStartTime.ToString("yyyy-MM-dd HH:mm:ss"), endTime.ToString("yyyy-MM-dd HH:mm:ss"));

            #endregion

            #region 查目前 hi_deal_city 所有 start_time (必須在新增 new regions 資料前, 更新舊資料後執行. old regions 新增時, 排除 time slots用. )

            List<DateTime> dbStartTimes = new List<DateTime>();
            Query q = new Query(HiDealCity.Schema);
            q.DISTINCT();
            q.SelectList = HiDealCity.Columns.StartTime;
            q.WHERE(HiDealCity.Columns.HiDealId, dealId);
            DataSet startTimeDataSet = q.ExecuteDataSet();
            foreach (DataRow row in startTimeDataSet.Tables[0].Rows)
            {
                dbStartTimes.Add((DateTime)row[HiDealCity.Columns.StartTime]);
            }

            #endregion

            #region new regions insert

            foreach (KeyValuePair<int, string> region in regions)
            {
                if (dbRegionIdList.Contains(region.Key))
                    continue;
                foreach (KeyValuePair<DateTime, DateTime> range in timeRanges)
                {
                    HiDealCity dc = new HiDealCity
                    {
                        HiDealId = dealId,
                        CityCodeGroup = "HiDealRegion",
                        CityCodeId = region.Key,
                        City = region.Value,
                        StartTime = range.Key,
                        EndTime = range.Value
                    };
                    DB.Insert(dc);
                }
            }

            #endregion

            #region remove items inside timeRanges, leave only needed times for old regions

            foreach (DateTime time in dbStartTimes)
            {
                if (timeRanges.ContainsKey(time))
                {
                    timeRanges.Remove(time);
                }
            }

            #endregion

            #region old regions insert

            foreach (KeyValuePair<int, string> region in regions)
            {
                if (!dbRegionIdList.Contains(region.Key))
                    continue;
                foreach (KeyValuePair<DateTime, DateTime> range in timeRanges)
                {
                    HiDealCity dc = new HiDealCity
                    {
                        HiDealId = dealId,
                        CityCodeGroup = "HiDealRegion",
                        CityCodeId = region.Key,
                        City = region.Value,
                        StartTime = range.Key,
                        EndTime = range.Value
                    };
                    DB.Insert(dc);
                }
            }

            #endregion

            #region update [is_main]

            string updateIsAlwaysMain =
                @"UPDATE hi_deal_city
					SET is_main = 1
					WHERE hi_deal_id = @dealId";
            string updateIsMain =
                string.Format(@"UPDATE hi_deal_city
					SET is_main = 0
					WHERE hi_deal_id = @dealId
					AND start_time >= (SELECT MAX(start_time) - {0} + 1 FROM hi_deal_city WHERE hi_deal_id=@dealId)", config.HiDealLastNDays);

            new InlineQuery().Execute(updateIsAlwaysMain, dealId);
            if (!IsAlwaysMain)
            {
                new InlineQuery().Execute(updateIsMain, dealId, dealId);
            }

            #endregion
        }

        public void HiDealCityCollectionUpdateSequence(int id, int seq)
        {
            string sql = "update " + HiDealCity.Schema.TableName + " set " + HiDealCity.Columns.Seq + "=@seq where "
                + HiDealCity.Columns.Id + " = @id ";
            QueryCommand qc = new QueryCommand(sql, HiDealCity.Schema.Provider.Name);
            qc.AddParameter("@seq", seq, DbType.Int32);
            qc.AddParameter("@id", id, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public int HiDealCityGetCityCountByPidAndTime(int productId, DateTime checkTime)
        {
            var strSql = "select count(*) from " + HiDealCity.Schema.TableName +
                         " where hi_deal_id = @pid " +
                         " and start_time <= @checkTime and end_time > @checkTime";
            var qc = new QueryCommand(strSql, HiDealCity.Schema.Provider.Name);
            qc.AddParameter("@pid", productId, DbType.Int64);
            qc.AddParameter("@checkTime", checkTime, DbType.DateTime);
            return (int)DataService.ExecuteScalar(qc);
        }

        public HiDealCityCollection HiDealCityGetListByCityIdAndDate(int cityId, DateTime startTime, DateTime endTime)
        {            
            HiDealCityCollection cityCollection = DB.Select().From<HiDealCity>()
                .Where(HiDealCity.CityCodeIdColumn).IsEqualTo(cityId)
                .And(HiDealCity.CityCodeGroupColumn).IsEqualTo("HiDealRegion")
                .And(HiDealCity.StartTimeColumn).IsEqualTo(startTime)//.And(HiDealCity.EndTimeColumn).IsEqualTo(endTime)
                .OrderDesc(HiDealCity.Columns.IsMain)
                .OrderAsc(HiDealCity.Columns.Seq)
                .ExecuteAsCollection<HiDealCityCollection>();
            return cityCollection ?? new HiDealCityCollection();
        }

        public HiDealCityCollection HiDealCityTodayDealsGetByCityId(int cityId, DateTime date)
        {
            HiDealCityCollection cityCollection = DB.Select().From<HiDealCity>()
                .Where(HiDealCity.CityCodeIdColumn).IsEqualTo(cityId).And(HiDealCity.IsMainColumn).IsEqualTo(1)
                .And(HiDealCity.StartTimeColumn).IsLessThanOrEqualTo(date).And(HiDealCity.EndTimeColumn).IsGreaterThan(date)
                .OrderAsc(HiDealCity.Columns.Seq)
                .ExecuteAsCollection<HiDealCityCollection>();
            return cityCollection ?? new HiDealCityCollection();
        }

        public HiDealCity HiDealCityGetByCityIdAndDealIdAndDate(int cityId, int dealId, DateTime startTime, DateTime endTime)
        {
            HiDealCity cityDeal = DB.Select().From<HiDealCity>().Where(HiDealCity.HiDealIdColumn).IsEqualTo(dealId)
                .And(HiDealCity.CityCodeIdColumn).IsEqualTo(cityId)
                .And(HiDealCity.StartTimeColumn).IsEqualTo(startTime)//.And(HiDealCity.EndTimeColumn).IsEqualTo(endTime)
                .ExecuteSingle<HiDealCity>();
            return cityDeal ?? new HiDealCity();
        }
        public HiDealCityCollection HiDealCityGetListByDateWithFirstDealOfAllCity(DateTime startTime)
        {
            HiDealCityCollection cityCollection = DB.Select().From<HiDealCity>()
                .Where(HiDealCity.CityCodeGroupColumn).IsEqualTo("HiDealRegion")
                .And(HiDealCity.StartTimeColumn).IsEqualTo(startTime)
                .And(HiDealCity.EndTimeColumn).IsEqualTo(startTime.AddDays(+1))
                .And(HiDealCity.SeqColumn).IsEqualTo(1)
                .And(HiDealCity.IsMainColumn).IsEqualTo(1)
                .ExecuteAsCollection<HiDealCityCollection>();
            return cityCollection ?? new HiDealCityCollection();
        }
        #endregion

        #region HiDealCategory

        public void HiDealCategorySet(HiDealCategory category)
        {
            DB.Save<HiDealCategory>(category);
        }
        public HiDealCategoryCollection HiDealDealCategoryGetListByDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealCategory>().Where(HiDealCategory.HiDealIdColumn).IsEqualTo(
                dealId).ExecuteAsCollection<HiDealCategoryCollection>() ?? new HiDealCategoryCollection();
        }
        public bool HiDealCategoryMerge(int dealId, List<int> catgIds)
        {
            catgIds.Add(0);

            //
            DB.Delete().From<HiDealCategory>().Where(HiDealCategory.HiDealIdColumn).IsEqualTo(dealId).
                And(HiDealCategory.CategoryCodeIdColumn).NotIn(catgIds).Execute();

            //
            string sql = string.Format(
                @"INSERT INTO hi_deal_category(hi_deal_id, category_code_id, category_code_group, category_code_name)
					SELECT  T.id, system_code.code_id, system_code.code_group, system_code.code_name 
					FROM system_code CROSS JOIN (SELECT @dealId) as T(id)
					WHERE system_code.code_id in ({0}) 
					AND system_code.code_group = @catg
					AND NOT EXISTS (SELECT * FROM hi_deal_category WHERE hi_deal_id = @dealId AND category_code_id = system_code.code_id)", string.Join(",", catgIds.ToArray()));
            new InlineQuery().Execute(sql, dealId, "HiDealCatg", dealId);

            return true;
        }
        #endregion

        #region HiDealSpecialDiscount

        public HiDealSpecialDiscountCollection HiDealSpecialDiscountGetListByDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealSpecialDiscount>().Where(HiDealSpecialDiscount.HiDealIdColumn).IsEqualTo(
                dealId).ExecuteAsCollection<HiDealSpecialDiscountCollection>() ?? new HiDealSpecialDiscountCollection();
        }
        public bool HiDealSpecialDiscountMerge(int dealId, List<int> discountIds)
        {
            if (discountIds == null || discountIds.Count == 0)
            {
                DB.Delete().From<HiDealSpecialDiscount>().Where(HiDealSpecialDiscount.HiDealIdColumn).IsEqualTo(
                    dealId).Execute();
                return true;
            }

            //
            DB.Delete().From<HiDealSpecialDiscount>().Where(HiDealSpecialDiscount.HiDealIdColumn).IsEqualTo(dealId).
                And(HiDealSpecialDiscount.CodeIdColumn).NotIn(discountIds).Execute();

            //
            string sql = string.Format(
                @"INSERT INTO hi_deal_special_discount(hi_deal_id, code_id, code_group, code_name)
					SELECT  T.id, system_code.code_id, system_code.code_group, system_code.code_name 
					FROM system_code CROSS JOIN (SELECT @dealId) as T(id)
					WHERE system_code.code_id in ({0}) 
					AND system_code.code_group = @catg
					AND NOT EXISTS (SELECT * FROM hi_deal_special_discount WHERE hi_deal_id = @dealId AND code_id = system_code.code_id)", string.Join(",", discountIds.ToArray()));
            new InlineQuery().Execute(sql, dealId, "HiDealSpecialDiscount", dealId);

            return true;
        }
        #endregion

        #region HiDealDeals
        public HiDealDeal HiDealDealGet(int id)
        {
            return DB.Get<HiDealDeal>(id);
        }
        public HiDealDeal HiDealDealGet(Guid guid)
        {
            return DB.Get<HiDealDeal>(HiDealDeal.Columns.HiDealGuid, guid);
        }
        public int HiDealDealSet(HiDealDeal deal)
        {
            return DB.Save<HiDealDeal>(deal);
        }
        public HiDealDealCollection HiDealDealGetListByPeriod(DateTime dateStart, DateTime dateEnd)
        {
            return DB.SelectAllColumnsFrom<HiDealDeal>().Where(HiDealDeal.Columns.DealStartTime).IsBetweenAnd(dateStart, dateEnd).ExecuteAsCollection<HiDealDealCollection>();
        }
        public HiDealDealCollection HiDealDealGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<HiDealDeal, HiDealDealCollection>(0, -1, null, filter);
        }
        #endregion

        #region HiDealContent
        public void HiDealContentSet(HiDealContent content)
        {
            DB.Save<HiDealContent>(content);
        }
        public HiDealContentCollection HiDealContentGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = HiDealContent.Columns.CreateTime + " desc";
            QueryCommand qc = GetHDWhere(HiDealContent.Schema, filter);
            qc.CommandSql = "select * from " + HiDealContent.Schema.Provider.DelimitDbName(HiDealContent.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            HiDealContentCollection vfcCol = new HiDealContentCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public HiDealContentCollection HiDealContentGetAllByDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealContent>().Where(HiDealContent.RefIdColumn).IsEqualTo(dealId)
                    .And(HiDealContent.RefTypeColumn).IsEqualTo(0)
                    .ExecuteAsCollection<HiDealContentCollection>() ?? new HiDealContentCollection();
        }

        #endregion

        #region HiDealProduct
        public HiDealProduct HiDealProductGet(int id)
        {
            return DB.Get<HiDealProduct>(id);
        }

        public HiDealProduct HiDealProductGet(Guid prodGuid)
        {
            return DB.Get<HiDealProduct>(HiDealProduct.Columns.Guid, prodGuid);
        }

        public int HiDealProductSet(HiDealProduct product)
        {
            return DB.Save<HiDealProduct>(product);
        }

        public bool IsDuplicateHiDealShipNo(int pid, string shipNo, int? orderShipId)
        {
            string sql = string.Format(@"
            SELECT count(1) from hi_deal_product hdp
            join hi_deal_order_detail od ON od.product_id = hdp.id
            join order_ship os ON os.order_guid = od.hi_deal_order_guid and os.type = 1  
            where hdp.id = '{0}' and os.ship_no='{1}'
           ", pid, shipNo);
            if (orderShipId != null)
            {
                sql += " and os.id <> " + orderShipId;
            }
            int odc = new InlineQuery().ExecuteScalar<int>(sql);
            return odc > 0;
        }
        public HiDealProductCollection HiDealProductGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {

            string defOrderBy = HiDealProduct.Columns.CreateTime + " desc";
            QueryCommand qc = GetHDWhere(HiDealProduct.Schema, filter);
            qc.CommandSql = "select * from " + HiDealProduct.Schema.Provider.DelimitDbName(HiDealProduct.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            HiDealProductCollection vfcCol = new HiDealProductCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public int HiDealProductGetCount(params string[] filter)
        {
            string providerName = HiDealProduct.Schema.Provider.Name;
            string tableName = HiDealProduct.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + HiDealProduct.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public HiDealProductCollection HiDealProductGetList(IEnumerable<int> productIds)
        {
            HiDealProductCollection result = new HiDealProductCollection();

            List<int> tempPids = productIds.Distinct().ToList();
            int idx = 0;
            while(idx <= tempPids.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<HiDealProduct>()
                    .Where(HiDealProduct.Columns.Id).In(tempPids.Skip(idx).Take(batchLimit))
                    .ExecuteAsCollection<HiDealProductCollection>();
                result.AddRange(batchProd);
                idx += batchLimit;
            }

            return result;        
        }

        public HiDealProductCollection HiDealProductCollectionGet(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealProduct>().Where(HiDealProduct.DealIdColumn).IsEqualTo(dealId).
                    ExecuteAsCollection<HiDealProductCollection>();
        }
 
        public HiDealProductCollection HiDealProductCollectionGetListByNoOrderShip()
        {
            string sql = string.Format(@"
            select * FROM {0} where id IN(
                Select distinct hdp.id FROM {1} hdp
                JOIN {2} od on od.product_id = hdp.id
                join {3} o ON od.hi_deal_id = o.hi_deal_id
                left join {4} os on os.order_guid = o.guid and os.type = 1
                where hdp.vendor_billing_model = {5} and os.ship_time is null
                and o.order_status = {6} and hdp.shipped_date is null
            )", HiDealProduct.Schema.TableName, HiDealProduct.Schema.TableName, HiDealOrderDetail.Schema.TableName, HiDealOrder.Schema.TableName, 
              OrderShip.Schema.TableName, (int)VendorBillingModel.BalanceSheetSystem, (int)HiDealOrderStatus.Completed);
            var qc = new QueryCommand(sql, HiDealProduct.Schema.Provider.Name);
            var vpolCol = new HiDealProductCollection();
            vpolCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vpolCol;
        }

        #endregion

        #region HiDealProductCost
        public int HiDealProductCostSet(HiDealProductCost productCost)
        {
            return DB.Save<HiDealProductCost>(productCost);
        }

        public HiDealProductCostCollection HiDealProductCostCollectionGetByProductId(int pid)
        {
            return DB.SelectAllColumnsFrom<HiDealProductCost>().Where(HiDealProductCost.Columns.ProductId).IsEqualTo(pid).
                OrderAsc(HiDealProductCost.Columns.Id).ExecuteAsCollection<HiDealProductCostCollection>();
        }

        public HiDealProductCost HiDealProductCostGetLatest(int pid)
        {
            return
                DB.SelectAllColumnsFrom<HiDealProductCost>().Top("1").Where(HiDealProductCost.ProductIdColumn).IsEqualTo(pid).
                    OrderDesc(HiDealProductCost.Columns.Id).ExecuteSingle<HiDealProductCost>();
        }

        public void HiDealProductCostDelete(HiDealProductCost productCost)
        {
            DB.Delete(productCost);
        }

        #endregion

        #region HiDealProductOptionCategory

        public int HiDealProductOptionCategorySet(HiDealProductOptionCategory category)
        {
            return DB.Save<HiDealProductOptionCategory>(category);
        }

        public HiDealProductOptionCategoryCollection HiDealProductOptionCategoryGetListByProductId(int pid)
        {
            return
                DB.SelectAllColumnsFrom<HiDealProductOptionCategory>().Where(
                    HiDealProductOptionCategory.Columns.ProductId).IsEqualTo(pid).OrderAsc(
                        HiDealProductOptionCategory.Columns.Seq).ExecuteAsCollection<HiDealProductOptionCategoryCollection>();
        }

        public void HiDealProductOptionCategoryDelete(int catgId)
        {
            DB.Delete().From<HiDealProductOptionItem>().Where(HiDealProductOptionItem.CatgIdColumn).IsEqualTo(catgId).Execute();
            DB.Delete().From<HiDealProductOptionCategory>().Where(HiDealProductOptionCategory.IdColumn).IsEqualTo(catgId).Execute();
        }

        #endregion

        #region HiDealProductOptionItem

        public int HiDealProductOptionItemSet(HiDealProductOptionItem item)
        {
            return DB.Save<HiDealProductOptionItem>(item);
        }

        public HiDealProductOptionItemCollection HiDealProductOptionItemGetListdByCategoryId(int cid)
        {
            return
                DB.SelectAllColumnsFrom<HiDealProductOptionItem>().Where(HiDealProductOptionItem.Columns.CatgId).
                    IsEqualTo(cid).OrderAsc(HiDealProductOptionItem.Columns.Seq).ExecuteAsCollection
                    <HiDealProductOptionItemCollection>();
        }

        public void HiDealProductOptionItemDeleteById(int itemId)
        {
            DB.Delete().From<HiDealProductOptionItem>().Where(HiDealProductOptionItem.IdColumn).IsEqualTo(itemId).Execute();
        }
        public void HiDealProductOptionItemUpdateSellQuantity(List<int> idList, int modifyQuantity)
        {
            string sql = "update " + HiDealProductOptionItem.Schema.TableName + " set " +
                         HiDealProductOptionItem.Columns.SellQuantity + " = " +
                         HiDealProductOptionItem.Columns.SellQuantity + " + ( @modifyQuantity ) " +
                         " where " + HiDealProductOptionItem.Columns.Id + " in ( " + string.Join(",", idList) + " ) ";
            QueryCommand qc = new QueryCommand(sql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@modifyQuantity", modifyQuantity, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }


        #endregion

        #region ViewHiDeals
        public ViewHiDealCollection ViewHiDealGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {

            string defOrderBy = ViewHiDeal.Columns.DealCreateTime + " desc";
            QueryCommand qc = GetHDWhere(ViewHiDeal.Schema, filter);
            qc.CommandSql = "select * from " + ViewHiDeal.Schema.Provider.DelimitDbName(ViewHiDeal.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            ViewHiDealCollection vfcCol = new ViewHiDealCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        protected QueryCommand GetHDWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        /// <summary>
        /// 上檔3天「內」的visa優先檔次 與 visa 專屬檔次
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public ViewHiDealCollection ViewHiDealFreshVisaPriority(DateTime startTime, DateTime endTime, int cityId)
        {
            /*
             * special_code_id = 2: visa優先檔次
             * category_code_id = 0: 分類 全部
             * datediff(hour, deal_start_time, getdate())<72 72小時內
             */

            string strSQL =
                @"select  * from view_hi_deal with(nolock) 
                    where city_code_id=@cityId and category_code_id = 0
                    and city_start_time between @startTime and @endTime 
                    and (special_code_id = 1 or (special_code_id = 2 and datediff(hour, deal_start_time, getdate())<72))
                    order by special_code_id desc,deal_is_always_main desc, deal_id desc";

            QueryCommand qc = new QueryCommand(strSQL, ViewHiDeal.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);

            ViewHiDealCollection vhdCol = new ViewHiDealCollection();
            vhdCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdCol;
        }

        /// <summary>
        /// 上檔3天「後」的visa優先檔次
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public ViewHiDealCollection ViewHiDealStaleVisaPriority(DateTime startTime, DateTime endTime, int cityId)
        {
            /*
             * special_code_id = 2: visa優先檔次
             * category_code_id = 0: 分類 全部
             * datediff(hour, deal_start_time, getdate())<72 72小時內
             */
            string strSQL =
                @"select  * from view_hi_deal with(nolock) 
                    where city_code_id=@cityId and category_code_id = 0
                    and city_start_time between @startTime and @endTime 
                    and special_code_id = 2 and datediff(hour, deal_start_time, getdate())>=72
                    order by special_code_id desc,deal_is_always_main desc, deal_id desc";

            QueryCommand qc = new QueryCommand(strSQL, ViewHiDeal.Schema.Provider.Name);
            qc.AddParameter("@cityId", cityId, DbType.Int32);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);

            ViewHiDealCollection vhdCol = new ViewHiDealCollection();
            vhdCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdCol;
        }

        public ViewHiDealCollection ViewHiDealGetTodayDeals(int city, string category, int? special, bool isMain,
                                                            string orderBy)
        {
            string strIsMain = "0";
            string defOrderBy = ViewHiDeal.Columns.CitySeq;
            if (isMain)
                strIsMain = "1";
            

            string strSQL = "";
            if (special == null)
            {
                if (config.IsVisa2013)
                {
                    strSQL =
                        @"select  * from view_hi_deal with(nolock)
where city_code_id=@city and category_code_id=@category
and GETDATE() between " + ViewHiDeal.Columns.CityStartTime + " and " + ViewHiDeal.Columns.CityEndTime +
                        " and " + ViewHiDeal.Columns.IsMain + " =" + strIsMain
                        + " and (special_code_id is null"
                        + " or (special_code_id = 2 and datediff(hour, deal_start_time, getdate())>=72))"; 
                        // visa 優先的檔次，3天後會回到一般檔次，用小時算才精確
                }
                else
                {
                    strSQL =
                        @"select  * from view_hi_deal with(nolock)
where city_code_id=@city and category_code_id=@category
and GETDATE() between " + ViewHiDeal.Columns.CityStartTime + " and " + ViewHiDeal.Columns.CityEndTime +
                        " and " + ViewHiDeal.Columns.IsMain + " =" + strIsMain
                        + " and (special_code_id is null"
                        + " or special_code_id = 1)";
                }
            }
            else if (special == 1) //VISA 專屬
            {
                category = "0"; //全部
                city = 2; // 總覽
                strSQL =
                    @"select  * from view_hi_deal with(nolock)
where city_code_id=@city and category_code_id=@category
and GETDATE() between " + ViewHiDeal.Columns.CityStartTime + " and " + ViewHiDeal.Columns.CityEndTime +
                    " and " + ViewHiDeal.Columns.IsMain + " =" + strIsMain + " and special_code_id = 1";

            }
            else if (special == 2) //VISA 優先
            {
                category = "0";
                city = 2;
                strSQL =
                    @"select  * from view_hi_deal with(nolock)
where city_code_id=@city and category_code_id=@category
and GETDATE() between " + ViewHiDeal.Columns.CityStartTime + " and " + ViewHiDeal.Columns.CityEndTime +
                    " and " + ViewHiDeal.Columns.IsMain + " =" + strIsMain + " and special_code_id = 2" +
                    " and datediff(hour, deal_start_time ,getdate()) < 72";
            }
            //TODO 原本的查詢語法，Visa 2013 穩定後刪除
            /* 
                        string sql = (@"select  * from {view_hi_deal} with(nolock)
            where {city_code_id}=@city and {category_code_id}=@category
            and GETDATE() between {city_start_time} and {city_end_time}
                                 and {is_main} =" + strIsMain + " and special_code_id = 2" +
                                " and datediff(day, {deal_start_time} ,getdate()) < 3")
                                .NameFormat(new
                                {
                                    view_hi_deal = ViewHiDeal.Schema.TableName,
                                    city_code_id = ViewHiDeal.Columns.CityCodeId,
                                    category_code_id = ViewHiDeal.Columns.CategoryCodeId,
                                    city_start_time = ViewHiDeal.Columns.CityStartTime,
                                    city_end_time = ViewHiDeal.Columns.CityEndTime,
                                    is_main = ViewHiDeal.Columns.IsMain,
                                    deal_start_time = ViewHiDeal.Columns.DealStartTime
                                });
            */
            if (!string.IsNullOrWhiteSpace(orderBy))
            {
                defOrderBy = orderBy;
            }
            strSQL += " order by " + defOrderBy;


            var vhdc = new InlineQuery().ExecuteAsCollection<ViewHiDealCollection>(strSQL, city, category);

            return vhdc;
        }

        public HiDealDealCollection HiDealGetTodaySpecial(int specialId)
        {
            string sql = @"select 
	d.* from hi_deal_deal d
inner join hi_deal_special_discount dsd on dsd.hi_deal_id = d.id
inner join system_code sc on 
	sc.enabled = 1 and sc.code_group = dsd.code_group and sc.code_id = dsd.code_id
where 
	dsd.code_group = 'HiDealSpecialDiscount' and dsd.code_id = @specialId
	--and  GETDATE() BETWEEN d.deal_start_time and d.deal_end_time
order by d.seq";

            QueryCommand qc = new QueryCommand(sql, HiDealDeal.Schema.Provider.Name);
            qc.AddParameter("@specialId", specialId, DbType.Int32);

            HiDealDealCollection hddCol = new HiDealDealCollection();
            hddCol.LoadAndCloseReader(DataService.GetReader(qc));
            return hddCol;
        }

        public DataTable ViewHiDealGetTodayCategorys(int cityId)
        {
            //TODO 原本的查詢語法，Visa 2013 穩定後刪除
            /*
            string strSQL = @"SELECT 
	CatgList.category_code_id AS code_id, MIN(CatgList.category_code_name) AS code_name
	 , CatgList.seq
FROM hi_deal_city city
INNER JOIN system_code sysReg
ON city.city_code_id = sysReg.code_id
AND sysReg.code_group = 'HiDealRegion'
AND sysReg.enabled = 1
CROSS APPLY (
	SELECT category_code_id, category_code_name ,sysCatg.seq
	FROM hi_deal_category catg
	INNER JOIN system_code sysCatg
	ON catg.category_code_id = sysCatg.code_id
	AND sysCatg.code_group = 'HiDealCatg'
	AND sysCatg.enabled = 1
	WHERE city.hi_deal_id = catg.hi_deal_id
) as CatgList
WHERE  GETDATE() BETWEEN start_time AND end_time
AND city.city_code_id = @cityId
GROUP BY CatgList.category_code_id ,sysReg.seq , CatgList.seq
order by CatgList.seq";
*/
            string strSQL;
            if (config.IsVisa2013 == false)
            {
                strSQL = @"
select t.*, sc.seq from (
select distinct category_code_id as code_id, category as code_name from view_hi_deal
where city_code_id = @cityId
and getdate() between city_start_time and city_end_time
and (special_code_id is null or special_code_id = 1)) as t
inner join system_code sc on sc.code_id = t.code_id and sc.code_group = 'HiDealCatg'
order by sc.seq";
            }
            else
            {
                strSQL =
                    @"
select t.*, sc.seq from (
select distinct category_code_id as code_id, category as code_name from view_hi_deal
where city_code_id = @cityId
and getdate() between city_start_time and city_end_time
and (special_code_id is null or (special_code_id = 2 and datediff(hour, deal_start_time, getdate())>=72)) ) as t
inner join system_code sc on sc.code_id = t.code_id and sc.code_group = 'HiDealCatg'
order by sc.seq
";
            }

            IDataReader idr = new InlineQuery().ExecuteReader(strSQL, cityId);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }

            return dt;
        }

        /// <summary>
        /// int: special code id, 該日visa特別檔的檔次數量
        /// special_cide_id 1 2 分別為 visa專屬、visa卡優先
        /// </summary>
        /// <param name="cityId"></param>
        /// <returns></returns>
        public Dictionary<HiDealSpecialDiscountType, int> ViewHiDealGetTodaySpecials(int cityId)
        {
            //category_code_id = 0 : 全部
            string sql =
                    @"
select special_code_id, COUNT(special_code_id) as data_count from view_hi_deal with(nolock)
where (special_code_id = 1 or (special_code_id = 2 and datediff(hour, deal_start_time, getdate())<72))
and city_code_id = @cityId and category_code_id = 0
and getdate() between city_start_time and city_end_time
group by special_code_id";

            IDataReader idr = new InlineQuery().ExecuteReader(sql, cityId);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }

            Dictionary<HiDealSpecialDiscountType, int> specials = new Dictionary<HiDealSpecialDiscountType, int>();
            specials.Add(HiDealSpecialDiscountType.VisaPrivate, 0);
            specials.Add(HiDealSpecialDiscountType.VisaPriority, 0);
            foreach(DataRow dr in dt.Rows)
            {
                int specialId = (int) dr["special_code_id"];
                int dataCount = (int)dr["data_count"];
                specials[(HiDealSpecialDiscountType)specialId] = dataCount;
            }
            return specials;
        }

        public ViewHiDealCityseqCollection ViewHiDealCollectionGetByTime(DateTime startTime, DateTime endTime, int CityCodeId = 0)
        {
            string sql = @"select * from " + ViewHiDealCityseq.Schema.TableName + " with(nolock) where " + ViewHiDealCityseq.Columns.StartTime + " >=@startTime and " +
                ViewHiDealCityseq.Columns.StartTime + "<@endTime " +
                (CityCodeId == 0 ? string.Empty : (" and " + ViewHiDealCityseq.Columns.CityCodeId + "=@CityCodeId ")) +
                " order by " + ViewHiDealCityseq.Columns.CityCodeId + "," + ViewHiDealCityseq.Columns.Seq;
            QueryCommand qc = new QueryCommand(sql, ViewHiDealCityseq.Schema.Provider.Name);
            qc.AddParameter("@startTime", startTime, DbType.DateTime);
            qc.AddParameter("@endTime", endTime, DbType.DateTime);
            if (CityCodeId != 0)
                qc.AddParameter("@CityCodeId", CityCodeId, DbType.Int32);
            ViewHiDealCityseqCollection data = new ViewHiDealCityseqCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        
        public ViewHiDeal ViewHiDealGetByDealId(int dealId)
        {
            ViewHiDeal view = new ViewHiDeal();
            view.LoadByParam(ViewHiDeal.Columns.DealId, dealId);
            return view;
        }

        #endregion

        #region HiDealDealRegion

        public HiDealRegionCollection HiDealRegionGetList(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealRegion>().Where(HiDealRegion.HiDealIdColumn).IsEqualTo(
                dealId).ExecuteAsCollection<HiDealRegionCollection>() ?? new HiDealRegionCollection();
        }

        /// <summary>
        /// 更新 hi_deal_region (hi_deal_deals 活動所屬區域更新).
        /// 根據傳入的城市串列, 把不在串列上的資料刪掉, 並新增資料裡沒有的城市.
        /// </summary>
        /// <param name="dealId">hi_deal 的 id</param>
        /// <param name="cityNames"></param>
        /// <returns></returns>
        public bool HiDealRegionMerge(int dealId, List<string> cityNames)
        {
            //delete all records when the provided parameter cityNames has nothing inside
            if (cityNames == null || cityNames.Count == 0)
            {
                DB.Delete().From<HiDealRegion>().Where(HiDealRegion.HiDealIdColumn).IsEqualTo(
                    dealId).Execute();
                return true;
            }

            List<int> regionIds = new List<int>();  // list of code_id that needs to exist in the final state 

            DataSet ds = DB.Select(SystemCode.Columns.CodeId)
                .From<SystemCode>().Where(SystemCode.CodeNameColumn).In(cityNames)
                .And(SystemCode.CodeGroupColumn).IsEqualTo("HiDealRegion").ExecuteDataSet();    //get codeId by cityNames
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                regionIds.Add(int.Parse(row[0].ToString()));
            }

            //delete records where city not in cityNames
            DB.Delete().From<HiDealRegion>().Where(HiDealRegion.HiDealIdColumn).IsEqualTo(dealId).
                And(HiDealRegion.CodeNameColumn).NotIn(cityNames).Execute();

            //insert records where new city in cityNames
            string regIds = string.Join(",", regionIds.ToArray());
            if (string.IsNullOrEmpty(regIds))
                regIds = "''";
            string sql = string.Format(
                @"INSERT INTO hi_deal_region(hi_deal_id, code_id, code_group, code_name)
					SELECT  T.id, system_code.code_id, system_code.code_group, system_code.code_name FROM system_code CROSS JOIN (SELECT @dealId) as T(id)
					WHERE system_code.code_id in ({0}) 
					AND system_code.code_group = @region
					AND NOT EXISTS (SELECT * FROM hi_deal_region WHERE hi_deal_id = @dealId AND code_id = system_code.code_id)", regIds);
            new InlineQuery().Execute(sql, dealId, "HiDealRegion", dealId);


            return true;
        }

        #endregion

        #region HiDealDealsStore

        public int HiDealDealsStoreSet(HiDealDealStore dealStore)
        {
            return DB.Save(dealStore);
        }

        public void HiDealDealsStoreDelete(HiDealDealStore dealStore)
        {
            DB.Delete(dealStore);
        }

        public HiDealDealStoreCollection HiDealDealsStoreGetListByHiDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<HiDealDealStore>().Where(HiDealDealStore.HiDealIdColumn).IsEqualTo(dealId).
                    ExecuteAsCollection<HiDealDealStoreCollection>() ?? new HiDealDealStoreCollection();
        }

        public DataTable HiDealDealStoreWithStoreInformation(int dealId)
        {
            string sql = @"
			select     
				ds.seq as Sequence, 
				s.Guid as StoreGuid, 
				s.store_name as StoreName, 
				ds.use_time as UseTime, 
				city.city_name + zone.city_name + s.address_string as StoreAddress
			from hi_deal_deal_store ds
			right join hi_deal_deal d
			inner join store s
				on d.seller_guid = s.seller_guid
			left join city as zone
			left join city
				on zone.parent_id = city.id
				on s.township_id = zone.id
				on ds.store_guid = s.Guid
					and ds.hi_deal_id = d.id
			where d.id = @dealId
			order by coalesce(ds.seq,999999)";

            IDataReader reader = new InlineQuery().ExecuteReader(sql, dealId.ToString());
            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        #endregion

        #region HiDealSubscription

        public void HiDealSubscriptionSet(HiDealSubscription subscript)
        {
            DB.Save<HiDealSubscription>(subscript);
        }
        public HiDealSubscriptionCollection HiDealSubscriptionGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = HiDealSubscription.Columns.CreateTime + " desc";
            QueryCommand qc = GetHDWhere(HiDealSubscription.Schema, filter);
            qc.CommandSql = "select * from " + HiDealSubscription.Schema.Provider.DelimitDbName(HiDealSubscription.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            HiDealSubscriptionCollection vfcCol = new HiDealSubscriptionCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public void HiDealSubscriptionSetToNewUser(string oldEmail, string newEmail)
        {
            DB.Update<HiDealSubscription>()
              .Set(HiDealSubscription.Columns.Email).EqualTo(newEmail)
              .Where(HiDealSubscription.Columns.Email).IsEqualTo(oldEmail).Execute();
        }

        public HiDealSubscriptionCollection HiDealSubscribersGetOverviewByIdRegion(int min_id, int max_id)
        {
            string sql = @"SELECT * FROM " + HiDealSubscription.Schema.TableName + " WITH(NOLOCK) WHERE " +
            HiDealSubscription.Columns.RegionCodeId + " = " + (int)PiinlifeMailSendCity.Overview + " AND " +
                HiDealSubscription.Columns.Flag + " IN(" + (int)PiinlifeSubscriptionFlag.General + "," + (int)PiinlifeSubscriptionFlag.Resubscribe + ",NULL) AND " +
                HiDealSubscription.Columns.Id + ">=@min_id and " + HiDealSubscription.Columns.Id + "<@max_id AND (" + HiDealSubscription.Columns.Reliable + "=1 OR " + HiDealSubscription.Columns.Reliable + " IS NULL)";
            var qc = new QueryCommand(sql, HiDealSubscription.Schema.Provider.Name);
            qc.Parameters.Add("@min_id", min_id, DbType.Int32);
            qc.Parameters.Add("@max_id", max_id, DbType.Int32);
            var vfcCol = new HiDealSubscriptionCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public void HiDealSuscriptionDeleteRepeat(int region_code_id)
        {
            string sql = "DELETE " + HiDealSubscription.Schema.TableName + " WHERE " + HiDealSubscription.Columns.Email
                + " IN (select " + HiDealSubscription.Columns.Email + " from " + HiDealSubscription.Schema.TableName + " where " + HiDealSubscription.Columns.RegionCodeId + "=@regioncodeid "
                + " and flag in (" + (int)PiinlifeSubscriptionFlag.General + "," + (int)PiinlifeSubscriptionFlag.Resubscribe + ",null) group by " + HiDealSubscription.Columns.Email
                + " having COUNT(" + HiDealSubscription.Columns.Email + ")>1) and " + HiDealSubscription.Columns.RegionCodeId + "=@regioncodeid and flag in ("
                + (int)PiinlifeSubscriptionFlag.General + "," + (int)PiinlifeSubscriptionFlag.Resubscribe + ",null)";
            QueryCommand qc = new QueryCommand(sql, HiDealSubscription.Schema.Provider.Name);
            qc.AddParameter("@regioncodeid", region_code_id, DbType.Int32);
            DB.Execute(qc);
        }

        public int HiDealSuscriptionGetLatestId()
        {
            string sql = "SELECT TOP 1  " + HiDealSubscription.Columns.Id + " FROM " + HiDealSubscription.Schema.TableName + " with(nolock) ORDER BY " + HiDealSubscription.Columns.Id + " DESC";
            QueryCommand qc = new QueryCommand(sql, HiDealSubscription.Schema.Provider.Name);
            return (int)DataService.ExecuteScalar(qc);
        }

        public void HiDealSubscriptionSetList(HiDealSubscriptionCollection items)
        {
            DB.SaveAll(items);
        }
        public HiDealSubscriptionCollection HiDealSubscriptionGetListByUser(string userName)
        {
            return DB.SelectAllColumnsFrom<HiDealSubscription>()
                .Where(HiDealSubscription.Columns.Email).IsEqualTo(userName).OrderAsc(HiDealSubscription.Columns.RegionCodeId).ExecuteAsCollection<HiDealSubscriptionCollection>();
        }
        public HiDealSubscription HiDealSubscriptionGetByUserRegionCodeId(string userName, int regionCodeId)
        {
            return DB.SelectAllColumnsFrom<HiDealSubscription>()
                .Where(HiDealSubscription.Columns.Email).IsEqualTo(userName)
                .And(HiDealSubscription.Columns.RegionCodeId).IsEqualTo(regionCodeId)
                .OrderAsc(HiDealSubscription.Columns.RegionCodeId)
                .ExecuteSingle<HiDealSubscription>();
        }
        public HiDealSubscription HiDealSubscriptionGetByUserCityId(string userName, int cityId)
        {
            return DB.SelectAllColumnsFrom<HiDealSubscription>()
                .Where(HiDealSubscription.Columns.Email).IsEqualTo(userName)
                .And(HiDealSubscription.Columns.City).IsEqualTo(cityId)
                .OrderAsc(HiDealSubscription.Columns.RegionCodeId)
                .ExecuteSingle<HiDealSubscription>();
        }
        #endregion

        #region Unsubscription
        public Unsubscription UnsubscriptionGet(string email)
        {
            return DB.Get<Unsubscription>(email);
        }

        public void UnsubscriptionSet(Unsubscription content)
        {
            DB.Save<Unsubscription>(content);
        }
        #endregion

        #region HiDealSubscriptionPromo
        public HiDealSubscriptionPromo HiDealSubscriptionPromoGet(int id)
        {
            return DB.Get<HiDealSubscriptionPromo>(id);
        }

        public void HiDealSubscriptionPromoSet(HiDealSubscriptionPromo content)
        {
            DB.Save<HiDealSubscriptionPromo>(content);
        }

        public HiDealSubscriptionPromoCollection HiDealSubscriptionPromoGetListWithOutDelete()
        {
            return DB.Select().From(HiDealSubscriptionPromo.Schema)
                .Where(HiDealSubscriptionPromo.Columns.Status).IsNotEqualTo(PiinlifeSubscriptionPromoStatus.Delete)
                .ExecuteAsCollection<HiDealSubscriptionPromoCollection>();
        }
        #endregion

        #region HiDealSubscriptionPromoSlot
        public int HiDealSubscriptionPromoSlotSet(HiDealSubscriptionPromoSlot content)
        {
            return DB.Save<HiDealSubscriptionPromoSlot>(content);
        }
        public HiDealSubscriptionPromoSlot HiDealSubscriptionPromoSlotGetByCodeIdAndPromoId(int codeId, int promoId)
        {
            return DB.Select().From(HiDealSubscriptionPromoSlot.Schema)
                .Where(HiDealSubscriptionPromoSlot.Columns.CodeGroup).IsEqualTo("HiDealRegion")
                .And(HiDealSubscriptionPromoSlot.Columns.CodeId).IsEqualTo(codeId)
                .And(HiDealSubscriptionPromoSlot.Columns.PromoId).IsEqualTo(promoId)
                .ExecuteSingle<HiDealSubscriptionPromoSlot>();
        }

        public HiDealSubscriptionPromoSlotCollection HiDealSubscriptionPromoSlotGetList(int codeId)
        {
            return DB.Select().From(HiDealSubscriptionPromoSlot.Schema)
                .Where(HiDealSubscriptionPromoSlot.Columns.CodeGroup).IsEqualTo("HiDealRegion")
                .And(HiDealSubscriptionPromoSlot.Columns.CodeId).IsEqualTo(codeId)
                .ExecuteAsCollection<HiDealSubscriptionPromoSlotCollection>();
        }
        public HiDealSubscriptionPromoSlotCollection HiDealSubscriptionPromoSlotGetList(int codeId, DateTime date)
        {
            return DB.Select().From(HiDealSubscriptionPromoSlot.Schema)
                .Where(HiDealSubscriptionPromoSlot.Columns.CodeGroup).IsEqualTo("HiDealRegion")
                .And(HiDealSubscriptionPromoSlot.Columns.CodeId).IsEqualTo(codeId)
                .And(HiDealSubscriptionPromoSlot.Columns.StartTime).IsLessThanOrEqualTo(date)
                .And(HiDealSubscriptionPromoSlot.Columns.EndTime).IsGreaterThanOrEqualTo(date)
                .ExecuteAsCollection<HiDealSubscriptionPromoSlotCollection>();
        }

        public DataTable HiDealSubscriptionPromoSlotGetListSort(int intOrderby, bool isAsc)
        {
            string orderby = string.Empty;
            string ascOrDesc = (isAsc) ? " ASC " : " DESC ";
            switch (intOrderby)
            {
                case 1: //城市
                    orderby = @" ORDER BY code_id {ascOrDesc},start_time DESC,  title, end_time".Replace("{ascOrDesc}", ascOrDesc);
                    break;
                case 2: //ad標題
                    orderby = @" ORDER BY title {ascOrDesc},start_time DESC, code_id, end_time".Replace("{ascOrDesc}", ascOrDesc);
                    break;
                case 3: //開始時間
                    orderby = @" ORDER BY start_time {ascOrDesc}, code_id, title, end_time".Replace("{ascOrDesc}", ascOrDesc);
                    break;
                default:
                    orderby = @" ORDER BY start_time DESC, code_id, title, end_time";
                    break;
            }

            string sql = @"SELECT sps.id AS id, sp.promo_title AS title
    , sps.code_id
	, CASE sps.code_id 
	  WHEN 0 THEN N'預設'
	  ELSE sc.code_name 
	 END AS city
	, sps.start_time, sps.end_time
FROM hi_deal_subscription_promo_slot AS sps WITH(NOLOCK)
	LEFT JOIN hi_deal_subscription_promo AS sp WITH(NOLOCK) ON sp.id = sps.promo_id
	LEFT JOIN system_code AS sc WITH(NOLOCK) ON (sc.code_group = sps.code_group AND sc.code_id = sps.code_id)
WHERE sp.status = 0	
" + orderby;
            IDataReader reader = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        #endregion

        #region ViewHiDealProductStore

        public ViewHiDealProductStoreCollection ViewHiDealProductStoreGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealProductStore>().Where(ViewHiDealProductStore.Columns.HiDealProductId).
                IsEqualTo(productId).OrderAsc(ViewHiDealProductStore.Columns.Seq).ExecuteAsCollection<ViewHiDealProductStoreCollection>();
        }

        #endregion ViewHiDealProductStore

        #region HiDealCoupon
        public HiDealCoupon HiDealCouponGet(long id)
        {
            return DB.Get<HiDealCoupon>(id);
        }

        public int HiDealCouponSet(HiDealCoupon c)
        {
            return DB.Save<HiDealCoupon>(c);
        }

        public HiDealCoupon HiDealCouponGetTop(int dealId, int productId, Guid? storeGuid)
        {
            return DB.Select().Top("1").From(HiDealCoupon.Schema.TableName)
                .Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId).And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsEqualTo(storeGuid).OrderDesc(HiDealCoupon.Columns.Sequence).ExecuteSingle<HiDealCoupon>();
        }

        public HiDealCouponCollection HiDealCouponGetListByOrderPk(int productId, int orderPk, HiDealCouponStatus? status)
        {
            if (status == null)
            {
                return
                DB.SelectAllColumnsFrom<HiDealCoupon>().Where(HiDealCoupon.Columns.ProductId).IsEqualTo(productId).
                    And(HiDealCoupon.Columns.OrderPk).IsEqualTo(orderPk).
                    ExecuteAsCollection<HiDealCouponCollection>();
            }
            else
            {
                return
                DB.SelectAllColumnsFrom<HiDealCoupon>().Where(HiDealCoupon.Columns.ProductId).IsEqualTo(productId).
                    And(HiDealCoupon.Columns.OrderPk).IsEqualTo(orderPk).And(HiDealCoupon.Columns.Status).IsEqualTo((int)status.Value).
                    ExecuteAsCollection<HiDealCouponCollection>();
            }
        }

        public HiDealCoupon HiDealCouponGetTop(int dealId, int productId)
        {
            return DB.Select().Top("1").From(HiDealCoupon.Schema.TableName)
                .Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId).And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsNull()
                .OrderDesc(HiDealCoupon.Columns.Sequence).ExecuteSingle<HiDealCoupon>();
        }

        public HiDealCouponCollection HiDealCouponGetListInAvailableAfterIndex(int dealId, int productId, long startIndex,
                                                                        int length)
        {
            return DB.SelectAllColumnsFrom<HiDealCoupon>().Top(length.ToString()).Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId)
                .And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsNull()
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)HiDealCouponStatus.Available)
                .And(HiDealCoupon.IdColumn).IsGreaterThanOrEqualTo(startIndex)
                .OrderAsc(HiDealCoupon.Columns.Id).ExecuteAsCollection<HiDealCouponCollection>();
        }
        public HiDealCouponCollection HiDealCouponGetListInAvailableAfterIndex(int dealId, int productId, Guid storeGuid, long startIndex,
                                                                int length)
        {
            return DB.SelectAllColumnsFrom<HiDealCoupon>().Top(length.ToString()).Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId)
                .And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsEqualTo(storeGuid)
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)HiDealCouponStatus.Available)
                .And(HiDealCoupon.IdColumn).IsGreaterThanOrEqualTo(startIndex)
                .OrderAsc(HiDealCoupon.Columns.Id).ExecuteAsCollection<HiDealCouponCollection>();
        }

        public void HiDealCouponSetList(HiDealCouponCollection dataList)
        {
            DB.SaveAll(dataList);
        }

        public int HiDealCouponGetNotInHiDealCouponOfferCount(int dealId, int productId)
        {
            var strSql = "select count(*) from " + HiDealCoupon.Schema.TableName +
                         " where deal_id = @dealId and product_id = @productId " +
                         " and store_guid is null " +
                         " and status = @status " +
                         " and  id not in (select coupon_id from " + HiDealCouponOffer.Schema.TableName +
                         " where deal_id = @dealId and product_id =  @productId ) ";
            var qc = new QueryCommand(strSql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@dealId", dealId, DbType.Int64);
            qc.AddParameter("@status", (int)HiDealCouponStatus.Available, DbType.Int16);
            qc.AddParameter("@productId", productId, DbType.Int64);
            return (int)DataService.ExecuteScalar(qc);
        }
        public int HiDealCouponGetNotInHiDealCouponOfferCount(int dealId, int productId, Guid storeGuid)
        {
            var strSql = "select count(*) from " + HiDealCoupon.Schema.TableName +
             " where deal_id = @dealId and product_id = @productId " +
             " and store_guid = @storeGuid " +
             " and status = @status " +
             " and id not in (select coupon_id from " + HiDealCouponOffer.Schema.TableName +
             " where deal_id = @dealId and product_id = @productId and store_guid = @storeGuid) ";
            var qc = new QueryCommand(strSql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@dealId", dealId, DbType.Int64);
            qc.AddParameter("@status", (int)HiDealCouponStatus.Available, DbType.Int16);
            qc.AddParameter("@productId", productId, DbType.Int64);
            qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            return (int)DataService.ExecuteScalar(qc);
        }

        public int HiDealCouponGetCountByProductAndUser(int productId, int userId)
        {
            var strSql = " select COUNT(*) from " +
                         HiDealCoupon.Schema.Provider.DelimitDbName(HiDealCoupon.Schema.TableName) + " with(nolock) " +
                         " where " + HiDealCoupon.ProductIdColumn + " = @productId and " + HiDealCoupon.UserIdColumn + " = @userId " +
                         " and "+ HiDealCoupon.StatusColumn +"=@status"
                         ;
            var qc = new QueryCommand(strSql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@productId", productId, DbType.Int32);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@status", HiDealCouponStatus.Assigned, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }


        public int HiDealCouponGetProductAvailable(int dealId, int productId)
        {
            return DB.Select().From(HiDealCoupon.Schema.TableName)
                .Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId).And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsNull()
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)HiDealCouponStatus.Available).GetRecordCount();
        }

        public int HiDealCouponGetStoreAvailable(int dealId, int productId, Guid? storeGuid)
        {
            return DB.Select().From(HiDealCoupon.Schema.TableName)
                .Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId).And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealCoupon.StoreGuidColumn).IsEqualTo(storeGuid)
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)HiDealCouponStatus.Available).GetRecordCount();
        }

        public void HiDealCouponUpdateStatus(HiDealCouponStatus statusFrom, HiDealCouponStatus statusTo, int dealId, int productId)
        {
            string sql = "update " + HiDealCoupon.Schema.TableName + " set " + HiDealCoupon.Columns.Status + "=@statusTo where "
                         + HiDealCoupon.Columns.DealId + " = @dealId and " + HiDealCoupon.Columns.ProductId + " = @productId and " + HiDealCoupon.Columns.Status + " = @statusFrom "
                         + " and " + HiDealCoupon.Columns.StoreGuid + " is null ";
            QueryCommand qc = new QueryCommand(sql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@statusFrom", statusFrom, DbType.Int32);
            qc.AddParameter("@statusTo", statusTo, DbType.Int32);
            qc.AddParameter("@dealId", dealId, DbType.Int32);
            qc.AddParameter("@productId", productId, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }

        public void HiDealCouponUpdateStatus(HiDealCouponStatus statusFrom, HiDealCouponStatus statusTo, int dealId, int productId, Guid? storeGuid)
        {
            string sql = "update " + HiDealCoupon.Schema.TableName + " set " + HiDealCoupon.Columns.Status +
                         "=@statusTo where "
                         + HiDealCoupon.Columns.DealId + " = @dealId and " + HiDealCoupon.Columns.ProductId +
                         " = @productId and " + HiDealCoupon.Columns.Status + " = @statusFrom ";
            if ((storeGuid != null) && (storeGuid != Guid.Empty))
            {
                sql += " and " + HiDealCoupon.Columns.StoreGuid + " = @storeGuid ";
            }

            QueryCommand qc = new QueryCommand(sql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@statusFrom", statusFrom, DbType.Int32);
            qc.AddParameter("@statusTo", statusTo, DbType.Int32);
            qc.AddParameter("@dealId", dealId, DbType.Int32);
            qc.AddParameter("@productId", productId, DbType.Int32);
            if ((storeGuid != null) && (storeGuid != Guid.Empty))
            {
                qc.AddParameter("@storeGuid", storeGuid, DbType.Guid);
            }

            DataService.ExecuteScalar(qc);
        }

        public void HiDealCouponUpdateToAssigned(List<long> couponId, int orderPk, Guid detailGuid, int userId)
        {
            var couponIdList = string.Join(",", couponId);
            string sql = "update " + HiDealCoupon.Schema.Provider.DelimitDbName(HiDealCoupon.Schema.TableName) + " set " +
                         HiDealCoupon.Columns.OrderPk + " = @orderPk ," +
                         HiDealCoupon.Columns.OrderDetailGuid + " = @detailGuid ," +
                         HiDealCoupon.Columns.Status + "= @statusNow ," +                         
                         HiDealCoupon.Columns.UserId + " = @userId," +
                         HiDealCoupon.Columns.BoughtDate + " = @boughtDate " +
                         " where id in (" + couponIdList + ") and " +
                         HiDealCoupon.Columns.Status + "= @statusOld ";
            QueryCommand qc = new QueryCommand(sql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@orderPk", orderPk, DbType.Int32);
            qc.AddParameter("@detailGuid", detailGuid, DbType.Guid);
            qc.AddParameter("@statusNow", (int)HiDealCouponStatus.Assigned, DbType.Int32);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@boughtDate", DateTime.Now, DbType.DateTime);
            qc.AddParameter("@statusOld", (int)HiDealCouponStatus.Available, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }
        
        /*
        public void HiDealCouponUpdateToAvailable(int orderPk)
        {
            string sql = "update " + HiDealCoupon.Schema.Provider.DelimitDbName(HiDealCoupon.Schema.TableName) + " set " +
                         HiDealCoupon.Columns.OrderPk + " = null ," +
                         HiDealCoupon.Columns.OrderDetailGuid + " null ," +
                         HiDealCoupon.Columns.Status + "= @statusNow ," +
                         HiDealCoupon.Columns.UserName + " = null ," +
                         HiDealCoupon.Columns.BoughtDate + " = null " +
                         " where " + HiDealCoupon.Columns.OrderPk + " = @orderPk  and " +
                         HiDealCoupon.Columns.Status + "= @statusOld ";
            QueryCommand qc = new QueryCommand(sql, HiDealCoupon.Schema.Provider.Name);
            qc.AddParameter("@statusNow", (int)HiDealCouponStatus.Available, DbType.Int32);
            qc.AddParameter("@orderPk", orderPk, DbType.Int32);
            qc.AddParameter("@statusOld", (int)HiDealCouponStatus.Assigned, DbType.Int32);
            DataService.ExecuteScalar(qc);
        }
        */

        public HiDealCouponCollection HiDealCouponGetByStatus(int dealId, int productId, HiDealCouponStatus st, List<string> orderBy)
        {
            var query = DB.SelectAllColumnsFrom<HiDealCoupon>().Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId).
                And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId).And(HiDealCoupon.StoreGuidColumn).IsNull()
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)st);
            query = SSHelper.GetOrderBySqlQuery(query, orderBy);
            return query.ExecuteAsCollection<HiDealCouponCollection>();
        }

        public HiDealCouponCollection HiDealCouponGetByStatus(int dealId, int productId, Guid? storeGuid, HiDealCouponStatus st, List<string> orderBy)
        {
            var query = DB.SelectAllColumnsFrom<HiDealCoupon>().Where(HiDealCoupon.DealIdColumn).IsEqualTo(dealId)
                .And(HiDealCoupon.ProductIdColumn).IsEqualTo(productId).And(HiDealCoupon.StoreGuidColumn).IsEqualTo(storeGuid)
                .And(HiDealCoupon.StatusColumn).IsEqualTo((int)st);
            query = SSHelper.GetOrderBySqlQuery(query, orderBy);
            return query.ExecuteAsCollection<HiDealCouponCollection>();
        }

        public bool HiDealCouponHaveAssignedOrUsed(int productId)
        {
            var rtn = DB.Select(Aggregate.Count(HiDealCoupon.IdColumn)).From(HiDealCoupon.Schema.TableName)
                .Where(HiDealCoupon.ProductIdColumn).IsEqualTo(productId).And(HiDealCoupon.StatusColumn).IsEqualTo(
                    HiDealCouponStatus.Assigned).OrExpression(HiDealCoupon.Columns.Status).IsEqualTo(HiDealCouponStatus.Used).ExecuteScalar<int>();
            if (rtn > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 刪除product所有的憑證資料。
        /// </summary>
        /// <param name="productId"></param>
        public void HiDealCouponDeleteAllByProductId(int productId)
        {
            DB.Delete<HiDealCoupon>(HiDealCoupon.Columns.ProductId, productId);
        }

        public HiDealCouponCollection HiDealCouponGetListByProdcutId(int productId)
        {
            return DB.SelectAllColumnsFrom<HiDealCoupon>().Where(HiDealCoupon.Columns.ProductId).IsEqualTo(productId).ExecuteAsCollection<HiDealCouponCollection>();
        }

        public DataTable GetHiDealCouponListForExport(Guid productGuid, Guid? storeGuid)
        {
            string sql = string.Format(@"
            Select ctl.coupon_sequence_number as 憑證編號
            , stuff(hdod.addressee_name,2,1,'*') As 姓名
            , '' As 選項
            , ctl.status
            , ctl.special_status
            , ctl.modify_time
            From {0} as hdod with(nolock)
            Inner Join {1} as hp with(nolock) ON hp.id = hdod.product_id
            Inner Join {2} as ctl with(nolock) ON ctl.order_guid = hdod.hi_deal_order_guid
            Where hdod.product_type = 1
            and hp.guid = '{3}'
            ", HiDealOrderDetail.Schema.Name
             , HiDealProduct.Schema.Name
             , CashTrustLog.Schema.Name
             , productGuid.ToString());
            sql += storeGuid == Guid.Empty ? string.Empty : " and hdod.store_guid = '" + storeGuid.ToString() + "' ";
            sql += " order by ctl.coupon_sequence_number asc ";
            IDataReader rdr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (rdr)
            {
                dt.Load(rdr);
            }
            return dt;
        }

        #endregion

        #region HiDealStaticDeal

        public int HiDealStaticDealSet(HiDealStaticDeal sd)
        {
            return DB.Save<HiDealStaticDeal>(sd);
        }

        public int HiDealStaticDealGetProductMaxChangeSet(int productId)
        {
            return DB.Select(Aggregate.Max(HiDealStaticDeal.ChangeSetColumn)).From(HiDealStaticDeal.Schema.TableName)
                .Where(HiDealStaticDeal.ProductIdColumn).IsEqualTo(productId).ExecuteScalar<int>();
        }

        //public int HiDealStaticDealGetStoreMaxChangeSet(int productId, Guid? storeGuid)
        //{
        //    return DB.Select(Aggregate.Max(HiDealStaticDeal.ChangeSetColumn)).From(HiDealStaticDeal.Schema.TableName)
        //        .Where(HiDealStaticDeal.ProductIdColumn).IsEqualTo(productId)
        //        .And(HiDealStaticDeal.StoreGuidColumn).IsEqualTo(storeGuid).ExecuteScalar<int>();
        //}

        //public HiDealStaticDealCollection HiDealStaticDealGetMaxProduct(int productId)
        //{
        //    return DB.SelectAllColumnsFrom<HiDealStaticDeal>()
        //        .Where(HiDealStaticDeal.ProductIdColumn).IsEqualTo(productId)
        //        .And(HiDealStaticDeal.ChangeSetColumn).IsEqualTo(HiDealStaticDealGetProductMaxChangeSet(productId))
        //        .And(HiDealStaticDeal.StoreGuidColumn).IsNull()
        //        .ExecuteAsCollection<HiDealStaticDealCollection>();
        //}

        //public HiDealStaticDealCollection HiDealStaticDealGetMaxStore(int productId, Guid? storeGuid)
        //{
        //    return DB.SelectAllColumnsFrom<HiDealStaticDeal>()
        //        .Where(HiDealStaticDeal.ProductIdColumn).IsEqualTo(productId)
        //        .And(HiDealStaticDeal.StoreGuidColumn).IsEqualTo(storeGuid)
        //        .And(HiDealStaticDeal.ChangeSetColumn).IsEqualTo(HiDealStaticDealGetProductMaxChangeSet(productId))
        //        .ExecuteAsCollection<HiDealStaticDealCollection>();
        //}
        public HiDealStaticDealCollection HiDealStaticDealGetListByProductIdAndMaxChangeSet(int productId)
        {
            return DB.SelectAllColumnsFrom<HiDealStaticDeal>()
                .Where(HiDealStaticDeal.ProductIdColumn).IsEqualTo(productId)
                .And(HiDealStaticDeal.ChangeSetColumn).IsEqualTo(HiDealStaticDealGetProductMaxChangeSet(productId))
                .ExecuteAsCollection<HiDealStaticDealCollection>();
        }
        #endregion

        #region HiDealProductStore
        public HiDealProductStore HiDealProductStoreGet(int id)
        {
            return DB.Get<HiDealProductStore>(id);
        }

        public HiDealProductStore HiDealProductStoreGetByPidAndStoreGuid(int productId, Guid storeGuid)
        {
            return
                DB.SelectAllColumnsFrom<HiDealProductStore>().Where(HiDealProductStore.HiDealProductIdColumn).IsEqualTo(
                    productId).And(HiDealProductStore.StoreGuidColumn).IsEqualTo(storeGuid).ExecuteSingle
                    <HiDealProductStore>();
        }

        public int HiDealProductStoreSet(HiDealProductStore c)
        {
            return DB.Save<HiDealProductStore>(c);
        }

        public HiDealProductStoreCollection HiDealProductStoreGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<HiDealProductStore>().Where(HiDealProductStore.HiDealProductIdColumn).IsEqualTo(productId).ExecuteAsCollection<HiDealProductStoreCollection>() ?? new HiDealProductStoreCollection();
        }

        public DataTable HiDealProductStoreWithStoreInformation(int productId)
        {
            string sql = @"
			select 
				ps.quantity_limit as QuantityLimit, 
				ps.seq as Sequence, 
				s.Guid as StoreGuid, 
				s.store_name as StoreName, 
				city.city_name + zone.city_name + s.address_string as StoreAddress
			from hi_deal_product_store ps
			right join hi_deal_product p
			    inner join store s
				    on p.seller_guid = s.seller_guid
			    left join city as zone
			        inner join city
				        on zone.parent_id = city.id
				    on s.township_id = zone.id
				on ps.store_guid = s.Guid
					and ps.hi_deal_product_id = p.id
			where p.id = @pid
			order by coalesce(ps.seq,999999)";

            IDataReader reader = new InlineQuery().ExecuteReader(sql, productId.ToString());
            DataTable dt = new DataTable();
            using (reader)
            {
                dt.Load(reader);
            }
            return dt;
        }

        public void HiDealProductStoreDelete(HiDealProductStore productStore)
        {
            DB.Delete(productStore);
        }

        #endregion

        #region HiDealCouponPrefix

        public int HiDealCouponPrefixSet(HiDealCouponPrefix c)
        {
            return DB.Save<HiDealCouponPrefix>(c);
        }

        public int HiDealCouponPrefixGetMaxSequence(int dealId)
        {
            return DB.Select(Aggregate.Max(HiDealCouponPrefix.SequenceColumn)).From(HiDealCouponPrefix.Schema.TableName)
                .Where(HiDealCouponPrefix.DealIdColumn).IsEqualTo(dealId).ExecuteScalar<int>();
        }

        /// <summary>
        /// 取得該賣家現存所有檔次Hi_Deal_Coupon_Prefix的最高Sequence by sam
        /// </summary>
        /// <param name="sellerguid">賣家GUID</param>
        /// <returns>回傳該賣家現存所有檔次Hi_Deal_Coupon_Prefix的最高Sequence</returns>
        public int HiDealCouponPrefixGetMaxSequenceBySellerGuid(Guid sellerguid)
        {
            return DB.Select(Aggregate.Max(HiDealCouponPrefix.SequenceColumn)).From(HiDealCouponPrefix.Schema.TableName)
                .Where(HiDealCouponPrefix.DealIdColumn).In(DB.Select(HiDealDeal.Columns.Id).From(HiDealDeal.Schema.TableName).Where(HiDealDeal.SellerGuidColumn).IsEqualTo(sellerguid)).ExecuteScalar<int>();
        }

        #endregion

        #region HiDealCouponLog
       
        public int HiDealCouponLogSet(HiDealCouponLog c)
        {
            return DB.Save<HiDealCouponLog>(c);
        }

        #endregion

        #region HiDealCouponOffer

        public bool HiDealCouponOfferSetList(HiDealCouponOfferCollection lists)
        {
            DB.SaveAll(lists);
            return true;
        }

        public void HiDealCouponOfferDeleteByOrderPk(int orderPk)
        {
            DB.Delete<HiDealCouponOffer>(HiDealCouponOffer.Columns.OrderPk, orderPk);
        }

        #endregion HiDealCouponOffer

        #region HiDealStoreInfo
        public ViewHiDealStoresInfoCollection ViewHiDealDealStoreInfoGetByHiDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealStoresInfo>().
                Where(ViewHiDealStoresInfo.Columns.HiDealId).IsEqualTo(dealId).
                And(ViewHiDealStoresInfo.Columns.Status).IsEqualTo(false).OrderAsc(ViewHiDealStoresInfo.Columns.Seq).
                ExecuteAsCollection<ViewHiDealStoresInfoCollection>();
        }
        #endregion

        #region HiDealSystemCode
        public SystemCodeCollection HiDealGetAllCityFromSystemCode()
        {
            SystemCodeCollection citySystemCode =
                DB.Select().From<SystemCode>().Where(SystemCode.CodeGroupColumn).IsEqualTo("HiDealRegion")
                    .ExecuteAsCollection<SystemCodeCollection>();
            return citySystemCode ?? new SystemCodeCollection();
        }

        public SystemCode HiDealGetOneCityFromSystemCode(int cityId)
        {
            SystemCodeCollection citySystemCode =
                DB.Select().From<SystemCode>()
                    .Where(SystemCode.CodeGroupColumn).IsEqualTo("HiDealRegion")
                    .And(SystemCode.CodeIdColumn).IsEqualTo(cityId)
                    .ExecuteAsCollection<SystemCodeCollection>();
            return (citySystemCode.Count > 0) ? citySystemCode[0] : new SystemCode();
        }
        #endregion

        #region HiDealReturned

        public int HiDealReturnedSet(HiDealReturned data)
        {
            if (data.UserId == 0)
            {
                if (data.UserId == 0)
                {
                    log.WarnFormat("儲存 HiDealReturned 發生異常 CreateTime={0}, CancelTime={1}, id={2}", 
                        data.CreateTime, data.CancelTime, data.Id);
                }                
            }
            return DB.Save(data);
        }
        public int HiDealReturnedSetList(HiDealReturnedCollection datas)
        {
            foreach(var data in datas)
            {
                if (data.UserId == 0)
                {
                    if (data.UserId == 0)
                    {
                        log.WarnFormat("儲存 HiDealReturned 發生異常 CreateTime={0}, CancelTime={1}, id={2}",
                                       data.CreateTime, data.CancelTime, data.Id);
                    }
                }
            }
            return DB.SaveAll(datas);
        }

        public HiDealReturned HiDealReturnedGet(int returnedId)
        {
            return DB.Get<HiDealReturned>(returnedId);
        }
        public HiDealReturnedCollection HiDealReturnedGetListByOrderPk(int orderPk)
        {
            return DB.SelectAllColumnsFrom<HiDealReturned>().Where(HiDealReturned.Columns.OrderPk).IsEqualTo(orderPk)
                .ExecuteAsCollection<HiDealReturnedCollection>();
        }
        public HiDealReturnedCollection HiDealReturnedGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = HiDealReturned.Columns.Id + " desc";
            QueryCommand qc = GetHDWhere(HiDealReturned.Schema, filter);
            qc.CommandSql = "select * from " + HiDealReturned.Schema.Provider.DelimitDbName(HiDealReturned.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            var cols = new HiDealReturnedCollection();
            cols.LoadAndCloseReader(DataService.GetReader(qc));
            return cols;
        }

        #endregion HiDealReturned

        #region HiDealReturnedCoupon
        public int HiDealReturnedCouponSetList(HiDealReturnedCouponCollection data)
        {
            return DB.SaveAll(data);
        }
        public HiDealReturnedCouponCollection HiDealReturnedCouponGetList(int returnedId)
        {
            return
            DB.SelectAllColumnsFrom<HiDealReturnedCoupon>().Where(HiDealReturnedCoupon.Columns.ReturnedId).IsEqualTo(returnedId).
            ExecuteAsCollection<HiDealReturnedCouponCollection>();
        }
        #endregion HiDealReturnedCoupon

        #region HiDealReturnedDetail

        public int HiDealReturnedDetailSetList(HiDealReturnedDetailCollection data)
        {
            return DB.SaveAll(data);
        }

        public HiDealReturnedDetailCollection HiDealReturnedDetailGetListByReturnedId(int returnedId)
        {
            return
                DB.SelectAllColumnsFrom<HiDealReturnedDetail>().Where(HiDealReturnedDetail.Columns.ReturnedId).IsEqualTo
                    (returnedId).ExecuteAsCollection<HiDealReturnedDetailCollection>();
        }

        #endregion HiDealReturned


        #region ViewHiDealCouponListSequence
        public ViewHiDealCouponListSequenceCollection GetHiDealCouponListSequenceListByOid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealCouponListSequence>().Where(ViewHiDealCouponListSequence.Columns.Guid).
                IsEqualTo(orderGuid).OrderAsc(ViewHiDealCouponListSequence.Columns.Sequence).ExecuteAsCollection<ViewHiDealCouponListSequenceCollection>();
        }
        #endregion

        #region ViewHiDealCoupon
        public ViewHiDealCoupon ViewHiDealCouponGet(int couponId)
        {
            ViewHiDealCoupon view = new ViewHiDealCoupon();
            view.LoadByParam(ViewHiDealCoupon.Columns.CouponId, couponId);
            return view;
        }
        public ViewHiDealCouponCollection ViewHiDealCouponGetListByOrderPk(int orderPk)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealCoupon>().Where(ViewHiDealCoupon.Columns.OrderPk).IsEqualTo(orderPk).ExecuteAsCollection<ViewHiDealCouponCollection>();
        }
        #endregion

        #region ViewHiDealCouponTrustStatus
        public ViewHiDealCouponTrustStatusCollection ViewHiDealCouponTrustStatusGetList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = ViewHiDealCouponTrustStatus.Columns.Sequence + " desc";
            QueryCommand qc = GetHDWhere(ViewHiDealCouponTrustStatus.Schema, filter);
            qc.CommandSql = "select * from " + ViewHiDealCouponTrustStatus.Schema.Provider.DelimitDbName(ViewHiDealCouponTrustStatus.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            var vfcCol = new ViewHiDealCouponTrustStatusCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        #endregion ViewHiDealCouponTrustStatus


        #region ViewHiDealReturned
        /// <summary>
        /// 取出某訂單的退貨資料。如果傳入狀態statuses，則只取出特定狀態的退貨單。
        /// </summary>
        /// <param name="orderPk"></param>
        /// <param name="statuses"></param>
        /// <returns></returns>
        public ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int orderPk, List<HiDealReturnedStatus> statuses)
        {
            return
                DB.SelectAllColumnsFrom<ViewHiDealReturned>().Where(ViewHiDealReturned.Columns.OrderPk).IsEqualTo(orderPk).
                And(ViewHiDealReturned.Columns.ReturnedStatus).In(statuses.Select(status => (int)status).ToList())
                    .ExecuteAsCollection<ViewHiDealReturnedCollection>();
        }
        /// <summary>
        /// 取出某訂單的退貨資料。
        /// </summary>
        /// <param name="orderPk"></param>
        /// <returns></returns>
        public ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int orderPk)
        {
            return
                DB.SelectAllColumnsFrom<ViewHiDealReturned>().Where(ViewHiDealReturned.Columns.OrderPk).IsEqualTo(orderPk)
                .ExecuteAsCollection<ViewHiDealReturnedCollection>();
        }

        public ViewHiDealReturnedCollection ViewHiDealReturnedGetList(int pageStart, int pageLength, string[] filter, string orderBy)
        {
            if (pageStart <= 0)
            {
                pageStart = 1;
            }
            var defOrderBy = ViewHiDealReturned.Columns.OrderId;
            var providerName = ViewHiDealReturned.Schema.Provider.Name;
            var tableName = ViewHiDealReturned.Schema.TableName;
            var qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealReturned.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            var cols = new ViewHiDealReturnedCollection();
            cols.LoadAndCloseReader(DataService.GetReader(qc));
            return cols;
        }

        #endregion ViewHiDealReturned

        #region ViewHiDealOrderDealOrderReturned

        public ViewHiDealOrderDealOrderReturnedCollection GetViewHiDealOrderDealOrderReturnedDialogData(int pageStart, int pageLength, string[] filter, string orderBy)
        {
            string defOrderBy = ViewHiDealOrderDealOrderReturned.Columns.OrderGuid;
            string providerName = ViewHiDealOrderDealOrderReturned.Schema.Provider.Name;
            string tableName = ViewHiDealOrderDealOrderReturned.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealOrderDealOrderReturned.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            ViewHiDealOrderDealOrderReturnedCollection vhdodorc = new ViewHiDealOrderDealOrderReturnedCollection();
            vhdodorc.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdodorc;
        }

        public int ViewHiDealOrderDealOrderReturnedDialogDataCount(string[] filter)
        {
            string providerName = ViewHiDealOrderDealOrderReturned.Schema.Provider.Name;
            string tableName = ViewHiDealOrderDealOrderReturned.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + ViewHiDealOrderDealOrderReturned.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public int ViewHiDealOrderDealOrderReturnedDialogDataCount(int did)
        {
            string providerName = ViewHiDealOrderDealOrderReturned.Schema.Provider.Name;
            string tableName = ViewHiDealOrderDealOrderReturned.Schema.TableName;
            string sql = "select count(1) from " + ViewHiDealOrderDealOrderReturned.Schema.Provider.DelimitDbName(tableName) + " with(nolock) where " +
                ViewHiDealOrderDealOrderReturned.Columns.DealId + "=@Did and " + ViewHiDealOrderDealOrderReturned.Columns.ReturnCreateTime + "<" + ViewHiDealOrderDealOrderReturned.Columns.DealEndTime;
            QueryCommand qc = new QueryCommand(sql, providerName);
            qc.AddParameter("@Did", did, DbType.Int16);
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetWhereQueryCommand(string providerName, string tableName, string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", providerName);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(tableName);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return qc;
        }

        #endregion

        #region ViewHiDealOrderMemberOrderShow

        public ViewHiDealOrderMemberOrderShowCollection ViewHiDealOrderMemberOrderShowDialogDataGet(int pageStart, int pageLength, string[] filter, string orderBy)
        {
            string defOrderBy = ViewHiDealOrderMemberOrderShow.Columns.CreateTime;
            string providerName = ViewHiDealOrderMemberOrderShow.Schema.Provider.Name;
            string tableName = ViewHiDealOrderMemberOrderShow.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealOrderMemberOrderShow.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            ViewHiDealOrderMemberOrderShowCollection vhdodorc = new ViewHiDealOrderMemberOrderShowCollection();
            vhdodorc.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdodorc;
        }

        public int ViewHiDealOrderMemberOrderShowDialogDataGetCount(string[] filter)
        {
            string providerName = ViewHiDealOrderMemberOrderShow.Schema.Provider.Name;
            string tableName = ViewHiDealOrderMemberOrderShow.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + ViewHiDealOrderMemberOrderShow.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewHiDealOrderMemberOrderShowCollection ViewHiDealOrderMemberOrderShowGetByOrderGuid(Guid orderGuid) 
        {
            return DB.SelectAllColumnsFrom<ViewHiDealOrderMemberOrderShow>()
                .Where(ViewHiDealOrderMemberOrderShow.Columns.Guid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewHiDealOrderMemberOrderShowCollection>();
        } 

        #endregion

        #region ViewHiDealSellerProduct
        public ViewHiDealSellerProductCollection ViewHiDealSellerProductGetList(int pageStart, int pageLength, string[] filter, string orderBy)
        {
            if (pageStart <= 0)
            {
                pageStart = 1;
            }
            var defOrderBy = ViewHiDealSellerProduct.Columns.DealId;
            var providerName = ViewHiDealSellerProduct.Schema.Provider.Name;
            var tableName = ViewHiDealSellerProduct.Schema.TableName;
            var qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealSellerProduct.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            var vhdsc = new ViewHiDealSellerProductCollection();
            vhdsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdsc;
        }
        public int ViewHiDealSellerProductDialogDataGetCount(string[] filter)
        {
            string providerName = ViewHiDealSellerProduct.Schema.Provider.Name;
            string tableName = ViewHiDealSellerProduct.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + ViewHiDealSellerProduct.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 過往好康查詢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="filter"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public ViewHiDealSellerProductCollection ViewHiDealSellerProductGetList(int pageStart, int pageLength, string CrossDeptTeam , string[] filter, string orderBy)
        {
            if (pageStart <= 0)
            {
                pageStart = 1;
            }
            var defOrderBy = ViewHiDealSellerProduct.Columns.DealId;
            var providerName = ViewHiDealSellerProduct.Schema.Provider.Name;
            var tableName = ViewHiDealSellerProduct.Schema.TableName;
            var qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealSellerProduct.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            qc.CommandSql = ViewHiDealSellerProductGetData(CrossDeptTeam, qc);
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            var vhdsc = new ViewHiDealSellerProductCollection();
            vhdsc.LoadAndCloseReader(DataService.GetReader(qc));
            return vhdsc;
        }
        public int ViewHiDealSellerProductDialogDataGetCount(string CrossDeptTeam, string[] filter)
        {
            string providerName = ViewHiDealSellerProduct.Schema.Provider.Name;
            string tableName = ViewHiDealSellerProduct.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + ViewHiDealSellerProduct.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            qc.CommandSql = ViewHiDealSellerProductGetData(CrossDeptTeam, qc);
            return (int)DataService.ExecuteScalar(qc);
        }

        private string ViewHiDealSellerProductGetData(string CrossDeptTeam, QueryCommand qc)
        {
            #region CrossDeptTeam
            if (CrossDeptTeam != string.Empty)
            {
                if (qc.Parameters.Count == 0 && !qc.CommandSql.Contains(SqlFragment.WHERE))
                {
                    qc.CommandSql += SqlFragment.WHERE;
                }
                else
                {
                    int whIdx = qc.CommandSql.IndexOf(" where ", StringComparison.InvariantCultureIgnoreCase) + 7;
                    qc.CommandSql = qc.CommandSql.Substring(0, whIdx) + " ( " + qc.CommandSql.Substring(whIdx);
                    qc.CommandSql = qc.CommandSql.TrimEnd(';') + " ) " + SqlFragment.AND;
                }

                string tmpsql = "";
                foreach (var c in CrossDeptTeam.Split('/'))
                {
                    if (c.Contains("["))
                    {
                        tmpsql += "(dept_id='" + c.Substring(0, c.IndexOf("[")) + "' and team_no in (" + c.Substring(c.IndexOf("[") + 1, c.IndexOf("]") - c.IndexOf("[") - 1) + ")) or ";
                    }
                    else
                    {
                        tmpsql += " (dept_id='" + c + "') or ";
                    }


                }
                tmpsql = "(" + tmpsql.TrimEnd("or ") + ")";

                qc.CommandSql += tmpsql;


            }

            #endregion

            return qc.CommandSql;
        }



        public ViewHiDealSellerProductCollection ViewHiDealSellerProductGetDeal(string dealid)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealSellerProduct>().
             Where(ViewHiDealSellerProduct.Columns.DealId).IsEqualTo(dealid).
             ExecuteAsCollection<ViewHiDealSellerProductCollection>();
        }

        #endregion ViewHiDealSellerProduct

        #region ViewHiDealProductSaleInfo
        public ViewHiDealProductSaleInfoCollection GetViewHiDealProductSaleInfoBySellerGuidOnStart(Guid seGuid)
        {
            return
                DB.Select().From(ViewHiDealProductSaleInfo.Schema)
                    .Where(ViewHiDealProductSaleInfo.Columns.SellerGuid).IsEqualTo(seGuid)
                    .And(ViewHiDealProductSaleInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today)
                    .ExecuteAsCollection<ViewHiDealProductSaleInfoCollection>();
        }

        public ViewHiDealProductSaleInfoCollection GetViewHiDealProductSaleInfoBySellerAndStoreGuidOnStart(Guid seGuid, Guid stGuid)
        {
            return
                DB.Select().From(ViewHiDealProductSaleInfo.Schema)
                    .Where(ViewHiDealProductSaleInfo.Columns.SellerGuid).IsEqualTo(seGuid)
                    .And(ViewHiDealProductSaleInfo.Columns.StoreGuid).IsEqualTo(stGuid)
                    .And(ViewHiDealProductSaleInfo.Columns.UseStartTime).IsLessThanOrEqualTo(DateTime.Today)
                    .ExecuteAsCollection<ViewHiDealProductSaleInfoCollection>();
        }

        #endregion


        #region ViewHiDealReturnedLight
        public ViewHiDealReturnedLightCollection ViewHiDealReturnedLightGetList(int pageStart, int pageLength, string[] filter, string orderBy)
        {
            if (pageStart <= 0)
            {
                pageStart = 1;
            }
            var defOrderBy = ViewHiDealReturnedLight.Columns.OrderId;
            var providerName = ViewHiDealReturnedLight.Schema.Provider.Name;
            var tableName = ViewHiDealReturnedLight.Schema.TableName;
            var qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select * from " + ViewHiDealReturnedLight.Schema.Provider.DelimitDbName(tableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);

            var cols = new ViewHiDealReturnedLightCollection();
            cols.LoadAndCloseReader(DataService.GetReader(qc));
            return cols;
        }
        public int ViewHiDealReturnedLightDialogDataGetCount(string[] filter)
        {
            string providerName = ViewHiDealReturnedLight.Schema.Provider.Name;
            string tableName = ViewHiDealReturnedLight.Schema.TableName;
            QueryCommand qc = GetWhereQueryCommand(providerName, tableName, filter);
            qc.CommandSql = "select count(1) from " + ViewHiDealReturnedLight.Schema.Provider.DelimitDbName(tableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion ViewHiDealReturnedLight

        #region ViewHiDealOrderReturnQuantity
        /// <summary>
        /// 計算銷售和退貨金額
        /// </summary>
        public ViewHiDealOrderReturnQuantityCollection ViewHiDealOrderReturnQuantityGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealOrderReturnQuantity>().Where(ViewHiDealOrderReturnQuantity.Columns.ProductId).
                IsEqualTo(productId).ExecuteAsCollection<ViewHiDealOrderReturnQuantityCollection>();
        }

        public ViewHiDealOrderReturnQuantityCollection ViewHiDealOrderReturnQuantityGetListByDealId(int dealId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealOrderReturnQuantity>().Where(ViewHiDealOrderReturnQuantity.Columns.HiDealId).
                IsEqualTo(dealId).ExecuteAsCollection<ViewHiDealOrderReturnQuantityCollection>();
        }
        #endregion ViewHiDealOrderReturnQuantity

        #region ViewHiDealProductInfo
        public ViewHiDealProductHouseInfoCollection ViewHiDealProductHouseInfoGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealProductHouseInfo>().Where(ViewHiDealProductHouseInfo.Columns.ProductId).
                IsEqualTo(productId).ExecuteAsCollection<ViewHiDealProductHouseInfoCollection>();
        }

        public ViewHiDealProductShopInfoCollection ViewHiDealProductShopInfoGetListByProductId(int productId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealProductShopInfo>().Where(ViewHiDealProductShopInfo.Columns.ProductId).
                IsEqualTo(productId).ExecuteAsCollection<ViewHiDealProductShopInfoCollection>();
        }


        #endregion

        #region ViewHiDealTimeSlot
        public ViewHiDealTimeslotCollection GetViewHiDealTimeslotCollectionByDate(int cityid, DateTime date)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealTimeslot>().Where(ViewHiDealTimeslot.Columns.StartTime).IsLessThanOrEqualTo(date)
                .And(ViewHiDealTimeslot.Columns.EndTime).IsGreaterThan(date)
                .And(ViewHiDealTimeslot.Columns.IsMain).IsEqualTo(true)
                .And(ViewHiDealTimeslot.Columns.CityCodeId).IsEqualTo(cityid)
                .OrderAsc(ViewHiDealTimeslot.Columns.Seq).ExecuteAsCollection<ViewHiDealTimeslotCollection>();
        }
        #endregion

        #region ACH

        public List<ViewHiDealWeeklyPayReport> ViewHiDealWeeklyPayReportGetList(DateTime? dateStart, DateTime? dateEnd,
            Guid? reportGuid, int? productId, int? reportId, bool? isSummary)
        {
            var query = DB.SelectAllColumnsFrom<ViewHiDealWeeklyPayReport>();
            bool hasCondition = false;
            if (dateStart != null && dateEnd != null)
            {
                hasCondition = true;
                query.Where(ViewHiDealWeeklyPayReport.Columns.IntervalStart).IsBetweenAnd(dateStart, dateEnd);
            }
            if (reportGuid != null)
            {
                if (hasCondition)
                {
                    query.And(ViewHiDealWeeklyPayReport.Columns.ReportGuid).IsEqualTo(reportGuid);
                }
                else
                {
                    hasCondition = true;
                    query.Where(ViewHiDealWeeklyPayReport.Columns.ReportGuid).IsEqualTo(reportGuid);
                }
            }
            if (productId != null)
            {
                if (hasCondition)
                {
                    query.And(ViewHiDealWeeklyPayReport.Columns.ProductId).IsEqualTo(productId.Value);
                }
                else
                {
                    hasCondition = true;
                    query.Where(ViewHiDealWeeklyPayReport.Columns.ProductId).IsEqualTo(productId.Value);
                }
            }
            if (reportId != null)
            {
                if (hasCondition)
                {
                    query.And(ViewHiDealWeeklyPayReport.Columns.Id).IsEqualTo(reportId.Value);
                }
                else
                {
                    hasCondition = true;
                    query.Where(ViewHiDealWeeklyPayReport.Columns.Id).IsEqualTo(reportId.Value);
                }
            }
            if (isSummary != null)
            {
                if (hasCondition)
                {
                    query.And(ViewHiDealWeeklyPayReport.Columns.IsSummary).IsEqualTo(isSummary);
                }
                else
                {
                    hasCondition = true;
                    query.Where(ViewHiDealWeeklyPayReport.Columns.IsSummary).IsEqualTo(isSummary);
                }
            }
            return query.ExecuteAsCollection<ViewHiDealWeeklyPayReportCollection>().ToList();
        }

        #endregion

        #region ViewHiDealCouponStoreListCount
        /// <summary>
        /// 依檔次ID查詢此檔次每個商品憑證產生總數
        /// </summary>
        /// <param name="DealId">檔次ID</param>
        /// <returns></returns>

        public ViewHiDealCouponStoreListCountCollection GetViewHiDealCouponStoreListCount(int DealId)
        {
            return DB.SelectAllColumnsFrom<ViewHiDealCouponStoreListCount>()
                .Where(ViewHiDealCouponStoreListCount.Columns.DealId).IsEqualTo(DealId)
                .ExecuteAsCollection<ViewHiDealCouponStoreListCountCollection>();
        }
        #endregion

        #region ViewHiDealProductSellerStore

        /// <summary>
        /// 商家後臺_核銷查詢:根據輸入之查詢條件取出已成檔之憑證檔次、賣家及分店資料
        /// </summary>
        /// <param name="storeGuidList">具檢視權限之分店Guid list 以,連結多筆資料</param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public ViewHiDealProductSellerStoreCollection ViewHiDealProductSellerStoreGetToShopDealList(string storeGuidList, params string[] filter)
        {
            QueryCommand qc = GetHDWhere(ViewHiDealProductSellerStore.Schema, filter);
            qc.CommandSql = "select * from " + ViewHiDealProductSellerStore.Schema.TableName + " with(nolock) " + qc.CommandSql;
            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " and " + ViewHiDealProductSellerStore.Columns.IsInStore + " = 1 ";

            //加上storeGuidList篩選 為避免無分店資料之檔次被剔除 針對store_guid為null的資料須另做轉換
            //filter內若加上isnull(store_guid..)會出現無此欄位錯誤 故只得將storeGuidList另做處理
            if (storeGuidList != string.Empty && storeGuidList != "00000000-0000-0000-0000-000000000000")
                qc.CommandSql = qc.CommandSql + " and isnull(store_guid, '00000000-0000-0000-0000-000000000000') in ('" + storeGuidList.Replace(",", "','") + "')";

            ViewHiDealProductSellerStoreCollection data = new ViewHiDealProductSellerStoreCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region ViewHiDealExpiration

        public ViewHiDealExpirationCollection ViewHiDealExpirationGetList(Guid productGuid)
        {
            string sql = string.Format(@"
				SELECT *
				FROM {0} with (nolock)
				WHERE {1} = @productGuid"
                , ViewHiDealExpiration.Schema.TableName
                , ViewHiDealExpiration.Columns.ProductGuid);

            QueryCommand qc = new QueryCommand(sql, ViewHiDealExpiration.Schema.Provider.Name);
            qc.AddParameter("productGuid", productGuid, DbType.Guid);
            ViewHiDealExpirationCollection result = new ViewHiDealExpirationCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewHiDealExpirationCollection ViewHiDealExpirationGetList(DateTime usageTime, int endBuffer = 0)
        {
            if (usageTime == DateTime.MinValue)
                return new ViewHiDealExpirationCollection();

            /*			
             * ... WHERE datetimeColumn + endBuffer >= @usageTime  ...
             * there are values '9999-12-31 23:59:59.997' inside our database, 
             * causing overflow exception when adding more days into it.
             */

            string sql = string.Format(@"
				SELECT *
				FROM {0} with (nolock)
				WHERE (
					{1} >= @usageTime {6}
					OR {2} >= @usageTime {6}
					OR {3} >= @usageTime {6}
					OR {4} >= @usageTime {6}
				) AND {5} <= @usageTime"
                , ViewHiDealExpiration.Schema.TableName
                , ViewHiDealExpiration.Columns.UseEndTime
                , ViewHiDealExpiration.Columns.ProductChangedExpireTime
                , ViewHiDealExpiration.Columns.SellerCloseDownDate
                , ViewHiDealExpiration.Columns.StoreCloseDownDate
                , ViewHiDealExpiration.Columns.UseStartTime
                , endBuffer == 0 ? string.Empty : string.Format(" - {0} ", endBuffer)
                );

            QueryCommand qc = new QueryCommand(sql, ViewHiDealExpiration.Schema.Provider.Name);
            qc.AddParameter("usageTime", usageTime, DbType.DateTime);

            ViewHiDealExpirationCollection result = new ViewHiDealExpirationCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion

        #region ViewHiDealCouponTrustStatusLog
        public ViewHiDealCouponTrustLogCollection ViewHiDealCouponTrustStatusLogGet(string prefix, string sequence,string queryType)
        {
            string sql = "select * from view_hi_deal_coupon_trust_log where 1=1 ";
            if (prefix != "")
                sql += " and prefix = @prefix";
            

            if (queryType.Equals("sequence"))
            {
                sql += " and sequence = @sequence ";
            }
            else if (queryType.Equals("code"))
            {
                sql += " and code = @code ";
            }
            QueryCommand qc = new QueryCommand(sql, ViewHiDealCouponTrustLog.Schema.Provider.Name);
            ViewHiDealCouponTrustLogCollection result = new ViewHiDealCouponTrustLogCollection();
            qc.Parameters.Add("@sequence", sequence, System.Data.DbType.String);
            qc.Parameters.Add("@code", sequence, System.Data.DbType.String);

            if (prefix != "")
                qc.Parameters.Add("@prefix", prefix, System.Data.DbType.String);
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
            
            //return DB.Select().From<ViewHiDealCouponTrustLog>()
            //    .Where(ViewHiDealCouponTrustLog.Columns.Sequence).IsEqualTo(sequence).ExecuteAsCollection<ViewHiDealCouponTrustLogCollection>(); ;
        }

        #endregion

        #region ViewSellerHideal

        public ViewSellerHiDealCollection ViewSellerHiDealGetList(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<ViewSellerHiDeal>()
                .Where(ViewSellerHiDeal.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .OrderDesc(ViewSellerHiDeal.Columns.DealId)
                .ExecuteAsCollection<ViewSellerHiDealCollection>();
        }

        #endregion
    }
}



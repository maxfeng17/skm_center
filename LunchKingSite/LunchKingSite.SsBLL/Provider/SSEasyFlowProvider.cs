﻿using LunchKingSite.Core;
using LunchKingSite.EasyFlow.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace LunchKingSite.SsBLL.Provider
{
    public class SSEasyFlowProvider : IEasyFlowProvider
    {
        #region Employee
        public List<Employee> EmployeeyGet()
        {
            //目前在職的人員
//            string sql = @" select top 10 emp.EmployeeId,emp.DepartmentId,emp.DirectorId,emp.CnName,emp.WorkingAgeBeginDate,emp.LastWorkDate,emp.Email
//                            from Employee emp with(nolock)
//                            where emp.LastWorkDate >= getdate()";
            string sql = @" select emp.Code,emp.EmployeeId,emp.DepartmentId,emp.DirectorId,emp.CnName,emp.WorkingAgeBeginDate,emp.LastWorkDate,emp.Email
                            from Employee emp with(nolock)
                            where emp.LastWorkDate >= getdate()
                            and emp.ITCode<>'test'
                            order by emp.Code asc";
            QueryCommand qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            DataTable dt = new DataTable();
            IDataReader idr = DataService.GetReader(qc);
            using (idr)
                dt.Load(idr);

            List<Employee> emps = (from q in dt.AsEnumerable()
                                      select new Employee {
                                          EmployeeId = q.Field<Guid>("EmployeeId"),
                                          DepartmentId = q.Field<Guid>("DepartmentId"),
                                          DirectorId = q.Field<Guid>("DirectorId"),
                                          CnName = q.Field<string>("CnName"),
                                          WorkingAgeBeginDate = q.Field<DateTime>("WorkingAgeBeginDate"),
                                          LastWorkDate = q.Field<DateTime>("LastWorkDate"),
                                          Email = q.Field<string>("Email")
                                      }).OrderBy(x=>x.Code).ToList();

            return emps;
        }

        public Employee EmployeeyGetByEmployeeId(Guid eid)
        {
            string sql = @" select emp.Code,emp.EmployeeId,emp.DepartmentId,emp.DirectorId,emp.CnName,emp.WorkingAgeBeginDate,emp.LastWorkDate,emp.Email
                            from Employee emp with(nolock)
                            where emp.EmployeeId=@eid";
            QueryCommand qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            qc.AddParameter("@eid", eid, DbType.Guid);
            Employee data = new Employee();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EmployeeCollection EmployeeyGetByEmail(string email)
        {
            //目前在職的人員
            string sql = @" select emp.Code,emp.EmployeeId,emp.DepartmentId,emp.DirectorId,emp.CnName,emp.WorkingAgeBeginDate,emp.LastWorkDate,emp.Email
                            from Employee emp with(nolock)
                            where emp.LastWorkDate >= getdate()
                            and emp.ITCode<>'test'
                            and emp.Email=@email";
            QueryCommand qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            qc.AddParameter("@email", email, DbType.String);
            EmployeeCollection data = new EmployeeCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public EmployeeCollection EmployeeyGetByDepartmentId(Guid departmentId)
        {
            //目前在職的人員
            string sql = @" select emp.Code,emp.EmployeeId,emp.DepartmentId,emp.DirectorId,emp.CnName,emp.WorkingAgeBeginDate,emp.LastWorkDate,emp.Email
                            from Employee emp with(nolock)
                            where emp.LastWorkDate >= getdate() 
                            and emp.ITCode<>'test'
                            and emp.DepartmentId=@DepartmentId";
            QueryCommand qc = new QueryCommand(sql, Employee.Schema.Provider.Name);
            qc.AddParameter("@DepartmentId", departmentId, DbType.Guid);
            EmployeeCollection data = new EmployeeCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        #endregion

        #region Department
        public DepartmentCollection DepartmentGet()
        {
            //未結束的單位
            string sql = @" select DepartmentId,Name,ShortName,ParentId,Flag,IsShowInOrg,DeptLevel
                            from Department dept with(nolock)
                            where Flag = 1 ";
            QueryCommand qc = new QueryCommand(sql, Department.Schema.Provider.Name);
            DepartmentCollection data = new DepartmentCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        #endregion Department
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.SsBLL
{
    public class SSAccountingProvider : IAccountingProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region BankAccount
        /*
         此 table 只可以新增, 不可修改
         */

        private void BankAccountSet(BankAccount account)
        {
            if (account.IsNew)
            {
                DB.Save<BankAccount>(account);
            }
        }

        public int BankAccountGetId(string bankNo, string branchNo, string accountNo, string companyId, string accountTitle)
        {
            accountNo = accountNo.PadLeft(14, '0');     // 帳號固定14碼

            BankAccount x = new BankAccount(); 
            
            SqlQuery query = DB.SelectAllColumnsFrom<BankAccount>()
                .Where(BankAccount.Columns.BankNo).IsEqualTo(bankNo)
                .And(BankAccount.Columns.BranchNo).IsEqualTo(branchNo)
                .And(BankAccount.Columns.AccountNo).IsEqualTo(accountNo)
                .And(BankAccount.Columns.CompanyId).IsEqualTo(companyId)
                .And(BankAccount.Columns.AccountTitle).IsEqualTo(accountTitle);
            
            x.LoadAndCloseReader(query.ExecuteReader());            

            if (x.IsLoaded)
            {
                return x.Id;
            }
            else
            {
                x.BankNo = bankNo;
                x.BranchNo = branchNo;
                x.AccountNo = accountNo;
                x.CompanyId = companyId;
                x.AccountTitle = accountTitle;
                BankAccountSet(x);
                return BankAccountGetId(bankNo, branchNo, accountNo, companyId, accountTitle);
            }
        }
                
        #endregion

        #region FundTransfer

        public FundTransfer FundTransferGet(int id)
        {
            return DB.Get<FundTransfer>(id);
        }

        public void FundTransferSet(FundTransfer fund)
        {
            DB.Save(fund);
        }

        public void FundTransferSetList(FundTransferCollection funds)
        {
            DB.SaveAll(funds);
        }

        #endregion

        #region Receivable 應收帳款
        public bool ReceivableSet(Receivable obj)
        {
            DB.Save(obj);
            return true;
        }
        public int ReceivableSetList(ReceivableCollection objList)
        {
            return DB.SaveAll(objList);
        }
        public ReceivableCollection ReceivableGetList(Guid classificationGuid, AccountsClassification classification)
        {
            return DB.SelectAllColumnsFrom<Receivable>().Where(Receivable.Columns.ClassificationGuid).IsEqualTo(classificationGuid).And(
                Receivable.Columns.AccountsClassification).IsEqualTo((int)classification).ExecuteAsCollection
                <ReceivableCollection>();
        }
        #endregion Receivable 應收帳款

        #region Payable 應付帳款

        public bool PayableSet(Payable obj)
        {
            return DB.Save(obj) == 1;
        }
        public int PayableSetList(PayableCollection objList)
        {
            return DB.SaveAll(objList);
        }

        /// <summary>
        /// 依據 會計帳款來源 查詢應付帳款資料
        /// </summary>
        /// <param name="classificationGuid"></param>
        /// <param name="classification"></param>
        /// <returns></returns>
        public PayableCollection PayableGetList(Guid classificationGuid, AccountsClassification classification)
        {
            return DB.SelectAllColumnsFrom<Payable>().Where(Payable.Columns.ClassificationGuid).IsEqualTo(classificationGuid).And(
            Payable.Columns.AccountsClassification).IsEqualTo((int)classification).ExecuteAsCollection
            <PayableCollection>();
        }

        #endregion Payable 應付帳款

        #region BalanceSheet 對帳單

		public BalanceSheet BalanceSheetGet(int balanceSheetId)
		{
			return DB.Get<BalanceSheet>(balanceSheetId);
		}

        public int BalanceSheetSet(BalanceSheet bs)
        {
            return DB.Save(bs);
        }

        public int BalanceSheetEstAmountSet(BalanceSheet bs,int vendorPositivePaymentOverdueAmount,int vendorNegativePaymentOverdueAmount)
        {
            //原正負對帳單 更新為發票金額為0對帳單
            if (bs.EstAmount == 0 && vendorPositivePaymentOverdueAmount == 0 && vendorNegativePaymentOverdueAmount == 0 && bs.PayReportId == null)
            {
                bs.IsConfirmedReadyToPay = true;
                bs.ConfirmedUserName = "sys";
                bs.ConfirmedTime = DateTime.Now;
                bs.IsReceiptReceived = true;
                bs.IsDetailFixed = true;
                //0元對帳單無須回報匯款結果
                bs.PayReportId = 0;
            }

            //原0元對帳單 更新為發票金額為正負金額對帳單
            else if ((bs.EstAmount != 0 ||  vendorPositivePaymentOverdueAmount != 0 || vendorNegativePaymentOverdueAmount != 0) && bs.PayReportId == 0)
            {
                //原0元對帳單 系統自動確認狀態 才須取消確認
                if (bs.IsConfirmedReadyToPay && bs.ConfirmedUserName == "sys")
                { 
                    bs.IsConfirmedReadyToPay = false;
                    bs.ConfirmedUserName = null;
                    bs.ConfirmedTime = null;
                    bs.IsReceiptReceived = false;
                    bs.PayReportId = null;
                }
                
            }

            return DB.Save(bs);
        }

        public BalanceSheet BalanceSheetGetDetailNotFixed(Guid productGuid, BusinessModel productType, Guid? storeGuid = null)
        {
            BalanceSheet bs;

            if (storeGuid == null)
            {
                bs = DB.SelectAllColumnsFrom<BalanceSheet>()
                    .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                    .And(BalanceSheet.Columns.ProductType).IsEqualTo((int)productType)
                    .And(BalanceSheet.Columns.StoreGuid).IsNull()
                    .And(BalanceSheet.Columns.IsDetailFixed).IsEqualTo(0)
                    .ExecuteSingle<BalanceSheet>();
            }
            else
            {
                bs = DB.SelectAllColumnsFrom<BalanceSheet>()
                    .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                    .And(BalanceSheet.Columns.ProductType).IsEqualTo((int)productType)
                    .And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid)
                    .And(BalanceSheet.Columns.IsDetailFixed).IsEqualTo(0)
                    .ExecuteSingle<BalanceSheet>();
            }
            if (bs != null && bs.IsLoaded && !bs.IsNew)
            {
                return bs;
            }
            
            return null;
        }

		public BalanceSheet BalanceSheetGetLatest(BusinessModel productType, Guid productGuid, Guid? storeGuid, BalanceSheetGenerationFrequency frequency)
		{
			BalanceSheet result;

		    SqlQuery query = DB.SelectAllColumnsFrom<BalanceSheet>().Top("1")
		        .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                .And(BalanceSheet.Columns.ProductType).IsEqualTo(productType);

            if (storeGuid == null)
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsNull();
            }
            else
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid);
            }

            if (frequency != BalanceSheetGenerationFrequency.ManualTriggeredBalanceSheet)
            {
                query.And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo(frequency);
            }

            result = query.OrderDesc(BalanceSheet.Columns.IntervalEnd)
                    .ExecuteSingle<BalanceSheet>();

			return result.IsNew ? null : result;
		}

        public int BalanceSheetCollectionSet(BalanceSheetCollection sheets)
        {
            return DB.SaveAll(sheets);
        }

        public BalanceSheetCollection BalanceSheetGetListByProductGuid(Guid productGuid)
        {
            BalanceSheetCollection bsCollection;
            bsCollection = DB.SelectAllColumnsFrom<BalanceSheet>()
                .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                .ExecuteAsCollection<BalanceSheetCollection>();
            return bsCollection ?? new BalanceSheetCollection();
        }

        public BalanceSheetCollection BalanceSheetGetListByProductGuidStoreGuid(Guid productGuid, Guid? storeGuid, BalanceSheetGenerationFrequency? frequency = null)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<BalanceSheet>()
                .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid);
            
            if (storeGuid.HasValue)
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid.Value);
            }
            else
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsNull();
            }

            if (frequency.HasValue)
            {
                query.And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo((int)frequency);
            }

            return query.ExecuteAsCollection<BalanceSheetCollection>() ?? new BalanceSheetCollection();
        }
        public BalanceSheetCollection BalanceSheetGetListByProductGuidStoreGuid(List<Guid> productGuids)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<BalanceSheet>()
               .Where(BalanceSheet.Columns.ProductGuid).In(productGuids);

            return query.ExecuteAsCollection<BalanceSheetCollection>() ?? new BalanceSheetCollection();
        }

        public BalanceSheetCollection BalanceSheetGetWeekBalanceSheetsByMonth(Guid productGuid, BusinessModel productType, int year, int month)
        {
            BalanceSheetCollection bsCollection = DB.SelectAllColumnsFrom<BalanceSheet>()
                    .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                    .And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
                    .And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo(BalanceSheetGenerationFrequency.WeekBalanceSheet)
                    .And(BalanceSheet.Columns.Year).IsEqualTo(year)
                    .And(BalanceSheet.Columns.Month).IsEqualTo(month)
                    .ExecuteAsCollection<BalanceSheetCollection>();
            
            return bsCollection ?? new BalanceSheetCollection();
        }

        public BalanceSheetCollection BalanceSheetGetWeekBalanceSheetsByMonth(Guid productGuid, Guid? storeGuid, BusinessModel productType, int year, int month)
        {
            BalanceSheetCollection result;

            SqlQuery query = DB.SelectAllColumnsFrom<BalanceSheet>()
                .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                .And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
                .And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo(BalanceSheetGenerationFrequency.WeekBalanceSheet)
                .And(BalanceSheet.Columns.Year).IsEqualTo(year)
                .And(BalanceSheet.Columns.Month).IsEqualTo(month);

            if (storeGuid == null)
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsNull();
            }
            else
            {
                query.And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid);
            }

            result = query.ExecuteAsCollection<BalanceSheetCollection>();

            return result ?? new BalanceSheetCollection();
        }

        public int BalanceSheetDuplicationCountCheck(Guid productGuid, BusinessModel productType, Guid? storeGuid, DateTime intervalStart, DateTime intervalEnd, BalanceSheetType bsType)
        {
            if (bsType == BalanceSheetType.ManualTriggered)
                return 0;

            if (storeGuid == null)
            {
                return DB.Select().From<BalanceSheet>()
                    .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                    .And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
                    .And(BalanceSheet.Columns.StoreGuid).IsNull()
                    .And(BalanceSheet.Columns.IntervalStart).IsEqualTo(intervalStart)
                    .And(BalanceSheet.Columns.IntervalEnd).IsEqualTo(intervalEnd)
                    .GetRecordCount();
            }
            else
            {
                return DB.Select().From<BalanceSheet>()
                    .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                    .And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
                    .And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid)
                    .And(BalanceSheet.Columns.IntervalStart).IsEqualTo(intervalStart)
                    .And(BalanceSheet.Columns.IntervalEnd).IsEqualTo(intervalEnd)
                    .GetRecordCount();
            }
        }

		public int BalanceSheetDuplicationCountCheck(Guid productGuid, BusinessModel productType, Guid? storeGuid, int year, int month, BalanceSheetType bsType)
		{
			if (bsType != BalanceSheetType.WeeklyPayMonthBalanceSheet && 
				bsType != BalanceSheetType.ManualWeeklyPayMonthBalanceSheet)
				return -1;

			if (storeGuid == null)
			{
				return DB.Select().From<BalanceSheet>()
					.Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
					.And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
					.And(BalanceSheet.Columns.StoreGuid).IsNull()
					.And(BalanceSheet.Columns.Year).IsEqualTo(year)
					.And(BalanceSheet.Columns.Month).IsEqualTo(month)
					.And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo(BalanceSheetGenerationFrequency.MonthBalanceSheet)
					.GetRecordCount();
			}
			else
			{
				return DB.Select().From<BalanceSheet>()
					.Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
					.And(BalanceSheet.Columns.ProductType).IsEqualTo(productType)
					.And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid)
					.And(BalanceSheet.Columns.Year).IsEqualTo(year)
					.And(BalanceSheet.Columns.Month).IsEqualTo(month)
					.And(BalanceSheet.Columns.GenerationFrequency).IsEqualTo(BalanceSheetGenerationFrequency.MonthBalanceSheet)
					.GetRecordCount();
			}
		}

        public void CompleteWeekBalanceSheetMonthInfo()
        {            
            string sql =
                string.Format(
                    @"
;WITH sheet AS (
  SELECT 
    bs.*
    ,DATEPART(YY, wpr.response_time) as response_year
    ,DATEPART(MM, wpr.response_time) as response_month    
  FROM balance_sheet bs
  INNER JOIN weekly_pay_report wpr
    ON bs.pay_report_id = wpr.id 
    AND bs.product_type = 0
  WHERE bs.generation_frequency = 1
    AND bs.balance_sheet_type = 1
    AND bs.month is null
    AND wpr.result = '00'
    AND wpr.response_time is not null
    AND bs.pay_report_id <> 0
)
UPDATE sheet SET sheet.year = response_year, sheet.month = response_month");
            QueryCommand command = new QueryCommand(sql, BalanceSheet.Schema.Provider.Name);
            command.ToDbCommand().ExecuteNonQuery();
        }

        public BalanceSheetCollection BalanceSheetGetList(string orderBy, params string[] filter)
        {
            QueryCommand qc = GetDCWhere(BalanceSheet.Schema, filter);
            qc.CommandSql = "select * from " + BalanceSheet.Schema.Provider.DelimitDbName(BalanceSheet.Schema.TableName) + " with(nolock) " + qc.CommandSql;
                        
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
            }

            BalanceSheetCollection bsCol = new BalanceSheetCollection();
            bsCol.LoadAndCloseReader(DataService.GetReader(qc));

            return bsCol;
        }

       protected QueryCommand GetDCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);
                qry = SSHelper.GetQueryByFilterOrder(qry, null, filter);
                qc.CommandSql += DataProvider.BuildWhere(qry);
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }

        public BalanceSheetCollection BalanceSheetGetListByIds(List<int> Ids)
        {
            var infos = new BalanceSheetCollection();
            List<int> tempIds = Ids.Distinct().ToList();
            int idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<BalanceSheet>()
                        .Where(BalanceSheet.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<BalanceSheetCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }
            return infos;            
        }

        public BalanceSheetCollection GetWeekBalanceSheetHasNoMonthBalanceSheet(Guid productGuid, BusinessModel productType, IEnumerable<Guid?> storeGuids, int year, int month)
        {
            var sql = GetWeekBalanceSheetHasNoMonthBalanceSheetSql();
            sql.Append(string.Format(@"and bsw.{0} = @productGuid 
and bsw.{1} = @productType  
"
                                        , BalanceSheet.Columns.ProductGuid
                                        , BalanceSheet.Columns.ProductType));

            if (storeGuids.Any(x => x.HasValue))
            {
                sql.Append(string.Format(" and bsw.{0} in ('{1}')"
                                        , BalanceSheet.Columns.StoreGuid
                                        , string.Join("','", storeGuids)));
            }

            var qc = new QueryCommand(sql.ToString(), BalanceSheet.Schema.Provider.Name);
            qc.AddParameter("year", year, DbType.Int32);
            qc.AddParameter("month", month, DbType.Int32);
            qc.AddParameter("productGuid", productGuid, DbType.Guid);
            qc.AddParameter("productType", (int)productType, DbType.Int32);

            var result = new BalanceSheetCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public BalanceSheetCollection GetWeekBalanceSheetHasNoMonthBalanceSheet(int year, int month)
        {
            var sql = GetWeekBalanceSheetHasNoMonthBalanceSheetSql();

            var qc = new QueryCommand(sql.ToString(), BalanceSheet.Schema.Provider.Name);
            qc.AddParameter("year", year, DbType.Int32);
            qc.AddParameter("month", month, DbType.Int32);

            var result = new BalanceSheetCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        private StringBuilder GetWeekBalanceSheetHasNoMonthBalanceSheetSql()
        {
            var sql = new StringBuilder();

            sql.Append(string.Format(@"
select bsw.*
from {0} as bsw with(nolock)
left join (
	select *
	from {0} with(nolock)
	where {6} in ({10}, {11})
) as bsm on bsm.{1} = bsw.{1} and bsm.{2} = bsw.{2} and bsm.{3} = bsw.{3} and bsm.{4} = bsw.{4} and bsm.{5} = bsw.{5}
join weekly_pay_report as pr with(nolock) on bsw.{7} = pr.{8}
where bsw.{4} * 100 + bsw.{5} <= @year * 100 + @month 
and bsw.{6} in ({12}, {13})
and bsw.{7} > 0
and bsm.{8} is null
and pr.{9} = 1 
"
                                        , BalanceSheet.Schema.TableName
                                        , BalanceSheet.Columns.ProductGuid
                                        , BalanceSheet.Columns.ProductType
                                        , BalanceSheet.Columns.StoreGuid
                                        , BalanceSheet.Columns.Year
                                        , BalanceSheet.Columns.Month
                                        , BalanceSheet.Columns.BalanceSheetType
                                        , BalanceSheet.Columns.PayReportId
                                        , BalanceSheet.Columns.Id
                                        , WeeklyPayReport.Columns.IsTransferComplete
                                        , (int)BalanceSheetType.WeeklyPayMonthBalanceSheet
                                        , (int)BalanceSheetType.ManualWeeklyPayMonthBalanceSheet
                                        , (int)BalanceSheetType.WeeklyPayWeekBalanceSheet
                                        , (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet));

            return sql;
        }

        public DataTable GetPayByBillBalanceSheetHasNegativeAmount(DateTime checkStartTime, DateTime checkEndTime)
        {
            var sql = string.Format(@"select case when cd.MainBusinessHourGuid is null then bs.product_guid else cd.MainBusinessHourGuid end as product_guid, 
bs.interval_start, bs.interval_end,
sum(bs.est_amount) as est_amount 
from balance_sheet as bs with(nolock)
left join combo_deals as cd with(nolock) on cd.BusinessHourGuid = bs.product_guid
where bs.balance_sheet_type in ({0}, {1}, {2})
and bs.create_time > @checkStartTime
and bs.create_time < @checkEndTime
group by case when cd.MainBusinessHourGuid is null then bs.product_guid else cd.MainBusinessHourGuid end, 
bs.interval_start, bs.interval_end 
having sum(est_amount) < 0  ",
                                (int)BalanceSheetType.FlexibleToHouseBalanceSheet,
                                (int)BalanceSheetType.MonthlyToHouseBalanceSheet,
                                (int)BalanceSheetType.WeeklyToHouseBalanceSheet);
            var idr = new InlineQuery().ExecuteReader(sql, string.Format("{0:yyyy/MM/dd}", checkStartTime), string.Format("{0:yyyy/MM/dd}", checkEndTime));
            var result = new DataTable();
            using (idr)
            {
                result.Load(idr);
            }

            return result;
        }

        public DataTable GetPayByBillBalanceSheetHasNegativeAmount(Guid bid)
        {
            var sql = string.Format(@"select case when cd.MainBusinessHourGuid is null then bs.product_guid else cd.MainBusinessHourGuid end as product_guid, 
bs.interval_start, bs.interval_end,
sum(bs.est_amount) as est_amount 
from balance_sheet as bs with(nolock)
left join combo_deals as cd with(nolock) on cd.BusinessHourGuid = bs.product_guid
where bs.balance_sheet_type in ({0}, {1}, {2})
and (bs.product_guid in (
	select businesshourguid from combo_deals
	where MainBusinessHourGuid = (
		select MainBusinessHourGuid from combo_deals 
		where businesshourguid = @bid
	)
) or bs.product_guid = @bid)
group by case when cd.MainBusinessHourGuid is null then bs.product_guid else cd.MainBusinessHourGuid end, 
bs.interval_start, bs.interval_end 
having sum(est_amount) < 0  ",
                                (int)BalanceSheetType.FlexibleToHouseBalanceSheet,
                                (int)BalanceSheetType.MonthlyToHouseBalanceSheet,
                                (int)BalanceSheetType.WeeklyToHouseBalanceSheet);
            var idr = new InlineQuery().ExecuteReader(sql, bid.ToString());
            var result = new DataTable();
            using (idr)
            {
                result.Load(idr);
            }

            return result;
        }


        public bool BalanceSheetDelete(int bsId)
        {
            DB.Destroy<BalanceSheet>(BalanceSheet.IdColumn.ColumnName, bsId);
            return true;
        }

        #endregion

        #region BalanceSheetDetail 對帳單明細

        public void BalanceSheetDetailDelete(BalanceSheetDetail detail)
		{
		    string sql = string.Format(@"
DELETE FROM {0} 
WHERE {1} = @balance_sheet_id 
AND {2} = @trust_id"
		                               , BalanceSheetDetail.Schema.TableName
		                               , BalanceSheetDetail.Columns.BalanceSheetId
		                               , BalanceSheetDetail.Columns.TrustId);
		    QueryCommand qc = new QueryCommand(sql, BalanceSheetDetail.Schema.Provider.Name);
		    qc.AddParameter("balance_sheet_id", detail.BalanceSheetId, DbType.Int32);
            qc.AddParameter("trust_id", detail.TrustId, DbType.Guid);
		    DataService.ExecuteScalar(qc);
		}

        public bool BalanceSheetDetailDelete(int BsId)
        {
            DB.Destroy<BalanceSheetDetail>(BalanceSheetDetail.BalanceSheetIdColumn.ColumnName, BsId);
            return true;
        }

        public int BalanceSheetDetailSet(BalanceSheetDetail detail)
		{
			return DB.Save(detail);
		}

        public int BalanceSheetDetailUndoSet(BalanceSheetDetail detail)
        {
            string sql = string.Format(
@"UPDATE {0}
  SET {1} = @status, 
  {2} = @mod_log_id,
  {3} = @undo_id,
  {4} = @undo_time
WHERE {5} = @balance_sheet_id
  AND {6} = @trust_id"
            , BalanceSheetDetail.Schema.TableName
            , BalanceSheetDetail.Columns.Status
            , BalanceSheetDetail.Columns.ModLogId
            , BalanceSheetDetail.Columns.UndoId
            , BalanceSheetDetail.Columns.UndoTime
            , BalanceSheetDetail.Columns.BalanceSheetId
            , BalanceSheetDetail.Columns.TrustId
            );
            QueryCommand qc = new QueryCommand(sql, BalanceSheetDetail.Schema.Provider.Name);
            qc.AddParameter("status", (int)detail.Status, DbType.Int32);
            qc.AddParameter("mod_log_id", detail.ModLogId, DbType.Int32);
            qc.AddParameter("undo_id", detail.UndoId, DbType.Int32);
            qc.AddParameter("undo_time", detail.UndoTime, DbType.DateTime);
            qc.AddParameter("balance_sheet_id", detail.BalanceSheetId, DbType.Int32);
            qc.AddParameter("trust_id", detail.TrustId, DbType.Guid);

            return DB.Execute(qc);
        }

        public int BalanceSheetDetailModifyStatus(BalanceSheetDetail detail, BalanceSheetDetailStatus newStatus)
        {
            string sql = string.Format(
@"UPDATE {0}
  SET {1} = @newStatus  
WHERE {2} = @balance_sheet_id
  AND {3} = @trust_id
  AND {1} = @status"
            , BalanceSheetDetail.Schema.TableName
            , BalanceSheetDetail.Columns.Status
            , BalanceSheetDetail.Columns.BalanceSheetId
            , BalanceSheetDetail.Columns.TrustId
            );
            QueryCommand qc = new QueryCommand(sql, BalanceSheetDetail.Schema.Provider.Name);
            qc.AddParameter("newStatus", (int)newStatus, DbType.Int32);
            qc.AddParameter("balance_sheet_id", detail.BalanceSheetId, DbType.Int32);
            qc.AddParameter("trust_id", detail.TrustId, DbType.Guid);
            qc.AddParameter("status", (int)detail.Status, DbType.Int32);

            return DB.Execute(qc);
        }

        public int BalanceSheetDetailSetList(BalanceSheetDetailCollection details)
        {
            return DB.SaveAll(details);
        }

		public BalanceSheetDetail BalanceSheetDetailGetByTrustIdForUndo(Guid trustId)
		{
			BalanceSheetDetail result = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                .Where(BalanceSheetDetail.Columns.TrustId).IsEqualTo(trustId)
                .And(BalanceSheetDetail.Columns.Status).IsEqualTo((int)BalanceSheetDetailStatus.Normal).Or(BalanceSheetDetail.Columns.TrustId).IsEqualTo(trustId).And(BalanceSheetDetail.Columns.Status).IsEqualTo((int)BalanceSheetDetailStatus.NoPay)
				.ExecuteSingle<BalanceSheetDetail>();
			
			if (result.IsLoaded)
				return result;
			
			return null;
		}

        public BalanceSheetDetailCollection BalanceSheetDetailGetListByTrustId(Guid trustId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                       .Where(BalanceSheetDetail.Columns.TrustId).IsEqualTo(trustId)
                       .ExecuteAsCollection<BalanceSheetDetailCollection>() ?? new BalanceSheetDetailCollection();
        }
        public BalanceSheetDetailCollection BalanceSheetDetailGetListByTrustId(List<Guid> trustId)
        {
            var infos = new BalanceSheetDetailCollection();
            List<Guid> tempTrustId = trustId.Distinct().ToList();
            int idx = 0;
            while (idx <= tempTrustId.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                        .Where(BalanceSheetDetail.Columns.TrustId).In(tempTrustId.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<BalanceSheetDetailCollection>() ?? new BalanceSheetDetailCollection();

                infos.AddRange(batchProd);
                idx += batchLimit;
            }
            return infos;
        }

        public BalanceSheetDetailCollection BalanceSheetDetailGetListByBalanceSheetId(int balanceSheetId)
        {
            BalanceSheetDetailCollection bsDetails;
            bsDetails = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                .Where(BalanceSheetDetail.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                .ExecuteAsCollection<BalanceSheetDetailCollection>();
            return bsDetails ?? new BalanceSheetDetailCollection();
        }

        public BalanceSheetDetailCollection BalanceSheetDetailGetListByBalanceSheetIds(List<int> balanceSheetIds)
        {
            BalanceSheetDetailCollection bsDetails;
            bsDetails = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                .Where(BalanceSheetDetail.Columns.BalanceSheetId).In(balanceSheetIds)
                .ExecuteAsCollection<BalanceSheetDetailCollection>();
            return bsDetails ?? new BalanceSheetDetailCollection();
        }

        public BalanceSheetDetailCollection BalanceSheetDetailGetListByProductGuidAndStoreGuid(Guid productGuid, Guid? storeGuid)
        {
            BalanceSheetDetailCollection bsDetails;
            if (storeGuid == null)
            {
                bsDetails = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                    .Where(BalanceSheetDetail.Columns.BalanceSheetId)
                    .In(new Select(BalanceSheet.Columns.Id).From<BalanceSheet>()
                            .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                            .And(BalanceSheet.Columns.StoreGuid).IsNull())
                    .ExecuteAsCollection<BalanceSheetDetailCollection>();
            }
            else
            {
                bsDetails = DB.SelectAllColumnsFrom<BalanceSheetDetail>().NoLock()
                    .Where(BalanceSheetDetail.Columns.BalanceSheetId)
                    .In(new Select(BalanceSheet.Columns.Id).From<BalanceSheet>()
                            .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(productGuid)
                            .And(BalanceSheet.Columns.StoreGuid).IsEqualTo(storeGuid))
                    .ExecuteAsCollection<BalanceSheetDetailCollection>();
            }
            return bsDetails ?? new BalanceSheetDetailCollection();
        }

        public DataTable CashTrustLogGetListByBalanceSheetId(int balanceSheetId)
        {
            string sql = @"SELECT ctl.trust_id, ctl.coupon_sequence_number, ctl.coupon_id, ctl.status, ctl.special_status, 
                            ctsl.modify_time, ctl.item_name, st.seller_name as store_name, bsd.status as unverify_status, bsd.undo_time, ctsl.create_id, ctl.prefix
                            FROM balance_Sheet_Detail bsd with(nolock) INNER JOIN
                            cash_trust_log ctl with(nolock) ON ctl.trust_id = bsd.trust_id LEFT OUTER JOIN
                            cash_trust_status_log ctsl with(nolock) ON ctsl.id = bsd.cash_trust_status_log_id LEFT OUTER JOIN
                            seller st with(nolock) ON st.guid = ctl.verified_store_guid
                            WHERE bsd.balance_sheet_id = " + balanceSheetId;                        
            IDataReader idr = new InlineQuery().ExecuteReader(sql);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        public DataTable OrderGetListByBalanceSheetId(int balanceSheetId)
        {
            string sql = string.Format(@"SELECT ctl.trust_id, ctl.status, ctl.special_status,
ctl.credit_card, ctl.pcash, ctl.scash, ctl.atm, ctl.bcash,
bsd.status as detail_status, bsd.undo_time, 
od.order_id, od.guid as order_guid , od.create_time as order_create_time, os.ship_time
FROM balance_Sheet_Detail as bsd with(nolock)  
INNER JOIN cash_trust_log as ctl with(nolock) ON ctl.trust_id = bsd.trust_id 
INNER JOIN [order] as od with(nolock) on od.guid = ctl.order_guid and od.order_status & {0} > 0
OUTER APPLY (SELECT TOP 1 ship_time AS ship_time
               FROM order_ship as os with(nolock) 
			  WHERE os.order_guid = od.guid
                AND os.type = 1) AS os
WHERE bsd.balance_sheet_id = @bsId", (int)OrderStatus.Complete);
            IDataReader idr = new InlineQuery().ExecuteReader(sql, balanceSheetId);
            DataTable dt = new DataTable();
            using (idr)
            {
                dt.Load(idr);
            }
            return dt;
        }

        #endregion

        #region BalanceSheetIspDetail 對帳單超取明細
        public int BalanceSheetIspDetailSetList(BalanceSheetIspDetailCollection ispDetails)
        {
            return DB.SaveAll(ispDetails);
        }

        public BalanceSheetIspDetailCollection BalanceSheetIspDetailGetListByBalanceSheetId(int balanceSheetId,int productDeliveryType)
        {
            BalanceSheetIspDetailCollection bsDetails;
            bsDetails = DB.SelectAllColumnsFrom<BalanceSheetIspDetail>().NoLock()
                .Where(BalanceSheetIspDetail.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                .And(BalanceSheetIspDetail.Columns.ProductDeliveryType).IsEqualTo(productDeliveryType)
                .ExecuteAsCollection<BalanceSheetIspDetailCollection>();
            return bsDetails ?? new BalanceSheetIspDetailCollection();
        }

        public ViewBalanceSheetIspDetailListCollection ViewBalanceSheetIspDetailListGetByBalanceSheetId(int balanceSheetId)
        {
            SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetIspDetailList>()
                             .Where(ViewBalanceSheetIspDetailList.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId);
            return sql.ExecuteAsCollection<ViewBalanceSheetIspDetailListCollection>();
        }

        public ViewBalanceSheetIspDetailListCollection ViewBalanceSheetIspDetailListGetByChangeDate(int balanceSheetId, int productDeliveryType, DateTime? ispFreightsChangeSDate, DateTime? ispFreightsChangeEDate)
        {
            if (ispFreightsChangeSDate != null && ispFreightsChangeEDate != null)
            {
                SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetIspDetailList>()
                             .Where(ViewBalanceSheetIspDetailList.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                             .And(ViewBalanceSheetIspDetailList.Columns.ProductDeliveryType).IsEqualTo(productDeliveryType)
                             .And(ViewBalanceSheetIspDetailList.Columns.ShipToStockTime).IsGreaterThanOrEqualTo(ispFreightsChangeSDate)
                             .And(ViewBalanceSheetIspDetailList.Columns.ShipToStockTime).IsLessThan(ispFreightsChangeEDate);
                return sql.ExecuteAsCollection<ViewBalanceSheetIspDetailListCollection>();
            }
            else
            {
                //代表沒有運費沒有異動過
                SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetIspDetailList>()
                             .Where(ViewBalanceSheetIspDetailList.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                             .And(ViewBalanceSheetIspDetailList.Columns.ProductDeliveryType).IsEqualTo(productDeliveryType);
                return sql.ExecuteAsCollection<ViewBalanceSheetIspDetailListCollection>();
            }

        }

        public int ViewBalanceSheetIspDetailListGetCountByChangeDate(int balanceSheetId, int productDeliveryType, DateTime? ispFreightsChangeSDate, DateTime? ispFreightsChangeEDate)
        {
            if (ispFreightsChangeSDate != null && ispFreightsChangeEDate != null)
            {
                SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetIspDetailList>()
                             .Where(ViewBalanceSheetIspDetailList.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                             .And(ViewBalanceSheetIspDetailList.Columns.ProductDeliveryType).IsEqualTo(productDeliveryType)
                             .And(ViewBalanceSheetIspDetailList.Columns.ShipToStockTime).IsGreaterThanOrEqualTo(ispFreightsChangeSDate)
                             .And(ViewBalanceSheetIspDetailList.Columns.ShipToStockTime).IsLessThan(ispFreightsChangeEDate);
                return sql.GetRecordCount();
            }
            else
            {
                //代表沒有運費沒有異動過
                SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetIspDetailList>()
                             .Where(ViewBalanceSheetIspDetailList.Columns.BalanceSheetId).IsEqualTo((int)balanceSheetId)
                             .And(ViewBalanceSheetIspDetailList.Columns.ProductDeliveryType).IsEqualTo(productDeliveryType);
                return sql.GetRecordCount();
            }

        }

        public BalanceSheetIspDetailCollection BalanceSheetIspDetailGetListByBid(Guid MerchandiseGuid)
        {
            BalanceSheetIspDetailCollection bsDetails;
           
            bsDetails = DB.SelectAllColumnsFrom<BalanceSheetIspDetail>().NoLock()
                .Where(BalanceSheetIspDetail.Columns.BalanceSheetId)
                .In(new Select(BalanceSheet.Columns.Id).From<BalanceSheet>()
                        .Where(BalanceSheet.Columns.ProductGuid).IsEqualTo(MerchandiseGuid))
                .ExecuteAsCollection<BalanceSheetIspDetailCollection>();

            return bsDetails ?? new BalanceSheetIspDetailCollection();
        }

        public bool BalanceSheetIspDetailDelete(int bsId)
        {
            DB.Destroy<BalanceSheetIspDetail>(BalanceSheetIspDetail.BalanceSheetIdColumn.ColumnName, bsId);
            return true;
        }
        #endregion

        #region BalanceModificationLog 對帳單異動歷程

        public BalanceModificationLog BalanceModificationLogSet(BalanceModificationLog log)
        {
            DB.Save(log);
            return log;
        }

        #endregion

        #region BalanceSheetWms PCHOME倉儲對帳單
        

        public BalanceSheetWm BalanceSheetWmsGet(int balanceSheetId)
        {
            return DB.Get<BalanceSheetWm>(balanceSheetId);
        }

        public BalanceSheetWmCollection BalanceSheetWmsGetBySeller(Guid sid, string state)
        {
            BalanceSheetWmCollection bsDetails;
            if (state == "Balancing")
            {
                
                bsDetails = DB.SelectAllColumnsFrom<BalanceSheetWm>().NoLock()
                    .Where(BalanceSheetWm.Columns.SellerGuid).IsEqualTo(sid)
                    .And(BalanceSheetWm.Columns.ConfirmedUserName).IsNull()
                    .ExecuteAsCollection<BalanceSheetWmCollection>();
                return bsDetails ?? new BalanceSheetWmCollection();
            }
            else if (state == "Finished")
            {
                bsDetails = DB.SelectAllColumnsFrom<BalanceSheetWm>().NoLock()
                    .Where(BalanceSheetWm.Columns.SellerGuid).IsEqualTo(sid)
                    .And(BalanceSheetWm.Columns.ConfirmedUserName).IsNotNull()
                    .ExecuteAsCollection<BalanceSheetWmCollection>();
                return bsDetails ?? new BalanceSheetWmCollection();
            }
            return new BalanceSheetWmCollection();


        }

        public int BalanceSheetWmsSet(BalanceSheetWm bsw)
        {
            return DB.Save(bsw);
        }

        public BalanceSheetWmCollection BalanceSheetWmsGetListByIds(List<int> Ids)
        {
            var infos = new BalanceSheetWmCollection();
            List<int> tempIds = Ids.Distinct().ToList();
            int idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<BalanceSheetWm>()
                        .Where(BalanceSheetWm.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<BalanceSheetWmCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }
            return infos;
        }

        public BalanceSheetWmCollection BalanceSheetWmsGetListBySellerGuid(Guid sellerGuid)
        {
            BalanceSheetWmCollection bswCollection;
            bswCollection = DB.SelectAllColumnsFrom<BalanceSheetWm>()
                .Where(BalanceSheetWm.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .ExecuteAsCollection<BalanceSheetWmCollection>();
            return bswCollection ?? new BalanceSheetWmCollection();
        }

        public int BalanceSheetWmsDuplicationCountCheck(Guid sellerGuid, DateTime intervalStart, DateTime intervalEnd, BalanceSheetType bsType)
        {
            if (bsType == BalanceSheetType.ManualTriggered)
                return 0;


            return DB.Select().From<BalanceSheetWm>()
                .Where(BalanceSheetWm.Columns.SellerGuid).IsEqualTo(sellerGuid)
                .And(BalanceSheetWm.Columns.IntervalStart).IsEqualTo(intervalStart)
                .And(BalanceSheetWm.Columns.IntervalEnd).IsEqualTo(intervalEnd)
                .GetRecordCount();
        }

        public BalanceSheetWm BalanceSheetWmsGetLatest(Guid sellerGuid)
        {
            BalanceSheetWm result;

            SqlQuery query = DB.SelectAllColumnsFrom<BalanceSheetWm>().Top("1")
                .Where(BalanceSheetWm.Columns.SellerGuid).IsEqualTo(sellerGuid);

            

            result = query.OrderDesc(BalanceSheetWm.Columns.IntervalEnd)
                    .ExecuteSingle<BalanceSheetWm>();

            return result.IsNew ? null : result;
        }

        public bool BalanceSheetWmsDelete(int bswId)
        {
            DB.Destroy<BalanceSheetWm>(BalanceSheetWm.IdColumn.ColumnName, bswId);
            return true;
        }
        #endregion

        #region ViewBalanceDetail

        public ViewBalanceDetailCollection ViewBalanceDetailGetList(IEnumerable<int> balanceSheetIds)
        {
            string sql = string.Format(@"select * from {0} where {1} in ({2})"
                , ViewBalanceDetail.Schema.TableName
                , ViewBalanceDetail.Columns.BalanceSheetId
                , string.Join(",", balanceSheetIds)
                );
            QueryCommand qc = new QueryCommand(sql, ViewBalanceDetail.Schema.Provider.Name);

            ViewBalanceDetailCollection result = new ViewBalanceDetailCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion

        #region ViewBalanceSheetUndeducted

        public ViewBalanceSheetUndeducted ViewBalanceSheetUndeductedGetByTrustId(Guid trustId)
        {
            string sql = string.Format(@"select * from {0} where {1} = @trustId"
                , ViewBalanceSheetUndeducted.Schema.TableName
                , ViewBalanceSheetUndeducted.Columns.TrustId                
                );
            QueryCommand qc = new QueryCommand(sql, ViewBalanceDetail.Schema.Provider.Name);

            qc.AddParameter("trustId", trustId, DbType.Guid);            

            ViewBalanceSheetUndeductedCollection result = new ViewBalanceSheetUndeductedCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result.FirstOrDefault();
        }

        public ViewBalanceSheetUndeductedCollection ViewBalanceSheetUndeductedGetList(Guid merchandiseGuid, Guid? storeGuid)
        {
            string sql = string.Format(@"select * from {0} where {1} = @merchandiseGuid and {2} = @storeGuid"
                , ViewBalanceSheetUndeducted.Schema.TableName
                , ViewBalanceSheetUndeducted.Columns.MerchandiseGuid
                , ViewBalanceSheetUndeducted.Columns.StoreGuid
                );
            QueryCommand qc = new QueryCommand(sql, ViewBalanceDetail.Schema.Provider.Name);

            qc.AddParameter("merchandiseGuid", merchandiseGuid, DbType.Guid);
            qc.AddParameter("storeGuid", storeGuid, DbType.Guid);

            ViewBalanceSheetUndeductedCollection result = new ViewBalanceSheetUndeductedCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion

        #region ViewBalanceSheetNoncorrespondedVerify

        public ViewBalanceSheetNoncorrespondedVerifyCollection ViewBalanceSheetNoncorrespondedVerifyGetList(Guid merchandiseGuid, Guid? storeGuid)
        {
            string sql = string.Format(@"select * from {0} where {1} = @merchandiseGuid and {2} = @storeGuid"
                , ViewBalanceSheetNoncorrespondedVerify.Schema.TableName
                , ViewBalanceSheetNoncorrespondedVerify.Columns.MerchandiseGuid
                , ViewBalanceSheetNoncorrespondedVerify.Columns.StoreGuid
                );
            QueryCommand qc = new QueryCommand(sql, ViewBalanceDetail.Schema.Provider.Name);

            qc.AddParameter("merchandiseGuid", merchandiseGuid, DbType.Guid);
            qc.AddParameter("storeGuid", storeGuid, DbType.Guid);

            ViewBalanceSheetNoncorrespondedVerifyCollection result = new ViewBalanceSheetNoncorrespondedVerifyCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        #endregion

        #region ViewBalanceSheetList

        public ViewBalanceSheetListCollection ViewBalanceSheetListGetList(BusinessModel businessModel, Guid productGuid, Guid? storeGuid)
        {
            SqlQuery sql = DB.SelectAllColumnsFrom<ViewBalanceSheetList>()
                             .Where(ViewBalanceSheetList.Columns.ProductType).IsEqualTo((int)businessModel)
                             .And(ViewBalanceSheetList.Columns.ProductGuid).IsEqualTo(productGuid);
            if (storeGuid.HasValue)
            {
                sql.And(ViewBalanceSheetList.Columns.StoreGuid).IsEqualTo(storeGuid.Value);
            }
            return sql.ExecuteAsCollection<ViewBalanceSheetListCollection>();
        }

        public ViewBalanceSheetListCollection ViewBalanceSheetListGetList(IEnumerable<Guid> productGuids)
        {
            ViewBalanceSheetListCollection infos = new ViewBalanceSheetListCollection();
            List<Guid> tempIds = productGuids.Distinct().ToList();
            int idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewBalanceSheetList>()
                        .Where(ViewBalanceSheetList.Columns.ProductGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewBalanceSheetListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }
            return infos;
        }

        public ViewBalanceSheetListCollection ViewBalanceSheetListGetListByIsNotConfirmed(IEnumerable<Guid> productGuids,string usernName)
        {

            ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
            ProposalPerformanceLog log = new ProposalPerformanceLog();
            log.Link = "BulletinBoardList";
            log.ContentLog = productGuids.Any() ? string.Join("','", productGuids) : string.Empty;
            log.CreateTime = DateTime.Now;
            log.CreateUser = usernName;
            sp.ProposalPerformanceLogSet(log);

            ViewBalanceSheetListCollection infos = new ViewBalanceSheetListCollection();
            List<Guid> tempIds = productGuids.Distinct().ToList();
            int idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewBalanceSheetList>()
                        .Where(ViewBalanceSheetList.Columns.ProductGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .And(ViewBalanceSheetList.Columns.IsConfirmedReadyToPay).IsEqualTo(false)
                        .ExecuteAsCollection<ViewBalanceSheetListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            log = new ProposalPerformanceLog();
            log.Link = "BulletinBoardList";
            log.ContentLog = "finish";
            log.CreateTime = DateTime.Now;
            log.CreateUser = usernName;
            sp.ProposalPerformanceLogSet(log);
            return infos;
        }

        public ViewBalanceSheetListCollection PartialPaymentToBalanceSheetList(IEnumerable<Guid> productGuids)
        {
            var infos = new ViewBalanceSheetListCollection();
            var tempIds = productGuids.Distinct().ToList();
            var idx = 0;
            //暫付七成付款日有值轉對帳單格式
            //傳進來的BID都是主檔
            //其他付款方式要過濾
            //單檔有對帳單才要撈出來，有可能先有對帳單開立日
            //多檔次主檔檢查開立日，主檔只有系統可以修改
            var bids = productGuids.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;

                var sql = string.Format(@"
select 0 as id, bh.GUID as product_guid, i.item_name as name, cec.app_title, dp.unique_id 
, bh.business_hour_deliver_time_s as use_start_time, bh.business_hour_deliver_time_e as use_end_time 
, {0} as product_type, dp.delivery_type, null as store_guid, '' as store_name, bh.seller_guid 
, se.seller_name, da.vendor_receipt_type, da.remittance_type,
coalesce(coalesce(cdbs.interval_end, bs.interval_end), getdate()) as interval_end,
case when da.remittance_type = {7} then {11}
else case when da.remittance_type = {8} then {12}
else case when da.remittance_type = {9} then {10}
else case when da.remittance_type = {13} then {14}
     else {1} 
     end end end
end as generation_frequency,

case when coalesce(cdbs.unConfirmedCount, bs.unConfirmedCount) > 0 then 0 
else case when coalesce(cdbs.bsCount, bs.bsCount) > 0 and coalesce(cdbs.unConfirmedCount, bs.unConfirmedCount) = 0 then 1 
else 0 end
end as is_confirmed_ready_to_pay, 
case when coalesce(cdbs.unReceiptReceivedCount, bs.unReceiptReceivedCount) > 0 then 0 
else case when coalesce(cdbs.bsCount, bs.bsCount) > 0 and coalesce(cdbs.unReceiptReceivedCount, bs.unReceiptReceivedCount) = 0 then 1 
	 else 0
	 end 
end as is_receipt_received,
case when da.remittance_type = {4} and (da.balance_sheet_create_date > GETDATE() or balance_sheet_create_date is null) then 0
else coalesce(cdbs.est_amount, bs.est_amount)
end as est_amount,
null as transfer_amount,
coalesce(cdbs.isp_family_amount, bs.isp_family_amount) as isp_family_amount,
coalesce(cdbs.isp_seven_amount, bs.isp_seven_amount) as isp_seven_amount,
coalesce(cdbs.overdue_amount, bs.overdue_amount) as overdue_amount,
coalesce(cdbs.wms_order_amount, bs.wms_order_amount) as wms_order_amount
from business_hour bh with(nolock)
join item i with(nolock) on i.business_hour_guid = bh.GUID
join coupon_event_content cec with(nolock) on cec.business_hour_guid = bh.GUID
join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID
join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
join seller se with(nolock) on se.guid = bh.seller_guid
left join (
	select product_guid,
	count(id) as bsCount, 
	count(case when is_confirmed_ready_to_pay = 0 then 1 end) as unConfirmedCount, 
	count(case when is_receipt_received = 0 then 1 end) as unReceiptReceivedCount,
	sum(case when is_receipt_received = 0 then est_amount else 0 end) as est_amount,
    sum(case when is_receipt_received = 0 then isp_family_amount else 0 end) as isp_family_amount,
	sum(case when is_receipt_received = 0 then isp_seven_amount else 0 end) as isp_seven_amount,
	sum(case when is_receipt_received = 0 then overdue_amount else 0 end) as overdue_amount,
    sum(case when is_receipt_received = 0 then wms_order_amount else 0 end) as wms_order_amount,
	max(interval_end) as interval_end  
	from balance_sheet WITH(NOLOCK)
	group by product_guid
) as bs on bs.product_guid = bh.GUID
left join (
	select MainBusinessHourGuid, 
	count(bs.id) as bsCount, 
	count(case when bs.is_confirmed_ready_to_pay = 0 then 1 end) as unConfirmedCount, 
	count(case when bs.is_receipt_received = 0 then 1 end) as unReceiptReceivedCount,
	sum(case when bs.is_receipt_received = 0 then bs.est_amount end) as est_amount,
    sum(case when is_receipt_received = 0 then isp_family_amount else 0 end) as isp_family_amount,
	sum(case when is_receipt_received = 0 then isp_seven_amount else 0 end) as isp_seven_amount,
	sum(case when is_receipt_received = 0 then overdue_amount else 0 end) as overdue_amount,
    sum(case when is_receipt_received = 0 then wms_order_amount else 0 end) as wms_order_amount,
	max(bs.interval_end) as interval_end 
	from combo_deals cd with(nolock)
	left join balance_sheet bs with(nolock) on bs.product_guid = cd.BusinessHourGuid
	group by cd.MainBusinessHourGuid
) as cdbs on cdbs.MainBusinessHourGuid = bh.GUID
where bh.guid in ({2})
and da.vendor_billing_model = {6} 
and dp.delivery_type = {3} 
and (((da.balance_sheet_create_date <= getDate() or partially_payment_date <= getDate()) and da.remittance_type = {4}) 
or (da.remittance_type = {5} and da.balance_sheet_create_date <= getDate())
or (da.remittance_type in ({7},{8},{13}) and coalesce(cdbs.bsCount, bs.bsCount) > 0)
or (da.remittance_type = {9} and bh.business_hour_deliver_time_s < getdate()))"
               , (int)BusinessModel.Ppon
               , (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet
               , string.Join(",", bids.Skip(idx).Take(batchLimit))
               , (int)DeliveryType.ToHouse
               , (int)RemittanceType.ManualPartially
               , (int)RemittanceType.Others
               , (int)VendorBillingModel.BalanceSheetSystem
               , (int)RemittanceType.Monthly
               , (int)RemittanceType.Weekly
               , (int)RemittanceType.Flexible
               , (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet
               , (int)BalanceSheetGenerationFrequency.MonthBalanceSheet
               , (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
               , (int)RemittanceType.Fortnightly
               , (int)BalanceSheetGenerationFrequency.FortnightlyBalanceSheet);

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                var data = new ViewBalanceSheetListCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }
            return infos;
        }

        public ViewBalanceSheetListCollection ViewBalanceSheetListGetListByBalanceSheetIds(List<int> balanceSheetIds)
        {
            ViewBalanceSheetListCollection infos = new ViewBalanceSheetListCollection();
            List<int> tempIds = balanceSheetIds.Distinct().ToList();
            int idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewBalanceSheetList>()
                        .Where(ViewBalanceSheetList.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewBalanceSheetListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }
            return infos;
            //return DB.SelectAllColumnsFrom<ViewBalanceSheetList>()
            //    .Where(ViewBalanceSheetList.Columns.Id).In(balanceSheetIds)
            //    .ExecuteAsCollection<ViewBalanceSheetListCollection>() ?? new ViewBalanceSheetListCollection();
        }

        public ViewBalanceSheetListCollection GetFlexiblePayToShopDealWithOutBalanceSheet(IEnumerable<Guid> productGuids)
        {
            var infos = new ViewBalanceSheetListCollection();
            var tempIds = productGuids.Distinct().ToList();
            var idx = 0;
            var bids = productGuids.Select(x => "'" + x.ToString() + "'").ToList();

            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;

                var sql = string.Format(@"
select 0 as id, bh.GUID as product_guid, i.item_name as name, dp.unique_id 
, bh.business_hour_deliver_time_s as use_start_time, bh.business_hour_deliver_time_e as use_end_time 
, {0} as product_type, dp.delivery_type, ps.store_guid, st.seller_name as store_name, bh.seller_guid 
, se.seller_name, da.vendor_receipt_type, da.remittance_type, 
getdate() as interval_end,
{1} as generation_frequency,
0 as is_confirmed_ready_to_pay, 
0 as is_receipt_received,
0 as est_amount,
null as transfer_amount
from business_hour bh with(nolock)
join item i with(nolock) on i.business_hour_guid = bh.GUID
join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID
join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
join seller se with(nolock) on se.guid = bh.seller_guid
inner join ppon_store as ps with(nolock) on ps.business_hour_guid = bh.guid and 
		((bh.business_hour_status & {5} > 0 and ps.vbs_right & {6} > 0) or 
	     (bh.business_hour_status & {5} = 0 and ps.vbs_right & {7} > 0))
inner join seller as st with(nolock) on st.Guid = ps.store_guid
left join balance_sheet as bs with(nolock) on bs.product_guid = bh.guid and bs.store_guid = ps.store_guid
where bh.guid in ({2})
and dp.delivery_type = {3}
and da.remittance_type = {4}
and bs.Id is null
and bh.business_hour_deliver_time_s < getdate()"
               , (int)BusinessModel.Ppon
               , (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet
               , string.Join(",", bids.Skip(idx).Take(batchLimit))
               , (int)DeliveryType.ToShop
               , (int)RemittanceType.Flexible
               , (int)BusinessHourStatus.NoRestrictedStore
               , (int)VbsRightFlag.Verify
               , (int)VbsRightFlag.VerifyShop);

                var qc = new QueryCommand(sql, ComboDeal.Schema.Provider.Name);
                var data = new ViewBalanceSheetListCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));

                infos.AddRange(data);

                idx += batchLimit;
            }
            return infos;
        }

        #endregion

        #region ViewBalanceSheetDetailList

        public ViewBalanceSheetDetailListCollection ViewBalanceSheetDetailListGetList(List<int> balanceSheetIds)
        {
            var infos = new ViewBalanceSheetDetailListCollection();
            var tempIds = balanceSheetIds.Distinct().ToList();
            var idx = 0;
            //搜尋對帳單
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<ViewBalanceSheetDetailList>()
                        .Where(ViewBalanceSheetDetailList.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<ViewBalanceSheetDetailListCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }
            return infos;
        }


        #endregion ViewBalanceSheetDetailList

        #region ViewBalanceSheetBillList

        public ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds)
        {
            ViewBalanceSheetBillListCollection bsBillDetails = DB.SelectAllColumnsFrom<ViewBalanceSheetBillList>()
                    .Where(ViewBalanceSheetBillList.Columns.Id).In(balanceSheetIds)
                    .ExecuteAsCollection<ViewBalanceSheetBillListCollection>();
            return bsBillDetails ?? new ViewBalanceSheetBillListCollection();
        }

        public ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetList(params string[] filter)
        {
            ViewBalanceSheetBillListCollection vbslps = new ViewBalanceSheetBillListCollection();
            QueryCommand qc = GetDCWhere(ViewBalanceSheetBillList.Schema, filter);
            qc.CommandSql = "select * from " + ViewBalanceSheetBillList.Schema.TableName + " with(nolock)" + qc.CommandSql + " order by " + ViewBalanceSheetBillList.Columns.CreateTime;
            vbslps.LoadAndCloseReader(DataService.GetReader(qc));

            return vbslps;
        }
        public ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetBalancingList(params string[] filter)
        {
            //使用subsonic build where只允許使用一個column使用in語法(即column in (str1,st2,..))
            //若有多個column使用in語法會被過濾掉(只保留最後一個) 造成錯誤
            //QueryCommand qc = GetDCWhere(ViewBalanceSheetBillListPpon.Schema, filter);
            string sql = "select * from " + ViewBalanceSheetBillList.Schema.TableName + " with(nolock) " +
                         " where " + ViewBalanceSheetBillList.Columns.IsConfirmedReadyToPay + " = 0 " +
                         " and " + ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.WeeklyPayWeekBalanceSheet + "  " +
                         " and " + ViewBalanceSheetBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet + "  ";
            if (filter.Length > 0)
            {
                foreach (var item in filter)
                {
                    sql += " and " + item;
                }
            }
            //qc.CommandSql.Replace("WHERE", "and");
            QueryCommand qc = new QueryCommand(sql, ViewBalanceSheetBillList.Schema.Provider.Name);

            ViewBalanceSheetBillListCollection data = new ViewBalanceSheetBillListCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data ?? new ViewBalanceSheetBillListCollection();
        }

        public ViewBalanceSheetBillListCollection ViewBalanceSheetBillListGetListPaging(
            int pageIndex, int pageSize, params string[] filter)
        {
            string defOrderBy = ViewBalanceSheetBillList.Columns.UniqueId + ", " + ViewBalanceSheetBillList.Columns.Year + ", " + ViewBalanceSheetBillList.Columns.Month;
            ViewBalanceSheetBillListCollection vbslps = new ViewBalanceSheetBillListCollection();
            QueryCommand qc = GetDCWhere(ViewBalanceSheetBillList.Schema, filter);
            qc.CommandSql = "select * from " + ViewBalanceSheetBillList.Schema.TableName + " with(nolock)" + qc.CommandSql;

            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + defOrderBy;

            if (pageSize > 0)
            {
                qc = SSHelper.MakePagable(qc, pageIndex, pageSize, defOrderBy);
            }
            vbslps.LoadAndCloseReader(DataService.GetReader(qc));

            return vbslps;
        }

        public DataTable GetShippingFeeOfLastMonth()
        {
            DateTime now = DateTime.Now;
            string sql = string.Format(@"
            SELECT tmp.*,
                   Isnull(vpo.amount, 0) AS overdue_amount,
	               0 as wms_amount
            INTO   #tmp
            FROM   (SELECT bs.id AS balance_sheet_id, bh.seller_GUID, b.invoice_com_id,
                           Max(bs.isp_family_amount + bs.isp_seven_amount + bs.wms_order_amount) fee
                    FROM   balance_sheet bs WITH(nolock) 
		            INNER JOIN balance_sheet_bill_relationship bsbr WITH(nolock) ON bs.id = bsbr.balance_sheet_id
                    INNER JOIN bill b WITH(NOLOCK) ON b.id = bsbr.bill_id
                    INNER JOIN weekly_pay_report wpr WITH(nolock) ON wpr.id = bs.pay_report_id
                    INNER JOIN business_hour bh WITH(nolock) ON bh.guid = bs.product_guid
                    WHERE  wpr.response_time >= '{0}' AND wpr.response_time < '{1}' AND b.vendor_einvoice_id IS NULL
                    GROUP  BY bs.id, b.vendor_einvoice_id, seller_GUID, invoice_com_id) tmp
                   OUTER APPLY (SELECT Sum(amount) amount
                                FROM   vendor_payment_overdue WITH(nolock)
                                WHERE  amount < 0
                                       AND balance_sheet_id = tmp.balance_sheet_id
                                GROUP  BY balance_sheet_id) vpo
                    where tmp.fee + abs(isnull(vpo.amount,0)) > 0
            UNION ALL
            SELECT tmp2.*,
	               0 as overdue_amount,
                   Isnull(wms.est_amount, 0) AS wms_amount
            FROM 
		            (SELECT bsw.id AS balance_sheet_id, bsw.seller_guid, b.invoice_com_id,
                           0 fee
                    FROM   balance_sheet_wms bsw WITH(nolock) 
		            INNER JOIN balance_sheet_wms_bill_relationship bswbr WITH(nolock) ON bsw.id = bswbr.balance_sheet_id
                    INNER JOIN bill b WITH(NOLOCK) ON b.id = bswbr.bill_id
                    INNER JOIN weekly_pay_report_wms wprw WITH(nolock) ON wprw.id = bsw.pay_report_id
                    WHERE  wprw.response_time >= '{0}' AND wprw.response_time < '{1}'  AND b.vendor_einvoice_id IS NULL
                    GROUP  BY bsw.id, b.vendor_einvoice_id, bsw.seller_GUID, invoice_com_id) tmp2
                    OUTER APPLY (SELECT Sum(est_amount) est_amount
                                FROM   balance_sheet_wms WITH(nolock)
                                WHERE  est_amount<0
                                       AND id = tmp2.balance_sheet_id
                                GROUP  BY id) wms
                    where tmp2.fee + abs(isnull(wms.est_amount,0)) > 0
		


            SELECT vbsbl.*,
                   s.seller_guid,
                   s.CompanyName,
                   ISNULL(s.accountant_name,'')  as accountant_name,
                   cp.city_name + c.city_name + s.seller_address AS seller_address
            FROM   (SELECT invoice_com_id,
                           Sum(fee)            fee,
                           Sum(overdue_amount) overdue_amount,
			               Sum(wms_amount) wms_amount,
                           (
						            ISNULL((
						            SELECT Ltrim(Str(bsbr.bill_id)) + '|'
						            FROM   #tmp t2
							               INNER JOIN balance_sheet_bill_relationship bsbr WITH(nolock) ON t2.balance_sheet_id = bsbr.balance_sheet_id
							               INNER JOIN bill b WITH(NOLOCK) ON b.id = bsbr.bill_id
						            WHERE  t1.invoice_com_id = t2.invoice_com_id
						            GROUP  BY bsbr.bill_id
						            FOR XML PATH('') 
			                        ),'') 
			              + 


						            ISNULL((
							            SELECT Isnull(Ltrim(Str(bswbr.bill_id)) + '|', '')
							            FROM   #tmp t2
									            INNER JOIN balance_sheet_wms_bill_relationship bswbr WITH(nolock)
											            ON t2.balance_sheet_id = bswbr.balance_sheet_id
									            INNER JOIN bill b2 WITH(NOLOCK)
											            ON b2.id = bswbr.bill_id
							            WHERE  t1.invoice_com_id = t2.invoice_com_id
							            GROUP  BY bswbr.bill_id
							            FOR XML PATH('') 
							            ),'')
			               )  AS bill_ids
                    FROM   #tmp t1
                    GROUP  BY invoice_com_id) vbsbl
                   CROSS APPLY (SELECT TOP 1 seller.guid AS seller_guid,
                                             city_id,
                                             CompanyName,
                                             accountant_name,
                                             seller_address
                                FROM   seller WITH(nolock)
                                       LEFT JOIN business_hour WITH(nolock)
                                              ON business_hour.seller_GUID = seller.guid
                                WHERE  SignCompanyID = vbsbl.invoice_com_id
                                ORDER  BY business_hour.create_time DESC) s
                   INNER JOIN city c WITH(nolock)
                           ON c.id = s.city_id
                   INNER JOIN city cp WITH(nolock)
                           ON c.parent_id = cp.id;
            drop table #tmp", now.AddMonths(-1).GetFirstDayOfMonth().ToString("yyyy/MM/dd"), now.GetFirstDayOfMonth().ToString("yyyy/MM/dd"));

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql))
            {
                dt.Load(idr);
            }

            return dt;
        }
        #endregion

        #region ViewBalanceSheetWmsBillList
        public ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetListPaging(
            int pageIndex, int pageSize, params string[] filter)
        {
            string defOrderBy = ViewBalanceSheetWmsBillList.Columns.UniqueId + ", " + ViewBalanceSheetWmsBillList.Columns.Year + ", " + ViewBalanceSheetWmsBillList.Columns.Month;
            ViewBalanceSheetWmsBillListCollection vbslps = new ViewBalanceSheetWmsBillListCollection();
            QueryCommand qc = GetDCWhere(ViewBalanceSheetWmsBillList.Schema, filter);
            qc.CommandSql = "select * from " + ViewBalanceSheetWmsBillList.Schema.TableName + " with(nolock)" + qc.CommandSql;

            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + defOrderBy;

            if (pageSize > 0)
            {
                qc = SSHelper.MakePagable(qc, pageIndex, pageSize, defOrderBy);
            }
            vbslps.LoadAndCloseReader(DataService.GetReader(qc));

            return vbslps;
        }

        public ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetList(params string[] filter)
        {
            ViewBalanceSheetWmsBillListCollection vbslps = new ViewBalanceSheetWmsBillListCollection();
            QueryCommand qc = GetDCWhere(ViewBalanceSheetWmsBillList.Schema, filter);
            qc.CommandSql = "select * from " + ViewBalanceSheetWmsBillList.Schema.TableName + " with(nolock)" + qc.CommandSql + " order by " + ViewBalanceSheetWmsBillList.Columns.CreateTime;
            vbslps.LoadAndCloseReader(DataService.GetReader(qc));

            return vbslps;
        }

        public ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetBalancingList(params string[] filter)
        {
            //使用subsonic build where只允許使用一個column使用in語法(即column in (str1,st2,..))
            //若有多個column使用in語法會被過濾掉(只保留最後一個) 造成錯誤
            //QueryCommand qc = GetDCWhere(ViewBalanceSheetBillListPpon.Schema, filter);
            string sql = "select * from " + ViewBalanceSheetWmsBillList.Schema.TableName + " with(nolock) " +
                         " where " + ViewBalanceSheetWmsBillList.Columns.IsConfirmedReadyToPay + " = 0 " +
                         " and " + ViewBalanceSheetWmsBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.WeeklyPayWeekBalanceSheet + "  " +
                         " and " + ViewBalanceSheetWmsBillList.Columns.BalanceSheetType + " <> " + (int)BalanceSheetType.ManualWeeklyPayWeekBalanceSheet + "  ";
            if (filter.Length > 0)
            {
                foreach (var item in filter)
                {
                    sql += " and " + item;
                }
            }
            //qc.CommandSql.Replace("WHERE", "and");
            QueryCommand qc = new QueryCommand(sql, ViewBalanceSheetWmsBillList.Schema.Provider.Name);

            ViewBalanceSheetWmsBillListCollection data = new ViewBalanceSheetWmsBillListCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data ?? new ViewBalanceSheetWmsBillListCollection();
        }

        public ViewBalanceSheetWmsBillListCollection ViewBalanceSheetWmsBillListGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds)
        {
            ViewBalanceSheetWmsBillListCollection bsBillDetails = DB.SelectAllColumnsFrom<ViewBalanceSheetWmsBillList>()
                    .Where(ViewBalanceSheetWmsBillList.Columns.Id).In(balanceSheetIds)
                    .ExecuteAsCollection<ViewBalanceSheetWmsBillListCollection>();
            return bsBillDetails ?? new ViewBalanceSheetWmsBillListCollection();
        } 
        #endregion

        #region ViewBalancingDeal

        public DataTable PartialPaymentToBalanceingDeal(string condition)
        {
            string sql = string.Format(@"
select bh.GUID as merchandise_guid, i.item_name as deal_name, dp.unique_id as deal_id
, bh.business_hour_order_time_s as deal_start_time, bh.business_hour_order_time_e as deal_end_time
, bh.business_hour_deliver_time_e as deal_use_end_time, bh.business_hour_deliver_time_s as deal_use_start_time
, da.final_balance_sheet_date, da.balance_sheet_create_date 
,case when bs.balance_sheet_type ={13} then {7} 
	  when bs.balance_sheet_type ={14} then {7}
	  when bs.balance_sheet_type ={15} then {6}
	  when bs.balance_sheet_type ={16} then {6}
	  when bs.balance_sheet_type ={17} then {5}
	  when bs.balance_sheet_type ={18} then {5}
	  when bs.balance_sheet_type ={19} then {12}
else da.remittance_type end as remittance_type
, dp.delivery_type,  
coalesce(coalesce(cdbs.interval_end, bs.interval_end), getdate()) as interval_end, 


case when bs.balance_sheet_type ={17} or bs.balance_sheet_type ={18} then {9}
when bs.balance_sheet_type ={15} or bs.balance_sheet_type ={16} then {10}
when bs.balance_sheet_type ={13} or bs.balance_sheet_type ={14} then {8}
when bs.balance_sheet_type ={19} then {20}
when da.remittance_type = {5} then {9}
else case when da.remittance_type = {6} then {10}
else case when da.remittance_type = {7} then {8}
else case when da.remittance_type = {12} then {20}
     else 4 
     end end end
end as generation_frequency,


case when coalesce(cdbs.unConfirmedCount, bs.unConfirmedCount) > 0 then 0 
else case when coalesce(cdbs.bsCount, bs.bsCount) > 0 and coalesce(cdbs.unConfirmedCount, bs.unConfirmedCount) = 0 then 1 
else 0 end
end as is_confirmed_ready_to_pay, 
case when coalesce(cdbs.unReceiptReceivedCount, bs.unReceiptReceivedCount) > 0 then 0 
else case when coalesce(cdbs.bsCount, bs.bsCount) > 0 and coalesce(cdbs.unReceiptReceivedCount, bs.unReceiptReceivedCount) = 0 then 1 
	 else 0
	 end 
end as is_receipt_received,
case when da.remittance_type = {2} and (da.balance_sheet_create_date > GETDATE() or balance_sheet_create_date is null) then 0
else coalesce(cdbs.est_amount, bs.est_amount)
end as est_amount
from business_hour bh with(nolock)
join item i with(nolock) on i.business_hour_guid = bh.GUID
join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID
join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
join (
    select ISNULL(cd.MainBusinessHourGuid,bh.guid) bid
    from business_hour bh with(nolock) 
    left join combo_deals cd with(nolock) on bh.GUID = cd.BusinessHourGuid 
    join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID 
    join seller s with(nolock) on bh.seller_GUID = s.GUID
    join item i with(nolock) on i.business_hour_guid = bh.GUID
    where 1 = 1 {0}
    group by ISNULL(cd.MainBusinessHourGuid, bh.guid)
) x on x.bid = bh.GUID
left join (
	select product_guid,balance_sheet_type,generation_frequency,
	count(id) as bsCount, 
	count(case when is_confirmed_ready_to_pay = 0 then 1 end) as unConfirmedCount, 
	count(case when is_receipt_received = 0 then 1 end) as unReceiptReceivedCount,
	sum(case when is_receipt_received = 0 then est_amount else 0 end) as est_amount,
	max(interval_end) as interval_end  
	from balance_sheet WITH(NOLOCK)
	group by product_guid,balance_sheet_type,generation_frequency
) as bs on bs.product_guid = bh.GUID
left join (
	select MainBusinessHourGuid, 
	count(bs.id) as bsCount, 
	count(case when bs.is_confirmed_ready_to_pay = 0 then 1 end) as unConfirmedCount, 
	count(case when bs.is_receipt_received = 0 then 1 end) as unReceiptReceivedCount,
	sum(case when bs.is_receipt_received = 0 then bs.est_amount end) as est_amount,
	max(bs.interval_end) as interval_end 
	from combo_deals cd with(nolock)
	left join balance_sheet bs with(nolock) on bs.product_guid = cd.BusinessHourGuid
	group by cd.MainBusinessHourGuid
) as cdbs on cdbs.MainBusinessHourGuid = bh.GUID
where dp.delivery_type = {1} 
and da.vendor_billing_model = {4} 
and(((da.balance_sheet_create_date <= getDate() or partially_payment_date <= getDate()) and da.remittance_type = {2}) 
or (da.remittance_type = {3} and da.balance_sheet_create_date <= getDate())
or (bs.balance_sheet_type in ({15},{16},{17},{18},{19}) and coalesce(cdbs.bsCount, bs.bsCount) > 0)
or (bs.balance_sheet_type in ({13},{14}) and bh.business_hour_deliver_time_s < getdate())
or (bs.balance_sheet_type is null and da.remittance_type in ({6},{5},{12}) and coalesce(cdbs.bsCount, bs.bsCount) > 0)
or (bs.balance_sheet_type is null and da.remittance_type = {7} and bh.business_hour_deliver_time_s < getdate()))"
                , condition
                , (int)DeliveryType.ToHouse
                , (int)RemittanceType.ManualPartially
                , (int)RemittanceType.Others
                , (int)VendorBillingModel.BalanceSheetSystem
                , (int)RemittanceType.Monthly
                , (int)RemittanceType.Weekly
                , (int)RemittanceType.Flexible
                , (int)BalanceSheetGenerationFrequency.FlexibleBalanceSheet
                , (int)BalanceSheetGenerationFrequency.MonthBalanceSheet
                , (int)BalanceSheetGenerationFrequency.WeekBalanceSheet
                , (int)BalanceSheetGenerationFrequency.LumpSumBalanceSheet
                , (int)RemittanceType.Fortnightly
                , (int)BalanceSheetType.FlexibleToHouseBalanceSheet
                , (int)BalanceSheetType.FlexiblePayBalanceSheet
                , (int)BalanceSheetType.WeeklyToHouseBalanceSheet
                , (int)BalanceSheetType.WeeklyPayBalanceSheet
                , (int)BalanceSheetType.MonthlyToHouseBalanceSheet
                , (int)BalanceSheetType.MonthlyPayBalanceSheet
                , (int)BalanceSheetType.FortnightlyToHouseBalanceSheet
                , (int)BalanceSheetGenerationFrequency.FortnightlyBalanceSheet);

            DataTable dt = new DataTable();
            using (IDataReader idr = new InlineQuery().ExecuteReader(sql))
            {
                dt.Load(idr);
            }

            return dt;
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListLikeSellerName(string sellerName, bool generatedOnly, bool filterBizModel, BusinessModel bizModel)
        {
            if (string.IsNullOrEmpty(sellerName))
            {
                return new ViewBalancingDealCollection();
            }
            
            SqlQuery partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.SellerName).Like(string.Format("%{0}%", sellerName))
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);

            if (generatedOnly)
            {
                partialQuery = partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery = partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }
            
            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection ViewBalancingDealGetByDealId(int dealId, bool generatedOnly, bool getComboDeals)
        {
            string sql = string.Format(@"
                    SELECT * 
                    FROM {0} WITH(NOLOCK)
                    WHERE {1}
                    {2} {3}",
                    ViewBalancingDeal.Schema.TableName,
                    getComboDeals 
                    ? string.Format(@"({0} IN (
                        SELECT {1} 
                        FROM {2} WITH(NOLOCK)
                        WHERE {3}  IN (
	                        SELECT {3} 
                            FROM {4} WITH(NOLOCK)
	                        WHERE {5} = @dealId
                        )
                    ) or {6} = @dealId)",
                    ViewBalancingDeal.Columns.MerchandiseGuid,
                    ComboDeal.Columns.BusinessHourGuid,
                    ComboDeal.Schema.TableName,
                    ComboDeal.Columns.MainBusinessHourGuid,
                    ViewComboDeal.Schema.TableName,
                    ViewComboDeal.Columns.UniqueId,
                    ViewBalancingDeal.Columns.DealId)
                    : string.Format("{0} =  @dealId", ViewBalancingDeal.Columns.DealId),
                    generatedOnly ? string.Format("AND ({0} is not null)", ViewBalancingDeal.Columns.GenerationFrequency) : string.Empty,
                    string.Format(" and {0} = {1}",
                    ViewBalancingDeal.Columns.DeliveryType, (int)DeliveryType.ToShop)
                    );

            QueryCommand qc = new QueryCommand(sql, ViewBalancingDeal.Schema.Provider.Name);
            qc.AddParameter("dealId", dealId, DbType.Int32);

            ViewBalancingDealCollection deals = new ViewBalancingDealCollection();
            deals.LoadAndCloseReader(DataService.GetReader(qc));
            return deals;
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListByMerchandiseGuid(IEnumerable<Guid> merchandiseGuids)
        {
            return DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                .Where(ViewBalancingDeal.Columns.MerchandiseGuid).In(merchandiseGuids)
                .ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListLikeDealName(string dealName, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon)
        {
            if (string.IsNullOrEmpty(dealName))
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                .Where(ViewBalancingDeal.Columns.DealName).Like(string.Format("%{0}%", dealName))
                .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);

            if (generatedOnly)
            {
                partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }

            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListByOrderStartRegion(DateTime? orderStartMin, DateTime? orderStartMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon)
        {
            if (!orderStartMin.HasValue && !orderStartMax.HasValue)
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = null; 
            if (orderStartMin.HasValue && orderStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                           .Where(ViewBalancingDeal.Columns.DealStartTime)
                           .IsLessThanOrEqualTo(orderStartMax.Value)
                           .And(ViewBalancingDeal.Columns.DealStartTime)
                           .IsGreaterThanOrEqualTo(orderStartMin.Value)
                           .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (orderStartMin.HasValue && !orderStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                           .Where(ViewBalancingDeal.Columns.DealStartTime)
                           .IsGreaterThanOrEqualTo(orderStartMin.Value)
                           .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (!orderStartMin.HasValue && orderStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                           .Where(ViewBalancingDeal.Columns.DealStartTime)
                           .IsLessThanOrEqualTo(orderStartMax.Value)
                           .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (generatedOnly)
            {
                partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }

            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListByOrderEndRegion(DateTime? orderEndMin, DateTime? orderEndMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon)
        {
            if (!orderEndMin.HasValue && !orderEndMax.HasValue)
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = null; 
            if (orderEndMin.HasValue && orderEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealEndTime)
                    .IsLessThanOrEqualTo(orderEndMax.Value)
                    .And(ViewBalancingDeal.Columns.DealEndTime)
                    .IsGreaterThanOrEqualTo(orderEndMin.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (orderEndMin.HasValue && !orderEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealEndTime)
                    .IsGreaterThanOrEqualTo(orderEndMin.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (!orderEndMin.HasValue && orderEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealEndTime)
                    .IsLessThanOrEqualTo(orderEndMax.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (generatedOnly)
            {
                partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }

            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListByUseStartRegion(DateTime? useStartMin, DateTime? useStartMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon)
        {
            if (!useStartMin.HasValue && !useStartMax.HasValue)
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = null; 
            if (useStartMin.HasValue && useStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseStartTime)
                    .IsLessThanOrEqualTo(useStartMax.Value)
                    .And(ViewBalancingDeal.Columns.DealUseStartTime)
                    .IsGreaterThanOrEqualTo(useStartMin.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (useStartMin.HasValue && !useStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseStartTime)
                    .IsGreaterThanOrEqualTo(useStartMin.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (!useStartMin.HasValue && useStartMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseStartTime)
                    .IsLessThanOrEqualTo(useStartMax.Value)
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);
            }

            if (generatedOnly)
            {
                partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }
            
            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
            
        }

        public ViewBalancingDealCollection ViewBalancingDealGetListByUseEndRegion(DateTime? useEndMin, DateTime? useEndMax, bool generatedOnly, bool filterBizModel, BusinessModel bizModel = BusinessModel.Ppon)
        {
            // ToDo: 
            // change ViewBalancingDeal.Columns.DealUseEndTime => ViewBalancingDeal.Columns.DealMaxUseEndTime
            // after system is able to provide a convienient way of retrieving the value.

            if (!useEndMin.HasValue && !useEndMax.HasValue)
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = null;

            if (useEndMin.HasValue && useEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseEndTime)
                    .IsLessThanOrEqualTo(useEndMax.Value)
                    .And(ViewBalancingDeal.Columns.DealUseEndTime)
                    .IsGreaterThanOrEqualTo(useEndMin.Value);
            }

            if (useEndMin.HasValue && !useEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseEndTime)
                    .IsGreaterThanOrEqualTo(useEndMin.Value);
            }

            if (!useEndMin.HasValue && useEndMax.HasValue)
            {
                partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.DealUseEndTime)
                    .IsLessThanOrEqualTo(useEndMax.Value);
            }

            partialQuery.And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);

            if (generatedOnly)
            {
                partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }
            
            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
            
        }
        
        public ViewBalancingDealCollection ViewBalancingDealGetListLikeSellerId(string sellerId, bool generatedOnly, bool filterBizModel, BusinessModel bizModel)
        {
            if (string.IsNullOrEmpty(sellerId))
            {
                return new ViewBalancingDealCollection();
            }

            SqlQuery partialQuery = DB.SelectAllColumnsFrom<ViewBalancingDeal>()
                    .Where(ViewBalancingDeal.Columns.SellerId).Like(string.Format("%{0}%", sellerId))
                    .And(ViewBalancingDeal.Columns.DeliveryType).IsEqualTo((int)DeliveryType.ToShop);

            if (generatedOnly)
            {
                partialQuery = partialQuery.And(ViewBalancingDeal.Columns.GenerationFrequency).IsNotNull();
            }

            if (filterBizModel)
            {
                partialQuery = partialQuery.And(ViewBalancingDeal.Columns.BusinessModel).IsEqualTo((int)bizModel);
            }

            return partialQuery.ExecuteAsCollection<ViewBalancingDealCollection>() ?? new ViewBalancingDealCollection();
        }

        public ViewBalancingDealCollection GetFlexiblePayToShopDealWithOutBalanceSheet(string condiction)
        {
            var sql = string.Format(@"select bh.GUID as merchandise_guid, i.item_name as deal_name, dp.unique_id as deal_id 
, bh.business_hour_order_time_s AS deal_start_time 
, bh.business_hour_order_time_e AS deal_end_time 
, bh.business_hour_deliver_time_s AS deal_use_start_time
, COALESCE (bh.changed_expire_date, bh.business_hour_deliver_time_e) AS deal_use_end_time
, bh.seller_GUID, pstore.store_count 
, CASE WHEN pstore.max_store_use_end_time > COALESCE (bh.changed_expire_date, bh.business_hour_deliver_time_e) 
  THEN pstore.max_store_use_end_time ELSE COALESCE (bh.changed_expire_date, bh.business_hour_deliver_time_e) END AS deal_max_use_end_time
, CASE WHEN goo.slug IS NULL THEN NULL ELSE CASE WHEN slug >= bh.business_hour_order_minimum THEN 1 ELSE 0 END END AS deal_established
, 0 as business_model, dp.delivery_type, s.seller_id 
, s.seller_name, da.vendor_receipt_type, da.remittance_type
, getdate() as interval_end
, 5 as generation_frequency
, 0 as is_confirmed_ready_to_pay
from business_hour bh with(nolock)
join item i with(nolock) on i.business_hour_guid = bh.GUID
join deal_property dp with(nolock) on dp.business_hour_guid = bh.GUID
join deal_accounting da with(nolock) on da.business_hour_guid = bh.GUID
join seller s with(nolock) on s.guid = bh.seller_guid
join group_order goo WITH (NOLOCK) ON bh.GUID = goo.business_hour_guid
join ppon_store as ps with(nolock) on ps.business_hour_guid = bh.guid and 
		((bh.business_hour_status & {4} > 0 and ps.vbs_right & {5} > 0) or 
	     (bh.business_hour_status & {4} = 0 and ps.vbs_right & {6} > 0))
OUTER APPLY (SELECT          COUNT(*) store_count, MAX(ps.changed_expire_date) 
                                AS max_store_use_end_time
    FROM               ppon_store ps
    WHERE           bh.GUID = ps.business_hour_guid) pstore
left join balance_sheet as bs with(nolock) on bs.product_guid = bh.guid and bs.store_guid = ps.store_guid
where dp.delivery_type = {0}
and da.remittance_type = {1}
and bh.business_hour_status & {2} = 0
and bs.Id is null
and bh.business_hour_deliver_time_s < getdate() {3}"
                , (int)DeliveryType.ToShop
                , (int)RemittanceType.Flexible
                , (int)BusinessHourStatus.ComboDealMain
                , condiction
                , (int)BusinessHourStatus.NoRestrictedStore
                , (int)VbsRightFlag.Verify
                , (int)VbsRightFlag.VerifyShop);

            var qc = new QueryCommand(sql, ViewBalancingDeal.Schema.Provider.Name);
            var data = new ViewBalancingDealCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        #endregion

        #region BalanceSheetBillRelationship
        public void BalanceSheetBillRelationshipSet(BalanceSheetBillRelationship bsbr)
        {
            DB.Save<BalanceSheetBillRelationship>(bsbr);
        }

        public BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBalanceSheetId(int balanceSheetId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetBillRelationship>()
                       .Where(BalanceSheetBillRelationship.Columns.BalanceSheetId).IsEqualTo(balanceSheetId)
                       .ExecuteAsCollection<BalanceSheetBillRelationshipCollection>()
                    ?? new BalanceSheetBillRelationshipCollection();
        }

        public BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBalanceSheetIds(IEnumerable<int> balanceSheetIds)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetBillRelationship>()
                       .Where(BalanceSheetBillRelationship.Columns.BalanceSheetId).In(balanceSheetIds)
                       .ExecuteAsCollection<BalanceSheetBillRelationshipCollection>()
                    ?? new BalanceSheetBillRelationshipCollection();
        }

        public BalanceSheetBillRelationshipCollection BalanceSheetBillRelationshipGetListByBillId(int billId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetBillRelationship>()
                       .Where(BalanceSheetBillRelationship.Columns.BillId).IsEqualTo(billId)
                       .ExecuteAsCollection<BalanceSheetBillRelationshipCollection>() 
                    ?? new BalanceSheetBillRelationshipCollection();
        }

        public bool BalanceSheetBillRelationshipDelete(int balanceSheetId, int billId)
        {
            //DB.Delete<BalanceSheetBillRelationship>(BalanceSheetBillRelationship.BillIdColumn.ColumnName, billId);
            var deleteSql = new List<SqlQuery>();
            deleteSql.Add(
                new Delete().From(BalanceSheetBillRelationship.Schema)
                .Where(BalanceSheetBillRelationship.BalanceSheetIdColumn).IsEqualTo(balanceSheetId)
                .And(BalanceSheetBillRelationship.BillIdColumn).IsEqualTo(billId)
                );
            SqlQuery.ExecuteTransaction(deleteSql);
            return true;
        }

        public BalanceSheetBillRelationship BalanceSheetBillRelationshipGet(int balanceSheetId, int billId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetBillRelationship>()
                .Where(BalanceSheetBillRelationship.Columns.BillId).IsEqualTo(billId)
                .And(BalanceSheetBillRelationship.Columns.BalanceSheetId).IsEqualTo(balanceSheetId)
                .ExecuteSingle<BalanceSheetBillRelationship>();
        }

        public int BalanceSheetBillRelationshipSetList(BalanceSheetBillRelationshipCollection relationships)
        {
            return DB.SaveAll(relationships);
        }

        public int BalanceSheetBillRelationshipGetCount(int sheetId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetBillRelationship>()
                .Where(BalanceSheetBillRelationship.Columns.BalanceSheetId).IsEqualTo(sheetId)
                .GetRecordCount();
        }

        #endregion

        #region BalanceSheetWmsBillRelation
        public void BalanceSheetWmsBillRelationshipSet(BalanceSheetWmsBillRelationship bsbr)
        {
            DB.Save<BalanceSheetWmsBillRelationship>(bsbr);
        }

        public int BalanceSheetWmsBillRelationshipSetList(BalanceSheetWmsBillRelationshipCollection relationships)
        {
            return DB.SaveAll(relationships);
        }

        public BalanceSheetWmsBillRelationshipCollection BalanceSheetWmsBillRelationshipGetListByBillId(int billId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetWmsBillRelationship>()
                       .Where(BalanceSheetWmsBillRelationship.Columns.BillId).IsEqualTo(billId)
                       .ExecuteAsCollection<BalanceSheetWmsBillRelationshipCollection>()
                    ?? new BalanceSheetWmsBillRelationshipCollection();
        }

        public BalanceSheetWmsBillRelationship BalanceSheetWmsBillRelationshipGet(int balanceSheetId, int billId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetWmsBillRelationship>()
                .Where(BalanceSheetWmsBillRelationship.Columns.BillId).IsEqualTo(billId)
                .And(BalanceSheetWmsBillRelationship.Columns.BalanceSheetId).IsEqualTo(balanceSheetId)
                .ExecuteSingle<BalanceSheetWmsBillRelationship>();
        }

        public BalanceSheetWmsBillRelationshipCollection BalanceSheetWmsBillRelationshipGetListByBalanceSheetId(int balanceSheetId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetWmsBillRelationship>()
                       .Where(BalanceSheetWmsBillRelationship.Columns.BalanceSheetId).IsEqualTo(balanceSheetId)
                       .ExecuteAsCollection<BalanceSheetWmsBillRelationshipCollection>()
                    ?? new BalanceSheetWmsBillRelationshipCollection();
        }

        public bool BalanceSheetWmsBillRelationshipDelete(int balanceSheetId, int billId)
        {
            //DB.Delete<BalanceSheetBillRelationship>(BalanceSheetBillRelationship.BillIdColumn.ColumnName, billId);
            var deleteSql = new List<SqlQuery>();
            deleteSql.Add(
                new Delete().From(BalanceSheetWmsBillRelationship.Schema)
                .Where(BalanceSheetWmsBillRelationship.BalanceSheetIdColumn).IsEqualTo(balanceSheetId)
                .And(BalanceSheetWmsBillRelationship.BillIdColumn).IsEqualTo(billId)
                );
            SqlQuery.ExecuteTransaction(deleteSql);
            return true;
        }

        public int BalanceSheetWmsBillRelationshipGetCount(int sheetId)
        {
            return DB.SelectAllColumnsFrom<BalanceSheetWmsBillRelationship>()
                .Where(BalanceSheetWmsBillRelationship.Columns.BalanceSheetId).IsEqualTo(sheetId)
                .GetRecordCount();
        }
        #endregion

        #region Bill

        public Bill BillGet(int billId)
        {
            return DB.Get<Bill>(billId);
        }

        public int BillSet(Bill bill)
        {
            return DB.Save<Bill>(bill);
        }

        public int BillSet(BillCollection bills)
        {
            return DB.SaveAll(bills);
        }

        public bool BillDelete(int billId)
        {
            DB.Destroy<Bill>(Bill.IdColumn.ColumnName, billId);
            return true;
        }

        public BillCollection BillGetList(params string[] filter)
        {
            QueryCommand qc = GetDCWhere(Bill.Schema, filter);
            qc.CommandSql = "select * from " + Bill.Schema.TableName + " with(nolock)" + qc.CommandSql;
            BillCollection bill = new BillCollection();
            bill.LoadAndCloseReader(DataService.GetReader(qc));
            return bill;
        }

        public BillCollection BillGetList(IEnumerable<int> billIds)
        {
            return DB.SelectAllColumnsFrom<Bill>()
                .Where(Bill.Columns.Id).In(billIds)
                .ExecuteAsCollection<BillCollection>() ?? new BillCollection();
        }

        public BillCollection BillGetByGuid(Guid guid)
        {
            string sql = string.Format(@"            
        select b.* from balance_sheet bs
join balance_sheet_bill_relationship bsbr on bs.id=bsbr.balance_sheet_id
join bill b on b.id = bsbr.bill_id
where bs.product_guid='{0}'
            ", guid);
            QueryCommand qc = new QueryCommand(sql, Bill.Schema.Provider.Name);
            var data = new ViewBalanceSheetListCollection();
            BillCollection bill = new BillCollection();
            bill.LoadAndCloseReader(DataService.GetReader(qc));
            return bill;
        }

        public BillCollection BillGetByBalanceSheetId(int bsid)
        {
            string sql = string.Format(@"            
        select b.* from balance_sheet bs
join balance_sheet_bill_relationship bsbr on bs.id=bsbr.balance_sheet_id
join bill b on b.id = bsbr.bill_id
where bs.id={0}
            ", bsid);
            QueryCommand qc = new QueryCommand(sql, Bill.Schema.Provider.Name);
            var data = new ViewBalanceSheetListCollection();
            BillCollection bill = new BillCollection();
            bill.LoadAndCloseReader(DataService.GetReader(qc));
            return bill;
        }
        #endregion

        #region BillBalanceSheetChangeLog

        public int BillBanaceSheetChangeLogSetList(BillBalanceSheetChangeLogCollection logs)
        {
            return DB.SaveAll(logs);
        }

        #endregion BillBalanceSheetChangeLog

        #region ViewBalanceSheetFundsTransferPpon  對帳單匯款狀態

        public ViewBalanceSheetFundsTransferPpon ViewBalanceSheetFundsTransferPponGetByBalanceSheetId(int bsId)
        {
            string sql = string.Format("select * from {0} where {1} = @bsId",
                ViewBalanceSheetFundsTransferPpon.Schema.TableName,
                ViewBalanceSheetFundsTransferPpon.Columns.BsId);

            var cmd = new QueryCommand(sql, ViewBalanceSheetFundsTransferPpon.Schema.Provider.Name);
            cmd.AddParameter("bsId", bsId, DbType.Int32);

            var funds = new ViewBalanceSheetFundsTransferPponCollection();
            funds.LoadAndCloseReader(DataService.GetReader(cmd));

            return funds.FirstOrDefault();
        }

        public ViewBalanceSheetFundsTransferPpon ViewBalanceSheetFundsTransferPponGetByPaymentId(int payId)
        {
            string sql = string.Format("select * from {0} where {1} = @payId",
                ViewBalanceSheetFundsTransferPpon.Schema.TableName,
                ViewBalanceSheetFundsTransferPpon.Columns.PayId);

            var cmd = new QueryCommand(sql, ViewBalanceSheetFundsTransferPpon.Schema.Provider.Name);
            cmd.AddParameter("payId", payId, DbType.Int32);

            var funds = new ViewBalanceSheetFundsTransferPponCollection();
            funds.LoadAndCloseReader(DataService.GetReader(cmd));

            return funds.FirstOrDefault();
        }

        public ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(BusinessModel biz, int? dealId, DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd, bool? isTransferComplete)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewBalanceSheetFundsTransferPpon>()
                .Where(ViewBalanceSheetFundsTransferPpon.Columns.ProductType).IsEqualTo((int)biz);
            
            if (dealId.HasValue)
            {
                query.And(ViewBalanceSheetFundsTransferPpon.Columns.DealId).IsEqualTo(dealId);
            }

            if (minBsIntervalStart.HasValue)
            {
                query.And(ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalStart)
                    .IsGreaterThanOrEqualTo(minBsIntervalStart);
            }

            if (maxBsIntervalEnd.HasValue)
            {
                query.And(ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalEnd)
                    .IsLessThanOrEqualTo(maxBsIntervalEnd);
            }

            if (isTransferComplete.HasValue)
            {
                if (isTransferComplete.Value)
                {
                    query.And(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsEqualTo(true);
                }
                else
                {
                    query.AndExpression(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsNull()
                        .Or(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete).IsNotEqualTo(true)
                        .CloseExpression();
                }
            }

            return query.ExecuteAsCollection<ViewBalanceSheetFundsTransferPponCollection>();
        }

        public ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(BusinessModel biz, int? dealId,
            DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd, DateTime? minBillCreateTime, DateTime? maxBillCreateTime, bool? isTransferComplete, List<BalanceSheetType> bsTypes)
        {
            //處理bsTypes
            string bsType = "";
            foreach (var type in bsTypes)
            {
                int typeToInt = Convert.ToInt32(type);
                bsType += typeToInt.ToString() + ",";
            }
            if (bsType.Length > 0)
            {
                bsType = bsType.Remove(bsType.Length-1, 1);
            }

            //組裝sql
            ViewBalanceSheetFundsTransferPponCollection vbfp = new ViewBalanceSheetFundsTransferPponCollection();
            string sql = string.Format("select vbftp.* from {0} vbftp "+
            "LEFT JOIN {1} rel ON vbftp.bs_id = rel.balance_sheet_id "+
            "LEFT JOIN {2} ON rel.bill_id = bill.id "+
            "where vbftp.{3} = @biz " +
            (dealId.HasValue ? string.Format(" and vbftp.{0} = @dealId ", ViewBalanceSheetFundsTransferPpon.Columns.DealId) : "") + 
            (minBsIntervalStart.HasValue ? string.Format(" and vbftp.{0} >= @minBsIntervalStart ", ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalStart) : "") + 
            (maxBsIntervalEnd.HasValue ? string.Format(" and vbftp.{0} <= @maxBsIntervalEnd ", ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalEnd) : "") +
            "and vbftp.is_confirmed_ready_to_pay=1 " +
            string.Format((isTransferComplete.HasValue ? (isTransferComplete.Value ? " and vbftp.{0}=1" : " and (vbftp.{0}=0 or vbftp.{0} is null)") : ""), ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete) +
            string.Format((minBillCreateTime.HasValue && maxBillCreateTime.HasValue ?
                " and bill.{0} >= @minBillCreateTime and bill.{0} <= @maxBillCreateTime " :
                    (minBillCreateTime.HasValue && !maxBillCreateTime.HasValue ? "and bill.{0} >= @minBillCreateTime " :
                        (!minBillCreateTime.HasValue && maxBillCreateTime.HasValue ? "and bill.{0} <= @maxBillCreateTime " : ""))),Bill.Columns.CreateTime),
            ViewBalanceSheetFundsTransferPpon.Schema.TableName,
            BalanceSheetBillRelationship.Schema.TableName,
            Bill.Schema.TableName,
            ViewBalanceSheetFundsTransferPpon.Columns.ProductType);            

            var cmd = new QueryCommand(sql, ViewBalanceSheetFundsTransferPpon.Schema.Provider.Name);
            cmd.AddParameter("biz", biz, DbType.Int32);
            if (dealId.HasValue) 
            {
                cmd.AddParameter("dealId", dealId, DbType.Int32);
            }
            if (minBsIntervalStart.HasValue)
            {
                cmd.AddParameter("minBsIntervalStart", minBsIntervalStart, DbType.DateTime);
            }
            if (maxBsIntervalEnd.HasValue)
            {
                cmd.AddParameter("maxBsIntervalEnd", maxBsIntervalEnd, DbType.DateTime);
            }
            if (minBillCreateTime.HasValue)
            {
                cmd.AddParameter("minBillCreateTime", minBillCreateTime, DbType.DateTime);
            }
            if (maxBillCreateTime.HasValue)
            {
                cmd.AddParameter("maxBillCreateTime", maxBillCreateTime, DbType.DateTime);
            }

            vbfp.LoadAndCloseReader(DataService.GetReader(cmd));

            return vbfp;
        }
        
        public ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetList(DateTime transferIntervalStart, DateTime transferIntervalEnd, bool? isTransferComplete)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewBalanceSheetFundsTransferPpon>()
                .Where(ViewBalanceSheetFundsTransferPpon.Columns.ProductType).IsEqualTo((int)BusinessModel.Ppon)
                .And(ViewBalanceSheetFundsTransferPpon.Columns.ResponseTime)
                    .IsGreaterThanOrEqualTo(transferIntervalStart)
                .And(ViewBalanceSheetFundsTransferPpon.Columns.ResponseTime)
                    .IsLessThanOrEqualTo(transferIntervalEnd);

            if (isTransferComplete.HasValue)
            {
                if (isTransferComplete.Value)
                {
                    query.And(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsEqualTo(true);
                }
                else
                {
                    query.AndExpression(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsNull()
                        .Or(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete).IsNotEqualTo(true)
                        .CloseExpression();
                }
            }

            return query.ExecuteAsCollection<ViewBalanceSheetFundsTransferPponCollection>();
        }

        public ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetListByIntervalEnd(DateTime minBsIntervalEnd, DateTime maxBsIntervalEnd, bool? isTransferComplete)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewBalanceSheetFundsTransferPpon>()
                .Where(ViewBalanceSheetFundsTransferPpon.Columns.ProductType).IsEqualTo((int)BusinessModel.Ppon)
                .And(ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalEnd)
                    .IsGreaterThanOrEqualTo(minBsIntervalEnd)
                .And(ViewBalanceSheetFundsTransferPpon.Columns.BsIntervalEnd)
                    .IsLessThanOrEqualTo(maxBsIntervalEnd);

            if (isTransferComplete.HasValue)
            {
                if (isTransferComplete.Value)
                {
                    query.And(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsEqualTo(true);
                }
                else
                {
                    query.AndExpression(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete)
                        .IsNull()
                        .Or(ViewBalanceSheetFundsTransferPpon.Columns.IsTransferComplete).IsNotEqualTo(true)
                        .CloseExpression();
                }
            }

            return query.ExecuteAsCollection<ViewBalanceSheetFundsTransferPponCollection>();
        }

        public ViewBalanceSheetFundsTransferPponCollection ViewBalanceSheetFundsTransferPponGetListByBalanceSheetIds(IEnumerable<int> bsIds)
        {
            return DB.SelectAllColumnsFrom<ViewBalanceSheetFundsTransferPpon>()
                .Where(ViewBalanceSheetFundsTransferPpon.Columns.BsId).In(bsIds)
                .ExecuteAsCollection<ViewBalanceSheetFundsTransferPponCollection>() ?? new ViewBalanceSheetFundsTransferPponCollection();
        }

        #endregion

        #region ViewRemainderBillList
        public ViewRemainderBillListCollection ViewRemainderBillListGetList(params string[] filter)
        {
            ViewRemainderBillListCollection vrbl = new ViewRemainderBillListCollection();
            QueryCommand qc = GetDCWhere(ViewRemainderBillList.Schema, filter);
            qc.CommandSql = "select * from " + ViewRemainderBillList.Schema.TableName + " with(nolock)" + qc.CommandSql;
            qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + ViewRemainderBillList.Columns.Id + " desc ";
            vrbl.LoadAndCloseReader(DataService.GetReader(qc));

            return vrbl;
        }
        #endregion

        #region ViewBalanceSheetBillFundTransfer

        public ViewBalanceSheetBillFundTransferCollection ViewBalanceSheetBillFundTransferGetList(
            IEnumerable<BalanceSheetType> bsTypes,
            BusinessModel biz, int? dealId, string billCreator, DateTime? minBsIntervalStart, DateTime? maxBsIntervalEnd, 
            DateTime? minBillCreateTime, DateTime? maxBillCreateTime, bool? isTransferComplete, bool confirmedOnly = true)
        {
            SqlQuery query = DB.SelectAllColumnsFrom<ViewBalanceSheetBillFundTransfer>()
                .Where(ViewBalanceSheetBillFundTransfer.Columns.ProductType).IsEqualTo((int)biz)
                .And(ViewBalanceSheetBillFundTransfer.Columns.BalanceSheetType).In(bsTypes);

            if (dealId.HasValue)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.DealId).IsEqualTo(dealId);
            }

            if (!string.IsNullOrWhiteSpace(billCreator))
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.BillCreator).IsEqualTo(billCreator);
            }

            if (minBsIntervalStart.HasValue)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.IntervalStart)
                    .IsGreaterThanOrEqualTo(minBsIntervalStart);
            }

            if (maxBsIntervalEnd.HasValue)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.IntervalEnd)
                    .IsLessThanOrEqualTo(maxBsIntervalEnd);
            }

            if (minBillCreateTime.HasValue)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.BillCreateTime)
                    .IsGreaterThanOrEqualTo(minBillCreateTime);
            }

            if (maxBillCreateTime.HasValue)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.BillCreateTime)
                    .IsLessThanOrEqualTo(maxBillCreateTime);
            }

            if (confirmedOnly)
            {
                query.And(ViewBalanceSheetBillFundTransfer.Columns.IsConfirmedReadyToPay)
                    .IsEqualTo(confirmedOnly);
            }

            if (isTransferComplete.HasValue)
            {
                if (isTransferComplete.Value)
                {
                    query.And(ViewBalanceSheetBillFundTransfer.Columns.IsTransferComplete)
                        .IsEqualTo(true);
                }
                else
                {
                    query.AndExpression(ViewBalanceSheetBillFundTransfer.Columns.IsTransferComplete)
                        .IsNull()
                        .Or(ViewBalanceSheetBillFundTransfer.Columns.IsTransferComplete).IsNotEqualTo(true)
                        .CloseExpression();
                }                
            }

            return query.ExecuteAsCollection<ViewBalanceSheetBillFundTransferCollection>();
        }

        #endregion

        #region ViewBalanceSheetFundTransfer

        public ViewBalanceSheetFundTransfer ViewBalanceSheetFundTransferGetByBalanceSheetId(int bsId)
        {
            string sql = string.Format("select * from {0} where {1} = @bsId",
                ViewBalanceSheetFundTransfer.Schema.TableName,
                ViewBalanceSheetFundTransfer.Columns.BalanceSheetId);

            var cmd = new QueryCommand(sql, ViewBalanceSheetFundTransfer.Schema.Provider.Name);
            cmd.AddParameter("bsId", bsId, DbType.Int32);

            var funds = new ViewBalanceSheetFundTransferCollection();
            funds.LoadAndCloseReader(DataService.GetReader(cmd));

            return funds.FirstOrDefault();
        }

        public ViewBalanceSheetFundTransferCollection ViewBalanceSheetFundTransferGetListByBalanceSheetIds(IEnumerable<int> bsIds)
        {
            return DB.SelectAllColumnsFrom<ViewBalanceSheetFundTransfer>()
                   .Where(ViewBalanceSheetFundTransfer.Columns.BalanceSheetId).In(bsIds)
                   .ExecuteAsCollection<ViewBalanceSheetFundTransferCollection>() ?? new ViewBalanceSheetFundTransferCollection();
        }

        #endregion

        #region VendorPaymentChange

        public VendorPaymentChange VendorPaymentChangeGet(int id)
        {
            return DB.Get<VendorPaymentChange>(id);
        }

        public VendorPaymentChangeCollection VendorPaymentChangeGetList(Guid bid)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentChange>().NoLock()
                     .Where(VendorPaymentChange.BusinessHourGuidColumn).IsEqualTo(bid)
                     .ExecuteAsCollection<VendorPaymentChangeCollection>() ?? new VendorPaymentChangeCollection();
        }

        public VendorPaymentChangeCollection VendorPaymentChangeGetList(IEnumerable<Guid> bids)
        {
            var infos = new VendorPaymentChangeCollection();
            List<Guid> tempIds = bids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<VendorPaymentChange>().NoLock()
                        .Where(VendorPaymentChange.Columns.BusinessHourGuid).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<VendorPaymentChangeCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public VendorPaymentChangeCollection VendorPaymentChangeGetList(int bsId)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentChange>().NoLock()
                     .Where(VendorPaymentChange.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<VendorPaymentChangeCollection>() ?? new VendorPaymentChangeCollection();
        }
        public VendorPaymentChangeCollection VendorPaymentChangeGetListByIds(List<int> vId)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentChange>().NoLock()
                     .Where(VendorPaymentChange.IdColumn).In(vId)
                     .ExecuteAsCollection<VendorPaymentChangeCollection>() ?? new VendorPaymentChangeCollection();
        }

        public VendorPaymentChangeCollection VendorPaymentChangeGetList(IEnumerable<int> ids)
        {
            var infos = new VendorPaymentChangeCollection();
            List<int> tempIds = ids.Distinct().ToList();
            int idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                int batchLimit = 2000;  // sql server cannot take more than 2100 parameters.
                var batchProd = DB.SelectAllColumnsFrom<VendorPaymentChange>().NoLock()
                        .Where(VendorPaymentChange.Columns.Id).In(tempIds.Skip(idx).Take(batchLimit))
                        .ExecuteAsCollection<VendorPaymentChangeCollection>();

                infos.AddRange(batchProd);

                idx += batchLimit;
            }

            return infos;
        }

        public int VendorPaymentChangeSet(VendorPaymentChange vendorPaymentChange)
        {
            return DB.Save<VendorPaymentChange>(vendorPaymentChange);
        }

        public int VendorPaymentChangeSetList(VendorPaymentChangeCollection vendorPaymentChanges)
        {
            return DB.SaveAll(vendorPaymentChanges);
        }

        #endregion VendorPaymentChange

        #region VendorPaymentOverdue
        public int VendorPaymentOverdueSet(VendorPaymentOverdue vendorPaymentOverdue)
        {
            return DB.Save<VendorPaymentOverdue>(vendorPaymentOverdue);
        }

        public VendorPaymentOverdueCollection VendorPaymentOverdueGetListByBid(Guid bid)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentOverdue>().NoLock()
                     .Where(VendorPaymentOverdue.BusinessHourGuidColumn).IsEqualTo(bid)
                     .ExecuteAsCollection<VendorPaymentOverdueCollection>() ?? new VendorPaymentOverdueCollection();
        }
        
        public int VendorPaymentOverdueSetList(VendorPaymentOverdueCollection vendorPaymentOverdues)
        {
            return DB.SaveAll(vendorPaymentOverdues);
        }

        public VendorPaymentOverdue VendorPaymentOverdueGetByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentOverdue>()
                .Where(VendorPaymentOverdue.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(VendorPaymentOverdue.Columns.CreateId).IsEqualTo(config.SystemEmail)
                .ExecuteSingle<VendorPaymentOverdue>();
        }

        public VendorPaymentOverdueCollection VendorPaymentOverdueGetListByBsid(int bsId)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentOverdue>().NoLock()
                     .Where(VendorPaymentOverdue.BalanceSheetIdColumn).IsEqualTo(bsId)
                     .ExecuteAsCollection<VendorPaymentOverdueCollection>() ?? new VendorPaymentOverdueCollection();
        }

        public VendorPaymentOverdueCollection VendorPaymentOverdueGetListByOverdueId(int id)
        {
            return DB.SelectAllColumnsFrom<VendorPaymentOverdue>().NoLock()
                     .Where(VendorPaymentOverdue.IdColumn).IsEqualTo(id)
                     .ExecuteAsCollection<VendorPaymentOverdueCollection>() ?? new VendorPaymentOverdueCollection();
        }

        #endregion

        #region ViewVendorPaymentOverdue
        public ViewVendorPaymentOverdueCollection ViewVendorPaymentOverdueGetListByBsid(int bsId)
        {
            return DB.SelectAllColumnsFrom<ViewVendorPaymentOverdue>().NoLock()
                     .Where(ViewVendorPaymentOverdue.Columns.BalanceSheetId).IsEqualTo(bsId)
                     .ExecuteAsCollection<ViewVendorPaymentOverdueCollection>() ?? new ViewVendorPaymentOverdueCollection();
        } 
        #endregion

        #region VendorPaymentFineCategory
        public VendorFineCategoryCollection VendorFineCategoryGetList()
        {
            return DB.SelectAllColumnsFrom<VendorFineCategory>().NoLock()
                     .ExecuteAsCollection<VendorFineCategoryCollection>() ?? new VendorFineCategoryCollection();
        }

        public VendorFineCategory VendorFineCategoryGetList(int id)
        {
            return DB.SelectAllColumnsFrom<VendorFineCategory>()
                .Where(VendorFineCategory.Columns.Id).IsEqualTo(id)
                .ExecuteSingle<VendorFineCategory>();
        }

        public int VendorFineCategorySet(VendorFineCategory vendorFineCategory)
        {
            return DB.Save<VendorFineCategory>(vendorFineCategory);
        }
        #endregion
    }
}

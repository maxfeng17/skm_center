﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class HiLifeProvider : IHiLifeProvider
    {
        #region HiLifeVerifyLog
        public bool SetHiLifeVerifyLog(List<HiLifeVerifyLog> logList)
        {
            using (var db = new HiLifeDbContextcs())
            {
                db.HiLifeVerifyLogEntities.AddRange(logList);
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region HiLifePincode

        public bool SetHiLifePincode(List<HiLifePincode> pincodeList)
        {
            using (var db = new HiLifeDbContextcs())
            {
                db.HiLifePincodeEntities.AddRange(pincodeList);
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdateHiLifePincode(HiLifePincode entity)
        {
            using (var db = new HiLifeDbContextcs())
            {
                entity.ModifyTime = DateTime.Now;
                db.Entry(entity).State = EntityState.Modified;
                var result = db.SaveChanges();
                return result > 0;
            }
        }



        public bool UpdateHiLifePincodeReturnStatus(List<HiLifePincode> list)
        {
            using (var db = new HiLifeDbContextcs())
            {
                list.ForEach(x => {
                    var pincode = db.HiLifePincodeEntities.FirstOrDefault(y => y.Id == x.Id);
                    if (pincode != null)
                    {
                        pincode.ReturnStatus = x.ReturnStatus;
                        pincode.ModifyTime = DateTime.Now;
                    }
                });
                db.SaveChanges();
            }
            return true;
        }


        public List<HiLifePincode> GetHiLifePincodeByGift(Guid orderGuid, int cnt, List<int> couponIds)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    //濾出是禮物的
                    var result = (from hp in db.HiLifePincodeEntities
                                  join p in db.PeztempEntities on hp.PeztempId equals p.Id
                                 
                                  where hp.OrderGuid == orderGuid
                                        && hp.ReturnStatus == (byte)HiLifeReturnStatus.Default
                                        && hp.IsVerified == false
                                        && couponIds.Contains(hp.CouponId)
                                  orderby hp.ViewCount ascending, hp.Id ascending
                                  select hp).Take(cnt).ToList();

                    if (result.Any())
                    {
                        //加ViewCount
                        UpdateHiLifePincodeViewCount(result.Select(x => x.Id).ToList());
                    }

                    return result;
                }
            }
        }

        public List<HiLifePincode> GetHiLifePincodeAndSee(Guid orderGuid, int cnt, List<int> excludeCouponId)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    var result = (from hp in db.HiLifePincodeEntities
                                  join p in db.PeztempEntities on hp.PeztempId equals p.Id
                                  where hp.OrderGuid == orderGuid
                                        && hp.ReturnStatus == (byte)HiLifeReturnStatus.Default
                                        && hp.IsVerified == false
                                        && !excludeCouponId.Contains(hp.CouponId)
                                  orderby hp.ViewCount ascending, hp.Id ascending
                                  select hp).Take(cnt).ToList();

                    if (result.Any())
                    {
                        //加ViewCount
                        UpdateHiLifePincodeViewCount(result.Select(x => x.Id).ToList());
                    }

                    return result;
                }
            }
        }

        public List<HiLifePincode> GetHiLifePincode(Guid orderGuid)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    var result = (from hp in db.HiLifePincodeEntities
                                  join p in db.PeztempEntities on hp.PeztempId equals p.Id
                                  where hp.OrderGuid == orderGuid
                                  orderby hp.ViewCount ascending, hp.Id ascending
                                  select hp).ToList();
                    
                    return result;
                }
            }
        }

        public HiLifePincode GetHiLifePincode(string pincode)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    var result = (from hp in db.HiLifePincodeEntities
                                  where hp.Pincode == pincode
                                  select hp).FirstOrDefault();
                  
                    return result;
                }
            }
        }

        public int UpdateHiLifePincodeViewCount(List<int> idList)
        {
            if (idList.Count == 0) return 0;

            StringBuilder sb = new StringBuilder("@oid0");
            if (idList.Count > 1)
            {
                for (int i = 1; i < idList.Count; i++)
                {
                    sb.Append(",@oid" + i);
                }
            }

            var sql = string.Format(@"UPDATE hi_life_pincode SET view_count = view_count + 1, modify_time = getdate() WHERE 
                                    id in ({0})", sb.ToString());

            var parameterList = idList.Select((t, i) => new SqlParameter("@oid" + i, t)).ToList();

            using (var db = new HiLifeDbContextcs())
            {
                return db.Database.ExecuteSqlCommand(sql, parameterList.ToArray());
            }
        }

        #endregion

        #region ViewCouponPeztempList

        public List<ViewCouponPeztempList> GetViewCouponPeztempList(Guid orderGuid)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    var result = (from vcp in db.ViewCouponPeztempListEntities
                                  where vcp.OrderGuid == orderGuid
                                  select vcp).ToList();
                    return result;
                }
            }
        }


        #endregion

        #region HiLifeGetPincodeLog

        public bool AddHiLifeGetPincodeLog(List<HiLifePincode> data, int userId, Guid orderGuid, int requestQty)
        {
            using (var db = new HiLifeDbContextcs())
            {
                var batchId = Guid.NewGuid();
                var requestTime = DateTime.Now;

                foreach (var d in data)
                {
                    db.Entry(new HiLifeGetPincodeLog()
                    {
                        BatchId = batchId,
                        OrderGuid = orderGuid,
                        RequestTime = requestTime,
                        RequestQty = requestQty,
                        UserId = userId,
                        CouponId = d.CouponId,
                        PeztempId = d.PeztempId,
                        Pincode = d.Pincode,
                        IpAddress = Helper.GetClientIP(),

                    }).State = EntityState.Added;
                }

                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region Peztemp

        public Peztemp GetPeztemp(int pezId)
        {
            using (var db = new HiLifeDbContextcs())
            {
                using (db.NoLock())
                {
                    var result = (from p in db.PeztempEntities
                                  where p.Id == pezId
                                  select p).FirstOrDefault();
                    return result;
                }
            }
        }
        #endregion

        #region  HiLifeReturnLog

        public List<HiLifeReturnLog> AddHiLifeNetReturnLog(List<HiLifeReturnLog> entity)
        {
            using (var db = new HiLifeDbContextcs())
            {

                db.HiLifeReturnLogEntities.AddRange(entity);
                db.SaveChanges();
                return entity;
            }
        }

        public List<HiLifeReturnLog> GetHiLifeReturnDataByTime(DateTime time)
        {
            using (var db = new HiLifeDbContextcs())
            {
                string sql = @"SELECT h.id,h.batch_id as batchid,h.return_date as returndate,h.active,h.reason,h.pincode,h.order_guid as orderguid,h.ip_address as ipaddress,
                                h.create_time as createtime,h.hilife_return as hilifereturn,h.return_type as returntype FROM   Hi_Life_Return_Log h
                                INNER JOIN Hi_Life_Pincode p ON h.order_guid = p.order_guid AND h.pincode = p.pincode
                                WHERE h.active = '" + HiLifeReturnReplyCode.Y.ToString() + @"' AND return_status = " + (int)HiLifeReturnStatus.Success + @" AND h.create_time > @time
                                AND h.return_date = (SELECT Max(h2.return_date) FROM   Hi_Life_Return_Log h2 WHERE  h.pincode = h2.pincode) 
                                order by h.return_date";

                var result = db.Database.SqlQuery<HiLifeReturnLog>(sql, new SqlParameter("@time", time)).ToList();
                return result;
            }
        }

        #endregion

        #region HiLifeApiLog
        public bool SetHiLifeApiLog(HiLifeApiLog log)
        {
            using (var db = new HiLifeDbContextcs())
            {
                db.Entry(log).State = log.Id == 0 ?
                                        EntityState.Added :
                                        EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        } 
        #endregion

    }
}

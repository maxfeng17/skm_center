﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.SsBLL
{
    public class SSBonusProvider : IBonusProvider
    {
        #region ViewIdentityCode

        /// <summary>
        /// 依 code 與商家 user_id 取回 Identity Code
        /// </summary>
        /// <param name="code"></param>
        /// <param name="sellerUserId"></param>
        /// <param name="transDateTime"></param>
        /// <returns></returns>
        public ViewIdentityCode ViewIdentityCodeGet(string code, int sellerUserId, DateTime transDateTime)
        {
            //建卡帳號核銷
            //const string sql = @"SELECT * FROM dbo.view_identity_code WHERE code = @code AND seller_user_id = @sellerUserId AND expired_time > GETDATE()";

            const string sql = @"SELECT * FROM dbo.view_identity_code WHERE code = @code AND expired_time > @transDateTime";

            var qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@code", code, DbType.String);
            qc.AddParameter("@transDateTime", transDateTime, DbType.DateTime);
            //qc.AddParameter("@sellerUserId", sellerUserId, DbType.Int32);

            var identityCode = new ViewIdentityCode();
            identityCode.LoadAndCloseReader(DataService.GetReader(qc));

            return identityCode;
        }

        /// <summary>
        /// 依Identity code id 取回資料
        /// </summary>
        /// <param name="identityCodeId"></param>
        /// <returns></returns>
        public ViewIdentityCode ViewIdentityCodeGet(long identityCodeId)
        {
            var vic = new ViewIdentityCode();
            vic.LoadAndCloseReader(DB.SelectAllColumnsFrom<ViewIdentityCode>()
                .Where(ViewIdentityCode.Columns.Id)
                .IsEqualTo(identityCodeId)
                .ExecuteReader());
            return vic;
        }

        public ViewIdentityCode IdentityCodeSet(int membershipCardId, int groupId, DateTime expiredTime, int createUserId, int? discountCodeId = null)
        {
            const string sql = @"Declare @isCreated bit, @identityCode varchar(10), @identityCodeId bigint, @executeTime int;
                            Select @isCreated = 0, @identityCodeId = 0, @executeTime = 0;

                            while @isCreated = 0 begin
	                            --create identity_code.code
	                            Set @identityCode = Right('0000000000' + convert(varchar, Convert(decimal , Round(RAND() * 9999999999, 0) + 1)), 10)
	                            INSERT INTO identity_code (card_id, code, user_id, create_time, expired_time, discount_code_id)
                                    SELECT @cardId, @identityCode, @userId, GETDATE(), @expiredTime, @discountCodeId
                                    WHERE NOT EXISTS (SELECT 1 FROM view_identity_code 
			                            WHERE card_group_id = @groupId 
			                            AND code = @identityCode
			                            AND expired_time > GETDATE());
	                            -- check success, if fail goto retry
                                if(@@IDENTITY Is Not Null And @identityCodeId <> @@IDENTITY) begin
                                    Set @isCreated = 1
                                    SELECT @identityCodeId = @@IDENTITY
                                end
                                -- retry 50000 times
                                Set @executeTime = @executeTime + 1
                                if @executeTime > 50000 begin
                                    break;
                                end
                            end;
                            Select * from view_identity_code where id = @@IDENTITY;
                          ";

            var qc = new QueryCommand(sql, CvsStore.Schema.Provider.Name);
            qc.AddParameter("@cardId", membershipCardId, DbType.Int32);
            qc.AddParameter("@userId", createUserId, DbType.Int32);
            qc.AddParameter("@ExpiredTime", expiredTime, DbType.DateTime);
            qc.AddParameter("@groupId", groupId, DbType.Int32);
            if (discountCodeId == null)
            {
                qc.AddParameter("@discountCodeId", DBNull.Value, DbType.Int32);
            }
            else
            {
                qc.AddParameter("@discountCodeId", discountCodeId, DbType.Int32);
            }


            var identityCode = new ViewIdentityCode();
            identityCode.LoadAndCloseReader(DataService.GetReader(qc));
            return identityCode;
        }

        #endregion ViewIdentityCode
    }
}

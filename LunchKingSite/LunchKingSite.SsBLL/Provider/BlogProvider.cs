﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Linq;
using LunchKingSite.Core.Enumeration;
using System.Collections.Generic;
using LunchKingSite.Core.Component;

namespace LunchKingSite.SsBLL.Provider
{
    public class BlogProvider : IBlogProvider
    {
        #region BlogPost
        public BlogPost BlogPostGet(int id)
        {
            return DB.Get<BlogPost>(id);
        }
        public BlogPost BlogPostGetByGid(Guid gid)
        {
            return DB.Select().From<BlogPost>()
                .Where(BlogPost.GuidColumn).IsEqualTo(gid)
                .And(BlogPost.StatusColumn).IsNotEqualTo(-1)
               .ExecuteSingle<BlogPost>();
        }
        public BlogPost BlogPostGetAllByGid(Guid gid)
        {
            return DB.Select().From<BlogPost>()
                .Where(BlogPost.GuidColumn).IsEqualTo(gid)
               .ExecuteSingle<BlogPost>();
        }
        public int BlogPostGetCount(params string[] filter)
        {
            int rowCnt = 0;
            QueryCommand qc = GetDCWhere(BlogPost.Schema, filter);
            qc.CommandSql = "select count(*) from " + BlogPost.Schema.Provider.DelimitDbName(BlogPost.Schema.TableName) + " with(nolock) " + qc.CommandSql;

            rowCnt = (int)DataService.ExecuteScalar(qc);
            return rowCnt;
        }
        public BlogPostCollection BlogPostGetList(int pageNumber, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<BlogPost, BlogPostCollection>(pageNumber, pageSize, orderBy, filter);
        }
        public bool BlogPostSet(BlogPost blog)
        {
            DB.Save<BlogPost>(blog);
            return true;
        }
        #endregion BlogPost

        #region BlogPageView
        public int BlogPageViewGetCount(params string[] filter)
        {
            int rowCnt = 0;
            QueryCommand qc = GetDCWhere(BlogPageView.Schema, filter);
            qc.CommandSql = "select count(*) from " + BlogPageView.Schema.Provider.DelimitDbName(BlogPageView.Schema.TableName) + " with(nolock) " + qc.CommandSql;

            rowCnt = (int)DataService.ExecuteScalar(qc);
            return rowCnt;
        }
        public bool BlogPageViewSet(BlogPageView view)
        {
            DB.Save<BlogPageView>(view);
            return true;
        }
        #endregion BlogPageView


        protected QueryCommand GetDCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }
    }
}

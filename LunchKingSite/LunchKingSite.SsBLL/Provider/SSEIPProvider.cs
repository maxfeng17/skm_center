﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.SsBLL.Provider
{
    public class SSEIPProvider: IEIPProvider
    {
        #region DocumentList
        public DocumentListCollection DocumentListGet(params string[] filter)
        {
            string AddWhere = "";
            foreach (string data in filter)
            {
                AddWhere += " And " + data;
            }

            string sql = @" select * FROM " + DocumentList.Schema.Provider.DelimitDbName(DocumentList.Schema.TableName) + @" With(nolock) " +
                             " Where 1=1 " + AddWhere + 
                            " Order By " + DocumentList.Columns.CreateTime + " Desc";

            

            QueryCommand qc = new QueryCommand(sql, DocumentList.Schema.Provider.Name);

            DocumentListCollection ocol = new DocumentListCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
            //return SSHelper.GetQueryResultByFilterOrder<DocumentList, DocumentListCollection>(1, -1, null, filter);

            //return DB.SelectAllColumnsFrom<DocumentList>().NoLock()
            //    .ExecuteAsCollection<DocumentListCollection>();
        }
        public DocumentList DocumentListGetById(Guid guid)
        {
            return DB.Get<DocumentList>(DocumentList.Columns.Guid, guid);
        }
        public DocumentListCollection DocumentListGetByCategoryId(Guid catGuid)
        {
            return DB.SelectAllColumnsFrom<DocumentList>().NoLock()
                .Where(DocumentList.Columns.CategoryGuid).IsEqualTo(catGuid)
                .ExecuteAsCollection<DocumentListCollection>();
        }
        public bool DocumentListSet(DocumentList dl)
        {
            DB.Save<DocumentList>(dl);
            return true;
        }
        #endregion DocumentList

        #region DocumentCategory

        public DocumentCategory DocumentCategoryGetById(Guid guid)
        {
            return DB.Get<DocumentCategory>(DocumentCategory.Columns.Guid, guid);
        }
        public DocumentCategoryCollection DocumentCategoryGet()
        {
            return DB.SelectAllColumnsFrom<DocumentCategory>().NoLock()
                 .Where(DocumentCategory.Columns.IsDelete).IsNotEqualTo(true)
                .ExecuteAsCollection<DocumentCategoryCollection>();
        }

        public bool DocumentCategorySet(DocumentCategory dc)
        {
            DB.Save<DocumentCategory>(dc);
            return true;
        }
        #endregion DocumentCategory

        #region TaskFlow
        public TaskFlow TaskFlowGetByProcessId(Guid processId)
        {
            return DB.Get<TaskFlow>(TaskFlow.Columns.ProcessId, processId);
        }
        
        /// <summary>
        /// 待辦事項
        /// </summary>
        /// <param name="execUser">員工Guid(EasyFlow)</param>
        /// <returns></returns>
        public TaskFlowCollection TaskFlowRunningGet(Guid execUser)
        {
            //本身的待辦跟代理的待辦都要可以看到
            string sql = "SELECT * FROM " + TaskFlow.Schema.Provider.DelimitDbName(TaskFlow.Schema.TableName) + " With(nolock) " +
                        " WHERE (" + TaskFlow.Columns.ExecUser + " = @execUser or " + TaskFlow.Columns.AgentUser + " = @execUser)" +
                        " And " + TaskFlow.Columns.EipTaskStatus + " = " + (int)EIPTaskStatus.Running + 
                        " Order By " + TaskFlow.Columns.CreateTime + " Desc ";
            QueryCommand qc = new QueryCommand(sql, TaskFlow.Schema.Provider.Name);
            qc.AddParameter("@execUser", execUser, DbType.Guid);
            TaskFlowCollection ocol = new TaskFlowCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }
        /// <summary>
        /// 我申請的清單
        /// </summary>
        /// <param name="execUser">員工Guid(EasyFlow)</param>
        /// <returns></returns>
        public TaskFlowCollection TaskFlowApplyGet(Guid applyUser)
        {
            //本身的待辦跟代理的待辦都要可以看到
            string sql = "SELECT * FROM " + TaskFlow.Schema.Provider.DelimitDbName(TaskFlow.Schema.TableName) + " With(nolock) " +
                        " WHERE " + TaskFlow.Columns.ApplyUser + " = @applyUser " +
                        " Order By " + TaskFlow.Columns.CreateTime + " Desc ";
            QueryCommand qc = new QueryCommand(sql, TaskFlow.Schema.Provider.Name);
            qc.AddParameter("@applyUser", applyUser, DbType.Guid);
            TaskFlowCollection ocol = new TaskFlowCollection();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }
        public bool TaskFlowSet(TaskFlow tf)
        {
            DB.Save<TaskFlow>(tf);
            return true;
        }
        #endregion TaskFlow


        #region TaskFlowContent
        public TaskFlowContent TaskFlowContentGetByTaskId(Guid taskId)
        {
            return DB.Get<TaskFlowContent>(TaskFlowContent.Columns.TaskId, taskId);
        }
        public TaskFlowContent TaskFlowContentRunningGet(Guid processId)
        {
            //本身的待辦跟代理的待辦都要可以看到
            string sql = "SELECT Top 1 * FROM " + TaskFlowContent.Schema.Provider.DelimitDbName(TaskFlowContent.Schema.TableName) + " With(nolock) " +
                        " WHERE " + TaskFlowContent.Columns.ProcessId + " = @processId " +
                        " And " + TaskFlow.Columns.EipTaskStatus + " = " + (int)EIPTaskStatus.Running ;
            QueryCommand qc = new QueryCommand(sql, TaskFlow.Schema.Provider.Name);
            qc.AddParameter("@processId", processId, DbType.Guid);
            TaskFlowContent ocol = new TaskFlowContent();
            ocol.LoadAndCloseReader(DataService.GetReader(qc));
            return ocol;
        }
        public bool TaskFlowContentSet(TaskFlowContent tfc)
        {
            DB.Save<TaskFlowContent>(tfc);
            return true;
        }
        #endregion TaskFlowContent
    }
}

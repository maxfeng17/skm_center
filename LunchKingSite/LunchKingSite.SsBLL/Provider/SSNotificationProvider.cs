﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.ModelCustom;
using LunchKingSite.DataOrm;
using SubSonic;

namespace LunchKingSite.SsBLL.Provider
{
    public class SSNotificationProvider : INotificationProvider
    {
        #region IdriverToken

        public int IdriverTokenSet(IdriverToken iDriverToken)
        {
            return DB.Save(iDriverToken);
        }

        public int IdriverTokenDeleteList(IdriverTokenCollection iDriverTokens)
        {
            return DB.Delete().From<IdriverToken>().Where(IdriverToken.Columns.Token).In(iDriverTokens.Select(x => x.Token)).Execute();
        }

        public IdriverToken IdriverTokenGetByTokenString(string tokenString)
        {
            return DB.Get<IdriverToken>(tokenString);
        }
        public IdriverToken IdriverTokenGetByTokenString(string tokenString, string deviceId)
        {
            return
                DB.Select().From(IdriverToken.Schema).Where(IdriverToken.Columns.Token).IsEqualTo(tokenString).And(
                    IdriverToken.Columns.DeviceId).IsEqualTo(deviceId).ExecuteSingle<IdriverToken>() ?? new IdriverToken();
        }

        public IdriverTokenCollection IdriverTokenGetListe(params string[] filter)
        {
            QueryCommand qc = GetSMCWhere(IdriverToken.Schema, filter);
            qc.CommandSql = "select * from " + IdriverToken.Schema.Provider.DelimitDbName(IdriverToken.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            IdriverTokenCollection col = new IdriverTokenCollection();
            col.LoadAndCloseReader(DataService.GetReader(qc));
            return col;
        }

        public IdriverTokenCollection IdriverTokenCollectionGetByCityId(int cityId)
        {
            return DB.SelectAllColumnsFrom<IdriverToken>().Where(IdriverToken.Columns.Enabled).IsEqualTo(SubscriptNotification.Enabled)
                .And(IdriverToken.Columns.DeviceTokenStatus).IsEqualTo((int)DeviceTokenStatus.Valid)
                .And(IdriverToken.Columns.CityId).IsEqualTo(cityId)
                .ExecuteAsCollection<IdriverTokenCollection>() ?? new IdriverTokenCollection();
        }


        #endregion 

        #region DeviceToken

        public int DeviceTokenSet(DeviceToken deviceToken)
        {
            try
            {
                return DB.Save(deviceToken);
            }
            catch (Exception)
            {
                return 0;
            }
        }

        public int DeviceTokenSaveAll(DeviceTokenCollection deviceTokenCols)
        {
            int result = DB.SaveAll(deviceTokenCols);
            return result;
        }


        public DeviceToken DeviceTokenGet(string device, MobileOsType type)
        {
            return DB.SelectAllColumnsFrom<DeviceToken>()
              .Where(DeviceToken.Columns.Device)
              .IsEqualTo(device)
              .And(DeviceToken.Columns.MobileOsType)
              .IsEqualTo((int)type).ExecuteSingle<DeviceToken>();
        }
        /// <summary>
        /// 依據裝置編號取得該裝置的推播資料
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        public DeviceTokenCollection DeviceTokenCollectionGetList(string device)
        {
            return DB.SelectAllColumnsFrom<DeviceToken>()
            .Where(DeviceToken.Columns.Device)
            .IsEqualTo(device).ExecuteAsCollection<DeviceTokenCollection>();

        }
        /// <summary>
        /// 依據PushToken取得所有裝置的推播資料
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public DeviceTokenCollection DeviceTokenCollectionGetListByToken(string token)
        {
            return DB.SelectAllColumnsFrom<DeviceToken>()
            .Where(DeviceToken.Columns.Token)
            .IsEqualTo(token).ExecuteAsCollection<DeviceTokenCollection>();
        }

        public DeviceToken DeviceTokenGet(string token)
        {
            return DB.SelectAllColumnsFrom<DeviceToken>()
             .Where(DeviceToken.Columns.Token)
             .IsEqualTo(token)
             .ExecuteSingle<DeviceToken>();
        }
        /// <summary>
        /// 取得推撥裝置列表
        /// </summary>
        /// <param name="filter">過濾條件</param>
        /// <returns></returns>
        public DeviceTokenCollection DeviceTokenCollectionGetList(params string[] filter)
        {
            //QueryCommand qc = GetSMCWhere(SubscriptionNotice.Schema, filter);
            DeviceTokenCollection col = new DeviceTokenCollection();

            if (filter == null || filter.Length == 0)
            {
                return col;
            }

            QueryCommand qc = new QueryCommand(" ", SubscriptionNotice.Schema.Provider.Name);

            string where = string.Join(" OR ", filter);


            string device_token = DeviceToken.Schema.Provider.DelimitDbName(DeviceToken.Schema.TableName);
            string subscription_notice = SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Schema.TableName);

            string device_token_id = string.Format("{0}.{1}", device_token, DeviceToken.Schema.Provider.DelimitDbName(DeviceToken.Columns.Id));
            string device_token_token_status = string.Format("{0}.{1}", device_token, DeviceToken.Schema.Provider.DelimitDbName(DeviceToken.Columns.TokenStatus));

            string subscription_notice_device_id = string.Format("{0}.{1}", subscription_notice, SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Columns.DeviceId));
            string subscription_notice_enabled = string.Format("{0}.{1}", subscription_notice, SubscriptionNotice.Schema.Provider.DelimitDbName(SubscriptionNotice.Columns.Enabled));


            qc.CommandSql = "SELECT DISTINCT " + device_token + ".* FROM " + device_token + " WITH(NOLOCK) " +
                " INNER JOIN " + subscription_notice + " WITH(NOLOCK)  ON " + device_token_id + " = " + subscription_notice_device_id + " WHERE (" +
                where + ") AND (" + subscription_notice_enabled + " = 1) AND (" + device_token_token_status + " = 1)";
            col.LoadAndCloseReader(DataService.GetReader(qc));

            return col;
        }

        #endregion DeviceToken

        #region PushApp
        public void PushAppSet(PushApp pushApp)
        {
            DB.Save(pushApp);
        }

        public void DeletePushApp(int id)
        {
            DB.Delete<PushApp>(PushApp.Columns.Id, id);
        }

        public PushApp GetPushApp(int id)
        {
            return DB.Get<PushApp>(PushApp.Columns.Id, id);
        }


        public PushAppCollection GetPushAppListByPeriod(DateTime pushTimeStart, DateTime pushTimeEnd)
        {
            return DB.SelectAllColumnsFrom<PushApp>().Where(PushApp.Columns.PushDate).IsBetweenAnd(pushTimeStart, pushTimeEnd)
                .And(PushApp.Columns.RealPushTime).IsNull()
                .OrderDesc(PushApp.Columns.Id).ExecuteAsCollection<PushAppCollection>();
        }

        public PushAppCollection GetPushAppList(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = PushApp.Columns.PushDate + " desc";
            QueryCommand qc = GetSMCWhere(PushApp.Schema, filter);
            qc.CommandSql = "select * from " + PushApp.Schema.Provider.DelimitDbName(PushApp.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            PushAppCollection vfcCol = new PushAppCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }

        public int GetPushAppListCount(params string[] filter)
        {
            QueryCommand qc = GetSMCWhere(PushApp.Schema, filter);
            qc.CommandSql = "select count(1) from " + PushApp.Schema.Provider.DelimitDbName(PushApp.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        /// <summary>
        /// 增加IOS推播點擊數字
        /// </summary>
        /// <param name="id">推播編號</param>
        /// <param name="addCount">增加的數字</param>
        /// <returns></returns>
        public int PushAppIOSViewCountAdd(int id, int addCount)
        {
            return DB.Update<PushApp>().SetExpression(PushApp.Columns.IosViewCount).EqualTo(
                PushApp.Columns.IosViewCount + "+" + addCount).Where(PushApp.IdColumn).IsEqualTo(id).Execute();
        }
        /// <summary>
        /// 增加Android推播點擊數字
        /// </summary>
        /// <param name="id">推播數字</param>
        /// <param name="addCount">增加的數字</param>
        /// <returns></returns>
        public int PushAppAndroidViewCountAdd(int id, int addCount)
        {
            return DB.Update<PushApp>().SetExpression(PushApp.Columns.AndriodViewCount).EqualTo(
                PushApp.Columns.AndriodViewCount + "+" + addCount).Where(PushApp.IdColumn).IsEqualTo(id).Execute();
        }

        protected QueryCommand GetSMCWhere(TableSchema.Table schema, params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }

            return qc;
        }
        #endregion

        #region PcpAssignment

        public PcpAssignmentCollection PcpAssignmentGetList(int userId, DateTime begin, DateTime end)
        {
            return DB.SelectAllColumnsFrom<PcpAssignment>()
                .Where(PcpAssignment.Columns.UserId).IsEqualTo(userId)
                .And(PcpAssignment.Columns.CreateTime).IsBetweenAnd(begin, end)
                .ExecuteAsCollection<PcpAssignmentCollection>();
        }

        /// <summary>
        /// 取得特定狀態的PcpAssignment
        /// </summary>
        /// <param name="executionTime"></param>
        /// <param name="status"></param>
        /// <param name="sendTypes"></param>
        /// <returns></returns>
        public PcpAssignmentCollection PcpAssignmentGet(DateTime executionTime, AssignmentStatus status, params AssignmentSendType[] sendTypes)
        {
            return DB.SelectAllColumnsFrom<PcpAssignment>()
                .Where(PcpAssignment.Columns.Status).IsEqualTo((int)status)
                .And(PcpAssignment.Columns.SendType).In(sendTypes)
                .And(PcpAssignment.Columns.ExecutionTime).IsLessThanOrEqualTo(executionTime)
                .OrderAsc(PcpAssignment.Columns.Id)
                .ExecuteAsCollection<PcpAssignmentCollection>();
        }

        /// <summary>
        /// 取得特定id的PcpAssignment
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PcpAssignment PcpAssignmentGet(int id)
        {
            return DB.Get<PcpAssignment>(PcpAssignment.Columns.Id, id);
        }

        /// <summary>
        /// 儲存PcpAssignment
        /// </summary>
        /// <param name="pcp"></param>
        public void PcpAssignmentSet(PcpAssignment pcp)
        {
            DB.Save(pcp);
        }

        /// <summary>
        /// 設定特定id的PcpAssignment的狀態
        /// </summary>
        /// <param name="pcpa"></param>
        /// <param name="status"></param>
        public void PcpAssignmentSetStatus(PcpAssignment pcpa, AssignmentStatus status)
        {
            if (!pcpa.IsLoaded)
            {
                return;
            }

            //起始執行，寫入開始時間
            if (status == AssignmentStatus.Executing)
            {
                pcpa.StartTime = DateTime.Now;
                pcpa.Status = (int)status;
            }
            else if (status == AssignmentStatus.Complete) //結束執行，寫入完成時間
            {
                pcpa.CompleteTime = DateTime.Now;
                pcpa.Status = (int)status;
            }
            else //其他
            {
                pcpa.Status = (int)status;
            }
            DB.Save(pcpa);

        }

        /// <summary>
        /// 設定特定id的PcpAssignment的狀態
        /// </summary>
        /// <param name="id"></param>
        /// <param name="status"></param>
        public void PcpAssignmentSetStatus(int id, AssignmentStatus status)
        {
            PcpAssignment pcpa = PcpAssignmentGet(id);
            PcpAssignmentSetStatus(pcpa, status);
        }

        public PcpAssignmentMember PcpAssignmentMemberGet(long id)
        {
            return DB.Get<PcpAssignmentMember>(PcpAssignmentMember.Columns.Id, id);
        }

        /// <summary>
        /// 儲存 pcp_assignment_member
        /// </summary>
        /// <param name="assignmentMember"></param>
        public void PcpAssignmentMemberSet(PcpAssignmentMember assignmentMember)
        {
            DB.Save(assignmentMember);
        }

        /// <summary>
        /// 記錄推撥發送的時間
        /// </summary>
        /// <param name="id"></param>
        /// <param name="sendTime"></param>
        public void PcpAssignmentMemberSetSendTime(long id, DateTime sendTime)
        {
            PcpAssignmentMember m = PcpAssignmentMemberGet(id);

            if (m != null && m.IsLoaded)
            {
                m.SendTime = sendTime;
                DB.Save(m);
            }
        }

        public ViewPcpAssignmentMemberDeviceTokenCollection ViewPcpAssignmentMemberDeviceTokenGetList(int assignmentId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpAssignmentMemberDeviceToken>()
                .Where(ViewPcpAssignmentMemberDeviceToken.Columns.AssignmentId).IsEqualTo(assignmentId)
                .And(ViewPcpAssignmentMemberDeviceToken.Columns.TokenStatus).IsEqualTo((int)DeviceTokenStatus.Valid)
                .OrderAsc(ViewPcpAssignmentMemberDeviceToken.Columns.Id)
                .ExecuteAsCollection<ViewPcpAssignmentMemberDeviceTokenCollection>();
        }
        #endregion PcpAssignment

        #region PcpAssignmentMember
        public List<PcpAssignmentMember> PcpAssignmentMemberGetList(int assignmentId)
        {
            return DB.QueryOver<PcpAssignmentMember>().Where(t => t.AssignmentId == assignmentId).ToList();
        }
        #endregion


        #region PcpAssignmentFilter

        public PcpAssignmentFilterCollection PcpAssignmentFilterGetList(int pcpAssignmentId)
        {
            return DB.SelectAllColumnsFrom<PcpAssignmentFilter>().Where(PcpAssignmentFilter.Columns.AssignmentId)
                .IsEqualTo(pcpAssignmentId).ExecuteAsCollection<PcpAssignmentFilterCollection>();
        }

        public void PcpAssignmentFilterSet(PcpAssignmentFilter filter)
        {
            DB.Save(filter);
        }

        #endregion PcpAssignmentFilter

        #region DevicePushRecord  (Notification Message to DevicePushRecord)

        public void NotificationToDevicePushRecordSet(int actionEventPushMessageId, params string[] filter)
        {
            if (filter == null || filter.Length == 0)
            {
                return;
            }

            QueryCommand qc = new QueryCommand(" ", SubscriptionNotice.Schema.Provider.Name);

            qc.CommandTimeout = 300;


            string where = string.Join(" OR ", filter);

            qc.CommandSql = string.Format(
                @"
insert into device_push_record (device_id,identifier_type,action_id,create_time) select id,1,{0},getdate() 
from device_identyfier_info 
where identifier_code in (
SELECT DISTINCT device_token.device FROM device_token WITH(NOLOCK) 
INNER JOIN subscription_notice WITH(NOLOCK)  ON device_token.id = subscription_notice.device_id WHERE 
({1}) AND (subscription_notice.enabled = 1) AND (device_token.token_status= 1))",
                actionEventPushMessageId, @where);



            DataService.ExecuteScalar(qc);


        }

        public void NotificationToDevicePushRecordSet(int actionEventPushMessageId, 
            DeviceTokenCollection deviceTokenCollection, 
            out List<Tuple<int, string>> androidTokens, 
            out List<Tuple<int, string>> iOSTokens)
        {
            androidTokens = new List<Tuple<int, string>>();
            iOSTokens = new List<Tuple<int, string>>();
            DateTime now = DateTime.Now;
            foreach (var dt in deviceTokenCollection)
            {
                var deviceInfo = GetUserDeviceInfoModel(dt.Token);
                if (deviceInfo == null)
                {
                    continue;
                }
                DevicePushRecord dpr = new DevicePushRecord
                {
                    ActionId = actionEventPushMessageId,
                    CreateTime = now,
                    IdentifierType = 1,
                    DeviceId = deviceInfo.DeviceId
                };
                DB.Save(dpr);
                if (dt.MobileOsType == (int)MobileOsType.iOS)
                {
                    iOSTokens.Add(new Tuple<int, string>(dpr.Id, dt.Token));
                }
                else if (dt.MobileOsType == (int)MobileOsType.Android)
                {
                    androidTokens.Add(new Tuple<int, string>(dpr.Id, dt.Token));
                }
            }
        }

        public List<Tuple<int, string>> DevicePushRecordMessageTokenGetDict(int actionEventPushMessageId, MobileOsType osType)
        {
            QueryCommand qc = new QueryCommand(" ", SubscriptionNotice.Schema.Provider.Name);
            qc.CommandTimeout = 300;

            qc.CommandSql = string.Format(
                @"
select dp.id as message_id, dt.token as device from device_push_record dp with(nolock)
inner join device_identyfier_info dii with(nolock) on dii.id = dp.device_id
inner join device_token dt with(nolock) on dt.device = dii.identifier_code
where 
dp.action_id = {0} and dt.mobile_os_type = {1}",
                actionEventPushMessageId, (int)osType);

            //避免重複推送
            HashSet<string> tokens = new HashSet<string>();

            List<Tuple<int, string>> result = new List<Tuple<int, string>>();
            using (var dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    int messageId = dr.GetInt32(0);
                    string device = dr.GetString(1);

                    if (tokens.Contains(device) == false)
                    {
                        tokens.Add(device);
                        result.Add(new Tuple<int, string>(messageId, device));
                    }
                }
            }
            return result;
        }

        public List<UserDeviceInfoModel> GetUserDeviceInfoModels(int userId)
        {
            if (userId == 0)
            {
                throw new ArgumentException("userId 不能為 0");
            }
            string sql = @"
select dt.token, dii.id as device_id, dii.model, dii.os_ver, dii.app_ver, ISNULL(dt.modify_time, dt.create_time) as modify_time, dt.mobile_os_type
from device_identyfier_info dii with(nolock)
inner join device_token dt with(nolock) on dt.device = dii.identifier_code
where dii.member_unique_id = @userId and dt.token_status = 1
";
            QueryCommand qc = new QueryCommand(sql, DeviceIdentyfierInfo.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, System.Data.DbType.Int32);
            List<UserDeviceInfoModel> result = new List<UserDeviceInfoModel>();
            using (var dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    UserDeviceInfoModel item = new UserDeviceInfoModel
                    {
                        Token = dr.GetString(0),

                    };
                    item.ShortToken = item.Token.Substring(0, 4) + "...." + item.Token.Substring(item.Token.Length - 2);
                    item.DeviceId = dr.GetInt32(1);
                    item.Model = dr.GetString(2);
                    item.OsVer = dr.GetString(3);
                    item.AppVer = dr.GetString(4);
                    item.LastUsedTime = dr.GetDateTime(5);
                    item.OsType = (MobileOsType)dr.GetInt32(6);
                    result.Add(item);
                }
            }

            return result;
        }

        public UserDeviceInfoModel GetUserDeviceInfoModel(string token)
        {
            string sql = @"
select dt.token, dii.id as device_id, dii.model, dii.os_ver, dii.app_ver, dii.modify_time, dt.mobile_os_type
from device_identyfier_info dii with(nolock)
inner join device_token dt with(nolock) on dt.device = dii.identifier_code
where dt.token=@token and dt.token_status = 1
";
            QueryCommand qc = new QueryCommand(sql, DeviceIdentyfierInfo.Schema.Provider.Name);
            qc.AddParameter("@token", token, System.Data.DbType.String);
            UserDeviceInfoModel result = null;
            using (var dr = DataService.GetReader(qc))
            {
                while (dr.Read())
                {
                    result = new UserDeviceInfoModel
                    {
                        Token = dr.GetString(0)
                    };
                    result.ShortToken = result.Token.Substring(0, 4) + "...." + result.Token.Substring(result.Token.Length - 2);
                    result.DeviceId = dr.GetInt32(1);
                    result.Model = dr.GetString(2);
                    result.OsVer = dr.GetString(3);
                    result.AppVer = dr.GetString(4);
                    result.LastUsedTime = dr.GetDateTime(5);
                    result.OsType = (MobileOsType)dr.GetInt32(6);                    
                }
            }

            return result;
        }

        #endregion DevicePushRecord  (Notification Message to DevicePushRecord)

    }
}

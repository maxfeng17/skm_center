﻿using System.Data;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using SubSonic;
using System;
using System.Linq;
using LunchKingSite.Core.Enumeration;
using System.Reflection;
using System.Collections.Generic;

namespace LunchKingSite.SsBLL.Provider
{
    public class SSServiceProvider : LunchKingSite.Core.IServiceProvider
    {

        #region customer_service_message
        public CustomerServiceMessage CustomerServiceMessageGet(string serviceNo)
        {
            return DB.Get<CustomerServiceMessage>(serviceNo);
        }
        public CustomerServiceMessageCollection CustomerServiceMessageByStatusGet(int status)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceMessage>()
                .Where(CustomerServiceMessage.Columns.Status).IsEqualTo(status)
                .And(CustomerServiceMessage.Columns.OrderGuid).IsNotNull()
                .ExecuteAsCollection<CustomerServiceMessageCollection>();
        }

        public void CustomerServiceMessageSet(CustomerServiceMessage entity)
        {
            DB.Save<CustomerServiceMessage>(entity);
        }
        public void CustomerServiceMessageDelete(CustomerServiceMessage entity)
        {
            DB.Delete<CustomerServiceMessage>(entity);
        }

        public int CustomerServiceMessageGetCountForFilter(params string[] filter)
        {
            QueryCommand qc = new QueryCommand(" ", CustomerServiceMessage.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(CustomerServiceMessage.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select count(1) from " + CustomerServiceMessage.Schema.Provider.DelimitDbName(CustomerServiceMessage.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public CustomerServiceMessageCollection CustomerServiceMessageGetListForFilter(int pageStart, int pageLength, string orderBy, params string[] filter)
        {
            string defOrderBy = CustomerServiceMessage.Columns.CreateTime + " desc";
            QueryCommand qc = new QueryCommand(" ", CustomerServiceMessage.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(CustomerServiceMessage.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrderForFilter(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            qc.CommandSql = "select * from " + CustomerServiceMessage.Schema.Provider.DelimitDbName(CustomerServiceMessage.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy;
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            CustomerServiceMessageCollection vfcCol = new CustomerServiceMessageCollection();
            vfcCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vfcCol;
        }
        
        public CustomerServiceMessageCollection GetCustomerMessageByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceMessage>().Where(CustomerServiceMessage.Columns.UserId).IsEqualTo(userId)
                                                             .OrderDesc(CustomerServiceMessage.Columns.ModifyTime)
                                                             .ExecuteAsCollection<CustomerServiceMessageCollection>();
        }
        
        /// <summary>
        /// 依訂單編號取得未結案案件
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public CustomerServiceMessage GetCustomerMessageByOrderGuidNotCompelete(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceMessage>()
                .Where(CustomerServiceMessage.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(CustomerServiceMessage.Columns.Status).IsNotEqualTo((int)statusConvert.complete)
                .OrderDesc(CustomerServiceMessage.Columns.ModifyTime)
                .ExecuteSingle<CustomerServiceMessage>();
        }

        /// <summary>
        /// 取得訂單最後一筆案件
        /// </summary>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public CustomerServiceMessage GetLastCustomerMessageByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceMessage>().Top("1")
                .Where(CustomerServiceMessage.Columns.OrderGuid).IsEqualTo(orderGuid)
                .OrderDesc(CustomerServiceMessage.Columns.CreateTime)
                .ExecuteSingle<CustomerServiceMessage>();
        }

        public ViewMergeOldNewServiceMessageCollection ViewMergeOldNewServiceMessageCollectionGet(int userId, int pageStart, int pageLength)
        {
            string sql =
                @"SELECT * FROM (
                SELECT 1 as cs_version, id as sm_id, '' as service_no, category, sub_category, message_type as sm_message_type, '' as csm_message_type, 
                    [message], phone, name, email, sm.status as sm_status, null as csm_status, sm.create_id, sm.create_time, 
                    m.last_name + m.first_name as modify_name, sm.modify_time, sm.order_id, o.GUID order_guid, sm.type as sm_type
	              FROM service_message sm WITH(NOLOCK) 
		            LEFT JOIN [order] o on o.order_id = sm.order_id
		            LEFT JOIN member m on sm.modify_id = m.user_name
	              WHERE sm.user_id = @userId
	              UNION ALL
                  SELECT 2 as cs_version, null as sm_id, service_no,category, sub_category, null as sm_message_type, message_type as csm_message_type,
		            null as [message], phone, name, email, '' as sm_status, csm.status as csm_status, csm.create_id, csm.create_time, 
		            m.last_name + m.first_name as modify_name, csm.modify_time, o.order_id, csm.order_guid, 0 as sm_typem
	              FROM customer_service_message csm WITH(NOLOCK) 
		            LEFT JOIN [order] o on o.GUID = csm.order_guid
		            LEFT JOIN member m on csm.modify_id = m.unique_id
	              WHERE csm.user_id = @userId ) as aa";

            QueryCommand qc = new QueryCommand(sql, Coupon.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            if (pageLength > 0)
            {
                string defOrderBy = ViewMergeOldNewServiceMessage.Columns.CreateTime + " desc";
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy);
            }

            ViewMergeOldNewServiceMessageCollection smCol = new ViewMergeOldNewServiceMessageCollection();
            smCol.LoadAndCloseReader(DataService.GetReader(qc));
            return smCol;
        }

        public int ViewMergeOldNewServiceMessageGetCount(int userId)
        {
            string sql =
                @"SELECT count(1) cnt FROM (
                SELECT 1 as cs_version, id as sm_id, '' as service_no, category, sub_category, message_type as sm_message_type, '' as csm_message_type, 
                    [message], phone, name, email, sm.status as sm_status, null as csm_status, sm.create_id, sm.create_time, 
                    m.last_name + m.first_name as modify_name, sm.modify_time, sm.order_id, o.GUID order_guid, sm.type as sm_type
	              FROM service_message sm WITH(NOLOCK) 
		            LEFT JOIN [order] o on o.order_id = sm.order_id
		            LEFT JOIN member m on sm.modify_id = m.user_name
	              WHERE sm.user_id = @userId
	              UNION ALL
                  SELECT 2 as cs_version, null as sm_id, service_no,category, sub_category, null as sm_message_type, message_type as csm_message_type,
		            null as [message], phone, name, email, '' as sm_status, csm.status as csm_status, csm.create_id, csm.create_time, 
		            m.last_name + m.first_name as modify_name, csm.modify_time, o.order_id, csm.order_guid, 0 as sm_typem
	              FROM customer_service_message csm WITH(NOLOCK) 
		            LEFT JOIN [order] o on o.GUID = csm.order_guid
		            LEFT JOIN member m on csm.modify_id = m.unique_id
	              WHERE csm.user_id = @userId ) as aa";

            QueryCommand qc = new QueryCommand(sql, Coupon.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            return (int)DataService.ExecuteScalar(qc);
        }
        #endregion

        #region customer_service_category
        public CustomerServiceCategory CustomerServiceCategoryGet(int category_id)
        {
            return DB.Get<CustomerServiceCategory>(category_id);
        }
        public void CustomerServiceCategorySet(CustomerServiceCategory entity)
        {
            DB.Save<CustomerServiceCategory>(entity);
        }
        public void CustomerServiceCategoryDelete(CustomerServiceCategory entity)
        {
            DB.Delete<CustomerServiceCategory>(entity);
        }
        public void CustomerServiceCategoryDeleteByCategoryId(int categoryId)
        {
            DB.Destroy<CustomerServiceCategory>(CustomerServiceCategory.Columns.CategoryId, categoryId);
        }
        public int GetCategoryListCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<CustomerServiceCategory>(filter);
            qc.CommandSql = "select count(1) from " + CustomerServiceCategory.Schema.Provider.DelimitDbName(CustomerServiceCategory.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }
        public CustomerServiceCategoryCollection GetCategoryListByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CustomerServiceCategory, CustomerServiceCategoryCollection>(page, pageSize, orderBy, false, filter);
        }
        public CustomerServiceCategoryCollection GetCustomerServiceCategory()
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>()
                   .And(CustomerServiceCategory.Columns.ParentCategoryId).IsNull()
                   .ExecuteAsCollection<CustomerServiceCategoryCollection>();
        }

        public CustomerServiceCategoryCollection GetCustomerServiceCategoryForFrontEnd()
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>().Where(CustomerServiceCategory.Columns.IsShowFrontEnd).IsEqualTo(1)
                   .And(CustomerServiceCategory.Columns.ParentCategoryId).IsNull()
                   .ExecuteAsCollection<CustomerServiceCategoryCollection>();
        }

        public string GetCustomerNameByCategoryId(int categoryId)
        {
            return DB.Get<CustomerServiceCategory>(CustomerServiceCategory.Columns.CategoryId, categoryId).CategoryName;
        }
        public string GetCustomerNameByCategoryIdMain(int categoryId)
        {
            var maincategoryId = DB.Get<CustomerServiceCategory>(CustomerServiceCategory.Columns.CategoryId, categoryId).ParentCategoryId;
            return DB.Get<CustomerServiceCategory>(CustomerServiceCategory.Columns.CategoryId, maincategoryId).CategoryName;
        }
        public CustomerServiceCategoryCollection GetCustomerServiceCategoryListByCategoryId(int? categoryId)
        {
            return categoryId == null || categoryId == 0
                ? DB.SelectAllColumnsFrom<CustomerServiceCategory>()
                    .Where(CustomerServiceCategory.Columns.ParentCategoryId).IsNull()
                    .OrderAsc(CustomerServiceCategory.Columns.CategoryOrder)
                    .ExecuteAsCollection<CustomerServiceCategoryCollection>()
                : DB.SelectAllColumnsFrom<CustomerServiceCategory>()
                    .Where(CustomerServiceCategory.Columns.ParentCategoryId).IsEqualTo(categoryId)
                    .OrderAsc(CustomerServiceCategory.Columns.CategoryOrder)
                    .ExecuteAsCollection<CustomerServiceCategoryCollection>();
        }

        public CustomerServiceCategoryCollection GetCategoryListByParentId(int parentId)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>()
                .Where(CustomerServiceCategory.Columns.ParentCategoryId).IsEqualTo(parentId)
                .OrderAsc(CustomerServiceCategory.Columns.CategoryOrder)
                .ExecuteAsCollection<CustomerServiceCategoryCollection>();
        }

        public CustomerServiceCategory GetCategoryByParentIdAndName(int parentId, string categoryname)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>().Where(CustomerServiceCategory.Columns.ParentCategoryId).IsEqualTo(parentId)
                   .And(CustomerServiceCategory.Columns.CategoryName).IsEqualTo(categoryname)
                   .ExecuteSingle<CustomerServiceCategory>();
        }

        public int GetLastCategoryId()
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>().OrderDesc(CustomerServiceCategory.Columns.CategoryId)
                .ExecuteSingle<CustomerServiceCategory>().CategoryId;
        }
        public int GetLastOrderId()
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategory>().Where(CustomerServiceCategory.Columns.ParentCategoryId).IsNull().OrderDesc(CustomerServiceCategory.Columns.CategoryId)
                 .ExecuteSingle<CustomerServiceCategory>().CategoryOrder;
        }
        #endregion

        #region customer_service_category_sample
        public CustomerServiceCategorySample CustomerServiceCategorySampleGet(int id)
        {
            return DB.Get<CustomerServiceCategorySample>(id);
        }
        public void CustomerServiceCategorySampleSet(CustomerServiceCategorySample entity)
        {
            DB.Save<CustomerServiceCategorySample>(entity);
        }
        public void CustomerServiceCategorySampleDelete(CustomerServiceCategorySample entity)
        {
            DB.Delete<CustomerServiceCategorySample>(entity);
        }
        public void CustomerServiceCategorySampleDeleteByCategoryId(int id)
        {
            DB.Destroy<CustomerServiceCategorySample>(CustomerServiceCategorySample.Columns.Id, id);
        }
        public int GetSampleListCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<CustomerServiceCategorySample>(filter);
            qc.CommandSql = "select count(1) from " + CustomerServiceCategorySample.Schema.Provider.DelimitDbName(CustomerServiceCategorySample.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }
        public CustomerServiceCategorySampleCollection CustomerServiceCategorySampleGetByCategoryId(int categoryId)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceCategorySample>().Where(CustomerServiceCategorySample.Columns.CategoryId).IsEqualTo(categoryId)
                                                                           .ExecuteAsCollection<CustomerServiceCategorySampleCollection>();
        }


        public CustomerServiceCategorySampleCollection GetSampleListByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<CustomerServiceCategorySample, CustomerServiceCategorySampleCollection>(page, pageSize, orderBy, false, filter);
        }



        #endregion

        #region customer_service_inside_log
        public CustomerServiceInsideLog CustomerServiceInsideLogGet(int id)
        {
            return DB.Get<CustomerServiceInsideLog>(id);
        }
        public CustomerServiceInsideLogCollection CustomerServiceInsideLogGet(string serviceNo)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceInsideLog>()
                .Where(CustomerServiceInsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                .ExecuteAsCollection<CustomerServiceInsideLogCollection>();
        }
        public int CustomerServiceInsideLogSet(CustomerServiceInsideLog entity)
        {
            return DB.Save<CustomerServiceInsideLog>(entity);
        }

        public int CustomerServiceInsideLogCollectionSet(CustomerServiceInsideLogCollection entities)
        {
            return DB.SaveAll(entities);
        }

        public void CustomerServiceInsideLogDelete(CustomerServiceInsideLog entity)
        {
            DB.Delete<CustomerServiceInsideLog>(entity);
        }
        public int CustomerServiceInsideLogByServiceNoAndStatus(string serviceNo, int status, DateTime? time)
        {
            if (time != null)
            {
                return DB.SelectAllColumnsFrom<CustomerServiceInsideLog>()
                    .Where(CustomerServiceInsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                    .And(CustomerServiceInsideLog.Columns.Status).IsEqualTo(status)
                    .And(CustomerServiceInsideLog.Columns.IsRead).IsEqualTo(0)
                    .And(CustomerServiceInsideLog.Columns.CreateTime).IsLessThanOrEqualTo(time)
                    .ExecuteAsCollection<CustomerServiceInsideLogCollection>().Count();
            }
            else
            {
                return DB.SelectAllColumnsFrom<CustomerServiceInsideLog>()
                    .Where(CustomerServiceInsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                    .And(CustomerServiceInsideLog.Columns.Status).IsEqualTo(status)
                    .And(CustomerServiceInsideLog.Columns.IsRead).IsEqualTo(0)
                    .ExecuteAsCollection<CustomerServiceInsideLogCollection>().Count();
            }
        }

        public int CustomerServiceInsideLogByStatus(int status)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceInsideLog>()
            .Where(CustomerServiceInsideLog.Columns.Status).IsEqualTo(status)
            .And(CustomerServiceInsideLog.Columns.IsRead).IsEqualTo(0)
            .ExecuteAsCollection<CustomerServiceInsideLogCollection>().Count();
        }

        public int GetCustomerServiceInsideLogCount(string sellerGuids)
        {
            string sql = @"
                        Select Count(Service_No) From Customer_Service_Inside_Log with(nolock)
                        Where Service_No in (
                            Select Service_No From View_Customer_Service_List with(nolock) ";
            sql += string.Format(" where {0} in ({1}) and {2}!={3}", ViewCustomerServiceList.Columns.SellerGuid, sellerGuids, ViewCustomerServiceList.Columns.CustomerServiceStatus, (int)statusConvert.complete);
            sql+= ")  and status=" + (int)statusConvert.transfer + " Group by Service_No ";

            CustomerServiceInsideLogCollection csi = new CustomerServiceInsideLogCollection();
            
            QueryCommand qc2 = new QueryCommand(sql, CustomerServiceInsideLog.Schema.Provider.Name);
            csi.LoadAndCloseReader(DataService.GetReader(qc2));
            int Count = csi.Count();
            return Count;
        }

        /// <summary>
        /// 轉單24小時已讀未回清單
        /// </summary>
        /// <returns></returns>
        public List<string> GetCustomerServiceInsideLogServiceNoListBy24HNoReply(DateTime startTime, DateTime endTime)
        {
            List<string> serviceNoList = new List<string>();
            var sql = @"SELECT distinct service_no FROM customer_service_inside_log isRead with(nolock)	
		                WHERE isRead.status = @Transfer  
		                AND isRead.is_read = 1 
		                AND isRead.read_time >= @StartTime 
                        AND isRead.read_time <= @EndTime
		                AND service_no NOT IN (
							SELECT service_no from customer_service_message msg with(nolock)
								WHERE msg.service_no = isRead.service_no
									and msg.status = @Complete
						)
		                AND service_no NOT IN (
			                SELECT service_no from customer_service_inside_log isNotBack with(nolock) 								
			                WHERE isNotBack.service_no = isRead.service_no 
				                and (isNotBack.status = @Transferback 
								and isNotBack.create_time > isRead.create_time) 
		                )"; //轉單後24小已讀未回 (排除已結案、排除廠商有回覆)
            QueryCommand qc = new QueryCommand(sql, ViewCouponListMain.Schema.Provider.Name);
            qc.AddParameter("@Transfer", (int)statusConvert.transfer, DbType.Int32);
            qc.AddParameter("@Transferback", (int)statusConvert.transferback, DbType.Int32);
            qc.AddParameter("@Complete", (int)statusConvert.complete, DbType.Int32);
            qc.AddParameter("@StartTime", startTime, DbType.DateTime);
            qc.AddParameter("@EndTime", endTime, DbType.DateTime);

            using (IDataReader reader = DataService.GetReader(qc))
            {
                while (reader.Read())
                {
                    serviceNoList.Add(reader.GetString(0));
                }
            }
            return serviceNoList;
        }

        #endregion

        #region customer_service_outside_log
        public CustomerServiceOutsideLog CustomerServiceOutsideLogGet(int id)
        {
            return DB.Get<CustomerServiceOutsideLog>(id);
        }
        public void CustomerServiceOutsideLogSet(CustomerServiceOutsideLog entity)
        {
            DB.Save<CustomerServiceOutsideLog>(entity);
        }
        public void CustomerServiceOutsideLogDelete(CustomerServiceOutsideLog entity)
        {
            DB.Delete<CustomerServiceOutsideLog>(entity);
        }

        public int GetCustomerServiceOutsideLogCountByuserId(int userId)
        {
            return DB.Select().From<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.Method).IsNotEqualTo(0)
                .And(ViewCustomerServiceOutsideLog.Columns.IsRead).IsEqualTo(0)
                .GetRecordCount();
        }

        /// <summary>
        /// 會員個人-未讀訊息數
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserId(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.Method).IsNotEqualTo(0)  //客服回覆
                .And(ViewCustomerServiceOutsideLog.Columns.IsRead).IsEqualTo(0)     //未讀
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }
        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogParentServiceNo(string parentServiceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.ParentServiceNo).IsEqualTo(parentServiceNo)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }

        public CustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogBySerivceNo(string serviceNo)
        {
            return DB.SelectAllColumnsFrom<CustomerServiceOutsideLog>().NoLock()
                .Where(CustomerServiceOutsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                .OrderDesc(CustomerServiceOutsideLog.Columns.CreateTime)
                .ExecuteAsCollection<CustomerServiceOutsideLogCollection>();
        }
        
        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserIdAndSerivceNo(string userId, string serviceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                .OrderDesc(ViewCustomerServiceOutsideLog.Columns.CreateTime)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }
        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByuserIdAndParentServiceNo(int userId, string parentServiceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.ParentServiceNo).IsEqualTo(parentServiceNo)
                .OrderDesc(ViewCustomerServiceOutsideLog.Columns.CreateTime)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }

        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByTsMemberNoAndParentServiceNo(string tsMemberNo, string parentServiceNo, int issueFromType)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.TsMemberNo).IsEqualTo(tsMemberNo)
                .And(ViewCustomerServiceOutsideLog.Columns.ParentServiceNo).IsEqualTo(parentServiceNo)
                .And(ViewCustomerServiceOutsideLog.Columns.IssueFromType).IsEqualTo(issueFromType)
                .OrderDesc(ViewCustomerServiceOutsideLog.Columns.CreateTime)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }

        /// <summary>
        /// 會員訂單-未讀訊息數
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="orderGuid"></param>
        /// <returns></returns>
        public int GetCustomerServiceOutsideLogCountByorderGuid(int userId, Guid orderGuid)
        {
            return DB.Select().From<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.OrderGuid).IsEqualTo(orderGuid) //By訂單
                .And(ViewCustomerServiceOutsideLog.Columns.Method).IsEqualTo(1) //客服回覆
                .And(ViewCustomerServiceOutsideLog.Columns.IsRead).IsEqualTo(0) //未讀
                .GetRecordCount();
        }

        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByorderGuid(int userId, Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceOutsideLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }

        public ViewCustomerServiceOutsideLogCollection GetCustomerServiceOutsideLogByTsMemberNoAndOrderGuid(string tsMemberNo, Guid orderGuid, int issueFromType)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().NoLock()
                .Where(ViewCustomerServiceOutsideLog.Columns.TsMemberNo).IsEqualTo(tsMemberNo)
                .And(ViewCustomerServiceOutsideLog.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(ViewCustomerServiceOutsideLog.Columns.IssueFromType).IsEqualTo(issueFromType)
                .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }

        #endregion

        #region view_customer_service_inside_log
        public ViewCustomerServiceInsideLogCollection GetInsideLogByServiceNo(string serviceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceInsideLog>().Where(ViewCustomerServiceInsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                                                               .OrderAsc(ViewCustomerServiceInsideLog.Columns.CreateTime)
                                                               .ExecuteAsCollection<ViewCustomerServiceInsideLogCollection>();
        }
        #endregion

        #region view_customer_service_outside_log
        public ViewCustomerServiceOutsideLogCollection GetOutsideLogByServiceNo(string serviceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceOutsideLog>().Where(ViewCustomerServiceOutsideLog.Columns.ServiceNo).IsEqualTo(serviceNo)
                                                               .OrderAsc(ViewCustomerServiceOutsideLog.Columns.CreateTime)
                                                               .ExecuteAsCollection<ViewCustomerServiceOutsideLogCollection>();
        }
        #endregion

        #region view_customer_service_list
        public ViewCustomerServiceListCollection GetViewCustomerServiceListByVbs(int pageStart, int pageLength, string orderBy, string sellerGuids, params string[] filter)
        {
            string defOrderBy = ViewCustomerServiceList.Columns.ServiceNo;
            QueryCommand qc = GetVcslWhereQC(sellerGuids, filter);
            qc.CommandSql = "select view_customer_service_list.* from " + ViewCustomerServiceList.Schema.Provider.DelimitDbName(ViewCustomerServiceList.Schema.TableName) + " with(nolock) " +
                @" inner join (select service_no service_no_from_inside, 
			                         sum(case [status] when 2 then 1 else 0 end) transfer_count, 
			                         sum(case [status] when 3 then 1 else 0 end) transfer_back_count
                                from customer_service_inside_log
			                   where status in (2,3,4) 
			                   group by service_no ) il on view_customer_service_list.service_no = il.service_no_from_inside 
			                                           and (il.transfer_count > 0 
									                    or il.transfer_back_count > 0) "
                + qc.CommandSql;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qc.CommandSql = qc.CommandSql.TrimEnd(';') + " order by " + orderBy + " desc ";
                defOrderBy = orderBy;
            }
            if (pageLength > 0)
                qc = SSHelper.MakePagable(qc, pageStart, pageLength, defOrderBy + " desc ");
            ViewCustomerServiceListCollection vcslCol = new ViewCustomerServiceListCollection();
            vcslCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcslCol;
        }
        
        public int GetViewCustomerServiceListByVbsCount(string sellerGuids, params string[] filter)
        {
            QueryCommand qc = GetVcslWhereQC(sellerGuids, filter);
            qc.CommandSql = "select count(1) from " + ViewCustomerServiceList.Schema.Provider.DelimitDbName(ViewCustomerServiceList.Schema.TableName) + " with(nolock) " +
                @" inner join (select service_no service_no_from_inside, 
			                         sum(case [status] when 2 then 1 else 0 end) transfer_count, 
			                         sum(case [status] when 3 then 1 else 0 end) transfer_back_count
                                from customer_service_inside_log
			                   where status in (2,3,4) 
			                   group by service_no ) il on view_customer_service_list.service_no = il.service_no_from_inside 
			                                           and (il.transfer_count > 0 
									                    or il.transfer_back_count > 0) "
                + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        protected QueryCommand GetVcslWhereQC(string sellerGuids, params string[] filter)
        {            
            QueryCommand qc = new QueryCommand(" ", ViewCustomerServiceList.Schema.Provider.Name);

            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(ViewCustomerServiceList.Schema);

                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                qc.CommandSql += string.Format(" and {0} in ({1}) ", ViewCustomerServiceList.Columns.SellerGuid, sellerGuids);
                qc.CommandSql += string.Format(" and {0} in ({1}) ", ViewCustomerServiceList.Columns.CustomerServiceStatus, string.Format("{0},{1},{2},{3}", (int)statusConvert.process,(int)statusConvert.transfer, (int)statusConvert.transferback, (int)statusConvert.complete)); 
                qc.CommandSql += string.Format(" and {0} is not null ", ViewCustomerServiceList.Columns.OrderGuid);
                DataProvider.AddWhereParameters(qc, qry);
            }
            else
            {
                qc.CommandSql += string.Format(" where {0} in ({1}) ", ViewCustomerServiceList.Columns.SellerGuid, sellerGuids);
                qc.CommandSql += string.Format(" and {0} in ({1}) ", ViewCustomerServiceList.Columns.CustomerServiceStatus, string.Format("{0},{1},{2},{3}", (int)statusConvert.process, (int)statusConvert.transfer, (int)statusConvert.transferback, (int)statusConvert.complete));
                qc.CommandSql += string.Format(" and {0} is not null ", ViewCustomerServiceList.Columns.OrderGuid);
            }
            return qc;
        }

        public ViewCustomerServiceList GetViewCustomerServiceMessageByServiceNo(string serviceNo)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>().Where(ViewCustomerServiceList.Columns.ServiceNo).IsEqualTo(serviceNo)
                                                                     .ExecuteSingle<ViewCustomerServiceList>();
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>()
                .Where(ViewCustomerServiceList.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<ViewCustomerServiceListCollection>();
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageByUserIdV2(int userId)
        {
            string sql = @"select * from view_customer_service_list 
                    where user_id = @userId and service_no = parent_service_no";

            QueryCommand qc = new QueryCommand(sql, CustomerServiceMessage.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);

            ViewCustomerServiceListCollection vcslCol = new ViewCustomerServiceListCollection();
            vcslCol.LoadAndCloseReader(DataService.GetReader(qc));
            return vcslCol;
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageByTsMemberNoAndIssueFromType(string tsMemberNo,int issueFromType)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>()
                .Where(ViewCustomerServiceList.Columns.TsMemberNo).IsEqualTo(tsMemberNo)
                .And(ViewCustomerServiceList.Columns.IssueFromType).IsEqualTo(issueFromType)
                .ExecuteAsCollection<ViewCustomerServiceListCollection>();
        }
        
        public ViewCustomerServiceList GetViewCustomerServiceMessageByUserIdAndOrderGuid(int userId,Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>().Top("1")
                .Where(ViewCustomerServiceList.Columns.UserId).IsEqualTo(userId)
                .And(ViewCustomerServiceList.Columns.OrderGuid).IsEqualTo(orderGuid)
                .OrderDesc(ViewCustomerServiceList.Columns.CreateTime)
                .ExecuteSingle<ViewCustomerServiceList>();
        }

        public ViewCustomerServiceList GetViewCustomerServiceMessageByTsMemberNoAndOrderGuid(string tsMemberNo, Guid orderGuid ,int issueFromType)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>().Top("1")
                .Where(ViewCustomerServiceList.Columns.TsMemberNo).IsEqualTo(tsMemberNo)
                .And(ViewCustomerServiceList.Columns.OrderGuid).IsEqualTo(orderGuid)
                .And(ViewCustomerServiceList.Columns.IssueFromType).IsEqualTo(issueFromType)
                .OrderDesc(ViewCustomerServiceList.Columns.CreateTime)
                .ExecuteSingle<ViewCustomerServiceList>();
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageByOrderGuid(Guid orderGuid)
        {
            return DB.SelectAllColumnsFrom<ViewCustomerServiceList>()
                .Where(ViewCustomerServiceList.Columns.OrderGuid).IsEqualTo(orderGuid)
                .ExecuteAsCollection<ViewCustomerServiceListCollection>();
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageByPage(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewCustomerServiceList, ViewCustomerServiceListCollection>(page, pageSize, orderBy, false, filter);
        }

        public ViewCustomerServiceListCollection GetViewCustomerServiceMessageForNotCaimed(int page, int pageSize, string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewCustomerServiceList, ViewCustomerServiceListCollection>(page, pageSize, orderBy, true, filter);
        }




        public ViewCustomerServiceListCollection GetQueryResultForProcess(int page, int pageSize, string orderBy,string orderByAsc, params string[] filter)
        {
            return SSHelper.GetQueryResultForProcess<ViewCustomerServiceList, ViewCustomerServiceListCollection>(page, pageSize, orderBy, false, orderByAsc, filter);
        }

        public int GetViewCustomerServiceMessageListCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<ViewCustomerServiceList>(filter);
            qc.CommandSql = "select count(1) from " + ViewCustomerServiceList.Schema.Provider.DelimitDbName(ViewCustomerServiceList.Schema.TableName) + " with(nolock) " + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion



    }
}

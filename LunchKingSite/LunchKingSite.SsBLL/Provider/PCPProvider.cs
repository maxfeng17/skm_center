﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Security;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Interface;
using LunchKingSite.DataOrm;
using SubSonic;
using LunchKingSite.Core;

namespace LunchKingSite.SsBLL.Provider
{
    public class PCPProvider : IPCPProvider
    {
        #region identity_code

        public void IdentityCodeSetStatus(Int64 identityCodeId, IdentityCodeStatus status)
        {
            DB.Update<IdentityCode>().Set(IdentityCode.Columns.Status).EqualTo((byte)status)
                .Where(IdentityCode.Columns.Id).IsEqualTo(identityCodeId).Execute();
        }

        public IdentityCode IdentityCodeGet(Int64 id)
        {
            return DB.Get<IdentityCode>(id);
        }

        #endregion

        #region membership_card

        public MembershipCardCollection MembershipCardGetByGroupAndVersion(int cardGroupId, int versionId)
        {
            return DB.SelectAllColumnsFrom<MembershipCard>()
                .Where(MembershipCard.Columns.CardGroupId).IsEqualTo(cardGroupId)
                .And(MembershipCard.Columns.VersionId).IsEqualTo(versionId)
                .ExecuteAsCollection<MembershipCardCollection>();
        }

        public MembershipCardCollection MembershipCardGetByGroupId(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<MembershipCard>()
                .Where(MembershipCard.Columns.CardGroupId).IsEqualTo(cardGroupId)
                .ExecuteAsCollection<MembershipCardCollection>();
        }
        public int MembershipCardUpdateSynchronizationData(int cardGroupId, int versionId, AvailableDateType type, bool combineUse)
        {
            return DB.Update<MembershipCard>()
                .Set(MembershipCard.AvailableDateTypeColumn)
                .EqualTo(type)
                .Set(MembershipCard.CombineUseColumn)
                .EqualTo(combineUse)
                .Where(MembershipCard.CardGroupIdColumn)
                .IsEqualTo(cardGroupId)
                .And(MembershipCard.VersionIdColumn)
                .IsEqualTo(versionId)
                .Execute();
        }

        public MembershipCard MembershipCardGet(int cardGroupId, DateTime closeTime, int level)
        {
            var sql = @"select top 1 mc.* from view_membership_card vc
                        inner join membership_card mc On vc.card_id = mc.id
                        where vc.card_group_id = @cardGroupId and vc.close_time = @closeTime
                        and vc.level = @level";

            var qc = new QueryCommand(sql, MembershipCard.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);
            qc.AddParameter("@closeTime", closeTime, DbType.DateTime);
            qc.AddParameter("@level", level, DbType.Int32);

            var data = new MembershipCard();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public MembershipCard MembershipCardGet(int membershipCardId)
        {
            return DB.Get<MembershipCard>(membershipCardId);
        }

        public bool MembershipCardSet(MembershipCard MembershipCard)
        {
            return DB.Save<MembershipCard>(MembershipCard) == 1;
        }
        public bool MembershipCardSet(MembershipCardCollection membershipCard)
        {
            DB.SaveAll(membershipCard);
            return true;
        }

        /// <summary>
        /// 取回符合目前消費次數與總額等級最高的會員卡
        /// </summary>
        /// <param name="userMembershipCardId">user_membership_card.id</param>
        /// <param name="versionId">membership_card.version_id</param>
        /// <returns></returns>
        public MembershipCard MembershipCardGetForLevelUp(int userMembershipCardId, int versionId, PcpLevelChangeType changelType = PcpLevelChangeType.Up)
        {
            string sql = @"DECLARE @orderCount int, @amountTotal int
                select @orderCount = order_count, @amountTotal = amount_total from user_membership_card where id = @userMembershipCardId

                select top 1 * from membership_card
                where version_id = @versionId 
                and
                ((conditional_logic = 0 AND ((order_needed<>0 AND order_needed <= @orderCount)OR (amount_needed<>0.00 AND amount_needed <= @amountTotal)))
                Or (
                 conditional_logic = 1 and (order_needed <= @orderCount And amount_needed <= @amountTotal)
                )) and status = 1 and enabled = 1 and getdate() between open_time and close_time";

            if (changelType == PcpLevelChangeType.Up)
            {
                sql += " order by level desc ";

            }
            else
            {
                sql += " order by level asc ";

            }

            var qc = new QueryCommand(sql, MembershipCard.Schema.Provider.Name);
            qc.AddParameter("@userMembershipCardId", userMembershipCardId, DbType.Int32);
            qc.AddParameter("@versionId", versionId, DbType.Int32);

            var data = new MembershipCard();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion membership_card

        #region user_membership_card
        public int UserMembershipCardSet(UserMembershipCard u)
        {
            return DB.Save<UserMembershipCard>(u);
        }

        public int UserMembershipCardSet(UserMembershipCardCollection userMembershipCardCollection)
        {
            return DB.SaveAll(userMembershipCardCollection);
        }

        public void UserMembershipCardUpdateOrderCount(int cardId, double amount, PcpLevelChangeType changelType = PcpLevelChangeType.Up)
        {
            if (changelType == PcpLevelChangeType.Up)
            {
                const string sql =
                    @"UPDATE user_membership_card SET order_count = order_count+1, amount_total=amount_total+@amount, last_use_time=GETDATE()
	                WHERE id = @cardId";

                var qc = new QueryCommand(sql, UserMembershipCard.Schema.Provider.Name);
                qc.AddParameter("@cardId", cardId, DbType.Int32);
                qc.AddParameter("@amount", amount, DbType.Decimal);

                DataService.ExecuteScalar(qc);
            }
            else
            {
                const string sql =
                    @"UPDATE user_membership_card SET order_count = order_count-1, amount_total=amount_total-@amount, last_use_time=GETDATE()
	                WHERE id = @cardId";

                var qc = new QueryCommand(sql, UserMembershipCard.Schema.Provider.Name);
                qc.AddParameter("@cardId", cardId, DbType.Int32);
                qc.AddParameter("@amount", amount, DbType.Decimal);

                DataService.ExecuteScalar(qc);
            }
        }

        public void UserMembershipCardLevelUp(int userMembershipCardId, int memberCardId)
        {
            DB.Update<UserMembershipCard>().Set(UserMembershipCard.Columns.CardId).EqualTo(memberCardId)
            .Where(UserMembershipCard.Columns.Id).IsEqualTo(userMembershipCardId).Execute();
        }

        public UserMembershipCard UserMembershipCardGet(int userMembershipCardId)
        {
            return DB.Get<UserMembershipCard>(userMembershipCardId);
        }

        public UserMembershipCard UserMembershipCardGetByCardGroupIdAndUserId(int cardGroupId, int userId)
        {
            UserMembershipCard card = new UserMembershipCard();
            var query = DB.SelectAllColumnsFrom<UserMembershipCard>().NoLock()
                         .Where(UserMembershipCard.CardGroupIdColumn).IsEqualTo(cardGroupId)
                         .And(UserMembershipCard.UserIdColumn).IsEqualTo(userId)
                         .ExecuteSingle<UserMembershipCard>();
            if (query != null)
            {
                card = query;
            }
            return card;
        }

        public UserMembershipCardCollection UserMembershipCardGetByCardId(int cardId)
        {
            return DB.Select().From<UserMembershipCard>()
                .Where(UserMembershipCard.Columns.CardId)
                .IsEqualTo(cardId)
                .ExecuteAsCollection<UserMembershipCardCollection>();
        }

        public string UserMembershipCardNoGet(DateTime d)
        {
            int result = 0;
            var data = new UserMembershipCard();

            string sql = @"select TOP 1 CAST(SUBSTRING(card_no,8,4) as INT) as card_no from user_membership_card
where @serialPrefix=SUBSTRING(card_no,1,7)
order by CAST(SUBSTRING(card_no,8,4) as INT) DESC";

            var qc = new QueryCommand(sql, UserMembershipCard.Schema.Provider.Name);
            var serialPrefix = string.Format("U{0:yyMMdd}", d);
            qc.AddParameter("@serialPrefix", serialPrefix, DbType.String);

            data.LoadAndCloseReader(DataService.GetReader(qc));

            if (data.IsLoaded)
            {
                int.TryParse(data.CardNo, out result);
            }
            string cardNo = "";
            //result++;
            if (result >= 999)
            {
                cardNo = string.Format("{0}{1:00000}", serialPrefix, ++result);
            }
            else
            {
                cardNo = string.Format("{0}{1:0000}", serialPrefix, ++result);
            }
            return cardNo;
        }

        public UserMembershipCardCollection UserMembershipCardGetByUserId(int userId)
        {
            return DB.Select().From<UserMembershipCard>()
                .Where(UserMembershipCard.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<UserMembershipCardCollection>();
        }

        public int UserMembershipCardRemove(List<int> userCardIdList, int userId)
        {
            string sql = "Update user_membership_card set enabled = 0 where user_id=@userId and id in (" + string.Join(",", userCardIdList) + ")";
            QueryCommand qc = new QueryCommand(sql, DiscountTemplateStore.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            return DB.Execute(qc);
        }

        public UserMembershipCard UserMembershipCardGetByCardGroupIDAndUserId(int cardGroupId, int userId)
        {
            var data = DB.SelectAllColumnsFrom<UserMembershipCard>()
                .Where(UserMembershipCard.Columns.CardGroupId).IsEqualTo(cardGroupId)
                .And(UserMembershipCard.Columns.UserId).IsEqualTo(userId)
                .ExecuteAsCollection<UserMembershipCardCollection>();
            return data.FirstOrDefault();
        }

        public int UserMembershipCardUpdateLevelZeroToOne(int cardGroupId, int cardId)
        {
            string sql = @"Update user_membership_card set card_id = @cardId
                from user_membership_card u inner join membership_card m On u.card_id = m.id
                where m.card_group_id = @groupId and m.level = 0";

            QueryCommand qc = new QueryCommand(sql, DiscountTemplateStore.Schema.Provider.Name);
            qc.AddParameter("@groupId", cardGroupId, DbType.Int32);
            qc.AddParameter("@cardId", cardId, DbType.Int32);
            return DB.Execute(qc);
        }

        #endregion

        #region membership_card_group

        public MembershipCardGroup MembershipCardGroupGetSet(Guid sellerGuid, bool isNullInsertNew = false)
        {
            var cardGroup = DB.Get<MembershipCardGroup>(MembershipCardGroup.Columns.SellerGuid, sellerGuid);
            if (cardGroup.IsLoaded) return cardGroup;

            if (isNullInsertNew)
            {
                cardGroup.SellerGuid = sellerGuid;
                DB.Save(cardGroup);
            }

            return cardGroup;
        }

        public int MembershipCardGroupSet(MembershipCardGroup m)
        {
            return DB.Save(m);
        }

        public MembershipCardGroup MembershipCardGroupGet(int groupId)
        {
            var cardGroup = DB.Get<MembershipCardGroup>(MembershipCardGroup.Columns.Id, groupId);
            return cardGroup;
        }

        public MembershipCardGroup MembershipCardGroupGetBySellerGuid(Guid sellerGuid)
        {
            MembershipCardGroup cardGroup = new MembershipCardGroup();
            var query = DB.SelectAllColumnsFrom<MembershipCardGroup>().NoLock()
                         .Where(MembershipCardGroup.SellerGuidColumn).IsEqualTo(sellerGuid)
                         .ExecuteSingle<MembershipCardGroup>();
            if (query != null)
            {
                cardGroup = query;
            }
            return cardGroup;
        }

        public MembershipCardGroupCollection MembershipCardGroupGetBySellerGuids(List<Guid> sellers)
        {
            var data = DB.SelectAllColumnsFrom<MembershipCardGroup>()
                        .Where(MembershipCardGroup.Columns.SellerGuid).In(sellers)
                        .ExecuteAsCollection<MembershipCardGroupCollection>();

            return data;
        }

        public MembershipCardGroupCollection MembershipCardGroupGetBySellerUserId(int sellerUserId)
        {
            var data = DB.SelectAllColumnsFrom<MembershipCardGroup>()
                        .Where(MembershipCardGroup.Columns.SellerUserId).IsEqualTo(sellerUserId)
                        .ExecuteAsCollection<MembershipCardGroupCollection>();

            return data;
        }



        #endregion

        #region membership_card_group_version

        public MembershipCardGroupVersion MembershipCardGroupVersionGet(int versionId)
        {
            return DB.Get<MembershipCardGroupVersion>(versionId);
        }

        public MembershipCardGroupVersion MembershipCardGroupVersionGetCardGroupLastData(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupVersion>().Top("1")
                     .Where(MembershipCardGroupVersion.Columns.CardGroupId).IsEqualTo(cardGroupId)
                     .And(MembershipCardGroupVersion.Columns.OpenTime).IsLessThanOrEqualTo(DateTime.Now)
                     .And(MembershipCardGroupVersion.Columns.CloseTime).IsGreaterThan(DateTime.Now)
                     .OrderDesc(MembershipCardGroupVersion.Columns.CloseTime).ExecuteSingle<MembershipCardGroupVersion>();
        }

        public bool MembershipCardGroupVersionSet(MembershipCardGroupVersion version)
        {
            DB.Save(version);
            return true;
        }

        #endregion membership_card_group_version    

        #region membership_card_group_store

        public bool MembershipCardGroupStoreGuidIsExists(int groupId, Guid storeGuid)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupStore>()
                .Where(MembershipCardGroupStore.Columns.CardGroupId)
                .IsEqualTo(groupId)
                .And(MembershipCardGroupStore.Columns.StoreGuid).IsEqualTo(storeGuid)
                .ExecuteAsCollection<MembershipCardGroupStoreCollection>().Any();
        }

        public MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(int groupId)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupStore>()
                .Where(MembershipCardGroupStore.Columns.CardGroupId)
                .IsEqualTo(groupId)
                .ExecuteAsCollection<MembershipCardGroupStoreCollection>();
        }

        public MembershipCardGroupStoreCollection MembershipCardGroupStoreGet(List<Guid> storeGuids)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupStore>()
                .Where(MembershipCardGroupStore.Columns.StoreGuid)
                .In(storeGuids)
                .ExecuteAsCollection<MembershipCardGroupStoreCollection>();
        }

        public MembershipCardGroupStore MembershipCardGroupStoreGetBySellerGuid(Guid sellerGuid)
        {
            MembershipCardGroupStore cardGroupStore = new MembershipCardGroupStore();
            var query = DB.SelectAllColumnsFrom<MembershipCardGroupStore>().Top("1").NoLock()
                          .Where(MembershipCardGroupStore.StoreGuidColumn).IsEqualTo(sellerGuid)
                          .ExecuteSingle<MembershipCardGroupStore>();
            if (query != null)
            {
                cardGroupStore = query;
            }
            return cardGroupStore;
        }

        public void MembershipCardGroupStoreDel(int groupId)
        {
            DB.Delete<MembershipCardGroupStore>(MembershipCardGroupStore.Columns.CardGroupId, groupId);
        }

        public void MembershipCardGroupStoreDelByStoreGuid(Guid storeGuid)
        {
            DB.Delete<MembershipCardGroupStore>(MembershipCardGroupStore.Columns.StoreGuid, storeGuid);
        }

        public SellerCollection MembershipCardGroupStoreCanAddedBySellGuid(string parentSellerGuid, int cardGroupId)
        {
            string sql = string.Format(@"
select store_address, * from seller with(nolock) where guid in (
 select seller_guid from seller_tree with(nolock) where parent_seller_guid = @parentSellerGuid
) 
and guid not in (
 select store_guid from membership_card_group_store where card_group_id = @cardGroupId
)
");
            QueryCommand qc = new QueryCommand(sql, ViewMembershipCard.Schema.Provider.Name);
            qc.AddParameter("parentSellerGuid", parentSellerGuid, DbType.String);
            qc.AddParameter("cardGroupId", cardGroupId, DbType.Int32);

            var result = new SellerCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public void MembershipCardGroupStoreSet(MembershipCardGroupStoreCollection stores)
        {
            foreach (var item in stores)
            {
                DB.Save(item);
            }
        }

        #endregion membership_card_group_store

        #region ViewMembershipCard

        public ViewMembershipCard ViewMembershipCardGet(int membershipCardId)
        {
            var data = DB.SelectAllColumnsFrom<ViewMembershipCard>()
                .Where(ViewMembershipCard.Columns.CardId)
                .IsEqualTo(membershipCardId)
                .ExecuteAsCollection<ViewMembershipCardCollection>();
            return data.FirstOrDefault();
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByAccountId(string accountId)
        {
            string sql = string.Format(@"
SELECT vmc.*
FROM view_membership_card vmc WITH (NOLOCK)
JOIN vbs_membership vm WITH (NOLOCK) ON vmc.seller_user_id = vm.user_id
WHERE vm.account_id = @accountId
");

            QueryCommand qc = new QueryCommand(sql, ViewMembershipCard.Schema.Provider.Name);
            qc.AddParameter("accountId", accountId, DbType.String);

            var result = new ViewMembershipCardCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByCardIds(List<int> cardIds)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                        .Where(ViewMembershipCard.Columns.CardId).In(cardIds)
                        .ExecuteAsCollection<ViewMembershipCardCollection>();
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByGuid(Guid sellerGuid)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                        .Where(ViewMembershipCard.Columns.SellerGuid).IsEqualTo(sellerGuid)
                        .ExecuteAsCollection<ViewMembershipCardCollection>();
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByUserId(int sellerUserId)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                        .Where(ViewMembershipCard.Columns.SellerUserId).IsEqualTo(sellerUserId)
                        .ExecuteAsCollection<ViewMembershipCardCollection>();
        }

        public int ViewMembershipCardGetCount(params string[] filter)
        {
            QueryCommand qc = SSHelper.GetWhereQC<ViewMembershipCard>(filter);
            qc.CommandSql = "select count(1) from " + ViewMembershipCard.Schema.Provider.DelimitDbName(ViewMembershipCard.Schema.TableName) + " with(nolock)" + qc.CommandSql;
            return (int)DataService.ExecuteScalar(qc);
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByDatetime(int groupId, DateTime d)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                            .Where(ViewMembershipCard.Columns.CardGroupId).IsEqualTo(groupId)
                            .And(ViewMembershipCard.Columns.OpenTime).IsLessThanOrEqualTo(d)
                            .And(ViewMembershipCard.Columns.CloseTime).IsGreaterThan(d)
                            .ExecuteAsCollection<ViewMembershipCardCollection>() ?? new ViewMembershipCardCollection();
        }

        public ViewMembershipCardCollection ViewMembershipCardGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewMembershipCard, ViewMembershipCardCollection>(0, -1, orderBy, filter);
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListByEnableDatetime(int status, DateTime s, DateTime e)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                            .Where(ViewMembershipCard.Columns.Status).IsLessThanOrEqualTo(status)
                            .And(ViewMembershipCard.Columns.OpenTime).IsLessThanOrEqualTo(s)
                            .And(ViewMembershipCard.Columns.CloseTime).IsGreaterThanOrEqualTo(e)
                            .ExecuteAsCollection<ViewMembershipCardCollection>();
        }

        public ViewMembershipCardCollection ViewMembershipCardGetListBySellerUserId(List<int> ids)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCard>()
                        .Where(ViewMembershipCard.Columns.SellerUserId).In(ids)
                        .ExecuteAsCollection<ViewMembershipCardCollection>();
        }

        #endregion

        #region seller_member

        public SellerMemberCollection SellerMemberGetByBirthMonth(int sellerUserId, int month)
        {
            string sql = string.Format(@"SELECT * FROM {0} WITH(NOLOCK) 
WHERE seller_user_id = @sellerUserId AND DATEPART(month, {1}) = @month"
                , SellerMember.Schema.TableName, SellerMember.Columns.Birthday);

            var qc = new QueryCommand(sql, ViewPcpPointDeposit.Schema.Provider.Name);
            qc.AddParameter("@sellerUserId", sellerUserId, DbType.Int32);
            qc.AddParameter("@month", month, DbType.Int16);

            var data = new SellerMemberCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }
        public SellerMemberCollection SellerMemberGet(params string[] filter)
        {
            var data = new SellerMemberCollection();
            var sql = filter.Aggregate(@"SELECT * FROM seller_member WHERE 1=1", (current, f) => current + string.Format(" AND {0}", f));
            var qc = new QueryCommand(sql, SellerMember.Schema.Provider.Name);
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public void SellerMemberSet(SellerMember m)
        {
            DB.Save(m);
        }

        public int SellerMemberSet(SellerMemberCollection m)
        {
            return DB.SaveAll<SellerMember, SellerMemberCollection>(m);
        }

        public SellerMember SellerMemberGet(int sellerMemberId)
        {
            return DB.Get<SellerMember>(sellerMemberId);
        }

        public SellerMember SellerMemberGet(int sellerUserId, string mobile)
        {
            var sellerMem = DB.SelectAllColumnsFrom<SellerMember>()
                .Where(SellerMember.Columns.SellerUserId).IsEqualTo(sellerUserId)
                .And(SellerMember.Columns.Mobile).IsEqualTo(mobile)
                .ExecuteAsCollection<SellerMemberCollection>();

            if (sellerMem.Any()) { return sellerMem.First(); }
            else { return new SellerMember() { CreateTime = DateTime.Now }; }
        }

        public bool SellerMemberCheck(int sellerUserId, string mobile)
        {
            var sellerMem = DB.SelectAllColumnsFrom<SellerMember>()
                    .Where(SellerMember.Columns.Id).IsNotEqualTo(sellerUserId)
                    .And(SellerMember.Columns.Mobile).IsEqualTo(mobile)
                    .ExecuteAsCollection<SellerMemberCollection>();

            return sellerMem.Any();
        }

        #endregion seller_member

        #region view_user_membership_card

        public ViewUserMembershipCardCollection ViewUserMembershipCardGet(string sql)
        {
            var qc = new QueryCommand(sql, ViewUserMembershipCard.Schema.Provider.Name);
            var result = new ViewUserMembershipCardCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewUserMembershipCardCollection ViewUserMembershipCardGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewUserMembershipCard, ViewUserMembershipCardCollection>(0, -1, orderBy, filter);
        }

        public ViewUserMembershipCard ViewUserMembershipCardGet(int userMembershipCardId)
        {
            var data = DB.SelectAllColumnsFrom<ViewUserMembershipCard>()
                .Where(ViewUserMembershipCard.Columns.Id).IsEqualTo(userMembershipCardId)
                .ExecuteAsCollection<ViewUserMembershipCardCollection>();
            return data.Any() ? data.FirstOrDefault() : new ViewUserMembershipCard();
        }

        public ViewUserMembershipCardCollection ViewUserMembershipCardGet(int userId, int groupId)
        {
            return DB.SelectAllColumnsFrom<ViewUserMembershipCard>()
                .Where(ViewUserMembershipCard.Columns.UserId).IsEqualTo(userId)
                .And(ViewUserMembershipCard.Columns.CardGroupId).IsEqualTo(groupId)
                .ExecuteAsCollection<ViewUserMembershipCardCollection>();
        }

        #endregion

        #region view_membership_card_group
        public ViewMembershipCardGroupCollection ViewMembershipCardGroupGetList(List<Guid> sellers)
        {
            ViewMembershipCardGroupCollection cardGroupList = new ViewMembershipCardGroupCollection();
            var query = DB.SelectAllColumnsFrom<ViewMembershipCardGroup>().NoLock()
                         .Where(ViewMembershipCardGroup.Columns.SellerGuid).In(sellers)
                         .ExecuteAsCollection<ViewMembershipCardGroupCollection>();
            if (query != null)
            {
                cardGroupList = query;
            }
            return cardGroupList;
        }
        public ViewMembershipCardGroupCollection ViewMembershipCardGroupGetList(List<int> groupIds)
        {
            ViewMembershipCardGroupCollection cardGroupList = new ViewMembershipCardGroupCollection();
            var query = DB.SelectAllColumnsFrom<ViewMembershipCardGroup>().NoLock()
                         .Where(ViewMembershipCardGroup.Columns.CardGroupId).In(groupIds)
                         .ExecuteAsCollection<ViewMembershipCardGroupCollection>();
            if (query != null)
            {
                cardGroupList = query;
            }
            return cardGroupList;
        }
        public ViewMembershipCardGroupCollection ViewMembershipCardGroupGetListBySellerUserId(int sellerUserId)
        {
            ViewMembershipCardGroupCollection cardGroupList = new ViewMembershipCardGroupCollection();
            var query = DB.SelectAllColumnsFrom<ViewMembershipCardGroup>().NoLock()
                         .Where(ViewMembershipCardGroup.Columns.SellerUserId).IsEqualTo(sellerUserId)
                         .ExecuteAsCollection<ViewMembershipCardGroupCollection>();
            if (query != null)
            {
                cardGroupList = query;
            }
            return cardGroupList;
        }

        public List<string> ViewMembershipCarOnlineVendorGet()
        {
            var data = DB.SelectAllColumnsFrom<ViewMembershipCardGroup>().NoLock()
                .Where(ViewMembershipCardGroup.Columns.Status).IsEqualTo((int)MembershipCardStatus.Open)
                .OrderDesc(ViewMembershipCardGroup.Columns.PublishTime)
                .ExecuteAsCollection<ViewMembershipCardGroupCollection>();

            var result = data.GroupBy(x => x.SellerName).Select(x => new { sellerName = x.Key }).Select(x => x.sellerName).ToList();

            return result;
        }


        #endregion

        #region view_user_membership_card_store

        public ViewUserMembershipCardStoreCollection ViewUserMembershipCardStoreGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewUserMembershipCardStore, ViewUserMembershipCardStoreCollection>(0, -1, orderBy, filter);
        }
        #endregion view_user_membership_card_store

        #region view_membership_card_group_version

        public ViewMembershipCardGroupVersionCollection ViewMembershipCardGroupVersionGetByQuarter(int cardGroup, int year, Quarter q)
        {
            DateTime start, end;

            Helper.GetQuarterDateRange(year, q, out start, out end);

            return DB.SelectAllColumnsFrom<ViewMembershipCardGroupVersion>()
                .Where(ViewMembershipCardGroupVersion.Columns.CardGroupId).IsEqualTo(cardGroup)
                .And(ViewMembershipCardGroupVersion.Columns.CloseTime).IsBetweenAnd(start, end)
                .ExecuteAsCollection<ViewMembershipCardGroupVersionCollection>();
        }

        #endregion view_membership_card_group_version

        #region 舊的熟客卡優惠集點相關
        #region pcp_point_deposit
        public int PcpPointDepositSet(PcpPointDeposit pcpPointDeposit)
        {
            return DB.Save<PcpPointDeposit>(pcpPointDeposit);
        }
        #endregion

        #region view_pcp_point_deposit


        public ViewPcpPointDepositCollection ViewPcpPointDepositGetList(string column, object value)
        {
            ViewPcpPointDepositCollection view = new ViewPcpPointDepositCollection();
            view.LoadAndCloseReader(ViewPcpPointDeposit.Query().WHERE(column, value).ExecuteReader());
            return view;
        }


        public ViewPcpPointDepositCollection ViewPcpPointDepositGetOnTimeData(int userId)
        {
            const string sql = @"SELECT * FROM view_pcp_point_deposit 
                    WHERE user_id = @userId AND point - withdrawal_point > 0 AND GETDATE() between start_time and expire_time
			        ORDER BY expire_time";

            var qc = new QueryCommand(sql, ViewPcpPointDeposit.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);

            var data = new ViewPcpPointDepositCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        public ViewPcpPointDepositCollection ViewPcpPointDepositGetByDatetime(DateTime dt)
        {
            string sql = @"SELECT * FROM view_pcp_point_deposit WITH(NOLOCK) 
                    WHERE expire_time <= @dt AND point <> withdrawal_point";

            var qc = new QueryCommand(sql, ViewPcpPointDeposit.Schema.Provider.Name);
            qc.AddParameter("@dt", dt, DbType.DateTime);

            var data = new ViewPcpPointDepositCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region pcp_point_deposit

        public PcpPointDepositCollection PcpPointDepositGetByDatetime(DateTime start, DateTime end, int type)
        {
            string sql = @"
SELECT * FROM pcp_point_deposit WITH(NOLOCK) 
WHERE type=@type and create_time>=@start and create_time<@end";

            var qc = new QueryCommand(sql, PcpPointDeposit.Schema.Provider.Name);
            qc.AddParameter("@type", type, DbType.Int16);
            qc.AddParameter("@start", start, DbType.DateTime);
            qc.AddParameter("@end", end, DbType.DateTime);

            var data = new PcpPointDepositCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region pcp_point_withdrawal

        public void PcpPointWithdrawalSet(PcpPointWithdrawal point)
        {
            DB.Save(point);
        }

        #endregion

        #region pcp_point_withdrawal_overall

        public void PcpPointWithdrawalOverallSet(PcpPointWithdrawalOverall point)
        {
            DB.Save(point);
        }

        public PcpPointWithdrawalOverall PcpPointWithdrawalOverallGet(int id)
        {
            return DB.Get<PcpPointWithdrawalOverall>(id);
        }

        public PcpPointWithdrawalOverallCollection PcpPointWithdrawalOverallGetByDatetime(DateTime start, DateTime end, int type)
        {
            string sql = @"
SELECT * FROM pcp_point_withdrawal_overall WITH(NOLOCK) 
WHERE point_type=@type and create_time>=@start and create_time<=@end";

            var qc = new QueryCommand(sql, PcpPointWithdrawalOverall.Schema.Provider.Name);
            qc.AddParameter("@type", type, DbType.Int16);
            qc.AddParameter("@start", start, DbType.DateTime);
            qc.AddParameter("@end", end, DbType.DateTime);

            var data = new PcpPointWithdrawalOverallCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region pcp_lock_point

        public void PcpLockPointSet(PcpLockPoint point)
        {
            DB.Save(point);
        }

        public PcpLockPointCollection PcpLockPointGetList(string column, object value)
        {
            return DB.SelectAllColumnsFrom<PcpLockPoint>().Where(PcpLockPoint.Schema.GetColumn(column)).IsEqualTo(value)
                .ExecuteAsCollection<PcpLockPointCollection>();
        }

        public PcpLockPointCollection PcpLockPointGetOnTime(int userId, PcpPointType type)
        {
            const string sql = @"select * from pcp_lock_point where user_id=@userId And point_type=@type and lock_point > verification_point";

            var qc = new QueryCommand(sql, PcpLockPoint.Schema.Provider.Name);
            qc.AddParameter("@userId", userId, DbType.Int32);
            qc.AddParameter("@type", (int)type, DbType.Int32);

            var data = new PcpLockPointCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region  pcp_point_transaction_order

        public Guid PcpPointTransactionOrderSet(PcpPointTransactionOrder pcpPointTransactionOrder)
        {

            DB.Save<PcpPointTransactionOrder>(pcpPointTransactionOrder);

            if (pcpPointTransactionOrder.IsLoaded)
            {
                return pcpPointTransactionOrder.Guid;
            }
            else
            {
                return default(Guid);
            }
        }

        public PcpPointTransactionOrderCollection PcpPointTransactionOrderListGet(int sellerUserId)
        {
            var col = new PcpPointTransactionOrderCollection();

            var result = DB.SelectAllColumnsFrom<PcpPointTransactionOrder>()
                .Where(PcpPointTransactionOrder.Columns.SellerUserId)
                .IsEqualTo(sellerUserId)
                .ExecuteAsCollection<PcpPointTransactionOrderCollection>();

            if (result.Count > 0)
            {
                col = result;
            }
            return col;
        }

        public PcpPointTransactionOrder PcpPointTransactionOrderGet(int pcpPointTransactionOrderId)
        {
            return DB.Get<PcpPointTransactionOrder>(PcpPointTransactionOrder.Columns.Id, pcpPointTransactionOrderId);
        }


        #endregion pcp_point_transaction_order

        #region  pcp_point_transaction_refund

        public void PcpPointTransactionRefundSet(PcpPointTransactionRefund pcpPointTransactionRefund)
        {
            DB.Save<PcpPointTransactionRefund>(pcpPointTransactionRefund);
        }

        public PcpPointTransactionRefundCollection PcpPointTransactionRefundListGet(int pcpPointTransactionRefundId)
        {
            var col = new PcpPointTransactionRefundCollection();

            var result = DB.SelectAllColumnsFrom<PcpPointTransactionRefund>()
                .Where(PcpPointTransactionRefund.Columns.Id)
                .IsEqualTo(pcpPointTransactionRefundId)
                .ExecuteAsCollection<PcpPointTransactionRefundCollection>();

            if (result.Count > 0)
            {
                col = result;
            }
            return col;
        }

        #endregion pcp_point_transaction_refund

        #region  pcp_point_transaction_exchange_order

        public void PcpPointTransactionExchangeOrderSet(PcpPointTransactionExchangeOrder pcpPointTransactionExchangeOrder)
        {
            DB.Save<PcpPointTransactionExchangeOrder>(pcpPointTransactionExchangeOrder);
        }

        public PcpPointTransactionExchangeOrderCollection PcpPointTransactionExchangeOrderListGet(int pcpPointTransactionExchangeOrderId)
        {
            var col = new PcpPointTransactionExchangeOrderCollection();

            var result = DB.SelectAllColumnsFrom<PcpPointTransactionExchangeOrder>()
                .Where(PcpPointTransactionExchangeOrder.Columns.Id)
                .IsEqualTo(pcpPointTransactionExchangeOrderId)
                .ExecuteAsCollection<PcpPointTransactionExchangeOrderCollection>();

            if (result.Count > 0)
            {
                col = result;
            }
            return col;
        }

        public PcpPointTransactionExchangeOrderCollection PcpPointTransactionExchangeOrderListGetBySellerUserId(int sellerUserId)
        {
            var col = new PcpPointTransactionExchangeOrderCollection();

            var result = DB.SelectAllColumnsFrom<PcpPointTransactionExchangeOrder>()
                .Where(PcpPointTransactionExchangeOrder.Columns.SellerUserId)
                .IsEqualTo(sellerUserId)
                .ExecuteAsCollection<PcpPointTransactionExchangeOrderCollection>();

            if (result.Count > 0)
            {
                col = result;
            }
            return col;
        }



        #endregion pcp_point_transaction_exchange_order

        #region view_pcp_point_transaction_order
        public ViewPcpPointTransactionOrderRecordCollection ViewPcpPointTransactionOrderRecordListGetByUserId(int userId, PcpPointType pcpPointType)
        {
            var result = DB.SelectAllColumnsFrom<ViewPcpPointTransactionOrderRecord>()
                .Where(ViewPcpPointTransactionOrderRecord.Columns.UserId).IsEqualTo(userId)
                .And(ViewPcpPointTransactionOrderRecord.Columns.PointType).IsEqualTo((int)pcpPointType)
                .ExecuteAsCollection<ViewPcpPointTransactionOrderRecordCollection>();

            return (result.Count > 0) ? result : new ViewPcpPointTransactionOrderRecordCollection();

        }
        #endregion

        #endregion

        #region view_pcp_order_detail

        public ViewPcpOrderInfo ViewPcpOrderInfoCollectionGet(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpOrderInfo>().NoLock()
                          .Where(ViewPcpOrderInfo.Columns.CardGroupId).IsEqualTo(cardGroupId)
                          .OrderDesc(ViewPcpOrderInfo.Columns.OrderTime)
                          .ExecuteSingle<ViewPcpOrderInfo>();
        }
        
        public ViewPcpOrderInfoCollection ViewPcpOrderInfoCollectionGet(int cardGroupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize)
        {
            string sql = @"select * from view_pcp_order_info with(nolock) ";
            sql += "where card_group_id = @cardGroupId ";
            sql += "and identity_code_id > 0 ";
            sql += "and status_description is null ";

            if (sellerGuid.HasValue)
                sql += "and store_guid = @sellerGuid ";
            if (queryTime.HasValue)
                sql += "and order_time >= @queryTime ";
            

            sql += " order by " + ViewPcpOrderInfo.Columns.Pk + " desc ";
            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);

            if (sellerGuid.HasValue)
                qc.AddParameter("@sellerGuid", sellerGuid, DbType.Guid);
            if (queryTime.HasValue)
                qc.AddParameter("@queryTime", queryTime, DbType.DateTime);

            //qc = SSHelper.MakePagable(qc, pageNumber, pageSize, ViewPcpOrderInfo.Columns.Pk + " desc");
            ViewPcpOrderInfoCollection data = new ViewPcpOrderInfoCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;

        }
        public ViewPcpOrderInfoCollection ViewPcpOrderInfoCollectionGet(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpOrderInfo>().NoLock()
                          .Where(ViewPcpOrderInfo.Columns.CardGroupId).IsEqualTo(cardGroupId)
                          .And(ViewPcpOrderInfo.Columns.UserId).IsEqualTo(userId)
                          .And(ViewPcpOrderInfo.Columns.IdentityCodeId).IsGreaterThan(0)
                          .And(ViewPcpOrderInfo.Columns.StatusDescription).IsNull()
                          .OrderAsc(ViewPcpOrderInfo.Columns.OrderTime)
                          .ExecuteAsCollection<ViewPcpOrderInfoCollection>();
        }
        #endregion


        #region pcp_order

        public void PcpOrderSet(PcpOrder order)
        {
            DB.Save<PcpOrder>(order);
        }

        public PcpOrder PcpOrderGetByGuid(Guid orderGuid)
        {
            var pcpOrder = DB.Get<PcpOrder>(PcpOrder.Columns.Guid, orderGuid);
            return pcpOrder;
        }

        public PcpOrderCollection PcpOrderGetByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpOrder>()
                     .Where(PcpOrder.Columns.UserId).IsEqualTo(userId)
                     .ExecuteAsCollection<PcpOrderCollection>();
        }

        #endregion

        #region store

        /// <summary>
        /// 取得設定權限的分店
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public SellerCollection AllowStoreGetList(int userId)
        {
            string accountId = VbsAccounGet(userId);
            if (String.IsNullOrEmpty(accountId))
            {
                return new SellerCollection();
            }

            string sql = @"
  select * from seller where guid in (

	(select 
		sl.Guid store_guid
	from resource_acl racl
		inner join seller sl on sl.GUID = racl.resource_guid
	where racl.account_id = @accountId and racl.resource_type = 1 and racl.permission_setting = 1
	and sl.store_status = 0 and isnull(sl.store_address,'') <> '')

  union 

	(select 
		store.Guid store_guid
	from resource_acl racl
		inner join seller sl on sl.GUID = racl.resource_guid
        inner join seller_tree as st WITH(NOLOCK) on sl.guid = st.parent_seller_guid
        inner join seller as store WITH(NOLOCK) on store.guid = st.seller_guid and store.store_status = 0
	where racl.account_id = @accountId and racl.resource_type = 1 and racl.permission_setting = 1)

   union

	(select 
		st.Guid store_guid
	from resource_acl racl
		inner join seller st on st.GUID = racl.resource_guid  AND st.store_status=0
	where racl.account_id = @accountId and racl.resource_type = 2 and racl.permission_setting = 1)

	except
	(select 
		st.Guid store_guid
	from resource_acl racl
		inner join seller st on st.GUID = racl.resource_guid  AND st.store_status=0
	where racl.account_id = @accountId and racl.resource_type = 2 and racl.permission_setting = 0)

)	
";
            QueryCommand qc = new QueryCommand(sql, Seller.Schema.Provider.Name);
            qc.AddParameter("@accountId", accountId, DbType.String);
            var data = new SellerCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data;
        }

        #endregion store

        #region vbs_account

        public string VbsAccounGet(int userId)
        {
            VbsMembership vbsMem = DB.Get<VbsMembership>(VbsMembership.Columns.UserId, userId);
            return vbsMem.AccountId;
        }

        #endregion vbs_account

        #region discount_template_store

        public void DiscountStoreSet(DiscountTemplateStore ds)
        {
            DB.Save(ds);
        }

        public void DiscountTemplateStoreDelete(int templateId)
        {
            string sql = "delete from discount_template_store where template_id=@templateId";
            QueryCommand qc = new QueryCommand(sql, DiscountTemplateStore.Schema.Provider.Name);
            qc.AddParameter("templateId", templateId, DbType.Int32);
            DB.Execute(qc);
        }

        #endregion discount_template_store

        #region discount_template

        public DiscountTemplate DiscountTemplateGet(int id)
        {
            return DB.Get<DiscountTemplate>(id);
        }

        public void DiscountTemplateSet(DiscountTemplate template)
        {
            DB.Save(template);
        }

        public ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(int sellerUserId,
            int status, DateTime? beginTime, DateTime? endTime)
        {

            SqlQuery query = DB.Select()
                .From<ViewPcpDiscountTemplate>()
                .Where(ViewPcpDiscountTemplate.Columns.SellerUserId)
                .IsEqualTo(sellerUserId);

            if (status == 1)
            {
                query.And(ViewPcpDiscountTemplate.Columns.StartTime).IsLessThanOrEqualTo(DateTime.Now)
                    .And(ViewPcpDiscountTemplate.Columns.EndTime).IsGreaterThanOrEqualTo(DateTime.Now);
            }
            else if (status == 2)
            {
                query.And(ViewPcpDiscountTemplate.Columns.EndTime).IsLessThan(DateTime.Now);
            }
            if (beginTime != null && endTime != null)
            {
                query.And(ViewPcpDiscountTemplate.Columns.EndTime).IsGreaterThanOrEqualTo(beginTime.Value)
                    .And(ViewPcpDiscountTemplate.Columns.EndTime).IsLessThanOrEqualTo(endTime.Value);
            }

            return query.ExecuteAsCollection<ViewPcpDiscountTemplateCollection>();
        }

        public ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(int templateId, int sellerUserId)
        {
            SqlQuery query = DB.Select().From<ViewPcpDiscountTemplate>()
                .Where(ViewPcpDiscountTemplate.Columns.Id).IsEqualTo(templateId)
                .And(ViewPcpDiscountTemplate.Columns.SellerUserId).IsEqualTo(sellerUserId);

            return query.ExecuteAsCollection<ViewPcpDiscountTemplateCollection>();
        }

        public ViewPcpDiscountTemplateCollection ViewPcpDiscountTemplateGetList(params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<ViewPcpDiscountTemplate, ViewPcpDiscountTemplateCollection>(0, -1, null, filter);
        }

        #endregion

        #region discount_template_detail_campaign

        public ViewPcpAssignmentDiscountCampaignCollection ViewPcpAssignmentDiscountCampaignGet(int templateId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpAssignmentDiscountCampaign>()
                .Where(ViewPcpAssignmentDiscountCampaign.Columns.TemplateId).IsEqualTo(templateId)
                .ExecuteAsCollection<ViewPcpAssignmentDiscountCampaignCollection>();
        }

        #endregion

        #region ViewPcpOrderDiscountLog
        public ViewPcpOrderDiscountLogCollection ViewPcpOrderDiscountLogGetList(string accountId, DiscountCampaignType t)
        {
            return DB.SelectAllColumnsFrom<ViewPcpOrderDiscountLog>()
                .Where(ViewPcpOrderDiscountLog.Columns.OwnerName).IsEqualTo(accountId)
                .And(ViewPcpOrderDiscountLog.Columns.Type).IsEqualTo((int)t)
                .ExecuteAsCollection<ViewPcpOrderDiscountLogCollection>();
        }

        #endregion

        #region ViewPcpMemberPromotion
        public ViewPcpMemberPromotionCollection ViewPcpMemberPromotionSuperBonuxGetList(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpMemberPromotion>()
                .Where(ViewPcpMemberPromotion.Columns.UserId).IsEqualTo(userId)
                .And(ViewPcpMemberPromotion.Columns.OrderClassification).IsEqualTo((int)OrderClassification.PcpOrder)
                .OrderDesc(ViewPcpMemberPromotion.Columns.CreateTime)
                .ExecuteAsCollection<ViewPcpMemberPromotionCollection>();
        }

        public ViewPcpMemberPromotionCollection ViewPcpMemberPromotionSuperBonusGetList(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpMemberPromotion>()
                .Where(ViewPcpMemberPromotion.Columns.UserId).IsEqualTo(userId)
                .And(ViewPcpMemberPromotion.Columns.OrderClassification).IsGreaterThanOrEqualTo((int)OrderClassification.PcpOrder)
                .OrderDesc(ViewPcpMemberPromotion.Columns.CreateTime)
                .ExecuteAsCollection<ViewPcpMemberPromotionCollection>();
        }


        #endregion

        #region ViewPcpPointRecord
        public ViewPcpPointRecordCollection ViewPcpPointRecordGetList(string column, object value)
        {
            ViewPcpPointRecordCollection view = new ViewPcpPointRecordCollection();
            view.LoadAndCloseReader(ViewPcpPointRecord.Query().WHERE(column, value).ExecuteReader());
            return view;
        }


        public ViewPcpPointRecordCollection PcpPointUsageLog(int userId, PcpPointType type)
        {
            string sql = @"
select * from view_pcp_point_record WITH(NOLOCK) 
WHERE user_id=@userId and point_type=@type
ORDER BY (CASE WHEN deposit_id IS NULL THEN 1 ELSE 0 END) DESC,deposit_id DESC,create_time DESC
";
            QueryCommand qc = new QueryCommand(sql, ViewPcpPointRecord.Schema.Provider.Name);
            qc.AddParameter("userId", userId, DbType.Int32);
            qc.AddParameter("type", (int)type, DbType.Int32);

            var result = new ViewPcpPointRecordCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));

            return result;
        }

        public ViewPcpPointRecordCollection PcpPointUsageLog(List<int> userId, PcpPointType type)
        {
            var result = new ViewPcpPointRecordCollection();
            var tempIds = userId.Distinct().ToList();
            var idx = 0;
            while (idx <= tempIds.Count - 1)
            {
                var batchLimit = 2000;  // sql server cannot take more than 2100 parameters.

                string sql = string.Format(@"
select * from (
	select *,ROW_NUMBER() OVER (PARTITION by user_id ORDER by (CASE WHEN deposit_id IS NULL THEN 1 ELSE 0 END) DESC, deposit_id DESC,withdrawaloverall_id DESC) as n from view_pcp_point_record WITH(NOLOCK) 
	WHERE user_id in ({0}) and point_type=@type
)x
WHERE x.n=1 and deposit_id IS NOT NULL
", string.Join(",", tempIds.Skip(idx).Take(batchLimit)));

                QueryCommand qc = new QueryCommand(sql, ViewPcpPointRecord.Schema.Provider.Name);
                qc.AddParameter("type", (int)type, DbType.Int32);

                var data = new ViewPcpPointRecordCollection();
                data.LoadAndCloseReader(DataService.GetReader(qc));
                result.AddRange(data);

                idx += batchLimit;
            }

            return result;
        }

        #endregion

        #region ViewPcpDiscountCampaignLockPoint

        public ViewPcpDiscountCampaignLockPointCollection ViewPcpDiscountCampaignLockPointGetList(string column, object value)
        {
            ViewPcpDiscountCampaignLockPointCollection view = new ViewPcpDiscountCampaignLockPointCollection();
            view.LoadAndCloseReader(ViewPcpDiscountCampaignLockPoint.Query().WHERE(column, value).ExecuteReader());
            return view;
        }

        #endregion ViewPcpDiscountCampaignLockPoint

        #region view_discountDetail

        public ViewDiscountDetailCollection ViewDiscountDetailByCampiagnId(int campaignId, int userId)
        {
            DiscountCampaign dc = DB.Get<DiscountCampaign>(campaignId);
            if (dc.IsLoaded == false || dc.TemplateId == null)
            {
                return new ViewDiscountDetailCollection();
            }
            if (dc.Type != (int)DiscountCampaignType.RegularsTicket &&
                dc.Type != (int)DiscountCampaignType.FavorTicket)
            {
                return new ViewDiscountDetailCollection();
            }

            DiscountTemplate template = this.DiscountTemplateGet((int)dc.TemplateId);
            if (template.SellerUserId != userId)
            {
                return new ViewDiscountDetailCollection();
            }

            return DB.Select().From<ViewDiscountDetail>()
                .Where(ViewDiscountDetail.Columns.Id)
                .IsEqualTo(campaignId)
                .ExecuteAsCollection<ViewDiscountDetailCollection>();
        }

        #endregion view_discountDetail

        #region PcpPosTransLog

        public PcpPosTransLog PcpPosTransLogSet(PcpPosTransLog pcpPosTransLog)
        {
            DB.Save(pcpPosTransLog);
            return pcpPosTransLog;
        }


        #endregion

        #region PcpQueueTicket

        public int PcpQueueTicketGetNumber(string character, DateTime ticketDate)
        {
            var sql = @"declare @number int;
update pcp_queue_ticket set @number=queue_number=queue_number+1
where ticket_code = @character and ticket_date = @ticketDate;

if(@@ROWCOUNT = 0) begin
	Insert into pcp_queue_ticket values(@character, @ticketDate, 1);
	set @number = 1;
end

select @number";

            var qc = new QueryCommand(sql, PcpQueueTicket.Schema.Provider.Name);
            qc.AddParameter("@character", character, DbType.String);
            qc.AddParameter("@ticketDate", ticketDate, DbType.DateTime);

            return (int)DataService.ExecuteScalar(qc);
        }

        #endregion

        #region user_get_card_daily_report
        public UserGetCardDailyReportCollection UserGetCardDailyReportGetList(DateTime d)
        {
            string sql = string.Format(@"
select umc.card_group_id,mcg.seller_user_id,COUNT(umc.card_group_id) as total_member_count
,SUM(IIF(request_time>=@start and request_time < @end,1,0))  as daily_new_member_count 
from user_membership_card umc
JOIN membership_card_group mcg on mcg.id=umc.card_group_id
where enabled=1 
GROUP BY umc.card_group_id,mcg.seller_user_id
");

            QueryCommand qc = new QueryCommand(sql, UserGetCardDailyReport.Schema.Provider.Name);
            qc.AddParameter("start", d, DbType.Date);
            qc.AddParameter("end", d.AddDays(1), DbType.Date);

            var data = new UserGetCardDailyReportCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));

            return data.Clone();
        }

        public UserGetCardDailyReportCollection UserGetCardDailyReportGet(DateTime d)
        {
            return DB.SelectAllColumnsFrom<UserGetCardDailyReport>()
                        .Where(UserGetCardDailyReport.Columns.ReportDate).IsEqualTo(d)
                        .ExecuteAsCollection<UserGetCardDailyReportCollection>();
        }

        public UserGetCardDailyReportCollection UserGetCardDailyReportGetList(string orderBy, params string[] filter)
        {
            return SSHelper.GetQueryResultByFilterOrder<UserGetCardDailyReport, UserGetCardDailyReportCollection>(0, -1, orderBy, filter);
        }

        public int UserGetCardDailyReportSet(UserGetCardDailyReportCollection data)
        {
            return DB.SaveAll(data);
        }
        #endregion

        #region vbs_company_detail 

        public VbsCompanyDetail VbsCompanyDetailGet(Guid sellerGuid)
        {
            return DB.Get<VbsCompanyDetail>(VbsCompanyDetail.Columns.SellerGuid, sellerGuid);
        }

        public VbsCompanyDetail VbsCompanyDetailSet(VbsCompanyDetail data)
        {
            DB.Save<VbsCompanyDetail>(data);
            return data;
        }

        #endregion

        #region ViewMemberShipCardStore
        public ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGet(int regionId, int category, double latitude, double longitude, MembershipCardGroupOrderby orderBy, int membershipCardId = 0)
        {
            var result = new ViewMembershipCardStoreCollection();

            string sql = string.Format(@"
                select * from view_membership_card_store
                    where getdate() > open_time 
                    and getdate() < close_time 
                    and card_level in (0,1)");

            if (category != 0)
            {
                sql += " and EXISTS(select * from view_membership_card_group_split where category=@Category and id=card_group_id) ";
            }

            if (regionId != 0)
            {
                sql += " and township_id in (select id from city where parent_id = @regionId )";
            }
            //else
            //{
            //    sql += string.Format(" and 10 > (geographic.STDistance (geography::STGeomFromText('POINT ({0} {1})', 4326)))/1000"
            //            , longitude, latitude);
            //}

            if (membershipCardId != 0)
            {
                sql += string.Format(" and card_id  = @cardId ");
            }

            switch (orderBy)
            {
                case MembershipCardGroupOrderby.Hot:
                    sql += " ORDER BY hot_point DESC ";
                    break;
                case MembershipCardGroupOrderby.Close:
                    sql += string.Format("order by geographic.STDistance (geography::STGeomFromText('POINT ({0} {1})', 4326))"
                        , longitude, latitude);
                    break;
                case MembershipCardGroupOrderby.PublishTime: //最新上架
                    sql += "ORDER BY publish_time DESC";
                    break;
            }


            QueryCommand qc = new QueryCommand(sql, ViewMembershipCardStore.Schema.Provider.Name);
            qc.AddParameter("@Category", category, DbType.String);
            qc.AddParameter("@regionId", regionId, DbType.Int32);

            if (membershipCardId != 0)
            {
                qc.AddParameter("@cardId", membershipCardId, DbType.Int32);
            }
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGetByPromo()
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCardStore>()
                        .Where(ViewMembershipCardStore.Columns.IsPromo).IsEqualTo(true)
                        .And(ViewMembershipCardStore.Columns.OpenTime).IsLessThanOrEqualTo(DateTime.Now)
                        .And(ViewMembershipCardStore.Columns.CloseTime).IsGreaterThan(DateTime.Now)
                        .And(ViewMembershipCardStore.Columns.Status).IsEqualTo((byte)MembershipCardStatus.Open)
                        .OrderDesc(ViewMembershipCardStore.Columns.PublishTime)
                        .ExecuteAsCollection<ViewMembershipCardStoreCollection>();
        }

        public ViewMembershipCardStoreCollection ViewMembershipCardStoreCollectionGetGroupIds(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCardStore>()
                        .Where(ViewMembershipCardStore.Columns.CardGroupId).In(cardGroupId)
                        .ExecuteAsCollection<ViewMembershipCardStoreCollection>();
        }

        public ViewMembershipCardStoreCollection ViewMembershipCardStoreGetByLastGroupId(int cardGroupId)
        {
            var result = new ViewMembershipCardStoreCollection();

            string sql = string.Format(@"
select * from view_membership_card_store
where @time BETWEEN open_time and close_time and card_group_id = @cardGroupId
            ");

            QueryCommand qc = new QueryCommand(sql, ViewMembershipCardStore.Schema.Provider.Name);
            qc.AddParameter("@time", DateTime.Now, DbType.DateTime);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int16);
            //qc.AddParameter("@cardLevel", (int)MembershipCardLevel.Level1, DbType.Int16);

            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }

        public ViewMembershipCardStoreCollection ViewMembershipCardStoreGetByHotTenGroup()
        {
            var result = new ViewMembershipCardStoreCollection();

            string sql = string.Format(@"
select * from view_membership_card_store with(nolock)
where @time BETWEEN open_time and close_time and card_group_id in (
select top 10 id from membership_card_group g with(nolock)
where exists(select * from membership_card c with(nolock) where c.card_group_id = g.id
and c.status = 1 and c.enabled = 1
and @time between c.open_time and c.close_time 
)
order by g.hot_point desc
)
            ");

            QueryCommand qc = new QueryCommand(sql, ViewMembershipCardStore.Schema.Provider.Name);
            qc.AddParameter("@time", DateTime.Now, DbType.DateTime);
            //qc.AddParameter("@cardLevel", (int)MembershipCardLevel.Level1, DbType.Int16);

            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }
        #endregion

        #region view_vbs_pcp_detail
        public ViewVbsPcpDetail ViewVbsPcpDetailGet(Guid sellerGuid)
        {
            var data = DB.SelectAllColumnsFrom<ViewVbsPcpDetail>()
                     .Where(ViewVbsPcpDetail.Columns.SellerGuid).IsEqualTo(sellerGuid)
                     .ExecuteAsCollection<ViewVbsPcpDetailCollection>();
            return data.Any() ? data.First() : new ViewVbsPcpDetail();
        }
        #endregion

        #region MembershipCardReferee membership_card_referee

        public bool MembershipCardRefereeSet(MembershipCardReferee m)
        {
            return DB.Save<MembershipCardReferee>(m) > 1;
        }

        #endregion

        #region pcp_image

        public bool PcpImageSet(PcpImage image)
        {
            DB.Save(image);
            return true;
        }

        public bool PcpImageSet(PcpImageCollection images)
        {
            DB.SaveAll(images);
            return true;
        }
        public PcpImageCollection PcpImageGet()
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                    .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                    .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImageCollection PcpImageGet(List<int> cardGroupIds, PcpImageType imageType)
        {
            return imageType == PcpImageType.None
                ? DB.SelectAllColumnsFrom<PcpImage>()
                    .Where(PcpImage.Columns.GroupId).In(cardGroupIds)
                    .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                    .ExecuteAsCollection<PcpImageCollection>().OrderByAsc(PcpImage.Columns.ImageType)
                : DB.SelectAllColumnsFrom<PcpImage>()
                    .Where(PcpImage.Columns.GroupId).In(cardGroupIds)
                    .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                    .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                    .ExecuteAsCollection<PcpImageCollection>();
        }
        public PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                     .Where(PcpImage.Columns.GroupId).IsEqualTo(cardGroupId)
                     .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                     .OrderAsc(PcpImage.Columns.Sequence)
                     .ExecuteAsCollection<PcpImageCollection>();
        }
        public PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                     .Where(PcpImage.Columns.GroupId).IsEqualTo(cardGroupId)
                     .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                     .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                     .OrderAsc(PcpImage.Columns.Sequence)
                     .ExecuteAsCollection<PcpImageCollection>();
        }
        public PcpImageCollection PcpImageGet(int sellerUserId, PcpImageType imageType)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                     .Where(PcpImage.Columns.SellerUserId).IsEqualTo(sellerUserId)
                     .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                     .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                     .OrderAsc(PcpImage.Columns.Sequence)
                     .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType, int seq)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                        .Where(PcpImage.Columns.GroupId).IsEqualTo(cardGroupId)
                        .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                        .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                        .And(PcpImage.Columns.Sequence).IsEqualTo(seq)
                        .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImageCollection PcpImageGetByCardGroupId(int cardGroupId, PcpImageType imageType, List<int> ids)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                        .Where(PcpImage.Columns.GroupId).IsEqualTo(cardGroupId)
                        .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                        .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                        .And(PcpImage.Columns.Id).In(ids)
                        .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImageCollection PcpImageGet(int sellerUserId, PcpImageType imageType, int seq)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                        .Where(PcpImage.Columns.SellerUserId).IsEqualTo(sellerUserId)
                        .And(PcpImage.Columns.ImageType).IsEqualTo((byte)imageType)
                        .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                        .And(PcpImage.Columns.Sequence).IsEqualTo(seq)
                        .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImage PcpImageGet(int imageId)
        {
            return DB.Get<PcpImage>(imageId);
        }

        public PcpImageCollection PcpImageGetIsDelete(DateTime d)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                        .Where(PcpImage.Columns.IsDelete).IsEqualTo(true)
                        .And(PcpImage.Columns.CreateTime).IsLessThan(d)
                        .ExecuteAsCollection<PcpImageCollection>();
        }

        public PcpImageCollection PcpImageGetUnCompress(int[] types)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                        .Where(PcpImage.Columns.IsDelete).IsEqualTo(false)
                        .And(PcpImage.Columns.ImageType).In(types)
                        .ExecuteAsCollection<PcpImageCollection>();
        }

        public bool PcpImageDelete(int imageId, int userId)
        {
            var image = PcpImageGet(imageId);

            image.IsDelete = true;
            image.ModifyUserId = userId;
            image.ModifyTime = DateTime.Now;

            return PcpImageSet(image);
        }

        public bool PcpImageDelete(int groupId, int editUserId, PcpImageType imgType, int seq = 1)
        {
            var image = PcpImageGetByCardGroupId(groupId, imgType, seq);

            image.ForEach(x =>
            {
                x.IsDelete = true;
                x.ModifyUserId = editUserId;
                x.ModifyTime = DateTime.Now;
            });

            return PcpImageSet(image);
        }

        public bool PcpImageDelete(int groupId, int editUserId, PcpImageType imgType, List<int> ids)
        {
            var image = PcpImageGetByCardGroupId(groupId, imgType, ids);

            image.ForEach(x =>
            {
                x.IsDelete = true;
                x.ModifyUserId = editUserId;
                x.ModifyTime = DateTime.Now;
            });

            return PcpImageSet(image);
        }

        public int PcpImageDelete(int[] ids)
        {
            return DB.Delete().From<PcpImage>()
                .Where(PcpImage.Columns.Id).In(ids)
                .Execute();
        }

        public PcpImageCollection PcpImageGetWithNonCompressFile(List<int> compressType)
        {
            return DB.SelectAllColumnsFrom<PcpImage>()
                    .Where(PcpImage.Columns.ImageType).In(compressType)
                    .And(PcpImage.Columns.ImageUrlCompressed).IsNull()
                    .And(PcpImage.Columns.IsDelete).IsEqualTo(false)
                    .ExecuteAsCollection<PcpImageCollection>();
        }

        #endregion

        #region pcp_intro

        public bool PcpIntroSet(PcpIntro intro)
        {
            DB.Save(intro);
            return true;
        }

        public bool PcpIntroSet(PcpIntroCollection introsCol)
        {
            DB.BulkInsert(introsCol);
            return true;
        }
        public bool PcpIntroDeleteByCardGroupId(PcpIntroType type, int cardGroupId)
        {
            DB.Delete().From(PcpIntro.Schema.TableName)
                       .Where(PcpIntro.Columns.GroupId).IsEqualTo(cardGroupId)
                       .And(PcpIntro.Columns.IntroType).IsEqualTo((byte)type)
                       .Execute();
            return true;
        }
        public bool PcpIntroDelete(PcpIntroType type, int groupId)
        {
            DB.Delete().From(PcpIntro.Schema.TableName)
                .Where(PcpIntro.Columns.GroupId).IsEqualTo(groupId)
                .And(PcpIntro.Columns.IntroType).IsEqualTo((byte)type)
                .Execute();
            return true;
        }

        public PcpIntroCollection PcpIntroGetByCardGroupId(PcpIntroType type, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<PcpIntro>()
                .Where(PcpIntro.Columns.GroupId).IsEqualTo(cardGroupId)
                .And(PcpIntro.Columns.IntroType).IsEqualTo((byte)type)
                .ExecuteAsCollection<PcpIntroCollection>();
        }

        public PcpIntroCollection PcpIntroGet(PcpIntroType type, int groupId)
        {
            if (groupId == 0)
                return new PcpIntroCollection();

            return DB.SelectAllColumnsFrom<PcpIntro>()
                .Where(PcpIntro.Columns.GroupId).IsEqualTo(groupId)
                .And(PcpIntro.Columns.IntroType).IsEqualTo((byte)type)
                .ExecuteAsCollection<PcpIntroCollection>();
        }

        #endregion

        #region pcp_marquee
        public PcpMarquee PcpMarqueeGet(int cardGroupId, int id)
        {
            return DB.SelectAllColumnsFrom<PcpMarquee>().NoLock()
                     .Where(PcpMarquee.IdColumn).IsEqualTo(id)
                     .And(PcpMarquee.GroupIdColumn).IsEqualTo(cardGroupId)
                     .ExecuteSingle<PcpMarquee>();
        }

        public PcpMarqueeCollection PcpMarqueeGetListByCardGroupId(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<PcpMarquee>().NoLock()
                     .Where(PcpMarquee.GroupIdColumn).IsEqualTo(cardGroupId)
                     .OrderAsc(PcpMarquee.Columns.Id)
                     .ExecuteAsCollection<PcpMarqueeCollection>();
        }

        public bool PcpMarqueeSet(PcpMarquee marquee)
        {
            return DB.Save(marquee) > 0;
        }

        public bool PcpMarqueeDelete(int cardGroupId, int id)
        {
            return DB.Delete().From<PcpMarquee>()
                     .Where(PcpMarquee.GroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpMarquee.IdColumn).IsEqualTo(id)
                     .Execute() > 0;
        }
        #endregion

        #region pcp_point_collect_rule
        public PcpPointCollectRule PcpPointCollectRuleGet(int cardGroupId, int id)
        {
            return DB.SelectAllColumnsFrom<PcpPointCollectRule>().NoLock()
                     .Where(PcpPointCollectRule.IdColumn).IsEqualTo(id)
                     .And(PcpPointCollectRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                     .ExecuteSingle<PcpPointCollectRule>();
        }
        public PcpPointCollectRuleCollection PcpPointCollectRuleGetListByCardGroupId(int cardGroupId)
        {
            PcpPointCollectRuleCollection collection = new PcpPointCollectRuleCollection();
            var result = DB.SelectAllColumnsFrom<PcpPointCollectRule>().NoLock()
                           .Where(PcpPointCollectRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                           .OrderAsc(PcpPointCollectRule.Columns.CardGroupId)
                           .ExecuteAsCollection<PcpPointCollectRuleCollection>();
            if (result != null)
            {
                collection = result;
            }
            return collection;
        }
        public bool PcpPointCollectRuleSet(PcpPointCollectRule rule)
        {
            return DB.Save(rule) > 0;
        }
        public bool PcpPointCollectRuleDelete(int cardGroupId, int id)
        {
            return DB.Delete().From<PcpPointCollectRule>()
                     .Where(PcpPointCollectRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpPointCollectRule.IdColumn).IsEqualTo(id)
                     .Execute() > 0;
        }
        #endregion

        #region pcp_point_exchange_rule
        public PcpPointExchangeRule PcpPointExchangeRuleGet(int cardGroupId, int id)
        {
            return DB.SelectAllColumnsFrom<PcpPointExchangeRule>().NoLock()
                     .Where(PcpPointExchangeRule.IdColumn).IsEqualTo(id)
                     .And(PcpPointExchangeRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                     .ExecuteSingle<PcpPointExchangeRule>();
        }
        public PcpPointExchangeRuleCollection PcpPointExchangeRuleGetListByCardGroupId(int cardGroupId)
        {
            PcpPointExchangeRuleCollection collection = new PcpPointExchangeRuleCollection();
            var result = DB.SelectAllColumnsFrom<PcpPointExchangeRule>().NoLock()
                           .Where(PcpPointExchangeRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                           .And(PcpPointExchangeRule.IsDeleteColumn).IsEqualTo(false)
                           .OrderAsc(PcpPointExchangeRule.Columns.Id)
                           .ExecuteAsCollection<PcpPointExchangeRuleCollection>();
            if (result != null)
            {
                collection = result;
            }
            return collection;
        }
        public bool PcpPointExchangeRuleSet(PcpPointExchangeRule rule)
        {
            return DB.Save(rule) > 0;
        }
        public bool PcpPointExchangeRuleDelete(int cardGroupId, int id)
        {
            return DB.Update<PcpPointExchangeRule>()
                     .Set(PcpPointExchangeRule.IsDeleteColumn).EqualTo(true)
                     .Where(PcpPointExchangeRule.CardGroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpPointExchangeRule.IdColumn).IsEqualTo(id)
                     .Execute() > 0;
        }
        #endregion

        #region pcp_point_rule_change_log
        public PcpPointRuleChangeLogCollection PcpPointRuleChangeLogGetListByCardGroupId(int cardGroupId)
        {
            PcpPointRuleChangeLogCollection collection = new PcpPointRuleChangeLogCollection();
            var result = DB.SelectAllColumnsFrom<PcpPointRuleChangeLog>().NoLock()
                           .Where(PcpPointRuleChangeLog.GroupIdColumn).IsEqualTo(cardGroupId)
                           .OrderDesc(PcpPointRuleChangeLog.Columns.ModifyTime)
                           .ExecuteAsCollection<PcpPointRuleChangeLogCollection>();
            if (result != null)
            {
                collection = result;
            }
            return collection;
        }
        public bool PcpPointRuleChangeLogSet(PcpPointRuleChangeLog log)
        {
            return DB.Save(log) > 0;
        }
        #endregion

        #region pcp_point_remark
        public PcpPointRemark PcpPointRemarkGet(int cardGroupId, int id)
        {
            return DB.SelectAllColumnsFrom<PcpPointRemark>().NoLock()
                     .Where(PcpPointRemark.IdColumn).IsEqualTo(id)
                     .And(PcpPointRemark.GroupIdColumn).IsEqualTo(cardGroupId)
                     .ExecuteSingle<PcpPointRemark>();
        }
        public PcpPointRemarkCollection PcpPointRemarkGetListByCardGroupId(int cardGroupId)
        {
            PcpPointRemarkCollection collection = new PcpPointRemarkCollection();
            var result = DB.SelectAllColumnsFrom<PcpPointRemark>().NoLock()
                           .Where(PcpPointRemark.GroupIdColumn).IsEqualTo(cardGroupId)
                           .OrderAsc(PcpPointRemark.Columns.Id)
                           .ExecuteAsCollection<PcpPointRemarkCollection>();
            if (result != null)
            {
                collection = result;
            }
            return collection;
        }
        public bool PcpPointRemarkSet(PcpPointRemark rule)
        {
            return DB.Save(rule) > 0;
        }
        public bool PcpPointRemarkDelete(int cardGroupId, int id)
        {
            return DB.Delete().From<PcpPointRemark>()
                     .Where(PcpPointRemark.GroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpPointRemark.IdColumn).IsEqualTo(id)
                     .Execute() > 0;
        }
        #endregion

        #region pcp_user_point
        public bool PcpUserPointSet(PcpUserPoint point)
        {
            return DB.Save(point) > 0;
        }
        public PcpUserPointCollection PcpUserPointListGetByCardGroupId(int cardGroupId, int userId)
        {
            return DB.SelectAllColumnsFrom<PcpUserPoint>().NoLock()
                              .Where(PcpUserPoint.CardGroupIdColumn).IsEqualTo(cardGroupId)
                              .And(PcpUserPoint.UserIdColumn).IsEqualTo(userId)
                              //TODO:點數有效日期的檢查 .And(PcpUserPoint.ExpireTimeColumn)
                              .OrderAsc(PcpUserPoint.Columns.Id)
                              .ExecuteAsCollection<PcpUserPointCollection>();
        }

        /// <summary>
        /// 取得使用者當前點數
        /// </summary>
        public PcpUserPointCollection PcpUserRemainPointListGetByGroupIdAndUserId(int cardGroupId, int userId)
        {
            return DB.SelectAllColumnsFrom<PcpUserPoint>().NoLock()
                           .Where(PcpUserPoint.CardGroupIdColumn).IsEqualTo(cardGroupId)
                           .And(PcpUserPoint.UserIdColumn).IsEqualTo(userId)
                           .And(PcpUserPoint.RemainPointColumn).IsGreaterThan(0)
                           .And(PcpUserPoint.ExpireTimeColumn).IsGreaterThan(DateTime.Now)
                           .OrderAsc(PcpUserPoint.Columns.Id)
                           .ExecuteAsCollection<PcpUserPointCollection>();
        }
        #endregion

        #region pcp_user_point_alter_pair
        public bool PcpUserPointAlterPairSet(PcpUserPointAlterPair alterPair)
        {
            return DB.Save(alterPair) > 0;
        }
        #endregion

        #region PcpOrderDetialCollectionSet
        public bool PcpOrderDetialCollectionSet(PcpOrderDetailCollection pcpOrderDetailCollection)
        {
            DB.SaveAll(pcpOrderDetailCollection);
            return true;
        }

        #endregion

        #region PcpOrderDetialCheckDataIsExistByPk
        public bool PcpOrderDetialCheckDataIsExistByPk(int pcpOrderPk)
        {
            return DB.SelectAllColumnsFrom<PcpOrderDetail>().Top("1")
                    .Where(PcpOrderDetail.PcpOrderPkColumn).IsEqualTo(pcpOrderPk)
                    .ExecuteAsCollection<PcpOrderDetailCollection>().Any();
        }

        #endregion

        #region pcp_user_token
        public bool PcpCheckUserTokenIsExistById(int userId, ref PcpUserToken pcpUserToken)
        {
            bool isExist = false;
            pcpUserToken = DB.SelectAllColumnsFrom<PcpUserToken>().Top("1")
                    .Where(PcpUserToken.Columns.UserId).IsEqualTo(userId)
                    .And(PcpUserToken.Columns.ExpiredTime)
                    .IsBetweenAnd(DateTime.Today, DateTime.Today.AddDays(1)) //判斷是否當天
                    .ExecuteSingle<PcpUserToken>();

            if (pcpUserToken.IsLoaded)
            {
                isExist = true;
            }

            return isExist;
        }

        public PcpUserToken PcpUserTokenGetByToken(string userToken)
        {
            return DB.SelectAllColumnsFrom<PcpUserToken>().Top("1")
                    .Where(PcpUserToken.Columns.AccessToken).IsEqualTo(userToken)
                    .Or(PcpUserToken.Columns.RefreshToken).IsEqualTo(userToken)
                    .OrderDesc(PcpUserToken.Columns.ExpiredTime)
                    .ExecuteSingle<PcpUserToken>();
        }

        public bool PcpUserTokenSet(PcpUserToken userToken)
        {
            return DB.Save(userToken) > 0;
        }

        #endregion

        #region pcp_polling_user_status
        public bool PcpPollingUserStatusSet(PcpPollingUserStatus pcpPollingUserStatus)
        {
            return DB.Save(pcpPollingUserStatus) > 0;
        }

        public PcpPollingUserStatus PcpPollingStatusGetByToken(string userToken, bool needCheckAlive)
        {
            var query = DB.SelectAllColumnsFrom<PcpPollingUserStatus>().Top("1")
                           .Where(PcpPollingUserStatus.Columns.UserToken).IsEqualTo(userToken);
            if (needCheckAlive)
            {
                query = query.And(PcpPollingUserStatus.Columns.Alive).IsEqualTo(true);
            }
            query = query.OrderDesc(PcpPollingUserStatus.Columns.ExpiredTime);
            return query.ExecuteSingle<PcpPollingUserStatus>();
        }

        public PcpPollingUserStatusCollection PcpPollingStatusCollectionGetByToken(string userToken)
        {
            return DB.SelectAllColumnsFrom<PcpPollingUserStatus>().Where(PcpPollingUserStatus.Columns.UserToken).IsEqualTo(userToken)
                                                                  .ExecuteAsCollection<PcpPollingUserStatusCollection>();
        } 

        public PcpPollingUserStatus PcpPollingStatusGetById(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpPollingUserStatus>().Top("1")
                   .Where(PcpPollingUserStatus.Columns.UserId).IsEqualTo(userId)
                   .And(PcpPollingUserStatus.Columns.Alive).IsEqualTo(true)
                   .And(PcpPollingUserStatus.Columns.ExpiredTime).IsGreaterThan(DateTime.Now)
                   .OrderDesc(PcpPollingUserStatus.Columns.ExpiredTime)
                   .ExecuteSingle<PcpPollingUserStatus>();
        }

        public PcpPollingUserStatusCollection PcpPollingStatusGetAllById(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpPollingUserStatus>()
                   .Where(PcpPollingUserStatus.Columns.UserId).IsEqualTo(userId)
                   .ExecuteAsCollection<PcpPollingUserStatusCollection>();
        }

        public bool PcpPollingStatusCollectionSet(PcpPollingUserStatusCollection pcpUserStatusCollection)
        {
            return DB.SaveAll(pcpUserStatusCollection) > 0;
        }

        #endregion

        #region pcp_user_agree_status

        public PcpUserAgreeStatus PcpAgreeStatusGetById(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpUserAgreeStatus>().Top("1")
                  .Where(PcpUserAgreeStatus.Columns.UserId).IsEqualTo(userId)
                  .ExecuteSingle<PcpUserAgreeStatus>();
        }

        public bool PcpUserAgreeStatusSet(PcpUserAgreeStatus pcpUserAgreeStatus)
        {
            return DB.Save(pcpUserAgreeStatus) > 0;
        }

        #endregion

        #region pcp_deposit_menu
        public PcpDepositMenu PcpDepositMenuGet(int menuId)
        {
            return DB.SelectAllColumnsFrom<PcpDepositMenu>()
                     .Where(PcpDepositMenu.IdColumn).IsEqualTo(menuId)
                     .ExecuteSingle<PcpDepositMenu>();
        }
        public bool PcpDepositMenuSet(PcpDepositMenu menu)
        {
            return DB.Save(menu) > 0;
        }

        public bool PcpDepositMenuDelete(int cardGroupId, int menuId)
        {
            return DB.Delete().From<PcpDepositMenu>().Where(PcpDepositMenu.GroupIdColumn).IsEqualTo(cardGroupId)
                                                     .And(PcpDepositMenu.IdColumn).IsEqualTo(menuId).Execute() > 0;
        }

        public bool PcpDepositMenuEnable(int cardGroupId, int menuId, int enable)
        {
            return DB.Update<PcpDepositMenu>().Set(PcpDepositMenu.EnabledColumn).EqualTo(enable)
                     .Where(PcpDepositMenu.GroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpDepositMenu.IdColumn).IsEqualTo(menuId).Execute() > 0;
        }

        public PcpDepositMenuCollection PcpDepositMenuGetByGroupId(int groupId)
        {
            return DB.SelectAllColumnsFrom<PcpDepositMenu>().NoLock()
                     .Where(PcpDepositMenu.GroupIdColumn).IsEqualTo(groupId)
                     .And(PcpDepositMenu.EnabledColumn).IsEqualTo(true)
                     .OrderDesc(PcpDepositMenu.Columns.CreatedTime)
                     .ExecuteAsCollection<PcpDepositMenuCollection>();
        }

        #endregion

        #region pcp_deposit_item
        public PcpDepositItem PcpDepositItemGet(int itemId)
        {
            return DB.SelectAllColumnsFrom<PcpDepositItem>()
                     .Where(PcpDepositItem.IdColumn).IsEqualTo(itemId)
                     .ExecuteSingle<PcpDepositItem>();
        }
        public bool PcpDepositItemSet(PcpDepositItem item)
        {
            return DB.Save(item) > 0;
        }
        public bool PcpDepositItemCollectionSet(PcpDepositItemCollection pcpDepositItemCollection)
        {
            return DB.SaveAll(pcpDepositItemCollection) > 0;
        }
        public bool PcpDepositItemDelete(int itemId)
        {
            return DB.Delete().From<PcpDepositItem>().Where(PcpDepositItem.IdColumn).IsEqualTo(itemId).Execute() > 0;
        }
        #endregion

        #region pcp_deposit_template
        public PcpDepositTemplate PcpDepositTemplateGet(int cardGroupId, int id)
        {
            return DB.SelectAllColumnsFrom<PcpDepositTemplate>().NoLock()
                     .Where(PcpDepositTemplate.IdColumn).IsEqualTo(id)
                     .And(PcpDepositTemplate.GroupIdColumn).IsEqualTo(cardGroupId)
                     .ExecuteSingle<PcpDepositTemplate>();
        }

        public bool PcpDepositTemplateSet(PcpDepositTemplate template)
        {
            return DB.Save(template) > 0;
        }
        /// <summary>
        /// 取得當前template排序
        /// </summary>
        /// <param name="cardGroupId"></param>
        /// <returns></returns>
        public int PcpDepositTemplateGetCurrentSeq(int cardGroupId, int type)
        {
            int currentMaxSeq = 0;
            var query = DB.Select(new Aggregate(PcpDepositTemplate.SeqColumn, AggregateFunction.Max))
                          .From(PcpDepositTemplate.Schema.TableName).NoLock()
                          .Where(PcpDepositTemplate.GroupIdColumn).IsEqualTo(cardGroupId)
                          .ExecuteScalar<int?>();
            if (query != null && query != 0)
            {
                currentMaxSeq = Convert.ToInt32(query);
            }
            return currentMaxSeq + 1;
        }

        public List<PcpDepositTemplate> PcpDepositTemplateGetListByCardGroupId(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<PcpDepositTemplate>().NoLock()
                     .Where(PcpDepositTemplate.Columns.GroupId).IsEqualTo(cardGroupId)
                     .OrderAsc(PcpDepositTemplate.Columns.Type, PcpDepositTemplate.Columns.Seq)
                     .ExecuteAsCollection<PcpDepositTemplateCollection>().ToList();
        }

        public bool PcpDepositTemplateDelete(int cardGroupId, int id)
        {
            return DB.Delete().From<PcpDepositTemplate>()
                     .Where(PcpDepositTemplate.GroupIdColumn).IsEqualTo(cardGroupId)
                     .And(PcpDepositTemplate.IdColumn).IsEqualTo(id)
                     .Execute() > 0;
        }
        #endregion

        #region pcp_user_deposit
        public bool PcpUserDepositSet(PcpUserDeposit pcpUserDeposit)
        {
            return DB.Save(pcpUserDeposit) > 0;
        }

        public bool PcpUserDepositCollectionSet(PcpUserDepositCollection pcpUserDepositCollection)
        {
            return DB.SaveAll(pcpUserDepositCollection) > 0;
        }

        public PcpUserDeposit UserDepositItemGetByUserAndId(int userId, int depositId)
        {
            return DB.SelectAllColumnsFrom<PcpUserDeposit>().NoLock()
             .Where(PcpUserDeposit.Columns.UserId).IsEqualTo(userId)
             .And(PcpUserDeposit.Columns.Id).IsEqualTo(depositId)
             .ExecuteSingle<PcpUserDeposit>();
        }

        #endregion

        #region Pcp_deposit_coupon

        public PcpDepositCouponCollection PcpDepositCouponCollectionGet(int depositId)
        {
            return DB.SelectAllColumnsFrom<PcpDepositCoupon>()
              .Where(PcpDepositCoupon.Columns.UserDepositItemId).IsEqualTo(depositId)
              .ExecuteAsCollection<PcpDepositCouponCollection>();
        }

        public bool PcpDepositCouponCollectionSet(PcpDepositCouponCollection pcpDepositCouponCollection)
        {
            DB.SaveAll(pcpDepositCouponCollection);
            return true;
        }

        #endregion

        #region pcp_user_deposit_log
        public bool PcpUserDepositLogSet(PcpUserDepositLog pcpUserDepositLog)
        {
            return DB.Save(pcpUserDepositLog) > 0;
        }

        public PcpUserDepositLog PcpUserDepositLogGetByUSerIdOrderByTime(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpUserDepositLog>().Top("1").NoLock()
                  .Where(PcpUserDepositLog.Columns.UserId).IsEqualTo(userId)
                  .OrderDesc(PcpUserDepositLog.Columns.CreateTime)
                  .ExecuteSingle<PcpUserDepositLog>();
        }

        public PcpUserDepositLogCollection PcpUserDepositLogDetailGetByUSerId(int userId)
        {
            return DB.SelectAllColumnsFrom<PcpUserDepositLog>().NoLock()
                  .Where(PcpUserDepositLog.Columns.UserId).IsEqualTo(userId)
                  .And(PcpUserDepositLog.Columns.CreateTime).IsGreaterThan(DateTime.Now.AddYears(-1)) //回傳一年以內的log
                  .OrderDesc(PcpUserDepositLog.Columns.CreateTime)
                  .ExecuteAsCollection<PcpUserDepositLogCollection>();
        }


        #endregion

        #region view_pcp_seller_menu_item

        public ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupId(int cardGroupId)
        {
            DateTime now = DateTime.Now;
            return DB.SelectAllColumnsFrom<ViewPcpSellerMenuItem>().NoLock()
                     .Where(ViewPcpSellerMenuItem.Columns.GroupId).IsEqualTo(cardGroupId)
                     .OrderDesc(ViewPcpSellerMenuItem.Columns.MenuId)
                     .ExecuteAsCollection<ViewPcpSellerMenuItemCollection>();
        }

        public ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupIdInEffective(int cardGroupId)
        {
            DateTime now = DateTime.Now;
            return DB.SelectAllColumnsFrom<ViewPcpSellerMenuItem>().NoLock()
                     .Where(ViewPcpSellerMenuItem.Columns.GroupId).IsEqualTo(cardGroupId)
                     .And(ViewPcpSellerMenuItem.Columns.Enabled).IsEqualTo(1)
                     .And(ViewPcpSellerMenuItem.Columns.STime).IsLessThanOrEqualTo(now)
                     .And(ViewPcpSellerMenuItem.Columns.ETime).IsGreaterThan(now)
                     .ExecuteAsCollection<ViewPcpSellerMenuItemCollection>();
        }

        public ViewPcpSellerMenuItemCollection ViewPcpSellerMenuItemCollectionGetByCardGroupIdAndMenuId(int cardGroupId, int menuId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpSellerMenuItem>().NoLock()
                     .Where(ViewPcpSellerMenuItem.Columns.GroupId).IsEqualTo(cardGroupId)
                     .And(ViewPcpSellerMenuItem.Columns.MenuId).IsEqualTo(menuId)
                     .ExecuteAsCollection<ViewPcpSellerMenuItemCollection>();
        }

        #endregion

        #region view_pcp_user_deposit
        public ViewPcpUserDepositCollection ViewPcpUserDepositGetDepositTotalAndRemain(int cardGroupId)
        {
            return DB.Select(
                new Aggregate(ViewPcpUserDeposit.Columns.MenuId, ViewPcpUserDeposit.Columns.MenuId, AggregateFunction.GroupBy),
                new Aggregate(ViewPcpUserDeposit.Columns.TotalAmount, ViewPcpUserDeposit.Columns.TotalAmount, AggregateFunction.Sum),
                new Aggregate(ViewPcpUserDeposit.Columns.RemainAmount, ViewPcpUserDeposit.Columns.RemainAmount, AggregateFunction.Sum)
              )
            .From(ViewPcpUserDeposit.Schema.TableName).NoLock()
            .Where(ViewPcpUserDeposit.Columns.GroupId).IsEqualTo(cardGroupId)
            .ExecuteAsCollection<ViewPcpUserDepositCollection>();
        }
        public ViewPcpUserDeposit ViewPcpUserDepositGetDepositTotalAndRemain(int cardGroupId, int menuId)
        {
            return DB.Select(
                new Aggregate(ViewPcpUserDeposit.Columns.TotalAmount, ViewPcpUserDeposit.Columns.TotalAmount, AggregateFunction.Sum),
                new Aggregate(ViewPcpUserDeposit.Columns.RemainAmount, ViewPcpUserDeposit.Columns.RemainAmount, AggregateFunction.Sum)
              )
            .From(ViewPcpUserDeposit.Schema.TableName).NoLock()
            .Where(ViewPcpUserDeposit.Columns.GroupId).IsEqualTo(cardGroupId)
            .And(ViewPcpUserDeposit.Columns.MenuId).IsEqualTo(menuId)
            .ExecuteSingle<ViewPcpUserDeposit>();
        }

        public ViewPcpUserDepositCollection ViewPcpUserDepositCollectionGetByUserAndCardGroup(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDeposit>().NoLock()
                     .Where(ViewPcpUserDeposit.Columns.UserId).IsEqualTo(userId)
                     .And(ViewPcpUserDeposit.Columns.GroupId).IsEqualTo(cardGroupId)
                     .OrderDesc(ViewPcpUserDeposit.Columns.CreateTime)
                     .ExecuteAsCollection<ViewPcpUserDepositCollection>();
        }

        public ViewPcpUserDeposit ViewPcpUserDepositCollectionGetByUserAndDepositId(int userId, int depositId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDeposit>().NoLock()
                     .Where(ViewPcpUserDeposit.Columns.UserId).IsEqualTo(userId)
                     .And(ViewPcpUserDeposit.Columns.UserDepositId).IsEqualTo(depositId)
                     .ExecuteSingle<ViewPcpUserDeposit>();
        }

        #endregion

        #region view_pcp_user_deposit_coupon
        public ViewPcpUserDepositCouponCollection ViewPcpUserDepositCouponCollectionGetByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDepositCoupon>()
             .Where(ViewPcpUserDepositCoupon.Columns.UserId).IsEqualTo(userId)
             .And(ViewPcpUserDepositCoupon.Columns.GroupId).IsEqualTo(cardGroupId)
             .ExecuteAsCollection<ViewPcpUserDepositCouponCollection>();
        }
        #endregion

        #region view_pcp_user_deposit_log

        public ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByUserIdAndCardGroupId(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDepositLog>()
                     .Where(ViewPcpUserDepositLog.Columns.UserId).IsEqualTo(userId)
                     .And(ViewPcpUserDepositLog.Columns.GroupId).IsEqualTo(cardGroupId)
                     .ExecuteAsCollection<ViewPcpUserDepositLogCollection>();
        }

        public ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDepositLog>()
                     .Where(ViewPcpUserDepositLog.Columns.UserId).IsEqualTo(userId)
                     .ExecuteAsCollection<ViewPcpUserDepositLogCollection>();
        }

        public ViewPcpUserDepositLogCollection ViewPcpUserDepositLogCollectionGetByCardGroupId(int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserDepositLog>()
                      .Where(ViewPcpUserDepositLog.Columns.GroupId).IsEqualTo(cardGroupId)
                      .ExecuteAsCollection<ViewPcpUserDepositLogCollection>();
        }
        #endregion

        #region view_pcp_user_point

        public ViewPcpUserPointCollection ViewPcpUserPointCollectionGet(int userId, int cardGroupId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserPoint>().NoLock()
                   .Where(ViewPcpUserPoint.Columns.UserId).IsEqualTo(userId)
                   .And(ViewPcpUserPoint.Columns.CardGroupId).IsEqualTo(cardGroupId)
                   .ExecuteAsCollection<ViewPcpUserPointCollection>();

        }

        public ViewPcpUserPointCollection ViewPcpUserPointCollectionGetByUserId(int userId)
        {
            return DB.SelectAllColumnsFrom<ViewPcpUserPoint>().NoLock()
                     .Where(ViewPcpUserPoint.Columns.UserId).IsEqualTo(userId)
                     .OrderDesc(ViewPcpUserPoint.Columns.UserPointId)
                     .ExecuteAsCollection<ViewPcpUserPointCollection>();
        }
        

        public ViewPcpUserPointCollection ViewPcpUserPointCollectionGetByCardGroupId(int cardGroupId, Guid? sellerGuid, DateTime? queryTime, int pageNumber, int pageSize)
        {
            string sql = @"select * from view_pcp_user_point with(nolock) ";
            sql += "where card_group_id = @cardGroupId ";

            if (sellerGuid.HasValue)
                sql += "and seller_guid = @sellerGuid ";
            if (queryTime.HasValue)
                sql += "and create_time >= @queryTime ";
            
            sql += "order by " + ViewPcpUserPoint.Columns.UserPointId + " desc";

            QueryCommand qc = new QueryCommand(sql, OrderDetail.Schema.Provider.Name);
            qc.AddParameter("@cardGroupId", cardGroupId, DbType.Int32);

            if (sellerGuid.HasValue)
                qc.AddParameter("@sellerGuid", sellerGuid.Value, DbType.Guid);
            if (queryTime.HasValue)
                qc.AddParameter("@queryTime", queryTime.Value, DbType.DateTime);

            //qc = SSHelper.MakePagable(qc, pageNumber, pageSize, ViewPcpUserPoint.Columns.UserPointId + " desc");
            ViewPcpUserPointCollection data = new ViewPcpUserPointCollection();
            data.LoadAndCloseReader(DataService.GetReader(qc));
            return data;
        }

        #endregion

        #region MembershipCardGroupPermission

        public bool MembershipCardGroupPermissionSet(MembershipCardGroupPermissionCollection mpc)
        {
            return DB.SaveAll(mpc) > 0;
        }

        public MembershipCardGroupPermissionCollection MembershipCardGroupPermissionGetList(int cardGroupId)
        {
            MembershipCardGroupPermissionCollection collection = new MembershipCardGroupPermissionCollection();
            var query = DB.SelectAllColumnsFrom<MembershipCardGroupPermission>().NoLock()
                          .Where(MembershipCardGroupPermission.CardGroudIdColumn).IsEqualTo(cardGroupId)
                          .ExecuteAsCollection<MembershipCardGroupPermissionCollection>();
            if (query != null)
            {
                collection = query;
            }
            return collection;
        }

        public MembershipCardGroupPermission MembershipCardGroupPermissionGet(int cardGroupId, MembershipService service)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupPermission>().NoLock()
                 .Where(MembershipCardGroupPermission.CardGroudIdColumn).IsEqualTo(cardGroupId)
                 .And(MembershipCardGroupPermission.ScopeColumn).IsEqualTo((int)service)
                 .ExecuteAsCollection<MembershipCardGroupPermissionCollection>().FirstOrDefault() ?? new MembershipCardGroupPermission();
        }

        public void MembershipCardGroupPermissionDel(MembershipCardGroupPermission p)
        {
            DB.Delete(p);
        }

        public void MembershipCardGroupPermissionDelAll(int cardGroupId)
        {
            DB.Delete<MembershipCardGroupPermission>(MembershipCardGroupPermission.Columns.CardGroudId, cardGroupId);
        }

        public MembershipCardGroupPermissionCollection MembershipCardGroupPermissionGetList(List<int> cardGroupIds)
        {
            return DB.SelectAllColumnsFrom<MembershipCardGroupPermission>().NoLock()
                 .Where(MembershipCardGroupPermission.CardGroudIdColumn).In(cardGroupIds)
                 .ExecuteAsCollection<MembershipCardGroupPermissionCollection>();
        }

        #endregion

        #region user_get_card_daily_report
        public UserGetCardDailyReportCollection ImmediatelyCountUserGetCard(int cardGroupId, DateTime start, DateTime end)
        {
            string sql = @"
--DECLARE @StartDate date = DATEADD(DAY, -6, GETDATE());
--DECLARE @EndDate date = GETDATE();

;WITH cte AS (
    SELECT @StartDate AS report_date
    UNION ALL
    SELECT DATEADD(day,1,report_date) as report_date
    FROM cte
    WHERE DATEADD(day,1,report_date) <=  @EndDate
), cte2 AS (
	SELECT G.id AS [card_group_id] FROM membership_card_group G WITH (NOLOCK)
)
SELECT cte2.card_group_id, cte.report_date,
	   [total_member_count] = (--累積
			SELECT COUNT(*) FROM user_membership_card C WITH (NOLOCK)
			 WHERE C.card_group_id = cte2.card_group_id
			   AND C.request_time < DATEADD(day, 1, cte.report_date)
	   ),
	   [daily_new_member_count] = (--當日
			SELECT COUNT(*) FROM user_membership_card D WITH (NOLOCK)
			 WHERE D.card_group_id = cte2.card_group_id
			   AND D.request_time >= cte.report_date AND D.request_time < DATEADD(day, 1, cte.report_date)
	   )
  FROM cte CROSS JOIN cte2
WHERE cte2.card_group_id = @CardGroupId
ORDER BY cte.report_date
OPTION (MAXRECURSION 0)
";
            QueryCommand qc = new QueryCommand(sql, ViewMembershipCard.Schema.Provider.Name);
            qc.AddParameter("CardGroupId", cardGroupId, DbType.Int32);
            qc.AddParameter("StartDate", start, DbType.Date);
            qc.AddParameter("EndDate", end, DbType.Date);

            var result = new UserGetCardDailyReportCollection();
            result.LoadAndCloseReader(DataService.GetReader(qc));
            return result;
        }
        #endregion

        #region pcp_category_image

        public PcpCategoryImageCollection PcpCategoryImageCollectionGet()
        {
            return DB.SelectAllColumnsFrom<PcpCategoryImage>().NoLock()
                  .ExecuteAsCollection<PcpCategoryImageCollection>();
        }

        #endregion

        #region view_membership_card_available_service
        public ViewMembershipCardAvailableServiceCollection PcpViewMembershipCardAvailableServiceCollectionGet()
        {
            return DB.SelectAllColumnsFrom<ViewMembershipCardAvailableService>().NoLock()
                  .ExecuteAsCollection<ViewMembershipCardAvailableServiceCollection>();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;
using LunchKingSite.Core.ModelCustom;

namespace LunchKingSite.SsBLL.Provider
{
    public class ChannelEventProvider : IChannelEventProvider
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        #region PrizeDrawEvent

        public int InsertPrizeDrawEvent(PrizeDrawEvent events)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(events).State = EntityState.Added;
                db.SaveChanges();
                return events.Id;
            }
        }

        public bool UpdatePrizeDrawEvent(PrizeDrawEvent events)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(events).State = EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdatePrizeDrawEventStatus(int prizeDrawEventId, int newStatus)
        {
            using (var db = new EventDbContext())
            {
                var d = (from p in db.PrizeDrawEventEntities
                         where p.Id == prizeDrawEventId
                         select p).FirstOrDefault();
                if (d != null)
                {
                    if (d.Status != newStatus)
                    {
                        d.Status = (byte)newStatus;
                        return db.SaveChanges() > 0;
                    }
                }
                return false;
            }
        }

        public PrizeDrawEventModel GetPrizeDrawEvent(int prizeDrawEventId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var d = (from pde in db.PrizeDrawEventEntities
                             where pde.Id == prizeDrawEventId
                             select new PrizeDrawEventModel
                             {
                                 Id = pde.Id,
                                 Token = pde.Token,
                                 ChannelOdmId = pde.ChannelOdmId,
                                 CreateTime = pde.CreateTime,
                                 Subject = pde.Subject,
                                 SD = pde.StartDate,
                                 ED = pde.EndDate,
                                 Status = pde.Status,
                                 JoinFee = pde.JoinFee,
                                 FullImg = pde.FullImg,
                                 PrizeImg = pde.PrizeImg,
                                 SmallImg = pde.SmallImg,
                                 EventContent = pde.EventContent,
                                 Temple = (SkmPrizeDrawTemple)pde.Temple,
                                 DailyLimit = pde.DailyLimit,
                                 TotalLimit = pde.TotalLimit,
                                 WinningSmallImg = pde.WinningImg,
                                 WinningBackgroundImg = pde.WinningBackgroundImg,
                                 SellerGuid = pde.SellerGuid.HasValue ? pde.SellerGuid.Value.ToString() : null,
                                 NoticeDate = pde.NoticeDate,
                                 PeriodHour = pde.PeriodHour,
                                 Notice = pde.Notice,
                             }).FirstOrDefault();

                    return d;
                }
            }
        }

        /// <summary>
        /// 依抽獎活動編號取得抽獎活動資訊
        /// </summary>
        /// <param name="prizeDrawEventId">抽獎活動編號</param>
        /// <returns>抽獎活動資訊</returns>
        public PrizeDrawEvent GetPrizeDrawEventByEventId(int prizeDrawEventId)
        {
            using (var db = new EventDbContext())
            {
                return db.PrizeDrawEventEntities.Where(item => item.Id == prizeDrawEventId).FirstOrDefault();
            }
        }

        /// <summary>
        /// 取得首頁的抽獎活動 (跟屁蟲)
        /// </summary>
        /// <returns></returns>
        public PrizeDrawEventModel GetIndexPrizeDrawEvent()
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    DateTime dt = DateTime.Now;
                    DateTime thisYear = new DateTime(DateTime.Now.Year, 1, 1);
                    var pde = db.PrizeDrawEventEntities.Where(p => dt >= p.StartDate//大於起始時間 活動才能開始
                                && dt <= config.SkmPrizeEventCustomEndDate //小於等於 強制活動截止時間
                                && p.EndDate >= thisYear //必須大於今年 必須是今年的活動
                                && p.Status == (int)SkmPrizeDrawEventStatus.Normal)
                                .FirstOrDefault();

                    PrizeDrawEventModel drawEvent = new PrizeDrawEventModel();
                    if (pde != null)
                    {
                        drawEvent.Id = pde.Id;
                        drawEvent.Token = pde.Token;
                        drawEvent.Subject = pde.Subject;
                        drawEvent.SD = pde.StartDate;
                        drawEvent.ED = pde.EndDate;
                        drawEvent.Status = pde.Status;
                        drawEvent.JoinFee = pde.JoinFee;
                        drawEvent.CreateTime = pde.CreateTime;
                        drawEvent.Temple = (SkmPrizeDrawTemple)pde.Temple;
                        drawEvent.FullImg = pde.FullImg;
                        drawEvent.SmallImg = pde.SmallImg;
                        drawEvent.PrizeImg = pde.PrizeImg;
                        drawEvent.WinningSmallImg = pde.WinningImg;
                        drawEvent.WinningBackgroundImg = pde.WinningBackgroundImg;
                        drawEvent.DailyLimit = pde.DailyLimit;
                        drawEvent.TotalLimit = pde.TotalLimit;
                        drawEvent.SellerGuid = pde.SellerGuid.HasValue ? pde.SellerGuid.Value.ToString() : null;
                        drawEvent.Notice = pde.Notice;
                    }
                    return drawEvent;
                }
            }
        }

        public List<PrizeDrawEventModel> GetPrizeDrawEventList(SkmPrizeDrawEventOrderField orderField, int sort = 0)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var data = from pde in db.PrizeDrawEventEntities
                               select new PrizeDrawEventModel
                               {
                                   Id = pde.Id,
                                   Token = pde.Token,
                                   Subject = pde.Subject,
                                   SD = pde.StartDate,
                                   ED = pde.EndDate,
                                   Status = pde.Status,
                                   JoinFee = pde.JoinFee,
                                   CreateTime = pde.CreateTime,
                                   Temple = (SkmPrizeDrawTemple)pde.Temple,
                                   FullImg = pde.FullImg,
                                   SmallImg = pde.SmallImg,
                                   PrizeImg = pde.PrizeImg,
                                   WinningSmallImg = pde.WinningImg,
                                   WinningBackgroundImg = pde.WinningBackgroundImg,
                                   DailyLimit = pde.DailyLimit,
                                   TotalLimit = pde.TotalLimit,
                                   SellerGuid = pde.SellerGuid.HasValue ? pde.SellerGuid.Value.ToString() : null,
                                   Notice = pde.Notice,
                               };

                    switch (orderField)
                    {
                        case SkmPrizeDrawEventOrderField.Sd:
                            if (sort == 0)
                            {
                                data = data.OrderByDescending(p => p.SD);
                            }
                            else
                            {
                                data = data.OrderBy(p => p.SD);
                            }
                            break;
                        case SkmPrizeDrawEventOrderField.Ed:
                            if (sort == 0)
                            {
                                data = data.OrderByDescending(p => p.ED);
                            }
                            else
                            {
                                data = data.OrderBy(p => p.ED);
                            }
                            break;
                        case SkmPrizeDrawEventOrderField.Temple:
                            if (sort == 0)
                            {
                                data = data.OrderByDescending(p => p.Temple);
                            }
                            else
                            {
                                data = data.OrderBy(p => p.Temple);
                            }
                            break;
                        case SkmPrizeDrawEventOrderField.Status:
                            if (sort == 0)
                            {
                                data = data.OrderByDescending(p => p.Status);
                            }
                            else
                            {
                                data = data.OrderBy(p => p.Status);
                            }
                            break;
                        case SkmPrizeDrawEventOrderField.App:
                            DateTime dt = DateTime.Now;
                            data = data.Where(p => p.SD <= dt && dt <= p.ED && p.Status == (int)SkmPrizeDrawEventStatus.Normal).OrderBy(p => p.SD);
                            break;
                        default:
                            data = data.OrderByDescending(p => p.CreateTime);
                            break;
                    }

                    return data.ToList();
                }
            }
        }

        public PrizeDrawEvent GetPrizeDrawEventByToken(string token)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var d = (from p in db.PrizeDrawEventEntities
                             where p.Token == token
                             select p).FirstOrDefault();

                    return d ?? new PrizeDrawEvent();
                }
            }
        }

        public PrizeDrawEventModel GetPrizeDrawEvent(string token)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var d = (from pde in db.PrizeDrawEventEntities
                             where pde.Token == token
                             select new PrizeDrawEventModel
                             {
                                 Id = pde.Id,
                                 Subject = pde.Subject,
                                 SD = pde.StartDate,
                                 ED = pde.EndDate,
                                 Status = pde.Status,
                                 JoinFee = pde.JoinFee,
                                 CreateTime = pde.CreateTime,
                                 Token = pde.Token,
                                 EventContent = pde.EventContent,
                                 Temple = (SkmPrizeDrawTemple)pde.Temple,
                                 FullImg = pde.FullImg,
                                 SmallImg = pde.SmallImg,
                                 PrizeImg = pde.PrizeImg,
                                 WinningSmallImg = pde.WinningImg,
                                 WinningBackgroundImg = pde.WinningBackgroundImg,
                                 NoticeDate = pde.NoticeDate,
                                 DailyLimit = pde.DailyLimit,
                                 TotalLimit = pde.TotalLimit
                             }).FirstOrDefault();

                    return d;
                }
            }
        }

        public bool CheckIsOtherEventOnline(DateTime sd, DateTime ed, int eventId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var col = from pde in db.PrizeDrawEventEntities
                              where
                              (
                                (pde.StartDate <= sd && pde.EndDate > sd) // *---|---*-------| OR // *---|----------*| OR // *---|----------|---*
                                || (pde.StartDate >= sd && pde.StartDate < ed && pde.EndDate >= ed)// |---*-------|---* OR |*----------|---*
                                || (pde.StartDate >= sd && pde.EndDate <= ed)// |--*-------*--|
                              )
                              && pde.Id != eventId
                              && !string.IsNullOrEmpty(pde.FullImg)
                              select pde;

                    return col.Any();
                }
            }
        }

        #endregion

        #region PrizeDrawItem

        public PrizeDrawItem GetPrizeDrawItem(int prizeDrawEventId, Guid bid, int prizeDrawitemId)
        {
            using (var db = new EventDbContext())
            {
                var item = (from pdi in db.PrizeDrawItemEntities
                            where pdi.PrizeDrawEventId == prizeDrawEventId
                            && pdi.ExternalDealId == bid
                            && pdi.Id == prizeDrawitemId
                            && pdi.IsRemove == false
                            select pdi).FirstOrDefault();
                return item ?? new PrizeDrawItem();
            }
        }

        /// <summary>
        /// 依照檔期GUID取得抽獎獎品資訊
        /// </summary>
        /// <param name="bid">檔期GUID</param>
        /// <returns>抽獎獎品資訊</returns>
        public PrizeDrawItem GetPrizeDrawItem(Guid externalDealId)
        {
            using (var db = new EventDbContext())
            {
                var item = (from pdi in db.PrizeDrawItemEntities
                            where pdi.ExternalDealId == externalDealId
                            && pdi.IsRemove == false
                            select pdi).FirstOrDefault();
                return item ?? new PrizeDrawItem();
            }
        }

        /// <summary>
        /// 依照獎品編號取得獎品資訊
        /// </summary>
        /// <param name="id">獎品編號</param>
        /// <returns>獎品資訊</returns>
        public PrizeDrawItem GetPrizeDrawItem(int id)
        {
            using (var db = new EventDbContext())
            {
                var item = (from pdi in db.PrizeDrawItemEntities
                            where pdi.Id == id
                            && pdi.IsRemove == false
                            select pdi).FirstOrDefault();
                return item ?? new PrizeDrawItem();
            }
        }

        public bool InsertPrizeDrawItemList(List<PrizeDrawItem> itemList)
        {
            using (var db = new EventDbContext())
            {
                foreach (var item in itemList)
                {
                    db.Entry(item).State = EntityState.Added;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdatePrizeDrawItemList(List<PrizeDrawItem> itemList)
        {
            using (var db = new EventDbContext())
            {
                foreach (var item in itemList)
                {
                    db.Entry(item).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdatePrizeDrawItemListWithoutIsUse(List<PrizeDrawItem> itemList)
        {
            using (var db = new EventDbContext())
            {
                foreach (var item in itemList)
                {
                    var prizeDrawItem = db.PrizeDrawItemEntities.AsNoTracking().FirstOrDefault(x=>x.Id == item.Id);
                    item.IsUse = prizeDrawItem != null ? prizeDrawItem.IsUse : true;
                    db.Entry(item).State = EntityState.Modified;
                }
                return db.SaveChanges() > 0;
            }
        }

        public List<PrizeDrawEditItemModel> GetPrizeDrawItemList(int prizeDrawEventId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from pdi in db.PrizeDrawItemEntities
                            join vpe in db.ViewPrizeExternalDealEntities on pdi.ExternalDealId equals vpe.Bid
                            where pdi.PrizeDrawEventId == prizeDrawEventId
                            && pdi.IsRemove == false
                            select new PrizeDrawEditItemModel
                            {
                                Id = pdi.Id,
                                Name = vpe.Title,
                                Bid = pdi.ExternalDealId,
                                Rate = pdi.WinningRate,
                                Count = pdi.Qty,
                                ED = pdi.EffectiveEndDate,
                                SD = pdi.EffectiveStartDate,
                                IsThanksFlag = pdi.IsThanks,
                                IsUse = pdi.IsUse,
                                DrawOutCount = 0,
                            }).Distinct().ToList();
                }
            }
        }

        public List<PrizeDrawItem> GetPrizeDrawItems(int eventId, bool getBetweenToday = true)
        {
            using (var db = new EventDbContext())
            {
                if (getBetweenToday)
                {
                    var today = DateTime.Now;
                    return (from i in db.PrizeDrawItemEntities
                            where i.PrizeDrawEventId == eventId
                                    && i.EffectiveStartDate < today && i.EffectiveEndDate > today
                                    && i.IsRemove == false

                            select i).ToList();
                }

                return (from i in db.PrizeDrawItemEntities
                        where i.PrizeDrawEventId == eventId
                        && i.IsRemove == false
                        select i).ToList();
            }
        }

        public List<PrizeDrawItem> GetPrizeDrawItemsWithNoLock(int eventId, bool getBetweenToday = true)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    if (getBetweenToday)
                    {
                        var today = DateTime.Now;
                        return (from i in db.PrizeDrawItemEntities
                                where i.PrizeDrawEventId == eventId
                                      && i.EffectiveStartDate < today && i.EffectiveEndDate > today
                                      && i.IsRemove == false
                                select i).ToList();
                    }

                    return (from i in db.PrizeDrawItemEntities
                            where i.PrizeDrawEventId == eventId
                            && i.IsRemove == false
                            select i).ToList();
                }
            }
        }
        #endregion

        #region PrizeDrawItemPeriod
        /// <summary>
        /// 依抽獎活動編號取得抽獎活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawEventId">抽獎活動編號</param>
        /// <returns>抽獎活動階段資料清單</returns>
        public List<PrizeDrawItemPeriod> GetPrizeDrawItemPeriodByEventId(int prizeDrawEventId)
        {
            using (var db = new EventDbContext())
            {
                IQueryable<int> prizeDrawItemIds = db.PrizeDrawItemEntities.Where(item => item.PrizeDrawEventId == prizeDrawEventId).Select(item => item.Id);
                List<PrizeDrawItemPeriod> returnValue = db.PrizeDrawItemPeriodEntities.Where(item => prizeDrawItemIds.Any(itemId => itemId == item.PrizeDrawItemId)).ToList();
                return returnValue;
            }
        }
        /// <summary>
        /// 依獎品編號刪除活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemId">抽獎活動編號</param>
        /// <returns></returns>
        public bool DeletePrizeDrawItemPeriods(int prizeDrawItemId)
        {
            using (var db = new EventDbContext())
            {
                IQueryable<PrizeDrawItemPeriod> prizeDrawItemPeriods =
                    db.PrizeDrawItemPeriodEntities.Where(item => item.PrizeDrawItemId == prizeDrawItemId);
                db.PrizeDrawItemPeriodEntities.RemoveRange(prizeDrawItemPeriods);
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 依獎品編號清單刪除活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemIds">抽獎活動編號清單</param>
        /// <returns></returns>
        public bool DeletePrizeDrawItemPeriods(List<int> prizeDrawItemIds)
        {
            using (var db = new EventDbContext())
            {
                IQueryable<PrizeDrawItemPeriod> prizeDrawItemPeriods =
                    db.PrizeDrawItemPeriodEntities.Where(item => prizeDrawItemIds.Contains(item.PrizeDrawItemId));
                db.PrizeDrawItemPeriodEntities.RemoveRange(prizeDrawItemPeriods);
                return db.SaveChanges() > 0;
            }
        }
        /// <summary>
        /// 新抽獎增活動階段資料清單
        /// </summary>
        /// <param name="prizeDrawItemPeriods"></param>
        /// <returns></returns>
        public bool InsertPrizeDrawItemPeriods(List<PrizeDrawItemPeriod> prizeDrawItemPeriods)
        {
            using (var db = new EventDbContext())
            {
                db.PrizeDrawItemPeriodEntities.AddRange(prizeDrawItemPeriods);
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依獎品編號取得目前可以抽獎的數量
        /// </summary>
        /// <param name="prizeDrawItemId">獎品編號</param>
        /// <returns>目前可以抽獎的數量</returns>
        public int GetCanDrawOutItemCountsByItemId(int prizeDrawItemId)
        {
            using (var db = new EventDbContext())
            {
                IQueryable<PrizeDrawItemPeriod> prizeDrawItemPeriods = db.PrizeDrawItemPeriodEntities
                    .Where(item => item.PrizeDrawItemId == prizeDrawItemId &&
                                 item.StartDate < DateTime.Now &&
                                 item.IsActive);
                return prizeDrawItemPeriods.Sum(item => item.Quantity);
            }
        }
        #endregion

        #region ViewPrizeExternal

        public ViewPrizeExternalDeal GetExternalDealRemainingQuantity(Guid bid)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var d = (from vped in db.ViewPrizeExternalDealEntities
                             where vped.Bid == bid
                             select vped).FirstOrDefault();

                    return d == null ? new ViewPrizeExternalDeal() : d;
                }
            }
        }

        public List<ViewPrizeExternalDeal> GetViewPrizeExternalDealList(List<Guid?> bidList)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var cols = (from vped in db.ViewPrizeExternalDealEntities
                                where bidList.Contains(vped.Bid.Value)
                                select vped).Distinct().ToList();

                    return cols;
                }
            }
        }

        public List<PrizeDrawEventDDLModel> GetPrizeDrawEventDDLList(DateTime sd, DateTime ed, int joinfee)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var list = (from vped in db.ViewPrizeExternalDealEntities
                                where (vped.BusinessHourOrderTimeS <= sd && vped.BusinessHourOrderTimeE >= ed)
                                      && (vped.BusinessHourOrderTimeE > DateTime.Now)//檔次未逾期
                                      && vped.BurningPoint == joinfee//符合扣點
                                select new PrizeDrawEventDDLModel
                                {
                                    Name = vped.Title,
                                    Bid = vped.Bid.Value,
                                    Count = vped.OrderTotalLimit,
                                }).Distinct().ToList();

                    return list;
                }
            }
        }

        #endregion

        #region PrizePreventingOverdraw

        public bool InsertPrizePreventingOverdraw(PrizePreventingOverdraw od)
        {
            od.CreateTime = od.ModifyTime = DateTime.Now;
            od.Processing = true;

            using (var db = new EventDbContext())
            {
                db.Entry(od).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        public int InsertPrizeDrawProcessingAndGetQty(int itemId, int qty, int userId)
        {
            DateTime timeoutPoint = DateTime.Now.AddSeconds(-config.PreventingOversellTimeoutSec);
            var sql = string.Format("insert into prize_preventing_overdraw (item_id, qty, user_id, create_time, modify_time, processing)" +
                                    "values ({0},{1},{2},getdate(),getdate(),1)", itemId, qty, userId);

            var sql2 = string.Format("select sum(qty) from prize_preventing_overdraw where item_id={0} and processing={1} and create_time > '{2}'",
               itemId, 1, timeoutPoint.ToString("yyyy/MM/dd HH:mm"));

            var newSQl = sql + " " + sql2;
            using (var db = new OrderDbContext())
            {
                return db.Database.SqlQuery<int>(newSQl).FirstOrDefault();
            }
        }

        public int PrizeDrawGetQty(int itemId, int qty)
        {
            DateTime timeoutPoint = DateTime.Now.AddSeconds(-config.PreventingOversellTimeoutSec);
            var sql = string.Format("select case when sum(qty) is null then 0 else sum(qty) end from prize_preventing_overdraw where item_id={0} and processing={1} and create_time > '{2}'",
                itemId, 1, timeoutPoint.ToString("yyyy/MM/dd HH:mm:ss"));

            using (var db = new OrderDbContext())
            {
                return db.Database.SqlQuery<int>(sql).FirstOrDefault();
            }
        }

        public bool SetPreventingOversellExpired(int itemId, int userId, int qty)
        {
            var sql = @"UPDATE prize_preventing_overdraw SET Processing = 0, modify_time = GETDATE() WHERE
                        id in (SELECT TOP 1 id FROM prize_preventing_overdraw WHERE item_id=@itemId AND user_id=@USERID AND qty=@QTY AND processing=1 order by id)";

            object[] para = {
                new SqlParameter("@itemId", itemId),
                new SqlParameter("@USERID", userId),
                new SqlParameter("@QTY", qty)
            };

            using (var db = new OrderDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, para) > 0;
            }
        }

        public bool SetPreventingOversellExpired(int preventingOverdrawId)
        {
            var sql = @"UPDATE prize_preventing_overdraw SET Processing = 0, modify_time = GETDATE() WHERE id=@pid";

            object[] para = {
                new SqlParameter("@pid", preventingOverdrawId)
            };

            using (var db = new OrderDbContext())
            {
                return db.Database.ExecuteSqlCommand(sql, para) > 0;
            }
        }

        #endregion PrizePreventingOverdrawEntities

        #region PrizeDrawRecord

        public bool InsertPrizeDrawRecord(PrizeDrawRecord record)
        {
            record.Status = (byte)PrizeDrawRecordStatus.Init;
            record.CreateTime = DateTime.Now;

            using (var db = new EventDbContext())
            {
                db.Entry(record).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        public bool UpdatePrizeDrawRecord(PrizeDrawRecord record)
        {
            var temp = GetPrizeDrawRecord(record.Id);
            if (temp.Id == 0) return false;

            using (var db = new EventDbContext())
            {
                record.ModifyTime = DateTime.Now;
                db.Entry(record).State = EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        public PrizeDrawRecord GetPrizeDrawRecord(int recordId)
        {
            using (var db = new EventDbContext())
            {
                return (from r in db.PrizeDrawRecordEntities
                        where r.Id == recordId
                        select r).FirstOrDefault();
            }
        }

        public PrizeDrawRecord GetPrizeDrawRecordWithNoLock(int recordId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.PrizeDrawRecordEntities
                            where r.Id == recordId
                            select r).FirstOrDefault();
                }
            }
        }

        public PrizeDrawRecord GetPrizeDrawRecord(int eventId, int userId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.PrizeDrawRecordEntities
                            where r.EventId == eventId
                            && r.UserId == userId
                            && r.Status == (int)PrizeDrawRecordStatus.PrizeDrawCompleted
                            orderby r.CreateTime descending
                            select r).FirstOrDefault();
                }
            }
        }

        public PrizeDrawRecord GetPrizeDrawRecord(int eventId, int userId, int recordId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.PrizeDrawRecordEntities
                            where r.EventId == eventId
                            && r.UserId == userId
                            && r.Status == (int)PrizeDrawRecordStatus.PrizeDrawCompleted
                            && r.Id == recordId
                            select r).FirstOrDefault();
                }
            }
        }

        public List<PrizeDrawRecord> GetPrizeDrawRecordByItemId(int itemId, PrizeDrawRecordStatus status)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.PrizeDrawRecordEntities
                            where r.ItemId == itemId
                                  && r.Status == (byte)status
                            select r).ToList();
                }
            }
        }

        public int GetPrizeDrawRecordCount(int eventId, int itemId, PrizeDrawRecordStatus status) 
        {
            var sql = string.Format("select count(1) from prize_draw_record with(nolock) where event_id={0} and item_id={1} and status={2}", eventId, itemId, (byte)status);

            using (var db = new EventDbContext())
            {
                return db.Database.SqlQuery<int>(sql).FirstOrDefault();
            }
        }

        public List<int> GetPrizeDrawRecordItemIds(int eventId, PrizeDrawRecordStatus status)
        {
            var sql = string.Format("select item_id from prize_draw_record with(nolock) where event_id={0} and status={1}", eventId, (byte)status);

            using (var db = new EventDbContext())
            {
                return db.Database.SqlQuery<int>(sql).ToList();
            }
        }

        public int GetPrizeDrawRecordCount(int itemId, PrizeDrawRecordStatus status)
        {
            var sql = string.Format("select sum(qty) from prize_draw_record where item_id={0} and status={1}", itemId, (byte)status);

            using (var db = new EventDbContext())
            {
                return db.Database.SqlQuery<int>(sql).FirstOrDefault();
            }
        }

        public List<ViewPrizeDrawRecord> GetViewPrizeDrawRecord(int eventId, int userId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.ViewPrizeDrawRecordEntities
                            where r.EventId == eventId && r.UserId == userId
                            select r).ToList() ?? new List<ViewPrizeDrawRecord>();
                }
            }
        }

        public List<PrizeDrawRecord> GetPrizeDrawRecordByUserId(int eventId, int userId, PrizeDrawRecordStatus status, bool getBetweenToday)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    if (getBetweenToday)
                    {
                        var st = DateTime.Now.Date;
                        var et = DateTime.Now.AddDays(1).Date;
                        return (from r in db.PrizeDrawRecordEntities
                                where r.EventId == eventId && r.UserId == userId
                                      && r.Status == (byte)status
                                      && r.CreateTime >= st
                                      && r.CreateTime < et
                                select r).ToList();
                    }
                    else
                    {
                        return (from r in db.PrizeDrawRecordEntities
                                where r.EventId == eventId && r.UserId == userId
                                      && r.Status == (byte)status
                                select r).ToList();
                    }
                }
            }
        }

        public List<PrizeDrawRecord> GetPrizeDrawRecordByEventId(int eventId, PrizeDrawRecordStatus status)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return (from r in db.PrizeDrawRecordEntities
                            where r.EventId == eventId
                                  && r.Status == (byte)status
                            select r).ToList();
                }
            }
        }

        public int GetPrizeDrawRecordQty(int itemId, PrizeDrawRecordStatus status)
        {
            using (var db = new EventDbContext())
            {
                var result = (from t in db.PrizeDrawRecordEntities
                              where t.ItemId == itemId && t.Status == (byte)status
                              select t);

                if (result.Any())
                {
                    return result.Sum(x => x.Qty);
                }
                return 0;

            }
        }

        #endregion PrizeDrawRecord

        #region PrizeDrawRecordLog

        public bool InsertOrUpdatePrizeDrawRecordLog(PrizeDrawRecordLog log)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(log).State = log.Id == 0 ? EntityState.Added : EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 依活動編號取得需要重新發送的活動資料清單
        /// </summary>
        /// <param name="prizeDrawEventId">活動編號</param>
        /// <returns>活動資料清單</returns>
        public List<PrizeDrawRecordLog> GetNeedResendPrizeDrawRecordLogsByEventId(int prizeDrawEventId)
        {
            using (var db = new EventDbContext())
            {
                var logIds = db.PrizeDrawRecordEntities.Where(item => item.EventId == prizeDrawEventId).Select(item => item.Id);
                return db.PrizeDrawRecordLogEntities.Where(item => logIds.Contains(item.RecordId) && string.IsNullOrEmpty(item.RequestLog)).ToList();
            }
        }
        #endregion

        #region ExternalDealIcon

        /// <summary>
        /// 取得標籤設定資料清單
        /// </summary>
        /// <returns>標籤設定資料清單</returns>
        public List<ExternalDealIcon> GetExternalDealIcons()
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    return db.ExternalDealIcons.ToList();
                }
            }
        }

        /// <summary>
        /// 新增標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料</param>
        /// <returns>是否新增成功</returns>
        public bool InsertExternalDealIcon(ExternalDealIcon externalDealIcon)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(externalDealIcon).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 編輯標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料</param>
        /// <returns>是否編輯成功</returns>
        public bool SetExternalDealIcon(ExternalDealIcon externalDealIcon)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(externalDealIcon).State = EntityState.Modified;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 刪除標籤設定資料
        /// </summary>
        /// <param name="externalDealIcon">標籤設定資料編號</param>
        /// <returns>是否刪除成功</returns>
        public bool DeleteExternalDealIcon(ExternalDealIcon externalDealIcon)
        {
            using (var db = new EventDbContext())
            {
                db.Entry(externalDealIcon).State = EntityState.Deleted;
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region External_deal_Exhibition_code

        public List<ExternalDealExhibitionCode> GetExternalDealExhibitionCode(Guid externalDealGuid)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from exc in db.ExternalDealExhibitionCodes
                                where exc.ExternalDealGuid == externalDealGuid
                                select exc).ToList();
                    return item;
                }
            }
        }

        public List<ExternalDealExhibitionCode> GetExternalDealExhibitionCode(Guid externalDealGuid, long code)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from exc in db.ExternalDealExhibitionCodes
                                where exc.ExternalDealGuid == externalDealGuid
                                      && exc.ExhibitionCodeId == code
                                select exc).ToList();
                    return item;
                }
            }
        }

        public ExternalDealExhibitionCode GetExternalDealExhibitionCode(int codeId)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from exc in db.ExternalDealExhibitionCodes
                                where exc.Id == codeId
                                select exc).SingleOrDefault();
                    return item;
                }
            }
        }

        public bool AddExternalDealExhibitionCodeList(Guid externalDealGuid, List<long> codes)
        {
            using (var db = new EventDbContext())
            {
                foreach (var c in codes)
                {
                    db.ExternalDealExhibitionCodes.Add(new ExternalDealExhibitionCode
                    {
                        ExternalDealGuid = externalDealGuid,
                        ExhibitionCodeId = c
                    });
                }
                return db.SaveChanges() > 0;
            }
        }

        public bool SetExternalDealExhibitionCode(Guid externalDealGuid, long code)
        {
            using (var db = new EventDbContext())
            {
                var checkData = GetExternalDealExhibitionCode(externalDealGuid, code);
                if (checkData.Any())
                {
                    return true;
                }

                db.Entry(new ExternalDealExhibitionCode
                {
                    ExternalDealGuid = externalDealGuid,
                    ExhibitionCodeId = code
                }).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteExternalDealExhibitionCode(Guid externalDealGuid, long code)
        {
            using (var db = new EventDbContext())
            {
                var checkData = GetExternalDealExhibitionCode(externalDealGuid, code);
                if (!checkData.Any())
                {
                    return true;
                }

                db.Entry(checkData.First()).State = EntityState.Deleted;
                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteExternalDealExhibitionCode(Guid externalDealGuid)
        {
            using (var db = new EventDbContext())
            {
                if (!db.ExternalDealExhibitionCodes.Any(item => item.ExternalDealGuid == externalDealGuid))
                {
                    return true;
                }

                IQueryable<ExternalDealExhibitionCode> deleteData = db.ExternalDealExhibitionCodes.Where(item => item.ExternalDealGuid == externalDealGuid);
                db.ExternalDealExhibitionCodes.RemoveRange(deleteData);

                return db.SaveChanges() > 0;
            }
        }

        public bool DeleteExternalDealExhibitionCode(int codeId)
        {
            using (var db = new EventDbContext())
            {
                var checkData = GetExternalDealExhibitionCode(codeId);
                if (checkData == null || checkData.Id == 0)
                {
                    return true;
                }

                db.Entry(checkData).State = EntityState.Deleted;
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 檢查策展代號是否已被使用
        /// </summary>
        /// <param name="exhibitionCode">策展代號</param>
        /// <returns>是否已被使用</returns>
        public bool ChechExhibitionCodeIsUsed(long id)
        {
            using (var eventDb = new EventDbContext())
            {
                using (var exhibitionDb = new ExhibitionDbContext())
                {
                    bool result = eventDb.ExternalDealExhibitionCodes.Any(item => item.ExhibitionCodeId == id) ||
                                  exhibitionDb.ExhibitionEventEntities.Any(item => item.ExhibitionCodeId == id);
                    return result;
                }
            }
        }

        #endregion

        #region external_deal_relation_store_display_name
        /// <summary>
        /// 依檔次編號刪除APP顯示領取櫃位資訊清單
        /// </summary>
        /// <param name="dealGuid">檔次編號</param>
        public bool DeleteExternalDealRelationStoreDisplayNameByDealGuid(Guid dealGuid)
        {
            using (var db = new EventDbContext())
            {
                IQueryable<ExternalDealRelationStoreDisplayName> deleteItems = db.ExternalDealRelationStoreDisplayNameEntities.Where(item => item.ExternalDealGuid == dealGuid);
                db.ExternalDealRelationStoreDisplayNameEntities.RemoveRange(deleteItems);
                return db.SaveChanges() > 0;
            }
        }

        /// <summary>
        /// 新增APP顯示領取櫃位資訊清單
        /// </summary>
        /// <param name="storeDisplayNames">APP顯示領取櫃位資訊清單</param>
        /// <returns>是否新增成功</returns>
        public bool InsertExternalDealRelationStoreDisplayNameByDealGuid(IEnumerable<ExternalDealRelationStoreDisplayName> storeDisplayNames)
        {
            using (var db = new EventDbContext())
            {
                db.ExternalDealRelationStoreDisplayNameEntities.AddRange(storeDisplayNames);
                return db.SaveChanges() > 0;
            }
        }

        #endregion

        #region View_External_deal_Exhibition_code

        public List<ViewExternalDealExhibitionCode> GetViewExternalDealExhibitionCode(Guid externalDealGuid)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from exc in db.ViewExternalDealExhibitionCodes
                                where exc.ExternalDealGuid == externalDealGuid
                                select exc).ToList();
                    return item;
                }
            }
        }

        public List<ViewExternalDealExhibitionCode> GetViewExternalDealExhibitionCode(Guid externalDealGuid, long code)
        {
            using (var db = new EventDbContext())
            {
                using (db.NoLock())
                {
                    var item = (from exc in db.ViewExternalDealExhibitionCodes
                                where exc.ExternalDealGuid == externalDealGuid
                                      && exc.ExhibitionCodeId == code
                                select exc).ToList();
                    return item;
                }
            }
        }

        #endregion
    }
}

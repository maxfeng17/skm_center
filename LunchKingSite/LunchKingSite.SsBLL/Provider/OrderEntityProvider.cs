﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Transactions;
using EntityFramework.BulkInsert.Extensions;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.Core.Models.OrderEntities;
using LunchKingSite.Core.Models.PponEntities;
using LunchKingSite.DataOrm;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class OrderEntityProvider : IOrderEntityProvider
    {
        ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        public bool InsertShopBackInfo(ShopbackInfo shopbackInfo)
        {
            using (var db = new OrderDbContext())
            {
                db.Entry(shopbackInfo).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        const string shopbackRsrc = "shopback_intro";
        public IEnumerable<ViewShopbackInfo> GetCompleteOrderViewShopbackInfoList(DateTime startTime, DateTime endTime)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from v in db.ViewShopbackInfoDbSet
                               where v.LastExternalRsrc == shopbackRsrc
                                     && (v.OrderStatus & 8) > 0
                                     && (v.OrderTime >= startTime && v.OrderTime <= endTime)
                               select v;
                    return cols.ToList();
                }
            }
        }

        public IEnumerable<ViewShopbackInfo> GetCompleteOrderViewShopbackInfoList(Guid orderGuid)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from v in db.ViewShopbackInfoDbSet
                               where v.LastExternalRsrc == shopbackRsrc
                                     && (v.OrderStatus & 8) > 0
                                     && (v.OrderGuid == orderGuid)
                               select v;
                    return cols.ToList();
                }
            }
        }

        //撈出這些日期條件的訂單
        public IEnumerable<ViewShopbackValidationInfo> GetValidationOrderViewShopbackInfoList(DateTime startTime,
            DateTime endTime, DateTime contractShipTimeStart, DateTime contractShipTimeEnd,
            DateTime contractOrderTimeStart, DateTime contractOrderTimeEnd)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    string sql = @"select id as Id
                          ,order_guid as OrderGuid
                          ,bid as Bid
                          ,gid as Gid
                          ,order_id as OrderId
                          ,order_status as OrderStatus
                          ,trust_id as TrustId
                          ,usage_verified_time as UsageVerifiedTime
                          ,coupon_sequence_number as CouponSequenceNumber
                          ,user_id as UserId
                          ,delivery_type as DeliveryType
                          ,amount as Amount
                          ,cash_trust_log_status as CashTrustLogStatus
                          ,order_time as OrderTime
                          ,item_name as ItemName
                          ,send_type as SendType
                          ,first_rsrc as FirstRsrc
                          ,last_rsrc as LastRsrc
                          ,last_external_rsrc as LastExternalRsrc
                          ,ship_time as ShipTime
                          ,return_form_time as ReturnFormTime
                          from view_shopback_validation_info 
                          where order_id in (
                            select order_id 
                            from view_shopback_validation_info v
                            where v.last_external_rsrc = @shopbackRsrc
                            and v.order_status & 8 > 0
                            and v.send_type = @orderAPIFinish
                            and (
                            (v.delivery_type = @toShop and v.usage_verified_time >= @startTime and v.usage_verified_time <= @endTime) --憑証
                            or (v.delivery_type = @toHouse and ((v.ship_time >= @contractShipTimeStart and v.ship_time <= @contractShipTimeEnd) --宅配在出貨n天後要傳回
                                or (v.order_time >= @contractOrderTimeStart and v.order_time <= @contractOrderTimeEnd))) --宅配在訂單產生n天後要傳回
                            --退貨(這裏可能會退貨單已經產生，可是cash_trust_log_status還未更改狀態(維持核銷的狀態)，所以不再判斷退貨單
                            --or (v.return_form_time >= @startTime and v.return_form_time <= @endTime) 
                            
                            )
                        )";

                    object[] para = {
                        new SqlParameter("@shopbackRsrc", shopbackRsrc),
                        new SqlParameter("@toShop", (int)DeliveryType.ToShop),
                        new SqlParameter("@toHouse", (int)DeliveryType.ToHouse),
                        new SqlParameter("@startTime", startTime),
                        new SqlParameter("@endTime", endTime),
                        new SqlParameter("@contractShipTimeStart", contractShipTimeStart),
                        new SqlParameter("@contractShipTimeEnd", contractShipTimeEnd),
                        new SqlParameter("@contractOrderTimeStart", contractOrderTimeStart),
                        new SqlParameter("@contractOrderTimeEnd", contractOrderTimeEnd),
                        new SqlParameter("@orderAPIFinish", (int)ShopbackSendType.OrderAPIFinish),

                    };

                    var result = db.Database.SqlQuery<ViewShopbackValidationInfo>(sql, para);
                    return result.ToList();
                }
            }
        }

        public ShopbackInfo ShopBackInfoGetById(int id)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    return db.ShopBackInfoDbSet.FirstOrDefault(s => s.Id == id);
                }
            }
        }

        public void SaveShopBackInfo(ShopbackInfo info)
        {
            using (var db = new OrderDbContext())
            {
                if (info.Id == 0)
                {
                    db.Entry(info).State = EntityState.Added;
                }
                else
                {
                    db.Entry(info).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        public void SaveShopBackInfo(Guid id, int shopbackSendType, string commissionType, string responseMessage,
            DateTime modifyDateTime)
        {
            using (var db = new PponDbContext())
            {
                string sql = @"update shopback_info set send_type = @send_type, return_code = @return_code
                                , request_result = @request_result
                                , send_count = send_count + 1, modify_time = @modify_time
                                where order_guid = @order_guid;";

                SqlParameter sendType = new SqlParameter("@send_type", System.Data.SqlDbType.Int)
                {
                    Value = (int)shopbackSendType
                };
                SqlParameter returnCode = new SqlParameter("@return_code", System.Data.SqlDbType.NVarChar)
                {
                    Value = commissionType
                };
                SqlParameter requestResult = new SqlParameter("@request_result", System.Data.SqlDbType.NVarChar)
                {
                    Value = responseMessage
                };
                SqlParameter modifyTime = new SqlParameter("@modify_time", System.Data.SqlDbType.DateTime)
                {
                    Value = modifyDateTime
                };
                SqlParameter orderGuid = new SqlParameter("@order_guid", System.Data.SqlDbType.UniqueIdentifier)
                {
                    Value = id
                };

                db.Database.ExecuteSqlCommand(sql, sendType, returnCode, requestResult, modifyTime, orderGuid);
            }
        }

        public List<ViewOrderEntity> GetSuccessfulViewOrderEntities(DateTime startTime, DateTime endTime)
        {
            using (var db = new OrderDbContext())
            {
                return db.ViewOrderEntityDbSet.Where(t => t.CreateTime >= startTime && t.CreateTime < endTime
                    && (t.OrderStatus & 8) > 0 && t.Total > 0).ToList();
            }
        }

        public List<ViewOrderEntity> GetUserSuccessfulOrders(int userId, DateTime startTime, DateTime endTime)
        {
            using (var db = new OrderDbContext())
            {
                return db.ViewOrderEntityDbSet.Where(t => t.UserId == userId &&
                    t.CreateTime >= startTime && t.CreateTime < endTime &&
                    (t.OrderStatus & 8) > 0 && t.Total > 0).ToList();
            }
        }

        #region Line導購
        public bool BulkInsertLineShopInfo(List<LineshopInfo> lineshopInfo)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    db.BulkInsert(lineshopInfo);
                    return db.SaveChanges() > 0;
                }
            }
        }

        public bool UpdateLineShopInfoSendType(Guid orderGuid, int sendType, DateTime modifyDateTime, string responseMsg)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var updateDt = db.LineshopInfoDbSet.Where(l => l.OrderGuid == orderGuid);
                    updateDt.ForEach(l =>
                    {
                        l.SendType = sendType;
                        l.SendCount = l.SendCount + 1;
                        l.ModifyTime = modifyDateTime;
                        db.Entry(l).State = EntityState.Modified;
                        l.ReturnCode = responseMsg;
                    });
                    return db.SaveChanges() > 0;
                }
            }
        }

        public List<ViewLineshopOrderinfo> GetOrderInfoForLineShop(DateTime startTime, DateTime endTime)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    //符合line的rsrc、訂單成立、拋單區間、首次回拋尚未拋單
                    var result = db.ViewLineshopOrderinfoDbSet
                    .Where(v => v.LastExternalRsrc == config.LineRsrc
                    && (v.OrderStatus & (int)OrderStatus.Complete) > 0
                    && v.OrderTime >= startTime
                    && v.OrderTime <= endTime
                    && v.SendType == (int)LineShopSendType.ReadySend
                        ).ToList();
                    return result;
                }
            }
        }

        public List<ViewLineshopOrderinfo> GetViewLineshopOrderinfoByOrderGuid(Guid guid)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var result = db.ViewLineshopOrderinfoDbSet
                    .Where(p => p.OrderGuid == guid)
                    .ToList();
                    return result;
                }
            }
        }

        public List<LineshopInfo> GetLineShopInfosByOrderGuid(Guid guid)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var result = db.LineshopInfoDbSet
                    .Where(p => p.OrderGuid == guid)
                    .ToList();
                    return result;
                }
            }
        }

        public List<ViewLineshopOrderfinish> GetOrderFinishForLineShop(DateTime orderStartDate, DateTime shipStartDate)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    DateTime orderEndDate = orderStartDate.AddDays(1);
                    DateTime shipEndDate = shipStartDate.AddDays(1);

                    //符合line的rsrc、訂單成立、已首拋、憑證+訂單日/宅配+出貨日
                    var result = db.ViewLineshopOrderfinishDbSet
                        .Where(
                        v => v.LastExternalRsrc == config.LineRsrc
                        && (v.OrderStatus & (int)OrderStatus.Complete) > 0
                        && (v.OrderStatus & (int)OrderStatus.Cancel) == 0
                        && v.SendType == (int)LineShopSendType.OrderInfoAPIFinish
                        && ((v.DeliveryType == (int)DeliveryType.ToShop && v.OrderTime >= orderStartDate && v.OrderTime < orderEndDate)
                         || (v.DeliveryType == (int)DeliveryType.ToHouse && v.MShipTime >= shipStartDate && v.MShipTime < shipEndDate))
                        ).ToList();
                    return result;
                }
            }
        }


        #endregion

        #region 

        public bool InsertAffiliatesInfo(AffiliatesInfo info)
        {
            using (var db = new OrderDbContext())
            {
                db.Entry(info).State = EntityState.Added;
                return db.SaveChanges() > 0;
            }
        }

        public AffiliatesInfo AffiliatesGetById(int id)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    return db.AffiliatesInfoDbSet.FirstOrDefault(s => s.Id == id);
                }
            }
        }

        public void SaveAffiliatesInfo(AffiliatesInfo info)
        {
            using (var db = new OrderDbContext())
            {
                if (info.Id == 0)
                {
                    db.Entry(info).State = EntityState.Added;
                }
                else
                {
                    db.Entry(info).State = EntityState.Modified;
                }
                db.SaveChanges();
            }
        }

        readonly string affiliatesRsrc = "affiliates_intro";
        public IEnumerable<ViewAffiliatesInfo> GetCompleteOrderViewAffiliatesInfoList(DateTime startTime, DateTime endTime)
        {
            using (var db = new OrderDbContext())
            {
                using (db.NoLock())
                {
                    var cols = from v in db.ViewAffiliatesInfoDbSet
                               where v.LastExternalRsrc == affiliatesRsrc
                                     && (v.OrderStatus & 8) > 0
                                     && (v.OrderTime >= startTime && v.OrderTime <= endTime)
                               select v;
                    return cols.ToList();
                }
            }
        }

        public void SaveAffiliatesInfo(Guid id, int affiliatesSendType, string responseMessage, DateTime modifyDateTime)
        {
            using (var db = new PponDbContext())
            {
                string sql = @"update affiliates_info set send_type = @send_type
                                , request_result = @request_result
                                , send_count = send_count + 1, modify_time = @modify_time
                                where order_guid = @order_guid;";

                SqlParameter sendType = new SqlParameter("@send_type", System.Data.SqlDbType.Int)
                {
                    Value = (int)affiliatesSendType
                };
                SqlParameter requestResult = new SqlParameter("@request_result", System.Data.SqlDbType.NVarChar)
                {
                    Value = responseMessage
                };
                SqlParameter modifyTime = new SqlParameter("@modify_time", System.Data.SqlDbType.DateTime)
                {
                    Value = modifyDateTime
                };
                SqlParameter orderGuid = new SqlParameter("@order_guid", System.Data.SqlDbType.UniqueIdentifier)
                {
                    Value = id
                };

                db.Database.ExecuteSqlCommand(sql, sendType, requestResult, modifyTime, orderGuid);
            }
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Interface;
using LunchKingSite.Core.Models.Entities;
using LunchKingSite.SsBLL.DbContexts;

namespace LunchKingSite.SsBLL.Provider
{
    public class QuestionProvider : IQuestionProvider
    {
        #region QuestionEvent

        public QuestionEvent GetQuestionEvent(string eventToken)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var queryDate = DateTime.Now;
                    var ev = (from qe in db.QuestionEventEntities
                        where qe.QuestionToken == eventToken
                              && qe.Disabled == false
                              && qe.StartDate <= queryDate && qe.EndDate > queryDate
                        select qe).FirstOrDefault();
                    return ev ?? new QuestionEvent();
                }
            }
        }

        public QuestionEvent GetQuestionEvent(int eventId)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var queryDate = DateTime.Now;
                    var ev = (from qe in db.QuestionEventEntities
                        where qe.Id == eventId
                        select qe).FirstOrDefault();
                    return ev ?? new QuestionEvent();
                }
            }
        }

        #endregion QuestionEvent

        #region QuestionItemCategory

        public List<QuestionItemCategory> GetQuestionItemCategory(int eventId)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var result = (from qc in db.QuestionItemCategoryEntities
                        where qc.EventId == eventId
                        select qc).ToList();
                    return result;
                }
            }
        }

        #endregion QuestionItemCategory

        #region ViewQuestionItem

        public List<ViewQuestionItem> GetViewQuestionItem(int eventId)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from vi in db.ViewQuestionItemEntities
                        where vi.EventId == eventId
                        select vi).ToList();
                    return items;
                }
            }
        }

        public List<ViewQuestionItem> GetViewQuestionItem(int eventId, int categoryId)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var items = (from vi in db.ViewQuestionItemEntities
                        where vi.EventId == eventId && vi.CategoryId == categoryId
                        select vi).ToList();
                    return items;
                }
            }
        }

        #endregion ViewQuestionItem

        #region QuestionItemOption

        public List<QuestionItemOption> GetQuestionItemOptions(int itemId)
        {
            using (var db = new QuestionDbContext())
            {
                using (db.NoLock())
                {
                    var options = (from o in db.QuestionItemOptionEntities
                        where o.ItemId == itemId orderby o.Seq
                        select o).ToList();
                    return options;
                }
            }
        }

        #endregion QuestionItemOption

        #region QuestionEventDiscount

        public List<QuestionEventDiscount> GetQuestionEventDiscount(int eventId)
        {
            using (var db = new QuestionDbContext())
            {
                var eds = (from qe in db.QuestionEventDiscountEntities
                    where qe.EventId == eventId
                            && qe.Disabled == false
                    select qe).ToList();
                return eds;
            }
        }

        #endregion QuestionEventDiscount

        #region QuestionAnswerDiscountLog

        public List<QuestionAnswerDiscountLog> GetQuestionAnswerDiscountLog(int userId, int eventDiscountId)
        {
            using (var db = new QuestionDbContext())
            {
                var result = (from dl in db.QuestionAnswerDiscountLogEntities
                    where dl.UserId == userId
                          && dl.EventDiscountId == eventDiscountId
                    select dl).ToList();
                return result;
            }
        }

        public bool SaveQuestionAnswerDiscountLog(QuestionAnswerDiscountLog log)
        {
            using (var db = new QuestionDbContext())
            {
                db.QuestionAnswerDiscountLogEntities.Add(log);
                return db.SaveChanges() > 0;
            }
        }

        #endregion QuestionAnswerDiscountLog

        #region QuestionAnswer & QuestionAnswerOption

        public List<QuestionAnswer> GetQuestionAnswer(List<int> answerIds)
        {
            using (var db = new QuestionDbContext())
            {
                var result = (from qc in db.QuestionAnswerEntities
                                 where answerIds.Contains(qc.Id)
                                 select qc).ToList() ?? new List<QuestionAnswer>();
                return result;
            }
        }

        public QuestionAnswer GetQuestionAnswer(int answerId)
        {
            using (var db = new QuestionDbContext())
            {
                var result = (from qc in db.QuestionAnswerEntities
                    where qc.Id == answerId
                              select qc).FirstOrDefault() ?? new QuestionAnswer();
                return result;
            }
        }

        public List<QuestionAnswer> GetQuestionAnswerByEventId(int userId, int eventId)
        {
            using (var db = new QuestionDbContext())
            {
                var result = (from qc in db.QuestionAnswerEntities
                    where qc.UserId == userId
                    && qc.EventId == eventId
                    select qc).ToList();
                return result;
            }
        }

        public bool SaveQuestionAnswer(QuestionAnswer answer)
        {
            if (answer != null && answer.Id > 0)
            {
                using (var db = new QuestionDbContext())
                {
                    db.Entry(answer).State = System.Data.Entity.EntityState.Modified;
                    var result = db.SaveChanges();
                    return result > 0;
                }
            }
            return false;
        }

        public bool AnswerQuestion(QuestionAnswer answer, List<QuestionAnswerOption> options)
        {
            using (TransactionScope ts = TransactionScopeBuilder.CreateReadCommitted())
            {
                using (var db = new QuestionDbContext())
                {
                    db.QuestionAnswerEntities.Add(answer);

                    if (db.SaveChanges() <= 0) return false;

                    foreach (var o in options)
                    {
                        o.AnswerId = answer.Id;
                    }

                    db.QuestionAnswerOptionEntities.AddRange(options);

                    if (db.SaveChanges() <= 0) return false;
                    
                    ts.Complete();
                    return true;
                }
            }
        }

        #endregion QuestionAnswer & QuestionAnswerOption
    }
}

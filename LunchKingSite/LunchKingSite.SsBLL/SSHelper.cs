using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using SubSonic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LunchKingSite.SsBLL
{
    internal static class SubSonicExtMethods
    {
        public static QueryCommandCollection AddOrNot(this QueryCommandCollection myself, QueryCommand qc)
        {
            if (qc != null)
            {
                myself.Add(qc);
            }

            return myself;
        }
    }

    internal class SSHelper
    {
        private const string PAGING_SQL2005 = @"SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY {0}) AS Row, {1}) AS PR WHERE Row >= {2} AND Row <= {3}";
        private static readonly string[] tokens = { "and ", "or " };
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();

        /// <summary>
        /// same as above but takes multiple where statement
        /// </summary>
        /// <param name="qry">original query object</param>
        /// <param name="orderBy">order by string</param>
        /// <param name="filter">filter array</param>
        /// <returns></returns>
        public static Query GetQueryByFilterOrder(Query qry, string orderBy, params string[] filter)
        {
            int i, j, k;
            string result;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }

            for (i = 0; filter != null && i < filter.Length; i++)
            {
                result = filter[i];
                if (!string.IsNullOrEmpty(result))
                {
                    k = -1;
                    for (j = 0; j < tokens.Length && k < 0; j++)
                        k = result.IndexOf(tokens[j], 0, Math.Min(result.Length, 5));
                    if (k >= 0) result = result.Substring(tokens[--j].Length);

                    if (result.ToLower().IndexOf(" in ") > 0) // special case for in statement
                    {
                        // matches "column_name in csv", if csv is enclosed in parenthesis, parenthesis is stripped down
                        // if csv is not enclosed in parenthesis, parenthesis can be included in value of csv
                        Regex regex = new Regex(@"(.+) +in +(\(?)((?(\2).+|[^\)]+))\)?", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            qry = qry.IN(m.Groups[1].Value, m.Groups[3].Value.Split(new char[] { ',' }));
                            continue;
                        }
                    }
                    if (result.IndexOf("between", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +between +(?<d1>([0123456789:/APMapm ]+)) +and +(?<d2>([0123456789:/APMapm ]+))", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            Where.WhereCondition wh = (j != 1) ? Where.WhereCondition.AND : Where.WhereCondition.OR;
                            qry = qry.BETWEEN_AND(qry.Schema.TableName, m.Groups["c"].Value, DateTime.Parse(m.Groups["d1"].Value), DateTime.Parse(m.Groups["d2"].Value), wh);
                            continue;
                        }
                    }
                    if (result.IndexOf(" like ") > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +like +(?<v>.+)", RegexOptions.Compiled);
                        Match m = regex.Match(result);
                        if (m.Success)
                        {
                            qry.AddWhere(m.Groups["c"].Value, Comparison.Like, m.Groups["v"].Value);
                            continue;
                        }
                    }
                    if (result.ToLower().IndexOf(" null") > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +(?<p>(is( +not)*)) +null", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            Comparison comp = Comparison.Is;
                            if (m.Groups["p"].Value.IndexOf("not") > 0)
                            {
                                comp = Comparison.IsNot;
                            }
                            qry = qry.AddWhere(m.Groups["c"].Value, comp, null);
                            continue;
                        }
                    }
                    switch (j)
                    {
                        case 0:
                            qry = qry.AND(result);
                            break;

                        case 1:
                            qry = qry.OR(result);
                            break;

                        case 2:
                        default:
                            qry = qry.WHERE(result);
                            break;
                    }
                }
            }
            return qry;
        }

        /// <summary>
        /// 璝filterず甧ㄤ逆嘿把计ず甧讽いSubsonicQuery.Where穦ぃ或replace把计ず甧把计砆э跑礚猭タ絋琩高
        /// 矪瞶拜肈э耞Τ"="暗琩高粂猭
        /// ex: column:email; value:pacy@livemial.com => email=pacy@liv.com
        /// </summary>
        /// <param name="qry">original query object</param>
        /// <param name="orderBy">order by string</param>
        /// <param name="filter">filter array</param>
        /// <returns></returns>
        public static Query GetQueryByFilterOrderForFilter(Query qry, string orderBy, params string[] filter)
        {
            int i, j, k;
            string result;
            if (!string.IsNullOrEmpty(orderBy))
            {
                qry = qry.ORDER_BY(orderBy);
            }

            for (i = 0; filter != null && i < filter.Length; i++)
            {
                result = filter[i];
                if (!string.IsNullOrEmpty(result))
                {
                    k = -1;
                    for (j = 0; j < tokens.Length && k < 0; j++)
                    {
                        k = result.IndexOf(tokens[j], 0, Math.Min(result.Length, 5));
                    }

                    if (k >= 0)
                    {
                        result = result.Substring(tokens[--j].Length);
                    }

                    if (result.ToLower().IndexOf(" in ") > 0) // special case for in statement
                    {
                        // matches "column_name in csv", if csv is enclosed in parenthesis, parenthesis is stripped down
                        // if csv is not enclosed in parenthesis, parenthesis can be included in value of csv
                        Regex regex = new Regex(@"(.+) +in +(\(?)((?(\2).+|[^\)]+))\)?", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            qry = qry.IN(m.Groups[1].Value, m.Groups[3].Value.Split(new char[] { ',' }));
                            continue;
                        }
                    }
                    if (result.ToLower().IndexOf(" null") > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +(?<p>(is( +not)*)) +null", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            Comparison comp = Comparison.Is;
                            if (m.Groups["p"].Value.IndexOf("not") > 0)
                            {
                                comp = Comparison.IsNot;
                            }

                            qry = qry.AddWhere(m.Groups["c"].Value, comp, null);
                            continue;
                        }
                    }
                    if (result.IndexOf("between", StringComparison.InvariantCultureIgnoreCase) > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +between +(?<d1>([0123456789:/APMapm ]+)) +and +(?<d2>([0123456789:/APMapm ]+))", RegexOptions.IgnoreCase);
                        Match m = regex.Match(result.Trim());
                        if (m.Success)
                        {
                            Where.WhereCondition wh = (j != 1) ? Where.WhereCondition.AND : Where.WhereCondition.OR;
                            qry = qry.BETWEEN_AND(qry.Schema.TableName, m.Groups["c"].Value, DateTime.Parse(m.Groups["d1"].Value), DateTime.Parse(m.Groups["d2"].Value), wh);
                            continue;
                        }
                    }
                    if (result.IndexOf(" like ") > 0)
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) +like +(?<v>.+)", RegexOptions.Compiled);
                        Match m = regex.Match(result);
                        if (m.Success)
                        {
                            qry.AddWhere(m.Groups["c"].Value, Comparison.Like, m.Groups["v"].Value);
                            continue;
                        }
                    }
                    {
                        Regex regex = new Regex(@"(?<c>(.+)) *= *(?<v>.+)", RegexOptions.Compiled);
                        Match m = regex.Match(result);
                        if (m.Success)
                        {
                            qry.AddWhere(m.Groups["c"].Value, Comparison.Equals, m.Groups["v"].Value);
                        }
                        else
                        {
                            switch (j)
                            {
                                case 0:
                                    qry = qry.AND(result);
                                    break;

                                case 1:
                                    qry = qry.OR(result);
                                    break;

                                case 2:
                                default:
                                    qry = qry.WHERE(result);
                                    break;
                            }
                        }
                    }
                }
            }
            return qry;
        }

        /// <summary>
        /// return the paged collection of a view that is filtered & ordered by arguments passed in
        /// </summary>
        /// <typeparam name="ItemType">the name of the view class</typeparam>
        /// <typeparam name="ListType">the name of the collection of the view class</typeparam>
        /// <param name="pageStart">start of the page, from 1</param>
        /// <param name="pageLength">length of each page</param>
        /// <param name="orderBy">order by string</param>
        /// <param name="filter">contraints, filter rule (the where statement)</param>
        /// <returns></returns>

        public static ListType GetQueryResultByFilterOrder<ItemType, ListType>(int pageStart, int pageLength, string orderBy, params string[] filter)
            where ItemType : RecordBase<ItemType>, new()
            where ListType : AbstractList<ItemType, ListType>, new()
        {
            Query qry = new Query(new ItemType().TableName);
            qry = GetQueryByFilterOrder(qry, orderBy, filter);
            if (pageLength > 0)
            {
                qry.PageIndex = pageStart;
                qry.PageSize = pageLength;
            }
            ListType view = new ListType();
            view.LoadAndCloseReader(qry.ExecuteReader());
            return view;
        }

        public static ListType GetQueryResultByFilterOrder<ItemType, ListType>(int pageStart, int pageLength, string orderBy, bool asc, params string[] filter)
            where ItemType : RecordBase<ItemType>, new()
            where ListType : AbstractList<ItemType, ListType>, new()
        {
            Query qry = new Query(new ItemType().TableName);
            qry = GetQueryByFilterOrder(qry, orderBy, filter);

            if (!asc && !string.IsNullOrEmpty(orderBy))
            {
                qry.OrderBy = OrderBy.Desc(orderBy);
            }
            if (pageLength > 0)
            {
                qry.PageIndex = pageStart;
                qry.PageSize = pageLength;
            }
            ListType view = new ListType();
            view.LoadAndCloseReader(qry.ExecuteReader());
            return view;
        }

        public static ListType GetQueryResultForProcess<ItemType, ListType>(int pageStart, int pageLength, string orderBy, bool asc,string orderByAsc, params string[] filter)
            where ItemType : RecordBase<ItemType>, new()
            where ListType : AbstractList<ItemType, ListType>, new()
        {
            Query qry = new Query(new ItemType().TableName);
            qry = GetQueryByFilterOrder(qry, orderBy, filter);


            if (!asc && !string.IsNullOrEmpty(orderBy))
            {
                qry.OrderBy = OrderBy.Desc(orderBy);
            }
            qry.OrderByCollection.Add(OrderBy.Asc(orderByAsc));
            if (pageLength > 0)
            {
                qry.PageIndex = pageStart;
                qry.PageSize = pageLength;
            }
            ListType view = new ListType();
            view.LoadAndCloseReader(qry.ExecuteReader());
            return view;
        }


        /// <summary>
        /// 矪瞶だ琩高
        /// </summary>
        /// <param name="qc">琩高sql</param>
        /// <param name="page">璶琩高材碭戈惠单1タ盽琩高玥穦琩ぃㄓ</param>
        /// <param name="pageSize">–戈掸计单0ボ琩ぃだ</param>
        /// <param name="orderBy">だ逼兵ン</param>
        /// <returns></returns>
        public static QueryCommand MakePagable(QueryCommand qc, int page, int pageSize, string orderBy)
        {
            if (pageSize <= 0)
            {
                return qc;
            }

            QueryCommand newQc = qc;
            string sql = qc.CommandSql;
            int tbStartIndex = sql.IndexOf("from ", StringComparison.CurrentCultureIgnoreCase);
            int orderStartIndex = sql.LastIndexOf("order by", StringComparison.CurrentCultureIgnoreCase);

            string defOrderBy = string.Empty;
            if (orderStartIndex > 0)
            {
                defOrderBy = sql.Substring(orderStartIndex, sql.Length - orderStartIndex);
                sql = sql.Substring(0, orderStartIndex);
            }
            else
            {
                defOrderBy = " order by " + orderBy;
            }
            sql = sql.Insert(tbStartIndex, " ,ROW_NUMBER() OVER ( " + defOrderBy + " ) AS __Serial ");
            sql = "SELECT * FROM " +
                  " ( " + sql + " ) as tb " +
                  " WHERE tb.__Serial BETWEEN ((@Page - 1) * @PageSize + 1) AND (@Page * @PageSize) " +
                  " order by __Serial ";
            newQc.CommandSql = sql;
            newQc.AddParameter("@Page", page, System.Data.DbType.Int32);
            newQc.AddParameter("@PageSize", pageSize, System.Data.DbType.Int32);
            return newQc;
        }

        public static string MakePagableSql(string origSql, int curPage, int pageSize)
        {
            if (pageSize < 0 || string.IsNullOrEmpty(origSql))
            {
                return origSql;
            }

            Regex r = new Regex(@"\s*\(*\s*select\s+(?<cols>.+\s+from\s+.+\s+.*)\s+(order\s+by\s+(?<ord>.+))*",
                                RegexOptions.IgnoreCase | RegexOptions.Compiled);
            Match m = r.Match(origSql);
            if (!m.Success)
            {
                return origSql;
            }

            string colList = m.Groups["cols"].Value;
            string firstCol = colList.Substring(0, colList.IndexOfAny(new char[] { ',', ' ' }));
            string orderBy = m.Groups["ord"].Value;
            if (string.IsNullOrEmpty(orderBy))
            {
                orderBy = firstCol;
            }
            int pageStart = (curPage - 1) * pageSize + 1;
            int pageEnd = curPage * pageSize;

            return string.Format(PAGING_SQL2005, orderBy, colList, pageStart, pageEnd);
        }

        public static QueryCommand GetWhereQC<ItemType>(params string[] filter)
            where ItemType : RecordBase<ItemType>, new()
        {
            SubSonic.TableSchema.Table schema = new ItemType().GetSchema();
            QueryCommand qc = new QueryCommand(" ", schema.Provider.Name);
            if (filter != null && filter.Length > 0)
            {
                Query qry = new Query(schema);
                qc.CommandSql += DataProvider.BuildWhere(SSHelper.GetQueryByFilterOrder(qry, null, filter));
                DataProvider.AddWhereParameters(qc, qry);
            }
            return qc;
        }

        public static QueryCommand GetQryCmd<T>(T data) where T : RecordBase<T>, new()
        {
            return (data.IsNew)
                       ? ActiveHelper<T>.GetInsertCommand(data, null)
                       : ActiveHelper<T>.GetUpdateCommand(data, null);
        }

        /// <summary>
        /// 舱SqlQuery籔orderBy兵ン
        /// </summary>
        /// <param name="query"></param>
        /// <param name="orderBy"></param>
        /// <returns></returns>
        public static SqlQuery GetOrderBySqlQuery(SqlQuery query, List<string> orderBy)
        {
            return orderBy.Aggregate(query,
                          (current, o) =>
                          o.ToLower().IndexOf("desc") > 0
                              ? current.OrderDesc(o.ToLower().Replace("desc", "").Trim())
                              : current.OrderAsc(o.ToLower().Replace("asc", "").Trim()));
        }

    }
}
﻿using System.ComponentModel.Composition;
using System.Reflection;
using Autofac;
using LunchKingSite.Core;
using Vodka.Container;

namespace LunchKingSite.SsBLL
{
    [Export(typeof(IModuleRegistrar))]
    public class SsBllModule : IModuleRegistrar
    {
        public void RegisterWithContainer(ContainerBuilder builder)
        {
            builder.RegisterAssemblyTypes(Assembly.GetExecutingAssembly()).Where(t => typeof(IProvider).IsAssignableFrom(t)).AsImplementedInterfaces().InstancePerLifetimeScope();
        }

        public void Initialize(IContainer container)
        {
        }
    }
}

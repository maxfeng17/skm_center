﻿using System;
using System.Web.Http;
using System.Web.Http.Dispatcher;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.WebApi.Core;
using LunchKingSite.WebApi.Core.OAuth;
using Newtonsoft.Json;
using System.Linq;
using System.Web;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using log4net;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.WebApi.Controllers.Base;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;
using System.Web.Http.ExceptionHandling;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Threading;
using LunchKingSite.BizLogic.Component;

namespace LunchKingSite.WebApi
{
    /// <summary>
    /// 
    /// </summary>
    public static class WebApiConfig
    {
        static ILog logger = LogManager.GetLogger(typeof(WebApiConfig));
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            //MessageHandler自動處理AccessToken轉成Principal
            config.MessageHandlers.Add(new AuthMessageHandler(
                new OAuth2TokenProvider(), new OAuth2PrincipalProvider(), new OAuth2AppClientProvider()
            ));
            config.Services.Replace(typeof(IExceptionHandler), new OopsExceptionHandler());

            // Attribute routing.
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "oauth2",
                routeTemplate: "oauth2/{action}",
                defaults: new
                {
                    controller = "OAuth2"
                });

            config.Routes.MapHttpRoute(
                name: "oauth2.v2",
                routeTemplate: "api/v2/oauth2/{action}",
                defaults: new
                {
                    controller = "OAuth2"
                });

            config.Routes.MapHttpRoute(
                name: "memberservice",
                routeTemplate: "api/sso/memberservice/{action}",
                defaults: new
                {
                    controller = "MemberService"
                });

            config.Routes.MapHttpRoute(
                name: "ga_token",
                routeTemplate: "api/getgatoken",
                defaults: new {controller = "Application", action = "GetGaToken"}
                );

            config.Routes.MapHttpRoute(
                name: "wechat",
                routeTemplate: "api/wechat/api",
                defaults: new {controller = "WeChat"}
                );

            config.Routes.MapHttpRoute(
                name: "ppondealindex",
                routeTemplate: "api/dealsearch/{querystring}/{max}",
                defaults: new {controller = "Ppon", action = "PponIndexSearch"}
                );

            config.Routes.MapHttpRoute(
                name: "ppondealindextest",
                routeTemplate: "api/dealsearchtest/{querystring}/{max}",
                defaults: new {controller = "Ppon", action = "PponIndexSearchTest"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherseller",
                routeTemplate: "api/voucher/yahoo/seller",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherSeller"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovouchersellerbyid",
                routeTemplate: "api/voucher/yahoo/sellerid/{id}",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherSellerById"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherstore",
                routeTemplate: "api/voucher/yahoo/store",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherStore"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherstorebyeventid",
                routeTemplate: "api/voucher/yahoo/storebyeventid/{eventid}",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherStoreByEventId"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovouchercategory",
                routeTemplate: "api/voucher/yahoo/category",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherCategory"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherpromo",
                routeTemplate: "api/voucher/yahoo/promo",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherPromo"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherpagecount",
                routeTemplate: "api/voucher/yahoo/pagecount",
                defaults: new {controller = "Vourcher", action = "GetYahooVoucherPageCountSum"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovouchersetpagecount",
                routeTemplate: "api/voucher/yahoo/setpagecount",
                defaults: new {controller = "Vourcher", action = "SetVoucherPageCount"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoovoucherpromosetpagecount",
                routeTemplate: "api/voucher/yahoo/setpromopagecount",
                defaults: new {controller = "Vourcher", action = "SetVoucherPromoPageCount"}
                );

            config.Routes.MapHttpRoute(
                name: "yahooppondeal",
                routeTemplate: "api/voucher/yahoo/deal/{channel_id}/{channel_area_id}",
                defaults: new {controller = "Ppon", action = "PponDealGet", channel_area_id = RouteParameter.Optional}
                );

            config.Routes.MapHttpRoute(
                name: "yahoopponcity",
                routeTemplate: "api/voucher/yahoo/city",
                defaults: new {controller = "Ppon", action = "PponCityGet"}
                );

            config.Routes.MapHttpRoute(
                name: "yahoocategorydependency",
                routeTemplate: "api/voucher/yahoo/categorydependency",
                defaults: new {controller = "Ppon", action = "CategoryDependencyGet"}
                );

            config.Routes.MapHttpRoute(
                name: "einvoice",
                routeTemplate: "api/einvoice/api",
                defaults: new {controller = "Einvoice"}
                );

            config.Routes.MapHttpRoute(
                name: "PponService",
                routeTemplate: "service/ppon/{action}",
                defaults: new
                {
                    controller = "PponService"
                });


            config.Routes.MapHttpRoute(
                name: "payment",
                routeTemplate: "api/payment/makeorder",
                defaults: new
                {
                    controller = "Payment",
                    action = "MakeOrder"
                });

            config.Routes.MapHttpRoute(
                name: "Promo",
                routeTemplate: "api/promo/{action}",
                defaults: new {controller = "Promo"}
                );

            config.Routes.MapHttpRoute(
                name: "POSVerify",
                routeTemplate: "api/POSVerify/{action}",
                defaults: new { controller = "POSVerify" }
                );

            config.Routes.MapHttpRoute(
                name: "ShoppingGuide",
                routeTemplate: "api/ShoppingGuide/{action}",
                defaults: new { controller = "ShoppingGuide" }
            );


            config.Routes.MapHttpRoute(
                name: "LionTravel_CategoryRootList",
                routeTemplate: "api/sharedeal/categoryrootlist/{accesstoken}",
                defaults: new { controller = "LionTravel", action = "CategoryRootList" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_CategoryList",
                routeTemplate: "api/sharedeal/categorylist/{accesstoken}",
                defaults: new { controller = "LionTravel", action = "CategoryList" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DealList",
                routeTemplate: "api/sharedeal/deallist/{accesstoken}/{channelid}/{areaid}",
                defaults: new { controller = "LionTravel", action = "DealList", channelid = RouteParameter.Optional, areaid = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DeliveryDealList",
                routeTemplate: "api/sharedeal/deliverydeallist/{accesstoken}/{channelid}",
                defaults: new { controller = "LionTravel", action = "DeliveryDealList", channelid = RouteParameter.Optional }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_Deal",
                routeTemplate: "api/sharedeal/deal/{accesstoken}/{dealguid}",
                defaults: new { controller = "LionTravel", action = "Deal" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DealSearch",
                routeTemplate: "api/sharedeal/dealsearch/{accesstoken}/{querystring}/{max}",
                defaults: new { controller = "LionTravel", action = "DealSearch" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_MakeOrderJson",
                routeTemplate: "api/sharedeal/DeliveryTestModel/{accesstoken}",
                defaults: new { controller = "LionTravel", action = "DeliveryTestModel" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DealStores",
                routeTemplate: "api/sharedeal/dealstores/{accesstoken}/{dealguid}",
                defaults: new {controller = "LionTravel", action = "DealStores"}
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DealDelivery",
                routeTemplate: "api/sharedeal/dealdelivery/{accesstoken}/{dealguid}",
                defaults: new { controller = "LionTravel", action = "DealDelivery" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_DealTypeList",
                routeTemplate: "api/sharedeal/dealtypelist/{accesstoken}",
                defaults: new { controller = "LionTravel", action = "DealTypeList" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_FamiStoreList",
                routeTemplate: "api/sharedeal/famistorelist/{accesstoken}",
                defaults: new { controller = "LionTravel", action = "FamiStoreList" }
                );

            config.Routes.MapHttpRoute(
               name: "LionTravel_BankInfoList",
               routeTemplate: "api/sharedeal/bankInfolist/{accesstoken}",
               defaults: new { controller = "LionTravel", action = "BankInfoList" }
               );

            config.Routes.MapHttpRoute(
                name: "LionTravel_MakeOrder",
                routeTemplate: "api/sharedeal/makeorder/{accesstoken}",
                defaults: new {controller = "Payment", action = "MakeLionTravelOrder"}
                );

            config.Routes.MapHttpRoute(
               name: "LionTravel_Refund",
               routeTemplate: "api/sharedeal/refund/{accesstoken}",
               defaults: new { controller = "Payment", action = "RefundLionTravelOrder" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_CheckDeliveryStatus",
               routeTemplate: "api/sharedeal/checkdeliverystatus/{accesstoken}",
               defaults: new { controller = "Payment", action = "CheckDeliveryStatus" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_CheckDeliveryRefundOrder",
               routeTemplate: "api/sharedeal/CheckDeliveryRefundOrder/{accesstoken}",
               defaults: new { controller = "Payment", action = "CheckDeliveryRefundOrder" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_DeliveryRefund",
               routeTemplate: "api/sharedeal/DeliveryRefund/{accesstoken}",
               defaults: new { controller = "Payment", action = "DeliveryRefund" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_CheckDeliveryRefundStatus",
               routeTemplate: "api/sharedeal/CheckDeliveryRefundStatus/{accesstoken}",
               defaults: new { controller = "Payment", action = "CheckDeliveryRefundStatus" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_CheckRefundFormStatus",
               routeTemplate: "api/sharedeal/CheckRefundFormStatus/{accesstoken}",
               defaults: new { controller = "Payment", action = "CheckRefundFormStatus" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_CheckCouponRefundForm",
               routeTemplate: "api/sharedeal/CheckCouponRefundForm/{accesstoken}",
               defaults: new { controller = "Payment", action = "CheckCouponRefundForm" }
               );

            config.Routes.MapHttpRoute(
               name: "LionTravel_GroupCouponTrustInfo",
               routeTemplate: "api/sharedeal/GroupCouponTrustInfo/{accessToken}",
               defaults: new { controller = "Payment", action = "GroupCouponTrustInfo" }
               );

            config.Routes.MapHttpRoute(
                name: "LionTravel_ReSms",
                routeTemplate: "api/sharedeal/sms/{accesstoken}",
                defaults: new {controller = "Payment", action = "SendSms"}
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_SendAuthCodeSms",
                routeTemplate: "api/sharedeal/sendauthcodesms/{accesstoken}",
                defaults: new { controller = "Payment", action = "SendAuthCodeSms" }
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_CouponStatus",
                routeTemplate: "api/sharedeal/couponstatus/{accesstoken}",
                defaults: new {controller = "Payment", action = "CheckLionTravelCouponStatus"}
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_MultiCouponStatus",
                routeTemplate: "api/sharedeal/multicouponstatus/{accesstoken}",
                defaults: new {controller = "Payment", action = "CheckLionTravelMultiCouponStatus"}
                );

            config.Routes.MapHttpRoute(
              name: "LionTravel_CouponStatusBySearchTime",
              routeTemplate: "api/sharedeal/couponstatusbysearchtime/{accesstoken}",
              defaults: new { controller = "Payment", action = "CheckLionTravelCouponStatusBySearchTime" }
              );

            config.Routes.MapHttpRoute(
                name: "LionTravel_OrderInfo",
                routeTemplate: "api/sharedeal/orderinfo/{accesstoken}",
                defaults: new {controller = "Payment", action = "CheckLionTravelOrder"}
                );

            config.Routes.MapHttpRoute(
                name: "LionTravel_MultiOrderInfo",
                routeTemplate: "api/sharedeal/multiorderinfo/{accesstoken}",
                defaults: new { controller = "Payment", action = "CheckLionTravelMultiOrder" }
                );

            config.Routes.MapHttpRoute(
                name: "PChome_MakeOrder",
                routeTemplate: "api/sharedeal/pcmakeorder/{accesstoken}",
                defaults: new { controller = "Payment", action = "MakePChomeOrder" }
                );

            config.Routes.MapHttpRoute(
               name: "PChome_Refund",
               routeTemplate: "api/sharedeal/pcrefund/{accesstoken}",
               defaults: new { controller = "Payment", action = "RefundPChomeOrder" }
               );

            config.Routes.MapHttpRoute(
                name: "PChome_MultiCouponStatus",
                routeTemplate: "api/sharedeal/pcmulticouponstatus/{accesstoken}",
                defaults: new { controller = "Payment", action = "CheckPChomeMultiCouponStatus" }
                );

            config.Routes.MapHttpRoute(
                name: "FamiMap",
                routeTemplate: "api/familymap",
                defaults: new {controller = "Fami", action = "FamiMap"}
                );

            config.Routes.MapHttpRoute(
                name: "Question",
                routeTemplate: "api/question/{action}",
                defaults: new { controller = "Question" }
            );

            config.Routes.MapHttpRoute(
                name: "Question_GetEvent",
                routeTemplate: "api/question/GetEvent/{token}",
                defaults: new { controller = "Question", action = "GetEvent" }
            );

            #region mock pay

            config.Routes.MapHttpRoute(
                name: "TaishinBindPayReq",
                routeTemplate: "mock/api/BindPayReq",
                defaults: new {controller = "Mock", action = "TaishinBindPayReq" }
            );
            config.Routes.MapHttpRoute(
                name: "TaishinAPIBindUserInfo",
                routeTemplate: "mock/api/BindUserInfo",
                defaults: new {controller = "Mock", action = "TaishinAPIBindUserInfo" }
            );
            config.Routes.MapHttpRoute(
                name: "TaishinThirdPartyPay",
                routeTemplate: "mock/third_party/Pay",
                defaults: new {controller = "Mock", action = "TaishinThirdPartyPay" }
            );
            config.Routes.MapHttpRoute(
                name: "MockLinePayRequest",
                routeTemplate: "mock/linepay/v1/payments/request",
                defaults: new { controller = "Mock", action = "LinePayRequest" }
            );
            config.Routes.MapHttpRoute(
                name: "MockLinePayPayments",
                routeTemplate: "mock/linepay/web/payments/wait",
                defaults: new { controller = "Mock", action = "LinePayPayments" }
            );
            config.Routes.MapHttpRoute(
                name: "MockLinePayConfirm",
                routeTemplate: "mock/linepay/v1/payments/{transactionId}/confirm",
                defaults: new { controller = "Mock", action = "LinePayConfirm" }
            );
            config.Routes.MapHttpRoute(
                name: "MockLinePayRefund",
                routeTemplate: "mock/linepay/v1/payments/{transactionId}/refund",
                defaults: new { controller = "Mock", action = "LinePayRefund" }
            );
            config.Routes.MapHttpRoute(
                name: "e7track",
                routeTemplate: "api/e7track",
                defaults: new { controller = "E7Track", action = "Record" }
            );


            #endregion

            //default
            config.Routes.MapHttpRoute(
                name: "api",
                routeTemplate: "api/{controller}/{action}/{accessToken}",
                defaults: new {accessToken = RouteParameter.Optional}
                );

            //config.Services.Replace(typeof(IHttpControllerSelector),
            //    new NamespaceHttpControllerSelector(config));

            //只移除XML Formatter，其他Formatter還是會需要用到，例如吃POST的FormUrlEncodedMediaTypeFormatter。
            var matches = config.Formatters
                            .Where(f => f.SupportedMediaTypes
                                         .Where(m => m.MediaType.ToString() == "application/xml" ||
                                                     m.MediaType.ToString() == "text/xml")
                                         .Count() > 0)
                            .ToList();
            foreach (var match in matches)
            {
                config.Formatters.Remove(match);
            }

            config.BindParameter(typeof(OAuthLoginInfo), new OAuthLoginInfoBinder());
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings = new JsonSerializerSettings
            {
                Formatting = Formatting.None
            };
            GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.Converters.Add(
                new AppDateTimeeConverter()
                );

            //var corsAttr = new EnableCorsAttribute("*", "Content-Type", "POST,GET,OPTIONS");
            var corsAttr = new EnableCorsAttribute("*", "*", "POST,GET,OPTIONS");
            corsAttr.SupportsCredentials = true;
            config.EnableCors(corsAttr);
        }
    }

    public class OopsExceptionHandler : ExceptionHandler
    {
        private static readonly log4net.ILog logger = log4net.LogManager.GetLogger("global");
        public override void Handle(ExceptionHandlerContext context)
        {
            if (ProviderFactory.Instance().GetConfig().OopsEnabled == false)
            {
                return;
            }
            try
            {
                Exception ex = context.Exception;                
                OopsClient.Instance.Write(ex, HttpContext.Current);
            }
            catch (Exception ex)
            {
                logger.Info(ex);
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public class AppDateTimeeConverter : JsonConverter
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="writer"></param>
        /// <param name="value"></param>
        /// <param name="serializer"></param>
        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }
            DateTime dt = (DateTime)value;
            if (dt == DateTime.MinValue)
            {
                writer.WriteNull();
            }
            else
            {
                writer.WriteValue(ApiSystemManager.DateTimeToDateTimeString(dt));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="objectType"></param>
        /// <param name="existingValue"></param>
        /// <param name="serializer"></param>
        /// <returns></returns>
        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (objectType == typeof(DateTime?) && string.IsNullOrWhiteSpace(reader.Value.ToString()))
            {
                return null;
            }
            return ApiSystemManager.DateTimeFromDateTimeString2(reader.Value.ToString());
            //return ApiSystemManager.DateTimeFromDateTimeString(reader.Value.ToString());
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectType"></param>
        /// <returns></returns>
        public override bool CanConvert(Type objectType)
        {
            if (objectType == typeof(DateTime) || objectType == typeof(DateTime?))
            {
                return true;
            }
            return false;
        }
    }
}

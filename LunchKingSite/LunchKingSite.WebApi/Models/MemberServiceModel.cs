﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.WebApi.Core;

namespace LunchKingSite.WebApi.Models
{
    #region input model

    /// <summary>
    /// 
    /// </summary>
    public class AddMemberCreditCardInputModel : IApiUserInputModel
    {
        /// <summary>
        /// APP Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 信用卡名稱
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// 信用卡號
        /// </summary>
        public string CardNo { get; set; }
        
        /// <summary>
        /// 有效年 YY
        /// </summary>
        public string ValidYear { get; set; }

        /// <summary>
        /// 有效月 MM
        /// </summary>
        public string ValidMonth { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class EditMemberCreditCardInputModel : IApiUserInputModel
    {
        /// <summary>
        /// APP Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 信用卡代碼
        /// </summary>
        public Guid CardGuid { get; set; }
        /// <summary>
        /// 信用卡名稱
        /// </summary>
        public string CardName { get; set; }
        /// <summary>
        /// 信用卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 有效年 YY
        /// </summary>
        public string ValidYear { get; set; }
        /// <summary>
        /// 有效月 MM
        /// </summary>
        public string ValidMonth { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetOrDeleteMemberCreditCardInputModel : IApiUserInputModel
    {
        /// <summary>
        /// APP Id
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 信用卡代碼
        /// </summary>
        public Guid CardGuid { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DefaultInputModel : IApiUserInputModel
    {
        /// <summary>
        /// APP Id
        /// </summary>
        public string UserId { get; set; }
    }

    #endregion
}

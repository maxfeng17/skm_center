﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointLogs
    {
        /// <summary>
        /// 
        /// </summary>
        public int PointType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PcpTransactionLog> Logs { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PcpTransactionLog
    {
        /// <summary>
        /// 
        /// </summary>
        public int LogType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LogTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Remind { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Point { get; set; }
    }
}

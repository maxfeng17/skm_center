﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class FamiApiLogModel
    {
        /// <summary>
        /// Exception Message
        /// </summary>
        public string ExMsg { set; get; }
        /// <summary>
        /// Exception StackTrace
        /// </summary>
        public string StackTrace { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string PinCode { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string TenCode { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string EventId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string MmkId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string PayDate { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string PayTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public Guid Bid { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountTemplateModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DiscountTemplateModel()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdc"></param>
        public DiscountTemplateModel(ViewPcpDiscountTemplate vpdc)
        {
            TemplateId = vpdc.Id;
            Amount = vpdc.Amount;
            MinimumAmount = vpdc.MinimumAmount.GetValueOrDefault();
            StartTime = vpdc.StartTime;
            EndTime = vpdc.EndTime;
            SituationType = vpdc.SituationType;
            AppendStoreDesc(vpdc.SellerName, vpdc.StoreName);
            this.IsDraft = vpdc.IsDraft;
        }
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Description
        {
            get
            {
                return string.Format("滿{0:0}元，出示熟客券折抵{1:0}元", MinimumAmount, Amount);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal MinimumAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SituationType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> StoreDesc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDraft { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdcCol"></param>
        /// <param name="useTestData"></param>
        /// <returns></returns>
        public static DiscountTemplateModel[] Create(ViewPcpDiscountTemplateCollection vpdcCol, bool useTestData = false)
        {
            if (useTestData)
            {
                return new DiscountTemplateModel[]
                {
                    new DiscountTemplateModel
                    {
                        TemplateId = 1,
                        Amount = 100,
                        MinimumAmount = 1000,
                        StartTime = ApiSystemManager.DateTimeFromDateTimeString("20130423 235900 +0800"),
                        EndTime = ApiSystemManager.DateTimeFromDateTimeString("20130423 235900 +0800"),
                        StoreDesc = new List<string>(new string[] {"王品《中山北店》"}),
                        IsDraft = true,
                        SituationType = 1
                    },
                    new DiscountTemplateModel
                    {
                        TemplateId = 2,
                        Amount = 100,
                        MinimumAmount = 800,
                        StartTime = ApiSystemManager.DateTimeFromDateTimeString("20130501 235900 +0800"),
                        EndTime = ApiSystemManager.DateTimeFromDateTimeString("20130524 235900 +0800"),
                        StoreDesc = new List<string>(new string[] {"陶板屋《七張店》"}),
                        IsDraft = true,
                        SituationType = 1
                    }
                };
            }


            Dictionary<int, DiscountTemplateModel> dict = new Dictionary<int, DiscountTemplateModel>();
            foreach (var vpdc in vpdcCol)
            {
                if (dict.ContainsKey(vpdc.Id) == false)
                {
                    dict[vpdc.Id] = new DiscountTemplateModel(vpdc);
                }
                else
                {
                    dict[vpdc.Id].AppendStoreDesc(vpdc.SellerName, vpdc.StoreName);
                }
            }

            DiscountTemplateModel[] resul = new DiscountTemplateModel[dict.Count];
            dict.Values.CopyTo(resul, 0);
            return resul;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sellerName"></param>
        /// <param name="storeName"></param>
        protected void AppendStoreDesc(string sellerName, string storeName)
        {
            if (StoreDesc == null)
            {
                StoreDesc = new List<string>();
            }
            StoreDesc.Add(sellerName + storeName);
        }

    }
    /// <summary>
    /// 
    /// </summary>
    public class CheckoutDiscountTemplateModel : DiscountTemplateModel
    {
        /// <summary>
        /// 
        /// </summary>
        public CheckoutDiscountTemplateModel() : base()
        {
            
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdc"></param>
        public CheckoutDiscountTemplateModel(ViewPcpDiscountTemplate vpdc):base(vpdc)
        {
            RecommendedPointType = PcpPointType.FavorPoint;
        }
        /// <summary>
        /// 
        /// </summary>
        public override string Description
        {
            get { return string.Format("{0:0}抵{1:0}(至{2:yyyy/MM/dd})", MinimumAmount, Amount , EndTime); }
        }

        /// <summary>
        /// 推薦使用熟客點或公關點
        /// </summary>
        public PcpPointType RecommendedPointType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdcCol"></param>
        /// <param name="useTestData"></param>
        /// <returns></returns>
        public new static CheckoutDiscountTemplateModel[] Create(ViewPcpDiscountTemplateCollection vpdcCol, bool useTestData = false)
        {
            if (useTestData)
            {
                return new CheckoutDiscountTemplateModel[]
                {
                    new CheckoutDiscountTemplateModel
                    {
                        TemplateId = 1,
                        Amount = 100,
                        MinimumAmount = 1000,
                        StartTime = ApiSystemManager.DateTimeFromDateTimeString("20130423 235900 +0800"),
                        EndTime = ApiSystemManager.DateTimeFromDateTimeString("20130423 235900 +0800"),
                        StoreDesc = new List<string>(new string[] {"王品《中山北店》"}),
                        IsDraft = true,
                        SituationType = 1
                    },
                    new CheckoutDiscountTemplateModel
                    {
                        TemplateId = 2,
                        Amount = 100,
                        MinimumAmount = 800,
                        StartTime = ApiSystemManager.DateTimeFromDateTimeString("20130501 235900 +0800"),
                        EndTime = ApiSystemManager.DateTimeFromDateTimeString("20130524 235900 +0800"),
                        StoreDesc = new List<string>(new string[] {"陶板屋《七張店》"}),
                        IsDraft = true,
                        SituationType = 1
                    }
                };
            }


            Dictionary<int, CheckoutDiscountTemplateModel> dict = new Dictionary<int, CheckoutDiscountTemplateModel>();
            foreach (var vpdc in vpdcCol)
            {
                if (dict.ContainsKey(vpdc.Id) == false)
                {
                    dict[vpdc.Id] = new CheckoutDiscountTemplateModel(vpdc);
                }
                else
                {
                    dict[vpdc.Id].AppendStoreDesc(vpdc.SellerName, vpdc.StoreName);
                }
            }

            CheckoutDiscountTemplateModel[] resul = new CheckoutDiscountTemplateModel[dict.Count];
            dict.Values.CopyTo(resul, 0);
            return resul;
        }
    }
}

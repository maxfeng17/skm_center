﻿using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class GiveDiscountCodeToMemberInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int UserCardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public PcpPointType PointType { set; get; }
    }
}

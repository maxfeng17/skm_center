﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountTemplateDetailModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int CampaignId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description
        {
            get
            {
                return string.Format("滿{0}元，出示熟客券折抵{1}元", MinimumAmount, Amount);                 
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal MinimumAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AvailableDateType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AvailableDateDesc { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CardCombineUse { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Instruction { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsDraft { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SituationType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<OptionStoreModel> Stores { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DiscountTemplateDetailCampaignModel> CampaignList { get; set; }

        private static Dictionary<int, string> availasbleDateDescDict = new Dictionary<int, string>()
        {
            { 0, ""},
            { 1, "均適用"},
            { 2, "特殊假日不適用"},
            { 3, "週六、日不適用"},
        };
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpdcCol"></param>
        /// <param name="vpadcCol"></param>
        /// <param name="allowStores"></param>
        /// <param name="testMode"></param>
        /// <returns></returns>
        public static DiscountTemplateDetailModel Create(ViewPcpDiscountTemplateCollection vpdcCol, ViewPcpAssignmentDiscountCampaignCollection vpadcCol, SellerCollection allowStores, bool testMode)
        {
            if (testMode)
            {
                return CreateTestData();
            }
            if (vpdcCol.Count == 0)
            {
                return null;
            }
            ViewPcpDiscountTemplate vpdc = vpdcCol[0];
            if (vpdcCol.Any(t => t.Id != vpdc.Id))
            {
                return null;
            }

            DiscountTemplateDetailModel result = new DiscountTemplateDetailModel
            {
                CampaignId = vpdc.Id,
                Amount = vpdc.Amount,
                MinimumAmount = vpdc.MinimumAmount.GetValueOrDefault(),
                StartTime = vpdc.StartTime,
                EndTime = vpdc.EndTime,
                IsDraft = vpdc.IsDraft,
                Instruction = @"‧ 本券限折抵乙次，每筆交易限用乙張。
‧ 結帳時請主動出示，恕無法折換現金。
‧ 本公司保留修改或取消活動之權益。
‧ 本券可與熟客卡合併使用",
                CardCombineUse = vpdc.CardCombineUse,
                AvailableDateType = vpdc.AvailableDateType,
                SituationType =vpdc.SituationType,
                AvailableDateDesc = availasbleDateDescDict[vpdc.AvailableDateType],
                CampaignList = vpadcCol.Select(vpadc => new DiscountTemplateDetailCampaignModel(vpadc)).ToList()
            };
            
            result.Stores = new List<OptionStoreModel>();
            foreach (var st in allowStores)
            {
                //判斷是否被選取
                bool selected = vpdcCol.FirstOrDefault(t => t.StoreGuid == st.Guid) != null;
                OptionStoreModel storeModel = new OptionStoreModel(st, selected , true);
                result.Stores.Add(storeModel);
            }

            return result;
        }

        private static DiscountTemplateDetailModel CreateTestData()
        {
            return new DiscountTemplateDetailModel
            {
                CampaignId = 1,
                Amount = 100,
                MinimumAmount = 1000,
                AvailableDateType = 1,
                AvailableDateDesc = "特殊假日及六日不試用",
                StartTime = ApiSystemManager.DateTimeFromDateTimeString("20130501 235900 +0800"),
                EndTime = ApiSystemManager.DateTimeFromDateTimeString("20130524 235900 +0800"),                
                SituationType = 1,
                CardCombineUse = true,
                Instruction = @"‧ 本券限折抵乙次，每筆交易限用乙張。
‧ 結帳時請主動出示，恕無法折換現金。
‧ 本公司保留修改或取消活動之權益。
‧ 本券可與熟客卡合併使用
",
                Stores = new List<OptionStoreModel>(new[]
                    {
                        new OptionStoreModel
                        {
                            Guid = Guid.Parse("df64374e-0437-4041-84d2-37e023b4897d"),
                            StoreName = "《中山店》",
                            Phone = "0912345678",
                            Address = "台北市中山區中山北路一段11號",
                            OpenTime = "09:00~21:00",
                            CloseDate = "",
                            Remarks = "",
                            Mrt = "台北捷運",
                            Car = "台北小黃",
                            Bus = "台北轉運站",
                            OtherVehicles = "",
                            WebUrl = "",
                            FbUrl = "",
                            PlurkUrl = "",
                            BlogUrl = "",
                            OtherUrl = "",
                            CreditcardAvailable = false,
                            Coordinates =
                                new ApiCoordinates (0.0, 0.0)
                                ,
                            Selected = true,
                            Enabled = true,
                        }
                    }),
                IsDraft = true
            };
        }
    }
}
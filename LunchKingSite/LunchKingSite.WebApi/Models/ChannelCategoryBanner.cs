﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class ChannelCategoryBanner
    {
        public ClassfyBanner classfyBanner1 { get; set; }
        public ClassfyBanner classfyBanner2 { set; get; }
        public List<ChannelCategorys> category { set; get; }
        public string link { set; get; }
    }

    public class ClassfyBanner
    {
        /// <summary>
        /// ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 圖片html
        /// </summary>
        public string imageHtml { set; get; }
    }

    public class ChannelCategorys
    {
        /// <summary>
        /// 分類ID
        /// </summary>
        public int id { get; set; }
        /// <summary>
        /// 分類名稱
        /// </summary>
        public string name { set; get; }
        /// <summary>
        /// 分類圖片路徑
        /// </summary>
        public string imagePath { set; get; }
        /// <summary>
        /// link
        /// </summary>
        public string link { set; get; }
    }


    public class PponChannelList
    {
        public List<PponChannel> categoryItem { get; set; }
        public List<PponChannel> linkItem { get; set; }
    }

    public class PponChannel
    {
        /// <summary>
        /// 分類ID
        /// </summary>
        public int channelId { get; set; }
        /// <summary>
        /// 分類名稱
        /// </summary>
        public string channelName { set; get; }
    }
}

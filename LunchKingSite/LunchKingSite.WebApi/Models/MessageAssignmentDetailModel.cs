﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageAssignmentDetailModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int AssignmentId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int AssignmentType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int Status { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ExecutionTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int MessageType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Subject { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Message { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int SendCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public List<AssignmentFilterModel> FilterData { set; get; }
    }
}

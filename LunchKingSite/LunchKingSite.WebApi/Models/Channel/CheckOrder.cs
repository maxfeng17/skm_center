﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model.Refund;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Views;

namespace LunchKingSite.WebApi.Models.Channel
{
    #region DeliveryRefund
    /// <summary>
    /// 宅配退貨
    /// </summary>
    public class DeliveryRefund
    {
        /// <summary>
        /// 廠商Order Id
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 退貨資料
        /// </summary>
        public List<RefundDetail> RefundData { get; set; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string Reason { get; set; }
        /// <summary>
        /// 是否收到商品
        /// </summary>
        public bool? IsReceive { get; set; }
        /// <summary>
        /// 收件人姓名
        /// </summary>
        public string ReceiverName { get; set; }
        /// <summary>
        /// 收件人地址
        /// </summary>
        public string ReceiverAddress { get; set; }
        /// <summary>
        /// 是否讀取到參數
        /// </summary>
        public bool IsLoaded { get; set; }
        /// <summary>
        /// 預設值
        /// </summary>
        public DeliveryRefund()
        {
            IsLoaded = false;
        }
    }

    /// <summary>
    /// 宅配退貨結果
    /// </summary>
    public class DeliveryRefundQuery
    {
        /// <summary>
        /// 廠商ReturnForm Id
        /// </summary>
        public int ReturnFormId { get; set; }
    }

    /// <summary>
    /// 退貨資料
    /// </summary>
    public class RefundDetail
    {
        /// <summary>
        /// ProductIds
        /// </summary>
        public string ProductIds { get; set; }
        /// <summary>
        /// 退貨數量
        /// </summary>
        public int RefundCount { get; set; }
    }

    /// <summary>
    /// 宅配退貨狀態查詢
    /// </summary>
    public class CheckDeliveryRefundStatus
    {
        /// <summary>
        /// 廠商Order Id
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 是否讀取到參數
        /// </summary>
        public bool IsLoaded { get; set; }
        /// <summary>
        /// 預設值
        /// </summary>
        public CheckDeliveryRefundStatus()
        {
            IsLoaded = false;
        }
    }

    /// <summary>
    /// 依退貨單查詢
    /// </summary>
    public class CheckRefundFormId
    {
        /// <summary>
        /// 退貨單號
        /// </summary>
        public int RefundFormId { get; set; }
        /// <summary>
        /// 是否讀取到參數
        /// </summary>
        public bool IsLoaded { get; set; }
        /// <summary>
        /// 預設值
        /// </summary>
        public CheckRefundFormId()
        {
            IsLoaded = false;
        }
    }

    /// <summary>
    /// 退貨單查詢
    /// </summary>
    public class DeliveryRefundStatusOutput
    {
        /// <summary>
        /// 代銷方Orderid
        /// </summary>
        public string RelatedOrderId { set; get; }
        /// <summary>
        /// 退貨資料
        /// </summary>
        public RefundStatusOrder RefundData { set; get; }
    }

    /// <summary>
    /// 退貨狀態查詢 OrderGuid
    /// </summary>
    public class RefundStatusOrder
    {
        /// <summary>
        /// 17Life 訂單編號
        /// </summary>
        public Guid OrderGuid { set; get; }
        /// <summary>
        /// 退貨單記錄
        /// </summary>
        public List<CheckDeliveryRefundStatusQuery> RefundLog { set; get; }
        /// <summary>
        /// 查詢備註
        /// </summary>
        public string Remark { set; get; }
    }

    /// <summary>
    /// 宅配退貨狀態查詢結果
    /// </summary>
    public class CheckDeliveryRefundStatusQuery
    {
        /// <summary>
        /// 退貨單ID
        /// </summary>
        public int ReturnFormId { set; get; }
        /// <summary>
        /// 申請時間
        /// </summary>
        public string RequestTime { set; get; }
        /// <summary>
        /// 品項
        /// </summary>
        public string ItemName { set; get; }
        /// <summary>
        /// 退貨原因
        /// </summary>
        public string Reason { set; get; }
        /// <summary>
        /// 處理時間
        /// </summary>
        public string ProcessTime { set; get; }
        /// <summary>
        /// 退貨進度
        /// </summary>
        public string Process { set; get; }
        /// <summary>
        /// 結案後退貨項目
        /// </summary>
        public string RefundedItem { set; get; }
        /// <summary>
        /// 退貨狀態
        /// </summary>
        public ChannelProgressStatus ProgressStatus { set; get; }

        private string GetRefundDescription(ReturnFormEntity entity)
        {
            string refundDescription;
            switch (entity.RefundType)
            {
                case RefundType.Scash:
                    refundDescription = "退回原款項";//"退購物金";
                    break;
                case RefundType.Cash:
                case RefundType.ScashToCash:
                    refundDescription = "刷退";
                    break;
                case RefundType.Atm:
                case RefundType.ScashToAtm:
                    refundDescription = "退ATM";
                    break;
                case RefundType.Tcash:
                case RefundType.ScashToTcash:
                    refundDescription = string.Format("退{0}", Helper.GetEnumDescription(entity.ThirdPartyPaymentSystem));
                    break;
                default:
                    refundDescription = "資料有問題, 請聯絡技術部";
                    break;
            }
            return refundDescription;
        }

        /// <summary>
        /// new object
        /// </summary>
        /// <param name="entity"></param>
        public CheckDeliveryRefundStatusQuery(ReturnFormEntity entity)
        {
            ReturnFormId = entity.Id;
            RequestTime = entity.CreateTime.ToString("yyyy/MM/dd hh:mm:ss");
            ItemName = ((ReturnFormEntity) (entity)).GetReturnSpec();
            Reason = entity.ReturnReason;
            ProcessTime = entity.LastProcessingTime.ToString("yyyy/MM/dd hh:mm:ss");
            Process =
                string.Format("[{0}]{1}{2}", GetRefundDescription(entity), "，", OrderFacade.GetRefundStatus(entity));
            RefundedItem = ((ReturnFormEntity) (entity)).GetRefundedSpec();
            ProgressStatus = OrderFacade.ChannelProgressStatus(entity);
        }
    }

    /// <summary>
    /// 宅配退貨前查詢
    /// </summary>
    public class DeliveryRefundOrder
    {
        /// <summary>
        /// 廠商Order Id
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 是否讀取到參數
        /// </summary>
        public bool IsLoaded { get; set; }
        /// <summary>
        /// 預設值
        /// </summary>
        public DeliveryRefundOrder()
        {
            IsLoaded = false;
        }
    }

    /// <summary>
    /// 宅配退貨前查詢結果
    /// </summary>
    public class DeliveryRefundOrderQuery
    {
        /// <summary>
        /// 是否可退貨
        /// </summary>
        public bool IsReturnable { get; set; }
        /// <summary>
        /// 是否可退貨原因
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 退貨資訊
        /// </summary>
        public IEnumerable<SummarizedProductSpec> RefundInfo { get; set; }
    }
    
    #endregion

    #region CheckDeliveryStatus

    /// <summary>
    /// 宅配訂單查尋
    /// </summary>
    public class CheckDelivery
    {
        /// <summary>
        /// 廠商Order Id
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 是否讀取到參數
        /// </summary>
        public bool IsLoaded { get; set; }

        /// <summary>
        /// 預設值
        /// </summary>
        public CheckDelivery()
        {
            IsLoaded = false;
        }
    }

    /// <summary>
    /// 宅配查尋結果
    /// </summary>
    public class CheckDeliveryQuery
    {
        /// <summary>
        /// 廠商Order Id
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 宅配資料
        /// </summary>
        public List<DeliveryDetail> DeliveryData { get; set; }
        /// <summary>
        /// 分潤代碼
        /// </summary>
        public string RevenueCode { get; set; }
    }

    /// <summary>
    /// 宅配資訊
    /// </summary>
    public class DeliveryDetail
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="order"></param>
        public DeliveryDetail(ApiUserOrder order)
        {
            OrderGuid = order.OrderGuid;
            DealGuid = order.BusinessHourGuid;
            EventName = order.CouponName;
            DealImage = order.MainImagePath;
            DeliveryDateStart = ApiSystemManager.DateTimeFromDateTimeString(order.UseStartDate);
            DeliveryDateEnd = ApiSystemManager.DateTimeFromDateTimeString(order.UseEndDate);
            BuyDate = ApiSystemManager.DateTimeFromDateTimeString(order.BuyDate);
            OrderDetail = order.OrderDetailList.Select(x => new DeliveryOrderDetail(x)).ToList();
            Freight = order.Freight;
            TotalAmount = order.TotalAmount;
            AddressName = order.AddresseeName;
            AddressPhone = order.AddresseePhone;
            AddressAddr = order.AddresseeAddress;
            OrderDesc = order.OrderTypeDesc;
            OrderStatus = order.OrderStatus;
            RefundLog = order.RefundLogList.Select(x => new RefundLog(x)).ToList();
            IsShipped = false;
            ShippedDate = null;
            ShippedCreateTime = null;
            var orderShipCol = OrderFacade.GetOrderShipByOrderGuid(order.OrderGuid);
            if (orderShipCol.Any())
            {
                var ship = orderShipCol.OrderBy(x => x.CreateTime).FirstOrDefault();
                if (ship != null && ship.IsLoaded && ship.ShipTime != null)
                {
                    IsShipped = true;
                    ShippedDate = Convert.ToDateTime(ship.ShipTime.Value.GetDateTimeFormats('s')[0].ToString());
                    ShippedCreateTime = Convert.ToDateTime(ship.CreateTime.GetDateTimeFormats('s')[0].ToString());
                }
            }
            ShipInfos = order.ShipInfoList.Select(y => new ShipInfos(y)).ToList();
        }

        /// <summary>
        /// 17Life Order Guid
        /// </summary>
        public Guid OrderGuid { set; get; }
        /// <summary>
        /// BusinessHourGuid
        /// </summary>
        public Guid DealGuid { set; get; }
        /// <summary>
        /// 檔次名稱
        /// </summary>
        public string EventName { set; get; }
        /// <summary>
        /// 檔次圖片
        /// </summary>
        public string DealImage { set; get; }
        /// <summary>
        /// 配送時間啟
        /// </summary>
        public DateTime DeliveryDateStart { set; get; }
        /// <summary>
        /// 配送時間迄
        /// </summary>
        public DateTime DeliveryDateEnd { set; get; }
        /// <summary>
        /// 購買日期
        /// </summary>
        public DateTime BuyDate { set; get; }
        /// <summary>
        /// 訂購明細
        /// </summary>
        public List<DeliveryOrderDetail> OrderDetail { set; get; }
        /// <summary>
        /// 運費
        /// </summary>
        public decimal Freight { set; get; }
        /// <summary>
        /// 總金額
        /// </summary>
        public decimal TotalAmount { set; get; }
        /// <summary>
        /// 退貨Log
        /// </summary>
        public List<RefundLog> RefundLog { set; get; }
        /// <summary>
        /// 收件人
        /// </summary>
        public string AddressName { set; get; }
        /// <summary>
        /// 收件人電話
        /// </summary>
        public string AddressPhone { set; get; }
        /// <summary>
        /// 收件地址
        /// </summary>
        public string AddressAddr { set; get; }
        /// <summary>
        /// 訂單狀態說明
        /// </summary>
        public string OrderDesc { set; get; }
        /// <summary>
        /// 訂單狀態
        /// </summary>
        public ApiOrderStatus OrderStatus { set; get; }
        /// <summary>
        /// 是否已出貨
        /// </summary>
        public bool IsShipped { get; set; }
        /// <summary>
        /// 出貨日期
        /// </summary>
        public DateTime? ShippedDate { get; set; }
        /// <summary>
        /// 廠商壓出貨當下日期
        /// </summary>
        public DateTime? ShippedCreateTime { get; set; }
        /// <summary>
        /// 出貨資訊
        /// </summary>
        public List<ShipInfos> ShipInfos { set; get; }
    }

    /// <summary>
    /// 訂購項目
    /// </summary>
    public class DeliveryOrderDetail
    {
        /// <summary>
        /// 品項
        /// </summary>
        public string ItemName { set; get; }
        /// <summary>
        /// 單價
        /// </summary>
        public decimal Price { set; get; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { set; get; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="detail"></param>
        public DeliveryOrderDetail(ApiUserOrderDetail detail)
        {
            ItemName = detail.ItemName;
            Price = detail.Price;
            Quantity = detail.Quantity;
        }
    }

    /// <summary>
    /// 退貨Log
    /// </summary>
    public class RefundLog
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="log"></param>
        public RefundLog(ApiRefundLog log)
        {
            RequestTime = DateTime.Parse(log.RequestTime);
            RequestSpec = log.RequestSpec;
            ReturnedSpec = log.ReturnedSpec;
            Message = log.Message;
        }

        /// <summary>
        /// 申請退貨時間
        /// </summary>
        public DateTime RequestTime { get; set; }
        /// <summary>
        /// 申請退貨數
        /// </summary>
        public string RequestSpec { get; set; }
        /// <summary>
        /// 實際退貨數
        /// </summary>
        public string ReturnedSpec { get; set; }
        /// <summary>
        /// 退貨進度
        /// </summary>
        public string Message { get; set; }
    }

    /// <summary>
    /// 訂貨資訊
    /// </summary>
    public class ShipInfos
    {
        public ShipInfos(BizLogic.Models.ShipInfo info)
        {
            ShipCompany = info.ShipCompanyName;
            ShipNo = info.ShipNo;
        }
        /// <summary>
        /// 物流商
        /// </summary>
        public string ShipCompany { set; get; }
        /// <summary>
        /// 物流單號
        /// </summary>
        public string ShipNo { set; get; }
    }

    #endregion CheckDeliveryStatus
}

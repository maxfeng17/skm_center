﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// CouponStatusMessage的暫存容器
    /// </summary>
    public class CouponStatusMessage
    {
        /// <summary>
        /// Code
        /// </summary>
        public PaymentUtilityRefundType Code { get; set; }
        /// <summary>
        /// Message
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// RefundStatus
        /// </summary>
        public ProgressStatus RefundStatus { get; set; }
        /// <summary>
        /// ReturnModifyTime
        /// </summary>
        public DateTime? ReturnModifyTime { get; set; }
    }
    
    /// <summary>
    /// 依時間區間搜尋該客戶的Coupon
    /// </summary>
    public class CouponStatusBySearchTimeObject
    {
        /// <summary>
        /// 開始時間區間
        /// </summary>
        public DateTime StartTime { get; set; }
        /// <summary>
        /// 結束時間區間
        /// </summary>
        public DateTime EndTime { get; set; }
    }

    /// <summary>
    /// 回傳Coupon的相關資訊
    /// </summary>
    public class CouponStatusBySearchTimeReturnObject
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// sequene_number
        /// </summary>
        public string SequenceNumber { get; set; }

        /// <summary>
        /// Code(PaymentUtilityRefundType)
        /// </summary>
        public PaymentUtilityRefundType Code { get; set; }

        /// <summary>
        /// 修改時間(ModifyTime)
        /// </summary>
        public DateTime? Data { get; set; }

        /// <summary>
        /// 核銷狀態
        /// </summary>
        public string Message { get; set; }
    }
}
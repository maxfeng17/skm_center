﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.DataOrm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.Channel
{
    public class CategoryModel 
    {
        public CategoryModel() { }
        public CategoryModel(ChannelPponCity cpc) 
        {
            ChannelId = cpc.PponCity.ChannelId ?? 0;
            CategoryId = cpc.PponCity.ChannelId ?? 0;
            CategoryName = cpc.PponCity.ChannelId == PponCityGroup.DefaultPponCityGroup.TaipeiCity.ChannelId ? "美食" : cpc.PponCity.CityName;
            CategoryType = (int)cpc.PponChannel;
            Sequence = cpc.Sequence;
        }

        public CategoryModel(ViewCategoryDependency x) 
        {
            ChannelId = x.ParentId;
            CategoryId = x.CategoryId;
            CategoryName = x.CategoryName;
            CategoryType = x.CategoryType ?? 0;
            Sequence = x.Seq;
        }

        public int ChannelId { get; set; }
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryType { get; set; }
        public int Sequence { get; set; }
    }
}

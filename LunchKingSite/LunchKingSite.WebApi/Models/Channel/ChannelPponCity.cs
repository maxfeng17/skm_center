﻿using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models.Channel
{
    public class ChannelPponCity
    {
        public ChannelPponCity(BizLogic.Component.PponCity pponCity, CategoryType ct, int sequence)
        {
            PponCity = pponCity;
            PponChannel = ct;
            Sequence = sequence;
        }
        public BizLogic.Component.PponCity PponCity { get; set; }
        public CategoryType PponChannel { get; set; }
        public int Sequence { get; set; }
    }
}

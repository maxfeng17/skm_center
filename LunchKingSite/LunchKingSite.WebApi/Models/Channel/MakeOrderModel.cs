﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Channel;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// 代銷通路 MakeOrder 
    /// </summary>
    public class ChannelMakeOrderModel
    {
        /// <summary>
        /// Bid
        /// </summary>
        public Guid DealGuid { get; set; }
        /// <summary>
        /// Store Guid -> Seller Guid
        /// </summary>
        public Guid StoreGuid { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 廠商訂單編號
        /// </summary>
        public string RelatedOrderId { get; set; }
        /// <summary>
        /// 簡訊發送號碼
        /// </summary>
        public string SmsMobile { get; set; }
        /// <summary>
        /// 是否發送簡訊
        /// </summary>
        public bool IsSendSms { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ChannelMakeOrderModel()
        {
            IsSendSms = true;
            IsConfirmAmount = false;
            IsDelivery = false;
        }
        /// <summary>
        /// 是否再確認金額
        /// </summary>
        public bool IsConfirmAmount { get; set; }
        /// <summary>
        /// 廠商輸入金額
        /// </summary>
        public int Amount { get; set; }
        /// <summary>
        /// 是否宅配
        /// </summary>
        public bool IsDelivery { get; set; }
        /// <summary>
        /// 宅配商品選項
        /// </summary>
        public List<ItemOptionsModel> ItemOption { get; set; }
        /// <summary>
        /// 宅配寄送資料
        /// </summary>
        public ChannelDeliveryInfo DeliveryInfo { get; set; }
        /// <summary>
        /// 建立零級會員對應的Token
        /// </summary>
        public string MemberToken { get; set; }
        /// <summary>
        /// 取得商品規格字串
        /// </summary>
        /// <returns></returns>
        public string GetItemOptionString()
        {
            List<string> allItemSpec = new List<string>();
            foreach (var item in ItemOption)
            {
                List<string> spec = item.ItemSpec.Select(cat => string.Format("{0}|^|{1}", cat.OptionName, cat.OptionGuid)).ToList();
                allItemSpec.Add(string.Format("{0}|#|{1}", string.Join(",", spec), item.Qty));
            }

            return allItemSpec.Any() ? string.Join("||", allItemSpec) : string.Format("|#|{0}", Quantity);
        }

        /// <summary>
        /// 取得17Life's PponDeliveryInfo
        /// </summary>
        /// <returns></returns>
        public PponDeliveryInfo GetPponDeliveryInfo()
        {
            return new PponDeliveryInfo
            {
                BuyerUserId = 0,
                WriteAddress = DeliveryInfo.WriteAddress,
                WriteZipCode = DeliveryInfo.WriteZipCode,
                Freight = DeliveryInfo.ShippingFee,
                WriteName = DeliveryInfo.WriteName,
                Phone = DeliveryInfo.Phone,
                PayType = LunchKingSite.Core.PaymentType.LCash,
                Quantity = Quantity,
                
                #region Hard Code property

                BonusPoints = 0,
                PayEasyCash = 0,
                SCash = 0,

                Notes = string.Empty,
                SellerType = LunchKingSite.Core.DepartmentTypes.PponItem,

                IsForFriend = false,
                FriendName = string.Empty,
                FriendEMail = string.Empty,
                ToFriendNote = string.Empty,
                DeliveryDateString = string.Empty,
                DeliveryTimeString = string.Empty,
                Genus = string.Empty,
                MemberDeliveryID = 0,
                WriteBuildingGuid = Guid.Empty,
                WriteShortAddress = string.Empty,
                DeliveryType =  LunchKingSite.Core.DeliveryType.ToHouse,
                ReceiptsType = LunchKingSite.Core.DonationReceiptsType.None,
                CarrierType = LunchKingSite.Core.CarrierType.None,
                CarrierId = string.Empty,
                LoveCode = string.Empty,
                Version = LunchKingSite.Core.InvoiceVersion.CarrierEra,
                InvoiceType = string.Empty,
                IsInvoiceSave = false,
                InvoiceSaveId = string.Empty,
                CompanyTitle = string.Empty,
                UnifiedSerialNumber = string.Empty,
                IsEinvoice = false,
                StoreGuid = Guid.Empty,
                EntrustSell = LunchKingSite.Core.EntrustSellType.No,
                DiscountCode = string.Empty,
                CreditCardInstallment= LunchKingSite.Core.OrderInstallment.No

                #endregion
            };
        }
    }
}

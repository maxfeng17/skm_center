﻿using LunchKingSite.BizLogic.Facade;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.Channel
{
    public class GCTrustInfoInput
    {
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
    }

    public class GroupCouponInfoResult 
    {
        [JsonProperty("orderGuid")]
        public Guid OrderGuid { get; set; }
        [JsonProperty("trustInfoData")]
        public List<TrustInfo> TrustInfoData { get; set; }
        [JsonProperty("note")]
        public string Note { get { return "信託碼不等於憑證，此處為本張訂單進入信託行的資料"; } }
    }
    public class TrustInfo 
    {
        public TrustInfo(CashTrustLog c, CashTrustLog r)
        {
            CouponId = c.CouponId ?? 0;
            TrustNumber = c.TrustSequenceNumber;
            CostAmount = c.Amount - c.DiscountAmount;
            Status = CouponFacade.GetCouponStatus(c, r);
            UsageVerifiedTime = c.UsageVerifiedTime == null ? string.Empty : ((DateTime)c.UsageVerifiedTime).ToString("yyyy/MM/dd HH:mm:ss");
            ReturnedTime = c.ReturnedTime == null ? string.Empty : ((DateTime)c.ReturnedTime).ToString("yyyy/MM/dd HH:mm:ss");
        }

        [JsonProperty("couponId")]
        public int CouponId { get; set; }
        [JsonProperty("trustNumber")]
        public string TrustNumber { get; set; }
        [JsonProperty("costAmount")]
        public int CostAmount { get; set; }
        [JsonProperty("status")]
        public string Status { get; set; }
        [JsonProperty("usageVerifiedTime")]
        public string UsageVerifiedTime { get; set; }
        [JsonProperty("returnedTime")]
        public string ReturnedTime { get; set; }
    }
}

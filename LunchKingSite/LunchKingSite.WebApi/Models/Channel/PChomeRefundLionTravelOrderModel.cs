﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Channel;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// PChome通路 Return 
    /// </summary>
    public class PChomeRefundLionTravelOrderModel
    {
        /// <summary>
        /// 廠商訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 退券數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 要退的票券唯一值
        /// </summary>
        public List<int> VoucherUIds { get; set; }
        /// <summary>
        /// 資料壓碼(驗證用)
        /// </summary>
        public string MacCode { get; set; }
    }
}

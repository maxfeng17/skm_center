﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// 代銷通路, 宅配 checkout data
    /// </summary>
    public class DeliveryCheckout
    {
        /// <summary>
        /// 建構
        /// </summary>
        /// <param name="checkout"></param>
        /// <param name="maxItemCount">最大購買數</param>
        public DeliveryCheckout(ApiPponDealCheckout checkout, int maxItemCount)
        {
            DealGuid = checkout.Bid;
            EventName = checkout.DealName;
            Price = checkout.Price;
            NotDeliveryIslands = checkout.NotDeliveryIslands;
            FreightList = checkout.FreightList;
            OptionCategories = checkout.OptionCategories;
            ShoppingCart = checkout.ShoppingCart;
            ComboPackCount = checkout.ComboPackCount;
            MaxOrderQuantity = ComboPackCount * maxItemCount;
            CheckOptionQuantity = OptionCategories.Count() == 1;
        }

        #region 檔次相關資料
        /// <summary>
        /// 檔次Guid
        /// </summary>
        public Guid DealGuid { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string EventName { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 是否不配送外島 true則不配送，false則為有配送
        /// </summary>
        public bool NotDeliveryIslands { get; set; }
        /// <summary>
        /// 階梯運費的資料
        /// </summary>
        public List<ApiFreight> FreightList { get; set; }
        /// <summary>
        /// 是否檢查選項數量
        /// </summary>
        public bool CheckOptionQuantity { get; set; }
        /// <summary>
        /// 商品選項群組
        /// </summary>
        public List<ApiPponOptionCategory> OptionCategories { get; set; }
        /// <summary>
        /// 是否為購物車類型的檔次
        /// </summary>
        public bool ShoppingCart { get; set; }
        /// <summary>
        /// 成套商品每套數量(目前為購物車使用)
        /// </summary>
        public int ComboPackCount { get; set; }
        /// <summary>
        /// 單次購買最大數量
        /// </summary>
        public int MaxOrderQuantity { get; set; }

        #endregion 檔次相關資料
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Channel;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// PChome通路 MultiCouponStatus 
    /// </summary>
    public class PChomeMultiCouponStatusModel
    {
        /// <summary>
        /// 廠商訂單編號
        /// </summary>
        public string OrderNo { get; set; }
        /// <summary>
        /// 要查的票券唯一值
        /// </summary>
        public List<int> VoucherUIds { get; set; }
    }
}

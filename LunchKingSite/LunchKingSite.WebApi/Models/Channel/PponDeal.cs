﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Models;
using LunchKingSite.DataOrm;
using LunchKingSite.BizLogic.Models.API;
using LunchKingSite.BizLogic.Models;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// 代銷檔次資料
    /// </summary>
    public class ChannelPponDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public ChannelPponDeal() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="channelIdList"></param>
        /// <param name="channelClientProperty"></param>
        /// <param name="siteUrl"></param>
        /// <param name="isRemoveStyle"></param>
        public ChannelPponDeal(AgentChannel channel, IViewPponDeal deal, List<int> channelIdList, ChannelClientProperty channelClientProperty, string siteUrl, bool isRemoveStyle)
        {
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            List<int> categoryList = deal.CategoryIds;
            categoryList = categoryList.Except(channelIdList).ToList();

            DealGuid = deal.BusinessHourGuid;
            ChannelId = ViewPponDealManager.DefaultManager.ViewPponDealGetChannelIdListByGuid(deal.BusinessHourGuid);
            if (ChannelId == null)
            {
                //隱藏檔的暫存抓不到,另外抓取
                ChannelId = new List<int>();
                List<int> cityList = deal.CityIds;
                foreach (int cityid in deal.CityIds)
                {
                    PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                    if (city.ChannelId != null && !ChannelId.Contains((int)city.ChannelId))
                    {
                        ChannelId.Add((int)city.ChannelId);
                    }   
                }

            }
            EventName = string.Empty; //deal.EventName; 2017/9/1 移除黑標
            EventTitle = deal.EventTitle;
            ItemName = deal.ItemName;
            DeliverTimeE = deal.ChangedExpireDate ?? deal.BusinessHourDeliverTimeE;
            DeliverTimeS = deal.BusinessHourDeliverTimeS;
            OrderTimeE = deal.BusinessHourOrderTimeE;
            OrderTimeS = deal.BusinessHourOrderTimeS;
            DeliveryType = deal.DeliveryType ?? 1;
            OriginalPrice = deal.ItemOrigPrice;
            Price = deal.ItemPrice;
            Percent =
                ViewPponDealManager.GetDealDiscountString(
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                    deal.ItemPrice, deal.ItemOrigPrice);
            TotalQuantity = PponFacade.GetNotMultiDealStock(deal);
            OrderedQuantity = deal.OrderedQuantity;
            MaxQuantity = deal.MaxItemCount;
            ComboDeals = (GetNormalComboDeal(deal.ComboDelas) ?? new List<ViewComboDeal>()).Select(y => new ChannelComboDeal
            {
                EventTitle = y.Title,
                DealGuid = y.BusinessHourGuid,
                OriginalPrice = y.ItemOrigPrice,
                Price = y.ItemPrice,
                TotalQuantity = y.OrderTotalLimit,
                OrderedQuantity = y.OrderedQuantity,
                PiinLifeMenu = ChannelFacade.GetPiinLifeMenu(contentLite.Remark),
                SaleMultipleBase = y.SaleMultipleBase
            }).ToList();
            IsDailyLimited = deal.IsDailyRestriction;
            Restrictions = contentLite.Restrictions;
            RefundRules = new ChannelRefundRules(deal);
            string dealDesc = contentLite.Description;
            Description = isRemoveStyle ? Helper.RemoveHtmlTagButImg(dealDesc) : dealDesc;
            EgDescription = ChannelFacade.GetEGDescription(contentLite.Remark).Replace("17Life APP", "APP");
            PiinLifeDescription = ChannelFacade.GetPiinLifeDescription(contentLite.Remark);
            CategoryList = categoryList;
            LabelTagList = ChannelFacade.GetDealLabelTagList(deal.LabelTagList, deal.CustomTag);
            IconTagList = ChannelFacade.GetDealIconList(deal);
            string[] imagesArray = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto);
            DealImage = imagesArray.FirstOrDefault(x => x.Contains(deal.BusinessHourGuid.ToString().Replace("-", "EDM")));
            DealImageWithoutMark = 
                (string.IsNullOrEmpty(deal.AppDealPic))
                    ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath)
                    : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic);
            DealImageList = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(x=>!x.IsEmpty()).ToList();
            Stores =
                ProviderFactory.Instance()
                    .GetSerializer()
                    .Deserialize<AvailableInformation[]>(contentLite.Availability)
                    .Select(y => new ChannelStore
                    {
                        Name = y.N,
                        Phone = y.P,
                        Info = y.OT,
                        Address = y.A,
                        Latitude = y.Latitude,
                        Longitude = y.Longitude,
                        Mrt = y.MR
                    }).ToList();
            IsLowGorssMargin =
                (channelClientProperty.MinGrossMargin.HasValue &&
                 channelClientProperty.GeneralGrossMargin.HasValue &&
                 Math.Round	(ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid).MinGrossMargin,4) < channelClientProperty.GeneralGrossMargin &&
                 Math.Round(ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid).MinGrossMargin,4) > channelClientProperty.MinGrossMargin);
            if (channel == AgentChannel.EzTravel && IsLowGorssMargin && ChannelFacade.IsChannleForcePass(channel, deal))
            {
                //易遊網會檢查該欄位，但會有白名單商品過去
                IsLowGorssMargin = false;
            }
            IsNeedBooking = ChannelFacade.IsNeedBooking(deal);
            IsChosen = deal.IsChosen.GetValueOrDefault();
            IsGroupCoupon = Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.GroupCoupon);
            CouponType = (int)ChannelFacade.GetChannelCouponType(deal);
            CouponTypeDesc = Helper.GetEnumDescription((ChannelCouponType)CouponType);
            SaleMultipleBase = deal.SaleMultipleBase ?? 0;
        }

        private static IList<ViewComboDeal> GetNormalComboDeal(IList<ViewComboDeal> vcds)
        {
            List<ViewComboDeal> list = new List<ViewComboDeal>();

            if (vcds == null) return list;

            foreach (ViewComboDeal vcd in vcds)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid);
                if (vpd.IsExpiredDeal.GetValueOrDefault(false))
                {
                    continue;
                }
                list.Add(vcd);
            }
            return list;
        }

        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid DealGuid { set; get; }
        /// <summary>
        /// 頻道
        /// </summary>
        public List<int> ChannelId { set; get; }
        /// <summary>
        /// 優惠活動標題
        /// </summary>
        public string EventName { set; get; }
        /// <summary>
        /// 行銷標題
        /// </summary>
        public string EventTitle { set; get; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { set; get; }
        /// <summary>
        /// 搶購結束時間
        /// </summary>
        public DateTime? DeliverTimeE { set; get; }
        /// <summary>
        /// 搶購開始時間
        /// </summary>
        public DateTime? DeliverTimeS { set; get; }
        /// <summary>
        /// 使用結束時間
        /// </summary>
        public DateTime OrderTimeE { set; get; }
        /// <summary>
        /// 使用開始時間
        /// </summary>
        public DateTime OrderTimeS { set; get; }
        /// <summary>
        /// 使用方式
        /// </summary>
        public int DeliveryType { set; get; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriginalPrice { set; get; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { set; get; }
        /// <summary>
        /// 折數
        /// </summary>
        public string Percent { set; get; }
        /// <summary>
        /// 最大購買數量
        /// </summary>
        public decimal? TotalQuantity { set; get; }
        /// <summary>
        /// 已訂購數量
        /// </summary>
        public int? OrderedQuantity { set; get; }
        /// <summary>
        /// 每人限購數量
        /// </summary>
        public int? MaxQuantity { set; get; }
        /// <summary>
        /// 多檔次商品
        /// </summary>
        public List<ChannelComboDeal> ComboDeals { set; get; }
        /// <summary>
        /// 是否為每日限購
        /// </summary>
        public bool? IsDailyLimited { set; get; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { set; get; }
        /// <summary>
        /// 退貨規則
        /// </summary>
        public ChannelRefundRules RefundRules { set; get; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { set; get; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string EgDescription { set; get; }
        /// <summary>
        /// 品生活檔次詳細介紹
        /// </summary>
        public List<PiinLifeDescription> PiinLifeDescription { set; get; }
        /// <summary>
        /// 檔次分類
        /// </summary>
        public List<int> CategoryList { set; get; }
        /// <summary>
        /// 檔次標籤
        /// </summary>
        public List<string> LabelTagList { set; get; }
        /// <summary>
        /// 權益說明標籤
        /// </summary>
        public List<string> IconTagList { set; get; }
        /// <summary>
        /// 檔次主要圖片網址
        /// </summary>
        public string DealImage { set; get; }
        /// <summary>
        /// 檔次無浮水印
        /// </summary>
        public string DealImageWithoutMark { set; get; }
        /// <summary>
        /// 檔次圖檔
        /// </summary>
        public List<string> DealImageList { set; get; }
        /// <summary>
        /// 分店資訊介紹
        /// </summary>
        public List<ChannelStore> Stores { set; get; }
        /// <summary>
        /// 是否低毛利檔次
        /// </summary>
        public bool IsLowGorssMargin { set; get; }
        /// <summary>
        /// 是否需要預約
        /// </summary>
        public bool IsNeedBooking { set; get; }
        /// <summary>
        /// 是否為特盤商品
        /// </summary>
        public bool IsChosen { get; set; }
        /// <summary>
        /// 是否是成套票券
        /// </summary>
        public bool IsGroupCoupon { get; set; }
        /// <summary>
        /// 成套票券類型
        /// </summary>
        public int CouponType { get; set; }
        public string CouponTypeDesc { get; set; }
        public int SaleMultipleBase { get; set; }
    }


    /// <summary>
    /// 代銷宅配檔次資料
    /// </summary>
    public class ChannelDeliveryPponDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public ChannelDeliveryPponDeal() { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deal"></param>
        /// <param name="channelIdList"></param>
        /// <param name="channelClientProperty"></param>
        /// <param name="siteUrl"></param>
        public ChannelDeliveryPponDeal(IViewPponDeal deal, List<int> channelIdList, ChannelClientProperty channelClientProperty, List<DealTypeNode> nodes, string siteUrl)
        {
            var contentLite = ViewPponDealManager.DefaultManager.GetPponContentLite(deal);
            List<int> categoryList = deal.CategoryIds;
            categoryList = categoryList.Except(channelIdList).ToList();
            DealTypeNode node = DealTypeFacade.Find(nodes, deal.DealTypeDetail == null ? (int)deal.DealType : (int)deal.DealTypeDetail);

            DealGuid = deal.BusinessHourGuid;
            ChannelId =ViewPponDealManager.DefaultManager.ViewPponDealGetChannelIdListByGuid(deal.BusinessHourGuid);
            if (ChannelId == null)
            {
                //隱藏檔的暫存抓不到,另外抓取
                ChannelId = new List<int>();
                List<int> cityList = deal.CityIds;
                foreach (int cityid in deal.CityIds)
                {
                    PponCity city = PponCityGroup.DefaultPponCityGroup.GetPponCityByCityId(cityid);
                    if (city.ChannelId != null && !ChannelId.Contains((int)city.ChannelId))
                    {
                        ChannelId.Add((int)city.ChannelId);
                    }
                }
                
            }
            EventName = string.Empty; //deal.EventName; 2017/9/1 移除黑標
            EventTitle = deal.EventTitle;
            ItemName = deal.ItemName;
            DeliverTimeE = deal.ChangedExpireDate ?? deal.BusinessHourDeliverTimeE;
            DeliverTimeS = deal.BusinessHourDeliverTimeS;
            OrderTimeE = deal.BusinessHourOrderTimeE;
            OrderTimeS = deal.BusinessHourOrderTimeS;
            DeliveryType = deal.DeliveryType ?? 1;
            OriginalPrice = deal.ItemOrigPrice;
            Price = deal.ItemPrice;
            Percent =
                ViewPponDealManager.GetDealDiscountString(
                    Helper.IsFlagSet(deal.BusinessHourStatus, BusinessHourStatus.NoDiscountShown),
                    deal.ItemPrice, deal.ItemOrigPrice);
            TotalQuantity = PponFacade.GetNotMultiDealStock(deal);
            OrderedQuantity = deal.OrderedQuantity;
            MaxQuantity = deal.MaxItemCount;
            ComboDeals = (GetNormalComboDeal(deal.ComboDelas) ?? new List<ViewComboDeal>()).Select(y => new ChannelComboDeal
            {
                EventTitle = y.Title,
                DealGuid = y.BusinessHourGuid,
                OriginalPrice = y.ItemOrigPrice,
                Price = y.ItemPrice,
                TotalQuantity = y.OrderTotalLimit,
                OrderedQuantity = y.OrderedQuantity,
                PiinLifeMenu = ChannelFacade.GetPiinLifeMenu(contentLite.Remark)
            }).ToList();
            IsDailyLimited = deal.IsDailyRestriction;
            Restrictions = contentLite.Restrictions;
            RefundRules = new ChannelRefundRules(deal);
            string dealDesc = contentLite.Description;
            Description = dealDesc;
            EgDescription = ChannelFacade.GetEGDescription(contentLite.Remark);
            ProductSpecDescription = contentLite.ProductSpec;
            CategoryList = categoryList;
            LabelTagList = ChannelFacade.GetDealLabelTagList(deal.LabelTagList, deal.CustomTag);
            IconTagList = ChannelFacade.GetDealIconList(deal);
            string[] imagesArray = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where((x, i) => i == 0).ToArray();
            DealImage = imagesArray.FirstOrDefault();
            DealImageWithoutMark =
                (string.IsNullOrEmpty(deal.AppDealPic))
                    ? PponFacade.GetPponDealFirstImagePath(deal.EventImagePath)
                    : PponFacade.GetPponDealFirstImagePath(deal.AppDealPic);
            DealRemoveBgImage = !string.IsNullOrEmpty(deal.RemoveBgPic) ? PponFacade.GetPponDealFirstImagePath(deal.RemoveBgPic) : "";
            DealImageList = ImageFacade.GetMediaPathsFromRawData(deal.EventImagePath, MediaType.PponDealPhoto).Where(x => !x.IsEmpty()).ToList();
            IsLowGorssMargin =
                (channelClientProperty.MinGrossMargin.HasValue &&
                 channelClientProperty.GeneralGrossMargin.HasValue &&
                 Math.Round(ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid)
                     .MinGrossMargin, 4) <
                 channelClientProperty.GeneralGrossMargin &&
                 Math.Round(ViewPponDealManager.DefaultManager.BaseCostGrossMarginGet(deal.BusinessHourGuid)
                     .MinGrossMargin, 4) >
                 channelClientProperty.MinGrossMargin);
            IsWms = deal.IsWms;
            DealTypes = DealTypeFacade.FindAllNodeCodeId(node, deal.BusinessHourGuid);
        }

        private static IList<ViewComboDeal> GetNormalComboDeal(IList<ViewComboDeal> vcds)
        {
            List<ViewComboDeal> list = new List<ViewComboDeal>();

            if (vcds == null) return list;

            foreach (ViewComboDeal vcd in vcds)
            {
                IViewPponDeal vpd = ViewPponDealManager.DefaultManager.ViewPponDealGetByBid(vcd.BusinessHourGuid);
                if (vpd.IsExpiredDeal.GetValueOrDefault(false) || vpd.ComboPackCount > 1)
                {
                    continue;
                }
                list.Add(vcd);
            }
            return list;
        }

        /// <summary>
        /// 檔次BID
        /// </summary>
        public Guid DealGuid { set; get; }
        /// <summary>
        /// 頻道
        /// </summary>
        public List<int> ChannelId { set; get; }
        /// <summary>
        /// 優惠活動標題
        /// </summary>
        public string EventName { set; get; }
        /// <summary>
        /// 行銷標題
        /// </summary>
        public string EventTitle { set; get; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public string ItemName { set; get; }
        /// <summary>
        /// 搶購結束時間
        /// </summary>
        public DateTime? DeliverTimeE { set; get; }
        /// <summary>
        /// 搶購開始時間
        /// </summary>
        public DateTime? DeliverTimeS { set; get; }
        /// <summary>
        /// 使用結束時間
        /// </summary>
        public DateTime OrderTimeE { set; get; }
        /// <summary>
        /// 使用開始時間
        /// </summary>
        public DateTime OrderTimeS { set; get; }
        /// <summary>
        /// 使用方式
        /// </summary>
        public int DeliveryType { set; get; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriginalPrice { set; get; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { set; get; }
        /// <summary>
        /// 折數
        /// </summary>
        public string Percent { set; get; }
        /// <summary>
        /// 最大購買數量
        /// </summary>
        public decimal? TotalQuantity { set; get; }
        /// <summary>
        /// 已訂購數量
        /// </summary>
        public int? OrderedQuantity { set; get; }
        /// <summary>
        /// 每人限購數量
        /// </summary>
        public int? MaxQuantity { set; get; }
        /// <summary>
        /// 多檔次商品
        /// </summary>
        public List<ChannelComboDeal> ComboDeals { set; get; }
        /// <summary>
        /// 是否為每日限購
        /// </summary>
        public bool? IsDailyLimited { set; get; }
        /// <summary>
        /// 權益說明
        /// </summary>
        public string Restrictions { set; get; }
        /// <summary>
        /// 退貨規則
        /// </summary>
        public ChannelRefundRules RefundRules { set; get; }
        /// <summary>
        /// 詳細介紹
        /// </summary>
        public string Description { set; get; }
        /// <summary>
        /// 一姬說好康
        /// </summary>
        public string EgDescription { set; get; }
        /// <summary>
        /// 商品規格
        /// </summary>
        public string ProductSpecDescription { set; get; }
        /// <summary>
        /// 檔次分類
        /// </summary>
        public List<int> CategoryList { set; get; }
        /// <summary>
        /// 檔次標籤
        /// </summary>
        public List<string> LabelTagList { set; get; }
        /// <summary>
        /// 權益說明標籤
        /// </summary>
        public List<string> IconTagList { set; get; }
        /// <summary>
        /// 檔次主要圖片網址
        /// </summary>
        public string DealImage { set; get; }
        /// <summary>
        /// 檔次無浮水印
        /// </summary>
        public string DealImageWithoutMark { set; get; }
        /// <summary>
        /// 去背方圖
        /// </summary>
        public string DealRemoveBgImage { set; get; }
        /// <summary>
        /// 檔次圖檔
        /// </summary>
        public List<string> DealImageList { set; get; }
        /// <summary>
        /// 是否低毛利檔次
        /// </summary>
        public bool IsLowGorssMargin { set; get; }
        /// <summary>
        /// 是否24到貨
        /// </summary>
        public bool IsWms { set; get; }
        /// <summary>
        /// 所有分類(業績歸屬)
        /// </summary>
        public List<int> DealTypes { set; get; }
    }

    /// <summary>
    /// 多檔次子檔
    /// </summary>
    public class ChannelComboDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public string EventTitle { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public Guid DealGuid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal OriginalPrice { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Price { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal? TotalQuantity { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderedQuantity { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string PiinLifeMenu { set; get; }
        public int SaleMultipleBase { get; set; }
    }

    /// <summary>
    /// 退貨規則
    /// </summary>
    public class ChannelRefundRules
    {
        /// <summary>
        /// 
        /// </summary>
        public bool NoRefund { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool DaysNoRefund { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool NoRefundBeforeDays { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool ExpireNoRefund { set; get; }

        public ChannelRefundRules(IViewPponDeal deal)
        {
            this.NoRefund = deal.GroupOrderStatus != null && (deal.GroupOrderStatus.HasValue ? (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.NoRefund) > 0 : false);
            this.DaysNoRefund = deal.GroupOrderStatus != null && (deal.GroupOrderStatus.HasValue ? (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.DaysNoRefund) > 0 : false);
            this.NoRefundBeforeDays =
                         deal.GroupOrderStatus != null && (deal.GroupOrderStatus.HasValue ? (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.NoRefundBeforeDays) > 0 : false);
            this.ExpireNoRefund = deal.GroupOrderStatus != null && (deal.GroupOrderStatus.HasValue ? (deal.GroupOrderStatus.Value & (int)GroupOrderStatus.ExpireNoRefund) > 0 : false);
        }
    }

    /// <summary>
    /// 分店資料
    /// </summary>
    public class ChannelStore
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Phone { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Latitude { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Longitude { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Mrt { set; get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealStores
    {
        /// <summary>
        /// 
        /// </summary>
        public DealStores()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="storeGuid"></param>
        /// <param name="totalQuantity"></param>
        /// <param name="orderedQuantity"></param>
        public DealStores(string name, Guid storeGuid, int totalQuantity, int orderedQuantity, int vpdSaleMultipleBase)
        {
            Name = name;
            StoreGuid = storeGuid;
            CityName = string.Empty;
            CountyName = string.Empty;
            Address = string.Empty;
            TotalQuantity = totalQuantity;
            OrderedQuantity = orderedQuantity;
            SaleMultipleBase = vpdSaleMultipleBase;
        }

        ///  <summary>
        ///  
        ///  </summary>
        ///  <param name="vps"></param>
        /// <param name="dealOrderTotalLimit"></param>
        public DealStores(ViewPponStore vps, int dealOrderTotalLimit, int vpdSaleMultipleBase)
        {
            Name = vps.StoreName;
            StoreGuid = vps.StoreGuid;
            CityName = vps.CityName;
            CountyName = vps.TownshipName;
            Address = vps.AddressString;
            TotalQuantity = vps.TotalQuantity ?? dealOrderTotalLimit;
            OrderedQuantity = vps.OrderedQuantity ?? 0;
            SaleMultipleBase = vpdSaleMultipleBase;
        }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string Name { set; get; }
        /// <summary>
        /// seller guid
        /// </summary>
        public Guid StoreGuid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CityName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CountyName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalQuantity { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderedQuantity { set; get; }
        public int SaleMultipleBase { get; set; }
    }
}

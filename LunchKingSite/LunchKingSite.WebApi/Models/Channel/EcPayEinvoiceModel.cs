﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.Channel
{
    public class EcPayEinvoice
    {
        public string Pid { get; set; }
        public string OrderId { get; set; }
        public string Amount { get; set; }
        public string InvoiceStatus { get; set; }
        public string NoTax { get; set; }
        public string ItemName { get; set; }
        public string ComId { get; set; }
        public string ComName { get; set; }
        public string BuyerName { get; set; }
        public string BuyerAddress { get; set; }
        public string BuyerEmail { get; set; }
        public string CarrierId { get; set; }
        public string CarrierType { get; set; }
        public string LoveCode { get; set; }
        public List<EcPayCouponInfo> CouponInfo { get; set; }
        public string InvoiceNumber { get; set; }
    }

    public class EcPayCouponInfo
    {
        public string SerNo { get; set; }
        public string Amount { get; set; }
        public string CouponId { get; set; }
    }

    public class EcPayWinnerEinvoice
    {
        public string Pid { get; set; }
        public string DateCode { get; set; }
    }
}

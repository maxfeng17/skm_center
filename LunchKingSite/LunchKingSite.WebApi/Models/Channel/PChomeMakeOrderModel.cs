﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Channel;

namespace LunchKingSite.WebApi.Models.Channel
{
    /// <summary>
    /// PChome通路 MakeOrder 
    /// </summary>
    public class PChomeMakeOrderModel
    {
        /// <summary>
        /// 廠商票券商品料號(uniqueid)
        /// </summary>
        public string ProductNo { get; set; }
        /// <summary>
        /// 數量
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// 資料壓碼(驗證用)
        /// </summary>
        public string MacCode { get; set; }
    }
}

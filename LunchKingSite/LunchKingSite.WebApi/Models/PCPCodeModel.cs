﻿using LunchKingSite.Core.Enumeration;
using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PCPCodeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public long IdentityCodeId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public double PaymentPercent { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public List<StoreDiscountCodeModel> DiscountCodes { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public double SuperBonus { set; get; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PCPImageUpload
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImageData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ImageSequence { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PcpImageType Type { get; set; }
        /// <summary>
        /// 是否將新上傳的圖片插入到 seq 的位置，然後原本的 seq 和之後的順序就往後排
        /// 如果false ，則蓋掉該seq 位置的圖片
        /// </summary>
        public bool IsInsert { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PCPImageInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ImageId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PcpImageType ImgType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PCPImageSeq
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupId { get; set; }
        /// <summary>
        /// ["id1", "id2", "id3", "id4", "id5"] // 圖片順序
        /// </summary>
        public List<int> ImgSeq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PcpImageType ImgType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PCPImageDelete
    {
        /// <summary>
        /// 
        /// </summary>
        public int GroupId { get; set; }
        /// <summary>
        /// ["id1", "id2", "id3", "id4", "id5"]
        /// </summary>
        public List<int> ImageId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PcpImageType ImgType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserId { get; set; }
    }

}

﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountTemplatesInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? BeginTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (Status < 0 || Status > 2)
                {
                    return false;
                }
                if (BeginTime > EndTime)
                {
                    return false;
                }
                return true;
            }
        }
    }

}

﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CheckoutModel
    {

        /// <summary>
        /// 使用者會員卡Id user_membership_card.id
        /// </summary>
        public int CardId { set; get; }
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int Level { set; get; }
        /// <summary>
        /// 累計消費次數
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 累計消費金額
        /// </summary>
        public int AmountTotal { set; get; }
        /// <summary>
        /// 最近一次的消費時間
        /// </summary>
        public DateTime LastUseTime { set; get; }
        /// <summary>
        /// 會員姓
        /// </summary>
        public string LastName { set; get; }
        /// <summary>
        /// 會員名
        /// </summary>
        public string FirstName { set; get; }
        /// <summary>
        /// 回傳完整的姓名
        /// </summary>
        public string Name { get { return LastName + FirstName; } }
        /// <summary>
        /// 會員電話
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 會員性別
        /// </summary>
        public int Gender { set; get; }
        /// <summary>
        /// 會員生日
        /// </summary>
        public DateTime? Birthday { set; get; }
        /// <summary>
        /// 備註
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 本次消費是否卡片升等
        /// </summary>
        public bool IsLevelUp { set; get; }
        /// <summary>
        /// 升等訊息
        /// </summary>
        public string LevelUpMsg { set; get; }
        /// <summary>
        /// 結帳完成後推薦發送熟客券或公關券給消費者
        /// </summary>
        public CheckoutDiscountTemplateModel[] DiscountTemplates { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpUserServiceInfo
    {
        /// <summary>
        /// 會員優惠是否有開啟使用權限
        /// </summary>
        public bool IsVipOn { get; set; }
        /// <summary>
        /// 集點功能是否有開啟使用權限
        /// </summary>
        public bool IsPointOn { set; get; }
        /// <summary>
        /// 寄杯功能是否有開啟使用權限
        /// </summary>
        public bool IsDepositOn { set; get; }
        /// <summary>
        /// 會員優惠有無設定
        /// </summary
        public bool IsVipSet { get; set; }
        /// <summary>
        /// 點數功能有無設定
        /// </summary>
        public bool IsPointSet { set; get; }
        /// <summary>
        /// 寄杯功能有無設定
        /// </summary>
        public bool IsDepositSet { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsHistoryOn { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int UserPoint { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int UserDeposit { set; get; }
    }
}

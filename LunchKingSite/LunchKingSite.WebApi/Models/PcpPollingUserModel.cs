﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 手機端會每隔1秒，輪詢問(Polling) 目前使用者的狀態
    /// </summary>
    public class PollingUserStatusModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public PollingUserData PollingUserData { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TransMessage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TransTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int GroupId { get; set; }
        /// <summary>
        /// 使用者點資訊
        /// </summary>
        public string UserServiceInfo { set; get; }
    
    }

    /// <summary>
    /// 
    /// </summary>
    public class PollingUserData
    {
        /// <summary>
        /// Identity  ID
        /// </summary>
        public int PollingId { get; set; }
        /// <summary>
        /// 過期時間
        /// </summary>
        public string ExpiredTime { get; set; }
        /// <summary>
        /// 動作狀態
        /// </summary>
        public int Action { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserCardId { get; set; }
    }
}

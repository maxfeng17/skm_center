﻿using System;
namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointLogsInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int PointType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime BeginTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndTime { set; get; }
    }
}

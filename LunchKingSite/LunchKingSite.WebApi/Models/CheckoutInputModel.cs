﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CheckoutInputModel
    {
        /// <summary>
        /// Identity_code.id
        /// </summary>
        public int IdentityCodeId { set; get; }
        /// <summary>
        /// 消費金額
        /// </summary>
        public double? Amount { set; get; }
        /// <summary>
        /// 折抵金額
        /// </summary>
        public double CardDiscount { set; get; }
        /// <summary>
        /// 使用的抵用券(熟客券/公關券) id
        /// </summary>
        public int? DiscountCodeId { set; get; }
        /// <summary>
        /// 超級紅利金使用點數
        /// </summary>
        public double SuperBonus { set; get; }
        /// <summary>
        /// 分店 guid
        /// </summary>
        public Guid StoreGuid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (IdentityCodeId < 1 || SuperBonus < 0 || CardDiscount < 0)
                {
                    return false;
                }

                if (Amount != null && Amount < 0)
                {
                    return false;
                }

                if (DiscountCodeId != null && DiscountCodeId < 1)
                {
                    return false;
                }

                return StoreGuid != Guid.Empty;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// GetDealTypeInfo input
    /// </summary>
    public class GetDealTypeInfoModel
    {
        /// <summary>
        /// Channel Id
        /// </summary>
        [JsonProperty("ChannelId")]
        public int ChannelId { get; set; }

        /// <summary>
        /// Query Date
        /// </summary>
        [JsonProperty("QueryDate")]
        public string QueryDate { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                DateTime checkDate;
                if (!DateTime.TryParse(QueryDate, out checkDate))
                {
                    return false;
                }

                if (new TimeSpan(checkDate.Ticks - DateTime.Now.Ticks).TotalDays < 3 && new TimeSpan(checkDate.Ticks - DateTime.Now.Ticks).TotalDays > -1)
                {
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// check DealTypeId exists 
        /// </summary>
        /// <returns></returns>
        public bool IsDealTypeExists
        {
            get
            {
                return Enum.IsDefined(typeof(ShoppingGuideDealType), ChannelId);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public DateTime GetQueryDate()
        {
            return DateTime.Parse(QueryDate);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetDealDataModel
    {
        /// <summary>
        /// QueryToken
        /// </summary>
        [JsonProperty("QueryToken")]
        public string QueryToken { get; set; }
        /// <summary>
        /// page
        /// </summary>
        [JsonProperty("Page")]
        public int Page { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool CheckPage
        {
            get { return Page > 0; }
        }
    }

    /// <summary>
    /// GetDealTypeInfo output
    /// </summary>
    public class DealTypeInfo
    {
        /// <summary>
        /// 查詢Token
        /// </summary>
        [JsonProperty("QueryToken")]
        public string QueryToken { get; set; }
        /// <summary>
        /// 檔次類型Id
        /// </summary>
        [JsonProperty("ChannelId")]
        public int ChannelId { get; set; }
        /// <summary>
        /// 檔次類型說明
        /// </summary>
        [JsonProperty("ChannelName")]
        public string ChannelName { get; set; }
        /// <summary>
        /// 檔次數量
        /// </summary>
        [JsonProperty("Qty")]
        public int Qty { get; set; }
        /// <summary>
        /// 檔次分頁
        /// </summary>
        [JsonProperty("Pages")]
        public int Pages { get; set; }
    }
}

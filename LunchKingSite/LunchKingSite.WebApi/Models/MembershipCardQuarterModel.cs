﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MembershipCardQuarterModel
    {
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardQuarterModel()
        {
            IsLoad = false;
        }
        /// <summary>
        /// membership_card_group.id
        /// </summary>
        public int CardGroupId { set; get; }
        /// <summary>
        /// 版本ID membership_card_group_version.id
        /// </summary>
        public int Version { set; get; }
        /// <summary>
        /// 卡片狀態 membership_card.status enum MembershipCardStatus
        /// </summary>
        public int Status { set; get; }
        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { set; get; }
        /// <summary>
        /// 熟客卡形象圖片
        /// </summary>
        public string SellerImagePath { get; set; }
        /// <summary>
        /// 熟客卡卡片圖片
        /// </summary>
        public string SellerFullCardPath { get; set; }
        /// <summary>
        /// 熟客卡卡片圖片
        /// </summary>
        public string SellerFullCardThumbnailPath { get; set; }
        /// <summary>
        /// 卡片種類
        /// </summary>
        public int SellerCardType { get; set; }
        /// <summary>
        /// 卡片的有效區間(含)
        /// </summary>
        public DateTime VersionOpenTime { set; get; }
        /// <summary>
        /// 卡片的有效區間(含)
        /// </summary>
        public DateTime VersionCloseTime { set; get; }
        /// <summary>
        /// 熟客卡主圖
        /// </summary>
        public string CardImage { set; get; }
        /// <summary>
        /// 熟客卡背景色編號
        /// </summary>
        public int BackgroundColorId { set; get; }
        /// <summary>
        /// 熟客卡背景圖片
        /// </summary>
        public int BackgroundImageId { set; get; }
        /// <summary>
        /// 卡片左上 icon
        /// </summary>
        public int IconImageId { set; get; }
        /// <summary>
        /// 此版本包含的熟客卡
        /// </summary>
        public List<MembershipCardModel> Cards { set; get; }
        /// <summary>
        /// 熟客卡可適用店家數
        /// </summary>
        public int SelectedStoreCount { set; get; }
        /// <summary>
        /// 熟客卡可適用店家資料
        /// </summary>
        public List<OptionStoreModel> Stores { set; get; }
        /// <summary>
        /// 是否有查詢到此筆資料，如果並沒有對應的membership_gruop或membership_group_version則回傳false
        /// </summary>
        public bool IsLoad { get; set; }
        /// <summary>
        /// true表示是否可與店家其他優惠合併使用，預設值為false
        /// </summary>
        public bool CombineUse { get; set; }
        /// <summary>
        /// 卡片適用日期類型
        /// </summary>
        public int AvailableDateType { set; get; }
        /// <summary>
        /// 適用日期中文描述
        /// </summary>
        public string AvailableDateDesc
        {
            get
            {
                return Helper.GetEnumDescription(((AvailableDateType)AvailableDateType));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CardValidDate { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string[] SellerIntro { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerServiceImgPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerServiceImgThumbnailPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerSrndgImgPath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ImageDetail[] SellerSrndgThumbnailPath { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ImageDetail
    {
        /// <summary>
        /// 
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="c"></param>
        /// <returns></returns>
        public static explicit operator ImageDetail(ImageDetails c)
        {
            ImageDetail result = new ImageDetail();
            result.Seq = c.Seq;
            result.ImagePath = c.ImagePath;
            return result;
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public class ImageDetails
    {
        /// <summary>
        /// 
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImagePath { get; set; }
    }

}

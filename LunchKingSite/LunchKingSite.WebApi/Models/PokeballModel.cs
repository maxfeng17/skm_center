﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models
{
    #region ShowEventPromo
    /// <summary>
    /// 依EventPromoId顯示活動頁
    /// </summary>
    public class ShowEventPromo
    {
        /// <summary>
        /// ShowEventPromo
        /// </summary>
        public ShowEventPromo()
        {
            PokeballList = new List<object>();
        }        

        /// <summary>
        /// BannerImage
        /// </summary>
        public string BannerImage { get; set; }
        /// <summary>
        /// PokeballList
        /// </summary>
        public List<object> PokeballList { get; set; }
    }
    /// <summary>
    /// ShowEventPromoPokeball
    /// </summary>
    public class ShowEventPromoPokeball
    {
        /// <summary>
        /// App-觸發事件Pokeball
        /// </summary>
        public ShowEventPromoPokeball()
        {
            Type = "ShowEventPromo";
        }

        /// <summary>
        /// 事件觸發行為
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// EventPromoId
        /// </summary>
        public int EventPromoId { get; set; }
    }
    #endregion
}

﻿using System;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MembershipCardModel
    {
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="card"></param>
        public MembershipCardModel(MembershipCard card)
        {
            CardId = card.Id;
            Level = card.Level;
            PaymentPercent = card.PaymentPercent;
            OtherPremiums = card.Others;
            OrderNeeded = card.OrderNeeded;
            AmountNeeded = (double?) card.AmountNeeded;
            ConditionalLogic = card.ConditionalLogic;
            OpenTime = card.OpenTime;
            CloseTime = card.CloseTime;
            Status = (MembershipCardStatus)card.Status;
            ModifyTime = card.ModifyTime.HasValue ? card.ModifyTime.Value : card.CreateTime;
            Instruction = string.IsNullOrWhiteSpace(card.Instruction) ? string.Empty : card.Instruction;
        }
        /// <summary>
        /// membership_card.id
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int Level { set; get; }
        /// <summary>
        /// 折數
        /// </summary>
        public double PaymentPercent { set; get; }
        /// <summary>
        /// 會員卡加贈
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 升等需消費次數
        /// </summary>
        public int? OrderNeeded { set; get; }
        /// <summary>
        /// 升等需銷費金額
        /// </summary>
        public double? AmountNeeded { set; get; }
        /// <summary>
        /// 升等條件邏輯 0:次數與金額擇一達成, 1:金額次數都需滿足條件
        /// </summary>
        public int ConditionalLogic { set; get; }
        /// <summary>
        /// 卡片上架日期
        /// </summary>
        public DateTime OpenTime { set; get; }
        /// <summary>
        /// 卡片下架日期
        /// </summary>
        public DateTime CloseTime { set; get; }
        /// <summary>
        /// 會員卡狀態;int;0：草稿 1：上架 2：作廢
        /// </summary>
        public MembershipCardStatus Status { get; set; }
        /// <summary>
        /// 最後異動時間
        /// </summary>
        public DateTime ModifyTime { get; set; }
        /// <summary>
        /// 限制條件
        /// </summary>
        public string Instruction { set; get; }

    }
}

﻿using System.Collections.Generic;
using LunchKingSite.BizLogic.Models.MGM;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

//using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models.MGM
{
    /// <summary>
    /// 
    /// </summary>
    public class GiftCardModel
    {
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("message")]
        public string Message { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [Required]
        [JsonProperty("senderId")]
        public int SenderId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("senderName")]
        public string SenderName { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("cardStyleId")]
        public int CardStyleId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [Required]
        [JsonProperty("orderId")]
        public string OrderId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [Required]
        [JsonProperty("matchMemberGroup")]
        public List<MatchMemberGroup> MatchMemberGroup { get; set; }

    
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.PCPVerifyModels
{
    /// <summary>
    /// 憑證掃碼確認 Input
    /// </summary>
    public class VerifyCheckModel
    {
        /// <summary>
        /// couponSerial
        /// </summary>
        public string CouponSerial { set; get; }
        /// <summary>
        /// CouponCode
        /// </summary>
        public string CouponCode { set; get; }
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// CouponType
        /// </summary>
        public int CouponType { set; get; }
    }
    /// <summary>
    /// 憑證針對 trustId 核銷
    /// </summary>
    public class VerifyCouponModel
    {
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// TrustId
        /// </summary>
        public Guid TrustId { set; get; }
    }
}

﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointLogOverview
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RegularsPointExpireTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RegularsPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RegularsLockPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FavorPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int FavorLockPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? FavorPointExpireTime { get; set; }
    }
}

﻿using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class AssignmentFilterModel
    {
        /// <summary>
        /// 
        /// </summary>
        public AssignmentFilterType FilterType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public Operators Operator { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string ParameterFirst { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string ParameterSecond { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(ParameterFirst))
                {
                    return false;
                }
                if (Operator == Operators.BetweenAnd && string.IsNullOrEmpty(ParameterSecond))
                {
                    return false;
                }
                return true;
            }
        }
    }
}

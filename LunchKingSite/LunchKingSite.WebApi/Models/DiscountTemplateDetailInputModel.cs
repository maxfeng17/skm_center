﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountTemplateDetailInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (TemplateId <= 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
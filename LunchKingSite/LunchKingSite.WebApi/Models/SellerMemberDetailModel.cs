﻿using System;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SellerMemberDetailModel
    {
        /// <summary>
        /// 
        /// </summary>
        public SellerMemberDetailModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        public SellerMemberDetailModel(ViewVbsSellerMember mem)
        {
            UserId = mem.UserId;
            SellerMemberId = mem.SellerMemberId;
            CardId = mem.UserMembershipCardId;
            Level = mem.Level;
            PaymentPercent = (float?)mem.PaymentPercent;
            Instruction = string.IsNullOrWhiteSpace(mem.Instruction) ? string.Empty : mem.Instruction;
            OtherPremiums = string.IsNullOrWhiteSpace(mem.OtherPremiums) ? string.Empty : mem.OtherPremiums;
            AvailableDateType = (AvailableDateType)mem.AvailableDateType;
            OrderCount = mem.OrderCount;
            AmountTotal = mem.AmountTotal;
            LastUseTime = mem.LastUseTime;
            FirstName = string.IsNullOrWhiteSpace(mem.SellerMemberFirstName) ? string.Empty : mem.SellerMemberFirstName;
            LastName = string.IsNullOrWhiteSpace(mem.SellerMemberLastName) ? string.Empty : mem.SellerMemberLastName;
            Mobile = mem.SellerMemberMobile;
            Gender = mem.SellerMemberGender.HasValue?(GenderType)mem.SellerMemberGender.Value:GenderType.Unknown;
            Birthday = mem.SellerMemberBirthday;
            Remarks = mem.Remarks;
            RequestTime = mem.RequestTime;
            CardNo = string.IsNullOrWhiteSpace(mem.CardNo) ? string.Empty : mem.CardNo;

            if (string.IsNullOrEmpty(FirstName) && string.IsNullOrEmpty(LastName))
            {
                FirstName = mem.UserEmail;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="mem"></param>
        /// <param name="modify"></param>
        /// <param name="totalCount"></param>
        public SellerMemberDetailModel(ViewVbsSellerMember mem,bool modify,int totalCount):this(mem)
        {
            IsModify = modify;
            SellerMemberCount = totalCount;
        }
        /// <summary>
        /// 
        /// </summary>
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public float? PaymentPercent { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Instruction { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public AvailableDateType AvailableDateType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string AvailableDateDesc {
            get
            {
                return Helper.GetEnumDescription(AvailableDateType);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal AmountTotal { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? LastUseTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { set; get; } 
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? Birthday { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? RequestTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CardNo { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsModify { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SellerMemberCount { get; set; }
    }
}

﻿using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class StoreModel
    {
        /// <summary>
        /// 
        /// </summary>
        public StoreModel()
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="store"></param>
        public StoreModel(Seller store)
        {
            Guid = store.Guid;
            StoreName = store.SellerName;
            Phone = store.StoreTel;
            Address = (CityManager.CityTownShopStringGet(store.StoreTownshipId == null ? -1 : store.StoreTownshipId.Value) +
                       store.StoreAddress);
            OpenTime = store.OpenTime;
            CloseDate = store.CloseDate;
            Remarks = store.StoreRemark;
            Mrt = store.Mrt;
            Car = store.Car;
            Bus = store.Bus;
            OtherVehicles = store.OtherVehicles;
            WebUrl = store.WebUrl;
            FbUrl = store.FacebookUrl;
            PlurkUrl = string.Empty;
            BlogUrl = store.BlogUrl;
            OtherUrl = store.OtherUrl;
            CreditcardAvailable = store.CreditcardAvailable;
            Coordinates = ApiCoordinates.FromSqlGeography(
                LocationFacade.GetGeographyByCoordinate(store.Coordinate));
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid Guid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Phone { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Address { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OpenTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CloseDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mrt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Car { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Bus { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherVehicles { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string WebUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FbUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PlurkUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BlogUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string OtherUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CreditcardAvailable { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ApiCoordinates Coordinates { get; set; }
    }
}

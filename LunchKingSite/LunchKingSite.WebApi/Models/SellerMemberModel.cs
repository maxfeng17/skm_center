﻿using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SellerMemberModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberLastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerMemberFirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? CardId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public MembershipCardLevel Level { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int OrderCount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal AmountTotal { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CardNo { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public SellerMemberModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        public SellerMemberModel(ViewVbsSellerMember m)
        {
            UserId = m.UserId;
            SellerMemberId = m.SellerMemberId;
            SellerMemberLastName = string.IsNullOrWhiteSpace(m.SellerMemberLastName) ? string.Empty : m.SellerMemberLastName;
            SellerMemberFirstName = m.UserId == null
                                    ? "XX"
                                    : string.IsNullOrWhiteSpace(m.SellerMemberFirstName) ? string.Empty : m.SellerMemberFirstName;
            CardId = m.UserMembershipCardId;
            Level = (MembershipCardLevel)m.Level;
            OrderCount = m.OrderCount;
            AmountTotal = m.AmountTotal;
            CardNo = m.CardNo;
            Gender = m.SellerMemberGender.HasValue ? (GenderType) m.SellerMemberGender : GenderType.Unknown;
        }
    }
}

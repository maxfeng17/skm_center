﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PCPSetCardStyleModel
    {
        /// <summary>
        /// 會員卡版本編號
        /// </summary>
        public int CardVersionId { get; set; }
        /// <summary>
        /// 背景顏色編號
        /// </summary>
        public int BackgroundColorId { get; set; }
        /// <summary>
        /// 背景圖片編號
        /// </summary>
        public int BackgroundImageId { get; set; }
        /// <summary>
        /// Icon編號
        /// </summary>
        public int IconImageId { get; set; }
    }
}

﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageAssignmentDetailInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int AssignmentId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (AssignmentId < 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}

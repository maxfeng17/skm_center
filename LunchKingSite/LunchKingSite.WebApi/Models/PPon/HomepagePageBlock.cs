﻿using log4net;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Ppon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Caching;
using System.Linq;
using System.Runtime.Caching;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models.PPon
{
    public class HomepagePageBlockBanner
    {
        public string link { get; set; }
        public string pic { get; set; }
        public string title { get; set; }

        private static HomepagePageBlockBanner _empty = new HomepagePageBlockBanner
        {
            link = string.Empty,
            pic = string.Empty,
            title = string.Empty
        };
        public static HomepagePageBlockBanner Empty
        {
            get
            {
                return _empty;
            }
        }
    }

    #region defines 

    public interface IHomepagePageBlockDefine
    {
        int typeId { get; }
    }

    public class HomepagePageBlockBannersDefine : IHomepagePageBlockDefine
    {
        public List<HomepagePageBlockBanner> banners { get; set; }

        public int typeId { get { return 1; } }
    }

    public class HomepagePageBlockNaviIconsDefine : IHomepagePageBlockDefine
    {
        public List<HomepagePageBlockBanner> icons { get; set; }

        public int typeId
        {
            get { return 2; }
        }
    }

    public class HomepagePageBlock24hHotDealsDefine : IHomepagePageBlockDefine
    {

        public int typeId
        {
            get { return 3; }
        }
    }

    public class HomepagePageBlockCurationDefine : IHomepagePageBlockDefine
    {
        public HomepagePageBlockBanner banner { get; set; }
        public List<Guid> deals { get; set; }
        public int typeId
        {
            get { return 4; }
        }
    }

    public class HomepagePageBlockChannelDefine : IHomepagePageBlockDefine
    {
        public HomepagePageBlockBanner banner { get; set; }
        public List<Guid> deals { get; set; }

        public int typeId
        {
            get { return 5; }
        }
    }

    public class HomepagePageBlockYouLikeDealsDefine : IHomepagePageBlockDefine
    {
        public int typeId
        {
            get { return 6; }
        }
    }

    #endregion

    #region contents

    public interface IHomepagePageBlockContent
    {
        string title { get; set; }
        int typeId { get; }
    }

    public abstract class HomepagePageBlockContentBase : IHomepagePageBlockContent
    {
        public string title { get; set; }

        public abstract int typeId { get; }
    }

    public class HomepagePageBlockBannersContent : HomepagePageBlockContentBase
    {
        public HomepagePageBlockBannersContent()
        {
            banners = new List<HomepagePageBlockBanner>();
        }
        public List<HomepagePageBlockBanner> banners { get; set; }

        public override int typeId
        {
            get { return 1; }
        }
    }

    public class HomepagePageBlockNaviIconsContent : HomepagePageBlockContentBase
    {
        public HomepagePageBlockNaviIconsContent()
        {
            icons = new List<HomepagePageBlockBanner>();
        }
        public List<HomepagePageBlockBanner> icons { get; set; }
        public override int typeId
        {
            get { return 2; }
        }
    }

    public class HomepagePageBlock24hHotDealsContent : HomepagePageBlockContentBase
    {
        public HomepagePageBlockBanner banner;
        public List<DealViewLite> deals = new List<DealViewLite>();

        public override int typeId
        {
            get { return 3; }
        }
    }

    public class HomepagePageBlockCurationContent : HomepagePageBlockContentBase
    {
        public HomepagePageBlockBanner banner = HomepagePageBlockBanner.Empty;
        public List<DealViewLite> deals = new List<DealViewLite>();

        public override int typeId
        {
            get { return 4; }
        }
    }

    public class HomepagePageBlockChannelContent : HomepagePageBlockContentBase
    {
        public HomepagePageBlockBanner banner = HomepagePageBlockBanner.Empty;
        public List<DealViewLite> deals = new List<DealViewLite>();

        public override int typeId
        {
            get { return 5; }
        }
    }

    public class HomepagePageBlockYouLikeContent : HomepagePageBlockContentBase
    {
        public List<DealViewLite> deals = new List<DealViewLite>();

        public override int typeId
        {
            get { return 6; }
        }
    }

    #endregion

    #region converters

    public interface IHomepagePageBlockConverter
    {
        IHomepagePageBlockContent Convert(string title, string data);
    }

    public class HomepagePageBlockBannersConverter : IHomepagePageBlockConverter
    {
        public IHomepagePageBlockContent Convert(string title, string data)
        {
            HomepagePageBlockBannersDefine define = JsonConvert.DeserializeObject<HomepagePageBlockBannersDefine>(data);
            HomepagePageBlockBannersContent result = new HomepagePageBlockBannersContent();
            foreach (var banner in define.banners)
            {
                result.banners.Add(banner);
            }
            result.title = title;
            return result;
        }
    }

    public class HomepagePageBlockNaviIconsConverter : IHomepagePageBlockConverter
    {
        public IHomepagePageBlockContent Convert(string title, string data)
        {
            HomepagePageBlockNaviIconsDefine define = JsonConvert.DeserializeObject<HomepagePageBlockNaviIconsDefine>(data);
            HomepagePageBlockNaviIconsContent result = new HomepagePageBlockNaviIconsContent();
            foreach (var icon in define.icons)
            {
                result.icons.Add(icon);
            }
            result.title = title;
            return result;
        }
    }

    public class HomepagePageBlock24hHotDealsConverter : IHomepagePageBlockConverter
    {
        public int? userId { get; set; }
        public HomepagePageBlock24hHotDealsConverter(int? userId)
        {
            this.userId = userId;
        }

        public IHomepagePageBlockContent Convert(string title, string data)
        {
            HomepagePageBlock24hHotDealsDefine define = JsonConvert.DeserializeObject<HomepagePageBlock24hHotDealsDefine>(data);
            HomepagePageBlock24hHotDealsContent result = new HomepagePageBlock24hHotDealsContent();

            List<PponDealSynopsisBasic> dealBasicList = PponFacade2.GetChannelFoodBeautyTravelHotDeals();


            var collectBids = new HashSet<Guid>();
            if(userId != null)
            {
                List<Guid> bids = MemberFacade.GetMemberCollectionDeal(userId.Value);
                collectBids = new HashSet<Guid>(bids);
            }
            
            result.deals.AddRange(dealBasicList.Select(t => PponFacade2.ConvertToChannelDealView(t, collectBids)));

            result.title = title;
            return result;
        }
    }

    public class HomepagePageBlockCurationConverter : IHomepagePageBlockConverter
    {
        public int? userId { get; set; }
        IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();

        public HomepagePageBlockCurationConverter(int? userId)
        {
            this.userId = userId;
        }

        public IHomepagePageBlockContent Convert(string title, string data)
        {
            HomepagePageBlockCurationDefine define = JsonConvert.DeserializeObject<HomepagePageBlockCurationDefine>(data);
            HomepagePageBlockCurationContent result = new HomepagePageBlockCurationContent();
            HashSet<Guid> collectBids;
            if (userId == null)
            {
                collectBids = new HashSet<Guid>();
            }
            else
            {
                DateTime baseDate = PponDealHelper.GetTodayBaseDate();
                collectBids = new HashSet<Guid>(mp.MemberCollectDealGetOnlineDealGuids(
                    userId.GetValueOrDefault(), baseDate.AddDays(-1), baseDate.AddDays(1)));
            }

            define.deals = define.deals ?? new List<Guid>();
            foreach (Guid bid in define.deals)
            {
                if (PponFacade2.CheckOnlineAndAvailable(bid) == false)
                {
                    continue;
                }
                var dealView = PponFacade2.ConvertToChannelDealView(bid, collectBids);
                if (dealView == null || dealView.id == Guid.Empty)
                {
                    continue;
                }
                result.deals.Add(dealView);
            }

            if (define.banner != null)
            {
                result.banner = define.banner;
            }
            result.title = title;
            return result;
        }
    }

    public class HomepagePageBlockChannelConverter : IHomepagePageBlockConverter
    {
        public int? userId { get; set; }
        IMemberProvider mp = ProviderFactory.Instance().GetDefaultProvider<IMemberProvider>();

        public HomepagePageBlockChannelConverter(int? userId)
        {
            this.userId = userId;
        }
        public IHomepagePageBlockContent Convert(string title, string data)
        {
            HomepagePageBlockChannelDefine define = JsonConvert.DeserializeObject<HomepagePageBlockChannelDefine>(data);
            HomepagePageBlockChannelContent result = new HomepagePageBlockChannelContent();

            HashSet<Guid> collectBids;
            if (userId == null)
            {
                collectBids = new HashSet<Guid>();
            }
            else
            {
                DateTime baseDate = PponDealHelper.GetTodayBaseDate();
                collectBids = new HashSet<Guid>(mp.MemberCollectDealGetOnlineDealGuids(
                    userId.GetValueOrDefault(), baseDate.AddDays(-1), baseDate.AddDays(1)));
            }

            define = define ?? new HomepagePageBlockChannelDefine();
            define.deals = define.deals ?? new List<Guid>();
            foreach (Guid bid in define.deals)
            {
                if (PponFacade2.CheckOnlineAndAvailable(bid) == false)
                {
                    continue;
                }
                var dealView = PponFacade2.ConvertToChannelDealView(bid, collectBids);
                if (dealView == null || dealView.id == Guid.Empty)
                {
                    continue;
                }
                result.deals.Add(dealView);
            }

            if (define.banner != null)
            {
                result.banner = define.banner;
            }
            result.title = title;
            return result;
        }
    }

    /// <summary>
    /// 只取前10筆，後面檔次資料透過其它API取得
    /// </summary>
    public class HomepagePageBlockYouLikeConverter : IHomepagePageBlockConverter
    {


        public int? userId { get; set; }
        public int? cityId { get; set; }

        public HomepagePageBlockYouLikeConverter(int? userId, int? cityId)
        {
            this.userId = userId;
            this.cityId = cityId;
        }

        public IHomepagePageBlockContent Convert(string title, string data)
        {
            int totalCount;
            return Convert(title, data, 0, 10, out totalCount);
        }

        public IHomepagePageBlockContent Convert(string title, string data, int startIndex, int pageSize,
            out int totalCount)
        {
            totalCount = 0;
            HomepagePageBlockYouLikeDealsDefine define = JsonConvert.DeserializeObject<HomepagePageBlockYouLikeDealsDefine>(data);
            HomepagePageBlockYouLikeContent result = new HomepagePageBlockYouLikeContent();
            var collectBids = new HashSet<Guid>();
            if (userId != null)
            {
                List<Guid> bids = MemberFacade.GetMemberCollectionDeal(userId.Value);
                collectBids = new HashSet<Guid>(bids);
            }

            string cacheKey = string.Format("UserAppFronterDeals://{0}", userId);
            List<PponDealSynopsisBasic> dealBasicList = MemoryCache.Default.Get(cacheKey) as List<PponDealSynopsisBasic>;
            if (dealBasicList == null)
            {
                dealBasicList = AppFrontDealManager.GetAppFronterDeals(
                    userId.GetValueOrDefault(), cityId.GetValueOrDefault());
                MemoryCache.Default.Set(cacheKey, dealBasicList, new DateTimeOffset(DateTime.Now.AddMinutes(60)));
            }

            totalCount = dealBasicList.Count;
            if (dealBasicList.Count > 10)
            {
                dealBasicList = dealBasicList.Skip(startIndex).Take(pageSize).ToList();
            }

            result.deals.AddRange(dealBasicList.Select(t => PponFacade2.ConvertToChannelDealView(t, collectBids)));

            result.title = title;
            return result;
        }
    }

    #endregion

    public class HomepagePageBlockConverterFactory
    {
        static ILog logger = LogManager.GetLogger(typeof(HomepagePageBlockBannersConverter));

        public static IHomepagePageBlockContent Convert(int typeId, string title, string data,
            int? userId, int? areaId)
        {
            IHomepagePageBlockContent content = null;
            IHomepagePageBlockConverter converter = null;
            try
            {
                if (typeId == 1)
                {
                    converter = new HomepagePageBlockBannersConverter();
                }
                else if (typeId == 2)
                {
                    converter = new HomepagePageBlockNaviIconsConverter();
                }
                else if (typeId == 3)
                {
                    converter = new HomepagePageBlock24hHotDealsConverter(userId);
                }
                else if (typeId == 4)
                {
                    converter = new HomepagePageBlockCurationConverter(userId);
                }
                else if (typeId == 5)
                {
                    converter = new HomepagePageBlockChannelConverter(userId);
                }
                else if (typeId == 6)
                {
                    converter = new HomepagePageBlockYouLikeConverter(userId, areaId);
                }
                content = converter.Convert(title, data);
            }
            catch (Exception ex)
            {
                logger.WarnFormat("HomepagePageBlockConverterFactory convert fail. type={0}, data={1}, ex={2}",
                    typeId, data, ex);
            }

            return content;
        }
    }
}

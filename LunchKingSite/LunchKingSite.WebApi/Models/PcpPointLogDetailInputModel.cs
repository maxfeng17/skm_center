﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointLogDetailInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int LogType { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LogTime { set; get; }
    }
}

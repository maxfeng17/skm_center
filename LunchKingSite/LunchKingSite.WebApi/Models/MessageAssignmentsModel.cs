﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageAssignmentsModel
    {
        /// <summary>
        /// 任務編號pcp_assignment
        /// </summary>
        public int AssignmentId { set; get; }
        /// <summary>
        /// 類型1：簡訊、2：2km推播、3：17life推播
        /// </summary>
        public int AssignmentType { set; get; }
        /// <summary>
        /// 狀態 0：新任務 1：名單確認 2：執行中 3：完成 4：失敗 5：取消
        /// </summary>
        public int Status { set; get; }
        /// <summary>
        /// 主旨描述
        /// </summary>
        public string Subject { set; get; }
        /// <summary>
        /// 建立時間
        /// </summary>
        public DateTime CreateTime { set; get; }
    }
}

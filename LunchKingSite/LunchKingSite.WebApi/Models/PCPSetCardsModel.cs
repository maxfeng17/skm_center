﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PCPSetCardsModel
    {
        /// <summary>
        /// 熟客卡隸屬賣家
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 會員卡群組編號
        /// </summary>
        public int CardGroupId { get; set; }
        /// <summary>
        /// 會員卡版本編號
        /// </summary>
        public int VersionId { get; set; }
        /// <summary>
        /// 會員卡列表
        /// </summary>
        public List<MembershipCardModel> Cards { get; set; }
        /// <summary>
        /// 可用分店GUID列表
        /// </summary>
        public List<Guid> StoreGuids { get; set; }
        /// <summary>
        /// true表示是否可與店家其他優惠合併使用，預設值為false
        /// </summary>
        public bool CombineUse { get; set; }
        /// <summary>
        /// 卡片適用日期類型
        /// </summary>
        public int AvailableDateType { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    #region API Parameter Model class

    /// <summary>
    ///  ActivityListenInspection Needs Parameter
    /// </summary>
    public class ActivityListenInspectionModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
        /// <summary>
        /// 裝置廠商
        /// </summary>
        public string Manufacturer { get; set; }
        /// <summary>
        /// 裝置名稱
        /// </summary>
        public string Model { get; set; }
        /// <summary>
        /// 系統
        /// </summary>
        public string Os { get; set; }
        /// <summary>
        /// 系統版本
        /// </summary>
        public string OsVer { get; set; }
        /// <summary>
        /// app版本
        /// </summary>
        public string PppVer { get; set; }
        /// <summary>
        /// 經度
        /// </summary>
        public double Longitude { get; set; }
        /// <summary>
        /// 緯度
        /// </summary>
        public double Latitude { get; set; }
        /// <summary>
        /// 觸發事件
        /// </summary>
        public string Trigger { get; set; }
    }

    /// <summary>
    /// ActivityTrigger Needs Parameter
    /// </summary>
    public class ActivityTriggerModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
        /// <summary>
        /// Beacon觸發UUID
        /// </summary>
        public string UuId { get; set; }
        /// <summary>
        /// major
        /// </summary>
        public int Major { get; set; }
        /// <summary>
        /// minor
        /// </summary>
        public int Minor { get; set; }
    }
    
    /// <summary>
    /// MessageCollection Needs Parameter
    /// </summary>
    public class MessageCollectionModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
        /// <summary>
        /// 訊息ID
        /// </summary>
        public string MessageId { get; set; }
    }

    /// <summary>
    /// ReadMessage Needs Base Parameter
    /// </summary>
    public class ReadMessageBaseModel
    {
        /// <summary>
        /// 訊息Id
        /// </summary>
        public int MessageId { get; set; }
        /// <summary>
        /// 訊息讀取事件類型
        /// </summary>
        public int TriggerType { get; set; }
    }

    /// <summary>
    /// ReadMessage Needs Parameter
    /// </summary>
    public class ReadMessageModel : ReadMessageBaseModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ReadMessagesModel
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
        public List<ReadMessageBaseModel> ReadMessages { get; set; }
    }

    /// <summary>
    /// RemoveMessage Needs Parameter
    /// </summary>
    public class RemoveMessageModel
    {
        public RemoveMessageModel()
        {
            IsInApp = false;
        }

        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
        /// <summary>
        /// 待移除訊息陣列
        /// </summary>
        public string MessageIds { get; set; }
        /// <summary>
        /// in app message
        /// </summary>
        public bool IsInApp { get; set; }
    }

    /// <summary>
    /// RemoveReadMessage Needs Parameter
    /// </summary>
    public class RemoveReadMessage
    {
        /// <summary>
        /// 裝置識別碼
        /// </summary>
        public string IdentifierCode { get; set; }
    }

    #endregion API Parameter Model class

    #region Beacon & MessageBox Class

    /// <summary>
    /// 事件觸發欄位
    /// </summary>
    public abstract class BaseActivityParameter
    {
        /// <summary>
        /// 事件觸發欄位
        /// </summary>
        public BaseActivityParameter()
        {
            Uuids = new List<string>();
            Bid = string.Empty;
            Url = string.Empty;
            MessageId = string.Empty;
            Message = string.Empty;
            TriggerType = string.Empty;
        }
        /// <summary>
        /// 檔次Bid
        /// </summary>
        public string Bid { get; set; }
        /// <summary>
        /// 事件觸發自訂網址
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 訊息中心-訊息Id
        /// </summary>
        public string MessageId { get; set; }
        /// <summary>
        /// 訊息中心-訊息內容
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 觸發事件
        /// </summary>
        public string TriggerType { get; set; }
        /// <summary>
        /// Beacon觸發監聽Id-UUID
        /// </summary>
        public List<string> Uuids { get; set; }
    }

/// <summary>
/// App-觸發事件Pokeball
/// </summary>
    public class Pokeball : BaseActivityParameter
    {
        /// <summary>
        /// App-觸發事件Pokeball
        /// </summary>
        public Pokeball():base()
        {
            ActionList = new List<Action>();
        }

        /// <summary>
        /// 事件觸發行為
        /// </summary>
        public string Type { get; set; }
        /// <summary>
        /// 後續觸發行為List
        /// </summary>
        public List<Action> ActionList { get; set; }

    }
    /// <summary>
    /// 點擊觸發行為
    /// </summary>
    public class Action : BaseActivityParameter
    {
        /// <summary>
        /// 點擊觸發行為
        /// </summary>
        public Action() :base()
        {
            ActionType = "";
        }
        /// <summary>
        /// 點擊觸發行為類型
        /// </summary>
        public string ActionType { get; set; }
    }

    #endregion Beacon & MessageBox Class
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// GetSkmPayBanner API Response
    /// </summary>
    public class SkmPayBannerRespose
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="banner"></param>
        public SkmPayBannerRespose(SkmPayBanner banner)
        {
            Id = banner.Id;
            Seq = banner.Seq;
            BannerUrl = SkmFacade.SkmAppStyleImgPath(banner.BannerPath);
            Name = banner.Name;
            StartDate = LunchKingSite.BizLogic.Component.API.ApiSystemManager.DateTimeToDateTimeString(banner.StartTime);
            EndDate = LunchKingSite.BizLogic.Component.API.ApiSystemManager.DateTimeToDateTimeString(banner.EndTime);
        }

        /// <summary>
        /// skm_pay_banner.id
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// banner img url
        /// </summary>
        [JsonProperty("bannerUrl")]
        public string BannerUrl { get; set; }
        /// <summary>
        /// sequence
        /// </summary>
        [JsonProperty("seq")]
        public int Seq { get; set; }
        /// <summary>
        /// Banner 名稱
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// Banner Start Date
        /// </summary>
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        /// <summary>
        /// Banner End Date
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }

    public class SkmPayBannerDetailResponse
    {
        /// <summary>
        /// 名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string BannerUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
    }

    public class SkmHomeImage
    {
        /// <summary>
        /// 相對位置描述 leftTop, leftMiddle, leftBottom, rightTop, rightMiddle, rightBottom
        /// </summary>
        public string Position { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImgUrl { get; set; }
    }


    /// <summary>
    /// 取得Skm Pay 預備交易號碼請求資訊
    /// </summary>
    [Serializable]
    public class GetSkmPayPreTransNoRequest
    {
        /// <summary> Business Hour ID </summary>
        [JsonProperty(PropertyName = "bid")]
        public string Bid { get; set; }
        /// <summary> Business Hour ID </summary>
        [JsonProperty(PropertyName = "quantity")]
        public int Quantity { get; set; }
    }


    /// <summary>
    /// 取得Skm Parking 預備交易號碼請求資訊
    /// </summary>
    [Serializable]
    public class GetSkmParkingPreTransNoRequest
    {
        /// <summary>
        /// 裝置類型
        /// Android:1 / IOS: 2
        /// </summary>
        public int DeviceType { get; set; }
        /// <summary> 會員卡號 </summary>
        [Required]
        public string CardNo { get; set; }
        /// <summary> 刷卡金額 </summary>
        public int OrderAmount { get; set; }
        /// <summary> 店櫃業者代號 </summary>
        [Required]
        public string ShopCode { get; set; }
        /// <summary> 車牌號碼</summary>
        public string ParkingToken { get; set; }
        /// <summary> 停車票號 </summary>
        [Required]
        public string ParkingTicketNo { get; set; }
        /// <summary> 車牌照片編號 </summary>
        public string ParkingLicensePictureUUID { get; set; }
        /// <summary> 停車場編號 </summary>
        [Required]
        public string ParkingAreaNo { get; set; }
        /// <summary> 停車折抵分鐘數 </summary>
        public decimal DiscountMinutes { get; set; }

        public bool IsDiscountByPoint { get; set; }

        /// <summary> 停車折抵金額 </summary>
        public int DiscountAmount { get; set; }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Models.Skm;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.DataOrm;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// 
    /// </summary>
    public class SellerDealModel
    {
        private static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        /// <summary>
        /// 
        /// </summary>
        public List<PromoEvent> PromoEventList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<CategoryDealList> sellerCategoryDealList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<LatestActivity> LatestActivityList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string EDMUrl { get; set; }
    }

    /// <summary>
    /// 首頁
    /// </summary>
    public class PromoIndex
    {
        /// <summary>
        /// 
        /// </summary>
        public List<PromoEvent> PromoEventList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<LatestActivity> LatestActivityList { get; set; }
        /// <summary>
        /// 抽獎活動
        /// </summary>
        public PrizeDrawModel PrizeDrawEventList { get; set; }

        /// <summary>
        /// APP EDM url
        /// </summary>
        public string EdmUrl { get; set; }
    }

    /// <summary>
    /// 精選商品
    /// </summary>
    public class SkmChoiceDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public List<CategoryDealList> SellerCategoryDealList { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PromoEvent
    {
        /// <summary>
        /// 
        /// </summary>
        public string BannerImageUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LinkUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MustLogin { get; set; }
        /// <summary>
        /// Start Date
        /// </summary>
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        /// <summary>
        /// End Date
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CategoryDealList
    {
        //[JsonProperty("Id")]
        /// <summary>
        /// 
        /// </summary>
        public int CategoryId { get; set; } //type = 6 and city_id = 450
        /// <summary>
        /// 
        /// </summary>
        public string CategoryName { get; set; }
        //public int CategoryStyle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<DealList> DealList { get; set; }
        //public int DealCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string categoryImageUrl { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class PrizeDrawItemModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Img { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// 活動名稱
        /// </summary>
        public string Subject { get; set; }
    }

    /// <summary>
    /// 抽獎活動
    /// </summary>
    public class PrizeDrawModel
    {
        /// <summary>
        /// 首頁蓋板bn
        /// </summary>
        public PrizeDrawItemModel CoverBannerData { get; set; }

        /// <summary>
        /// 首頁跟屁蟲小icon圖
        /// </summary>
        public List<PrizeDrawItemModel> SmallLogoData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public PrizeDrawModel()
        {
            SmallLogoData = new List<PrizeDrawItemModel>();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class LatestActivity
    {
        /// <summary>
        /// 
        /// </summary>
        public string ActivityDescription { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ActivityTitle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Date { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LinkUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StoreFloor { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ActivityId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MustLogin { get; set; }
        /// <summary>
        /// Start Date
        /// </summary>
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        /// <summary>
        /// End Date
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DealInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DealDescription { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ImageSize { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DealList
    {
        /// <summary>
        /// 
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ImageUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DealDescription { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class SellerInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Coordinate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
    }

    /// <summary>
    /// 首頁與精選列表傳入值
    /// </summary>
    public class SkmIndexModel
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid SellerGuid { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetDealSynopsesListData
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (SellerGuid == Guid.Empty)
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public GetDealSynopsesListData()
        {
            OutputType = (int)DealOutputType.Default;
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int OutputType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealSynopsesListInfoModel
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (Categories == null || SellerGuid == Guid.Empty)
                {
                    return false;
                }
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> Categories { get; set; }
    }

    /// <summary>
    /// 檔次列表Input Model
    /// </summary>
    public class DealSynopsesListByDealType
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (Categories == null || SellerGuid == Guid.Empty)
                {
                    return false;
                }
                if (!Enum.IsDefined(typeof(SkmCategorySortType), SortBy))
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public DealSynopsesListByDealType()
        {
            StartIndex = 0;
            OutputType = (int) DealOutputType.OldSchool;
        }

        /// <summary>
        /// 分店 GUID
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 檔次列類
        /// </summary>
        public List<int> Categories { get; set; }
        /// <summary>
        /// 排序方式
        /// </summary>
        public int SortBy { get; set; }
        /// <summary>
        /// 檔次類型 0:優惠活動 1:熱賣商品 2:注目商品
        /// </summary>
        public List<int> Filters { get; set; }
        /// <summary>
        /// 分頁起始
        /// </summary>
        public int StartIndex { get; set; }
        /// <summary>
        /// 取得類型 0:到店優惠 1:立即購買 2:不分 3:舊版(預設)
        /// </summary>
        public int OutputType { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class CategoryFiltersModel
    {
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (Categories == null || SellerGuid == Guid.Empty)
                {
                    return false;
                }
                if (!Enum.IsDefined(typeof(CategorySortType), SortBy))
                {
                    return false;
                }
                return true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Channel { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Area { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> Categories { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SortBy { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<int> Filters { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
    }

    /// <summary>
    /// 檔次列表
    /// </summary>
    public class DealSynopsesListModel : GroupDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public string PublicImage { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealListModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string PublicImage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<GroupDeal> Group { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DealListModel()
        {
            Group = new List<GroupDeal>();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class DealListInfo
    {
        /// <summary>
        /// 
        /// </summary>
        public DealListInfo()
        {
            DealTypeInfo = new List<SkmDealType>();
        }

        /// <summary>
        /// 賣家名稱
        /// </summary>
        public string SellerName { set; get; }
        /// <summary>
        /// 檔次資訊
        /// </summary>
        public List<SkmDealType> DealTypeInfo { set; get; }
        /// <summary>
        /// 預設類別Id(精選優惠)
        /// </summary>
        public int CategoryId { set; get; }

        /// <summary>
        /// 預設類別顯示名稱(精選優惠)
        /// </summary>
        public string CategoryName { get; set; }

        /// <summary>
        /// 自訂類別 (SkmPay)
        /// </summary>
        public List<CategoryInfo> CategoryListBySkmPay { get; set; }

        /// <summary>
        /// 自訂類別 (0元優惠)
        /// </summary>
        public List<CategoryInfo> CategoryListByZero { get; set; }
    }

    public class CategoryInfo
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public List<CategoryInfo> SubCategoryList { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class SkmDealType
    {
        /// <summary>
        /// 檔次屬性
        /// </summary>
        public SkmDealTypes DealType { get; set; }
        /// <summary>
        /// 屬性名稱
        /// </summary>
        public string DealTypeName { set; get; }
        /// <summary>
        /// 檔次數量
        /// </summary>
        public int DealCount { get; set; }
    }

    /// <summary>
    /// SKM 檔次列表資訊
    /// </summary>
    public class GroupDeal
    {
        /// <summary>
        /// 
        /// </summary>
        public int TotalDealCount { set; get; }
        /// <summary>
        /// 商品列表
        /// </summary>
        public List<SKMDealSynopsis> DealList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SellerName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public GroupDeal()
        {
            DealList = new List<SKMDealSynopsis>();
            TotalDealCount = 0;
        }
    }

    /// <summary>
    /// 會員訂單列表
    /// </summary>
    public class OrderListModel
    {
        /// <summary>
        /// 
        /// </summary>
        public List<dynamic> OrderList { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        public OrderListModel(List<dynamic> obj)
        {
            this.OrderList = obj;
        }
    }

    /// <summary>
    /// 商品列表物件
    /// </summary>
    public class SKMDealSynopsis
    {
        /// <summary>
        /// 檔次代號
        /// </summary>
        public Guid Bid { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示。
        /// </summary>
        public string DealName { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次名稱顯示，移除【TravelPlace】。
        /// </summary>
        public string MiniDealName { get; set; }
        /// <summary>
        /// 手機版頁面當作檔次介紹顯示(優惠活動，黑標)。
        /// </summary>
        public string DealEventName { get; set; }
        /// <summary>
        /// 畫面顯示的圖片路徑，回傳的圖片大小因應參數回傳適當大小。
        /// </summary>
        public string ImagePath { get; set; }
        /// <summary>
        /// 方形(M版)畫面顯示的圖片路徑
        /// </summary>
        public string SquareImagePath { get; set; }
        /// <summary>
        /// 已銷售數量-查詢時商品已銷售的數量
        /// </summary>
        public int SoldNum { get; set; }
        /// <summary>
        /// 已銷售數量顯示值-已銷售數量*設定的倍數 所得到用於顯示的的數值
        /// </summary>
        public int SoldNumShow { get; set; }
        /// <summary>
        /// 銷售數量顯示的單位
        /// </summary>
        public string ShowUnit { get; set; }
        /// <summary>
        /// 售價
        /// </summary>
        public decimal Price { get; set; }
        /// <summary>
        /// 原價
        /// </summary>
        public decimal OriPrice { get; set; }
        /// <summary>
        /// 折數顯示用文字"5折"等，特殊檔次可能顯示”特惠”等
        /// </summary>
        public string DiscountString { get; set; }
        /// <summary>
        /// 開始販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealStartTime { get; set; }
        /// <summary>
        /// 結束販售時間-時間字串，格式遵循RFC822規範。
        /// </summary>
        public string DealEndTime { get; set; }
        /// <summary>
        /// 是否已售完 true為售完
        /// </summary>
        public bool SoldOut { get; set; }
        /// <summary>
        /// 旅遊地點
        /// </summary>
        public string TravelPlace { get; set; }
        /// <summary>
        /// 檔次promo圖片,用於瀑布流介面
        /// </summary>
        public string PromoImage { get; set; }
        /// <summary>
        /// 檔次的ICON標示，如破千熱銷之類的ID值
        /// </summary>
        public List<int> DealTags { get; set; }
        /// <summary>
        /// 是否為全家檔次
        /// </summary>
        public bool IsFami { get; set; }
        /// <summary>
        /// 頻道類型
        /// </summary>
        public int BehaviorType { set; get; }
        /// <summary>
        /// 是否顯示兌換價
        /// </summary>
        public bool ShowExchangePrice { get; set; }
        /// <summary>
        /// 兌換價
        /// </summary>
        public decimal ExchangePrice { get; set; }
        /// <summary>
        /// 檔次擁有的分類
        /// </summary>
        public List<int> Categories { get; set; }
        /// <summary>
        /// 是否為體驗商品
        /// </summary>
        public bool isExperience { set; get; }
        /// <summary>
        /// 體驗商品收藏次數
        /// </summary>
        public string ExperienceDealCollectCount { set; get; }
        /// <summary>
        /// 已兌換人數
        /// </summary>
        public string ExchangeDealCount { set; get; }
        /// <summary>
        /// 序號 推薦的排序順序
        /// </summary>
        public int Seq { get; set; }
        /// <summary>
        /// 櫃位
        /// </summary>
        public string StoreName { get; set; }
        /// <summary>
        /// 館別
        /// </summary>
        public string Shop { get; set; }
        /// <summary>
        /// 優惠類型
        /// </summary>
        public int DiscountType { get; set; }
        /// <summary>
        /// 優惠內容
        /// </summary>
        public int Discount { get; set; }
        /// <summary>
        /// 是否為清算檔次
        /// </summary>
        public bool IsSettlementDeal { get; set; }
        /// <summary>
        /// 下次清算時間
        /// </summary>
        public string NextSettlementTime { get; set; }
        /// <summary>
        /// 檔次收藏數量
        /// </summary>
        public int DealMemberCollectCount { get; set; }
        /// <summary>
        /// 是否燒點活動
        /// </summary>
        public bool IsBurningEvent { get; set; }
        /// <summary>
        /// 需燒點數
        /// </summary>
        public int BurningPoint { get; set; }
        /// <summary>
        /// 用來做排序的價錢欄位,不做顯示用
        /// 規則:
        /// 原價A=特價B則顯示A
        /// 原價A>0特價B=0則顯示A
        /// 原價A>特價B則顯示B
        /// </summary>
        [JsonIgnore]
        public decimal PriceBySort { get; set; }
        /// <summary>
        /// 檔次Icon
        /// </summary>
        public SkmDealIcon IconInfo { get; set; }
        /// <summary>
        /// 買幾
        /// </summary>
        public int Buy { get; set; }
        /// <summary>
        /// 送幾
        /// </summary>
        public int Free { get; set; }
        /// <summary>
        /// skm pay
        /// </summary>
        public bool IsSkmPay { get; set; }
        /// <summary>
        /// 是否加入收藏
        /// </summary>
        public bool IsCollected { get; set; }
        /// <summary>
        /// 銷售數量
        /// </summary>
        [JsonIgnore]
        public int OrderedQuanitity { get; set; }
        /// <summary>
        /// Ph值
        /// </summary>
        [JsonIgnore]
        public string Ph { get; set; }

        /// <summary>
        /// 組織新光檔次內容
        /// </summary>
        /// <param name="skmDeal"></param>
        /// <param name="deal"></param>
        /// <param name="dealCategory"></param>
        /// <param name="bid"></param>
        /// <param name="seq"></param>
        public SKMDealSynopsis(SkmPponDeal skmDeal, IViewPponDeal deal, IEnumerable<CategoryDeal> dealCategory, Guid bid, int seq)
        {
            Bid = skmDeal.BusinessHourGuid;
            DealName = skmDeal.ApiDealName;
            MiniDealName = skmDeal.ApiDealName;
            DealEventName = skmDeal.ApiDealName;
            DealStartTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeS);
            DealEndTime = ApiSystemManager.DateTimeToDateTimeString(deal.BusinessHourOrderTimeE);
            DiscountString = ViewPponDealManager.GetDealDiscountString(deal);
            ImagePath = string.IsNullOrEmpty(skmDeal.PicUrl) ? skmDeal.AppPicUrl : skmDeal.PicUrl;
            SquareImagePath = skmDeal.AppPicUrl;
            Price = skmDeal.ItemPrice;
            OriPrice = skmDeal.ItemOrigPrice;
            SoldNum = 0;
            QuantityAdjustment qa = (deal.BusinessHourOrderTimeE < DateTime.Now) ? deal.GetAdjustedSlug() : deal.GetAdjustedOrderedQuantity();
            SoldNumShow = qa.Quantity;
            ExchangeDealCount = skmDeal.ExchangeDealCount;
            ExperienceDealCollectCount = skmDeal.ExperienceDealCollectCount;
            SoldOut = deal.IsSoldOut;
            TravelPlace = "優惠";
            PromoImage = "";
            DealTags = string.IsNullOrEmpty(deal.LabelIconList) ? new List<int> { 0 } : deal.LabelIconList.Split(',').Select(int.Parse).ToList();
            IsSkmPay = IsFami = false;
            BehaviorType = (int)PponDealApiManager.GetDealBehaviorType(deal);
            ShowExchangePrice = ((deal.GroupOrderStatus & (int) GroupOrderStatus.FamiDeal) > 0);
            ExchangePrice = skmDeal.ItemDefaultDailyAmount ?? 0;
            Categories = dealCategory.Where(x => x.Bid == bid).Select(x => x.Cid).ToList();
            isExperience = skmDeal.IsExperience ?? false;
            Seq = seq;
            DiscountType = skmDeal.DiscountType;
            Discount = skmDeal.Discount;
            IsSettlementDeal = skmDeal.IsSettlementDeal;
            NextSettlementTime = skmDeal.NextSettlementTime;
            DealMemberCollectCount = skmDeal.DealMemberCollectCount;
            IsBurningEvent = false;
            BurningPoint = 0;
            PriceBySort = (skmDeal.Discount == 0)//B=0
                ? skmDeal.ItemOrigPrice//=A
                : (skmDeal.Discount < skmDeal.ItemOrigPrice)//B<A
                  ? skmDeal.Discount//=B
                  : skmDeal.ItemOrigPrice;//=A
        }

        /// <summary>
        /// 組織新光檔次內容
        /// </summary>
        /// <param name="deal"></param>
        public SKMDealSynopsis(SkmPponDealSynopsis deal)
        {
            Bid = deal.Bid;
            DealName = deal.DealName;
            MiniDealName = deal.MiniDealName;
            DealEventName = deal.DealEventName;
            ImagePath = deal.ImagePath;
            SquareImagePath = deal.SquareImagePath;
            SoldNum = deal.SoldNum;
            SoldNumShow = deal.SoldNumShow;
            ShowUnit = deal.ShowUnit;
            Price = deal.Price;
            OriPrice = deal.OriPrice;
            DiscountString = deal.DiscountString;
            DealStartTime = deal.DealStartTime;
            DealEndTime = deal.DealEndTime;
            SoldOut = deal.SoldOut;
            TravelPlace = deal.TravelPlace;
            PromoImage = deal.PromoImage;
            DealTags = deal.DealTags;
            IsFami = deal.IsFami;
            BehaviorType = deal.BehaviorType;
            ShowExchangePrice = deal.ShowExchangePrice;
            ExchangePrice = deal.ExchangePrice;
            Categories = deal.Categories;
            isExperience = deal.isExperience;
            Seq = deal.Seq;
            ExperienceDealCollectCount = deal.ExperienceDealCollectCount;
            ExchangeDealCount = deal.ExchangeDealCount;
            DiscountType = deal.DiscountType;
            Discount = deal.Discount;
            IsCollected = false;
        }
    }
    

    /// <summary>
    /// 查詢贈品訂購量與庫存 Request Model
    /// </summary>
    public class GetGiftInventoryRequestModel
    {
        /// <summary>
        /// 贈品代號
        /// </summary>
        public string ExchangeItemId { get; set; }
    }

    /// <summary>
    /// 查詢新光三越贈品庫結果
    /// </summary>
    public class GiftOrderedAndInventoryQty
    {
        /// <summary>
        /// 建檔代號
        /// </summary>
        public Guid ExternalDealGuid { get; set; }
        /// <summary>
        /// 館別代號
        /// </summary>
        public string ShopCode { get; set; }
        /// <summary>
        /// 贈品代號
        /// </summary>
        public string ExchangeItemId { get; set; }
        /// <summary>
        /// 活動代號
        /// </summary>
        public string SkmEventId { get; set; }
        /// <summary>
        /// 庫存數量
        /// </summary>
        public int OrderedQty { get; set; }
        /// <summary>
        /// 剩餘庫存
        /// </summary>
        public int AvailableQty { get; set; }
    }

    /// <summary>
    /// 更新新光三越贈品列表 Request Model
    /// </summary>
    public class UpdateSkmGiftInventoryRequestModel
    {
        /// <summary>
        /// 
        /// </summary>
        public List<SkmGiftInventoryModel> SkmGiftInventory { get; set; }
    }

    /// <summary>
    /// 首頁客製樣板資訊
    /// </summary>
    public class IndexCustomizedBoardResponse
    {
        /// <summary> 連結網址 </summary>
        public string Url { get; set; }
        /// <summary> 圖片網址 </summary>
        public string Image { get; set; }
        /// <summary> 樣板類型 </summary>
        public int LayoutType { get; set; }
        /// <summary> 連結類型 </summary>
        public int ContentType { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core.ModelCustom;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    public class MessageBoxObj
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("isRead")]
        public string IsRead { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("subject")]
        public string Subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("content")]
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("sendTime")]
        public string SendTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("pokeballList")]
        public List<Pokeball> PokeballList { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("isInApp")]
        public string IsInApp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("shopCode")]
        public string ShopCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("orderTime")]
        public DateTime OrderTime { get; set; }
    }
}
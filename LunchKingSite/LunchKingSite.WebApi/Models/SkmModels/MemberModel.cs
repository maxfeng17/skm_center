﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Model.OAuth;
using LunchKingSite.Core;
using LunchKingSite.BizLogic.Facade;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// 
    /// </summary>
    public class GetTicketModel
    {
        /// <summary>
        /// SKM提供的會員token
        /// </summary>
        public string SkmToken { get; set; }

        /// <summary>
        /// 是否同意共同行銷會員條款
        /// </summary>
        public bool IsShared { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (SkmToken.Trim() == string.Empty)
                {
                    return false;
                }
                if (CardNumber.Trim() == string.Empty)
                {
                    SkmFacade.SkmLog(SkmToken, 0, SkmLogType.CheckSkmTokenApi, string.Format("deviceOs:{0}", DeviceOs), "CardNumber is null.");
                    return false;
                }
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public GetTicketModel()
        {
            IsShared = false;
        }

        #region 驗證skm會員身份傳入參數
        /// <summary>
        /// 
        /// </summary>
        public string CardCheckCode { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string CardNumber { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string DeviceId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string DeviceOs { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string RegistrationId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string WebLoginPassword { set; get; }

        #endregion

    }

    /// <summary>
    /// 回傳 Skm Oauth Ticket Model
    /// </summary>
    public class ResponseTicketModel
    {
        /// <summary>
        /// 是否同意共同會員
        /// </summary>
        public bool IsShared { set; get; }
        /// <summary>
        /// Access Token
        /// </summary>
        public string AccessToken { get; set; }
        /// <summary>
        /// Expires sec
        /// </summary>
        public int ExpiresIn { get; set; }
        /// <summary>
        /// token type
        /// </summary>
        public string TokenType { get; set; }
        /// <summary>
        /// refresh token
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <param name="isShared"></param>
        public ResponseTicketModel(ApiTokenResponse token, bool isShared)
        {
            IsShared = isShared;
            AccessToken = token.AccessToken;
            ExpiresIn = token.ExpiresIn;
            TokenType = token.TokenType;
            RefreshToken = token.RefreshToken;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetMemberCollectDealListInputModel
    {
        private int _version = 0;

        /// <summary>
        /// 
        /// </summary>
        public MemberCollectDealType CollectType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Version {
            get { return _version; }
            set
            {
                _version = Enum.IsDefined(typeof(ExternalDealVersion), _version) ? value : 0;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public ExternalDealVersion DealVersion
        {
            get { return (ExternalDealVersion)Version; }
        }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (CollectType == MemberCollectDealType.Coupon || CollectType == MemberCollectDealType.Experience)
                {
                    return true;
                }
                return false;
            }
        }
    }


    /// <summary>
    /// 收藏/體驗檔次
    /// </summary>
    public class MemberCollectDealSetModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string  Bid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool Collect { set; get; }
        /// <summary>
        /// 是否提示
        /// </summary>
        public bool Notice { set; get; }
        /// <summary>
        /// 收藏類型
        /// </summary>
        public byte CollectType { set; get; }
        
    }

    /// <summary>
    /// 取消登入會員的特定收藏檔次/體驗商品的收藏紀錄，指定方式為傳入檔次的BID陣列
    /// </summary>
    public class MemberCollectDealRemoveModel
    {
        /// <summary>
        /// BidList
        /// </summary>
        public List<string> BidList { set; get; }
    }
    /// <summary>
    /// 取得付款時需要的檔次資料，指定方式為傳入Bid 
    /// </summary>
    public class DealDataForCheckoutModel
    {
        /// <summary>
        /// Bid
        /// </summary>
        public string Bid { set; get; }
    }

    /// <summary>
    /// RefreshToken 傳入值
    /// </summary>
    public class RefreshTokenInputModel
    {
        /// <summary>
        /// RefreshToken
        /// </summary>
        public string RefreshToken { get; set; }
    }

    /// <summary>
    /// 設定共享會員
    /// </summary>
    public class SetMemberSharedInputModel
    {
        /// <summary>
        /// 是否共享
        /// </summary>
        public bool IsShared { set; get; }
    }
}

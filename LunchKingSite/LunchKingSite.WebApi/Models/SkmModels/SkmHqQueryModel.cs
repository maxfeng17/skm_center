﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    public class GetSkmpayOrderInfoRequestModel
    { 
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class GetSkmInfoRequestModel
    {
        public DateTime StartOpDate { get; set; }
        public DateTime EndOpDate { get; set; }
    }
}

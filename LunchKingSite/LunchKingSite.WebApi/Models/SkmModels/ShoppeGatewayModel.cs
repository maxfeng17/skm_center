﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// 
    /// </summary>
    public class KeepAliveModel
    {
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("serverTime")]
        public string ServerTime { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ShoppeGatewayModel
    {
        /// <summary>
        ///  館別代號
        /// </summary>
        [JsonProperty("shopId")]
        public string ShopId { get; set; }
        /// <summary>
        ///  館別名稱
        /// </summary>
        [JsonProperty("shopName")]
        public string ShopName { get; set; }
        /// <summary>
        ///  櫃位代號
        /// </summary>

        [JsonProperty("brandCounterId")]
        public string BrandCounterId { get; set; }
        /// <summary>
        ///  櫃位名稱
        /// </summary>
        [JsonProperty("brandCounterName")]
        public string BrandCounterName { get; set; }
        /// <summary>
        ///  櫃位代號(6X)
        /// </summary>
        [JsonProperty("skmStoreId")]
        public string SkmStoreId { get; set; }
        /// <summary>
        ///  櫃位名稱(6X)
        /// </summary>
        [JsonProperty("skmStoreName")]
        public string SkmStoreName { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("floor")]
        public string Floor { get; set; }
        /// <summary>
        ///  大分類編碼
        /// </summary>
        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }
        /// <summary>
        ///  大分類名稱
        /// </summary>
        [JsonProperty("categoryName")]
        public string CategoryName { get; set; }
        /// <summary>
        ///  次分類編碼
        /// </summary>
        [JsonProperty("subCategoryId")]
        public string SubcategoryId { get; set; }
        /// <summary>
        ///  次分類名稱
        /// </summary>
        [JsonProperty("subCategoryName")]
        public string SubcategoryName { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("modifyTime")]
        public string ModifyTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("storeGuid")]
        public Guid StoreGuid { get; set;}
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("brandGuid")]
        public Guid BrandGuid { get; set;}
    }
}

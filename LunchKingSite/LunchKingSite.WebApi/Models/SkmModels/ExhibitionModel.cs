﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// GetExhibitionEventsModel
    /// </summary>
    public class GetExhibitionEventsModel
    {
        /// <summary>
        /// 店別GUID
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int CardLevel { get; set; }
    }

    /// <summary>
    /// GetCrmEventsModel
    /// </summary>
    public class GetCrmEventsModel
    {
        /// <summary>
        /// 店別GUID
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int CardLevel { get; set; }
    }

    /// <summary>
    /// GetExhibitionEventsResponse
    /// </summary>
    public class GetExhibitionEventsResponse
    {
        /// <summary>
        /// 策展ID
        /// </summary>
        public int ExhibitionEventId { get; set; }

        /// <summary>
        /// 策展Banner
        /// </summary>
        public string BannerImgUrl { get; set; }

        /// <summary>
        /// 策展類型
        /// </summary>
        public int EventType { get; set; }

        /// <summary>
        /// 含檔量
        /// </summary>
        public int DealCount { get; set; }
        /// <summary>
        /// 單一活動
        /// </summary>
        public string EventUrl { get; set; }
        /// <summary>
        /// 策展名稱
        /// </summary>
        public string ExhibitionEventName { get; set; }
        /// <summary>
        /// Exhibition Event Start Date
        /// </summary>
        [JsonProperty("startDate")]
        public string StartDate { get; set; }
        /// <summary>
        /// Exhibition Event End Date
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }

    /// <summary>
    /// CRM Event
    /// </summary>
    public class GetCrmEventsResponse
    {
        /// <summary>
        /// 
        /// </summary>
        public GetCrmEventsResponse()
        {
            ExhibitionEvents = new List<GetExhibitionEventsResponse>();
            LatestActivityList = new List<LatestActivity>();
        }
        /// <summary>
        /// 策展
        /// </summary>
        public List<GetExhibitionEventsResponse> ExhibitionEvents { get; set; }
        /// <summary>
        /// 活動
        /// </summary>
        public List<LatestActivity> LatestActivityList { get; set; }
    }

    /// <summary>
    /// 策展檔次
    /// </summary>
    public class GetExhibitionDealModel
    {
        /// <summary>
        /// 店別GUID
        /// </summary>
        public Guid SellerGuid { get; set; }
        /// <summary>
        /// 策展ID
        /// </summary>
        public int ExhibitionId { get; set; }
    }

    /// <summary>
    /// 單一策展輸出
    /// </summary>
    public class ExhibitionDealResponse
    {
        /// <summary>
        /// 策展名稱
        /// </summary>
        public string ExhibitionName { get; set; }
        /// <summary>
        /// 策展主圖
        /// </summary>
        public string MainBanner { get; set; }
        /// <summary>
        /// 策展內文
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 活動規則
        /// </summary>
        public string Rules { get; set; }
        /// <summary>
        /// 策展類型
        /// </summary>
        public int EventType { get; set; }
        /// <summary>
        /// 單一活動
        /// </summary>
        public string EventUrl { get; set; }
        /// <summary>
        /// 策展開始時間
        /// </summary>
        public string StartDate { get; set; }
        /// <summary>
        /// 策展結束時間
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// 主打
        /// </summary>
        public List<SKMDealSynopsis> TopDeals { get; set; }
        /// <summary>
        /// 策展商品
        /// </summary>
        public List<SkmExhibitionCategoryDeal> CategoryDeals { get; set; }
        /// <summary>
        /// 多重活動
        /// </summary>
        public List<SkmExhibitionActivity> Activities { get; set; }
    }

    /// <summary>
    /// 策展檔次
    /// </summary>
    public class SkmExhibitionCategoryDeal
    {
        /// <summary>
        /// 策展分類
        /// </summary>
        public string CategoryName { get; set; }
        /// <summary>
        /// 商品名稱
        /// </summary>
        public List<SKMDealSynopsis> Deals { get; set; }
    }

    /// <summary>
    /// 多重活動策展
    /// </summary>
    public class SkmExhibitionActivity
    {
        /// <summary>
        /// BannerUrl
        /// </summary>
        public string BannerUrl { get; set; }
        /// <summary>
        /// ActivityUrl
        /// </summary>
        public string ActivityUrl { get; set; }
        /// <summary>
        /// inline 登入
        /// </summary>
        public bool IsRsvLogin { get; set; }
    }

    /// <summary>
    /// SKM餐廳訂候位導頁登入Object
    /// </summary>
    public class Rsv
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public Rsv(string url)
        {
            App = new RsvUrl(url);
        }

        /// <summary>
        /// APP
        /// </summary>
        [JsonProperty("app")]
        public RsvUrl App { get; set; }
    }

    /// <summary>
    /// SKM導頁
    /// </summary>
    public class RsvUrl
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="url"></param>
        public RsvUrl(string url)
        {
            Redirect = url;
        }

        /// <summary>
        /// URL
        /// </summary>
        [JsonProperty("redirect")]
        public string Redirect { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class TestRsv
    {
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }

    /// <summary>
    /// SKM餐廳分店首頁
    /// </summary>
    public class RsvHomeUrl
    {
        /// <summary>
        /// 
        /// </summary>
        public string ShopCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Url { get; set; }
    }
}

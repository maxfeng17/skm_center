﻿using Newtonsoft.Json;
using System;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// 
    /// </summary>
    public class VerifyGatewayModel
    {

        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("skmId")]
        public int SkmId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("skmTransId")]
        public string SkmTransId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("createTime")]
        public DateTime CreateTime { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("isForce")]
        public bool IsForce { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("syncStatus")]
        public int SyncStatus { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("verifyAction")]
        public string VerifyAction { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("trustId")]
        public Guid TrustId { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("discountType")]
        public int DiscountType { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("discountValue")]
        public double DiscountValue { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("usingDateS")]
        public DateTime UsingDateS { get; set; }
        /// <summary>
        ///  
        /// </summary>
        [JsonProperty("usingDateE")]
        public DateTime UsingDateE { get; set; }
        /// <summary>
        ///  館別代號
        /// </summary>
        [JsonProperty("shopCode")]
        public string ShopeCode { get; set; }
        /// <summary>
        ///  櫃位代號
        /// </summary>
        [JsonProperty("brandCounterCode")]
        public string BrandCounterCode { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return string.Format("[SkmId] = {0} [CouponId] = {1} [CreateTime] = {2} [IsForce] = {3} [SyncStatus] = {4} [VerifyAction] = {5} [TrustId] = {6} [DiscountType] = {7} [DiscountValue] = {8} [UsingDateS] = {9} [UsingDateE] = {10}",
                SkmId, CreateTime, IsForce, SyncStatus, 
                VerifyAction, TrustId, DiscountType, DiscountValue, 
                UsingDateS, UsingDateE);
        }
    }
}

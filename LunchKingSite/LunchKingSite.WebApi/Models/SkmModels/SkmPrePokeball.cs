﻿using LunchKingSite.Core.ModelCustom;
using LunchKingSite.Core.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.WebLib.Controllers;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    public class SkmPrePokeball
    {
        /// <summary>
        /// 訊息編號
        /// </summary>
        public int MessageId { get; set; }
        public string Subject { get; set; }
        public int EventType { get; set; }
        public string ActionUrl { get; set; }
        public Guid ActionBid { get; set; }
        public PokeballActionType ActionType { get; set; }
        public byte MsgType { get; set; }

        public SkmPrePokeball(BeaconMessageModel beacon)
        {
            MessageId = beacon.Id;
            Subject = beacon.Subject;
            EventType = beacon.EventType;
            ActionType = PokeballActionType.None;
            ActionUrl = beacon.ActionUrl;
            ActionBid = beacon.ActionBid;
            MsgType = (byte)SkmInAppMessageType.Beacon;
        }

        public SkmPrePokeball(SkmInAppMessage msg)
        {
            MessageId = msg.Id;
            Subject = msg.Subject;
            EventType = msg.EventType;
            ActionType = PokeballActionType.None;
            ActionUrl = msg.Url;
            ActionBid = msg.GuidVal ?? Guid.Empty;
            MsgType = msg.MsgType;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.Core.Models.Entities;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.WebApi.Models.SkmModels
{
    /// <summary>
    /// 重新整理憑證QrCode Model
    /// </summary>
    public class RefreshCouponModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 憑證編號
        /// </summary>
        public string SequenceNumber { get; set; }
    }

    /// <summary>
    /// 訂單詳細資料 Model
    /// </summary>
    public class GetMemberOrderByOrderGuidModel
    {
        /// <summary>
        /// 訂單Guid
        /// </summary>
        public Guid OrderGuid { get; set; }

        /// <summary>
        /// 使用者unique id
        /// </summary>
        public int UserId { get; set; }
    }

    /// <summary>
    /// QrCodeModel
    /// </summary>
    public class QrCodeModel 
    {
        /// <summary>
        /// Base64 QrCode
        /// </summary>
        public string QrCode { get; set; }
        /// <summary>
        /// 憑證存活期限
        /// </summary>
        public string QrCodeExpireTime { get; set; }
    }

    /// <summary>
    /// 零元檔input
    /// </summary>
    public class ZeroPayMakeOrderInputModel
    {
        /// <summary>
        /// PaymentDto
        /// </summary>
        public JObject DtoString { set; get; }
        /// <summary>
        /// DeviceType
        /// </summary>
        public int DeviceType { set; get; }
        /// <summary>
        /// Skm 會員卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 端末設備代號
        /// </summary>
        public string DeviceCode { get; set; }
        /// <summary>
        /// 活動token
        /// </summary>
        public string ActivityToken { get; set; }
        /// <summary>
        /// APP所選店別Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
    }
    
    /// <summary>
    /// 抽獎Model
    /// </summary>
    public class PrizeDrawOrderModel
    {
        /// <summary>
        /// DeviceType
        /// </summary>
        public int DeviceType { set; get; }
        /// <summary>
        /// Skm 會員卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 端末設備代號
        /// </summary>
        public string DeviceCode { get; set; }
        /// <summary>
        /// 活動token
        /// </summary>
        public string EventToken { get; set; }
        /// <summary>
        /// APP所選店別Guid
        /// </summary>
        public Guid SellerGuid { get; set; }
    }

    /// <summary>
    /// 兌換獎項model
    /// </summary>
    public class PrizeDrawExchangeModel
    {
        /// <summary>
        /// 端末設備代號
        /// </summary>
        public string DeviceCode { get; set; }
        /// <summary>
        /// Skm 會員卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 活動token
        /// </summary>
        public string EventToken { get; set; }
        /// <summary>
        /// 兌換館別
        /// </summary>
        public string ShopCode { get;set;}
        /// <summary>
        /// 抽獎guid
        /// </summary>
        public int RecordId { get; set; }
    }

    public class DrawDetailModel
    {
        public int ActiveType { get; set; }
        public string ItemName { get; set; }
        public string CreateTime { get; set; }
        public string ShopName { get; set; }
        public int RecordId { get;set;}
        public bool IsThanks { get;set;}
    }

    /// <summary>
    /// 抽獎Record
    /// </summary>
    public class DrawResultModel
    {
        /// <summary>
        /// 本次抽獎的商品是否為銘謝惠顧
        /// </summary>
        public bool ItemIsThanks { get; set; }
        /// <summary>
        /// 本次抽獎的商品名稱
        /// </summary>
        public string ItemName { get; set; }

        /// <summary>
        /// 本次抽獎的型態
        /// </summary>
        public int ItemActiveType { get; set; }
        /// <summary>
        /// Payment result data
        /// </summary>
        public ApiFreeDealPayReply Data { set; get; }

        /// <summary>
        /// 已抽獎次數
        /// </summary>
        public int TotalDrawQty { get; set; }

        /// <summary>
        /// 剩餘抽獎次數
        /// </summary>
        public int RemainDrawQty { get; set; }

        /// <summary>
        /// 是否為當日限制抽獎上限/false則為總活動次數上限
        /// </summary>
        public bool IsDailyLimit { get; set; }

        /// <summary>
        /// 中獎Record
        /// </summary>
        public List<DrawDetailModel> DrawRecord { get; set; }

        /// <summary>
        /// 抽獎紀錄唯一ID
        /// </summary>
        public int RecordId { get; set; }
    }

    /// <summary> 完成OTP購買流程請求資訊 </summary>
    public class CompleteOTPOrderRequest
    {
        /// <summary> 交易TOKEN </summary>
        public string PaymentToken { get; set; }
    }

    /// <summary> SKM PAY MakeOrder 請求資訊 </summary>
    public class SkmPayMakeOrderRequest
    {
        /// <summary>
        /// 驗證
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(CardNo.Trim()))
                {
                    return false;
                }

                if (PaymentToken == string.Empty || PreTransNo == string.Empty)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// PaymentDto
        /// </summary>
        public JObject DtoString { set; get; }
        /// <summary>
        /// DeviceType
        /// </summary>
        public int DeviceType { set; get; }
        /// <summary>
        /// Skm 會員卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 預約交易TOKEN
        /// </summary>
        public string PreTransNo { get; set; }
        /// <summary>
        /// 交易TOKEN
        /// </summary>
        public string PaymentToken { get; set; }
        /// <summary>
        /// 載具
        /// </summary>
        public SkmPayInvoiceCarrierType ECInvoiceType { get; set; }
        /// <summary>
        /// 統編
        /// </summary>
        public string UniformNumber { get; set; }
        /// <summary>
        /// 自然人憑證或手機條碼
        /// </summary>
        public string InvoiceVehicleBarcode { get; set; }
        /// <summary>
        /// 愛心碼
        /// </summary>
        public string DonateNo { get; set; }

        /// <summary>
        /// 會員 APP 專屬密碼
        /// </summary>
        public string VipAppPass { get; set; }
        /// <summary>
        /// 授權版本
        /// </summary>
        public int AuthVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SkmPayMakeOrderRequest()
        {
            AuthVersion = 0;
        }

    }


    /// <summary> SKM PAY Parking MakeOrder 請求資訊 </summary>
    public class SkmPayParkingMakeOrderRequest
    {
        /// <summary>
        /// 驗證
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(CardNo.Trim()))
                {
                    return false;
                }

                if (PaymentToken == string.Empty || PreTransNo == string.Empty)
                {
                    return false;
                }

                return true;
            }
        }

        /// <summary>
        /// Skm 會員卡號
        /// </summary>
        public string CardNo { get; set; }
        /// <summary>
        /// 預約交易TOKEN
        /// </summary>
        public string PreTransNo { get; set; }
        /// <summary>
        /// 交易TOKEN
        /// </summary>
        public string PaymentToken { get; set; }
        /// <summary>
        /// 載具
        /// </summary>
        public SkmPayInvoiceCarrierType ECInvoiceType { get; set; }
        /// <summary>
        /// 統編
        /// </summary>
        public string UniformNumber { get; set; }
        /// <summary>
        /// 自然人憑證或手機條碼
        /// </summary>
        public string InvoiceVehicleBarcode { get; set; }
        /// <summary>
        /// 愛心碼
        /// </summary>
        public string DonateNo { get; set; }

        /// <summary>
        /// 會員 APP 專屬密碼
        /// </summary>
        public string VipAppPass { get; set; }
        /// <summary>
        /// 授權版本
        /// </summary>
        public int AuthVersion { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SkmPayParkingMakeOrderRequest()
        {
            AuthVersion = 0;
        }
    }

}

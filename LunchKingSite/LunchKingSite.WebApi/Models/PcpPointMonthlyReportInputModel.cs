﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointMonthlyReportInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Year { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int Month { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int PointType { set; get; }
    }
}

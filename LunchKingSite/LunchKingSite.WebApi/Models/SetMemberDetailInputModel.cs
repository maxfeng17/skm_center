﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SetMemberDetailInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? Gender { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime?Birthday { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public string Remarks { set; get; }

    }
}

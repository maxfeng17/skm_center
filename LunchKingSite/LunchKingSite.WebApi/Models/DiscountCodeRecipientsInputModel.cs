﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountCodeRecipientsInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int CampaignId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (CampaignId <= 0)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
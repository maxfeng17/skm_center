﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// Result Code 描述
    /// </summary>
    public class ApiResultCodeModel
    {
        /// <summary>
        /// 代碼
        /// </summary>
        public string ResultCode { get; set; }
        /// <summary>
        /// 編號
        /// </summary>
        public int ResultNo { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string ResultDescript { get; set; }
    }
}

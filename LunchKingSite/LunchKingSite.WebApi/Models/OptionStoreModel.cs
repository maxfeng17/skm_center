﻿using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class OptionStoreModel : StoreModel
    {
        /// <summary>
        /// 
        /// </summary>
        public OptionStoreModel()
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="store"></param>
        /// <param name="selected"></param>
        /// <param name="enabled"></param>
        public OptionStoreModel(Seller store, bool selected, bool enabled) : base(store)
        {
            Selected = selected;
            Enabled = enabled;
        }

        /// <summary>
        /// 
        /// </summary>
        public bool Selected { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool Enabled { get; set; }
    }
}

﻿using System;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CheckPCPCodeInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Code { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public Guid StoreGuid { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (string.IsNullOrEmpty(Code))
                {
                    return false;
                }

                return StoreGuid != Guid.Empty;
            }
        }
    }
}

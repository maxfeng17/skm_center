﻿using System;
using System.Collections.Generic;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountCodeRecipientModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string Nickname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? UseTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaignId"></param>
        /// <param name="userId"></param>
        /// <param name="testMode"></param>
        /// <returns></returns>
        public static DiscountCodeRecipientModel[] Create(int campaignId, int userId, bool testMode)
        {
            return new DiscountCodeRecipientModel[]
            {
                new DiscountCodeRecipientModel
                {
                    Nickname = "00001",
                    UseTime = new DateTime(2004, 10, 15)
                },
                new DiscountCodeRecipientModel
                {
                    Nickname = "00002",
                    UseTime = new DateTime(2004, 10, 16)
                },
                new DiscountCodeRecipientModel
                {
                    Nickname = "00004",
                    UseTime = new DateTime(2004, 10, 21)
                }
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vddCol"></param>
        /// <returns></returns>
        public static DiscountCodeRecipientModel[] Create(ViewDiscountDetailCollection vddCol)
        {
            List<DiscountCodeRecipientModel> result = new List<DiscountCodeRecipientModel>();
            foreach (ViewDiscountDetail vdd in vddCol)
            {
                result.Add(new DiscountCodeRecipientModel
                {
                    Nickname = vdd.NickName,
                    UseTime = vdd.UseTime
                });
            }
            return result.ToArray();
        }
    }
}

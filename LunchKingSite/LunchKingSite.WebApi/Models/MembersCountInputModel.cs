﻿using System;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MembersCountInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int Type { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get { return Enum.IsDefined(typeof (SellerMemberType), Type); }
        }
    }
}

﻿using System;
using LunchKingSite.DataOrm;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class DiscountTemplateDetailCampaignModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int CampaignId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CampaignNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? SendDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CampaignType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SendCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? UsedCount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DiscountTemplateDetailCampaignModel() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vpadc"></param>
        public DiscountTemplateDetailCampaignModel(ViewPcpAssignmentDiscountCampaign vpadc)
        {
            CampaignId = vpadc.Id;
            CampaignNo = vpadc.CampaignNo;
            SendDate = vpadc.StartTime;
            CampaignType = vpadc.Type;
            SendCount = vpadc.SendCount;
            UsedCount = vpadc.UseCount;
        }
    }

    
}

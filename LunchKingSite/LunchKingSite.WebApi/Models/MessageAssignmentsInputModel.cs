﻿using System;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MessageAssignmentsInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime BeginTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndTime { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (BeginTime > EndTime)
                {
                    return false;
                }
                return Helper.CheckSqlDateTime(BeginTime) && Helper.CheckSqlDateTime(EndTime);
            }
        }
    }
}

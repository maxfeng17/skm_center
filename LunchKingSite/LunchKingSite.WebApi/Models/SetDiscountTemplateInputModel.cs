﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SetDiscountTemplateInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int TemplateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal MinimumAmount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AvailableDateType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? StartTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool CardCombineUse { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SituationType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<Guid> Stores { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (StartTime > EndTime)
                {
                    return false;
                }
                if (Helper.CheckSqlDateTime(StartTime) == false || Helper.CheckSqlDateTime(EndTime) == false)
                {
                    return false;
                }
                if (AvailableDateType < 1 || AvailableDateType > 3)
                {
                    return false;
                }
                return true;
            }
        }
    }
}
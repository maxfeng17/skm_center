﻿namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MemberDetailInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int? UserId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int? SellerMemberId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                return UserId != null || SellerMemberId != null;
            }
        }
    }
}

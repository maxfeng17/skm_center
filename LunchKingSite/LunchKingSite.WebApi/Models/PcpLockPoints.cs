﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpLockPoints
    {
        /// <summary>
        /// 
        /// </summary>
        public int PointType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalLockPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PcpLockPointLog> Logs { get; set; }

    }
    /// <summary>
    /// 
    /// </summary>
    public class PcpLockPointLog
    {
        /// <summary>
        /// 
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DiscountCampaignNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime StarTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime EndTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalLockPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UsePoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LockPoint { get; set; }
    }
}

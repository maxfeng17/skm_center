﻿using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class StoreDiscountCodeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int CodeId { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public int MinimumAmount { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public DiscountCampaignType Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Selected { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public bool CardCombineUse { set; get; }
    }
}

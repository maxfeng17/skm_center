﻿using System;
using System.Collections.Generic;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class VBSAccountOverview
    {
        /// <summary>
        /// 
        /// </summary>
        public VBSAccountOverview()
        {
            DiscountCodeUsingDatas = new List<DiscountCodeUsingData>();
        }
        /// <summary>
        /// 本月新增會員數
        /// </summary>
        public int NewMember { get; set; }

        /// <summary>
        /// 商家剩餘熟客點點數
        /// </summary>
        public int RegularPoint { get; set; }

        /// <summary>
        /// 商家剩餘公關點點數;int
        /// </summary>
        public int FavorPoint { get; set; }
        /// <summary>
        /// 本月生日會員總覽資料
        /// </summary>
        public PCPMemberBirthdayOverall BirthdayMemberData { get; set; }
        /// <summary>
        /// 是否有發行會員卡
        /// </summary>
        public bool HaveIssuedCard { get; set; }
        /// <summary>
        /// 累積會員人次的折線圖資料
        /// </summary>
        public LineChart TotalMemberLineChart { get; set; }
        /// <summary>
        /// 每日人次的折線圖資料
        /// </summary>
        public LineChart DailyNewMemberLineChart { get; set; }
        /// <summary>
        /// 抵用券使用資料
        /// </summary>
        public List<DiscountCodeUsingData> DiscountCodeUsingDatas { get; set; } 
    }
    /// <summary>
    /// 
    /// </summary>
    public class LineChart
    {
        /// <summary>
        /// 
        /// </summary>
        public LineChart()
        {
            DataList = new List<LineChartPoint>();
        }

        /// <summary>
        /// 說明
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<LineChartPoint> DataList { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class LineChartPoint
    {
        /// <summary>
        /// 
        /// </summary>
        public DateTime XAxisDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? YAxisNumber { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PCPMemberBirthdayOverall
    {
        /// <summary>
        /// 
        /// </summary>
        public PCPMemberBirthdayOverall()
        {
            Representatives = new List<Representative>();
        }
        /// <summary>
        /// 生日會員人數
        /// </summary>
        public int BirthdayMemberCount { get; set; }
        /// <summary>
        /// 代表會員名稱;string陣列;用來顯示於畫面豐富內容
        /// </summary>
        public List<Representative> Representatives { get; set; } 
    }
    /// <summary>
    /// 代表名單
    /// </summary>
    public class Representative
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public GenderType Gender { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class DiscountCodeUsingData
    {
        /// <summary>
        /// 折價券設定的類型
        /// </summary>
        public int SituationType { get; set; }
        /// <summary>
        /// 折價券設定的類型說明文字;string;
        /// </summary>
        public string SituationTypeDesc { get; set; }
        /// <summary>
        /// 熟客券發送日期
        /// </summary>
        public DateTime SendDate { get; set; }
        /// <summary>
        /// 圖表顯示的百分比
        /// </summary>
        public double ImagePercentage { get; set; }
        /// <summary>
        /// 實際兌換百分比;double;熟客券兌換率的實際百分比
        /// </summary>
        public double UsingPercentage { get; set; }
        /// <summary>
        /// 折價券說明
        /// </summary>
        public string Description { get; set; }
    }
}

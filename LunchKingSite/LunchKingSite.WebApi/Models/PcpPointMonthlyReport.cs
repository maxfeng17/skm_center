﻿using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointMonthlyReport
    {
        /// <summary>
        /// 
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Month { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PointType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Point { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LockPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PcpBalanceLog> Items { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PcpBalanceLog
    {
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Point { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LockPoint { get; set; }
    }
}

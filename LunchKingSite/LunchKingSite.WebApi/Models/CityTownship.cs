﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CityTownship
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("vi")]
        public bool Visible { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("towns", NullValueHandling = NullValueHandling.Ignore)]
        public List<CityTownship> Townships { get; set; }
    }
}
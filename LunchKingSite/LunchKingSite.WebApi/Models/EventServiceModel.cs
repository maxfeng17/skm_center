﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.WebApi.Controllers;
using LunchKingSite.WebApi.Core;

namespace LunchKingSite.WebApi.Models
{
    #region input models

    /// <summary>
    /// 
    /// </summary>
    public class GetMainPageCurationsModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetCurationInfoByChannelInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ChannelId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? AreaId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetEventInfoByEventIdInputModel : IApiUserInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 測試用參數，強制從DB讀取資料
        /// </summary>
        public bool Reload { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public GetEventInfoByEventIdInputModel()
        //{
        //    Type = null;
        //}
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetEventPromoDealListInputModel : IApiUserInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? Type { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EventId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Category { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SubCategory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? GetAll { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int? StartIndex { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? Reload { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public GetEventPromoDealListInputModel()
        {
            Type = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetAppBannerByChannelInputModel : IApiUserInputModel
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string DeviceId { get; set; }
        /// <summary>
        /// 目前沒有作用
        /// </summary>
        public string ChannelId { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class RushBuyInputModel
    {
        public bool IsAll { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetEventDiscountModel
    {
        /// <summary>
        /// 活動類型
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 活動編號
        /// </summary>
        public int EventId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetCurationDiscountCodeModel
    {
        /// <summary>
        /// 折價券編號
        /// </summary>
        public int DiscountId { get; set; }
    }

    /// <summary>
    /// 
    /// </summary>
    public class GetCurationsByThemeModel
    {
        /// <summary>
        /// 
        /// </summary>
        public int ThemeId { get; set; }
    }


    #endregion

    #region output models

    /// <summary>
    /// 
    /// </summary>
    public class GetCurationDiscountCodeResult
    {
        /// <summary>
        /// 
        /// </summary>
        public ApiGetDiscountCodeResult ResultCode { get; set; }
    }

    #endregion

    /// <summary>
    /// 
    /// </summary>
    public enum ApiGetDiscountCodeResult
    {
        /// <summary>
        /// 領取成功
        /// </summary>
        Success = 0,
        /// <summary>
        /// 已索取完畢
        /// </summary>
        Complete = 1,
        /// <summary>
        /// 已領取
        /// </summary>
        Received = 2,
        /// <summary>
        /// 領取失敗
        /// </summary>
        Faild = 3
    }
}

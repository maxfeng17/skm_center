﻿using System;
using LunchKingSite.Core;
using LunchKingSite.Core.Enumeration;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class MembersInputModel
    {
        /// <summary>
        /// 會員類型 0：未指定   1：17life會員   2：商家會員
        /// </summary>
        public int Type { set; get; }
        /// <summary>
        /// 由第幾筆開始查詢
        /// </summary>
        public int StartIndex { set; get; }
        /// <summary>
        /// 一次查幾筆, 0表示查詢所有資料
        /// </summary>
        public int Size { set; get; }
        /// <summary>
        /// 查詢條件
        /// </summary>
        public string Criteria { set; get; }
        /// <summary>
        /// 
        /// </summary>
        public MembersInputModelFilter Filter { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (StartIndex < 0)
                {
                    return false;
                }

                if (Size < 0)
                {
                    return false;
                }

                return Enum.IsDefined(typeof(SellerMemberType), Type);
            }
        }
    }
}

﻿using Newtonsoft.Json;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SubscribeModel
    {
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [JsonProperty("categoryId")]
        public int CategoryId { get;set; }
    }
}

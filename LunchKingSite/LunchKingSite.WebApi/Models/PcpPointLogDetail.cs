﻿using System;
using System.Collections.Generic;

namespace LunchKingSite.WebApi.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class PcpPointLogDetail
    {
        /// <summary>
        /// 
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PointType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LogType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LogTime { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int PaymentMethod { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public double Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TotalPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SubTotalPoint { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<PcpRedeemingLog> DiscountCodeVerify { get; set; }
    }
    /// <summary>
    /// 
    /// </summary>
    public class PcpRedeemingLog
    {
        /// <summary>
        /// 
        /// </summary>
        public int CampaignId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string CampaignNo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CostPoint { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.BizLogic.Model.VBS;

namespace LunchKingSite.WebApi.Models.POSVerifyModels
{
    /// <summary>
    /// 憑證掃碼確認 Input
    /// </summary>
    public class VerifyCheckModel
    {
        /// <summary>
        /// couponSerial
        /// </summary>
        public string CouponSerial { set; get; }
        /// <summary>
        /// CouponCode
        /// </summary>
        public string CouponCode { set; get; }
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// CouponType
        /// </summary>
        public int CouponType { set; get; }
    }
    /// <summary>
    /// 成套商品憑證掃碼確認 Input
    /// </summary>
    public class VerifyGroupCouponCheckModel
    {
        /// <summary>
        /// couponSerial
        /// </summary>
        public string QRCode { set; get; }
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
    }
    /// <summary>
    /// 成套商品憑證掃碼確認 output
    /// </summary>
    public class VerifyCheckGroupCouponOutputModel
    {
        /// <summary>
        /// OrderGuid
        /// </summary>
        public Guid OrderGuid { set; get; }
        /// <summary>
        /// Timestamp
        /// </summary>
        public string Timestamp { set; get; }
        /// <summary>
        /// Detail
        /// </summary>
        public List<VbsApiVerifyCouponInfo> Detail { set; get; }
    }
    /// <summary>
    /// 憑證針對 trustId 核銷
    /// </summary>
    public class VerifyCouponModel
    {
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// TrustId
        /// </summary>
        public Guid TrustId { set; get; }
    }

    /// <summary>
    /// 成套憑證針對 trustid 陣列核銷
    /// </summary>
    public class UndoVerifiedGroupCoupon
    {
        /// <summary>
        /// TrustIds
        /// </summary>
        public List<Guid> TrustIds { get; set; }
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
    }
    /// <summary>
    /// 成套商品針對 trustId 核銷
    /// </summary>
    public class VerifyGroupCouponModel
    {
        /// <summary>
        /// OrderGuid
        /// </summary>
        public Guid OrderGuid { set; get; }
        /// <summary>
        /// Timestamp
        /// </summary>
        public string Timestamp { set; get; }
        /// <summary>
        /// StoreCode
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// TrustId
        /// </summary>
        public List<Guid> TrustId { set; get; }
    }
}

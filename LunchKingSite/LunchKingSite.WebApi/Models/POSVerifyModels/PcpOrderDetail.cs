﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LunchKingSite.Core;

namespace LunchKingSite.WebApi.Models.POSVerifyModels
{
    /// <summary>
    /// 上傳交易明細相關資訊 Input
    /// </summary>
    public class PcpOrderDetailInputModel
    {
        /// <summary>
        /// 店家分店代碼
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// 店家分店名稱
        /// </summary>
        public string StoreName { set; get; }

        /// <summary>
        /// 交易明細資料
        /// </summary>
        public List<PcpOrderDetail> OrderDetail { get; set; }
    }

    /// <summary>
    /// 交易明細相關資訊 
    /// </summary>
    public class PcpOrderDetail
    {
        /// <summary>
        /// 關聯店代碼
        /// </summary>
        public string RelatedStoreCode { set; get; }

        /// <summary>
        /// 關聯店名
        /// </summary>
        public string RelatedStoreName { set; get; }

        /// <summary>
        /// 關聯訂單單號
        /// </summary>
        public string RelatedOrderId { set; get; }

        /// <summary>
        /// OrderId(GUID)
        /// </summary>
        public string OrderId { set; get; }

        /// <summary>
        /// 熟客卡識別碼
        /// </summary>IdentityCode
        public string IdentityCode { set; get; }

        /// <summary>
        /// 交易時間
        /// </summary>
        public DateTime TradeTime { set; get; }

        /// <summary>
        /// 產品編號
        /// </summary>
        public string ItemId { set; get; }

        /// <summary>
        /// 產品名稱
        /// </summary>
        public string ItemName { set; get; }

        /// <summary>
        /// 產品規格
        /// </summary>
        public string ItemSpec { set; get; }

        /// <summary>
        /// 產品備註
        /// </summary>
        public string ItemNotes { set; get; }

        /// <summary>
        /// 價格
        /// </summary>
        public double Price { set; get; }

        /// <summary>
        /// 數量
        /// </summary>
        public long Qty { set; get; }

        /// <summary>
        /// 單位
        /// </summary>
        public string Unit { set; get; }
    }

    /// <summary>
    /// 上傳交易明細相關資訊 output
    /// </summary>
    public class PcpOrderDetailInputModelData
    {
        /// <summary>
        /// Input總OrderDetail數
        /// </summary>
        public int TotleDetail { get; set; }

        /// <summary>
        /// 成功寫入Detail的總數
        /// </summary>
        public int SuccessDetailCount { get; set; }

        /// <summary>
        /// 不同總數
        /// </summary>
        public int DiffDetailCount { get; set; }

        /// <summary>
        /// 失敗總數
        /// </summary>
        public int FailDetailCount { get; set; }

        /// <summary>
        /// 資料不同步的Detail
        /// </summary>
        public List<PcpOrderKeyInfo> DiffOrderDetail { get; set; }

        /// <summary>
        /// 失敗的Detail
        /// </summary>
        public List<PcpOrderResult> FailOrderDetail { get; set; }
    }

    /// <summary>
    /// 上傳交易明細相關錯誤資訊 output
    /// </summary>
    public class PcpOrderResult : PcpOrderKeyInfo
    {
        /// <summary>
        /// 錯誤代碼
        /// </summary>
        public ApiResultCode Code { set; get; }

        /// <summary>
        /// 錯誤訊息
        /// </summary>
        public string Message { set; get; }
    }

    /// <summary>
    /// 上傳交易明細相關資訊 output
    /// </summary>
    public class PcpOrderKeyInfo
    {
        /// <summary>
        /// OrderId(GUID)
        /// </summary>
        public string OrderId { set; get; }

        /// <summary>
        /// 熟客卡識別碼
        /// </summary>
        public string IdentityCode { set; get; }

    }
}

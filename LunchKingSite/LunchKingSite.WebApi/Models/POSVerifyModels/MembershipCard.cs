﻿using System;

namespace LunchKingSite.WebApi.Models.POSVerifyModels
{
    /// <summary>
    /// MembershipCardCheckModel
    /// </summary>
    public class PosMembershipCardCheckModel
    {
        /// <summary>
        /// Pcp Identity Code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 外部商家代碼
        /// </summary>
        public string StoreCode { get; set; }
        /// <summary>
        /// 外部商家分店名稱
        /// </summary>
        public string StoreName { get; set; }
    }

    /// <summary>
    /// 會員資料
    /// </summary>
    public class MemberInfo
    {
        /// <summary>
        /// 會員名稱
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 性別 0:未知 1:男性 2:女性
        /// </summary>
        public int Gender { get; set; }
        /// <summary>
        /// 手機號碼
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 生日
        /// </summary>
        public DateTime Birthday { get; set; }
    }

    /// <summary>
    /// Pcp 會員卡資訊
    /// </summary>
    public class MembershipCardData
    {
        /// <summary>
        /// 卡片等級
        /// </summary>
        public int CardLevel { get; set; }
        /// <summary>
        /// 折數
        /// </summary>
        public double PaymentPercent { set; get; }
        /// <summary>
        /// 限制條件
        /// </summary>
        public string Instruction { get; set; }
        /// <summary>
        /// 其他優惠
        /// </summary>
        public string OtherPremiums { set; get; }
        /// <summary>
        /// 適用日期類型
        /// </summary>
        public int AvailableDateType { get; set; }
        /// <summary>
        /// 適用日期說明
        /// </summary>
        public string AvailableDateDesc { get; set; }
        /// <summary>
        /// 消費次數
        /// </summary>
        public int OrderCount { get; set; }
        /// <summary>
        /// 總消費金額
        /// </summary>
        public decimal AmountTotal { get; set; }
        /// <summary>
        /// 最近一次使用時間
        /// </summary>
        public DateTime LastUseTime { get; set; }
    }

    /// <summary>
    /// Pcp 會員卡 check data
    /// </summary>
    public class MembershipCardCheckData
    {
        /// <summary>
        /// Identity_code.id
        /// </summary>
        public long IdentityCodeId { set; get; }
        /// <summary>
        /// 會員資料
        /// </summary>
        public MemberInfo MemberInfo { get; set; }
        /// <summary>
        /// 卡片資料
        /// </summary>
        public MembershipCardData Card { get; set; }
    }

    /// <summary>
    /// 核銷 Pcp 會員卡 input model
    /// </summary>
    public class MembershipCardVerifyModel
    {
        /// <summary>
        /// Identity_code.id
        /// </summary>
        public long IdentityCodeId { set; get; }
        /// <summary>
        /// 消費金額
        /// </summary>
        public double? Amount { set; get; }
        /// <summary>
        /// 折扣後消費金額
        /// </summary>
        public double? DiscountAmount { set; get; }
        /// <summary>
        /// 店家分店代碼
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// 店家分店名稱
        /// </summary>
        public string StoreName { set; get; }
    }

    /// <summary>
    /// 核銷 Pcp 會員卡回傳資料
    /// </summary>
    public class MembershipCardVerifyData
    {
        /// <summary>
        /// Pcp_Order guid
        /// </summary>
        public Guid OrderId { set; get; }
        /// <summary>
        /// 本次消費後是否升等
        /// </summary>
        public bool IsLevelUp { set; get; }
        /// <summary>
        /// 升等後卡片名稱
        /// </summary>
        public string LevelUpMsg { set; get; }
        /// <summary>
        /// 消費次數
        /// </summary>
        public int OrderCount { get; set; }
        /// <summary>
        /// 總消費金額
        /// </summary>
        public decimal AmountTotal { get; set; }
    }

    /// <summary>
    /// 取銷 Pcp 會員卡核銷 input model
    /// </summary>
    public class MembershipCardCancelModel
    {
        /// <summary>
        /// 取銷訂單 Guid
        /// </summary>
        public Guid OrderId { get; set; }
        /// <summary>
        /// 店家分店代碼
        /// </summary>
        public string StoreCode { set; get; }
        /// <summary>
        /// 店家分店名稱
        /// </summary>
        public string StoreName { set; get; }
    }

    /// <summary>
    /// 熟客卡取消核銷回傳值
    /// </summary>
    public class MembershipCardCancelData
    {
        /// <summary>
        /// 本次核銷完後是否有降級卡片
        /// </summary>
        public bool IsLevelDown { set; get; }
        /// <summary>
        /// 配合isLevelDown，若有降級則顯示降級後的熟客卡等級(ex:金卡)
        /// </summary>
        public string LevelDownMsg { set; get; }
        /// <summary>
        /// 消費次數
        /// </summary>
        public int OrderCount { get; set; }
        /// <summary>
        /// 總消費金額
        /// </summary>
        public double AmountTotal { get; set; }
    }
}

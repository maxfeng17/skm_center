﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Component.API;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.Enumeration;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using LunchKingSite.WebApi.Core.Attributes;
using LunchKingSite.WebApi.Core.OAuth;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [RequireHttps(false)]
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class VourcherServiceController : BaseController
    {
        /// <summary>
        /// 取得優惠券相關的 搜尋選單，包含 區域、商家類型、排序條件
        /// </summary>
        /// <param name="cityId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetVourcherQueryData(int cityId, string userId)
        {
            const string methodName = "GetVourcherQueryData";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var categories = ApiVourcherManager.ApiSellerSampleCategoryGetListByCityId(cityId);
            var regions = ApiVourcherManager.ApiVourcherRegionGetList();
            var sortTypes = ApiVourcherManager.ApiVourcherSortTypeGetList();

            result.Data = new { Categories = categories, Regions = regions, SortTypes = sortTypes };

            SetApiLog(GetMethodName(methodName), userId, new { cityId = cityId, userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 取得城市區域清單
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetCitysJSon(string userId)
        {
            const string methodName = "GetCitysJSon";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var jsonCol = ApiVourcherManager.ApiVourcherRegionGetList();

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 回傳賣家類別(列舉 SellerSampleCategory)
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetSellerSampleCategory(string userId)
        {
            const string methodName = "GetSellerSampleCategory";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var jsonCol = ApiVourcherManager.ApiSellerSampleCategoryGetList();

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依據傳遞城市編號回傳賣家類別(列舉 SellerSampleCategory)
        /// </summary>
        /// <param name="cityId">城市</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetSellerSampleCategoryByCity(int cityId, string userId)
        {
            const string methodName = "GetSellerSampleCategoryByCity";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var jsonCol = ApiVourcherManager.ApiSellerSampleCategoryGetListByCityId(cityId);

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { cityId= cityId ,userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 取得排序條件的選項
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetVourcherSortTypeList(string userId)
        {
            const string methodName = "GetVourcherSortTypeList";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            //轉成類似ListItem格式(Text,Value)
            var jsonCol = ApiVourcherManager.ApiVourcherSortTypeGetList();

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市編號</param>
        /// <param name="category">類型</param>
        /// <param name="orderByType">排序方式</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetVourcherStoreListByRegionIdAndCategoryWithOrderBy(string pageStart, string pageLength, string regionId
            , string category, string orderByType, string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetVourcherStoreListByRegionIdAndCategoryWithOrderBy";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int cityId = int.TryParse(regionId, out cityId) ? cityId : 0;


            SellerSampleCategory? sellerCategory = null;
            if (category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if (Enum.TryParse(category, out paresCategory))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    return result;
                }
            }
            VourcherSortType sortType;
            if (!Enum.TryParse(orderByType, out sortType))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }
            int distanceParse;
            int? distanceLength = null;
            if (!string.IsNullOrWhiteSpace(distance))
            {
                if (!int.TryParse(distance, out distanceParse))
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    return result;
                }
                distanceLength = distanceParse;
            }
            //將傳入的經緯度轉型，若失敗，設為null。
            ApiCoordinates coordinates;
            if (!ApiCoordinates.TryParse(longitude, latitude, out coordinates))
            {
                coordinates = null;
            }


            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetList(pagestart, pagelength, cityId,
                                                                                       sellerCategory, sortType,
                                                                                       coordinates,
                                                                                       distanceLength);

            result.Data = stores;

            //紀錄LOG
            object parameter = new
            {
                pageStart = pageStart,
                pageLength = pageLength,
                regionId = regionId,
                category = category,
                orderByType = orderByType,
                latitude = latitude,
                longitude = longitude,
                distance = distance,
                userId = userId
            };
            object rtnValue = new { count = (stores == null ? 0 : stores.Count), message = "回傳資料筆數" };

            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 查詢優惠店家列表，用於地圖顯示 作廢
        /// </summary>
        /// <param name="regionId">城市編號</param>
        /// <param name="category">類型</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetVourcherStoreListByRegionIdAndCategoryForMap(string regionId, string category
            , string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetVourcherStoreListByRegionIdAndCategoryForMap";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int cityId = int.TryParse(regionId, out cityId) ? cityId : 0;


            SellerSampleCategory? sellerCategory = null;
            if (category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if (Enum.TryParse(category, out paresCategory))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    return result;
                }
            }

            int distanceParse = 0;
            if (!int.TryParse(distance, out distanceParse))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }
            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetListForMapShow(cityId,
                                                                                       sellerCategory,
                                                                                       latitude, longitude,
                                                                                       distanceParse);

            result.Data = stores;

            //紀錄LOG
            object parameter = new
            {
                regionId = regionId,
                category = category,
                latitude = latitude,
                longitude = longitude,
                distance = distance,
                userId = userId
            };
            object rtnValue = new { count = (stores == null ? 0 : stores.Count), message = "回傳資料筆數" };

            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 查詢優惠店家列表，用於地圖顯示
        /// </summary>
        /// <param name="category">類型</param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetVourcherStoreListByCategoryForMap(string category
            , string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetVourcherStoreListByCategoryForMap";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            SellerSampleCategory? sellerCategory = null;
            if (category.Trim() != "-1")
            {
                SellerSampleCategory paresCategory;
                if (Enum.TryParse(category, out paresCategory))
                {
                    sellerCategory = paresCategory;
                }
                else
                {
                    result.Code = ApiResultCode.InputError;
                    result.Message = I18N.Phrase.ApiResultCodeInputError;
                    return result;
                }
            }

            int distanceParse = 0;
            if (!int.TryParse(distance, out distanceParse))
            {
                result.Code = ApiResultCode.InputError;
                result.Message = I18N.Phrase.ApiResultCodeInputError;
                return result;
            }
            List<ApiVourcherStore> stores = ApiVourcherManager.ApiVourcherStoreGetListForMapShow(sellerCategory,
                                                                                       latitude, longitude,
                                                                                       distanceParse);

            result.Data = stores;

            //紀錄LOG
            object parameter = new
            {
                category = category,
                latitude = latitude,
                longitude = longitude,
                distance = distance,
                userId = userId
            };
            object rtnValue = new { count = (stores == null ? 0 : stores.Count), message = "回傳資料筆數" };

            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依城市搜尋賣家及優惠券資訊列表 預定作廢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="regionId">城市區域id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetViewVourcherStoreFacadeCollectionByRegionId(string pageStart, string pageLength, string regionId, string userId)
        {
            const string methodName = "GetViewVourcherStoreFacadeCollectionByRegionId";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int id = int.TryParse(regionId, out id) ? id : 0;

            var jsonCol = VourcherFacade.ViewVourcherStoreFacadeCollectionGetByRegionId(pagestart, pagelength, id, string.Empty).GroupBy(x => new { x.SellerId, x.SellerName, x.SellerLogoimgPath })
                .Select(x => new
                {
                    SellerId = x.Key.SellerId,
                    SellerName = x.Key.SellerName,
                    SellerLogoimgPath = x.Key.SellerLogoimgPath,
                    AttentionCount = x.Sum(s => s.VourcherPageCount),
                    Distance = string.Empty,
                    VourcherContent = x.FirstOrDefault() != null ? x.FirstOrDefault().Contents : string.Empty,
                    StartDate = x.FirstOrDefault() != null ? x.FirstOrDefault().StartDate : DateTime.Now,
                    VourcherCount = x.GroupBy(y => y.VourcherEventId).Count(),
                }).OrderByDescending(g => g.StartDate);

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { pageStart = pageStart, pageLength = pageLength, regionId = regionId, userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }


        /// <summary>
        /// 依經緯度搜尋賣家及優惠券資訊列表 預定作廢
        /// </summary>
        /// <param name="pageStart"></param>
        /// <param name="pageLength"></param>
        /// <param name="latitude">緯度座標</param>
        /// <param name="longitude">經度座標</param>
        /// <param name="distance">查詢距離經緯度的距離</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetViewVourcherStoreFacadeCollectionByCoordinate(string pageStart, string pageLength, string latitude, string longitude, string distance, string userId)
        {
            const string methodName = "GetViewVourcherStoreFacadeCollectionByCoordinate";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int storeDistance = int.TryParse(distance, out storeDistance) ? storeDistance : 0;

            var jsonCol = VourcherFacade.ViewVourcherStoreFacadeCollectionGetByCoordinate(pagestart, pagelength, storeDistance, latitude, longitude, string.Empty).GroupBy(x => new { x.SellerId, x.SellerName, x.SellerLogoimgPath })
                .Select(x => new
                {
                    SellerId = x.Key.SellerId,
                    SellerName = x.Key.SellerName,
                    SellerLogoimgPath = x.Key.SellerLogoimgPath,
                    AttentionCount = x.Sum(s => s.PageCount),
                    Distance = string.Empty,
                    VourcherContent = x.FirstOrDefault() != null ? x.FirstOrDefault().Contents : string.Empty,
                    StartDate = x.FirstOrDefault() != null ? x.FirstOrDefault().StartDate : DateTime.Now,
                    VourcherCount = x.GroupBy(y => y.VourcherEventId).Count(),
                }).OrderByDescending(g => g.StartDate);

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { pageStart = pageStart, pageLength = pageLength, latitude = latitude, longitude = longitude, distance = distance, userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 回傳賣家平均消費(列舉 SellerConsumptionAvg)  預定作廢
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetSellerConsumptionAvg(string userId)
        {
            const string methodName = "GetSellerConsumptionAvg";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            var jsonCol = VourcherFacade.GetSellerConsumptionAvgDictionary().Select(x => new { Text = x.Value, Value = x.Key });

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { userId = userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依賣家id搜尋優惠券內容
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="sellerId">賣家id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetViewVourcherSellerBySellerId(string pageStart, string pageLength, string sellerId, string userId)
        {
            const string methodName = "GetViewVourcherSellerBySellerId";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonCol = ApiVourcherManager.ApiVourcherEventSynopsesGetList(pagestart, pagelength, sellerId);

            result.Data = jsonCol;


            //紀錄LOG
            object parameter = new
            {
                pageStart = pageStart,
                pageLength = pageLength,
                sellerId = sellerId,
                userId = userId
            };
            object rtnValue = new { count = (jsonCol == null ? 0 : jsonCol.Count), message = "回傳資料筆數" };

            SetApiLog(GetMethodName(methodName), userId, parameter, rtnValue, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依優惠券id搜尋優惠券內容(查取優惠券內容時，關注數會加1)
        /// </summary>
        /// <param name="eventId">優惠券id</param>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId, userName")]
        public ApiResult GetViewVourcherSellerByEventId(string eventId, string latitude, string longitude, string userId)
        {
            const string methodName = "GetViewVourcherSellerByEventId";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int eventid = int.TryParse(eventId, out eventid) ? eventid : 0;
            //取得會員名稱
            int? uniqueId = null;
            string userName = HttpContext.Current.User.Identity.Name;
            if (!string.IsNullOrWhiteSpace(userName))
            {
                uniqueId = MemberFacade.GetUniqueId(userName);
            }

            var jsonData = ApiVourcherManager.ApiVourcherEventGet(eventid, latitude, longitude, uniqueId);

            if (userId == "AND2012250752")//針對andiord舊版app調整。
            {
                if (!string.IsNullOrWhiteSpace(jsonData.Restriction))
                {
                    jsonData.Instruction = jsonData.Instruction + "\n" + "不適用：" + jsonData.Restriction;
                }
                if (!string.IsNullOrWhiteSpace(jsonData.Others))
                {
                    jsonData.Instruction = jsonData.Instruction + "\n" + jsonData.Others;
                }
            }
            result.Data = jsonData;

            //紀錄LOG
            object parameter = new
            {
                eventId = eventId,
                latitude = latitude,
                longitude = longitude,
                userId = userId
            };

            SetApiLog(GetMethodName(methodName), userId, parameter, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依優惠券id搜尋所有分店 預定作廢
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="eventId">優惠券id</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetViewVourcherStoreCollectionGetByEventId(string pageStart, string pageLength, string eventId, string userId)
        {
            const string methodName = "GetViewVourcherStoreCollectionGetByEventId";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;
            int eventid = int.TryParse(eventId, out eventid) ? eventid : 0;

            var jsonCol = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(pagestart, pagelength, eventid, string.Empty)
                .Select(x => new
                {
                    Id = x.Id,
                    EventId = x.VourcherEventId,
                    StoreName = x.StoreName,
                    Address = x.City + x.Town + x.AddressString,
                    Tel = x.Phone,
                    SellerGuid = x.SellerGuid
                });

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { pageStart, pageLength, eventId, userId }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依分店guid搜尋分店資訊 預定作廢
        /// </summary>
        /// <param name="storeGuid">分店guid</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult GetStoreByGuid(string storeGuid, string userId)
        {
            const string methodName = "GetStoreByGuid";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            Guid storeguid = Guid.TryParse(storeGuid, out storeguid) ? storeguid : Guid.Empty;
            Store store = VourcherFacade.StoreGetByGuid(storeguid);

            var jsonCol = new
            {
                StoreGuid = store.Guid,
                StoreName = store.StoreName,
                Tel = store.Phone,
                OpenTime = store.OpenTime,
                CloseDate = store.CloseDate,
                WebUrl = store.WebUrl,
                BlogUrl = store.BlogUrl,
                CreditCard = store.CreditcardAvailable,
                Mrt = store.Mrt,
                Car = store.Car,
                Bus = store.Bus,
                OtherVehicles = store.OtherVehicles
            };

            result.Data = jsonCol;

            SetApiLog(GetMethodName(methodName), userId, new { storeGuid , userId}, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 收藏/取消收藏 優惠券
        /// </summary>
        /// <param name="eventId">優惠券id</param>
        /// <param name="status">收藏狀態</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId, userName")]
        public ApiResult SetVourcherCollect(int eventId, int status, string userId)
        {
            const string methodName = "SetVourcherCollect";
            ApiResult result = new ApiResult();

            //檢查會員是否已登入
            string userName = HttpContext.Current.User.Identity.Name;

            int uId = MemberFacade.GetUniqueId(userName);

            VourcherFacade.SetVourcherCollect(uId, eventId, (VourcherCollectStatus)status);
            result.Code = ApiResultCode.Success;
            result.Data = "";


            //紀錄LOG
            object parameter = new
            {
                eventId = eventId,
                status = status,
                userId = userId
            };

            SetApiLog(GetMethodName(methodName), userId, parameter, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 使用優惠券
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="eventId">優惠券id</param>
        /// <param name="status">使用狀態</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId, userName")]
        public ApiResult SetVourcherOrder(string userId, int eventId, int status)
        {
            const string methodName = "SetVourcherOrder";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            //檢查會員是否已登入
            string userName = HttpContext.Current.User.Identity.Name;

            int uId = MemberFacade.GetUniqueId(userName);

            VourcherFacade.SetVourcherOrder(uId, eventId, (VourcherOrderStatus)status);

            result.Data = "";

            //紀錄LOG
            object parameter = new
            {
                eventId = eventId,
                status = status,
                userId = userId
            };

            SetApiLog(GetMethodName(methodName), userId, parameter, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依使用者id搜尋收藏的優惠券
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId, userName")]
        public ApiResult ViewVourcherSellerCollectGetList(string pageStart, string pageLength, string userId)
        {
            const string methodName = "ViewVourcherSellerCollectGetList";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            //檢查會員是否已登入
            string userName = HttpContext.Current.User.Identity.Name;

            int uId = MemberFacade.GetUniqueId(userName);

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonCol = ApiVourcherManager.ApiVourcherEventSynopsesGetUserCollectListByUserId(pagestart, pagelength, uId);

            result.Data = jsonCol;

            //紀錄LOG
            object parameter = new
            {
                pageStart = pageStart,
                pageLength = pageLength,
                userId = userId
            };

            SetApiLog(GetMethodName(methodName), userId, parameter, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 依使用者id搜尋使用的優惠券 預定作廢
        /// </summary>
        /// <param name="pageStart">起頁(-1全選)</param>
        /// <param name="pageLength">每頁大小</param>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId, userName")]
        public ApiResult ViewVourcherSellerOrderGetList(string pageStart, string pageLength, string userId)
        {
            const string methodName = "ViewVourcherSellerOrderGetList";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            //檢查會員是否已登入
            string userName = HttpContext.Current.User.Identity.Name;

            int uId = MemberFacade.GetUniqueId(userName);

            int pagestart = int.TryParse(pageStart, out pagestart) ? pagestart : -1;
            int pagelength = int.TryParse(pageLength, out pagelength) ? pagelength : 10;

            var jsonCol = VourcherFacade.ViewVourcherSellerOrderGetList(pagestart, pagelength, uId, string.Empty)
                .Select(x => new
                {
                    Id = x.Id,
                    Contents = x.Contents,
                    Instruction = x.Instruction,
                    Restriction = x.Restriction,
                    EndDate = ApiSystemManager.DateTimeToDateTimeString((x.EndDate.Value)),
                    AttentionCount = x.PageCount,
                    Mode = x.Mode,
                    MaxQuantity = x.MaxQuantity,
                    CurrentQuantity = x.CurrentQuantity,
                    PicUrl = ImageFacade.GetMediaPathsFromRawData(x.PicUrl,
                                                    MediaType.PponDealPhoto).ToList(),
                    SellerName = x.SellerName
                });

            result.Data = jsonCol;

            //紀錄LOG
            object parameter = new
            {
                pageStart = pageStart,
                pageLength = pageLength,
                userId = userId
            };


            SetApiLog(GetMethodName(methodName), userId, parameter, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 行銷Banner
        /// Type=(int)VourcherPromoType.Seller=0;以賣家sellerid作為連結(帶出所有該賣家的優惠券)
        /// Type=(int)VourcherPromoType.Vourcher=1;直接帶出該優惠券連結
        /// Banner圖片為賣家logo
        /// </summary>
        /// <param name="userId">呼叫API的client端裝置或合作機關的帳號</param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        [MemberValidate(true, "userId")]
        public ApiResult VourcherPromoDataGetList(string userId)
        {
            const string methodName = "VourcherPromoDataGetList";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };


            var jsonCol = ApiVourcherManager.ApiVourcherPromoDataGetList();

            result.Data = jsonCol;

            //紀錄LOG
            object rtnValue = new { count = (jsonCol == null ? 0 : jsonCol.Count), message = "回傳資料筆數" };

            SetApiLog(GetMethodName(methodName), userId, new { userId}, rtnValue, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult TodayVourchers()
        {
            const string methodName = "TodayVourchers";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            List<ApiToyotaVourcherEvent> apiToyotaVourcherEventCol = new List<ApiToyotaVourcherEvent>();

            ViewVourcherSellerCollection viewVourcherSellerCollection =
                VourcherFacade.TodayVourcherEventCollectionGet(DateTime.Now.Date);

            foreach (var item in viewVourcherSellerCollection)
            {
                var apiToyotaVourcherEvent = GetApiToyotaVourcherEvent(item);
                if (apiToyotaVourcherEvent != null)
                {
                    apiToyotaVourcherEventCol.Add(apiToyotaVourcherEvent);
                }
            }

            result.Data = apiToyotaVourcherEventCol;

            SetApiLog(GetMethodName(methodName), string.Empty, null, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        private ApiToyotaVourcherEvent GetApiToyotaVourcherEvent(ViewVourcherSeller vourcherSeller)
        {
            //商家說明
            Seller seller = ProviderFactory.Instance().GetProvider<ISellerProvider>().SellerGet(vourcherSeller.SellerGuid);
            string sellerDescription = string.Empty;
            if (seller.IsLoaded)
            {
                sellerDescription = seller.SellerDescription;
            }

            ApiToyotaVourcherEvent apiVourcherEvent = new ApiToyotaVourcherEvent();
            apiVourcherEvent.Id = vourcherSeller.Id;
            apiVourcherEvent.Contents = vourcherSeller.Contents;
            apiVourcherEvent.Instruction = vourcherSeller.Instruction;
            apiVourcherEvent.Restriction = vourcherSeller.Restriction;
            apiVourcherEvent.Others = vourcherSeller.Others;
            apiVourcherEvent.EndDate = ApiSystemManager.DateTimeToDateTimeString(vourcherSeller.EndDate.Value);
            apiVourcherEvent.Mode = (VourcherEventMode)vourcherSeller.Mode;
            apiVourcherEvent.MaxQuantity = vourcherSeller.MaxQuantity;
            apiVourcherEvent.CurrentQuantity = vourcherSeller.CurrentQuantity;
            apiVourcherEvent.PicUrl = ImageFacade.GetMediaPathsFromRawData(vourcherSeller.PicUrl, MediaType.PponDealPhoto).ToList();
            apiVourcherEvent.SellerName = vourcherSeller.SellerName;
            apiVourcherEvent.SellerGuid = vourcherSeller.SellerGuid;
            apiVourcherEvent.SellerCategory = (vourcherSeller.SellerCategory.HasValue) ? vourcherSeller.SellerCategory.Value : (int)SellerSampleCategory.Others;
            apiVourcherEvent.SellerDescription = sellerDescription;
            apiVourcherEvent.ConsumptionAvg = (SellerConsumptionAvg?)vourcherSeller.SellerConsumptionAvg; //平均店消費

            //分店
            ViewVourcherStoreCollection stores = VourcherFacade.ViewVourcherStoreCollectionGetByEventId(0, 0, vourcherSeller.Id, string.Empty);
            apiVourcherEvent.EventStores = new List<ApiToyotaVourcherEventStore>();
            foreach (ViewVourcherStore store in stores)
            {
                ApiToyotaVourcherEventStore apiStore = new ApiToyotaVourcherEventStore();

                apiStore.Id = store.Id;
                apiStore.StoreGuid = store.StoreGuid;
                apiStore.StoreName = store.StoreName;
                apiStore.Address = store.City + store.Town + store.AddressString;
                apiStore.Phone = store.Phone;
                apiStore.OpenTime = store.OpenTime;
                apiStore.CloseDate = store.CloseDate;
                apiStore.Remarks = store.Remarks;
                Microsoft.SqlServer.Types.SqlGeography storeGeo = LocationFacade.GetGeographyByCoordinate(store.Coordinate);
                if (storeGeo != null && storeGeo != Microsoft.SqlServer.Types.SqlGeography.Null)
                {
                    double geoLong;
                    double geoLat;
                    apiStore.Longitude = (double.TryParse(storeGeo.Long.ToString(), out geoLong)) ? geoLong : default(double);
                    apiStore.Latitude = (double.TryParse(storeGeo.Lat.ToString(), out geoLat)) ? geoLat : default(double);
                }
                else
                {
                    apiStore.Longitude = default(double);
                    apiStore.Latitude = default(double);
                }

                apiVourcherEvent.EventStores.Add(apiStore);
            }
            apiVourcherEvent.AttentionCount = vourcherSeller.PageCount;

            return apiVourcherEvent;
        }

    }
}

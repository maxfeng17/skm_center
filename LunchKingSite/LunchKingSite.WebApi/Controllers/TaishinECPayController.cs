﻿using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.BizLogic.Models;
using LunchKingSite.Core;
using LunchKingSite.Core.Component;
using LunchKingSite.Core.ModelPartial;
using LunchKingSite.DataOrm;
using LunchKingSite.WebApi.Controllers;
using LunchKingSite.WebApi.Core.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using LunchKingSite.WebApi.Models.Channel;
using LunchKingSite.WebApi.Core.OAuth;
using System.Net.Http.Formatting;
using log4net;
using Newtonsoft.Json.Linq;

namespace LunchKingSite.Controllers
{
    public class TaishinECPayController : BaseController
    {
        private ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        private ISellerProvider sp = ProviderFactory.Instance().GetProvider<ISellerProvider>();
        private ISkmProvider skm = ProviderFactory.Instance().GetProvider<ISkmProvider>();
        private ILog logger = LogManager.GetLogger("TaishinECPayService");

        /// <summary>
        /// 取本站頻道
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetChannelList()
        {
            CategoryTypeNode channelList = CategoryManager.PponChannelCategoryTree;

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(channelList.CategoryNodes.
                Select(x => new
                {
                    x.CategoryId,
                    x.CategoryName,
                    x.IsFinal,
                    x.IsShowBackEnd,
                    x.IsShowFrontEnd,
                    x.Seq
                })), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 取本站所有分類
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetCategoryList()
        {
            var allCategory = sp.CategoryGetAll(Category.Columns.Type);

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(allCategory.
                Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Rank,
                    x.Code,
                    x.Type,
                    x.Status,
                    x.IsFinal,
                    x.IconType,
                    x.Image

                })), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 取本站所有分類排除新光+頻道
        /// </summary>
        [HttpPost]
        public HttpResponseMessage GetCategoryListExcludedSome()
        {
            var allCategory = sp.CategoryGetAll(Category.Columns.Type)
                .Where(i => i.Type != (int)CategoryType.CommercialCircle && i.Type != (int)CategoryType.PponChannel);

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(allCategory.
                Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.Rank,
                    x.Code,
                    x.Type,
                    x.Status,
                    x.IsFinal,
                    x.IconType,
                    x.Image

                })), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 取本站分類by頻道
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetCategoryByChannel()
        {
            var channelList = CategoryManager.PponChannelCategoryTree;
            var categoryList = new List<CentralCategory>();
            foreach (var channelNode in channelList.CategoryNodes)
            {
                List<CategoryNode> categoryMain = channelNode.GetSubCategoryByCategoryId(channelNode.CategoryId);
                //List<CategoryNode> categoryMain = channelNode.GetSubCategoryTypeNode(CategoryType.DealCategory);
                if (categoryMain != null && categoryMain.Count > 0)
                {
                    foreach (var categoryNode in categoryMain)
                    {
                        categoryList.Add(new CentralCategory(channelNode.CategoryId, categoryNode.CategoryId, categoryNode.CategoryName, categoryNode.CategoryType, categoryNode.Seq));

                        List<CategoryNode> categoryMinor = categoryNode.GetSubCategoryTypeNode(CategoryType.DealCategory);
                        if (categoryMinor != null && categoryMain.Count > 0)
                        {
                            foreach (var category in categoryMinor)
                            {
                                categoryList.Add(new CentralCategory(channelNode.CategoryId, category.CategoryId, category.CategoryName, category.CategoryType, category.Seq));
                            }
                        }
                    }
                }
            }
            ApiCategoryNode categoryNode24h = PponDealPreviewManager.GetArrival24hCategoryNode();
            foreach (ApiCategoryTypeNode typeNode in categoryNode24h.NodeDatas)
            {
                foreach (ApiCategoryNode nodeLvl1 in typeNode.CategoryNodes)
                {
                    categoryList.Add(new CentralCategory(categoryNode24h.CategoryId, nodeLvl1.CategoryId, nodeLvl1.CategoryName, nodeLvl1.CategoryType, nodeLvl1.Seq));
                }
            }

            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(categoryList), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 本站分類頻道關聯表
        /// </summary>
        [HttpGet]
        public HttpResponseMessage GetCategoryDependency()
        {
            return new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(
                    sp.CategoryDependencyGetAll(CategoryDependency.Columns.ParentId)
                    .Select(x => new
                    {
                        x.ParentId,
                        x.CategoryId,
                        x.Seq
                    })
                    ), Encoding.UTF8, "application/json")
            };
        }

        /// <summary>
        /// 本站商品
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage GetItemList(string categoryId)
        {
            List<IViewPponDeal> deals = ViewPponDealManager.DefaultManager.ViewPponDealGetList(false, true);
            var skmSellerList = skm.SkmShoppeGetAll().Select(r => r.SellerGuid).ToList();

            if (deals != null && deals.Count > 0)
            {
                var postDeals = deals.Where(d => !skmSellerList.Contains(d.SellerGuid) &&
                //d.MainBid == d.BusinessHourGuid && 
                d.CategoryList.Replace("[", ",").Replace("]", ",").Contains("," + categoryId + ",")).ToList();

                return new HttpResponseMessage()
                {
                    Content = new StringContent(JsonConvert.SerializeObject(postDeals), Encoding.UTF8, "application/json")
                };
            }
            else
            {
                return null;
            }
        }

        #region 發票相關

        [HttpPost]
        //[OAuthScope(TokenScope.Entrepot)]
        public HttpResponseMessage ECPayEinvoiceSet(EcPayEinvoice einvoice)
        {
            ApiReturnObject resultObject;
            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Ready,
                Data = ""
            };
            try
            {
                logger.Info("[ECPayEinvoiceSet]" + JsonConvert.SerializeObject(einvoice));
                if (config.EnbaledTaishinEcPayApi)
                {
                    resultObject.Code = ApiReturnCode.Success;
                    Dictionary<string, int> couponInfo = new Dictionary<string, int>();
                    //開立
                    if (einvoice.CouponInfo != null && einvoice.CouponInfo.Count > 0)
                    {
                        couponInfo = einvoice.CouponInfo.ToDictionary(x => x.CouponId.ToString(), y => int.Parse(y.Amount));
                    }
                    else
                    {
                        couponInfo.Add(string.Empty, int.Parse(einvoice.Amount));
                    }
                    //check data
                    if (einvoice.CarrierType == ((int)CarrierType.Phone).ToString())
                    {
                        if (!(CheckCarrid(einvoice.CarrierId) == "Y" || CheckCarrid(einvoice.CarrierId) == "P"))
                        {
                            resultObject.Code = ApiReturnCode.InputError;
                            resultObject.Message = "手機條碼輸入錯誤";
                            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
                        }
                    }

                    if (einvoice.InvoiceStatus == ((int)EinvoiceType.C0401).ToString())
                    {
                        EinvoiceFacade.PartnerEinvoiceMainSet(einvoice.OrderId, einvoice.ComId, einvoice.ComName, einvoice.BuyerName, einvoice.BuyerAddress, einvoice.BuyerEmail, einvoice.ItemName, couponInfo, int.Parse(einvoice.Amount), int.Parse(einvoice.NoTax), int.Parse(einvoice.CarrierType), einvoice.CarrierId, AgentChannel.LifeEntrepot);
                        resultObject.Code = ApiReturnCode.Success2;
                        resultObject.Message = "呼叫成功";
                    }
                    else if (einvoice.InvoiceStatus == ((int)EinvoiceType.C0701).ToString())
                    {
                        //變更發票資訊
                         EinvoiceFacade.PartnerEinvoiceMainVoid(einvoice.OrderId, einvoice.ComId, einvoice.ComName, einvoice.BuyerName, einvoice.BuyerAddress, einvoice.ItemName, couponInfo, int.Parse(einvoice.NoTax), int.Parse(einvoice.CarrierType), einvoice.CarrierId, AgentChannel.LifeEntrepot);
                        resultObject.Code = ApiReturnCode.Success2;
                        resultObject.Message = "呼叫成功";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("[ex]" + ex.Message);
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        [HttpPost]
        //[OAuthScope(TokenScope.Entrepot)]
        public HttpResponseMessage ECPayEinvoiceCancel(EcPayEinvoice einvoice)
        {
            ApiReturnObject resultObject;
            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Ready,
                Data = ""
            };
            try
            {
                logger.Info("[ECPayEinvoiceCancel]" + JsonConvert.SerializeObject(einvoice));
                if (config.EnbaledTaishinEcPayApi)
                {
                    resultObject.Code = ApiReturnCode.Success;
                    Dictionary<string, int> couponInfo = new Dictionary<string, int>();
                    
                    if (einvoice.CouponInfo != null && einvoice.CouponInfo.Count > 0)
                    {
                        couponInfo = einvoice.CouponInfo.ToDictionary(x => x.CouponId.ToString(), y => int.Parse(y.Amount));
                    }
                    else
                    {
                        couponInfo.Add(string.Empty, int.Parse(einvoice.Amount));
                    }
                    //check data

                    if (einvoice.InvoiceStatus == ((int)EinvoiceType.D0401).ToString() || einvoice.InvoiceStatus == ((int)EinvoiceType.C0501).ToString())
                    {
                        EinvoiceType invoiceStatus = einvoice.InvoiceStatus == ((int)EinvoiceType.D0401).ToString() ? EinvoiceType.D0401 : EinvoiceType.C0501;
                        EinvoiceFacade.PartnerEinvoiceMainCancel(einvoice.OrderId, invoiceStatus, couponInfo, int.Parse(einvoice.Amount), AgentChannel.LifeEntrepot);
                        resultObject.Code = ApiReturnCode.Success2;
                        resultObject.Message = "呼叫成功";
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("[ex]" + ex.Message);
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        [HttpPost]
        //[OAuthScope(TokenScope.Entrepot)]
        public HttpResponseMessage ECPayEinvoiceGet(EcPayEinvoice einvoice)
        {
            ApiReturnObject resultObject;
            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.Ready,
                Data = ""
            };

            try
            {
                logger.Info("[ECPayEinvoiceGet]" + JsonConvert.SerializeObject(einvoice));
                if (config.EnbaledTaishinEcPayApi)
                {
                    int pid = 0;
                    int.TryParse(einvoice.Pid, out pid);
                    var data = EinvoiceFacade.PartnerEinvoiceMainGet(einvoice.OrderId, einvoice.InvoiceNumber, (AgentChannel)pid);
                    if (data.Count > 0)
                    {
                        var model = data.Where(x => x.InvoiceStatus > (int)EinvoiceType.Initial && !string.IsNullOrEmpty(x.InvoiceNumber)).Select(x => new {
                            OrderId = x.PartnerOrderId,
                            InvoiceNumber = x.InvoiceNumber,
                            InvoiceNumberTime = x.InvoiceNumberTime.Value.ToString("yyyy/MM/dd HH:mm:ss"),
                            InvoiceStatus = x.InvoiceStatus.ToString(),
                            InvoiceAmount = ((int)x.OrderAmount).ToString(),
                            CouponId = x.CouponId == null ? string.Empty : x.CouponId.ToString(),
                            CarrierId = x.CarrierId,
                            CarrierType = x.CarrierType.ToString(),
                            LoveCode = x.LoveCode ?? string.Empty,
                            IsWinning = Convert.ToInt32(x.InvoiceWinning).ToString(),
                            PaperedTime = x.InvoicePaperedTime == null ? string.Empty : x.InvoicePaperedTime.Value.ToString("yyyy/MM/dd HH:mm:ss")
                        }).ToArray();
                        resultObject = new ApiReturnObject
                        {
                            Code = ApiReturnCode.Success2,
                            Message = "呼叫成功",
                            Data = new LunchKingSite.Core.JsonSerializer().Serialize(model)
                        };
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("[ex]" + ex.Message);
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }

        [HttpPost]
        //[OAuthScope(TokenScope.Entrepot)]
        public HttpResponseMessage ECPayWinnerEinvoiceGet(EcPayWinnerEinvoice model)
        {
            ApiReturnObject resultObject;
            resultObject = new ApiReturnObject
            {
                Code = ApiReturnCode.DataNotExist,
                Data = "查無此code。"
            };

            try
            {
                logger.Info("[ECPayWinnerEinvoiceGet]" + JsonConvert.SerializeObject(model));
                int pid = 0;
                int.TryParse(model.Pid, out pid);
                var data = EinvoiceFacade.PartnerWinnerEinvoiceGet(model.DateCode, pid);
                if (data != null)
                {
                    var winnerInvNumbers = data.Select(x => x.InvoiceNumber).ToArray();
                    resultObject = new ApiReturnObject
                    {
                        Code = ApiReturnCode.Success2,
                        Message = "呼叫成功",
                        Data = new LunchKingSite.Core.JsonSerializer().Serialize(winnerInvNumbers)
                    };
                }
            }
            catch (Exception ex)
            {
                logger.Error("[ex]" + ex.Message);
                resultObject = new ApiReturnObject
                {
                    Code = ApiReturnCode.InputError,
                    Message = "輸入的參數錯誤"
                };
                return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
            }

            return new HttpResponseMessage() { Content = new ObjectContent<ApiReturnObject>(resultObject, new JsonMediaTypeFormatter(), "application/json") };
        }
        #endregion

        private string CheckCarrid(string carrid)
        {
            try
            {
                string TxID = System.DateTime.Now.ToString("yyyyMMddHHmmss"); //暫無用途
                string targetUrl = string.Format(@"https://www-vc.einvoice.nat.gov.tw/BIZAPIVAN/biz?version=1.0&action=bcv&barCode={0}&TxID={1}&appId=EINV1201410300635", System.Web.HttpUtility.UrlEncode(carrid), TxID);

                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(targetUrl);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.Timeout = 10000;

                string result = "";
                // 取得回應資料
                using (System.Net.HttpWebResponse response = (System.Net.HttpWebResponse)request.GetResponse())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                    {
                        result = sr.ReadToEnd();
                    }
                }
                JObject jb = Newtonsoft.Json.JsonConvert.DeserializeObject<JObject>(result);
                return jb.Property("code").Value.ToString() == "200" ? jb.Property("isExist").Value.ToString() : string.Empty;
                //return "N";
            }
            catch
            {
                //發生不明錯誤則PASS手機載具驗證
                return "P";
            }
        }
    }
}

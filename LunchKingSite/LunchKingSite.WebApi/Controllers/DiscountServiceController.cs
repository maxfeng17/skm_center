﻿using System;
using LunchKingSite.BizLogic.Component;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.BizLogic.Model.API;
using LunchKingSite.Core;
using LunchKingSite.DataOrm;
using System.Collections.Generic;
using System.Web.Http;
using LunchKingSite.WebApi.Core.Attributes;

namespace LunchKingSite.WebApi.Controllers
{
    /// <summary>
    /// 
    /// </summary>
    [MvcStyleBinding]//不要再使用MvcStyleBinding
    public class DiscountServiceController : BaseController
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <param name="user_name"></param>
        /// <param name="bid"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetDiscountAmountByUserAndDep(string code, string user_name, string bid)
        {
            const string methodName = "GetDiscountAmountByUserAndDep";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };

            int flag;
            string msg;
            KeyValuePair<decimal, decimal> kv = PromotionFacade.GetAmountCheckDepFromDiscountCode(code.ToUpper(), user_name, (int)DiscountCampaignUsedFlags.Ppon, bid, out flag, out msg, false);

            result.Data = new { Discount = kv.Key, Minimum = kv.Value, Flag = flag };

            SetApiLog(GetMethodName(methodName), string.Empty, new { code, user_name, bid }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }
        
        /// <summary>
        /// 原本直接帶user_name的方式，應該僅量的淘汱掉
        /// 登入才能查詢
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ApiResult GetDiscountAmount(GetDiscountAmountByUserInputModel model)
        {
            string code = model.Code;
            string bid = model.Bid;
            const string methodName = "GetDiscountAmount";
            ApiResult result = new ApiResult { Code = ApiResultCode.Success };

            string userName = User.Identity.Name;

            int flag;
            string msg;
            KeyValuePair<decimal, decimal> kv = PromotionFacade.GetAmountCheckDepFromDiscountCode(
                code.ToUpper(), userName, (int)DiscountCampaignUsedFlags.Ppon, bid, out flag, out msg);

            result.Data = new { Discount = kv.Key, Minimum = kv.Value, Flag = flag };

            SetApiLog(GetMethodName(methodName), string.Empty, new { code, userName, bid }, result, Helper.GetClientIP());

            return result;
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cardnum"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetCreditCardBankInfo(string cardnum)
        {
            const string methodName = "GetCreditCardInfo";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };
            CreditcardBankInfo info = CreditCardPremiumManager.GetCreditCardBankInfo(cardnum);
            result.Data = new { BankName = info.BankName ?? string.Empty, Id = info.Id };

            SetApiLog(GetMethodName(methodName), string.Empty, new { cardnum }, result, LunchKingSite.Core.Helper.GetClientIP());

            return result;
        }

        /// <summary>
        /// 檢查是否為visa卡 折價券折抵適用 (2016/10/13 很少用，但還是有在用)
        /// </summary>
        /// <param name="cardnum"></param>
        /// <returns></returns>
        [HttpGet, HttpPost]
        public ApiResult GetVisaCardCheck(string cardnum)
        {
            const string methodName = "GetVisaCardCheck";
            ApiResult result = new ApiResult() { Code = ApiResultCode.Success };
            //bool is_visa = CreditCardPremiumManager.IsVisaCardNumber(cardnum);
            result.Data = new { IsVisa = true };
            SetApiLog(GetMethodName(methodName), string.Empty, new { cardnum }, result, LunchKingSite.Core.Helper.GetClientIP());
            return result;
        }


        #region input models 

        /// <summary>
        /// 
        /// </summary>
        public class GetDiscountAmountByUserInputModel
        {
            /// <summary>
            /// 
            /// </summary>
            public string Code { get; set; }
            /// <summary>
            /// 
            /// </summary>
            public string Bid { get; set; }
        }

        #endregion
    }
}

﻿using System;
using System.Web.Http;
using LunchKingSite.BizLogic.Facade;
using LunchKingSite.Core;
using LunchKingSite.WebLib.Component;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;
using LunchKingSite.Core.Component;

namespace LunchKingSite.WebApi.Controllers
{
    public class WebEditController : ApiController
    {
        static ISysConfProvider config = ProviderFactory.Instance().GetConfig();
        static ILog logger = LogManager.GetLogger(typeof(WebHelper));
        [HttpPost]
        public dynamic LoadPageBlock(LoadPageBlockModel model)
        {
            string dirPath = Path.Combine(
                config.WebAppDataPath, "web", "pageBlocks");
            string fileName = model.name + ".html";

            string filePath = System.IO.Path.Combine(dirPath, fileName);
            if (string.IsNullOrEmpty(model.version) == false)
            {
                filePath = string.Format("{0}.{1}.bak", filePath, model.version);
            }
            string html = string.Empty;
            string desc = string.Empty;
            List<dynamic> versions = new List<dynamic>();
            versions.Add(new { text = "最新", value = "" });
            if (System.IO.File.Exists(filePath))
            {
                html = System.IO.File.ReadAllText(filePath);
                desc = "於" + LunchKingSite.Core.Helper
                    .GetFriendlyTimeDesc(File.GetLastWriteTime(filePath)) + "修改";

                DateTime now = DateTime.Now;
                foreach (var fi in new DirectoryInfo(dirPath).GetFiles("*.*").OrderByDescending(t => t.Name))
                {
                    if (fi.Name == fileName)
                    {
                        continue;
                    }
                    if (fi.Name.Contains(model.name) == false)
                    {
                        continue;
                    }
                    if ((now - fi.CreationTime).TotalDays > 365)
                    {
                        continue;
                    }
                    string name = fi.Name.Replace(fileName, string.Empty);
                    name = Path.GetFileNameWithoutExtension(name);
                    name = name.Trim(new char[] { '.' });
                    versions.Add(new { text = name, value = name });
                }

                return new
                {
                    success = true,
                    content = html,
                    desc = desc,
                    versions = versions
                };
            }
            return new
            {
                success = false,
                content = html
            };
        }
        [HttpPost]
        public dynamic SavePageBlock(SavePageBlockModel model)
        {
            string filePath = Path.Combine(
                config.WebAppDataPath, "web", "pageBlocks", model.name + ".html");
            if (string.IsNullOrEmpty(model.name))
            {
                return new
                {
                    success = false,
                };
            }
            string newText = model.content.Trim();
            string oldText = string.Empty;
            string message = string.Empty;
            try
            {
                if (File.Exists(filePath))
                {
                    oldText = File.ReadAllText(filePath).Trim();
                    if (newText != oldText)
                    {
                        string backupName = filePath + "." + DateTime.Now.ToString("yyyyMMdd-HHmmss") + ".bak";
                        File.Move(filePath, backupName);
                    }
                    else
                    {
                        message = "沒有異動";
                    }
                }
                File.WriteAllText(filePath, newText);
            }
            catch (Exception ex)
            {
                throw new Exception("error at write " + filePath, ex);
            }
            SystemFacade.IncrementImageVer();
            return new
            {
                success = true,
                message = message
            };
        }

        public class LoadPageBlockModel
        {
            public string name { get; set; }
            public string version { get; set; }
        }
        public class SavePageBlockModel
        {
            public string name { get; set; }
            public string content { get; set; }
        }
    }



}
